.class public Lcom/miui/expose/utils/MethodHolder;
.super Ljava/lang/Object;


# instance fields
.field private volatile initialized:Z

.field private volatile method:Ljava/lang/reflect/Method;

.field private name:Ljava/lang/String;

.field private parameterTypes:Lcom/miui/expose/utils/ParameterTypes;

.field private type:Lcom/miui/expose/utils/ClassHolder;


# direct methods
.method public constructor <init>(Lcom/miui/expose/utils/ClassHolder;Ljava/lang/String;Lcom/miui/expose/utils/ParameterTypes;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/miui/expose/utils/MethodHolder;->type:Lcom/miui/expose/utils/ClassHolder;

    iput-object p2, p0, Lcom/miui/expose/utils/MethodHolder;->name:Ljava/lang/String;

    iput-object p3, p0, Lcom/miui/expose/utils/MethodHolder;->parameterTypes:Lcom/miui/expose/utils/ParameterTypes;

    return-void
.end method

.method private buildExceptionMessage()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/miui/expose/utils/MethodHolder;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/miui/expose/utils/MethodHolder;->parameterTypes:Lcom/miui/expose/utils/ParameterTypes;

    invoke-interface {v1}, Lcom/miui/expose/utils/ParameterTypes;->get()[Ljava/lang/Class;

    move-result-object v1

    array-length v2, v1

    if-lez v2, :cond_0

    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    :goto_0
    array-length v3, v1

    if-ge v2, v3, :cond_0

    const-string v3, ", "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v3, v1, v2

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " is not found in "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/miui/expose/utils/MethodHolder;->type:Lcom/miui/expose/utils/ClassHolder;

    invoke-virtual {v1}, Lcom/miui/expose/utils/ClassHolder;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public varargs invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    iget-boolean v0, p0, Lcom/miui/expose/utils/MethodHolder;->initialized:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/expose/utils/MethodHolder;->initialized:Z

    :try_start_0
    iget-object v1, p0, Lcom/miui/expose/utils/MethodHolder;->parameterTypes:Lcom/miui/expose/utils/ParameterTypes;

    invoke-interface {v1}, Lcom/miui/expose/utils/ParameterTypes;->get()[Ljava/lang/Class;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/expose/utils/MethodHolder;->type:Lcom/miui/expose/utils/ClassHolder;

    invoke-virtual {v2}, Lcom/miui/expose/utils/ClassHolder;->get()Ljava/lang/Class;

    move-result-object v2

    iget-object v3, p0, Lcom/miui/expose/utils/MethodHolder;->name:Ljava/lang/String;

    invoke-virtual {v2, v3, v1}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    iput-object v1, p0, Lcom/miui/expose/utils/MethodHolder;->method:Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v1, p0, Lcom/miui/expose/utils/MethodHolder;->method:Ljava/lang/reflect/Method;

    invoke-virtual {v1, v0}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    goto :goto_0

    :catch_0
    move-exception p1

    new-instance p2, Lcom/miui/expose/utils/ReflectiveOperationError;

    invoke-direct {p2, p1}, Lcom/miui/expose/utils/ReflectiveOperationError;-><init>(Ljava/lang/Throwable;)V

    throw p2

    :cond_0
    iget-object v0, p0, Lcom/miui/expose/utils/MethodHolder;->method:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_1

    :goto_0
    :try_start_1
    iget-object v0, p0, Lcom/miui/expose/utils/MethodHolder;->method:Ljava/lang/reflect/Method;

    invoke-virtual {v0, p1, p2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1
    :try_end_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_1

    return-object p1

    :catch_1
    move-exception p1

    new-instance p2, Lcom/miui/expose/utils/ReflectiveOperationError;

    invoke-direct {p2, p1}, Lcom/miui/expose/utils/ReflectiveOperationError;-><init>(Ljava/lang/Throwable;)V

    throw p2

    :cond_1
    new-instance p1, Lcom/miui/expose/utils/ReflectiveOperationError;

    new-instance p2, Ljava/lang/NoSuchMethodException;

    invoke-direct {p0}, Lcom/miui/expose/utils/MethodHolder;->buildExceptionMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p2, v0}, Ljava/lang/NoSuchMethodException;-><init>(Ljava/lang/String;)V

    invoke-direct {p1, p2}, Lcom/miui/expose/utils/ReflectiveOperationError;-><init>(Ljava/lang/Throwable;)V

    throw p1
.end method
