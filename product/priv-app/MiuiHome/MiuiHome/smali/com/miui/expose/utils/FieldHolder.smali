.class public Lcom/miui/expose/utils/FieldHolder;
.super Ljava/lang/Object;


# instance fields
.field private volatile field:Ljava/lang/reflect/Field;

.field private fieldType:Ljava/lang/Object;

.field private volatile initialized:Z

.field private name:Ljava/lang/String;

.field private type:Lcom/miui/expose/utils/ClassHolder;


# direct methods
.method public constructor <init>(Lcom/miui/expose/utils/ClassHolder;Ljava/lang/String;Lcom/miui/expose/utils/ClassHolder;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/miui/expose/utils/FieldHolder;->type:Lcom/miui/expose/utils/ClassHolder;

    iput-object p2, p0, Lcom/miui/expose/utils/FieldHolder;->name:Ljava/lang/String;

    iput-object p3, p0, Lcom/miui/expose/utils/FieldHolder;->fieldType:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Lcom/miui/expose/utils/ClassHolder;Ljava/lang/String;Ljava/lang/Class;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/miui/expose/utils/ClassHolder;",
            "Ljava/lang/String;",
            "Ljava/lang/Class<",
            "*>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/miui/expose/utils/FieldHolder;->type:Lcom/miui/expose/utils/ClassHolder;

    iput-object p2, p0, Lcom/miui/expose/utils/FieldHolder;->name:Ljava/lang/String;

    iput-object p3, p0, Lcom/miui/expose/utils/FieldHolder;->fieldType:Ljava/lang/Object;

    return-void
.end method

.method private buildExceptionMessage(Ljava/lang/Class;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "*>;)",
            "Ljava/lang/String;"
        }
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/miui/expose/utils/FieldHolder;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-direct {p0}, Lcom/miui/expose/utils/FieldHolder;->resolveType()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ") "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "is not found in "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/miui/expose/utils/FieldHolder;->type:Lcom/miui/expose/utils/ClassHolder;

    invoke-virtual {v1}, Lcom/miui/expose/utils/ClassHolder;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz p1, :cond_0

    const-string v1, "; "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "do you mean "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/miui/expose/utils/FieldHolder;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ")"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "?"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private ensureInitialized()V
    .locals 4

    iget-boolean v0, p0, Lcom/miui/expose/utils/FieldHolder;->initialized:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/expose/utils/FieldHolder;->initialized:Z

    :try_start_0
    iget-object v1, p0, Lcom/miui/expose/utils/FieldHolder;->type:Lcom/miui/expose/utils/ClassHolder;

    invoke-virtual {v1}, Lcom/miui/expose/utils/ClassHolder;->get()Ljava/lang/Class;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/expose/utils/FieldHolder;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-virtual {v1}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v2

    invoke-direct {p0}, Lcom/miui/expose/utils/FieldHolder;->resolveType()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iput-object v1, p0, Lcom/miui/expose/utils/FieldHolder;->field:Ljava/lang/reflect/Field;

    iget-object v1, p0, Lcom/miui/expose/utils/FieldHolder;->field:Ljava/lang/reflect/Field;

    invoke-virtual {v1, v0}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/miui/expose/utils/ReflectiveOperationError;

    new-instance v2, Ljava/lang/NoSuchFieldException;

    invoke-virtual {v1}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/miui/expose/utils/FieldHolder;->buildExceptionMessage(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/NoSuchFieldException;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v2}, Lcom/miui/expose/utils/ReflectiveOperationError;-><init>(Ljava/lang/Throwable;)V

    throw v0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/miui/expose/utils/ReflectiveOperationError;

    invoke-direct {v1, v0}, Lcom/miui/expose/utils/ReflectiveOperationError;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_1
    iget-object v0, p0, Lcom/miui/expose/utils/FieldHolder;->field:Ljava/lang/reflect/Field;

    if-eqz v0, :cond_2

    :goto_0
    return-void

    :cond_2
    new-instance v0, Lcom/miui/expose/utils/ReflectiveOperationError;

    new-instance v1, Ljava/lang/NoSuchFieldException;

    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/miui/expose/utils/FieldHolder;->buildExceptionMessage(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/NoSuchFieldException;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lcom/miui/expose/utils/ReflectiveOperationError;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method private resolveType()Ljava/lang/Class;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/miui/expose/utils/FieldHolder;->fieldType:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/Class;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/Class;

    return-object v0

    :cond_0
    check-cast v0, Lcom/miui/expose/utils/ClassHolder;

    invoke-virtual {v0}, Lcom/miui/expose/utils/ClassHolder;->get()Ljava/lang/Class;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lcom/miui/expose/utils/FieldHolder;->ensureInitialized()V

    :try_start_0
    iget-object v0, p0, Lcom/miui/expose/utils/FieldHolder;->field:Ljava/lang/reflect/Field;

    invoke-virtual {v0, p1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    new-instance v0, Lcom/miui/expose/utils/ReflectiveOperationError;

    invoke-direct {v0, p1}, Lcom/miui/expose/utils/ReflectiveOperationError;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method
