.class Lcom/miui/home/launcher/LauncherThemeController;
.super Ljava/lang/Object;


# instance fields
.field private mLauncher:Lcom/miui/home/launcher/Launcher;


# direct methods
.method constructor <init>(Lcom/miui/home/launcher/Launcher;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/miui/home/launcher/LauncherThemeController;->mLauncher:Lcom/miui/home/launcher/Launcher;

    return-void
.end method


# virtual methods
.method public sendEvent(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/miui/home/launcher/LauncherThemeController;->mLauncher:Lcom/miui/home/launcher/Launcher;

    iget-object v0, v0, Lcom/miui/home/launcher/Launcher;->mMaMlViews:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/home/launcher/maml/MaMlWidgetView;

    invoke-virtual {v1, p1}, Lcom/miui/home/launcher/maml/MaMlWidgetView;->sendEvent(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    return-void
.end method
