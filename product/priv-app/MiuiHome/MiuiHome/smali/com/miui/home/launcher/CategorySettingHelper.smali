.class public Lcom/miui/home/launcher/CategorySettingHelper;
.super Lcom/miui/home/launcher/common/BaseSharePreference;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/home/launcher/CategorySettingHelper$Holder;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 1

    const-string v0, "category"

    invoke-direct {p0, v0}, Lcom/miui/home/launcher/common/BaseSharePreference;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/miui/home/launcher/CategorySettingHelper$1;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/home/launcher/CategorySettingHelper;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/miui/home/launcher/CategorySettingHelper;
    .locals 1

    invoke-static {}, Lcom/miui/home/launcher/CategorySettingHelper$Holder;->access$100()Lcom/miui/home/launcher/CategorySettingHelper;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public disableAppCategoryUpdateEnable()V
    .locals 2

    const-string v0, "app_category_update_enable"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/miui/home/launcher/CategorySettingHelper;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public getAppCategoryUpdateVersionCode()I
    .locals 2

    const-string v0, "app_category_update_version_code"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/miui/home/launcher/CategorySettingHelper;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method getAppCategoryVersion(I)I
    .locals 2

    goto/32 :goto_3

    nop

    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_5

    nop

    :goto_1
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_7

    nop

    :goto_2
    invoke-virtual {p0, p1, v0}, Lcom/miui/home/launcher/CategorySettingHelper;->getInt(Ljava/lang/String;I)I

    move-result p1

    goto/32 :goto_6

    nop

    :goto_3
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_1

    nop

    :goto_4
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/32 :goto_0

    nop

    :goto_5
    const/4 v0, 0x0

    goto/32 :goto_2

    nop

    :goto_6
    return p1

    :goto_7
    const-string v1, "app_category_version"

    goto/32 :goto_8

    nop

    :goto_8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_4

    nop
.end method

.method public isAppCategoryUpdateEnable()Z
    .locals 2

    const-string v0, "app_category_update_enable"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/miui/home/launcher/CategorySettingHelper;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public isInitAppCategory()Z
    .locals 2

    const-string v0, "app_category_init"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/miui/home/launcher/CategorySettingHelper;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method setAppCategoryUpdateVersionCode()V
    .locals 2

    goto/32 :goto_2

    nop

    :goto_0
    invoke-static {}, Lcom/miui/home/launcher/Application;->getInstance()Lcom/miui/home/launcher/Application;

    move-result-object v1

    goto/32 :goto_1

    nop

    :goto_1
    invoke-virtual {v1}, Lcom/miui/home/launcher/Application;->getCurrentVersion()I

    move-result v1

    goto/32 :goto_4

    nop

    :goto_2
    const-string v0, "app_category_update_version_code"

    goto/32 :goto_0

    nop

    :goto_3
    return-void

    :goto_4
    invoke-virtual {p0, v0, v1}, Lcom/miui/home/launcher/CategorySettingHelper;->putInt(Ljava/lang/String;I)V

    goto/32 :goto_3

    nop
.end method

.method setAppCategoryVersion(II)V
    .locals 2

    goto/32 :goto_7

    nop

    :goto_0
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/32 :goto_3

    nop

    :goto_1
    invoke-virtual {p0, p1, p2}, Lcom/miui/home/launcher/CategorySettingHelper;->putInt(Ljava/lang/String;I)V

    goto/32 :goto_4

    nop

    :goto_2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_0

    nop

    :goto_3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_1

    nop

    :goto_4
    return-void

    :goto_5
    const-string v1, "app_category_version"

    goto/32 :goto_2

    nop

    :goto_6
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_5

    nop

    :goto_7
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_6

    nop
.end method

.method public setInitAppCategoryDone()V
    .locals 2

    const-string v0, "app_category_init"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/miui/home/launcher/CategorySettingHelper;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method
