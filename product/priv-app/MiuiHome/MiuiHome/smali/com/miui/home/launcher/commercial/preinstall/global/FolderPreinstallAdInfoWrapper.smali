.class public Lcom/miui/home/launcher/commercial/preinstall/global/FolderPreinstallAdInfoWrapper;
.super Ljava/lang/Object;


# instance fields
.field private final mOriginalAd:Lcom/miui/msa/preinstall/v1/PreinstallAdInfo;


# direct methods
.method constructor <init>(Lcom/miui/msa/preinstall/v1/PreinstallAdInfo;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/miui/home/launcher/commercial/preinstall/global/FolderPreinstallAdInfoWrapper;->mOriginalAd:Lcom/miui/msa/preinstall/v1/PreinstallAdInfo;

    return-void
.end method


# virtual methods
.method public getIconUri()Landroid/net/Uri;
    .locals 3

    iget-object v0, p0, Lcom/miui/home/launcher/commercial/preinstall/global/FolderPreinstallAdInfoWrapper;->mOriginalAd:Lcom/miui/msa/preinstall/v1/PreinstallAdInfo;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/miui/msa/preinstall/v1/PreinstallAdInfo;->getIconUri()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    return-object v1

    :cond_1
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method getOriginalAd()Lcom/miui/msa/preinstall/v1/PreinstallAdInfo;
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Lcom/miui/home/launcher/commercial/preinstall/global/FolderPreinstallAdInfoWrapper;->mOriginalAd:Lcom/miui/msa/preinstall/v1/PreinstallAdInfo;

    goto/32 :goto_1

    nop

    :goto_1
    return-object v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/home/launcher/commercial/preinstall/global/FolderPreinstallAdInfoWrapper;->mOriginalAd:Lcom/miui/msa/preinstall/v1/PreinstallAdInfo;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/miui/msa/preinstall/v1/PreinstallAdInfo;->getPackageName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/home/launcher/commercial/preinstall/global/FolderPreinstallAdInfoWrapper;->mOriginalAd:Lcom/miui/msa/preinstall/v1/PreinstallAdInfo;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/miui/msa/preinstall/v1/PreinstallAdInfo;->getTitle()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
