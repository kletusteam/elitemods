.class public Lcom/miui/home/launcher/allapps/recommend/RecommendAppProviderManager;
.super Ljava/lang/Object;


# static fields
.field private static volatile mRecommendAppProviderManager:Lcom/miui/home/launcher/allapps/recommend/RecommendAppProviderManager;


# instance fields
.field private mCallBack:Lcom/miui/launcher/appprediction/GoogleRecommendApp$OnDateChangedListener;

.field private mGoogleRecommendApp:Lcom/miui/launcher/appprediction/GoogleRecommendApp;

.field private mMiuiRecommendApp:Lcom/miui/home/launcher/allapps/recommend/MiuiRecommendApp;


# direct methods
.method private constructor <init>()V
    .locals 5

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/miui/launcher/appprediction/GoogleRecommendApp;

    invoke-static {}, Lcom/miui/home/launcher/Application;->getInstance()Lcom/miui/home/launcher/Application;

    move-result-object v1

    invoke-static {}, Lcom/miui/home/launcher/DeviceConfig;->getAllAppsRecommendCount()I

    move-result v2

    sget-object v3, Lcom/miui/home/recents/util/Executors;->UI_HELPER_EXECUTOR:Lcom/miui/home/library/utils/LooperExecutor;

    invoke-static {}, Lcom/miui/home/launcher/allapps/settings/AllAppsSettingHelper;->getInstance()Lcom/miui/home/launcher/allapps/settings/AllAppsSettingHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/miui/home/launcher/allapps/settings/AllAppsSettingHelper;->isRecommendAppsEnable()Z

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/miui/launcher/appprediction/GoogleRecommendApp;-><init>(Landroid/content/Context;ILjava/util/concurrent/Executor;Z)V

    iput-object v0, p0, Lcom/miui/home/launcher/allapps/recommend/RecommendAppProviderManager;->mGoogleRecommendApp:Lcom/miui/launcher/appprediction/GoogleRecommendApp;

    new-instance v0, Lcom/miui/home/launcher/allapps/recommend/MiuiRecommendApp;

    invoke-direct {v0}, Lcom/miui/home/launcher/allapps/recommend/MiuiRecommendApp;-><init>()V

    iput-object v0, p0, Lcom/miui/home/launcher/allapps/recommend/RecommendAppProviderManager;->mMiuiRecommendApp:Lcom/miui/home/launcher/allapps/recommend/MiuiRecommendApp;

    return-void
.end method

.method private getGoogleRecommendApps(Ljava/util/List;Ljava/util/List;I)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/miui/home/launcher/AppInfo;",
            ">;",
            "Ljava/util/List<",
            "Lcom/miui/launcher/appprediction/AppPredictionInfo;",
            ">;I)",
            "Ljava/util/List<",
            "Lcom/miui/home/launcher/AppInfo;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "rankData\uff1a"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/launcher/appprediction/AppPredictionInfo;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/miui/home/launcher/AppInfo;

    invoke-virtual {v4}, Lcom/miui/home/launcher/AppInfo;->getClassName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2}, Lcom/miui/launcher/appprediction/AppPredictionInfo;->getClassName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v4}, Lcom/miui/home/launcher/AppInfo;->getUser()Landroid/os/UserHandle;

    move-result-object v5

    invoke-virtual {v2}, Lcom/miui/launcher/appprediction/AppPredictionInfo;->getUser()Landroid/os/UserHandle;

    move-result-object v6

    if-ne v5, v6, :cond_1

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v5, p3, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Lcom/miui/home/launcher/AppInfo;->getTitle()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Lcom/miui/home/launcher/AppInfo;->getUser()Landroid/os/UserHandle;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v3, "];"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v2, p3, :cond_0

    :cond_3
    const-string p1, "AppPredictionTag"

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-object v0
.end method

.method public static getInstance()Lcom/miui/home/launcher/allapps/recommend/RecommendAppProviderManager;
    .locals 2

    sget-object v0, Lcom/miui/home/launcher/allapps/recommend/RecommendAppProviderManager;->mRecommendAppProviderManager:Lcom/miui/home/launcher/allapps/recommend/RecommendAppProviderManager;

    if-nez v0, :cond_1

    const-class v0, Lcom/miui/home/launcher/allapps/recommend/RecommendAppProviderManager;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/miui/home/launcher/allapps/recommend/RecommendAppProviderManager;->mRecommendAppProviderManager:Lcom/miui/home/launcher/allapps/recommend/RecommendAppProviderManager;

    if-nez v1, :cond_0

    new-instance v1, Lcom/miui/home/launcher/allapps/recommend/RecommendAppProviderManager;

    invoke-direct {v1}, Lcom/miui/home/launcher/allapps/recommend/RecommendAppProviderManager;-><init>()V

    sput-object v1, Lcom/miui/home/launcher/allapps/recommend/RecommendAppProviderManager;->mRecommendAppProviderManager:Lcom/miui/home/launcher/allapps/recommend/RecommendAppProviderManager;

    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_1
    :goto_0
    sget-object v0, Lcom/miui/home/launcher/allapps/recommend/RecommendAppProviderManager;->mRecommendAppProviderManager:Lcom/miui/home/launcher/allapps/recommend/RecommendAppProviderManager;

    return-object v0
.end method


# virtual methods
.method public getRecommendApps(Ljava/util/List;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/miui/home/launcher/AppInfo;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/miui/home/launcher/AppInfo;",
            ">;"
        }
    .end annotation

    invoke-static {}, Lcom/miui/home/launcher/DeviceConfig;->getAllAppsRecommendCount()I

    move-result v0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iget-object v1, p0, Lcom/miui/home/launcher/allapps/recommend/RecommendAppProviderManager;->mGoogleRecommendApp:Lcom/miui/launcher/appprediction/GoogleRecommendApp;

    invoke-virtual {v1}, Lcom/miui/launcher/appprediction/GoogleRecommendApp;->isSupportPredict()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/miui/home/launcher/allapps/recommend/RecommendAppProviderManager;->mGoogleRecommendApp:Lcom/miui/launcher/appprediction/GoogleRecommendApp;

    invoke-virtual {v1}, Lcom/miui/launcher/appprediction/GoogleRecommendApp;->getPredictList()Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, p1, v1, v0}, Lcom/miui/home/launcher/allapps/recommend/RecommendAppProviderManager;->getGoogleRecommendApps(Ljava/util/List;Ljava/util/List;I)Ljava/util/List;

    move-result-object p1

    return-object p1

    :cond_0
    iget-object v1, p0, Lcom/miui/home/launcher/allapps/recommend/RecommendAppProviderManager;->mMiuiRecommendApp:Lcom/miui/home/launcher/allapps/recommend/MiuiRecommendApp;

    invoke-virtual {v1, p1, v0}, Lcom/miui/home/launcher/allapps/recommend/MiuiRecommendApp;->getRecommendApps(Ljava/util/List;I)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public notifyAppTargetEvent(Landroid/os/UserHandle;Landroid/content/ComponentName;)V
    .locals 1

    iget-object v0, p0, Lcom/miui/home/launcher/allapps/recommend/RecommendAppProviderManager;->mGoogleRecommendApp:Lcom/miui/launcher/appprediction/GoogleRecommendApp;

    invoke-virtual {v0, p1, p2}, Lcom/miui/launcher/appprediction/GoogleRecommendApp;->notifyAppTargetEvent(Landroid/os/UserHandle;Landroid/content/ComponentName;)V

    return-void
.end method

.method public registerPredictionUpdates()V
    .locals 1

    invoke-static {}, Lcom/miui/home/launcher/allapps/settings/AllAppsSettingHelper;->getInstance()Lcom/miui/home/launcher/allapps/settings/AllAppsSettingHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/home/launcher/allapps/settings/AllAppsSettingHelper;->isRecommendAppsEnable()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/miui/home/launcher/allapps/recommend/RecommendAppProviderManager;->mGoogleRecommendApp:Lcom/miui/launcher/appprediction/GoogleRecommendApp;

    invoke-virtual {v0}, Lcom/miui/launcher/appprediction/GoogleRecommendApp;->registerPredictionUpdates()V

    :cond_0
    return-void
.end method

.method public setPredictAppListener(Lcom/miui/launcher/appprediction/GoogleRecommendApp$OnDateChangedListener;)V
    .locals 1

    iget-object v0, p0, Lcom/miui/home/launcher/allapps/recommend/RecommendAppProviderManager;->mCallBack:Lcom/miui/launcher/appprediction/GoogleRecommendApp$OnDateChangedListener;

    if-nez v0, :cond_0

    iput-object p1, p0, Lcom/miui/home/launcher/allapps/recommend/RecommendAppProviderManager;->mCallBack:Lcom/miui/launcher/appprediction/GoogleRecommendApp$OnDateChangedListener;

    iget-object p1, p0, Lcom/miui/home/launcher/allapps/recommend/RecommendAppProviderManager;->mGoogleRecommendApp:Lcom/miui/launcher/appprediction/GoogleRecommendApp;

    iget-object v0, p0, Lcom/miui/home/launcher/allapps/recommend/RecommendAppProviderManager;->mCallBack:Lcom/miui/launcher/appprediction/GoogleRecommendApp$OnDateChangedListener;

    invoke-virtual {p1, v0}, Lcom/miui/launcher/appprediction/GoogleRecommendApp;->setPredictAppListener(Lcom/miui/launcher/appprediction/GoogleRecommendApp$OnDateChangedListener;)V

    :cond_0
    return-void
.end method

.method public unRegisterPredictionUpdates()V
    .locals 1

    iget-object v0, p0, Lcom/miui/home/launcher/allapps/recommend/RecommendAppProviderManager;->mGoogleRecommendApp:Lcom/miui/launcher/appprediction/GoogleRecommendApp;

    invoke-virtual {v0}, Lcom/miui/launcher/appprediction/GoogleRecommendApp;->unRegisterPredictionUpdates()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/miui/home/launcher/allapps/recommend/RecommendAppProviderManager;->mCallBack:Lcom/miui/launcher/appprediction/GoogleRecommendApp$OnDateChangedListener;

    return-void
.end method
