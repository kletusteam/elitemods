.class Lcom/miui/home/launcher/CellLayout$ViewConfiguration;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/home/launcher/CellLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ViewConfiguration"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/home/launcher/CellLayout$ViewConfiguration$SpanComparator;
    }
.end annotation


# instance fields
.field comparator:Lcom/miui/home/launcher/CellLayout$ViewConfiguration$SpanComparator;

.field map:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Landroid/view/View;",
            "Lcom/miui/home/launcher/ItemInfo;",
            ">;"
        }
    .end annotation
.end field

.field sortedViews:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/miui/home/launcher/CellLayout;


# direct methods
.method private constructor <init>(Lcom/miui/home/launcher/CellLayout;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/home/launcher/CellLayout$ViewConfiguration;->this$0:Lcom/miui/home/launcher/CellLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/miui/home/launcher/CellLayout$ViewConfiguration;->map:Ljava/util/HashMap;

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/miui/home/launcher/CellLayout$ViewConfiguration;->sortedViews:Ljava/util/ArrayList;

    new-instance p1, Lcom/miui/home/launcher/CellLayout$ViewConfiguration$SpanComparator;

    invoke-direct {p1, p0}, Lcom/miui/home/launcher/CellLayout$ViewConfiguration$SpanComparator;-><init>(Lcom/miui/home/launcher/CellLayout$ViewConfiguration;)V

    iput-object p1, p0, Lcom/miui/home/launcher/CellLayout$ViewConfiguration;->comparator:Lcom/miui/home/launcher/CellLayout$ViewConfiguration$SpanComparator;

    return-void
.end method

.method synthetic constructor <init>(Lcom/miui/home/launcher/CellLayout;Lcom/miui/home/launcher/CellLayout$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/home/launcher/CellLayout$ViewConfiguration;-><init>(Lcom/miui/home/launcher/CellLayout;)V

    return-void
.end method


# virtual methods
.method addView(Landroid/view/View;)V
    .locals 6

    goto/32 :goto_1

    nop

    :goto_0
    iget-object v0, p0, Lcom/miui/home/launcher/CellLayout$ViewConfiguration;->this$0:Lcom/miui/home/launcher/CellLayout;

    goto/32 :goto_8

    nop

    :goto_1
    iget-object v0, p0, Lcom/miui/home/launcher/CellLayout$ViewConfiguration;->map:Ljava/util/HashMap;

    goto/32 :goto_7

    nop

    :goto_2
    return-void

    :goto_3
    iget-object v1, p0, Lcom/miui/home/launcher/CellLayout$ViewConfiguration;->map:Ljava/util/HashMap;

    goto/32 :goto_9

    nop

    :goto_4
    iget v5, v0, Lcom/miui/home/launcher/ItemInfo;->spanX:I

    goto/32 :goto_10

    nop

    :goto_5
    if-nez v0, :cond_0

    goto/32 :goto_e

    :cond_0
    goto/32 :goto_d

    nop

    :goto_6
    invoke-direct {v2, v3, v4, v5, v0}, Lcom/miui/home/launcher/ItemInfo;-><init>(IIII)V

    goto/32 :goto_b

    nop

    :goto_7
    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    goto/32 :goto_f

    nop

    :goto_8
    invoke-static {v0, p1}, Lcom/miui/home/launcher/CellLayout;->access$1600(Lcom/miui/home/launcher/CellLayout;Landroid/view/View;)Lcom/miui/home/launcher/ItemInfo;

    move-result-object v0

    goto/32 :goto_3

    nop

    :goto_9
    new-instance v2, Lcom/miui/home/launcher/ItemInfo;

    goto/32 :goto_c

    nop

    :goto_a
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/32 :goto_2

    nop

    :goto_b
    invoke-virtual {v1, p1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/32 :goto_11

    nop

    :goto_c
    iget v3, v0, Lcom/miui/home/launcher/ItemInfo;->cellX:I

    goto/32 :goto_12

    nop

    :goto_d
    return-void

    :goto_e
    goto/32 :goto_0

    nop

    :goto_f
    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto/32 :goto_5

    nop

    :goto_10
    iget v0, v0, Lcom/miui/home/launcher/ItemInfo;->spanY:I

    goto/32 :goto_6

    nop

    :goto_11
    iget-object v0, p0, Lcom/miui/home/launcher/CellLayout$ViewConfiguration;->sortedViews:Ljava/util/ArrayList;

    goto/32 :goto_a

    nop

    :goto_12
    iget v4, v0, Lcom/miui/home/launcher/ItemInfo;->cellY:I

    goto/32 :goto_4

    nop
.end method

.method clear()V
    .locals 1

    goto/32 :goto_3

    nop

    :goto_0
    iget-object v0, p0, Lcom/miui/home/launcher/CellLayout$ViewConfiguration;->sortedViews:Ljava/util/ArrayList;

    goto/32 :goto_1

    nop

    :goto_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto/32 :goto_4

    nop

    :goto_2
    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    goto/32 :goto_0

    nop

    :goto_3
    iget-object v0, p0, Lcom/miui/home/launcher/CellLayout$ViewConfiguration;->map:Ljava/util/HashMap;

    goto/32 :goto_2

    nop

    :goto_4
    return-void
.end method

.method resetViewConfig()V
    .locals 4

    goto/32 :goto_3

    nop

    :goto_0
    if-nez v1, :cond_0

    goto/32 :goto_9

    :cond_0
    goto/32 :goto_f

    nop

    :goto_1
    iget v3, v1, Lcom/miui/home/launcher/ItemInfo;->cellX:I

    goto/32 :goto_c

    nop

    :goto_2
    invoke-static {v2, v1}, Lcom/miui/home/launcher/CellLayout;->access$1600(Lcom/miui/home/launcher/CellLayout;Landroid/view/View;)Lcom/miui/home/launcher/ItemInfo;

    move-result-object v2

    goto/32 :goto_d

    nop

    :goto_3
    iget-object v0, p0, Lcom/miui/home/launcher/CellLayout$ViewConfiguration;->sortedViews:Ljava/util/ArrayList;

    goto/32 :goto_4

    nop

    :goto_4
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_5
    goto/32 :goto_10

    nop

    :goto_6
    check-cast v1, Lcom/miui/home/launcher/ItemInfo;

    goto/32 :goto_1

    nop

    :goto_7
    iget-object v2, p0, Lcom/miui/home/launcher/CellLayout$ViewConfiguration;->this$0:Lcom/miui/home/launcher/CellLayout;

    goto/32 :goto_2

    nop

    :goto_8
    goto :goto_5

    :goto_9
    goto/32 :goto_e

    nop

    :goto_a
    invoke-virtual {v3, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_6

    nop

    :goto_b
    iput v1, v2, Lcom/miui/home/launcher/ItemInfo;->cellY:I

    goto/32 :goto_8

    nop

    :goto_c
    iput v3, v2, Lcom/miui/home/launcher/ItemInfo;->cellX:I

    goto/32 :goto_11

    nop

    :goto_d
    iget-object v3, p0, Lcom/miui/home/launcher/CellLayout$ViewConfiguration;->map:Ljava/util/HashMap;

    goto/32 :goto_a

    nop

    :goto_e
    return-void

    :goto_f
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_12

    nop

    :goto_10
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_0

    nop

    :goto_11
    iget v1, v1, Lcom/miui/home/launcher/ItemInfo;->cellY:I

    goto/32 :goto_b

    nop

    :goto_12
    check-cast v1, Landroid/view/View;

    goto/32 :goto_7

    nop
.end method
