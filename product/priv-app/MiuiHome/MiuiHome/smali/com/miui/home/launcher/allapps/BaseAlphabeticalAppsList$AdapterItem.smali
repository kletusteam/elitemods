.class public abstract Lcom/miui/home/launcher/allapps/BaseAlphabeticalAppsList$AdapterItem;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/home/launcher/allapps/BaseAlphabeticalAppsList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "AdapterItem"
.end annotation


# instance fields
.field appDrawable:Landroid/graphics/drawable/Drawable;

.field public appIndex:I

.field public appInfo:Lcom/miui/home/launcher/AppInfo;

.field appLabel:Ljava/lang/CharSequence;

.field categoryName:Ljava/lang/String;

.field public data:Ljava/lang/Object;

.field public dividerHeight:I

.field iconHeight:I

.field isDisable:Z

.field isRecommend:Z

.field public isSearchContent:Z

.field public position:I

.field public resultGroup:I

.field rowAppIndex:I

.field rowIndex:I

.field public sectionName:Ljava/lang/String;

.field public str:Ljava/lang/String;

.field public viewType:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/miui/home/launcher/allapps/BaseAlphabeticalAppsList$AdapterItem;->sectionName:Ljava/lang/String;

    iput-object v0, p0, Lcom/miui/home/launcher/allapps/BaseAlphabeticalAppsList$AdapterItem;->categoryName:Ljava/lang/String;

    iput-object v0, p0, Lcom/miui/home/launcher/allapps/BaseAlphabeticalAppsList$AdapterItem;->appInfo:Lcom/miui/home/launcher/AppInfo;

    const/4 v0, -0x1

    iput v0, p0, Lcom/miui/home/launcher/allapps/BaseAlphabeticalAppsList$AdapterItem;->appIndex:I

    return-void
.end method

.method static asAllAppsTitle(ILjava/lang/String;)Lcom/miui/home/launcher/allapps/BaseAlphabeticalAppsList$AdapterItem;
    .locals 2

    new-instance v0, Lcom/miui/home/launcher/allapps/BaseAlphabeticalAppsList$AdapterItem$3;

    invoke-direct {v0}, Lcom/miui/home/launcher/allapps/BaseAlphabeticalAppsList$AdapterItem$3;-><init>()V

    const/16 v1, 0x8

    iput v1, v0, Lcom/miui/home/launcher/allapps/BaseAlphabeticalAppsList$AdapterItem;->viewType:I

    iput p0, v0, Lcom/miui/home/launcher/allapps/BaseAlphabeticalAppsList$AdapterItem;->position:I

    iput-object p1, v0, Lcom/miui/home/launcher/allapps/BaseAlphabeticalAppsList$AdapterItem;->categoryName:Ljava/lang/String;

    const/4 p0, 0x1

    iput p0, v0, Lcom/miui/home/launcher/allapps/BaseAlphabeticalAppsList$AdapterItem;->resultGroup:I

    return-object v0
.end method

.method static asApp(ILjava/lang/String;Lcom/miui/home/launcher/AppInfo;IZ)Lcom/miui/home/launcher/allapps/BaseAlphabeticalAppsList$AdapterItem;
    .locals 2

    new-instance v0, Lcom/miui/home/launcher/allapps/BaseAlphabeticalAppsList$AdapterItem$1;

    invoke-direct {v0}, Lcom/miui/home/launcher/allapps/BaseAlphabeticalAppsList$AdapterItem$1;-><init>()V

    const/4 v1, 0x2

    iput v1, v0, Lcom/miui/home/launcher/allapps/BaseAlphabeticalAppsList$AdapterItem;->viewType:I

    iput p0, v0, Lcom/miui/home/launcher/allapps/BaseAlphabeticalAppsList$AdapterItem;->position:I

    iput-object p1, v0, Lcom/miui/home/launcher/allapps/BaseAlphabeticalAppsList$AdapterItem;->sectionName:Ljava/lang/String;

    iput-object p2, v0, Lcom/miui/home/launcher/allapps/BaseAlphabeticalAppsList$AdapterItem;->appInfo:Lcom/miui/home/launcher/AppInfo;

    iput p3, v0, Lcom/miui/home/launcher/allapps/BaseAlphabeticalAppsList$AdapterItem;->appIndex:I

    iput-boolean p4, v0, Lcom/miui/home/launcher/allapps/BaseAlphabeticalAppsList$AdapterItem;->isRecommend:Z

    invoke-virtual {p2}, Lcom/miui/home/launcher/AppInfo;->getLable()Ljava/lang/CharSequence;

    move-result-object p0

    iput-object p0, v0, Lcom/miui/home/launcher/allapps/BaseAlphabeticalAppsList$AdapterItem;->appLabel:Ljava/lang/CharSequence;

    invoke-virtual {p2}, Lcom/miui/home/launcher/AppInfo;->getIconDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object p0

    iput-object p0, v0, Lcom/miui/home/launcher/allapps/BaseAlphabeticalAppsList$AdapterItem;->appDrawable:Landroid/graphics/drawable/Drawable;

    invoke-static {}, Lcom/miui/home/launcher/DeviceConfig;->getAllAppsCellHeight()I

    move-result p0

    iput p0, v0, Lcom/miui/home/launcher/allapps/BaseAlphabeticalAppsList$AdapterItem;->iconHeight:I

    invoke-virtual {p2}, Lcom/miui/home/launcher/AppInfo;->isDisabled()Z

    move-result p0

    iput-boolean p0, v0, Lcom/miui/home/launcher/allapps/BaseAlphabeticalAppsList$AdapterItem;->isDisable:Z

    const/4 p0, 0x1

    iput p0, v0, Lcom/miui/home/launcher/allapps/BaseAlphabeticalAppsList$AdapterItem;->resultGroup:I

    return-object v0
.end method

.method public static asEdit(I)Lcom/miui/home/launcher/allapps/BaseAlphabeticalAppsList$AdapterItem;
    .locals 2

    new-instance v0, Lcom/miui/home/launcher/allapps/BaseAlphabeticalAppsList$AdapterItem$6;

    invoke-direct {v0}, Lcom/miui/home/launcher/allapps/BaseAlphabeticalAppsList$AdapterItem$6;-><init>()V

    const/16 v1, 0x40

    iput v1, v0, Lcom/miui/home/launcher/allapps/BaseAlphabeticalAppsList$AdapterItem;->viewType:I

    iput p0, v0, Lcom/miui/home/launcher/allapps/BaseAlphabeticalAppsList$AdapterItem;->position:I

    invoke-static {}, Lcom/miui/home/launcher/DeviceConfig;->getAllAppsCellHeight()I

    move-result p0

    iput p0, v0, Lcom/miui/home/launcher/allapps/BaseAlphabeticalAppsList$AdapterItem;->iconHeight:I

    return-object v0
.end method

.method public static asEmptySearch(I)Lcom/miui/home/launcher/allapps/BaseAlphabeticalAppsList$AdapterItem;
    .locals 2

    new-instance v0, Lcom/miui/home/launcher/allapps/BaseAlphabeticalAppsList$AdapterItem$2;

    invoke-direct {v0}, Lcom/miui/home/launcher/allapps/BaseAlphabeticalAppsList$AdapterItem$2;-><init>()V

    const/4 v1, 0x4

    iput v1, v0, Lcom/miui/home/launcher/allapps/BaseAlphabeticalAppsList$AdapterItem;->viewType:I

    iput p0, v0, Lcom/miui/home/launcher/allapps/BaseAlphabeticalAppsList$AdapterItem;->position:I

    const/4 p0, 0x5

    iput p0, v0, Lcom/miui/home/launcher/allapps/BaseAlphabeticalAppsList$AdapterItem;->resultGroup:I

    return-object v0
.end method

.method public static asMarketSearch(I)Lcom/miui/home/launcher/allapps/BaseAlphabeticalAppsList$AdapterItem;
    .locals 2

    new-instance v0, Lcom/miui/home/launcher/allapps/BaseAlphabeticalAppsList$AdapterItem$5;

    invoke-direct {v0}, Lcom/miui/home/launcher/allapps/BaseAlphabeticalAppsList$AdapterItem$5;-><init>()V

    const/16 v1, 0x20

    iput v1, v0, Lcom/miui/home/launcher/allapps/BaseAlphabeticalAppsList$AdapterItem;->viewType:I

    iput p0, v0, Lcom/miui/home/launcher/allapps/BaseAlphabeticalAppsList$AdapterItem;->position:I

    return-object v0
.end method

.method static asRecommendDivider(I)Lcom/miui/home/launcher/allapps/BaseAlphabeticalAppsList$AdapterItem;
    .locals 2

    new-instance v0, Lcom/miui/home/launcher/allapps/BaseAlphabeticalAppsList$AdapterItem$4;

    invoke-direct {v0}, Lcom/miui/home/launcher/allapps/BaseAlphabeticalAppsList$AdapterItem$4;-><init>()V

    const/16 v1, 0x10

    iput v1, v0, Lcom/miui/home/launcher/allapps/BaseAlphabeticalAppsList$AdapterItem;->viewType:I

    iput p0, v0, Lcom/miui/home/launcher/allapps/BaseAlphabeticalAppsList$AdapterItem;->position:I

    return-object v0
.end method


# virtual methods
.method public abstract areContentsTheSame(Lcom/miui/home/launcher/allapps/BaseAlphabeticalAppsList$AdapterItem;)Z
.end method

.method public abstract areItemsTheSame(Lcom/miui/home/launcher/allapps/BaseAlphabeticalAppsList$AdapterItem;)Z
.end method
