.class public Lcom/miui/home/launcher/anim/NewInstallAnimStarter;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/miui/home/launcher/ShortcutIcon$ImageSetCallback;


# instance fields
.field private mIcon:Lcom/miui/home/launcher/ShortcutIcon;


# direct methods
.method public constructor <init>(Lcom/miui/home/launcher/ShortcutIcon;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/miui/home/launcher/anim/NewInstallAnimStarter;->mIcon:Lcom/miui/home/launcher/ShortcutIcon;

    return-void
.end method

.method private createBitmapFromDrawable(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Bitmap;
    .locals 3

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0, v2, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    :cond_0
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {p1, v1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    return-object v0
.end method

.method private startInternal(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    iget-object v0, p0, Lcom/miui/home/launcher/anim/NewInstallAnimStarter;->mIcon:Lcom/miui/home/launcher/ShortcutIcon;

    invoke-virtual {v0}, Lcom/miui/home/launcher/ShortcutIcon;->getIconImageView()Lcom/miui/home/launcher/LauncherIconImageView;

    move-result-object v0

    invoke-direct {p0, p1}, Lcom/miui/home/launcher/anim/NewInstallAnimStarter;->createBitmapFromDrawable(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Bitmap;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/miui/home/launcher/LauncherIconImageView;->startLightAnim(Landroid/graphics/Bitmap;)V

    iget-object p1, p0, Lcom/miui/home/launcher/anim/NewInstallAnimStarter;->mIcon:Lcom/miui/home/launcher/ShortcutIcon;

    invoke-virtual {p1}, Lcom/miui/home/launcher/ShortcutIcon;->getTag()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/miui/home/launcher/ShortcutInfo;

    if-nez p1, :cond_0

    return-void

    :cond_0
    iget v0, p1, Lcom/miui/home/launcher/ShortcutInfo;->itemFlags:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p1, Lcom/miui/home/launcher/ShortcutInfo;->itemFlags:I

    const-string v0, "dynamic_icon"

    invoke-virtual {p1, v0}, Lcom/miui/home/launcher/ShortcutInfo;->setTrackMessage(Ljava/lang/String;)V

    invoke-static {}, Lcom/miui/home/launcher/Application;->getLauncher()Lcom/miui/home/launcher/Launcher;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/miui/home/launcher/LauncherModel;->updateItemInDatabase(Landroid/content/Context;Lcom/miui/home/launcher/ItemInfo;)V

    return-void
.end method


# virtual methods
.method public onImageDrawableSet(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    if-eqz p1, :cond_0

    invoke-direct {p0, p1}, Lcom/miui/home/launcher/anim/NewInstallAnimStarter;->startInternal(Landroid/graphics/drawable/Drawable;)V

    iget-object p1, p0, Lcom/miui/home/launcher/anim/NewInstallAnimStarter;->mIcon:Lcom/miui/home/launcher/ShortcutIcon;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/miui/home/launcher/ShortcutIcon;->setImageSetCallback(Lcom/miui/home/launcher/ShortcutIcon$ImageSetCallback;)V

    :cond_0
    return-void
.end method

.method public start()V
    .locals 1

    iget-object v0, p0, Lcom/miui/home/launcher/anim/NewInstallAnimStarter;->mIcon:Lcom/miui/home/launcher/ShortcutIcon;

    invoke-virtual {v0}, Lcom/miui/home/launcher/ShortcutIcon;->getIconImageView()Lcom/miui/home/launcher/LauncherIconImageView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/home/launcher/LauncherIconImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0, v0}, Lcom/miui/home/launcher/anim/NewInstallAnimStarter;->startInternal(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/miui/home/launcher/anim/NewInstallAnimStarter;->mIcon:Lcom/miui/home/launcher/ShortcutIcon;

    invoke-virtual {v0, p0}, Lcom/miui/home/launcher/ShortcutIcon;->setImageSetCallback(Lcom/miui/home/launcher/ShortcutIcon$ImageSetCallback;)V

    :goto_0
    return-void
.end method
