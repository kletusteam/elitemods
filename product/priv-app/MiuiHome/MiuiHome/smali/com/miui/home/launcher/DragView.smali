.class public Lcom/miui/home/launcher/DragView;
.super Landroid/view/View;

# interfaces
.implements Lcom/miui/home/launcher/AutoLayoutAnimation$GhostView;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/home/launcher/DragView$DragViewPropertyProvider;,
        Lcom/miui/home/launcher/DragView$CountLabel;
    }
.end annotation


# instance fields
.field private final isDrawOutline:Z

.field private mAnimateTarget:Landroid/view/View;

.field private mBgAnimController:Lcom/miui/home/launcher/anim/BackgroundAnimController;

.field private mCanceledMode:Z

.field private mContent:Landroid/view/View;

.field private mCountLabel:Lcom/miui/home/launcher/DragView$CountLabel;

.field private mCubicEaseOutInterpolator:Landroid/animation/TimeInterpolator;

.field private final mDragController:Lcom/miui/home/launcher/DragController;

.field private mDragGroup:Lcom/miui/home/launcher/DragObject;

.field private mDragInfo:Lcom/miui/home/launcher/ItemInfo;

.field private mDragSource:Lcom/miui/home/launcher/DragSource;

.field private mDragViewOldVisibility:I

.field private mDragViewsCount:I

.field private mDragVisualizeOffset:[I

.field private mDropAnimationStartDelay:I

.field private mDropSucceeded:Z

.field private mFadeoutAnimation:Z

.field private mFakeTargetMode:Z

.field private mHasDrawn:Z

.field private mIsAutoDrag:Z

.field private mIsBigFolderAnimStyle:Z

.field private mIsForceFadeOut:Z

.field private mIsMoved:Z

.field private mIsMultiDrag:Z

.field private mLauncher:Lcom/miui/home/launcher/Launcher;

.field private mMaxDropAnimationDistance:I

.field private mMaxDropAnimationDuration:I

.field private mMinDropAnimationDuration:I

.field private mMyIndex:I

.field private mNeedOutline:Z

.field private mNextDragView:Lcom/miui/home/launcher/DragView;

.field private mOldDragTitleAlpha:F

.field private mOnAnimationEndCallback:Ljava/lang/Runnable;

.field private mOnRemoveCallback:Ljava/lang/Runnable;

.field private mOutline:Landroid/graphics/Bitmap;

.field private mOwner:Landroid/view/ViewGroup;

.field private mPaint:Landroid/graphics/Paint;

.field private mPaintFlagsDrawFilter:Landroid/graphics/PaintFlagsDrawFilter;

.field private mPrevAnimateTarget:Landroid/view/View;

.field private mRegistrationX:I

.field private mRegistrationY:I

.field private mSpringLayerController:Lcom/miui/home/launcher/graphics/drawable/SpringLayerDragController;

.field private mTargetLoc:[F

.field private mTargetScale:F

.field private mTargetScaleAndAlphaAnimator:Landroid/animation/ValueAnimator;

.field private mTargetTranslationAnimator:Lcom/miui/home/launcher/graphics/drawable/SpringLayerAnimator;

.field private mTitleAlphaAnimate:Landroid/animation/ValueAnimator;

.field private mTmpPos:[F

.field private mXSpringAnimation:Landroidx/dynamicanimation/animation/SpringAnimation;

.field private mYSpringAnimation:Landroidx/dynamicanimation/animation/SpringAnimation;

.field private final showDeleteTint:Z


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;Landroid/view/View;Landroid/graphics/Bitmap;Lcom/miui/home/launcher/ItemInfo;IIZLcom/miui/home/launcher/DragSource;Lcom/miui/home/launcher/DragController;)V
    .locals 5

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/miui/home/launcher/DragView;->mOldDragTitleAlpha:F

    const/4 v0, 0x0

    iput v0, p0, Lcom/miui/home/launcher/DragView;->mDragViewOldVisibility:I

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/miui/home/launcher/DragView;->mNeedOutline:Z

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lcom/miui/home/launcher/DragView;->mPaint:Landroid/graphics/Paint;

    const/4 v2, 0x2

    new-array v3, v2, [I

    fill-array-data v3, :array_0

    iput-object v3, p0, Lcom/miui/home/launcher/DragView;->mDragVisualizeOffset:[I

    new-array v2, v2, [F

    fill-array-data v2, :array_1

    iput-object v2, p0, Lcom/miui/home/launcher/DragView;->mTmpPos:[F

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/miui/home/launcher/DragView;->mTargetLoc:[F

    iput-object v2, p0, Lcom/miui/home/launcher/DragView;->mOwner:Landroid/view/ViewGroup;

    iput-object v2, p0, Lcom/miui/home/launcher/DragView;->mDragGroup:Lcom/miui/home/launcher/DragObject;

    iput-object v2, p0, Lcom/miui/home/launcher/DragView;->mDragSource:Lcom/miui/home/launcher/DragSource;

    iput-boolean v0, p0, Lcom/miui/home/launcher/DragView;->mHasDrawn:Z

    iput-boolean v0, p0, Lcom/miui/home/launcher/DragView;->mDropSucceeded:Z

    iput-object v2, p0, Lcom/miui/home/launcher/DragView;->mOnAnimationEndCallback:Ljava/lang/Runnable;

    new-instance v3, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v4, 0x3fc00000    # 1.5f

    invoke-direct {v3, v4}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    iput-object v3, p0, Lcom/miui/home/launcher/DragView;->mCubicEaseOutInterpolator:Landroid/animation/TimeInterpolator;

    iput v0, p0, Lcom/miui/home/launcher/DragView;->mDropAnimationStartDelay:I

    iput-object v2, p0, Lcom/miui/home/launcher/DragView;->mPrevAnimateTarget:Landroid/view/View;

    iput-object v2, p0, Lcom/miui/home/launcher/DragView;->mAnimateTarget:Landroid/view/View;

    iput-boolean v0, p0, Lcom/miui/home/launcher/DragView;->mFakeTargetMode:Z

    iput-boolean v0, p0, Lcom/miui/home/launcher/DragView;->mFadeoutAnimation:Z

    iput-boolean v0, p0, Lcom/miui/home/launcher/DragView;->mCanceledMode:Z

    const/high16 v3, 0x7fc00000    # Float.NaN

    iput v3, p0, Lcom/miui/home/launcher/DragView;->mTargetScale:F

    iput-object v2, p0, Lcom/miui/home/launcher/DragView;->mOnRemoveCallback:Ljava/lang/Runnable;

    iput-boolean v0, p0, Lcom/miui/home/launcher/DragView;->mIsBigFolderAnimStyle:Z

    iput-object v2, p0, Lcom/miui/home/launcher/DragView;->mSpringLayerController:Lcom/miui/home/launcher/graphics/drawable/SpringLayerDragController;

    iput-boolean v0, p0, Lcom/miui/home/launcher/DragView;->mIsMoved:Z

    new-instance v2, Landroid/graphics/PaintFlagsDrawFilter;

    const/4 v3, 0x3

    invoke-direct {v2, v0, v3}, Landroid/graphics/PaintFlagsDrawFilter;-><init>(II)V

    iput-object v2, p0, Lcom/miui/home/launcher/DragView;->mPaintFlagsDrawFilter:Landroid/graphics/PaintFlagsDrawFilter;

    iput-boolean v0, p0, Lcom/miui/home/launcher/DragView;->mIsForceFadeOut:Z

    iput-object p9, p0, Lcom/miui/home/launcher/DragView;->mDragController:Lcom/miui/home/launcher/DragController;

    invoke-static {p1}, Lcom/miui/home/launcher/Launcher;->getLauncher(Landroid/view/View;)Lcom/miui/home/launcher/Launcher;

    move-result-object p9

    iput-object p9, p0, Lcom/miui/home/launcher/DragView;->mLauncher:Lcom/miui/home/launcher/Launcher;

    iput-object p1, p0, Lcom/miui/home/launcher/DragView;->mOwner:Landroid/view/ViewGroup;

    iput-object p3, p0, Lcom/miui/home/launcher/DragView;->mOutline:Landroid/graphics/Bitmap;

    iput-object p4, p0, Lcom/miui/home/launcher/DragView;->mDragInfo:Lcom/miui/home/launcher/ItemInfo;

    iget-object p1, p0, Lcom/miui/home/launcher/DragView;->mDragInfo:Lcom/miui/home/launcher/ItemInfo;

    if-eqz p1, :cond_0

    const-string p1, "Launcher_DragView"

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string p4, "new DragView:"

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p4, p0, Lcom/miui/home/launcher/DragView;->mDragInfo:Lcom/miui/home/launcher/ItemInfo;

    invoke-virtual {p4}, Lcom/miui/home/launcher/ItemInfo;->printIdentity()Ljava/lang/String;

    move-result-object p4

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-static {p1, p3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iput p5, p0, Lcom/miui/home/launcher/DragView;->mMyIndex:I

    iput-boolean p7, p0, Lcom/miui/home/launcher/DragView;->mIsAutoDrag:Z

    iput-object p8, p0, Lcom/miui/home/launcher/DragView;->mDragSource:Lcom/miui/home/launcher/DragSource;

    iput-object p2, p0, Lcom/miui/home/launcher/DragView;->mContent:Landroid/view/View;

    iput p6, p0, Lcom/miui/home/launcher/DragView;->mDragViewsCount:I

    iget p1, p0, Lcom/miui/home/launcher/DragView;->mDragViewsCount:I

    if-le p1, v1, :cond_1

    move p1, v1

    goto :goto_0

    :cond_1
    move p1, v0

    :goto_0
    iput-boolean p1, p0, Lcom/miui/home/launcher/DragView;->mIsMultiDrag:Z

    invoke-virtual {p0}, Lcom/miui/home/launcher/DragView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p3, 0x7f0b0026

    invoke-virtual {p1, p3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result p3

    iput p3, p0, Lcom/miui/home/launcher/DragView;->mMaxDropAnimationDuration:I

    const p3, 0x7f0b0027

    invoke-virtual {p1, p3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result p3

    invoke-virtual {p0, p3}, Lcom/miui/home/launcher/DragView;->setMinDropAnimationDuration(I)V

    const p3, 0x7f0b0025

    invoke-virtual {p1, p3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result p1

    iput p1, p0, Lcom/miui/home/launcher/DragView;->mMaxDropAnimationDistance:I

    instance-of p1, p2, Lcom/miui/home/launcher/DragController$VisualizeCalibration;

    if-eqz p1, :cond_2

    check-cast p2, Lcom/miui/home/launcher/DragController$VisualizeCalibration;

    iget-object p1, p0, Lcom/miui/home/launcher/DragView;->mDragVisualizeOffset:[I

    invoke-interface {p2, p1}, Lcom/miui/home/launcher/DragController$VisualizeCalibration;->getVisionOffset([I)V

    goto :goto_1

    :cond_2
    iget-object p1, p0, Lcom/miui/home/launcher/DragView;->mDragVisualizeOffset:[I

    aput v0, p1, v0

    aput v0, p1, v1

    :goto_1
    iget-boolean p1, p0, Lcom/miui/home/launcher/DragView;->mIsAutoDrag:Z

    if-nez p1, :cond_3

    iget-boolean p1, p0, Lcom/miui/home/launcher/DragView;->mIsMultiDrag:Z

    if-eqz p1, :cond_3

    iget p1, p0, Lcom/miui/home/launcher/DragView;->mMyIndex:I

    if-nez p1, :cond_3

    new-instance p1, Lcom/miui/home/launcher/DragView$CountLabel;

    invoke-virtual {p0}, Lcom/miui/home/launcher/DragView;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    iget-object p3, p0, Lcom/miui/home/launcher/DragView;->mContent:Landroid/view/View;

    check-cast p3, Lcom/miui/home/launcher/ItemIcon;

    invoke-direct {p1, p2, p3}, Lcom/miui/home/launcher/DragView$CountLabel;-><init>(Landroid/content/res/Resources;Lcom/miui/home/launcher/ItemIcon;)V

    iput-object p1, p0, Lcom/miui/home/launcher/DragView;->mCountLabel:Lcom/miui/home/launcher/DragView$CountLabel;

    :cond_3
    sget-object p1, Landroidx/dynamicanimation/animation/SpringAnimation;->TRANSLATION_X:Landroidx/dynamicanimation/animation/DynamicAnimation$ViewProperty;

    new-instance p2, Lcom/miui/home/launcher/-$$Lambda$DragView$R8gCOrJdhkouiwIhLSPb-t1ynXk;

    invoke-direct {p2, p0}, Lcom/miui/home/launcher/-$$Lambda$DragView$R8gCOrJdhkouiwIhLSPb-t1ynXk;-><init>(Lcom/miui/home/launcher/DragView;)V

    const/4 p3, 0x0

    invoke-direct {p0, p1, p3, p2}, Lcom/miui/home/launcher/DragView;->createSpringAnimation(Landroidx/dynamicanimation/animation/DynamicAnimation$ViewProperty;FLjava/util/function/Consumer;)Landroidx/dynamicanimation/animation/SpringAnimation;

    move-result-object p1

    iput-object p1, p0, Lcom/miui/home/launcher/DragView;->mXSpringAnimation:Landroidx/dynamicanimation/animation/SpringAnimation;

    sget-object p1, Landroidx/dynamicanimation/animation/SpringAnimation;->TRANSLATION_Y:Landroidx/dynamicanimation/animation/DynamicAnimation$ViewProperty;

    new-instance p2, Lcom/miui/home/launcher/-$$Lambda$DragView$w40zJ8NW6c9kMPh2GDUM4ZWYNlU;

    invoke-direct {p2, p0}, Lcom/miui/home/launcher/-$$Lambda$DragView$w40zJ8NW6c9kMPh2GDUM4ZWYNlU;-><init>(Lcom/miui/home/launcher/DragView;)V

    invoke-direct {p0, p1, p3, p2}, Lcom/miui/home/launcher/DragView;->createSpringAnimation(Landroidx/dynamicanimation/animation/DynamicAnimation$ViewProperty;FLjava/util/function/Consumer;)Landroidx/dynamicanimation/animation/SpringAnimation;

    move-result-object p1

    iput-object p1, p0, Lcom/miui/home/launcher/DragView;->mYSpringAnimation:Landroidx/dynamicanimation/animation/SpringAnimation;

    invoke-static {}, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawableUtils;->isSupport()Z

    move-result p1

    if-eqz p1, :cond_5

    iget-object p1, p0, Lcom/miui/home/launcher/DragView;->mContent:Landroid/view/View;

    instance-of p2, p1, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawableContainer;

    if-eqz p2, :cond_5

    check-cast p1, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawableContainer;

    invoke-interface {p1}, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawableContainer;->getLayerAdaptiveIconDrawable()Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable;

    move-result-object p2

    iget-object p3, p0, Lcom/miui/home/launcher/DragView;->mDragInfo:Lcom/miui/home/launcher/ItemInfo;

    if-eqz p3, :cond_4

    instance-of p4, p3, Lcom/miui/home/launcher/ShortcutInfo;

    if-eqz p4, :cond_4

    check-cast p3, Lcom/miui/home/launcher/ShortcutInfo;

    invoke-virtual {p3}, Lcom/miui/home/launcher/ShortcutInfo;->getSystemApplicationConfig()Lcom/miui/home/launcher/SystemAppStubConfig;

    move-result-object p3

    if-eqz p3, :cond_4

    invoke-virtual {p3}, Lcom/miui/home/launcher/SystemAppStubConfig;->isMiuiAppStub()Z

    move-result v0

    :cond_4
    if-eqz p2, :cond_5

    if-nez v0, :cond_5

    new-instance p3, Lcom/miui/home/launcher/graphics/drawable/SpringLayerDragController;

    invoke-direct {p3, p1, p2}, Lcom/miui/home/launcher/graphics/drawable/SpringLayerDragController;-><init>(Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawableContainer;Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable;)V

    iput-object p3, p0, Lcom/miui/home/launcher/DragView;->mSpringLayerController:Lcom/miui/home/launcher/graphics/drawable/SpringLayerDragController;

    :cond_5
    iget-object p1, p0, Lcom/miui/home/launcher/DragView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object p1, p0, Lcom/miui/home/launcher/DragView;->mDragInfo:Lcom/miui/home/launcher/ItemInfo;

    invoke-static {p1}, Lcom/miui/home/launcher/DragView;->shouldDrawOutline(Lcom/miui/home/launcher/ItemInfo;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/miui/home/launcher/DragView;->isDrawOutline:Z

    iget-object p1, p0, Lcom/miui/home/launcher/DragView;->mDragInfo:Lcom/miui/home/launcher/ItemInfo;

    invoke-static {p1}, Lcom/miui/home/launcher/DragView;->shouldShowDeleteHint(Lcom/miui/home/launcher/ItemInfo;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/miui/home/launcher/DragView;->showDeleteTint:Z

    iget-object p1, p0, Lcom/miui/home/launcher/DragView;->mContent:Landroid/view/View;

    instance-of p2, p1, Lcom/miui/home/launcher/anim/IBackgroundController;

    if-eqz p2, :cond_6

    check-cast p1, Lcom/miui/home/launcher/anim/IBackgroundController;

    invoke-interface {p1}, Lcom/miui/home/launcher/anim/IBackgroundController;->getBackgroundAnimController()Lcom/miui/home/launcher/anim/BackgroundAnimController;

    move-result-object p1

    iput-object p1, p0, Lcom/miui/home/launcher/DragView;->mBgAnimController:Lcom/miui/home/launcher/anim/BackgroundAnimController;

    iget-object p1, p0, Lcom/miui/home/launcher/DragView;->mBgAnimController:Lcom/miui/home/launcher/anim/BackgroundAnimController;

    if-eqz p1, :cond_6

    invoke-interface {p1, p0}, Lcom/miui/home/launcher/anim/BackgroundAnimController;->setTargetView(Landroid/view/View;)V

    :cond_6
    iget-boolean p1, p0, Lcom/miui/home/launcher/DragView;->isDrawOutline:Z

    if-nez p1, :cond_7

    iget-boolean p1, p0, Lcom/miui/home/launcher/DragView;->showDeleteTint:Z

    if-nez p1, :cond_7

    iget-object p1, p0, Lcom/miui/home/launcher/DragView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p0, v1, p1}, Lcom/miui/home/launcher/DragView;->setLayerType(ILandroid/graphics/Paint;)V

    :cond_7
    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data

    :array_1
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method static synthetic access$000(Lcom/miui/home/launcher/DragView;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/miui/home/launcher/DragView;->mIsBigFolderAnimStyle:Z

    return p0
.end method

.method static synthetic access$100(Lcom/miui/home/launcher/DragView;)Lcom/miui/home/launcher/anim/BackgroundAnimController;
    .locals 0

    iget-object p0, p0, Lcom/miui/home/launcher/DragView;->mBgAnimController:Lcom/miui/home/launcher/anim/BackgroundAnimController;

    return-object p0
.end method

.method static synthetic access$200(Lcom/miui/home/launcher/DragView;F)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/home/launcher/DragView;->updateSurroundingViews(F)V

    return-void
.end method

.method static synthetic access$300(Lcom/miui/home/launcher/DragView;)Ljava/lang/Runnable;
    .locals 0

    iget-object p0, p0, Lcom/miui/home/launcher/DragView;->mOnAnimationEndCallback:Ljava/lang/Runnable;

    return-object p0
.end method

.method static synthetic access$400(Lcom/miui/home/launcher/DragView;)Z
    .locals 0

    invoke-direct {p0}, Lcom/miui/home/launcher/DragView;->isTargetAnimateRunning()Z

    move-result p0

    return p0
.end method

.method static synthetic access$500(Lcom/miui/home/launcher/DragView;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/home/launcher/DragView;->onDropAnimationFinished()V

    return-void
.end method

.method static synthetic access$600(Lcom/miui/home/launcher/DragView;)Lcom/miui/home/launcher/graphics/drawable/SpringLayerDragController;
    .locals 0

    iget-object p0, p0, Lcom/miui/home/launcher/DragView;->mSpringLayerController:Lcom/miui/home/launcher/graphics/drawable/SpringLayerDragController;

    return-object p0
.end method

.method static synthetic access$700(Lcom/miui/home/launcher/DragView;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/home/launcher/DragView;->calcAndStartAnimate(Z)V

    return-void
.end method

.method static synthetic access$800(Lcom/miui/home/launcher/DragView;)Lcom/miui/home/launcher/graphics/drawable/SpringLayerAnimator;
    .locals 0

    iget-object p0, p0, Lcom/miui/home/launcher/DragView;->mTargetTranslationAnimator:Lcom/miui/home/launcher/graphics/drawable/SpringLayerAnimator;

    return-object p0
.end method

.method static synthetic access$900(Lcom/miui/home/launcher/DragView;)Landroid/view/View;
    .locals 0

    iget-object p0, p0, Lcom/miui/home/launcher/DragView;->mContent:Landroid/view/View;

    return-object p0
.end method

.method private animateToTargetInner(Z)V
    .locals 2

    iget-object v0, p0, Lcom/miui/home/launcher/DragView;->mContent:Landroid/view/View;

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/DragView;->resetView(Landroid/view/View;)V

    :cond_0
    invoke-virtual {p0}, Lcom/miui/home/launcher/DragView;->getLayerType()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Lcom/miui/home/launcher/DragView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p0, v0, v1}, Lcom/miui/home/launcher/DragView;->setLayerType(ILandroid/graphics/Paint;)V

    :cond_1
    iget-object v0, p0, Lcom/miui/home/launcher/DragView;->mAnimateTarget:Landroid/view/View;

    if-eqz v0, :cond_3

    iget-boolean v1, p0, Lcom/miui/home/launcher/DragView;->mFakeTargetMode:Z

    if-nez v1, :cond_3

    instance-of v1, v0, Lcom/miui/home/launcher/folder/IItemDragAnim;

    if-eqz v1, :cond_2

    check-cast v0, Lcom/miui/home/launcher/folder/IItemDragAnim;

    invoke-interface {v0}, Lcom/miui/home/launcher/folder/IItemDragAnim;->performPreViewItemHiddenAnim()V

    goto :goto_0

    :cond_2
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_3
    :goto_0
    if-eqz p1, :cond_4

    iget-object p1, p0, Lcom/miui/home/launcher/DragView;->mXSpringAnimation:Landroidx/dynamicanimation/animation/SpringAnimation;

    invoke-virtual {p1}, Landroidx/dynamicanimation/animation/SpringAnimation;->cancel()V

    iget-object p1, p0, Lcom/miui/home/launcher/DragView;->mYSpringAnimation:Landroidx/dynamicanimation/animation/SpringAnimation;

    invoke-virtual {p1}, Landroidx/dynamicanimation/animation/SpringAnimation;->cancel()V

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lcom/miui/home/launcher/DragView;->calcAndStartAnimate(Z)V

    :cond_4
    return-void
.end method

.method private calcAndStartAnimate(Z)V
    .locals 24

    move-object/from16 v9, p0

    iget-object v0, v9, Lcom/miui/home/launcher/DragView;->mTmpPos:[F

    invoke-virtual/range {p0 .. p0}, Lcom/miui/home/launcher/DragView;->getScaleX()F

    move-result v2

    iget-object v1, v9, Lcom/miui/home/launcher/DragView;->mTargetLoc:[F

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v4, 0x1

    if-eqz v1, :cond_0

    move-object v0, v1

    move v1, v3

    goto :goto_0

    :cond_0
    iget-object v1, v9, Lcom/miui/home/launcher/DragView;->mAnimateTarget:Landroid/view/View;

    iget-object v5, v9, Lcom/miui/home/launcher/DragView;->mOwner:Landroid/view/ViewGroup;

    iget-object v6, v9, Lcom/miui/home/launcher/DragView;->mLauncher:Lcom/miui/home/launcher/Launcher;

    invoke-virtual {v6}, Lcom/miui/home/launcher/Launcher;->isInQuickOrEditingModeExiting()Z

    move-result v6

    xor-int/2addr v6, v4

    invoke-static {v1, v5, v0, v6, v4}, Lcom/miui/home/launcher/common/Utilities;->getDescendantCoordRelativeToAncestor(Landroid/view/View;Landroid/view/View;[FZZ)F

    move-result v1

    iget-object v5, v9, Lcom/miui/home/launcher/DragView;->mAnimateTarget:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v5

    instance-of v5, v5, Lcom/miui/home/launcher/DragController$LocationCalibration;

    if-eqz v5, :cond_1

    iget-object v5, v9, Lcom/miui/home/launcher/DragView;->mAnimateTarget:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v5

    check-cast v5, Lcom/miui/home/launcher/DragController$LocationCalibration;

    iget-object v6, v9, Lcom/miui/home/launcher/DragView;->mDragGroup:Lcom/miui/home/launcher/DragObject;

    invoke-interface {v5, v0, v6}, Lcom/miui/home/launcher/DragController$LocationCalibration;->offset([FLcom/miui/home/launcher/DragObject;)V

    :cond_1
    :goto_0
    iget v5, v9, Lcom/miui/home/launcher/DragView;->mTargetScale:F

    invoke-static {v5}, Ljava/lang/Float;->isNaN(F)Z

    move-result v5

    if-eqz v5, :cond_2

    move v5, v1

    goto :goto_1

    :cond_2
    iget v5, v9, Lcom/miui/home/launcher/DragView;->mTargetScale:F

    :goto_1
    iget-boolean v6, v9, Lcom/miui/home/launcher/DragView;->mFakeTargetMode:Z

    const/4 v7, 0x0

    const/4 v8, 0x0

    if-nez v6, :cond_3

    iget-object v6, v9, Lcom/miui/home/launcher/DragView;->mAnimateTarget:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getWidth()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v6, v1

    iget-object v1, v9, Lcom/miui/home/launcher/DragView;->mContent:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v5

    sub-float/2addr v6, v1

    cmpl-float v1, v6, v7

    if-eqz v1, :cond_3

    const-string v1, "Launcher_DragView"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "targetView\'s width and draggingView\'s width is difference="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v1, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    aget v1, v0, v8

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v6, v10

    add-float/2addr v1, v6

    aput v1, v0, v8

    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/miui/home/launcher/DragView;->getAlpha()F

    move-result v6

    iget-boolean v1, v9, Lcom/miui/home/launcher/DragView;->mFadeoutAnimation:Z

    if-eqz v1, :cond_4

    goto :goto_2

    :cond_4
    move v7, v3

    :goto_2
    aget v1, v0, v8

    iget-object v3, v9, Lcom/miui/home/launcher/DragView;->mDragVisualizeOffset:[I

    aget v10, v3, v8

    int-to-float v10, v10

    mul-float/2addr v10, v5

    sub-float/2addr v1, v10

    aput v1, v0, v8

    aget v1, v0, v4

    aget v3, v3, v4

    int-to-float v3, v3

    mul-float/2addr v3, v5

    sub-float/2addr v1, v3

    aput v1, v0, v4

    iget-object v1, v9, Lcom/miui/home/launcher/DragView;->mAnimateTarget:Landroid/view/View;

    instance-of v3, v1, Lcom/miui/home/launcher/DragController$VisualizeCalibration;

    const/4 v10, 0x2

    if-eqz v3, :cond_5

    new-array v3, v10, [I

    fill-array-data v3, :array_0

    check-cast v1, Lcom/miui/home/launcher/DragController$VisualizeCalibration;

    invoke-interface {v1, v3}, Lcom/miui/home/launcher/DragController$VisualizeCalibration;->getVisionOffset([I)V

    aget v1, v0, v8

    aget v11, v3, v8

    int-to-float v11, v11

    mul-float/2addr v11, v5

    add-float/2addr v1, v11

    aput v1, v0, v8

    aget v1, v0, v4

    aget v3, v3, v4

    int-to-float v3, v3

    mul-float/2addr v3, v5

    add-float/2addr v1, v3

    aput v1, v0, v4

    :cond_5
    iget-boolean v1, v9, Lcom/miui/home/launcher/DragView;->mFadeoutAnimation:Z

    if-eqz v1, :cond_7

    iget-object v1, v9, Lcom/miui/home/launcher/DragView;->mAnimateTarget:Landroid/view/View;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    invoke-virtual/range {p0 .. p0}, Lcom/miui/home/launcher/DragView;->getWidth()I

    move-result v3

    if-le v1, v3, :cond_6

    aget v1, v0, v8

    iget-object v3, v9, Lcom/miui/home/launcher/DragView;->mAnimateTarget:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v3

    invoke-virtual/range {p0 .. p0}, Lcom/miui/home/launcher/DragView;->getWidth()I

    move-result v11

    sub-int/2addr v3, v11

    div-int/2addr v3, v10

    int-to-float v3, v3

    add-float/2addr v1, v3

    aput v1, v0, v8

    :cond_6
    iget-object v1, v9, Lcom/miui/home/launcher/DragView;->mAnimateTarget:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    invoke-virtual/range {p0 .. p0}, Lcom/miui/home/launcher/DragView;->getHeight()I

    move-result v3

    if-le v1, v3, :cond_7

    aget v1, v0, v4

    iget-object v3, v9, Lcom/miui/home/launcher/DragView;->mAnimateTarget:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    invoke-virtual/range {p0 .. p0}, Lcom/miui/home/launcher/DragView;->getHeight()I

    move-result v11

    sub-int/2addr v3, v11

    div-int/2addr v3, v10

    int-to-float v3, v3

    add-float/2addr v1, v3

    aput v1, v0, v4

    :cond_7
    invoke-virtual/range {p0 .. p0}, Lcom/miui/home/launcher/DragView;->getTranslationX()F

    move-result v11

    invoke-virtual/range {p0 .. p0}, Lcom/miui/home/launcher/DragView;->getTranslationY()F

    move-result v12

    aget v13, v0, v8

    aget v14, v0, v4

    invoke-virtual/range {p0 .. p0}, Lcom/miui/home/launcher/DragView;->getRotation()F

    move-result v15

    sub-float v0, v13, v11

    float-to-double v0, v0

    const-wide/high16 v3, 0x4000000000000000L    # 2.0

    invoke-static {v0, v1, v3, v4}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    sub-float v8, v14, v12

    move/from16 v16, v11

    float-to-double v10, v8

    invoke-static {v10, v11, v3, v4}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v3

    add-double/2addr v0, v3

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    iget v1, v9, Lcom/miui/home/launcher/DragView;->mMaxDropAnimationDuration:I

    iget v3, v9, Lcom/miui/home/launcher/DragView;->mMaxDropAnimationDistance:I

    int-to-float v4, v3

    cmpg-float v4, v0, v4

    if-gez v4, :cond_8

    int-to-float v1, v1

    iget-object v4, v9, Lcom/miui/home/launcher/DragView;->mCubicEaseOutInterpolator:Landroid/animation/TimeInterpolator;

    int-to-float v3, v3

    div-float/2addr v0, v3

    invoke-interface {v4, v0}, Landroid/animation/TimeInterpolator;->getInterpolation(F)F

    move-result v0

    mul-float/2addr v1, v0

    float-to-int v1, v1

    :cond_8
    iget v0, v9, Lcom/miui/home/launcher/DragView;->mMinDropAnimationDuration:I

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iget-object v1, v9, Lcom/miui/home/launcher/DragView;->mTargetScaleAndAlphaAnimator:Landroid/animation/ValueAnimator;

    const/4 v3, 0x0

    if-eqz v1, :cond_9

    iput-object v3, v9, Lcom/miui/home/launcher/DragView;->mTargetScaleAndAlphaAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->cancel()V

    :cond_9
    iget-object v1, v9, Lcom/miui/home/launcher/DragView;->mTargetTranslationAnimator:Lcom/miui/home/launcher/graphics/drawable/SpringLayerAnimator;

    if-eqz v1, :cond_a

    iput-object v3, v9, Lcom/miui/home/launcher/DragView;->mTargetTranslationAnimator:Lcom/miui/home/launcher/graphics/drawable/SpringLayerAnimator;

    invoke-virtual {v1}, Lcom/miui/home/launcher/graphics/drawable/SpringLayerAnimator;->cancel()V

    :cond_a
    if-eqz p1, :cond_b

    iget v1, v9, Lcom/miui/home/launcher/DragView;->mMyIndex:I

    mul-int/lit8 v8, v1, 0x32

    goto :goto_3

    :cond_b
    const/4 v8, 0x0

    :goto_3
    iget-object v1, v9, Lcom/miui/home/launcher/DragView;->mBgAnimController:Lcom/miui/home/launcher/anim/BackgroundAnimController;

    if-eqz v1, :cond_c

    invoke-interface {v1}, Lcom/miui/home/launcher/anim/BackgroundAnimController;->getAnimDuration()J

    move-result-wide v0

    goto :goto_4

    :cond_c
    int-to-long v0, v0

    :goto_4
    new-instance v3, Landroid/animation/ValueAnimator;

    invoke-direct {v3}, Landroid/animation/ValueAnimator;-><init>()V

    iput-object v3, v9, Lcom/miui/home/launcher/DragView;->mTargetScaleAndAlphaAnimator:Landroid/animation/ValueAnimator;

    iget-object v3, v9, Lcom/miui/home/launcher/DragView;->mTargetScaleAndAlphaAnimator:Landroid/animation/ValueAnimator;

    iget-object v4, v9, Lcom/miui/home/launcher/DragView;->mCubicEaseOutInterpolator:Landroid/animation/TimeInterpolator;

    invoke-virtual {v3, v4}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    iget-object v3, v9, Lcom/miui/home/launcher/DragView;->mTargetScaleAndAlphaAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v3, v0, v1}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    iget-object v0, v9, Lcom/miui/home/launcher/DragView;->mTargetScaleAndAlphaAnimator:Landroid/animation/ValueAnimator;

    int-to-long v10, v8

    invoke-virtual {v0, v10, v11}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    iget-object v0, v9, Lcom/miui/home/launcher/DragView;->mTargetScaleAndAlphaAnimator:Landroid/animation/ValueAnimator;

    const/4 v1, 0x2

    new-array v3, v1, [F

    fill-array-data v3, :array_1

    invoke-virtual {v0, v3}, Landroid/animation/ValueAnimator;->setFloatValues([F)V

    iget-object v8, v9, Lcom/miui/home/launcher/DragView;->mTargetScaleAndAlphaAnimator:Landroid/animation/ValueAnimator;

    new-instance v4, Lcom/miui/home/launcher/DragView$1;

    move-object v0, v4

    move-object/from16 v1, p0

    move v3, v5

    move-object v5, v4

    move v4, v7

    move-object v7, v5

    move v5, v6

    move v6, v15

    invoke-direct/range {v0 .. v6}, Lcom/miui/home/launcher/DragView$1;-><init>(Lcom/miui/home/launcher/DragView;FFFFF)V

    invoke-virtual {v8, v7}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    iget-object v0, v9, Lcom/miui/home/launcher/DragView;->mTargetScaleAndAlphaAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/miui/home/launcher/DragView$2;

    invoke-direct {v1, v9}, Lcom/miui/home/launcher/DragView$2;-><init>(Lcom/miui/home/launcher/DragView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    iget-object v8, v9, Lcom/miui/home/launcher/DragView;->mOwner:Landroid/view/ViewGroup;

    iget-object v0, v9, Lcom/miui/home/launcher/DragView;->mDragController:Lcom/miui/home/launcher/DragController;

    invoke-virtual {v0}, Lcom/miui/home/launcher/DragController;->getVelocityX()F

    move-result v0

    const/high16 v1, 0x44fa0000    # 2000.0f

    const/high16 v2, -0x3b060000    # -2000.0f

    invoke-static {v0, v2, v1}, Lcom/miui/home/launcher/common/Utilities;->boundToRange(FFF)F

    move-result v6

    iget-object v0, v9, Lcom/miui/home/launcher/DragView;->mDragController:Lcom/miui/home/launcher/DragController;

    invoke-virtual {v0}, Lcom/miui/home/launcher/DragController;->getVelocityY()F

    move-result v0

    invoke-static {v0, v2, v1}, Lcom/miui/home/launcher/common/Utilities;->boundToRange(FFF)F

    move-result v7

    new-instance v0, Lcom/miui/home/launcher/graphics/drawable/SpringLayerAnimator;

    const v1, 0x3f59999a    # 0.85f

    const v2, 0x3eb33333    # 0.35f

    invoke-direct {v0, v1, v2}, Lcom/miui/home/launcher/graphics/drawable/SpringLayerAnimator;-><init>(FF)V

    iput-object v0, v9, Lcom/miui/home/launcher/DragView;->mTargetTranslationAnimator:Lcom/miui/home/launcher/graphics/drawable/SpringLayerAnimator;

    iget-object v0, v9, Lcom/miui/home/launcher/DragView;->mTargetTranslationAnimator:Lcom/miui/home/launcher/graphics/drawable/SpringLayerAnimator;

    invoke-virtual {v0, v10, v11}, Lcom/miui/home/launcher/graphics/drawable/SpringLayerAnimator;->setStartDelay(J)V

    iget-object v0, v9, Lcom/miui/home/launcher/DragView;->mTargetTranslationAnimator:Lcom/miui/home/launcher/graphics/drawable/SpringLayerAnimator;

    sub-float v18, v16, v13

    sub-float v19, v12, v14

    const/16 v20, 0x0

    const/16 v21, 0x0

    move-object/from16 v17, v0

    move/from16 v22, v6

    move/from16 v23, v7

    invoke-virtual/range {v17 .. v23}, Lcom/miui/home/launcher/graphics/drawable/SpringLayerAnimator;->setValues(FFFFFF)V

    iget-object v0, v9, Lcom/miui/home/launcher/DragView;->mTargetTranslationAnimator:Lcom/miui/home/launcher/graphics/drawable/SpringLayerAnimator;

    new-instance v1, Lcom/miui/home/launcher/DragView$3;

    invoke-direct {v1, v9, v13, v14}, Lcom/miui/home/launcher/DragView$3;-><init>(Lcom/miui/home/launcher/DragView;FF)V

    invoke-virtual {v0, v1}, Lcom/miui/home/launcher/graphics/drawable/SpringLayerAnimator;->addUpdateListener(Lcom/miui/home/launcher/graphics/drawable/SpringLayerAnimator$AnimatorUpdateListener;)V

    iget-object v15, v9, Lcom/miui/home/launcher/DragView;->mTargetTranslationAnimator:Lcom/miui/home/launcher/graphics/drawable/SpringLayerAnimator;

    new-instance v5, Lcom/miui/home/launcher/DragView$4;

    move-object v0, v5

    move-object/from16 v1, p0

    move/from16 v2, v16

    move v3, v12

    move v4, v13

    move-object v12, v5

    move v5, v14

    invoke-direct/range {v0 .. v8}, Lcom/miui/home/launcher/DragView$4;-><init>(Lcom/miui/home/launcher/DragView;FFFFFFLandroid/view/ViewGroup;)V

    invoke-virtual {v15, v12}, Lcom/miui/home/launcher/graphics/drawable/SpringLayerAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    iget-object v0, v9, Lcom/miui/home/launcher/DragView;->mAnimateTarget:Landroid/view/View;

    if-eqz v0, :cond_d

    iget-boolean v0, v9, Lcom/miui/home/launcher/DragView;->mIsMultiDrag:Z

    if-nez v0, :cond_d

    iget-object v0, v9, Lcom/miui/home/launcher/DragView;->mContent:Landroid/view/View;

    instance-of v0, v0, Lcom/miui/home/launcher/ItemIcon;

    if-eqz v0, :cond_d

    invoke-virtual/range {p0 .. p0}, Lcom/miui/home/launcher/DragView;->getDragSource()Lcom/miui/home/launcher/DragSource;

    move-result-object v0

    instance-of v0, v0, Lcom/miui/home/launcher/hotseats/HotSeats;

    if-nez v0, :cond_d

    iget-object v0, v9, Lcom/miui/home/launcher/DragView;->mAnimateTarget:Landroid/view/View;

    instance-of v1, v0, Lcom/miui/home/launcher/ItemIcon;

    if-eqz v1, :cond_d

    check-cast v0, Lcom/miui/home/launcher/ItemIcon;

    invoke-virtual {v0}, Lcom/miui/home/launcher/ItemIcon;->getIsHideTitle()Z

    move-result v0

    if-eqz v0, :cond_d

    new-instance v0, Landroid/animation/ValueAnimator;

    invoke-direct {v0}, Landroid/animation/ValueAnimator;-><init>()V

    iput-object v0, v9, Lcom/miui/home/launcher/DragView;->mTitleAlphaAnimate:Landroid/animation/ValueAnimator;

    iget-object v0, v9, Lcom/miui/home/launcher/DragView;->mTitleAlphaAnimate:Landroid/animation/ValueAnimator;

    const-wide/16 v1, 0x12c

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    iget-object v0, v9, Lcom/miui/home/launcher/DragView;->mTitleAlphaAnimate:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v2, 0x3fc00000    # 1.5f

    invoke-direct {v1, v2}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    iget-object v0, v9, Lcom/miui/home/launcher/DragView;->mTitleAlphaAnimate:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, v10, v11}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    iget-object v0, v9, Lcom/miui/home/launcher/DragView;->mTitleAlphaAnimate:Landroid/animation/ValueAnimator;

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_2

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setFloatValues([F)V

    iget-object v0, v9, Lcom/miui/home/launcher/DragView;->mTitleAlphaAnimate:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/miui/home/launcher/DragView$5;

    invoke-direct {v1, v9}, Lcom/miui/home/launcher/DragView$5;-><init>(Lcom/miui/home/launcher/DragView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    iget-object v0, v9, Lcom/miui/home/launcher/DragView;->mTitleAlphaAnimate:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/miui/home/launcher/DragView$6;

    invoke-direct {v1, v9}, Lcom/miui/home/launcher/DragView$6;-><init>(Lcom/miui/home/launcher/DragView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    iget-object v0, v9, Lcom/miui/home/launcher/DragView;->mTitleAlphaAnimate:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    :cond_d
    iget-object v0, v9, Lcom/miui/home/launcher/DragView;->mTargetScaleAndAlphaAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    iget-object v0, v9, Lcom/miui/home/launcher/DragView;->mTargetTranslationAnimator:Lcom/miui/home/launcher/graphics/drawable/SpringLayerAnimator;

    invoke-virtual {v0}, Lcom/miui/home/launcher/graphics/drawable/SpringLayerAnimator;->start()V

    return-void

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data

    :array_1
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    :array_2
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private calcTouchX(I)I
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/miui/home/launcher/DragView;->calcTranslationX(I)I

    move-result v0

    sub-int/2addr p1, v0

    return p1
.end method

.method private calcTouchY(I)I
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/miui/home/launcher/DragView;->calcTranslationY(I)I

    move-result v0

    sub-int/2addr p1, v0

    return p1
.end method

.method private calcTranslationX(I)I
    .locals 1

    iget v0, p0, Lcom/miui/home/launcher/DragView;->mRegistrationX:I

    sub-int/2addr p1, v0

    iget v0, p0, Lcom/miui/home/launcher/DragView;->mMyIndex:I

    invoke-static {v0}, Lcom/miui/home/launcher/DragView$DragViewPropertyProvider;->getOffsetX(I)F

    move-result v0

    float-to-int v0, v0

    add-int/2addr p1, v0

    return p1
.end method

.method private calcTranslationY(I)I
    .locals 1

    iget v0, p0, Lcom/miui/home/launcher/DragView;->mRegistrationY:I

    sub-int/2addr p1, v0

    iget v0, p0, Lcom/miui/home/launcher/DragView;->mMyIndex:I

    invoke-static {v0}, Lcom/miui/home/launcher/DragView$DragViewPropertyProvider;->getOffsetY(I)F

    move-result v0

    float-to-int v0, v0

    add-int/2addr p1, v0

    return p1
.end method

.method private createSpringAnimation(Landroidx/dynamicanimation/animation/DynamicAnimation$ViewProperty;FLjava/util/function/Consumer;)Landroidx/dynamicanimation/animation/SpringAnimation;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/dynamicanimation/animation/DynamicAnimation$ViewProperty;",
            "F",
            "Ljava/util/function/Consumer<",
            "Ljava/lang/Integer;",
            ">;)",
            "Landroidx/dynamicanimation/animation/SpringAnimation;"
        }
    .end annotation

    new-instance v0, Landroidx/dynamicanimation/animation/SpringAnimation;

    invoke-direct {v0, p0, p1, p2}, Landroidx/dynamicanimation/animation/SpringAnimation;-><init>(Ljava/lang/Object;Landroidx/dynamicanimation/animation/FloatPropertyCompat;F)V

    invoke-virtual {v0}, Landroidx/dynamicanimation/animation/SpringAnimation;->getSpring()Landroidx/dynamicanimation/animation/SpringForce;

    move-result-object p1

    const p2, 0x3f666666    # 0.9f

    invoke-virtual {p1, p2}, Landroidx/dynamicanimation/animation/SpringForce;->setDampingRatio(F)Landroidx/dynamicanimation/animation/SpringForce;

    move-result-object p1

    const p2, 0x3dcccccd    # 0.1f

    invoke-static {p2}, Lcom/miui/home/launcher/animate/SpringAnimator;->stiffnessConvert(F)F

    move-result p2

    invoke-virtual {p1, p2}, Landroidx/dynamicanimation/animation/SpringForce;->setStiffness(F)Landroidx/dynamicanimation/animation/SpringForce;

    new-instance p1, Lcom/miui/home/launcher/-$$Lambda$DragView$FhjA2sC3LYU0sH__cmGhDVzdyqo;

    invoke-direct {p1, p3}, Lcom/miui/home/launcher/-$$Lambda$DragView$FhjA2sC3LYU0sH__cmGhDVzdyqo;-><init>(Ljava/util/function/Consumer;)V

    invoke-virtual {v0, p1}, Landroidx/dynamicanimation/animation/SpringAnimation;->addUpdateListener(Landroidx/dynamicanimation/animation/DynamicAnimation$OnAnimationUpdateListener;)Landroidx/dynamicanimation/animation/DynamicAnimation;

    return-object v0
.end method

.method private doDragAnim(FFIIII)V
    .locals 11

    new-instance v0, Landroidx/dynamicanimation/animation/SpringAnimation;

    sget-object v1, Landroidx/dynamicanimation/animation/SpringAnimation;->SCALE_X:Landroidx/dynamicanimation/animation/DynamicAnimation$ViewProperty;

    move-object v10, p0

    move v5, p2

    invoke-direct {v0, p0, v1, p2}, Landroidx/dynamicanimation/animation/SpringAnimation;-><init>(Ljava/lang/Object;Landroidx/dynamicanimation/animation/FloatPropertyCompat;F)V

    invoke-virtual {v0}, Landroidx/dynamicanimation/animation/SpringAnimation;->getSpring()Landroidx/dynamicanimation/animation/SpringForce;

    move-result-object v1

    const v2, 0x3f666666    # 0.9f

    invoke-virtual {v1, v2}, Landroidx/dynamicanimation/animation/SpringForce;->setDampingRatio(F)Landroidx/dynamicanimation/animation/SpringForce;

    move-result-object v1

    invoke-direct {p0}, Lcom/miui/home/launcher/DragView;->getDragAnimResponse()F

    move-result v2

    invoke-static {v2}, Lcom/miui/home/launcher/animate/SpringAnimator;->stiffnessConvert(F)F

    move-result v2

    invoke-virtual {v1, v2}, Landroidx/dynamicanimation/animation/SpringForce;->setStiffness(F)Landroidx/dynamicanimation/animation/SpringForce;

    new-instance v1, Lcom/miui/home/launcher/-$$Lambda$DragView$TCZ_Iu7TvSjRl2yqszaPvwfgapY;

    move-object v2, v1

    move-object v3, p0

    move v4, p1

    move/from16 v6, p5

    move v7, p3

    move/from16 v8, p6

    move v9, p4

    invoke-direct/range {v2 .. v9}, Lcom/miui/home/launcher/-$$Lambda$DragView$TCZ_Iu7TvSjRl2yqszaPvwfgapY;-><init>(Lcom/miui/home/launcher/DragView;FFIIII)V

    invoke-virtual {v0, v1}, Landroidx/dynamicanimation/animation/SpringAnimation;->addUpdateListener(Landroidx/dynamicanimation/animation/DynamicAnimation$OnAnimationUpdateListener;)Landroidx/dynamicanimation/animation/DynamicAnimation;

    invoke-virtual {v0}, Landroidx/dynamicanimation/animation/SpringAnimation;->start()V

    return-void
.end method

.method private drawContent(Landroid/graphics/Canvas;)V
    .locals 2

    iget-object v0, p0, Lcom/miui/home/launcher/DragView;->mContent:Landroid/view/View;

    instance-of v1, v0, Lcom/miui/home/launcher/ShortcutIcon;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/miui/home/launcher/ShortcutIcon;

    invoke-virtual {v0, p1}, Lcom/miui/home/launcher/ShortcutIcon;->drawDragView(Landroid/graphics/Canvas;)V

    :cond_0
    iget-object v0, p0, Lcom/miui/home/launcher/DragView;->mContent:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method private fadeOut()Z
    .locals 3

    invoke-virtual {p0}, Lcom/miui/home/launcher/DragView;->getScaleX()F

    move-result v0

    const v1, 0x3f666666    # 0.9f

    mul-float/2addr v0, v1

    invoke-virtual {p0}, Lcom/miui/home/launcher/DragView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget v1, p0, Lcom/miui/home/launcher/DragView;->mDropAnimationStartDelay:I

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v1, 0x64

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/miui/home/launcher/DragView$7;

    invoke-direct {v1, p0}, Lcom/miui/home/launcher/DragView$7;-><init>(Lcom/miui/home/launcher/DragView;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    const/4 v0, 0x1

    return v0
.end method

.method private getDragAnimResponse()F
    .locals 2

    iget-object v0, p0, Lcom/miui/home/launcher/DragView;->mBgAnimController:Lcom/miui/home/launcher/anim/BackgroundAnimController;

    instance-of v1, v0, Lcom/miui/home/launcher/anim/DragViewBgAnimContoller;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/miui/home/launcher/anim/DragViewBgAnimContoller;

    invoke-virtual {v0}, Lcom/miui/home/launcher/anim/DragViewBgAnimContoller;->getFoldThumbnailDragAnimResponse()F

    move-result v0

    return v0

    :cond_0
    const v0, 0x3dcccccd    # 0.1f

    return v0
.end method

.method private hideDragViewTitleInternal()V
    .locals 2

    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/miui/home/launcher/DragView;->mOldDragTitleAlpha:F

    const/4 v0, 0x0

    iput v0, p0, Lcom/miui/home/launcher/DragView;->mDragViewOldVisibility:I

    iget-boolean v0, p0, Lcom/miui/home/launcher/DragView;->mIsBigFolderAnimStyle:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/miui/home/launcher/DragView;->mContent:Landroid/view/View;

    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/miui/home/launcher/ItemIcon;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/miui/home/launcher/ItemIcon;

    invoke-virtual {v0}, Lcom/miui/home/launcher/ItemIcon;->getTitleContainer()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/miui/home/launcher/DragView;->mContent:Landroid/view/View;

    check-cast v0, Lcom/miui/home/launcher/ItemIcon;

    invoke-virtual {v0}, Lcom/miui/home/launcher/ItemIcon;->getTitle()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->getAlpha()F

    move-result v0

    iput v0, p0, Lcom/miui/home/launcher/DragView;->mOldDragTitleAlpha:F

    iget-object v0, p0, Lcom/miui/home/launcher/DragView;->mContent:Landroid/view/View;

    check-cast v0, Lcom/miui/home/launcher/ItemIcon;

    invoke-virtual {v0}, Lcom/miui/home/launcher/ItemIcon;->getTitle()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    iput v0, p0, Lcom/miui/home/launcher/DragView;->mDragViewOldVisibility:I

    iget-object v0, p0, Lcom/miui/home/launcher/DragView;->mContent:Landroid/view/View;

    check-cast v0, Lcom/miui/home/launcher/ItemIcon;

    invoke-virtual {v0}, Lcom/miui/home/launcher/ItemIcon;->getTitle()Landroid/widget/TextView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAlpha(F)V

    iget-object v0, p0, Lcom/miui/home/launcher/DragView;->mContent:Landroid/view/View;

    check-cast v0, Lcom/miui/home/launcher/ItemIcon;

    invoke-virtual {v0}, Lcom/miui/home/launcher/ItemIcon;->getTitleContainer()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method private isTargetAnimateRunning()Z
    .locals 1

    iget-object v0, p0, Lcom/miui/home/launcher/DragView;->mTargetTranslationAnimator:Lcom/miui/home/launcher/graphics/drawable/SpringLayerAnimator;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/miui/home/launcher/graphics/drawable/SpringLayerAnimator;->isRunning()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/miui/home/launcher/DragView;->mTargetScaleAndAlphaAnimator:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/miui/home/launcher/DragView;->mTitleAlphaAnimate:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public static synthetic lambda$animateToTarget$5(Lcom/miui/home/launcher/DragView;)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/miui/home/launcher/DragView;->animateToTargetInner(Z)V

    return-void
.end method

.method static synthetic lambda$createSpringAnimation$3(Ljava/util/function/Consumer;Landroidx/dynamicanimation/animation/DynamicAnimation;FF)V
    .locals 0

    float-to-int p1, p2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {p0, p1}, Ljava/util/function/Consumer;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public static synthetic lambda$doDragAnim$2(Lcom/miui/home/launcher/DragView;FFIIIILandroidx/dynamicanimation/animation/DynamicAnimation;FF)V
    .locals 0

    sub-float p7, p8, p1

    sub-float/2addr p2, p1

    div-float/2addr p7, p2

    iget-object p1, p0, Lcom/miui/home/launcher/DragView;->mBgAnimController:Lcom/miui/home/launcher/anim/BackgroundAnimController;

    if-eqz p1, :cond_0

    invoke-interface {p1, p7}, Lcom/miui/home/launcher/anim/BackgroundAnimController;->updateBackgroundColor(F)V

    :cond_0
    invoke-virtual {p0, p8}, Lcom/miui/home/launcher/DragView;->setScaleY(F)V

    iget-boolean p1, p0, Lcom/miui/home/launcher/DragView;->mIsMultiDrag:Z

    if-eqz p1, :cond_1

    iget-boolean p1, p0, Lcom/miui/home/launcher/DragView;->mIsAutoDrag:Z

    if-nez p1, :cond_1

    iget p1, p0, Lcom/miui/home/launcher/DragView;->mMyIndex:I

    invoke-static {p1}, Lcom/miui/home/launcher/DragView$DragViewPropertyProvider;->getRotation(I)F

    move-result p1

    mul-float/2addr p1, p7

    invoke-virtual {p0, p1}, Lcom/miui/home/launcher/DragView;->setRotation(F)V

    iget p1, p0, Lcom/miui/home/launcher/DragView;->mMyIndex:I

    iget p2, p0, Lcom/miui/home/launcher/DragView;->mDragViewsCount:I

    invoke-static {p1, p2}, Lcom/miui/home/launcher/DragView$DragViewPropertyProvider;->getAlpha(II)F

    move-result p1

    const/high16 p2, 0x3f800000    # 1.0f

    sub-float/2addr p1, p2

    mul-float/2addr p1, p7

    add-float/2addr p1, p2

    invoke-virtual {p0, p1}, Lcom/miui/home/launcher/DragView;->setAlpha(F)V

    invoke-direct {p0, p7}, Lcom/miui/home/launcher/DragView;->updateSurroundingViews(F)V

    iget-boolean p1, p0, Lcom/miui/home/launcher/DragView;->mIsMoved:Z

    if-nez p1, :cond_1

    int-to-float p1, p3

    invoke-direct {p0, p4}, Lcom/miui/home/launcher/DragView;->calcTranslationX(I)I

    move-result p2

    sub-int/2addr p2, p3

    int-to-float p2, p2

    mul-float/2addr p2, p7

    add-float/2addr p1, p2

    int-to-float p2, p5

    invoke-direct {p0, p6}, Lcom/miui/home/launcher/DragView;->calcTranslationY(I)I

    move-result p3

    sub-int/2addr p3, p5

    int-to-float p3, p3

    mul-float/2addr p3, p7

    add-float/2addr p2, p3

    invoke-virtual {p0, p1}, Lcom/miui/home/launcher/DragView;->setTranslationX(F)V

    invoke-virtual {p0, p2}, Lcom/miui/home/launcher/DragView;->setTranslationY(F)V

    :cond_1
    return-void
.end method

.method public static synthetic lambda$new$0(Lcom/miui/home/launcher/DragView;Ljava/lang/Integer;)V
    .locals 0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-direct {p0, p1}, Lcom/miui/home/launcher/DragView;->calcTouchX(I)I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/miui/home/launcher/DragView;->onMoveX(I)V

    return-void
.end method

.method public static synthetic lambda$new$1(Lcom/miui/home/launcher/DragView;Ljava/lang/Integer;)V
    .locals 0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-direct {p0, p1}, Lcom/miui/home/launcher/DragView;->calcTouchY(I)I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/miui/home/launcher/DragView;->onMoveY(I)V

    return-void
.end method

.method public static synthetic lambda$remove$4(Lcom/miui/home/launcher/DragView;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/home/launcher/DragView;->removeSelf()V

    return-void
.end method

.method private move(Landroid/util/Property;I)V
    .locals 2

    sget-object v0, Landroid/view/View;->TRANSLATION_X:Landroid/util/Property;

    if-ne p1, v0, :cond_0

    invoke-direct {p0, p2}, Lcom/miui/home/launcher/DragView;->calcTranslationX(I)I

    move-result v0

    goto :goto_0

    :cond_0
    invoke-direct {p0, p2}, Lcom/miui/home/launcher/DragView;->calcTranslationY(I)I

    move-result v0

    :goto_0
    iget v1, p0, Lcom/miui/home/launcher/DragView;->mMyIndex:I

    if-nez v1, :cond_2

    sget-object v1, Landroid/view/View;->TRANSLATION_X:Landroid/util/Property;

    if-ne p1, v1, :cond_1

    int-to-float p1, v0

    invoke-virtual {p0, p1}, Lcom/miui/home/launcher/DragView;->setTranslationX(F)V

    invoke-virtual {p0, p2}, Lcom/miui/home/launcher/DragView;->onMoveX(I)V

    goto :goto_2

    :cond_1
    int-to-float p1, v0

    invoke-virtual {p0, p1}, Lcom/miui/home/launcher/DragView;->setTranslationY(F)V

    invoke-virtual {p0, p2}, Lcom/miui/home/launcher/DragView;->onMoveY(I)V

    goto :goto_2

    :cond_2
    sget-object p2, Landroid/view/View;->TRANSLATION_X:Landroid/util/Property;

    if-ne p1, p2, :cond_3

    iget-object p1, p0, Lcom/miui/home/launcher/DragView;->mXSpringAnimation:Landroidx/dynamicanimation/animation/SpringAnimation;

    goto :goto_1

    :cond_3
    iget-object p1, p0, Lcom/miui/home/launcher/DragView;->mYSpringAnimation:Landroidx/dynamicanimation/animation/SpringAnimation;

    :goto_1
    int-to-float p2, v0

    invoke-virtual {p1, p2}, Landroidx/dynamicanimation/animation/SpringAnimation;->animateToFinalPosition(F)V

    :goto_2
    return-void
.end method

.method private onDropAnimationFinished()V
    .locals 2

    iget-object v0, p0, Lcom/miui/home/launcher/DragView;->mContent:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/miui/home/launcher/DragView;->mOnAnimationEndCallback:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    :cond_0
    iget-object v0, p0, Lcom/miui/home/launcher/DragView;->mDragGroup:Lcom/miui/home/launcher/DragObject;

    invoke-virtual {v0, p0}, Lcom/miui/home/launcher/DragObject;->onDropAnimationFinished(Lcom/miui/home/launcher/DragView;)V

    return-void
.end method

.method private removeSelf()V
    .locals 1

    iget-object v0, p0, Lcom/miui/home/launcher/DragView;->mOwner:Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/miui/home/launcher/DragView;->mOnRemoveCallback:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/miui/home/launcher/DragView;->mOnRemoveCallback:Ljava/lang/Runnable;

    :cond_0
    return-void
.end method

.method private restoreHideDragViewTitleInternal()V
    .locals 3

    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/miui/home/launcher/DragView;->mOldDragTitleAlpha:F

    const/4 v1, 0x0

    iput v1, p0, Lcom/miui/home/launcher/DragView;->mDragViewOldVisibility:I

    iget-boolean v1, p0, Lcom/miui/home/launcher/DragView;->mIsBigFolderAnimStyle:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/miui/home/launcher/DragView;->mContent:Landroid/view/View;

    if-eqz v1, :cond_0

    instance-of v2, v1, Lcom/miui/home/launcher/ItemIcon;

    if-eqz v2, :cond_0

    check-cast v1, Lcom/miui/home/launcher/ItemIcon;

    invoke-virtual {v1}, Lcom/miui/home/launcher/ItemIcon;->getTitleContainer()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/miui/home/launcher/DragView;->mOldDragTitleAlpha:F

    cmpl-float v0, v1, v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/miui/home/launcher/DragView;->mDragViewOldVisibility:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/miui/home/launcher/DragView;->mContent:Landroid/view/View;

    check-cast v0, Lcom/miui/home/launcher/ItemIcon;

    invoke-virtual {v0}, Lcom/miui/home/launcher/ItemIcon;->getTitle()Landroid/widget/TextView;

    move-result-object v0

    iget v1, p0, Lcom/miui/home/launcher/DragView;->mOldDragTitleAlpha:F

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAlpha(F)V

    iget-object v0, p0, Lcom/miui/home/launcher/DragView;->mContent:Landroid/view/View;

    check-cast v0, Lcom/miui/home/launcher/ItemIcon;

    invoke-virtual {v0}, Lcom/miui/home/launcher/ItemIcon;->getTitle()Landroid/widget/TextView;

    move-result-object v0

    iget v1, p0, Lcom/miui/home/launcher/DragView;->mDragViewOldVisibility:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public static shouldDrawOutline(Landroid/view/View;)Z
    .locals 1

    invoke-virtual {p0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object p0

    instance-of v0, p0, Lcom/miui/home/launcher/ItemInfo;

    if-eqz v0, :cond_0

    check-cast p0, Lcom/miui/home/launcher/ItemInfo;

    invoke-static {p0}, Lcom/miui/home/launcher/DragView;->shouldDrawOutline(Lcom/miui/home/launcher/ItemInfo;)Z

    move-result p0

    return p0

    :cond_0
    const/4 p0, 0x0

    return p0
.end method

.method public static shouldDrawOutline(Lcom/miui/home/launcher/ItemInfo;)Z
    .locals 1

    instance-of v0, p0, Lcom/miui/home/launcher/MIUIWidgetBasicInfo;

    if-eqz v0, :cond_1

    check-cast p0, Lcom/miui/home/launcher/MIUIWidgetBasicInfo;

    iget-boolean p0, p0, Lcom/miui/home/launcher/MIUIWidgetBasicInfo;->isMIUIWidget:Z

    if-nez p0, :cond_0

    sget-boolean p0, Lcom/miui/home/launcher/common/Utilities;->ATLEAST_S:Z

    if-eqz p0, :cond_1

    :cond_0
    const/4 p0, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private static shouldShowDeleteHint(Lcom/miui/home/launcher/ItemInfo;)Z
    .locals 3

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return v0

    :cond_0
    iget v1, p0, Lcom/miui/home/launcher/ItemInfo;->itemType:I

    const/4 v2, 0x4

    if-ne v1, v2, :cond_1

    check-cast p0, Lcom/miui/home/launcher/LauncherAppWidgetInfo;

    iget-boolean p0, p0, Lcom/miui/home/launcher/LauncherAppWidgetInfo;->isMIUIWidget:Z

    return p0

    :cond_1
    iget p0, p0, Lcom/miui/home/launcher/ItemInfo;->itemType:I

    const/16 v1, 0x13

    if-ne p0, v1, :cond_2

    const/4 v0, 0x1

    :cond_2
    return v0
.end method

.method private updateSurroundingViews(F)V
    .locals 5

    iget-boolean v0, p0, Lcom/miui/home/launcher/DragView;->mIsMultiDrag:Z

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lcom/miui/home/launcher/DragView;->mIsAutoDrag:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/miui/home/launcher/DragView;->mContent:Landroid/view/View;

    instance-of v2, v1, Lcom/miui/home/launcher/ItemIcon;

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v4, 0x1

    if-eqz v2, :cond_1

    check-cast v1, Lcom/miui/home/launcher/ItemIcon;

    invoke-virtual {v1}, Lcom/miui/home/launcher/ItemIcon;->getTitleContainer()Landroid/view/View;

    move-result-object v0

    sub-float v1, v3, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    move v0, v4

    :cond_1
    iget-object v1, p0, Lcom/miui/home/launcher/DragView;->mContent:Landroid/view/View;

    instance-of v2, v1, Lcom/miui/home/launcher/ShortcutIcon;

    if-eqz v2, :cond_2

    check-cast v1, Lcom/miui/home/launcher/ShortcutIcon;

    sub-float/2addr v3, p1

    invoke-virtual {v1, v3}, Lcom/miui/home/launcher/ShortcutIcon;->updateCheckBoxAnimProgress(F)V

    move v0, v4

    :cond_2
    iget-object v1, p0, Lcom/miui/home/launcher/DragView;->mCountLabel:Lcom/miui/home/launcher/DragView$CountLabel;

    if-eqz v1, :cond_3

    const/high16 v0, 0x437f0000    # 255.0f

    mul-float/2addr p1, v0

    float-to-int p1, p1

    invoke-virtual {v1, p1}, Lcom/miui/home/launcher/DragView$CountLabel;->updateAlpha(I)V

    move v0, v4

    :cond_3
    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/miui/home/launcher/DragView;->invalidate()V

    :cond_4
    return-void

    :cond_5
    :goto_0
    return-void
.end method


# virtual methods
.method public animateToTarget()Z
    .locals 5

    invoke-virtual {p0}, Lcom/miui/home/launcher/DragView;->isTargetAnimating()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    iget-object v0, p0, Lcom/miui/home/launcher/DragView;->mDragGroup:Lcom/miui/home/launcher/DragObject;

    invoke-virtual {v0, p0}, Lcom/miui/home/launcher/DragObject;->onDropAnimationStart(Lcom/miui/home/launcher/DragView;)V

    iget-boolean v0, p0, Lcom/miui/home/launcher/DragView;->mCanceledMode:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/miui/home/launcher/DragView;->onDropAnimationFinished()V

    return v1

    :cond_1
    iget-object v0, p0, Lcom/miui/home/launcher/DragView;->mAnimateTarget:Landroid/view/View;

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/miui/home/launcher/DragView;->mIsForceFadeOut:Z

    if-eqz v0, :cond_2

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/miui/home/launcher/DragView;->mDropAnimationStartDelay:I

    if-nez v0, :cond_3

    invoke-direct {p0, v1}, Lcom/miui/home/launcher/DragView;->animateToTargetInner(Z)V

    goto :goto_1

    :cond_3
    new-instance v2, Lcom/miui/home/launcher/-$$Lambda$DragView$f8nKOEK2WHRz3sFPWZPUQWGngWw;

    invoke-direct {v2, p0}, Lcom/miui/home/launcher/-$$Lambda$DragView$f8nKOEK2WHRz3sFPWZPUQWGngWw;-><init>(Lcom/miui/home/launcher/DragView;)V

    int-to-long v3, v0

    invoke-virtual {p0, v2, v3, v4}, Lcom/miui/home/launcher/DragView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1

    :cond_4
    :goto_0
    invoke-direct {p0}, Lcom/miui/home/launcher/DragView;->fadeOut()Z

    :goto_1
    return v1
.end method

.method public canShowShortcutMenu()Z
    .locals 1

    iget-object v0, p0, Lcom/miui/home/launcher/DragView;->mDragInfo:Lcom/miui/home/launcher/ItemInfo;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/miui/home/launcher/ItemInfo;->canShowShortcutMenu()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public clearForceFadeOut()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/miui/home/launcher/DragView;->mIsForceFadeOut:Z

    return-void
.end method

.method public getContent()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/miui/home/launcher/DragView;->mContent:Landroid/view/View;

    return-object v0
.end method

.method public getDragInfo()Lcom/miui/home/launcher/ItemInfo;
    .locals 1

    iget-object v0, p0, Lcom/miui/home/launcher/DragView;->mDragInfo:Lcom/miui/home/launcher/ItemInfo;

    return-object v0
.end method

.method public getDragSource()Lcom/miui/home/launcher/DragSource;
    .locals 1

    iget-object v0, p0, Lcom/miui/home/launcher/DragView;->mDragSource:Lcom/miui/home/launcher/DragSource;

    return-object v0
.end method

.method public getOutline()Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, Lcom/miui/home/launcher/DragView;->mOutline:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getOwner()Landroid/view/ViewGroup;
    .locals 1

    iget-object v0, p0, Lcom/miui/home/launcher/DragView;->mOwner:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public getRegistrationX()I
    .locals 1

    iget v0, p0, Lcom/miui/home/launcher/DragView;->mRegistrationX:I

    return v0
.end method

.method public getRegistrationY()I
    .locals 1

    iget v0, p0, Lcom/miui/home/launcher/DragView;->mRegistrationY:I

    return v0
.end method

.method public hasDrawn()Z
    .locals 1

    iget-boolean v0, p0, Lcom/miui/home/launcher/DragView;->mHasDrawn:Z

    return v0
.end method

.method public initRegistrationOffset(IIII)V
    .locals 1

    iget-boolean v0, p0, Lcom/miui/home/launcher/DragView;->mIsMultiDrag:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/miui/home/launcher/DragView;->mIsAutoDrag:Z

    if-nez v0, :cond_0

    iget-object p1, p0, Lcom/miui/home/launcher/DragView;->mContent:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result p1

    div-int/lit8 p1, p1, 0x2

    iput p1, p0, Lcom/miui/home/launcher/DragView;->mRegistrationX:I

    iget-object p1, p0, Lcom/miui/home/launcher/DragView;->mContent:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result p1

    div-int/lit8 p1, p1, 0x2

    iput p1, p0, Lcom/miui/home/launcher/DragView;->mRegistrationY:I

    goto :goto_0

    :cond_0
    sub-int/2addr p1, p3

    iput p1, p0, Lcom/miui/home/launcher/DragView;->mRegistrationX:I

    sub-int/2addr p2, p4

    iput p2, p0, Lcom/miui/home/launcher/DragView;->mRegistrationY:I

    :goto_0
    return-void
.end method

.method public isCanceledMode()Z
    .locals 1

    iget-boolean v0, p0, Lcom/miui/home/launcher/DragView;->mCanceledMode:Z

    return v0
.end method

.method isDropSucceeded()Z
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return v0

    :goto_1
    iget-boolean v0, p0, Lcom/miui/home/launcher/DragView;->mDropSucceeded:Z

    goto/32 :goto_0

    nop
.end method

.method public isTargetAnimating()Z
    .locals 1

    iget-object v0, p0, Lcom/miui/home/launcher/DragView;->mTargetTranslationAnimator:Lcom/miui/home/launcher/graphics/drawable/SpringLayerAnimator;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/miui/home/launcher/graphics/drawable/SpringLayerAnimator;->isStarted()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/miui/home/launcher/DragView;->mTargetScaleAndAlphaAnimator:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method moveX(I)V
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    return-void

    :goto_1
    invoke-direct {p0, v0, p1}, Lcom/miui/home/launcher/DragView;->move(Landroid/util/Property;I)V

    goto/32 :goto_0

    nop

    :goto_2
    sget-object v0, Landroid/view/View;->TRANSLATION_X:Landroid/util/Property;

    goto/32 :goto_1

    nop
.end method

.method moveY(I)V
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    sget-object v0, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    goto/32 :goto_1

    nop

    :goto_1
    invoke-direct {p0, v0, p1}, Lcom/miui/home/launcher/DragView;->move(Landroid/util/Property;I)V

    goto/32 :goto_2

    nop

    :goto_2
    return-void
.end method

.method public needOutline()Z
    .locals 1

    iget-boolean v0, p0, Lcom/miui/home/launcher/DragView;->mNeedOutline:Z

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3

    iget-object v0, p0, Lcom/miui/home/launcher/DragView;->mBgAnimController:Lcom/miui/home/launcher/anim/BackgroundAnimController;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lcom/miui/home/launcher/anim/BackgroundAnimController;->drawBackground(Landroid/graphics/Canvas;)V

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/home/launcher/DragView;->mHasDrawn:Z

    iget-boolean v1, p0, Lcom/miui/home/launcher/DragView;->isDrawOutline:Z

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/miui/home/launcher/DragView;->mNeedOutline:Z

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/miui/home/launcher/DragView;->mOutline:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_3

    iget-object v1, p0, Lcom/miui/home/launcher/DragView;->mPaint:Landroid/graphics/Paint;

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2, v2, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/miui/home/launcher/DragView;->mPaintFlagsDrawFilter:Landroid/graphics/PaintFlagsDrawFilter;

    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->setDrawFilter(Landroid/graphics/DrawFilter;)V

    iget-boolean v1, p0, Lcom/miui/home/launcher/DragView;->mIsMultiDrag:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/miui/home/launcher/DragView;->mContent:Landroid/view/View;

    instance-of v2, v1, Lcom/miui/home/launcher/ItemIcon;

    if-eqz v2, :cond_2

    check-cast v1, Lcom/miui/home/launcher/ItemIcon;

    invoke-virtual {v1}, Lcom/miui/home/launcher/ItemIcon;->getIsHideShadow()Z

    move-result v2

    invoke-virtual {v1, v0}, Lcom/miui/home/launcher/ItemIcon;->setIsHideShadow(Z)V

    invoke-direct {p0, p1}, Lcom/miui/home/launcher/DragView;->drawContent(Landroid/graphics/Canvas;)V

    invoke-virtual {v1, v2}, Lcom/miui/home/launcher/ItemIcon;->setIsHideShadow(Z)V

    iget-object v0, p0, Lcom/miui/home/launcher/DragView;->mCountLabel:Lcom/miui/home/launcher/DragView$CountLabel;

    if-eqz v0, :cond_3

    iget v1, p0, Lcom/miui/home/launcher/DragView;->mDragViewsCount:I

    invoke-virtual {v0, p1, v1}, Lcom/miui/home/launcher/DragView$CountLabel;->draw(Landroid/graphics/Canvas;I)V

    goto :goto_0

    :cond_2
    invoke-direct {p0, p1}, Lcom/miui/home/launcher/DragView;->drawContent(Landroid/graphics/Canvas;)V

    :cond_3
    :goto_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 0

    iget-object p1, p0, Lcom/miui/home/launcher/DragView;->mContent:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result p1

    iget-object p2, p0, Lcom/miui/home/launcher/DragView;->mContent:Landroid/view/View;

    invoke-virtual {p2}, Landroid/view/View;->getMeasuredHeight()I

    move-result p2

    invoke-virtual {p0, p1, p2}, Lcom/miui/home/launcher/DragView;->setMeasuredDimension(II)V

    return-void
.end method

.method public onMoveX(I)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/home/launcher/DragView;->mIsMoved:Z

    iget-object v0, p0, Lcom/miui/home/launcher/DragView;->mNextDragView:Lcom/miui/home/launcher/DragView;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/miui/home/launcher/DragView;->onPreDragViewMoveX(I)V

    :cond_0
    return-void
.end method

.method public onMoveY(I)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/home/launcher/DragView;->mIsMoved:Z

    iget-object v0, p0, Lcom/miui/home/launcher/DragView;->mNextDragView:Lcom/miui/home/launcher/DragView;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/miui/home/launcher/DragView;->onPreDragViewMoveY(I)V

    :cond_0
    return-void
.end method

.method public onPreDragViewMoveX(I)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/miui/home/launcher/DragView;->moveX(I)V

    return-void
.end method

.method public onPreDragViewMoveY(I)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/miui/home/launcher/DragView;->moveY(I)V

    return-void
.end method

.method remove()V
    .locals 3

    goto/32 :goto_5

    nop

    :goto_0
    instance-of v1, v0, Lcom/miui/home/launcher/AutoLayoutAnimation$HostView;

    goto/32 :goto_6

    nop

    :goto_1
    invoke-interface {v0, v1}, Lcom/miui/home/launcher/AutoLayoutAnimation$HostView;->setGhostView(Lcom/miui/home/launcher/AutoLayoutAnimation$GhostView;)V

    :goto_2
    goto/32 :goto_1f

    nop

    :goto_3
    const/4 v1, 0x0

    goto/32 :goto_1

    nop

    :goto_4
    if-nez v1, :cond_0

    goto/32 :goto_15

    :cond_0
    goto/32 :goto_11

    nop

    :goto_5
    iget-object v0, p0, Lcom/miui/home/launcher/DragView;->mAnimateTarget:Landroid/view/View;

    goto/32 :goto_0

    nop

    :goto_6
    if-nez v1, :cond_1

    goto/32 :goto_2

    :cond_1
    goto/32 :goto_1a

    nop

    :goto_7
    instance-of v1, v0, Lcom/miui/home/launcher/folder/IItemDragAnim;

    goto/32 :goto_16

    nop

    :goto_8
    invoke-direct {v0, p0}, Lcom/miui/home/launcher/-$$Lambda$DragView$WsczcW4budSAYwCCINafoznKT64;-><init>(Lcom/miui/home/launcher/DragView;)V

    goto/32 :goto_17

    nop

    :goto_9
    const/4 v1, 0x0

    goto/32 :goto_f

    nop

    :goto_a
    iget-object v0, p0, Lcom/miui/home/launcher/DragView;->mAnimateTarget:Landroid/view/View;

    goto/32 :goto_1e

    nop

    :goto_b
    new-instance v0, Lcom/miui/home/launcher/-$$Lambda$DragView$WsczcW4budSAYwCCINafoznKT64;

    goto/32 :goto_8

    nop

    :goto_c
    return-void

    :goto_d
    if-nez v0, :cond_2

    goto/32 :goto_10

    :cond_2
    goto/32 :goto_7

    nop

    :goto_e
    invoke-interface {v0}, Lcom/miui/home/launcher/folder/IItemDragAnim;->performPreViewItemShowAnim()V

    goto/32 :goto_12

    nop

    :goto_f
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_10
    goto/32 :goto_a

    nop

    :goto_11
    check-cast v0, Lcom/miui/home/launcher/ShortcutIcon;

    goto/32 :goto_18

    nop

    :goto_12
    goto :goto_10

    :goto_13
    goto/32 :goto_9

    nop

    :goto_14
    goto :goto_1c

    :goto_15
    goto/32 :goto_1b

    nop

    :goto_16
    if-nez v1, :cond_3

    goto/32 :goto_13

    :cond_3
    goto/32 :goto_1d

    nop

    :goto_17
    const-wide/16 v1, 0x1e

    goto/32 :goto_20

    nop

    :goto_18
    invoke-virtual {v0}, Lcom/miui/home/launcher/ShortcutIcon;->needPostWhenDrop()Z

    move-result v0

    goto/32 :goto_19

    nop

    :goto_19
    if-nez v0, :cond_4

    goto/32 :goto_15

    :cond_4
    goto/32 :goto_b

    nop

    :goto_1a
    check-cast v0, Lcom/miui/home/launcher/AutoLayoutAnimation$HostView;

    goto/32 :goto_3

    nop

    :goto_1b
    invoke-direct {p0}, Lcom/miui/home/launcher/DragView;->removeSelf()V

    :goto_1c
    goto/32 :goto_c

    nop

    :goto_1d
    check-cast v0, Lcom/miui/home/launcher/folder/IItemDragAnim;

    goto/32 :goto_e

    nop

    :goto_1e
    instance-of v1, v0, Lcom/miui/home/launcher/ShortcutIcon;

    goto/32 :goto_4

    nop

    :goto_1f
    iget-object v0, p0, Lcom/miui/home/launcher/DragView;->mAnimateTarget:Landroid/view/View;

    goto/32 :goto_d

    nop

    :goto_20
    invoke-virtual {p0, v0, v1, v2}, Lcom/miui/home/launcher/DragView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/32 :goto_14

    nop
.end method

.method public resetFlagBigFolderAnimStyle()V
    .locals 1

    iget-boolean v0, p0, Lcom/miui/home/launcher/DragView;->mIsBigFolderAnimStyle:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/miui/home/launcher/DragView;->restoreHideDragViewTitleInternal()V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/miui/home/launcher/DragView;->mIsBigFolderAnimStyle:Z

    return-void
.end method

.method public resetView(Landroid/view/View;)V
    .locals 2

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Landroid/view/View;->getScaleX()F

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    invoke-virtual {p1, v1}, Landroid/view/View;->setScaleX(F)V

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getScaleY()F

    move-result v0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_1

    invoke-virtual {p1, v1}, Landroid/view/View;->setScaleY(F)V

    :cond_1
    return-void
.end method

.method public setAnimateTarget(Landroid/view/View;)Z
    .locals 3

    iget-object v0, p0, Lcom/miui/home/launcher/DragView;->mAnimateTarget:Landroid/view/View;

    const/4 v1, 0x0

    if-eq v0, p1, :cond_3

    iput-object v0, p0, Lcom/miui/home/launcher/DragView;->mPrevAnimateTarget:Landroid/view/View;

    iput-object p1, p0, Lcom/miui/home/launcher/DragView;->mAnimateTarget:Landroid/view/View;

    iget-object p1, p0, Lcom/miui/home/launcher/DragView;->mAnimateTarget:Landroid/view/View;

    instance-of v0, p1, Lcom/miui/home/launcher/AutoLayoutAnimation$HostView;

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    check-cast p1, Lcom/miui/home/launcher/AutoLayoutAnimation$HostView;

    invoke-interface {p1, p0}, Lcom/miui/home/launcher/AutoLayoutAnimation$HostView;->setGhostView(Lcom/miui/home/launcher/AutoLayoutAnimation$GhostView;)V

    iget-object p1, p0, Lcom/miui/home/launcher/DragView;->mAnimateTarget:Landroid/view/View;

    check-cast p1, Lcom/miui/home/launcher/AutoLayoutAnimation$HostView;

    invoke-interface {p1, v2}, Lcom/miui/home/launcher/AutoLayoutAnimation$HostView;->setSkipNextAutoLayoutAnimation(Z)V

    :cond_0
    iget-object p1, p0, Lcom/miui/home/launcher/DragView;->mAnimateTarget:Landroid/view/View;

    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/miui/home/launcher/DragView;->mPrevAnimateTarget:Landroid/view/View;

    if-eq v0, p1, :cond_2

    instance-of p1, v0, Lcom/miui/home/launcher/AutoLayoutAnimation$HostView;

    if-eqz p1, :cond_1

    check-cast v0, Lcom/miui/home/launcher/AutoLayoutAnimation$HostView;

    const/4 p1, 0x0

    invoke-interface {v0, p1}, Lcom/miui/home/launcher/AutoLayoutAnimation$HostView;->setGhostView(Lcom/miui/home/launcher/AutoLayoutAnimation$GhostView;)V

    :cond_1
    iget-object p1, p0, Lcom/miui/home/launcher/DragView;->mPrevAnimateTarget:Landroid/view/View;

    if-eqz p1, :cond_2

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_2
    return v2

    :cond_3
    return v1
.end method

.method public setCanceledMode()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/home/launcher/DragView;->mCanceledMode:Z

    return-void
.end method

.method public setDragGroup(Lcom/miui/home/launcher/DragObject;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/home/launcher/DragView;->mDragGroup:Lcom/miui/home/launcher/DragObject;

    return-void
.end method

.method public setDragVisualizeOffset(II)V
    .locals 2

    iget-object v0, p0, Lcom/miui/home/launcher/DragView;->mDragVisualizeOffset:[I

    const/4 v1, 0x0

    aput p1, v0, v1

    const/4 p1, 0x1

    aput p2, v0, p1

    return-void
.end method

.method public setDropAnimationStartDelay(I)V
    .locals 0

    iput p1, p0, Lcom/miui/home/launcher/DragView;->mDropAnimationStartDelay:I

    return-void
.end method

.method setDropSucceed()V
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    iput-boolean v0, p0, Lcom/miui/home/launcher/DragView;->mDropSucceeded:Z

    goto/32 :goto_2

    nop

    :goto_1
    const/4 v0, 0x1

    goto/32 :goto_0

    nop

    :goto_2
    return-void
.end method

.method public setFadeoutAnimationMode()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/home/launcher/DragView;->mFadeoutAnimation:Z

    return-void
.end method

.method public setFakeTargetMode()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/home/launcher/DragView;->mFakeTargetMode:Z

    return-void
.end method

.method public setFlagBigFolderAnimStyle(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/miui/home/launcher/DragView;->mIsBigFolderAnimStyle:Z

    invoke-direct {p0}, Lcom/miui/home/launcher/DragView;->hideDragViewTitleInternal()V

    return-void
.end method

.method public setForceFadeOut()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/home/launcher/DragView;->mIsForceFadeOut:Z

    return-void
.end method

.method public setMinDropAnimationDuration(I)V
    .locals 0

    iput p1, p0, Lcom/miui/home/launcher/DragView;->mMinDropAnimationDuration:I

    return-void
.end method

.method public setNeedOutline(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/miui/home/launcher/DragView;->mNeedOutline:Z

    return-void
.end method

.method public setNextDragView(Lcom/miui/home/launcher/DragView;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/home/launcher/DragView;->mNextDragView:Lcom/miui/home/launcher/DragView;

    return-void
.end method

.method public setOnAnimationEndCallback(Ljava/lang/Runnable;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/home/launcher/DragView;->mOnAnimationEndCallback:Ljava/lang/Runnable;

    return-void
.end method

.method public setOnRemoveCallback(Ljava/lang/Runnable;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/home/launcher/DragView;->mOnRemoveCallback:Ljava/lang/Runnable;

    return-void
.end method

.method public setOutline(Landroid/graphics/Bitmap;)V
    .locals 2

    iput-object p1, p0, Lcom/miui/home/launcher/DragView;->mOutline:Landroid/graphics/Bitmap;

    iget-boolean v0, p0, Lcom/miui/home/launcher/DragView;->showDeleteTint:Z

    if-eqz v0, :cond_0

    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/miui/home/launcher/DragView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    const-string p1, "#66FF0000"

    invoke-static {p1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result p1

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, p1, v1}, Landroid/graphics/drawable/BitmapDrawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    new-instance p1, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {p1}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    sget-object v1, Landroid/view/View;->SELECTED_STATE_SET:[I

    invoke-virtual {p1, v1, v0}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    const/16 v0, 0x12c

    invoke-virtual {p1, v0}, Landroid/graphics/drawable/StateListDrawable;->setEnterFadeDuration(I)V

    invoke-virtual {p1, v0}, Landroid/graphics/drawable/StateListDrawable;->setExitFadeDuration(I)V

    invoke-virtual {p0, p1}, Lcom/miui/home/launcher/DragView;->setForeground(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    return-void
.end method

.method public setTargetScale(F)V
    .locals 1

    invoke-static {p1}, Ljava/lang/Float;->isInfinite(F)Z

    move-result v0

    if-nez v0, :cond_0

    iput p1, p0, Lcom/miui/home/launcher/DragView;->mTargetScale:F

    :cond_0
    return-void
.end method

.method public showWithAnim(FFIIII)V
    .locals 4

    invoke-virtual {p0, p1}, Lcom/miui/home/launcher/DragView;->setScaleX(F)V

    invoke-virtual {p0, p1}, Lcom/miui/home/launcher/DragView;->setScaleY(F)V

    int-to-float v0, p5

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/DragView;->setTranslationX(F)V

    int-to-float v0, p6

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/DragView;->setTranslationY(F)V

    iget-object v0, p0, Lcom/miui/home/launcher/DragView;->mOwner:Landroid/view/ViewGroup;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v2, -0x1

    const/16 v3, 0x33

    invoke-direct {v1, v2, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {v0, p0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-static {p0}, Lcom/miui/home/launcher/Launcher;->performLayoutNow(Landroid/view/View;)V

    invoke-direct/range {p0 .. p6}, Lcom/miui/home/launcher/DragView;->doDragAnim(FFIIII)V

    return-void
.end method

.method public updateAnimateTarget(Landroid/view/View;)V
    .locals 1

    invoke-virtual {p0}, Lcom/miui/home/launcher/DragView;->isTargetAnimating()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/miui/home/launcher/DragView;->setAnimateTarget(Landroid/view/View;)Z

    iget-object p1, p0, Lcom/miui/home/launcher/DragView;->mTargetLoc:[F

    if-nez p1, :cond_0

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lcom/miui/home/launcher/DragView;->animateToTargetInner(Z)V

    :cond_0
    return-void
.end method

.method public updateAnimateTarget([F)V
    .locals 0

    iput-object p1, p0, Lcom/miui/home/launcher/DragView;->mTargetLoc:[F

    invoke-virtual {p0}, Lcom/miui/home/launcher/DragView;->isTargetAnimating()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lcom/miui/home/launcher/DragView;->animateToTargetInner(Z)V

    :cond_0
    return-void
.end method
