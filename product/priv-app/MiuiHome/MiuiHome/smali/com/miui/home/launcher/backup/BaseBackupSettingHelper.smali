.class public Lcom/miui/home/launcher/backup/BaseBackupSettingHelper;
.super Lcom/miui/home/launcher/common/BaseSharePreference;


# instance fields
.field protected final mSettings:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/miui/home/launcher/backup/settings/BackupSettingsBase<",
            "*>;>;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>()V
    .locals 2

    const-string v0, "backup"

    invoke-direct {p0, v0}, Lcom/miui/home/launcher/common/BaseSharePreference;-><init>(Ljava/lang/String;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/home/launcher/backup/BaseBackupSettingHelper;->mSettings:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/miui/home/launcher/backup/BaseBackupSettingHelper;->mSettings:Ljava/util/ArrayList;

    new-instance v1, Lcom/miui/home/launcher/backup/settings/BackupSettingsAutoFill;

    invoke-direct {v1}, Lcom/miui/home/launcher/backup/settings/BackupSettingsAutoFill;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/miui/home/launcher/backup/BaseBackupSettingHelper;->mSettings:Ljava/util/ArrayList;

    new-instance v1, Lcom/miui/home/launcher/backup/settings/BackupSettingsLockCells;

    invoke-direct {v1}, Lcom/miui/home/launcher/backup/settings/BackupSettingsLockCells;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/miui/home/launcher/backup/BaseBackupSettingHelper;->mSettings:Ljava/util/ArrayList;

    new-instance v1, Lcom/miui/home/launcher/backup/settings/BackupSettingsMemoryInfo;

    invoke-direct {v1}, Lcom/miui/home/launcher/backup/settings/BackupSettingsMemoryInfo;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/miui/home/launcher/backup/BaseBackupSettingHelper;->mSettings:Ljava/util/ArrayList;

    new-instance v1, Lcom/miui/home/launcher/backup/settings/BackupSettingsNoWord;

    invoke-direct {v1}, Lcom/miui/home/launcher/backup/settings/BackupSettingsNoWord;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/miui/home/launcher/backup/BaseBackupSettingHelper;->mSettings:Ljava/util/ArrayList;

    new-instance v1, Lcom/miui/home/launcher/backup/settings/BackupSettingsOnlyWidgetNoWord;

    invoke-direct {v1}, Lcom/miui/home/launcher/backup/settings/BackupSettingsOnlyWidgetNoWord;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {}, Lcom/miui/home/launcher/common/Utilities;->isPadDevice()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/miui/home/launcher/backup/BaseBackupSettingHelper;->mSettings:Ljava/util/ArrayList;

    new-instance v1, Lcom/miui/home/launcher/backup/settings/BackupSettingsAssistant;

    invoke-direct {v1}, Lcom/miui/home/launcher/backup/settings/BackupSettingsAssistant;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/miui/home/launcher/backup/BaseBackupSettingHelper;->mSettings:Ljava/util/ArrayList;

    new-instance v1, Lcom/miui/home/launcher/backup/settings/BackupSettingsTaskLayoutStyle;

    invoke-direct {v1}, Lcom/miui/home/launcher/backup/settings/BackupSettingsTaskLayoutStyle;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-object v0, p0, Lcom/miui/home/launcher/backup/BaseBackupSettingHelper;->mSettings:Ljava/util/ArrayList;

    new-instance v1, Lcom/miui/home/launcher/backup/settings/BackupSettingsPerfectIcon;

    invoke-direct {v1}, Lcom/miui/home/launcher/backup/settings/BackupSettingsPerfectIcon;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {}, Lcom/miui/home/launcher/ApplicationConfig;->isFeedSupport()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/miui/home/launcher/backup/BaseBackupSettingHelper;->mSettings:Ljava/util/ArrayList;

    new-instance v1, Lcom/miui/home/launcher/backup/settings/BackupSettingsPullDown;

    invoke-direct {v1}, Lcom/miui/home/launcher/backup/settings/BackupSettingsPullDown;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/miui/home/launcher/backup/BaseBackupSettingHelper;->mSettings:Ljava/util/ArrayList;

    new-instance v1, Lcom/miui/home/launcher/backup/settings/BackupSettingsSlideUp;

    invoke-direct {v1}, Lcom/miui/home/launcher/backup/settings/BackupSettingsSlideUp;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    return-void
.end method


# virtual methods
.method backupSettings()V
    .locals 2

    goto/32 :goto_3

    nop

    :goto_0
    invoke-virtual {v1}, Lcom/miui/home/launcher/backup/settings/BackupSettingsBase;->backup()V

    goto/32 :goto_1

    nop

    :goto_1
    goto :goto_8

    :goto_2
    goto/32 :goto_6

    nop

    :goto_3
    const-string v0, "Launcher.Backup"

    goto/32 :goto_d

    nop

    :goto_4
    if-nez v1, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_9

    nop

    :goto_5
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_c

    nop

    :goto_6
    return-void

    :goto_7
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_8
    goto/32 :goto_b

    nop

    :goto_9
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_a

    nop

    :goto_a
    check-cast v1, Lcom/miui/home/launcher/backup/settings/BackupSettingsBase;

    goto/32 :goto_0

    nop

    :goto_b
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_4

    nop

    :goto_c
    iget-object v0, p0, Lcom/miui/home/launcher/backup/BaseBackupSettingHelper;->mSettings:Ljava/util/ArrayList;

    goto/32 :goto_7

    nop

    :goto_d
    const-string v1, "backup settings"

    goto/32 :goto_5

    nop
.end method

.method restoreSettings()V
    .locals 2

    goto/32 :goto_1

    nop

    :goto_0
    invoke-virtual {v1}, Lcom/miui/home/launcher/backup/settings/BackupSettingsBase;->restore()V

    goto/32 :goto_7

    nop

    :goto_1
    const-string v0, "Launcher.Backup"

    goto/32 :goto_4

    nop

    :goto_2
    check-cast v1, Lcom/miui/home/launcher/backup/settings/BackupSettingsBase;

    goto/32 :goto_0

    nop

    :goto_3
    iget-object v0, p0, Lcom/miui/home/launcher/backup/BaseBackupSettingHelper;->mSettings:Ljava/util/ArrayList;

    goto/32 :goto_c

    nop

    :goto_4
    const-string v1, "restore settings"

    goto/32 :goto_b

    nop

    :goto_5
    return-void

    :goto_6
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_9

    nop

    :goto_7
    goto :goto_d

    :goto_8
    goto/32 :goto_5

    nop

    :goto_9
    if-nez v1, :cond_0

    goto/32 :goto_8

    :cond_0
    goto/32 :goto_a

    nop

    :goto_a
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_2

    nop

    :goto_b
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_3

    nop

    :goto_c
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_d
    goto/32 :goto_6

    nop
.end method
