.class Lcom/miui/home/launcher/CellLayout$DragPos;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/home/launcher/CellLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DragPos"
.end annotation


# instance fields
.field cellXY:[I

.field stayType:I

.field final synthetic this$0:Lcom/miui/home/launcher/CellLayout;


# direct methods
.method public constructor <init>(Lcom/miui/home/launcher/CellLayout;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/home/launcher/CellLayout$DragPos;->this$0:Lcom/miui/home/launcher/CellLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p1, 0x2

    new-array p1, p1, [I

    iput-object p1, p0, Lcom/miui/home/launcher/CellLayout$DragPos;->cellXY:[I

    const/16 p1, 0x8

    iput p1, p0, Lcom/miui/home/launcher/CellLayout$DragPos;->stayType:I

    invoke-virtual {p0}, Lcom/miui/home/launcher/CellLayout$DragPos;->reset()V

    return-void
.end method


# virtual methods
.method equal(Lcom/miui/home/launcher/CellLayout$DragPos;)Z
    .locals 6

    goto/32 :goto_2

    nop

    :goto_0
    iget p1, p1, Lcom/miui/home/launcher/CellLayout$DragPos;->stayType:I

    goto/32 :goto_a

    nop

    :goto_1
    iget-object v3, p1, Lcom/miui/home/launcher/CellLayout$DragPos;->cellXY:[I

    goto/32 :goto_e

    nop

    :goto_2
    iget-object v0, p0, Lcom/miui/home/launcher/CellLayout$DragPos;->cellXY:[I

    goto/32 :goto_4

    nop

    :goto_3
    aget v0, v0, v5

    goto/32 :goto_8

    nop

    :goto_4
    const/4 v1, 0x0

    goto/32 :goto_f

    nop

    :goto_5
    if-eq v0, v2, :cond_0

    goto/32 :goto_d

    :cond_0
    goto/32 :goto_6

    nop

    :goto_6
    iget v0, p0, Lcom/miui/home/launcher/CellLayout$DragPos;->stayType:I

    goto/32 :goto_0

    nop

    :goto_7
    const/4 v5, 0x1

    goto/32 :goto_b

    nop

    :goto_8
    aget v2, v3, v5

    goto/32 :goto_5

    nop

    :goto_9
    return v1

    :goto_a
    if-eq v0, p1, :cond_1

    goto/32 :goto_d

    :cond_1
    goto/32 :goto_c

    nop

    :goto_b
    if-eq v2, v4, :cond_2

    goto/32 :goto_d

    :cond_2
    goto/32 :goto_3

    nop

    :goto_c
    move v1, v5

    :goto_d
    goto/32 :goto_9

    nop

    :goto_e
    aget v4, v3, v1

    goto/32 :goto_7

    nop

    :goto_f
    aget v2, v0, v1

    goto/32 :goto_1

    nop
.end method

.method public isInvalid()Z
    .locals 2

    iget v0, p0, Lcom/miui/home/launcher/CellLayout$DragPos;->stayType:I

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method reset()V
    .locals 3

    goto/32 :goto_5

    nop

    :goto_0
    iput v0, p0, Lcom/miui/home/launcher/CellLayout$DragPos;->stayType:I

    goto/32 :goto_4

    nop

    :goto_1
    aput v2, v0, v1

    goto/32 :goto_8

    nop

    :goto_2
    const/4 v1, 0x1

    goto/32 :goto_7

    nop

    :goto_3
    const/16 v0, 0x8

    goto/32 :goto_0

    nop

    :goto_4
    return-void

    :goto_5
    iget-object v0, p0, Lcom/miui/home/launcher/CellLayout$DragPos;->cellXY:[I

    goto/32 :goto_2

    nop

    :goto_6
    aput v2, v0, v1

    goto/32 :goto_3

    nop

    :goto_7
    const/4 v2, -0x1

    goto/32 :goto_1

    nop

    :goto_8
    const/4 v1, 0x0

    goto/32 :goto_6

    nop
.end method

.method set(Lcom/miui/home/launcher/CellLayout$DragPos;)V
    .locals 4

    goto/32 :goto_3

    nop

    :goto_0
    const/4 v2, 0x0

    goto/32 :goto_2

    nop

    :goto_1
    iput p1, p0, Lcom/miui/home/launcher/CellLayout$DragPos;->stayType:I

    goto/32 :goto_a

    nop

    :goto_2
    aget v3, v1, v2

    goto/32 :goto_7

    nop

    :goto_3
    iget-object v0, p0, Lcom/miui/home/launcher/CellLayout$DragPos;->cellXY:[I

    goto/32 :goto_6

    nop

    :goto_4
    aput v1, v0, v2

    goto/32 :goto_8

    nop

    :goto_5
    aget v1, v1, v2

    goto/32 :goto_4

    nop

    :goto_6
    iget-object v1, p1, Lcom/miui/home/launcher/CellLayout$DragPos;->cellXY:[I

    goto/32 :goto_0

    nop

    :goto_7
    aput v3, v0, v2

    goto/32 :goto_9

    nop

    :goto_8
    iget p1, p1, Lcom/miui/home/launcher/CellLayout$DragPos;->stayType:I

    goto/32 :goto_1

    nop

    :goto_9
    const/4 v2, 0x1

    goto/32 :goto_5

    nop

    :goto_a
    return-void
.end method
