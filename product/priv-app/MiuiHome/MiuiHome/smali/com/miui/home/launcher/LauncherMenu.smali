.class public Lcom/miui/home/launcher/LauncherMenu;
.super Landroid/widget/LinearLayout;


# instance fields
.field private mAutoFill:Landroid/widget/LinearLayout;

.field private mAutoFillSlidingButton:Lmiuix/slidingwidget/widget/SlidingButton;

.field private mCellType1:Landroid/widget/Button;

.field private mCellType2:Landroid/widget/Button;

.field private mCellType3:Landroid/widget/Button;

.field private mCellsSetting:Landroid/widget/LinearLayout;

.field private mContext:Landroid/content/Context;

.field private mDefaultScreenPreview:Landroid/widget/LinearLayout;

.field private mLauncher:Lcom/miui/home/launcher/Launcher;

.field private mLock:Landroid/widget/LinearLayout;

.field private mLockSlidingButton:Lmiuix/slidingwidget/widget/SlidingButton;

.field private mMenuTransEffect:Landroid/widget/LinearLayout;

.field private mMore:Landroid/widget/LinearLayout;

.field private mNoWordSetting:Landroid/widget/LinearLayout;

.field private mNoWordSlidingButton:Lmiuix/slidingwidget/widget/SlidingButton;

.field private mOnlyWidgetNoWordSetting:Landroid/widget/LinearLayout;

.field private mOnlyWidgetNoWordSlidingButton:Lmiuix/slidingwidget/widget/SlidingButton;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/miui/home/launcher/LauncherMenu;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/miui/home/launcher/LauncherMenu;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-static {p0}, Lcom/miui/home/launcher/Launcher;->getLauncher(Landroid/view/View;)Lcom/miui/home/launcher/Launcher;

    move-result-object p2

    iput-object p2, p0, Lcom/miui/home/launcher/LauncherMenu;->mLauncher:Lcom/miui/home/launcher/Launcher;

    iput-object p1, p0, Lcom/miui/home/launcher/LauncherMenu;->mContext:Landroid/content/Context;

    return-void
.end method

.method static synthetic access$000(Lcom/miui/home/launcher/LauncherMenu;)Lcom/miui/home/launcher/Launcher;
    .locals 0

    iget-object p0, p0, Lcom/miui/home/launcher/LauncherMenu;->mLauncher:Lcom/miui/home/launcher/Launcher;

    return-object p0
.end method

.method static synthetic access$100(Lcom/miui/home/launcher/LauncherMenu;)Lmiuix/slidingwidget/widget/SlidingButton;
    .locals 0

    iget-object p0, p0, Lcom/miui/home/launcher/LauncherMenu;->mAutoFillSlidingButton:Lmiuix/slidingwidget/widget/SlidingButton;

    return-object p0
.end method

.method static synthetic access$200(Lcom/miui/home/launcher/LauncherMenu;)Lmiuix/slidingwidget/widget/SlidingButton;
    .locals 0

    iget-object p0, p0, Lcom/miui/home/launcher/LauncherMenu;->mLockSlidingButton:Lmiuix/slidingwidget/widget/SlidingButton;

    return-object p0
.end method

.method static synthetic access$300(Lcom/miui/home/launcher/LauncherMenu;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/miui/home/launcher/LauncherMenu;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic access$400(Lcom/miui/home/launcher/LauncherMenu;)Landroid/widget/Button;
    .locals 0

    iget-object p0, p0, Lcom/miui/home/launcher/LauncherMenu;->mCellType1:Landroid/widget/Button;

    return-object p0
.end method

.method static synthetic access$500(Lcom/miui/home/launcher/LauncherMenu;)Landroid/widget/Button;
    .locals 0

    iget-object p0, p0, Lcom/miui/home/launcher/LauncherMenu;->mCellType2:Landroid/widget/Button;

    return-object p0
.end method

.method static synthetic access$600(Lcom/miui/home/launcher/LauncherMenu;)Landroid/widget/Button;
    .locals 0

    iget-object p0, p0, Lcom/miui/home/launcher/LauncherMenu;->mCellType3:Landroid/widget/Button;

    return-object p0
.end method

.method static synthetic access$700(Lcom/miui/home/launcher/LauncherMenu;[Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/home/launcher/LauncherMenu;->setSelectedCellType([Landroid/view/View;)V

    return-void
.end method

.method private initCellSetting()V
    .locals 6

    iget-object v0, p0, Lcom/miui/home/launcher/LauncherMenu;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/home/launcher/DeviceConfig;->needShowCellsEntry(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/miui/home/launcher/LauncherMenu;->mCellsSetting:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    const v0, 0x7f0a00b1

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/LauncherMenu;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/miui/home/launcher/LauncherMenu;->mCellType1:Landroid/widget/Button;

    const v0, 0x7f0a00b2

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/LauncherMenu;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/miui/home/launcher/LauncherMenu;->mCellType2:Landroid/widget/Button;

    const v0, 0x7f0a00b3

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/LauncherMenu;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/miui/home/launcher/LauncherMenu;->mCellType3:Landroid/widget/Button;

    iget-object v0, p0, Lcom/miui/home/launcher/LauncherMenu;->mLauncher:Lcom/miui/home/launcher/Launcher;

    invoke-static {v0}, Lcom/miui/home/launcher/ScreenUtils;->getScreenCellsSizeOptions(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_1

    iget-object v2, p0, Lcom/miui/home/launcher/LauncherMenu;->mCellType1:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/miui/home/launcher/LauncherMenu;->mCellType1:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setTag(Ljava/lang/Object;)V

    iget-object v2, p0, Lcom/miui/home/launcher/LauncherMenu;->mCellType2:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/miui/home/launcher/LauncherMenu;->mCellType2:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setTag(Ljava/lang/Object;)V

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v4, 0x3

    const/4 v5, 0x2

    if-lt v2, v4, :cond_0

    invoke-static {}, Lcom/miui/home/launcher/common/Utilities;->isNoWordModel()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/miui/home/launcher/LauncherMenu;->mCellType3:Landroid/widget/Button;

    invoke-virtual {v2, v1}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v2, p0, Lcom/miui/home/launcher/LauncherMenu;->mCellType3:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/miui/home/launcher/LauncherMenu;->mCellType3:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/Button;->setTag(Ljava/lang/Object;)V

    :cond_0
    new-array v0, v5, [Landroid/view/View;

    iget-object v2, p0, Lcom/miui/home/launcher/LauncherMenu;->mCellType1:Landroid/widget/Button;

    aput-object v2, v0, v1

    iget-object v1, p0, Lcom/miui/home/launcher/LauncherMenu;->mCellType2:Landroid/widget/Button;

    aput-object v1, v0, v3

    invoke-direct {p0, v0}, Lcom/miui/home/launcher/LauncherMenu;->setSelectedCellType([Landroid/view/View;)V

    :cond_1
    new-instance v0, Lcom/miui/home/launcher/LauncherMenu$8;

    invoke-direct {v0, p0}, Lcom/miui/home/launcher/LauncherMenu$8;-><init>(Lcom/miui/home/launcher/LauncherMenu;)V

    iget-object v1, p0, Lcom/miui/home/launcher/LauncherMenu;->mCellType1:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/miui/home/launcher/LauncherMenu;->mCellType2:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/miui/home/launcher/LauncherMenu;->mCellType3:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/miui/home/launcher/LauncherMenu;->mCellsSetting:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :goto_0
    iget-object v0, p0, Lcom/miui/home/launcher/LauncherMenu;->mAutoFillSlidingButton:Lmiuix/slidingwidget/widget/SlidingButton;

    invoke-static {}, Lcom/miui/home/launcher/common/Utilities;->enableAutoFillEmpty()Z

    move-result v1

    invoke-virtual {v0, v1}, Lmiuix/slidingwidget/widget/SlidingButton;->setChecked(Z)V

    iget-object v0, p0, Lcom/miui/home/launcher/LauncherMenu;->mLockSlidingButton:Lmiuix/slidingwidget/widget/SlidingButton;

    invoke-static {}, Lcom/miui/home/launcher/common/Utilities;->isScreenCellsLocked()Z

    move-result v1

    invoke-virtual {v0, v1}, Lmiuix/slidingwidget/widget/SlidingButton;->setChecked(Z)V

    iget-object v0, p0, Lcom/miui/home/launcher/LauncherMenu;->mNoWordSetting:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/miui/home/launcher/LauncherMenu;->mNoWordSlidingButton:Lmiuix/slidingwidget/widget/SlidingButton;

    invoke-static {}, Lcom/miui/home/launcher/common/Utilities;->isNoWordModel()Z

    move-result v1

    invoke-virtual {v0, v1}, Lmiuix/slidingwidget/widget/SlidingButton;->setChecked(Z)V

    :cond_3
    iget-object v0, p0, Lcom/miui/home/launcher/LauncherMenu;->mOnlyWidgetNoWordSetting:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/miui/home/launcher/LauncherMenu;->mOnlyWidgetNoWordSlidingButton:Lmiuix/slidingwidget/widget/SlidingButton;

    invoke-static {}, Lcom/miui/home/launcher/common/Utilities;->isOnlyWidgetNoWordModel()Z

    move-result v1

    invoke-virtual {v0, v1}, Lmiuix/slidingwidget/widget/SlidingButton;->setChecked(Z)V

    :cond_4
    return-void
.end method

.method private isCellSettingInited()Z
    .locals 1

    iget-object v0, p0, Lcom/miui/home/launcher/LauncherMenu;->mCellType1:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public static synthetic lambda$onFinishInflate$0(Lcom/miui/home/launcher/LauncherMenu;Landroid/view/View;)V
    .locals 1

    iget-object p1, p0, Lcom/miui/home/launcher/LauncherMenu;->mNoWordSlidingButton:Lmiuix/slidingwidget/widget/SlidingButton;

    invoke-virtual {p1}, Lmiuix/slidingwidget/widget/SlidingButton;->isChecked()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-virtual {p1, v0}, Lmiuix/slidingwidget/widget/SlidingButton;->setChecked(Z)V

    return-void
.end method

.method public static synthetic lambda$onFinishInflate$1(Lcom/miui/home/launcher/LauncherMenu;Landroid/view/View;)V
    .locals 1

    iget-object p1, p0, Lcom/miui/home/launcher/LauncherMenu;->mOnlyWidgetNoWordSlidingButton:Lmiuix/slidingwidget/widget/SlidingButton;

    invoke-virtual {p1}, Lmiuix/slidingwidget/widget/SlidingButton;->isChecked()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-virtual {p1, v0}, Lmiuix/slidingwidget/widget/SlidingButton;->setChecked(Z)V

    return-void
.end method

.method public static synthetic lambda$onFinishInflate$2(Lcom/miui/home/launcher/LauncherMenu;Z)V
    .locals 3

    sget-object v0, Lcom/miui/home/launcher/util/noword/NoWordSettingHelper;->INSTANCE:Lcom/miui/home/launcher/util/noword/NoWordSettingHelper;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/miui/home/launcher/util/noword/NoWordSettingHelper;->setSwitchingNoWordModel(Z)V

    iget-object v0, p0, Lcom/miui/home/launcher/LauncherMenu;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "miui_home_no_word_model"

    invoke-static {v0, v1, p1}, Lcom/miui/launcher/utils/MiuiSettingsUtils;->putBooleanToSystem(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/miui/home/launcher/LauncherMenu;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "miui_home_only_widget_no_word_model"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/miui/launcher/utils/MiuiSettingsUtils;->putBooleanToSystem(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    :cond_0
    invoke-static {p1}, Lcom/miui/home/launcher/AnalyticalDataCollector;->trackHomeSettingNoWord(Z)V

    return-void
.end method

.method public static synthetic lambda$onFinishInflate$3(Lcom/miui/home/launcher/LauncherMenu;Landroid/widget/CompoundButton;Z)V
    .locals 4

    iget-object p1, p0, Lcom/miui/home/launcher/LauncherMenu;->mOnlyWidgetNoWordSlidingButton:Lmiuix/slidingwidget/widget/SlidingButton;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lmiuix/slidingwidget/widget/SlidingButton;->setChecked(Z)V

    iget-object p1, p0, Lcom/miui/home/launcher/LauncherMenu;->mOnlyWidgetNoWordSetting:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object p1, p0, Lcom/miui/home/launcher/LauncherMenu;->mOnlyWidgetNoWordSetting:Landroid/widget/LinearLayout;

    const v2, 0x7f0a03c8

    invoke-virtual {p1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, v0}, Landroid/view/View;->setEnabled(Z)V

    iget-object p1, p0, Lcom/miui/home/launcher/LauncherMenu;->mOnlyWidgetNoWordSlidingButton:Lmiuix/slidingwidget/widget/SlidingButton;

    invoke-virtual {p1, v0}, Lmiuix/slidingwidget/widget/SlidingButton;->setEnabled(Z)V

    invoke-static {}, Lcom/miui/home/launcher/common/Utilities;->isNoWordModel()Z

    move-result p1

    if-ne p2, p1, :cond_0

    return-void

    :cond_0
    iget-object p1, p0, Lcom/miui/home/launcher/LauncherMenu;->mLauncher:Lcom/miui/home/launcher/Launcher;

    const/4 v0, 0x7

    new-instance v2, Lcom/miui/home/launcher/EditStateChangeReason;

    const-string v3, "event_finish_button"

    invoke-direct {v2, v3}, Lcom/miui/home/launcher/EditStateChangeReason;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0, v1, v2}, Lcom/miui/home/launcher/Launcher;->setEditingState(ILjava/lang/Runnable;Lcom/miui/home/launcher/EditStateChangeReason;)V

    new-instance p1, Lcom/miui/home/launcher/-$$Lambda$LauncherMenu$vQWuXSr5GeYnia089wTrRy3ErI0;

    invoke-direct {p1, p0, p2}, Lcom/miui/home/launcher/-$$Lambda$LauncherMenu$vQWuXSr5GeYnia089wTrRy3ErI0;-><init>(Lcom/miui/home/launcher/LauncherMenu;Z)V

    invoke-static {p1}, Lcom/miui/home/launcher/common/BackgroundThread;->postAtFrontOfQueue(Ljava/lang/Runnable;)V

    return-void
.end method

.method public static synthetic lambda$onFinishInflate$4(Lcom/miui/home/launcher/LauncherMenu;Landroid/widget/CompoundButton;Z)V
    .locals 1

    invoke-static {}, Lcom/miui/home/launcher/common/Utilities;->isOnlyWidgetNoWordModel()Z

    move-result p1

    if-ne p2, p1, :cond_0

    return-void

    :cond_0
    sget-object p1, Lcom/miui/home/launcher/util/noword/NoWordSettingHelper;->INSTANCE:Lcom/miui/home/launcher/util/noword/NoWordSettingHelper;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/miui/home/launcher/util/noword/NoWordSettingHelper;->setSwitchingNoWordModel(Z)V

    iget-object p1, p0, Lcom/miui/home/launcher/LauncherMenu;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p1

    const-string v0, "miui_home_only_widget_no_word_model"

    invoke-static {p1, v0, p2}, Lcom/miui/launcher/utils/MiuiSettingsUtils;->putBooleanToSystem(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    invoke-static {p2}, Lcom/miui/home/launcher/AnalyticalDataCollector;->trackHomeSettingOnlyWidgetNoWord(Z)V

    return-void
.end method

.method private setSelectedCellType([Landroid/view/View;)V
    .locals 6

    if-nez p1, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x2

    new-array v0, v0, [I

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    array-length v3, p1

    if-ge v2, v3, :cond_4

    aget-object v3, p1, v2

    invoke-virtual {v3, v1}, Landroid/view/View;->setSelected(Z)V

    aget-object v3, p1, v2

    invoke-virtual {v3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_1

    goto :goto_1

    :cond_1
    aget-object v3, p1, v2

    invoke-virtual {v3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v0}, Lcom/miui/home/launcher/ScreenUtils;->parseCellsSize(Ljava/lang/String;[I)Z

    move-result v3

    if-nez v3, :cond_2

    return-void

    :cond_2
    aget v3, v0, v1

    invoke-static {}, Lcom/miui/home/launcher/DeviceConfig;->getCellCountX()I

    move-result v4

    if-ne v3, v4, :cond_3

    const/4 v3, 0x1

    aget v4, v0, v3

    invoke-static {}, Lcom/miui/home/launcher/DeviceConfig;->getCellCountY()I

    move-result v5

    if-ne v4, v5, :cond_3

    aget-object v4, p1, v2

    invoke-virtual {v4, v3}, Landroid/view/View;->setSelected(Z)V

    :cond_3
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_4
    return-void
.end method


# virtual methods
.method protected dispatchSaveInstanceState(Landroid/util/SparseArray;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method protected onFinishInflate()V
    .locals 5

    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    const v0, 0x7f0a0232

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/LauncherMenu;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/miui/home/launcher/LauncherMenu;->mNoWordSetting:Landroid/widget/LinearLayout;

    const v0, 0x7f0a0233

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/LauncherMenu;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/miui/home/launcher/LauncherMenu;->mOnlyWidgetNoWordSetting:Landroid/widget/LinearLayout;

    const v0, 0x7f0a0234

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/LauncherMenu;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/miui/home/launcher/LauncherMenu;->mMenuTransEffect:Landroid/widget/LinearLayout;

    invoke-static {}, Lcom/miui/home/launcher/DeviceConfig;->getDeviceType()Lcom/miui/home/launcher/DeviceType;

    move-result-object v0

    sget-object v1, Lcom/miui/home/launcher/DeviceType;->FOLDABLE_DEVICE:Lcom/miui/home/launcher/DeviceType;

    const/16 v2, 0x8

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/miui/home/launcher/LauncherMenu;->mMenuTransEffect:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :cond_0
    const v0, 0x7f0a0225

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/LauncherMenu;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/miui/home/launcher/LauncherMenu;->mDefaultScreenPreview:Landroid/widget/LinearLayout;

    const v0, 0x7f0a0224

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/LauncherMenu;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/miui/home/launcher/LauncherMenu;->mCellsSetting:Landroid/widget/LinearLayout;

    const v0, 0x7f0a0223

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/LauncherMenu;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/miui/home/launcher/LauncherMenu;->mAutoFill:Landroid/widget/LinearLayout;

    const v0, 0x7f0a0230

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/LauncherMenu;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/miui/home/launcher/LauncherMenu;->mLock:Landroid/widget/LinearLayout;

    const v0, 0x7f0a0231

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/LauncherMenu;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/miui/home/launcher/LauncherMenu;->mMore:Landroid/widget/LinearLayout;

    const v0, 0x7f0a0274

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/LauncherMenu;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiuix/slidingwidget/widget/SlidingButton;

    iput-object v0, p0, Lcom/miui/home/launcher/LauncherMenu;->mNoWordSlidingButton:Lmiuix/slidingwidget/widget/SlidingButton;

    const v0, 0x7f0a027e

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/LauncherMenu;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiuix/slidingwidget/widget/SlidingButton;

    iput-object v0, p0, Lcom/miui/home/launcher/LauncherMenu;->mOnlyWidgetNoWordSlidingButton:Lmiuix/slidingwidget/widget/SlidingButton;

    const v0, 0x7f0a0081

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/LauncherMenu;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiuix/slidingwidget/widget/SlidingButton;

    iput-object v0, p0, Lcom/miui/home/launcher/LauncherMenu;->mAutoFillSlidingButton:Lmiuix/slidingwidget/widget/SlidingButton;

    const v0, 0x7f0a0200

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/LauncherMenu;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiuix/slidingwidget/widget/SlidingButton;

    iput-object v0, p0, Lcom/miui/home/launcher/LauncherMenu;->mLockSlidingButton:Lmiuix/slidingwidget/widget/SlidingButton;

    invoke-static {}, Lcom/miui/home/launcher/util/noword/NoWordSettingHelperKt;->isNoWordAvailable()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/miui/home/launcher/LauncherMenu;->mNoWordSetting:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/miui/home/launcher/LauncherMenu;->mOnlyWidgetNoWordSetting:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    move v0, v1

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    iget-object v3, p0, Lcom/miui/home/launcher/LauncherMenu;->mNoWordSetting:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v3, p0, Lcom/miui/home/launcher/LauncherMenu;->mOnlyWidgetNoWordSetting:Landroid/widget/LinearLayout;

    invoke-static {}, Lcom/miui/home/launcher/MIUIWidgetUtil;->isMIUIWidgetSupport()Z

    move-result v4

    if-eqz v4, :cond_2

    move v2, v1

    :cond_2
    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :goto_0
    if-eqz v0, :cond_3

    iget-object v2, p0, Lcom/miui/home/launcher/LauncherMenu;->mNoWordSetting:Landroid/widget/LinearLayout;

    new-instance v3, Lcom/miui/home/launcher/-$$Lambda$LauncherMenu$MHR_4wX2wGC8uKfVwAvocOhv8q4;

    invoke-direct {v3, p0}, Lcom/miui/home/launcher/-$$Lambda$LauncherMenu$MHR_4wX2wGC8uKfVwAvocOhv8q4;-><init>(Lcom/miui/home/launcher/LauncherMenu;)V

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/miui/home/launcher/LauncherMenu;->mOnlyWidgetNoWordSetting:Landroid/widget/LinearLayout;

    new-instance v3, Lcom/miui/home/launcher/-$$Lambda$LauncherMenu$J-Y9WvRWDrTZ6148OGyMe1Ulqf8;

    invoke-direct {v3, p0}, Lcom/miui/home/launcher/-$$Lambda$LauncherMenu$J-Y9WvRWDrTZ6148OGyMe1Ulqf8;-><init>(Lcom/miui/home/launcher/LauncherMenu;)V

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_3
    iget-object v2, p0, Lcom/miui/home/launcher/LauncherMenu;->mMenuTransEffect:Landroid/widget/LinearLayout;

    new-instance v3, Lcom/miui/home/launcher/LauncherMenu$1;

    invoke-direct {v3, p0}, Lcom/miui/home/launcher/LauncherMenu$1;-><init>(Lcom/miui/home/launcher/LauncherMenu;)V

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/miui/home/launcher/LauncherMenu;->mDefaultScreenPreview:Landroid/widget/LinearLayout;

    new-instance v3, Lcom/miui/home/launcher/LauncherMenu$2;

    invoke-direct {v3, p0}, Lcom/miui/home/launcher/LauncherMenu$2;-><init>(Lcom/miui/home/launcher/LauncherMenu;)V

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/miui/home/launcher/LauncherMenu;->mAutoFill:Landroid/widget/LinearLayout;

    new-instance v3, Lcom/miui/home/launcher/LauncherMenu$3;

    invoke-direct {v3, p0}, Lcom/miui/home/launcher/LauncherMenu$3;-><init>(Lcom/miui/home/launcher/LauncherMenu;)V

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/miui/home/launcher/LauncherMenu;->mLock:Landroid/widget/LinearLayout;

    new-instance v3, Lcom/miui/home/launcher/LauncherMenu$4;

    invoke-direct {v3, p0}, Lcom/miui/home/launcher/LauncherMenu$4;-><init>(Lcom/miui/home/launcher/LauncherMenu;)V

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/miui/home/launcher/LauncherMenu;->mNoWordSlidingButton:Lmiuix/slidingwidget/widget/SlidingButton;

    new-instance v2, Lcom/miui/home/launcher/-$$Lambda$LauncherMenu$AKBg5RJGwjDyq9u1TELcnXP8Ykw;

    invoke-direct {v2, p0}, Lcom/miui/home/launcher/-$$Lambda$LauncherMenu$AKBg5RJGwjDyq9u1TELcnXP8Ykw;-><init>(Lcom/miui/home/launcher/LauncherMenu;)V

    invoke-virtual {v0, v2}, Lmiuix/slidingwidget/widget/SlidingButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v0, p0, Lcom/miui/home/launcher/LauncherMenu;->mOnlyWidgetNoWordSlidingButton:Lmiuix/slidingwidget/widget/SlidingButton;

    new-instance v2, Lcom/miui/home/launcher/-$$Lambda$LauncherMenu$Qs3PsVOQJiO5x_Tp6legmB0ZyVM;

    invoke-direct {v2, p0}, Lcom/miui/home/launcher/-$$Lambda$LauncherMenu$Qs3PsVOQJiO5x_Tp6legmB0ZyVM;-><init>(Lcom/miui/home/launcher/LauncherMenu;)V

    invoke-virtual {v0, v2}, Lmiuix/slidingwidget/widget/SlidingButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    :cond_4
    iget-object v0, p0, Lcom/miui/home/launcher/LauncherMenu;->mLockSlidingButton:Lmiuix/slidingwidget/widget/SlidingButton;

    new-instance v2, Lcom/miui/home/launcher/LauncherMenu$5;

    invoke-direct {v2, p0}, Lcom/miui/home/launcher/LauncherMenu$5;-><init>(Lcom/miui/home/launcher/LauncherMenu;)V

    invoke-virtual {v0, v2}, Lmiuix/slidingwidget/widget/SlidingButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v0, p0, Lcom/miui/home/launcher/LauncherMenu;->mAutoFillSlidingButton:Lmiuix/slidingwidget/widget/SlidingButton;

    new-instance v2, Lcom/miui/home/launcher/LauncherMenu$6;

    invoke-direct {v2, p0}, Lcom/miui/home/launcher/LauncherMenu$6;-><init>(Lcom/miui/home/launcher/LauncherMenu;)V

    invoke-virtual {v0, v2}, Lmiuix/slidingwidget/widget/SlidingButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    invoke-direct {p0}, Lcom/miui/home/launcher/LauncherMenu;->initCellSetting()V

    iget-object v0, p0, Lcom/miui/home/launcher/LauncherMenu;->mMore:Landroid/widget/LinearLayout;

    new-instance v2, Lcom/miui/home/launcher/LauncherMenu$7;

    invoke-direct {v2, p0}, Lcom/miui/home/launcher/LauncherMenu$7;-><init>(Lcom/miui/home/launcher/LauncherMenu;)V

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lcom/miui/home/launcher/LauncherMenu;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/miui/home/launcher/common/Utilities;->getMiuiDialogCornerRadius(Landroid/content/Context;)I

    move-result v0

    iget-object v2, p0, Lcom/miui/home/launcher/LauncherMenu;->mMenuTransEffect:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    int-to-float v0, v0

    const/4 v3, 0x0

    invoke-static {v2, v0, v3}, Lcom/miui/home/launcher/common/Utilities;->changeDrawableRadius(Landroid/graphics/drawable/Drawable;FF)V

    sget-boolean v0, Lcom/miui/home/launcher/common/Utilities;->IS_MIUI_10:Z

    if-eqz v0, :cond_5

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/LauncherMenu;->setShowDividers(I)V

    iget-object v0, p0, Lcom/miui/home/launcher/LauncherMenu;->mMenuTransEffect:Landroid/widget/LinearLayout;

    const v1, 0x7f0802fb

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_1

    :cond_5
    invoke-virtual {p0, v1}, Lcom/miui/home/launcher/LauncherMenu;->setShowDividers(I)V

    iget-object v0, p0, Lcom/miui/home/launcher/LauncherMenu;->mMenuTransEffect:Landroid/widget/LinearLayout;

    const v1, 0x7f0802fa

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    :goto_1
    return-void
.end method

.method public onShow()V
    .locals 4

    iget-object v0, p0, Lcom/miui/home/launcher/LauncherMenu;->mAutoFillSlidingButton:Lmiuix/slidingwidget/widget/SlidingButton;

    invoke-static {}, Lcom/miui/home/launcher/common/Utilities;->enableAutoFillEmpty()Z

    move-result v1

    invoke-virtual {v0, v1}, Lmiuix/slidingwidget/widget/SlidingButton;->setChecked(Z)V

    iget-object v0, p0, Lcom/miui/home/launcher/LauncherMenu;->mLockSlidingButton:Lmiuix/slidingwidget/widget/SlidingButton;

    invoke-static {}, Lcom/miui/home/launcher/common/Utilities;->isScreenCellsLocked()Z

    move-result v1

    invoke-virtual {v0, v1}, Lmiuix/slidingwidget/widget/SlidingButton;->setChecked(Z)V

    iget-object v0, p0, Lcom/miui/home/launcher/LauncherMenu;->mNoWordSetting:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/miui/home/launcher/LauncherMenu;->mNoWordSlidingButton:Lmiuix/slidingwidget/widget/SlidingButton;

    invoke-static {}, Lcom/miui/home/launcher/common/Utilities;->isNoWordModel()Z

    move-result v1

    invoke-virtual {v0, v1}, Lmiuix/slidingwidget/widget/SlidingButton;->setChecked(Z)V

    :cond_0
    iget-object v0, p0, Lcom/miui/home/launcher/LauncherMenu;->mOnlyWidgetNoWordSetting:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/miui/home/launcher/LauncherMenu;->mOnlyWidgetNoWordSlidingButton:Lmiuix/slidingwidget/widget/SlidingButton;

    invoke-static {}, Lcom/miui/home/launcher/common/Utilities;->isOnlyWidgetNoWordModel()Z

    move-result v1

    invoke-virtual {v0, v1}, Lmiuix/slidingwidget/widget/SlidingButton;->setChecked(Z)V

    :cond_1
    iget-object v0, p0, Lcom/miui/home/launcher/LauncherMenu;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/miui/home/launcher/DeviceConfig;->needShowCellsEntry(Landroid/content/Context;)Z

    move-result v0

    const/16 v1, 0x8

    if-eqz v0, :cond_4

    invoke-static {}, Lcom/miui/home/launcher/DeviceConfig;->isFoldDevice()Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/miui/home/launcher/LauncherMenu;->isCellSettingInited()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-direct {p0}, Lcom/miui/home/launcher/LauncherMenu;->initCellSetting()V

    goto :goto_1

    :cond_3
    const/4 v0, 0x3

    new-array v0, v0, [Landroid/view/View;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/miui/home/launcher/LauncherMenu;->mCellType1:Landroid/widget/Button;

    aput-object v3, v0, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/miui/home/launcher/LauncherMenu;->mCellType2:Landroid/widget/Button;

    aput-object v3, v0, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/miui/home/launcher/LauncherMenu;->mCellType3:Landroid/widget/Button;

    aput-object v3, v0, v2

    invoke-direct {p0, v0}, Lcom/miui/home/launcher/LauncherMenu;->setSelectedCellType([Landroid/view/View;)V

    goto :goto_1

    :cond_4
    :goto_0
    iget-object v0, p0, Lcom/miui/home/launcher/LauncherMenu;->mCellsSetting:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :goto_1
    invoke-static {}, Lcom/miui/home/launcher/common/Utilities;->isPadDevice()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/miui/home/launcher/LauncherMenu;->mDefaultScreenPreview:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :cond_5
    return-void
.end method
