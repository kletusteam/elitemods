.class public Lcom/miui/home/launcher/LauncherAppWidgetHost;
.super Landroid/appwidget/AppWidgetHost;


# instance fields
.field private mLauncher:Lcom/miui/home/launcher/Launcher;

.field private mTempWidgetInfo:Lcom/miui/home/launcher/LauncherAppWidgetInfo;

.field private mViews:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Ljava/lang/ref/WeakReference<",
            "Lcom/miui/home/launcher/LauncherWidgetView;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/miui/home/launcher/Launcher;I)V
    .locals 0

    invoke-direct {p0, p1, p3}, Landroid/appwidget/AppWidgetHost;-><init>(Landroid/content/Context;I)V

    new-instance p1, Landroid/util/SparseArray;

    invoke-direct {p1}, Landroid/util/SparseArray;-><init>()V

    iput-object p1, p0, Lcom/miui/home/launcher/LauncherAppWidgetHost;->mViews:Landroid/util/SparseArray;

    iput-object p2, p0, Lcom/miui/home/launcher/LauncherAppWidgetHost;->mLauncher:Lcom/miui/home/launcher/Launcher;

    return-void
.end method

.method static synthetic lambda$sendActionCancelled$0(Lcom/miui/home/launcher/BaseActivity;I)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lcom/miui/home/launcher/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    return-void
.end method

.method private sendActionCancelled(Lcom/miui/home/launcher/BaseActivity;I)V
    .locals 2

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/miui/home/launcher/-$$Lambda$LauncherAppWidgetHost$5VfgBH3VX5od3agHiCKVQTfamZs;

    invoke-direct {v1, p1, p2}, Lcom/miui/home/launcher/-$$Lambda$LauncherAppWidgetHost$5VfgBH3VX5od3agHiCKVQTfamZs;-><init>(Lcom/miui/home/launcher/BaseActivity;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method


# virtual methods
.method createLauncherWidgetView(Landroid/content/Context;ILcom/miui/home/launcher/LauncherAppWidgetInfo;Landroid/appwidget/AppWidgetProviderInfo;)Lcom/miui/home/launcher/LauncherWidgetView;
    .locals 3

    goto/32 :goto_6

    nop

    :goto_0
    invoke-virtual {v0, v1}, Lcom/miui/home/launcher/TitleTextView;->setText(Ljava/lang/CharSequence;)V

    goto/32 :goto_a

    nop

    :goto_1
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p4

    goto/32 :goto_d

    nop

    :goto_2
    iget-boolean v0, p3, Lcom/miui/home/launcher/LauncherAppWidgetInfo;->isMIUIWidget:Z

    goto/32 :goto_23

    nop

    :goto_3
    const/16 v1, 0x8

    goto/32 :goto_14

    nop

    :goto_4
    iget-object v0, p0, Lcom/miui/home/launcher/LauncherAppWidgetHost;->mLauncher:Lcom/miui/home/launcher/Launcher;

    goto/32 :goto_16

    nop

    :goto_5
    iget-object p3, p0, Lcom/miui/home/launcher/LauncherAppWidgetHost;->mViews:Landroid/util/SparseArray;

    goto/32 :goto_c

    nop

    :goto_6
    iput-object p3, p0, Lcom/miui/home/launcher/LauncherAppWidgetHost;->mTempWidgetInfo:Lcom/miui/home/launcher/LauncherAppWidgetInfo;

    goto/32 :goto_20

    nop

    :goto_7
    return-object p4

    :goto_8
    check-cast p4, Lcom/miui/home/launcher/LauncherWidgetView;

    goto/32 :goto_22

    nop

    :goto_9
    invoke-virtual {p4, v0}, Lcom/miui/home/launcher/LauncherWidgetView;->setWidgetTouchDetector(Lcom/miui/home/launcher/widget/device/WidgetTouchDetector;)V

    goto/32 :goto_21

    nop

    :goto_a
    goto :goto_15

    :goto_b
    goto/32 :goto_18

    nop

    :goto_c
    new-instance v0, Ljava/lang/ref/WeakReference;

    goto/32 :goto_e

    nop

    :goto_d
    const v1, 0x7f0d009f

    goto/32 :goto_1a

    nop

    :goto_e
    invoke-direct {v0, p4}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    goto/32 :goto_1b

    nop

    :goto_f
    invoke-virtual {v0}, Landroid/appwidget/AppWidgetHostView;->clearFocus()V

    goto/32 :goto_12

    nop

    :goto_10
    invoke-virtual {v0, p3}, Landroid/appwidget/AppWidgetHostView;->setTag(Ljava/lang/Object;)V

    goto/32 :goto_1e

    nop

    :goto_11
    iput-object p4, p3, Lcom/miui/home/launcher/LauncherAppWidgetInfo;->hostView:Lcom/miui/home/launcher/LauncherWidgetView;

    goto/32 :goto_5

    nop

    :goto_12
    const/4 p4, 0x1

    goto/32 :goto_19

    nop

    :goto_13
    invoke-virtual {p4, v0}, Lcom/miui/home/launcher/LauncherWidgetView;->setWidget(Landroid/appwidget/AppWidgetHostView;)V

    goto/32 :goto_4

    nop

    :goto_14
    invoke-virtual {v0, v1}, Lcom/miui/home/launcher/TitleTextView;->setVisibility(I)V

    :goto_15
    goto/32 :goto_11

    nop

    :goto_16
    invoke-virtual {v0}, Lcom/miui/home/launcher/Launcher;->getWidgetTouchDetector()Lcom/miui/home/launcher/widget/device/WidgetTouchDetector;

    move-result-object v0

    goto/32 :goto_9

    nop

    :goto_17
    iget-object v1, p3, Lcom/miui/home/launcher/LauncherAppWidgetInfo;->appName:Ljava/lang/String;

    goto/32 :goto_0

    nop

    :goto_18
    invoke-virtual {p4}, Lcom/miui/home/launcher/LauncherWidgetView;->getTitleView()Lcom/miui/home/launcher/TitleTextView;

    move-result-object v0

    goto/32 :goto_3

    nop

    :goto_19
    iput p4, p3, Lcom/miui/home/launcher/LauncherAppWidgetInfo;->status:I

    goto/32 :goto_1

    nop

    :goto_1a
    const/4 v2, 0x0

    goto/32 :goto_1f

    nop

    :goto_1b
    invoke-virtual {p3, p2, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto/32 :goto_1c

    nop

    :goto_1c
    invoke-static {p1, p2}, Lcom/miui/home/launcher/widget/device/MIUIWidgetDeviceAdapter;->updateMIUILargeScreenDeviceOptions(Landroid/content/Context;I)V

    goto/32 :goto_7

    nop

    :goto_1d
    invoke-virtual {p4}, Lcom/miui/home/launcher/LauncherWidgetView;->getTitleView()Lcom/miui/home/launcher/TitleTextView;

    move-result-object v0

    goto/32 :goto_17

    nop

    :goto_1e
    invoke-virtual {v0, p2, p4}, Landroid/appwidget/AppWidgetHostView;->setAppWidget(ILandroid/appwidget/AppWidgetProviderInfo;)V

    goto/32 :goto_f

    nop

    :goto_1f
    invoke-virtual {p4, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p4

    goto/32 :goto_8

    nop

    :goto_20
    invoke-virtual {p0, p1, p2, p4}, Lcom/miui/home/launcher/LauncherAppWidgetHost;->createView(Landroid/content/Context;ILandroid/appwidget/AppWidgetProviderInfo;)Landroid/appwidget/AppWidgetHostView;

    move-result-object v0

    goto/32 :goto_10

    nop

    :goto_21
    if-nez p3, :cond_0

    goto/32 :goto_b

    :cond_0
    goto/32 :goto_2

    nop

    :goto_22
    invoke-virtual {p4, p3}, Lcom/miui/home/launcher/LauncherWidgetView;->setTag(Ljava/lang/Object;)V

    goto/32 :goto_13

    nop

    :goto_23
    if-nez v0, :cond_1

    goto/32 :goto_b

    :cond_1
    goto/32 :goto_1d

    nop
.end method

.method createPendingWidgetView(Landroid/content/Context;Lcom/miui/home/launcher/Launcher;Lcom/miui/home/launcher/LauncherAppWidgetInfo;)Lcom/miui/home/launcher/LauncherWidgetView;
    .locals 2

    goto/32 :goto_1e

    nop

    :goto_0
    invoke-virtual {p1}, Lcom/miui/home/launcher/LauncherWidgetView;->getTitleView()Lcom/miui/home/launcher/TitleTextView;

    move-result-object p2

    goto/32 :goto_20

    nop

    :goto_1
    goto :goto_9

    :goto_2
    goto/32 :goto_0

    nop

    :goto_3
    invoke-virtual {p1, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    goto/32 :goto_14

    nop

    :goto_4
    if-nez p2, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_b

    nop

    :goto_5
    if-nez p3, :cond_1

    goto/32 :goto_2

    :cond_1
    goto/32 :goto_11

    nop

    :goto_6
    invoke-virtual {v0, p2, v1}, Lcom/miui/home/launcher/LauncherAppWidgetHostView;->setAppWidget(ILandroid/appwidget/AppWidgetProviderInfo;)V

    goto/32 :goto_1d

    nop

    :goto_7
    invoke-virtual {p1, p3}, Lcom/miui/home/launcher/LauncherWidgetView;->setTag(Ljava/lang/Object;)V

    goto/32 :goto_f

    nop

    :goto_8
    invoke-virtual {p2, v0}, Lcom/miui/home/launcher/TitleTextView;->setVisibility(I)V

    :goto_9
    goto/32 :goto_e

    nop

    :goto_a
    new-instance v0, Lcom/miui/home/launcher/PendingAppWidgetHostView;

    goto/32 :goto_c

    nop

    :goto_b
    invoke-virtual {p1}, Lcom/miui/home/launcher/LauncherWidgetView;->getTitleView()Lcom/miui/home/launcher/TitleTextView;

    move-result-object p2

    goto/32 :goto_16

    nop

    :goto_c
    invoke-direct {v0, p1, p2, p3}, Lcom/miui/home/launcher/PendingAppWidgetHostView;-><init>(Landroid/content/Context;Lcom/miui/home/launcher/Launcher;Lcom/miui/home/launcher/LauncherAppWidgetInfo;)V

    goto/32 :goto_1a

    nop

    :goto_d
    new-instance v0, Lcom/miui/home/launcher/PendingLoadWidgetHostView;

    goto/32 :goto_17

    nop

    :goto_e
    iput-object p1, p3, Lcom/miui/home/launcher/LauncherAppWidgetInfo;->hostView:Lcom/miui/home/launcher/LauncherWidgetView;

    goto/32 :goto_13

    nop

    :goto_f
    invoke-virtual {p1, v0}, Lcom/miui/home/launcher/LauncherWidgetView;->setWidget(Landroid/appwidget/AppWidgetHostView;)V

    goto/32 :goto_5

    nop

    :goto_10
    const/4 v1, 0x0

    goto/32 :goto_6

    nop

    :goto_11
    iget-boolean p2, p3, Lcom/miui/home/launcher/LauncherAppWidgetInfo;->isMIUIWidget:Z

    goto/32 :goto_4

    nop

    :goto_12
    invoke-virtual {p2, v0}, Lcom/miui/home/launcher/TitleTextView;->setText(Ljava/lang/CharSequence;)V

    goto/32 :goto_1

    nop

    :goto_13
    return-object p1

    :goto_14
    check-cast p1, Lcom/miui/home/launcher/LauncherWidgetView;

    goto/32 :goto_7

    nop

    :goto_15
    const p2, 0x7f0d009f

    goto/32 :goto_3

    nop

    :goto_16
    iget-object v0, p3, Lcom/miui/home/launcher/LauncherAppWidgetInfo;->appName:Ljava/lang/String;

    goto/32 :goto_12

    nop

    :goto_17
    invoke-direct {v0, p1, p2}, Lcom/miui/home/launcher/PendingLoadWidgetHostView;-><init>(Landroid/content/Context;Lcom/miui/home/launcher/Launcher;)V

    :goto_18
    goto/32 :goto_19

    nop

    :goto_19
    invoke-virtual {v0, p3}, Lcom/miui/home/launcher/LauncherAppWidgetHostView;->setTag(Ljava/lang/Object;)V

    goto/32 :goto_1c

    nop

    :goto_1a
    goto :goto_18

    :goto_1b
    goto/32 :goto_d

    nop

    :goto_1c
    iget p2, p3, Lcom/miui/home/launcher/LauncherAppWidgetInfo;->appWidgetId:I

    goto/32 :goto_10

    nop

    :goto_1d
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    goto/32 :goto_15

    nop

    :goto_1e
    invoke-virtual {p3}, Lcom/miui/home/launcher/LauncherAppWidgetInfo;->isRestore()Z

    move-result v0

    goto/32 :goto_1f

    nop

    :goto_1f
    if-nez v0, :cond_2

    goto/32 :goto_1b

    :cond_2
    goto/32 :goto_a

    nop

    :goto_20
    const/16 v0, 0x8

    goto/32 :goto_8

    nop
.end method

.method protected onCreateView(Landroid/content/Context;ILandroid/appwidget/AppWidgetProviderInfo;)Landroid/appwidget/AppWidgetHostView;
    .locals 1

    new-instance p3, Lcom/miui/home/launcher/LauncherAppWidgetHostView;

    iget-object v0, p0, Lcom/miui/home/launcher/LauncherAppWidgetHost;->mLauncher:Lcom/miui/home/launcher/Launcher;

    invoke-direct {p3, p1, v0}, Lcom/miui/home/launcher/LauncherAppWidgetHostView;-><init>(Landroid/content/Context;Lcom/miui/home/launcher/Launcher;)V

    iget-object p1, p0, Lcom/miui/home/launcher/LauncherAppWidgetHost;->mTempWidgetInfo:Lcom/miui/home/launcher/LauncherAppWidgetInfo;

    if-eqz p1, :cond_0

    iget p1, p1, Lcom/miui/home/launcher/LauncherAppWidgetInfo;->appWidgetId:I

    if-ne p1, p2, :cond_0

    const p1, 0x7f0a023f

    iget-object p2, p0, Lcom/miui/home/launcher/LauncherAppWidgetHost;->mTempWidgetInfo:Lcom/miui/home/launcher/LauncherAppWidgetInfo;

    iget-boolean p2, p2, Lcom/miui/home/launcher/LauncherAppWidgetInfo;->miuiAutoScale:Z

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-virtual {p3, p1, p2}, Lcom/miui/home/launcher/LauncherAppWidgetHostView;->setTag(ILjava/lang/Object;)V

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/miui/home/launcher/LauncherAppWidgetHost;->mTempWidgetInfo:Lcom/miui/home/launcher/LauncherAppWidgetInfo;

    :cond_0
    return-object p3
.end method

.method protected onProviderChanged(ILandroid/appwidget/AppWidgetProviderInfo;)V
    .locals 0

    invoke-super {p0, p1, p2}, Landroid/appwidget/AppWidgetHost;->onProviderChanged(ILandroid/appwidget/AppWidgetProviderInfo;)V

    iget-object p2, p0, Lcom/miui/home/launcher/LauncherAppWidgetHost;->mViews:Landroid/util/SparseArray;

    invoke-virtual {p2, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/ref/WeakReference;

    if-nez p1, :cond_0

    return-void

    :cond_0
    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/miui/home/launcher/LauncherWidgetView;

    if-nez p1, :cond_1

    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/miui/home/launcher/LauncherWidgetView;->onProviderChanged()V

    return-void
.end method

.method public onScreenSizeChanged()V
    .locals 3

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/miui/home/launcher/LauncherAppWidgetHost;->mViews:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lcom/miui/home/launcher/LauncherAppWidgetHost;->mViews:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    if-nez v1, :cond_0

    goto :goto_1

    :cond_0
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/home/launcher/LauncherWidgetView;

    if-nez v1, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {v1}, Lcom/miui/home/launcher/LauncherWidgetView;->onScreenSizeChanged()V

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method public startConfigActivity(Lcom/miui/home/launcher/BaseActivity;II)V
    .locals 6

    const/4 v3, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v4, p3

    :try_start_0
    invoke-virtual/range {v0 .. v5}, Lcom/miui/home/launcher/LauncherAppWidgetHost;->startAppWidgetConfigureActivityForResult(Landroid/app/Activity;IIILandroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const p2, 0x7f110031

    const/4 v0, 0x0

    invoke-static {p1, p2, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object p2

    invoke-virtual {p2}, Landroid/widget/Toast;->show()V

    invoke-direct {p0, p1, p3}, Lcom/miui/home/launcher/LauncherAppWidgetHost;->sendActionCancelled(Lcom/miui/home/launcher/BaseActivity;I)V

    :goto_0
    return-void
.end method
