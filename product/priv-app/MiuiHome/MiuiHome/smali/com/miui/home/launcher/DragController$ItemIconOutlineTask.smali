.class Lcom/miui/home/launcher/DragController$ItemIconOutlineTask;
.super Lcom/miui/home/launcher/DragController$OutlineTask;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/home/launcher/DragController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ItemIconOutlineTask"
.end annotation


# direct methods
.method constructor <init>(Landroid/view/View;F)V
    .locals 1

    invoke-static {}, Lcom/miui/home/library/utils/AsyncTaskExecutorHelper;->getScreenSerialExecutor()Lcom/miui/home/library/utils/AsyncTaskExecutorHelper$SerialExecutor;

    move-result-object v0

    invoke-direct {p0, v0, p1, p2}, Lcom/miui/home/launcher/DragController$OutlineTask;-><init>(Ljava/util/concurrent/Executor;Landroid/view/View;F)V

    return-void
.end method


# virtual methods
.method getOutline()Landroid/graphics/Bitmap;
    .locals 2

    goto/32 :goto_2

    nop

    :goto_0
    iget v1, p0, Lcom/miui/home/launcher/DragController$ItemIconOutlineTask;->scale:F

    goto/32 :goto_3

    nop

    :goto_1
    return-object v0

    :goto_2
    iget-object v0, p0, Lcom/miui/home/launcher/DragController$ItemIconOutlineTask;->view:Landroid/view/View;

    goto/32 :goto_0

    nop

    :goto_3
    invoke-static {v0, v1}, Lcom/miui/home/launcher/DragController;->createOutline(Landroid/view/View;F)Landroid/graphics/Bitmap;

    move-result-object v0

    goto/32 :goto_1

    nop
.end method
