.class public Lcom/miui/home/launcher/backup/settings/BackupSettingsPerfectIcon;
.super Lcom/miui/home/launcher/backup/settings/BackupSettingsBase$BackupSettingsBaseBoolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    const-string v0, "key_miui_mod_icon_enable"

    invoke-direct {p0, v0}, Lcom/miui/home/launcher/backup/settings/BackupSettingsBase$BackupSettingsBaseBoolean;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method getSettingsValue()Ljava/lang/Boolean;
    .locals 3

    goto/32 :goto_3

    nop

    :goto_0
    invoke-virtual {v0}, Lcom/miui/home/launcher/Application;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    goto/32 :goto_6

    nop

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/32 :goto_2

    nop

    :goto_2
    return-object v0

    :goto_3
    invoke-static {}, Lcom/miui/home/launcher/Application;->getInstance()Lcom/miui/home/launcher/Application;

    move-result-object v0

    goto/32 :goto_0

    nop

    :goto_4
    invoke-static {v0, v1, v2}, Lcom/miui/launcher/utils/MiuiSettingsUtils;->getBooleanFromSystem(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    goto/32 :goto_1

    nop

    :goto_5
    const/4 v2, 0x1

    goto/32 :goto_4

    nop

    :goto_6
    const-string v1, "key_miui_mod_icon_enable"

    goto/32 :goto_5

    nop
.end method

.method bridge synthetic getSettingsValue()Ljava/lang/Object;
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    invoke-virtual {p0}, Lcom/miui/home/launcher/backup/settings/BackupSettingsPerfectIcon;->getSettingsValue()Ljava/lang/Boolean;

    move-result-object v0

    goto/32 :goto_1

    nop

    :goto_1
    return-object v0
.end method

.method putSettingsValue(Ljava/lang/Boolean;)V
    .locals 2

    goto/32 :goto_5

    nop

    :goto_0
    invoke-static {v0, v1, p1}, Lcom/miui/launcher/utils/MiuiSettingsUtils;->putBooleanToSystem(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    goto/32 :goto_2

    nop

    :goto_1
    invoke-virtual {v0}, Lcom/miui/home/launcher/Application;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    goto/32 :goto_4

    nop

    :goto_2
    return-void

    :goto_3
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    goto/32 :goto_0

    nop

    :goto_4
    const-string v1, "key_miui_mod_icon_enable"

    goto/32 :goto_3

    nop

    :goto_5
    invoke-static {}, Lcom/miui/home/launcher/Application;->getInstance()Lcom/miui/home/launcher/Application;

    move-result-object v0

    goto/32 :goto_1

    nop
.end method

.method bridge synthetic putSettingsValue(Ljava/lang/Object;)V
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    check-cast p1, Ljava/lang/Boolean;

    goto/32 :goto_2

    nop

    :goto_1
    return-void

    :goto_2
    invoke-virtual {p0, p1}, Lcom/miui/home/launcher/backup/settings/BackupSettingsPerfectIcon;->putSettingsValue(Ljava/lang/Boolean;)V

    goto/32 :goto_1

    nop
.end method
