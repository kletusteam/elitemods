.class public Lcom/miui/home/launcher/LauncherWidgetContainerView;
.super Landroid/widget/FrameLayout;


# instance fields
.field private final DEBUG_ALIGNMENT:Z

.field private mGestureDetector:Landroid/view/GestureDetector;

.field private final mPaddingTop:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/miui/home/launcher/LauncherWidgetContainerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/miui/home/launcher/LauncherWidgetContainerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 p2, 0x0

    iput-boolean p2, p0, Lcom/miui/home/launcher/LauncherWidgetContainerView;->DEBUG_ALIGNMENT:Z

    new-instance p3, Landroid/view/GestureDetector;

    new-instance v0, Lcom/miui/home/launcher/LauncherWidgetContainerView$1;

    invoke-direct {v0, p0}, Lcom/miui/home/launcher/LauncherWidgetContainerView$1;-><init>(Lcom/miui/home/launcher/LauncherWidgetContainerView;)V

    invoke-direct {p3, p1, v0}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object p3, p0, Lcom/miui/home/launcher/LauncherWidgetContainerView;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-static {}, Lcom/miui/home/launcher/DeviceConfig;->getMiuiWidgetPaddingTop()I

    move-result p1

    iput p1, p0, Lcom/miui/home/launcher/LauncherWidgetContainerView;->mPaddingTop:I

    iget p1, p0, Lcom/miui/home/launcher/LauncherWidgetContainerView;->mPaddingTop:I

    invoke-virtual {p0, p2, p1, p2, p2}, Lcom/miui/home/launcher/LauncherWidgetContainerView;->setPadding(IIII)V

    return-void
.end method

.method private getTheme()Lcom/miui/home/launcher/maml/Theme;
    .locals 1

    invoke-virtual {p0}, Lcom/miui/home/launcher/LauncherWidgetContainerView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Lcom/miui/home/launcher/maml/ThemeSupport;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/miui/home/launcher/LauncherWidgetContainerView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Lcom/miui/home/launcher/maml/ThemeSupport;

    invoke-interface {v0}, Lcom/miui/home/launcher/maml/ThemeSupport;->getTheme()Lcom/miui/home/launcher/maml/Theme;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method


# virtual methods
.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/miui/home/launcher/LauncherWidgetContainerView;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    invoke-static {}, Lcom/miui/home/launcher/Application;->getLauncher()Lcom/miui/home/launcher/Launcher;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/home/launcher/Launcher;->isInEditing()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1
.end method

.method protected onMeasure(II)V
    .locals 6

    invoke-virtual {p0}, Lcom/miui/home/launcher/LauncherWidgetContainerView;->getParent()Landroid/view/ViewParent;

    move-result-object p2

    instance-of v0, p2, Lcom/miui/home/launcher/widget/LauncherAppWidgetHostViewContainer;

    if-eqz v0, :cond_4

    check-cast p2, Landroid/view/View;

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/miui/home/launcher/ItemInfo;

    instance-of v0, p2, Lcom/miui/home/launcher/MIUIWidgetBasicInfo;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    move-object v0, p2

    check-cast v0, Lcom/miui/home/launcher/MIUIWidgetBasicInfo;

    iget-boolean v0, v0, Lcom/miui/home/launcher/MIUIWidgetBasicInfo;->isMIUIWidget:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_0
    invoke-direct {p0}, Lcom/miui/home/launcher/LauncherWidgetContainerView;->getTheme()Lcom/miui/home/launcher/maml/Theme;

    move-result-object v2

    const/4 v3, 0x2

    if-eqz v2, :cond_1

    invoke-interface {v2}, Lcom/miui/home/launcher/maml/Theme;->getTitleStyle()I

    move-result v4

    if-ne v4, v3, :cond_1

    move v0, v1

    :cond_1
    iget v1, p2, Lcom/miui/home/launcher/ItemInfo;->spanX:I

    iget p2, p2, Lcom/miui/home/launcher/ItemInfo;->spanY:I

    invoke-static {v1, p2, v0}, Lcom/miui/home/launcher/DeviceConfig;->getMiuiWidgetSizeSpec(IIZ)J

    move-result-wide v0

    const/16 p2, 0x20

    shr-long v4, v0, p2

    long-to-int p2, v4

    long-to-int v0, v0

    if-eqz v2, :cond_2

    invoke-interface {v2}, Lcom/miui/home/launcher/maml/Theme;->getWidgetPadding()Landroid/graphics/Rect;

    move-result-object v1

    invoke-static {}, Lcom/miui/home/launcher/DeviceConfig;->getMiuiWidgetPaddingTop()I

    move-result v2

    mul-int/2addr v2, v3

    iget v3, v1, Landroid/graphics/Rect;->top:I

    sub-int/2addr v2, v3

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v2, v1

    add-int/2addr v0, v2

    :cond_2
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p1

    if-lez p1, :cond_3

    if-lt p2, p1, :cond_3

    goto :goto_1

    :cond_3
    move p1, p2

    :goto_1
    const/high16 p2, 0x40000000    # 2.0f

    invoke-static {p1, p2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    invoke-static {v0, p2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    return-void

    :cond_4
    new-instance p1, Ljava/lang/RuntimeException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "parent must be LauncherAppWidgetHostViewContainer:"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/miui/home/launcher/LauncherWidgetContainerView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public onViewAdded(Landroid/view/View;)V
    .locals 5

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onViewAdded(Landroid/view/View;)V

    instance-of v0, p1, Lcom/miui/home/launcher/interfaces/AutoAlignable;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/miui/home/launcher/LauncherWidgetContainerView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/miui/home/launcher/MIUIWidgetUtil;->getMiuiWidgetPadding(Landroid/content/Context;)Landroid/graphics/Rect;

    move-result-object v0

    invoke-direct {p0}, Lcom/miui/home/launcher/LauncherWidgetContainerView;->getTheme()Lcom/miui/home/launcher/maml/Theme;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v1}, Lcom/miui/home/launcher/maml/Theme;->getWidgetPadding()Landroid/graphics/Rect;

    move-result-object v0

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v2, v0, Landroid/graphics/Rect;->left:I

    iget v3, v0, Landroid/graphics/Rect;->top:I

    iget v4, v0, Landroid/graphics/Rect;->right:I

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v1, v2, v3, v4, v0}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    invoke-virtual {p1, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v0, 0x0

    invoke-virtual {p1, v0, v0, v0, v0}, Landroid/view/View;->setPadding(IIII)V

    :cond_1
    return-void
.end method
