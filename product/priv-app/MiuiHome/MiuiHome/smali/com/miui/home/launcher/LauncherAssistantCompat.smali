.class public abstract Lcom/miui/home/launcher/LauncherAssistantCompat;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/miui/home/launcher/LauncherCallbacks;


# static fields
.field public static CAN_SWITCH_MINUS_SCREEN:Z

.field public static CLIENT_ID_BASE_4_0:Ljava/lang/String;

.field public static ONLY_USE_GOOGLE_MINUS_SCREEN:Z


# instance fields
.field protected final mLauncher:Lcom/miui/home/launcher/Launcher;

.field protected final mPackageName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v0, 0x0

    sput-boolean v0, Lcom/miui/home/launcher/LauncherAssistantCompat;->ONLY_USE_GOOGLE_MINUS_SCREEN:Z

    sget-boolean v1, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v1, :cond_0

    sget-boolean v0, Lcom/miui/home/launcher/LauncherAssistantCompat;->ONLY_USE_GOOGLE_MINUS_SCREEN:Z

    xor-int/lit8 v0, v0, 0x1

    :cond_0
    sput-boolean v0, Lcom/miui/home/launcher/LauncherAssistantCompat;->CAN_SWITCH_MINUS_SCREEN:Z

    return-void
.end method

.method constructor <init>(Lcom/miui/home/launcher/Launcher;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/miui/home/launcher/LauncherAssistantCompat;->mLauncher:Lcom/miui/home/launcher/Launcher;

    iput-object p2, p0, Lcom/miui/home/launcher/LauncherAssistantCompat;->mPackageName:Ljava/lang/String;

    return-void
.end method

.method public static newInstance(Lcom/miui/home/launcher/Launcher;)Lcom/miui/home/launcher/LauncherAssistantCompat;
    .locals 2

    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/miui/home/launcher/DeviceConfig;->isUseGoogleMinusScreen()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/miui/home/launcher/LauncherAssistantCompatGoogle;

    const-string v1, "com.google.android.googlequicksearchbox"

    invoke-direct {v0, p0, v1}, Lcom/miui/home/launcher/LauncherAssistantCompatGoogle;-><init>(Lcom/miui/home/launcher/Launcher;Ljava/lang/String;)V

    return-object v0

    :cond_0
    invoke-static {}, Lcom/miui/home/launcher/DeviceConfig;->isUseGlobalMinusScreen()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/miui/home/launcher/ApplicationConfig;->isNewGlobalAssistantInstalled()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lcom/miui/home/launcher/LauncherAssistantCompatMIUI;

    const-string v1, "com.mi.globalminusscreen"

    invoke-direct {v0, p0, v1}, Lcom/miui/home/launcher/LauncherAssistantCompatMIUI;-><init>(Lcom/miui/home/launcher/Launcher;Ljava/lang/String;)V

    return-object v0

    :cond_1
    new-instance v0, Lcom/miui/home/launcher/LauncherAssistantCompatGoogle;

    const-string v1, "com.mi.android.globalminusscreen"

    invoke-direct {v0, p0, v1}, Lcom/miui/home/launcher/LauncherAssistantCompatGoogle;-><init>(Lcom/miui/home/launcher/Launcher;Ljava/lang/String;)V

    return-object v0

    :cond_2
    new-instance v0, Lcom/miui/home/launcher/LauncherAssistantCompatMIUI;

    const-string v1, "com.miui.personalassistant"

    invoke-direct {v0, p0, v1}, Lcom/miui/home/launcher/LauncherAssistantCompatMIUI;-><init>(Lcom/miui/home/launcher/Launcher;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public abstract destroyLauncherClient()V
.end method

.method public abstract updateLauncherClient(Lcom/miui/home/launcher/allapps/LauncherMode;)V
.end method
