.class public Lcom/miui/home/launcher/UninstallDialogWrapper;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final TAG:Ljava/lang/String;

.field private mConfirmUninstall:Ljava/lang/Runnable;

.field private mDropTargetBar:Lcom/miui/home/launcher/DropTargetBar;

.field private mIsUninstallDialogHideAnimRunning:Z

.field private mLauncher:Lcom/miui/home/launcher/Launcher;

.field private mShowUninstallDialogAnimDuration:I

.field private mShowUninstallDialogAnimator:Landroid/animation/ValueAnimator;

.field private mShowUninstallDialogStartSize:I

.field private mUninstallAnimShowing:Z

.field private mUninstallConfirmAppDialog:Lmiui/home/lib/dialog/AlertDialog;

.field private mUninstallDialog:Lcom/miui/home/launcher/UninstallDialog;

.field private mUninstallDialogCanceled:Z

.field private mUninstallDialogConflictsListener:Lcom/miui/home/launcher/common/ConflictsManager$ConflictsListener;

.field private mUninstallDialogShowing:Z


# direct methods
.method constructor <init>(Lcom/miui/home/launcher/DropTargetBar;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "UninstallDialogWrapper"

    iput-object v0, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->TAG:Ljava/lang/String;

    new-instance v0, Landroid/animation/ValueAnimator;

    invoke-direct {v0}, Landroid/animation/ValueAnimator;-><init>()V

    iput-object v0, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mShowUninstallDialogAnimator:Landroid/animation/ValueAnimator;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mUninstallDialogShowing:Z

    iput-boolean v0, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mUninstallAnimShowing:Z

    iput-boolean v0, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mUninstallDialogCanceled:Z

    iput-boolean v0, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mIsUninstallDialogHideAnimRunning:Z

    new-instance v0, Lcom/miui/home/launcher/UninstallDialogWrapper$8;

    invoke-direct {v0, p0}, Lcom/miui/home/launcher/UninstallDialogWrapper$8;-><init>(Lcom/miui/home/launcher/UninstallDialogWrapper;)V

    iput-object v0, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mUninstallDialogConflictsListener:Lcom/miui/home/launcher/common/ConflictsManager$ConflictsListener;

    iput-object p1, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mDropTargetBar:Lcom/miui/home/launcher/DropTargetBar;

    iget-object p1, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mDropTargetBar:Lcom/miui/home/launcher/DropTargetBar;

    invoke-static {p1}, Lcom/miui/home/launcher/Launcher;->getLauncher(Landroid/view/View;)Lcom/miui/home/launcher/Launcher;

    move-result-object p1

    iput-object p1, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mLauncher:Lcom/miui/home/launcher/Launcher;

    iget-object p1, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mDropTargetBar:Lcom/miui/home/launcher/DropTargetBar;

    const v0, 0x7f0a03dc

    invoke-virtual {p1, v0}, Lcom/miui/home/launcher/DropTargetBar;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/miui/home/launcher/UninstallDialog;

    iput-object p1, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mUninstallDialog:Lcom/miui/home/launcher/UninstallDialog;

    iget-object p1, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mUninstallDialog:Lcom/miui/home/launcher/UninstallDialog;

    iget-object v0, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mLauncher:Lcom/miui/home/launcher/Launcher;

    invoke-virtual {p1, v0}, Lcom/miui/home/launcher/UninstallDialog;->setLauncher(Lcom/miui/home/launcher/Launcher;)V

    iget-object p1, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mUninstallDialog:Lcom/miui/home/launcher/UninstallDialog;

    const v0, 0x7f0a00a0

    invoke-virtual {p1, v0}, Lcom/miui/home/launcher/UninstallDialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object p1, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mUninstallDialog:Lcom/miui/home/launcher/UninstallDialog;

    const v0, 0x7f0a00a2

    invoke-virtual {p1, v0}, Lcom/miui/home/launcher/UninstallDialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object p1, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mLauncher:Lcom/miui/home/launcher/Launcher;

    invoke-virtual {p1}, Lcom/miui/home/launcher/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const v0, 0x7f0b0031

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result p1

    iput p1, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mShowUninstallDialogAnimDuration:I

    iget-object p1, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mShowUninstallDialogAnimator:Landroid/animation/ValueAnimator;

    iget v0, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mShowUninstallDialogAnimDuration:I

    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    iget-object p1, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mShowUninstallDialogAnimator:Landroid/animation/ValueAnimator;

    new-instance v0, Lcom/miui/home/launcher/UninstallDialogWrapper$1;

    invoke-direct {v0, p0}, Lcom/miui/home/launcher/UninstallDialogWrapper$1;-><init>(Lcom/miui/home/launcher/UninstallDialogWrapper;)V

    invoke-virtual {p1, v0}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    iget-object p1, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mShowUninstallDialogAnimator:Landroid/animation/ValueAnimator;

    new-instance v0, Lcom/miui/home/launcher/UninstallDialogWrapper$2;

    invoke-direct {v0, p0}, Lcom/miui/home/launcher/UninstallDialogWrapper$2;-><init>(Lcom/miui/home/launcher/UninstallDialogWrapper;)V

    invoke-virtual {p1, v0}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    return-void
.end method

.method static synthetic access$000(Lcom/miui/home/launcher/UninstallDialogWrapper;)Lcom/miui/home/launcher/Launcher;
    .locals 0

    iget-object p0, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mLauncher:Lcom/miui/home/launcher/Launcher;

    return-object p0
.end method

.method static synthetic access$100(Lcom/miui/home/launcher/UninstallDialogWrapper;)I
    .locals 0

    iget p0, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mShowUninstallDialogStartSize:I

    return p0
.end method

.method static synthetic access$200(Lcom/miui/home/launcher/UninstallDialogWrapper;)Lcom/miui/home/launcher/UninstallDialog;
    .locals 0

    iget-object p0, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mUninstallDialog:Lcom/miui/home/launcher/UninstallDialog;

    return-object p0
.end method

.method static synthetic access$300(Lcom/miui/home/launcher/UninstallDialogWrapper;)Ljava/lang/Runnable;
    .locals 0

    iget-object p0, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mConfirmUninstall:Ljava/lang/Runnable;

    return-object p0
.end method

.method static synthetic access$400(Lcom/miui/home/launcher/UninstallDialogWrapper;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mUninstallDialogShowing:Z

    return p0
.end method

.method static synthetic access$502(Lcom/miui/home/launcher/UninstallDialogWrapper;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mUninstallAnimShowing:Z

    return p1
.end method

.method static synthetic access$600(Lcom/miui/home/launcher/UninstallDialogWrapper;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mIsUninstallDialogHideAnimRunning:Z

    return p0
.end method

.method static synthetic access$602(Lcom/miui/home/launcher/UninstallDialogWrapper;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mIsUninstallDialogHideAnimRunning:Z

    return p1
.end method

.method static synthetic access$700(Lcom/miui/home/launcher/UninstallDialogWrapper;)Lcom/miui/home/launcher/common/ConflictsManager$ConflictsListener;
    .locals 0

    iget-object p0, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mUninstallDialogConflictsListener:Lcom/miui/home/launcher/common/ConflictsManager$ConflictsListener;

    return-object p0
.end method

.method static synthetic access$800(Lcom/miui/home/launcher/UninstallDialogWrapper;Ljava/util/List;)Z
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/home/launcher/UninstallDialogWrapper;->onConfirmUninstall(Ljava/util/List;)Z

    move-result p0

    return p0
.end method

.method static synthetic access$900(Lcom/miui/home/launcher/UninstallDialogWrapper;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/home/launcher/UninstallDialogWrapper;->showUninstallConfirmAppDialog()V

    return-void
.end method

.method private confirmUninstall()V
    .locals 3

    new-instance v0, Lcom/miui/home/launcher/UninstallDialogWrapper$5;

    invoke-direct {v0, p0}, Lcom/miui/home/launcher/UninstallDialogWrapper$5;-><init>(Lcom/miui/home/launcher/UninstallDialogWrapper;)V

    new-instance v1, Lcom/miui/home/launcher/UninstallDialogWrapper$6;

    invoke-direct {v1, p0}, Lcom/miui/home/launcher/UninstallDialogWrapper$6;-><init>(Lcom/miui/home/launcher/UninstallDialogWrapper;)V

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/miui/home/library/utils/AsyncTaskExecutorHelper;->execParallel(Ljava/util/function/Function;Ljava/util/function/Consumer;Ljava/lang/Object;)V

    return-void
.end method

.method private dismissUninstallDualAppDialog()V
    .locals 1

    iget-object v0, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mUninstallConfirmAppDialog:Lmiui/home/lib/dialog/AlertDialog;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lmiui/home/lib/dialog/AlertDialog;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mUninstallConfirmAppDialog:Lmiui/home/lib/dialog/AlertDialog;

    :cond_0
    return-void
.end method

.method private onConfirmUninstall(Ljava/util/List;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/miui/home/launcher/ShortcutInfo;",
            ">;)Z"
        }
    .end annotation

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v0, p1}, Lcom/miui/home/launcher/UninstallDialogWrapper;->showUninstallDialog(ZZLjava/util/List;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    return v0
.end method

.method private showUninstallConfirmAppDialog()V
    .locals 3

    iget-object v0, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mUninstallDialog:Lcom/miui/home/launcher/UninstallDialog;

    invoke-virtual {v0}, Lcom/miui/home/launcher/UninstallDialog;->getAllInfos()Ljava/util/List;

    move-result-object v0

    new-instance v1, Lcom/miui/home/launcher/uninstall/SecondConfirmDialog;

    iget-object v2, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mLauncher:Lcom/miui/home/launcher/Launcher;

    invoke-direct {v1, v2, v0}, Lcom/miui/home/launcher/uninstall/SecondConfirmDialog;-><init>(Landroid/content/Context;Ljava/util/List;)V

    new-instance v0, Lcom/miui/home/launcher/UninstallDialogWrapper$3;

    invoke-direct {v0, p0, v1}, Lcom/miui/home/launcher/UninstallDialogWrapper$3;-><init>(Lcom/miui/home/launcher/UninstallDialogWrapper;Lcom/miui/home/launcher/uninstall/SecondConfirmDialog;)V

    invoke-virtual {v1, v0}, Lcom/miui/home/launcher/uninstall/SecondConfirmDialog;->setPositiveConsumer(Ljava/util/function/Consumer;)V

    new-instance v0, Lcom/miui/home/launcher/UninstallDialogWrapper$4;

    invoke-direct {v0, p0}, Lcom/miui/home/launcher/UninstallDialogWrapper$4;-><init>(Lcom/miui/home/launcher/UninstallDialogWrapper;)V

    invoke-virtual {v1, v0}, Lcom/miui/home/launcher/uninstall/SecondConfirmDialog;->setCancelConsumer(Ljava/util/function/Consumer;)V

    iput-object v1, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mUninstallConfirmAppDialog:Lmiui/home/lib/dialog/AlertDialog;

    const v0, 0x7f110143

    invoke-virtual {v1, v0}, Lcom/miui/home/launcher/uninstall/SecondConfirmDialog;->setPositiveButTitle(I)V

    invoke-virtual {v1}, Lcom/miui/home/launcher/uninstall/SecondConfirmDialog;->show()V

    return-void
.end method


# virtual methods
.method public getShowUninstallDialogAnimDuration()I
    .locals 1

    iget v0, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mShowUninstallDialogAnimDuration:I

    return v0
.end method

.method getUninstallDialog()Lcom/miui/home/launcher/UninstallDialog;
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return-object v0

    :goto_1
    iget-object v0, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mUninstallDialog:Lcom/miui/home/launcher/UninstallDialog;

    goto/32 :goto_0

    nop
.end method

.method public getUninstallDialogConflictsListener()Lcom/miui/home/launcher/common/ConflictsManager$ConflictsListener;
    .locals 1

    iget-object v0, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mUninstallDialogConflictsListener:Lcom/miui/home/launcher/common/ConflictsManager$ConflictsListener;

    return-object v0
.end method

.method public isUninstallAnimShowing()Z
    .locals 1

    iget-boolean v0, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mUninstallAnimShowing:Z

    return v0
.end method

.method public isUninstallDialogShowing()Z
    .locals 1

    iget-boolean v0, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mUninstallDialogShowing:Z

    return v0
.end method

.method public onCancelUninstall()Z
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/UninstallDialogWrapper;->onCancelUninstall(Z)Z

    move-result v0

    return v0
.end method

.method public onCancelUninstall(Z)Z
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v0, v2}, Lcom/miui/home/launcher/UninstallDialogWrapper;->showUninstallDialog(ZZLjava/util/List;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-direct {p0}, Lcom/miui/home/launcher/UninstallDialogWrapper;->dismissUninstallDualAppDialog()V

    iget-object v1, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mUninstallDialog:Lcom/miui/home/launcher/UninstallDialog;

    invoke-virtual {v1, p1}, Lcom/miui/home/launcher/UninstallDialog;->cancelUninstall(Z)V

    return v0

    :cond_0
    return v1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result p1

    const v0, 0x7f0a00a0

    if-eq p1, v0, :cond_1

    const v0, 0x7f0a00a2

    if-eq p1, v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/miui/home/launcher/UninstallDialogWrapper;->confirmUninstall()V

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mConfirmUninstall:Ljava/lang/Runnable;

    if-nez p1, :cond_2

    invoke-virtual {p0}, Lcom/miui/home/launcher/UninstallDialogWrapper;->onCancelUninstall()Z

    :cond_2
    :goto_0
    return-void
.end method

.method public showUninstallDialog(ZZLjava/util/List;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ",
            "Ljava/util/List<",
            "Lcom/miui/home/launcher/ShortcutInfo;",
            ">;)Z"
        }
    .end annotation

    const-string v0, "UninstallDialogWrapper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mUninstallDialogShowing != isShow"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mUninstallDialogShowing:Z

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-eq v2, p1, :cond_0

    move v2, v3

    goto :goto_0

    :cond_0
    move v2, v4

    :goto_0
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v2, ", mUninstallDialogShowing"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mUninstallAnimShowing:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v2, ",isShow"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mUninstallDialogShowing:Z

    if-eq v0, p1, :cond_2

    iput-boolean p1, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mUninstallDialogShowing:Z

    iput-boolean p2, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mUninstallDialogCanceled:Z

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mLauncher:Lcom/miui/home/launcher/Launcher;

    invoke-virtual {p1}, Lcom/miui/home/launcher/Launcher;->getTipConflictsManager()Lcom/miui/home/launcher/common/ConflictsManager;

    move-result-object p1

    iget-object p2, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mUninstallDialogConflictsListener:Lcom/miui/home/launcher/common/ConflictsManager$ConflictsListener;

    invoke-virtual {p1, p2}, Lcom/miui/home/launcher/common/ConflictsManager;->obtainLock(Lcom/miui/home/launcher/common/ConflictsManager$ConflictsListener;)V

    goto :goto_1

    :cond_1
    invoke-virtual {p0, v4, p3}, Lcom/miui/home/launcher/UninstallDialogWrapper;->showUninstallDialogAnim(ZLjava/util/List;)V

    :goto_1
    return v3

    :cond_2
    return v4
.end method

.method public showUninstallDialogAnim(ZLjava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/List<",
            "Lcom/miui/home/launcher/ShortcutInfo;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mUninstallAnimShowing:Z

    iget-object v1, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mLauncher:Lcom/miui/home/launcher/Launcher;

    invoke-virtual {v1, p1}, Lcom/miui/home/launcher/Launcher;->blurBehindWithAnim(Z)V

    iget-object v1, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mLauncher:Lcom/miui/home/launcher/Launcher;

    xor-int/lit8 v2, p1, 0x1

    invoke-virtual {v1, v2}, Lcom/miui/home/launcher/Launcher;->enableFolderInteractive(Z)V

    iget-object v1, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mUninstallDialog:Lcom/miui/home/launcher/UninstallDialog;

    iget-boolean v2, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mUninstallDialogCanceled:Z

    invoke-virtual {v1, p1, v2, p2}, Lcom/miui/home/launcher/UninstallDialog;->onShow(ZZLjava/util/List;)V

    const-wide/16 v1, 0x0

    const/4 p2, 0x2

    const/4 v3, 0x0

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mConfirmUninstall:Ljava/lang/Runnable;

    iget-object p1, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mUninstallDialog:Lcom/miui/home/launcher/UninstallDialog;

    invoke-virtual {p1}, Lcom/miui/home/launcher/UninstallDialog;->getHeight()I

    move-result p1

    iput p1, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mShowUninstallDialogStartSize:I

    iget-object p1, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mShowUninstallDialogAnimator:Landroid/animation/ValueAnimator;

    new-array p2, p2, [I

    iget v4, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mShowUninstallDialogStartSize:I

    aput v4, p2, v3

    iget-object v4, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mUninstallDialog:Lcom/miui/home/launcher/UninstallDialog;

    invoke-virtual {v4}, Lcom/miui/home/launcher/UninstallDialog;->getMeasuredHeight()I

    move-result v4

    aput v4, p2, v0

    invoke-virtual {p1, p2}, Landroid/animation/ValueAnimator;->setIntValues([I)V

    iget-object p1, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mShowUninstallDialogAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {p1, v1, v2}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    new-instance p1, Lcom/miui/home/launcher/UninstallDialogWrapper$7;

    invoke-direct {p1, p0}, Lcom/miui/home/launcher/UninstallDialogWrapper$7;-><init>(Lcom/miui/home/launcher/UninstallDialogWrapper;)V

    invoke-static {p1}, Lcom/miui/home/launcher/common/Utilities;->useViewToPost(Ljava/lang/Runnable;)V

    iput-boolean v3, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mIsUninstallDialogHideAnimRunning:Z

    invoke-static {}, Lcom/miui/home/launcher/multiselect/MultiSelectMonitor;->getMonitor()Lcom/miui/home/launcher/multiselect/MultiSelectMonitor;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/miui/home/launcher/multiselect/MultiSelectMonitor;->onShowOrHideUninstallDialog(Z)V

    goto :goto_1

    :cond_0
    iput v3, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mShowUninstallDialogStartSize:I

    iget-object p1, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mShowUninstallDialogAnimator:Landroid/animation/ValueAnimator;

    new-array p2, p2, [I

    iget-object v4, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mUninstallDialog:Lcom/miui/home/launcher/UninstallDialog;

    invoke-virtual {v4}, Lcom/miui/home/launcher/UninstallDialog;->getHeight()I

    move-result v4

    aput v4, p2, v3

    aput v3, p2, v0

    invoke-virtual {p1, p2}, Landroid/animation/ValueAnimator;->setIntValues([I)V

    iget-object p1, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mShowUninstallDialogAnimator:Landroid/animation/ValueAnimator;

    iget-boolean p2, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mUninstallDialogCanceled:Z

    if-nez p2, :cond_2

    invoke-static {}, Lcom/miui/home/launcher/DeviceConfig;->isSupportCompleteAnimation()Z

    move-result p2

    if-nez p2, :cond_1

    goto :goto_0

    :cond_1
    const-wide/16 v1, 0x258

    :cond_2
    :goto_0
    invoke-virtual {p1, v1, v2}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    iget-object p1, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mLauncher:Lcom/miui/home/launcher/Launcher;

    invoke-virtual {p1}, Lcom/miui/home/launcher/Launcher;->isInNormalEditing()Z

    move-result p1

    if-nez p1, :cond_3

    iget-object p1, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mLauncher:Lcom/miui/home/launcher/Launcher;

    invoke-virtual {p1, v0}, Lcom/miui/home/launcher/Launcher;->showStatusBar(Z)V

    :cond_3
    iput-boolean v0, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mIsUninstallDialogHideAnimRunning:Z

    :goto_1
    iget-object p1, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mShowUninstallDialogAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->start()V

    return-void
.end method

.method startUninstallDialog()V
    .locals 3

    goto/32 :goto_1

    nop

    :goto_0
    const/4 v2, 0x0

    goto/32 :goto_4

    nop

    :goto_1
    iget-object v0, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mUninstallDialog:Lcom/miui/home/launcher/UninstallDialog;

    goto/32 :goto_9

    nop

    :goto_2
    if-gtz v0, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_3

    nop

    :goto_3
    iget-object v0, p0, Lcom/miui/home/launcher/UninstallDialogWrapper;->mDropTargetBar:Lcom/miui/home/launcher/DropTargetBar;

    goto/32 :goto_6

    nop

    :goto_4
    invoke-virtual {p0, v0, v1, v2}, Lcom/miui/home/launcher/UninstallDialogWrapper;->showUninstallDialog(ZZLjava/util/List;)Z

    :goto_5
    goto/32 :goto_7

    nop

    :goto_6
    invoke-virtual {v0}, Lcom/miui/home/launcher/DropTargetBar;->getRootView()Landroid/view/View;

    move-result-object v0

    goto/32 :goto_a

    nop

    :goto_7
    return-void

    :goto_8
    const/4 v0, 0x1

    goto/32 :goto_b

    nop

    :goto_9
    invoke-virtual {v0}, Lcom/miui/home/launcher/UninstallDialog;->getUninstallItemCount()I

    move-result v0

    goto/32 :goto_2

    nop

    :goto_a
    invoke-static {v0}, Lcom/miui/home/launcher/Launcher;->performLayoutNow(Landroid/view/View;)V

    goto/32 :goto_8

    nop

    :goto_b
    const/4 v1, 0x0

    goto/32 :goto_0

    nop
.end method
