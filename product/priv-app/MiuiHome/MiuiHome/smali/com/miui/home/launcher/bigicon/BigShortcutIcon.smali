.class public final Lcom/miui/home/launcher/bigicon/BigShortcutIcon;
.super Lcom/miui/home/launcher/ShortcutIcon;


# instance fields
.field private mBigIconImageViewContainer:Lcom/miui/home/launcher/bigicon/LauncherBigIconImageViewContainer;

.field private mInfo:Lcom/miui/home/launcher/ShortcutInfo;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/miui/home/launcher/ShortcutIcon;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method private final updateInfo(Ljava/lang/Object;)V
    .locals 3

    if-eqz p1, :cond_3

    check-cast p1, Lcom/miui/home/launcher/ShortcutInfo;

    iput-object p1, p0, Lcom/miui/home/launcher/bigicon/BigShortcutIcon;->mInfo:Lcom/miui/home/launcher/ShortcutInfo;

    iget-object p1, p0, Lcom/miui/home/launcher/bigicon/BigShortcutIcon;->mBigIconImageViewContainer:Lcom/miui/home/launcher/bigicon/LauncherBigIconImageViewContainer;

    if-nez p1, :cond_0

    const-string v0, "mBigIconImageViewContainer"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/miui/home/launcher/bigicon/BigShortcutIcon;->mInfo:Lcom/miui/home/launcher/ShortcutInfo;

    if-nez v0, :cond_1

    const-string v1, "mInfo"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    iget v0, v0, Lcom/miui/home/launcher/ShortcutInfo;->spanX:I

    iget-object v1, p0, Lcom/miui/home/launcher/bigicon/BigShortcutIcon;->mInfo:Lcom/miui/home/launcher/ShortcutInfo;

    if-nez v1, :cond_2

    const-string v2, "mInfo"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    iget v1, v1, Lcom/miui/home/launcher/ShortcutInfo;->spanY:I

    invoke-virtual {p1, v0, v1}, Lcom/miui/home/launcher/bigicon/LauncherBigIconImageViewContainer;->setSpanXY(II)V

    return-void

    :cond_3
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.miui.home.launcher.ShortcutInfo"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public acceptDrop(Lcom/miui/home/launcher/DragObject;)Z
    .locals 1

    const-string v0, "dragObject"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p1, 0x0

    return p1
.end method

.method public getHeightDiffBetweenImageAndImageView()I
    .locals 2

    iget-object v0, p0, Lcom/miui/home/launcher/bigicon/BigShortcutIcon;->mBigIconImageViewContainer:Lcom/miui/home/launcher/bigicon/LauncherBigIconImageViewContainer;

    if-nez v0, :cond_0

    const-string v1, "mBigIconImageViewContainer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/miui/home/launcher/bigicon/LauncherBigIconImageViewContainer;->getHeightDiffBetweenImageAndImageView()I

    move-result v0

    return v0
.end method

.method public getIconTransparentEdge()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final getItemInfoFromTag()Lcom/miui/home/launcher/ItemInfo;
    .locals 2

    iget-object v0, p0, Lcom/miui/home/launcher/bigicon/BigShortcutIcon;->mInfo:Lcom/miui/home/launcher/ShortcutInfo;

    if-nez v0, :cond_0

    const-string v1, "mInfo"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Lcom/miui/home/launcher/ItemInfo;

    return-object v0
.end method

.method protected getSpanX()I
    .locals 2

    iget-object v0, p0, Lcom/miui/home/launcher/bigicon/BigShortcutIcon;->mInfo:Lcom/miui/home/launcher/ShortcutInfo;

    if-nez v0, :cond_0

    const-string v1, "mInfo"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    iget v0, v0, Lcom/miui/home/launcher/ShortcutInfo;->spanX:I

    return v0
.end method

.method public getWidthDiffBetweenImageAndImageView()I
    .locals 2

    iget-object v0, p0, Lcom/miui/home/launcher/bigicon/BigShortcutIcon;->mBigIconImageViewContainer:Lcom/miui/home/launcher/bigicon/LauncherBigIconImageViewContainer;

    if-nez v0, :cond_0

    const-string v1, "mBigIconImageViewContainer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/miui/home/launcher/bigicon/LauncherBigIconImageViewContainer;->getWidthDiffBetweenImageAndImageView()I

    move-result v0

    return v0
.end method

.method public isBigIcon()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public isShownCheckBox()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onDrop(Lcom/miui/home/launcher/DragObject;)Z
    .locals 1

    const-string v0, "dragObject"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p1, 0x0

    return p1
.end method

.method protected onFinishInflate()V
    .locals 2

    const v0, 0x7f0a008e

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/bigicon/BigShortcutIcon;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.big_icon_top_container)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/miui/home/launcher/bigicon/LauncherBigIconImageViewContainer;

    iput-object v0, p0, Lcom/miui/home/launcher/bigicon/BigShortcutIcon;->mBigIconImageViewContainer:Lcom/miui/home/launcher/bigicon/LauncherBigIconImageViewContainer;

    invoke-super {p0}, Lcom/miui/home/launcher/ShortcutIcon;->onFinishInflate()V

    return-void
.end method

.method public onLaunchAppAnimStart()V
    .locals 0

    return-void
.end method

.method public setTag(Ljava/lang/Object;)V
    .locals 1

    const-string v0, "tag"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/miui/home/launcher/ShortcutIcon;->setTag(Ljava/lang/Object;)V

    invoke-direct {p0, p1}, Lcom/miui/home/launcher/bigicon/BigShortcutIcon;->updateInfo(Ljava/lang/Object;)V

    return-void
.end method

.method public updateSizeOnIconSizeChanged()V
    .locals 4

    invoke-static {}, Lcom/miui/home/launcher/Application;->getInstance()Lcom/miui/home/launcher/Application;

    move-result-object v0

    const-string v1, "Application.getInstance()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/miui/home/launcher/Application;->getIconCache()Lcom/miui/home/launcher/IconCache;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/home/launcher/bigicon/BigShortcutIcon;->mInfo:Lcom/miui/home/launcher/ShortcutInfo;

    if-nez v1, :cond_0

    const-string v2, "mInfo"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v1}, Lcom/miui/home/launcher/ShortcutInfo;->getPackageName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/home/launcher/bigicon/BigShortcutIcon;->mInfo:Lcom/miui/home/launcher/ShortcutInfo;

    if-nez v2, :cond_1

    const-string v3, "mInfo"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    iget-object v2, v2, Lcom/miui/home/launcher/ShortcutInfo;->user:Landroid/os/UserHandle;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, v2}, Lcom/miui/home/launcher/IconCache;->removeBigIcons(Ljava/lang/String;Ljava/lang/String;Landroid/os/UserHandle;)V

    iget-object v0, p0, Lcom/miui/home/launcher/bigicon/BigShortcutIcon;->mInfo:Lcom/miui/home/launcher/ShortcutInfo;

    if-nez v0, :cond_2

    const-string v1, "mInfo"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    check-cast v3, Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v3}, Lcom/miui/home/launcher/ShortcutInfo;->setIconDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/miui/home/launcher/bigicon/BigShortcutIcon;->mInfo:Lcom/miui/home/launcher/ShortcutInfo;

    if-nez v0, :cond_3

    const-string v1, "mInfo"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-static {}, Lcom/miui/home/launcher/Application;->getLauncher()Lcom/miui/home/launcher/Launcher;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/miui/home/launcher/ShortcutInfo;->updateBuddyIconView(Lcom/miui/home/launcher/Launcher;)V

    invoke-super {p0}, Lcom/miui/home/launcher/ShortcutIcon;->updateSizeOnIconSizeChanged()V

    return-void
.end method
