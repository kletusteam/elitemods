.class Lcom/miui/home/launcher/CategoryProvider$DatabaseHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/home/launcher/CategoryProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DatabaseHelper"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/home/launcher/CategoryProvider$DatabaseHelper$Upgrade;
    }
.end annotation


# instance fields
.field private mMaxCategoryId:I


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 3

    const-string v0, "app_category_new.db"

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    const/4 p1, -0x1

    iput p1, p0, Lcom/miui/home/launcher/CategoryProvider$DatabaseHelper;->mMaxCategoryId:I

    invoke-virtual {p0}, Lcom/miui/home/launcher/CategoryProvider$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iget v1, p0, Lcom/miui/home/launcher/CategoryProvider$DatabaseHelper;->mMaxCategoryId:I

    if-ne v1, p1, :cond_0

    invoke-direct {p0, v0}, Lcom/miui/home/launcher/CategoryProvider$DatabaseHelper;->initializeMaxCategoryId(Landroid/database/sqlite/SQLiteDatabase;)I

    move-result p1

    iput p1, p0, Lcom/miui/home/launcher/CategoryProvider$DatabaseHelper;->mMaxCategoryId:I

    :cond_0
    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Lcom/miui/home/launcher/CategoryProvider$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/home/launcher/CategoryProvider$DatabaseHelper;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method private initializeMaxCategoryId(Landroid/database/sqlite/SQLiteDatabase;)I
    .locals 2

    const-string v0, "SELECT MAX(categoryId) FROM category"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p1

    const/4 v0, -0x1

    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    goto :goto_0

    :cond_0
    move v1, v0

    :goto_0
    if-eqz p1, :cond_1

    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    :cond_1
    if-eq v1, v0, :cond_2

    return v1

    :cond_2
    new-instance p1, Ljava/lang/RuntimeException;

    const-string v0, "Error: could not query max id in category"

    invoke-direct {p1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method generateNewCategoryId()I
    .locals 2

    goto/32 :goto_2

    nop

    :goto_0
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_7

    nop

    :goto_1
    iput v0, p0, Lcom/miui/home/launcher/CategoryProvider$DatabaseHelper;->mMaxCategoryId:I

    goto/32 :goto_c

    nop

    :goto_2
    iget v0, p0, Lcom/miui/home/launcher/CategoryProvider$DatabaseHelper;->mMaxCategoryId:I

    goto/32 :goto_6

    nop

    :goto_3
    const/16 v1, 0x3e8

    goto/32 :goto_b

    nop

    :goto_4
    const-string v1, "Error: max category id was not initialized"

    goto/32 :goto_0

    nop

    :goto_5
    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto/32 :goto_1

    nop

    :goto_6
    if-gez v0, :cond_0

    goto/32 :goto_a

    :cond_0
    goto/32 :goto_3

    nop

    :goto_7
    throw v0

    :goto_8
    new-instance v0, Ljava/lang/RuntimeException;

    goto/32 :goto_4

    nop

    :goto_9
    return v0

    :goto_a
    goto/32 :goto_8

    nop

    :goto_b
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_5

    nop

    :goto_c
    iget v0, p0, Lcom/miui/home/launcher/CategoryProvider$DatabaseHelper;->mMaxCategoryId:I

    goto/32 :goto_9

    nop
.end method

.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    const-string v0, "CREATE TABLE IF NOT EXISTS category (categoryId INTEGER PRIMARY KEY, categoryName TEXT, categoryOrder INTEGER NOT NULL DEFAULT 0);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE IF NOT EXISTS repository (packageName TEXT, categoryId INTEGER);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE IF NOT EXISTS favorites (componentKey TEXT, categoryId INTEGER, PRIMARY KEY (componentKey,categoryId));"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0

    invoke-static {p1, p2, p3}, Lcom/miui/home/launcher/CategoryProvider$DatabaseHelper$Upgrade;->access$100(Landroid/database/sqlite/SQLiteDatabase;II)V

    return-void
.end method
