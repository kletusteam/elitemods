.class public final Lcom/miui/home/launcher/Workspace$CellInfo;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/home/launcher/Workspace;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CellInfo"
.end annotation


# instance fields
.field cellX:I

.field cellY:I

.field screenId:J

.field screenOrder:I

.field spanX:I

.field spanY:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method nextScreen(I)V
    .locals 1

    goto/32 :goto_b

    nop

    :goto_0
    if-gtz p1, :cond_0

    goto/32 :goto_e

    :cond_0
    goto/32 :goto_1

    nop

    :goto_1
    add-int/lit8 p1, p1, -0x1

    goto/32 :goto_7

    nop

    :goto_2
    return-void

    :goto_3
    iget p1, p0, Lcom/miui/home/launcher/Workspace$CellInfo;->screenOrder:I

    goto/32 :goto_0

    nop

    :goto_4
    goto :goto_9

    :goto_5
    goto/32 :goto_f

    nop

    :goto_6
    add-int/lit8 p1, p1, 0x1

    goto/32 :goto_8

    nop

    :goto_7
    iput p1, p0, Lcom/miui/home/launcher/Workspace$CellInfo;->screenOrder:I

    goto/32 :goto_d

    nop

    :goto_8
    iput p1, p0, Lcom/miui/home/launcher/Workspace$CellInfo;->screenOrder:I

    :goto_9
    goto/32 :goto_2

    nop

    :goto_a
    if-eq p1, v0, :cond_1

    goto/32 :goto_5

    :cond_1
    goto/32 :goto_3

    nop

    :goto_b
    const/4 v0, -0x1

    goto/32 :goto_a

    nop

    :goto_c
    iput v0, p0, Lcom/miui/home/launcher/Workspace$CellInfo;->screenOrder:I

    goto/32 :goto_4

    nop

    :goto_d
    goto :goto_9

    :goto_e
    goto/32 :goto_c

    nop

    :goto_f
    iget p1, p0, Lcom/miui/home/launcher/Workspace$CellInfo;->screenOrder:I

    goto/32 :goto_6

    nop
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CellInfo{screenOrder="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/miui/home/launcher/Workspace$CellInfo;->screenOrder:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", screenId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/miui/home/launcher/Workspace$CellInfo;->screenId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", cellX="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/miui/home/launcher/Workspace$CellInfo;->cellX:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", cellY="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/miui/home/launcher/Workspace$CellInfo;->cellY:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", spanX="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/miui/home/launcher/Workspace$CellInfo;->spanX:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", spanY="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/miui/home/launcher/Workspace$CellInfo;->spanY:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
