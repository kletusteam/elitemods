.class public Lcom/miui/home/launcher/LauncherMtzGadgetView;
.super Lcom/miui/home/launcher/widget/LauncherAppWidgetHostViewContainer;


# instance fields
.field private mMtzGadget:Lcom/miui/home/launcher/gadget/Gadget;

.field private mTitleTextView:Lcom/miui/home/launcher/TitleTextView;

.field private mWidgetContainer:Lcom/miui/home/launcher/LauncherWidgetContainerView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/home/launcher/widget/LauncherAppWidgetHostViewContainer;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/miui/home/launcher/widget/LauncherAppWidgetHostViewContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/miui/home/launcher/widget/LauncherAppWidgetHostViewContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method


# virtual methods
.method getGadget()Lcom/miui/home/launcher/gadget/Gadget;
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Lcom/miui/home/launcher/LauncherMtzGadgetView;->mMtzGadget:Lcom/miui/home/launcher/gadget/Gadget;

    goto/32 :goto_1

    nop

    :goto_1
    return-object v0
.end method

.method public getTitleView()Lcom/miui/home/launcher/TitleTextView;
    .locals 1

    iget-object v0, p0, Lcom/miui/home/launcher/LauncherMtzGadgetView;->mTitleTextView:Lcom/miui/home/launcher/TitleTextView;

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 1

    invoke-super {p0}, Lcom/miui/home/launcher/widget/LauncherAppWidgetHostViewContainer;->onFinishInflate()V

    const v0, 0x7f0a03fb

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/LauncherMtzGadgetView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/miui/home/launcher/LauncherWidgetContainerView;

    iput-object v0, p0, Lcom/miui/home/launcher/LauncherMtzGadgetView;->mWidgetContainer:Lcom/miui/home/launcher/LauncherWidgetContainerView;

    const v0, 0x7f0a01bf

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/LauncherMtzGadgetView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/miui/home/launcher/TitleTextView;

    iput-object v0, p0, Lcom/miui/home/launcher/LauncherMtzGadgetView;->mTitleTextView:Lcom/miui/home/launcher/TitleTextView;

    return-void
.end method

.method public onWallpaperColorChanged()V
    .locals 1

    invoke-super {p0}, Lcom/miui/home/launcher/widget/LauncherAppWidgetHostViewContainer;->onWallpaperColorChanged()V

    iget-object v0, p0, Lcom/miui/home/launcher/LauncherMtzGadgetView;->mMtzGadget:Lcom/miui/home/launcher/gadget/Gadget;

    invoke-virtual {v0}, Lcom/miui/home/launcher/gadget/Gadget;->onWallpaperColorChanged()V

    return-void
.end method

.method setGadget(Lcom/miui/home/launcher/gadget/Gadget;)V
    .locals 1

    goto/32 :goto_5

    nop

    :goto_0
    invoke-virtual {p1}, Lcom/miui/home/launcher/gadget/Gadget;->getTag()Ljava/lang/Object;

    move-result-object p1

    goto/32 :goto_1

    nop

    :goto_1
    invoke-virtual {p0, p1}, Lcom/miui/home/launcher/LauncherMtzGadgetView;->setTag(Ljava/lang/Object;)V

    goto/32 :goto_4

    nop

    :goto_2
    invoke-virtual {v0, p1}, Lcom/miui/home/launcher/LauncherWidgetContainerView;->addView(Landroid/view/View;)V

    goto/32 :goto_0

    nop

    :goto_3
    iget-object v0, p0, Lcom/miui/home/launcher/LauncherMtzGadgetView;->mWidgetContainer:Lcom/miui/home/launcher/LauncherWidgetContainerView;

    goto/32 :goto_2

    nop

    :goto_4
    return-void

    :goto_5
    iput-object p1, p0, Lcom/miui/home/launcher/LauncherMtzGadgetView;->mMtzGadget:Lcom/miui/home/launcher/gadget/Gadget;

    goto/32 :goto_3

    nop
.end method
