.class public Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/miui/home/launcher/allapps/AllAppsGridAdapter$BindViewCallback;


# instance fields
.field private mApps:Lcom/miui/home/launcher/allapps/AlphabeticalAppsList;

.field private mCurrentFastScrollSection:Ljava/lang/String;

.field private mFastScrollFrameIndex:I

.field private final mFastScrollFrames:[I

.field private mFastScrollToTargetSectionRunnable:Ljava/lang/Runnable;

.field private mHasFastScrollTouchSettled:Z

.field private mHasFastScrollTouchSettledAtLeastOnce:Z

.field private mRv:Lcom/miui/home/launcher/allapps/BaseAllAppsRecyclerView;

.field private mSmoothSnapNextFrameRunnable:Ljava/lang/Runnable;

.field private mTargetFastScrollPosition:I

.field private mTargetFastScrollSection:Ljava/lang/String;

.field private mTrackedFastScrollViews:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/miui/home/launcher/allapps/BaseAllAppsRecyclerView;Lcom/miui/home/launcher/allapps/AlphabeticalAppsList;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;->mTargetFastScrollPosition:I

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;->mTrackedFastScrollViews:Ljava/util/HashSet;

    const/16 v0, 0xa

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;->mFastScrollFrames:[I

    new-instance v0, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper$1;

    invoke-direct {v0, p0}, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper$1;-><init>(Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;)V

    iput-object v0, p0, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;->mSmoothSnapNextFrameRunnable:Ljava/lang/Runnable;

    new-instance v0, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper$2;

    invoke-direct {v0, p0}, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper$2;-><init>(Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;)V

    iput-object v0, p0, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;->mFastScrollToTargetSectionRunnable:Ljava/lang/Runnable;

    iput-object p1, p0, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;->mRv:Lcom/miui/home/launcher/allapps/BaseAllAppsRecyclerView;

    iput-object p2, p0, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;->mApps:Lcom/miui/home/launcher/allapps/AlphabeticalAppsList;

    return-void
.end method

.method static synthetic access$000(Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;)I
    .locals 0

    iget p0, p0, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;->mFastScrollFrameIndex:I

    return p0
.end method

.method static synthetic access$008(Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;)I
    .locals 2

    iget v0, p0, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;->mFastScrollFrameIndex:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;->mFastScrollFrameIndex:I

    return v0
.end method

.method static synthetic access$100(Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;)[I
    .locals 0

    iget-object p0, p0, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;->mFastScrollFrames:[I

    return-object p0
.end method

.method static synthetic access$200(Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;)Lcom/miui/home/launcher/allapps/BaseAllAppsRecyclerView;
    .locals 0

    iget-object p0, p0, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;->mRv:Lcom/miui/home/launcher/allapps/BaseAllAppsRecyclerView;

    return-object p0
.end method

.method static synthetic access$300(Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;)Ljava/lang/Runnable;
    .locals 0

    iget-object p0, p0, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;->mSmoothSnapNextFrameRunnable:Ljava/lang/Runnable;

    return-object p0
.end method

.method static synthetic access$402(Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;->mCurrentFastScrollSection:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$500(Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;->mTargetFastScrollSection:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$602(Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;->mHasFastScrollTouchSettled:Z

    return p1
.end method

.method static synthetic access$702(Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;->mHasFastScrollTouchSettledAtLeastOnce:Z

    return p1
.end method

.method static synthetic access$800(Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;->updateTrackedViewsFastScrollFocusState()V

    return-void
.end method

.method private resetTrackedViewsFastScrollFocusState()V
    .locals 2

    iget-object v0, p0, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;->mTrackedFastScrollViews:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    invoke-direct {p0, v1}, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;->resetTrackedViewsFastScrollFocusState(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private resetTrackedViewsFastScrollFocusState(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V
    .locals 1

    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->setEnabled(Z)V

    return-void
.end method

.method private smoothSnapToPosition(IILcom/miui/home/launcher/allapps/BaseAlphabeticalAppsList$FastScrollSectionInfo;)V
    .locals 6

    iget-object v0, p0, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;->mRv:Lcom/miui/home/launcher/allapps/BaseAllAppsRecyclerView;

    iget-object v1, p0, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;->mSmoothSnapNextFrameRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/miui/home/launcher/allapps/BaseAllAppsRecyclerView;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;->mRv:Lcom/miui/home/launcher/allapps/BaseAllAppsRecyclerView;

    iget-object v1, p0, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;->mFastScrollToTargetSectionRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/miui/home/launcher/allapps/BaseAllAppsRecyclerView;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-boolean v0, p0, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;->mHasFastScrollTouchSettled:Z

    const/4 v1, 0x0

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    iget-object v0, p3, Lcom/miui/home/launcher/allapps/BaseAlphabeticalAppsList$FastScrollSectionInfo;->sectionName:Ljava/lang/String;

    iput-object v0, p0, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;->mCurrentFastScrollSection:Ljava/lang/String;

    iput-object v1, p0, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;->mTargetFastScrollSection:Ljava/lang/String;

    invoke-direct {p0}, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;->updateTrackedViewsFastScrollFocusState()V

    goto :goto_1

    :cond_0
    iput-object v1, p0, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;->mCurrentFastScrollSection:Ljava/lang/String;

    iget-object v0, p3, Lcom/miui/home/launcher/allapps/BaseAlphabeticalAppsList$FastScrollSectionInfo;->sectionName:Ljava/lang/String;

    iput-object v0, p0, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;->mTargetFastScrollSection:Ljava/lang/String;

    iput-boolean v2, p0, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;->mHasFastScrollTouchSettled:Z

    invoke-direct {p0}, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;->updateTrackedViewsFastScrollFocusState()V

    iget-object v0, p0, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;->mRv:Lcom/miui/home/launcher/allapps/BaseAllAppsRecyclerView;

    iget-object v1, p0, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;->mFastScrollToTargetSectionRunnable:Ljava/lang/Runnable;

    iget-boolean v3, p0, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;->mHasFastScrollTouchSettledAtLeastOnce:Z

    if-eqz v3, :cond_1

    const-wide/16 v3, 0xc8

    goto :goto_0

    :cond_1
    const-wide/16 v3, 0x64

    :goto_0
    invoke-virtual {v0, v1, v3, v4}, Lcom/miui/home/launcher/allapps/BaseAllAppsRecyclerView;->postDelayed(Ljava/lang/Runnable;J)Z

    :goto_1
    iget-object v0, p0, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;->mApps:Lcom/miui/home/launcher/allapps/AlphabeticalAppsList;

    invoke-virtual {v0}, Lcom/miui/home/launcher/allapps/AlphabeticalAppsList;->getFastScrollerSections()Ljava/util/List;

    move-result-object v0

    iget-object v1, p3, Lcom/miui/home/launcher/allapps/BaseAlphabeticalAppsList$FastScrollSectionInfo;->fastScrollToItem:Lcom/miui/home/launcher/allapps/BaseAlphabeticalAppsList$AdapterItem;

    iget v1, v1, Lcom/miui/home/launcher/allapps/BaseAlphabeticalAppsList$AdapterItem;->position:I

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_2

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p3, :cond_2

    move p2, v2

    goto :goto_2

    :cond_2
    iget-object p3, p0, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;->mRv:Lcom/miui/home/launcher/allapps/BaseAllAppsRecyclerView;

    invoke-virtual {p3}, Lcom/miui/home/launcher/allapps/BaseAllAppsRecyclerView;->getVerticalFadingEdgeLength()I

    move-result v0

    invoke-virtual {p3, v1, v0}, Lcom/miui/home/launcher/allapps/BaseAllAppsRecyclerView;->getCurrentScrollY(II)I

    move-result p3

    invoke-static {p2, p3}, Ljava/lang/Math;->min(II)I

    move-result p2

    :goto_2
    iget-object p3, p0, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;->mFastScrollFrames:[I

    array-length p3, p3

    sub-int/2addr p2, p1

    int-to-float p1, p2

    invoke-static {p1}, Ljava/lang/Math;->signum(F)F

    move-result p1

    float-to-double v0, p1

    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result v3

    int-to-float v3, v3

    int-to-float v4, p3

    div-float/2addr v3, v4

    float-to-double v3, v3

    invoke-static {v3, v4}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v3

    mul-double/2addr v0, v3

    double-to-int v0, v0

    move v1, p2

    move p2, v2

    :goto_3
    if-ge p2, p3, :cond_3

    iget-object v3, p0, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;->mFastScrollFrames:[I

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v4

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, p1

    float-to-int v4, v4

    aput v4, v3, p2

    sub-int/2addr v1, v0

    add-int/lit8 p2, p2, 0x1

    goto :goto_3

    :cond_3
    iput v2, p0, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;->mFastScrollFrameIndex:I

    iget-object p1, p0, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;->mRv:Lcom/miui/home/launcher/allapps/BaseAllAppsRecyclerView;

    iget-object p2, p0, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;->mSmoothSnapNextFrameRunnable:Ljava/lang/Runnable;

    invoke-virtual {p1, p2}, Lcom/miui/home/launcher/allapps/BaseAllAppsRecyclerView;->postOnAnimation(Ljava/lang/Runnable;)V

    return-void
.end method

.method private updateTrackedViewsFastScrollFocusState()V
    .locals 2

    iget-object v0, p0, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;->mTrackedFastScrollViews:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    invoke-direct {p0, v1}, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;->updateTrackedViewsFastScrollFocusState(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private updateTrackedViewsFastScrollFocusState(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V
    .locals 3

    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->getAdapterPosition()I

    move-result v0

    iget-object v1, p0, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;->mCurrentFastScrollSection:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    const/4 v1, -0x1

    if-le v0, v1, :cond_0

    iget-object v1, p0, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;->mApps:Lcom/miui/home/launcher/allapps/AlphabeticalAppsList;

    invoke-virtual {v1}, Lcom/miui/home/launcher/allapps/AlphabeticalAppsList;->getAdapterItems()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;->mApps:Lcom/miui/home/launcher/allapps/AlphabeticalAppsList;

    invoke-virtual {v1}, Lcom/miui/home/launcher/allapps/AlphabeticalAppsList;->getAdapterItems()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/home/launcher/allapps/BaseAlphabeticalAppsList$AdapterItem;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;->mCurrentFastScrollSection:Ljava/lang/String;

    iget-object v0, v0, Lcom/miui/home/launcher/allapps/BaseAlphabeticalAppsList$AdapterItem;->sectionName:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    move v2, v0

    :cond_0
    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {p1, v2}, Landroid/view/View;->setEnabled(Z)V

    return-void
.end method


# virtual methods
.method public onBindView(Lcom/miui/home/launcher/allapps/AllAppsGridAdapter$ViewHolder;)V
    .locals 1

    iget-object v0, p0, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;->mTrackedFastScrollViews:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;->mCurrentFastScrollSection:Ljava/lang/String;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;->mTargetFastScrollSection:Ljava/lang/String;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-direct {p0, p1}, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;->resetTrackedViewsFastScrollFocusState(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V

    goto :goto_1

    :cond_1
    :goto_0
    invoke-direct {p0, p1}, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;->updateTrackedViewsFastScrollFocusState(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V

    :goto_1
    return-void
.end method

.method onFastScrollCompleted()V
    .locals 2

    goto/32 :goto_a

    nop

    :goto_0
    const/4 v0, 0x0

    goto/32 :goto_5

    nop

    :goto_1
    iput-boolean v0, p0, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;->mHasFastScrollTouchSettled:Z

    goto/32 :goto_7

    nop

    :goto_2
    const/4 v0, -0x1

    goto/32 :goto_d

    nop

    :goto_3
    iget-object v1, p0, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;->mFastScrollToTargetSectionRunnable:Ljava/lang/Runnable;

    goto/32 :goto_4

    nop

    :goto_4
    invoke-virtual {v0, v1}, Lcom/miui/home/launcher/allapps/BaseAllAppsRecyclerView;->removeCallbacks(Ljava/lang/Runnable;)Z

    goto/32 :goto_c

    nop

    :goto_5
    iput-object v0, p0, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;->mCurrentFastScrollSection:Ljava/lang/String;

    goto/32 :goto_9

    nop

    :goto_6
    iget-object v1, p0, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;->mSmoothSnapNextFrameRunnable:Ljava/lang/Runnable;

    goto/32 :goto_e

    nop

    :goto_7
    iput-boolean v0, p0, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;->mHasFastScrollTouchSettledAtLeastOnce:Z

    goto/32 :goto_0

    nop

    :goto_8
    invoke-direct {p0}, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;->resetTrackedViewsFastScrollFocusState()V

    goto/32 :goto_f

    nop

    :goto_9
    iput-object v0, p0, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;->mTargetFastScrollSection:Ljava/lang/String;

    goto/32 :goto_2

    nop

    :goto_a
    iget-object v0, p0, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;->mRv:Lcom/miui/home/launcher/allapps/BaseAllAppsRecyclerView;

    goto/32 :goto_6

    nop

    :goto_b
    iget-object v0, p0, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;->mRv:Lcom/miui/home/launcher/allapps/BaseAllAppsRecyclerView;

    goto/32 :goto_3

    nop

    :goto_c
    const/4 v0, 0x0

    goto/32 :goto_1

    nop

    :goto_d
    iput v0, p0, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;->mTargetFastScrollPosition:I

    goto/32 :goto_8

    nop

    :goto_e
    invoke-virtual {v0, v1}, Lcom/miui/home/launcher/allapps/BaseAllAppsRecyclerView;->removeCallbacks(Ljava/lang/Runnable;)Z

    goto/32 :goto_b

    nop

    :goto_f
    return-void
.end method

.method onSetAdapter(Lcom/miui/home/launcher/allapps/AllAppsGridAdapter;)V
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    invoke-virtual {p1, p0}, Lcom/miui/home/launcher/allapps/AllAppsGridAdapter;->setBindViewCallback(Lcom/miui/home/launcher/allapps/AllAppsGridAdapter$BindViewCallback;)V

    goto/32 :goto_1

    nop

    :goto_1
    return-void
.end method

.method public onViewRecycle(Lcom/miui/home/launcher/allapps/AllAppsGridAdapter$ViewHolder;)V
    .locals 1

    iget-object v0, p0, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;->mTrackedFastScrollViews:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    iget-object p1, p1, Lcom/miui/home/launcher/allapps/AllAppsGridAdapter$ViewHolder;->itemView:Landroid/view/View;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setEnabled(Z)V

    return-void
.end method

.method setAllTrackedViewsFastScrollFocusState(Z)V
    .locals 2

    goto/32 :goto_6

    nop

    :goto_0
    check-cast v1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    goto/32 :goto_4

    nop

    :goto_1
    goto :goto_8

    :goto_2
    goto/32 :goto_9

    nop

    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_0

    nop

    :goto_4
    iget-object v1, v1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_5

    nop

    :goto_5
    invoke-virtual {v1, p1}, Landroid/view/View;->setEnabled(Z)V

    goto/32 :goto_1

    nop

    :goto_6
    iget-object v0, p0, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;->mTrackedFastScrollViews:Ljava/util/HashSet;

    goto/32 :goto_7

    nop

    :goto_7
    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_8
    goto/32 :goto_a

    nop

    :goto_9
    return-void

    :goto_a
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_b

    nop

    :goto_b
    if-nez v1, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_3

    nop
.end method

.method smoothScrollToSection(IILcom/miui/home/launcher/allapps/BaseAlphabeticalAppsList$FastScrollSectionInfo;)Z
    .locals 2

    goto/32 :goto_2

    nop

    :goto_0
    iget v0, v0, Lcom/miui/home/launcher/allapps/BaseAlphabeticalAppsList$AdapterItem;->position:I

    goto/32 :goto_b

    nop

    :goto_1
    iget v1, v1, Lcom/miui/home/launcher/allapps/BaseAlphabeticalAppsList$AdapterItem;->position:I

    goto/32 :goto_5

    nop

    :goto_2
    iget v0, p0, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;->mTargetFastScrollPosition:I

    goto/32 :goto_7

    nop

    :goto_3
    invoke-direct {p0, p1, p2, p3}, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;->smoothSnapToPosition(IILcom/miui/home/launcher/allapps/BaseAlphabeticalAppsList$FastScrollSectionInfo;)V

    goto/32 :goto_4

    nop

    :goto_4
    const/4 p1, 0x1

    goto/32 :goto_8

    nop

    :goto_5
    if-ne v0, v1, :cond_0

    goto/32 :goto_9

    :cond_0
    goto/32 :goto_a

    nop

    :goto_6
    return p1

    :goto_7
    iget-object v1, p3, Lcom/miui/home/launcher/allapps/BaseAlphabeticalAppsList$FastScrollSectionInfo;->fastScrollToItem:Lcom/miui/home/launcher/allapps/BaseAlphabeticalAppsList$AdapterItem;

    goto/32 :goto_1

    nop

    :goto_8
    return p1

    :goto_9
    goto/32 :goto_c

    nop

    :goto_a
    iget-object v0, p3, Lcom/miui/home/launcher/allapps/BaseAlphabeticalAppsList$FastScrollSectionInfo;->fastScrollToItem:Lcom/miui/home/launcher/allapps/BaseAlphabeticalAppsList$AdapterItem;

    goto/32 :goto_0

    nop

    :goto_b
    iput v0, p0, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;->mTargetFastScrollPosition:I

    goto/32 :goto_3

    nop

    :goto_c
    invoke-direct {p0}, Lcom/miui/home/launcher/allapps/AllAppsFastScrollHelper;->updateTrackedViewsFastScrollFocusState()V

    goto/32 :goto_d

    nop

    :goto_d
    const/4 p1, 0x0

    goto/32 :goto_6

    nop
.end method
