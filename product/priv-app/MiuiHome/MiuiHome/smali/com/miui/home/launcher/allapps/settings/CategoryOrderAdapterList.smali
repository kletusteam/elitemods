.class Lcom/miui/home/launcher/allapps/settings/CategoryOrderAdapterList;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/miui/home/launcher/allapps/AllAppsStore$OnUpdateListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/home/launcher/allapps/settings/CategoryOrderAdapterList$AdapterItem;
    }
.end annotation


# instance fields
.field private mAdapter:Lcom/miui/home/launcher/allapps/settings/CategoryOrderAdapter;

.field private final mAdapterItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/miui/home/launcher/allapps/settings/CategoryOrderAdapterList$AdapterItem;",
            ">;"
        }
    .end annotation
.end field

.field private final mAllAppsStore:Lcom/miui/home/launcher/allapps/AllAppsStore;

.field private final mCategories:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/miui/home/launcher/allapps/category/CategoryInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mCategoryComparator:Lcom/miui/home/launcher/allapps/category/CategoryInfoComparator;

.field private final mOrderedCategories:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/miui/home/launcher/allapps/category/CategoryInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/miui/home/launcher/allapps/AllAppsStore;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/home/launcher/allapps/settings/CategoryOrderAdapterList;->mCategories:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/home/launcher/allapps/settings/CategoryOrderAdapterList;->mOrderedCategories:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/home/launcher/allapps/settings/CategoryOrderAdapterList;->mAdapterItems:Ljava/util/ArrayList;

    new-instance v0, Lcom/miui/home/launcher/allapps/category/CategoryInfoComparator;

    invoke-direct {v0}, Lcom/miui/home/launcher/allapps/category/CategoryInfoComparator;-><init>()V

    iput-object v0, p0, Lcom/miui/home/launcher/allapps/settings/CategoryOrderAdapterList;->mCategoryComparator:Lcom/miui/home/launcher/allapps/category/CategoryInfoComparator;

    iput-object p1, p0, Lcom/miui/home/launcher/allapps/settings/CategoryOrderAdapterList;->mAllAppsStore:Lcom/miui/home/launcher/allapps/AllAppsStore;

    iget-object p1, p0, Lcom/miui/home/launcher/allapps/settings/CategoryOrderAdapterList;->mAllAppsStore:Lcom/miui/home/launcher/allapps/AllAppsStore;

    invoke-virtual {p1, p0}, Lcom/miui/home/launcher/allapps/AllAppsStore;->addUpdateListener(Lcom/miui/home/launcher/allapps/AllAppsStore$OnUpdateListener;)V

    return-void
.end method

.method private refillAdapterItems()V
    .locals 3

    iget-object v0, p0, Lcom/miui/home/launcher/allapps/settings/CategoryOrderAdapterList;->mAdapterItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/miui/home/launcher/allapps/settings/CategoryOrderAdapterList;->mOrderedCategories:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/home/launcher/allapps/category/CategoryInfo;

    iget-object v2, p0, Lcom/miui/home/launcher/allapps/settings/CategoryOrderAdapterList;->mAdapterItems:Ljava/util/ArrayList;

    invoke-static {v1}, Lcom/miui/home/launcher/allapps/settings/CategoryOrderAdapterList$AdapterItem;->asCategory(Lcom/miui/home/launcher/allapps/category/CategoryInfo;)Lcom/miui/home/launcher/allapps/settings/CategoryOrderAdapterList$AdapterItem;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/miui/home/launcher/allapps/settings/CategoryOrderAdapterList;->mAdapterItems:Ljava/util/ArrayList;

    invoke-static {}, Lcom/miui/home/launcher/allapps/settings/CategoryOrderAdapterList$AdapterItem;->asAddCategory()Lcom/miui/home/launcher/allapps/settings/CategoryOrderAdapterList$AdapterItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private refreshRecyclerView()V
    .locals 3

    iget-object v0, p0, Lcom/miui/home/launcher/allapps/settings/CategoryOrderAdapterList;->mAdapter:Lcom/miui/home/launcher/allapps/settings/CategoryOrderAdapter;

    if-eqz v0, :cond_0

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/miui/home/launcher/allapps/settings/CategoryOrderAdapterList;->mAdapterItems:Ljava/util/ArrayList;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0, v1}, Lcom/miui/home/launcher/allapps/settings/CategoryOrderAdapter;->submitList(Ljava/util/List;)V

    :cond_0
    return-void
.end method

.method private updateAdapterItems()V
    .locals 0

    invoke-direct {p0}, Lcom/miui/home/launcher/allapps/settings/CategoryOrderAdapterList;->refillAdapterItems()V

    invoke-direct {p0}, Lcom/miui/home/launcher/allapps/settings/CategoryOrderAdapterList;->refreshRecyclerView()V

    return-void
.end method


# virtual methods
.method getDifferConfig()Landroidx/recyclerview/widget/AsyncDifferConfig;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroidx/recyclerview/widget/AsyncDifferConfig<",
            "Lcom/miui/home/launcher/allapps/settings/CategoryOrderAdapterList$AdapterItem;",
            ">;"
        }
    .end annotation

    goto/32 :goto_9

    nop

    :goto_0
    invoke-virtual {v1, v0}, Landroidx/recyclerview/widget/AsyncDifferConfig$Builder;->setMainThreadExecutor(Ljava/util/concurrent/Executor;)Landroidx/recyclerview/widget/AsyncDifferConfig$Builder;

    move-result-object v1

    goto/32 :goto_8

    nop

    :goto_1
    invoke-virtual {v0}, Landroidx/recyclerview/widget/AsyncDifferConfig$Builder;->build()Landroidx/recyclerview/widget/AsyncDifferConfig;

    move-result-object v0

    goto/32 :goto_6

    nop

    :goto_2
    invoke-direct {v0}, Lcom/miui/home/launcher/MainThreadExecutor;-><init>()V

    goto/32 :goto_4

    nop

    :goto_3
    invoke-direct {v2, p0}, Lcom/miui/home/launcher/allapps/settings/CategoryOrderAdapterList$1;-><init>(Lcom/miui/home/launcher/allapps/settings/CategoryOrderAdapterList;)V

    goto/32 :goto_7

    nop

    :goto_4
    new-instance v1, Landroidx/recyclerview/widget/AsyncDifferConfig$Builder;

    goto/32 :goto_5

    nop

    :goto_5
    new-instance v2, Lcom/miui/home/launcher/allapps/settings/CategoryOrderAdapterList$1;

    goto/32 :goto_3

    nop

    :goto_6
    return-object v0

    :goto_7
    invoke-direct {v1, v2}, Landroidx/recyclerview/widget/AsyncDifferConfig$Builder;-><init>(Landroidx/recyclerview/widget/DiffUtil$ItemCallback;)V

    goto/32 :goto_0

    nop

    :goto_8
    invoke-virtual {v1, v0}, Landroidx/recyclerview/widget/AsyncDifferConfig$Builder;->setBackgroundThreadExecutor(Ljava/util/concurrent/Executor;)Landroidx/recyclerview/widget/AsyncDifferConfig$Builder;

    move-result-object v0

    goto/32 :goto_1

    nop

    :goto_9
    new-instance v0, Lcom/miui/home/launcher/MainThreadExecutor;

    goto/32 :goto_2

    nop
.end method

.method getOrderedCategoryList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/miui/home/launcher/allapps/category/CategoryInfo;",
            ">;"
        }
    .end annotation

    goto/32 :goto_2

    nop

    :goto_0
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    goto/32 :goto_3

    nop

    :goto_1
    iget-object v1, p0, Lcom/miui/home/launcher/allapps/settings/CategoryOrderAdapterList;->mOrderedCategories:Ljava/util/List;

    goto/32 :goto_0

    nop

    :goto_2
    new-instance v0, Ljava/util/ArrayList;

    goto/32 :goto_1

    nop

    :goto_3
    return-object v0
.end method

.method isCategoryOrderChanged()Z
    .locals 2

    goto/32 :goto_1

    nop

    :goto_0
    invoke-interface {v0, v1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto/32 :goto_4

    nop

    :goto_1
    iget-object v0, p0, Lcom/miui/home/launcher/allapps/settings/CategoryOrderAdapterList;->mCategories:Ljava/util/List;

    goto/32 :goto_2

    nop

    :goto_2
    iget-object v1, p0, Lcom/miui/home/launcher/allapps/settings/CategoryOrderAdapterList;->mOrderedCategories:Ljava/util/List;

    goto/32 :goto_0

    nop

    :goto_3
    return v0

    :goto_4
    xor-int/lit8 v0, v0, 0x1

    goto/32 :goto_3

    nop
.end method

.method public onAppsUpdated(ILjava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "Lcom/miui/home/launcher/AppInfo;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/miui/home/launcher/allapps/settings/CategoryOrderAdapterList;->updateCategoryList()V

    return-void
.end method

.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/miui/home/launcher/allapps/settings/CategoryOrderAdapterList;->mAllAppsStore:Lcom/miui/home/launcher/allapps/AllAppsStore;

    invoke-virtual {v0, p0}, Lcom/miui/home/launcher/allapps/AllAppsStore;->removeUpdateListener(Lcom/miui/home/launcher/allapps/AllAppsStore$OnUpdateListener;)V

    return-void
.end method

.method setAdapter(Lcom/miui/home/launcher/allapps/settings/CategoryOrderAdapter;)V
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    iput-object p1, p0, Lcom/miui/home/launcher/allapps/settings/CategoryOrderAdapterList;->mAdapter:Lcom/miui/home/launcher/allapps/settings/CategoryOrderAdapter;

    goto/32 :goto_1

    nop

    :goto_1
    return-void
.end method

.method swap(II)V
    .locals 1

    goto/32 :goto_5

    nop

    :goto_0
    invoke-static {v0, p1, p2}, Ljava/util/Collections;->swap(Ljava/util/List;II)V

    goto/32 :goto_2

    nop

    :goto_1
    if-nez v0, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_6

    nop

    :goto_2
    invoke-direct {p0}, Lcom/miui/home/launcher/allapps/settings/CategoryOrderAdapterList;->refillAdapterItems()V

    goto/32 :goto_3

    nop

    :goto_3
    iget-object v0, p0, Lcom/miui/home/launcher/allapps/settings/CategoryOrderAdapterList;->mAdapter:Lcom/miui/home/launcher/allapps/settings/CategoryOrderAdapter;

    goto/32 :goto_1

    nop

    :goto_4
    return-void

    :goto_5
    iget-object v0, p0, Lcom/miui/home/launcher/allapps/settings/CategoryOrderAdapterList;->mOrderedCategories:Ljava/util/List;

    goto/32 :goto_0

    nop

    :goto_6
    invoke-virtual {v0, p1, p2}, Lcom/miui/home/launcher/allapps/settings/CategoryOrderAdapter;->notifyItemMoved(II)V

    :goto_7
    goto/32 :goto_4

    nop
.end method

.method updateCategoryList()V
    .locals 2

    goto/32 :goto_a

    nop

    :goto_0
    iget-object v1, p0, Lcom/miui/home/launcher/allapps/settings/CategoryOrderAdapterList;->mCategories:Ljava/util/List;

    goto/32 :goto_b

    nop

    :goto_1
    invoke-interface {v0}, Ljava/util/List;->clear()V

    goto/32 :goto_2

    nop

    :goto_2
    iget-object v0, p0, Lcom/miui/home/launcher/allapps/settings/CategoryOrderAdapterList;->mCategories:Ljava/util/List;

    goto/32 :goto_3

    nop

    :goto_3
    iget-object v1, p0, Lcom/miui/home/launcher/allapps/settings/CategoryOrderAdapterList;->mAllAppsStore:Lcom/miui/home/launcher/allapps/AllAppsStore;

    goto/32 :goto_8

    nop

    :goto_4
    invoke-interface {v0}, Ljava/util/List;->clear()V

    goto/32 :goto_5

    nop

    :goto_5
    iget-object v0, p0, Lcom/miui/home/launcher/allapps/settings/CategoryOrderAdapterList;->mOrderedCategories:Ljava/util/List;

    goto/32 :goto_0

    nop

    :goto_6
    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/32 :goto_7

    nop

    :goto_7
    iget-object v0, p0, Lcom/miui/home/launcher/allapps/settings/CategoryOrderAdapterList;->mCategories:Ljava/util/List;

    goto/32 :goto_f

    nop

    :goto_8
    invoke-virtual {v1}, Lcom/miui/home/launcher/allapps/AllAppsStore;->getAppCategories()Ljava/util/Collection;

    move-result-object v1

    goto/32 :goto_6

    nop

    :goto_9
    return-void

    :goto_a
    iget-object v0, p0, Lcom/miui/home/launcher/allapps/settings/CategoryOrderAdapterList;->mCategories:Ljava/util/List;

    goto/32 :goto_1

    nop

    :goto_b
    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/32 :goto_d

    nop

    :goto_c
    iget-object v0, p0, Lcom/miui/home/launcher/allapps/settings/CategoryOrderAdapterList;->mOrderedCategories:Ljava/util/List;

    goto/32 :goto_4

    nop

    :goto_d
    invoke-direct {p0}, Lcom/miui/home/launcher/allapps/settings/CategoryOrderAdapterList;->updateAdapterItems()V

    goto/32 :goto_9

    nop

    :goto_e
    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto/32 :goto_c

    nop

    :goto_f
    iget-object v1, p0, Lcom/miui/home/launcher/allapps/settings/CategoryOrderAdapterList;->mCategoryComparator:Lcom/miui/home/launcher/allapps/category/CategoryInfoComparator;

    goto/32 :goto_e

    nop
.end method
