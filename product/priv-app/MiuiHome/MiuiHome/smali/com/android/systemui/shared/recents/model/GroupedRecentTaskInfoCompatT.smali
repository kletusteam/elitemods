.class public Lcom/android/systemui/shared/recents/model/GroupedRecentTaskInfoCompatT;
.super Lcom/android/systemui/shared/recents/model/GroupedRecentTaskInfoCompat;


# direct methods
.method public constructor <init>(Lcom/android/wm/shell/util/GroupedRecentTaskInfo;)V
    .locals 2

    invoke-direct {p0}, Lcom/android/systemui/shared/recents/model/GroupedRecentTaskInfoCompat;-><init>()V

    iget-object v0, p1, Lcom/android/wm/shell/util/GroupedRecentTaskInfo;->mTaskInfo1:Landroid/app/ActivityManager$RecentTaskInfo;

    iput-object v0, p0, Lcom/android/systemui/shared/recents/model/GroupedRecentTaskInfoCompatT;->mMainTaskInfo:Landroid/app/ActivityManager$RecentTaskInfo;

    iget-object v0, p1, Lcom/android/wm/shell/util/GroupedRecentTaskInfo;->mStagedSplitBounds:Lcom/android/wm/shell/util/StagedSplitBounds;

    iput-object v0, p0, Lcom/android/systemui/shared/recents/model/GroupedRecentTaskInfoCompatT;->mSplitBoundsConfig:Lcom/android/wm/shell/util/StagedSplitBounds;

    iget-object v0, p1, Lcom/android/wm/shell/util/GroupedRecentTaskInfo;->mTaskInfo2:Landroid/app/ActivityManager$RecentTaskInfo;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/android/wm/shell/util/GroupedRecentTaskInfo;->mStagedSplitBounds:Lcom/android/wm/shell/util/StagedSplitBounds;

    iget v0, v0, Lcom/android/wm/shell/util/StagedSplitBounds;->leftTopTaskId:I

    iget-object v1, p1, Lcom/android/wm/shell/util/GroupedRecentTaskInfo;->mTaskInfo1:Landroid/app/ActivityManager$RecentTaskInfo;

    iget v1, v1, Landroid/app/ActivityManager$RecentTaskInfo;->taskId:I

    if-ne v0, v1, :cond_0

    iget-object v0, p1, Lcom/android/wm/shell/util/GroupedRecentTaskInfo;->mTaskInfo1:Landroid/app/ActivityManager$RecentTaskInfo;

    iput-object v0, p0, Lcom/android/systemui/shared/recents/model/GroupedRecentTaskInfoCompatT;->mPrimaryTaskInfo:Landroid/app/ActivityManager$RecentTaskInfo;

    iget-object v0, p1, Lcom/android/wm/shell/util/GroupedRecentTaskInfo;->mTaskInfo2:Landroid/app/ActivityManager$RecentTaskInfo;

    iput-object v0, p0, Lcom/android/systemui/shared/recents/model/GroupedRecentTaskInfoCompatT;->mSecondTaskInfo:Landroid/app/ActivityManager$RecentTaskInfo;

    goto :goto_0

    :cond_0
    iget-object v0, p1, Lcom/android/wm/shell/util/GroupedRecentTaskInfo;->mTaskInfo2:Landroid/app/ActivityManager$RecentTaskInfo;

    iput-object v0, p0, Lcom/android/systemui/shared/recents/model/GroupedRecentTaskInfoCompatT;->mPrimaryTaskInfo:Landroid/app/ActivityManager$RecentTaskInfo;

    iget-object v0, p1, Lcom/android/wm/shell/util/GroupedRecentTaskInfo;->mTaskInfo1:Landroid/app/ActivityManager$RecentTaskInfo;

    iput-object v0, p0, Lcom/android/systemui/shared/recents/model/GroupedRecentTaskInfoCompatT;->mSecondTaskInfo:Landroid/app/ActivityManager$RecentTaskInfo;

    :goto_0
    iget-object v0, p1, Lcom/android/wm/shell/util/GroupedRecentTaskInfo;->mStagedSplitBounds:Lcom/android/wm/shell/util/StagedSplitBounds;

    iget-object v0, v0, Lcom/android/wm/shell/util/StagedSplitBounds;->leftTopBounds:Landroid/graphics/Rect;

    iput-object v0, p0, Lcom/android/systemui/shared/recents/model/GroupedRecentTaskInfoCompatT;->mPrimaryBound:Landroid/graphics/Rect;

    iget-object p1, p1, Lcom/android/wm/shell/util/GroupedRecentTaskInfo;->mStagedSplitBounds:Lcom/android/wm/shell/util/StagedSplitBounds;

    iget-object p1, p1, Lcom/android/wm/shell/util/StagedSplitBounds;->rightBottomBounds:Landroid/graphics/Rect;

    iput-object p1, p0, Lcom/android/systemui/shared/recents/model/GroupedRecentTaskInfoCompatT;->mSecondBound:Landroid/graphics/Rect;

    :cond_1
    return-void
.end method
