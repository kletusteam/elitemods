.class public abstract Lcom/android/systemui/shared/recents/model/GroupedRecentTaskInfoCompat;
.super Ljava/lang/Object;


# instance fields
.field public mMainTaskInfo:Landroid/app/ActivityManager$RecentTaskInfo;

.field public mPrimaryBound:Landroid/graphics/Rect;

.field public mPrimaryTaskInfo:Landroid/app/ActivityManager$RecentTaskInfo;

.field public mSecondBound:Landroid/graphics/Rect;

.field public mSecondTaskInfo:Landroid/app/ActivityManager$RecentTaskInfo;

.field public mSplitBoundsConfig:Lcom/android/wm/shell/util/StagedSplitBounds;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getMainPackageName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/shared/recents/model/GroupedRecentTaskInfoCompat;->mMainTaskInfo:Landroid/app/ActivityManager$RecentTaskInfo;

    if-eqz v0, :cond_0

    iget-object v0, v0, Landroid/app/ActivityManager$RecentTaskInfo;->baseIntent:Landroid/content/Intent;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/shared/recents/model/GroupedRecentTaskInfoCompat;->mMainTaskInfo:Landroid/app/ActivityManager$RecentTaskInfo;

    iget-object v0, v0, Landroid/app/ActivityManager$RecentTaskInfo;->baseIntent:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/shared/recents/model/GroupedRecentTaskInfoCompat;->mMainTaskInfo:Landroid/app/ActivityManager$RecentTaskInfo;

    iget-object v0, v0, Landroid/app/ActivityManager$RecentTaskInfo;->baseIntent:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMainTaskId()I
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/shared/recents/model/GroupedRecentTaskInfoCompat;->mMainTaskInfo:Landroid/app/ActivityManager$RecentTaskInfo;

    if-eqz v0, :cond_0

    iget v0, v0, Landroid/app/ActivityManager$RecentTaskInfo;->taskId:I

    goto :goto_0

    :cond_0
    const/high16 v0, -0x80000000

    :goto_0
    return v0
.end method

.method public getMainTaskInfo()Landroid/app/ActivityManager$RecentTaskInfo;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/shared/recents/model/GroupedRecentTaskInfoCompat;->mMainTaskInfo:Landroid/app/ActivityManager$RecentTaskInfo;

    return-object v0
.end method

.method public getMainUserId()I
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/shared/recents/model/GroupedRecentTaskInfoCompat;->mMainTaskInfo:Landroid/app/ActivityManager$RecentTaskInfo;

    if-eqz v0, :cond_0

    iget v0, v0, Landroid/app/ActivityManager$RecentTaskInfo;->userId:I

    goto :goto_0

    :cond_0
    const/high16 v0, -0x80000000

    :goto_0
    return v0
.end method

.method public getPrimaryBound()Landroid/graphics/Rect;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/shared/recents/model/GroupedRecentTaskInfoCompat;->mPrimaryBound:Landroid/graphics/Rect;

    return-object v0
.end method

.method public getPrimaryPackageName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/shared/recents/model/GroupedRecentTaskInfoCompat;->mPrimaryTaskInfo:Landroid/app/ActivityManager$RecentTaskInfo;

    if-eqz v0, :cond_0

    iget-object v0, v0, Landroid/app/ActivityManager$RecentTaskInfo;->baseIntent:Landroid/content/Intent;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/shared/recents/model/GroupedRecentTaskInfoCompat;->mPrimaryTaskInfo:Landroid/app/ActivityManager$RecentTaskInfo;

    iget-object v0, v0, Landroid/app/ActivityManager$RecentTaskInfo;->baseIntent:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/shared/recents/model/GroupedRecentTaskInfoCompat;->mPrimaryTaskInfo:Landroid/app/ActivityManager$RecentTaskInfo;

    iget-object v0, v0, Landroid/app/ActivityManager$RecentTaskInfo;->baseIntent:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getPrimaryTaskId()I
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/shared/recents/model/GroupedRecentTaskInfoCompat;->mPrimaryTaskInfo:Landroid/app/ActivityManager$RecentTaskInfo;

    if-eqz v0, :cond_0

    iget v0, v0, Landroid/app/ActivityManager$RecentTaskInfo;->taskId:I

    goto :goto_0

    :cond_0
    const/high16 v0, -0x80000000

    :goto_0
    return v0
.end method

.method public getPrimaryTaskInfo()Landroid/app/ActivityManager$RecentTaskInfo;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/shared/recents/model/GroupedRecentTaskInfoCompat;->mPrimaryTaskInfo:Landroid/app/ActivityManager$RecentTaskInfo;

    return-object v0
.end method

.method public getPrimaryUserId()I
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/shared/recents/model/GroupedRecentTaskInfoCompat;->mPrimaryTaskInfo:Landroid/app/ActivityManager$RecentTaskInfo;

    if-eqz v0, :cond_0

    iget v0, v0, Landroid/app/ActivityManager$RecentTaskInfo;->userId:I

    goto :goto_0

    :cond_0
    const/high16 v0, -0x80000000

    :goto_0
    return v0
.end method

.method public getSecondBound()Landroid/graphics/Rect;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/shared/recents/model/GroupedRecentTaskInfoCompat;->mSecondBound:Landroid/graphics/Rect;

    return-object v0
.end method

.method public getSecondPackageName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/shared/recents/model/GroupedRecentTaskInfoCompat;->mSecondTaskInfo:Landroid/app/ActivityManager$RecentTaskInfo;

    if-eqz v0, :cond_0

    iget-object v0, v0, Landroid/app/ActivityManager$RecentTaskInfo;->baseIntent:Landroid/content/Intent;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/shared/recents/model/GroupedRecentTaskInfoCompat;->mSecondTaskInfo:Landroid/app/ActivityManager$RecentTaskInfo;

    iget-object v0, v0, Landroid/app/ActivityManager$RecentTaskInfo;->baseIntent:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/systemui/shared/recents/model/GroupedRecentTaskInfoCompat;->mSecondTaskInfo:Landroid/app/ActivityManager$RecentTaskInfo;

    iget-object v0, v0, Landroid/app/ActivityManager$RecentTaskInfo;->baseIntent:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getSecondTaskId()I
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/shared/recents/model/GroupedRecentTaskInfoCompat;->mSecondTaskInfo:Landroid/app/ActivityManager$RecentTaskInfo;

    if-eqz v0, :cond_0

    iget v0, v0, Landroid/app/ActivityManager$RecentTaskInfo;->taskId:I

    goto :goto_0

    :cond_0
    const/high16 v0, -0x80000000

    :goto_0
    return v0
.end method

.method public getSecondTaskInfo()Landroid/app/ActivityManager$RecentTaskInfo;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/shared/recents/model/GroupedRecentTaskInfoCompat;->mSecondTaskInfo:Landroid/app/ActivityManager$RecentTaskInfo;

    return-object v0
.end method

.method public getSecondUserId()I
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/shared/recents/model/GroupedRecentTaskInfoCompat;->mSecondTaskInfo:Landroid/app/ActivityManager$RecentTaskInfo;

    if-eqz v0, :cond_0

    iget v0, v0, Landroid/app/ActivityManager$RecentTaskInfo;->userId:I

    goto :goto_0

    :cond_0
    const/high16 v0, -0x80000000

    :goto_0
    return v0
.end method

.method public getSplitBoundsConfig()Lcom/android/wm/shell/util/StagedSplitBounds;
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/shared/recents/model/GroupedRecentTaskInfoCompat;->mSplitBoundsConfig:Lcom/android/wm/shell/util/StagedSplitBounds;

    return-object v0
.end method

.method public hasMultipleTasks()Z
    .locals 1

    iget-object v0, p0, Lcom/android/systemui/shared/recents/model/GroupedRecentTaskInfoCompat;->mSecondTaskInfo:Landroid/app/ActivityManager$RecentTaskInfo;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isExcludedFromRecents()Z
    .locals 5

    iget-object v0, p0, Lcom/android/systemui/shared/recents/model/GroupedRecentTaskInfoCompat;->mMainTaskInfo:Landroid/app/ActivityManager$RecentTaskInfo;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/high16 v3, 0x800000

    if-eqz v0, :cond_1

    iget-object v0, v0, Landroid/app/ActivityManager$RecentTaskInfo;->baseIntent:Landroid/content/Intent;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/systemui/shared/recents/model/GroupedRecentTaskInfoCompat;->mMainTaskInfo:Landroid/app/ActivityManager$RecentTaskInfo;

    iget-object v0, v0, Landroid/app/ActivityManager$RecentTaskInfo;->baseIntent:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getFlags()I

    move-result v0

    and-int/2addr v0, v3

    if-ne v0, v3, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    move v0, v2

    :goto_0
    or-int/2addr v0, v2

    goto :goto_1

    :cond_1
    move v0, v2

    :goto_1
    iget-object v4, p0, Lcom/android/systemui/shared/recents/model/GroupedRecentTaskInfoCompat;->mPrimaryTaskInfo:Landroid/app/ActivityManager$RecentTaskInfo;

    if-eqz v4, :cond_3

    iget-object v4, v4, Landroid/app/ActivityManager$RecentTaskInfo;->baseIntent:Landroid/content/Intent;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/android/systemui/shared/recents/model/GroupedRecentTaskInfoCompat;->mPrimaryTaskInfo:Landroid/app/ActivityManager$RecentTaskInfo;

    iget-object v4, v4, Landroid/app/ActivityManager$RecentTaskInfo;->baseIntent:Landroid/content/Intent;

    invoke-virtual {v4}, Landroid/content/Intent;->getFlags()I

    move-result v4

    and-int/2addr v4, v3

    if-ne v4, v3, :cond_2

    move v4, v1

    goto :goto_2

    :cond_2
    move v4, v2

    :goto_2
    or-int/2addr v0, v4

    :cond_3
    iget-object v4, p0, Lcom/android/systemui/shared/recents/model/GroupedRecentTaskInfoCompat;->mSecondTaskInfo:Landroid/app/ActivityManager$RecentTaskInfo;

    if-eqz v4, :cond_5

    iget-object v4, v4, Landroid/app/ActivityManager$RecentTaskInfo;->baseIntent:Landroid/content/Intent;

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/android/systemui/shared/recents/model/GroupedRecentTaskInfoCompat;->mSecondTaskInfo:Landroid/app/ActivityManager$RecentTaskInfo;

    iget-object v4, v4, Landroid/app/ActivityManager$RecentTaskInfo;->baseIntent:Landroid/content/Intent;

    invoke-virtual {v4}, Landroid/content/Intent;->getFlags()I

    move-result v4

    and-int/2addr v4, v3

    if-ne v4, v3, :cond_4

    goto :goto_3

    :cond_4
    move v1, v2

    :goto_3
    or-int/2addr v0, v1

    :cond_5
    return v0
.end method
