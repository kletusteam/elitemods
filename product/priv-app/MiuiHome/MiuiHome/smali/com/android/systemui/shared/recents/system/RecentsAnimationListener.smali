.class public interface abstract Lcom/android/systemui/shared/recents/system/RecentsAnimationListener;
.super Ljava/lang/Object;


# virtual methods
.method public abstract onAnimationCanceled(Z)V
.end method

.method public abstract onAnimationStart(Lcom/android/systemui/shared/recents/system/RecentsAnimationControllerCompat;[Lcom/android/systemui/shared/recents/system/RemoteAnimationTargetCompat;Landroid/graphics/Rect;Landroid/graphics/Rect;)V
.end method

.method public abstract onTaskAppeared(Lcom/android/systemui/shared/recents/system/RemoteAnimationTargetCompat;)V
.end method

.method public abstract onTasksAppeared([Lcom/android/systemui/shared/recents/system/RemoteAnimationTargetCompat;)V
.end method
