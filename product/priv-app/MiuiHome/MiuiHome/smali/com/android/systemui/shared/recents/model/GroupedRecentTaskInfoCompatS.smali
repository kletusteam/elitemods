.class public Lcom/android/systemui/shared/recents/model/GroupedRecentTaskInfoCompatS;
.super Lcom/android/systemui/shared/recents/model/GroupedRecentTaskInfoCompat;


# direct methods
.method public constructor <init>(Landroid/app/ActivityManager$RecentTaskInfo;)V
    .locals 7

    invoke-direct {p0}, Lcom/android/systemui/shared/recents/model/GroupedRecentTaskInfoCompat;-><init>()V

    iput-object p1, p0, Lcom/android/systemui/shared/recents/model/GroupedRecentTaskInfoCompatS;->mMainTaskInfo:Landroid/app/ActivityManager$RecentTaskInfo;

    const-class v0, Landroid/app/ActivityManager$RecentTaskInfo;

    const-string v1, "childrenTaskInfos"

    const-class v2, Ljava/util/ArrayList;

    invoke-static {v0, p1, v1, v2}, Lcom/android/systemui/shared/recents/utilities/ReflectUtils;->getObjectField(Ljava/lang/Class;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/ArrayList;

    if-eqz p1, :cond_3

    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_3

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager$RecentTaskInfo;

    const/4 v2, 0x1

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/app/ActivityManager$RecentTaskInfo;

    invoke-direct {p0, v1}, Lcom/android/systemui/shared/recents/model/GroupedRecentTaskInfoCompatS;->getTaskBounds(Landroid/app/TaskInfo;)Landroid/graphics/Rect;

    move-result-object v3

    invoke-direct {p0, p1}, Lcom/android/systemui/shared/recents/model/GroupedRecentTaskInfoCompatS;->getTaskBounds(Landroid/app/TaskInfo;)Landroid/graphics/Rect;

    move-result-object v4

    iget v5, v3, Landroid/graphics/Rect;->left:I

    iget v6, v4, Landroid/graphics/Rect;->left:I

    if-gt v5, v6, :cond_0

    iget v5, v3, Landroid/graphics/Rect;->top:I

    iget v6, v4, Landroid/graphics/Rect;->top:I

    if-le v5, v6, :cond_1

    :cond_0
    move v0, v2

    :cond_1
    if-eqz v0, :cond_2

    iput-object p1, p0, Lcom/android/systemui/shared/recents/model/GroupedRecentTaskInfoCompatS;->mPrimaryTaskInfo:Landroid/app/ActivityManager$RecentTaskInfo;

    iput-object v1, p0, Lcom/android/systemui/shared/recents/model/GroupedRecentTaskInfoCompatS;->mSecondTaskInfo:Landroid/app/ActivityManager$RecentTaskInfo;

    iput-object v4, p0, Lcom/android/systemui/shared/recents/model/GroupedRecentTaskInfoCompatS;->mPrimaryBound:Landroid/graphics/Rect;

    iput-object v3, p0, Lcom/android/systemui/shared/recents/model/GroupedRecentTaskInfoCompatS;->mSecondBound:Landroid/graphics/Rect;

    goto :goto_0

    :cond_2
    iput-object v1, p0, Lcom/android/systemui/shared/recents/model/GroupedRecentTaskInfoCompatS;->mPrimaryTaskInfo:Landroid/app/ActivityManager$RecentTaskInfo;

    iput-object p1, p0, Lcom/android/systemui/shared/recents/model/GroupedRecentTaskInfoCompatS;->mSecondTaskInfo:Landroid/app/ActivityManager$RecentTaskInfo;

    iput-object v3, p0, Lcom/android/systemui/shared/recents/model/GroupedRecentTaskInfoCompatS;->mPrimaryBound:Landroid/graphics/Rect;

    iput-object v4, p0, Lcom/android/systemui/shared/recents/model/GroupedRecentTaskInfoCompatS;->mSecondBound:Landroid/graphics/Rect;

    :cond_3
    :goto_0
    return-void
.end method

.method private getTaskBounds(Landroid/app/TaskInfo;)Landroid/graphics/Rect;
    .locals 3

    const-class v0, Landroid/app/TaskInfo;

    const-string v1, "bounds"

    const-class v2, Landroid/graphics/Rect;

    invoke-static {v0, p1, v1, v2}, Lcom/android/systemui/shared/recents/utilities/ReflectUtils;->getObjectField(Ljava/lang/Class;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/graphics/Rect;

    return-object p1
.end method
