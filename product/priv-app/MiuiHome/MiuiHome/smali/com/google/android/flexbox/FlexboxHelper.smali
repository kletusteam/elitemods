.class Lcom/google/android/flexbox/FlexboxHelper;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/flexbox/FlexboxHelper$FlexLinesResult;,
        Lcom/google/android/flexbox/FlexboxHelper$Order;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private mChildrenFrozen:[Z

.field private final mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

.field mIndexToFlexLine:[I

.field mMeasureSpecCache:[J

.field private mMeasuredSizeCache:[J


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>(Lcom/google/android/flexbox/FlexContainer;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    return-void
.end method

.method private addFlexLine(Ljava/util/List;Lcom/google/android/flexbox/FlexLine;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/google/android/flexbox/FlexLine;",
            ">;",
            "Lcom/google/android/flexbox/FlexLine;",
            "II)V"
        }
    .end annotation

    iput p4, p2, Lcom/google/android/flexbox/FlexLine;->mSumCrossSizeBefore:I

    iget-object p4, p0, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    invoke-interface {p4, p2}, Lcom/google/android/flexbox/FlexContainer;->onNewFlexLineAdded(Lcom/google/android/flexbox/FlexLine;)V

    iput p3, p2, Lcom/google/android/flexbox/FlexLine;->mLastIndex:I

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private checkSizeConstraints(Landroid/view/View;I)V
    .locals 6

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/flexbox/FlexItem;

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->getMinWidth()I

    move-result v3

    const/4 v4, 0x1

    if-ge v1, v3, :cond_0

    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->getMinWidth()I

    move-result v1

    move v3, v4

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->getMaxWidth()I

    move-result v3

    if-le v1, v3, :cond_1

    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->getMaxWidth()I

    move-result v1

    move v3, v4

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    :goto_0
    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->getMinHeight()I

    move-result v5

    if-ge v2, v5, :cond_2

    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->getMinHeight()I

    move-result v2

    goto :goto_1

    :cond_2
    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->getMaxHeight()I

    move-result v5

    if-le v2, v5, :cond_3

    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->getMaxHeight()I

    move-result v2

    goto :goto_1

    :cond_3
    move v4, v3

    :goto_1
    if-eqz v4, :cond_4

    const/high16 v0, 0x40000000    # 2.0f

    invoke-static {v1, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-static {v2, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {p1, v1, v0}, Landroid/view/View;->measure(II)V

    invoke-direct {p0, p2, v1, v0, p1}, Lcom/google/android/flexbox/FlexboxHelper;->updateMeasureCache(IIILandroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    invoke-interface {v0, p2, p1}, Lcom/google/android/flexbox/FlexContainer;->updateViewCache(ILandroid/view/View;)V

    :cond_4
    return-void
.end method

.method private constructFlexLinesForAlignContentCenter(Ljava/util/List;II)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/google/android/flexbox/FlexLine;",
            ">;II)",
            "Ljava/util/List<",
            "Lcom/google/android/flexbox/FlexLine;",
            ">;"
        }
    .end annotation

    sub-int/2addr p2, p3

    div-int/lit8 p2, p2, 0x2

    new-instance p3, Ljava/util/ArrayList;

    invoke-direct {p3}, Ljava/util/ArrayList;-><init>()V

    new-instance v0, Lcom/google/android/flexbox/FlexLine;

    invoke-direct {v0}, Lcom/google/android/flexbox/FlexLine;-><init>()V

    iput p2, v0, Lcom/google/android/flexbox/FlexLine;->mCrossSize:I

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, p2, :cond_2

    if-nez v1, :cond_0

    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/flexbox/FlexLine;

    invoke-interface {p3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ne v1, v2, :cond_1

    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    return-object p3
.end method

.method private createOrders(I)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List<",
            "Lcom/google/android/flexbox/FlexboxHelper$Order;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v1, 0x0

    :goto_0
    if-ge v1, p1, :cond_0

    iget-object v2, p0, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    invoke-interface {v2, v1}, Lcom/google/android/flexbox/FlexContainer;->getFlexItemAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Lcom/google/android/flexbox/FlexItem;

    new-instance v3, Lcom/google/android/flexbox/FlexboxHelper$Order;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, Lcom/google/android/flexbox/FlexboxHelper$Order;-><init>(Lcom/google/android/flexbox/FlexboxHelper$1;)V

    invoke-interface {v2}, Lcom/google/android/flexbox/FlexItem;->getOrder()I

    move-result v2

    iput v2, v3, Lcom/google/android/flexbox/FlexboxHelper$Order;->order:I

    iput v1, v3, Lcom/google/android/flexbox/FlexboxHelper$Order;->index:I

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method private ensureChildrenFrozen(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxHelper;->mChildrenFrozen:[Z

    if-nez v0, :cond_1

    const/16 v0, 0xa

    if-ge p1, v0, :cond_0

    move p1, v0

    :cond_0
    new-array p1, p1, [Z

    iput-object p1, p0, Lcom/google/android/flexbox/FlexboxHelper;->mChildrenFrozen:[Z

    goto :goto_0

    :cond_1
    array-length v1, v0

    if-ge v1, p1, :cond_3

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x2

    if-lt v0, p1, :cond_2

    move p1, v0

    :cond_2
    new-array p1, p1, [Z

    iput-object p1, p0, Lcom/google/android/flexbox/FlexboxHelper;->mChildrenFrozen:[Z

    goto :goto_0

    :cond_3
    const/4 p1, 0x0

    invoke-static {v0, p1}, Ljava/util/Arrays;->fill([ZZ)V

    :goto_0
    return-void
.end method

.method private evaluateMinimumSizeForCompoundButton(Landroid/widget/CompoundButton;)V
    .locals 5

    invoke-virtual {p1}, Landroid/widget/CompoundButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/flexbox/FlexItem;

    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->getMinWidth()I

    move-result v1

    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->getMinHeight()I

    move-result v2

    invoke-static {p1}, Landroidx/core/widget/CompoundButtonCompat;->getButtonDrawable(Landroid/widget/CompoundButton;)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    const/4 v3, 0x0

    if-nez p1, :cond_0

    move v4, v3

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getMinimumWidth()I

    move-result v4

    :goto_0
    if-nez p1, :cond_1

    move p1, v3

    goto :goto_1

    :cond_1
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getMinimumHeight()I

    move-result p1

    :goto_1
    const/4 v3, -0x1

    if-ne v1, v3, :cond_2

    move v1, v4

    :cond_2
    invoke-interface {v0, v1}, Lcom/google/android/flexbox/FlexItem;->setMinWidth(I)V

    if-ne v2, v3, :cond_3

    goto :goto_2

    :cond_3
    move p1, v2

    :goto_2
    invoke-interface {v0, p1}, Lcom/google/android/flexbox/FlexItem;->setMinHeight(I)V

    return-void
.end method

.method private expandFlexItems(IILcom/google/android/flexbox/FlexLine;IIZ)V
    .locals 22

    move-object/from16 v7, p0

    move-object/from16 v3, p3

    move/from16 v4, p4

    iget v0, v3, Lcom/google/android/flexbox/FlexLine;->mTotalFlexGrow:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-lez v0, :cond_16

    iget v0, v3, Lcom/google/android/flexbox/FlexLine;->mMainSize:I

    if-ge v4, v0, :cond_0

    goto/16 :goto_9

    :cond_0
    iget v0, v3, Lcom/google/android/flexbox/FlexLine;->mMainSize:I

    iget v2, v3, Lcom/google/android/flexbox/FlexLine;->mMainSize:I

    sub-int v2, v4, v2

    int-to-float v2, v2

    iget v5, v3, Lcom/google/android/flexbox/FlexLine;->mTotalFlexGrow:F

    div-float/2addr v2, v5

    iget v5, v3, Lcom/google/android/flexbox/FlexLine;->mDividerLengthInMainSize:I

    add-int v5, p5, v5

    iput v5, v3, Lcom/google/android/flexbox/FlexLine;->mMainSize:I

    if-nez p6, :cond_1

    const/high16 v5, -0x80000000

    iput v5, v3, Lcom/google/android/flexbox/FlexLine;->mCrossSize:I

    :cond_1
    const/4 v5, 0x0

    move v8, v1

    move v6, v5

    move v9, v6

    :goto_0
    iget v10, v3, Lcom/google/android/flexbox/FlexLine;->mItemCount:I

    if-ge v5, v10, :cond_14

    iget v10, v3, Lcom/google/android/flexbox/FlexLine;->mFirstIndex:I

    add-int/2addr v10, v5

    iget-object v11, v7, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    invoke-interface {v11, v10}, Lcom/google/android/flexbox/FlexContainer;->getReorderedFlexItemAt(I)Landroid/view/View;

    move-result-object v11

    if-eqz v11, :cond_13

    invoke-virtual {v11}, Landroid/view/View;->getVisibility()I

    move-result v12

    const/16 v13, 0x8

    if-ne v12, v13, :cond_2

    move/from16 v13, p2

    move/from16 v21, v0

    goto/16 :goto_8

    :cond_2
    invoke-virtual {v11}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v12

    check-cast v12, Lcom/google/android/flexbox/FlexItem;

    iget-object v13, v7, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    invoke-interface {v13}, Lcom/google/android/flexbox/FlexContainer;->getFlexDirection()I

    move-result v13

    const-wide/high16 v17, 0x3ff0000000000000L    # 1.0

    const/4 v14, 0x1

    if-eqz v13, :cond_b

    if-ne v13, v14, :cond_3

    goto/16 :goto_3

    :cond_3
    invoke-virtual {v11}, Landroid/view/View;->getMeasuredHeight()I

    move-result v13

    iget-object v15, v7, Lcom/google/android/flexbox/FlexboxHelper;->mMeasuredSizeCache:[J

    if-eqz v15, :cond_4

    aget-wide v14, v15, v10

    invoke-virtual {v7, v14, v15}, Lcom/google/android/flexbox/FlexboxHelper;->extractHigherInt(J)I

    move-result v13

    :cond_4
    invoke-virtual {v11}, Landroid/view/View;->getMeasuredWidth()I

    move-result v14

    iget-object v15, v7, Lcom/google/android/flexbox/FlexboxHelper;->mMeasuredSizeCache:[J

    if-eqz v15, :cond_5

    aget-wide v14, v15, v10

    invoke-virtual {v7, v14, v15}, Lcom/google/android/flexbox/FlexboxHelper;->extractLowerInt(J)I

    move-result v14

    :cond_5
    iget-object v15, v7, Lcom/google/android/flexbox/FlexboxHelper;->mChildrenFrozen:[Z

    aget-boolean v15, v15, v10

    if-nez v15, :cond_a

    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->getFlexGrow()F

    move-result v15

    cmpl-float v15, v15, v1

    if-lez v15, :cond_a

    int-to-float v13, v13

    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->getFlexGrow()F

    move-result v14

    mul-float/2addr v14, v2

    add-float/2addr v13, v14

    iget v14, v3, Lcom/google/android/flexbox/FlexLine;->mItemCount:I

    const/4 v15, 0x1

    sub-int/2addr v14, v15

    if-ne v5, v14, :cond_6

    add-float/2addr v13, v8

    move v8, v1

    :cond_6
    invoke-static {v13}, Ljava/lang/Math;->round(F)I

    move-result v14

    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->getMaxHeight()I

    move-result v1

    if-le v14, v1, :cond_7

    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->getMaxHeight()I

    move-result v14

    iget-object v1, v7, Lcom/google/android/flexbox/FlexboxHelper;->mChildrenFrozen:[Z

    aput-boolean v15, v1, v10

    iget v1, v3, Lcom/google/android/flexbox/FlexLine;->mTotalFlexGrow:F

    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->getFlexGrow()F

    move-result v6

    sub-float/2addr v1, v6

    iput v1, v3, Lcom/google/android/flexbox/FlexLine;->mTotalFlexGrow:F

    move v15, v0

    const/4 v6, 0x1

    goto :goto_1

    :cond_7
    int-to-float v1, v14

    sub-float/2addr v13, v1

    add-float/2addr v8, v13

    move v15, v0

    float-to-double v0, v8

    cmpl-double v13, v0, v17

    if-lez v13, :cond_8

    add-int/lit8 v14, v14, 0x1

    sub-double v0, v0, v17

    double-to-float v0, v0

    move v8, v0

    goto :goto_1

    :cond_8
    const-wide/high16 v19, -0x4010000000000000L    # -1.0

    cmpg-double v13, v0, v19

    if-gez v13, :cond_9

    add-int/lit8 v14, v14, -0x1

    add-double v0, v0, v17

    double-to-float v0, v0

    move v8, v0

    :cond_9
    :goto_1
    iget v0, v3, Lcom/google/android/flexbox/FlexLine;->mSumCrossSizeBefore:I

    move/from16 v1, p1

    invoke-direct {v7, v1, v12, v0}, Lcom/google/android/flexbox/FlexboxHelper;->getChildWidthMeasureSpecInternal(ILcom/google/android/flexbox/FlexItem;I)I

    move-result v0

    const/high16 v13, 0x40000000    # 2.0f

    invoke-static {v14, v13}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v13

    invoke-virtual {v11, v0, v13}, Landroid/view/View;->measure(II)V

    invoke-virtual {v11}, Landroid/view/View;->getMeasuredWidth()I

    move-result v14

    invoke-virtual {v11}, Landroid/view/View;->getMeasuredHeight()I

    move-result v16

    invoke-direct {v7, v10, v0, v13, v11}, Lcom/google/android/flexbox/FlexboxHelper;->updateMeasureCache(IIILandroid/view/View;)V

    iget-object v0, v7, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    invoke-interface {v0, v10, v11}, Lcom/google/android/flexbox/FlexContainer;->updateViewCache(ILandroid/view/View;)V

    move/from16 v13, v16

    goto :goto_2

    :cond_a
    move/from16 v1, p1

    move v15, v0

    :goto_2
    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->getMarginLeft()I

    move-result v0

    add-int/2addr v14, v0

    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->getMarginRight()I

    move-result v0

    add-int/2addr v14, v0

    iget-object v0, v7, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    invoke-interface {v0, v11}, Lcom/google/android/flexbox/FlexContainer;->getDecorationLengthCrossAxis(Landroid/view/View;)I

    move-result v0

    add-int/2addr v14, v0

    invoke-static {v9, v14}, Ljava/lang/Math;->max(II)I

    move-result v0

    iget v9, v3, Lcom/google/android/flexbox/FlexLine;->mMainSize:I

    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->getMarginTop()I

    move-result v10

    add-int/2addr v13, v10

    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->getMarginBottom()I

    move-result v10

    add-int/2addr v13, v10

    add-int/2addr v9, v13

    iput v9, v3, Lcom/google/android/flexbox/FlexLine;->mMainSize:I

    move/from16 v13, p2

    move/from16 v21, v15

    goto/16 :goto_7

    :cond_b
    :goto_3
    move/from16 v1, p1

    move v15, v0

    invoke-virtual {v11}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    iget-object v13, v7, Lcom/google/android/flexbox/FlexboxHelper;->mMeasuredSizeCache:[J

    if-eqz v13, :cond_c

    aget-wide v0, v13, v10

    invoke-virtual {v7, v0, v1}, Lcom/google/android/flexbox/FlexboxHelper;->extractLowerInt(J)I

    move-result v0

    :cond_c
    invoke-virtual {v11}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    iget-object v13, v7, Lcom/google/android/flexbox/FlexboxHelper;->mMeasuredSizeCache:[J

    if-eqz v13, :cond_d

    move/from16 v21, v15

    aget-wide v14, v13, v10

    invoke-virtual {v7, v14, v15}, Lcom/google/android/flexbox/FlexboxHelper;->extractHigherInt(J)I

    move-result v1

    goto :goto_4

    :cond_d
    move/from16 v21, v15

    :goto_4
    iget-object v13, v7, Lcom/google/android/flexbox/FlexboxHelper;->mChildrenFrozen:[Z

    aget-boolean v13, v13, v10

    if-nez v13, :cond_12

    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->getFlexGrow()F

    move-result v13

    const/4 v14, 0x0

    cmpl-float v13, v13, v14

    if-lez v13, :cond_12

    int-to-float v0, v0

    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->getFlexGrow()F

    move-result v1

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iget v1, v3, Lcom/google/android/flexbox/FlexLine;->mItemCount:I

    const/4 v13, 0x1

    sub-int/2addr v1, v13

    if-ne v5, v1, :cond_e

    add-float/2addr v0, v8

    move v8, v14

    :cond_e
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->getMaxWidth()I

    move-result v15

    if-le v1, v15, :cond_f

    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->getMaxWidth()I

    move-result v1

    iget-object v0, v7, Lcom/google/android/flexbox/FlexboxHelper;->mChildrenFrozen:[Z

    aput-boolean v13, v0, v10

    iget v0, v3, Lcom/google/android/flexbox/FlexLine;->mTotalFlexGrow:F

    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->getFlexGrow()F

    move-result v6

    sub-float/2addr v0, v6

    iput v0, v3, Lcom/google/android/flexbox/FlexLine;->mTotalFlexGrow:F

    move v6, v13

    goto :goto_5

    :cond_f
    int-to-float v13, v1

    sub-float/2addr v0, v13

    add-float/2addr v8, v0

    float-to-double v14, v8

    cmpl-double v0, v14, v17

    if-lez v0, :cond_10

    add-int/lit8 v1, v1, 0x1

    sub-double v14, v14, v17

    double-to-float v0, v14

    move v8, v0

    goto :goto_5

    :cond_10
    const-wide/high16 v19, -0x4010000000000000L    # -1.0

    cmpg-double v0, v14, v19

    if-gez v0, :cond_11

    add-int/lit8 v1, v1, -0x1

    add-double v14, v14, v17

    double-to-float v0, v14

    move v8, v0

    :cond_11
    :goto_5
    iget v0, v3, Lcom/google/android/flexbox/FlexLine;->mSumCrossSizeBefore:I

    move/from16 v13, p2

    invoke-direct {v7, v13, v12, v0}, Lcom/google/android/flexbox/FlexboxHelper;->getChildHeightMeasureSpecInternal(ILcom/google/android/flexbox/FlexItem;I)I

    move-result v0

    const/high16 v14, 0x40000000    # 2.0f

    invoke-static {v1, v14}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {v11, v1, v0}, Landroid/view/View;->measure(II)V

    invoke-virtual {v11}, Landroid/view/View;->getMeasuredWidth()I

    move-result v14

    invoke-virtual {v11}, Landroid/view/View;->getMeasuredHeight()I

    move-result v15

    invoke-direct {v7, v10, v1, v0, v11}, Lcom/google/android/flexbox/FlexboxHelper;->updateMeasureCache(IIILandroid/view/View;)V

    iget-object v0, v7, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    invoke-interface {v0, v10, v11}, Lcom/google/android/flexbox/FlexContainer;->updateViewCache(ILandroid/view/View;)V

    goto :goto_6

    :cond_12
    move/from16 v13, p2

    move v14, v0

    move v15, v1

    :goto_6
    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->getMarginTop()I

    move-result v0

    add-int/2addr v15, v0

    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->getMarginBottom()I

    move-result v0

    add-int/2addr v15, v0

    iget-object v0, v7, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    invoke-interface {v0, v11}, Lcom/google/android/flexbox/FlexContainer;->getDecorationLengthCrossAxis(Landroid/view/View;)I

    move-result v0

    add-int/2addr v15, v0

    invoke-static {v9, v15}, Ljava/lang/Math;->max(II)I

    move-result v0

    iget v1, v3, Lcom/google/android/flexbox/FlexLine;->mMainSize:I

    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->getMarginLeft()I

    move-result v9

    add-int/2addr v14, v9

    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->getMarginRight()I

    move-result v9

    add-int/2addr v14, v9

    add-int/2addr v1, v14

    iput v1, v3, Lcom/google/android/flexbox/FlexLine;->mMainSize:I

    :goto_7
    iget v1, v3, Lcom/google/android/flexbox/FlexLine;->mCrossSize:I

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, v3, Lcom/google/android/flexbox/FlexLine;->mCrossSize:I

    move v9, v0

    goto :goto_8

    :cond_13
    move/from16 v13, p2

    move/from16 v21, v0

    :goto_8
    add-int/lit8 v5, v5, 0x1

    move/from16 v0, v21

    const/4 v1, 0x0

    goto/16 :goto_0

    :cond_14
    move/from16 v13, p2

    move/from16 v21, v0

    if-eqz v6, :cond_15

    iget v0, v3, Lcom/google/android/flexbox/FlexLine;->mMainSize:I

    move/from16 v1, v21

    if-eq v1, v0, :cond_15

    const/4 v6, 0x1

    move-object/from16 v0, p0

    move/from16 v1, p1

    move/from16 v2, p2

    move-object/from16 v3, p3

    move/from16 v4, p4

    move/from16 v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/flexbox/FlexboxHelper;->expandFlexItems(IILcom/google/android/flexbox/FlexLine;IIZ)V

    :cond_15
    return-void

    :cond_16
    :goto_9
    return-void
.end method

.method private getChildHeightMeasureSpecInternal(ILcom/google/android/flexbox/FlexItem;I)I
    .locals 3

    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    invoke-interface {v0}, Lcom/google/android/flexbox/FlexContainer;->getPaddingTop()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    invoke-interface {v2}, Lcom/google/android/flexbox/FlexContainer;->getPaddingBottom()I

    move-result v2

    add-int/2addr v1, v2

    invoke-interface {p2}, Lcom/google/android/flexbox/FlexItem;->getMarginTop()I

    move-result v2

    add-int/2addr v1, v2

    invoke-interface {p2}, Lcom/google/android/flexbox/FlexItem;->getMarginBottom()I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v1, p3

    invoke-interface {p2}, Lcom/google/android/flexbox/FlexItem;->getHeight()I

    move-result p3

    invoke-interface {v0, p1, v1, p3}, Lcom/google/android/flexbox/FlexContainer;->getChildHeightMeasureSpec(III)I

    move-result p1

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p3

    invoke-interface {p2}, Lcom/google/android/flexbox/FlexItem;->getMaxHeight()I

    move-result v0

    if-le p3, v0, :cond_0

    invoke-interface {p2}, Lcom/google/android/flexbox/FlexItem;->getMaxHeight()I

    move-result p2

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result p1

    invoke-static {p2, p1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    goto :goto_0

    :cond_0
    invoke-interface {p2}, Lcom/google/android/flexbox/FlexItem;->getMinHeight()I

    move-result v0

    if-ge p3, v0, :cond_1

    invoke-interface {p2}, Lcom/google/android/flexbox/FlexItem;->getMinHeight()I

    move-result p2

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result p1

    invoke-static {p2, p1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    :cond_1
    :goto_0
    return p1
.end method

.method private getChildWidthMeasureSpecInternal(ILcom/google/android/flexbox/FlexItem;I)I
    .locals 3

    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    invoke-interface {v0}, Lcom/google/android/flexbox/FlexContainer;->getPaddingLeft()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    invoke-interface {v2}, Lcom/google/android/flexbox/FlexContainer;->getPaddingRight()I

    move-result v2

    add-int/2addr v1, v2

    invoke-interface {p2}, Lcom/google/android/flexbox/FlexItem;->getMarginLeft()I

    move-result v2

    add-int/2addr v1, v2

    invoke-interface {p2}, Lcom/google/android/flexbox/FlexItem;->getMarginRight()I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v1, p3

    invoke-interface {p2}, Lcom/google/android/flexbox/FlexItem;->getWidth()I

    move-result p3

    invoke-interface {v0, p1, v1, p3}, Lcom/google/android/flexbox/FlexContainer;->getChildWidthMeasureSpec(III)I

    move-result p1

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p3

    invoke-interface {p2}, Lcom/google/android/flexbox/FlexItem;->getMaxWidth()I

    move-result v0

    if-le p3, v0, :cond_0

    invoke-interface {p2}, Lcom/google/android/flexbox/FlexItem;->getMaxWidth()I

    move-result p2

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result p1

    invoke-static {p2, p1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    goto :goto_0

    :cond_0
    invoke-interface {p2}, Lcom/google/android/flexbox/FlexItem;->getMinWidth()I

    move-result v0

    if-ge p3, v0, :cond_1

    invoke-interface {p2}, Lcom/google/android/flexbox/FlexItem;->getMinWidth()I

    move-result p2

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result p1

    invoke-static {p2, p1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    :cond_1
    :goto_0
    return p1
.end method

.method private getFlexItemMarginEndCross(Lcom/google/android/flexbox/FlexItem;Z)I
    .locals 0

    if-eqz p2, :cond_0

    invoke-interface {p1}, Lcom/google/android/flexbox/FlexItem;->getMarginBottom()I

    move-result p1

    return p1

    :cond_0
    invoke-interface {p1}, Lcom/google/android/flexbox/FlexItem;->getMarginRight()I

    move-result p1

    return p1
.end method

.method private getFlexItemMarginEndMain(Lcom/google/android/flexbox/FlexItem;Z)I
    .locals 0

    if-eqz p2, :cond_0

    invoke-interface {p1}, Lcom/google/android/flexbox/FlexItem;->getMarginRight()I

    move-result p1

    return p1

    :cond_0
    invoke-interface {p1}, Lcom/google/android/flexbox/FlexItem;->getMarginBottom()I

    move-result p1

    return p1
.end method

.method private getFlexItemMarginStartCross(Lcom/google/android/flexbox/FlexItem;Z)I
    .locals 0

    if-eqz p2, :cond_0

    invoke-interface {p1}, Lcom/google/android/flexbox/FlexItem;->getMarginTop()I

    move-result p1

    return p1

    :cond_0
    invoke-interface {p1}, Lcom/google/android/flexbox/FlexItem;->getMarginLeft()I

    move-result p1

    return p1
.end method

.method private getFlexItemMarginStartMain(Lcom/google/android/flexbox/FlexItem;Z)I
    .locals 0

    if-eqz p2, :cond_0

    invoke-interface {p1}, Lcom/google/android/flexbox/FlexItem;->getMarginLeft()I

    move-result p1

    return p1

    :cond_0
    invoke-interface {p1}, Lcom/google/android/flexbox/FlexItem;->getMarginTop()I

    move-result p1

    return p1
.end method

.method private getFlexItemSizeCross(Lcom/google/android/flexbox/FlexItem;Z)I
    .locals 0

    if-eqz p2, :cond_0

    invoke-interface {p1}, Lcom/google/android/flexbox/FlexItem;->getHeight()I

    move-result p1

    return p1

    :cond_0
    invoke-interface {p1}, Lcom/google/android/flexbox/FlexItem;->getWidth()I

    move-result p1

    return p1
.end method

.method private getFlexItemSizeMain(Lcom/google/android/flexbox/FlexItem;Z)I
    .locals 0

    if-eqz p2, :cond_0

    invoke-interface {p1}, Lcom/google/android/flexbox/FlexItem;->getWidth()I

    move-result p1

    return p1

    :cond_0
    invoke-interface {p1}, Lcom/google/android/flexbox/FlexItem;->getHeight()I

    move-result p1

    return p1
.end method

.method private getPaddingEndCross(Z)I
    .locals 0

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    invoke-interface {p1}, Lcom/google/android/flexbox/FlexContainer;->getPaddingBottom()I

    move-result p1

    return p1

    :cond_0
    iget-object p1, p0, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    invoke-interface {p1}, Lcom/google/android/flexbox/FlexContainer;->getPaddingEnd()I

    move-result p1

    return p1
.end method

.method private getPaddingEndMain(Z)I
    .locals 0

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    invoke-interface {p1}, Lcom/google/android/flexbox/FlexContainer;->getPaddingEnd()I

    move-result p1

    return p1

    :cond_0
    iget-object p1, p0, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    invoke-interface {p1}, Lcom/google/android/flexbox/FlexContainer;->getPaddingBottom()I

    move-result p1

    return p1
.end method

.method private getPaddingStartCross(Z)I
    .locals 0

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    invoke-interface {p1}, Lcom/google/android/flexbox/FlexContainer;->getPaddingTop()I

    move-result p1

    return p1

    :cond_0
    iget-object p1, p0, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    invoke-interface {p1}, Lcom/google/android/flexbox/FlexContainer;->getPaddingStart()I

    move-result p1

    return p1
.end method

.method private getPaddingStartMain(Z)I
    .locals 0

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    invoke-interface {p1}, Lcom/google/android/flexbox/FlexContainer;->getPaddingStart()I

    move-result p1

    return p1

    :cond_0
    iget-object p1, p0, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    invoke-interface {p1}, Lcom/google/android/flexbox/FlexContainer;->getPaddingTop()I

    move-result p1

    return p1
.end method

.method private getViewMeasuredSizeCross(Landroid/view/View;Z)I
    .locals 0

    if-eqz p2, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result p1

    return p1

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result p1

    return p1
.end method

.method private getViewMeasuredSizeMain(Landroid/view/View;Z)I
    .locals 0

    if-eqz p2, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result p1

    return p1

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result p1

    return p1
.end method

.method private isLastFlexItem(IILcom/google/android/flexbox/FlexLine;)Z
    .locals 1

    const/4 v0, 0x1

    sub-int/2addr p2, v0

    if-ne p1, p2, :cond_0

    invoke-virtual {p3}, Lcom/google/android/flexbox/FlexLine;->getItemCountNotGone()I

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private isWrapRequired(Landroid/view/View;IIIILcom/google/android/flexbox/FlexItem;III)Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    invoke-interface {v0}, Lcom/google/android/flexbox/FlexContainer;->getFlexWrap()I

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    invoke-interface {p6}, Lcom/google/android/flexbox/FlexItem;->isWrapBefore()Z

    move-result p6

    const/4 v0, 0x1

    if-eqz p6, :cond_1

    return v0

    :cond_1
    if-nez p2, :cond_2

    return v1

    :cond_2
    iget-object p2, p0, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    invoke-interface {p2}, Lcom/google/android/flexbox/FlexContainer;->getMaxLine()I

    move-result p2

    const/4 p6, -0x1

    if-eq p2, p6, :cond_3

    add-int/2addr p9, v0

    if-gt p2, p9, :cond_3

    return v1

    :cond_3
    iget-object p2, p0, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    invoke-interface {p2, p1, p7, p8}, Lcom/google/android/flexbox/FlexContainer;->getDecorationLengthMainAxis(Landroid/view/View;II)I

    move-result p1

    if-lez p1, :cond_4

    add-int/2addr p5, p1

    :cond_4
    add-int/2addr p4, p5

    if-ge p3, p4, :cond_5

    goto :goto_0

    :cond_5
    move v0, v1

    :goto_0
    return v0
.end method

.method private shrinkFlexItems(IILcom/google/android/flexbox/FlexLine;IIZ)V
    .locals 20

    move-object/from16 v7, p0

    move-object/from16 v3, p3

    move/from16 v4, p4

    iget v0, v3, Lcom/google/android/flexbox/FlexLine;->mMainSize:I

    iget v1, v3, Lcom/google/android/flexbox/FlexLine;->mTotalFlexShrink:F

    const/4 v2, 0x0

    cmpg-float v1, v1, v2

    if-lez v1, :cond_16

    iget v1, v3, Lcom/google/android/flexbox/FlexLine;->mMainSize:I

    if-le v4, v1, :cond_0

    goto/16 :goto_8

    :cond_0
    iget v1, v3, Lcom/google/android/flexbox/FlexLine;->mMainSize:I

    sub-int/2addr v1, v4

    int-to-float v1, v1

    iget v5, v3, Lcom/google/android/flexbox/FlexLine;->mTotalFlexShrink:F

    div-float/2addr v1, v5

    iget v5, v3, Lcom/google/android/flexbox/FlexLine;->mDividerLengthInMainSize:I

    add-int v5, p5, v5

    iput v5, v3, Lcom/google/android/flexbox/FlexLine;->mMainSize:I

    if-nez p6, :cond_1

    const/high16 v5, -0x80000000

    iput v5, v3, Lcom/google/android/flexbox/FlexLine;->mCrossSize:I

    :cond_1
    const/4 v5, 0x0

    move v8, v2

    move v6, v5

    move v9, v6

    :goto_0
    iget v10, v3, Lcom/google/android/flexbox/FlexLine;->mItemCount:I

    if-ge v5, v10, :cond_14

    iget v10, v3, Lcom/google/android/flexbox/FlexLine;->mFirstIndex:I

    add-int/2addr v10, v5

    iget-object v11, v7, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    invoke-interface {v11, v10}, Lcom/google/android/flexbox/FlexContainer;->getReorderedFlexItemAt(I)Landroid/view/View;

    move-result-object v11

    if-eqz v11, :cond_13

    invoke-virtual {v11}, Landroid/view/View;->getVisibility()I

    move-result v12

    const/16 v13, 0x8

    if-ne v12, v13, :cond_2

    move/from16 v14, p2

    move v2, v5

    move/from16 v5, p1

    goto/16 :goto_7

    :cond_2
    invoke-virtual {v11}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v12

    check-cast v12, Lcom/google/android/flexbox/FlexItem;

    iget-object v13, v7, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    invoke-interface {v13}, Lcom/google/android/flexbox/FlexContainer;->getFlexDirection()I

    move-result v13

    const-wide/high16 v17, 0x3ff0000000000000L    # 1.0

    const/high16 v19, 0x3f800000    # 1.0f

    const/4 v14, 0x1

    if-eqz v13, :cond_b

    if-ne v13, v14, :cond_3

    goto/16 :goto_3

    :cond_3
    invoke-virtual {v11}, Landroid/view/View;->getMeasuredHeight()I

    move-result v13

    iget-object v15, v7, Lcom/google/android/flexbox/FlexboxHelper;->mMeasuredSizeCache:[J

    if-eqz v15, :cond_4

    aget-wide v14, v15, v10

    invoke-virtual {v7, v14, v15}, Lcom/google/android/flexbox/FlexboxHelper;->extractHigherInt(J)I

    move-result v13

    :cond_4
    invoke-virtual {v11}, Landroid/view/View;->getMeasuredWidth()I

    move-result v14

    iget-object v15, v7, Lcom/google/android/flexbox/FlexboxHelper;->mMeasuredSizeCache:[J

    if-eqz v15, :cond_5

    aget-wide v14, v15, v10

    invoke-virtual {v7, v14, v15}, Lcom/google/android/flexbox/FlexboxHelper;->extractLowerInt(J)I

    move-result v14

    :cond_5
    iget-object v15, v7, Lcom/google/android/flexbox/FlexboxHelper;->mChildrenFrozen:[Z

    aget-boolean v15, v15, v10

    if-nez v15, :cond_a

    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->getFlexShrink()F

    move-result v15

    cmpl-float v15, v15, v2

    if-lez v15, :cond_a

    int-to-float v13, v13

    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->getFlexShrink()F

    move-result v14

    mul-float/2addr v14, v1

    sub-float/2addr v13, v14

    iget v14, v3, Lcom/google/android/flexbox/FlexLine;->mItemCount:I

    const/4 v15, 0x1

    sub-int/2addr v14, v15

    if-ne v5, v14, :cond_6

    add-float/2addr v13, v8

    move v8, v2

    :cond_6
    invoke-static {v13}, Ljava/lang/Math;->round(F)I

    move-result v14

    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->getMinHeight()I

    move-result v2

    if-ge v14, v2, :cond_7

    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->getMinHeight()I

    move-result v14

    iget-object v2, v7, Lcom/google/android/flexbox/FlexboxHelper;->mChildrenFrozen:[Z

    aput-boolean v15, v2, v10

    iget v2, v3, Lcom/google/android/flexbox/FlexLine;->mTotalFlexShrink:F

    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->getFlexShrink()F

    move-result v6

    sub-float/2addr v2, v6

    iput v2, v3, Lcom/google/android/flexbox/FlexLine;->mTotalFlexShrink:F

    move v2, v5

    const/4 v6, 0x1

    goto :goto_1

    :cond_7
    int-to-float v2, v14

    sub-float/2addr v13, v2

    add-float/2addr v8, v13

    move v2, v5

    float-to-double v4, v8

    cmpl-double v13, v4, v17

    if-lez v13, :cond_8

    add-int/lit8 v14, v14, 0x1

    sub-float v8, v8, v19

    goto :goto_1

    :cond_8
    const-wide/high16 v15, -0x4010000000000000L    # -1.0

    cmpg-double v4, v4, v15

    if-gez v4, :cond_9

    add-int/lit8 v14, v14, -0x1

    add-float v8, v8, v19

    :cond_9
    :goto_1
    iget v4, v3, Lcom/google/android/flexbox/FlexLine;->mSumCrossSizeBefore:I

    move/from16 v5, p1

    invoke-direct {v7, v5, v12, v4}, Lcom/google/android/flexbox/FlexboxHelper;->getChildWidthMeasureSpecInternal(ILcom/google/android/flexbox/FlexItem;I)I

    move-result v4

    const/high16 v13, 0x40000000    # 2.0f

    invoke-static {v14, v13}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v13

    invoke-virtual {v11, v4, v13}, Landroid/view/View;->measure(II)V

    invoke-virtual {v11}, Landroid/view/View;->getMeasuredWidth()I

    move-result v14

    invoke-virtual {v11}, Landroid/view/View;->getMeasuredHeight()I

    move-result v15

    invoke-direct {v7, v10, v4, v13, v11}, Lcom/google/android/flexbox/FlexboxHelper;->updateMeasureCache(IIILandroid/view/View;)V

    iget-object v4, v7, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    invoke-interface {v4, v10, v11}, Lcom/google/android/flexbox/FlexContainer;->updateViewCache(ILandroid/view/View;)V

    move v13, v15

    goto :goto_2

    :cond_a
    move v2, v5

    move/from16 v5, p1

    :goto_2
    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->getMarginLeft()I

    move-result v4

    add-int/2addr v14, v4

    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->getMarginRight()I

    move-result v4

    add-int/2addr v14, v4

    iget-object v4, v7, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    invoke-interface {v4, v11}, Lcom/google/android/flexbox/FlexContainer;->getDecorationLengthCrossAxis(Landroid/view/View;)I

    move-result v4

    add-int/2addr v14, v4

    invoke-static {v9, v14}, Ljava/lang/Math;->max(II)I

    move-result v4

    iget v9, v3, Lcom/google/android/flexbox/FlexLine;->mMainSize:I

    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->getMarginTop()I

    move-result v10

    add-int/2addr v13, v10

    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->getMarginBottom()I

    move-result v10

    add-int/2addr v13, v10

    add-int/2addr v9, v13

    iput v9, v3, Lcom/google/android/flexbox/FlexLine;->mMainSize:I

    move/from16 v14, p2

    goto/16 :goto_6

    :cond_b
    :goto_3
    move v2, v5

    move/from16 v5, p1

    invoke-virtual {v11}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    iget-object v13, v7, Lcom/google/android/flexbox/FlexboxHelper;->mMeasuredSizeCache:[J

    if-eqz v13, :cond_c

    aget-wide v14, v13, v10

    invoke-virtual {v7, v14, v15}, Lcom/google/android/flexbox/FlexboxHelper;->extractLowerInt(J)I

    move-result v4

    :cond_c
    invoke-virtual {v11}, Landroid/view/View;->getMeasuredHeight()I

    move-result v13

    iget-object v14, v7, Lcom/google/android/flexbox/FlexboxHelper;->mMeasuredSizeCache:[J

    if-eqz v14, :cond_d

    aget-wide v13, v14, v10

    invoke-virtual {v7, v13, v14}, Lcom/google/android/flexbox/FlexboxHelper;->extractHigherInt(J)I

    move-result v13

    :cond_d
    iget-object v14, v7, Lcom/google/android/flexbox/FlexboxHelper;->mChildrenFrozen:[Z

    aget-boolean v14, v14, v10

    if-nez v14, :cond_12

    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->getFlexShrink()F

    move-result v14

    const/4 v15, 0x0

    cmpl-float v14, v14, v15

    if-lez v14, :cond_12

    int-to-float v4, v4

    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->getFlexShrink()F

    move-result v13

    mul-float/2addr v13, v1

    sub-float/2addr v4, v13

    iget v13, v3, Lcom/google/android/flexbox/FlexLine;->mItemCount:I

    const/4 v14, 0x1

    sub-int/2addr v13, v14

    if-ne v2, v13, :cond_e

    add-float/2addr v4, v8

    move v8, v15

    :cond_e
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v13

    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->getMinWidth()I

    move-result v15

    if-ge v13, v15, :cond_f

    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->getMinWidth()I

    move-result v13

    iget-object v4, v7, Lcom/google/android/flexbox/FlexboxHelper;->mChildrenFrozen:[Z

    aput-boolean v14, v4, v10

    iget v4, v3, Lcom/google/android/flexbox/FlexLine;->mTotalFlexShrink:F

    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->getFlexShrink()F

    move-result v6

    sub-float/2addr v4, v6

    iput v4, v3, Lcom/google/android/flexbox/FlexLine;->mTotalFlexShrink:F

    move v6, v14

    goto :goto_4

    :cond_f
    int-to-float v14, v13

    sub-float/2addr v4, v14

    add-float/2addr v8, v4

    float-to-double v14, v8

    cmpl-double v4, v14, v17

    if-lez v4, :cond_10

    add-int/lit8 v13, v13, 0x1

    sub-float v8, v8, v19

    goto :goto_4

    :cond_10
    const-wide/high16 v16, -0x4010000000000000L    # -1.0

    cmpg-double v4, v14, v16

    if-gez v4, :cond_11

    add-int/lit8 v13, v13, -0x1

    add-float v8, v8, v19

    :cond_11
    :goto_4
    iget v4, v3, Lcom/google/android/flexbox/FlexLine;->mSumCrossSizeBefore:I

    move/from16 v14, p2

    invoke-direct {v7, v14, v12, v4}, Lcom/google/android/flexbox/FlexboxHelper;->getChildHeightMeasureSpecInternal(ILcom/google/android/flexbox/FlexItem;I)I

    move-result v4

    const/high16 v15, 0x40000000    # 2.0f

    invoke-static {v13, v15}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v13

    invoke-virtual {v11, v13, v4}, Landroid/view/View;->measure(II)V

    invoke-virtual {v11}, Landroid/view/View;->getMeasuredWidth()I

    move-result v15

    invoke-virtual {v11}, Landroid/view/View;->getMeasuredHeight()I

    move-result v16

    invoke-direct {v7, v10, v13, v4, v11}, Lcom/google/android/flexbox/FlexboxHelper;->updateMeasureCache(IIILandroid/view/View;)V

    iget-object v4, v7, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    invoke-interface {v4, v10, v11}, Lcom/google/android/flexbox/FlexContainer;->updateViewCache(ILandroid/view/View;)V

    move/from16 v13, v16

    goto :goto_5

    :cond_12
    move/from16 v14, p2

    move v15, v4

    :goto_5
    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->getMarginTop()I

    move-result v4

    add-int/2addr v13, v4

    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->getMarginBottom()I

    move-result v4

    add-int/2addr v13, v4

    iget-object v4, v7, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    invoke-interface {v4, v11}, Lcom/google/android/flexbox/FlexContainer;->getDecorationLengthCrossAxis(Landroid/view/View;)I

    move-result v4

    add-int/2addr v13, v4

    invoke-static {v9, v13}, Ljava/lang/Math;->max(II)I

    move-result v4

    iget v9, v3, Lcom/google/android/flexbox/FlexLine;->mMainSize:I

    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->getMarginLeft()I

    move-result v10

    add-int/2addr v15, v10

    invoke-interface {v12}, Lcom/google/android/flexbox/FlexItem;->getMarginRight()I

    move-result v10

    add-int/2addr v15, v10

    add-int/2addr v9, v15

    iput v9, v3, Lcom/google/android/flexbox/FlexLine;->mMainSize:I

    :goto_6
    iget v9, v3, Lcom/google/android/flexbox/FlexLine;->mCrossSize:I

    invoke-static {v9, v4}, Ljava/lang/Math;->max(II)I

    move-result v9

    iput v9, v3, Lcom/google/android/flexbox/FlexLine;->mCrossSize:I

    move v9, v4

    goto :goto_7

    :cond_13
    move/from16 v14, p2

    move v2, v5

    move/from16 v5, p1

    :goto_7
    add-int/lit8 v2, v2, 0x1

    move v5, v2

    const/4 v2, 0x0

    move/from16 v4, p4

    goto/16 :goto_0

    :cond_14
    move/from16 v5, p1

    move/from16 v14, p2

    if-eqz v6, :cond_15

    iget v1, v3, Lcom/google/android/flexbox/FlexLine;->mMainSize:I

    if-eq v0, v1, :cond_15

    const/4 v6, 0x1

    move-object/from16 v0, p0

    move/from16 v1, p1

    move/from16 v2, p2

    move-object/from16 v3, p3

    move/from16 v4, p4

    move/from16 v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/flexbox/FlexboxHelper;->shrinkFlexItems(IILcom/google/android/flexbox/FlexLine;IIZ)V

    :cond_15
    return-void

    :cond_16
    :goto_8
    return-void
.end method

.method private sortOrdersIntoReorderedIndices(ILjava/util/List;Landroid/util/SparseIntArray;)[I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "Lcom/google/android/flexbox/FlexboxHelper$Order;",
            ">;",
            "Landroid/util/SparseIntArray;",
            ")[I"
        }
    .end annotation

    invoke-static {p2}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    invoke-virtual {p3}, Landroid/util/SparseIntArray;->clear()V

    new-array p1, p1, [I

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    const/4 v0, 0x0

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/flexbox/FlexboxHelper$Order;

    iget v2, v1, Lcom/google/android/flexbox/FlexboxHelper$Order;->index:I

    aput v2, p1, v0

    iget v2, v1, Lcom/google/android/flexbox/FlexboxHelper$Order;->index:I

    iget v1, v1, Lcom/google/android/flexbox/FlexboxHelper$Order;->order:I

    invoke-virtual {p3, v2, v1}, Landroid/util/SparseIntArray;->append(II)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object p1
.end method

.method private stretchViewHorizontally(Landroid/view/View;II)V
    .locals 3

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/flexbox/FlexItem;

    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->getMarginLeft()I

    move-result v1

    sub-int/2addr p2, v1

    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->getMarginRight()I

    move-result v1

    sub-int/2addr p2, v1

    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    invoke-interface {v1, p1}, Lcom/google/android/flexbox/FlexContainer;->getDecorationLengthCrossAxis(Landroid/view/View;)I

    move-result v1

    sub-int/2addr p2, v1

    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->getMinWidth()I

    move-result v1

    invoke-static {p2, v1}, Ljava/lang/Math;->max(II)I

    move-result p2

    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->getMaxWidth()I

    move-result v0

    invoke-static {p2, v0}, Ljava/lang/Math;->min(II)I

    move-result p2

    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxHelper;->mMeasuredSizeCache:[J

    if-eqz v0, :cond_0

    aget-wide v1, v0, p3

    invoke-virtual {p0, v1, v2}, Lcom/google/android/flexbox/FlexboxHelper;->extractHigherInt(J)I

    move-result v0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    :goto_0
    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-static {p2, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    invoke-virtual {p1, p2, v0}, Landroid/view/View;->measure(II)V

    invoke-direct {p0, p3, p2, v0, p1}, Lcom/google/android/flexbox/FlexboxHelper;->updateMeasureCache(IIILandroid/view/View;)V

    iget-object p2, p0, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    invoke-interface {p2, p3, p1}, Lcom/google/android/flexbox/FlexContainer;->updateViewCache(ILandroid/view/View;)V

    return-void
.end method

.method private stretchViewVertically(Landroid/view/View;II)V
    .locals 3

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/flexbox/FlexItem;

    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->getMarginTop()I

    move-result v1

    sub-int/2addr p2, v1

    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->getMarginBottom()I

    move-result v1

    sub-int/2addr p2, v1

    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    invoke-interface {v1, p1}, Lcom/google/android/flexbox/FlexContainer;->getDecorationLengthCrossAxis(Landroid/view/View;)I

    move-result v1

    sub-int/2addr p2, v1

    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->getMinHeight()I

    move-result v1

    invoke-static {p2, v1}, Ljava/lang/Math;->max(II)I

    move-result p2

    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->getMaxHeight()I

    move-result v0

    invoke-static {p2, v0}, Ljava/lang/Math;->min(II)I

    move-result p2

    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxHelper;->mMeasuredSizeCache:[J

    if-eqz v0, :cond_0

    aget-wide v1, v0, p3

    invoke-virtual {p0, v1, v2}, Lcom/google/android/flexbox/FlexboxHelper;->extractLowerInt(J)I

    move-result v0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    :goto_0
    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-static {p2, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    invoke-virtual {p1, v0, p2}, Landroid/view/View;->measure(II)V

    invoke-direct {p0, p3, v0, p2, p1}, Lcom/google/android/flexbox/FlexboxHelper;->updateMeasureCache(IIILandroid/view/View;)V

    iget-object p2, p0, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    invoke-interface {p2, p3, p1}, Lcom/google/android/flexbox/FlexContainer;->updateViewCache(ILandroid/view/View;)V

    return-void
.end method

.method private updateMeasureCache(IIILandroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxHelper;->mMeasureSpecCache:[J

    if-eqz v0, :cond_0

    invoke-virtual {p0, p2, p3}, Lcom/google/android/flexbox/FlexboxHelper;->makeCombinedLong(II)J

    move-result-wide p2

    aput-wide p2, v0, p1

    :cond_0
    iget-object p2, p0, Lcom/google/android/flexbox/FlexboxHelper;->mMeasuredSizeCache:[J

    if-eqz p2, :cond_1

    invoke-virtual {p4}, Landroid/view/View;->getMeasuredWidth()I

    move-result p3

    invoke-virtual {p4}, Landroid/view/View;->getMeasuredHeight()I

    move-result p4

    invoke-virtual {p0, p3, p4}, Lcom/google/android/flexbox/FlexboxHelper;->makeCombinedLong(II)J

    move-result-wide p3

    aput-wide p3, p2, p1

    :cond_1
    return-void
.end method


# virtual methods
.method calculateFlexLines(Lcom/google/android/flexbox/FlexboxHelper$FlexLinesResult;IIIIILjava/util/List;)V
    .locals 31
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/flexbox/FlexboxHelper$FlexLinesResult;",
            "IIIII",
            "Ljava/util/List<",
            "Lcom/google/android/flexbox/FlexLine;",
            ">;)V"
        }
    .end annotation

    goto/32 :goto_b2

    nop

    :goto_0
    goto/16 :goto_1b4

    :goto_1
    goto/32 :goto_1b3

    nop

    :goto_2
    const/4 v5, 0x1

    goto/32 :goto_35

    nop

    :goto_3
    invoke-direct {v3}, Lcom/google/android/flexbox/FlexLine;-><init>()V

    goto/32 :goto_22

    nop

    :goto_4
    invoke-interface {v5, v6}, Lcom/google/android/flexbox/FlexContainer;->getReorderedFlexItemAt(I)Landroid/view/View;

    move-result-object v5

    goto/32 :goto_a0

    nop

    :goto_5
    move/from16 v21, v18

    goto/32 :goto_11c

    nop

    :goto_6
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    :goto_7
    goto/32 :goto_113

    nop

    :goto_8
    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    goto/32 :goto_62

    nop

    :goto_9
    if-eq v1, v2, :cond_0

    goto/32 :goto_13d

    :cond_0
    goto/32 :goto_fc

    nop

    :goto_a
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto/32 :goto_1a

    nop

    :goto_b
    invoke-direct {v3}, Lcom/google/android/flexbox/FlexLine;-><init>()V

    goto/32 :goto_183

    nop

    :goto_c
    invoke-interface {v11}, Lcom/google/android/flexbox/FlexItem;->getWidth()I

    move-result v1

    goto/32 :goto_83

    nop

    :goto_d
    move/from16 v26, v9

    goto/32 :goto_e9

    nop

    :goto_e
    iget-object v8, v10, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    goto/32 :goto_53

    nop

    :goto_f
    iget v4, v3, Lcom/google/android/flexbox/FlexLine;->mCrossSize:I

    goto/32 :goto_174

    nop

    :goto_10
    invoke-interface {v14, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    goto/32 :goto_6e

    nop

    :goto_11
    neg-int v12, v4

    goto/32 :goto_153

    nop

    :goto_12
    if-nez v4, :cond_1

    goto/32 :goto_150

    :cond_1
    goto/32 :goto_5e

    nop

    :goto_13
    invoke-direct {v10, v7, v3, v6, v0}, Lcom/google/android/flexbox/FlexboxHelper;->addFlexLine(Ljava/util/List;Lcom/google/android/flexbox/FlexLine;II)V

    :goto_14
    goto/32 :goto_162

    nop

    :goto_15
    if-nez v4, :cond_2

    goto/32 :goto_14d

    :cond_2
    goto/32 :goto_7d

    nop

    :goto_16
    move-object/from16 v3, v28

    goto/32 :goto_139

    nop

    :goto_17
    invoke-direct {v10, v11, v8}, Lcom/google/android/flexbox/FlexboxHelper;->getFlexItemMarginStartCross(Lcom/google/android/flexbox/FlexItem;Z)I

    move-result v26

    goto/32 :goto_e8

    nop

    :goto_18
    or-int/2addr v4, v8

    goto/32 :goto_d9

    nop

    :goto_19
    move-object v14, v7

    goto/32 :goto_11b

    nop

    :goto_1a
    move-object v7, v0

    goto/32 :goto_bb

    nop

    :goto_1b
    invoke-direct {v10, v11, v15}, Lcom/google/android/flexbox/FlexboxHelper;->getFlexItemMarginStartCross(Lcom/google/android/flexbox/FlexItem;Z)I

    move-result v8

    goto/32 :goto_79

    nop

    :goto_1c
    new-instance v3, Lcom/google/android/flexbox/FlexLine;

    goto/32 :goto_3

    nop

    :goto_1d
    invoke-virtual {v5, v4, v1}, Landroid/view/View;->measure(II)V

    goto/32 :goto_c1

    nop

    :goto_1e
    iget-object v4, v10, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    goto/32 :goto_f1

    nop

    :goto_1f
    invoke-direct/range {v0 .. v9}, Lcom/google/android/flexbox/FlexboxHelper;->isWrapRequired(Landroid/view/View;IIIILcom/google/android/flexbox/FlexItem;III)Z

    move-result v0

    goto/32 :goto_10a

    nop

    :goto_20
    add-int v9, v9, v23

    goto/32 :goto_161

    nop

    :goto_21
    invoke-interface {v1, v5, v4, v7}, Lcom/google/android/flexbox/FlexContainer;->getChildWidthMeasureSpec(III)I

    move-result v1

    goto/32 :goto_18f

    nop

    :goto_22
    const/4 v1, 0x1

    goto/32 :goto_9b

    nop

    :goto_23
    goto/16 :goto_a2

    :goto_24
    goto/32 :goto_1a4

    nop

    :goto_25
    add-int/2addr v8, v2

    goto/32 :goto_58

    nop

    :goto_26
    iget-object v0, v10, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    goto/32 :goto_104

    nop

    :goto_27
    move/from16 v3, v24

    goto/32 :goto_d5

    nop

    :goto_28
    if-gtz v0, :cond_3

    goto/32 :goto_9e

    :cond_3
    goto/32 :goto_112

    nop

    :goto_29
    add-int/2addr v4, v0

    goto/32 :goto_11e

    nop

    :goto_2a
    invoke-direct {v10, v11, v15}, Lcom/google/android/flexbox/FlexboxHelper;->getFlexItemMarginEndMain(Lcom/google/android/flexbox/FlexItem;Z)I

    move-result v9

    goto/32 :goto_11d

    nop

    :goto_2b
    add-int v24, v2, v24

    goto/32 :goto_bf

    nop

    :goto_2c
    add-int/2addr v2, v3

    goto/32 :goto_c3

    nop

    :goto_2d
    invoke-direct {v10, v15}, Lcom/google/android/flexbox/FlexboxHelper;->getPaddingStartMain(Z)I

    move-result v1

    goto/32 :goto_185

    nop

    :goto_2e
    move-object/from16 p7, v14

    goto/32 :goto_117

    nop

    :goto_2f
    const/4 v8, 0x1

    goto/32 :goto_17

    nop

    :goto_30
    invoke-virtual {v2}, Landroid/view/View;->getBaseline()I

    move-result v2

    goto/32 :goto_b7

    nop

    :goto_31
    add-int v14, v24, v26

    goto/32 :goto_136

    nop

    :goto_32
    const/4 v9, -0x1

    goto/32 :goto_b4

    nop

    :goto_33
    add-int/2addr v2, v1

    goto/32 :goto_165

    nop

    :goto_34
    add-int/2addr v2, v3

    goto/32 :goto_c5

    nop

    :goto_35
    add-int/2addr v4, v5

    goto/32 :goto_12e

    nop

    :goto_36
    move/from16 v4, v19

    goto/32 :goto_80

    nop

    :goto_37
    move v2, v7

    goto/32 :goto_166

    nop

    :goto_38
    invoke-interface {v1}, Lcom/google/android/flexbox/FlexContainer;->getPaddingTop()I

    move-result v2

    goto/32 :goto_f7

    nop

    :goto_39
    iput v4, v3, Lcom/google/android/flexbox/FlexLine;->mMainSize:I

    goto/32 :goto_151

    nop

    :goto_3a
    add-int/lit8 v6, v6, 0x1

    goto/32 :goto_f2

    nop

    :goto_3b
    const/4 v11, 0x1

    :goto_3c
    goto/32 :goto_ba

    nop

    :goto_3d
    goto/16 :goto_a9

    :goto_3e
    goto/32 :goto_a8

    nop

    :goto_3f
    invoke-virtual/range {v28 .. v28}, Lcom/google/android/flexbox/FlexLine;->getItemCountNotGone()I

    move-result v0

    goto/32 :goto_28

    nop

    :goto_40
    add-int v9, v9, v23

    goto/32 :goto_17a

    nop

    :goto_41
    move/from16 v8, v20

    goto/32 :goto_47

    nop

    :goto_42
    const/4 v9, -0x1

    :goto_43
    goto/32 :goto_12f

    nop

    :goto_44
    move v13, v5

    goto/32 :goto_37

    nop

    :goto_45
    iput v4, v3, Lcom/google/android/flexbox/FlexLine;->mCrossSize:I

    goto/32 :goto_8f

    nop

    :goto_46
    move/from16 v2, v25

    goto/32 :goto_15d

    nop

    :goto_47
    move/from16 v30, v9

    goto/32 :goto_154

    nop

    :goto_48
    const/4 v8, 0x1

    goto/32 :goto_b5

    nop

    :goto_49
    move/from16 v5, p3

    goto/32 :goto_dc

    nop

    :goto_4a
    if-eq v9, v4, :cond_4

    goto/32 :goto_7

    :cond_4
    goto/32 :goto_129

    nop

    :goto_4b
    move v5, v8

    goto/32 :goto_105

    nop

    :goto_4c
    move v8, v14

    goto/32 :goto_32

    nop

    :goto_4d
    move-object/from16 v11, p1

    goto/32 :goto_16f

    nop

    :goto_4e
    invoke-direct {v10, v11, v15}, Lcom/google/android/flexbox/FlexboxHelper;->getFlexItemMarginEndMain(Lcom/google/android/flexbox/FlexItem;Z)I

    move-result v8

    goto/32 :goto_89

    nop

    :goto_4f
    cmpl-float v8, v8, v9

    goto/32 :goto_1ab

    nop

    :goto_50
    iget-object v4, v10, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    goto/32 :goto_19f

    nop

    :goto_51
    iget-object v4, v10, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    goto/32 :goto_137

    nop

    :goto_52
    const/4 v0, 0x1

    goto/32 :goto_0

    nop

    :goto_53
    invoke-interface {v8, v2}, Lcom/google/android/flexbox/FlexContainer;->getDecorationLengthCrossAxis(Landroid/view/View;)I

    move-result v8

    goto/32 :goto_175

    nop

    :goto_54
    add-int/2addr v4, v7

    goto/32 :goto_b3

    nop

    :goto_55
    invoke-virtual {v2}, Landroid/view/View;->getBaseline()I

    move-result v2

    goto/32 :goto_f8

    nop

    :goto_56
    if-nez v4, :cond_5

    goto/32 :goto_7

    :cond_5
    goto/32 :goto_b1

    nop

    :goto_57
    or-int/2addr v4, v8

    goto/32 :goto_111

    nop

    :goto_58
    invoke-static {v4, v8}, Ljava/lang/Math;->max(II)I

    move-result v2

    goto/32 :goto_190

    nop

    :goto_59
    invoke-direct {v10, v6, v2, v3}, Lcom/google/android/flexbox/FlexboxHelper;->isLastFlexItem(IILcom/google/android/flexbox/FlexLine;)Z

    move-result v4

    goto/32 :goto_15

    nop

    :goto_5a
    new-instance v3, Lcom/google/android/flexbox/FlexLine;

    goto/32 :goto_b

    nop

    :goto_5b
    invoke-interface {v1}, Lcom/google/android/flexbox/FlexContainer;->getFlexItemCount()I

    move-result v1

    goto/32 :goto_17f

    nop

    :goto_5c
    add-float/2addr v4, v8

    goto/32 :goto_ef

    nop

    :goto_5d
    sub-int/2addr v4, v11

    goto/32 :goto_10

    nop

    :goto_5e
    move-object v4, v5

    goto/32 :goto_14b

    nop

    :goto_5f
    invoke-interface {v4}, Lcom/google/android/flexbox/FlexContainer;->getFlexWrap()I

    move-result v4

    goto/32 :goto_12a

    nop

    :goto_60
    invoke-interface {v11}, Lcom/google/android/flexbox/FlexItem;->getAlignSelf()I

    move-result v4

    goto/32 :goto_ec

    nop

    :goto_61
    iget-boolean v4, v3, Lcom/google/android/flexbox/FlexLine;->mAnyItemsHaveFlexGrow:Z

    goto/32 :goto_124

    nop

    :goto_62
    move-object v11, v4

    goto/32 :goto_7b

    nop

    :goto_63
    invoke-direct {v10, v2, v15}, Lcom/google/android/flexbox/FlexboxHelper;->getViewMeasuredSizeMain(Landroid/view/View;Z)I

    move-result v8

    goto/32 :goto_131

    nop

    :goto_64
    if-eqz v22, :cond_6

    goto/32 :goto_3c

    :cond_6
    goto/32 :goto_8d

    nop

    :goto_65
    iput v4, v3, Lcom/google/android/flexbox/FlexLine;->mItemCount:I

    goto/32 :goto_cf

    nop

    :goto_66
    goto :goto_75

    :goto_67
    goto/32 :goto_74

    nop

    :goto_68
    invoke-interface {v4, v13, v9, v8}, Lcom/google/android/flexbox/FlexContainer;->getChildWidthMeasureSpec(III)I

    move-result v4

    goto/32 :goto_14a

    nop

    :goto_69
    iput-object v7, v11, Lcom/google/android/flexbox/FlexboxHelper$FlexLinesResult;->mFlexLines:Ljava/util/List;

    goto/32 :goto_88

    nop

    :goto_6a
    move v6, v13

    goto/32 :goto_da

    nop

    :goto_6b
    aput v8, v4, v6

    :goto_6c
    goto/32 :goto_76

    nop

    :goto_6d
    move-object/from16 v2, p7

    goto/32 :goto_6a

    nop

    :goto_6e
    check-cast v4, Lcom/google/android/flexbox/FlexLine;

    goto/32 :goto_182

    nop

    :goto_6f
    move/from16 v0, v27

    goto/32 :goto_19b

    nop

    :goto_70
    move/from16 v7, v29

    goto/32 :goto_1b2

    nop

    :goto_71
    const/4 v2, -0x1

    goto/32 :goto_9

    nop

    :goto_72
    iget v4, v3, Lcom/google/android/flexbox/FlexLine;->mMainSize:I

    goto/32 :goto_f0

    nop

    :goto_73
    if-nez v4, :cond_7

    goto/32 :goto_14

    :cond_7
    goto/32 :goto_13

    nop

    :goto_74
    move/from16 v8, v27

    :goto_75
    goto/32 :goto_18

    nop

    :goto_76
    iget v4, v3, Lcom/google/android/flexbox/FlexLine;->mMainSize:I

    goto/32 :goto_63

    nop

    :goto_77
    move v0, v12

    :goto_78
    goto/32 :goto_d4

    nop

    :goto_79
    add-int/2addr v4, v8

    goto/32 :goto_a7

    nop

    :goto_7a
    move v9, v1

    goto/32 :goto_d2

    nop

    :goto_7b
    check-cast v11, Lcom/google/android/flexbox/FlexItem;

    goto/32 :goto_60

    nop

    :goto_7c
    const/16 v19, 0x0

    goto/32 :goto_141

    nop

    :goto_7d
    invoke-direct {v10, v14, v3, v6, v12}, Lcom/google/android/flexbox/FlexboxHelper;->addFlexLine(Ljava/util/List;Lcom/google/android/flexbox/FlexLine;II)V

    goto/32 :goto_f

    nop

    :goto_7e
    iput v6, v3, Lcom/google/android/flexbox/FlexLine;->mFirstIndex:I

    goto/32 :goto_18d

    nop

    :goto_7f
    iput v6, v3, Lcom/google/android/flexbox/FlexLine;->mFirstIndex:I

    goto/32 :goto_33

    nop

    :goto_80
    move-object/from16 v0, p1

    goto/32 :goto_132

    nop

    :goto_81
    move/from16 v2, v26

    goto/32 :goto_144

    nop

    :goto_82
    move/from16 v12, p4

    goto/32 :goto_23

    nop

    :goto_83
    const/4 v4, -0x1

    goto/32 :goto_147

    nop

    :goto_84
    move/from16 v21, v1

    goto/32 :goto_fa

    nop

    :goto_85
    iget-object v1, v3, Lcom/google/android/flexbox/FlexLine;->mIndicesAlignSelfStretch:Ljava/util/List;

    goto/32 :goto_a5

    nop

    :goto_86
    invoke-direct {v10, v11, v15}, Lcom/google/android/flexbox/FlexboxHelper;->getFlexItemSizeMain(Lcom/google/android/flexbox/FlexItem;Z)I

    move-result v1

    goto/32 :goto_98

    nop

    :goto_87
    if-gt v4, v12, :cond_8

    goto/32 :goto_133

    :cond_8
    goto/32 :goto_149

    nop

    :goto_88
    const/4 v6, -0x1

    goto/32 :goto_aa

    nop

    :goto_89
    add-int/2addr v8, v1

    goto/32 :goto_116

    nop

    :goto_8a
    iget v4, v3, Lcom/google/android/flexbox/FlexLine;->mMaxBaseline:I

    goto/32 :goto_ad

    nop

    :goto_8b
    add-int/2addr v1, v8

    goto/32 :goto_4e

    nop

    :goto_8c
    move/from16 v1, v18

    goto/32 :goto_6f

    nop

    :goto_8d
    invoke-virtual {v3}, Lcom/google/android/flexbox/FlexLine;->getCrossSize()I

    move-result v4

    goto/32 :goto_11

    nop

    :goto_8e
    add-int/2addr v9, v2

    goto/32 :goto_172

    nop

    :goto_8f
    if-nez v15, :cond_9

    goto/32 :goto_e6

    :cond_9
    goto/32 :goto_198

    nop

    :goto_90
    move/from16 v2, v25

    :goto_91
    goto/32 :goto_59

    nop

    :goto_92
    iget-boolean v4, v3, Lcom/google/android/flexbox/FlexLine;->mAnyItemsHaveFlexShrink:Z

    goto/32 :goto_ac

    nop

    :goto_93
    iput v2, v3, Lcom/google/android/flexbox/FlexLine;->mMaxBaseline:I

    goto/32 :goto_46

    nop

    :goto_94
    invoke-direct {v10, v2, v6}, Lcom/google/android/flexbox/FlexboxHelper;->checkSizeConstraints(Landroid/view/View;I)V

    goto/32 :goto_13c

    nop

    :goto_95
    invoke-direct {v10, v11, v14}, Lcom/google/android/flexbox/FlexboxHelper;->getFlexItemMarginStartMain(Lcom/google/android/flexbox/FlexItem;Z)I

    move-result v24

    goto/32 :goto_2b

    nop

    :goto_96
    move-object v6, v11

    goto/32 :goto_c7

    nop

    :goto_97
    invoke-static {v4, v1}, Landroid/view/View;->combineMeasuredStates(II)I

    move-result v19

    goto/32 :goto_72

    nop

    :goto_98
    invoke-interface {v11}, Lcom/google/android/flexbox/FlexItem;->getFlexBasisPercent()F

    move-result v4

    goto/32 :goto_184

    nop

    :goto_99
    invoke-direct {v10, v2, v6}, Lcom/google/android/flexbox/FlexboxHelper;->checkSizeConstraints(Landroid/view/View;I)V

    :goto_9a
    goto/32 :goto_1c

    nop

    :goto_9b
    iput v1, v3, Lcom/google/android/flexbox/FlexLine;->mItemCount:I

    goto/32 :goto_10f

    nop

    :goto_9c
    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v4

    goto/32 :goto_cc

    nop

    :goto_9d
    goto/16 :goto_78

    :goto_9e
    goto/32 :goto_77

    nop

    :goto_9f
    invoke-static {v4, v1}, Ljava/lang/Math;->max(II)I

    move-result v4

    goto/32 :goto_45

    nop

    :goto_a0
    if-eqz v5, :cond_a

    goto/32 :goto_169

    :cond_a
    goto/32 :goto_1a8

    nop

    :goto_a1
    move/from16 v12, p4

    :goto_a2
    goto/32 :goto_87

    nop

    :goto_a3
    const/4 v9, -0x1

    goto/32 :goto_14c

    nop

    :goto_a4
    invoke-direct {v10, v11, v14}, Lcom/google/android/flexbox/FlexboxHelper;->getFlexItemMarginEndCross(Lcom/google/android/flexbox/FlexItem;Z)I

    move-result v23

    goto/32 :goto_40

    nop

    :goto_a5
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    goto/32 :goto_163

    nop

    :goto_a6
    move/from16 v14, v25

    goto/32 :goto_ff

    nop

    :goto_a7
    invoke-direct {v10, v11, v15}, Lcom/google/android/flexbox/FlexboxHelper;->getFlexItemMarginEndCross(Lcom/google/android/flexbox/FlexItem;Z)I

    move-result v8

    goto/32 :goto_1a7

    nop

    :goto_a8
    move/from16 v5, v27

    :goto_a9
    goto/32 :goto_16

    nop

    :goto_aa
    if-eq v14, v6, :cond_b

    goto/32 :goto_1

    :cond_b
    goto/32 :goto_52

    nop

    :goto_ab
    invoke-interface {v11}, Lcom/google/android/flexbox/FlexItem;->getMarginBottom()I

    move-result v2

    goto/32 :goto_25

    nop

    :goto_ac
    invoke-interface {v11}, Lcom/google/android/flexbox/FlexItem;->getFlexShrink()F

    move-result v8

    goto/32 :goto_4f

    nop

    :goto_ad
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v8

    goto/32 :goto_30

    nop

    :goto_ae
    move-object/from16 v0, p1

    :goto_af
    goto/32 :goto_15a

    nop

    :goto_b0
    invoke-direct {v10, v2, v15}, Lcom/google/android/flexbox/FlexboxHelper;->getViewMeasuredSizeCross(Landroid/view/View;Z)I

    move-result v4

    goto/32 :goto_1b

    nop

    :goto_b1
    const/high16 v4, 0x40000000    # 2.0f

    goto/32 :goto_4a

    nop

    :goto_b2
    move-object/from16 v10, p0

    goto/32 :goto_4d

    nop

    :goto_b3
    invoke-interface {v11}, Lcom/google/android/flexbox/FlexItem;->getMarginLeft()I

    move-result v7

    goto/32 :goto_12b

    nop

    :goto_b4
    const/4 v11, 0x1

    goto/32 :goto_123

    nop

    :goto_b5
    goto/16 :goto_d1

    :goto_b6
    goto/32 :goto_d0

    nop

    :goto_b7
    sub-int/2addr v8, v2

    goto/32 :goto_ab

    nop

    :goto_b8
    add-int/2addr v4, v8

    goto/32 :goto_39

    nop

    :goto_b9
    add-int/2addr v0, v1

    goto/32 :goto_12c

    nop

    :goto_ba
    move v4, v12

    goto/32 :goto_a1

    nop

    :goto_bb
    goto/16 :goto_16c

    :goto_bc
    goto/32 :goto_16b

    nop

    :goto_bd
    const/4 v9, 0x0

    goto/32 :goto_125

    nop

    :goto_be
    if-ne v4, v8, :cond_c

    goto/32 :goto_15e

    :cond_c
    goto/32 :goto_114

    nop

    :goto_bf
    invoke-direct {v10, v11, v14}, Lcom/google/android/flexbox/FlexboxHelper;->getFlexItemMarginEndMain(Lcom/google/android/flexbox/FlexItem;Z)I

    move-result v26

    goto/32 :goto_31

    nop

    :goto_c0
    invoke-interface {v7}, Lcom/google/android/flexbox/FlexContainer;->getPaddingRight()I

    move-result v7

    goto/32 :goto_54

    nop

    :goto_c1
    invoke-direct {v10, v6, v4, v1, v5}, Lcom/google/android/flexbox/FlexboxHelper;->updateMeasureCache(IIILandroid/view/View;)V

    goto/32 :goto_cd

    nop

    :goto_c2
    add-float/2addr v4, v8

    goto/32 :goto_145

    nop

    :goto_c3
    add-int/2addr v2, v0

    goto/32 :goto_118

    nop

    :goto_c4
    if-ge v4, v8, :cond_d

    goto/32 :goto_3c

    :cond_d
    goto/32 :goto_178

    nop

    :goto_c5
    invoke-interface {v11}, Lcom/google/android/flexbox/FlexItem;->getMarginTop()I

    move-result v3

    goto/32 :goto_119

    nop

    :goto_c6
    invoke-direct {v10, v15}, Lcom/google/android/flexbox/FlexboxHelper;->getPaddingStartCross(Z)I

    move-result v16

    goto/32 :goto_19d

    nop

    :goto_c7
    move-object v14, v7

    goto/32 :goto_121

    nop

    :goto_c8
    instance-of v4, v5, Landroid/widget/CompoundButton;

    goto/32 :goto_12

    nop

    :goto_c9
    move v14, v8

    goto/32 :goto_fd

    nop

    :goto_ca
    goto/16 :goto_fb

    :goto_cb
    goto/32 :goto_c8

    nop

    :goto_cc
    const/4 v11, 0x1

    goto/32 :goto_5d

    nop

    :goto_cd
    move v9, v1

    :goto_ce
    goto/32 :goto_186

    nop

    :goto_cf
    invoke-direct {v10, v6, v1, v3}, Lcom/google/android/flexbox/FlexboxHelper;->isLastFlexItem(IILcom/google/android/flexbox/FlexLine;)Z

    move-result v4

    goto/32 :goto_73

    nop

    :goto_d0
    move/from16 v8, v27

    :goto_d1
    goto/32 :goto_57

    nop

    :goto_d2
    const/4 v14, 0x0

    goto/32 :goto_1a9

    nop

    :goto_d3
    move v5, v13

    goto/32 :goto_4c

    nop

    :goto_d4
    if-nez v15, :cond_e

    goto/32 :goto_108

    :cond_e
    goto/32 :goto_191

    nop

    :goto_d5
    move/from16 v29, v14

    goto/32 :goto_134

    nop

    :goto_d6
    return-void

    :goto_d7
    move/from16 v22, v0

    goto/32 :goto_5

    nop

    :goto_d8
    invoke-direct {v10, v11, v14}, Lcom/google/android/flexbox/FlexboxHelper;->getFlexItemSizeCross(Lcom/google/android/flexbox/FlexItem;Z)I

    move-result v8

    goto/32 :goto_68

    nop

    :goto_d9
    iput-boolean v4, v3, Lcom/google/android/flexbox/FlexLine;->mAnyItemsHaveFlexShrink:Z

    goto/32 :goto_1ac

    nop

    :goto_da
    move/from16 v5, p3

    goto/32 :goto_107

    nop

    :goto_db
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v8

    goto/32 :goto_16e

    nop

    :goto_dc
    invoke-interface {v1, v5, v2, v3}, Lcom/google/android/flexbox/FlexContainer;->getChildHeightMeasureSpec(III)I

    move-result v1

    goto/32 :goto_102

    nop

    :goto_dd
    move v6, v13

    goto/32 :goto_49

    nop

    :goto_de
    new-instance v0, Ljava/util/ArrayList;

    goto/32 :goto_a

    nop

    :goto_df
    iget v4, v3, Lcom/google/android/flexbox/FlexLine;->mCrossSize:I

    goto/32 :goto_9f

    nop

    :goto_e0
    invoke-direct {v10, v11, v14}, Lcom/google/android/flexbox/FlexboxHelper;->getFlexItemMarginStartCross(Lcom/google/android/flexbox/FlexItem;Z)I

    move-result v23

    goto/32 :goto_15c

    nop

    :goto_e1
    move v12, v1

    goto/32 :goto_a6

    nop

    :goto_e2
    move v6, v13

    goto/32 :goto_1a5

    nop

    :goto_e3
    const/16 v27, 0x0

    goto/32 :goto_4b

    nop

    :goto_e4
    add-int/lit8 v0, v20, 0x1

    goto/32 :goto_1b0

    nop

    :goto_e5
    goto/16 :goto_91

    :goto_e6
    goto/32 :goto_90

    nop

    :goto_e7
    move/from16 v24, v8

    goto/32 :goto_d

    nop

    :goto_e8
    add-int v14, v14, v26

    goto/32 :goto_181

    nop

    :goto_e9
    const/4 v8, 0x1

    goto/32 :goto_158

    nop

    :goto_ea
    invoke-virtual {v5, v1, v4}, Landroid/view/View;->measure(II)V

    goto/32 :goto_1ae

    nop

    :goto_eb
    invoke-direct {v10, v11, v15}, Lcom/google/android/flexbox/FlexboxHelper;->getFlexItemMarginStartMain(Lcom/google/android/flexbox/FlexItem;Z)I

    move-result v8

    goto/32 :goto_8b

    nop

    :goto_ec
    move/from16 v25, v1

    goto/32 :goto_156

    nop

    :goto_ed
    move-object v14, v5

    goto/32 :goto_e3

    nop

    :goto_ee
    add-int v14, v14, v26

    goto/32 :goto_126

    nop

    :goto_ef
    iput v4, v3, Lcom/google/android/flexbox/FlexLine;->mTotalFlexShrink:F

    goto/32 :goto_1e

    nop

    :goto_f0
    invoke-direct {v10, v5, v15}, Lcom/google/android/flexbox/FlexboxHelper;->getViewMeasuredSizeMain(Landroid/view/View;Z)I

    move-result v1

    goto/32 :goto_eb

    nop

    :goto_f1
    invoke-interface {v4, v2, v6, v0, v3}, Lcom/google/android/flexbox/FlexContainer;->onNewFlexItemAdded(Landroid/view/View;IILcom/google/android/flexbox/FlexLine;)V

    goto/32 :goto_b0

    nop

    :goto_f2
    move v1, v2

    goto/32 :goto_44

    nop

    :goto_f3
    iget-object v1, v10, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    goto/32 :goto_1a2

    nop

    :goto_f4
    add-int/lit8 v5, v13, -0x1

    goto/32 :goto_3d

    nop

    :goto_f5
    invoke-direct {v10, v11, v14}, Lcom/google/android/flexbox/FlexboxHelper;->getFlexItemMarginStartMain(Lcom/google/android/flexbox/FlexItem;Z)I

    move-result v9

    goto/32 :goto_8e

    nop

    :goto_f6
    invoke-interface {v11}, Lcom/google/android/flexbox/FlexItem;->getFlexShrink()F

    move-result v8

    goto/32 :goto_5c

    nop

    :goto_f7
    iget-object v3, v10, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    goto/32 :goto_135

    nop

    :goto_f8
    invoke-interface {v11}, Lcom/google/android/flexbox/FlexItem;->getMarginTop()I

    move-result v8

    goto/32 :goto_171

    nop

    :goto_f9
    cmpl-float v4, v4, v26

    goto/32 :goto_56

    nop

    :goto_fa
    move v0, v4

    :goto_fb
    goto/32 :goto_3a

    nop

    :goto_fc
    iget-object v1, v10, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    goto/32 :goto_38

    nop

    :goto_fd
    move/from16 v8, v24

    goto/32 :goto_13f

    nop

    :goto_fe
    move/from16 v24, v8

    goto/32 :goto_2f

    nop

    :goto_ff
    move-object v1, v5

    goto/32 :goto_17c

    nop

    :goto_100
    invoke-interface {v11}, Lcom/google/android/flexbox/FlexItem;->getMarginBottom()I

    move-result v3

    goto/32 :goto_2c

    nop

    :goto_101
    const/4 v14, 0x0

    goto/32 :goto_e0

    nop

    :goto_102
    move-object/from16 v2, p7

    goto/32 :goto_146

    nop

    :goto_103
    invoke-interface {v11}, Lcom/google/android/flexbox/FlexItem;->getMarginRight()I

    move-result v7

    goto/32 :goto_14e

    nop

    :goto_104
    invoke-interface {v0}, Lcom/google/android/flexbox/FlexContainer;->isMainAxisDirectionHorizontal()Z

    move-result v15

    goto/32 :goto_16a

    nop

    :goto_105
    move v8, v6

    goto/32 :goto_2e

    nop

    :goto_106
    if-gtz v4, :cond_f

    goto/32 :goto_24

    :cond_f
    goto/32 :goto_9c

    nop

    :goto_107
    goto/16 :goto_9a

    :goto_108
    goto/32 :goto_180

    nop

    :goto_109
    invoke-interface {v4, v13, v14, v9}, Lcom/google/android/flexbox/FlexContainer;->getChildHeightMeasureSpec(III)I

    move-result v4

    goto/32 :goto_ea

    nop

    :goto_10a
    if-nez v0, :cond_10

    goto/32 :goto_19c

    :cond_10
    goto/32 :goto_3f

    nop

    :goto_10b
    if-nez v4, :cond_11

    goto/32 :goto_6c

    :cond_11
    goto/32 :goto_13b

    nop

    :goto_10c
    invoke-static {v1, v4}, Ljava/lang/Math;->max(II)I

    move-result v1

    goto/32 :goto_df

    nop

    :goto_10d
    move/from16 v8, p6

    goto/32 :goto_a3

    nop

    :goto_10e
    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v4

    goto/32 :goto_106

    nop

    :goto_10f
    move/from16 v7, v29

    goto/32 :goto_130

    nop

    :goto_110
    invoke-interface {v1, v6, v5}, Lcom/google/android/flexbox/FlexContainer;->updateViewCache(ILandroid/view/View;)V

    goto/32 :goto_19e

    nop

    :goto_111
    iput-boolean v4, v3, Lcom/google/android/flexbox/FlexLine;->mAnyItemsHaveFlexGrow:Z

    goto/32 :goto_92

    nop

    :goto_112
    if-gtz v13, :cond_12

    goto/32 :goto_3e

    :cond_12
    goto/32 :goto_f4

    nop

    :goto_113
    if-nez v15, :cond_13

    goto/32 :goto_1aa

    :cond_13
    goto/32 :goto_51

    nop

    :goto_114
    iget v4, v3, Lcom/google/android/flexbox/FlexLine;->mMaxBaseline:I

    goto/32 :goto_55

    nop

    :goto_115
    move/from16 v12, p2

    goto/32 :goto_c9

    nop

    :goto_116
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v23

    goto/32 :goto_13e

    nop

    :goto_117
    const/4 v14, -0x1

    goto/32 :goto_96

    nop

    :goto_118
    invoke-interface {v11}, Lcom/google/android/flexbox/FlexItem;->getHeight()I

    move-result v3

    goto/32 :goto_dd

    nop

    :goto_119
    add-int/2addr v2, v3

    goto/32 :goto_100

    nop

    :goto_11a
    move/from16 v5, p3

    goto/32 :goto_127

    nop

    :goto_11b
    move v7, v2

    goto/32 :goto_196

    nop

    :goto_11c
    const/4 v0, 0x0

    goto/32 :goto_7c

    nop

    :goto_11d
    add-int/2addr v8, v9

    goto/32 :goto_b8

    nop

    :goto_11e
    invoke-interface {v11}, Lcom/google/android/flexbox/FlexItem;->getWidth()I

    move-result v7

    goto/32 :goto_21

    nop

    :goto_11f
    move/from16 v26, v9

    goto/32 :goto_d3

    nop

    :goto_120
    invoke-virtual {v2, v3, v1}, Landroid/view/View;->measure(II)V

    goto/32 :goto_94

    nop

    :goto_121
    move v7, v8

    goto/32 :goto_1af

    nop

    :goto_122
    iget v0, v3, Lcom/google/android/flexbox/FlexLine;->mCrossSize:I

    goto/32 :goto_143

    nop

    :goto_123
    const/16 v27, 0x0

    goto/32 :goto_19

    nop

    :goto_124
    invoke-interface {v11}, Lcom/google/android/flexbox/FlexItem;->getFlexGrow()F

    move-result v8

    goto/32 :goto_bd

    nop

    :goto_125
    cmpl-float v8, v8, v9

    goto/32 :goto_170

    nop

    :goto_126
    add-int/2addr v14, v0

    goto/32 :goto_187

    nop

    :goto_127
    iget v0, v3, Lcom/google/android/flexbox/FlexLine;->mItemCount:I

    goto/32 :goto_b9

    nop

    :goto_128
    invoke-static {v4, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    goto/32 :goto_93

    nop

    :goto_129
    int-to-float v1, v8

    goto/32 :goto_1a6

    nop

    :goto_12a
    const/4 v8, 0x2

    goto/32 :goto_be

    nop

    :goto_12b
    add-int/2addr v4, v7

    goto/32 :goto_103

    nop

    :goto_12c
    iput v0, v3, Lcom/google/android/flexbox/FlexLine;->mItemCount:I

    goto/32 :goto_e4

    nop

    :goto_12d
    move/from16 v4, v19

    goto/32 :goto_97

    nop

    :goto_12e
    iput v4, v3, Lcom/google/android/flexbox/FlexLine;->mGoneItemCount:I

    goto/32 :goto_179

    nop

    :goto_12f
    if-ne v8, v9, :cond_14

    goto/32 :goto_177

    :cond_14
    goto/32 :goto_10e

    nop

    :goto_130
    iput v7, v3, Lcom/google/android/flexbox/FlexLine;->mMainSize:I

    goto/32 :goto_7e

    nop

    :goto_131
    invoke-direct {v10, v11, v15}, Lcom/google/android/flexbox/FlexboxHelper;->getFlexItemMarginStartMain(Lcom/google/android/flexbox/FlexItem;Z)I

    move-result v9

    goto/32 :goto_157

    nop

    :goto_132
    goto/16 :goto_af

    :goto_133
    goto/32 :goto_18c

    nop

    :goto_134
    const/4 v14, 0x1

    goto/32 :goto_ed

    nop

    :goto_135
    invoke-interface {v3}, Lcom/google/android/flexbox/FlexContainer;->getPaddingBottom()I

    move-result v3

    goto/32 :goto_34

    nop

    :goto_136
    invoke-interface {v4, v12, v14, v1}, Lcom/google/android/flexbox/FlexContainer;->getChildWidthMeasureSpec(III)I

    move-result v1

    goto/32 :goto_50

    nop

    :goto_137
    const/4 v14, 0x1

    goto/32 :goto_95

    nop

    :goto_138
    move/from16 v9, v26

    goto/32 :goto_193

    nop

    :goto_139
    invoke-direct {v10, v14, v3, v5, v12}, Lcom/google/android/flexbox/FlexboxHelper;->addFlexLine(Ljava/util/List;Lcom/google/android/flexbox/FlexLine;II)V

    goto/32 :goto_122

    nop

    :goto_13a
    iget v4, v3, Lcom/google/android/flexbox/FlexLine;->mGoneItemCount:I

    goto/32 :goto_2

    nop

    :goto_13b
    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v8

    goto/32 :goto_6b

    nop

    :goto_13c
    goto/16 :goto_9a

    :goto_13d
    goto/32 :goto_6d

    nop

    :goto_13e
    move v1, v0

    goto/32 :goto_17d

    nop

    :goto_13f
    goto/16 :goto_142

    :goto_140
    goto/32 :goto_1a0

    nop

    :goto_141
    const/16 v20, 0x0

    :goto_142
    goto/32 :goto_152

    nop

    :goto_143
    add-int/2addr v0, v12

    goto/32 :goto_9d

    nop

    :goto_144
    move-object/from16 v28, v3

    goto/32 :goto_27

    nop

    :goto_145
    iput v4, v3, Lcom/google/android/flexbox/FlexLine;->mTotalFlexGrow:F

    goto/32 :goto_15f

    nop

    :goto_146
    move/from16 v3, v30

    goto/32 :goto_120

    nop

    :goto_147
    if-eq v1, v4, :cond_15

    goto/32 :goto_9a

    :cond_15
    goto/32 :goto_f3

    nop

    :goto_148
    add-int v9, v16, v17

    goto/32 :goto_101

    nop

    :goto_149
    if-nez v22, :cond_16

    goto/32 :goto_133

    :cond_16
    goto/32 :goto_36

    nop

    :goto_14a
    iget-object v8, v10, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    goto/32 :goto_f5

    nop

    :goto_14b
    check-cast v4, Landroid/widget/CompoundButton;

    goto/32 :goto_14f

    nop

    :goto_14c
    goto/16 :goto_43

    :goto_14d
    goto/32 :goto_199

    nop

    :goto_14e
    add-int/2addr v4, v7

    goto/32 :goto_29

    nop

    :goto_14f
    invoke-direct {v10, v4}, Lcom/google/android/flexbox/FlexboxHelper;->evaluateMinimumSizeForCompoundButton(Landroid/widget/CompoundButton;)V

    :goto_150
    goto/32 :goto_8

    nop

    :goto_151
    iget v4, v3, Lcom/google/android/flexbox/FlexLine;->mTotalFlexGrow:F

    goto/32 :goto_19a

    nop

    :goto_152
    if-lt v6, v1, :cond_17

    goto/32 :goto_140

    :cond_17
    goto/32 :goto_194

    nop

    :goto_153
    move/from16 v22, v11

    goto/32 :goto_1b5

    nop

    :goto_154
    move/from16 v9, v23

    goto/32 :goto_1f

    nop

    :goto_155
    add-int/2addr v4, v5

    goto/32 :goto_65

    nop

    :goto_156
    const/4 v1, 0x4

    goto/32 :goto_1a3

    nop

    :goto_157
    add-int/2addr v8, v9

    goto/32 :goto_2a

    nop

    :goto_158
    iget-object v4, v10, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    goto/32 :goto_148

    nop

    :goto_159
    invoke-virtual {v5}, Landroid/view/View;->getVisibility()I

    move-result v4

    goto/32 :goto_18a

    nop

    :goto_15a
    iput v4, v0, Lcom/google/android/flexbox/FlexboxHelper$FlexLinesResult;->mChildState:I

    goto/32 :goto_d6

    nop

    :goto_15b
    move/from16 v14, p6

    goto/32 :goto_26

    nop

    :goto_15c
    add-int v9, v9, v23

    goto/32 :goto_a4

    nop

    :goto_15d
    goto/16 :goto_91

    :goto_15e
    goto/32 :goto_8a

    nop

    :goto_15f
    iget v4, v3, Lcom/google/android/flexbox/FlexLine;->mTotalFlexShrink:F

    goto/32 :goto_f6

    nop

    :goto_160
    const/4 v8, 0x1

    goto/32 :goto_66

    nop

    :goto_161
    invoke-interface {v8, v12, v9, v1}, Lcom/google/android/flexbox/FlexContainer;->getChildHeightMeasureSpec(III)I

    move-result v1

    goto/32 :goto_1d

    nop

    :goto_162
    move/from16 v12, p4

    goto/32 :goto_189

    nop

    :goto_163
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_164
    goto/32 :goto_86

    nop

    :goto_165
    iput v2, v3, Lcom/google/android/flexbox/FlexLine;->mMainSize:I

    goto/32 :goto_16d

    nop

    :goto_166
    move-object v7, v14

    goto/32 :goto_138

    nop

    :goto_167
    invoke-direct {v10, v11, v8}, Lcom/google/android/flexbox/FlexboxHelper;->getFlexItemSizeCross(Lcom/google/android/flexbox/FlexItem;Z)I

    move-result v9

    goto/32 :goto_109

    nop

    :goto_168
    goto/16 :goto_14

    :goto_169
    goto/32 :goto_159

    nop

    :goto_16a
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v9

    goto/32 :goto_db

    nop

    :goto_16b
    move-object/from16 v7, p7

    :goto_16c
    goto/32 :goto_69

    nop

    :goto_16d
    iget-object v1, v10, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    goto/32 :goto_5b

    nop

    :goto_16e
    if-eqz p7, :cond_18

    goto/32 :goto_bc

    :cond_18
    goto/32 :goto_de

    nop

    :goto_16f
    move/from16 v12, p2

    goto/32 :goto_188

    nop

    :goto_170
    if-nez v8, :cond_19

    goto/32 :goto_b6

    :cond_19
    goto/32 :goto_48

    nop

    :goto_171
    add-int/2addr v2, v8

    goto/32 :goto_128

    nop

    :goto_172
    invoke-direct {v10, v11, v14}, Lcom/google/android/flexbox/FlexboxHelper;->getFlexItemMarginEndMain(Lcom/google/android/flexbox/FlexItem;Z)I

    move-result v23

    goto/32 :goto_20

    nop

    :goto_173
    move-object/from16 v3, v28

    goto/32 :goto_70

    nop

    :goto_174
    add-int/2addr v12, v4

    goto/32 :goto_10d

    nop

    :goto_175
    add-int/2addr v4, v8

    goto/32 :goto_10c

    nop

    :goto_176
    goto/16 :goto_3c

    :goto_177
    goto/32 :goto_3b

    nop

    :goto_178
    if-ge v6, v8, :cond_1a

    goto/32 :goto_3c

    :cond_1a
    goto/32 :goto_64

    nop

    :goto_179
    iget v4, v3, Lcom/google/android/flexbox/FlexLine;->mItemCount:I

    goto/32 :goto_155

    nop

    :goto_17a
    add-int/2addr v9, v0

    goto/32 :goto_d8

    nop

    :goto_17b
    iget-object v7, v10, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    goto/32 :goto_c0

    nop

    :goto_17c
    move v14, v2

    goto/32 :goto_81

    nop

    :goto_17d
    move-object/from16 v0, p0

    goto/32 :goto_e1

    nop

    :goto_17e
    move-object/from16 v2, p7

    goto/32 :goto_195

    nop

    :goto_17f
    const/high16 v18, -0x80000000

    goto/32 :goto_d7

    nop

    :goto_180
    move-object/from16 v2, p7

    goto/32 :goto_e2

    nop

    :goto_181
    invoke-direct {v10, v11, v8}, Lcom/google/android/flexbox/FlexboxHelper;->getFlexItemMarginEndCross(Lcom/google/android/flexbox/FlexItem;Z)I

    move-result v26

    goto/32 :goto_ee

    nop

    :goto_182
    iget v4, v4, Lcom/google/android/flexbox/FlexLine;->mLastIndex:I

    goto/32 :goto_c4

    nop

    :goto_183
    move/from16 v6, p5

    goto/32 :goto_7f

    nop

    :goto_184
    const/high16 v26, -0x40800000    # -1.0f

    goto/32 :goto_f9

    nop

    :goto_185
    invoke-direct {v10, v15}, Lcom/google/android/flexbox/FlexboxHelper;->getPaddingEndMain(Z)I

    move-result v2

    goto/32 :goto_c6

    nop

    :goto_186
    iget-object v1, v10, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    goto/32 :goto_110

    nop

    :goto_187
    move/from16 v26, v9

    goto/32 :goto_167

    nop

    :goto_188
    move/from16 v13, p3

    goto/32 :goto_15b

    nop

    :goto_189
    move/from16 v24, v8

    goto/32 :goto_11f

    nop

    :goto_18a
    const/16 v11, 0x8

    goto/32 :goto_1b6

    nop

    :goto_18b
    move/from16 v2, v25

    goto/32 :goto_e5

    nop

    :goto_18c
    move/from16 v20, v0

    goto/32 :goto_84

    nop

    :goto_18d
    move v12, v0

    goto/32 :goto_8c

    nop

    :goto_18e
    if-nez v5, :cond_1b

    goto/32 :goto_14

    :cond_1b
    goto/32 :goto_192

    nop

    :goto_18f
    invoke-virtual {v2, v1, v3}, Landroid/view/View;->measure(II)V

    goto/32 :goto_99

    nop

    :goto_190
    iput v2, v3, Lcom/google/android/flexbox/FlexLine;->mMaxBaseline:I

    goto/32 :goto_18b

    nop

    :goto_191
    invoke-interface {v11}, Lcom/google/android/flexbox/FlexItem;->getHeight()I

    move-result v1

    goto/32 :goto_71

    nop

    :goto_192
    invoke-direct {v10, v7, v3, v6, v0}, Lcom/google/android/flexbox/FlexboxHelper;->addFlexLine(Ljava/util/List;Lcom/google/android/flexbox/FlexLine;II)V

    goto/32 :goto_168

    nop

    :goto_193
    move-object/from16 v11, p1

    goto/32 :goto_115

    nop

    :goto_194
    iget-object v5, v10, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    goto/32 :goto_4

    nop

    :goto_195
    move v6, v13

    goto/32 :goto_173

    nop

    :goto_196
    move v2, v1

    goto/32 :goto_ca

    nop

    :goto_197
    mul-float/2addr v1, v4

    goto/32 :goto_6

    nop

    :goto_198
    iget-object v4, v10, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    goto/32 :goto_5f

    nop

    :goto_199
    move/from16 v8, p6

    goto/32 :goto_42

    nop

    :goto_19a
    invoke-interface {v11}, Lcom/google/android/flexbox/FlexItem;->getFlexGrow()F

    move-result v8

    goto/32 :goto_c2

    nop

    :goto_19b
    goto/16 :goto_1b1

    :goto_19c
    goto/32 :goto_17e

    nop

    :goto_19d
    invoke-direct {v10, v15}, Lcom/google/android/flexbox/FlexboxHelper;->getPaddingEndCross(Z)I

    move-result v17

    goto/32 :goto_5a

    nop

    :goto_19e
    invoke-direct {v10, v5, v6}, Lcom/google/android/flexbox/FlexboxHelper;->checkSizeConstraints(Landroid/view/View;I)V

    goto/32 :goto_1ad

    nop

    :goto_19f
    add-int v14, v16, v17

    goto/32 :goto_fe

    nop

    :goto_1a0
    move/from16 v4, v19

    goto/32 :goto_ae

    nop

    :goto_1a1
    move/from16 v5, p3

    goto/32 :goto_c

    nop

    :goto_1a2
    invoke-interface {v1}, Lcom/google/android/flexbox/FlexContainer;->getPaddingLeft()I

    move-result v4

    goto/32 :goto_17b

    nop

    :goto_1a3
    if-eq v4, v1, :cond_1c

    goto/32 :goto_164

    :cond_1c
    goto/32 :goto_85

    nop

    :goto_1a4
    const/4 v11, 0x1

    goto/32 :goto_176

    nop

    :goto_1a5
    move/from16 v3, v30

    goto/32 :goto_1a1

    nop

    :goto_1a6
    invoke-interface {v11}, Lcom/google/android/flexbox/FlexItem;->getFlexBasisPercent()F

    move-result v4

    goto/32 :goto_197

    nop

    :goto_1a7
    add-int/2addr v4, v8

    goto/32 :goto_e

    nop

    :goto_1a8
    invoke-direct {v10, v6, v1, v3}, Lcom/google/android/flexbox/FlexboxHelper;->isLastFlexItem(IILcom/google/android/flexbox/FlexLine;)Z

    move-result v5

    goto/32 :goto_18e

    nop

    :goto_1a9
    goto/16 :goto_ce

    :goto_1aa
    goto/32 :goto_e7

    nop

    :goto_1ab
    if-nez v8, :cond_1d

    goto/32 :goto_67

    :cond_1d
    goto/32 :goto_160

    nop

    :goto_1ac
    iget-object v4, v10, Lcom/google/android/flexbox/FlexboxHelper;->mIndexToFlexLine:[I

    goto/32 :goto_10b

    nop

    :goto_1ad
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredState()I

    move-result v1

    goto/32 :goto_12d

    nop

    :goto_1ae
    invoke-direct {v10, v6, v1, v4, v5}, Lcom/google/android/flexbox/FlexboxHelper;->updateMeasureCache(IIILandroid/view/View;)V

    goto/32 :goto_7a

    nop

    :goto_1af
    move v13, v8

    goto/32 :goto_41

    nop

    :goto_1b0
    move/from16 v1, v21

    :goto_1b1
    goto/32 :goto_61

    nop

    :goto_1b2
    const/4 v1, 0x1

    goto/32 :goto_11a

    nop

    :goto_1b3
    const/4 v0, 0x0

    :goto_1b4
    goto/32 :goto_2d

    nop

    :goto_1b5
    move v4, v12

    goto/32 :goto_82

    nop

    :goto_1b6
    if-eq v4, v11, :cond_1e

    goto/32 :goto_cb

    :cond_1e
    goto/32 :goto_13a

    nop
.end method

.method calculateHorizontalFlexLines(Lcom/google/android/flexbox/FlexboxHelper$FlexLinesResult;II)V
    .locals 8

    goto/32 :goto_3

    nop

    :goto_0
    move v3, p3

    goto/32 :goto_7

    nop

    :goto_1
    move-object v1, p1

    goto/32 :goto_5

    nop

    :goto_2
    const/4 v5, 0x0

    goto/32 :goto_9

    nop

    :goto_3
    const v4, 0x7fffffff

    goto/32 :goto_2

    nop

    :goto_4
    move-object v0, p0

    goto/32 :goto_1

    nop

    :goto_5
    move v2, p2

    goto/32 :goto_0

    nop

    :goto_6
    return-void

    :goto_7
    invoke-virtual/range {v0 .. v7}, Lcom/google/android/flexbox/FlexboxHelper;->calculateFlexLines(Lcom/google/android/flexbox/FlexboxHelper$FlexLinesResult;IIIIILjava/util/List;)V

    goto/32 :goto_6

    nop

    :goto_8
    const/4 v7, 0x0

    goto/32 :goto_4

    nop

    :goto_9
    const/4 v6, -0x1

    goto/32 :goto_8

    nop
.end method

.method calculateHorizontalFlexLines(Lcom/google/android/flexbox/FlexboxHelper$FlexLinesResult;IIIILjava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/flexbox/FlexboxHelper$FlexLinesResult;",
            "IIII",
            "Ljava/util/List<",
            "Lcom/google/android/flexbox/FlexLine;",
            ">;)V"
        }
    .end annotation

    goto/32 :goto_2

    nop

    :goto_0
    move v3, p3

    goto/32 :goto_7

    nop

    :goto_1
    move-object v1, p1

    goto/32 :goto_5

    nop

    :goto_2
    const/4 v6, -0x1

    goto/32 :goto_8

    nop

    :goto_3
    invoke-virtual/range {v0 .. v7}, Lcom/google/android/flexbox/FlexboxHelper;->calculateFlexLines(Lcom/google/android/flexbox/FlexboxHelper$FlexLinesResult;IIIIILjava/util/List;)V

    goto/32 :goto_9

    nop

    :goto_4
    move v5, p5

    goto/32 :goto_6

    nop

    :goto_5
    move v2, p2

    goto/32 :goto_0

    nop

    :goto_6
    move-object v7, p6

    goto/32 :goto_3

    nop

    :goto_7
    move v4, p4

    goto/32 :goto_4

    nop

    :goto_8
    move-object v0, p0

    goto/32 :goto_1

    nop

    :goto_9
    return-void
.end method

.method calculateHorizontalFlexLinesToIndex(Lcom/google/android/flexbox/FlexboxHelper$FlexLinesResult;IIIILjava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/flexbox/FlexboxHelper$FlexLinesResult;",
            "IIII",
            "Ljava/util/List<",
            "Lcom/google/android/flexbox/FlexLine;",
            ">;)V"
        }
    .end annotation

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    const/4 v5, 0x0

    goto/32 :goto_2

    nop

    :goto_2
    move-object v0, p0

    goto/32 :goto_4

    nop

    :goto_3
    move v4, p4

    goto/32 :goto_9

    nop

    :goto_4
    move-object v1, p1

    goto/32 :goto_8

    nop

    :goto_5
    move-object v7, p6

    goto/32 :goto_7

    nop

    :goto_6
    move v3, p3

    goto/32 :goto_3

    nop

    :goto_7
    invoke-virtual/range {v0 .. v7}, Lcom/google/android/flexbox/FlexboxHelper;->calculateFlexLines(Lcom/google/android/flexbox/FlexboxHelper$FlexLinesResult;IIIIILjava/util/List;)V

    goto/32 :goto_0

    nop

    :goto_8
    move v2, p2

    goto/32 :goto_6

    nop

    :goto_9
    move v6, p5

    goto/32 :goto_5

    nop
.end method

.method calculateVerticalFlexLines(Lcom/google/android/flexbox/FlexboxHelper$FlexLinesResult;II)V
    .locals 8

    goto/32 :goto_9

    nop

    :goto_0
    invoke-virtual/range {v0 .. v7}, Lcom/google/android/flexbox/FlexboxHelper;->calculateFlexLines(Lcom/google/android/flexbox/FlexboxHelper$FlexLinesResult;IIIIILjava/util/List;)V

    goto/32 :goto_1

    nop

    :goto_1
    return-void

    :goto_2
    const/4 v7, 0x0

    goto/32 :goto_7

    nop

    :goto_3
    move-object v1, p1

    goto/32 :goto_4

    nop

    :goto_4
    move v2, p3

    goto/32 :goto_8

    nop

    :goto_5
    const/4 v5, 0x0

    goto/32 :goto_6

    nop

    :goto_6
    const/4 v6, -0x1

    goto/32 :goto_2

    nop

    :goto_7
    move-object v0, p0

    goto/32 :goto_3

    nop

    :goto_8
    move v3, p2

    goto/32 :goto_0

    nop

    :goto_9
    const v4, 0x7fffffff

    goto/32 :goto_5

    nop
.end method

.method calculateVerticalFlexLines(Lcom/google/android/flexbox/FlexboxHelper$FlexLinesResult;IIIILjava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/flexbox/FlexboxHelper$FlexLinesResult;",
            "IIII",
            "Ljava/util/List<",
            "Lcom/google/android/flexbox/FlexLine;",
            ">;)V"
        }
    .end annotation

    goto/32 :goto_1

    nop

    :goto_0
    move v3, p2

    goto/32 :goto_2

    nop

    :goto_1
    const/4 v6, -0x1

    goto/32 :goto_9

    nop

    :goto_2
    move v4, p4

    goto/32 :goto_7

    nop

    :goto_3
    move-object v7, p6

    goto/32 :goto_5

    nop

    :goto_4
    move v2, p3

    goto/32 :goto_0

    nop

    :goto_5
    invoke-virtual/range {v0 .. v7}, Lcom/google/android/flexbox/FlexboxHelper;->calculateFlexLines(Lcom/google/android/flexbox/FlexboxHelper$FlexLinesResult;IIIIILjava/util/List;)V

    goto/32 :goto_8

    nop

    :goto_6
    move-object v1, p1

    goto/32 :goto_4

    nop

    :goto_7
    move v5, p5

    goto/32 :goto_3

    nop

    :goto_8
    return-void

    :goto_9
    move-object v0, p0

    goto/32 :goto_6

    nop
.end method

.method calculateVerticalFlexLinesToIndex(Lcom/google/android/flexbox/FlexboxHelper$FlexLinesResult;IIIILjava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/flexbox/FlexboxHelper$FlexLinesResult;",
            "IIII",
            "Ljava/util/List<",
            "Lcom/google/android/flexbox/FlexLine;",
            ">;)V"
        }
    .end annotation

    goto/32 :goto_1

    nop

    :goto_0
    move-object v0, p0

    goto/32 :goto_2

    nop

    :goto_1
    const/4 v5, 0x0

    goto/32 :goto_0

    nop

    :goto_2
    move-object v1, p1

    goto/32 :goto_4

    nop

    :goto_3
    move v3, p2

    goto/32 :goto_5

    nop

    :goto_4
    move v2, p3

    goto/32 :goto_3

    nop

    :goto_5
    move v4, p4

    goto/32 :goto_9

    nop

    :goto_6
    move-object v7, p6

    goto/32 :goto_7

    nop

    :goto_7
    invoke-virtual/range {v0 .. v7}, Lcom/google/android/flexbox/FlexboxHelper;->calculateFlexLines(Lcom/google/android/flexbox/FlexboxHelper$FlexLinesResult;IIIIILjava/util/List;)V

    goto/32 :goto_8

    nop

    :goto_8
    return-void

    :goto_9
    move v6, p5

    goto/32 :goto_6

    nop
.end method

.method clearFlexLines(Ljava/util/List;I)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/google/android/flexbox/FlexLine;",
            ">;I)V"
        }
    .end annotation

    goto/32 :goto_9

    nop

    :goto_0
    invoke-static {p1, p2, v0, v1, v2}, Ljava/util/Arrays;->fill([JIIJ)V

    :goto_1
    goto/32 :goto_21

    nop

    :goto_2
    invoke-static {p1, v1}, Ljava/util/Arrays;->fill([II)V

    goto/32 :goto_3

    nop

    :goto_3
    goto :goto_14

    :goto_4
    goto/32 :goto_13

    nop

    :goto_5
    if-gt p2, v0, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_2

    nop

    :goto_6
    invoke-static {p1, v1, v2}, Ljava/util/Arrays;->fill([JJ)V

    goto/32 :goto_16

    nop

    :goto_7
    if-ge v2, v0, :cond_1

    goto/32 :goto_1b

    :cond_1
    goto/32 :goto_1e

    nop

    :goto_8
    add-int/lit8 v0, v0, -0x1

    goto/32 :goto_d

    nop

    :goto_9
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxHelper;->mIndexToFlexLine:[I

    goto/32 :goto_e

    nop

    :goto_a
    iget-object p1, p0, Lcom/google/android/flexbox/FlexboxHelper;->mIndexToFlexLine:[I

    goto/32 :goto_10

    nop

    :goto_b
    if-eq v0, v1, :cond_2

    goto/32 :goto_19

    :cond_2
    goto/32 :goto_18

    nop

    :goto_c
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    goto/32 :goto_1f

    nop

    :goto_d
    const-wide/16 v1, 0x0

    goto/32 :goto_f

    nop

    :goto_e
    aget v0, v0, p2

    goto/32 :goto_1d

    nop

    :goto_f
    if-gt p2, v0, :cond_3

    goto/32 :goto_17

    :cond_3
    goto/32 :goto_6

    nop

    :goto_10
    array-length v0, p1

    goto/32 :goto_12

    nop

    :goto_11
    iget-object p1, p0, Lcom/google/android/flexbox/FlexboxHelper;->mMeasureSpecCache:[J

    goto/32 :goto_15

    nop

    :goto_12
    add-int/lit8 v0, v0, -0x1

    goto/32 :goto_5

    nop

    :goto_13
    invoke-static {p1, p2, v0, v1}, Ljava/util/Arrays;->fill([IIII)V

    :goto_14
    goto/32 :goto_11

    nop

    :goto_15
    array-length v0, p1

    goto/32 :goto_8

    nop

    :goto_16
    goto :goto_1

    :goto_17
    goto/32 :goto_0

    nop

    :goto_18
    const/4 v0, 0x0

    :goto_19
    goto/32 :goto_c

    nop

    :goto_1a
    goto :goto_20

    :goto_1b
    goto/32 :goto_a

    nop

    :goto_1c
    add-int/lit8 v2, v2, -0x1

    goto/32 :goto_1a

    nop

    :goto_1d
    const/4 v1, -0x1

    goto/32 :goto_b

    nop

    :goto_1e
    invoke-interface {p1, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto/32 :goto_1c

    nop

    :goto_1f
    add-int/lit8 v2, v2, -0x1

    :goto_20
    goto/32 :goto_7

    nop

    :goto_21
    return-void
.end method

.method createReorderedIndices(Landroid/util/SparseIntArray;)[I
    .locals 2

    goto/32 :goto_2

    nop

    :goto_0
    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/flexbox/FlexboxHelper;->sortOrdersIntoReorderedIndices(ILjava/util/List;Landroid/util/SparseIntArray;)[I

    move-result-object p1

    goto/32 :goto_4

    nop

    :goto_1
    invoke-interface {v0}, Lcom/google/android/flexbox/FlexContainer;->getFlexItemCount()I

    move-result v0

    goto/32 :goto_3

    nop

    :goto_2
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    goto/32 :goto_1

    nop

    :goto_3
    invoke-direct {p0, v0}, Lcom/google/android/flexbox/FlexboxHelper;->createOrders(I)Ljava/util/List;

    move-result-object v1

    goto/32 :goto_0

    nop

    :goto_4
    return-object p1
.end method

.method createReorderedIndices(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Landroid/util/SparseIntArray;)[I
    .locals 4

    goto/32 :goto_f

    nop

    :goto_0
    check-cast p3, Lcom/google/android/flexbox/FlexItem;

    goto/32 :goto_10

    nop

    :goto_1
    iput p2, v2, Lcom/google/android/flexbox/FlexboxHelper$Order;->index:I

    :goto_2
    goto/32 :goto_27

    nop

    :goto_3
    goto :goto_2

    :goto_4
    goto/32 :goto_16

    nop

    :goto_5
    goto :goto_a

    :goto_6
    goto/32 :goto_9

    nop

    :goto_7
    if-lt p2, p1, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_1

    nop

    :goto_8
    iput p3, p1, Lcom/google/android/flexbox/FlexboxHelper$Order;->index:I

    goto/32 :goto_23

    nop

    :goto_9
    iput v0, v2, Lcom/google/android/flexbox/FlexboxHelper$Order;->index:I

    :goto_a
    goto/32 :goto_c

    nop

    :goto_b
    add-int/2addr p3, v3

    goto/32 :goto_8

    nop

    :goto_c
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/32 :goto_11

    nop

    :goto_d
    const/4 p1, -0x1

    goto/32 :goto_29

    nop

    :goto_e
    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    goto/32 :goto_28

    nop

    :goto_f
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    goto/32 :goto_1f

    nop

    :goto_10
    invoke-interface {p3}, Lcom/google/android/flexbox/FlexItem;->getOrder()I

    move-result p1

    goto/32 :goto_12

    nop

    :goto_11
    add-int/2addr v0, v3

    goto/32 :goto_1d

    nop

    :goto_12
    iput p1, v2, Lcom/google/android/flexbox/FlexboxHelper$Order;->order:I

    goto/32 :goto_2b

    nop

    :goto_13
    if-eq p2, v0, :cond_1

    goto/32 :goto_1a

    :cond_1
    goto/32 :goto_19

    nop

    :goto_14
    if-nez p1, :cond_2

    goto/32 :goto_2c

    :cond_2
    goto/32 :goto_15

    nop

    :goto_15
    instance-of p1, p3, Lcom/google/android/flexbox/FlexItem;

    goto/32 :goto_2a

    nop

    :goto_16
    iput v0, v2, Lcom/google/android/flexbox/FlexboxHelper$Order;->index:I

    goto/32 :goto_5

    nop

    :goto_17
    invoke-direct {v2, v3}, Lcom/google/android/flexbox/FlexboxHelper$Order;-><init>(Lcom/google/android/flexbox/FlexboxHelper$1;)V

    goto/32 :goto_26

    nop

    :goto_18
    new-instance v2, Lcom/google/android/flexbox/FlexboxHelper$Order;

    goto/32 :goto_24

    nop

    :goto_19
    goto :goto_6

    :goto_1a
    goto/32 :goto_1c

    nop

    :goto_1b
    invoke-interface {p1}, Lcom/google/android/flexbox/FlexContainer;->getFlexItemCount()I

    move-result p1

    goto/32 :goto_7

    nop

    :goto_1c
    iget-object p1, p0, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    goto/32 :goto_1b

    nop

    :goto_1d
    invoke-direct {p0, v0, v1, p4}, Lcom/google/android/flexbox/FlexboxHelper;->sortOrdersIntoReorderedIndices(ILjava/util/List;Landroid/util/SparseIntArray;)[I

    move-result-object p1

    goto/32 :goto_25

    nop

    :goto_1e
    iget p3, p1, Lcom/google/android/flexbox/FlexboxHelper$Order;->index:I

    goto/32 :goto_b

    nop

    :goto_1f
    invoke-interface {v0}, Lcom/google/android/flexbox/FlexContainer;->getFlexItemCount()I

    move-result v0

    goto/32 :goto_20

    nop

    :goto_20
    invoke-direct {p0, v0}, Lcom/google/android/flexbox/FlexboxHelper;->createOrders(I)Ljava/util/List;

    move-result-object v1

    goto/32 :goto_18

    nop

    :goto_21
    iput v3, v2, Lcom/google/android/flexbox/FlexboxHelper$Order;->order:I

    :goto_22
    goto/32 :goto_d

    nop

    :goto_23
    add-int/lit8 p2, p2, 0x1

    goto/32 :goto_3

    nop

    :goto_24
    const/4 v3, 0x0

    goto/32 :goto_17

    nop

    :goto_25
    return-object p1

    :goto_26
    const/4 v3, 0x1

    goto/32 :goto_14

    nop

    :goto_27
    if-lt p2, v0, :cond_3

    goto/32 :goto_a

    :cond_3
    goto/32 :goto_e

    nop

    :goto_28
    check-cast p1, Lcom/google/android/flexbox/FlexboxHelper$Order;

    goto/32 :goto_1e

    nop

    :goto_29
    if-ne p2, p1, :cond_4

    goto/32 :goto_6

    :cond_4
    goto/32 :goto_13

    nop

    :goto_2a
    if-nez p1, :cond_5

    goto/32 :goto_2c

    :cond_5
    goto/32 :goto_0

    nop

    :goto_2b
    goto :goto_22

    :goto_2c
    goto/32 :goto_21

    nop
.end method

.method determineCrossSize(III)V
    .locals 11

    goto/32 :goto_a1

    nop

    :goto_0
    sub-int/2addr p2, p1

    goto/32 :goto_ae

    nop

    :goto_1
    add-float/2addr p3, v6

    :goto_2
    goto/32 :goto_e

    nop

    :goto_3
    check-cast v8, Lcom/google/android/flexbox/FlexLine;

    goto/32 :goto_67

    nop

    :goto_4
    invoke-static {p1}, Ljava/lang/Math;->round(F)I

    move-result v9

    goto/32 :goto_9a

    nop

    :goto_5
    if-gtz v9, :cond_0

    goto/32 :goto_1c

    :cond_0
    goto/32 :goto_3f

    nop

    :goto_6
    sub-int/2addr p2, v3

    goto/32 :goto_7

    nop

    :goto_7
    int-to-float p2, p2

    goto/32 :goto_33

    nop

    :goto_8
    invoke-interface {p1, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/32 :goto_b6

    nop

    :goto_9
    if-lt v2, p2, :cond_1

    goto/32 :goto_a3

    :cond_1
    goto/32 :goto_bb

    nop

    :goto_a
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result p2

    goto/32 :goto_45

    nop

    :goto_b
    if-ltz v7, :cond_2

    goto/32 :goto_2

    :cond_2
    goto/32 :goto_a8

    nop

    :goto_c
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/32 :goto_3b

    nop

    :goto_d
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v8

    goto/32 :goto_81

    nop

    :goto_e
    iput v8, v1, Lcom/google/android/flexbox/FlexLine;->mCrossSize:I

    goto/32 :goto_7e

    nop

    :goto_f
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_10
    goto/32 :goto_60

    nop

    :goto_11
    invoke-interface {p2, p1}, Lcom/google/android/flexbox/FlexContainer;->setFlexLines(Ljava/util/List;)V

    goto/32 :goto_4c

    nop

    :goto_12
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_18

    nop

    :goto_13
    invoke-direct {p0, v0, p2, p1}, Lcom/google/android/flexbox/FlexboxHelper;->constructFlexLinesForAlignContentCenter(Ljava/util/List;II)Ljava/util/List;

    move-result-object p1

    goto/32 :goto_15

    nop

    :goto_14
    goto/16 :goto_a3

    :pswitch_0
    goto/32 :goto_86

    nop

    :goto_15
    invoke-interface {p3, p1}, Lcom/google/android/flexbox/FlexContainer;->setFlexLines(Ljava/util/List;)V

    goto/32 :goto_57

    nop

    :goto_16
    int-to-float v9, v8

    goto/32 :goto_27

    nop

    :goto_17
    if-lt v2, p3, :cond_3

    goto/32 :goto_9f

    :cond_3
    goto/32 :goto_29

    nop

    :goto_18
    const-string p3, "Invalid flex direction: "

    goto/32 :goto_a0

    nop

    :goto_19
    sub-int/2addr p2, p1

    goto/32 :goto_1d

    nop

    :goto_1a
    iget v9, v8, Lcom/google/android/flexbox/FlexLine;->mCrossSize:I

    goto/32 :goto_96

    nop

    :goto_1b
    goto/16 :goto_7b

    :goto_1c
    goto/32 :goto_35

    nop

    :goto_1d
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result p1

    goto/32 :goto_8b

    nop

    :goto_1e
    new-instance p2, Ljava/util/ArrayList;

    goto/32 :goto_8e

    nop

    :goto_1f
    iget-object p1, p0, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    goto/32 :goto_3c

    nop

    :goto_20
    new-instance p1, Ljava/util/ArrayList;

    goto/32 :goto_78

    nop

    :goto_21
    sub-int/2addr v9, v1

    goto/32 :goto_4b

    nop

    :goto_22
    if-ne v2, v8, :cond_4

    goto/32 :goto_74

    :cond_4
    goto/32 :goto_ac

    nop

    :goto_23
    add-float/2addr v7, v9

    goto/32 :goto_af

    nop

    :goto_24
    div-float/2addr p1, p2

    goto/32 :goto_8d

    nop

    :goto_25
    div-int/2addr p2, p1

    goto/32 :goto_20

    nop

    :goto_26
    add-float/2addr p3, v7

    goto/32 :goto_95

    nop

    :goto_27
    sub-float/2addr v7, v9

    goto/32 :goto_26

    nop

    :goto_28
    goto/16 :goto_a3

    :pswitch_1
    goto/32 :goto_8f

    nop

    :goto_29
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    goto/32 :goto_3

    nop

    :goto_2a
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_4
        :pswitch_4
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_5
        :pswitch_6
    .end packed-switch

    :goto_2b
    add-int/lit8 v2, v2, 0x1

    goto/32 :goto_9e

    nop

    :goto_2c
    move p2, p1

    goto/32 :goto_8c

    nop

    :goto_2d
    int-to-float v9, v9

    goto/32 :goto_52

    nop

    :goto_2e
    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_bc

    nop

    :goto_2f
    const/high16 v4, -0x40800000    # -1.0f

    goto/32 :goto_77

    nop

    :goto_30
    const/high16 v6, 0x3f800000    # 1.0f

    packed-switch p3, :pswitch_data_1

    goto/32 :goto_a9

    nop

    :goto_31
    iget-object p1, p0, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    goto/32 :goto_5d

    nop

    :goto_32
    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v8

    goto/32 :goto_16

    nop

    :goto_33
    div-float/2addr p1, p2

    goto/32 :goto_1e

    nop

    :goto_34
    iget v9, v8, Lcom/google/android/flexbox/FlexLine;->mCrossSize:I

    goto/32 :goto_2d

    nop

    :goto_35
    cmpg-float v9, v7, v4

    goto/32 :goto_b3

    nop

    :goto_36
    iput p2, p1, Lcom/google/android/flexbox/FlexLine;->mCrossSize:I

    goto/32 :goto_b0

    nop

    :goto_37
    if-ge p1, p2, :cond_5

    goto/32 :goto_58

    :cond_5
    goto/32 :goto_b4

    nop

    :goto_38
    add-int/2addr v9, v3

    goto/32 :goto_b9

    nop

    :goto_39
    if-nez v0, :cond_6

    goto/32 :goto_50

    :cond_6
    goto/32 :goto_2e

    nop

    :goto_3a
    new-instance p1, Lcom/google/android/flexbox/FlexLine;

    goto/32 :goto_97

    nop

    :goto_3b
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    goto/32 :goto_4a

    nop

    :goto_3c
    invoke-interface {p1}, Lcom/google/android/flexbox/FlexContainer;->getSumOfCrossSize()I

    move-result p1

    goto/32 :goto_3e

    nop

    :goto_3d
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v9

    goto/32 :goto_21

    nop

    :goto_3e
    add-int/2addr p1, p3

    goto/32 :goto_70

    nop

    :goto_3f
    iget v9, v8, Lcom/google/android/flexbox/FlexLine;->mCrossSize:I

    goto/32 :goto_38

    nop

    :goto_40
    goto/16 :goto_a3

    :goto_41
    goto/32 :goto_0

    nop

    :goto_42
    move p3, v5

    :goto_43
    goto/32 :goto_9

    nop

    :goto_44
    invoke-interface {v0}, Lcom/google/android/flexbox/FlexContainer;->getFlexLinesInternal()Ljava/util/List;

    move-result-object v0

    goto/32 :goto_66

    nop

    :goto_45
    int-to-float p2, p2

    goto/32 :goto_24

    nop

    :goto_46
    const/4 v1, 0x2

    goto/32 :goto_47

    nop

    :goto_47
    if-ge p3, v1, :cond_7

    goto/32 :goto_a3

    :cond_7
    goto/32 :goto_b8

    nop

    :goto_48
    goto/16 :goto_80

    :pswitch_2
    goto/32 :goto_62

    nop

    :goto_49
    sub-float/2addr p3, v6

    goto/32 :goto_75

    nop

    :goto_4a
    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_83

    nop

    :goto_4b
    if-eq v2, v9, :cond_8

    goto/32 :goto_4e

    :cond_8
    goto/32 :goto_a4

    nop

    :goto_4c
    goto/16 :goto_a3

    :pswitch_3
    goto/32 :goto_92

    nop

    :goto_4d
    goto/16 :goto_9b

    :goto_4e
    goto/32 :goto_4

    nop

    :goto_4f
    goto/16 :goto_10

    :goto_50
    goto/32 :goto_84

    nop

    :goto_51
    add-int/lit8 v8, v8, 0x1

    goto/32 :goto_49

    nop

    :goto_52
    sub-float v9, p1, v9

    goto/32 :goto_23

    nop

    :goto_53
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result p2

    goto/32 :goto_82

    nop

    :goto_54
    move p3, v5

    :goto_55
    goto/32 :goto_32

    nop

    :goto_56
    int-to-float v7, v7

    goto/32 :goto_63

    nop

    :goto_57
    goto/16 :goto_a3

    :goto_58
    goto/32 :goto_19

    nop

    :goto_59
    move v10, p2

    goto/32 :goto_2c

    nop

    :goto_5a
    invoke-interface {p3, p1}, Lcom/google/android/flexbox/FlexContainer;->setFlexLines(Ljava/util/List;)V

    goto/32 :goto_14

    nop

    :goto_5b
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v8

    goto/32 :goto_65

    nop

    :goto_5c
    new-instance p2, Ljava/lang/StringBuilder;

    goto/32 :goto_12

    nop

    :goto_5d
    invoke-interface {p1, p2}, Lcom/google/android/flexbox/FlexContainer;->setFlexLines(Ljava/util/List;)V

    goto/32 :goto_28

    nop

    :goto_5e
    new-instance p1, Ljava/lang/IllegalArgumentException;

    goto/32 :goto_5c

    nop

    :goto_5f
    invoke-direct {p0, v0, p2, p1}, Lcom/google/android/flexbox/FlexboxHelper;->constructFlexLinesForAlignContentCenter(Ljava/util/List;II)Ljava/util/List;

    move-result-object p1

    goto/32 :goto_5a

    nop

    :goto_60
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    goto/32 :goto_39

    nop

    :goto_61
    invoke-interface {p3}, Lcom/google/android/flexbox/FlexContainer;->getAlignContent()I

    move-result p3

    goto/32 :goto_2f

    nop

    :goto_62
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result p1

    goto/32 :goto_7f

    nop

    :goto_63
    add-float/2addr v7, p1

    goto/32 :goto_d

    nop

    :goto_64
    if-gtz v7, :cond_9

    goto/32 :goto_76

    :cond_9
    goto/32 :goto_51

    nop

    :goto_65
    sub-int/2addr v8, v3

    goto/32 :goto_22

    nop

    :goto_66
    const/high16 v1, 0x40000000    # 2.0f

    goto/32 :goto_90

    nop

    :goto_67
    invoke-interface {p2, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/32 :goto_5b

    nop

    :goto_68
    goto/16 :goto_a3

    :goto_69
    goto/32 :goto_6f

    nop

    :goto_6a
    move v7, v5

    :goto_6b
    goto/32 :goto_17

    nop

    :goto_6c
    int-to-float p1, p2

    goto/32 :goto_a

    nop

    :goto_6d
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result p3

    goto/32 :goto_6a

    nop

    :goto_6e
    invoke-interface {p1, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/32 :goto_4f

    nop

    :goto_6f
    sub-int/2addr p2, p1

    goto/32 :goto_6c

    nop

    :goto_70
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    goto/32 :goto_a6

    nop

    :goto_71
    move v7, v5

    goto/32 :goto_4d

    nop

    :goto_72
    check-cast p1, Lcom/google/android/flexbox/FlexLine;

    goto/32 :goto_b5

    nop

    :goto_73
    invoke-interface {p2, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_74
    goto/32 :goto_2b

    nop

    :goto_75
    goto/16 :goto_2

    :goto_76
    goto/32 :goto_89

    nop

    :goto_77
    const/4 v5, 0x0

    goto/32 :goto_30

    nop

    :goto_78
    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    goto/32 :goto_8a

    nop

    :goto_79
    iput v7, v8, Lcom/google/android/flexbox/FlexLine;->mCrossSize:I

    goto/32 :goto_71

    nop

    :goto_7a
    add-float/2addr v7, v6

    :goto_7b
    goto/32 :goto_73

    nop

    :goto_7c
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    goto/32 :goto_44

    nop

    :goto_7d
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result p2

    goto/32 :goto_6

    nop

    :goto_7e
    add-int/lit8 v2, v2, 0x1

    goto/32 :goto_85

    nop

    :goto_7f
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p2

    :goto_80
    goto/32 :goto_7c

    nop

    :goto_81
    sub-int/2addr v8, v3

    goto/32 :goto_ba

    nop

    :goto_82
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p1

    goto/32 :goto_59

    nop

    :goto_83
    throw p1

    :pswitch_4
    goto/32 :goto_53

    nop

    :goto_84
    iget-object p2, p0, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    goto/32 :goto_11

    nop

    :goto_85
    goto/16 :goto_43

    :pswitch_5
    goto/32 :goto_37

    nop

    :goto_86
    sub-int/2addr p2, p1

    goto/32 :goto_3a

    nop

    :goto_87
    invoke-interface {v0}, Lcom/google/android/flexbox/FlexContainer;->getFlexDirection()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto/32 :goto_5e

    nop

    :goto_88
    check-cast v1, Lcom/google/android/flexbox/FlexLine;

    goto/32 :goto_b2

    nop

    :goto_89
    cmpg-float v7, p3, v4

    goto/32 :goto_b

    nop

    :goto_8a
    new-instance p3, Lcom/google/android/flexbox/FlexLine;

    goto/32 :goto_ad

    nop

    :goto_8b
    mul-int/2addr p1, v1

    goto/32 :goto_25

    nop

    :goto_8c
    move p1, v10

    goto/32 :goto_48

    nop

    :goto_8d
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result p2

    goto/32 :goto_42

    nop

    :goto_8e
    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    goto/32 :goto_6d

    nop

    :goto_8f
    iget-object p3, p0, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    goto/32 :goto_5f

    nop

    :goto_90
    if-eq p1, v1, :cond_a

    goto/32 :goto_a3

    :cond_a
    goto/32 :goto_1f

    nop

    :goto_91
    invoke-direct {v8}, Lcom/google/android/flexbox/FlexLine;-><init>()V

    goto/32 :goto_3d

    nop

    :goto_92
    if-ge p1, p2, :cond_b

    goto/32 :goto_41

    :cond_b
    goto/32 :goto_40

    nop

    :goto_93
    if-eq v1, v3, :cond_c

    goto/32 :goto_b1

    :cond_c
    goto/32 :goto_ab

    nop

    :goto_94
    iput p2, p3, Lcom/google/android/flexbox/FlexLine;->mCrossSize:I

    goto/32 :goto_f

    nop

    :goto_95
    cmpl-float v7, p3, v6

    goto/32 :goto_64

    nop

    :goto_96
    sub-int/2addr v9, v3

    goto/32 :goto_99

    nop

    :goto_97
    invoke-direct {p1}, Lcom/google/android/flexbox/FlexLine;-><init>()V

    goto/32 :goto_9c

    nop

    :goto_98
    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v7

    goto/32 :goto_79

    nop

    :goto_99
    iput v9, v8, Lcom/google/android/flexbox/FlexLine;->mCrossSize:I

    goto/32 :goto_7a

    nop

    :goto_9a
    iput v9, v8, Lcom/google/android/flexbox/FlexLine;->mCrossSize:I

    :goto_9b
    goto/32 :goto_34

    nop

    :goto_9c
    iput p2, p1, Lcom/google/android/flexbox/FlexLine;->mCrossSize:I

    goto/32 :goto_a2

    nop

    :goto_9d
    if-ge p1, p2, :cond_d

    goto/32 :goto_69

    :cond_d
    goto/32 :goto_68

    nop

    :goto_9e
    goto/16 :goto_6b

    :goto_9f
    goto/32 :goto_31

    nop

    :goto_a0
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_c

    nop

    :goto_a1
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    goto/32 :goto_87

    nop

    :goto_a2
    invoke-interface {v0, v2, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    :goto_a3
    goto/32 :goto_2a

    nop

    :goto_a4
    add-float/2addr v7, p1

    goto/32 :goto_98

    nop

    :goto_a5
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result p3

    goto/32 :goto_46

    nop

    :goto_a6
    const/4 v2, 0x0

    goto/32 :goto_aa

    nop

    :goto_a7
    sub-float/2addr v7, v6

    goto/32 :goto_1b

    nop

    :goto_a8
    add-int/lit8 v8, v8, -0x1

    goto/32 :goto_1

    nop

    :goto_a9
    goto/16 :goto_a3

    :pswitch_6
    goto/32 :goto_9d

    nop

    :goto_aa
    const/4 v3, 0x1

    goto/32 :goto_93

    nop

    :goto_ab
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    goto/32 :goto_72

    nop

    :goto_ac
    new-instance v8, Lcom/google/android/flexbox/FlexLine;

    goto/32 :goto_91

    nop

    :goto_ad
    invoke-direct {p3}, Lcom/google/android/flexbox/FlexLine;-><init>()V

    goto/32 :goto_94

    nop

    :goto_ae
    int-to-float p1, p2

    goto/32 :goto_7d

    nop

    :goto_af
    cmpl-float v9, v7, v6

    goto/32 :goto_5

    nop

    :goto_b0
    goto/16 :goto_a3

    :goto_b1
    goto/32 :goto_a5

    nop

    :goto_b2
    iget v7, v1, Lcom/google/android/flexbox/FlexLine;->mCrossSize:I

    goto/32 :goto_56

    nop

    :goto_b3
    if-ltz v9, :cond_e

    goto/32 :goto_7b

    :cond_e
    goto/32 :goto_1a

    nop

    :goto_b4
    iget-object p3, p0, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    goto/32 :goto_13

    nop

    :goto_b5
    sub-int/2addr p2, p3

    goto/32 :goto_36

    nop

    :goto_b6
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/32 :goto_6e

    nop

    :goto_b7
    add-float/2addr v7, p3

    goto/32 :goto_54

    nop

    :goto_b8
    iget-object p3, p0, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    goto/32 :goto_61

    nop

    :goto_b9
    iput v9, v8, Lcom/google/android/flexbox/FlexLine;->mCrossSize:I

    goto/32 :goto_a7

    nop

    :goto_ba
    if-eq v2, v8, :cond_f

    goto/32 :goto_55

    :cond_f
    goto/32 :goto_b7

    nop

    :goto_bb
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_88

    nop

    :goto_bc
    check-cast v0, Lcom/google/android/flexbox/FlexLine;

    goto/32 :goto_8

    nop
.end method

.method determineMainSize(II)V
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    const/4 v0, 0x0

    goto/32 :goto_2

    nop

    :goto_2
    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/flexbox/FlexboxHelper;->determineMainSize(III)V

    goto/32 :goto_0

    nop
.end method

.method determineMainSize(III)V
    .locals 11

    goto/32 :goto_5d

    nop

    :goto_0
    move-object v2, p0

    goto/32 :goto_4b

    nop

    :goto_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    goto/32 :goto_a

    nop

    :goto_2
    invoke-interface {v1}, Lcom/google/android/flexbox/FlexContainer;->getFlexDirection()I

    move-result v1

    goto/32 :goto_2a

    nop

    :goto_3
    invoke-interface {v2}, Lcom/google/android/flexbox/FlexContainer;->getPaddingRight()I

    move-result v2

    goto/32 :goto_46

    nop

    :goto_4
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    goto/32 :goto_21

    nop

    :goto_5
    goto :goto_c

    :goto_6
    goto/32 :goto_1c

    nop

    :goto_7
    move-object v2, p0

    goto/32 :goto_32

    nop

    :goto_8
    move-object v5, v2

    goto/32 :goto_5b

    nop

    :goto_9
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    goto/32 :goto_19

    nop

    :goto_a
    new-instance p2, Ljava/lang/StringBuilder;

    goto/32 :goto_1d

    nop

    :goto_b
    invoke-interface {v0}, Lcom/google/android/flexbox/FlexContainer;->getLargestMainSize()I

    move-result v1

    :goto_c
    goto/32 :goto_51

    nop

    :goto_d
    if-gt v3, v1, :cond_0

    goto/32 :goto_18

    :cond_0
    goto/32 :goto_17

    nop

    :goto_e
    goto/16 :goto_47

    :pswitch_0
    goto/32 :goto_1f

    nop

    :goto_f
    invoke-direct {p0, v0}, Lcom/google/android/flexbox/FlexboxHelper;->ensureChildrenFrozen(I)V

    goto/32 :goto_1b

    nop

    :goto_10
    if-nez v2, :cond_1

    goto/32 :goto_29

    :cond_1
    goto/32 :goto_3c

    nop

    :goto_11
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v9

    goto/32 :goto_13

    nop

    :goto_12
    invoke-interface {p3}, Lcom/google/android/flexbox/FlexContainer;->getFlexLinesInternal()Ljava/util/List;

    move-result-object p3

    goto/32 :goto_11

    nop

    :goto_13
    move v10, v2

    :goto_14
    goto/32 :goto_27

    nop

    :goto_15
    const-string p3, "Invalid flex direction: "

    goto/32 :goto_4e

    nop

    :goto_16
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/32 :goto_9

    nop

    :goto_17
    goto/16 :goto_55

    :goto_18
    goto/32 :goto_54

    nop

    :goto_19
    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_40

    nop

    :goto_1a
    iget-object p3, p0, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    goto/32 :goto_12

    nop

    :goto_1b
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    goto/32 :goto_57

    nop

    :goto_1c
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    goto/32 :goto_b

    nop

    :goto_1d
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_15

    nop

    :goto_1e
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    goto/32 :goto_50

    nop

    :goto_1f
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    goto/32 :goto_60

    nop

    :goto_20
    add-int/lit8 v10, v10, 0x1

    goto/32 :goto_4c

    nop

    :goto_21
    invoke-interface {v0}, Lcom/google/android/flexbox/FlexContainer;->getPaddingLeft()I

    move-result v0

    goto/32 :goto_5c

    nop

    :goto_22
    if-ge p3, v0, :cond_2

    goto/32 :goto_26

    :cond_2
    goto/32 :goto_25

    nop

    :goto_23
    iget v2, v5, Lcom/google/android/flexbox/FlexLine;->mMainSize:I

    goto/32 :goto_31

    nop

    :goto_24
    move v6, v1

    goto/32 :goto_52

    nop

    :goto_25
    return-void

    :goto_26
    goto/32 :goto_44

    nop

    :goto_27
    if-lt v10, v9, :cond_3

    goto/32 :goto_4d

    :cond_3
    goto/32 :goto_36

    nop

    :goto_28
    goto/16 :goto_3f

    :goto_29
    goto/32 :goto_2d

    nop

    :goto_2a
    const/high16 v2, 0x40000000    # 2.0f

    packed-switch v1, :pswitch_data_0

    goto/32 :goto_1

    nop

    :goto_2b
    aget v2, v3, p3

    :goto_2c
    goto/32 :goto_1a

    nop

    :goto_2d
    iget v2, v5, Lcom/google/android/flexbox/FlexLine;->mMainSize:I

    goto/32 :goto_3a

    nop

    :goto_2e
    invoke-interface {v0}, Lcom/google/android/flexbox/FlexContainer;->getFlexItemCount()I

    move-result v0

    goto/32 :goto_f

    nop

    :goto_2f
    move v4, p2

    goto/32 :goto_24

    nop

    :goto_30
    const/4 v2, 0x0

    goto/32 :goto_3d

    nop

    :goto_31
    if-lt v2, v1, :cond_4

    goto/32 :goto_29

    :cond_4
    goto/32 :goto_41

    nop

    :goto_32
    move v3, p1

    goto/32 :goto_39

    nop

    :goto_33
    invoke-interface {v0}, Lcom/google/android/flexbox/FlexContainer;->getFlexDirection()I

    move-result v0

    goto/32 :goto_5e

    nop

    :goto_34
    invoke-direct/range {v2 .. v8}, Lcom/google/android/flexbox/FlexboxHelper;->expandFlexItems(IILcom/google/android/flexbox/FlexLine;IIZ)V

    goto/32 :goto_28

    nop

    :goto_35
    move v7, v0

    goto/32 :goto_34

    nop

    :goto_36
    invoke-interface {p3, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_8

    nop

    :goto_37
    invoke-interface {v2}, Lcom/google/android/flexbox/FlexContainer;->getPaddingBottom()I

    move-result v2

    goto/32 :goto_4f

    nop

    :goto_38
    invoke-interface {v0}, Lcom/google/android/flexbox/FlexContainer;->getPaddingTop()I

    move-result v0

    goto/32 :goto_49

    nop

    :goto_39
    move v4, p2

    goto/32 :goto_4a

    nop

    :goto_3a
    if-gt v2, v1, :cond_5

    goto/32 :goto_3f

    :cond_5
    goto/32 :goto_45

    nop

    :goto_3b
    if-nez v2, :cond_6

    goto/32 :goto_3f

    :cond_6
    goto/32 :goto_5a

    nop

    :goto_3c
    const/4 v8, 0x0

    goto/32 :goto_7

    nop

    :goto_3d
    iget-object v3, p0, Lcom/google/android/flexbox/FlexboxHelper;->mIndexToFlexLine:[I

    goto/32 :goto_56

    nop

    :goto_3e
    invoke-direct/range {v2 .. v8}, Lcom/google/android/flexbox/FlexboxHelper;->shrinkFlexItems(IILcom/google/android/flexbox/FlexLine;IIZ)V

    :goto_3f
    goto/32 :goto_20

    nop

    :goto_40
    throw p1

    :pswitch_1
    goto/32 :goto_1e

    nop

    :goto_41
    iget-boolean v2, v5, Lcom/google/android/flexbox/FlexLine;->mAnyItemsHaveFlexGrow:Z

    goto/32 :goto_10

    nop

    :goto_42
    if-eq v0, v2, :cond_7

    goto/32 :goto_59

    :cond_7
    goto/32 :goto_58

    nop

    :goto_43
    invoke-interface {v3}, Lcom/google/android/flexbox/FlexContainer;->getLargestMainSize()I

    move-result v3

    goto/32 :goto_42

    nop

    :goto_44
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    goto/32 :goto_33

    nop

    :goto_45
    iget-boolean v2, v5, Lcom/google/android/flexbox/FlexLine;->mAnyItemsHaveFlexShrink:Z

    goto/32 :goto_3b

    nop

    :goto_46
    add-int/2addr v0, v2

    :goto_47
    goto/32 :goto_30

    nop

    :goto_48
    iget-object v3, p0, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    goto/32 :goto_43

    nop

    :goto_49
    iget-object v2, p0, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    goto/32 :goto_37

    nop

    :goto_4a
    move v6, v1

    goto/32 :goto_35

    nop

    :goto_4b
    move v3, p1

    goto/32 :goto_2f

    nop

    :goto_4c
    goto/16 :goto_14

    :goto_4d
    goto/32 :goto_5f

    nop

    :goto_4e
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_16

    nop

    :goto_4f
    add-int/2addr v0, v2

    goto/32 :goto_e

    nop

    :goto_50
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    goto/32 :goto_53

    nop

    :goto_51
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    goto/32 :goto_38

    nop

    :goto_52
    move v7, v0

    goto/32 :goto_3e

    nop

    :goto_53
    if-eq v0, v2, :cond_8

    goto/32 :goto_6

    :cond_8
    goto/32 :goto_5

    nop

    :goto_54
    move v1, v3

    :goto_55
    goto/32 :goto_4

    nop

    :goto_56
    if-nez v3, :cond_9

    goto/32 :goto_2c

    :cond_9
    goto/32 :goto_2b

    nop

    :goto_57
    invoke-interface {v0}, Lcom/google/android/flexbox/FlexContainer;->getFlexItemCount()I

    move-result v0

    goto/32 :goto_22

    nop

    :goto_58
    goto :goto_55

    :goto_59
    goto/32 :goto_d

    nop

    :goto_5a
    const/4 v8, 0x0

    goto/32 :goto_0

    nop

    :goto_5b
    check-cast v5, Lcom/google/android/flexbox/FlexLine;

    goto/32 :goto_23

    nop

    :goto_5c
    iget-object v2, p0, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    goto/32 :goto_3

    nop

    :goto_5d
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    goto/32 :goto_2e

    nop

    :goto_5e
    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    goto/32 :goto_2

    nop

    :goto_5f
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :goto_60
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    goto/32 :goto_48

    nop
.end method

.method ensureIndexToFlexLine(I)V
    .locals 2

    goto/32 :goto_12

    nop

    :goto_0
    if-ge v0, p1, :cond_0

    goto/32 :goto_b

    :cond_0
    goto/32 :goto_a

    nop

    :goto_1
    invoke-static {v0, p1}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object p1

    goto/32 :goto_7

    nop

    :goto_2
    if-lt p1, v0, :cond_1

    goto/32 :goto_5

    :cond_1
    goto/32 :goto_4

    nop

    :goto_3
    iput-object p1, p0, Lcom/google/android/flexbox/FlexboxHelper;->mIndexToFlexLine:[I

    goto/32 :goto_f

    nop

    :goto_4
    move p1, v0

    :goto_5
    goto/32 :goto_13

    nop

    :goto_6
    if-lt v1, p1, :cond_2

    goto/32 :goto_8

    :cond_2
    goto/32 :goto_e

    nop

    :goto_7
    iput-object p1, p0, Lcom/google/android/flexbox/FlexboxHelper;->mIndexToFlexLine:[I

    :goto_8
    goto/32 :goto_15

    nop

    :goto_9
    const/16 v0, 0xa

    goto/32 :goto_2

    nop

    :goto_a
    move p1, v0

    :goto_b
    goto/32 :goto_c

    nop

    :goto_c
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxHelper;->mIndexToFlexLine:[I

    goto/32 :goto_1

    nop

    :goto_d
    array-length v1, v0

    goto/32 :goto_6

    nop

    :goto_e
    array-length v0, v0

    goto/32 :goto_11

    nop

    :goto_f
    goto :goto_8

    :goto_10
    goto/32 :goto_d

    nop

    :goto_11
    mul-int/lit8 v0, v0, 0x2

    goto/32 :goto_0

    nop

    :goto_12
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxHelper;->mIndexToFlexLine:[I

    goto/32 :goto_14

    nop

    :goto_13
    new-array p1, p1, [I

    goto/32 :goto_3

    nop

    :goto_14
    if-eqz v0, :cond_3

    goto/32 :goto_10

    :cond_3
    goto/32 :goto_9

    nop

    :goto_15
    return-void
.end method

.method ensureMeasureSpecCache(I)V
    .locals 2

    goto/32 :goto_14

    nop

    :goto_0
    if-lt v1, p1, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_f

    nop

    :goto_1
    mul-int/lit8 v0, v0, 0x2

    goto/32 :goto_12

    nop

    :goto_2
    move p1, v0

    :goto_3
    goto/32 :goto_4

    nop

    :goto_4
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxHelper;->mMeasureSpecCache:[J

    goto/32 :goto_8

    nop

    :goto_5
    iput-object p1, p0, Lcom/google/android/flexbox/FlexboxHelper;->mMeasureSpecCache:[J

    goto/32 :goto_a

    nop

    :goto_6
    iput-object p1, p0, Lcom/google/android/flexbox/FlexboxHelper;->mMeasureSpecCache:[J

    :goto_7
    goto/32 :goto_10

    nop

    :goto_8
    invoke-static {v0, p1}, Ljava/util/Arrays;->copyOf([JI)[J

    move-result-object p1

    goto/32 :goto_6

    nop

    :goto_9
    if-eqz v0, :cond_1

    goto/32 :goto_b

    :cond_1
    goto/32 :goto_11

    nop

    :goto_a
    goto :goto_7

    :goto_b
    goto/32 :goto_13

    nop

    :goto_c
    move p1, v0

    :goto_d
    goto/32 :goto_e

    nop

    :goto_e
    new-array p1, p1, [J

    goto/32 :goto_5

    nop

    :goto_f
    array-length v0, v0

    goto/32 :goto_1

    nop

    :goto_10
    return-void

    :goto_11
    const/16 v0, 0xa

    goto/32 :goto_15

    nop

    :goto_12
    if-ge v0, p1, :cond_2

    goto/32 :goto_3

    :cond_2
    goto/32 :goto_2

    nop

    :goto_13
    array-length v1, v0

    goto/32 :goto_0

    nop

    :goto_14
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxHelper;->mMeasureSpecCache:[J

    goto/32 :goto_9

    nop

    :goto_15
    if-lt p1, v0, :cond_3

    goto/32 :goto_d

    :cond_3
    goto/32 :goto_c

    nop
.end method

.method ensureMeasuredSizeCache(I)V
    .locals 2

    goto/32 :goto_15

    nop

    :goto_0
    if-ge v0, p1, :cond_0

    goto/32 :goto_10

    :cond_0
    goto/32 :goto_f

    nop

    :goto_1
    iput-object p1, p0, Lcom/google/android/flexbox/FlexboxHelper;->mMeasuredSizeCache:[J

    :goto_2
    goto/32 :goto_11

    nop

    :goto_3
    move p1, v0

    :goto_4
    goto/32 :goto_14

    nop

    :goto_5
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxHelper;->mMeasuredSizeCache:[J

    goto/32 :goto_e

    nop

    :goto_6
    if-lt p1, v0, :cond_1

    goto/32 :goto_4

    :cond_1
    goto/32 :goto_3

    nop

    :goto_7
    array-length v1, v0

    goto/32 :goto_9

    nop

    :goto_8
    iput-object p1, p0, Lcom/google/android/flexbox/FlexboxHelper;->mMeasuredSizeCache:[J

    goto/32 :goto_a

    nop

    :goto_9
    if-lt v1, p1, :cond_2

    goto/32 :goto_2

    :cond_2
    goto/32 :goto_13

    nop

    :goto_a
    goto :goto_2

    :goto_b
    goto/32 :goto_7

    nop

    :goto_c
    mul-int/lit8 v0, v0, 0x2

    goto/32 :goto_0

    nop

    :goto_d
    const/16 v0, 0xa

    goto/32 :goto_6

    nop

    :goto_e
    invoke-static {v0, p1}, Ljava/util/Arrays;->copyOf([JI)[J

    move-result-object p1

    goto/32 :goto_1

    nop

    :goto_f
    move p1, v0

    :goto_10
    goto/32 :goto_5

    nop

    :goto_11
    return-void

    :goto_12
    if-eqz v0, :cond_3

    goto/32 :goto_b

    :cond_3
    goto/32 :goto_d

    nop

    :goto_13
    array-length v0, v0

    goto/32 :goto_c

    nop

    :goto_14
    new-array p1, p1, [J

    goto/32 :goto_8

    nop

    :goto_15
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxHelper;->mMeasuredSizeCache:[J

    goto/32 :goto_12

    nop
.end method

.method extractHigherInt(J)I
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    long-to-int p1, p1

    goto/32 :goto_1

    nop

    :goto_1
    return p1

    :goto_2
    const/16 v0, 0x20

    goto/32 :goto_3

    nop

    :goto_3
    shr-long/2addr p1, v0

    goto/32 :goto_0

    nop
.end method

.method extractLowerInt(J)I
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return p1

    :goto_1
    long-to-int p1, p1

    goto/32 :goto_0

    nop
.end method

.method isOrderChangedFromLastMeasurement(Landroid/util/SparseIntArray;)Z
    .locals 6

    goto/32 :goto_12

    nop

    :goto_0
    const/4 v1, 0x0

    goto/32 :goto_19

    nop

    :goto_1
    if-ne v1, v0, :cond_0

    goto/32 :goto_18

    :cond_0
    goto/32 :goto_17

    nop

    :goto_2
    return v2

    :goto_3
    goto/32 :goto_4

    nop

    :goto_4
    add-int/lit8 v3, v3, 0x1

    goto/32 :goto_15

    nop

    :goto_5
    invoke-virtual {p1}, Landroid/util/SparseIntArray;->size()I

    move-result v1

    goto/32 :goto_b

    nop

    :goto_6
    if-eqz v4, :cond_1

    goto/32 :goto_d

    :cond_1
    goto/32 :goto_c

    nop

    :goto_7
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    goto/32 :goto_11

    nop

    :goto_8
    if-ne v4, v5, :cond_2

    goto/32 :goto_3

    :cond_2
    goto/32 :goto_2

    nop

    :goto_9
    if-lt v3, v0, :cond_3

    goto/32 :goto_16

    :cond_3
    goto/32 :goto_14

    nop

    :goto_a
    invoke-interface {v4}, Lcom/google/android/flexbox/FlexItem;->getOrder()I

    move-result v4

    goto/32 :goto_e

    nop

    :goto_b
    const/4 v2, 0x1

    goto/32 :goto_1

    nop

    :goto_c
    goto :goto_3

    :goto_d
    goto/32 :goto_7

    nop

    :goto_e
    invoke-virtual {p1, v3}, Landroid/util/SparseIntArray;->get(I)I

    move-result v5

    goto/32 :goto_8

    nop

    :goto_f
    invoke-interface {v4, v3}, Lcom/google/android/flexbox/FlexContainer;->getFlexItemAt(I)Landroid/view/View;

    move-result-object v4

    goto/32 :goto_6

    nop

    :goto_10
    return v1

    :goto_11
    check-cast v4, Lcom/google/android/flexbox/FlexItem;

    goto/32 :goto_a

    nop

    :goto_12
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    goto/32 :goto_13

    nop

    :goto_13
    invoke-interface {v0}, Lcom/google/android/flexbox/FlexContainer;->getFlexItemCount()I

    move-result v0

    goto/32 :goto_5

    nop

    :goto_14
    iget-object v4, p0, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    goto/32 :goto_f

    nop

    :goto_15
    goto :goto_1a

    :goto_16
    goto/32 :goto_10

    nop

    :goto_17
    return v2

    :goto_18
    goto/32 :goto_0

    nop

    :goto_19
    move v3, v1

    :goto_1a
    goto/32 :goto_9

    nop
.end method

.method layoutSingleChildHorizontal(Landroid/view/View;Lcom/google/android/flexbox/FlexLine;IIII)V
    .locals 4

    goto/32 :goto_6

    nop

    :goto_0
    invoke-interface {p2}, Lcom/google/android/flexbox/FlexContainer;->getFlexWrap()I

    move-result p2

    goto/32 :goto_13

    nop

    :goto_1
    add-int/2addr p6, p2

    goto/32 :goto_23

    nop

    :goto_2
    sub-int/2addr p6, p2

    goto/32 :goto_46

    nop

    :goto_3
    add-int/2addr p4, p2

    goto/32 :goto_54

    nop

    :goto_4
    invoke-virtual {p1}, Landroid/view/View;->getBaseline()I

    move-result v1

    goto/32 :goto_25

    nop

    :goto_5
    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->getMarginBottom()I

    move-result p6

    goto/32 :goto_a

    nop

    :goto_6
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    goto/32 :goto_5e

    nop

    :goto_7
    invoke-static {p2, v0}, Ljava/lang/Math;->max(II)I

    move-result p2

    goto/32 :goto_33

    nop

    :goto_8
    goto/16 :goto_47

    :goto_9
    goto/32 :goto_4d

    nop

    :goto_a
    sub-int/2addr p2, p6

    goto/32 :goto_1e

    nop

    :goto_b
    add-int/2addr p4, p2

    goto/32 :goto_56

    nop

    :goto_c
    sub-int/2addr p2, v1

    goto/32 :goto_4e

    nop

    :goto_d
    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    goto/32 :goto_f

    nop

    :goto_e
    goto/16 :goto_47

    :pswitch_0
    goto/32 :goto_44

    nop

    :goto_f
    invoke-interface {v1}, Lcom/google/android/flexbox/FlexContainer;->getFlexWrap()I

    move-result v1

    goto/32 :goto_43

    nop

    :goto_10
    add-int/2addr p4, p2

    goto/32 :goto_5a

    nop

    :goto_11
    div-int/2addr v2, v3

    goto/32 :goto_53

    nop

    :goto_12
    invoke-virtual {p1, p3, p4, p5, p6}, Landroid/view/View;->layout(IIII)V

    goto/32 :goto_e

    nop

    :goto_13
    if-ne p2, v3, :cond_0

    goto/32 :goto_9

    :cond_0
    goto/32 :goto_2a

    nop

    :goto_14
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result p2

    goto/32 :goto_17

    nop

    :goto_15
    sub-int/2addr p4, v2

    goto/32 :goto_3e

    nop

    :goto_16
    sub-int/2addr p4, p6

    goto/32 :goto_5d

    nop

    :goto_17
    add-int/2addr p2, p4

    goto/32 :goto_1d

    nop

    :goto_18
    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->getMarginTop()I

    move-result p2

    goto/32 :goto_1

    nop

    :goto_19
    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->getMarginBottom()I

    move-result v0

    goto/32 :goto_7

    nop

    :goto_1a
    invoke-virtual {p1, p3, p4, p5, p6}, Landroid/view/View;->layout(IIII)V

    goto/32 :goto_2f

    nop

    :goto_1b
    invoke-interface {p2}, Lcom/google/android/flexbox/FlexContainer;->getFlexWrap()I

    move-result p2

    goto/32 :goto_42

    nop

    :goto_1c
    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->getAlignSelf()I

    move-result v2

    goto/32 :goto_4a

    nop

    :goto_1d
    invoke-virtual {p1, p3, p4, p5, p2}, Landroid/view/View;->layout(IIII)V

    goto/32 :goto_4c

    nop

    :goto_1e
    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->getMarginBottom()I

    move-result p6

    goto/32 :goto_16

    nop

    :goto_1f
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    goto/32 :goto_3a

    nop

    :goto_20
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result p2

    goto/32 :goto_45

    nop

    :goto_21
    sub-int/2addr p4, p2

    goto/32 :goto_2b

    nop

    :goto_22
    goto/16 :goto_47

    :pswitch_1
    goto/32 :goto_20

    nop

    :goto_23
    invoke-virtual {p1, p3, p4, p5, p6}, Landroid/view/View;->layout(IIII)V

    goto/32 :goto_8

    nop

    :goto_24
    iget-object p2, p0, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    goto/32 :goto_1b

    nop

    :goto_25
    add-int/2addr p2, v1

    goto/32 :goto_19

    nop

    :goto_26
    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->getMarginTop()I

    move-result p2

    goto/32 :goto_61

    nop

    :goto_27
    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->getMarginBottom()I

    move-result p2

    goto/32 :goto_2e

    nop

    :goto_28
    add-int/2addr p4, p2

    goto/32 :goto_18

    nop

    :goto_29
    const/4 v3, 0x2

    packed-switch v1, :pswitch_data_0

    goto/32 :goto_50

    nop

    :goto_2a
    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->getMarginTop()I

    move-result p2

    goto/32 :goto_28

    nop

    :goto_2b
    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->getMarginBottom()I

    move-result p2

    goto/32 :goto_2

    nop

    :goto_2c
    invoke-interface {p2}, Lcom/google/android/flexbox/FlexContainer;->getFlexWrap()I

    move-result p2

    goto/32 :goto_2d

    nop

    :goto_2d
    if-ne p2, v3, :cond_1

    goto/32 :goto_60

    :cond_1
    goto/32 :goto_31

    nop

    :goto_2e
    sub-int/2addr v2, p2

    goto/32 :goto_11

    nop

    :goto_2f
    goto/16 :goto_47

    :goto_30
    goto/32 :goto_4f

    nop

    :goto_31
    add-int/2addr p4, v2

    goto/32 :goto_51

    nop

    :goto_32
    iget v2, p2, Lcom/google/android/flexbox/FlexLine;->mCrossSize:I

    goto/32 :goto_29

    nop

    :goto_33
    sub-int/2addr p4, p2

    goto/32 :goto_35

    nop

    :goto_34
    sub-int p2, p4, p2

    goto/32 :goto_5

    nop

    :goto_35
    sub-int/2addr p6, p2

    goto/32 :goto_37

    nop

    :goto_36
    add-int/2addr p2, p4

    goto/32 :goto_58

    nop

    :goto_37
    invoke-virtual {p1, p3, p4, p5, p6}, Landroid/view/View;->layout(IIII)V

    goto/32 :goto_22

    nop

    :goto_38
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result p2

    goto/32 :goto_57

    nop

    :goto_39
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_0
    .end packed-switch

    :goto_3a
    sub-int/2addr p2, v1

    goto/32 :goto_4

    nop

    :goto_3b
    invoke-virtual {p1}, Landroid/view/View;->getBaseline()I

    move-result v1

    goto/32 :goto_c

    nop

    :goto_3c
    invoke-interface {v1}, Lcom/google/android/flexbox/FlexContainer;->getAlignItems()I

    move-result v1

    goto/32 :goto_1c

    nop

    :goto_3d
    add-int/2addr p4, v2

    goto/32 :goto_48

    nop

    :goto_3e
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result p2

    goto/32 :goto_3

    nop

    :goto_3f
    add-int/2addr p6, p2

    goto/32 :goto_12

    nop

    :goto_40
    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->getAlignSelf()I

    move-result v1

    :goto_41
    goto/32 :goto_32

    nop

    :goto_42
    if-ne p2, v3, :cond_2

    goto/32 :goto_5c

    :cond_2
    goto/32 :goto_3d

    nop

    :goto_43
    if-ne v1, v3, :cond_3

    goto/32 :goto_30

    :cond_3
    goto/32 :goto_52

    nop

    :goto_44
    iget-object p2, p0, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    goto/32 :goto_0

    nop

    :goto_45
    sub-int/2addr v2, p2

    goto/32 :goto_26

    nop

    :goto_46
    invoke-virtual {p1, p3, p4, p5, p6}, Landroid/view/View;->layout(IIII)V

    :goto_47
    goto/32 :goto_39

    nop

    :goto_48
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result p2

    goto/32 :goto_34

    nop

    :goto_49
    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    goto/32 :goto_3c

    nop

    :goto_4a
    const/4 v3, -0x1

    goto/32 :goto_55

    nop

    :goto_4b
    sub-int/2addr p4, v2

    goto/32 :goto_14

    nop

    :goto_4c
    goto :goto_47

    :pswitch_2
    goto/32 :goto_24

    nop

    :goto_4d
    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->getMarginBottom()I

    move-result p2

    goto/32 :goto_21

    nop

    :goto_4e
    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->getMarginTop()I

    move-result v0

    goto/32 :goto_62

    nop

    :goto_4f
    iget p2, p2, Lcom/google/android/flexbox/FlexLine;->mMaxBaseline:I

    goto/32 :goto_1f

    nop

    :goto_50
    goto/16 :goto_47

    :pswitch_3
    goto/32 :goto_d

    nop

    :goto_51
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result p2

    goto/32 :goto_36

    nop

    :goto_52
    iget p2, p2, Lcom/google/android/flexbox/FlexLine;->mMaxBaseline:I

    goto/32 :goto_3b

    nop

    :goto_53
    iget-object p2, p0, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    goto/32 :goto_2c

    nop

    :goto_54
    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->getMarginTop()I

    move-result p2

    goto/32 :goto_10

    nop

    :goto_55
    if-ne v2, v3, :cond_4

    goto/32 :goto_41

    :cond_4
    goto/32 :goto_40

    nop

    :goto_56
    add-int/2addr p6, p2

    goto/32 :goto_1a

    nop

    :goto_57
    add-int/2addr p6, p2

    goto/32 :goto_59

    nop

    :goto_58
    invoke-virtual {p1, p3, p4, p5, p2}, Landroid/view/View;->layout(IIII)V

    goto/32 :goto_5f

    nop

    :goto_59
    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->getMarginTop()I

    move-result p2

    goto/32 :goto_3f

    nop

    :goto_5a
    sub-int/2addr p6, v2

    goto/32 :goto_38

    nop

    :goto_5b
    goto :goto_47

    :goto_5c
    goto/32 :goto_15

    nop

    :goto_5d
    invoke-virtual {p1, p3, p2, p5, p4}, Landroid/view/View;->layout(IIII)V

    goto/32 :goto_5b

    nop

    :goto_5e
    check-cast v0, Lcom/google/android/flexbox/FlexItem;

    goto/32 :goto_49

    nop

    :goto_5f
    goto/16 :goto_47

    :goto_60
    goto/32 :goto_4b

    nop

    :goto_61
    add-int/2addr v2, p2

    goto/32 :goto_27

    nop

    :goto_62
    invoke-static {p2, v0}, Ljava/lang/Math;->max(II)I

    move-result p2

    goto/32 :goto_b

    nop
.end method

.method layoutSingleChildVertical(Landroid/view/View;Lcom/google/android/flexbox/FlexLine;ZIIII)V
    .locals 4

    goto/32 :goto_26

    nop

    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :goto_1
    div-int/lit8 p2, p2, 0x2

    goto/32 :goto_b

    nop

    :goto_2
    sub-int/2addr p4, p2

    goto/32 :goto_33

    nop

    :goto_3
    add-int/2addr p6, p2

    goto/32 :goto_3a

    nop

    :goto_4
    sub-int/2addr p4, p2

    goto/32 :goto_3e

    nop

    :goto_5
    sub-int/2addr p4, p3

    goto/32 :goto_1e

    nop

    :goto_6
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result p2

    goto/32 :goto_8

    nop

    :goto_7
    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->getAlignSelf()I

    move-result v2

    goto/32 :goto_45

    nop

    :goto_8
    sub-int/2addr p6, p2

    goto/32 :goto_1b

    nop

    :goto_9
    invoke-static {v0}, Landroidx/core/view/MarginLayoutParamsCompat;->getMarginEnd(Landroid/view/ViewGroup$MarginLayoutParams;)I

    move-result v0

    goto/32 :goto_27

    nop

    :goto_a
    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->getMarginLeft()I

    move-result p2

    goto/32 :goto_32

    nop

    :goto_b
    if-eqz p3, :cond_0

    goto/32 :goto_37

    :cond_0
    goto/32 :goto_22

    nop

    :goto_c
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result p3

    goto/32 :goto_13

    nop

    :goto_d
    sub-int/2addr p2, v1

    goto/32 :goto_38

    nop

    :goto_e
    add-int/2addr p4, p2

    goto/32 :goto_f

    nop

    :goto_f
    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->getMarginLeft()I

    move-result p2

    goto/32 :goto_3

    nop

    :goto_10
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    goto/32 :goto_29

    nop

    :goto_11
    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    goto/32 :goto_39

    nop

    :goto_12
    if-eqz p3, :cond_1

    goto/32 :goto_2c

    :cond_1
    goto/32 :goto_1d

    nop

    :goto_13
    add-int/2addr p4, p3

    goto/32 :goto_17

    nop

    :goto_14
    check-cast v0, Lcom/google/android/flexbox/FlexItem;

    goto/32 :goto_11

    nop

    :goto_15
    add-int/2addr p6, p2

    goto/32 :goto_a

    nop

    :goto_16
    sub-int/2addr p6, p2

    goto/32 :goto_3d

    nop

    :goto_17
    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->getMarginLeft()I

    move-result p3

    goto/32 :goto_3b

    nop

    :goto_18
    sub-int/2addr p4, p3

    goto/32 :goto_1a

    nop

    :goto_19
    iget p2, p2, Lcom/google/android/flexbox/FlexLine;->mCrossSize:I

    packed-switch v1, :pswitch_data_0

    goto/32 :goto_2f

    nop

    :goto_1a
    add-int/2addr p6, p2

    goto/32 :goto_6

    nop

    :goto_1b
    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->getMarginRight()I

    move-result p2

    goto/32 :goto_40

    nop

    :goto_1c
    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->getMarginLeft()I

    move-result p2

    goto/32 :goto_e

    nop

    :goto_1d
    add-int/2addr p4, p2

    goto/32 :goto_2a

    nop

    :goto_1e
    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->getMarginRight()I

    move-result p3

    goto/32 :goto_18

    nop

    :goto_1f
    add-int/2addr p2, v1

    goto/32 :goto_9

    nop

    :goto_20
    invoke-virtual {p1, p4, p5, p6, p7}, Landroid/view/View;->layout(IIII)V

    goto/32 :goto_21

    nop

    :goto_21
    goto/16 :goto_42

    :pswitch_0
    goto/32 :goto_12

    nop

    :goto_22
    add-int/2addr p4, p2

    goto/32 :goto_25

    nop

    :goto_23
    goto/16 :goto_42

    :pswitch_1
    goto/32 :goto_34

    nop

    :goto_24
    sub-int/2addr p6, p2

    goto/32 :goto_41

    nop

    :goto_25
    add-int/2addr p6, p2

    goto/32 :goto_31

    nop

    :goto_26
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    goto/32 :goto_14

    nop

    :goto_27
    sub-int/2addr p2, v0

    goto/32 :goto_1

    nop

    :goto_28
    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->getMarginRight()I

    move-result p2

    goto/32 :goto_4

    nop

    :goto_29
    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    goto/32 :goto_35

    nop

    :goto_2a
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result p3

    goto/32 :goto_5

    nop

    :goto_2b
    goto/16 :goto_42

    :goto_2c
    goto/32 :goto_3c

    nop

    :goto_2d
    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->getAlignSelf()I

    move-result v1

    :goto_2e
    goto/32 :goto_19

    nop

    :goto_2f
    goto/16 :goto_42

    :pswitch_2
    goto/32 :goto_10

    nop

    :goto_30
    invoke-virtual {p1, p4, p5, p6, p7}, Landroid/view/View;->layout(IIII)V

    goto/32 :goto_2b

    nop

    :goto_31
    invoke-virtual {p1, p4, p5, p6, p7}, Landroid/view/View;->layout(IIII)V

    goto/32 :goto_36

    nop

    :goto_32
    add-int/2addr p6, p2

    goto/32 :goto_3f

    nop

    :goto_33
    sub-int/2addr p6, p2

    goto/32 :goto_20

    nop

    :goto_34
    if-eqz p3, :cond_2

    goto/32 :goto_44

    :cond_2
    goto/32 :goto_1c

    nop

    :goto_35
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    goto/32 :goto_d

    nop

    :goto_36
    goto :goto_42

    :goto_37
    goto/32 :goto_2

    nop

    :goto_38
    invoke-static {v0}, Landroidx/core/view/MarginLayoutParamsCompat;->getMarginStart(Landroid/view/ViewGroup$MarginLayoutParams;)I

    move-result v1

    goto/32 :goto_1f

    nop

    :goto_39
    invoke-interface {v1}, Lcom/google/android/flexbox/FlexContainer;->getAlignItems()I

    move-result v1

    goto/32 :goto_7

    nop

    :goto_3a
    invoke-virtual {p1, p4, p5, p6, p7}, Landroid/view/View;->layout(IIII)V

    goto/32 :goto_43

    nop

    :goto_3b
    add-int/2addr p4, p3

    goto/32 :goto_16

    nop

    :goto_3c
    sub-int/2addr p4, p2

    goto/32 :goto_c

    nop

    :goto_3d
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result p2

    goto/32 :goto_15

    nop

    :goto_3e
    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->getMarginRight()I

    move-result p2

    goto/32 :goto_24

    nop

    :goto_3f
    invoke-virtual {p1, p4, p5, p6, p7}, Landroid/view/View;->layout(IIII)V

    goto/32 :goto_23

    nop

    :goto_40
    sub-int/2addr p6, p2

    goto/32 :goto_30

    nop

    :goto_41
    invoke-virtual {p1, p4, p5, p6, p7}, Landroid/view/View;->layout(IIII)V

    :goto_42
    goto/32 :goto_0

    nop

    :goto_43
    goto :goto_42

    :goto_44
    goto/32 :goto_28

    nop

    :goto_45
    const/4 v3, -0x1

    goto/32 :goto_46

    nop

    :goto_46
    if-ne v2, v3, :cond_3

    goto/32 :goto_2e

    :cond_3
    goto/32 :goto_2d

    nop
.end method

.method makeCombinedLong(II)J
    .locals 4

    goto/32 :goto_3

    nop

    :goto_0
    or-long/2addr p1, v0

    goto/32 :goto_6

    nop

    :goto_1
    int-to-long p1, p1

    goto/32 :goto_5

    nop

    :goto_2
    const/16 p2, 0x20

    goto/32 :goto_4

    nop

    :goto_3
    int-to-long v0, p2

    goto/32 :goto_2

    nop

    :goto_4
    shl-long/2addr v0, p2

    goto/32 :goto_1

    nop

    :goto_5
    const-wide v2, 0xffffffffL

    goto/32 :goto_7

    nop

    :goto_6
    return-wide p1

    :goto_7
    and-long/2addr p1, v2

    goto/32 :goto_0

    nop
.end method

.method stretchViews()V
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    return-void

    :goto_1
    invoke-virtual {p0, v0}, Lcom/google/android/flexbox/FlexboxHelper;->stretchViews(I)V

    goto/32 :goto_0

    nop

    :goto_2
    const/4 v0, 0x0

    goto/32 :goto_1

    nop
.end method

.method stretchViews(I)V
    .locals 13

    goto/32 :goto_61

    nop

    :goto_0
    goto/16 :goto_1b

    :goto_1
    packed-switch v0, :pswitch_data_0

    goto/32 :goto_5

    nop

    :goto_2
    invoke-interface {v0}, Lcom/google/android/flexbox/FlexContainer;->getFlexDirection()I

    move-result v0

    goto/32 :goto_12

    nop

    :goto_3
    new-instance v1, Ljava/lang/StringBuilder;

    goto/32 :goto_6

    nop

    :goto_4
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_36

    nop

    :goto_5
    new-instance p1, Ljava/lang/IllegalArgumentException;

    goto/32 :goto_3

    nop

    :goto_6
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_6e

    nop

    :goto_7
    if-lt v7, v6, :cond_0

    goto/32 :goto_41

    :cond_0
    goto/32 :goto_2d

    nop

    :goto_8
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :goto_9
    add-int/2addr v8, v7

    goto/32 :goto_65

    nop

    :goto_a
    new-instance v1, Ljava/lang/StringBuilder;

    goto/32 :goto_27

    nop

    :goto_b
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_c
    goto/32 :goto_4

    nop

    :goto_d
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/32 :goto_57

    nop

    :goto_e
    invoke-interface {v10}, Lcom/google/android/flexbox/FlexItem;->getAlignSelf()I

    move-result v10

    goto/32 :goto_4a

    nop

    :goto_f
    invoke-interface {v4, v5}, Lcom/google/android/flexbox/FlexContainer;->getReorderedFlexItemAt(I)Landroid/view/View;

    move-result-object v4

    packed-switch v0, :pswitch_data_1

    goto/32 :goto_3f

    nop

    :goto_10
    return-void

    :goto_11
    goto/32 :goto_35

    nop

    :goto_12
    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    goto/32 :goto_5c

    nop

    :goto_13
    iget v5, v1, Lcom/google/android/flexbox/FlexLine;->mCrossSize:I

    goto/32 :goto_1c

    nop

    :goto_14
    if-nez v1, :cond_1

    goto/32 :goto_5b

    :cond_1
    goto/32 :goto_59

    nop

    :goto_15
    goto/16 :goto_30

    :goto_16
    goto/32 :goto_43

    nop

    :goto_17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_d

    nop

    :goto_18
    check-cast v3, Ljava/lang/Integer;

    goto/32 :goto_19

    nop

    :goto_19
    iget-object v4, p0, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    goto/32 :goto_29

    nop

    :goto_1a
    invoke-direct {p0, v9, v10, v8}, Lcom/google/android/flexbox/FlexboxHelper;->stretchViewVertically(Landroid/view/View;II)V

    :goto_1b
    goto/32 :goto_33

    nop

    :goto_1c
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    goto/32 :goto_2c

    nop

    :goto_1d
    if-ge v7, v9, :cond_2

    goto/32 :goto_4f

    :cond_2
    goto/32 :goto_4e

    nop

    :goto_1e
    throw p1

    :pswitch_0
    goto/32 :goto_60

    nop

    :goto_1f
    invoke-interface {p1}, Lcom/google/android/flexbox/FlexContainer;->getFlexLinesInternal()Ljava/util/List;

    move-result-object p1

    goto/32 :goto_b

    nop

    :goto_20
    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_6d

    nop

    :goto_21
    const/4 v2, 0x4

    goto/32 :goto_23

    nop

    :goto_22
    if-lt p1, v4, :cond_3

    goto/32 :goto_4c

    :cond_3
    goto/32 :goto_4d

    nop

    :goto_23
    if-eq v1, v2, :cond_4

    goto/32 :goto_16

    :cond_4
    goto/32 :goto_47

    nop

    :goto_24
    invoke-interface {v0}, Lcom/google/android/flexbox/FlexContainer;->getFlexItemCount()I

    move-result v0

    goto/32 :goto_62

    nop

    :goto_25
    if-eq v10, v11, :cond_5

    goto/32 :goto_3d

    :cond_5
    goto/32 :goto_3c

    nop

    :goto_26
    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    goto/32 :goto_28

    nop

    :goto_27
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_6f

    nop

    :goto_28
    invoke-interface {v1}, Lcom/google/android/flexbox/FlexContainer;->getFlexLinesInternal()Ljava/util/List;

    move-result-object v1

    goto/32 :goto_2f

    nop

    :goto_29
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v5

    goto/32 :goto_f

    nop

    :goto_2a
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/32 :goto_49

    nop

    :goto_2b
    if-nez v3, :cond_6

    goto/32 :goto_c

    :cond_6
    goto/32 :goto_3b

    nop

    :goto_2c
    invoke-direct {p0, v4, v5, v3}, Lcom/google/android/flexbox/FlexboxHelper;->stretchViewVertically(Landroid/view/View;II)V

    goto/32 :goto_4b

    nop

    :goto_2d
    iget v8, v5, Lcom/google/android/flexbox/FlexLine;->mFirstIndex:I

    goto/32 :goto_9

    nop

    :goto_2e
    goto/16 :goto_5f

    :pswitch_1
    goto/32 :goto_13

    nop

    :goto_2f
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    :goto_30
    goto/32 :goto_22

    nop

    :goto_31
    iget v6, v5, Lcom/google/android/flexbox/FlexLine;->mItemCount:I

    goto/32 :goto_52

    nop

    :goto_32
    invoke-direct {p0, v9, v10, v8}, Lcom/google/android/flexbox/FlexboxHelper;->stretchViewHorizontally(Landroid/view/View;II)V

    goto/32 :goto_64

    nop

    :goto_33
    add-int/lit8 v7, v7, 0x1

    goto/32 :goto_40

    nop

    :goto_34
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    goto/32 :goto_51

    nop

    :goto_35
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    goto/32 :goto_2

    nop

    :goto_36
    if-nez v1, :cond_7

    goto/32 :goto_4c

    :cond_7
    goto/32 :goto_5d

    nop

    :goto_37
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    goto/32 :goto_2b

    nop

    :goto_38
    iget v10, v5, Lcom/google/android/flexbox/FlexLine;->mCrossSize:I

    goto/32 :goto_1a

    nop

    :goto_39
    iget v10, v5, Lcom/google/android/flexbox/FlexLine;->mCrossSize:I

    goto/32 :goto_32

    nop

    :goto_3a
    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_1e

    nop

    :goto_3b
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    goto/32 :goto_18

    nop

    :goto_3c
    goto/16 :goto_1b

    :goto_3d
    goto/32 :goto_44

    nop

    :goto_3e
    const/4 v12, -0x1

    goto/32 :goto_46

    nop

    :goto_3f
    new-instance p1, Ljava/lang/IllegalArgumentException;

    goto/32 :goto_a

    nop

    :goto_40
    goto :goto_53

    :goto_41
    goto/32 :goto_42

    nop

    :goto_42
    add-int/lit8 p1, p1, 0x1

    goto/32 :goto_15

    nop

    :goto_43
    iget-object p1, p0, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    goto/32 :goto_1f

    nop

    :goto_44
    invoke-virtual {v9}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v10

    goto/32 :goto_6c

    nop

    :goto_45
    invoke-interface {v9, v8}, Lcom/google/android/flexbox/FlexContainer;->getReorderedFlexItemAt(I)Landroid/view/View;

    move-result-object v9

    goto/32 :goto_63

    nop

    :goto_46
    if-ne v11, v12, :cond_8

    goto/32 :goto_1

    :cond_8
    goto/32 :goto_e

    nop

    :goto_47
    iget-object v1, p0, Lcom/google/android/flexbox/FlexboxHelper;->mIndexToFlexLine:[I

    goto/32 :goto_66

    nop

    :goto_48
    invoke-interface {v10}, Lcom/google/android/flexbox/FlexItem;->getAlignSelf()I

    move-result v11

    goto/32 :goto_3e

    nop

    :goto_49
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_3a

    nop

    :goto_4a
    if-ne v10, v2, :cond_9

    goto/32 :goto_1

    :cond_9
    goto/32 :goto_0

    nop

    :goto_4b
    goto :goto_5f

    :goto_4c
    goto/32 :goto_8

    nop

    :goto_4d
    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    goto/32 :goto_54

    nop

    :goto_4e
    goto/16 :goto_1b

    :goto_4f
    goto/32 :goto_56

    nop

    :goto_50
    invoke-virtual {v9}, Landroid/view/View;->getVisibility()I

    move-result v10

    goto/32 :goto_68

    nop

    :goto_51
    invoke-direct {p0, v4, v5, v3}, Lcom/google/android/flexbox/FlexboxHelper;->stretchViewHorizontally(Landroid/view/View;II)V

    goto/32 :goto_2e

    nop

    :goto_52
    move v7, v3

    :goto_53
    goto/32 :goto_7

    nop

    :goto_54
    check-cast v5, Lcom/google/android/flexbox/FlexLine;

    goto/32 :goto_31

    nop

    :goto_55
    iget-object v2, v1, Lcom/google/android/flexbox/FlexLine;->mIndicesAlignSelfStretch:Ljava/util/List;

    goto/32 :goto_5e

    nop

    :goto_56
    iget-object v9, p0, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    goto/32 :goto_45

    nop

    :goto_57
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_20

    nop

    :goto_58
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_2a

    nop

    :goto_59
    aget p1, v1, p1

    goto/32 :goto_5a

    nop

    :goto_5a
    goto :goto_6a

    :goto_5b
    goto/32 :goto_69

    nop

    :goto_5c
    invoke-interface {v1}, Lcom/google/android/flexbox/FlexContainer;->getAlignItems()I

    move-result v1

    goto/32 :goto_21

    nop

    :goto_5d
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_6b

    nop

    :goto_5e
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_5f
    goto/32 :goto_37

    nop

    :goto_60
    iget v5, v1, Lcom/google/android/flexbox/FlexLine;->mCrossSize:I

    goto/32 :goto_34

    nop

    :goto_61
    iget-object v0, p0, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    goto/32 :goto_24

    nop

    :goto_62
    if-ge p1, v0, :cond_a

    goto/32 :goto_11

    :cond_a
    goto/32 :goto_10

    nop

    :goto_63
    if-nez v9, :cond_b

    goto/32 :goto_1b

    :cond_b
    goto/32 :goto_50

    nop

    :goto_64
    goto/16 :goto_1b

    :pswitch_2
    goto/32 :goto_38

    nop

    :goto_65
    iget-object v9, p0, Lcom/google/android/flexbox/FlexboxHelper;->mFlexContainer:Lcom/google/android/flexbox/FlexContainer;

    goto/32 :goto_67

    nop

    :goto_66
    const/4 v3, 0x0

    goto/32 :goto_14

    nop

    :goto_67
    invoke-interface {v9}, Lcom/google/android/flexbox/FlexContainer;->getFlexItemCount()I

    move-result v9

    goto/32 :goto_1d

    nop

    :goto_68
    const/16 v11, 0x8

    goto/32 :goto_25

    nop

    :goto_69
    move p1, v3

    :goto_6a
    goto/32 :goto_26

    nop

    :goto_6b
    check-cast v1, Lcom/google/android/flexbox/FlexLine;

    goto/32 :goto_55

    nop

    :goto_6c
    check-cast v10, Lcom/google/android/flexbox/FlexItem;

    goto/32 :goto_48

    nop

    :goto_6d
    throw p1

    :pswitch_3
    goto/32 :goto_39

    nop

    :goto_6e
    const-string v2, "Invalid flex direction: "

    goto/32 :goto_17

    nop

    :goto_6f
    const-string v2, "Invalid flex direction: "

    goto/32 :goto_58

    nop
.end method
