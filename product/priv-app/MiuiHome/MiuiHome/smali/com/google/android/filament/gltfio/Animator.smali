.class public Lcom/google/android/filament/gltfio/Animator;
.super Ljava/lang/Object;


# instance fields
.field private mNativeObject:J


# direct methods
.method constructor <init>(J)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/google/android/filament/gltfio/Animator;->mNativeObject:J

    return-void
.end method

.method private static native nApplyAnimation(JIF)V
.end method

.method private static native nGetAnimationCount(J)I
.end method

.method private static native nGetAnimationDuration(JI)F
.end method

.method private static native nGetAnimationName(JI)Ljava/lang/String;
.end method

.method private static native nUpdateBoneMatrices(J)V
.end method


# virtual methods
.method public applyAnimation(IF)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/filament/gltfio/Animator;->getNativeObject()J

    move-result-wide v0

    invoke-static {v0, v1, p1, p2}, Lcom/google/android/filament/gltfio/Animator;->nApplyAnimation(JIF)V

    return-void
.end method

.method clearNativeObject()V
    .locals 2

    goto/32 :goto_2

    nop

    :goto_0
    return-void

    :goto_1
    iput-wide v0, p0, Lcom/google/android/filament/gltfio/Animator;->mNativeObject:J

    goto/32 :goto_0

    nop

    :goto_2
    const-wide/16 v0, 0x0

    goto/32 :goto_1

    nop
.end method

.method public getAnimationCount()I
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/filament/gltfio/Animator;->getNativeObject()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/filament/gltfio/Animator;->nGetAnimationCount(J)I

    move-result v0

    return v0
.end method

.method public getAnimationDuration(I)F
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/filament/gltfio/Animator;->getNativeObject()J

    move-result-wide v0

    invoke-static {v0, v1, p1}, Lcom/google/android/filament/gltfio/Animator;->nGetAnimationDuration(JI)F

    move-result p1

    return p1
.end method

.method public getAnimationName(I)Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/filament/gltfio/Animator;->getNativeObject()J

    move-result-wide v0

    invoke-static {v0, v1, p1}, Lcom/google/android/filament/gltfio/Animator;->nGetAnimationName(JI)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method getNativeObject()J
    .locals 4

    goto/32 :goto_7

    nop

    :goto_0
    return-wide v0

    :goto_1
    goto/32 :goto_5

    nop

    :goto_2
    const-string v1, "Using Animator on destroyed asset"

    goto/32 :goto_6

    nop

    :goto_3
    cmp-long v2, v0, v2

    goto/32 :goto_9

    nop

    :goto_4
    throw v0

    :goto_5
    new-instance v0, Ljava/lang/IllegalStateException;

    goto/32 :goto_2

    nop

    :goto_6
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_4

    nop

    :goto_7
    iget-wide v0, p0, Lcom/google/android/filament/gltfio/Animator;->mNativeObject:J

    goto/32 :goto_8

    nop

    :goto_8
    const-wide/16 v2, 0x0

    goto/32 :goto_3

    nop

    :goto_9
    if-nez v2, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_0

    nop
.end method

.method public updateBoneMatrices()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/filament/gltfio/Animator;->getNativeObject()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/filament/gltfio/Animator;->nUpdateBoneMatrices(J)V

    return-void
.end method
