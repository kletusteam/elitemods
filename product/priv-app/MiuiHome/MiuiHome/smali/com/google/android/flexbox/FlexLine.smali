.class public Lcom/google/android/flexbox/FlexLine;
.super Ljava/lang/Object;


# instance fields
.field mAnyItemsHaveFlexGrow:Z

.field mAnyItemsHaveFlexShrink:Z

.field mBottom:I

.field mCrossSize:I

.field mDividerLengthInMainSize:I

.field mFirstIndex:I

.field mGoneItemCount:I

.field mIndicesAlignSelfStretch:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field mItemCount:I

.field mLastIndex:I

.field mLeft:I

.field mMainSize:I

.field mMaxBaseline:I

.field mRight:I

.field mSumCrossSizeBefore:I

.field mTop:I

.field mTotalFlexGrow:F

.field mTotalFlexShrink:F


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const v0, 0x7fffffff

    iput v0, p0, Lcom/google/android/flexbox/FlexLine;->mLeft:I

    iput v0, p0, Lcom/google/android/flexbox/FlexLine;->mTop:I

    const/high16 v0, -0x80000000

    iput v0, p0, Lcom/google/android/flexbox/FlexLine;->mRight:I

    iput v0, p0, Lcom/google/android/flexbox/FlexLine;->mBottom:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/flexbox/FlexLine;->mIndicesAlignSelfStretch:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public getCrossSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/flexbox/FlexLine;->mCrossSize:I

    return v0
.end method

.method public getItemCount()I
    .locals 1

    iget v0, p0, Lcom/google/android/flexbox/FlexLine;->mItemCount:I

    return v0
.end method

.method public getItemCountNotGone()I
    .locals 2

    iget v0, p0, Lcom/google/android/flexbox/FlexLine;->mItemCount:I

    iget v1, p0, Lcom/google/android/flexbox/FlexLine;->mGoneItemCount:I

    sub-int/2addr v0, v1

    return v0
.end method

.method updatePositionFromView(Landroid/view/View;IIII)V
    .locals 4

    goto/32 :goto_1c

    nop

    :goto_0
    iget p2, p0, Lcom/google/android/flexbox/FlexLine;->mBottom:I

    goto/32 :goto_2

    nop

    :goto_1
    invoke-static {p2, p3}, Ljava/lang/Math;->max(II)I

    move-result p2

    goto/32 :goto_1a

    nop

    :goto_2
    invoke-virtual {p1}, Landroid/view/View;->getBottom()I

    move-result p1

    goto/32 :goto_a

    nop

    :goto_3
    invoke-virtual {p1}, Landroid/view/View;->getRight()I

    move-result p3

    goto/32 :goto_9

    nop

    :goto_4
    invoke-static {p2, p1}, Ljava/lang/Math;->max(II)I

    move-result p1

    goto/32 :goto_17

    nop

    :goto_5
    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result p2

    goto/32 :goto_7

    nop

    :goto_6
    iput p2, p0, Lcom/google/android/flexbox/FlexLine;->mTop:I

    goto/32 :goto_8

    nop

    :goto_7
    iput p2, p0, Lcom/google/android/flexbox/FlexLine;->mLeft:I

    goto/32 :goto_c

    nop

    :goto_8
    iget p2, p0, Lcom/google/android/flexbox/FlexLine;->mRight:I

    goto/32 :goto_3

    nop

    :goto_9
    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->getMarginRight()I

    move-result v1

    goto/32 :goto_16

    nop

    :goto_a
    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->getMarginBottom()I

    move-result p3

    goto/32 :goto_15

    nop

    :goto_b
    sub-int/2addr v1, v2

    goto/32 :goto_1d

    nop

    :goto_c
    iget p2, p0, Lcom/google/android/flexbox/FlexLine;->mTop:I

    goto/32 :goto_19

    nop

    :goto_d
    iget v1, p0, Lcom/google/android/flexbox/FlexLine;->mLeft:I

    goto/32 :goto_13

    nop

    :goto_e
    invoke-static {p2, v1}, Ljava/lang/Math;->min(II)I

    move-result p2

    goto/32 :goto_6

    nop

    :goto_f
    add-int/2addr p3, p4

    goto/32 :goto_1

    nop

    :goto_10
    sub-int/2addr v2, p2

    goto/32 :goto_5

    nop

    :goto_11
    sub-int/2addr v2, v3

    goto/32 :goto_10

    nop

    :goto_12
    return-void

    :goto_13
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v2

    goto/32 :goto_18

    nop

    :goto_14
    check-cast v0, Lcom/google/android/flexbox/FlexItem;

    goto/32 :goto_d

    nop

    :goto_15
    add-int/2addr p1, p3

    goto/32 :goto_1b

    nop

    :goto_16
    add-int/2addr p3, v1

    goto/32 :goto_f

    nop

    :goto_17
    iput p1, p0, Lcom/google/android/flexbox/FlexLine;->mBottom:I

    goto/32 :goto_12

    nop

    :goto_18
    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->getMarginLeft()I

    move-result v3

    goto/32 :goto_11

    nop

    :goto_19
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v1

    goto/32 :goto_1e

    nop

    :goto_1a
    iput p2, p0, Lcom/google/android/flexbox/FlexLine;->mRight:I

    goto/32 :goto_0

    nop

    :goto_1b
    add-int/2addr p1, p5

    goto/32 :goto_4

    nop

    :goto_1c
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    goto/32 :goto_14

    nop

    :goto_1d
    sub-int/2addr v1, p3

    goto/32 :goto_e

    nop

    :goto_1e
    invoke-interface {v0}, Lcom/google/android/flexbox/FlexItem;->getMarginTop()I

    move-result v2

    goto/32 :goto_b

    nop
.end method
