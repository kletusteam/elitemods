.class public final enum Lcom/google/android/filament/utils/VectorComponent;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/google/android/filament/utils/VectorComponent;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/filament/utils/VectorComponent;

.field public static final enum A:Lcom/google/android/filament/utils/VectorComponent;

.field public static final enum B:Lcom/google/android/filament/utils/VectorComponent;

.field public static final enum G:Lcom/google/android/filament/utils/VectorComponent;

.field public static final enum P:Lcom/google/android/filament/utils/VectorComponent;

.field public static final enum Q:Lcom/google/android/filament/utils/VectorComponent;

.field public static final enum R:Lcom/google/android/filament/utils/VectorComponent;

.field public static final enum S:Lcom/google/android/filament/utils/VectorComponent;

.field public static final enum T:Lcom/google/android/filament/utils/VectorComponent;

.field public static final enum W:Lcom/google/android/filament/utils/VectorComponent;

.field public static final enum X:Lcom/google/android/filament/utils/VectorComponent;

.field public static final enum Y:Lcom/google/android/filament/utils/VectorComponent;

.field public static final enum Z:Lcom/google/android/filament/utils/VectorComponent;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/16 v0, 0xc

    new-array v0, v0, [Lcom/google/android/filament/utils/VectorComponent;

    new-instance v1, Lcom/google/android/filament/utils/VectorComponent;

    const-string v2, "X"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/google/android/filament/utils/VectorComponent;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/google/android/filament/utils/VectorComponent;->X:Lcom/google/android/filament/utils/VectorComponent;

    aput-object v1, v0, v3

    new-instance v1, Lcom/google/android/filament/utils/VectorComponent;

    const-string v2, "Y"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/google/android/filament/utils/VectorComponent;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/google/android/filament/utils/VectorComponent;->Y:Lcom/google/android/filament/utils/VectorComponent;

    aput-object v1, v0, v3

    new-instance v1, Lcom/google/android/filament/utils/VectorComponent;

    const-string v2, "Z"

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, Lcom/google/android/filament/utils/VectorComponent;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/google/android/filament/utils/VectorComponent;->Z:Lcom/google/android/filament/utils/VectorComponent;

    aput-object v1, v0, v3

    new-instance v1, Lcom/google/android/filament/utils/VectorComponent;

    const-string v2, "W"

    const/4 v3, 0x3

    invoke-direct {v1, v2, v3}, Lcom/google/android/filament/utils/VectorComponent;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/google/android/filament/utils/VectorComponent;->W:Lcom/google/android/filament/utils/VectorComponent;

    aput-object v1, v0, v3

    new-instance v1, Lcom/google/android/filament/utils/VectorComponent;

    const-string v2, "R"

    const/4 v3, 0x4

    invoke-direct {v1, v2, v3}, Lcom/google/android/filament/utils/VectorComponent;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/google/android/filament/utils/VectorComponent;->R:Lcom/google/android/filament/utils/VectorComponent;

    aput-object v1, v0, v3

    new-instance v1, Lcom/google/android/filament/utils/VectorComponent;

    const-string v2, "G"

    const/4 v3, 0x5

    invoke-direct {v1, v2, v3}, Lcom/google/android/filament/utils/VectorComponent;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/google/android/filament/utils/VectorComponent;->G:Lcom/google/android/filament/utils/VectorComponent;

    aput-object v1, v0, v3

    new-instance v1, Lcom/google/android/filament/utils/VectorComponent;

    const-string v2, "B"

    const/4 v3, 0x6

    invoke-direct {v1, v2, v3}, Lcom/google/android/filament/utils/VectorComponent;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/google/android/filament/utils/VectorComponent;->B:Lcom/google/android/filament/utils/VectorComponent;

    aput-object v1, v0, v3

    new-instance v1, Lcom/google/android/filament/utils/VectorComponent;

    const-string v2, "A"

    const/4 v3, 0x7

    invoke-direct {v1, v2, v3}, Lcom/google/android/filament/utils/VectorComponent;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/google/android/filament/utils/VectorComponent;->A:Lcom/google/android/filament/utils/VectorComponent;

    aput-object v1, v0, v3

    new-instance v1, Lcom/google/android/filament/utils/VectorComponent;

    const-string v2, "S"

    const/16 v3, 0x8

    invoke-direct {v1, v2, v3}, Lcom/google/android/filament/utils/VectorComponent;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/google/android/filament/utils/VectorComponent;->S:Lcom/google/android/filament/utils/VectorComponent;

    aput-object v1, v0, v3

    new-instance v1, Lcom/google/android/filament/utils/VectorComponent;

    const-string v2, "T"

    const/16 v3, 0x9

    invoke-direct {v1, v2, v3}, Lcom/google/android/filament/utils/VectorComponent;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/google/android/filament/utils/VectorComponent;->T:Lcom/google/android/filament/utils/VectorComponent;

    aput-object v1, v0, v3

    new-instance v1, Lcom/google/android/filament/utils/VectorComponent;

    const-string v2, "P"

    const/16 v3, 0xa

    invoke-direct {v1, v2, v3}, Lcom/google/android/filament/utils/VectorComponent;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/google/android/filament/utils/VectorComponent;->P:Lcom/google/android/filament/utils/VectorComponent;

    aput-object v1, v0, v3

    new-instance v1, Lcom/google/android/filament/utils/VectorComponent;

    const-string v2, "Q"

    const/16 v3, 0xb

    invoke-direct {v1, v2, v3}, Lcom/google/android/filament/utils/VectorComponent;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/google/android/filament/utils/VectorComponent;->Q:Lcom/google/android/filament/utils/VectorComponent;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/filament/utils/VectorComponent;->$VALUES:[Lcom/google/android/filament/utils/VectorComponent;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/filament/utils/VectorComponent;
    .locals 1

    const-class v0, Lcom/google/android/filament/utils/VectorComponent;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/google/android/filament/utils/VectorComponent;

    return-object p0
.end method

.method public static values()[Lcom/google/android/filament/utils/VectorComponent;
    .locals 1

    sget-object v0, Lcom/google/android/filament/utils/VectorComponent;->$VALUES:[Lcom/google/android/filament/utils/VectorComponent;

    invoke-virtual {v0}, [Lcom/google/android/filament/utils/VectorComponent;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/filament/utils/VectorComponent;

    return-object v0
.end method
