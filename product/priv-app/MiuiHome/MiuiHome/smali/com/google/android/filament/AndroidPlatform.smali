.class final Lcom/google/android/filament/AndroidPlatform;
.super Lcom/google/android/filament/Platform;


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "Filament"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    invoke-static {v0}, Landroid/opengl/EGL14;->eglGetDisplay(I)Landroid/opengl/EGLDisplay;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/filament/Platform;-><init>()V

    return-void
.end method


# virtual methods
.method getSharedContextNativeHandle(Ljava/lang/Object;)J
    .locals 4

    goto/32 :goto_b

    nop

    :goto_0
    invoke-static {v0, v1, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/32 :goto_2

    nop

    :goto_1
    if-ge v0, v1, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_7

    nop

    :goto_2
    const-wide/16 v0, 0x0

    goto/32 :goto_5

    nop

    :goto_3
    return-wide v0

    :goto_4
    :try_start_0
    const-class v0, Landroid/opengl/EGLContext;

    const-string v1, "getHandle"

    const/4 v2, 0x0

    new-array v3, v2, [Ljava/lang/Class;

    invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    new-array v1, v2, [Ljava/lang/Object;

    invoke-virtual {v0, p1, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->longValue()J

    move-result-wide v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_a

    nop

    :goto_5
    return-wide v0

    :goto_6
    const-string v1, "Could not access shared context\'s native handle"

    goto/32 :goto_0

    nop

    :goto_7
    invoke-static {p1}, Lcom/google/android/filament/AndroidPlatform21;->getSharedContextNativeHandle(Ljava/lang/Object;)J

    move-result-wide v0

    goto/32 :goto_3

    nop

    :goto_8
    const-string v0, "Filament"

    goto/32 :goto_6

    nop

    :goto_9
    const/16 v1, 0x15

    goto/32 :goto_1

    nop

    :goto_a
    return-wide v0

    :catch_0
    move-exception p1

    goto/32 :goto_8

    nop

    :goto_b
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    goto/32 :goto_9

    nop
.end method

.method log(Ljava/lang/String;)V
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_2

    nop

    :goto_1
    const-string v0, "Filament"

    goto/32 :goto_0

    nop

    :goto_2
    return-void
.end method

.method validateSharedContext(Ljava/lang/Object;)Z
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    instance-of p1, p1, Landroid/opengl/EGLContext;

    goto/32 :goto_1

    nop

    :goto_1
    return p1
.end method

.method validateStreamSource(Ljava/lang/Object;)Z
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    instance-of p1, p1, Landroid/graphics/SurfaceTexture;

    goto/32 :goto_1

    nop

    :goto_1
    return p1
.end method

.method validateSurface(Ljava/lang/Object;)Z
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    instance-of p1, p1, Landroid/view/Surface;

    goto/32 :goto_1

    nop

    :goto_1
    return p1
.end method

.method warn(Ljava/lang/String;)V
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    const-string v0, "Filament"

    goto/32 :goto_2

    nop

    :goto_2
    invoke-static {v0, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_0

    nop
.end method
