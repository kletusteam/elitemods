.class Lcom/mi/globallauncher/view/GeneralDialog$CommonDialogParam;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mi/globallauncher/view/GeneralDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CommonDialogParam"
.end annotation


# instance fields
.field public activity:Landroid/app/Activity;

.field public animation:I

.field content:Ljava/lang/String;

.field contentRes:I

.field customView:I

.field public disAmount:F

.field enableMask:Z

.field public gravity:I

.field negativeButtonListener:Landroid/view/View$OnClickListener;

.field negativeButtonText:I

.field negativeButtonTextColor:I

.field onDismissListener:Landroid/content/DialogInterface$OnDismissListener;

.field onShowListener:Landroid/content/DialogInterface$OnShowListener;

.field positiveButtonListener:Landroid/view/View$OnClickListener;

.field positiveButtonText:I

.field positiveButtonTextColor:I

.field public title:I


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x50

    iput v0, p0, Lcom/mi/globallauncher/view/GeneralDialog$CommonDialogParam;->gravity:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/mi/globallauncher/view/GeneralDialog$CommonDialogParam;->positiveButtonText:I

    iput v0, p0, Lcom/mi/globallauncher/view/GeneralDialog$CommonDialogParam;->negativeButtonText:I

    iput v0, p0, Lcom/mi/globallauncher/view/GeneralDialog$CommonDialogParam;->positiveButtonTextColor:I

    iput v0, p0, Lcom/mi/globallauncher/view/GeneralDialog$CommonDialogParam;->negativeButtonTextColor:I

    iput v0, p0, Lcom/mi/globallauncher/view/GeneralDialog$CommonDialogParam;->title:I

    iput v0, p0, Lcom/mi/globallauncher/view/GeneralDialog$CommonDialogParam;->customView:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/mi/globallauncher/view/GeneralDialog$1;)V
    .locals 0

    invoke-direct {p0}, Lcom/mi/globallauncher/view/GeneralDialog$CommonDialogParam;-><init>()V

    return-void
.end method


# virtual methods
.method public hasAnimation()Z
    .locals 2

    iget v0, p0, Lcom/mi/globallauncher/view/GeneralDialog$CommonDialogParam;->animation:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method hasNegativeButton()Z
    .locals 2

    goto/32 :goto_2

    nop

    :goto_0
    const/4 v1, -0x1

    goto/32 :goto_8

    nop

    :goto_1
    return v0

    :goto_2
    iget v0, p0, Lcom/mi/globallauncher/view/GeneralDialog$CommonDialogParam;->negativeButtonText:I

    goto/32 :goto_0

    nop

    :goto_3
    const/4 v0, 0x1

    goto/32 :goto_6

    nop

    :goto_4
    const/4 v0, 0x0

    :goto_5
    goto/32 :goto_1

    nop

    :goto_6
    goto :goto_5

    :goto_7
    goto/32 :goto_4

    nop

    :goto_8
    if-ne v0, v1, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_3

    nop
.end method

.method hasPositiveButton()Z
    .locals 2

    goto/32 :goto_4

    nop

    :goto_0
    const/4 v0, 0x1

    goto/32 :goto_5

    nop

    :goto_1
    const/4 v1, -0x1

    goto/32 :goto_7

    nop

    :goto_2
    const/4 v0, 0x0

    :goto_3
    goto/32 :goto_8

    nop

    :goto_4
    iget v0, p0, Lcom/mi/globallauncher/view/GeneralDialog$CommonDialogParam;->positiveButtonText:I

    goto/32 :goto_1

    nop

    :goto_5
    goto :goto_3

    :goto_6
    goto/32 :goto_2

    nop

    :goto_7
    if-ne v0, v1, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_0

    nop

    :goto_8
    return v0
.end method

.method public isCustomView()Z
    .locals 2

    iget v0, p0, Lcom/mi/globallauncher/view/GeneralDialog$CommonDialogParam;->customView:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
