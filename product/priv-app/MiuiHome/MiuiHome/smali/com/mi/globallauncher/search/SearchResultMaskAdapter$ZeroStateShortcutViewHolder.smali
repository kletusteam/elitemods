.class Lcom/mi/globallauncher/search/SearchResultMaskAdapter$ZeroStateShortcutViewHolder;
.super Lcom/mi/globallauncher/search/SearchResultMaskAdapter$ViewHolder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mi/globallauncher/search/SearchResultMaskAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ZeroStateShortcutViewHolder"
.end annotation


# instance fields
.field container:Landroid/view/View;

.field final synthetic this$0:Lcom/mi/globallauncher/search/SearchResultMaskAdapter;

.field zeroStateDesc:Landroid/widget/TextView;

.field zeroStateIcon:Landroid/widget/ImageView;

.field zeroStateSecondIcon:Landroid/widget/ImageView;

.field zeroStateTitle:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Lcom/mi/globallauncher/search/SearchResultMaskAdapter;Landroid/view/View;)V
    .locals 1

    iput-object p1, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$ZeroStateShortcutViewHolder;->this$0:Lcom/mi/globallauncher/search/SearchResultMaskAdapter;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$ViewHolder;-><init>(Lcom/mi/globallauncher/search/SearchResultMaskAdapter;Lcom/mi/globallauncher/search/SearchResultMaskAdapter$1;)V

    iput-object p2, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$ZeroStateShortcutViewHolder;->container:Landroid/view/View;

    sget p1, Lcom/mi/globallauncher/R$id;->zero_state_icon:I

    invoke-virtual {p2, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$ZeroStateShortcutViewHolder;->zeroStateIcon:Landroid/widget/ImageView;

    sget p1, Lcom/mi/globallauncher/R$id;->text:I

    invoke-virtual {p2, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$ZeroStateShortcutViewHolder;->zeroStateTitle:Landroid/widget/TextView;

    sget p1, Lcom/mi/globallauncher/R$id;->desc:I

    invoke-virtual {p2, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$ZeroStateShortcutViewHolder;->zeroStateDesc:Landroid/widget/TextView;

    sget p1, Lcom/mi/globallauncher/R$id;->second_app_icon:I

    invoke-virtual {p2, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$ZeroStateShortcutViewHolder;->zeroStateSecondIcon:Landroid/widget/ImageView;

    return-void
.end method

.method public static synthetic lambda$updateView$0(Lcom/mi/globallauncher/search/SearchResultMaskAdapter$ZeroStateShortcutViewHolder;Lio/branch/search/ui/BranchEntity;Landroid/view/View;)V
    .locals 0

    iget-object p2, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$ZeroStateShortcutViewHolder;->container:Landroid/view/View;

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-interface {p1, p2}, Lio/branch/search/ui/BranchEntity;->open(Landroid/content/Context;)Lio/branch/search/BranchError;

    return-void
.end method


# virtual methods
.method updateView(I)V
    .locals 4

    goto/32 :goto_44

    nop

    :goto_0
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$ZeroStateShortcutViewHolder;->this$0:Lcom/mi/globallauncher/search/SearchResultMaskAdapter;

    goto/32 :goto_35

    nop

    :goto_1
    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    goto/32 :goto_2e

    nop

    :goto_2
    invoke-interface {p1}, Lio/branch/search/ui/BranchEntity;->getSecondaryImage()Lio/branch/search/ui/Image;

    move-result-object v0

    goto/32 :goto_f

    nop

    :goto_3
    sget v2, Lcom/mi/globallauncher/R$color;->alpha90black:I

    goto/32 :goto_11

    nop

    :goto_4
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/32 :goto_1d

    nop

    :goto_5
    if-nez v0, :cond_0

    goto/32 :goto_41

    :cond_0
    goto/32 :goto_7

    nop

    :goto_6
    iget-object v1, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$ZeroStateShortcutViewHolder;->container:Landroid/view/View;

    goto/32 :goto_c

    nop

    :goto_7
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$ZeroStateShortcutViewHolder;->zeroStateDesc:Landroid/widget/TextView;

    goto/32 :goto_25

    nop

    :goto_8
    sget v2, Lcom/mi/globallauncher/R$color;->alpha50black:I

    goto/32 :goto_4f

    nop

    :goto_9
    iget-object v3, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$ZeroStateShortcutViewHolder;->zeroStateIcon:Landroid/widget/ImageView;

    goto/32 :goto_16

    nop

    :goto_a
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$ZeroStateShortcutViewHolder;->this$0:Lcom/mi/globallauncher/search/SearchResultMaskAdapter;

    goto/32 :goto_39

    nop

    :goto_b
    iget-object v1, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$ZeroStateShortcutViewHolder;->container:Landroid/view/View;

    goto/32 :goto_17

    nop

    :goto_c
    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    goto/32 :goto_8

    nop

    :goto_d
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$ZeroStateShortcutViewHolder;->zeroStateDesc:Landroid/widget/TextView;

    goto/32 :goto_4c

    nop

    :goto_e
    invoke-static {v1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    goto/32 :goto_4d

    nop

    :goto_f
    iget-object v2, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$ZeroStateShortcutViewHolder;->zeroStateSecondIcon:Landroid/widget/ImageView;

    goto/32 :goto_21

    nop

    :goto_10
    invoke-direct {v1, p0, p1}, Lcom/mi/globallauncher/search/-$$Lambda$SearchResultMaskAdapter$ZeroStateShortcutViewHolder$mKN3mZ81JL3keInXdwfVk8hOOKg;-><init>(Lcom/mi/globallauncher/search/SearchResultMaskAdapter$ZeroStateShortcutViewHolder;Lio/branch/search/ui/BranchEntity;)V

    goto/32 :goto_48

    nop

    :goto_11
    invoke-static {v1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    goto/32 :goto_24

    nop

    :goto_12
    check-cast p1, Lio/branch/search/ui/BranchEntity;

    goto/32 :goto_43

    nop

    :goto_13
    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    :goto_14
    goto/32 :goto_34

    nop

    :goto_15
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$ZeroStateShortcutViewHolder;->zeroStateTitle:Landroid/widget/TextView;

    goto/32 :goto_b

    nop

    :goto_16
    invoke-interface {v0, v3}, Lio/branch/search/ui/Image;->load(Landroid/widget/ImageView;)V

    goto/32 :goto_2f

    nop

    :goto_17
    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    goto/32 :goto_3

    nop

    :goto_18
    sget v1, Lcom/mi/globallauncher/R$drawable;->branch_item_card_bg:I

    goto/32 :goto_20

    nop

    :goto_19
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto/32 :goto_27

    nop

    :goto_1a
    goto :goto_14

    :goto_1b
    goto/32 :goto_38

    nop

    :goto_1c
    invoke-interface {p1}, Lio/branch/search/ui/BranchEntity;->getPrimaryImage()Lio/branch/search/ui/Image;

    move-result-object v0

    goto/32 :goto_9

    nop

    :goto_1d
    goto/16 :goto_33

    :goto_1e
    goto/32 :goto_50

    nop

    :goto_1f
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$ZeroStateShortcutViewHolder;->zeroStateDesc:Landroid/widget/TextView;

    goto/32 :goto_26

    nop

    :goto_20
    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    goto/32 :goto_1a

    nop

    :goto_21
    invoke-interface {v0, v2}, Lio/branch/search/ui/Image;->load(Landroid/widget/ImageView;)V

    goto/32 :goto_3e

    nop

    :goto_22
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$ZeroStateShortcutViewHolder;->zeroStateDesc:Landroid/widget/TextView;

    goto/32 :goto_30

    nop

    :goto_23
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$ZeroStateShortcutViewHolder;->container:Landroid/view/View;

    goto/32 :goto_18

    nop

    :goto_24
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto/32 :goto_4b

    nop

    :goto_25
    invoke-interface {p1}, Lio/branch/search/ui/BranchEntity;->getDescription()Ljava/lang/String;

    move-result-object v3

    goto/32 :goto_36

    nop

    :goto_26
    iget-object v1, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$ZeroStateShortcutViewHolder;->container:Landroid/view/View;

    goto/32 :goto_1

    nop

    :goto_27
    if-lt p1, v0, :cond_1

    goto/32 :goto_49

    :cond_1
    goto/32 :goto_2d

    nop

    :goto_28
    if-nez v0, :cond_2

    goto/32 :goto_1b

    :cond_2
    goto/32 :goto_15

    nop

    :goto_29
    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    goto/32 :goto_45

    nop

    :goto_2a
    invoke-virtual {p1}, Lcom/mi/globallauncher/search/SearchResultMaskView$SearchMaskItem;->getData()Ljava/lang/Object;

    move-result-object p1

    goto/32 :goto_12

    nop

    :goto_2b
    invoke-static {v0}, Lcom/mi/globallauncher/search/SearchResultMaskAdapter;->access$300(Lcom/mi/globallauncher/search/SearchResultMaskAdapter;)Ljava/util/List;

    move-result-object v0

    goto/32 :goto_42

    nop

    :goto_2c
    new-instance v1, Lcom/mi/globallauncher/search/-$$Lambda$SearchResultMaskAdapter$ZeroStateShortcutViewHolder$mKN3mZ81JL3keInXdwfVk8hOOKg;

    goto/32 :goto_10

    nop

    :goto_2d
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$ZeroStateShortcutViewHolder;->this$0:Lcom/mi/globallauncher/search/SearchResultMaskAdapter;

    goto/32 :goto_2b

    nop

    :goto_2e
    sget v2, Lcom/mi/globallauncher/R$color;->alpha50white:I

    goto/32 :goto_3c

    nop

    :goto_2f
    invoke-interface {p1}, Lio/branch/search/ui/BranchEntity;->getSecondaryImage()Lio/branch/search/ui/Image;

    move-result-object v0

    goto/32 :goto_46

    nop

    :goto_30
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_31
    goto/32 :goto_1c

    nop

    :goto_32
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_33
    goto/32 :goto_0

    nop

    :goto_34
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$ZeroStateShortcutViewHolder;->container:Landroid/view/View;

    goto/32 :goto_2c

    nop

    :goto_35
    invoke-static {v0}, Lcom/mi/globallauncher/search/SearchResultMaskAdapter;->access$100(Lcom/mi/globallauncher/search/SearchResultMaskAdapter;)Z

    move-result v0

    goto/32 :goto_28

    nop

    :goto_36
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/32 :goto_d

    nop

    :goto_37
    iget-object v1, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$ZeroStateShortcutViewHolder;->container:Landroid/view/View;

    goto/32 :goto_29

    nop

    :goto_38
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$ZeroStateShortcutViewHolder;->zeroStateTitle:Landroid/widget/TextView;

    goto/32 :goto_37

    nop

    :goto_39
    invoke-static {v0}, Lcom/mi/globallauncher/search/SearchResultMaskAdapter;->access$300(Lcom/mi/globallauncher/search/SearchResultMaskAdapter;)Ljava/util/List;

    move-result-object v0

    goto/32 :goto_19

    nop

    :goto_3a
    return-void

    :goto_3b
    const/16 v2, 0x8

    goto/32 :goto_5

    nop

    :goto_3c
    invoke-static {v1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    goto/32 :goto_54

    nop

    :goto_3d
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto/32 :goto_23

    nop

    :goto_3e
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$ZeroStateShortcutViewHolder;->zeroStateSecondIcon:Landroid/widget/ImageView;

    goto/32 :goto_4

    nop

    :goto_3f
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/32 :goto_4a

    nop

    :goto_40
    goto :goto_31

    :goto_41
    goto/32 :goto_22

    nop

    :goto_42
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    goto/32 :goto_52

    nop

    :goto_43
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$ZeroStateShortcutViewHolder;->zeroStateTitle:Landroid/widget/TextView;

    goto/32 :goto_51

    nop

    :goto_44
    if-gez p1, :cond_3

    goto/32 :goto_49

    :cond_3
    goto/32 :goto_a

    nop

    :goto_45
    sget v2, Lcom/mi/globallauncher/R$color;->alpha90white:I

    goto/32 :goto_e

    nop

    :goto_46
    if-nez v0, :cond_4

    goto/32 :goto_1e

    :cond_4
    goto/32 :goto_2

    nop

    :goto_47
    sget v1, Lcom/mi/globallauncher/R$drawable;->branch_item_card_bg_dark:I

    goto/32 :goto_13

    nop

    :goto_48
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_49
    goto/32 :goto_3a

    nop

    :goto_4a
    invoke-interface {p1}, Lio/branch/search/ui/BranchEntity;->getDescription()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_53

    nop

    :goto_4b
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$ZeroStateShortcutViewHolder;->zeroStateDesc:Landroid/widget/TextView;

    goto/32 :goto_6

    nop

    :goto_4c
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto/32 :goto_40

    nop

    :goto_4d
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto/32 :goto_1f

    nop

    :goto_4e
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$ZeroStateShortcutViewHolder;->container:Landroid/view/View;

    goto/32 :goto_47

    nop

    :goto_4f
    invoke-static {v1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    goto/32 :goto_3d

    nop

    :goto_50
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$ZeroStateShortcutViewHolder;->zeroStateSecondIcon:Landroid/widget/ImageView;

    goto/32 :goto_32

    nop

    :goto_51
    invoke-interface {p1}, Lio/branch/search/ui/BranchEntity;->getTitle()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_3f

    nop

    :goto_52
    check-cast p1, Lcom/mi/globallauncher/search/SearchResultMaskView$SearchMaskItem;

    goto/32 :goto_2a

    nop

    :goto_53
    const/4 v1, 0x0

    goto/32 :goto_3b

    nop

    :goto_54
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto/32 :goto_4e

    nop
.end method
