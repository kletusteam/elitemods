.class Lcom/mi/globallauncher/search/SearchResultMaskAdapter$RecommendAppsViewHolder;
.super Lcom/mi/globallauncher/search/SearchResultMaskAdapter$ViewHolder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mi/globallauncher/search/SearchResultMaskAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "RecommendAppsViewHolder"
.end annotation


# instance fields
.field recommendContainer:Landroid/widget/LinearLayout;

.field recommendLine1:Landroid/widget/GridView;

.field recommendLine2:Landroid/widget/GridView;

.field recommendTitle:Landroid/widget/TextView;

.field final synthetic this$0:Lcom/mi/globallauncher/search/SearchResultMaskAdapter;


# direct methods
.method constructor <init>(Lcom/mi/globallauncher/search/SearchResultMaskAdapter;Landroid/view/View;)V
    .locals 1

    iput-object p1, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$RecommendAppsViewHolder;->this$0:Lcom/mi/globallauncher/search/SearchResultMaskAdapter;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$ViewHolder;-><init>(Lcom/mi/globallauncher/search/SearchResultMaskAdapter;Lcom/mi/globallauncher/search/SearchResultMaskAdapter$1;)V

    sget p1, Lcom/mi/globallauncher/R$id;->recommend_container:I

    invoke-virtual {p2, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$RecommendAppsViewHolder;->recommendContainer:Landroid/widget/LinearLayout;

    sget p1, Lcom/mi/globallauncher/R$id;->recommend_title:I

    invoke-virtual {p2, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$RecommendAppsViewHolder;->recommendTitle:Landroid/widget/TextView;

    sget p1, Lcom/mi/globallauncher/R$id;->recommend_line_1:I

    invoke-virtual {p2, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/GridView;

    iput-object p1, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$RecommendAppsViewHolder;->recommendLine1:Landroid/widget/GridView;

    sget p1, Lcom/mi/globallauncher/R$id;->recommend_line_2:I

    invoke-virtual {p2, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/GridView;

    iput-object p1, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$RecommendAppsViewHolder;->recommendLine2:Landroid/widget/GridView;

    return-void
.end method


# virtual methods
.method updateView(I)V
    .locals 2

    goto/32 :goto_13

    nop

    :goto_0
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$RecommendAppsViewHolder;->recommendContainer:Landroid/widget/LinearLayout;

    goto/32 :goto_14

    nop

    :goto_1
    invoke-static {v0}, Lcom/mi/globallauncher/search/SearchResultMaskAdapter;->access$800(Lcom/mi/globallauncher/search/SearchResultMaskAdapter;)Lcom/mi/globallauncher/advertise/adapter/RecommendGamesGridAdapter$RecommendGameClickListener;

    move-result-object v0

    goto/32 :goto_c

    nop

    :goto_2
    sget v1, Lcom/mi/globallauncher/R$drawable;->branch_item_card_bg:I

    goto/32 :goto_1e

    nop

    :goto_3
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/32 :goto_24

    nop

    :goto_4
    invoke-virtual {p1, v0}, Lcom/mi/globallauncher/advertise/adapter/RecommendGamesGridAdapter;->setRecommendGameItems(Ljava/util/List;)V

    goto/32 :goto_21

    nop

    :goto_5
    invoke-virtual {v0, p1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto/32 :goto_b

    nop

    :goto_6
    return-void

    :goto_7
    if-nez v0, :cond_0

    goto/32 :goto_1d

    :cond_0
    goto/32 :goto_16

    nop

    :goto_8
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$RecommendAppsViewHolder;->recommendLine1:Landroid/widget/GridView;

    goto/32 :goto_5

    nop

    :goto_9
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    goto/32 :goto_1c

    nop

    :goto_a
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$RecommendAppsViewHolder;->this$0:Lcom/mi/globallauncher/search/SearchResultMaskAdapter;

    goto/32 :goto_23

    nop

    :goto_b
    invoke-virtual {p1}, Lcom/mi/globallauncher/advertise/adapter/RecommendGamesGridAdapter;->notifyDataSetChanged()V

    goto/32 :goto_6

    nop

    :goto_c
    invoke-virtual {p1, v0}, Lcom/mi/globallauncher/advertise/adapter/RecommendGamesGridAdapter;->setGameClickListener(Lcom/mi/globallauncher/advertise/adapter/RecommendGamesGridAdapter$RecommendGameClickListener;)V

    goto/32 :goto_8

    nop

    :goto_d
    invoke-static {p1, v1}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p1

    goto/32 :goto_10

    nop

    :goto_e
    invoke-static {v0}, Lcom/mi/globallauncher/search/SearchResultMaskAdapter;->access$700(Lcom/mi/globallauncher/search/SearchResultMaskAdapter;)Ljava/util/List;

    move-result-object v0

    goto/32 :goto_4

    nop

    :goto_f
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/32 :goto_1a

    nop

    :goto_10
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    :goto_11
    goto/32 :goto_22

    nop

    :goto_12
    sget v1, Lcom/mi/globallauncher/R$color;->white:I

    goto/32 :goto_d

    nop

    :goto_13
    iget-object p1, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$RecommendAppsViewHolder;->recommendContainer:Landroid/widget/LinearLayout;

    goto/32 :goto_20

    nop

    :goto_14
    sget v1, Lcom/mi/globallauncher/R$drawable;->branch_item_card_bg_dark:I

    goto/32 :goto_25

    nop

    :goto_15
    sget v1, Lcom/mi/globallauncher/R$color;->black:I

    goto/32 :goto_1b

    nop

    :goto_16
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$RecommendAppsViewHolder;->recommendContainer:Landroid/widget/LinearLayout;

    goto/32 :goto_2

    nop

    :goto_17
    invoke-direct {p1, v0}, Lcom/mi/globallauncher/advertise/adapter/RecommendGamesGridAdapter;-><init>(Landroid/content/Context;)V

    goto/32 :goto_18

    nop

    :goto_18
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$RecommendAppsViewHolder;->this$0:Lcom/mi/globallauncher/search/SearchResultMaskAdapter;

    goto/32 :goto_e

    nop

    :goto_19
    invoke-static {v0}, Lcom/mi/globallauncher/search/SearchResultMaskAdapter;->access$200(Lcom/mi/globallauncher/search/SearchResultMaskAdapter;)Landroid/content/Context;

    move-result-object v0

    goto/32 :goto_17

    nop

    :goto_1a
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$RecommendAppsViewHolder;->recommendTitle:Landroid/widget/TextView;

    goto/32 :goto_12

    nop

    :goto_1b
    invoke-static {p1, v1}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p1

    goto/32 :goto_9

    nop

    :goto_1c
    goto :goto_11

    :goto_1d
    goto/32 :goto_0

    nop

    :goto_1e
    invoke-static {p1, v1}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    goto/32 :goto_3

    nop

    :goto_1f
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$RecommendAppsViewHolder;->this$0:Lcom/mi/globallauncher/search/SearchResultMaskAdapter;

    goto/32 :goto_19

    nop

    :goto_20
    invoke-virtual {p1}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    goto/32 :goto_a

    nop

    :goto_21
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$RecommendAppsViewHolder;->this$0:Lcom/mi/globallauncher/search/SearchResultMaskAdapter;

    goto/32 :goto_1

    nop

    :goto_22
    new-instance p1, Lcom/mi/globallauncher/advertise/adapter/RecommendGamesGridAdapter;

    goto/32 :goto_1f

    nop

    :goto_23
    invoke-static {v0}, Lcom/mi/globallauncher/search/SearchResultMaskAdapter;->access$100(Lcom/mi/globallauncher/search/SearchResultMaskAdapter;)Z

    move-result v0

    goto/32 :goto_7

    nop

    :goto_24
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$RecommendAppsViewHolder;->recommendTitle:Landroid/widget/TextView;

    goto/32 :goto_15

    nop

    :goto_25
    invoke-static {p1, v1}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    goto/32 :goto_f

    nop
.end method
