.class public final enum Lcom/mi/globallauncher/config/CommercialRemoteConfig;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mi/globallauncher/config/CommercialRemoteConfig$RemoteConfigListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/mi/globallauncher/config/CommercialRemoteConfig;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mi/globallauncher/config/CommercialRemoteConfig;

.field public static final enum mInstance:Lcom/mi/globallauncher/config/CommercialRemoteConfig;


# instance fields
.field private disabled:Z

.field private mFirebaseRemoteConfig:Lcom/google/firebase/remoteconfig/FirebaseRemoteConfig;

.field private mRemoteConfigListener:Lcom/mi/globallauncher/config/CommercialRemoteConfig$RemoteConfigListener;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/mi/globallauncher/config/CommercialRemoteConfig;

    const-string v1, "mInstance"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/mi/globallauncher/config/CommercialRemoteConfig;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mi/globallauncher/config/CommercialRemoteConfig;->mInstance:Lcom/mi/globallauncher/config/CommercialRemoteConfig;

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/mi/globallauncher/config/CommercialRemoteConfig;

    sget-object v1, Lcom/mi/globallauncher/config/CommercialRemoteConfig;->mInstance:Lcom/mi/globallauncher/config/CommercialRemoteConfig;

    aput-object v1, v0, v2

    sput-object v0, Lcom/mi/globallauncher/config/CommercialRemoteConfig;->$VALUES:[Lcom/mi/globallauncher/config/CommercialRemoteConfig;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/mi/globallauncher/config/CommercialRemoteConfig;->disabled:Z

    return-void
.end method

.method public static synthetic lambda$fetchRemoteConfig$0(Lcom/mi/globallauncher/config/CommercialRemoteConfig;Ljava/lang/Void;)V
    .locals 0

    const-string p1, "fetch remote config successful"

    invoke-static {p1}, Lcom/mi/globallauncher/util/CommercialLogger;->d(Ljava/lang/String;)V

    iget-object p1, p0, Lcom/mi/globallauncher/config/CommercialRemoteConfig;->mFirebaseRemoteConfig:Lcom/google/firebase/remoteconfig/FirebaseRemoteConfig;

    invoke-virtual {p1}, Lcom/google/firebase/remoteconfig/FirebaseRemoteConfig;->activateFetched()Z

    iget-object p1, p0, Lcom/mi/globallauncher/config/CommercialRemoteConfig;->mRemoteConfigListener:Lcom/mi/globallauncher/config/CommercialRemoteConfig$RemoteConfigListener;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/mi/globallauncher/config/CommercialRemoteConfig$RemoteConfigListener;->onFetchConfigSucceed()V

    :cond_0
    return-void
.end method

.method public static synthetic lambda$fetchRemoteConfig$1(Lcom/mi/globallauncher/config/CommercialRemoteConfig;Ljava/lang/Exception;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "fetch remote config failed: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/mi/globallauncher/util/CommercialLogger;->d(Ljava/lang/String;)V

    iget-object p1, p0, Lcom/mi/globallauncher/config/CommercialRemoteConfig;->mRemoteConfigListener:Lcom/mi/globallauncher/config/CommercialRemoteConfig$RemoteConfigListener;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/mi/globallauncher/config/CommercialRemoteConfig$RemoteConfigListener;->onFetchConfigFailed()V

    :cond_0
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mi/globallauncher/config/CommercialRemoteConfig;
    .locals 1

    const-class v0, Lcom/mi/globallauncher/config/CommercialRemoteConfig;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/mi/globallauncher/config/CommercialRemoteConfig;

    return-object p0
.end method

.method public static values()[Lcom/mi/globallauncher/config/CommercialRemoteConfig;
    .locals 1

    sget-object v0, Lcom/mi/globallauncher/config/CommercialRemoteConfig;->$VALUES:[Lcom/mi/globallauncher/config/CommercialRemoteConfig;

    invoke-virtual {v0}, [Lcom/mi/globallauncher/config/CommercialRemoteConfig;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mi/globallauncher/config/CommercialRemoteConfig;

    return-object v0
.end method


# virtual methods
.method public disable()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mi/globallauncher/config/CommercialRemoteConfig;->disabled:Z

    return-void
.end method

.method public enable()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mi/globallauncher/config/CommercialRemoteConfig;->disabled:Z

    return-void
.end method

.method public fetchRemoteConfig()V
    .locals 3

    invoke-virtual {p0}, Lcom/mi/globallauncher/config/CommercialRemoteConfig;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/mi/globallauncher/config/CommercialRemoteConfig;->disabled:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, "fetch remote config"

    invoke-static {v0}, Lcom/mi/globallauncher/util/CommercialLogger;->d(Ljava/lang/String;)V

    const-wide/16 v0, 0xe10

    iget-object v2, p0, Lcom/mi/globallauncher/config/CommercialRemoteConfig;->mFirebaseRemoteConfig:Lcom/google/firebase/remoteconfig/FirebaseRemoteConfig;

    invoke-virtual {v2}, Lcom/google/firebase/remoteconfig/FirebaseRemoteConfig;->getInfo()Lcom/google/firebase/remoteconfig/FirebaseRemoteConfigInfo;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/firebase/remoteconfig/FirebaseRemoteConfigInfo;->getConfigSettings()Lcom/google/firebase/remoteconfig/FirebaseRemoteConfigSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/firebase/remoteconfig/FirebaseRemoteConfigSettings;->isDeveloperModeEnabled()Z

    move-result v2

    if-eqz v2, :cond_1

    const-wide/16 v0, 0x0

    :cond_1
    iget-object v2, p0, Lcom/mi/globallauncher/config/CommercialRemoteConfig;->mFirebaseRemoteConfig:Lcom/google/firebase/remoteconfig/FirebaseRemoteConfig;

    invoke-virtual {v2, v0, v1}, Lcom/google/firebase/remoteconfig/FirebaseRemoteConfig;->fetch(J)Lcom/google/android/gms/tasks/Task;

    move-result-object v0

    new-instance v1, Lcom/mi/globallauncher/config/-$$Lambda$CommercialRemoteConfig$0gCIDPFjn51qS6C7Q3lAz7KCXzg;

    invoke-direct {v1, p0}, Lcom/mi/globallauncher/config/-$$Lambda$CommercialRemoteConfig$0gCIDPFjn51qS6C7Q3lAz7KCXzg;-><init>(Lcom/mi/globallauncher/config/CommercialRemoteConfig;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/tasks/Task;->addOnSuccessListener(Lcom/google/android/gms/tasks/OnSuccessListener;)Lcom/google/android/gms/tasks/Task;

    move-result-object v0

    new-instance v1, Lcom/mi/globallauncher/config/-$$Lambda$CommercialRemoteConfig$myqlnhMLIQDnfrivuBIs5ZehCro;

    invoke-direct {v1, p0}, Lcom/mi/globallauncher/config/-$$Lambda$CommercialRemoteConfig$myqlnhMLIQDnfrivuBIs5ZehCro;-><init>(Lcom/mi/globallauncher/config/CommercialRemoteConfig;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/tasks/Task;->addOnFailureListener(Lcom/google/android/gms/tasks/OnFailureListener;)Lcom/google/android/gms/tasks/Task;

    return-void

    :cond_2
    :goto_0
    return-void
.end method

.method public final getBoolean(Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 1

    iget-object v0, p0, Lcom/mi/globallauncher/config/CommercialRemoteConfig;->mFirebaseRemoteConfig:Lcom/google/firebase/remoteconfig/FirebaseRemoteConfig;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/google/firebase/remoteconfig/FirebaseRemoteConfig;->getBoolean(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public final getLong(Ljava/lang/String;)J
    .locals 2

    iget-object v0, p0, Lcom/mi/globallauncher/config/CommercialRemoteConfig;->mFirebaseRemoteConfig:Lcom/google/firebase/remoteconfig/FirebaseRemoteConfig;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/google/firebase/remoteconfig/FirebaseRemoteConfig;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    goto :goto_0

    :cond_0
    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0
.end method

.method public final getString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mi/globallauncher/config/CommercialRemoteConfig;->mFirebaseRemoteConfig:Lcom/google/firebase/remoteconfig/FirebaseRemoteConfig;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/google/firebase/remoteconfig/FirebaseRemoteConfig;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const-string p1, ""

    :goto_0
    return-object p1
.end method

.method public init()Lcom/mi/globallauncher/config/CommercialRemoteConfig;
    .locals 2

    iget-object v0, p0, Lcom/mi/globallauncher/config/CommercialRemoteConfig;->mFirebaseRemoteConfig:Lcom/google/firebase/remoteconfig/FirebaseRemoteConfig;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/firebase/remoteconfig/FirebaseRemoteConfig;->getInstance()Lcom/google/firebase/remoteconfig/FirebaseRemoteConfig;

    move-result-object v0

    iput-object v0, p0, Lcom/mi/globallauncher/config/CommercialRemoteConfig;->mFirebaseRemoteConfig:Lcom/google/firebase/remoteconfig/FirebaseRemoteConfig;

    :try_start_0
    new-instance v0, Lcom/google/firebase/remoteconfig/FirebaseRemoteConfigSettings$Builder;

    invoke-direct {v0}, Lcom/google/firebase/remoteconfig/FirebaseRemoteConfigSettings$Builder;-><init>()V

    invoke-static {}, Lcom/mi/globallauncher/manager/BranchImplement;->getInstance()Lcom/mi/globallauncher/manager/BranchImplement;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mi/globallauncher/manager/BranchImplement;->isDebug()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/firebase/remoteconfig/FirebaseRemoteConfigSettings$Builder;->setDeveloperModeEnabled(Z)Lcom/google/firebase/remoteconfig/FirebaseRemoteConfigSettings$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/firebase/remoteconfig/FirebaseRemoteConfigSettings$Builder;->build()Lcom/google/firebase/remoteconfig/FirebaseRemoteConfigSettings;

    move-result-object v0

    iget-object v1, p0, Lcom/mi/globallauncher/config/CommercialRemoteConfig;->mFirebaseRemoteConfig:Lcom/google/firebase/remoteconfig/FirebaseRemoteConfig;

    invoke-virtual {v1, v0}, Lcom/google/firebase/remoteconfig/FirebaseRemoteConfig;->setConfigSettings(Lcom/google/firebase/remoteconfig/FirebaseRemoteConfigSettings;)V

    iget-object v0, p0, Lcom/mi/globallauncher/config/CommercialRemoteConfig;->mFirebaseRemoteConfig:Lcom/google/firebase/remoteconfig/FirebaseRemoteConfig;

    sget v1, Lcom/mi/globallauncher/R$xml;->remote_config_defaults:I

    invoke-virtual {v0, v1}, Lcom/google/firebase/remoteconfig/FirebaseRemoteConfig;->setDefaults(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :cond_0
    :goto_0
    return-object p0
.end method

.method public isInitialized()Z
    .locals 1

    iget-object v0, p0, Lcom/mi/globallauncher/config/CommercialRemoteConfig;->mFirebaseRemoteConfig:Lcom/google/firebase/remoteconfig/FirebaseRemoteConfig;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public setRemoteConfigListener(Lcom/mi/globallauncher/config/CommercialRemoteConfig$RemoteConfigListener;)Lcom/mi/globallauncher/config/CommercialRemoteConfig;
    .locals 0

    iput-object p1, p0, Lcom/mi/globallauncher/config/CommercialRemoteConfig;->mRemoteConfigListener:Lcom/mi/globallauncher/config/CommercialRemoteConfig$RemoteConfigListener;

    return-object p0
.end method
