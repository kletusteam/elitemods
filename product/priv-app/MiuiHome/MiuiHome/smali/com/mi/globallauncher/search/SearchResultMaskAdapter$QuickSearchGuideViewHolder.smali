.class Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;
.super Lcom/mi/globallauncher/search/SearchResultMaskAdapter$ViewHolder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mi/globallauncher/search/SearchResultMaskAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "QuickSearchGuideViewHolder"
.end annotation


# instance fields
.field allAppsCheckBox:Landroid/widget/CheckBox;

.field allAppsContainer:Landroid/widget/RelativeLayout;

.field allAppsDesc:Landroid/widget/TextView;

.field allAppsImage:Landroid/widget/ImageView;

.field allAppsTitle:Landroid/widget/TextView;

.field closeBtn:Landroid/widget/ImageView;

.field container:Landroid/widget/RelativeLayout;

.field guideTitle:Landroid/widget/TextView;

.field okBtn:Landroid/widget/TextView;

.field quickSearchCheckBox:Landroid/widget/CheckBox;

.field quickSearchContainer:Landroid/widget/RelativeLayout;

.field quickSearchDesc:Landroid/widget/TextView;

.field quickSearchImage:Landroid/widget/ImageView;

.field quickSearchTitle:Landroid/widget/TextView;

.field final synthetic this$0:Lcom/mi/globallauncher/search/SearchResultMaskAdapter;


# direct methods
.method constructor <init>(Lcom/mi/globallauncher/search/SearchResultMaskAdapter;Landroid/view/View;)V
    .locals 1

    iput-object p1, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;->this$0:Lcom/mi/globallauncher/search/SearchResultMaskAdapter;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$ViewHolder;-><init>(Lcom/mi/globallauncher/search/SearchResultMaskAdapter;Lcom/mi/globallauncher/search/SearchResultMaskAdapter$1;)V

    sget p1, Lcom/mi/globallauncher/R$id;->quick_search_guide_container:I

    invoke-virtual {p2, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/RelativeLayout;

    iput-object p1, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;->container:Landroid/widget/RelativeLayout;

    sget p1, Lcom/mi/globallauncher/R$id;->quick_search_title:I

    invoke-virtual {p2, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;->guideTitle:Landroid/widget/TextView;

    sget p1, Lcom/mi/globallauncher/R$id;->quick_search_close_btn:I

    invoke-virtual {p2, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;->closeBtn:Landroid/widget/ImageView;

    sget p1, Lcom/mi/globallauncher/R$id;->guide_all_apps_container:I

    invoke-virtual {p2, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/RelativeLayout;

    iput-object p1, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;->allAppsContainer:Landroid/widget/RelativeLayout;

    sget p1, Lcom/mi/globallauncher/R$id;->guide_quick_search_container:I

    invoke-virtual {p2, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/RelativeLayout;

    iput-object p1, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;->quickSearchContainer:Landroid/widget/RelativeLayout;

    sget p1, Lcom/mi/globallauncher/R$id;->checkbox_all_apps:I

    invoke-virtual {p2, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/CheckBox;

    iput-object p1, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;->allAppsCheckBox:Landroid/widget/CheckBox;

    sget p1, Lcom/mi/globallauncher/R$id;->checkbox_quick_search:I

    invoke-virtual {p2, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/CheckBox;

    iput-object p1, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;->quickSearchCheckBox:Landroid/widget/CheckBox;

    sget p1, Lcom/mi/globallauncher/R$id;->guide_image_all_apps:I

    invoke-virtual {p2, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;->allAppsImage:Landroid/widget/ImageView;

    sget p1, Lcom/mi/globallauncher/R$id;->guide_image_quick_search:I

    invoke-virtual {p2, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;->quickSearchImage:Landroid/widget/ImageView;

    sget p1, Lcom/mi/globallauncher/R$id;->title_all_apps:I

    invoke-virtual {p2, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;->allAppsTitle:Landroid/widget/TextView;

    sget p1, Lcom/mi/globallauncher/R$id;->title_quick_search:I

    invoke-virtual {p2, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;->quickSearchTitle:Landroid/widget/TextView;

    sget p1, Lcom/mi/globallauncher/R$id;->desc_all_apps:I

    invoke-virtual {p2, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;->allAppsDesc:Landroid/widget/TextView;

    sget p1, Lcom/mi/globallauncher/R$id;->desc_quick_search:I

    invoke-virtual {p2, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;->quickSearchDesc:Landroid/widget/TextView;

    sget p1, Lcom/mi/globallauncher/R$id;->search_guide_ok_btn:I

    invoke-virtual {p2, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;->okBtn:Landroid/widget/TextView;

    return-void
.end method

.method public static synthetic lambda$updateView$0(Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;Landroid/view/View;)V
    .locals 1

    iget-object p1, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;->allAppsCheckBox:Landroid/widget/CheckBox;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    iget-object p1, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;->quickSearchCheckBox:Landroid/widget/CheckBox;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    return-void
.end method

.method public static synthetic lambda$updateView$1(Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;Landroid/view/View;)V
    .locals 1

    iget-object p1, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;->allAppsCheckBox:Landroid/widget/CheckBox;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    iget-object p1, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;->quickSearchCheckBox:Landroid/widget/CheckBox;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    return-void
.end method

.method public static synthetic lambda$updateView$2(Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;ILandroid/view/View;)V
    .locals 2

    iget-object p2, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;->quickSearchCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {p2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result p2

    const/4 v0, 0x1

    if-eqz p2, :cond_0

    invoke-static {}, Lcom/mi/globallauncher/manager/BranchInterface;->getCommercialPref()Lcom/mi/globallauncher/branchInterface/ICommercialPreference;

    move-result-object p2

    invoke-interface {p2, v0}, Lcom/mi/globallauncher/branchInterface/ICommercialPreference;->setFocusSearchOnEnterDrawerSwitch(Z)V

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/mi/globallauncher/manager/BranchInterface;->getCommercialPref()Lcom/mi/globallauncher/branchInterface/ICommercialPreference;

    move-result-object p2

    const/4 v1, 0x0

    invoke-interface {p2, v1}, Lcom/mi/globallauncher/branchInterface/ICommercialPreference;->setFocusSearchOnEnterDrawerSwitch(Z)V

    :goto_0
    invoke-direct {p0, p1}, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;->removeViewHolder(I)V

    iget-object p1, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;->this$0:Lcom/mi/globallauncher/search/SearchResultMaskAdapter;

    invoke-static {p1}, Lcom/mi/globallauncher/search/SearchResultMaskAdapter;->access$1300(Lcom/mi/globallauncher/search/SearchResultMaskAdapter;)Lcom/mi/globallauncher/search/SearchResultMaskView$QuickSearchGuideListener;

    move-result-object p1

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;->this$0:Lcom/mi/globallauncher/search/SearchResultMaskAdapter;

    invoke-static {p1}, Lcom/mi/globallauncher/search/SearchResultMaskAdapter;->access$1300(Lcom/mi/globallauncher/search/SearchResultMaskAdapter;)Lcom/mi/globallauncher/search/SearchResultMaskView$QuickSearchGuideListener;

    move-result-object p1

    iget-object p2, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;->quickSearchCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {p2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result p2

    if-eqz p2, :cond_1

    goto :goto_1

    :cond_1
    const/4 v0, -0x1

    :goto_1
    invoke-interface {p1, v0}, Lcom/mi/globallauncher/search/SearchResultMaskView$QuickSearchGuideListener;->onGuideOpen(I)V

    :cond_2
    return-void
.end method

.method public static synthetic lambda$updateView$3(Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;ILandroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;->removeViewHolder(I)V

    iget-object p1, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;->this$0:Lcom/mi/globallauncher/search/SearchResultMaskAdapter;

    invoke-static {p1}, Lcom/mi/globallauncher/search/SearchResultMaskAdapter;->access$1300(Lcom/mi/globallauncher/search/SearchResultMaskAdapter;)Lcom/mi/globallauncher/search/SearchResultMaskView$QuickSearchGuideListener;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;->this$0:Lcom/mi/globallauncher/search/SearchResultMaskAdapter;

    invoke-static {p1}, Lcom/mi/globallauncher/search/SearchResultMaskAdapter;->access$1300(Lcom/mi/globallauncher/search/SearchResultMaskAdapter;)Lcom/mi/globallauncher/search/SearchResultMaskView$QuickSearchGuideListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/mi/globallauncher/search/SearchResultMaskView$QuickSearchGuideListener;->onGuideClose()V

    :cond_0
    return-void
.end method

.method private removeViewHolder(I)V
    .locals 1

    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;->this$0:Lcom/mi/globallauncher/search/SearchResultMaskAdapter;

    invoke-static {v0}, Lcom/mi/globallauncher/search/SearchResultMaskAdapter;->access$300(Lcom/mi/globallauncher/search/SearchResultMaskAdapter;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;->this$0:Lcom/mi/globallauncher/search/SearchResultMaskAdapter;

    invoke-static {v0}, Lcom/mi/globallauncher/search/SearchResultMaskAdapter;->access$300(Lcom/mi/globallauncher/search/SearchResultMaskAdapter;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;->this$0:Lcom/mi/globallauncher/search/SearchResultMaskAdapter;

    invoke-static {v0}, Lcom/mi/globallauncher/search/SearchResultMaskAdapter;->access$300(Lcom/mi/globallauncher/search/SearchResultMaskAdapter;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;->this$0:Lcom/mi/globallauncher/search/SearchResultMaskAdapter;

    invoke-static {v0}, Lcom/mi/globallauncher/search/SearchResultMaskAdapter;->access$300(Lcom/mi/globallauncher/search/SearchResultMaskAdapter;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    iget-object p1, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;->this$0:Lcom/mi/globallauncher/search/SearchResultMaskAdapter;

    invoke-virtual {p1}, Lcom/mi/globallauncher/search/SearchResultMaskAdapter;->notifyDataSetChanged()V

    invoke-static {}, Lcom/mi/globallauncher/manager/BranchInterface;->getCommercialPref()Lcom/mi/globallauncher/branchInterface/ICommercialPreference;

    move-result-object p1

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Lcom/mi/globallauncher/branchInterface/ICommercialPreference;->setShouldQuickSearchGuideShow(Z)V

    :cond_0
    return-void
.end method


# virtual methods
.method updateView(I)V
    .locals 3

    goto/32 :goto_30

    nop

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto/32 :goto_67

    nop

    :goto_1
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;->quickSearchImage:Landroid/widget/ImageView;

    goto/32 :goto_37

    nop

    :goto_2
    iget-object v1, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;->this$0:Lcom/mi/globallauncher/search/SearchResultMaskAdapter;

    goto/32 :goto_27

    nop

    :goto_3
    invoke-interface {p1, v0, v1}, Lcom/mi/globallauncher/branchInterface/ICommercialPreference;->setQuickSearchGuideShowTime(J)V

    goto/32 :goto_38

    nop

    :goto_4
    invoke-direct {v1, p0, p1}, Lcom/mi/globallauncher/search/-$$Lambda$SearchResultMaskAdapter$QuickSearchGuideViewHolder$wVuXUopAg_zAgbozy2ok3aqubKI;-><init>(Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;I)V

    goto/32 :goto_39

    nop

    :goto_5
    goto/16 :goto_1e

    :goto_6
    goto/32 :goto_72

    nop

    :goto_7
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto/32 :goto_4c

    nop

    :goto_8
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;->quickSearchCheckBox:Landroid/widget/CheckBox;

    goto/32 :goto_74

    nop

    :goto_9
    invoke-static {v1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    goto/32 :goto_62

    nop

    :goto_a
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/32 :goto_d

    nop

    :goto_b
    new-instance v1, Lcom/mi/globallauncher/search/-$$Lambda$SearchResultMaskAdapter$QuickSearchGuideViewHolder$ru_szDfemZCEliIfzOCdqQJ7rPc;

    goto/32 :goto_48

    nop

    :goto_c
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto/32 :goto_6f

    nop

    :goto_d
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;->closeBtn:Landroid/widget/ImageView;

    goto/32 :goto_1f

    nop

    :goto_e
    sget v1, Lcom/mi/globallauncher/R$drawable;->guide_all_apps_dark:I

    goto/32 :goto_3d

    nop

    :goto_f
    invoke-static {v1}, Lcom/mi/globallauncher/search/SearchResultMaskAdapter;->access$200(Lcom/mi/globallauncher/search/SearchResultMaskAdapter;)Landroid/content/Context;

    move-result-object v1

    goto/32 :goto_56

    nop

    :goto_10
    invoke-static {v1}, Lcom/mi/globallauncher/search/SearchResultMaskAdapter;->access$200(Lcom/mi/globallauncher/search/SearchResultMaskAdapter;)Landroid/content/Context;

    move-result-object v1

    goto/32 :goto_6d

    nop

    :goto_11
    if-nez v0, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_76

    nop

    :goto_12
    sget v2, Lcom/mi/globallauncher/R$color;->search_guide_desc_text_color:I

    goto/32 :goto_64

    nop

    :goto_13
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;->allAppsTitle:Landroid/widget/TextView;

    goto/32 :goto_2

    nop

    :goto_14
    invoke-static {v1}, Lcom/mi/globallauncher/search/SearchResultMaskAdapter;->access$200(Lcom/mi/globallauncher/search/SearchResultMaskAdapter;)Landroid/content/Context;

    move-result-object v1

    goto/32 :goto_26

    nop

    :goto_15
    invoke-direct {v1, p0, p1}, Lcom/mi/globallauncher/search/-$$Lambda$SearchResultMaskAdapter$QuickSearchGuideViewHolder$BlsPL8R-c7MFvL9T2f6JwHMlMS8;-><init>(Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;I)V

    goto/32 :goto_a

    nop

    :goto_16
    invoke-static {v1}, Lcom/mi/globallauncher/search/SearchResultMaskAdapter;->access$200(Lcom/mi/globallauncher/search/SearchResultMaskAdapter;)Landroid/content/Context;

    move-result-object v1

    goto/32 :goto_60

    nop

    :goto_17
    invoke-static {v1}, Lcom/mi/globallauncher/search/SearchResultMaskAdapter;->access$200(Lcom/mi/globallauncher/search/SearchResultMaskAdapter;)Landroid/content/Context;

    move-result-object v1

    goto/32 :goto_12

    nop

    :goto_18
    sget v1, Lcom/mi/globallauncher/R$drawable;->search_guide_close_btn:I

    goto/32 :goto_50

    nop

    :goto_19
    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setBackgroundResource(I)V

    goto/32 :goto_28

    nop

    :goto_1a
    sget v2, Lcom/mi/globallauncher/R$color;->white:I

    goto/32 :goto_9

    nop

    :goto_1b
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    goto/32 :goto_5c

    nop

    :goto_1c
    sget v1, Lcom/mi/globallauncher/R$drawable;->guide_quick_search:I

    goto/32 :goto_59

    nop

    :goto_1d
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_1e
    goto/32 :goto_3e

    nop

    :goto_1f
    new-instance v1, Lcom/mi/globallauncher/search/-$$Lambda$SearchResultMaskAdapter$QuickSearchGuideViewHolder$wVuXUopAg_zAgbozy2ok3aqubKI;

    goto/32 :goto_4

    nop

    :goto_20
    sget v2, Lcom/mi/globallauncher/R$color;->alpha90white:I

    goto/32 :goto_35

    nop

    :goto_21
    iget-object v1, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;->this$0:Lcom/mi/globallauncher/search/SearchResultMaskAdapter;

    goto/32 :goto_78

    nop

    :goto_22
    invoke-static {v1}, Lcom/mi/globallauncher/search/SearchResultMaskAdapter;->access$200(Lcom/mi/globallauncher/search/SearchResultMaskAdapter;)Landroid/content/Context;

    move-result-object v1

    goto/32 :goto_3c

    nop

    :goto_23
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;->allAppsDesc:Landroid/widget/TextView;

    goto/32 :goto_68

    nop

    :goto_24
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;->okBtn:Landroid/widget/TextView;

    goto/32 :goto_36

    nop

    :goto_25
    iget-object v1, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;->this$0:Lcom/mi/globallauncher/search/SearchResultMaskAdapter;

    goto/32 :goto_14

    nop

    :goto_26
    sget v2, Lcom/mi/globallauncher/R$color;->white:I

    goto/32 :goto_5d

    nop

    :goto_27
    invoke-static {v1}, Lcom/mi/globallauncher/search/SearchResultMaskAdapter;->access$200(Lcom/mi/globallauncher/search/SearchResultMaskAdapter;)Landroid/content/Context;

    move-result-object v1

    goto/32 :goto_1a

    nop

    :goto_28
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;->quickSearchCheckBox:Landroid/widget/CheckBox;

    goto/32 :goto_5e

    nop

    :goto_29
    sget v2, Lcom/mi/globallauncher/R$color;->search_guide_desc_text_color:I

    goto/32 :goto_54

    nop

    :goto_2a
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;->quickSearchDesc:Landroid/widget/TextView;

    goto/32 :goto_21

    nop

    :goto_2b
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;->allAppsCheckBox:Landroid/widget/CheckBox;

    goto/32 :goto_5a

    nop

    :goto_2c
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    goto/32 :goto_3

    nop

    :goto_2d
    sget v1, Lcom/mi/globallauncher/R$drawable;->branch_item_card_bg:I

    goto/32 :goto_1b

    nop

    :goto_2e
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto/32 :goto_23

    nop

    :goto_2f
    invoke-static {v1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    goto/32 :goto_5b

    nop

    :goto_30
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;->this$0:Lcom/mi/globallauncher/search/SearchResultMaskAdapter;

    goto/32 :goto_3b

    nop

    :goto_31
    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setBackgroundResource(I)V

    goto/32 :goto_70

    nop

    :goto_32
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;->allAppsDesc:Landroid/widget/TextView;

    goto/32 :goto_66

    nop

    :goto_33
    invoke-static {v1}, Lcom/mi/globallauncher/search/SearchResultMaskAdapter;->access$200(Lcom/mi/globallauncher/search/SearchResultMaskAdapter;)Landroid/content/Context;

    move-result-object v1

    goto/32 :goto_4b

    nop

    :goto_34
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto/32 :goto_2a

    nop

    :goto_35
    invoke-static {v1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    goto/32 :goto_53

    nop

    :goto_36
    new-instance v1, Lcom/mi/globallauncher/search/-$$Lambda$SearchResultMaskAdapter$QuickSearchGuideViewHolder$BlsPL8R-c7MFvL9T2f6JwHMlMS8;

    goto/32 :goto_15

    nop

    :goto_37
    sget v1, Lcom/mi/globallauncher/R$drawable;->guide_quick_search_dark:I

    goto/32 :goto_1d

    nop

    :goto_38
    return-void

    :goto_39
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/32 :goto_40

    nop

    :goto_3a
    invoke-static {v1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    goto/32 :goto_c

    nop

    :goto_3b
    invoke-static {v0}, Lcom/mi/globallauncher/search/SearchResultMaskAdapter;->access$100(Lcom/mi/globallauncher/search/SearchResultMaskAdapter;)Z

    move-result v0

    goto/32 :goto_11

    nop

    :goto_3c
    sget v2, Lcom/mi/globallauncher/R$color;->alpha90black:I

    goto/32 :goto_3f

    nop

    :goto_3d
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/32 :goto_1

    nop

    :goto_3e
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;->allAppsContainer:Landroid/widget/RelativeLayout;

    goto/32 :goto_b

    nop

    :goto_3f
    invoke-static {v1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    goto/32 :goto_7

    nop

    :goto_40
    invoke-static {}, Lcom/mi/globallauncher/manager/BranchInterface;->getCommercialPref()Lcom/mi/globallauncher/branchInterface/ICommercialPreference;

    move-result-object p1

    goto/32 :goto_2c

    nop

    :goto_41
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/32 :goto_46

    nop

    :goto_42
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/32 :goto_43

    nop

    :goto_43
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;->quickSearchContainer:Landroid/widget/RelativeLayout;

    goto/32 :goto_57

    nop

    :goto_44
    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setBackgroundResource(I)V

    goto/32 :goto_69

    nop

    :goto_45
    invoke-direct {v1, p0}, Lcom/mi/globallauncher/search/-$$Lambda$SearchResultMaskAdapter$QuickSearchGuideViewHolder$ayl_-eG2t3Am6JD9dNInC417ZUI;-><init>(Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;)V

    goto/32 :goto_58

    nop

    :goto_46
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;->allAppsCheckBox:Landroid/widget/CheckBox;

    goto/32 :goto_73

    nop

    :goto_47
    sget v1, Lcom/mi/globallauncher/R$drawable;->branch_item_card_bg_dark:I

    goto/32 :goto_6a

    nop

    :goto_48
    invoke-direct {v1, p0}, Lcom/mi/globallauncher/search/-$$Lambda$SearchResultMaskAdapter$QuickSearchGuideViewHolder$ru_szDfemZCEliIfzOCdqQJ7rPc;-><init>(Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;)V

    goto/32 :goto_42

    nop

    :goto_49
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;->closeBtn:Landroid/widget/ImageView;

    goto/32 :goto_18

    nop

    :goto_4a
    iget-object v1, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;->this$0:Lcom/mi/globallauncher/search/SearchResultMaskAdapter;

    goto/32 :goto_22

    nop

    :goto_4b
    sget v2, Lcom/mi/globallauncher/R$color;->alpha50white:I

    goto/32 :goto_3a

    nop

    :goto_4c
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;->quickSearchTitle:Landroid/widget/TextView;

    goto/32 :goto_5f

    nop

    :goto_4d
    invoke-static {v1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    goto/32 :goto_4f

    nop

    :goto_4e
    iget-object v1, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;->this$0:Lcom/mi/globallauncher/search/SearchResultMaskAdapter;

    goto/32 :goto_16

    nop

    :goto_4f
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto/32 :goto_71

    nop

    :goto_50
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/32 :goto_2b

    nop

    :goto_51
    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setBackgroundResource(I)V

    goto/32 :goto_8

    nop

    :goto_52
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;->guideTitle:Landroid/widget/TextView;

    goto/32 :goto_55

    nop

    :goto_53
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto/32 :goto_13

    nop

    :goto_54
    invoke-static {v1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    goto/32 :goto_65

    nop

    :goto_55
    iget-object v1, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;->this$0:Lcom/mi/globallauncher/search/SearchResultMaskAdapter;

    goto/32 :goto_79

    nop

    :goto_56
    sget v2, Lcom/mi/globallauncher/R$color;->alpha50white:I

    goto/32 :goto_4d

    nop

    :goto_57
    new-instance v1, Lcom/mi/globallauncher/search/-$$Lambda$SearchResultMaskAdapter$QuickSearchGuideViewHolder$ayl_-eG2t3Am6JD9dNInC417ZUI;

    goto/32 :goto_45

    nop

    :goto_58
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/32 :goto_24

    nop

    :goto_59
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/32 :goto_5

    nop

    :goto_5a
    sget v1, Lcom/mi/globallauncher/R$drawable;->search_guide_checkbox_bg:I

    goto/32 :goto_19

    nop

    :goto_5b
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto/32 :goto_32

    nop

    :goto_5c
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;->guideTitle:Landroid/widget/TextView;

    goto/32 :goto_4e

    nop

    :goto_5d
    invoke-static {v1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    goto/32 :goto_2e

    nop

    :goto_5e
    sget v1, Lcom/mi/globallauncher/R$drawable;->search_guide_checkbox_bg:I

    goto/32 :goto_31

    nop

    :goto_5f
    iget-object v1, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;->this$0:Lcom/mi/globallauncher/search/SearchResultMaskAdapter;

    goto/32 :goto_10

    nop

    :goto_60
    sget v2, Lcom/mi/globallauncher/R$color;->alpha90black:I

    goto/32 :goto_6b

    nop

    :goto_61
    sget v1, Lcom/mi/globallauncher/R$drawable;->search_guide_close_btn_dark:I

    goto/32 :goto_41

    nop

    :goto_62
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto/32 :goto_6e

    nop

    :goto_63
    iget-object v1, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;->this$0:Lcom/mi/globallauncher/search/SearchResultMaskAdapter;

    goto/32 :goto_33

    nop

    :goto_64
    invoke-static {v1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    goto/32 :goto_34

    nop

    :goto_65
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto/32 :goto_49

    nop

    :goto_66
    iget-object v1, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;->this$0:Lcom/mi/globallauncher/search/SearchResultMaskAdapter;

    goto/32 :goto_17

    nop

    :goto_67
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;->allAppsTitle:Landroid/widget/TextView;

    goto/32 :goto_4a

    nop

    :goto_68
    iget-object v1, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;->this$0:Lcom/mi/globallauncher/search/SearchResultMaskAdapter;

    goto/32 :goto_f

    nop

    :goto_69
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;->allAppsImage:Landroid/widget/ImageView;

    goto/32 :goto_e

    nop

    :goto_6a
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    goto/32 :goto_52

    nop

    :goto_6b
    invoke-static {v1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    goto/32 :goto_0

    nop

    :goto_6c
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;->quickSearchImage:Landroid/widget/ImageView;

    goto/32 :goto_1c

    nop

    :goto_6d
    sget v2, Lcom/mi/globallauncher/R$color;->alpha90black:I

    goto/32 :goto_2f

    nop

    :goto_6e
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;->quickSearchTitle:Landroid/widget/TextView;

    goto/32 :goto_25

    nop

    :goto_6f
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;->closeBtn:Landroid/widget/ImageView;

    goto/32 :goto_61

    nop

    :goto_70
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;->allAppsImage:Landroid/widget/ImageView;

    goto/32 :goto_75

    nop

    :goto_71
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;->quickSearchDesc:Landroid/widget/TextView;

    goto/32 :goto_63

    nop

    :goto_72
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;->container:Landroid/widget/RelativeLayout;

    goto/32 :goto_47

    nop

    :goto_73
    sget v1, Lcom/mi/globallauncher/R$drawable;->search_guide_checkbox_bg_dark:I

    goto/32 :goto_51

    nop

    :goto_74
    sget v1, Lcom/mi/globallauncher/R$drawable;->search_guide_checkbox_bg_dark:I

    goto/32 :goto_44

    nop

    :goto_75
    sget v1, Lcom/mi/globallauncher/R$drawable;->guide_all_apps:I

    goto/32 :goto_77

    nop

    :goto_76
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$QuickSearchGuideViewHolder;->container:Landroid/widget/RelativeLayout;

    goto/32 :goto_2d

    nop

    :goto_77
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/32 :goto_6c

    nop

    :goto_78
    invoke-static {v1}, Lcom/mi/globallauncher/search/SearchResultMaskAdapter;->access$200(Lcom/mi/globallauncher/search/SearchResultMaskAdapter;)Landroid/content/Context;

    move-result-object v1

    goto/32 :goto_29

    nop

    :goto_79
    invoke-static {v1}, Lcom/mi/globallauncher/search/SearchResultMaskAdapter;->access$200(Lcom/mi/globallauncher/search/SearchResultMaskAdapter;)Landroid/content/Context;

    move-result-object v1

    goto/32 :goto_20

    nop
.end method
