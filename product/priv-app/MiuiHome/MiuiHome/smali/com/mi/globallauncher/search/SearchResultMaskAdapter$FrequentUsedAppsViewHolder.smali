.class Lcom/mi/globallauncher/search/SearchResultMaskAdapter$FrequentUsedAppsViewHolder;
.super Lcom/mi/globallauncher/search/SearchResultMaskAdapter$ViewHolder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mi/globallauncher/search/SearchResultMaskAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "FrequentUsedAppsViewHolder"
.end annotation


# instance fields
.field frequentContainer:Landroid/widget/LinearLayout;

.field frequentGrid1:Landroid/widget/GridView;

.field frequentGrid2:Landroid/widget/GridView;

.field final synthetic this$0:Lcom/mi/globallauncher/search/SearchResultMaskAdapter;


# direct methods
.method constructor <init>(Lcom/mi/globallauncher/search/SearchResultMaskAdapter;Landroid/view/View;)V
    .locals 1

    iput-object p1, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$FrequentUsedAppsViewHolder;->this$0:Lcom/mi/globallauncher/search/SearchResultMaskAdapter;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$ViewHolder;-><init>(Lcom/mi/globallauncher/search/SearchResultMaskAdapter;Lcom/mi/globallauncher/search/SearchResultMaskAdapter$1;)V

    sget p1, Lcom/mi/globallauncher/R$id;->frequent_apps_container:I

    invoke-virtual {p2, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$FrequentUsedAppsViewHolder;->frequentContainer:Landroid/widget/LinearLayout;

    sget p1, Lcom/mi/globallauncher/R$id;->frequent_apps_grid1:I

    invoke-virtual {p2, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/GridView;

    iput-object p1, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$FrequentUsedAppsViewHolder;->frequentGrid1:Landroid/widget/GridView;

    sget p1, Lcom/mi/globallauncher/R$id;->frequent_apps_grid2:I

    invoke-virtual {p2, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/GridView;

    iput-object p1, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$FrequentUsedAppsViewHolder;->frequentGrid2:Landroid/widget/GridView;

    return-void
.end method


# virtual methods
.method updateView(I)V
    .locals 3

    goto/32 :goto_1a

    nop

    :goto_0
    invoke-static {v1}, Lcom/mi/globallauncher/search/SearchResultMaskAdapter;->access$900(Lcom/mi/globallauncher/search/SearchResultMaskAdapter;)I

    move-result v1

    goto/32 :goto_1

    nop

    :goto_1
    invoke-virtual {p1, v0, v1}, Lcom/mi/globallauncher/local/FrequentUsedAppsGridAdapter;->setFrequentUsedApps(Ljava/util/List;I)V

    goto/32 :goto_55

    nop

    :goto_2
    invoke-static {p1}, Lcom/mi/globallauncher/search/SearchResultMaskAdapter;->access$1000(Lcom/mi/globallauncher/search/SearchResultMaskAdapter;)Ljava/util/List;

    move-result-object p1

    goto/32 :goto_e

    nop

    :goto_3
    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setNumColumns(I)V

    goto/32 :goto_61

    nop

    :goto_4
    goto/16 :goto_37

    :goto_5
    goto/32 :goto_2f

    nop

    :goto_6
    invoke-static {v0}, Lcom/mi/globallauncher/search/SearchResultMaskAdapter;->access$1200(Lcom/mi/globallauncher/search/SearchResultMaskAdapter;)Lcom/mi/globallauncher/local/FrequentUsedAppClickListener;

    move-result-object v0

    goto/32 :goto_5a

    nop

    :goto_7
    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/32 :goto_40

    nop

    :goto_8
    invoke-static {p1}, Lcom/mi/globallauncher/search/SearchResultMaskAdapter;->access$1000(Lcom/mi/globallauncher/search/SearchResultMaskAdapter;)Ljava/util/List;

    move-result-object p1

    goto/32 :goto_58

    nop

    :goto_9
    invoke-virtual {v0, p1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto/32 :goto_39

    nop

    :goto_a
    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setNumColumns(I)V

    goto/32 :goto_18

    nop

    :goto_b
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$FrequentUsedAppsViewHolder;->frequentGrid1:Landroid/widget/GridView;

    goto/32 :goto_9

    nop

    :goto_c
    iget-object v2, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$FrequentUsedAppsViewHolder;->this$0:Lcom/mi/globallauncher/search/SearchResultMaskAdapter;

    goto/32 :goto_13

    nop

    :goto_d
    mul-int/lit8 v0, v0, 0x2

    goto/32 :goto_4d

    nop

    :goto_e
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    goto/32 :goto_43

    nop

    :goto_f
    const/4 v0, 0x0

    goto/32 :goto_36

    nop

    :goto_10
    invoke-static {v0}, Lcom/mi/globallauncher/search/SearchResultMaskAdapter;->access$100(Lcom/mi/globallauncher/search/SearchResultMaskAdapter;)Z

    move-result v0

    goto/32 :goto_24

    nop

    :goto_11
    iget-object p1, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$FrequentUsedAppsViewHolder;->this$0:Lcom/mi/globallauncher/search/SearchResultMaskAdapter;

    goto/32 :goto_5e

    nop

    :goto_12
    const/4 v1, 0x0

    goto/32 :goto_21

    nop

    :goto_13
    invoke-static {v2}, Lcom/mi/globallauncher/search/SearchResultMaskAdapter;->access$1000(Lcom/mi/globallauncher/search/SearchResultMaskAdapter;)Ljava/util/List;

    move-result-object v2

    goto/32 :goto_48

    nop

    :goto_14
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$FrequentUsedAppsViewHolder;->frequentGrid1:Landroid/widget/GridView;

    goto/32 :goto_1c

    nop

    :goto_15
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$FrequentUsedAppsViewHolder;->this$0:Lcom/mi/globallauncher/search/SearchResultMaskAdapter;

    goto/32 :goto_32

    nop

    :goto_16
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$FrequentUsedAppsViewHolder;->frequentContainer:Landroid/widget/LinearLayout;

    goto/32 :goto_3a

    nop

    :goto_17
    invoke-virtual {p1, v0, v1}, Lcom/mi/globallauncher/local/FrequentUsedAppsGridAdapter;->setFrequentUsedApps(Ljava/util/List;I)V

    goto/32 :goto_4c

    nop

    :goto_18
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$FrequentUsedAppsViewHolder;->frequentGrid1:Landroid/widget/GridView;

    goto/32 :goto_12

    nop

    :goto_19
    invoke-static {v0}, Lcom/mi/globallauncher/search/SearchResultMaskAdapter;->access$100(Lcom/mi/globallauncher/search/SearchResultMaskAdapter;)Z

    move-result v0

    goto/32 :goto_44

    nop

    :goto_1a
    iget-object p1, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$FrequentUsedAppsViewHolder;->frequentContainer:Landroid/widget/LinearLayout;

    goto/32 :goto_2c

    nop

    :goto_1b
    new-instance p1, Lcom/mi/globallauncher/local/FrequentUsedAppsGridAdapter;

    goto/32 :goto_31

    nop

    :goto_1c
    iget-object v1, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$FrequentUsedAppsViewHolder;->this$0:Lcom/mi/globallauncher/search/SearchResultMaskAdapter;

    goto/32 :goto_35

    nop

    :goto_1d
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$FrequentUsedAppsViewHolder;->frequentGrid2:Landroid/widget/GridView;

    goto/32 :goto_2e

    nop

    :goto_1e
    invoke-static {v0}, Lcom/mi/globallauncher/search/SearchResultMaskAdapter;->access$1000(Lcom/mi/globallauncher/search/SearchResultMaskAdapter;)Ljava/util/List;

    move-result-object v0

    goto/32 :goto_3c

    nop

    :goto_1f
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$FrequentUsedAppsViewHolder;->frequentGrid2:Landroid/widget/GridView;

    goto/32 :goto_51

    nop

    :goto_20
    invoke-virtual {p1, v0}, Lcom/mi/globallauncher/local/FrequentUsedAppsGridAdapter;->setScale(F)V

    goto/32 :goto_2b

    nop

    :goto_21
    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setVerticalScrollBarEnabled(Z)V

    goto/32 :goto_1d

    nop

    :goto_22
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$FrequentUsedAppsViewHolder;->this$0:Lcom/mi/globallauncher/search/SearchResultMaskAdapter;

    goto/32 :goto_19

    nop

    :goto_23
    iget-object v1, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$FrequentUsedAppsViewHolder;->this$0:Lcom/mi/globallauncher/search/SearchResultMaskAdapter;

    goto/32 :goto_5f

    nop

    :goto_24
    if-nez v0, :cond_0

    goto/32 :goto_41

    :cond_0
    goto/32 :goto_59

    nop

    :goto_25
    return-void

    :goto_26
    if-nez p1, :cond_1

    goto/32 :goto_5

    :cond_1
    goto/32 :goto_1b

    nop

    :goto_27
    invoke-static {p1, v1}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    goto/32 :goto_7

    nop

    :goto_28
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    goto/32 :goto_26

    nop

    :goto_29
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$FrequentUsedAppsViewHolder;->this$0:Lcom/mi/globallauncher/search/SearchResultMaskAdapter;

    goto/32 :goto_6

    nop

    :goto_2a
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$FrequentUsedAppsViewHolder;->this$0:Lcom/mi/globallauncher/search/SearchResultMaskAdapter;

    goto/32 :goto_10

    nop

    :goto_2b
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$FrequentUsedAppsViewHolder;->this$0:Lcom/mi/globallauncher/search/SearchResultMaskAdapter;

    goto/32 :goto_33

    nop

    :goto_2c
    invoke-virtual {p1}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    goto/32 :goto_14

    nop

    :goto_2d
    invoke-virtual {p1}, Lcom/mi/globallauncher/local/FrequentUsedAppsGridAdapter;->notifyDataSetChanged()V

    goto/32 :goto_4

    nop

    :goto_2e
    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setVerticalScrollBarEnabled(Z)V

    goto/32 :goto_2a

    nop

    :goto_2f
    iget-object p1, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$FrequentUsedAppsViewHolder;->frequentContainer:Landroid/widget/LinearLayout;

    goto/32 :goto_f

    nop

    :goto_30
    invoke-static {v0}, Lcom/mi/globallauncher/search/SearchResultMaskAdapter;->access$200(Lcom/mi/globallauncher/search/SearchResultMaskAdapter;)Landroid/content/Context;

    move-result-object v0

    goto/32 :goto_60

    nop

    :goto_31
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$FrequentUsedAppsViewHolder;->this$0:Lcom/mi/globallauncher/search/SearchResultMaskAdapter;

    goto/32 :goto_30

    nop

    :goto_32
    invoke-static {v0}, Lcom/mi/globallauncher/search/SearchResultMaskAdapter;->access$200(Lcom/mi/globallauncher/search/SearchResultMaskAdapter;)Landroid/content/Context;

    move-result-object v0

    goto/32 :goto_45

    nop

    :goto_33
    invoke-static {v0}, Lcom/mi/globallauncher/search/SearchResultMaskAdapter;->access$1200(Lcom/mi/globallauncher/search/SearchResultMaskAdapter;)Lcom/mi/globallauncher/local/FrequentUsedAppClickListener;

    move-result-object v0

    goto/32 :goto_34

    nop

    :goto_34
    invoke-virtual {p1, v0}, Lcom/mi/globallauncher/local/FrequentUsedAppsGridAdapter;->setAppClickListener(Lcom/mi/globallauncher/local/FrequentUsedAppClickListener;)V

    goto/32 :goto_22

    nop

    :goto_35
    invoke-static {v1}, Lcom/mi/globallauncher/search/SearchResultMaskAdapter;->access$900(Lcom/mi/globallauncher/search/SearchResultMaskAdapter;)I

    move-result v1

    goto/32 :goto_3

    nop

    :goto_36
    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    :goto_37
    goto/32 :goto_25

    nop

    :goto_38
    invoke-static {v0}, Lcom/mi/globallauncher/search/SearchResultMaskAdapter;->access$1000(Lcom/mi/globallauncher/search/SearchResultMaskAdapter;)Ljava/util/List;

    move-result-object v0

    goto/32 :goto_47

    nop

    :goto_39
    invoke-virtual {p1}, Lcom/mi/globallauncher/local/FrequentUsedAppsGridAdapter;->notifyDataSetChanged()V

    goto/32 :goto_4b

    nop

    :goto_3a
    sget v1, Lcom/mi/globallauncher/R$drawable;->branch_item_card_bg_dark:I

    goto/32 :goto_4f

    nop

    :goto_3b
    invoke-static {v0}, Lcom/mi/globallauncher/search/SearchResultMaskAdapter;->access$100(Lcom/mi/globallauncher/search/SearchResultMaskAdapter;)Z

    move-result v0

    goto/32 :goto_3d

    nop

    :goto_3c
    iget-object v1, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$FrequentUsedAppsViewHolder;->this$0:Lcom/mi/globallauncher/search/SearchResultMaskAdapter;

    goto/32 :goto_42

    nop

    :goto_3d
    invoke-virtual {p1, v0}, Lcom/mi/globallauncher/local/FrequentUsedAppsGridAdapter;->setIsInLightMode(Z)V

    goto/32 :goto_b

    nop

    :goto_3e
    invoke-interface {v0, v1, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    goto/32 :goto_23

    nop

    :goto_3f
    sget v1, Lcom/mi/globallauncher/R$drawable;->branch_item_card_bg:I

    goto/32 :goto_27

    nop

    :goto_40
    goto :goto_53

    :goto_41
    goto/32 :goto_16

    nop

    :goto_42
    invoke-static {v1}, Lcom/mi/globallauncher/search/SearchResultMaskAdapter;->access$900(Lcom/mi/globallauncher/search/SearchResultMaskAdapter;)I

    move-result v1

    goto/32 :goto_c

    nop

    :goto_43
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$FrequentUsedAppsViewHolder;->this$0:Lcom/mi/globallauncher/search/SearchResultMaskAdapter;

    goto/32 :goto_50

    nop

    :goto_44
    invoke-virtual {p1, v0}, Lcom/mi/globallauncher/local/FrequentUsedAppsGridAdapter;->setIsInLightMode(Z)V

    goto/32 :goto_1f

    nop

    :goto_45
    invoke-direct {p1, v0}, Lcom/mi/globallauncher/local/FrequentUsedAppsGridAdapter;-><init>(Landroid/content/Context;)V

    goto/32 :goto_5d

    nop

    :goto_46
    invoke-virtual {p1, v0}, Lcom/mi/globallauncher/local/FrequentUsedAppsGridAdapter;->setScale(F)V

    goto/32 :goto_29

    nop

    :goto_47
    iget-object v1, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$FrequentUsedAppsViewHolder;->this$0:Lcom/mi/globallauncher/search/SearchResultMaskAdapter;

    goto/32 :goto_0

    nop

    :goto_48
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    goto/32 :goto_3e

    nop

    :goto_49
    iget-object v1, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$FrequentUsedAppsViewHolder;->this$0:Lcom/mi/globallauncher/search/SearchResultMaskAdapter;

    goto/32 :goto_57

    nop

    :goto_4a
    invoke-static {v0}, Lcom/mi/globallauncher/search/SearchResultMaskAdapter;->access$1100(Lcom/mi/globallauncher/search/SearchResultMaskAdapter;)F

    move-result v0

    goto/32 :goto_20

    nop

    :goto_4b
    iget-object p1, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$FrequentUsedAppsViewHolder;->this$0:Lcom/mi/globallauncher/search/SearchResultMaskAdapter;

    goto/32 :goto_2

    nop

    :goto_4c
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$FrequentUsedAppsViewHolder;->this$0:Lcom/mi/globallauncher/search/SearchResultMaskAdapter;

    goto/32 :goto_4a

    nop

    :goto_4d
    if-ge p1, v0, :cond_2

    goto/32 :goto_37

    :cond_2
    goto/32 :goto_56

    nop

    :goto_4e
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$FrequentUsedAppsViewHolder;->this$0:Lcom/mi/globallauncher/search/SearchResultMaskAdapter;

    goto/32 :goto_38

    nop

    :goto_4f
    invoke-static {p1, v1}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    goto/32 :goto_52

    nop

    :goto_50
    invoke-static {v0}, Lcom/mi/globallauncher/search/SearchResultMaskAdapter;->access$900(Lcom/mi/globallauncher/search/SearchResultMaskAdapter;)I

    move-result v0

    goto/32 :goto_d

    nop

    :goto_51
    invoke-virtual {v0, p1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto/32 :goto_2d

    nop

    :goto_52
    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    :goto_53
    goto/32 :goto_5b

    nop

    :goto_54
    invoke-static {v0}, Lcom/mi/globallauncher/search/SearchResultMaskAdapter;->access$1100(Lcom/mi/globallauncher/search/SearchResultMaskAdapter;)F

    move-result v0

    goto/32 :goto_46

    nop

    :goto_55
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$FrequentUsedAppsViewHolder;->this$0:Lcom/mi/globallauncher/search/SearchResultMaskAdapter;

    goto/32 :goto_54

    nop

    :goto_56
    new-instance p1, Lcom/mi/globallauncher/local/FrequentUsedAppsGridAdapter;

    goto/32 :goto_15

    nop

    :goto_57
    invoke-static {v1}, Lcom/mi/globallauncher/search/SearchResultMaskAdapter;->access$900(Lcom/mi/globallauncher/search/SearchResultMaskAdapter;)I

    move-result v1

    goto/32 :goto_a

    nop

    :goto_58
    if-nez p1, :cond_3

    goto/32 :goto_5

    :cond_3
    goto/32 :goto_11

    nop

    :goto_59
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$FrequentUsedAppsViewHolder;->frequentContainer:Landroid/widget/LinearLayout;

    goto/32 :goto_3f

    nop

    :goto_5a
    invoke-virtual {p1, v0}, Lcom/mi/globallauncher/local/FrequentUsedAppsGridAdapter;->setAppClickListener(Lcom/mi/globallauncher/local/FrequentUsedAppClickListener;)V

    goto/32 :goto_5c

    nop

    :goto_5b
    iget-object p1, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$FrequentUsedAppsViewHolder;->this$0:Lcom/mi/globallauncher/search/SearchResultMaskAdapter;

    goto/32 :goto_8

    nop

    :goto_5c
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$FrequentUsedAppsViewHolder;->this$0:Lcom/mi/globallauncher/search/SearchResultMaskAdapter;

    goto/32 :goto_3b

    nop

    :goto_5d
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$FrequentUsedAppsViewHolder;->this$0:Lcom/mi/globallauncher/search/SearchResultMaskAdapter;

    goto/32 :goto_1e

    nop

    :goto_5e
    invoke-static {p1}, Lcom/mi/globallauncher/search/SearchResultMaskAdapter;->access$1000(Lcom/mi/globallauncher/search/SearchResultMaskAdapter;)Ljava/util/List;

    move-result-object p1

    goto/32 :goto_28

    nop

    :goto_5f
    invoke-static {v1}, Lcom/mi/globallauncher/search/SearchResultMaskAdapter;->access$900(Lcom/mi/globallauncher/search/SearchResultMaskAdapter;)I

    move-result v1

    goto/32 :goto_17

    nop

    :goto_60
    invoke-direct {p1, v0}, Lcom/mi/globallauncher/local/FrequentUsedAppsGridAdapter;-><init>(Landroid/content/Context;)V

    goto/32 :goto_4e

    nop

    :goto_61
    iget-object v0, p0, Lcom/mi/globallauncher/search/SearchResultMaskAdapter$FrequentUsedAppsViewHolder;->frequentGrid2:Landroid/widget/GridView;

    goto/32 :goto_49

    nop
.end method
