.class public final Lcom/mi/google/gson/internal/LinkedTreeMap;
.super Ljava/util/AbstractMap;

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mi/google/gson/internal/LinkedTreeMap$KeySet;,
        Lcom/mi/google/gson/internal/LinkedTreeMap$EntrySet;,
        Lcom/mi/google/gson/internal/LinkedTreeMap$LinkedTreeMapIterator;,
        Lcom/mi/google/gson/internal/LinkedTreeMap$Node;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/AbstractMap<",
        "TK;TV;>;",
        "Ljava/io/Serializable;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final NATURAL_ORDER:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator<",
            "Ljava/lang/Comparable;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field comparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator<",
            "-TK;>;"
        }
    .end annotation
.end field

.field private entrySet:Lcom/mi/google/gson/internal/LinkedTreeMap$EntrySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/mi/google/gson/internal/LinkedTreeMap<",
            "TK;TV;>.EntrySet;"
        }
    .end annotation
.end field

.field final header:Lcom/mi/google/gson/internal/LinkedTreeMap$Node;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/mi/google/gson/internal/LinkedTreeMap$Node<",
            "TK;TV;>;"
        }
    .end annotation
.end field

.field private keySet:Lcom/mi/google/gson/internal/LinkedTreeMap$KeySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/mi/google/gson/internal/LinkedTreeMap<",
            "TK;TV;>.KeySet;"
        }
    .end annotation
.end field

.field modCount:I

.field root:Lcom/mi/google/gson/internal/LinkedTreeMap$Node;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/mi/google/gson/internal/LinkedTreeMap$Node<",
            "TK;TV;>;"
        }
    .end annotation
.end field

.field size:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mi/google/gson/internal/LinkedTreeMap$1;

    invoke-direct {v0}, Lcom/mi/google/gson/internal/LinkedTreeMap$1;-><init>()V

    sput-object v0, Lcom/mi/google/gson/internal/LinkedTreeMap;->NATURAL_ORDER:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    sget-object v0, Lcom/mi/google/gson/internal/LinkedTreeMap;->NATURAL_ORDER:Ljava/util/Comparator;

    invoke-direct {p0, v0}, Lcom/mi/google/gson/internal/LinkedTreeMap;-><init>(Ljava/util/Comparator;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/Comparator;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Comparator<",
            "-TK;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/util/AbstractMap;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/mi/google/gson/internal/LinkedTreeMap;->size:I

    iput v0, p0, Lcom/mi/google/gson/internal/LinkedTreeMap;->modCount:I

    new-instance v0, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    invoke-direct {v0}, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;-><init>()V

    iput-object v0, p0, Lcom/mi/google/gson/internal/LinkedTreeMap;->header:Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/mi/google/gson/internal/LinkedTreeMap;->NATURAL_ORDER:Ljava/util/Comparator;

    :goto_0
    iput-object p1, p0, Lcom/mi/google/gson/internal/LinkedTreeMap;->comparator:Ljava/util/Comparator;

    return-void
.end method

.method private equal(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 0

    if-eq p1, p2, :cond_1

    if-eqz p1, :cond_0

    invoke-virtual {p1, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method private rebalance(Lcom/mi/google/gson/internal/LinkedTreeMap$Node;Z)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mi/google/gson/internal/LinkedTreeMap$Node<",
            "TK;TV;>;Z)V"
        }
    .end annotation

    :goto_0
    if-eqz p1, :cond_e

    iget-object v0, p1, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->left:Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    iget-object v1, p1, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->right:Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    iget v3, v0, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->height:I

    goto :goto_1

    :cond_0
    move v3, v2

    :goto_1
    if-eqz v1, :cond_1

    iget v4, v1, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->height:I

    goto :goto_2

    :cond_1
    move v4, v2

    :goto_2
    sub-int v5, v3, v4

    const/4 v6, -0x2

    if-ne v5, v6, :cond_6

    iget-object v0, v1, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->left:Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    iget-object v3, v1, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->right:Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    if-eqz v3, :cond_2

    iget v3, v3, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->height:I

    goto :goto_3

    :cond_2
    move v3, v2

    :goto_3
    if-eqz v0, :cond_3

    iget v2, v0, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->height:I

    :cond_3
    sub-int/2addr v2, v3

    const/4 v0, -0x1

    if-eq v2, v0, :cond_5

    if-nez v2, :cond_4

    if-nez p2, :cond_4

    goto :goto_4

    :cond_4
    invoke-direct {p0, v1}, Lcom/mi/google/gson/internal/LinkedTreeMap;->rotateRight(Lcom/mi/google/gson/internal/LinkedTreeMap$Node;)V

    invoke-direct {p0, p1}, Lcom/mi/google/gson/internal/LinkedTreeMap;->rotateLeft(Lcom/mi/google/gson/internal/LinkedTreeMap$Node;)V

    goto :goto_5

    :cond_5
    :goto_4
    invoke-direct {p0, p1}, Lcom/mi/google/gson/internal/LinkedTreeMap;->rotateLeft(Lcom/mi/google/gson/internal/LinkedTreeMap$Node;)V

    :goto_5
    if-eqz p2, :cond_d

    goto :goto_9

    :cond_6
    const/4 v1, 0x2

    const/4 v6, 0x1

    if-ne v5, v1, :cond_b

    iget-object v1, v0, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->left:Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    iget-object v3, v0, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->right:Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    if-eqz v3, :cond_7

    iget v3, v3, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->height:I

    goto :goto_6

    :cond_7
    move v3, v2

    :goto_6
    if-eqz v1, :cond_8

    iget v2, v1, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->height:I

    :cond_8
    sub-int/2addr v2, v3

    if-eq v2, v6, :cond_a

    if-nez v2, :cond_9

    if-nez p2, :cond_9

    goto :goto_7

    :cond_9
    invoke-direct {p0, v0}, Lcom/mi/google/gson/internal/LinkedTreeMap;->rotateLeft(Lcom/mi/google/gson/internal/LinkedTreeMap$Node;)V

    invoke-direct {p0, p1}, Lcom/mi/google/gson/internal/LinkedTreeMap;->rotateRight(Lcom/mi/google/gson/internal/LinkedTreeMap$Node;)V

    goto :goto_8

    :cond_a
    :goto_7
    invoke-direct {p0, p1}, Lcom/mi/google/gson/internal/LinkedTreeMap;->rotateRight(Lcom/mi/google/gson/internal/LinkedTreeMap$Node;)V

    :goto_8
    if-eqz p2, :cond_d

    goto :goto_9

    :cond_b
    if-nez v5, :cond_c

    add-int/lit8 v3, v3, 0x1

    iput v3, p1, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->height:I

    if-eqz p2, :cond_d

    goto :goto_9

    :cond_c
    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v0

    add-int/2addr v0, v6

    iput v0, p1, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->height:I

    if-nez p2, :cond_d

    goto :goto_9

    :cond_d
    iget-object p1, p1, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->parent:Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    goto :goto_0

    :cond_e
    :goto_9
    return-void
.end method

.method private replaceInParent(Lcom/mi/google/gson/internal/LinkedTreeMap$Node;Lcom/mi/google/gson/internal/LinkedTreeMap$Node;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mi/google/gson/internal/LinkedTreeMap$Node<",
            "TK;TV;>;",
            "Lcom/mi/google/gson/internal/LinkedTreeMap$Node<",
            "TK;TV;>;)V"
        }
    .end annotation

    iget-object v0, p1, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->parent:Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    const/4 v1, 0x0

    iput-object v1, p1, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->parent:Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    if-eqz p2, :cond_0

    iput-object v0, p2, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->parent:Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    :cond_0
    if-eqz v0, :cond_2

    iget-object v1, v0, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->left:Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    if-ne v1, p1, :cond_1

    iput-object p2, v0, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->left:Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    goto :goto_0

    :cond_1
    iput-object p2, v0, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->right:Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    goto :goto_0

    :cond_2
    iput-object p2, p0, Lcom/mi/google/gson/internal/LinkedTreeMap;->root:Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    :goto_0
    return-void
.end method

.method private rotateLeft(Lcom/mi/google/gson/internal/LinkedTreeMap$Node;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mi/google/gson/internal/LinkedTreeMap$Node<",
            "TK;TV;>;)V"
        }
    .end annotation

    iget-object v0, p1, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->left:Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    iget-object v1, p1, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->right:Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    iget-object v2, v1, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->left:Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    iget-object v3, v1, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->right:Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    iput-object v2, p1, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->right:Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    if-eqz v2, :cond_0

    iput-object p1, v2, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->parent:Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    :cond_0
    invoke-direct {p0, p1, v1}, Lcom/mi/google/gson/internal/LinkedTreeMap;->replaceInParent(Lcom/mi/google/gson/internal/LinkedTreeMap$Node;Lcom/mi/google/gson/internal/LinkedTreeMap$Node;)V

    iput-object p1, v1, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->left:Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    iput-object v1, p1, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->parent:Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    const/4 v4, 0x0

    if-eqz v0, :cond_1

    iget v0, v0, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->height:I

    goto :goto_0

    :cond_1
    move v0, v4

    :goto_0
    if-eqz v2, :cond_2

    iget v2, v2, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->height:I

    goto :goto_1

    :cond_2
    move v2, v4

    :goto_1
    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iput v0, p1, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->height:I

    iget p1, p1, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->height:I

    if-eqz v3, :cond_3

    iget v4, v3, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->height:I

    :cond_3
    invoke-static {p1, v4}, Ljava/lang/Math;->max(II)I

    move-result p1

    add-int/lit8 p1, p1, 0x1

    iput p1, v1, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->height:I

    return-void
.end method

.method private rotateRight(Lcom/mi/google/gson/internal/LinkedTreeMap$Node;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mi/google/gson/internal/LinkedTreeMap$Node<",
            "TK;TV;>;)V"
        }
    .end annotation

    iget-object v0, p1, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->left:Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    iget-object v1, p1, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->right:Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    iget-object v2, v0, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->left:Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    iget-object v3, v0, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->right:Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    iput-object v3, p1, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->left:Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    if-eqz v3, :cond_0

    iput-object p1, v3, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->parent:Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    :cond_0
    invoke-direct {p0, p1, v0}, Lcom/mi/google/gson/internal/LinkedTreeMap;->replaceInParent(Lcom/mi/google/gson/internal/LinkedTreeMap$Node;Lcom/mi/google/gson/internal/LinkedTreeMap$Node;)V

    iput-object p1, v0, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->right:Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    iput-object v0, p1, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->parent:Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    const/4 v4, 0x0

    if-eqz v1, :cond_1

    iget v1, v1, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->height:I

    goto :goto_0

    :cond_1
    move v1, v4

    :goto_0
    if-eqz v3, :cond_2

    iget v3, v3, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->height:I

    goto :goto_1

    :cond_2
    move v3, v4

    :goto_1
    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    iput v1, p1, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->height:I

    iget p1, p1, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->height:I

    if-eqz v2, :cond_3

    iget v4, v2, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->height:I

    :cond_3
    invoke-static {p1, v4}, Ljava/lang/Math;->max(II)I

    move-result p1

    add-int/lit8 p1, p1, 0x1

    iput p1, v0, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->height:I

    return-void
.end method

.method private writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0, p0}, Ljava/util/LinkedHashMap;-><init>(Ljava/util/Map;)V

    return-object v0
.end method


# virtual methods
.method public clear()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mi/google/gson/internal/LinkedTreeMap;->root:Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    const/4 v0, 0x0

    iput v0, p0, Lcom/mi/google/gson/internal/LinkedTreeMap;->size:I

    iget v0, p0, Lcom/mi/google/gson/internal/LinkedTreeMap;->modCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/mi/google/gson/internal/LinkedTreeMap;->modCount:I

    iget-object v0, p0, Lcom/mi/google/gson/internal/LinkedTreeMap;->header:Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    iput-object v0, v0, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->prev:Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    iput-object v0, v0, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->next:Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    return-void
.end method

.method public containsKey(Ljava/lang/Object;)Z
    .locals 0

    invoke-virtual {p0, p1}, Lcom/mi/google/gson/internal/LinkedTreeMap;->findByObject(Ljava/lang/Object;)Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    move-result-object p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public entrySet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/util/Map$Entry<",
            "TK;TV;>;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/mi/google/gson/internal/LinkedTreeMap;->entrySet:Lcom/mi/google/gson/internal/LinkedTreeMap$EntrySet;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/mi/google/gson/internal/LinkedTreeMap$EntrySet;

    invoke-direct {v0, p0}, Lcom/mi/google/gson/internal/LinkedTreeMap$EntrySet;-><init>(Lcom/mi/google/gson/internal/LinkedTreeMap;)V

    iput-object v0, p0, Lcom/mi/google/gson/internal/LinkedTreeMap;->entrySet:Lcom/mi/google/gson/internal/LinkedTreeMap$EntrySet;

    :goto_0
    return-object v0
.end method

.method find(Ljava/lang/Object;Z)Lcom/mi/google/gson/internal/LinkedTreeMap$Node;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;Z)",
            "Lcom/mi/google/gson/internal/LinkedTreeMap$Node<",
            "TK;TV;>;"
        }
    .end annotation

    goto/32 :goto_18

    nop

    :goto_0
    instance-of v0, p1, Ljava/lang/Comparable;

    goto/32 :goto_35

    nop

    :goto_1
    goto/16 :goto_41

    :goto_2
    goto/32 :goto_16

    nop

    :goto_3
    invoke-direct {v0, v1, p1, p2, v3}, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;-><init>(Lcom/mi/google/gson/internal/LinkedTreeMap$Node;Ljava/lang/Object;Lcom/mi/google/gson/internal/LinkedTreeMap$Node;Lcom/mi/google/gson/internal/LinkedTreeMap$Node;)V

    goto/32 :goto_51

    nop

    :goto_4
    if-eqz p2, :cond_0

    goto/32 :goto_2a

    :cond_0
    goto/32 :goto_29

    nop

    :goto_5
    new-instance p2, Ljava/lang/ClassCastException;

    goto/32 :goto_3e

    nop

    :goto_6
    invoke-direct {p0, v1, v2}, Lcom/mi/google/gson/internal/LinkedTreeMap;->rebalance(Lcom/mi/google/gson/internal/LinkedTreeMap$Node;Z)V

    :goto_7
    goto/32 :goto_45

    nop

    :goto_8
    add-int/2addr p1, v2

    goto/32 :goto_3f

    nop

    :goto_9
    sget-object v3, Lcom/mi/google/gson/internal/LinkedTreeMap;->NATURAL_ORDER:Ljava/util/Comparator;

    goto/32 :goto_d

    nop

    :goto_a
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_47

    nop

    :goto_b
    if-ltz v4, :cond_1

    goto/32 :goto_12

    :cond_1
    goto/32 :goto_44

    nop

    :goto_c
    iget-object v4, v1, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->key:Ljava/lang/Object;

    goto/32 :goto_15

    nop

    :goto_d
    if-eq v0, v3, :cond_2

    goto/32 :goto_f

    :cond_2
    goto/32 :goto_50

    nop

    :goto_e
    goto/16 :goto_4a

    :goto_f
    goto/32 :goto_49

    nop

    :goto_10
    const/4 v2, 0x0

    goto/32 :goto_32

    nop

    :goto_11
    goto :goto_23

    :goto_12
    goto/32 :goto_22

    nop

    :goto_13
    sget-object v3, Lcom/mi/google/gson/internal/LinkedTreeMap;->NATURAL_ORDER:Ljava/util/Comparator;

    goto/32 :goto_1d

    nop

    :goto_14
    iget-object p2, p0, Lcom/mi/google/gson/internal/LinkedTreeMap;->header:Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    goto/32 :goto_46

    nop

    :goto_15
    invoke-interface {v3, v4}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v4

    goto/32 :goto_1

    nop

    :goto_16
    iget-object v4, v1, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->key:Ljava/lang/Object;

    goto/32 :goto_40

    nop

    :goto_17
    new-instance v0, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    goto/32 :goto_53

    nop

    :goto_18
    iget-object v0, p0, Lcom/mi/google/gson/internal/LinkedTreeMap;->comparator:Ljava/util/Comparator;

    goto/32 :goto_2e

    nop

    :goto_19
    new-instance v0, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    goto/32 :goto_24

    nop

    :goto_1a
    iput-object v0, v1, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->right:Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    :goto_1b
    goto/32 :goto_6

    nop

    :goto_1c
    move-object v1, v5

    goto/32 :goto_3a

    nop

    :goto_1d
    if-eq v0, v3, :cond_3

    goto/32 :goto_28

    :cond_3
    goto/32 :goto_0

    nop

    :goto_1e
    goto :goto_28

    :goto_1f
    goto/32 :goto_5

    nop

    :goto_20
    return-object v0

    :goto_21
    if-eqz v1, :cond_4

    goto/32 :goto_2d

    :cond_4
    goto/32 :goto_13

    nop

    :goto_22
    iget-object v5, v1, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->right:Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    :goto_23
    goto/32 :goto_52

    nop

    :goto_24
    iget-object v3, p2, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->prev:Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    goto/32 :goto_3

    nop

    :goto_25
    return-object v1

    :goto_26
    goto/32 :goto_b

    nop

    :goto_27
    throw p2

    :goto_28
    goto/32 :goto_19

    nop

    :goto_29
    return-object v2

    :goto_2a
    goto/32 :goto_14

    nop

    :goto_2b
    check-cast v3, Ljava/lang/Comparable;

    goto/32 :goto_e

    nop

    :goto_2c
    goto/16 :goto_7

    :goto_2d
    goto/32 :goto_17

    nop

    :goto_2e
    iget-object v1, p0, Lcom/mi/google/gson/internal/LinkedTreeMap;->root:Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    goto/32 :goto_10

    nop

    :goto_2f
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_4e

    nop

    :goto_30
    iput-object v0, v1, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->left:Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    goto/32 :goto_38

    nop

    :goto_31
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    goto/32 :goto_3c

    nop

    :goto_32
    if-nez v1, :cond_5

    goto/32 :goto_3b

    :cond_5
    goto/32 :goto_9

    nop

    :goto_33
    invoke-direct {v0, v1, p1, p2, v3}, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;-><init>(Lcom/mi/google/gson/internal/LinkedTreeMap$Node;Ljava/lang/Object;Lcom/mi/google/gson/internal/LinkedTreeMap$Node;Lcom/mi/google/gson/internal/LinkedTreeMap$Node;)V

    goto/32 :goto_48

    nop

    :goto_34
    if-nez v3, :cond_6

    goto/32 :goto_2

    :cond_6
    goto/32 :goto_c

    nop

    :goto_35
    if-nez v0, :cond_7

    goto/32 :goto_1f

    :cond_7
    goto/32 :goto_1e

    nop

    :goto_36
    const/4 v4, 0x0

    :goto_37
    goto/32 :goto_4

    nop

    :goto_38
    goto/16 :goto_1b

    :goto_39
    goto/32 :goto_1a

    nop

    :goto_3a
    goto :goto_4a

    :goto_3b
    goto/32 :goto_36

    nop

    :goto_3c
    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_2f

    nop

    :goto_3d
    iget p1, p0, Lcom/mi/google/gson/internal/LinkedTreeMap;->modCount:I

    goto/32 :goto_8

    nop

    :goto_3e
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_4f

    nop

    :goto_3f
    iput p1, p0, Lcom/mi/google/gson/internal/LinkedTreeMap;->modCount:I

    goto/32 :goto_20

    nop

    :goto_40
    invoke-interface {v0, p1, v4}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v4

    :goto_41
    goto/32 :goto_4d

    nop

    :goto_42
    goto :goto_37

    :goto_43
    goto/32 :goto_1c

    nop

    :goto_44
    iget-object v5, v1, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->left:Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    goto/32 :goto_11

    nop

    :goto_45
    iget p1, p0, Lcom/mi/google/gson/internal/LinkedTreeMap;->size:I

    goto/32 :goto_4b

    nop

    :goto_46
    const/4 v2, 0x1

    goto/32 :goto_21

    nop

    :goto_47
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_4c

    nop

    :goto_48
    if-ltz v4, :cond_8

    goto/32 :goto_39

    :cond_8
    goto/32 :goto_30

    nop

    :goto_49
    move-object v3, v2

    :goto_4a
    goto/32 :goto_34

    nop

    :goto_4b
    add-int/2addr p1, v2

    goto/32 :goto_54

    nop

    :goto_4c
    invoke-direct {p2, p1}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_27

    nop

    :goto_4d
    if-eqz v4, :cond_9

    goto/32 :goto_26

    :cond_9
    goto/32 :goto_25

    nop

    :goto_4e
    const-string p1, " is not Comparable"

    goto/32 :goto_a

    nop

    :goto_4f
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_31

    nop

    :goto_50
    move-object v3, p1

    goto/32 :goto_2b

    nop

    :goto_51
    iput-object v0, p0, Lcom/mi/google/gson/internal/LinkedTreeMap;->root:Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    goto/32 :goto_2c

    nop

    :goto_52
    if-eqz v5, :cond_a

    goto/32 :goto_43

    :cond_a
    goto/32 :goto_42

    nop

    :goto_53
    iget-object v3, p2, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->prev:Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    goto/32 :goto_33

    nop

    :goto_54
    iput p1, p0, Lcom/mi/google/gson/internal/LinkedTreeMap;->size:I

    goto/32 :goto_3d

    nop
.end method

.method findByEntry(Ljava/util/Map$Entry;)Lcom/mi/google/gson/internal/LinkedTreeMap$Node;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map$Entry<",
            "**>;)",
            "Lcom/mi/google/gson/internal/LinkedTreeMap$Node<",
            "TK;TV;>;"
        }
    .end annotation

    goto/32 :goto_4

    nop

    :goto_0
    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object p1

    goto/32 :goto_5

    nop

    :goto_1
    if-nez v0, :cond_0

    goto/32 :goto_9

    :cond_0
    goto/32 :goto_11

    nop

    :goto_2
    const/4 p1, 0x0

    :goto_3
    goto/32 :goto_a

    nop

    :goto_4
    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_e

    nop

    :goto_5
    invoke-direct {p0, v1, p1}, Lcom/mi/google/gson/internal/LinkedTreeMap;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    goto/32 :goto_b

    nop

    :goto_6
    goto :goto_10

    :goto_7
    goto/32 :goto_f

    nop

    :goto_8
    goto :goto_3

    :goto_9
    goto/32 :goto_2

    nop

    :goto_a
    if-nez p1, :cond_1

    goto/32 :goto_7

    :cond_1
    goto/32 :goto_6

    nop

    :goto_b
    if-nez p1, :cond_2

    goto/32 :goto_9

    :cond_2
    goto/32 :goto_d

    nop

    :goto_c
    return-object v0

    :goto_d
    const/4 p1, 0x1

    goto/32 :goto_8

    nop

    :goto_e
    invoke-virtual {p0, v0}, Lcom/mi/google/gson/internal/LinkedTreeMap;->findByObject(Ljava/lang/Object;)Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    move-result-object v0

    goto/32 :goto_1

    nop

    :goto_f
    const/4 v0, 0x0

    :goto_10
    goto/32 :goto_c

    nop

    :goto_11
    iget-object v1, v0, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->value:Ljava/lang/Object;

    goto/32 :goto_0

    nop
.end method

.method findByObject(Ljava/lang/Object;)Lcom/mi/google/gson/internal/LinkedTreeMap$Node;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Lcom/mi/google/gson/internal/LinkedTreeMap$Node<",
            "TK;TV;>;"
        }
    .end annotation

    goto/32 :goto_5

    nop

    :goto_0
    return-object v0

    :goto_1
    goto/32 :goto_6

    nop

    :goto_2
    goto :goto_1

    :catch_0
    goto/32 :goto_0

    nop

    :goto_3
    if-nez p1, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_4

    nop

    :goto_4
    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p0, p1, v1}, Lcom/mi/google/gson/internal/LinkedTreeMap;->find(Ljava/lang/Object;Z)Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_2

    nop

    :goto_5
    const/4 v0, 0x0

    goto/32 :goto_3

    nop

    :goto_6
    return-object v0
.end method

.method public get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/mi/google/gson/internal/LinkedTreeMap;->findByObject(Ljava/lang/Object;)Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object p1, p1, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->value:Ljava/lang/Object;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method public keySet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "TK;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/mi/google/gson/internal/LinkedTreeMap;->keySet:Lcom/mi/google/gson/internal/LinkedTreeMap$KeySet;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/mi/google/gson/internal/LinkedTreeMap$KeySet;

    invoke-direct {v0, p0}, Lcom/mi/google/gson/internal/LinkedTreeMap$KeySet;-><init>(Lcom/mi/google/gson/internal/LinkedTreeMap;)V

    iput-object v0, p0, Lcom/mi/google/gson/internal/LinkedTreeMap;->keySet:Lcom/mi/google/gson/internal/LinkedTreeMap$KeySet;

    :goto_0
    return-object v0
.end method

.method public put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)TV;"
        }
    .end annotation

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/mi/google/gson/internal/LinkedTreeMap;->find(Ljava/lang/Object;Z)Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    move-result-object p1

    iget-object v0, p1, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->value:Ljava/lang/Object;

    iput-object p2, p1, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->value:Ljava/lang/Object;

    return-object v0

    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    const-string p2, "key == null"

    invoke-direct {p1, p2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public remove(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/mi/google/gson/internal/LinkedTreeMap;->removeInternalByKey(Ljava/lang/Object;)Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object p1, p1, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->value:Ljava/lang/Object;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method removeInternal(Lcom/mi/google/gson/internal/LinkedTreeMap$Node;Z)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mi/google/gson/internal/LinkedTreeMap$Node<",
            "TK;TV;>;Z)V"
        }
    .end annotation

    goto/32 :goto_3a

    nop

    :goto_0
    iput-object v3, p1, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->left:Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    goto/32 :goto_5

    nop

    :goto_1
    invoke-direct {p0, p1, v3}, Lcom/mi/google/gson/internal/LinkedTreeMap;->replaceInParent(Lcom/mi/google/gson/internal/LinkedTreeMap$Node;Lcom/mi/google/gson/internal/LinkedTreeMap$Node;)V

    :goto_2
    goto/32 :goto_3

    nop

    :goto_3
    invoke-direct {p0, v1, v2}, Lcom/mi/google/gson/internal/LinkedTreeMap;->rebalance(Lcom/mi/google/gson/internal/LinkedTreeMap$Node;Z)V

    goto/32 :goto_27

    nop

    :goto_4
    add-int/lit8 p1, p1, -0x1

    goto/32 :goto_37

    nop

    :goto_5
    goto :goto_12

    :goto_6
    goto/32 :goto_11

    nop

    :goto_7
    return-void

    :goto_8
    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto/32 :goto_26

    nop

    :goto_9
    if-nez v0, :cond_0

    goto/32 :goto_24

    :cond_0
    goto/32 :goto_d

    nop

    :goto_a
    iget v1, v0, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->height:I

    goto/32 :goto_33

    nop

    :goto_b
    const/4 v3, 0x0

    goto/32 :goto_32

    nop

    :goto_c
    if-nez v0, :cond_1

    goto/32 :goto_36

    :cond_1
    goto/32 :goto_39

    nop

    :goto_d
    invoke-direct {p0, p1, v0}, Lcom/mi/google/gson/internal/LinkedTreeMap;->replaceInParent(Lcom/mi/google/gson/internal/LinkedTreeMap$Node;Lcom/mi/google/gson/internal/LinkedTreeMap$Node;)V

    goto/32 :goto_2c

    nop

    :goto_e
    iget p1, p0, Lcom/mi/google/gson/internal/LinkedTreeMap;->modCount:I

    goto/32 :goto_3c

    nop

    :goto_f
    if-gt v1, v4, :cond_2

    goto/32 :goto_31

    :cond_2
    goto/32 :goto_18

    nop

    :goto_10
    invoke-direct {p0, p1, p2}, Lcom/mi/google/gson/internal/LinkedTreeMap;->replaceInParent(Lcom/mi/google/gson/internal/LinkedTreeMap$Node;Lcom/mi/google/gson/internal/LinkedTreeMap$Node;)V

    goto/32 :goto_41

    nop

    :goto_11
    move v1, v2

    :goto_12
    goto/32 :goto_28

    nop

    :goto_13
    iput-object v0, p2, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->next:Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    goto/32 :goto_34

    nop

    :goto_14
    iput p1, p0, Lcom/mi/google/gson/internal/LinkedTreeMap;->modCount:I

    goto/32 :goto_7

    nop

    :goto_15
    return-void

    :goto_16
    goto/32 :goto_19

    nop

    :goto_17
    iget-object v1, p1, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->parent:Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    goto/32 :goto_3b

    nop

    :goto_18
    invoke-virtual {p2}, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->last()Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    move-result-object p2

    goto/32 :goto_30

    nop

    :goto_19
    if-nez p2, :cond_3

    goto/32 :goto_20

    :cond_3
    goto/32 :goto_10

    nop

    :goto_1a
    invoke-virtual {v0}, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->first()Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    move-result-object p2

    :goto_1b
    goto/32 :goto_25

    nop

    :goto_1c
    iget-object v0, p1, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->next:Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    goto/32 :goto_13

    nop

    :goto_1d
    if-nez v0, :cond_4

    goto/32 :goto_16

    :cond_4
    goto/32 :goto_38

    nop

    :goto_1e
    invoke-direct {p0, p1, p2}, Lcom/mi/google/gson/internal/LinkedTreeMap;->replaceInParent(Lcom/mi/google/gson/internal/LinkedTreeMap$Node;Lcom/mi/google/gson/internal/LinkedTreeMap$Node;)V

    goto/32 :goto_15

    nop

    :goto_1f
    goto/16 :goto_2

    :goto_20
    goto/32 :goto_9

    nop

    :goto_21
    iput-object p2, v0, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->parent:Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    goto/32 :goto_35

    nop

    :goto_22
    iput-object p2, v0, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->parent:Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    goto/32 :goto_0

    nop

    :goto_23
    goto/16 :goto_2

    :goto_24
    goto/32 :goto_1

    nop

    :goto_25
    invoke-virtual {p0, p2, v2}, Lcom/mi/google/gson/internal/LinkedTreeMap;->removeInternal(Lcom/mi/google/gson/internal/LinkedTreeMap$Node;Z)V

    goto/32 :goto_2b

    nop

    :goto_26
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_2a

    nop

    :goto_27
    iget p1, p0, Lcom/mi/google/gson/internal/LinkedTreeMap;->size:I

    goto/32 :goto_4

    nop

    :goto_28
    iget-object v0, p1, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->right:Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    goto/32 :goto_c

    nop

    :goto_29
    iget-object v0, p1, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->right:Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    goto/32 :goto_17

    nop

    :goto_2a
    iput v0, p2, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->height:I

    goto/32 :goto_1e

    nop

    :goto_2b
    iget-object v0, p1, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->left:Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    goto/32 :goto_42

    nop

    :goto_2c
    iput-object v3, p1, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->right:Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    goto/32 :goto_23

    nop

    :goto_2d
    iget-object v0, p1, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->prev:Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    goto/32 :goto_3f

    nop

    :goto_2e
    iget-object p2, p1, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->left:Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    goto/32 :goto_29

    nop

    :goto_2f
    iput-object v0, p2, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->right:Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    goto/32 :goto_21

    nop

    :goto_30
    goto :goto_1b

    :goto_31
    goto/32 :goto_1a

    nop

    :goto_32
    if-nez p2, :cond_5

    goto/32 :goto_16

    :cond_5
    goto/32 :goto_1d

    nop

    :goto_33
    iput-object v0, p2, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->left:Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    goto/32 :goto_22

    nop

    :goto_34
    iget-object p2, p1, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->next:Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    goto/32 :goto_2d

    nop

    :goto_35
    iput-object v3, p1, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->right:Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    :goto_36
    goto/32 :goto_8

    nop

    :goto_37
    iput p1, p0, Lcom/mi/google/gson/internal/LinkedTreeMap;->size:I

    goto/32 :goto_e

    nop

    :goto_38
    iget v1, p2, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->height:I

    goto/32 :goto_3e

    nop

    :goto_39
    iget v2, v0, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->height:I

    goto/32 :goto_2f

    nop

    :goto_3a
    if-nez p2, :cond_6

    goto/32 :goto_40

    :cond_6
    goto/32 :goto_3d

    nop

    :goto_3b
    const/4 v2, 0x0

    goto/32 :goto_b

    nop

    :goto_3c
    add-int/lit8 p1, p1, 0x1

    goto/32 :goto_14

    nop

    :goto_3d
    iget-object p2, p1, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->prev:Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    goto/32 :goto_1c

    nop

    :goto_3e
    iget v4, v0, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->height:I

    goto/32 :goto_f

    nop

    :goto_3f
    iput-object v0, p2, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->prev:Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    :goto_40
    goto/32 :goto_2e

    nop

    :goto_41
    iput-object v3, p1, Lcom/mi/google/gson/internal/LinkedTreeMap$Node;->left:Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    goto/32 :goto_1f

    nop

    :goto_42
    if-nez v0, :cond_7

    goto/32 :goto_6

    :cond_7
    goto/32 :goto_a

    nop
.end method

.method removeInternalByKey(Ljava/lang/Object;)Lcom/mi/google/gson/internal/LinkedTreeMap$Node;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Lcom/mi/google/gson/internal/LinkedTreeMap$Node<",
            "TK;TV;>;"
        }
    .end annotation

    goto/32 :goto_0

    nop

    :goto_0
    invoke-virtual {p0, p1}, Lcom/mi/google/gson/internal/LinkedTreeMap;->findByObject(Ljava/lang/Object;)Lcom/mi/google/gson/internal/LinkedTreeMap$Node;

    move-result-object p1

    goto/32 :goto_3

    nop

    :goto_1
    invoke-virtual {p0, p1, v0}, Lcom/mi/google/gson/internal/LinkedTreeMap;->removeInternal(Lcom/mi/google/gson/internal/LinkedTreeMap$Node;Z)V

    :goto_2
    goto/32 :goto_5

    nop

    :goto_3
    if-nez p1, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_4

    nop

    :goto_4
    const/4 v0, 0x1

    goto/32 :goto_1

    nop

    :goto_5
    return-object p1
.end method

.method public size()I
    .locals 1

    iget v0, p0, Lcom/mi/google/gson/internal/LinkedTreeMap;->size:I

    return v0
.end method
