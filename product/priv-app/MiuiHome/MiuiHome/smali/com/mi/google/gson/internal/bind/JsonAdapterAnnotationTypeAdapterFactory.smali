.class public final Lcom/mi/google/gson/internal/bind/JsonAdapterAnnotationTypeAdapterFactory;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/mi/google/gson/TypeAdapterFactory;


# instance fields
.field private final constructorConstructor:Lcom/mi/google/gson/internal/ConstructorConstructor;


# direct methods
.method public constructor <init>(Lcom/mi/google/gson/internal/ConstructorConstructor;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/mi/google/gson/internal/bind/JsonAdapterAnnotationTypeAdapterFactory;->constructorConstructor:Lcom/mi/google/gson/internal/ConstructorConstructor;

    return-void
.end method


# virtual methods
.method public create(Lcom/mi/google/gson/Gson;Lcom/mi/google/gson/reflect/TypeToken;)Lcom/mi/google/gson/TypeAdapter;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/mi/google/gson/Gson;",
            "Lcom/mi/google/gson/reflect/TypeToken<",
            "TT;>;)",
            "Lcom/mi/google/gson/TypeAdapter<",
            "TT;>;"
        }
    .end annotation

    invoke-virtual {p2}, Lcom/mi/google/gson/reflect/TypeToken;->getRawType()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/mi/google/gson/annotations/JsonAdapter;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/mi/google/gson/annotations/JsonAdapter;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    iget-object v1, p0, Lcom/mi/google/gson/internal/bind/JsonAdapterAnnotationTypeAdapterFactory;->constructorConstructor:Lcom/mi/google/gson/internal/ConstructorConstructor;

    invoke-virtual {p0, v1, p1, p2, v0}, Lcom/mi/google/gson/internal/bind/JsonAdapterAnnotationTypeAdapterFactory;->getTypeAdapter(Lcom/mi/google/gson/internal/ConstructorConstructor;Lcom/mi/google/gson/Gson;Lcom/mi/google/gson/reflect/TypeToken;Lcom/mi/google/gson/annotations/JsonAdapter;)Lcom/mi/google/gson/TypeAdapter;

    move-result-object p1

    return-object p1
.end method

.method getTypeAdapter(Lcom/mi/google/gson/internal/ConstructorConstructor;Lcom/mi/google/gson/Gson;Lcom/mi/google/gson/reflect/TypeToken;Lcom/mi/google/gson/annotations/JsonAdapter;)Lcom/mi/google/gson/TypeAdapter;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mi/google/gson/internal/ConstructorConstructor;",
            "Lcom/mi/google/gson/Gson;",
            "Lcom/mi/google/gson/reflect/TypeToken<",
            "*>;",
            "Lcom/mi/google/gson/annotations/JsonAdapter;",
            ")",
            "Lcom/mi/google/gson/TypeAdapter<",
            "*>;"
        }
    .end annotation

    goto/32 :goto_27

    nop

    :goto_0
    check-cast v1, Lcom/mi/google/gson/JsonDeserializer;

    :goto_1
    goto/32 :goto_21

    nop

    :goto_2
    instance-of v1, p1, Lcom/mi/google/gson/JsonDeserializer;

    goto/32 :goto_25

    nop

    :goto_3
    invoke-interface {p1, p2, p3}, Lcom/mi/google/gson/TypeAdapterFactory;->create(Lcom/mi/google/gson/Gson;Lcom/mi/google/gson/reflect/TypeToken;)Lcom/mi/google/gson/TypeAdapter;

    move-result-object p1

    goto/32 :goto_1d

    nop

    :goto_4
    goto/16 :goto_29

    :goto_5
    goto/32 :goto_12

    nop

    :goto_6
    move-object v1, p1

    goto/32 :goto_0

    nop

    :goto_7
    invoke-interface {p1}, Lcom/mi/google/gson/internal/ObjectConstructor;->construct()Ljava/lang/Object;

    move-result-object p1

    goto/32 :goto_14

    nop

    :goto_8
    if-nez v0, :cond_0

    goto/32 :goto_1e

    :cond_0
    goto/32 :goto_18

    nop

    :goto_9
    goto :goto_1c

    :goto_a
    goto/32 :goto_2d

    nop

    :goto_b
    check-cast v0, Lcom/mi/google/gson/JsonSerializer;

    goto/32 :goto_35

    nop

    :goto_c
    const-string p2, "@JsonAdapter value must be TypeAdapter, TypeAdapterFactory, JsonSerializer or JsonDeserializer reference."

    goto/32 :goto_11

    nop

    :goto_d
    instance-of v0, p1, Lcom/mi/google/gson/JsonDeserializer;

    goto/32 :goto_32

    nop

    :goto_e
    instance-of v0, p1, Lcom/mi/google/gson/JsonSerializer;

    goto/32 :goto_16

    nop

    :goto_f
    move-object v3, v1

    :goto_10
    goto/32 :goto_d

    nop

    :goto_11
    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_28

    nop

    :goto_12
    new-instance p1, Ljava/lang/IllegalArgumentException;

    goto/32 :goto_c

    nop

    :goto_13
    new-instance p1, Lcom/mi/google/gson/internal/bind/TreeTypeAdapter;

    goto/32 :goto_1f

    nop

    :goto_14
    instance-of v0, p1, Lcom/mi/google/gson/TypeAdapter;

    goto/32 :goto_2c

    nop

    :goto_15
    move-object v6, p3

    goto/32 :goto_1b

    nop

    :goto_16
    if-eqz v0, :cond_1

    goto/32 :goto_29

    :cond_1
    goto/32 :goto_2

    nop

    :goto_17
    if-nez p1, :cond_2

    goto/32 :goto_2f

    :cond_2
    goto/32 :goto_1a

    nop

    :goto_18
    check-cast p1, Lcom/mi/google/gson/TypeAdapterFactory;

    goto/32 :goto_3

    nop

    :goto_19
    move-object v2, p1

    goto/32 :goto_2a

    nop

    :goto_1a
    invoke-interface {p4}, Lcom/mi/google/gson/annotations/JsonAdapter;->nullSafe()Z

    move-result p2

    goto/32 :goto_26

    nop

    :goto_1b
    invoke-direct/range {v2 .. v7}, Lcom/mi/google/gson/internal/bind/TreeTypeAdapter;-><init>(Lcom/mi/google/gson/JsonSerializer;Lcom/mi/google/gson/JsonDeserializer;Lcom/mi/google/gson/Gson;Lcom/mi/google/gson/reflect/TypeToken;Lcom/mi/google/gson/TypeAdapterFactory;)V

    :goto_1c
    goto/32 :goto_17

    nop

    :goto_1d
    goto :goto_1c

    :goto_1e
    goto/32 :goto_e

    nop

    :goto_1f
    const/4 v7, 0x0

    goto/32 :goto_19

    nop

    :goto_20
    move-object v0, p1

    goto/32 :goto_b

    nop

    :goto_21
    move-object v4, v1

    goto/32 :goto_13

    nop

    :goto_22
    goto :goto_10

    :goto_23
    goto/32 :goto_f

    nop

    :goto_24
    return-object p1

    :goto_25
    if-nez v1, :cond_3

    goto/32 :goto_5

    :cond_3
    goto/32 :goto_4

    nop

    :goto_26
    if-nez p2, :cond_4

    goto/32 :goto_2f

    :cond_4
    goto/32 :goto_2e

    nop

    :goto_27
    invoke-interface {p4}, Lcom/mi/google/gson/annotations/JsonAdapter;->value()Ljava/lang/Class;

    move-result-object v0

    goto/32 :goto_34

    nop

    :goto_28
    throw p1

    :goto_29
    goto/32 :goto_30

    nop

    :goto_2a
    move-object v5, p2

    goto/32 :goto_15

    nop

    :goto_2b
    check-cast p1, Lcom/mi/google/gson/TypeAdapter;

    goto/32 :goto_9

    nop

    :goto_2c
    if-nez v0, :cond_5

    goto/32 :goto_a

    :cond_5
    goto/32 :goto_2b

    nop

    :goto_2d
    instance-of v0, p1, Lcom/mi/google/gson/TypeAdapterFactory;

    goto/32 :goto_8

    nop

    :goto_2e
    invoke-virtual {p1}, Lcom/mi/google/gson/TypeAdapter;->nullSafe()Lcom/mi/google/gson/TypeAdapter;

    move-result-object p1

    :goto_2f
    goto/32 :goto_24

    nop

    :goto_30
    const/4 v1, 0x0

    goto/32 :goto_33

    nop

    :goto_31
    invoke-virtual {p1, v0}, Lcom/mi/google/gson/internal/ConstructorConstructor;->get(Lcom/mi/google/gson/reflect/TypeToken;)Lcom/mi/google/gson/internal/ObjectConstructor;

    move-result-object p1

    goto/32 :goto_7

    nop

    :goto_32
    if-nez v0, :cond_6

    goto/32 :goto_1

    :cond_6
    goto/32 :goto_6

    nop

    :goto_33
    if-nez v0, :cond_7

    goto/32 :goto_23

    :cond_7
    goto/32 :goto_20

    nop

    :goto_34
    invoke-static {v0}, Lcom/mi/google/gson/reflect/TypeToken;->get(Ljava/lang/Class;)Lcom/mi/google/gson/reflect/TypeToken;

    move-result-object v0

    goto/32 :goto_31

    nop

    :goto_35
    move-object v3, v0

    goto/32 :goto_22

    nop
.end method
