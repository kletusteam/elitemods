.class Lcom/mi/google/gson/internal/bind/ReflectiveTypeAdapterFactory$1;
.super Lcom/mi/google/gson/internal/bind/ReflectiveTypeAdapterFactory$BoundField;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mi/google/gson/internal/bind/ReflectiveTypeAdapterFactory;->createBoundField(Lcom/mi/google/gson/Gson;Ljava/lang/reflect/Field;Ljava/lang/String;Lcom/mi/google/gson/reflect/TypeToken;ZZ)Lcom/mi/google/gson/internal/bind/ReflectiveTypeAdapterFactory$BoundField;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mi/google/gson/internal/bind/ReflectiveTypeAdapterFactory;

.field final synthetic val$context:Lcom/mi/google/gson/Gson;

.field final synthetic val$field:Ljava/lang/reflect/Field;

.field final synthetic val$fieldType:Lcom/mi/google/gson/reflect/TypeToken;

.field final synthetic val$isPrimitive:Z

.field final synthetic val$jsonAdapterPresent:Z

.field final synthetic val$typeAdapter:Lcom/mi/google/gson/TypeAdapter;


# direct methods
.method constructor <init>(Lcom/mi/google/gson/internal/bind/ReflectiveTypeAdapterFactory;Ljava/lang/String;ZZLjava/lang/reflect/Field;ZLcom/mi/google/gson/TypeAdapter;Lcom/mi/google/gson/Gson;Lcom/mi/google/gson/reflect/TypeToken;Z)V
    .locals 0

    iput-object p1, p0, Lcom/mi/google/gson/internal/bind/ReflectiveTypeAdapterFactory$1;->this$0:Lcom/mi/google/gson/internal/bind/ReflectiveTypeAdapterFactory;

    iput-object p5, p0, Lcom/mi/google/gson/internal/bind/ReflectiveTypeAdapterFactory$1;->val$field:Ljava/lang/reflect/Field;

    iput-boolean p6, p0, Lcom/mi/google/gson/internal/bind/ReflectiveTypeAdapterFactory$1;->val$jsonAdapterPresent:Z

    iput-object p7, p0, Lcom/mi/google/gson/internal/bind/ReflectiveTypeAdapterFactory$1;->val$typeAdapter:Lcom/mi/google/gson/TypeAdapter;

    iput-object p8, p0, Lcom/mi/google/gson/internal/bind/ReflectiveTypeAdapterFactory$1;->val$context:Lcom/mi/google/gson/Gson;

    iput-object p9, p0, Lcom/mi/google/gson/internal/bind/ReflectiveTypeAdapterFactory$1;->val$fieldType:Lcom/mi/google/gson/reflect/TypeToken;

    iput-boolean p10, p0, Lcom/mi/google/gson/internal/bind/ReflectiveTypeAdapterFactory$1;->val$isPrimitive:Z

    invoke-direct {p0, p2, p3, p4}, Lcom/mi/google/gson/internal/bind/ReflectiveTypeAdapterFactory$BoundField;-><init>(Ljava/lang/String;ZZ)V

    return-void
.end method


# virtual methods
.method read(Lcom/mi/google/gson/stream/JsonReader;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalAccessException;
        }
    .end annotation

    goto/32 :goto_1

    nop

    :goto_0
    iget-boolean v0, p0, Lcom/mi/google/gson/internal/bind/ReflectiveTypeAdapterFactory$1;->val$isPrimitive:Z

    goto/32 :goto_5

    nop

    :goto_1
    iget-object v0, p0, Lcom/mi/google/gson/internal/bind/ReflectiveTypeAdapterFactory$1;->val$typeAdapter:Lcom/mi/google/gson/TypeAdapter;

    goto/32 :goto_2

    nop

    :goto_2
    invoke-virtual {v0, p1}, Lcom/mi/google/gson/TypeAdapter;->read(Lcom/mi/google/gson/stream/JsonReader;)Ljava/lang/Object;

    move-result-object p1

    goto/32 :goto_8

    nop

    :goto_3
    invoke-virtual {v0, p2, p1}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_4
    goto/32 :goto_7

    nop

    :goto_5
    if-eqz v0, :cond_0

    goto/32 :goto_4

    :cond_0
    :goto_6
    goto/32 :goto_9

    nop

    :goto_7
    return-void

    :goto_8
    if-eqz p1, :cond_1

    goto/32 :goto_6

    :cond_1
    goto/32 :goto_0

    nop

    :goto_9
    iget-object v0, p0, Lcom/mi/google/gson/internal/bind/ReflectiveTypeAdapterFactory$1;->val$field:Ljava/lang/reflect/Field;

    goto/32 :goto_3

    nop
.end method

.method write(Lcom/mi/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalAccessException;
        }
    .end annotation

    goto/32 :goto_e

    nop

    :goto_0
    invoke-direct {v0, v1, v2, v3}, Lcom/mi/google/gson/internal/bind/TypeAdapterRuntimeTypeWrapper;-><init>(Lcom/mi/google/gson/Gson;Lcom/mi/google/gson/TypeAdapter;Ljava/lang/reflect/Type;)V

    :goto_1
    goto/32 :goto_5

    nop

    :goto_2
    iget-object v1, p0, Lcom/mi/google/gson/internal/bind/ReflectiveTypeAdapterFactory$1;->val$context:Lcom/mi/google/gson/Gson;

    goto/32 :goto_7

    nop

    :goto_3
    invoke-virtual {v3}, Lcom/mi/google/gson/reflect/TypeToken;->getType()Ljava/lang/reflect/Type;

    move-result-object v3

    goto/32 :goto_0

    nop

    :goto_4
    iget-boolean v0, p0, Lcom/mi/google/gson/internal/bind/ReflectiveTypeAdapterFactory$1;->val$jsonAdapterPresent:Z

    goto/32 :goto_8

    nop

    :goto_5
    invoke-virtual {v0, p1, p2}, Lcom/mi/google/gson/TypeAdapter;->write(Lcom/mi/google/gson/stream/JsonWriter;Ljava/lang/Object;)V

    goto/32 :goto_b

    nop

    :goto_6
    iget-object v3, p0, Lcom/mi/google/gson/internal/bind/ReflectiveTypeAdapterFactory$1;->val$fieldType:Lcom/mi/google/gson/reflect/TypeToken;

    goto/32 :goto_3

    nop

    :goto_7
    iget-object v2, p0, Lcom/mi/google/gson/internal/bind/ReflectiveTypeAdapterFactory$1;->val$typeAdapter:Lcom/mi/google/gson/TypeAdapter;

    goto/32 :goto_6

    nop

    :goto_8
    if-nez v0, :cond_0

    goto/32 :goto_d

    :cond_0
    goto/32 :goto_f

    nop

    :goto_9
    new-instance v0, Lcom/mi/google/gson/internal/bind/TypeAdapterRuntimeTypeWrapper;

    goto/32 :goto_2

    nop

    :goto_a
    invoke-virtual {v0, p2}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    goto/32 :goto_4

    nop

    :goto_b
    return-void

    :goto_c
    goto :goto_1

    :goto_d
    goto/32 :goto_9

    nop

    :goto_e
    iget-object v0, p0, Lcom/mi/google/gson/internal/bind/ReflectiveTypeAdapterFactory$1;->val$field:Ljava/lang/reflect/Field;

    goto/32 :goto_a

    nop

    :goto_f
    iget-object v0, p0, Lcom/mi/google/gson/internal/bind/ReflectiveTypeAdapterFactory$1;->val$typeAdapter:Lcom/mi/google/gson/TypeAdapter;

    goto/32 :goto_c

    nop
.end method

.method public writeField(Ljava/lang/Object;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalAccessException;
        }
    .end annotation

    iget-boolean v0, p0, Lcom/mi/google/gson/internal/bind/ReflectiveTypeAdapterFactory$1;->serialized:Z

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    iget-object v0, p0, Lcom/mi/google/gson/internal/bind/ReflectiveTypeAdapterFactory$1;->val$field:Ljava/lang/reflect/Field;

    invoke-virtual {v0, p1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eq v0, p1, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method
