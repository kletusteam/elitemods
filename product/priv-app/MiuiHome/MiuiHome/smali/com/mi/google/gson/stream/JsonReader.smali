.class public Lcom/mi/google/gson/stream/JsonReader;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Closeable;


# static fields
.field private static final NON_EXECUTE_PREFIX:[C


# instance fields
.field private final buffer:[C

.field private final in:Ljava/io/Reader;

.field private lenient:Z

.field private limit:I

.field private lineNumber:I

.field private lineStart:I

.field private pathIndices:[I

.field private pathNames:[Ljava/lang/String;

.field peeked:I

.field private peekedLong:J

.field private peekedNumberLength:I

.field private peekedString:Ljava/lang/String;

.field private pos:I

.field private stack:[I

.field private stackSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, ")]}\'\n"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lcom/mi/google/gson/stream/JsonReader;->NON_EXECUTE_PREFIX:[C

    new-instance v0, Lcom/mi/google/gson/stream/JsonReader$1;

    invoke-direct {v0}, Lcom/mi/google/gson/stream/JsonReader$1;-><init>()V

    sput-object v0, Lcom/mi/google/gson/internal/JsonReaderInternalAccess;->INSTANCE:Lcom/mi/google/gson/internal/JsonReaderInternalAccess;

    return-void
.end method

.method public constructor <init>(Ljava/io/Reader;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mi/google/gson/stream/JsonReader;->lenient:Z

    const/16 v1, 0x400

    new-array v1, v1, [C

    iput-object v1, p0, Lcom/mi/google/gson/stream/JsonReader;->buffer:[C

    iput v0, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    iput v0, p0, Lcom/mi/google/gson/stream/JsonReader;->limit:I

    iput v0, p0, Lcom/mi/google/gson/stream/JsonReader;->lineNumber:I

    iput v0, p0, Lcom/mi/google/gson/stream/JsonReader;->lineStart:I

    iput v0, p0, Lcom/mi/google/gson/stream/JsonReader;->peeked:I

    const/16 v1, 0x20

    new-array v2, v1, [I

    iput-object v2, p0, Lcom/mi/google/gson/stream/JsonReader;->stack:[I

    iput v0, p0, Lcom/mi/google/gson/stream/JsonReader;->stackSize:I

    iget-object v0, p0, Lcom/mi/google/gson/stream/JsonReader;->stack:[I

    iget v2, p0, Lcom/mi/google/gson/stream/JsonReader;->stackSize:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/mi/google/gson/stream/JsonReader;->stackSize:I

    const/4 v3, 0x6

    aput v3, v0, v2

    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, Lcom/mi/google/gson/stream/JsonReader;->pathNames:[Ljava/lang/String;

    new-array v0, v1, [I

    iput-object v0, p0, Lcom/mi/google/gson/stream/JsonReader;->pathIndices:[I

    if-eqz p1, :cond_0

    iput-object p1, p0, Lcom/mi/google/gson/stream/JsonReader;->in:Ljava/io/Reader;

    return-void

    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    const-string v0, "in == null"

    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method static synthetic access$000(Lcom/mi/google/gson/stream/JsonReader;)Ljava/lang/String;
    .locals 0

    invoke-direct {p0}, Lcom/mi/google/gson/stream/JsonReader;->locationString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private checkLenient()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-boolean v0, p0, Lcom/mi/google/gson/stream/JsonReader;->lenient:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const-string v0, "Use JsonReader.setLenient(true) to accept malformed JSON"

    invoke-direct {p0, v0}, Lcom/mi/google/gson/stream/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0
.end method

.method private consumeNonExecutePrefix()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/mi/google/gson/stream/JsonReader;->nextNonWhitespace(Z)I

    iget v1, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    sub-int/2addr v1, v0

    iput v1, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    iget v0, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    sget-object v1, Lcom/mi/google/gson/stream/JsonReader;->NON_EXECUTE_PREFIX:[C

    array-length v2, v1

    add-int/2addr v0, v2

    iget v2, p0, Lcom/mi/google/gson/stream/JsonReader;->limit:I

    if-le v0, v2, :cond_0

    array-length v0, v1

    invoke-direct {p0, v0}, Lcom/mi/google/gson/stream/JsonReader;->fillBuffer(I)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    :goto_0
    sget-object v1, Lcom/mi/google/gson/stream/JsonReader;->NON_EXECUTE_PREFIX:[C

    array-length v2, v1

    if-ge v0, v2, :cond_2

    iget-object v2, p0, Lcom/mi/google/gson/stream/JsonReader;->buffer:[C

    iget v3, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    add-int/2addr v3, v0

    aget-char v2, v2, v3

    aget-char v1, v1, v0

    if-eq v2, v1, :cond_1

    return-void

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    array-length v1, v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    return-void
.end method

.method private fillBuffer(I)Z
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/mi/google/gson/stream/JsonReader;->buffer:[C

    iget v1, p0, Lcom/mi/google/gson/stream/JsonReader;->lineStart:I

    iget v2, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/mi/google/gson/stream/JsonReader;->lineStart:I

    iget v1, p0, Lcom/mi/google/gson/stream/JsonReader;->limit:I

    const/4 v3, 0x0

    if-eq v1, v2, :cond_0

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/mi/google/gson/stream/JsonReader;->limit:I

    iget v1, p0, Lcom/mi/google/gson/stream/JsonReader;->limit:I

    invoke-static {v0, v2, v0, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0

    :cond_0
    iput v3, p0, Lcom/mi/google/gson/stream/JsonReader;->limit:I

    :goto_0
    iput v3, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    :cond_1
    iget-object v1, p0, Lcom/mi/google/gson/stream/JsonReader;->in:Ljava/io/Reader;

    iget v2, p0, Lcom/mi/google/gson/stream/JsonReader;->limit:I

    array-length v4, v0

    sub-int/2addr v4, v2

    invoke-virtual {v1, v0, v2, v4}, Ljava/io/Reader;->read([CII)I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_3

    iget v2, p0, Lcom/mi/google/gson/stream/JsonReader;->limit:I

    add-int/2addr v2, v1

    iput v2, p0, Lcom/mi/google/gson/stream/JsonReader;->limit:I

    iget v1, p0, Lcom/mi/google/gson/stream/JsonReader;->lineNumber:I

    const/4 v2, 0x1

    if-nez v1, :cond_2

    iget v1, p0, Lcom/mi/google/gson/stream/JsonReader;->lineStart:I

    if-nez v1, :cond_2

    iget v4, p0, Lcom/mi/google/gson/stream/JsonReader;->limit:I

    if-lez v4, :cond_2

    aget-char v4, v0, v3

    const v5, 0xfeff

    if-ne v4, v5, :cond_2

    iget v4, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    add-int/2addr v4, v2

    iput v4, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/mi/google/gson/stream/JsonReader;->lineStart:I

    add-int/lit8 p1, p1, 0x1

    :cond_2
    iget v1, p0, Lcom/mi/google/gson/stream/JsonReader;->limit:I

    if-lt v1, p1, :cond_1

    return v2

    :cond_3
    return v3
.end method

.method private isLiteral(C)Z
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sparse-switch p1, :sswitch_data_0

    const/4 p1, 0x1

    return p1

    :sswitch_0
    invoke-direct {p0}, Lcom/mi/google/gson/stream/JsonReader;->checkLenient()V

    :sswitch_1
    const/4 p1, 0x0

    return p1

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_1
        0xa -> :sswitch_1
        0xc -> :sswitch_1
        0xd -> :sswitch_1
        0x20 -> :sswitch_1
        0x23 -> :sswitch_0
        0x2c -> :sswitch_1
        0x2f -> :sswitch_0
        0x3a -> :sswitch_1
        0x3b -> :sswitch_0
        0x3d -> :sswitch_0
        0x5b -> :sswitch_1
        0x5c -> :sswitch_0
        0x5d -> :sswitch_1
        0x7b -> :sswitch_1
        0x7d -> :sswitch_1
    .end sparse-switch
.end method

.method private locationString()Ljava/lang/String;
    .locals 4

    iget v0, p0, Lcom/mi/google/gson/stream/JsonReader;->lineNumber:I

    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    iget v2, p0, Lcom/mi/google/gson/stream/JsonReader;->lineStart:I

    sub-int/2addr v1, v2

    add-int/lit8 v1, v1, 0x1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " at line "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, " column "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, " path "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/mi/google/gson/stream/JsonReader;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private nextNonWhitespace(Z)I
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/mi/google/gson/stream/JsonReader;->buffer:[C

    iget v1, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    iget v2, p0, Lcom/mi/google/gson/stream/JsonReader;->limit:I

    :goto_0
    const/4 v3, 0x1

    if-ne v1, v2, :cond_2

    iput v1, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    invoke-direct {p0, v3}, Lcom/mi/google/gson/stream/JsonReader;->fillBuffer(I)Z

    move-result v1

    if-nez v1, :cond_1

    if-nez p1, :cond_0

    const/4 p1, -0x1

    return p1

    :cond_0
    new-instance p1, Ljava/io/EOFException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "End of input"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-direct {p0}, Lcom/mi/google/gson/stream/JsonReader;->locationString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    iget v1, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    iget v2, p0, Lcom/mi/google/gson/stream/JsonReader;->limit:I

    :cond_2
    add-int/lit8 v4, v1, 0x1

    aget-char v1, v0, v1

    const/16 v5, 0xa

    if-ne v1, v5, :cond_3

    iget v1, p0, Lcom/mi/google/gson/stream/JsonReader;->lineNumber:I

    add-int/2addr v1, v3

    iput v1, p0, Lcom/mi/google/gson/stream/JsonReader;->lineNumber:I

    iput v4, p0, Lcom/mi/google/gson/stream/JsonReader;->lineStart:I

    goto/16 :goto_1

    :cond_3
    const/16 v5, 0x20

    if-eq v1, v5, :cond_b

    const/16 v5, 0xd

    if-eq v1, v5, :cond_b

    const/16 v5, 0x9

    if-ne v1, v5, :cond_4

    goto :goto_1

    :cond_4
    const/16 v5, 0x2f

    if-ne v1, v5, :cond_9

    iput v4, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    const/4 v6, 0x2

    if-ne v4, v2, :cond_5

    iget v2, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    sub-int/2addr v2, v3

    iput v2, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    invoke-direct {p0, v6}, Lcom/mi/google/gson/stream/JsonReader;->fillBuffer(I)Z

    move-result v2

    iget v4, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    add-int/2addr v4, v3

    iput v4, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    if-nez v2, :cond_5

    return v1

    :cond_5
    invoke-direct {p0}, Lcom/mi/google/gson/stream/JsonReader;->checkLenient()V

    iget v2, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    aget-char v3, v0, v2

    const/16 v4, 0x2a

    if-eq v3, v4, :cond_7

    if-eq v3, v5, :cond_6

    return v1

    :cond_6
    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    invoke-direct {p0}, Lcom/mi/google/gson/stream/JsonReader;->skipToEndOfLine()V

    iget v1, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    iget v2, p0, Lcom/mi/google/gson/stream/JsonReader;->limit:I

    goto :goto_0

    :cond_7
    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    const-string v1, "*/"

    invoke-direct {p0, v1}, Lcom/mi/google/gson/stream/JsonReader;->skipTo(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    iget v1, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    add-int/2addr v1, v6

    iget v2, p0, Lcom/mi/google/gson/stream/JsonReader;->limit:I

    goto/16 :goto_0

    :cond_8
    const-string p1, "Unterminated comment"

    invoke-direct {p0, p1}, Lcom/mi/google/gson/stream/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object p1

    throw p1

    :cond_9
    const/16 v2, 0x23

    if-ne v1, v2, :cond_a

    iput v4, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    invoke-direct {p0}, Lcom/mi/google/gson/stream/JsonReader;->checkLenient()V

    invoke-direct {p0}, Lcom/mi/google/gson/stream/JsonReader;->skipToEndOfLine()V

    iget v1, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    iget v2, p0, Lcom/mi/google/gson/stream/JsonReader;->limit:I

    goto/16 :goto_0

    :cond_a
    iput v4, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    return v1

    :cond_b
    :goto_1
    move v1, v4

    goto/16 :goto_0
.end method

.method private nextQuotedValue(C)Ljava/lang/String;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/mi/google/gson/stream/JsonReader;->buffer:[C

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    :goto_0
    iget v2, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    iget v3, p0, Lcom/mi/google/gson/stream/JsonReader;->limit:I

    move v4, v2

    :goto_1
    const/4 v5, 0x1

    if-ge v2, v3, :cond_3

    add-int/lit8 v6, v2, 0x1

    aget-char v2, v0, v2

    if-ne v2, p1, :cond_0

    iput v6, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    sub-int/2addr v6, v4

    sub-int/2addr v6, v5

    invoke-virtual {v1, v0, v4, v6}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_0
    const/16 v7, 0x5c

    if-ne v2, v7, :cond_1

    iput v6, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    sub-int/2addr v6, v4

    sub-int/2addr v6, v5

    invoke-virtual {v1, v0, v4, v6}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    invoke-direct {p0}, Lcom/mi/google/gson/stream/JsonReader;->readEscapeCharacter()C

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    iget v3, p0, Lcom/mi/google/gson/stream/JsonReader;->limit:I

    move v4, v2

    goto :goto_1

    :cond_1
    const/16 v7, 0xa

    if-ne v2, v7, :cond_2

    iget v2, p0, Lcom/mi/google/gson/stream/JsonReader;->lineNumber:I

    add-int/2addr v2, v5

    iput v2, p0, Lcom/mi/google/gson/stream/JsonReader;->lineNumber:I

    iput v6, p0, Lcom/mi/google/gson/stream/JsonReader;->lineStart:I

    :cond_2
    move v2, v6

    goto :goto_1

    :cond_3
    sub-int v3, v2, v4

    invoke-virtual {v1, v0, v4, v3}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    iput v2, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    invoke-direct {p0, v5}, Lcom/mi/google/gson/stream/JsonReader;->fillBuffer(I)Z

    move-result v2

    if-eqz v2, :cond_4

    goto :goto_0

    :cond_4
    const-string p1, "Unterminated string"

    invoke-direct {p0, p1}, Lcom/mi/google/gson/stream/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object p1

    throw p1
.end method

.method private nextUnquotedValue()Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    const/4 v1, 0x0

    move-object v2, v1

    move v1, v0

    :goto_0
    iget v3, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    add-int v4, v3, v1

    iget v5, p0, Lcom/mi/google/gson/stream/JsonReader;->limit:I

    if-ge v4, v5, :cond_0

    iget-object v4, p0, Lcom/mi/google/gson/stream/JsonReader;->buffer:[C

    add-int/2addr v3, v1

    aget-char v3, v4, v3

    sparse-switch v3, :sswitch_data_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :sswitch_0
    invoke-direct {p0}, Lcom/mi/google/gson/stream/JsonReader;->checkLenient()V

    goto :goto_1

    :cond_0
    iget-object v3, p0, Lcom/mi/google/gson/stream/JsonReader;->buffer:[C

    array-length v3, v3

    if-ge v1, v3, :cond_2

    add-int/lit8 v3, v1, 0x1

    invoke-direct {p0, v3}, Lcom/mi/google/gson/stream/JsonReader;->fillBuffer(I)Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_0

    :cond_1
    :goto_1
    :sswitch_1
    move v0, v1

    goto :goto_2

    :cond_2
    if-nez v2, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    :cond_3
    iget-object v3, p0, Lcom/mi/google/gson/stream/JsonReader;->buffer:[C

    iget v4, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    invoke-virtual {v2, v3, v4, v1}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    iget v3, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    add-int/2addr v3, v1

    iput v3, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/mi/google/gson/stream/JsonReader;->fillBuffer(I)Z

    move-result v1

    if-nez v1, :cond_5

    :goto_2
    if-nez v2, :cond_4

    new-instance v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/mi/google/gson/stream/JsonReader;->buffer:[C

    iget v3, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    invoke-direct {v1, v2, v3, v0}, Ljava/lang/String;-><init>([CII)V

    goto :goto_3

    :cond_4
    iget-object v1, p0, Lcom/mi/google/gson/stream/JsonReader;->buffer:[C

    iget v3, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    invoke-virtual {v2, v1, v3, v0}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_3
    iget v2, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    add-int/2addr v2, v0

    iput v2, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    return-object v1

    :cond_5
    move v1, v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_1
        0xa -> :sswitch_1
        0xc -> :sswitch_1
        0xd -> :sswitch_1
        0x20 -> :sswitch_1
        0x23 -> :sswitch_0
        0x2c -> :sswitch_1
        0x2f -> :sswitch_0
        0x3a -> :sswitch_1
        0x3b -> :sswitch_0
        0x3d -> :sswitch_0
        0x5b -> :sswitch_1
        0x5c -> :sswitch_0
        0x5d -> :sswitch_1
        0x7b -> :sswitch_1
        0x7d -> :sswitch_1
    .end sparse-switch
.end method

.method private peekKeyword()I
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/mi/google/gson/stream/JsonReader;->buffer:[C

    iget v1, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    aget-char v0, v0, v1

    const/4 v1, 0x0

    const/16 v2, 0x74

    if-eq v0, v2, :cond_5

    const/16 v2, 0x54

    if-ne v0, v2, :cond_0

    goto :goto_2

    :cond_0
    const/16 v2, 0x66

    if-eq v0, v2, :cond_4

    const/16 v2, 0x46

    if-ne v0, v2, :cond_1

    goto :goto_1

    :cond_1
    const/16 v2, 0x6e

    if-eq v0, v2, :cond_3

    const/16 v2, 0x4e

    if-ne v0, v2, :cond_2

    goto :goto_0

    :cond_2
    return v1

    :cond_3
    :goto_0
    const-string v0, "null"

    const-string v2, "NULL"

    const/4 v3, 0x7

    goto :goto_3

    :cond_4
    :goto_1
    const-string v0, "false"

    const-string v2, "FALSE"

    const/4 v3, 0x6

    goto :goto_3

    :cond_5
    :goto_2
    const-string/jumbo v0, "true"

    const-string v2, "TRUE"

    const/4 v3, 0x5

    :goto_3
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    const/4 v5, 0x1

    :goto_4
    if-ge v5, v4, :cond_8

    iget v6, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    add-int/2addr v6, v5

    iget v7, p0, Lcom/mi/google/gson/stream/JsonReader;->limit:I

    if-lt v6, v7, :cond_6

    add-int/lit8 v6, v5, 0x1

    invoke-direct {p0, v6}, Lcom/mi/google/gson/stream/JsonReader;->fillBuffer(I)Z

    move-result v6

    if-nez v6, :cond_6

    return v1

    :cond_6
    iget-object v6, p0, Lcom/mi/google/gson/stream/JsonReader;->buffer:[C

    iget v7, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    add-int/2addr v7, v5

    aget-char v6, v6, v7

    invoke-virtual {v0, v5}, Ljava/lang/String;->charAt(I)C

    move-result v7

    if-eq v6, v7, :cond_7

    invoke-virtual {v2, v5}, Ljava/lang/String;->charAt(I)C

    move-result v7

    if-eq v6, v7, :cond_7

    return v1

    :cond_7
    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    :cond_8
    iget v0, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    add-int/2addr v0, v4

    iget v2, p0, Lcom/mi/google/gson/stream/JsonReader;->limit:I

    if-lt v0, v2, :cond_9

    add-int/lit8 v0, v4, 0x1

    invoke-direct {p0, v0}, Lcom/mi/google/gson/stream/JsonReader;->fillBuffer(I)Z

    move-result v0

    if-eqz v0, :cond_a

    :cond_9
    iget-object v0, p0, Lcom/mi/google/gson/stream/JsonReader;->buffer:[C

    iget v2, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    add-int/2addr v2, v4

    aget-char v0, v0, v2

    invoke-direct {p0, v0}, Lcom/mi/google/gson/stream/JsonReader;->isLiteral(C)Z

    move-result v0

    if-eqz v0, :cond_a

    return v1

    :cond_a
    iget v0, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    add-int/2addr v0, v4

    iput v0, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    iput v3, p0, Lcom/mi/google/gson/stream/JsonReader;->peeked:I

    return v3
.end method

.method private peekNumber()I
    .locals 18
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mi/google/gson/stream/JsonReader;->buffer:[C

    iget v2, v0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    iget v3, v0, Lcom/mi/google/gson/stream/JsonReader;->limit:I

    const/4 v6, 0x1

    const/4 v7, 0x0

    move v8, v3

    move v10, v6

    move v3, v7

    move v9, v3

    move v13, v9

    const-wide/16 v11, 0x0

    :goto_0
    add-int v14, v2, v3

    const/4 v15, 0x2

    if-ne v14, v8, :cond_2

    array-length v2, v1

    if-ne v3, v2, :cond_0

    return v7

    :cond_0
    add-int/lit8 v2, v3, 0x1

    invoke-direct {v0, v2}, Lcom/mi/google/gson/stream/JsonReader;->fillBuffer(I)Z

    move-result v2

    if-nez v2, :cond_1

    goto/16 :goto_6

    :cond_1
    iget v2, v0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    iget v8, v0, Lcom/mi/google/gson/stream/JsonReader;->limit:I

    :cond_2
    add-int v14, v2, v3

    aget-char v14, v1, v14

    const/16 v7, 0x2b

    const/4 v4, 0x3

    const/4 v5, 0x5

    if-eq v14, v7, :cond_1a

    const/16 v7, 0x45

    if-eq v14, v7, :cond_17

    const/16 v7, 0x65

    if-eq v14, v7, :cond_17

    packed-switch v14, :pswitch_data_0

    const/16 v7, 0x30

    if-lt v14, v7, :cond_d

    const/16 v7, 0x39

    if-le v14, v7, :cond_3

    goto :goto_5

    :cond_3
    if-eq v9, v6, :cond_c

    if-nez v9, :cond_4

    goto :goto_4

    :cond_4
    if-ne v9, v15, :cond_8

    const-wide/16 v16, 0x0

    cmp-long v4, v11, v16

    if-nez v4, :cond_5

    const/4 v4, 0x0

    return v4

    :cond_5
    const-wide/16 v4, 0xa

    mul-long/2addr v4, v11

    add-int/lit8 v14, v14, -0x30

    int-to-long v14, v14

    sub-long/2addr v4, v14

    const-wide v14, -0xcccccccccccccccL

    cmp-long v7, v11, v14

    if-gtz v7, :cond_7

    if-nez v7, :cond_6

    cmp-long v7, v4, v11

    if-gez v7, :cond_6

    goto :goto_1

    :cond_6
    const/4 v7, 0x0

    goto :goto_2

    :cond_7
    :goto_1
    move v7, v6

    :goto_2
    and-int/2addr v7, v10

    move-wide v11, v4

    move v10, v7

    const/4 v7, 0x0

    goto/16 :goto_a

    :cond_8
    const-wide/16 v16, 0x0

    if-ne v9, v4, :cond_9

    const/4 v7, 0x0

    const/4 v9, 0x4

    goto/16 :goto_a

    :cond_9
    if-eq v9, v5, :cond_b

    const/4 v4, 0x6

    if-ne v9, v4, :cond_a

    goto :goto_3

    :cond_a
    const/4 v7, 0x0

    goto/16 :goto_a

    :cond_b
    :goto_3
    const/4 v7, 0x0

    const/4 v9, 0x7

    goto/16 :goto_a

    :cond_c
    :goto_4
    const-wide/16 v16, 0x0

    add-int/lit8 v14, v14, -0x30

    neg-int v4, v14

    int-to-long v4, v4

    move-wide v11, v4

    move v9, v15

    const/4 v7, 0x0

    goto/16 :goto_a

    :cond_d
    :goto_5
    invoke-direct {v0, v14}, Lcom/mi/google/gson/stream/JsonReader;->isLiteral(C)Z

    move-result v1

    if-nez v1, :cond_13

    :goto_6
    if-ne v9, v15, :cond_10

    if-eqz v10, :cond_10

    const-wide/high16 v1, -0x8000000000000000L

    cmp-long v1, v11, v1

    if-nez v1, :cond_e

    if-eqz v13, :cond_10

    :cond_e
    if-eqz v13, :cond_f

    goto :goto_7

    :cond_f
    neg-long v11, v11

    :goto_7
    iput-wide v11, v0, Lcom/mi/google/gson/stream/JsonReader;->peekedLong:J

    iget v1, v0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    add-int/2addr v1, v3

    iput v1, v0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    const/16 v1, 0xf

    iput v1, v0, Lcom/mi/google/gson/stream/JsonReader;->peeked:I

    return v1

    :cond_10
    if-eq v9, v15, :cond_12

    const/4 v1, 0x4

    if-eq v9, v1, :cond_12

    const/4 v1, 0x7

    if-ne v9, v1, :cond_11

    goto :goto_8

    :cond_11
    const/4 v7, 0x0

    return v7

    :cond_12
    :goto_8
    iput v3, v0, Lcom/mi/google/gson/stream/JsonReader;->peekedNumberLength:I

    const/16 v1, 0x10

    iput v1, v0, Lcom/mi/google/gson/stream/JsonReader;->peeked:I

    return v1

    :cond_13
    const/4 v7, 0x0

    return v7

    :pswitch_0
    const/4 v7, 0x0

    const-wide/16 v16, 0x0

    if-ne v9, v15, :cond_14

    move v9, v4

    goto :goto_a

    :cond_14
    return v7

    :pswitch_1
    const/4 v4, 0x6

    const/4 v7, 0x0

    const-wide/16 v16, 0x0

    if-nez v9, :cond_15

    move v9, v6

    move v13, v9

    goto :goto_a

    :cond_15
    if-ne v9, v5, :cond_16

    move v9, v4

    goto :goto_a

    :cond_16
    return v7

    :cond_17
    const/4 v7, 0x0

    const-wide/16 v16, 0x0

    if-eq v9, v15, :cond_19

    const/4 v4, 0x4

    if-ne v9, v4, :cond_18

    goto :goto_9

    :cond_18
    return v7

    :cond_19
    :goto_9
    move v9, v5

    goto :goto_a

    :cond_1a
    const/4 v4, 0x6

    const/4 v7, 0x0

    const-wide/16 v16, 0x0

    if-ne v9, v5, :cond_1b

    move v9, v4

    :goto_a
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_1b
    return v7

    nop

    :pswitch_data_0
    .packed-switch 0x2d
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private push(I)V
    .locals 6

    iget v0, p0, Lcom/mi/google/gson/stream/JsonReader;->stackSize:I

    iget-object v1, p0, Lcom/mi/google/gson/stream/JsonReader;->stack:[I

    array-length v2, v1

    if-ne v0, v2, :cond_0

    mul-int/lit8 v2, v0, 0x2

    new-array v2, v2, [I

    mul-int/lit8 v3, v0, 0x2

    new-array v3, v3, [I

    mul-int/lit8 v4, v0, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {v1, v5, v2, v5, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v0, p0, Lcom/mi/google/gson/stream/JsonReader;->pathIndices:[I

    iget v1, p0, Lcom/mi/google/gson/stream/JsonReader;->stackSize:I

    invoke-static {v0, v5, v3, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v0, p0, Lcom/mi/google/gson/stream/JsonReader;->pathNames:[Ljava/lang/String;

    iget v1, p0, Lcom/mi/google/gson/stream/JsonReader;->stackSize:I

    invoke-static {v0, v5, v4, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v2, p0, Lcom/mi/google/gson/stream/JsonReader;->stack:[I

    iput-object v3, p0, Lcom/mi/google/gson/stream/JsonReader;->pathIndices:[I

    iput-object v4, p0, Lcom/mi/google/gson/stream/JsonReader;->pathNames:[Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lcom/mi/google/gson/stream/JsonReader;->stack:[I

    iget v1, p0, Lcom/mi/google/gson/stream/JsonReader;->stackSize:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/mi/google/gson/stream/JsonReader;->stackSize:I

    aput p1, v0, v1

    return-void
.end method

.method private readEscapeCharacter()C
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget v0, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    iget v1, p0, Lcom/mi/google/gson/stream/JsonReader;->limit:I

    const/4 v2, 0x1

    if-ne v0, v1, :cond_1

    invoke-direct {p0, v2}, Lcom/mi/google/gson/stream/JsonReader;->fillBuffer(I)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, "Unterminated escape sequence"

    invoke-direct {p0, v0}, Lcom/mi/google/gson/stream/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/mi/google/gson/stream/JsonReader;->buffer:[C

    iget v1, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    add-int/lit8 v3, v1, 0x1

    iput v3, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    aget-char v0, v0, v1

    const/16 v1, 0xa

    if-eq v0, v1, :cond_c

    const/16 v2, 0x22

    if-eq v0, v2, :cond_d

    const/16 v2, 0x27

    if-eq v0, v2, :cond_d

    const/16 v2, 0x2f

    if-eq v0, v2, :cond_d

    const/16 v2, 0x5c

    if-eq v0, v2, :cond_d

    const/16 v2, 0x62

    if-eq v0, v2, :cond_b

    const/16 v2, 0x66

    if-eq v0, v2, :cond_a

    const/16 v3, 0x6e

    if-eq v0, v3, :cond_9

    const/16 v3, 0x72

    if-eq v0, v3, :cond_8

    packed-switch v0, :pswitch_data_0

    const-string v0, "Invalid escape sequence"

    invoke-direct {p0, v0}, Lcom/mi/google/gson/stream/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :pswitch_0
    iget v0, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    const/4 v3, 0x4

    add-int/2addr v0, v3

    iget v4, p0, Lcom/mi/google/gson/stream/JsonReader;->limit:I

    if-le v0, v4, :cond_3

    invoke-direct {p0, v3}, Lcom/mi/google/gson/stream/JsonReader;->fillBuffer(I)Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_1

    :cond_2
    const-string v0, "Unterminated escape sequence"

    invoke-direct {p0, v0}, Lcom/mi/google/gson/stream/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_3
    :goto_1
    const/4 v0, 0x0

    iget v4, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    add-int/lit8 v5, v4, 0x4

    :goto_2
    if-ge v4, v5, :cond_7

    iget-object v6, p0, Lcom/mi/google/gson/stream/JsonReader;->buffer:[C

    aget-char v6, v6, v4

    shl-int/lit8 v0, v0, 0x4

    int-to-char v0, v0

    const/16 v7, 0x30

    if-lt v6, v7, :cond_4

    const/16 v7, 0x39

    if-gt v6, v7, :cond_4

    add-int/lit8 v6, v6, -0x30

    add-int/2addr v0, v6

    int-to-char v0, v0

    goto :goto_3

    :cond_4
    const/16 v7, 0x61

    if-lt v6, v7, :cond_5

    if-gt v6, v2, :cond_5

    add-int/lit8 v6, v6, -0x61

    add-int/2addr v6, v1

    add-int/2addr v0, v6

    int-to-char v0, v0

    goto :goto_3

    :cond_5
    const/16 v7, 0x41

    if-lt v6, v7, :cond_6

    const/16 v7, 0x46

    if-gt v6, v7, :cond_6

    add-int/lit8 v6, v6, -0x41

    add-int/2addr v6, v1

    add-int/2addr v0, v6

    int-to-char v0, v0

    :goto_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_6
    new-instance v0, Ljava/lang/NumberFormatException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\\u"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v2, Ljava/lang/String;

    iget-object v4, p0, Lcom/mi/google/gson/stream/JsonReader;->buffer:[C

    iget v5, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    invoke-direct {v2, v4, v5, v3}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_7
    iget v1, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    add-int/2addr v1, v3

    iput v1, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    return v0

    :pswitch_1
    const/16 v0, 0x9

    return v0

    :cond_8
    const/16 v0, 0xd

    return v0

    :cond_9
    return v1

    :cond_a
    const/16 v0, 0xc

    return v0

    :cond_b
    const/16 v0, 0x8

    return v0

    :cond_c
    iget v1, p0, Lcom/mi/google/gson/stream/JsonReader;->lineNumber:I

    add-int/2addr v1, v2

    iput v1, p0, Lcom/mi/google/gson/stream/JsonReader;->lineNumber:I

    iget v1, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    iput v1, p0, Lcom/mi/google/gson/stream/JsonReader;->lineStart:I

    :cond_d
    return v0

    nop

    :pswitch_data_0
    .packed-switch 0x74
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private skipQuotedValue(C)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/mi/google/gson/stream/JsonReader;->buffer:[C

    :goto_0
    iget v1, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    iget v2, p0, Lcom/mi/google/gson/stream/JsonReader;->limit:I

    :goto_1
    const/4 v3, 0x1

    if-ge v1, v2, :cond_3

    add-int/lit8 v4, v1, 0x1

    aget-char v1, v0, v1

    if-ne v1, p1, :cond_0

    iput v4, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    return-void

    :cond_0
    const/16 v5, 0x5c

    if-ne v1, v5, :cond_1

    iput v4, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    invoke-direct {p0}, Lcom/mi/google/gson/stream/JsonReader;->readEscapeCharacter()C

    iget v1, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    iget v2, p0, Lcom/mi/google/gson/stream/JsonReader;->limit:I

    goto :goto_1

    :cond_1
    const/16 v5, 0xa

    if-ne v1, v5, :cond_2

    iget v1, p0, Lcom/mi/google/gson/stream/JsonReader;->lineNumber:I

    add-int/2addr v1, v3

    iput v1, p0, Lcom/mi/google/gson/stream/JsonReader;->lineNumber:I

    iput v4, p0, Lcom/mi/google/gson/stream/JsonReader;->lineStart:I

    :cond_2
    move v1, v4

    goto :goto_1

    :cond_3
    iput v1, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    invoke-direct {p0, v3}, Lcom/mi/google/gson/stream/JsonReader;->fillBuffer(I)Z

    move-result v1

    if-eqz v1, :cond_4

    goto :goto_0

    :cond_4
    const-string p1, "Unterminated string"

    invoke-direct {p0, p1}, Lcom/mi/google/gson/stream/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object p1

    throw p1
.end method

.method private skipTo(Ljava/lang/String;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :goto_0
    iget v0, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v0, v1

    iget v1, p0, Lcom/mi/google/gson/stream/JsonReader;->limit:I

    const/4 v2, 0x0

    if-le v0, v1, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mi/google/gson/stream/JsonReader;->fillBuffer(I)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    :cond_0
    return v2

    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/mi/google/gson/stream/JsonReader;->buffer:[C

    iget v1, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    aget-char v0, v0, v1

    const/16 v3, 0xa

    const/4 v4, 0x1

    if-ne v0, v3, :cond_2

    iget v0, p0, Lcom/mi/google/gson/stream/JsonReader;->lineNumber:I

    add-int/2addr v0, v4

    iput v0, p0, Lcom/mi/google/gson/stream/JsonReader;->lineNumber:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/mi/google/gson/stream/JsonReader;->lineStart:I

    goto :goto_3

    :cond_2
    :goto_2
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-ge v2, v0, :cond_4

    iget-object v0, p0, Lcom/mi/google/gson/stream/JsonReader;->buffer:[C

    iget v1, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    add-int/2addr v1, v2

    aget-char v0, v0, v1

    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v1

    if-eq v0, v1, :cond_3

    :goto_3
    iget v0, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    add-int/2addr v0, v4

    iput v0, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    goto :goto_0

    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_4
    return v4
.end method

.method private skipToEndOfLine()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    iget v0, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    iget v1, p0, Lcom/mi/google/gson/stream/JsonReader;->limit:I

    const/4 v2, 0x1

    if-lt v0, v1, :cond_1

    invoke-direct {p0, v2}, Lcom/mi/google/gson/stream/JsonReader;->fillBuffer(I)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_1
    iget-object v0, p0, Lcom/mi/google/gson/stream/JsonReader;->buffer:[C

    iget v1, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    add-int/lit8 v3, v1, 0x1

    iput v3, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    aget-char v0, v0, v1

    const/16 v1, 0xa

    if-ne v0, v1, :cond_2

    iget v0, p0, Lcom/mi/google/gson/stream/JsonReader;->lineNumber:I

    add-int/2addr v0, v2

    iput v0, p0, Lcom/mi/google/gson/stream/JsonReader;->lineNumber:I

    iget v0, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    iput v0, p0, Lcom/mi/google/gson/stream/JsonReader;->lineStart:I

    goto :goto_0

    :cond_2
    const/16 v1, 0xd

    if-ne v0, v1, :cond_0

    :cond_3
    :goto_0
    return-void
.end method

.method private skipUnquotedValue()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    add-int v2, v1, v0

    iget v3, p0, Lcom/mi/google/gson/stream/JsonReader;->limit:I

    if-ge v2, v3, :cond_1

    iget-object v2, p0, Lcom/mi/google/gson/stream/JsonReader;->buffer:[C

    add-int/2addr v1, v0

    aget-char v1, v2, v1

    sparse-switch v1, :sswitch_data_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :sswitch_0
    invoke-direct {p0}, Lcom/mi/google/gson/stream/JsonReader;->checkLenient()V

    :sswitch_1
    iget v1, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    add-int/2addr v1, v0

    iput v1, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    return-void

    :cond_1
    add-int/2addr v1, v0

    iput v1, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/mi/google/gson/stream/JsonReader;->fillBuffer(I)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_1
        0xa -> :sswitch_1
        0xc -> :sswitch_1
        0xd -> :sswitch_1
        0x20 -> :sswitch_1
        0x23 -> :sswitch_0
        0x2c -> :sswitch_1
        0x2f -> :sswitch_0
        0x3a -> :sswitch_1
        0x3b -> :sswitch_0
        0x3d -> :sswitch_0
        0x5b -> :sswitch_1
        0x5c -> :sswitch_0
        0x5d -> :sswitch_1
        0x7b -> :sswitch_1
        0x7d -> :sswitch_1
    .end sparse-switch
.end method

.method private syntaxError(Ljava/lang/String;)Ljava/io/IOException;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/mi/google/gson/stream/MalformedJsonException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-direct {p0}, Lcom/mi/google/gson/stream/JsonReader;->locationString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/mi/google/gson/stream/MalformedJsonException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public beginArray()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget v0, p0, Lcom/mi/google/gson/stream/JsonReader;->peeked:I

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/mi/google/gson/stream/JsonReader;->doPeek()I

    move-result v0

    :cond_0
    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/mi/google/gson/stream/JsonReader;->push(I)V

    iget-object v1, p0, Lcom/mi/google/gson/stream/JsonReader;->pathIndices:[I

    iget v2, p0, Lcom/mi/google/gson/stream/JsonReader;->stackSize:I

    sub-int/2addr v2, v0

    const/4 v0, 0x0

    aput v0, v1, v2

    iput v0, p0, Lcom/mi/google/gson/stream/JsonReader;->peeked:I

    return-void

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected BEGIN_ARRAY but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/mi/google/gson/stream/JsonReader;->peek()Lcom/mi/google/gson/stream/JsonToken;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-direct {p0}, Lcom/mi/google/gson/stream/JsonReader;->locationString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public beginObject()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget v0, p0, Lcom/mi/google/gson/stream/JsonReader;->peeked:I

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/mi/google/gson/stream/JsonReader;->doPeek()I

    move-result v0

    :cond_0
    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/mi/google/gson/stream/JsonReader;->push(I)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/mi/google/gson/stream/JsonReader;->peeked:I

    return-void

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected BEGIN_OBJECT but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/mi/google/gson/stream/JsonReader;->peek()Lcom/mi/google/gson/stream/JsonToken;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-direct {p0}, Lcom/mi/google/gson/stream/JsonReader;->locationString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public close()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    iput v0, p0, Lcom/mi/google/gson/stream/JsonReader;->peeked:I

    iget-object v1, p0, Lcom/mi/google/gson/stream/JsonReader;->stack:[I

    const/16 v2, 0x8

    aput v2, v1, v0

    const/4 v0, 0x1

    iput v0, p0, Lcom/mi/google/gson/stream/JsonReader;->stackSize:I

    iget-object v0, p0, Lcom/mi/google/gson/stream/JsonReader;->in:Ljava/io/Reader;

    invoke-virtual {v0}, Ljava/io/Reader;->close()V

    return-void
.end method

.method doPeek()I
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    goto/32 :goto_c4

    nop

    :goto_0
    if-ne v0, v4, :cond_0

    goto/32 :goto_d8

    :cond_0
    goto/32 :goto_11

    nop

    :goto_1
    if-eq v2, v0, :cond_1

    goto/32 :goto_42

    :cond_1
    goto/32 :goto_3

    nop

    :goto_2
    const-string v0, "Expected value"

    goto/32 :goto_39

    nop

    :goto_3
    iget-boolean v0, p0, Lcom/mi/google/gson/stream/JsonReader;->lenient:Z

    goto/32 :goto_10

    nop

    :goto_4
    iput v1, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    goto/32 :goto_46

    nop

    :goto_5
    if-eq v0, v14, :cond_2

    goto/32 :goto_61

    :cond_2
    goto/32 :goto_27

    nop

    :goto_6
    return v0

    :goto_7
    goto/32 :goto_2

    nop

    :goto_8
    iput v0, p0, Lcom/mi/google/gson/stream/JsonReader;->peeked:I

    goto/32 :goto_6

    nop

    :goto_9
    if-ne v1, v10, :cond_3

    goto/32 :goto_ba

    :cond_3
    goto/32 :goto_c1

    nop

    :goto_a
    iget v0, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    goto/32 :goto_96

    nop

    :goto_b
    invoke-direct {p0}, Lcom/mi/google/gson/stream/JsonReader;->peekNumber()I

    move-result v0

    goto/32 :goto_83

    nop

    :goto_c
    return v0

    :goto_d
    goto/32 :goto_b7

    nop

    :goto_e
    if-ne v0, v1, :cond_4

    goto/32 :goto_cc

    :cond_4
    goto/32 :goto_a

    nop

    :goto_f
    iput v0, p0, Lcom/mi/google/gson/stream/JsonReader;->peeked:I

    goto/32 :goto_4f

    nop

    :goto_10
    if-nez v0, :cond_5

    goto/32 :goto_7b

    :cond_5
    goto/32 :goto_7a

    nop

    :goto_11
    if-ne v0, v10, :cond_6

    goto/32 :goto_1c

    :cond_6
    goto/32 :goto_a4

    nop

    :goto_12
    sub-int/2addr v0, v13

    goto/32 :goto_dd

    nop

    :goto_13
    if-ge v0, v1, :cond_7

    goto/32 :goto_21

    :cond_7
    goto/32 :goto_44

    nop

    :goto_14
    if-ne v0, v1, :cond_8

    goto/32 :goto_bf

    :cond_8
    goto/32 :goto_55

    nop

    :goto_15
    if-eq v0, v1, :cond_9

    goto/32 :goto_40

    :cond_9
    goto/32 :goto_18

    nop

    :goto_16
    aget v2, v0, v2

    goto/32 :goto_2f

    nop

    :goto_17
    iget v0, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    goto/32 :goto_98

    nop

    :goto_18
    const/16 v0, 0x11

    goto/32 :goto_b6

    nop

    :goto_19
    iget v0, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    goto/32 :goto_1e

    nop

    :goto_1a
    int-to-char v0, v1

    goto/32 :goto_b0

    nop

    :goto_1b
    return v7

    :goto_1c
    goto/32 :goto_99

    nop

    :goto_1d
    if-ne v0, v9, :cond_a

    goto/32 :goto_91

    :cond_a
    goto/32 :goto_57

    nop

    :goto_1e
    sub-int/2addr v0, v13

    goto/32 :goto_78

    nop

    :goto_1f
    if-ne v1, v0, :cond_b

    goto/32 :goto_6e

    :cond_b
    goto/32 :goto_5e

    nop

    :goto_20
    if-nez v0, :cond_c

    goto/32 :goto_61

    :cond_c
    :goto_21
    goto/32 :goto_8d

    nop

    :goto_22
    const/16 v6, 0x5d

    goto/32 :goto_ca

    nop

    :goto_23
    const/16 v0, 0x9

    goto/32 :goto_ab

    nop

    :goto_24
    if-ne v0, v5, :cond_d

    goto/32 :goto_75

    :cond_d
    goto/32 :goto_0

    nop

    :goto_25
    if-nez v0, :cond_e

    goto/32 :goto_50

    :cond_e
    goto/32 :goto_9c

    nop

    :goto_26
    iput v0, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    goto/32 :goto_87

    nop

    :goto_27
    add-int/2addr v1, v13

    goto/32 :goto_4

    nop

    :goto_28
    const/16 v1, 0x3d

    goto/32 :goto_a9

    nop

    :goto_29
    return v12

    :goto_2a
    goto/32 :goto_93

    nop

    :goto_2b
    invoke-direct {p0, v0}, Lcom/mi/google/gson/stream/JsonReader;->nextNonWhitespace(Z)I

    move-result v0

    goto/32 :goto_53

    nop

    :goto_2c
    invoke-direct {p0, v13}, Lcom/mi/google/gson/stream/JsonReader;->nextNonWhitespace(Z)I

    move-result v0

    goto/32 :goto_24

    nop

    :goto_2d
    const/16 v1, 0x3a

    goto/32 :goto_48

    nop

    :goto_2e
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_65

    nop

    :goto_2f
    const/16 v3, 0x8

    goto/32 :goto_30

    nop

    :goto_30
    const/16 v4, 0x27

    goto/32 :goto_a1

    nop

    :goto_31
    const-string v0, "Expected \':\'"

    goto/32 :goto_67

    nop

    :goto_32
    iput v7, p0, Lcom/mi/google/gson/stream/JsonReader;->peeked:I

    goto/32 :goto_1b

    nop

    :goto_33
    invoke-direct {p0}, Lcom/mi/google/gson/stream/JsonReader;->checkLenient()V

    goto/32 :goto_70

    nop

    :goto_34
    aget-char v0, v0, v1

    goto/32 :goto_3b

    nop

    :goto_35
    return v0

    :goto_36
    if-ne v1, v5, :cond_f

    goto/32 :goto_d

    :cond_f
    goto/32 :goto_69

    nop

    :goto_37
    goto/16 :goto_a0

    :goto_38
    goto/32 :goto_b5

    nop

    :goto_39
    invoke-direct {p0, v0}, Lcom/mi/google/gson/stream/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    goto/32 :goto_cb

    nop

    :goto_3a
    iget v1, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    goto/32 :goto_b4

    nop

    :goto_3b
    const/16 v14, 0x3e

    goto/32 :goto_5

    nop

    :goto_3c
    sub-int/2addr v1, v13

    goto/32 :goto_c2

    nop

    :goto_3d
    return v0

    :goto_3e
    goto/32 :goto_d4

    nop

    :goto_3f
    return v0

    :goto_40
    goto/32 :goto_33

    nop

    :goto_41
    goto/16 :goto_61

    :goto_42
    goto/32 :goto_c5

    nop

    :goto_43
    invoke-direct {p0}, Lcom/mi/google/gson/stream/JsonReader;->checkLenient()V

    goto/32 :goto_19

    nop

    :goto_44
    invoke-direct {p0, v13}, Lcom/mi/google/gson/stream/JsonReader;->fillBuffer(I)Z

    move-result v0

    goto/32 :goto_20

    nop

    :goto_45
    const/4 v0, 0x0

    goto/32 :goto_2b

    nop

    :goto_46
    goto/16 :goto_61

    :goto_47
    goto/32 :goto_31

    nop

    :goto_48
    if-ne v0, v1, :cond_10

    goto/32 :goto_61

    :cond_10
    goto/32 :goto_28

    nop

    :goto_49
    iput v8, p0, Lcom/mi/google/gson/stream/JsonReader;->peeked:I

    goto/32 :goto_d7

    nop

    :goto_4a
    invoke-direct {p0, v13}, Lcom/mi/google/gson/stream/JsonReader;->nextNonWhitespace(Z)I

    move-result v0

    goto/32 :goto_b2

    nop

    :goto_4b
    iget v1, p0, Lcom/mi/google/gson/stream/JsonReader;->stackSize:I

    goto/32 :goto_5d

    nop

    :goto_4c
    new-instance v0, Ljava/lang/IllegalStateException;

    goto/32 :goto_54

    nop

    :goto_4d
    if-nez v0, :cond_11

    goto/32 :goto_95

    :cond_11
    goto/32 :goto_94

    nop

    :goto_4e
    iget v1, p0, Lcom/mi/google/gson/stream/JsonReader;->limit:I

    goto/32 :goto_13

    nop

    :goto_4f
    return v0

    :goto_50
    goto/32 :goto_bb

    nop

    :goto_51
    const/4 v8, 0x7

    goto/32 :goto_85

    nop

    :goto_52
    iget v0, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    goto/32 :goto_4e

    nop

    :goto_53
    const/4 v1, -0x1

    goto/32 :goto_15

    nop

    :goto_54
    const-string v1, "JsonReader is closed"

    goto/32 :goto_2e

    nop

    :goto_55
    if-ne v0, v6, :cond_12

    goto/32 :goto_ad

    :cond_12
    goto/32 :goto_6c

    nop

    :goto_56
    sub-int/2addr v1, v13

    goto/32 :goto_bc

    nop

    :goto_57
    if-eq v0, v6, :cond_13

    goto/32 :goto_d1

    :cond_13
    goto/32 :goto_c9

    nop

    :goto_58
    iput v13, p0, Lcom/mi/google/gson/stream/JsonReader;->peeked:I

    goto/32 :goto_ac

    nop

    :goto_59
    invoke-direct {p0, v0}, Lcom/mi/google/gson/stream/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    goto/32 :goto_63

    nop

    :goto_5a
    const/4 v14, 0x5

    goto/32 :goto_9b

    nop

    :goto_5b
    return v0

    :goto_5c
    goto/32 :goto_4c

    nop

    :goto_5d
    add-int/lit8 v2, v1, -0x1

    goto/32 :goto_16

    nop

    :goto_5e
    invoke-direct {p0}, Lcom/mi/google/gson/stream/JsonReader;->checkLenient()V

    goto/32 :goto_17

    nop

    :goto_5f
    sub-int/2addr v1, v13

    goto/32 :goto_82

    nop

    :goto_60
    if-ne v2, v3, :cond_14

    goto/32 :goto_5c

    :cond_14
    :goto_61
    goto/32 :goto_2c

    nop

    :goto_62
    if-eq v2, v13, :cond_15

    goto/32 :goto_89

    :cond_15
    goto/32 :goto_5f

    nop

    :goto_63
    throw v0

    :goto_64
    goto/32 :goto_b9

    nop

    :goto_65
    throw v0

    :goto_66
    goto/32 :goto_db

    nop

    :goto_67
    invoke-direct {p0, v0}, Lcom/mi/google/gson/stream/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    goto/32 :goto_7e

    nop

    :goto_68
    invoke-direct {p0}, Lcom/mi/google/gson/stream/JsonReader;->checkLenient()V

    goto/32 :goto_6a

    nop

    :goto_69
    if-ne v1, v4, :cond_16

    goto/32 :goto_a3

    :cond_16
    goto/32 :goto_1f

    nop

    :goto_6a
    iput v3, p0, Lcom/mi/google/gson/stream/JsonReader;->peeked:I

    goto/32 :goto_74

    nop

    :goto_6b
    invoke-direct {p0}, Lcom/mi/google/gson/stream/JsonReader;->checkLenient()V

    goto/32 :goto_52

    nop

    :goto_6c
    const/16 v1, 0x7b

    goto/32 :goto_e

    nop

    :goto_6d
    throw v0

    :goto_6e
    goto/32 :goto_cf

    nop

    :goto_6f
    iget-object v0, p0, Lcom/mi/google/gson/stream/JsonReader;->stack:[I

    goto/32 :goto_76

    nop

    :goto_70
    iget v0, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    goto/32 :goto_12

    nop

    :goto_71
    iget v1, p0, Lcom/mi/google/gson/stream/JsonReader;->stackSize:I

    goto/32 :goto_7d

    nop

    :goto_72
    invoke-direct {p0, v13}, Lcom/mi/google/gson/stream/JsonReader;->nextNonWhitespace(Z)I

    move-result v1

    goto/32 :goto_9

    nop

    :goto_73
    invoke-direct {p0, v13}, Lcom/mi/google/gson/stream/JsonReader;->nextNonWhitespace(Z)I

    move-result v1

    goto/32 :goto_36

    nop

    :goto_74
    return v3

    :goto_75
    goto/32 :goto_23

    nop

    :goto_76
    iget v1, p0, Lcom/mi/google/gson/stream/JsonReader;->stackSize:I

    goto/32 :goto_56

    nop

    :goto_77
    iput v0, p0, Lcom/mi/google/gson/stream/JsonReader;->peeked:I

    goto/32 :goto_c

    nop

    :goto_78
    iput v0, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    goto/32 :goto_49

    nop

    :goto_79
    const/16 v10, 0x2c

    goto/32 :goto_dc

    nop

    :goto_7a
    invoke-direct {p0}, Lcom/mi/google/gson/stream/JsonReader;->consumeNonExecutePrefix()V

    :goto_7b
    goto/32 :goto_6f

    nop

    :goto_7c
    if-eq v2, v12, :cond_17

    goto/32 :goto_a8

    :cond_17
    goto/32 :goto_4a

    nop

    :goto_7d
    sub-int/2addr v1, v13

    goto/32 :goto_a5

    nop

    :goto_7e
    throw v0

    :goto_7f
    goto/32 :goto_a6

    nop

    :goto_80
    iput v0, p0, Lcom/mi/google/gson/stream/JsonReader;->peeked:I

    goto/32 :goto_35

    nop

    :goto_81
    invoke-direct {p0, v0}, Lcom/mi/google/gson/stream/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    goto/32 :goto_90

    nop

    :goto_82
    aput v12, v0, v1

    goto/32 :goto_88

    nop

    :goto_83
    if-nez v0, :cond_18

    goto/32 :goto_3e

    :cond_18
    goto/32 :goto_3d

    nop

    :goto_84
    const/4 v13, 0x1

    goto/32 :goto_62

    nop

    :goto_85
    const/16 v9, 0x3b

    goto/32 :goto_79

    nop

    :goto_86
    invoke-direct {p0, v0}, Lcom/mi/google/gson/stream/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    goto/32 :goto_a2

    nop

    :goto_87
    invoke-direct {p0}, Lcom/mi/google/gson/stream/JsonReader;->peekKeyword()I

    move-result v0

    goto/32 :goto_4d

    nop

    :goto_88
    goto/16 :goto_61

    :goto_89
    goto/32 :goto_7c

    nop

    :goto_8a
    const-string v0, "Expected name"

    goto/32 :goto_86

    nop

    :goto_8b
    iget v1, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    goto/32 :goto_34

    nop

    :goto_8c
    iput v11, p0, Lcom/mi/google/gson/stream/JsonReader;->peeked:I

    goto/32 :goto_be

    nop

    :goto_8d
    iget-object v0, p0, Lcom/mi/google/gson/stream/JsonReader;->buffer:[C

    goto/32 :goto_8b

    nop

    :goto_8e
    goto/16 :goto_61

    :goto_8f
    goto/32 :goto_60

    nop

    :goto_90
    throw v0

    :goto_91
    goto/32 :goto_c3

    nop

    :goto_92
    if-eq v2, v13, :cond_19

    goto/32 :goto_1c

    :cond_19
    goto/32 :goto_8c

    nop

    :goto_93
    const-string v0, "Unterminated object"

    goto/32 :goto_59

    nop

    :goto_94
    return v0

    :goto_95
    goto/32 :goto_b

    nop

    :goto_96
    sub-int/2addr v0, v13

    goto/32 :goto_26

    nop

    :goto_97
    const/16 v0, 0xa

    goto/32 :goto_8

    nop

    :goto_98
    sub-int/2addr v0, v13

    goto/32 :goto_c8

    nop

    :goto_99
    if-ne v2, v13, :cond_1a

    goto/32 :goto_a0

    :cond_1a
    goto/32 :goto_c6

    nop

    :goto_9a
    if-eq v2, v14, :cond_1b

    goto/32 :goto_ce

    :cond_1b
    goto/32 :goto_cd

    nop

    :goto_9b
    if-ne v2, v7, :cond_1c

    goto/32 :goto_66

    :cond_1c
    goto/32 :goto_9a

    nop

    :goto_9c
    const/16 v0, 0xe

    goto/32 :goto_f

    nop

    :goto_9d
    return v12

    :goto_9e
    goto/32 :goto_8a

    nop

    :goto_9f
    throw v0

    :goto_a0
    goto/32 :goto_43

    nop

    :goto_a1
    const/16 v5, 0x22

    goto/32 :goto_22

    nop

    :goto_a2
    throw v0

    :goto_a3
    goto/32 :goto_b8

    nop

    :goto_a4
    if-ne v0, v9, :cond_1d

    goto/32 :goto_1c

    :cond_1d
    goto/32 :goto_d2

    nop

    :goto_a5
    aput v11, v0, v1

    goto/32 :goto_aa

    nop

    :goto_a6
    const/4 v0, 0x6

    goto/32 :goto_1

    nop

    :goto_a7
    goto/16 :goto_61

    :goto_a8
    goto/32 :goto_5a

    nop

    :goto_a9
    if-eq v0, v1, :cond_1e

    goto/32 :goto_47

    :cond_1e
    goto/32 :goto_6b

    nop

    :goto_aa
    const/16 v0, 0x7d

    goto/32 :goto_b1

    nop

    :goto_ab
    iput v0, p0, Lcom/mi/google/gson/stream/JsonReader;->peeked:I

    goto/32 :goto_5b

    nop

    :goto_ac
    return v13

    :goto_ad
    goto/32 :goto_92

    nop

    :goto_ae
    iput v12, p0, Lcom/mi/google/gson/stream/JsonReader;->peeked:I

    goto/32 :goto_29

    nop

    :goto_af
    const/16 v0, 0xc

    goto/32 :goto_77

    nop

    :goto_b0
    invoke-direct {p0, v0}, Lcom/mi/google/gson/stream/JsonReader;->isLiteral(C)Z

    move-result v0

    goto/32 :goto_25

    nop

    :goto_b1
    if-eq v2, v14, :cond_1f

    goto/32 :goto_ba

    :cond_1f
    goto/32 :goto_72

    nop

    :goto_b2
    if-ne v0, v10, :cond_20

    goto/32 :goto_61

    :cond_20
    goto/32 :goto_1d

    nop

    :goto_b3
    invoke-direct {p0, v0}, Lcom/mi/google/gson/stream/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    goto/32 :goto_9f

    nop

    :goto_b4
    aget-char v0, v0, v1

    goto/32 :goto_bd

    nop

    :goto_b5
    const-string v0, "Unexpected value"

    goto/32 :goto_b3

    nop

    :goto_b6
    iput v0, p0, Lcom/mi/google/gson/stream/JsonReader;->peeked:I

    goto/32 :goto_3f

    nop

    :goto_b7
    const/16 v0, 0xd

    goto/32 :goto_80

    nop

    :goto_b8
    invoke-direct {p0}, Lcom/mi/google/gson/stream/JsonReader;->checkLenient()V

    goto/32 :goto_af

    nop

    :goto_b9
    invoke-direct {p0}, Lcom/mi/google/gson/stream/JsonReader;->checkLenient()V

    :goto_ba
    goto/32 :goto_73

    nop

    :goto_bb
    const-string v0, "Expected name"

    goto/32 :goto_d6

    nop

    :goto_bc
    aput v8, v0, v1

    goto/32 :goto_41

    nop

    :goto_bd
    invoke-direct {p0, v0}, Lcom/mi/google/gson/stream/JsonReader;->isLiteral(C)Z

    move-result v0

    goto/32 :goto_de

    nop

    :goto_be
    return v11

    :goto_bf
    goto/32 :goto_32

    nop

    :goto_c0
    invoke-direct {p0}, Lcom/mi/google/gson/stream/JsonReader;->checkLenient()V

    goto/32 :goto_97

    nop

    :goto_c1
    if-ne v1, v9, :cond_21

    goto/32 :goto_64

    :cond_21
    goto/32 :goto_d9

    nop

    :goto_c2
    aput v14, v0, v1

    goto/32 :goto_c7

    nop

    :goto_c3
    invoke-direct {p0}, Lcom/mi/google/gson/stream/JsonReader;->checkLenient()V

    goto/32 :goto_a7

    nop

    :goto_c4
    iget-object v0, p0, Lcom/mi/google/gson/stream/JsonReader;->stack:[I

    goto/32 :goto_4b

    nop

    :goto_c5
    if-eq v2, v8, :cond_22

    goto/32 :goto_8f

    :cond_22
    goto/32 :goto_45

    nop

    :goto_c6
    if-eq v2, v12, :cond_23

    goto/32 :goto_38

    :cond_23
    goto/32 :goto_37

    nop

    :goto_c7
    invoke-direct {p0, v13}, Lcom/mi/google/gson/stream/JsonReader;->nextNonWhitespace(Z)I

    move-result v0

    goto/32 :goto_2d

    nop

    :goto_c8
    iput v0, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    goto/32 :goto_1a

    nop

    :goto_c9
    iput v11, p0, Lcom/mi/google/gson/stream/JsonReader;->peeked:I

    goto/32 :goto_d0

    nop

    :goto_ca
    const/4 v7, 0x3

    goto/32 :goto_51

    nop

    :goto_cb
    throw v0

    :goto_cc
    goto/32 :goto_58

    nop

    :goto_cd
    goto/16 :goto_66

    :goto_ce
    goto/32 :goto_da

    nop

    :goto_cf
    if-ne v2, v14, :cond_24

    goto/32 :goto_9e

    :cond_24
    goto/32 :goto_d3

    nop

    :goto_d0
    return v11

    :goto_d1
    goto/32 :goto_df

    nop

    :goto_d2
    const/16 v1, 0x5b

    goto/32 :goto_14

    nop

    :goto_d3
    iput v12, p0, Lcom/mi/google/gson/stream/JsonReader;->peeked:I

    goto/32 :goto_9d

    nop

    :goto_d4
    iget-object v0, p0, Lcom/mi/google/gson/stream/JsonReader;->buffer:[C

    goto/32 :goto_3a

    nop

    :goto_d5
    const/4 v12, 0x2

    goto/32 :goto_84

    nop

    :goto_d6
    invoke-direct {p0, v0}, Lcom/mi/google/gson/stream/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    goto/32 :goto_6d

    nop

    :goto_d7
    return v8

    :goto_d8
    goto/32 :goto_68

    nop

    :goto_d9
    if-eq v1, v0, :cond_25

    goto/32 :goto_2a

    :cond_25
    goto/32 :goto_ae

    nop

    :goto_da
    if-eq v2, v11, :cond_26

    goto/32 :goto_7f

    :cond_26
    goto/32 :goto_3c

    nop

    :goto_db
    iget-object v0, p0, Lcom/mi/google/gson/stream/JsonReader;->stack:[I

    goto/32 :goto_71

    nop

    :goto_dc
    const/4 v11, 0x4

    goto/32 :goto_d5

    nop

    :goto_dd
    iput v0, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    goto/32 :goto_8e

    nop

    :goto_de
    if-nez v0, :cond_27

    goto/32 :goto_7

    :cond_27
    goto/32 :goto_c0

    nop

    :goto_df
    const-string v0, "Unterminated array"

    goto/32 :goto_81

    nop
.end method

.method public endArray()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget v0, p0, Lcom/mi/google/gson/stream/JsonReader;->peeked:I

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/mi/google/gson/stream/JsonReader;->doPeek()I

    move-result v0

    :cond_0
    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/mi/google/gson/stream/JsonReader;->stackSize:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/mi/google/gson/stream/JsonReader;->stackSize:I

    iget-object v0, p0, Lcom/mi/google/gson/stream/JsonReader;->pathIndices:[I

    iget v1, p0, Lcom/mi/google/gson/stream/JsonReader;->stackSize:I

    add-int/lit8 v1, v1, -0x1

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    const/4 v0, 0x0

    iput v0, p0, Lcom/mi/google/gson/stream/JsonReader;->peeked:I

    return-void

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected END_ARRAY but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/mi/google/gson/stream/JsonReader;->peek()Lcom/mi/google/gson/stream/JsonToken;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-direct {p0}, Lcom/mi/google/gson/stream/JsonReader;->locationString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public endObject()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget v0, p0, Lcom/mi/google/gson/stream/JsonReader;->peeked:I

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/mi/google/gson/stream/JsonReader;->doPeek()I

    move-result v0

    :cond_0
    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/mi/google/gson/stream/JsonReader;->stackSize:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/mi/google/gson/stream/JsonReader;->stackSize:I

    iget-object v0, p0, Lcom/mi/google/gson/stream/JsonReader;->pathNames:[Ljava/lang/String;

    iget v1, p0, Lcom/mi/google/gson/stream/JsonReader;->stackSize:I

    const/4 v2, 0x0

    aput-object v2, v0, v1

    iget-object v0, p0, Lcom/mi/google/gson/stream/JsonReader;->pathIndices:[I

    add-int/lit8 v1, v1, -0x1

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    const/4 v0, 0x0

    iput v0, p0, Lcom/mi/google/gson/stream/JsonReader;->peeked:I

    return-void

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected END_OBJECT but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/mi/google/gson/stream/JsonReader;->peek()Lcom/mi/google/gson/stream/JsonToken;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-direct {p0}, Lcom/mi/google/gson/stream/JsonReader;->locationString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 5

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/mi/google/gson/stream/JsonReader;->stackSize:I

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    iget-object v3, p0, Lcom/mi/google/gson/stream/JsonReader;->stack:[I

    aget v3, v3, v2

    packed-switch v3, :pswitch_data_0

    goto :goto_1

    :pswitch_0
    const/16 v3, 0x2e

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/mi/google/gson/stream/JsonReader;->pathNames:[Ljava/lang/String;

    aget-object v4, v3, v2

    if-eqz v4, :cond_0

    aget-object v3, v3, v2

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :pswitch_1
    const/16 v3, 0x5b

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/mi/google/gson/stream/JsonReader;->pathIndices:[I

    aget v3, v3, v2

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v3, 0x5d

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_0
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public hasNext()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget v0, p0, Lcom/mi/google/gson/stream/JsonReader;->peeked:I

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/mi/google/gson/stream/JsonReader;->doPeek()I

    move-result v0

    :cond_0
    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final isLenient()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mi/google/gson/stream/JsonReader;->lenient:Z

    return v0
.end method

.method public nextBoolean()Z
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget v0, p0, Lcom/mi/google/gson/stream/JsonReader;->peeked:I

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/mi/google/gson/stream/JsonReader;->doPeek()I

    move-result v0

    :cond_0
    const/4 v1, 0x5

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-ne v0, v1, :cond_1

    iput v2, p0, Lcom/mi/google/gson/stream/JsonReader;->peeked:I

    iget-object v0, p0, Lcom/mi/google/gson/stream/JsonReader;->pathIndices:[I

    iget v1, p0, Lcom/mi/google/gson/stream/JsonReader;->stackSize:I

    sub-int/2addr v1, v3

    aget v2, v0, v1

    add-int/2addr v2, v3

    aput v2, v0, v1

    return v3

    :cond_1
    const/4 v1, 0x6

    if-ne v0, v1, :cond_2

    iput v2, p0, Lcom/mi/google/gson/stream/JsonReader;->peeked:I

    iget-object v0, p0, Lcom/mi/google/gson/stream/JsonReader;->pathIndices:[I

    iget v1, p0, Lcom/mi/google/gson/stream/JsonReader;->stackSize:I

    sub-int/2addr v1, v3

    aget v4, v0, v1

    add-int/2addr v4, v3

    aput v4, v0, v1

    return v2

    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected a boolean but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/mi/google/gson/stream/JsonReader;->peek()Lcom/mi/google/gson/stream/JsonToken;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-direct {p0}, Lcom/mi/google/gson/stream/JsonReader;->locationString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public nextDouble()D
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget v0, p0, Lcom/mi/google/gson/stream/JsonReader;->peeked:I

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/mi/google/gson/stream/JsonReader;->doPeek()I

    move-result v0

    :cond_0
    const/16 v1, 0xf

    const/4 v2, 0x0

    if-ne v0, v1, :cond_1

    iput v2, p0, Lcom/mi/google/gson/stream/JsonReader;->peeked:I

    iget-object v0, p0, Lcom/mi/google/gson/stream/JsonReader;->pathIndices:[I

    iget v1, p0, Lcom/mi/google/gson/stream/JsonReader;->stackSize:I

    add-int/lit8 v1, v1, -0x1

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    iget-wide v0, p0, Lcom/mi/google/gson/stream/JsonReader;->peekedLong:J

    long-to-double v0, v0

    return-wide v0

    :cond_1
    const/16 v1, 0x10

    const/16 v3, 0xb

    if-ne v0, v1, :cond_2

    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/mi/google/gson/stream/JsonReader;->buffer:[C

    iget v4, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    iget v5, p0, Lcom/mi/google/gson/stream/JsonReader;->peekedNumberLength:I

    invoke-direct {v0, v1, v4, v5}, Ljava/lang/String;-><init>([CII)V

    iput-object v0, p0, Lcom/mi/google/gson/stream/JsonReader;->peekedString:Ljava/lang/String;

    iget v0, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    iget v1, p0, Lcom/mi/google/gson/stream/JsonReader;->peekedNumberLength:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    goto :goto_2

    :cond_2
    const/16 v1, 0x8

    if-eq v0, v1, :cond_6

    const/16 v4, 0x9

    if-ne v0, v4, :cond_3

    goto :goto_0

    :cond_3
    const/16 v1, 0xa

    if-ne v0, v1, :cond_4

    invoke-direct {p0}, Lcom/mi/google/gson/stream/JsonReader;->nextUnquotedValue()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mi/google/gson/stream/JsonReader;->peekedString:Ljava/lang/String;

    goto :goto_2

    :cond_4
    if-ne v0, v3, :cond_5

    goto :goto_2

    :cond_5
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected a double but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/mi/google/gson/stream/JsonReader;->peek()Lcom/mi/google/gson/stream/JsonToken;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-direct {p0}, Lcom/mi/google/gson/stream/JsonReader;->locationString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    :goto_0
    if-ne v0, v1, :cond_7

    const/16 v0, 0x27

    goto :goto_1

    :cond_7
    const/16 v0, 0x22

    :goto_1
    invoke-direct {p0, v0}, Lcom/mi/google/gson/stream/JsonReader;->nextQuotedValue(C)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mi/google/gson/stream/JsonReader;->peekedString:Ljava/lang/String;

    :goto_2
    iput v3, p0, Lcom/mi/google/gson/stream/JsonReader;->peeked:I

    iget-object v0, p0, Lcom/mi/google/gson/stream/JsonReader;->peekedString:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    iget-boolean v3, p0, Lcom/mi/google/gson/stream/JsonReader;->lenient:Z

    if-nez v3, :cond_9

    invoke-static {v0, v1}, Ljava/lang/Double;->isNaN(D)Z

    move-result v3

    if-nez v3, :cond_8

    invoke-static {v0, v1}, Ljava/lang/Double;->isInfinite(D)Z

    move-result v3

    if-nez v3, :cond_8

    goto :goto_3

    :cond_8
    new-instance v2, Lcom/mi/google/gson/stream/MalformedJsonException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "JSON forbids NaN and infinities: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-direct {p0}, Lcom/mi/google/gson/stream/JsonReader;->locationString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/mi/google/gson/stream/MalformedJsonException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_9
    :goto_3
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/mi/google/gson/stream/JsonReader;->peekedString:Ljava/lang/String;

    iput v2, p0, Lcom/mi/google/gson/stream/JsonReader;->peeked:I

    iget-object v2, p0, Lcom/mi/google/gson/stream/JsonReader;->pathIndices:[I

    iget v3, p0, Lcom/mi/google/gson/stream/JsonReader;->stackSize:I

    add-int/lit8 v3, v3, -0x1

    aget v4, v2, v3

    add-int/lit8 v4, v4, 0x1

    aput v4, v2, v3

    return-wide v0
.end method

.method public nextInt()I
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget v0, p0, Lcom/mi/google/gson/stream/JsonReader;->peeked:I

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/mi/google/gson/stream/JsonReader;->doPeek()I

    move-result v0

    :cond_0
    const/16 v1, 0xf

    const/4 v2, 0x0

    if-ne v0, v1, :cond_2

    iget-wide v0, p0, Lcom/mi/google/gson/stream/JsonReader;->peekedLong:J

    long-to-int v3, v0

    int-to-long v4, v3

    cmp-long v0, v0, v4

    if-nez v0, :cond_1

    iput v2, p0, Lcom/mi/google/gson/stream/JsonReader;->peeked:I

    iget-object v0, p0, Lcom/mi/google/gson/stream/JsonReader;->pathIndices:[I

    iget v1, p0, Lcom/mi/google/gson/stream/JsonReader;->stackSize:I

    add-int/lit8 v1, v1, -0x1

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    return v3

    :cond_1
    new-instance v0, Ljava/lang/NumberFormatException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected an int but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, p0, Lcom/mi/google/gson/stream/JsonReader;->peekedLong:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-direct {p0}, Lcom/mi/google/gson/stream/JsonReader;->locationString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    const/16 v1, 0x10

    if-ne v0, v1, :cond_3

    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/mi/google/gson/stream/JsonReader;->buffer:[C

    iget v3, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    iget v4, p0, Lcom/mi/google/gson/stream/JsonReader;->peekedNumberLength:I

    invoke-direct {v0, v1, v3, v4}, Ljava/lang/String;-><init>([CII)V

    iput-object v0, p0, Lcom/mi/google/gson/stream/JsonReader;->peekedString:Ljava/lang/String;

    iget v0, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    iget v1, p0, Lcom/mi/google/gson/stream/JsonReader;->peekedNumberLength:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    goto :goto_3

    :cond_3
    const/16 v1, 0xa

    const/16 v3, 0x8

    if-eq v0, v3, :cond_5

    const/16 v4, 0x9

    if-eq v0, v4, :cond_5

    if-ne v0, v1, :cond_4

    goto :goto_0

    :cond_4
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected an int but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/mi/google/gson/stream/JsonReader;->peek()Lcom/mi/google/gson/stream/JsonToken;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-direct {p0}, Lcom/mi/google/gson/stream/JsonReader;->locationString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    :goto_0
    if-ne v0, v1, :cond_6

    invoke-direct {p0}, Lcom/mi/google/gson/stream/JsonReader;->nextUnquotedValue()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mi/google/gson/stream/JsonReader;->peekedString:Ljava/lang/String;

    goto :goto_2

    :cond_6
    if-ne v0, v3, :cond_7

    const/16 v0, 0x27

    goto :goto_1

    :cond_7
    const/16 v0, 0x22

    :goto_1
    invoke-direct {p0, v0}, Lcom/mi/google/gson/stream/JsonReader;->nextQuotedValue(C)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mi/google/gson/stream/JsonReader;->peekedString:Ljava/lang/String;

    :goto_2
    :try_start_0
    iget-object v0, p0, Lcom/mi/google/gson/stream/JsonReader;->peekedString:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v2, p0, Lcom/mi/google/gson/stream/JsonReader;->peeked:I

    iget-object v1, p0, Lcom/mi/google/gson/stream/JsonReader;->pathIndices:[I

    iget v3, p0, Lcom/mi/google/gson/stream/JsonReader;->stackSize:I

    add-int/lit8 v3, v3, -0x1

    aget v4, v1, v3

    add-int/lit8 v4, v4, 0x1

    aput v4, v1, v3
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    :catch_0
    :goto_3
    const/16 v0, 0xb

    iput v0, p0, Lcom/mi/google/gson/stream/JsonReader;->peeked:I

    iget-object v0, p0, Lcom/mi/google/gson/stream/JsonReader;->peekedString:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    double-to-int v3, v0

    int-to-double v4, v3

    cmpl-double v0, v4, v0

    if-nez v0, :cond_8

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mi/google/gson/stream/JsonReader;->peekedString:Ljava/lang/String;

    iput v2, p0, Lcom/mi/google/gson/stream/JsonReader;->peeked:I

    iget-object v0, p0, Lcom/mi/google/gson/stream/JsonReader;->pathIndices:[I

    iget v1, p0, Lcom/mi/google/gson/stream/JsonReader;->stackSize:I

    add-int/lit8 v1, v1, -0x1

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    return v3

    :cond_8
    new-instance v0, Ljava/lang/NumberFormatException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected an int but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/mi/google/gson/stream/JsonReader;->peekedString:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-direct {p0}, Lcom/mi/google/gson/stream/JsonReader;->locationString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public nextLong()J
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget v0, p0, Lcom/mi/google/gson/stream/JsonReader;->peeked:I

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/mi/google/gson/stream/JsonReader;->doPeek()I

    move-result v0

    :cond_0
    const/16 v1, 0xf

    const/4 v2, 0x0

    if-ne v0, v1, :cond_1

    iput v2, p0, Lcom/mi/google/gson/stream/JsonReader;->peeked:I

    iget-object v0, p0, Lcom/mi/google/gson/stream/JsonReader;->pathIndices:[I

    iget v1, p0, Lcom/mi/google/gson/stream/JsonReader;->stackSize:I

    add-int/lit8 v1, v1, -0x1

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    iget-wide v0, p0, Lcom/mi/google/gson/stream/JsonReader;->peekedLong:J

    return-wide v0

    :cond_1
    const/16 v1, 0x10

    if-ne v0, v1, :cond_2

    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/mi/google/gson/stream/JsonReader;->buffer:[C

    iget v3, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    iget v4, p0, Lcom/mi/google/gson/stream/JsonReader;->peekedNumberLength:I

    invoke-direct {v0, v1, v3, v4}, Ljava/lang/String;-><init>([CII)V

    iput-object v0, p0, Lcom/mi/google/gson/stream/JsonReader;->peekedString:Ljava/lang/String;

    iget v0, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    iget v1, p0, Lcom/mi/google/gson/stream/JsonReader;->peekedNumberLength:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    goto :goto_3

    :cond_2
    const/16 v1, 0xa

    const/16 v3, 0x8

    if-eq v0, v3, :cond_4

    const/16 v4, 0x9

    if-eq v0, v4, :cond_4

    if-ne v0, v1, :cond_3

    goto :goto_0

    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected a long but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/mi/google/gson/stream/JsonReader;->peek()Lcom/mi/google/gson/stream/JsonToken;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-direct {p0}, Lcom/mi/google/gson/stream/JsonReader;->locationString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    :goto_0
    if-ne v0, v1, :cond_5

    invoke-direct {p0}, Lcom/mi/google/gson/stream/JsonReader;->nextUnquotedValue()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mi/google/gson/stream/JsonReader;->peekedString:Ljava/lang/String;

    goto :goto_2

    :cond_5
    if-ne v0, v3, :cond_6

    const/16 v0, 0x27

    goto :goto_1

    :cond_6
    const/16 v0, 0x22

    :goto_1
    invoke-direct {p0, v0}, Lcom/mi/google/gson/stream/JsonReader;->nextQuotedValue(C)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mi/google/gson/stream/JsonReader;->peekedString:Ljava/lang/String;

    :goto_2
    :try_start_0
    iget-object v0, p0, Lcom/mi/google/gson/stream/JsonReader;->peekedString:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iput v2, p0, Lcom/mi/google/gson/stream/JsonReader;->peeked:I

    iget-object v3, p0, Lcom/mi/google/gson/stream/JsonReader;->pathIndices:[I

    iget v4, p0, Lcom/mi/google/gson/stream/JsonReader;->stackSize:I

    add-int/lit8 v4, v4, -0x1

    aget v5, v3, v4

    add-int/lit8 v5, v5, 0x1

    aput v5, v3, v4
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    return-wide v0

    :catch_0
    :goto_3
    const/16 v0, 0xb

    iput v0, p0, Lcom/mi/google/gson/stream/JsonReader;->peeked:I

    iget-object v0, p0, Lcom/mi/google/gson/stream/JsonReader;->peekedString:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    double-to-long v3, v0

    long-to-double v5, v3

    cmpl-double v0, v5, v0

    if-nez v0, :cond_7

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mi/google/gson/stream/JsonReader;->peekedString:Ljava/lang/String;

    iput v2, p0, Lcom/mi/google/gson/stream/JsonReader;->peeked:I

    iget-object v0, p0, Lcom/mi/google/gson/stream/JsonReader;->pathIndices:[I

    iget v1, p0, Lcom/mi/google/gson/stream/JsonReader;->stackSize:I

    add-int/lit8 v1, v1, -0x1

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    return-wide v3

    :cond_7
    new-instance v0, Ljava/lang/NumberFormatException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected a long but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/mi/google/gson/stream/JsonReader;->peekedString:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-direct {p0}, Lcom/mi/google/gson/stream/JsonReader;->locationString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public nextName()Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget v0, p0, Lcom/mi/google/gson/stream/JsonReader;->peeked:I

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/mi/google/gson/stream/JsonReader;->doPeek()I

    move-result v0

    :cond_0
    const/16 v1, 0xe

    if-ne v0, v1, :cond_1

    invoke-direct {p0}, Lcom/mi/google/gson/stream/JsonReader;->nextUnquotedValue()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/16 v1, 0xc

    if-ne v0, v1, :cond_2

    const/16 v0, 0x27

    invoke-direct {p0, v0}, Lcom/mi/google/gson/stream/JsonReader;->nextQuotedValue(C)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    const/16 v1, 0xd

    if-ne v0, v1, :cond_3

    const/16 v0, 0x22

    invoke-direct {p0, v0}, Lcom/mi/google/gson/stream/JsonReader;->nextQuotedValue(C)Ljava/lang/String;

    move-result-object v0

    :goto_0
    const/4 v1, 0x0

    iput v1, p0, Lcom/mi/google/gson/stream/JsonReader;->peeked:I

    iget-object v1, p0, Lcom/mi/google/gson/stream/JsonReader;->pathNames:[Ljava/lang/String;

    iget v2, p0, Lcom/mi/google/gson/stream/JsonReader;->stackSize:I

    add-int/lit8 v2, v2, -0x1

    aput-object v0, v1, v2

    return-object v0

    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected a name but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/mi/google/gson/stream/JsonReader;->peek()Lcom/mi/google/gson/stream/JsonToken;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-direct {p0}, Lcom/mi/google/gson/stream/JsonReader;->locationString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public nextNull()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget v0, p0, Lcom/mi/google/gson/stream/JsonReader;->peeked:I

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/mi/google/gson/stream/JsonReader;->doPeek()I

    move-result v0

    :cond_0
    const/4 v1, 0x7

    if-ne v0, v1, :cond_1

    const/4 v0, 0x0

    iput v0, p0, Lcom/mi/google/gson/stream/JsonReader;->peeked:I

    iget-object v0, p0, Lcom/mi/google/gson/stream/JsonReader;->pathIndices:[I

    iget v1, p0, Lcom/mi/google/gson/stream/JsonReader;->stackSize:I

    add-int/lit8 v1, v1, -0x1

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    return-void

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected null but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/mi/google/gson/stream/JsonReader;->peek()Lcom/mi/google/gson/stream/JsonToken;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-direct {p0}, Lcom/mi/google/gson/stream/JsonReader;->locationString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public nextString()Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget v0, p0, Lcom/mi/google/gson/stream/JsonReader;->peeked:I

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/mi/google/gson/stream/JsonReader;->doPeek()I

    move-result v0

    :cond_0
    const/16 v1, 0xa

    if-ne v0, v1, :cond_1

    invoke-direct {p0}, Lcom/mi/google/gson/stream/JsonReader;->nextUnquotedValue()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/16 v1, 0x8

    if-ne v0, v1, :cond_2

    const/16 v0, 0x27

    invoke-direct {p0, v0}, Lcom/mi/google/gson/stream/JsonReader;->nextQuotedValue(C)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    const/16 v1, 0x9

    if-ne v0, v1, :cond_3

    const/16 v0, 0x22

    invoke-direct {p0, v0}, Lcom/mi/google/gson/stream/JsonReader;->nextQuotedValue(C)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    const/16 v1, 0xb

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lcom/mi/google/gson/stream/JsonReader;->peekedString:Ljava/lang/String;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/mi/google/gson/stream/JsonReader;->peekedString:Ljava/lang/String;

    goto :goto_0

    :cond_4
    const/16 v1, 0xf

    if-ne v0, v1, :cond_5

    iget-wide v0, p0, Lcom/mi/google/gson/stream/JsonReader;->peekedLong:J

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_5
    const/16 v1, 0x10

    if-ne v0, v1, :cond_6

    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/mi/google/gson/stream/JsonReader;->buffer:[C

    iget v2, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    iget v3, p0, Lcom/mi/google/gson/stream/JsonReader;->peekedNumberLength:I

    invoke-direct {v0, v1, v2, v3}, Ljava/lang/String;-><init>([CII)V

    iget v1, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    iget v2, p0, Lcom/mi/google/gson/stream/JsonReader;->peekedNumberLength:I

    add-int/2addr v1, v2

    iput v1, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    :goto_0
    const/4 v1, 0x0

    iput v1, p0, Lcom/mi/google/gson/stream/JsonReader;->peeked:I

    iget-object v1, p0, Lcom/mi/google/gson/stream/JsonReader;->pathIndices:[I

    iget v2, p0, Lcom/mi/google/gson/stream/JsonReader;->stackSize:I

    add-int/lit8 v2, v2, -0x1

    aget v3, v1, v2

    add-int/lit8 v3, v3, 0x1

    aput v3, v1, v2

    return-object v0

    :cond_6
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected a string but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/mi/google/gson/stream/JsonReader;->peek()Lcom/mi/google/gson/stream/JsonToken;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-direct {p0}, Lcom/mi/google/gson/stream/JsonReader;->locationString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public peek()Lcom/mi/google/gson/stream/JsonToken;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget v0, p0, Lcom/mi/google/gson/stream/JsonReader;->peeked:I

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/mi/google/gson/stream/JsonReader;->doPeek()I

    move-result v0

    :cond_0
    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :pswitch_0
    sget-object v0, Lcom/mi/google/gson/stream/JsonToken;->END_DOCUMENT:Lcom/mi/google/gson/stream/JsonToken;

    return-object v0

    :pswitch_1
    sget-object v0, Lcom/mi/google/gson/stream/JsonToken;->NUMBER:Lcom/mi/google/gson/stream/JsonToken;

    return-object v0

    :pswitch_2
    sget-object v0, Lcom/mi/google/gson/stream/JsonToken;->NAME:Lcom/mi/google/gson/stream/JsonToken;

    return-object v0

    :pswitch_3
    sget-object v0, Lcom/mi/google/gson/stream/JsonToken;->STRING:Lcom/mi/google/gson/stream/JsonToken;

    return-object v0

    :pswitch_4
    sget-object v0, Lcom/mi/google/gson/stream/JsonToken;->NULL:Lcom/mi/google/gson/stream/JsonToken;

    return-object v0

    :pswitch_5
    sget-object v0, Lcom/mi/google/gson/stream/JsonToken;->BOOLEAN:Lcom/mi/google/gson/stream/JsonToken;

    return-object v0

    :pswitch_6
    sget-object v0, Lcom/mi/google/gson/stream/JsonToken;->END_ARRAY:Lcom/mi/google/gson/stream/JsonToken;

    return-object v0

    :pswitch_7
    sget-object v0, Lcom/mi/google/gson/stream/JsonToken;->BEGIN_ARRAY:Lcom/mi/google/gson/stream/JsonToken;

    return-object v0

    :pswitch_8
    sget-object v0, Lcom/mi/google/gson/stream/JsonToken;->END_OBJECT:Lcom/mi/google/gson/stream/JsonToken;

    return-object v0

    :pswitch_9
    sget-object v0, Lcom/mi/google/gson/stream/JsonToken;->BEGIN_OBJECT:Lcom/mi/google/gson/stream/JsonToken;

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final setLenient(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/mi/google/gson/stream/JsonReader;->lenient:Z

    return-void
.end method

.method public skipValue()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    move v1, v0

    :cond_0
    iget v2, p0, Lcom/mi/google/gson/stream/JsonReader;->peeked:I

    if-nez v2, :cond_1

    invoke-virtual {p0}, Lcom/mi/google/gson/stream/JsonReader;->doPeek()I

    move-result v2

    :cond_1
    const/4 v3, 0x3

    const/4 v4, 0x1

    if-ne v2, v3, :cond_2

    invoke-direct {p0, v4}, Lcom/mi/google/gson/stream/JsonReader;->push(I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_2
    if-ne v2, v4, :cond_3

    invoke-direct {p0, v3}, Lcom/mi/google/gson/stream/JsonReader;->push(I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_3
    const/4 v3, 0x4

    if-ne v2, v3, :cond_4

    iget v2, p0, Lcom/mi/google/gson/stream/JsonReader;->stackSize:I

    sub-int/2addr v2, v4

    iput v2, p0, Lcom/mi/google/gson/stream/JsonReader;->stackSize:I

    add-int/lit8 v1, v1, -0x1

    goto :goto_3

    :cond_4
    const/4 v3, 0x2

    if-ne v2, v3, :cond_5

    iget v2, p0, Lcom/mi/google/gson/stream/JsonReader;->stackSize:I

    sub-int/2addr v2, v4

    iput v2, p0, Lcom/mi/google/gson/stream/JsonReader;->stackSize:I

    add-int/lit8 v1, v1, -0x1

    goto :goto_3

    :cond_5
    const/16 v3, 0xe

    if-eq v2, v3, :cond_b

    const/16 v3, 0xa

    if-ne v2, v3, :cond_6

    goto :goto_2

    :cond_6
    const/16 v3, 0x8

    if-eq v2, v3, :cond_a

    const/16 v3, 0xc

    if-ne v2, v3, :cond_7

    goto :goto_1

    :cond_7
    const/16 v3, 0x9

    if-eq v2, v3, :cond_9

    const/16 v3, 0xd

    if-ne v2, v3, :cond_8

    goto :goto_0

    :cond_8
    const/16 v3, 0x10

    if-ne v2, v3, :cond_c

    iget v2, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    iget v3, p0, Lcom/mi/google/gson/stream/JsonReader;->peekedNumberLength:I

    add-int/2addr v2, v3

    iput v2, p0, Lcom/mi/google/gson/stream/JsonReader;->pos:I

    goto :goto_3

    :cond_9
    :goto_0
    const/16 v2, 0x22

    invoke-direct {p0, v2}, Lcom/mi/google/gson/stream/JsonReader;->skipQuotedValue(C)V

    goto :goto_3

    :cond_a
    :goto_1
    const/16 v2, 0x27

    invoke-direct {p0, v2}, Lcom/mi/google/gson/stream/JsonReader;->skipQuotedValue(C)V

    goto :goto_3

    :cond_b
    :goto_2
    invoke-direct {p0}, Lcom/mi/google/gson/stream/JsonReader;->skipUnquotedValue()V

    :cond_c
    :goto_3
    iput v0, p0, Lcom/mi/google/gson/stream/JsonReader;->peeked:I

    if-nez v1, :cond_0

    iget-object v0, p0, Lcom/mi/google/gson/stream/JsonReader;->pathIndices:[I

    iget v1, p0, Lcom/mi/google/gson/stream/JsonReader;->stackSize:I

    add-int/lit8 v2, v1, -0x1

    aget v3, v0, v2

    add-int/2addr v3, v4

    aput v3, v0, v2

    iget-object v0, p0, Lcom/mi/google/gson/stream/JsonReader;->pathNames:[Ljava/lang/String;

    sub-int/2addr v1, v4

    const-string v2, "null"

    aput-object v2, v0, v1

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-direct {p0}, Lcom/mi/google/gson/stream/JsonReader;->locationString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
