.class public Lcom/airbnb/lottie/animation/keyframe/TextKeyframeAnimation;
.super Lcom/airbnb/lottie/animation/keyframe/KeyframeAnimation;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/airbnb/lottie/animation/keyframe/KeyframeAnimation<",
        "Lcom/airbnb/lottie/model/DocumentData;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/airbnb/lottie/value/Keyframe<",
            "Lcom/airbnb/lottie/model/DocumentData;",
            ">;>;)V"
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/airbnb/lottie/animation/keyframe/KeyframeAnimation;-><init>(Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method getValue(Lcom/airbnb/lottie/value/Keyframe;F)Lcom/airbnb/lottie/model/DocumentData;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/airbnb/lottie/value/Keyframe<",
            "Lcom/airbnb/lottie/model/DocumentData;",
            ">;F)",
            "Lcom/airbnb/lottie/model/DocumentData;"
        }
    .end annotation

    goto/32 :goto_2b

    nop

    :goto_0
    iget-object v0, p1, Lcom/airbnb/lottie/value/Keyframe;->endFrame:Ljava/lang/Float;

    goto/32 :goto_8

    nop

    :goto_1
    iget-object p1, p1, Lcom/airbnb/lottie/value/Keyframe;->startValue:Ljava/lang/Object;

    goto/32 :goto_1b

    nop

    :goto_2
    check-cast p1, Lcom/airbnb/lottie/model/DocumentData;

    goto/32 :goto_d

    nop

    :goto_3
    if-eqz p2, :cond_0

    goto/32 :goto_20

    :cond_0
    goto/32 :goto_1f

    nop

    :goto_4
    if-nez v0, :cond_1

    goto/32 :goto_10

    :cond_1
    goto/32 :goto_25

    nop

    :goto_5
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    goto/32 :goto_6

    nop

    :goto_6
    goto :goto_b

    :goto_7
    goto/32 :goto_1a

    nop

    :goto_8
    if-eqz v0, :cond_2

    goto/32 :goto_7

    :cond_2
    goto/32 :goto_5

    nop

    :goto_9
    if-eqz v0, :cond_3

    goto/32 :goto_22

    :cond_3
    goto/32 :goto_12

    nop

    :goto_a
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    :goto_b
    goto/32 :goto_19

    nop

    :goto_c
    check-cast p1, Lcom/airbnb/lottie/model/DocumentData;

    goto/32 :goto_f

    nop

    :goto_d
    return-object p1

    :goto_e
    goto/32 :goto_1

    nop

    :goto_f
    return-object p1

    :goto_10
    goto/32 :goto_2c

    nop

    :goto_11
    invoke-virtual {p0}, Lcom/airbnb/lottie/animation/keyframe/TextKeyframeAnimation;->getInterpolatedCurrentKeyframeProgress()F

    move-result v7

    goto/32 :goto_18

    nop

    :goto_12
    iget-object p1, p1, Lcom/airbnb/lottie/value/Keyframe;->startValue:Ljava/lang/Object;

    goto/32 :goto_21

    nop

    :goto_13
    iget-object v0, p1, Lcom/airbnb/lottie/value/Keyframe;->endValue:Ljava/lang/Object;

    goto/32 :goto_9

    nop

    :goto_14
    move-object v4, v0

    goto/32 :goto_1d

    nop

    :goto_15
    return-object p1

    :goto_16
    iget-object p2, p1, Lcom/airbnb/lottie/value/Keyframe;->endValue:Ljava/lang/Object;

    goto/32 :goto_3

    nop

    :goto_17
    move v6, p2

    goto/32 :goto_27

    nop

    :goto_18
    invoke-virtual {p0}, Lcom/airbnb/lottie/animation/keyframe/TextKeyframeAnimation;->getProgress()F

    move-result v8

    goto/32 :goto_17

    nop

    :goto_19
    move v3, v0

    goto/32 :goto_1e

    nop

    :goto_1a
    iget-object v0, p1, Lcom/airbnb/lottie/value/Keyframe;->endFrame:Ljava/lang/Float;

    goto/32 :goto_a

    nop

    :goto_1b
    check-cast p1, Lcom/airbnb/lottie/model/DocumentData;

    goto/32 :goto_15

    nop

    :goto_1c
    check-cast p1, Lcom/airbnb/lottie/model/DocumentData;

    goto/32 :goto_26

    nop

    :goto_1d
    check-cast v4, Lcom/airbnb/lottie/model/DocumentData;

    goto/32 :goto_13

    nop

    :goto_1e
    iget-object v0, p1, Lcom/airbnb/lottie/value/Keyframe;->startValue:Ljava/lang/Object;

    goto/32 :goto_14

    nop

    :goto_1f
    goto :goto_e

    :goto_20
    goto/32 :goto_2d

    nop

    :goto_21
    goto :goto_2a

    :goto_22
    goto/32 :goto_29

    nop

    :goto_23
    if-eqz p2, :cond_4

    goto/32 :goto_e

    :cond_4
    goto/32 :goto_16

    nop

    :goto_24
    iget v2, p1, Lcom/airbnb/lottie/value/Keyframe;->startFrame:F

    goto/32 :goto_0

    nop

    :goto_25
    iget-object v1, p0, Lcom/airbnb/lottie/animation/keyframe/TextKeyframeAnimation;->valueCallback:Lcom/airbnb/lottie/value/LottieValueCallback;

    goto/32 :goto_24

    nop

    :goto_26
    move-object v5, p1

    goto/32 :goto_11

    nop

    :goto_27
    invoke-virtual/range {v1 .. v8}, Lcom/airbnb/lottie/value/LottieValueCallback;->getValueInternal(FFLjava/lang/Object;Ljava/lang/Object;FFF)Ljava/lang/Object;

    move-result-object p1

    goto/32 :goto_c

    nop

    :goto_28
    cmpl-float p2, p2, v0

    goto/32 :goto_23

    nop

    :goto_29
    iget-object p1, p1, Lcom/airbnb/lottie/value/Keyframe;->endValue:Ljava/lang/Object;

    :goto_2a
    goto/32 :goto_1c

    nop

    :goto_2b
    iget-object v0, p0, Lcom/airbnb/lottie/animation/keyframe/TextKeyframeAnimation;->valueCallback:Lcom/airbnb/lottie/value/LottieValueCallback;

    goto/32 :goto_4

    nop

    :goto_2c
    const/high16 v0, 0x3f800000    # 1.0f

    goto/32 :goto_28

    nop

    :goto_2d
    iget-object p1, p1, Lcom/airbnb/lottie/value/Keyframe;->endValue:Ljava/lang/Object;

    goto/32 :goto_2

    nop
.end method

.method bridge synthetic getValue(Lcom/airbnb/lottie/value/Keyframe;F)Ljava/lang/Object;
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-object p1

    :goto_1
    invoke-virtual {p0, p1, p2}, Lcom/airbnb/lottie/animation/keyframe/TextKeyframeAnimation;->getValue(Lcom/airbnb/lottie/value/Keyframe;F)Lcom/airbnb/lottie/model/DocumentData;

    move-result-object p1

    goto/32 :goto_0

    nop
.end method

.method public setStringValueCallback(Lcom/airbnb/lottie/value/LottieValueCallback;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/airbnb/lottie/value/LottieValueCallback<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    new-instance v0, Lcom/airbnb/lottie/value/LottieFrameInfo;

    invoke-direct {v0}, Lcom/airbnb/lottie/value/LottieFrameInfo;-><init>()V

    new-instance v1, Lcom/airbnb/lottie/model/DocumentData;

    invoke-direct {v1}, Lcom/airbnb/lottie/model/DocumentData;-><init>()V

    new-instance v2, Lcom/airbnb/lottie/animation/keyframe/TextKeyframeAnimation$1;

    invoke-direct {v2, p0, v0, p1, v1}, Lcom/airbnb/lottie/animation/keyframe/TextKeyframeAnimation$1;-><init>(Lcom/airbnb/lottie/animation/keyframe/TextKeyframeAnimation;Lcom/airbnb/lottie/value/LottieFrameInfo;Lcom/airbnb/lottie/value/LottieValueCallback;Lcom/airbnb/lottie/model/DocumentData;)V

    invoke-super {p0, v2}, Lcom/airbnb/lottie/animation/keyframe/KeyframeAnimation;->setValueCallback(Lcom/airbnb/lottie/value/LottieValueCallback;)V

    return-void
.end method
