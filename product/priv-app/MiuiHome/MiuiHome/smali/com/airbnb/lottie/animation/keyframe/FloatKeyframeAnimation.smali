.class public Lcom/airbnb/lottie/animation/keyframe/FloatKeyframeAnimation;
.super Lcom/airbnb/lottie/animation/keyframe/KeyframeAnimation;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/airbnb/lottie/animation/keyframe/KeyframeAnimation<",
        "Ljava/lang/Float;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/airbnb/lottie/value/Keyframe<",
            "Ljava/lang/Float;",
            ">;>;)V"
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/airbnb/lottie/animation/keyframe/KeyframeAnimation;-><init>(Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method public getFloatValue()F
    .locals 2

    invoke-virtual {p0}, Lcom/airbnb/lottie/animation/keyframe/FloatKeyframeAnimation;->getCurrentKeyframe()Lcom/airbnb/lottie/value/Keyframe;

    move-result-object v0

    invoke-virtual {p0}, Lcom/airbnb/lottie/animation/keyframe/FloatKeyframeAnimation;->getInterpolatedCurrentKeyframeProgress()F

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/airbnb/lottie/animation/keyframe/FloatKeyframeAnimation;->getFloatValue(Lcom/airbnb/lottie/value/Keyframe;F)F

    move-result v0

    return v0
.end method

.method getFloatValue(Lcom/airbnb/lottie/value/Keyframe;F)F
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/airbnb/lottie/value/Keyframe<",
            "Ljava/lang/Float;",
            ">;F)F"
        }
    .end annotation

    goto/32 :goto_4

    nop

    :goto_0
    return p1

    :goto_1
    goto/32 :goto_d

    nop

    :goto_2
    if-nez v0, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_1e

    nop

    :goto_3
    iget-object v0, p1, Lcom/airbnb/lottie/value/Keyframe;->startValue:Ljava/lang/Object;

    goto/32 :goto_10

    nop

    :goto_4
    iget-object v0, p1, Lcom/airbnb/lottie/value/Keyframe;->startValue:Ljava/lang/Object;

    goto/32 :goto_c

    nop

    :goto_5
    if-nez v0, :cond_1

    goto/32 :goto_9

    :cond_1
    goto/32 :goto_15

    nop

    :goto_6
    invoke-virtual {p0}, Lcom/airbnb/lottie/animation/keyframe/FloatKeyframeAnimation;->getLinearCurrentKeyframeProgress()F

    move-result v7

    goto/32 :goto_b

    nop

    :goto_7
    check-cast v0, Ljava/lang/Float;

    goto/32 :goto_17

    nop

    :goto_8
    return p1

    :goto_9
    goto/32 :goto_f

    nop

    :goto_a
    move v6, p2

    goto/32 :goto_11

    nop

    :goto_b
    invoke-virtual {p0}, Lcom/airbnb/lottie/animation/keyframe/FloatKeyframeAnimation;->getProgress()F

    move-result v8

    goto/32 :goto_a

    nop

    :goto_c
    if-nez v0, :cond_2

    goto/32 :goto_9

    :cond_2
    goto/32 :goto_19

    nop

    :goto_d
    invoke-virtual {p1}, Lcom/airbnb/lottie/value/Keyframe;->getStartValueFloat()F

    move-result v0

    goto/32 :goto_1c

    nop

    :goto_e
    check-cast v4, Ljava/lang/Float;

    goto/32 :goto_18

    nop

    :goto_f
    new-instance p1, Ljava/lang/IllegalStateException;

    goto/32 :goto_21

    nop

    :goto_10
    move-object v4, v0

    goto/32 :goto_e

    nop

    :goto_11
    invoke-virtual/range {v1 .. v8}, Lcom/airbnb/lottie/value/LottieValueCallback;->getValueInternal(FFLjava/lang/Object;Ljava/lang/Object;FFF)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_7

    nop

    :goto_12
    iget-object v0, p1, Lcom/airbnb/lottie/value/Keyframe;->endFrame:Ljava/lang/Float;

    goto/32 :goto_1b

    nop

    :goto_13
    iget v2, p1, Lcom/airbnb/lottie/value/Keyframe;->startFrame:F

    goto/32 :goto_12

    nop

    :goto_14
    invoke-static {v0, p1, p2}, Lcom/airbnb/lottie/utils/MiscUtils;->lerp(FFF)F

    move-result p1

    goto/32 :goto_8

    nop

    :goto_15
    iget-object v0, p0, Lcom/airbnb/lottie/animation/keyframe/FloatKeyframeAnimation;->valueCallback:Lcom/airbnb/lottie/value/LottieValueCallback;

    goto/32 :goto_2

    nop

    :goto_16
    check-cast v5, Ljava/lang/Float;

    goto/32 :goto_6

    nop

    :goto_17
    if-nez v0, :cond_3

    goto/32 :goto_1

    :cond_3
    goto/32 :goto_1f

    nop

    :goto_18
    iget-object v0, p1, Lcom/airbnb/lottie/value/Keyframe;->endValue:Ljava/lang/Object;

    goto/32 :goto_1d

    nop

    :goto_19
    iget-object v0, p1, Lcom/airbnb/lottie/value/Keyframe;->endValue:Ljava/lang/Object;

    goto/32 :goto_5

    nop

    :goto_1a
    throw p1

    :goto_1b
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v3

    goto/32 :goto_3

    nop

    :goto_1c
    invoke-virtual {p1}, Lcom/airbnb/lottie/value/Keyframe;->getEndValueFloat()F

    move-result p1

    goto/32 :goto_14

    nop

    :goto_1d
    move-object v5, v0

    goto/32 :goto_16

    nop

    :goto_1e
    iget-object v1, p0, Lcom/airbnb/lottie/animation/keyframe/FloatKeyframeAnimation;->valueCallback:Lcom/airbnb/lottie/value/LottieValueCallback;

    goto/32 :goto_13

    nop

    :goto_1f
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result p1

    goto/32 :goto_0

    nop

    :goto_20
    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_1a

    nop

    :goto_21
    const-string p2, "Missing values for keyframe."

    goto/32 :goto_20

    nop
.end method

.method getValue(Lcom/airbnb/lottie/value/Keyframe;F)Ljava/lang/Float;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/airbnb/lottie/value/Keyframe<",
            "Ljava/lang/Float;",
            ">;F)",
            "Ljava/lang/Float;"
        }
    .end annotation

    goto/32 :goto_2

    nop

    :goto_0
    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p1

    goto/32 :goto_1

    nop

    :goto_1
    return-object p1

    :goto_2
    invoke-virtual {p0, p1, p2}, Lcom/airbnb/lottie/animation/keyframe/FloatKeyframeAnimation;->getFloatValue(Lcom/airbnb/lottie/value/Keyframe;F)F

    move-result p1

    goto/32 :goto_0

    nop
.end method

.method bridge synthetic getValue(Lcom/airbnb/lottie/value/Keyframe;F)Ljava/lang/Object;
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    invoke-virtual {p0, p1, p2}, Lcom/airbnb/lottie/animation/keyframe/FloatKeyframeAnimation;->getValue(Lcom/airbnb/lottie/value/Keyframe;F)Ljava/lang/Float;

    move-result-object p1

    goto/32 :goto_1

    nop

    :goto_1
    return-object p1
.end method
