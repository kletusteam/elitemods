.class public final Landroidx/appcompat/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroidx/appcompat/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final action_bar:I = 0x7f0a0031

.field public static final action_bar_activity_content:I = 0x7f0a0032

.field public static final action_bar_container:I = 0x7f0a0036

.field public static final action_bar_root:I = 0x7f0a003d

.field public static final action_bar_spinner:I = 0x7f0a003e

.field public static final action_bar_subtitle:I = 0x7f0a003f

.field public static final action_bar_title:I = 0x7f0a0041

.field public static final action_context_bar:I = 0x7f0a0045

.field public static final action_menu_divider:I = 0x7f0a0049

.field public static final action_menu_presenter:I = 0x7f0a004c

.field public static final action_mode_bar:I = 0x7f0a004d

.field public static final action_mode_bar_stub:I = 0x7f0a004e

.field public static final action_mode_close_button:I = 0x7f0a004f

.field public static final activity_chooser_view_content:I = 0x7f0a0052

.field public static final add:I = 0x7f0a0054

.field public static final alertTitle:I = 0x7f0a0057

.field public static final buttonPanel:I = 0x7f0a00a9

.field public static final checkbox:I = 0x7f0a00bd

.field public static final checked:I = 0x7f0a00c4

.field public static final content:I = 0x7f0a00df

.field public static final contentPanel:I = 0x7f0a00e0

.field public static final custom:I = 0x7f0a00eb

.field public static final customPanel:I = 0x7f0a00ec

.field public static final decor_content_parent:I = 0x7f0a00f6

.field public static final default_activity_button:I = 0x7f0a00f8

.field public static final edit_query:I = 0x7f0a0144

.field public static final expand_activities_button:I = 0x7f0a0156

.field public static final expanded_menu:I = 0x7f0a0157

.field public static final group_divider:I = 0x7f0a019a

.field public static final home:I = 0x7f0a01aa

.field public static final icon:I = 0x7f0a01b5

.field public static final image:I = 0x7f0a01c5

.field public static final listMode:I = 0x7f0a01f9

.field public static final list_item:I = 0x7f0a01fa

.field public static final message:I = 0x7f0a0235

.field public static final multiply:I = 0x7f0a0268

.field public static final none:I = 0x7f0a0275

.field public static final normal:I = 0x7f0a0276

.field public static final off:I = 0x7f0a027c

.field public static final on:I = 0x7f0a027d

.field public static final parentPanel:I = 0x7f0a0289

.field public static final progress_circular:I = 0x7f0a02a2

.field public static final progress_horizontal:I = 0x7f0a02a3

.field public static final radio:I = 0x7f0a02a9

.field public static final screen:I = 0x7f0a02dd

.field public static final scrollIndicatorDown:I = 0x7f0a02e1

.field public static final scrollIndicatorUp:I = 0x7f0a02e2

.field public static final scrollView:I = 0x7f0a02e3

.field public static final search_badge:I = 0x7f0a02e8

.field public static final search_bar:I = 0x7f0a02e9

.field public static final search_button:I = 0x7f0a02f6

.field public static final search_close_btn:I = 0x7f0a02f8

.field public static final search_edit_frame:I = 0x7f0a02fa

.field public static final search_go_btn:I = 0x7f0a02fb

.field public static final search_mag_icon:I = 0x7f0a0309

.field public static final search_plate:I = 0x7f0a030d

.field public static final search_src_text:I = 0x7f0a0310

.field public static final search_voice_btn:I = 0x7f0a0313

.field public static final select_dialog_listview:I = 0x7f0a031c

.field public static final shortcut:I = 0x7f0a0325

.field public static final spacer:I = 0x7f0a0346

.field public static final split_action_bar:I = 0x7f0a034c

.field public static final src_atop:I = 0x7f0a0353

.field public static final src_in:I = 0x7f0a0354

.field public static final src_over:I = 0x7f0a0355

.field public static final submenuarrow:I = 0x7f0a0362

.field public static final submit_area:I = 0x7f0a0363

.field public static final tabMode:I = 0x7f0a036e

.field public static final textSpacerNoButtons:I = 0x7f0a0390

.field public static final textSpacerNoTitle:I = 0x7f0a0391

.field public static final title:I = 0x7f0a03a5

.field public static final titleDividerNoCustom:I = 0x7f0a03a6

.field public static final title_template:I = 0x7f0a03ab

.field public static final topPanel:I = 0x7f0a03ae

.field public static final unchecked:I = 0x7f0a03d8

.field public static final uniform:I = 0x7f0a03da

.field public static final up:I = 0x7f0a03e0

.field public static final wrap_content:I = 0x7f0a0414
