.class public Lmiuix/animation/internal/AnimTask;
.super Lmiuix/animation/utils/LinkNode;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmiuix/animation/utils/LinkNode<",
        "Lmiuix/animation/internal/AnimTask;",
        ">;",
        "Ljava/lang/Runnable;"
    }
.end annotation


# static fields
.field public static final sTaskCount:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field public final animStats:Lmiuix/animation/internal/AnimStats;

.field public volatile deltaT:J

.field public volatile info:Lmiuix/animation/internal/TransitionInfo;

.field public volatile startPos:I

.field public volatile toPage:Z

.field public volatile totalT:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    sput-object v0, Lmiuix/animation/internal/AnimTask;->sTaskCount:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lmiuix/animation/utils/LinkNode;-><init>()V

    new-instance v0, Lmiuix/animation/internal/AnimStats;

    invoke-direct {v0}, Lmiuix/animation/internal/AnimStats;-><init>()V

    iput-object v0, p0, Lmiuix/animation/internal/AnimTask;->animStats:Lmiuix/animation/internal/AnimStats;

    return-void
.end method

.method public static isRunning(B)Z
    .locals 2

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v1, 0x2

    if-ne p0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :cond_1
    :goto_0
    return v0
.end method


# virtual methods
.method public getAnimCount()I
    .locals 1

    iget-object v0, p0, Lmiuix/animation/internal/AnimTask;->animStats:Lmiuix/animation/internal/AnimStats;

    iget v0, v0, Lmiuix/animation/internal/AnimStats;->animCount:I

    return v0
.end method

.method public getTotalAnimCount()I
    .locals 3

    const/4 v0, 0x0

    move v1, v0

    move-object v0, p0

    :goto_0
    if-eqz v0, :cond_0

    iget-object v2, v0, Lmiuix/animation/internal/AnimTask;->animStats:Lmiuix/animation/internal/AnimStats;

    iget v2, v2, Lmiuix/animation/internal/AnimStats;->animCount:I

    add-int/2addr v1, v2

    iget-object v0, v0, Lmiuix/animation/internal/AnimTask;->next:Lmiuix/animation/utils/LinkNode;

    check-cast v0, Lmiuix/animation/internal/AnimTask;

    goto :goto_0

    :cond_0
    return v1
.end method

.method public run()V
    .locals 7

    :try_start_0
    iget-wide v1, p0, Lmiuix/animation/internal/AnimTask;->totalT:J

    iget-wide v3, p0, Lmiuix/animation/internal/AnimTask;->deltaT:J

    const/4 v5, 0x1

    iget-boolean v6, p0, Lmiuix/animation/internal/AnimTask;->toPage:Z

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Lmiuix/animation/internal/AnimRunnerTask;->doAnimationFrame(Lmiuix/animation/internal/AnimTask;JJZZ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "miuix_anim"

    const-string v2, "doAnimationFrame failed"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_0
    sget-object v0, Lmiuix/animation/internal/AnimTask;->sTaskCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lmiuix/animation/internal/AnimRunner;->sRunnerHandler:Lmiuix/animation/internal/RunnerHandler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lmiuix/animation/internal/RunnerHandler;->sendEmptyMessage(I)Z

    :cond_0
    return-void
.end method

.method public setup(II)V
    .locals 1

    iget-object v0, p0, Lmiuix/animation/internal/AnimTask;->animStats:Lmiuix/animation/internal/AnimStats;

    invoke-virtual {v0}, Lmiuix/animation/internal/AnimStats;->clear()V

    iget-object v0, p0, Lmiuix/animation/internal/AnimTask;->animStats:Lmiuix/animation/internal/AnimStats;

    iput p2, v0, Lmiuix/animation/internal/AnimStats;->animCount:I

    iput p1, p0, Lmiuix/animation/internal/AnimTask;->startPos:I

    return-void
.end method

.method public start(JJZ)V
    .locals 0

    iput-wide p1, p0, Lmiuix/animation/internal/AnimTask;->totalT:J

    iput-wide p3, p0, Lmiuix/animation/internal/AnimTask;->deltaT:J

    iput-boolean p5, p0, Lmiuix/animation/internal/AnimTask;->toPage:Z

    invoke-static {p0}, Lmiuix/animation/internal/ThreadPoolUtil;->post(Ljava/lang/Runnable;)V

    return-void
.end method

.method updateAnimStats()V
    .locals 6

    goto/32 :goto_f

    nop

    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :goto_1
    iget-object v2, p0, Lmiuix/animation/internal/AnimTask;->animStats:Lmiuix/animation/internal/AnimStats;

    goto/32 :goto_20

    nop

    :goto_2
    iput v3, v2, Lmiuix/animation/internal/AnimStats;->endCount:I

    goto/32 :goto_1d

    nop

    :goto_3
    goto :goto_18

    :pswitch_0
    goto/32 :goto_24

    nop

    :goto_4
    iget-object v2, v2, Lmiuix/animation/internal/TransitionInfo;->updateList:Ljava/util/List;

    goto/32 :goto_32

    nop

    :goto_5
    goto :goto_18

    :pswitch_1
    goto/32 :goto_1b

    nop

    :goto_6
    iget-byte v2, v2, Lmiuix/animation/internal/AnimInfo;->op:B

    packed-switch v2, :pswitch_data_0

    goto/32 :goto_3

    nop

    :goto_7
    iget-byte v3, v3, Lmiuix/animation/internal/AnimInfo;->op:B

    goto/32 :goto_28

    nop

    :goto_8
    iget-object v2, p0, Lmiuix/animation/internal/AnimTask;->info:Lmiuix/animation/internal/TransitionInfo;

    goto/32 :goto_4

    nop

    :goto_9
    iput v5, v3, Lmiuix/animation/internal/AnimStats;->initCount:I

    goto/32 :goto_31

    nop

    :goto_a
    add-int/2addr v3, v4

    goto/32 :goto_e

    nop

    :goto_b
    add-int/2addr v3, v4

    goto/32 :goto_17

    nop

    :goto_c
    iget v3, v2, Lmiuix/animation/internal/AnimStats;->failCount:I

    goto/32 :goto_2a

    nop

    :goto_d
    iget-object v3, p0, Lmiuix/animation/internal/AnimTask;->animStats:Lmiuix/animation/internal/AnimStats;

    goto/32 :goto_12

    nop

    :goto_e
    iput v3, v2, Lmiuix/animation/internal/AnimStats;->cancelCount:I

    goto/32 :goto_2d

    nop

    :goto_f
    iget v0, p0, Lmiuix/animation/internal/AnimTask;->startPos:I

    goto/32 :goto_22

    nop

    :goto_10
    iput v3, v2, Lmiuix/animation/internal/AnimStats;->failCount:I

    goto/32 :goto_5

    nop

    :goto_11
    if-eq v3, v4, :cond_0

    goto/32 :goto_34

    :cond_0
    goto/32 :goto_33

    nop

    :goto_12
    iget v5, v3, Lmiuix/animation/internal/AnimStats;->initCount:I

    goto/32 :goto_1f

    nop

    :goto_13
    if-lt v0, v1, :cond_1

    goto/32 :goto_2c

    :cond_1
    goto/32 :goto_8

    nop

    :goto_14
    iget-object v3, v2, Lmiuix/animation/listener/UpdateInfo;->animInfo:Lmiuix/animation/internal/AnimInfo;

    goto/32 :goto_29

    nop

    :goto_15
    goto :goto_18

    :goto_16
    goto/32 :goto_30

    nop

    :goto_17
    iput v3, v2, Lmiuix/animation/internal/AnimStats;->startCount:I

    :goto_18
    goto/32 :goto_21

    nop

    :goto_19
    iget v3, v2, Lmiuix/animation/internal/AnimStats;->cancelCount:I

    goto/32 :goto_a

    nop

    :goto_1a
    if-nez v3, :cond_2

    goto/32 :goto_1e

    :cond_2
    goto/32 :goto_14

    nop

    :goto_1b
    iget-object v2, p0, Lmiuix/animation/internal/AnimTask;->animStats:Lmiuix/animation/internal/AnimStats;

    goto/32 :goto_19

    nop

    :goto_1c
    check-cast v2, Lmiuix/animation/listener/UpdateInfo;

    goto/32 :goto_36

    nop

    :goto_1d
    goto :goto_18

    :goto_1e
    goto/32 :goto_27

    nop

    :goto_1f
    add-int/2addr v5, v4

    goto/32 :goto_9

    nop

    :goto_20
    iget v3, v2, Lmiuix/animation/internal/AnimStats;->endCount:I

    goto/32 :goto_35

    nop

    :goto_21
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_2b

    nop

    :goto_22
    iget v1, p0, Lmiuix/animation/internal/AnimTask;->startPos:I

    goto/32 :goto_26

    nop

    :goto_23
    iget v2, v2, Lmiuix/animation/internal/AnimStats;->animCount:I

    goto/32 :goto_2e

    nop

    :goto_24
    iget-object v2, p0, Lmiuix/animation/internal/AnimTask;->animStats:Lmiuix/animation/internal/AnimStats;

    goto/32 :goto_c

    nop

    :goto_25
    iget v3, v2, Lmiuix/animation/internal/AnimStats;->startCount:I

    goto/32 :goto_b

    nop

    :goto_26
    iget-object v2, p0, Lmiuix/animation/internal/AnimTask;->animStats:Lmiuix/animation/internal/AnimStats;

    goto/32 :goto_23

    nop

    :goto_27
    iget-object v2, p0, Lmiuix/animation/internal/AnimTask;->animStats:Lmiuix/animation/internal/AnimStats;

    goto/32 :goto_25

    nop

    :goto_28
    const/4 v4, 0x1

    goto/32 :goto_1a

    nop

    :goto_29
    iget-byte v3, v3, Lmiuix/animation/internal/AnimInfo;->op:B

    goto/32 :goto_11

    nop

    :goto_2a
    add-int/2addr v3, v4

    goto/32 :goto_10

    nop

    :goto_2b
    goto :goto_2f

    :goto_2c
    goto/32 :goto_0

    nop

    :goto_2d
    goto :goto_18

    :pswitch_2
    goto/32 :goto_1

    nop

    :goto_2e
    add-int/2addr v1, v2

    :goto_2f
    goto/32 :goto_13

    nop

    :goto_30
    iget-object v3, v2, Lmiuix/animation/listener/UpdateInfo;->animInfo:Lmiuix/animation/internal/AnimInfo;

    goto/32 :goto_7

    nop

    :goto_31
    iget-object v2, v2, Lmiuix/animation/listener/UpdateInfo;->animInfo:Lmiuix/animation/internal/AnimInfo;

    goto/32 :goto_6

    nop

    :goto_32
    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_1c

    nop

    :goto_33
    goto :goto_1e

    :goto_34
    goto/32 :goto_d

    nop

    :goto_35
    add-int/2addr v3, v4

    goto/32 :goto_2

    nop

    :goto_36
    if-eqz v2, :cond_3

    goto/32 :goto_16

    :cond_3
    goto/32 :goto_15

    nop
.end method
