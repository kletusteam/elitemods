.class public final Lmiuix/animation/physics/SpringAnimation;
.super Lmiuix/animation/physics/DynamicAnimation;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmiuix/animation/physics/DynamicAnimation<",
        "Lmiuix/animation/physics/SpringAnimation;",
        ">;"
    }
.end annotation


# instance fields
.field private mEndRequested:Z

.field private mPendingPosition:F

.field private mSpring:Lmiuix/animation/physics/SpringForce;


# direct methods
.method public constructor <init>(Ljava/lang/Object;Lmiuix/animation/property/FloatProperty;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            ">(TK;",
            "Lmiuix/animation/property/FloatProperty<",
            "TK;>;)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lmiuix/animation/physics/DynamicAnimation;-><init>(Ljava/lang/Object;Lmiuix/animation/property/FloatProperty;)V

    const/4 p1, 0x0

    iput-object p1, p0, Lmiuix/animation/physics/SpringAnimation;->mSpring:Lmiuix/animation/physics/SpringForce;

    const p1, 0x7f7fffff    # Float.MAX_VALUE

    iput p1, p0, Lmiuix/animation/physics/SpringAnimation;->mPendingPosition:F

    const/4 p1, 0x0

    iput-boolean p1, p0, Lmiuix/animation/physics/SpringAnimation;->mEndRequested:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;Lmiuix/animation/property/FloatProperty;F)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            ">(TK;",
            "Lmiuix/animation/property/FloatProperty<",
            "TK;>;F)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lmiuix/animation/physics/DynamicAnimation;-><init>(Ljava/lang/Object;Lmiuix/animation/property/FloatProperty;)V

    const/4 p1, 0x0

    iput-object p1, p0, Lmiuix/animation/physics/SpringAnimation;->mSpring:Lmiuix/animation/physics/SpringForce;

    const p1, 0x7f7fffff    # Float.MAX_VALUE

    iput p1, p0, Lmiuix/animation/physics/SpringAnimation;->mPendingPosition:F

    const/4 p1, 0x0

    iput-boolean p1, p0, Lmiuix/animation/physics/SpringAnimation;->mEndRequested:Z

    new-instance p1, Lmiuix/animation/physics/SpringForce;

    invoke-direct {p1, p3}, Lmiuix/animation/physics/SpringForce;-><init>(F)V

    iput-object p1, p0, Lmiuix/animation/physics/SpringAnimation;->mSpring:Lmiuix/animation/physics/SpringForce;

    return-void
.end method

.method private sanityCheck()V
    .locals 4

    iget-object v0, p0, Lmiuix/animation/physics/SpringAnimation;->mSpring:Lmiuix/animation/physics/SpringForce;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lmiuix/animation/physics/SpringForce;->getFinalPosition()F

    move-result v0

    float-to-double v0, v0

    iget v2, p0, Lmiuix/animation/physics/SpringAnimation;->mMaxValue:F

    float-to-double v2, v2

    cmpl-double v2, v0, v2

    if-gtz v2, :cond_1

    iget v2, p0, Lmiuix/animation/physics/SpringAnimation;->mMinValue:F

    float-to-double v2, v2

    cmpg-double v0, v0, v2

    if-ltz v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Final position of the spring cannot be less than the min value."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Final position of the spring cannot be greater than the max value."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Incomplete SpringAnimation: Either final position or a spring force needs to be set."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public canSkipToEnd()Z
    .locals 4

    iget-object v0, p0, Lmiuix/animation/physics/SpringAnimation;->mSpring:Lmiuix/animation/physics/SpringForce;

    iget-wide v0, v0, Lmiuix/animation/physics/SpringForce;->mDampingRatio:D

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public getSpring()Lmiuix/animation/physics/SpringForce;
    .locals 1

    iget-object v0, p0, Lmiuix/animation/physics/SpringAnimation;->mSpring:Lmiuix/animation/physics/SpringForce;

    return-object v0
.end method

.method isAtEquilibrium(FF)Z
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return p1

    :goto_1
    iget-object v0, p0, Lmiuix/animation/physics/SpringAnimation;->mSpring:Lmiuix/animation/physics/SpringForce;

    goto/32 :goto_2

    nop

    :goto_2
    invoke-virtual {v0, p1, p2}, Lmiuix/animation/physics/SpringForce;->isAtEquilibrium(FF)Z

    move-result p1

    goto/32 :goto_0

    nop
.end method

.method public setSpring(Lmiuix/animation/physics/SpringForce;)Lmiuix/animation/physics/SpringAnimation;
    .locals 0

    iput-object p1, p0, Lmiuix/animation/physics/SpringAnimation;->mSpring:Lmiuix/animation/physics/SpringForce;

    return-object p0
.end method

.method setValueThreshold(F)V
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    return-void
.end method

.method public skipToEnd()V
    .locals 2

    invoke-virtual {p0}, Lmiuix/animation/physics/SpringAnimation;->canSkipToEnd()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_1

    iget-boolean v0, p0, Lmiuix/animation/physics/SpringAnimation;->mRunning:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiuix/animation/physics/SpringAnimation;->mEndRequested:Z

    :cond_0
    return-void

    :cond_1
    new-instance v0, Landroid/util/AndroidRuntimeException;

    const-string v1, "Animations may only be started on the main thread"

    invoke-direct {v0, v1}, Landroid/util/AndroidRuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Spring animations can only come to an end when there is damping"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public start()V
    .locals 3

    invoke-direct {p0}, Lmiuix/animation/physics/SpringAnimation;->sanityCheck()V

    iget-object v0, p0, Lmiuix/animation/physics/SpringAnimation;->mSpring:Lmiuix/animation/physics/SpringForce;

    invoke-virtual {p0}, Lmiuix/animation/physics/SpringAnimation;->getValueThreshold()F

    move-result v1

    float-to-double v1, v1

    invoke-virtual {v0, v1, v2}, Lmiuix/animation/physics/SpringForce;->setValueThreshold(D)V

    invoke-super {p0}, Lmiuix/animation/physics/DynamicAnimation;->start()V

    return-void
.end method

.method updateValueAndVelocity(J)Z
    .locals 20

    goto/32 :goto_4

    nop

    :goto_0
    float-to-double v14, v5

    goto/32 :goto_10

    nop

    :goto_1
    const/4 v4, 0x0

    goto/32 :goto_3

    nop

    :goto_2
    cmpl-float v1, v1, v5

    goto/32 :goto_44

    nop

    :goto_3
    const v5, 0x7f7fffff    # Float.MAX_VALUE

    goto/32 :goto_1e

    nop

    :goto_4
    move-object/from16 v0, p0

    goto/32 :goto_26

    nop

    :goto_5
    iput v5, v0, Lmiuix/animation/physics/SpringAnimation;->mValue:F

    goto/32 :goto_28

    nop

    :goto_6
    iget v1, v0, Lmiuix/animation/physics/SpringAnimation;->mVelocity:F

    goto/32 :goto_46

    nop

    :goto_7
    iget v1, v0, Lmiuix/animation/physics/SpringAnimation;->mPendingPosition:F

    goto/32 :goto_17

    nop

    :goto_8
    iget v1, v0, Lmiuix/animation/physics/SpringAnimation;->mValue:F

    goto/32 :goto_45

    nop

    :goto_9
    if-nez v6, :cond_0

    goto/32 :goto_2d

    :cond_0
    goto/32 :goto_53

    nop

    :goto_a
    iget v5, v0, Lmiuix/animation/physics/SpringAnimation;->mMinValue:F

    goto/32 :goto_32

    nop

    :goto_b
    iget-object v1, v0, Lmiuix/animation/physics/SpringAnimation;->mSpring:Lmiuix/animation/physics/SpringForce;

    goto/32 :goto_4f

    nop

    :goto_c
    iget v1, v0, Lmiuix/animation/physics/SpringAnimation;->mVelocity:F

    goto/32 :goto_25

    nop

    :goto_d
    iget v1, v0, Lmiuix/animation/physics/SpringAnimation;->mValue:F

    goto/32 :goto_23

    nop

    :goto_e
    invoke-static {v1, v5}, Ljava/lang/Math;->min(FF)F

    move-result v1

    goto/32 :goto_33

    nop

    :goto_f
    return v3

    :goto_10
    iget v1, v1, Lmiuix/animation/physics/DynamicAnimation$MassState;->mVelocity:F

    goto/32 :goto_47

    nop

    :goto_11
    invoke-virtual/range {v13 .. v19}, Lmiuix/animation/physics/SpringForce;->updateValues(DDJ)Lmiuix/animation/physics/DynamicAnimation$MassState;

    move-result-object v1

    goto/32 :goto_40

    nop

    :goto_12
    invoke-virtual {v1}, Lmiuix/animation/physics/SpringForce;->getFinalPosition()F

    goto/32 :goto_15

    nop

    :goto_13
    iget v5, v1, Lmiuix/animation/physics/DynamicAnimation$MassState;->mValue:F

    goto/32 :goto_5

    nop

    :goto_14
    const/4 v2, 0x1

    goto/32 :goto_43

    nop

    :goto_15
    iget-object v6, v0, Lmiuix/animation/physics/SpringAnimation;->mSpring:Lmiuix/animation/physics/SpringForce;

    goto/32 :goto_4d

    nop

    :goto_16
    const-wide/16 v11, 0x2

    goto/32 :goto_42

    nop

    :goto_17
    cmpl-float v6, v1, v5

    goto/32 :goto_9

    nop

    :goto_18
    goto/16 :goto_4c

    :goto_19
    goto/32 :goto_30

    nop

    :goto_1a
    invoke-virtual {v6, v7}, Lmiuix/animation/physics/SpringForce;->setFinalPosition(F)Lmiuix/animation/physics/SpringForce;

    goto/32 :goto_20

    nop

    :goto_1b
    iget-object v1, v0, Lmiuix/animation/physics/SpringAnimation;->mSpring:Lmiuix/animation/physics/SpringForce;

    goto/32 :goto_12

    nop

    :goto_1c
    iput v5, v0, Lmiuix/animation/physics/SpringAnimation;->mValue:F

    goto/32 :goto_36

    nop

    :goto_1d
    invoke-virtual {v0, v1, v5}, Lmiuix/animation/physics/SpringAnimation;->isAtEquilibrium(FF)Z

    move-result v1

    goto/32 :goto_31

    nop

    :goto_1e
    if-nez v1, :cond_1

    goto/32 :goto_2f

    :cond_1
    goto/32 :goto_7

    nop

    :goto_1f
    iput v4, v0, Lmiuix/animation/physics/SpringAnimation;->mVelocity:F

    goto/32 :goto_29

    nop

    :goto_20
    iput v5, v0, Lmiuix/animation/physics/SpringAnimation;->mPendingPosition:F

    goto/32 :goto_52

    nop

    :goto_21
    invoke-virtual/range {v6 .. v12}, Lmiuix/animation/physics/SpringForce;->updateValues(DDJ)Lmiuix/animation/physics/DynamicAnimation$MassState;

    move-result-object v1

    goto/32 :goto_49

    nop

    :goto_22
    iget-object v1, v0, Lmiuix/animation/physics/SpringAnimation;->mSpring:Lmiuix/animation/physics/SpringForce;

    goto/32 :goto_51

    nop

    :goto_23
    iget v5, v0, Lmiuix/animation/physics/SpringAnimation;->mMaxValue:F

    goto/32 :goto_e

    nop

    :goto_24
    iput v1, v0, Lmiuix/animation/physics/SpringAnimation;->mValue:F

    goto/32 :goto_d

    nop

    :goto_25
    float-to-double v5, v1

    goto/32 :goto_3c

    nop

    :goto_26
    iget-boolean v1, v0, Lmiuix/animation/physics/SpringAnimation;->mEndRequested:Z

    goto/32 :goto_14

    nop

    :goto_27
    iput v1, v0, Lmiuix/animation/physics/SpringAnimation;->mValue:F

    goto/32 :goto_3d

    nop

    :goto_28
    iget v1, v1, Lmiuix/animation/physics/DynamicAnimation$MassState;->mVelocity:F

    goto/32 :goto_4b

    nop

    :goto_29
    return v2

    :goto_2a
    goto/32 :goto_f

    nop

    :goto_2b
    iput v1, v0, Lmiuix/animation/physics/SpringAnimation;->mValue:F

    goto/32 :goto_1f

    nop

    :goto_2c
    iput v5, v0, Lmiuix/animation/physics/SpringAnimation;->mPendingPosition:F

    :goto_2d
    goto/32 :goto_b

    nop

    :goto_2e
    return v2

    :goto_2f
    goto/32 :goto_35

    nop

    :goto_30
    iget-object v13, v0, Lmiuix/animation/physics/SpringAnimation;->mSpring:Lmiuix/animation/physics/SpringForce;

    goto/32 :goto_8

    nop

    :goto_31
    if-nez v1, :cond_2

    goto/32 :goto_2a

    :cond_2
    goto/32 :goto_22

    nop

    :goto_32
    invoke-static {v1, v5}, Ljava/lang/Math;->max(FF)F

    move-result v1

    goto/32 :goto_24

    nop

    :goto_33
    iput v1, v0, Lmiuix/animation/physics/SpringAnimation;->mValue:F

    goto/32 :goto_3a

    nop

    :goto_34
    iget v1, v0, Lmiuix/animation/physics/SpringAnimation;->mValue:F

    goto/32 :goto_a

    nop

    :goto_35
    iget v1, v0, Lmiuix/animation/physics/SpringAnimation;->mPendingPosition:F

    goto/32 :goto_2

    nop

    :goto_36
    iget v1, v1, Lmiuix/animation/physics/DynamicAnimation$MassState;->mVelocity:F

    goto/32 :goto_50

    nop

    :goto_37
    move-wide/from16 v18, p1

    goto/32 :goto_39

    nop

    :goto_38
    iget v5, v1, Lmiuix/animation/physics/DynamicAnimation$MassState;->mValue:F

    goto/32 :goto_0

    nop

    :goto_39
    invoke-virtual/range {v13 .. v19}, Lmiuix/animation/physics/SpringForce;->updateValues(DDJ)Lmiuix/animation/physics/DynamicAnimation$MassState;

    move-result-object v1

    goto/32 :goto_13

    nop

    :goto_3a
    iget v1, v0, Lmiuix/animation/physics/SpringAnimation;->mValue:F

    goto/32 :goto_4e

    nop

    :goto_3b
    invoke-virtual {v6, v1}, Lmiuix/animation/physics/SpringForce;->setFinalPosition(F)Lmiuix/animation/physics/SpringForce;

    goto/32 :goto_2c

    nop

    :goto_3c
    move-wide/from16 v16, v5

    goto/32 :goto_37

    nop

    :goto_3d
    iput v4, v0, Lmiuix/animation/physics/SpringAnimation;->mVelocity:F

    goto/32 :goto_41

    nop

    :goto_3e
    iget v7, v0, Lmiuix/animation/physics/SpringAnimation;->mPendingPosition:F

    goto/32 :goto_1a

    nop

    :goto_3f
    move-wide/from16 v11, v18

    goto/32 :goto_21

    nop

    :goto_40
    iget v5, v1, Lmiuix/animation/physics/DynamicAnimation$MassState;->mValue:F

    goto/32 :goto_1c

    nop

    :goto_41
    iput-boolean v3, v0, Lmiuix/animation/physics/SpringAnimation;->mEndRequested:Z

    goto/32 :goto_2e

    nop

    :goto_42
    div-long v18, p1, v11

    goto/32 :goto_3f

    nop

    :goto_43
    const/4 v3, 0x0

    goto/32 :goto_1

    nop

    :goto_44
    if-nez v1, :cond_3

    goto/32 :goto_19

    :cond_3
    goto/32 :goto_1b

    nop

    :goto_45
    float-to-double v14, v1

    goto/32 :goto_c

    nop

    :goto_46
    float-to-double v9, v1

    goto/32 :goto_16

    nop

    :goto_47
    float-to-double v5, v1

    goto/32 :goto_4a

    nop

    :goto_48
    float-to-double v7, v1

    goto/32 :goto_6

    nop

    :goto_49
    iget-object v6, v0, Lmiuix/animation/physics/SpringAnimation;->mSpring:Lmiuix/animation/physics/SpringForce;

    goto/32 :goto_3e

    nop

    :goto_4a
    move-wide/from16 v16, v5

    goto/32 :goto_11

    nop

    :goto_4b
    iput v1, v0, Lmiuix/animation/physics/SpringAnimation;->mVelocity:F

    :goto_4c
    goto/32 :goto_34

    nop

    :goto_4d
    iget v1, v0, Lmiuix/animation/physics/SpringAnimation;->mValue:F

    goto/32 :goto_48

    nop

    :goto_4e
    iget v5, v0, Lmiuix/animation/physics/SpringAnimation;->mVelocity:F

    goto/32 :goto_1d

    nop

    :goto_4f
    invoke-virtual {v1}, Lmiuix/animation/physics/SpringForce;->getFinalPosition()F

    move-result v1

    goto/32 :goto_27

    nop

    :goto_50
    iput v1, v0, Lmiuix/animation/physics/SpringAnimation;->mVelocity:F

    goto/32 :goto_18

    nop

    :goto_51
    invoke-virtual {v1}, Lmiuix/animation/physics/SpringForce;->getFinalPosition()F

    move-result v1

    goto/32 :goto_2b

    nop

    :goto_52
    iget-object v13, v0, Lmiuix/animation/physics/SpringAnimation;->mSpring:Lmiuix/animation/physics/SpringForce;

    goto/32 :goto_38

    nop

    :goto_53
    iget-object v6, v0, Lmiuix/animation/physics/SpringAnimation;->mSpring:Lmiuix/animation/physics/SpringForce;

    goto/32 :goto_3b

    nop
.end method
