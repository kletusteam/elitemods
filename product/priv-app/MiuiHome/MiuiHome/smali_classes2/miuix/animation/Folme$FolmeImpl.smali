.class Lmiuix/animation/Folme$FolmeImpl;
.super Ljava/lang/Object;

# interfaces
.implements Lmiuix/animation/IFolme;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiuix/animation/Folme;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "FolmeImpl"
.end annotation


# instance fields
.field private mHover:Lmiuix/animation/IHoverStyle;

.field private mState:Lmiuix/animation/IStateStyle;

.field private mTargets:[Lmiuix/animation/IAnimTarget;

.field private mTouch:Lmiuix/animation/ITouchStyle;

.field private mVisible:Lmiuix/animation/IVisibleStyle;


# direct methods
.method private varargs constructor <init>([Lmiuix/animation/IAnimTarget;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmiuix/animation/Folme$FolmeImpl;->mTargets:[Lmiuix/animation/IAnimTarget;

    const/4 p1, 0x0

    invoke-static {p1}, Lmiuix/animation/Folme;->access$000(Z)V

    invoke-static {}, Lmiuix/animation/Folme;->access$100()V

    return-void
.end method

.method synthetic constructor <init>([Lmiuix/animation/IAnimTarget;Lmiuix/animation/Folme$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lmiuix/animation/Folme$FolmeImpl;-><init>([Lmiuix/animation/IAnimTarget;)V

    return-void
.end method


# virtual methods
.method clean()V
    .locals 1

    goto/32 :goto_4

    nop

    :goto_0
    iget-object v0, p0, Lmiuix/animation/Folme$FolmeImpl;->mState:Lmiuix/animation/IStateStyle;

    goto/32 :goto_f

    nop

    :goto_1
    invoke-interface {v0}, Lmiuix/animation/IStateStyle;->clean()V

    :goto_2
    goto/32 :goto_8

    nop

    :goto_3
    if-nez v0, :cond_0

    goto/32 :goto_e

    :cond_0
    goto/32 :goto_d

    nop

    :goto_4
    iget-object v0, p0, Lmiuix/animation/Folme$FolmeImpl;->mTouch:Lmiuix/animation/ITouchStyle;

    goto/32 :goto_3

    nop

    :goto_5
    invoke-interface {v0}, Lmiuix/animation/IHoverStyle;->clean()V

    :goto_6
    goto/32 :goto_9

    nop

    :goto_7
    if-nez v0, :cond_1

    goto/32 :goto_b

    :cond_1
    goto/32 :goto_a

    nop

    :goto_8
    iget-object v0, p0, Lmiuix/animation/Folme$FolmeImpl;->mHover:Lmiuix/animation/IHoverStyle;

    goto/32 :goto_10

    nop

    :goto_9
    return-void

    :goto_a
    invoke-interface {v0}, Lmiuix/animation/IVisibleStyle;->clean()V

    :goto_b
    goto/32 :goto_0

    nop

    :goto_c
    iget-object v0, p0, Lmiuix/animation/Folme$FolmeImpl;->mVisible:Lmiuix/animation/IVisibleStyle;

    goto/32 :goto_7

    nop

    :goto_d
    invoke-interface {v0}, Lmiuix/animation/ITouchStyle;->clean()V

    :goto_e
    goto/32 :goto_c

    nop

    :goto_f
    if-nez v0, :cond_2

    goto/32 :goto_2

    :cond_2
    goto/32 :goto_1

    nop

    :goto_10
    if-nez v0, :cond_3

    goto/32 :goto_6

    :cond_3
    goto/32 :goto_5

    nop
.end method

.method end()V
    .locals 3

    goto/32 :goto_f

    nop

    :goto_0
    if-nez v0, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_10

    nop

    :goto_1
    invoke-interface {v0, v2}, Lmiuix/animation/IStateStyle;->end([Ljava/lang/Object;)V

    :goto_2
    goto/32 :goto_15

    nop

    :goto_3
    const/4 v1, 0x0

    goto/32 :goto_0

    nop

    :goto_4
    iget-object v0, p0, Lmiuix/animation/Folme$FolmeImpl;->mState:Lmiuix/animation/IStateStyle;

    goto/32 :goto_7

    nop

    :goto_5
    invoke-interface {v0, v2}, Lmiuix/animation/ITouchStyle;->end([Ljava/lang/Object;)V

    :goto_6
    goto/32 :goto_9

    nop

    :goto_7
    if-nez v0, :cond_1

    goto/32 :goto_2

    :cond_1
    goto/32 :goto_8

    nop

    :goto_8
    new-array v2, v1, [Ljava/lang/Object;

    goto/32 :goto_1

    nop

    :goto_9
    iget-object v0, p0, Lmiuix/animation/Folme$FolmeImpl;->mVisible:Lmiuix/animation/IVisibleStyle;

    goto/32 :goto_a

    nop

    :goto_a
    if-nez v0, :cond_2

    goto/32 :goto_12

    :cond_2
    goto/32 :goto_c

    nop

    :goto_b
    new-array v1, v1, [Ljava/lang/Object;

    goto/32 :goto_13

    nop

    :goto_c
    new-array v2, v1, [Ljava/lang/Object;

    goto/32 :goto_11

    nop

    :goto_d
    return-void

    :goto_e
    if-nez v0, :cond_3

    goto/32 :goto_14

    :cond_3
    goto/32 :goto_b

    nop

    :goto_f
    iget-object v0, p0, Lmiuix/animation/Folme$FolmeImpl;->mTouch:Lmiuix/animation/ITouchStyle;

    goto/32 :goto_3

    nop

    :goto_10
    new-array v2, v1, [Ljava/lang/Object;

    goto/32 :goto_5

    nop

    :goto_11
    invoke-interface {v0, v2}, Lmiuix/animation/IVisibleStyle;->end([Ljava/lang/Object;)V

    :goto_12
    goto/32 :goto_4

    nop

    :goto_13
    invoke-interface {v0, v1}, Lmiuix/animation/IHoverStyle;->end([Ljava/lang/Object;)V

    :goto_14
    goto/32 :goto_d

    nop

    :goto_15
    iget-object v0, p0, Lmiuix/animation/Folme$FolmeImpl;->mHover:Lmiuix/animation/IHoverStyle;

    goto/32 :goto_e

    nop
.end method

.method public hover()Lmiuix/animation/IHoverStyle;
    .locals 2

    iget-object v0, p0, Lmiuix/animation/Folme$FolmeImpl;->mHover:Lmiuix/animation/IHoverStyle;

    if-nez v0, :cond_0

    new-instance v0, Lmiuix/animation/controller/FolmeHover;

    iget-object v1, p0, Lmiuix/animation/Folme$FolmeImpl;->mTargets:[Lmiuix/animation/IAnimTarget;

    invoke-direct {v0, v1}, Lmiuix/animation/controller/FolmeHover;-><init>([Lmiuix/animation/IAnimTarget;)V

    iput-object v0, p0, Lmiuix/animation/Folme$FolmeImpl;->mHover:Lmiuix/animation/IHoverStyle;

    :cond_0
    iget-object v0, p0, Lmiuix/animation/Folme$FolmeImpl;->mHover:Lmiuix/animation/IHoverStyle;

    return-object v0
.end method

.method public state()Lmiuix/animation/IStateStyle;
    .locals 1

    iget-object v0, p0, Lmiuix/animation/Folme$FolmeImpl;->mState:Lmiuix/animation/IStateStyle;

    if-nez v0, :cond_0

    iget-object v0, p0, Lmiuix/animation/Folme$FolmeImpl;->mTargets:[Lmiuix/animation/IAnimTarget;

    invoke-static {v0}, Lmiuix/animation/controller/StateComposer;->composeStyle([Lmiuix/animation/IAnimTarget;)Lmiuix/animation/controller/IFolmeStateStyle;

    move-result-object v0

    iput-object v0, p0, Lmiuix/animation/Folme$FolmeImpl;->mState:Lmiuix/animation/IStateStyle;

    :cond_0
    iget-object v0, p0, Lmiuix/animation/Folme$FolmeImpl;->mState:Lmiuix/animation/IStateStyle;

    return-object v0
.end method

.method public touch()Lmiuix/animation/ITouchStyle;
    .locals 2

    iget-object v0, p0, Lmiuix/animation/Folme$FolmeImpl;->mTouch:Lmiuix/animation/ITouchStyle;

    if-nez v0, :cond_0

    new-instance v0, Lmiuix/animation/controller/FolmeTouch;

    iget-object v1, p0, Lmiuix/animation/Folme$FolmeImpl;->mTargets:[Lmiuix/animation/IAnimTarget;

    invoke-direct {v0, v1}, Lmiuix/animation/controller/FolmeTouch;-><init>([Lmiuix/animation/IAnimTarget;)V

    new-instance v1, Lmiuix/animation/controller/FolmeFont;

    invoke-direct {v1}, Lmiuix/animation/controller/FolmeFont;-><init>()V

    invoke-virtual {v0, v1}, Lmiuix/animation/controller/FolmeTouch;->setFontStyle(Lmiuix/animation/controller/FolmeFont;)V

    iput-object v0, p0, Lmiuix/animation/Folme$FolmeImpl;->mTouch:Lmiuix/animation/ITouchStyle;

    :cond_0
    iget-object v0, p0, Lmiuix/animation/Folme$FolmeImpl;->mTouch:Lmiuix/animation/ITouchStyle;

    return-object v0
.end method

.method public visible()Lmiuix/animation/IVisibleStyle;
    .locals 2

    iget-object v0, p0, Lmiuix/animation/Folme$FolmeImpl;->mVisible:Lmiuix/animation/IVisibleStyle;

    if-nez v0, :cond_0

    new-instance v0, Lmiuix/animation/controller/FolmeVisible;

    iget-object v1, p0, Lmiuix/animation/Folme$FolmeImpl;->mTargets:[Lmiuix/animation/IAnimTarget;

    invoke-direct {v0, v1}, Lmiuix/animation/controller/FolmeVisible;-><init>([Lmiuix/animation/IAnimTarget;)V

    iput-object v0, p0, Lmiuix/animation/Folme$FolmeImpl;->mVisible:Lmiuix/animation/IVisibleStyle;

    :cond_0
    iget-object v0, p0, Lmiuix/animation/Folme$FolmeImpl;->mVisible:Lmiuix/animation/IVisibleStyle;

    return-object v0
.end method
