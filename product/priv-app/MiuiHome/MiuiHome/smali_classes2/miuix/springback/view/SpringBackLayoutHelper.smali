.class public Lmiuix/springback/view/SpringBackLayoutHelper;
.super Ljava/lang/Object;


# instance fields
.field mActivePointerId:I

.field mInitialDownX:F

.field mInitialDownY:F

.field mScrollOrientation:I

.field private mTarget:Landroid/view/ViewGroup;

.field mTargetScrollOrientation:I

.field private mTouchSlop:I


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lmiuix/springback/view/SpringBackLayoutHelper;->mActivePointerId:I

    iput-object p1, p0, Lmiuix/springback/view/SpringBackLayoutHelper;->mTarget:Landroid/view/ViewGroup;

    iput p2, p0, Lmiuix/springback/view/SpringBackLayoutHelper;->mTargetScrollOrientation:I

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result p1

    iput p1, p0, Lmiuix/springback/view/SpringBackLayoutHelper;->mTouchSlop:I

    return-void
.end method


# virtual methods
.method checkOrientation(Landroid/view/MotionEvent;)V
    .locals 3

    goto/32 :goto_c

    nop

    :goto_0
    sub-float/2addr p1, v0

    goto/32 :goto_19

    nop

    :goto_1
    iput v2, p0, Lmiuix/springback/view/SpringBackLayoutHelper;->mInitialDownY:F

    goto/32 :goto_20

    nop

    :goto_2
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result p1

    goto/32 :goto_1a

    nop

    :goto_3
    iput v1, p0, Lmiuix/springback/view/SpringBackLayoutHelper;->mScrollOrientation:I

    :goto_4
    goto/32 :goto_30

    nop

    :goto_5
    goto/16 :goto_36

    :goto_6
    goto/32 :goto_35

    nop

    :goto_7
    iget v0, p0, Lmiuix/springback/view/SpringBackLayoutHelper;->mInitialDownX:F

    goto/32 :goto_0

    nop

    :goto_8
    if-gtz p1, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_26

    nop

    :goto_9
    iget v0, p0, Lmiuix/springback/view/SpringBackLayoutHelper;->mActivePointerId:I

    goto/32 :goto_a

    nop

    :goto_a
    const/4 v1, -0x1

    goto/32 :goto_16

    nop

    :goto_b
    if-ltz v0, :cond_1

    goto/32 :goto_1e

    :cond_1
    goto/32 :goto_1d

    nop

    :goto_c
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    goto/32 :goto_f

    nop

    :goto_d
    cmpl-float p1, p1, v0

    goto/32 :goto_8

    nop

    :goto_e
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    goto/32 :goto_14

    nop

    :goto_f
    const/4 v1, 0x0

    packed-switch v0, :pswitch_data_0

    goto/32 :goto_22

    nop

    :goto_10
    iget-object p1, p0, Lmiuix/springback/view/SpringBackLayoutHelper;->mTarget:Landroid/view/ViewGroup;

    goto/32 :goto_23

    nop

    :goto_11
    iget v0, p0, Lmiuix/springback/view/SpringBackLayoutHelper;->mInitialDownY:F

    goto/32 :goto_34

    nop

    :goto_12
    if-lez v0, :cond_2

    goto/32 :goto_28

    :cond_2
    goto/32 :goto_2c

    nop

    :goto_13
    iput p1, p0, Lmiuix/springback/view/SpringBackLayoutHelper;->mInitialDownX:F

    goto/32 :goto_3

    nop

    :goto_14
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result p1

    goto/32 :goto_11

    nop

    :goto_15
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v0

    goto/32 :goto_32

    nop

    :goto_16
    if-eq v0, v1, :cond_3

    goto/32 :goto_2b

    :cond_3
    goto/32 :goto_2a

    nop

    :goto_17
    iget v2, p0, Lmiuix/springback/view/SpringBackLayoutHelper;->mTouchSlop:I

    goto/32 :goto_31

    nop

    :goto_18
    cmpl-float v0, v0, v2

    goto/32 :goto_12

    nop

    :goto_19
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v0

    goto/32 :goto_33

    nop

    :goto_1a
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v0

    goto/32 :goto_d

    nop

    :goto_1b
    return-void

    :goto_1c
    goto/32 :goto_e

    nop

    :goto_1d
    return-void

    :goto_1e
    goto/32 :goto_1f

    nop

    :goto_1f
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    goto/32 :goto_1

    nop

    :goto_20
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result p1

    goto/32 :goto_13

    nop

    :goto_21
    goto/16 :goto_4

    :pswitch_0
    goto/32 :goto_38

    nop

    :goto_22
    goto/16 :goto_4

    :pswitch_1
    goto/32 :goto_9

    nop

    :goto_23
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->requestDisallowInterceptTouchEvent(Z)V

    goto/32 :goto_2e

    nop

    :goto_24
    iget v0, p0, Lmiuix/springback/view/SpringBackLayoutHelper;->mActivePointerId:I

    goto/32 :goto_29

    nop

    :goto_25
    iput p1, p0, Lmiuix/springback/view/SpringBackLayoutHelper;->mScrollOrientation:I

    goto/32 :goto_21

    nop

    :goto_26
    const/4 p1, 0x1

    goto/32 :goto_5

    nop

    :goto_27
    if-gtz v0, :cond_4

    goto/32 :goto_4

    :cond_4
    :goto_28
    goto/32 :goto_2

    nop

    :goto_29
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v0

    goto/32 :goto_b

    nop

    :goto_2a
    return-void

    :goto_2b
    goto/32 :goto_15

    nop

    :goto_2c
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v0

    goto/32 :goto_17

    nop

    :goto_2d
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    goto/32 :goto_39

    nop

    :goto_2e
    goto/16 :goto_4

    :pswitch_2
    goto/32 :goto_2d

    nop

    :goto_2f
    int-to-float v2, v2

    goto/32 :goto_18

    nop

    :goto_30
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :goto_31
    int-to-float v2, v2

    goto/32 :goto_37

    nop

    :goto_32
    if-ltz v0, :cond_5

    goto/32 :goto_1c

    :cond_5
    goto/32 :goto_1b

    nop

    :goto_33
    iget v2, p0, Lmiuix/springback/view/SpringBackLayoutHelper;->mTouchSlop:I

    goto/32 :goto_2f

    nop

    :goto_34
    sub-float/2addr v1, v0

    goto/32 :goto_7

    nop

    :goto_35
    const/4 p1, 0x2

    :goto_36
    goto/32 :goto_25

    nop

    :goto_37
    cmpl-float v0, v0, v2

    goto/32 :goto_27

    nop

    :goto_38
    iput v1, p0, Lmiuix/springback/view/SpringBackLayoutHelper;->mScrollOrientation:I

    goto/32 :goto_10

    nop

    :goto_39
    iput v0, p0, Lmiuix/springback/view/SpringBackLayoutHelper;->mActivePointerId:I

    goto/32 :goto_24

    nop
.end method

.method public isTouchInTarget(Landroid/view/MotionEvent;)Z
    .locals 6

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v1

    if-ltz v1, :cond_0

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result p1

    const/4 v1, 0x2

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    iget-object v3, p0, Lmiuix/springback/view/SpringBackLayoutHelper;->mTarget:Landroid/view/ViewGroup;

    invoke-virtual {v3, v1}, Landroid/view/ViewGroup;->getLocationInWindow([I)V

    aget v0, v1, v0

    const/4 v3, 0x1

    aget v1, v1, v3

    iget-object v3, p0, Lmiuix/springback/view/SpringBackLayoutHelper;->mTarget:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getHeight()I

    move-result v3

    add-int/2addr v3, v1

    iget-object v4, p0, Lmiuix/springback/view/SpringBackLayoutHelper;->mTarget:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getWidth()I

    move-result v4

    add-int/2addr v4, v0

    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5, v0, v1, v4, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    float-to-int p1, p1

    float-to-int v0, v2

    invoke-virtual {v5, p1, v0}, Landroid/graphics/Rect;->contains(II)Z

    move-result p1

    return p1

    :cond_0
    return v0

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method
