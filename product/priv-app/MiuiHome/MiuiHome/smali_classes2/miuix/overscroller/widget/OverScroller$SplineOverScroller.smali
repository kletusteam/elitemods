.class Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiuix/overscroller/widget/OverScroller;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "SplineOverScroller"
.end annotation


# static fields
.field private static DECELERATION_RATE:F

.field private static final SPLINE_POSITION:[F

.field private static final SPLINE_TIME:[F


# instance fields
.field private mContext:Landroid/content/Context;

.field private mCurrVelocity:D

.field private mCurrentPosition:D

.field private mDeceleration:F

.field private mDuration:I

.field private mFinal:D

.field private mFinished:Z

.field private mFlingFriction:F

.field private mLastStep:Z

.field private mOriginStart:D

.field private mOver:I

.field private mPhysicalCoeff:F

.field private mSplineDistance:I

.field private mSplineDuration:I

.field private mSpringOperator:Lmiuix/animation/physics/SpringOperator;

.field private mSpringParams:[D

.field private mStart:D

.field private mStartTime:J

.field private mState:I

.field private mVelocity:D


# direct methods
.method static constructor <clinit>()V
    .locals 19

    const-wide v0, 0x3fe8f5c28f5c28f6L    # 0.78

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    const-wide v2, 0x3feccccccccccccdL    # 0.9

    invoke-static {v2, v3}, Ljava/lang/Math;->log(D)D

    move-result-wide v2

    div-double/2addr v0, v2

    double-to-float v0, v0

    sput v0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->DECELERATION_RATE:F

    const/16 v0, 0x65

    new-array v1, v0, [F

    sput-object v1, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->SPLINE_POSITION:[F

    new-array v0, v0, [F

    sput-object v0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->SPLINE_TIME:[F

    const/4 v0, 0x0

    const/4 v1, 0x0

    move v2, v0

    :goto_0
    const/16 v3, 0x64

    const/high16 v4, 0x3f800000    # 1.0f

    if-ge v1, v3, :cond_4

    int-to-float v3, v1

    const/high16 v5, 0x42c80000    # 100.0f

    div-float v5, v3, v5

    move v3, v4

    :goto_1
    sub-float v6, v3, v0

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    add-float/2addr v6, v0

    const/high16 v8, 0x40400000    # 3.0f

    mul-float v9, v6, v8

    sub-float v10, v4, v6

    mul-float/2addr v9, v10

    const v11, 0x3e333333    # 0.175f

    mul-float v12, v10, v11

    const v13, 0x3eb33334    # 0.35000002f

    mul-float v14, v6, v13

    add-float/2addr v12, v14

    mul-float/2addr v12, v9

    mul-float v14, v6, v6

    mul-float/2addr v14, v6

    add-float/2addr v12, v14

    sub-float v15, v12, v5

    invoke-static {v15}, Ljava/lang/Math;->abs(F)F

    move-result v15

    move/from16 v16, v12

    float-to-double v11, v15

    const-wide v17, 0x3ee4f8b588e368f1L    # 1.0E-5

    cmpg-double v11, v11, v17

    if-gez v11, :cond_2

    sget-object v3, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->SPLINE_POSITION:[F

    const/high16 v11, 0x3f000000    # 0.5f

    mul-float/2addr v10, v11

    add-float/2addr v10, v6

    mul-float/2addr v9, v10

    add-float/2addr v9, v14

    aput v9, v3, v1

    move v3, v4

    :goto_2
    sub-float v6, v3, v2

    div-float/2addr v6, v7

    add-float/2addr v6, v2

    mul-float v9, v6, v8

    sub-float v10, v4, v6

    mul-float/2addr v9, v10

    mul-float v12, v10, v11

    add-float/2addr v12, v6

    mul-float/2addr v12, v9

    mul-float v14, v6, v6

    mul-float/2addr v14, v6

    add-float/2addr v12, v14

    sub-float v15, v12, v5

    invoke-static {v15}, Ljava/lang/Math;->abs(F)F

    move-result v15

    float-to-double v7, v15

    cmpg-double v7, v7, v17

    if-gez v7, :cond_0

    sget-object v3, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->SPLINE_TIME:[F

    const v7, 0x3e333333    # 0.175f

    mul-float/2addr v10, v7

    mul-float/2addr v6, v13

    add-float/2addr v10, v6

    mul-float/2addr v9, v10

    add-float/2addr v9, v14

    aput v9, v3, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    const v7, 0x3e333333    # 0.175f

    cmpl-float v8, v12, v5

    if-lez v8, :cond_1

    move v3, v6

    const/high16 v7, 0x40000000    # 2.0f

    const/high16 v8, 0x40400000    # 3.0f

    goto :goto_2

    :cond_1
    move v2, v6

    const/high16 v7, 0x40000000    # 2.0f

    const/high16 v8, 0x40400000    # 3.0f

    goto :goto_2

    :cond_2
    cmpl-float v7, v16, v5

    if-lez v7, :cond_3

    move v3, v6

    goto :goto_1

    :cond_3
    move v0, v6

    goto :goto_1

    :cond_4
    sget-object v0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->SPLINE_POSITION:[F

    sget-object v1, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->SPLINE_TIME:[F

    aput v4, v1, v3

    aput v4, v0, v3

    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Landroid/view/ViewConfiguration;->getScrollFriction()F

    move-result v0

    iput v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mFlingFriction:F

    const/4 v0, 0x0

    iput v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mState:I

    iput-object p1, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mContext:Landroid/content/Context;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mFinished:Z

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p1

    iget p1, p1, Landroid/util/DisplayMetrics;->density:F

    const/high16 v0, 0x43200000    # 160.0f

    mul-float/2addr p1, v0

    const v0, 0x43c10b3d

    mul-float/2addr p1, v0

    const v0, 0x3f570a3d    # 0.84f

    mul-float/2addr p1, v0

    iput p1, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mPhysicalCoeff:F

    return-void
.end method

.method static synthetic access$000(Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;)Z
    .locals 0

    iget-boolean p0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mFinished:Z

    return p0
.end method

.method static synthetic access$100(Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;)D
    .locals 2

    iget-wide v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mCurrentPosition:D

    return-wide v0
.end method

.method static synthetic access$200(Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;)D
    .locals 2

    iget-wide v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mCurrVelocity:D

    return-wide v0
.end method

.method static synthetic access$300(Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;)D
    .locals 2

    iget-wide v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mStart:D

    return-wide v0
.end method

.method static synthetic access$400(Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;)D
    .locals 2

    iget-wide v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mFinal:D

    return-wide v0
.end method

.method static synthetic access$500(Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;)I
    .locals 0

    iget p0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mDuration:I

    return p0
.end method

.method static synthetic access$600(Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;)J
    .locals 2

    iget-wide v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mStartTime:J

    return-wide v0
.end method

.method private adjustDuration(III)V
    .locals 3

    sub-int/2addr p2, p1

    sub-int/2addr p3, p1

    int-to-float p1, p3

    int-to-float p2, p2

    div-float/2addr p1, p2

    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result p1

    const/high16 p2, 0x42c80000    # 100.0f

    mul-float p3, p1, p2

    float-to-int p3, p3

    const/16 v0, 0x64

    if-ge p3, v0, :cond_0

    int-to-float v0, p3

    div-float/2addr v0, p2

    add-int/lit8 v1, p3, 0x1

    int-to-float v2, v1

    div-float/2addr v2, p2

    sget-object p2, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->SPLINE_TIME:[F

    aget p3, p2, p3

    aget p2, p2, v1

    sub-float/2addr p1, v0

    sub-float/2addr v2, v0

    div-float/2addr p1, v2

    sub-float/2addr p2, p3

    mul-float/2addr p1, p2

    add-float/2addr p3, p1

    iget p1, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mDuration:I

    int-to-float p1, p1

    mul-float/2addr p1, p3

    float-to-int p1, p1

    iput p1, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mDuration:I

    :cond_0
    return-void
.end method

.method private fitOnBounceCurve(III)V
    .locals 5

    neg-int v0, p3

    int-to-float v0, v0

    iget v1, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mDeceleration:F

    div-float/2addr v0, v1

    int-to-float p3, p3

    mul-float/2addr p3, p3

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr p3, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    div-float/2addr p3, v1

    sub-int p1, p2, p1

    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result p1

    int-to-float p1, p1

    add-float/2addr p3, p1

    float-to-double v1, p3

    const-wide/high16 v3, 0x4000000000000000L    # 2.0

    mul-double/2addr v1, v3

    iget p1, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mDeceleration:F

    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result p1

    float-to-double v3, p1

    div-double/2addr v1, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v1

    double-to-float p1, v1

    iget-wide v1, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mStartTime:J

    sub-float p3, p1, v0

    const/high16 v0, 0x447a0000    # 1000.0f

    mul-float/2addr p3, v0

    float-to-int p3, p3

    int-to-long v3, p3

    sub-long/2addr v1, v3

    iput-wide v1, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mStartTime:J

    int-to-double p2, p2

    iput-wide p2, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mStart:D

    iput-wide p2, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mCurrentPosition:D

    iget p2, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mDeceleration:F

    neg-float p2, p2

    mul-float/2addr p2, p1

    float-to-int p1, p2

    int-to-double p1, p1

    iput-wide p1, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mVelocity:D

    return-void
.end method

.method private static getDeceleration(I)F
    .locals 0

    if-lez p0, :cond_0

    const/high16 p0, -0x3b060000    # -2000.0f

    goto :goto_0

    :cond_0
    const/high16 p0, 0x44fa0000    # 2000.0f

    :goto_0
    return p0
.end method

.method private getSplineDeceleration(I)D
    .locals 2

    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result p1

    int-to-float p1, p1

    const v0, 0x3eb33333    # 0.35f

    mul-float/2addr p1, v0

    iget v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mFlingFriction:F

    iget v1, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mPhysicalCoeff:F

    mul-float/2addr v0, v1

    div-float/2addr p1, v0

    float-to-double v0, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    return-wide v0
.end method

.method private getSplineFlingDistance(I)D
    .locals 8

    invoke-direct {p0, p1}, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->getSplineDeceleration(I)D

    move-result-wide v0

    sget p1, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->DECELERATION_RATE:F

    float-to-double v2, p1

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v2, v4

    iget v4, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mFlingFriction:F

    iget v5, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mPhysicalCoeff:F

    mul-float/2addr v4, v5

    float-to-double v4, v4

    float-to-double v6, p1

    div-double/2addr v6, v2

    mul-double/2addr v6, v0

    invoke-static {v6, v7}, Ljava/lang/Math;->exp(D)D

    move-result-wide v0

    mul-double/2addr v4, v0

    return-wide v4
.end method

.method private getSplineFlingDuration(I)I
    .locals 6

    invoke-direct {p0, p1}, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->getSplineDeceleration(I)D

    move-result-wide v0

    sget p1, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->DECELERATION_RATE:F

    float-to-double v2, p1

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v2, v4

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->exp(D)D

    move-result-wide v0

    const-wide v2, 0x408f400000000000L    # 1000.0

    mul-double/2addr v0, v2

    double-to-int p1, v0

    return p1
.end method

.method private onEdgeReached()V
    .locals 7

    iget-wide v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mVelocity:D

    double-to-float v2, v0

    double-to-float v0, v0

    mul-float/2addr v2, v0

    iget v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mDeceleration:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    mul-float/2addr v0, v1

    div-float v0, v2, v0

    iget-wide v3, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mVelocity:D

    double-to-float v3, v3

    invoke-static {v3}, Ljava/lang/Math;->signum(F)F

    move-result v3

    iget v4, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mOver:I

    int-to-float v5, v4

    cmpl-float v5, v0, v5

    if-lez v5, :cond_0

    neg-float v0, v3

    mul-float/2addr v0, v2

    int-to-float v2, v4

    mul-float/2addr v2, v1

    div-float/2addr v0, v2

    iput v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mDeceleration:F

    int-to-float v0, v4

    :cond_0
    float-to-int v1, v0

    iput v1, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mOver:I

    const/4 v1, 0x2

    iput v1, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mState:I

    iget-wide v1, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mStart:D

    iget-wide v3, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mVelocity:D

    const-wide/16 v5, 0x0

    cmpl-double v3, v3, v5

    if-lez v3, :cond_1

    goto :goto_0

    :cond_1
    neg-float v0, v0

    :goto_0
    float-to-int v0, v0

    int-to-double v3, v0

    add-double/2addr v1, v3

    iput-wide v1, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mFinal:D

    const-wide v0, 0x408f400000000000L    # 1000.0

    iget-wide v2, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mVelocity:D

    mul-double/2addr v2, v0

    iget v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mDeceleration:F

    float-to-double v0, v0

    div-double/2addr v2, v0

    double-to-int v0, v2

    neg-int v0, v0

    iput v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mDuration:I

    return-void
.end method

.method private startAfterEdge(IIII)V
    .locals 10

    const/4 v0, 0x1

    if-le p1, p2, :cond_0

    if-ge p1, p3, :cond_0

    const-string p1, "OverScroller"

    const-string p2, "startAfterEdge called from a valid position"

    invoke-static {p1, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mFinished:Z

    return-void

    :cond_0
    const/4 v1, 0x0

    if-le p1, p3, :cond_1

    move v2, v0

    goto :goto_0

    :cond_1
    move v2, v1

    :goto_0
    if-eqz v2, :cond_2

    move v3, p3

    goto :goto_1

    :cond_2
    move v3, p2

    :goto_1
    sub-int v4, p1, v3

    mul-int v5, v4, p4

    if-ltz v5, :cond_3

    goto :goto_2

    :cond_3
    move v0, v1

    :goto_2
    if-eqz v0, :cond_4

    invoke-direct {p0, p1, v3, p4}, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->startBounceAfterEdge(III)V

    goto :goto_5

    :cond_4
    invoke-direct {p0, p4}, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->getSplineFlingDistance(I)D

    move-result-wide v0

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    int-to-double v4, v4

    cmpl-double v0, v0, v4

    if-lez v0, :cond_7

    if-eqz v2, :cond_5

    move v7, p2

    goto :goto_3

    :cond_5
    move v7, p1

    :goto_3
    if-eqz v2, :cond_6

    move v8, p1

    goto :goto_4

    :cond_6
    move v8, p3

    :goto_4
    iget v9, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mOver:I

    move-object v4, p0

    move v5, p1

    move v6, p4

    invoke-virtual/range {v4 .. v9}, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->fling(IIIII)V

    goto :goto_5

    :cond_7
    invoke-direct {p0, p1, v3, p4}, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->startSpringback(III)V

    :goto_5
    return-void
.end method

.method private startBounceAfterEdge(III)V
    .locals 1

    if-nez p3, :cond_0

    sub-int v0, p1, p2

    goto :goto_0

    :cond_0
    move v0, p3

    :goto_0
    invoke-static {v0}, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->getDeceleration(I)F

    move-result v0

    iput v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mDeceleration:F

    invoke-direct {p0, p1, p2, p3}, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->fitOnBounceCurve(III)V

    invoke-direct {p0}, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->onEdgeReached()V

    return-void
.end method

.method private startSpringback(III)V
    .locals 2

    const/4 p3, 0x0

    iput-boolean p3, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mFinished:Z

    const/4 p3, 0x1

    iput p3, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mState:I

    int-to-double v0, p1

    iput-wide v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mStart:D

    iput-wide v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mCurrentPosition:D

    int-to-double v0, p2

    iput-wide v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mFinal:D

    sub-int/2addr p1, p2

    invoke-static {p1}, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->getDeceleration(I)F

    move-result p2

    iput p2, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mDeceleration:F

    neg-int p2, p1

    int-to-double p2, p2

    iput-wide p2, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mVelocity:D

    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result p2

    iput p2, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mOver:I

    int-to-double p1, p1

    const-wide/high16 v0, -0x4000000000000000L    # -2.0

    mul-double/2addr p1, v0

    iget p3, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mDeceleration:F

    float-to-double v0, p3

    div-double/2addr p1, v0

    invoke-static {p1, p2}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide p1

    const-wide v0, 0x408f400000000000L    # 1000.0

    mul-double/2addr p1, v0

    double-to-int p1, p1

    iput p1, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mDuration:I

    return-void
.end method


# virtual methods
.method computeScrollOffset()Z
    .locals 20

    goto/32 :goto_2

    nop

    :goto_0
    iget-wide v6, v0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mStartTime:J

    goto/32 :goto_18

    nop

    :goto_1
    iget-boolean v1, v0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mLastStep:Z

    goto/32 :goto_1f

    nop

    :goto_2
    move-object/from16 v0, p0

    goto/32 :goto_23

    nop

    :goto_3
    iput-wide v1, v0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mCurrVelocity:D

    goto/32 :goto_5

    nop

    :goto_4
    iget-boolean v1, v0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mFinished:Z

    goto/32 :goto_13

    nop

    :goto_5
    iget-wide v1, v0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mCurrentPosition:D

    goto/32 :goto_3b

    nop

    :goto_6
    div-float/2addr v1, v6

    goto/32 :goto_f

    nop

    :goto_7
    const/4 v1, 0x2

    goto/32 :goto_32

    nop

    :goto_8
    const-wide/16 v10, 0x0

    goto/32 :goto_e

    nop

    :goto_9
    const-wide v8, 0x3f90624de0000000L    # 0.01600000075995922

    goto/32 :goto_20

    nop

    :goto_a
    iput-wide v1, v0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mStart:D

    :goto_b
    goto/32 :goto_37

    nop

    :goto_c
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v4

    goto/32 :goto_0

    nop

    :goto_d
    iget-object v1, v0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mSpringParams:[D

    goto/32 :goto_10

    nop

    :goto_e
    cmpl-double v1, v6, v10

    goto/32 :goto_12

    nop

    :goto_f
    float-to-double v6, v1

    goto/32 :goto_9

    nop

    :goto_10
    aget-wide v13, v1, v2

    goto/32 :goto_35

    nop

    :goto_11
    move-wide/from16 v17, v6

    goto/32 :goto_21

    nop

    :goto_12
    if-eqz v1, :cond_0

    goto/32 :goto_2c

    :cond_0
    goto/32 :goto_2b

    nop

    :goto_13
    if-nez v1, :cond_1

    goto/32 :goto_27

    :cond_1
    goto/32 :goto_26

    nop

    :goto_14
    return v2

    :goto_15
    iput-boolean v3, v0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mFinished:Z

    goto/32 :goto_1a

    nop

    :goto_16
    long-to-float v1, v6

    goto/32 :goto_2d

    nop

    :goto_17
    iput-boolean v3, v0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mLastStep:Z

    goto/32 :goto_29

    nop

    :goto_18
    sub-long v6, v4, v6

    goto/32 :goto_16

    nop

    :goto_19
    invoke-virtual {v0, v1, v2, v4, v5}, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->isAtEquilibrium(DD)Z

    move-result v1

    goto/32 :goto_3c

    nop

    :goto_1a
    iget-wide v1, v0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mFinal:D

    goto/32 :goto_2e

    nop

    :goto_1b
    iget-wide v4, v0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mStart:D

    goto/32 :goto_3e

    nop

    :goto_1c
    if-nez v1, :cond_2

    goto/32 :goto_38

    :cond_2
    goto/32 :goto_4

    nop

    :goto_1d
    if-nez v1, :cond_3

    goto/32 :goto_31

    :cond_3
    goto/32 :goto_15

    nop

    :goto_1e
    iget-object v10, v0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mSpringOperator:Lmiuix/animation/physics/SpringOperator;

    goto/32 :goto_3d

    nop

    :goto_1f
    const/4 v3, 0x1

    goto/32 :goto_1d

    nop

    :goto_20
    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->min(DD)D

    move-result-wide v6

    goto/32 :goto_8

    nop

    :goto_21
    move-object/from16 v19, v1

    goto/32 :goto_34

    nop

    :goto_22
    iget-wide v4, v0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mStart:D

    goto/32 :goto_24

    nop

    :goto_23
    iget-object v1, v0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mSpringOperator:Lmiuix/animation/physics/SpringOperator;

    goto/32 :goto_3a

    nop

    :goto_24
    mul-double/2addr v6, v1

    goto/32 :goto_39

    nop

    :goto_25
    iget-wide v1, v0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mCurrentPosition:D

    goto/32 :goto_a

    nop

    :goto_26
    goto :goto_38

    :goto_27
    goto/32 :goto_1

    nop

    :goto_28
    iget-wide v4, v0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mFinal:D

    goto/32 :goto_33

    nop

    :goto_29
    goto/16 :goto_b

    :goto_2a
    goto/32 :goto_25

    nop

    :goto_2b
    move-wide v6, v8

    :goto_2c
    goto/32 :goto_2f

    nop

    :goto_2d
    const/high16 v6, 0x447a0000    # 1000.0f

    goto/32 :goto_6

    nop

    :goto_2e
    iput-wide v1, v0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mCurrentPosition:D

    goto/32 :goto_30

    nop

    :goto_2f
    iput-wide v4, v0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mStartTime:J

    goto/32 :goto_1e

    nop

    :goto_30
    return v3

    :goto_31
    goto/32 :goto_c

    nop

    :goto_32
    new-array v1, v1, [D

    goto/32 :goto_28

    nop

    :goto_33
    aput-wide v4, v1, v2

    goto/32 :goto_1b

    nop

    :goto_34
    invoke-virtual/range {v10 .. v19}, Lmiuix/animation/physics/SpringOperator;->updateVelocity(DDDD[D)D

    move-result-wide v1

    goto/32 :goto_22

    nop

    :goto_35
    aget-wide v15, v1, v3

    goto/32 :goto_7

    nop

    :goto_36
    iput-wide v4, v0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mCurrentPosition:D

    goto/32 :goto_3

    nop

    :goto_37
    return v3

    :goto_38
    goto/32 :goto_14

    nop

    :goto_39
    add-double/2addr v4, v6

    goto/32 :goto_36

    nop

    :goto_3a
    const/4 v2, 0x0

    goto/32 :goto_1c

    nop

    :goto_3b
    iget-wide v4, v0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mFinal:D

    goto/32 :goto_19

    nop

    :goto_3c
    if-nez v1, :cond_4

    goto/32 :goto_2a

    :cond_4
    goto/32 :goto_17

    nop

    :goto_3d
    iget-wide v11, v0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mCurrVelocity:D

    goto/32 :goto_d

    nop

    :goto_3e
    aput-wide v4, v1, v3

    goto/32 :goto_11

    nop
.end method

.method continueWhenFinished()Z
    .locals 6

    goto/32 :goto_3

    nop

    :goto_0
    iget v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mDuration:I

    goto/32 :goto_a

    nop

    :goto_1
    if-lt v0, v2, :cond_0

    goto/32 :goto_21

    :cond_0
    goto/32 :goto_e

    nop

    :goto_2
    iget-wide v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mStartTime:J

    goto/32 :goto_29

    nop

    :goto_3
    iget v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mState:I

    goto/32 :goto_28

    nop

    :goto_4
    int-to-long v4, v0

    goto/32 :goto_26

    nop

    :goto_5
    double-to-int v0, v2

    goto/32 :goto_f

    nop

    :goto_6
    double-to-int v0, v0

    goto/32 :goto_16

    nop

    :goto_7
    invoke-direct {p0, v0, v2, v1}, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->startSpringback(III)V

    goto/32 :goto_1e

    nop

    :goto_8
    iget v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mDuration:I

    goto/32 :goto_4

    nop

    :goto_9
    return v1

    :pswitch_0
    goto/32 :goto_0

    nop

    :goto_a
    iget v2, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mSplineDuration:I

    goto/32 :goto_1

    nop

    :goto_b
    iput-wide v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mCurrentPosition:D

    goto/32 :goto_1d

    nop

    :goto_c
    iput-wide v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mStart:D

    goto/32 :goto_b

    nop

    :goto_d
    int-to-long v2, v2

    goto/32 :goto_27

    nop

    :goto_e
    iget-wide v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mFinal:D

    goto/32 :goto_c

    nop

    :goto_f
    iget-wide v2, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mStart:D

    goto/32 :goto_10

    nop

    :goto_10
    double-to-int v2, v2

    goto/32 :goto_7

    nop

    :goto_11
    goto :goto_24

    :pswitch_1
    goto/32 :goto_1f

    nop

    :goto_12
    iput v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mDeceleration:F

    goto/32 :goto_2

    nop

    :goto_13
    const/4 v0, 0x1

    goto/32 :goto_18

    nop

    :goto_14
    iput-wide v2, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mStartTime:J

    goto/32 :goto_1b

    nop

    :goto_15
    invoke-static {v0}, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->getDeceleration(I)F

    move-result v0

    goto/32 :goto_12

    nop

    :goto_16
    int-to-double v0, v0

    goto/32 :goto_17

    nop

    :goto_17
    iput-wide v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mVelocity:D

    goto/32 :goto_1c

    nop

    :goto_18
    return v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch

    :goto_19
    iput-wide v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mStartTime:J

    goto/32 :goto_1a

    nop

    :goto_1a
    invoke-direct {p0}, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->onEdgeReached()V

    goto/32 :goto_20

    nop

    :goto_1b
    iget-wide v2, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mFinal:D

    goto/32 :goto_5

    nop

    :goto_1c
    iget-wide v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mVelocity:D

    goto/32 :goto_22

    nop

    :goto_1d
    iget-wide v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mCurrVelocity:D

    goto/32 :goto_6

    nop

    :goto_1e
    goto :goto_24

    :pswitch_2
    goto/32 :goto_9

    nop

    :goto_1f
    iget-wide v2, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mStartTime:J

    goto/32 :goto_8

    nop

    :goto_20
    goto :goto_24

    :goto_21
    goto/32 :goto_23

    nop

    :goto_22
    double-to-int v0, v0

    goto/32 :goto_15

    nop

    :goto_23
    return v1

    :goto_24
    goto/32 :goto_25

    nop

    :goto_25
    invoke-virtual {p0}, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->update()Z

    goto/32 :goto_13

    nop

    :goto_26
    add-long/2addr v2, v4

    goto/32 :goto_14

    nop

    :goto_27
    add-long/2addr v0, v2

    goto/32 :goto_19

    nop

    :goto_28
    const/4 v1, 0x0

    packed-switch v0, :pswitch_data_0

    goto/32 :goto_11

    nop

    :goto_29
    iget v2, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mDuration:I

    goto/32 :goto_d

    nop
.end method

.method finish()V
    .locals 2

    goto/32 :goto_0

    nop

    :goto_0
    iget-wide v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mFinal:D

    goto/32 :goto_1

    nop

    :goto_1
    iput-wide v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mCurrentPosition:D

    goto/32 :goto_2

    nop

    :goto_2
    const/4 v0, 0x1

    goto/32 :goto_4

    nop

    :goto_3
    return-void

    :goto_4
    iput-boolean v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mFinished:Z

    goto/32 :goto_3

    nop
.end method

.method fling(IIIII)V
    .locals 4

    goto/32 :goto_16

    nop

    :goto_0
    iput p2, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mSplineDistance:I

    goto/32 :goto_32

    nop

    :goto_1
    double-to-int p3, v2

    goto/32 :goto_7

    nop

    :goto_2
    iput-wide v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mCurrVelocity:D

    goto/32 :goto_1b

    nop

    :goto_3
    iput-wide v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mStart:D

    goto/32 :goto_10

    nop

    :goto_4
    return-void

    :goto_5
    invoke-direct {p0, p2}, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->getSplineFlingDistance(I)D

    move-result-wide v0

    :goto_6
    goto/32 :goto_b

    nop

    :goto_7
    double-to-int p1, p1

    goto/32 :goto_37

    nop

    :goto_8
    cmpl-double p3, p1, v0

    goto/32 :goto_20

    nop

    :goto_9
    iput p5, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mState:I

    goto/32 :goto_f

    nop

    :goto_a
    iget-wide p1, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mFinal:D

    goto/32 :goto_1c

    nop

    :goto_b
    int-to-float p2, p2

    goto/32 :goto_23

    nop

    :goto_c
    cmpg-double p5, p1, v0

    goto/32 :goto_31

    nop

    :goto_d
    invoke-direct {p0, p5, p1, p3}, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->adjustDuration(III)V

    goto/32 :goto_11

    nop

    :goto_e
    iput-wide p1, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mFinal:D

    goto/32 :goto_a

    nop

    :goto_f
    const-wide/16 v0, 0x0

    goto/32 :goto_24

    nop

    :goto_10
    iput-wide v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mCurrentPosition:D

    goto/32 :goto_1d

    nop

    :goto_11
    iput-wide v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mFinal:D

    :goto_12
    goto/32 :goto_2a

    nop

    :goto_13
    iput p5, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mSplineDuration:I

    goto/32 :goto_1a

    nop

    :goto_14
    int-to-double p1, p1

    goto/32 :goto_e

    nop

    :goto_15
    iput-wide v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mVelocity:D

    goto/32 :goto_2

    nop

    :goto_16
    iput p5, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mOver:I

    goto/32 :goto_29

    nop

    :goto_17
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v0

    goto/32 :goto_39

    nop

    :goto_18
    invoke-direct {p0, p2}, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->getSplineFlingDuration(I)I

    move-result p5

    goto/32 :goto_13

    nop

    :goto_19
    double-to-int p2, v0

    goto/32 :goto_0

    nop

    :goto_1a
    iput p5, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mDuration:I

    goto/32 :goto_5

    nop

    :goto_1b
    iput p5, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mSplineDuration:I

    goto/32 :goto_36

    nop

    :goto_1c
    int-to-double v0, p3

    goto/32 :goto_c

    nop

    :goto_1d
    if-le p1, p4, :cond_0

    goto/32 :goto_2c

    :cond_0
    goto/32 :goto_22

    nop

    :goto_1e
    int-to-double v0, p1

    goto/32 :goto_3

    nop

    :goto_1f
    double-to-int p5, v2

    goto/32 :goto_34

    nop

    :goto_20
    if-gtz p3, :cond_1

    goto/32 :goto_2e

    :cond_1
    goto/32 :goto_25

    nop

    :goto_21
    iput-boolean p5, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mFinished:Z

    goto/32 :goto_26

    nop

    :goto_22
    if-lt p1, p3, :cond_2

    goto/32 :goto_30

    :cond_2
    goto/32 :goto_2f

    nop

    :goto_23
    invoke-static {p2}, Ljava/lang/Math;->signum(F)F

    move-result p2

    goto/32 :goto_33

    nop

    :goto_24
    if-nez p2, :cond_3

    goto/32 :goto_6

    :cond_3
    goto/32 :goto_18

    nop

    :goto_25
    iget-wide v2, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mStart:D

    goto/32 :goto_1

    nop

    :goto_26
    int-to-double v0, p2

    goto/32 :goto_15

    nop

    :goto_27
    invoke-direct {p0, p1, p3, p4, p2}, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->startAfterEdge(IIII)V

    goto/32 :goto_4

    nop

    :goto_28
    iget-wide v2, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mStart:D

    goto/32 :goto_1f

    nop

    :goto_29
    const/4 p5, 0x0

    goto/32 :goto_21

    nop

    :goto_2a
    iget-wide p1, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mFinal:D

    goto/32 :goto_38

    nop

    :goto_2b
    return-void

    :goto_2c
    goto/32 :goto_27

    nop

    :goto_2d
    iput-wide v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mFinal:D

    :goto_2e
    goto/32 :goto_2b

    nop

    :goto_2f
    goto :goto_2c

    :goto_30
    goto/32 :goto_9

    nop

    :goto_31
    if-ltz p5, :cond_4

    goto/32 :goto_12

    :cond_4
    goto/32 :goto_28

    nop

    :goto_32
    iget p2, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mSplineDistance:I

    goto/32 :goto_3a

    nop

    :goto_33
    float-to-double v2, p2

    goto/32 :goto_35

    nop

    :goto_34
    double-to-int p1, p1

    goto/32 :goto_d

    nop

    :goto_35
    mul-double/2addr v0, v2

    goto/32 :goto_19

    nop

    :goto_36
    iput p5, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mDuration:I

    goto/32 :goto_17

    nop

    :goto_37
    invoke-direct {p0, p3, p1, p4}, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->adjustDuration(III)V

    goto/32 :goto_2d

    nop

    :goto_38
    int-to-double v0, p4

    goto/32 :goto_8

    nop

    :goto_39
    iput-wide v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mStartTime:J

    goto/32 :goto_1e

    nop

    :goto_3a
    add-int/2addr p1, p2

    goto/32 :goto_14

    nop
.end method

.method final getCurrVelocity()F
    .locals 2

    goto/32 :goto_0

    nop

    :goto_0
    iget-wide v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mCurrVelocity:D

    goto/32 :goto_2

    nop

    :goto_1
    return v0

    :goto_2
    double-to-float v0, v0

    goto/32 :goto_1

    nop
.end method

.method final getCurrentPosition()I
    .locals 2

    goto/32 :goto_2

    nop

    :goto_0
    return v0

    :goto_1
    double-to-int v0, v0

    goto/32 :goto_0

    nop

    :goto_2
    iget-wide v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mCurrentPosition:D

    goto/32 :goto_1

    nop
.end method

.method final getFinal()I
    .locals 2

    goto/32 :goto_2

    nop

    :goto_0
    return v0

    :goto_1
    double-to-int v0, v0

    goto/32 :goto_0

    nop

    :goto_2
    iget-wide v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mFinal:D

    goto/32 :goto_1

    nop
.end method

.method final getStart()I
    .locals 2

    goto/32 :goto_2

    nop

    :goto_0
    double-to-int v0, v0

    goto/32 :goto_1

    nop

    :goto_1
    return v0

    :goto_2
    iget-wide v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mStart:D

    goto/32 :goto_0

    nop
.end method

.method final getState()I
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mState:I

    goto/32 :goto_1

    nop

    :goto_1
    return v0
.end method

.method isAtEquilibrium(DD)Z
    .locals 0

    goto/32 :goto_6

    nop

    :goto_0
    const-wide/high16 p3, 0x3ff0000000000000L    # 1.0

    goto/32 :goto_1

    nop

    :goto_1
    cmpg-double p1, p1, p3

    goto/32 :goto_2

    nop

    :goto_2
    if-ltz p1, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_a

    nop

    :goto_3
    goto :goto_9

    :goto_4
    goto/32 :goto_8

    nop

    :goto_5
    return p1

    :goto_6
    sub-double/2addr p1, p3

    goto/32 :goto_7

    nop

    :goto_7
    invoke-static {p1, p2}, Ljava/lang/Math;->abs(D)D

    move-result-wide p1

    goto/32 :goto_0

    nop

    :goto_8
    const/4 p1, 0x0

    :goto_9
    goto/32 :goto_5

    nop

    :goto_a
    const/4 p1, 0x1

    goto/32 :goto_3

    nop
.end method

.method final isFinished()Z
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return v0

    :goto_1
    iget-boolean v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mFinished:Z

    goto/32 :goto_0

    nop
.end method

.method notifyEdgeReached(III)V
    .locals 2

    goto/32 :goto_2

    nop

    :goto_0
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v0

    goto/32 :goto_6

    nop

    :goto_1
    return-void

    :goto_2
    iget v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mState:I

    goto/32 :goto_7

    nop

    :goto_3
    iput p3, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mOver:I

    goto/32 :goto_0

    nop

    :goto_4
    double-to-int p3, v0

    goto/32 :goto_8

    nop

    :goto_5
    iget-wide v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mCurrVelocity:D

    goto/32 :goto_4

    nop

    :goto_6
    iput-wide v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mStartTime:J

    goto/32 :goto_5

    nop

    :goto_7
    if-eqz v0, :cond_0

    goto/32 :goto_9

    :cond_0
    goto/32 :goto_3

    nop

    :goto_8
    invoke-direct {p0, p1, p2, p2, p3}, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->startAfterEdge(IIII)V

    :goto_9
    goto/32 :goto_1

    nop
.end method

.method final setCurrVelocity(F)V
    .locals 2

    goto/32 :goto_0

    nop

    :goto_0
    float-to-double v0, p1

    goto/32 :goto_1

    nop

    :goto_1
    iput-wide v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mCurrVelocity:D

    goto/32 :goto_2

    nop

    :goto_2
    return-void
.end method

.method final setCurrentPosition(I)V
    .locals 2

    goto/32 :goto_1

    nop

    :goto_0
    iput-wide v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mCurrentPosition:D

    goto/32 :goto_2

    nop

    :goto_1
    int-to-double v0, p1

    goto/32 :goto_0

    nop

    :goto_2
    return-void
.end method

.method final setDuration(I)V
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    iput p1, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mDuration:I

    goto/32 :goto_1

    nop

    :goto_1
    return-void
.end method

.method final setFinal(I)V
    .locals 2

    goto/32 :goto_2

    nop

    :goto_0
    return-void

    :goto_1
    iput-wide v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mFinal:D

    goto/32 :goto_0

    nop

    :goto_2
    int-to-double v0, p1

    goto/32 :goto_1

    nop
.end method

.method setFinalPosition(I)V
    .locals 2

    goto/32 :goto_2

    nop

    :goto_0
    iput-wide v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mFinal:D

    goto/32 :goto_1

    nop

    :goto_1
    const/4 p1, 0x0

    goto/32 :goto_3

    nop

    :goto_2
    int-to-double v0, p1

    goto/32 :goto_0

    nop

    :goto_3
    iput-boolean p1, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mFinished:Z

    goto/32 :goto_4

    nop

    :goto_4
    return-void
.end method

.method final setFinished(Z)V
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    iput-boolean p1, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mFinished:Z

    goto/32 :goto_0

    nop
.end method

.method final setStart(I)V
    .locals 2

    goto/32 :goto_2

    nop

    :goto_0
    iput-wide v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mStart:D

    goto/32 :goto_1

    nop

    :goto_1
    return-void

    :goto_2
    int-to-double v0, p1

    goto/32 :goto_0

    nop
.end method

.method final setStartTime(J)V
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    iput-wide p1, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mStartTime:J

    goto/32 :goto_0

    nop
.end method

.method final setState(I)V
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    iput p1, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mState:I

    goto/32 :goto_1

    nop

    :goto_1
    return-void
.end method

.method springback(III)Z
    .locals 3

    goto/32 :goto_d

    nop

    :goto_0
    iput-wide v1, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mStartTime:J

    goto/32 :goto_10

    nop

    :goto_1
    invoke-direct {p0, p1, p2, v1}, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->startSpringback(III)V

    goto/32 :goto_11

    nop

    :goto_2
    xor-int/2addr p1, v0

    goto/32 :goto_14

    nop

    :goto_3
    iput-wide v1, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mCurrentPosition:D

    goto/32 :goto_7

    nop

    :goto_4
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v1

    goto/32 :goto_0

    nop

    :goto_5
    iput-wide v1, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mFinal:D

    goto/32 :goto_f

    nop

    :goto_6
    if-gt p1, p3, :cond_0

    goto/32 :goto_c

    :cond_0
    goto/32 :goto_b

    nop

    :goto_7
    const-wide/16 v1, 0x0

    goto/32 :goto_a

    nop

    :goto_8
    iget-boolean p1, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mFinished:Z

    goto/32 :goto_2

    nop

    :goto_9
    if-lt p1, p2, :cond_1

    goto/32 :goto_12

    :cond_1
    goto/32 :goto_1

    nop

    :goto_a
    iput-wide v1, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mVelocity:D

    goto/32 :goto_4

    nop

    :goto_b
    invoke-direct {p0, p1, p3, v1}, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->startSpringback(III)V

    :goto_c
    goto/32 :goto_8

    nop

    :goto_d
    const/4 v0, 0x1

    goto/32 :goto_13

    nop

    :goto_e
    int-to-double v1, p1

    goto/32 :goto_5

    nop

    :goto_f
    iput-wide v1, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mStart:D

    goto/32 :goto_3

    nop

    :goto_10
    const/4 v1, 0x0

    goto/32 :goto_15

    nop

    :goto_11
    goto :goto_c

    :goto_12
    goto/32 :goto_6

    nop

    :goto_13
    iput-boolean v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mFinished:Z

    goto/32 :goto_e

    nop

    :goto_14
    return p1

    :goto_15
    iput v1, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mDuration:I

    goto/32 :goto_9

    nop
.end method

.method startScroll(III)V
    .locals 2

    goto/32 :goto_a

    nop

    :goto_0
    iput p3, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mDuration:I

    goto/32 :goto_6

    nop

    :goto_1
    int-to-double v0, p1

    goto/32 :goto_c

    nop

    :goto_2
    iput p1, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mDeceleration:F

    goto/32 :goto_9

    nop

    :goto_3
    iput-boolean v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mFinished:Z

    goto/32 :goto_1

    nop

    :goto_4
    iput-wide p1, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mStartTime:J

    goto/32 :goto_0

    nop

    :goto_5
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide p1

    goto/32 :goto_4

    nop

    :goto_6
    const/4 p1, 0x0

    goto/32 :goto_2

    nop

    :goto_7
    add-int/2addr p1, p2

    goto/32 :goto_f

    nop

    :goto_8
    iput-wide v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mCurrentPosition:D

    goto/32 :goto_7

    nop

    :goto_9
    const-wide/16 p1, 0x0

    goto/32 :goto_d

    nop

    :goto_a
    const/4 v0, 0x0

    goto/32 :goto_3

    nop

    :goto_b
    iput-wide p1, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mFinal:D

    goto/32 :goto_5

    nop

    :goto_c
    iput-wide v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mStart:D

    goto/32 :goto_8

    nop

    :goto_d
    iput-wide p1, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mVelocity:D

    goto/32 :goto_e

    nop

    :goto_e
    return-void

    :goto_f
    int-to-double p1, p1

    goto/32 :goto_b

    nop
.end method

.method startScrollByFling(FII)V
    .locals 2

    goto/32 :goto_a

    nop

    :goto_0
    iput-boolean v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mLastStep:Z

    goto/32 :goto_5

    nop

    :goto_1
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide p1

    goto/32 :goto_4

    nop

    :goto_2
    iput-wide v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mOriginStart:D

    goto/32 :goto_16

    nop

    :goto_3
    int-to-float p2, p2

    goto/32 :goto_18

    nop

    :goto_4
    iput-wide p1, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mStartTime:J

    goto/32 :goto_f

    nop

    :goto_5
    invoke-virtual {p0, v0}, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->setState(I)V

    goto/32 :goto_12

    nop

    :goto_6
    iput-wide v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mCurrentPosition:D

    goto/32 :goto_3

    nop

    :goto_7
    iput-wide p1, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mVelocity:D

    goto/32 :goto_11

    nop

    :goto_8
    new-array p1, p1, [F

    fill-array-data p1, :array_0

    goto/32 :goto_b

    nop

    :goto_9
    float-to-double p1, p1

    goto/32 :goto_1a

    nop

    :goto_a
    const/4 v0, 0x0

    goto/32 :goto_1b

    nop

    :goto_b
    iget-object p3, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mSpringParams:[D

    goto/32 :goto_14

    nop

    :goto_c
    iput-object p2, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mSpringParams:[D

    goto/32 :goto_15

    nop

    :goto_d
    new-instance p1, Lmiuix/animation/physics/SpringOperator;

    goto/32 :goto_10

    nop

    :goto_e
    return-void

    :array_0
    .array-data 4
        0x3f7d70a4    # 0.99f
        0x3ecccccd    # 0.4f
    .end array-data

    :goto_f
    int-to-double p1, p3

    goto/32 :goto_7

    nop

    :goto_10
    invoke-direct {p1}, Lmiuix/animation/physics/SpringOperator;-><init>()V

    goto/32 :goto_17

    nop

    :goto_11
    iput-wide p1, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mCurrVelocity:D

    goto/32 :goto_d

    nop

    :goto_12
    float-to-double v0, p1

    goto/32 :goto_2

    nop

    :goto_13
    new-array p2, p1, [D

    goto/32 :goto_c

    nop

    :goto_14
    invoke-virtual {p2, p1, p3}, Lmiuix/animation/physics/SpringOperator;->getParameters([F[D)V

    goto/32 :goto_e

    nop

    :goto_15
    iget-object p2, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mSpringOperator:Lmiuix/animation/physics/SpringOperator;

    goto/32 :goto_8

    nop

    :goto_16
    iput-wide v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mStart:D

    goto/32 :goto_6

    nop

    :goto_17
    iput-object p1, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mSpringOperator:Lmiuix/animation/physics/SpringOperator;

    goto/32 :goto_19

    nop

    :goto_18
    add-float/2addr p1, p2

    goto/32 :goto_9

    nop

    :goto_19
    const/4 p1, 0x2

    goto/32 :goto_13

    nop

    :goto_1a
    iput-wide p1, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mFinal:D

    goto/32 :goto_1

    nop

    :goto_1b
    iput-boolean v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mFinished:Z

    goto/32 :goto_0

    nop
.end method

.method update()Z
    .locals 9

    goto/32 :goto_52

    nop

    :goto_0
    float-to-double v5, v5

    goto/32 :goto_e

    nop

    :goto_1
    add-float/2addr v0, v1

    goto/32 :goto_6f

    nop

    :goto_2
    const/4 v3, 0x0

    goto/32 :goto_59

    nop

    :goto_3
    long-to-float v0, v0

    goto/32 :goto_8

    nop

    :goto_4
    int-to-float v0, v0

    goto/32 :goto_67

    nop

    :goto_5
    mul-float/2addr v5, v6

    goto/32 :goto_42

    nop

    :goto_6
    cmp-long v2, v0, v2

    goto/32 :goto_2

    nop

    :goto_7
    goto/16 :goto_23

    :pswitch_0
    goto/32 :goto_4e

    nop

    :goto_8
    iget v1, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mSplineDuration:I

    goto/32 :goto_38

    nop

    :goto_9
    int-to-double v2, v2

    goto/32 :goto_61

    nop

    :goto_a
    mul-float/2addr v8, v1

    goto/32 :goto_12

    nop

    :goto_b
    float-to-double v1, v3

    goto/32 :goto_70

    nop

    :goto_c
    mul-float v5, v3, v0

    goto/32 :goto_0

    nop

    :goto_d
    iput-wide v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mCurrentPosition:D

    goto/32 :goto_3b

    nop

    :goto_e
    add-double/2addr v5, v1

    goto/32 :goto_1d

    nop

    :goto_f
    cmp-long v5, v0, v5

    goto/32 :goto_6e

    nop

    :goto_10
    return v3

    :goto_11
    goto/32 :goto_24

    nop

    :goto_12
    sub-float/2addr v6, v8

    goto/32 :goto_5

    nop

    :goto_13
    iget v3, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mDeceleration:F

    goto/32 :goto_c

    nop

    :goto_14
    mul-float v1, v0, v0

    goto/32 :goto_55

    nop

    :goto_15
    div-float/2addr v3, v8

    goto/32 :goto_1a

    nop

    :goto_16
    iget v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mDuration:I

    goto/32 :goto_2c

    nop

    :goto_17
    const/high16 v6, 0x40400000    # 3.0f

    goto/32 :goto_25

    nop

    :goto_18
    const/high16 v7, 0x447a0000    # 1000.0f

    goto/32 :goto_6d

    nop

    :goto_19
    long-to-float v0, v0

    goto/32 :goto_41

    nop

    :goto_1a
    float-to-double v5, v3

    goto/32 :goto_60

    nop

    :goto_1b
    if-eqz v2, :cond_0

    goto/32 :goto_11

    :cond_0
    goto/32 :goto_16

    nop

    :goto_1c
    iget-wide v1, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mVelocity:D

    goto/32 :goto_13

    nop

    :goto_1d
    iput-wide v5, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mCurrVelocity:D

    goto/32 :goto_57

    nop

    :goto_1e
    iget v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mSplineDuration:I

    goto/32 :goto_4

    nop

    :goto_1f
    goto :goto_23

    :pswitch_1
    goto/32 :goto_19

    nop

    :goto_20
    mul-double/2addr v1, v5

    goto/32 :goto_4c

    nop

    :goto_21
    mul-float/2addr v0, v5

    goto/32 :goto_45

    nop

    :goto_22
    move-wide v5, v1

    :goto_23
    goto/32 :goto_4d

    nop

    :goto_24
    iget v2, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mDuration:I

    goto/32 :goto_48

    nop

    :goto_25
    mul-float/2addr v6, v1

    goto/32 :goto_43

    nop

    :goto_26
    div-float/2addr v0, v1

    goto/32 :goto_14

    nop

    :goto_27
    return v3

    :goto_28
    goto/32 :goto_50

    nop

    :goto_29
    mul-float v2, v0, v1

    goto/32 :goto_62

    nop

    :goto_2a
    float-to-double v5, v5

    goto/32 :goto_5e

    nop

    :goto_2b
    div-float v5, v1, v6

    goto/32 :goto_5d

    nop

    :goto_2c
    if-gtz v0, :cond_1

    goto/32 :goto_5c

    :cond_1
    goto/32 :goto_5b

    nop

    :goto_2d
    sub-float/2addr v1, v2

    goto/32 :goto_63

    nop

    :goto_2e
    mul-float/2addr v5, v0

    goto/32 :goto_1e

    nop

    :goto_2f
    const-wide/16 v2, 0x0

    goto/32 :goto_6

    nop

    :goto_30
    if-lt v2, v6, :cond_2

    goto/32 :goto_46

    :cond_2
    goto/32 :goto_66

    nop

    :goto_31
    int-to-float v1, v0

    goto/32 :goto_3a

    nop

    :goto_32
    sget-object v1, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->SPLINE_POSITION:[F

    goto/32 :goto_37

    nop

    :goto_33
    aget v1, v1, v5

    goto/32 :goto_2d

    nop

    :goto_34
    int-to-float v1, v2

    goto/32 :goto_26

    nop

    :goto_35
    const/high16 v1, 0x42c80000    # 100.0f

    goto/32 :goto_29

    nop

    :goto_36
    const/4 v5, 0x0

    goto/32 :goto_65

    nop

    :goto_37
    aget v2, v1, v2

    goto/32 :goto_33

    nop

    :goto_38
    int-to-float v1, v1

    goto/32 :goto_64

    nop

    :goto_39
    invoke-static {v2}, Ljava/lang/Math;->signum(F)F

    move-result v2

    goto/32 :goto_3d

    nop

    :goto_3a
    mul-float/2addr v3, v1

    goto/32 :goto_b

    nop

    :goto_3b
    return v4

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :goto_3c
    float-to-double v0, v2

    goto/32 :goto_6a

    nop

    :goto_3d
    iget v3, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mOver:I

    goto/32 :goto_53

    nop

    :goto_3e
    long-to-int v2, v2

    goto/32 :goto_9

    nop

    :goto_3f
    double-to-float v2, v2

    goto/32 :goto_39

    nop

    :goto_40
    div-float/2addr v6, v1

    goto/32 :goto_32

    nop

    :goto_41
    div-float/2addr v0, v7

    goto/32 :goto_1c

    nop

    :goto_42
    float-to-double v5, v5

    goto/32 :goto_51

    nop

    :goto_43
    mul-float/2addr v8, v0

    goto/32 :goto_a

    nop

    :goto_44
    neg-float v0, v0

    goto/32 :goto_1

    nop

    :goto_45
    add-float v3, v2, v0

    :goto_46
    goto/32 :goto_4f

    nop

    :goto_47
    mul-float/2addr v5, v7

    goto/32 :goto_2a

    nop

    :goto_48
    int-to-long v5, v2

    goto/32 :goto_f

    nop

    :goto_49
    mul-float/2addr v3, v0

    goto/32 :goto_15

    nop

    :goto_4a
    const/high16 v3, 0x3f800000    # 1.0f

    goto/32 :goto_36

    nop

    :goto_4b
    mul-float/2addr v2, v3

    goto/32 :goto_5f

    nop

    :goto_4c
    mul-float/2addr v3, v0

    goto/32 :goto_49

    nop

    :goto_4d
    iget-wide v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mStart:D

    goto/32 :goto_72

    nop

    :goto_4e
    long-to-float v0, v0

    goto/32 :goto_34

    nop

    :goto_4f
    iget v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mSplineDistance:I

    goto/32 :goto_31

    nop

    :goto_50
    const-wide/16 v5, 0x0

    goto/32 :goto_5a

    nop

    :goto_51
    int-to-float v3, v3

    goto/32 :goto_4b

    nop

    :goto_52
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v0

    goto/32 :goto_68

    nop

    :goto_53
    int-to-float v5, v3

    goto/32 :goto_56

    nop

    :goto_54
    goto/16 :goto_23

    :pswitch_2
    goto/32 :goto_3

    nop

    :goto_55
    iget-wide v2, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mVelocity:D

    goto/32 :goto_3f

    nop

    :goto_56
    mul-float/2addr v5, v2

    goto/32 :goto_17

    nop

    :goto_57
    float-to-double v5, v0

    goto/32 :goto_20

    nop

    :goto_58
    mul-float/2addr v2, v3

    goto/32 :goto_44

    nop

    :goto_59
    const/4 v4, 0x1

    goto/32 :goto_1b

    nop

    :goto_5a
    iget v3, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mState:I

    goto/32 :goto_18

    nop

    :goto_5b
    move v3, v4

    :goto_5c
    goto/32 :goto_10

    nop

    :goto_5d
    sub-float/2addr v0, v3

    goto/32 :goto_21

    nop

    :goto_5e
    iput-wide v5, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mCurrVelocity:D

    goto/32 :goto_22

    nop

    :goto_5f
    const/high16 v3, 0x40c00000    # 6.0f

    goto/32 :goto_58

    nop

    :goto_60
    add-double/2addr v5, v1

    goto/32 :goto_7

    nop

    :goto_61
    add-double/2addr v0, v2

    goto/32 :goto_d

    nop

    :goto_62
    float-to-int v2, v2

    goto/32 :goto_4a

    nop

    :goto_63
    sub-float/2addr v6, v3

    goto/32 :goto_2b

    nop

    :goto_64
    div-float/2addr v0, v1

    goto/32 :goto_35

    nop

    :goto_65
    const/16 v6, 0x64

    goto/32 :goto_30

    nop

    :goto_66
    int-to-float v3, v2

    goto/32 :goto_6c

    nop

    :goto_67
    div-float/2addr v5, v0

    goto/32 :goto_47

    nop

    :goto_68
    iget-wide v2, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mStartTime:J

    goto/32 :goto_71

    nop

    :goto_69
    int-to-float v6, v5

    goto/32 :goto_40

    nop

    :goto_6a
    iput-wide v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mCurrVelocity:D

    goto/32 :goto_54

    nop

    :goto_6b
    add-int/lit8 v5, v2, 0x1

    goto/32 :goto_69

    nop

    :goto_6c
    div-float/2addr v3, v1

    goto/32 :goto_6b

    nop

    :goto_6d
    const/high16 v8, 0x40000000    # 2.0f

    packed-switch v3, :pswitch_data_0

    goto/32 :goto_1f

    nop

    :goto_6e
    if-gtz v5, :cond_3

    goto/32 :goto_28

    :cond_3
    goto/32 :goto_27

    nop

    :goto_6f
    mul-float/2addr v2, v0

    goto/32 :goto_3c

    nop

    :goto_70
    int-to-float v0, v0

    goto/32 :goto_2e

    nop

    :goto_71
    sub-long/2addr v0, v2

    goto/32 :goto_2f

    nop

    :goto_72
    invoke-static {v5, v6}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    goto/32 :goto_3e

    nop
.end method

.method updateScroll(F)V
    .locals 6

    goto/32 :goto_1

    nop

    :goto_0
    mul-double/2addr v2, v4

    goto/32 :goto_9

    nop

    :goto_1
    iget-wide v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mStart:D

    goto/32 :goto_5

    nop

    :goto_2
    iget-wide v4, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mFinal:D

    goto/32 :goto_4

    nop

    :goto_3
    add-double/2addr v0, v2

    goto/32 :goto_8

    nop

    :goto_4
    sub-double/2addr v4, v0

    goto/32 :goto_0

    nop

    :goto_5
    float-to-double v2, p1

    goto/32 :goto_2

    nop

    :goto_6
    return-void

    :goto_7
    long-to-double v2, v2

    goto/32 :goto_3

    nop

    :goto_8
    iput-wide v0, p0, Lmiuix/overscroller/widget/OverScroller$SplineOverScroller;->mCurrentPosition:D

    goto/32 :goto_6

    nop

    :goto_9
    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    goto/32 :goto_7

    nop
.end method
