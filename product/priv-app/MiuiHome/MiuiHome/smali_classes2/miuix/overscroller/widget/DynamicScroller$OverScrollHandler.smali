.class Lmiuix/overscroller/widget/DynamicScroller$OverScrollHandler;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiuix/overscroller/widget/DynamicScroller;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "OverScrollHandler"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiuix/overscroller/widget/DynamicScroller$OverScrollHandler$OnFinishedListener;,
        Lmiuix/overscroller/widget/DynamicScroller$OverScrollHandler$Monitor;
    }
.end annotation


# instance fields
.field private mAnimMaxValue:F

.field private mAnimMinValue:F

.field mAnimation:Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation<",
            "*>;"
        }
    .end annotation
.end field

.field private mLastUpdateTime:J

.field private final mMaxLegalValue:I

.field private final mMinLegalValue:I

.field private mMonitor:Lmiuix/overscroller/widget/DynamicScroller$OverScrollHandler$Monitor;

.field private mOnFinishedListener:Lmiuix/overscroller/widget/DynamicScroller$OverScrollHandler$OnFinishedListener;

.field mStartValue:I

.field mValue:I

.field mVelocity:F


# direct methods
.method constructor <init>(Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;IF)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation<",
            "*>;IF)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lmiuix/overscroller/widget/DynamicScroller$OverScrollHandler$Monitor;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lmiuix/overscroller/widget/DynamicScroller$OverScrollHandler$Monitor;-><init>(Lmiuix/overscroller/widget/DynamicScroller$OverScrollHandler;Lmiuix/overscroller/widget/DynamicScroller$1;)V

    iput-object v0, p0, Lmiuix/overscroller/widget/DynamicScroller$OverScrollHandler;->mMonitor:Lmiuix/overscroller/widget/DynamicScroller$OverScrollHandler$Monitor;

    iput-object p1, p0, Lmiuix/overscroller/widget/DynamicScroller$OverScrollHandler;->mAnimation:Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;

    iget-object p1, p0, Lmiuix/overscroller/widget/DynamicScroller$OverScrollHandler;->mAnimation:Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;

    const v0, -0x800001

    invoke-virtual {p1, v0}, Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;->setMinValue(F)Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;

    iget-object p1, p0, Lmiuix/overscroller/widget/DynamicScroller$OverScrollHandler;->mAnimation:Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;

    const v0, 0x7f7fffff    # Float.MAX_VALUE

    invoke-virtual {p1, v0}, Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;->setMaxValue(F)Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;

    iput p2, p0, Lmiuix/overscroller/widget/DynamicScroller$OverScrollHandler;->mStartValue:I

    iput p3, p0, Lmiuix/overscroller/widget/DynamicScroller$OverScrollHandler;->mVelocity:F

    const p1, 0x7fffffff

    const/high16 v0, -0x80000000

    if-lez p2, :cond_0

    add-int/2addr v0, p2

    goto :goto_0

    :cond_0
    if-gez p2, :cond_1

    add-int/2addr p1, p2

    :cond_1
    :goto_0
    iput v0, p0, Lmiuix/overscroller/widget/DynamicScroller$OverScrollHandler;->mMinLegalValue:I

    iput p1, p0, Lmiuix/overscroller/widget/DynamicScroller$OverScrollHandler;->mMaxLegalValue:I

    iget-object p1, p0, Lmiuix/overscroller/widget/DynamicScroller$OverScrollHandler;->mAnimation:Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;->setStartValue(F)Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;

    iget-object p1, p0, Lmiuix/overscroller/widget/DynamicScroller$OverScrollHandler;->mAnimation:Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;

    invoke-virtual {p1, p3}, Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;->setStartVelocity(F)Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;

    return-void
.end method

.method static synthetic access$500(Lmiuix/overscroller/widget/DynamicScroller$OverScrollHandler;)F
    .locals 0

    iget p0, p0, Lmiuix/overscroller/widget/DynamicScroller$OverScrollHandler;->mAnimMinValue:F

    return p0
.end method

.method static synthetic access$600(Lmiuix/overscroller/widget/DynamicScroller$OverScrollHandler;)F
    .locals 0

    iget p0, p0, Lmiuix/overscroller/widget/DynamicScroller$OverScrollHandler;->mAnimMaxValue:F

    return p0
.end method


# virtual methods
.method cancel()V
    .locals 2

    goto/32 :goto_6

    nop

    :goto_0
    iput-wide v0, p0, Lmiuix/overscroller/widget/DynamicScroller$OverScrollHandler;->mLastUpdateTime:J

    goto/32 :goto_1

    nop

    :goto_1
    iget-object v0, p0, Lmiuix/overscroller/widget/DynamicScroller$OverScrollHandler;->mAnimation:Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;

    goto/32 :goto_4

    nop

    :goto_2
    iget-object v1, p0, Lmiuix/overscroller/widget/DynamicScroller$OverScrollHandler;->mMonitor:Lmiuix/overscroller/widget/DynamicScroller$OverScrollHandler$Monitor;

    goto/32 :goto_3

    nop

    :goto_3
    invoke-virtual {v0, v1}, Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;->removeUpdateListener(Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation$OnAnimationUpdateListener;)V

    goto/32 :goto_5

    nop

    :goto_4
    invoke-virtual {v0}, Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;->cancel()V

    goto/32 :goto_7

    nop

    :goto_5
    return-void

    :goto_6
    const-wide/16 v0, 0x0

    goto/32 :goto_0

    nop

    :goto_7
    iget-object v0, p0, Lmiuix/overscroller/widget/DynamicScroller$OverScrollHandler;->mAnimation:Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;

    goto/32 :goto_2

    nop
.end method

.method continueWhenFinished()Z
    .locals 3

    goto/32 :goto_6

    nop

    :goto_0
    return v0

    :goto_1
    goto/32 :goto_5

    nop

    :goto_2
    if-nez v0, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_3

    nop

    :goto_3
    iget v1, p0, Lmiuix/overscroller/widget/DynamicScroller$OverScrollHandler;->mValue:I

    goto/32 :goto_8

    nop

    :goto_4
    invoke-interface {v0, v1, v2}, Lmiuix/overscroller/widget/DynamicScroller$OverScrollHandler$OnFinishedListener;->whenFinished(FF)Z

    move-result v0

    goto/32 :goto_0

    nop

    :goto_5
    const/4 v0, 0x0

    goto/32 :goto_7

    nop

    :goto_6
    iget-object v0, p0, Lmiuix/overscroller/widget/DynamicScroller$OverScrollHandler;->mOnFinishedListener:Lmiuix/overscroller/widget/DynamicScroller$OverScrollHandler$OnFinishedListener;

    goto/32 :goto_2

    nop

    :goto_7
    return v0

    :goto_8
    int-to-float v1, v1

    goto/32 :goto_9

    nop

    :goto_9
    iget v2, p0, Lmiuix/overscroller/widget/DynamicScroller$OverScrollHandler;->mVelocity:F

    goto/32 :goto_4

    nop
.end method

.method getAnimation()Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation<",
            "*>;"
        }
    .end annotation

    goto/32 :goto_1

    nop

    :goto_0
    return-object v0

    :goto_1
    iget-object v0, p0, Lmiuix/overscroller/widget/DynamicScroller$OverScrollHandler;->mAnimation:Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;

    goto/32 :goto_0

    nop
.end method

.method getOffset(I)I
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    sub-int/2addr p1, v0

    goto/32 :goto_2

    nop

    :goto_1
    iget v0, p0, Lmiuix/overscroller/widget/DynamicScroller$OverScrollHandler;->mStartValue:I

    goto/32 :goto_0

    nop

    :goto_2
    return p1
.end method

.method setMaxValue(I)V
    .locals 1

    goto/32 :goto_c

    nop

    :goto_0
    iput p1, p0, Lmiuix/overscroller/widget/DynamicScroller$OverScrollHandler;->mAnimMaxValue:F

    goto/32 :goto_2

    nop

    :goto_1
    iget-object v0, p0, Lmiuix/overscroller/widget/DynamicScroller$OverScrollHandler;->mAnimation:Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;

    goto/32 :goto_4

    nop

    :goto_2
    return-void

    :goto_3
    iget v0, p0, Lmiuix/overscroller/widget/DynamicScroller$OverScrollHandler;->mStartValue:I

    goto/32 :goto_7

    nop

    :goto_4
    invoke-virtual {v0, p1}, Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;->setMaxValue(F)Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;

    goto/32 :goto_0

    nop

    :goto_5
    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result p1

    goto/32 :goto_6

    nop

    :goto_6
    int-to-float p1, p1

    goto/32 :goto_1

    nop

    :goto_7
    sub-int/2addr p1, v0

    goto/32 :goto_b

    nop

    :goto_8
    if-gt p1, v0, :cond_0

    goto/32 :goto_a

    :cond_0
    goto/32 :goto_9

    nop

    :goto_9
    move p1, v0

    :goto_a
    goto/32 :goto_3

    nop

    :goto_b
    const/4 v0, 0x0

    goto/32 :goto_5

    nop

    :goto_c
    iget v0, p0, Lmiuix/overscroller/widget/DynamicScroller$OverScrollHandler;->mMaxLegalValue:I

    goto/32 :goto_8

    nop
.end method

.method setMinValue(I)V
    .locals 1

    goto/32 :goto_5

    nop

    :goto_0
    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result p1

    goto/32 :goto_6

    nop

    :goto_1
    invoke-virtual {v0, p1}, Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;->setMinValue(F)Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;

    goto/32 :goto_9

    nop

    :goto_2
    move p1, v0

    :goto_3
    goto/32 :goto_7

    nop

    :goto_4
    if-lt p1, v0, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_2

    nop

    :goto_5
    iget v0, p0, Lmiuix/overscroller/widget/DynamicScroller$OverScrollHandler;->mMinLegalValue:I

    goto/32 :goto_4

    nop

    :goto_6
    int-to-float p1, p1

    goto/32 :goto_c

    nop

    :goto_7
    iget v0, p0, Lmiuix/overscroller/widget/DynamicScroller$OverScrollHandler;->mStartValue:I

    goto/32 :goto_8

    nop

    :goto_8
    sub-int/2addr p1, v0

    goto/32 :goto_b

    nop

    :goto_9
    iput p1, p0, Lmiuix/overscroller/widget/DynamicScroller$OverScrollHandler;->mAnimMinValue:F

    goto/32 :goto_a

    nop

    :goto_a
    return-void

    :goto_b
    const/4 v0, 0x0

    goto/32 :goto_0

    nop

    :goto_c
    iget-object v0, p0, Lmiuix/overscroller/widget/DynamicScroller$OverScrollHandler;->mAnimation:Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;

    goto/32 :goto_1

    nop
.end method

.method setOnFinishedListener(Lmiuix/overscroller/widget/DynamicScroller$OverScrollHandler$OnFinishedListener;)V
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    iput-object p1, p0, Lmiuix/overscroller/widget/DynamicScroller$OverScrollHandler;->mOnFinishedListener:Lmiuix/overscroller/widget/DynamicScroller$OverScrollHandler$OnFinishedListener;

    goto/32 :goto_1

    nop

    :goto_1
    return-void
.end method

.method start()V
    .locals 2

    goto/32 :goto_2

    nop

    :goto_0
    iput-wide v0, p0, Lmiuix/overscroller/widget/DynamicScroller$OverScrollHandler;->mLastUpdateTime:J

    goto/32 :goto_7

    nop

    :goto_1
    const/4 v1, 0x1

    goto/32 :goto_5

    nop

    :goto_2
    iget-object v0, p0, Lmiuix/overscroller/widget/DynamicScroller$OverScrollHandler;->mAnimation:Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;

    goto/32 :goto_6

    nop

    :goto_3
    iget-object v0, p0, Lmiuix/overscroller/widget/DynamicScroller$OverScrollHandler;->mAnimation:Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;

    goto/32 :goto_1

    nop

    :goto_4
    const-wide/16 v0, 0x0

    goto/32 :goto_0

    nop

    :goto_5
    invoke-virtual {v0, v1}, Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;->start(Z)V

    goto/32 :goto_4

    nop

    :goto_6
    iget-object v1, p0, Lmiuix/overscroller/widget/DynamicScroller$OverScrollHandler;->mMonitor:Lmiuix/overscroller/widget/DynamicScroller$OverScrollHandler$Monitor;

    goto/32 :goto_8

    nop

    :goto_7
    return-void

    :goto_8
    invoke-virtual {v0, v1}, Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;->addUpdateListener(Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation$OnAnimationUpdateListener;)Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;

    goto/32 :goto_3

    nop
.end method

.method update()Z
    .locals 8

    goto/32 :goto_11

    nop

    :goto_0
    return v0

    :goto_1
    iget-object v4, p0, Lmiuix/overscroller/widget/DynamicScroller$OverScrollHandler;->mMonitor:Lmiuix/overscroller/widget/DynamicScroller$OverScrollHandler$Monitor;

    goto/32 :goto_13

    nop

    :goto_2
    const-string v4, "%s finishing value(%d) velocity(%f)"

    goto/32 :goto_1d

    nop

    :goto_3
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    goto/32 :goto_26

    nop

    :goto_4
    if-nez v0, :cond_0

    goto/32 :goto_1c

    :cond_0
    goto/32 :goto_2

    nop

    :goto_5
    const-wide/16 v4, 0x0

    goto/32 :goto_1b

    nop

    :goto_6
    iget v6, p0, Lmiuix/overscroller/widget/DynamicScroller$OverScrollHandler;->mValue:I

    goto/32 :goto_3

    nop

    :goto_7
    const/4 v6, 0x0

    goto/32 :goto_21

    nop

    :goto_8
    iget v6, p0, Lmiuix/overscroller/widget/DynamicScroller$OverScrollHandler;->mVelocity:F

    goto/32 :goto_20

    nop

    :goto_9
    invoke-virtual {v0, v2, v3}, Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;->doAnimationFrame(J)Z

    move-result v0

    goto/32 :goto_4

    nop

    :goto_a
    invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    goto/32 :goto_10

    nop

    :goto_b
    cmp-long v0, v2, v0

    goto/32 :goto_25

    nop

    :goto_c
    iput-wide v2, p0, Lmiuix/overscroller/widget/DynamicScroller$OverScrollHandler;->mLastUpdateTime:J

    goto/32 :goto_0

    nop

    :goto_d
    iget-object v0, p0, Lmiuix/overscroller/widget/DynamicScroller$OverScrollHandler;->mAnimation:Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;

    goto/32 :goto_19

    nop

    :goto_e
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v2

    goto/32 :goto_b

    nop

    :goto_f
    xor-int/2addr v0, v1

    goto/32 :goto_22

    nop

    :goto_10
    invoke-virtual {v7}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v7

    goto/32 :goto_14

    nop

    :goto_11
    iget-wide v0, p0, Lmiuix/overscroller/widget/DynamicScroller$OverScrollHandler;->mLastUpdateTime:J

    goto/32 :goto_e

    nop

    :goto_12
    new-array v5, v5, [Ljava/lang/Object;

    goto/32 :goto_7

    nop

    :goto_13
    invoke-virtual {v1, v4}, Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;->removeUpdateListener(Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation$OnAnimationUpdateListener;)V

    goto/32 :goto_5

    nop

    :goto_14
    aput-object v7, v5, v6

    goto/32 :goto_6

    nop

    :goto_15
    aput-object v6, v5, v1

    goto/32 :goto_18

    nop

    :goto_16
    const/4 v1, 0x2

    goto/32 :goto_8

    nop

    :goto_17
    const-string v0, "update done in this frame, dropping current update request"

    goto/32 :goto_1e

    nop

    :goto_18
    invoke-static {v4, v5}, Lmiuix/overscroller/widget/OverScrollLogger;->verbose(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/32 :goto_1a

    nop

    :goto_19
    invoke-virtual {v0}, Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;->isRunning()Z

    move-result v0

    goto/32 :goto_f

    nop

    :goto_1a
    iget-object v1, p0, Lmiuix/overscroller/widget/DynamicScroller$OverScrollHandler;->mAnimation:Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;

    goto/32 :goto_1

    nop

    :goto_1b
    iput-wide v4, p0, Lmiuix/overscroller/widget/DynamicScroller$OverScrollHandler;->mLastUpdateTime:J

    :goto_1c
    goto/32 :goto_c

    nop

    :goto_1d
    const/4 v5, 0x3

    goto/32 :goto_12

    nop

    :goto_1e
    invoke-static {v0}, Lmiuix/overscroller/widget/OverScrollLogger;->verbose(Ljava/lang/String;)V

    goto/32 :goto_d

    nop

    :goto_1f
    if-eqz v0, :cond_1

    goto/32 :goto_23

    :cond_1
    goto/32 :goto_17

    nop

    :goto_20
    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    goto/32 :goto_15

    nop

    :goto_21
    iget-object v7, p0, Lmiuix/overscroller/widget/DynamicScroller$OverScrollHandler;->mAnimation:Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;

    goto/32 :goto_a

    nop

    :goto_22
    return v0

    :goto_23
    goto/32 :goto_24

    nop

    :goto_24
    iget-object v0, p0, Lmiuix/overscroller/widget/DynamicScroller$OverScrollHandler;->mAnimation:Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;

    goto/32 :goto_9

    nop

    :goto_25
    const/4 v1, 0x1

    goto/32 :goto_1f

    nop

    :goto_26
    aput-object v6, v5, v1

    goto/32 :goto_16

    nop
.end method
