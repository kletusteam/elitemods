.class final Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation$DragForce;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "DragForce"
.end annotation


# instance fields
.field private final MILLISECONDS_PER_SECOND:F

.field private mDragRate:D

.field private mFriction:F

.field private final mMassState:Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation$MassState;

.field private mVelocityThreshold:F


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const v0, -0x3f79999a    # -4.2f

    iput v0, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation$DragForce;->mFriction:F

    new-instance v0, Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation$MassState;

    invoke-direct {v0}, Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation$MassState;-><init>()V

    iput-object v0, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation$DragForce;->mMassState:Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation$MassState;

    const/high16 v0, 0x447a0000    # 1000.0f

    iput v0, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation$DragForce;->MILLISECONDS_PER_SECOND:F

    return-void
.end method

.method static synthetic access$000(Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation$DragForce;)F
    .locals 0

    iget p0, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation$DragForce;->mFriction:F

    return p0
.end method

.method static synthetic access$100(Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation$DragForce;)F
    .locals 0

    iget p0, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation$DragForce;->mVelocityThreshold:F

    return p0
.end method


# virtual methods
.method public isAtEquilibrium(FF)Z
    .locals 0

    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result p1

    iget p2, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation$DragForce;->mVelocityThreshold:F

    cmpg-float p1, p1, p2

    if-gez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method setFrictionScalar(F)V
    .locals 4

    goto/32 :goto_2

    nop

    :goto_0
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    goto/32 :goto_8

    nop

    :goto_1
    iput-wide v2, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation$DragForce;->mDragRate:D

    goto/32 :goto_a

    nop

    :goto_2
    const v0, -0x3f79999a    # -4.2f

    goto/32 :goto_3

    nop

    :goto_3
    mul-float/2addr p1, v0

    goto/32 :goto_6

    nop

    :goto_4
    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    goto/32 :goto_0

    nop

    :goto_5
    float-to-double v0, p1

    goto/32 :goto_7

    nop

    :goto_6
    iput p1, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation$DragForce;->mFriction:F

    goto/32 :goto_9

    nop

    :goto_7
    const-wide v2, 0x4005bf0a8b145769L    # Math.E

    goto/32 :goto_4

    nop

    :goto_8
    sub-double/2addr v2, v0

    goto/32 :goto_1

    nop

    :goto_9
    iget p1, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation$DragForce;->mFriction:F

    goto/32 :goto_5

    nop

    :goto_a
    return-void
.end method

.method setValueThreshold(F)V
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    return-void

    :goto_1
    iput p1, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation$DragForce;->mVelocityThreshold:F

    goto/32 :goto_0

    nop

    :goto_2
    const/high16 v0, 0x427a0000    # 62.5f

    goto/32 :goto_3

    nop

    :goto_3
    mul-float/2addr p1, v0

    goto/32 :goto_1

    nop
.end method

.method updateValueAndVelocity(FFJ)Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation$MassState;
    .locals 4

    goto/32 :goto_14

    nop

    :goto_0
    float-to-double v2, p2

    goto/32 :goto_11

    nop

    :goto_1
    long-to-float p3, p3

    goto/32 :goto_a

    nop

    :goto_2
    invoke-static {p3, p4, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide p3

    goto/32 :goto_15

    nop

    :goto_3
    iget-object p1, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation$DragForce;->mMassState:Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation$MassState;

    goto/32 :goto_b

    nop

    :goto_4
    iget p2, p2, Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation$MassState;->mVelocity:F

    goto/32 :goto_c

    nop

    :goto_5
    iget-object p2, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation$DragForce;->mMassState:Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation$MassState;

    goto/32 :goto_4

    nop

    :goto_6
    iget-object p4, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation$DragForce;->mMassState:Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation$MassState;

    goto/32 :goto_0

    nop

    :goto_7
    iget p1, p1, Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation$MassState;->mValue:F

    goto/32 :goto_5

    nop

    :goto_8
    add-float/2addr p1, p2

    goto/32 :goto_9

    nop

    :goto_9
    iput p1, p4, Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation$MassState;->mValue:F

    goto/32 :goto_12

    nop

    :goto_a
    const/high16 p4, 0x447a0000    # 1000.0f

    goto/32 :goto_e

    nop

    :goto_b
    const/4 p2, 0x0

    goto/32 :goto_17

    nop

    :goto_c
    invoke-virtual {p0, p1, p2}, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation$DragForce;->isAtEquilibrium(FF)Z

    move-result p1

    goto/32 :goto_10

    nop

    :goto_d
    iput p2, p4, Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation$MassState;->mVelocity:F

    goto/32 :goto_f

    nop

    :goto_e
    div-float/2addr p3, p4

    goto/32 :goto_1b

    nop

    :goto_f
    iget p2, p4, Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation$MassState;->mVelocity:F

    goto/32 :goto_19

    nop

    :goto_10
    if-nez p1, :cond_0

    goto/32 :goto_18

    :cond_0
    goto/32 :goto_3

    nop

    :goto_11
    mul-double/2addr v2, v0

    goto/32 :goto_1e

    nop

    :goto_12
    iget-object p1, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation$DragForce;->mMassState:Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation$MassState;

    goto/32 :goto_7

    nop

    :goto_13
    iget-object p1, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation$DragForce;->mMassState:Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation$MassState;

    goto/32 :goto_16

    nop

    :goto_14
    const-wide/16 v0, 0x10

    goto/32 :goto_2

    nop

    :goto_15
    iget-wide v0, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation$DragForce;->mDragRate:D

    goto/32 :goto_1c

    nop

    :goto_16
    return-object p1

    :goto_17
    iput p2, p1, Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation$MassState;->mVelocity:F

    :goto_18
    goto/32 :goto_13

    nop

    :goto_19
    mul-float/2addr p2, p3

    goto/32 :goto_8

    nop

    :goto_1a
    sub-double/2addr v2, v0

    goto/32 :goto_1

    nop

    :goto_1b
    float-to-double v0, p3

    goto/32 :goto_1d

    nop

    :goto_1c
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    goto/32 :goto_1a

    nop

    :goto_1d
    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    goto/32 :goto_6

    nop

    :goto_1e
    double-to-float p2, v2

    goto/32 :goto_d

    nop
.end method
