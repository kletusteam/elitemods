.class public final Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation;
.super Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation$DragForce;,
        Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation$FinalValueListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation<",
        "Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation;",
        ">;"
    }
.end annotation


# instance fields
.field private mFinalValueListener:Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation$FinalValueListener;

.field private final mFlingForce:Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation$DragForce;


# direct methods
.method public constructor <init>(Lmiuix/overscroller/internal/dynamicanimation/animation/FloatValueHolder;Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation$FinalValueListener;)V
    .locals 1

    invoke-direct {p0, p1}, Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;-><init>(Lmiuix/overscroller/internal/dynamicanimation/animation/FloatValueHolder;)V

    new-instance p1, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation$DragForce;

    invoke-direct {p1}, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation$DragForce;-><init>()V

    iput-object p1, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation;->mFlingForce:Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation$DragForce;

    iget-object p1, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation;->mFlingForce:Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation$DragForce;

    invoke-virtual {p0}, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation;->getValueThreshold()F

    move-result v0

    invoke-virtual {p1, v0}, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation$DragForce;->setValueThreshold(F)V

    iput-object p2, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation;->mFinalValueListener:Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation$FinalValueListener;

    return-void
.end method

.method private predictTimeWithVelocity(F)F
    .locals 4

    iget v0, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation;->mVelocity:F

    div-float/2addr p1, v0

    float-to-double v0, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    const-wide v2, 0x408f400000000000L    # 1000.0

    mul-double/2addr v0, v2

    iget-object p1, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation;->mFlingForce:Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation$DragForce;

    invoke-static {p1}, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation$DragForce;->access$000(Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation$DragForce;)F

    move-result p1

    float-to-double v2, p1

    div-double/2addr v0, v2

    double-to-float p1, v0

    return p1
.end method


# virtual methods
.method isAtEquilibrium(FF)Z
    .locals 1

    goto/32 :goto_c

    nop

    :goto_0
    goto :goto_4

    :goto_1
    goto/32 :goto_e

    nop

    :goto_2
    cmpg-float v0, p1, v0

    goto/32 :goto_10

    nop

    :goto_3
    goto :goto_b

    :goto_4
    goto/32 :goto_a

    nop

    :goto_5
    iget-object v0, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation;->mFlingForce:Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation$DragForce;

    goto/32 :goto_8

    nop

    :goto_6
    if-nez p1, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_0

    nop

    :goto_7
    if-ltz v0, :cond_1

    goto/32 :goto_4

    :cond_1
    goto/32 :goto_d

    nop

    :goto_8
    invoke-virtual {v0, p1, p2}, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation$DragForce;->isAtEquilibrium(FF)Z

    move-result p1

    goto/32 :goto_6

    nop

    :goto_9
    cmpl-float v0, p1, v0

    goto/32 :goto_7

    nop

    :goto_a
    const/4 p1, 0x1

    :goto_b
    goto/32 :goto_f

    nop

    :goto_c
    iget v0, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation;->mMaxValue:F

    goto/32 :goto_9

    nop

    :goto_d
    iget v0, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation;->mMinValue:F

    goto/32 :goto_2

    nop

    :goto_e
    const/4 p1, 0x0

    goto/32 :goto_3

    nop

    :goto_f
    return p1

    :goto_10
    if-gtz v0, :cond_2

    goto/32 :goto_4

    :cond_2
    goto/32 :goto_5

    nop
.end method

.method public predictDuration()F
    .locals 2

    iget v0, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation;->mVelocity:F

    invoke-static {v0}, Ljava/lang/Math;->signum(F)F

    move-result v0

    iget-object v1, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation;->mFlingForce:Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation$DragForce;

    invoke-static {v1}, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation$DragForce;->access$100(Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation$DragForce;)F

    move-result v1

    mul-float/2addr v0, v1

    invoke-direct {p0, v0}, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation;->predictTimeWithVelocity(F)F

    move-result v0

    return v0
.end method

.method public predictNaturalDest()F
    .locals 4

    iget v0, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation;->mVelocity:F

    invoke-static {v0}, Ljava/lang/Math;->signum(F)F

    move-result v0

    iget v1, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation;->mValue:F

    iget v2, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation;->mVelocity:F

    iget-object v3, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation;->mFlingForce:Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation$DragForce;

    invoke-static {v3}, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation$DragForce;->access$000(Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation$DragForce;)F

    move-result v3

    div-float/2addr v2, v3

    sub-float/2addr v1, v2

    iget-object v2, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation;->mFlingForce:Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation$DragForce;

    invoke-static {v2}, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation$DragForce;->access$100(Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation$DragForce;)F

    move-result v2

    mul-float/2addr v0, v2

    iget-object v2, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation;->mFlingForce:Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation$DragForce;

    invoke-static {v2}, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation$DragForce;->access$000(Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation$DragForce;)F

    move-result v2

    div-float/2addr v0, v2

    add-float/2addr v1, v0

    return v1
.end method

.method public predictTimeTo(F)F
    .locals 2

    iget v0, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation;->mValue:F

    sub-float/2addr p1, v0

    iget v0, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation;->mVelocity:F

    iget-object v1, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation;->mFlingForce:Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation$DragForce;

    invoke-static {v1}, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation$DragForce;->access$000(Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation$DragForce;)F

    move-result v1

    div-float/2addr v0, v1

    add-float/2addr p1, v0

    iget-object v0, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation;->mFlingForce:Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation$DragForce;

    invoke-static {v0}, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation$DragForce;->access$000(Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation$DragForce;)F

    move-result v0

    mul-float/2addr p1, v0

    invoke-direct {p0, p1}, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation;->predictTimeWithVelocity(F)F

    move-result p1

    return p1
.end method

.method public setFriction(F)Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation;
    .locals 1

    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation;->mFlingForce:Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation$DragForce;

    invoke-virtual {v0, p1}, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation$DragForce;->setFrictionScalar(F)V

    return-object p0

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Friction must be positive"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public bridge synthetic setMaxValue(F)Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;
    .locals 0

    invoke-virtual {p0, p1}, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation;->setMaxValue(F)Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation;

    move-result-object p1

    return-object p1
.end method

.method public setMaxValue(F)Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation;
    .locals 0

    invoke-super {p0, p1}, Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;->setMaxValue(F)Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;

    return-object p0
.end method

.method public bridge synthetic setMinValue(F)Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;
    .locals 0

    invoke-virtual {p0, p1}, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation;->setMinValue(F)Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation;

    move-result-object p1

    return-object p1
.end method

.method public setMinValue(F)Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation;
    .locals 0

    invoke-super {p0, p1}, Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;->setMinValue(F)Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;

    return-object p0
.end method

.method public bridge synthetic setStartVelocity(F)Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;
    .locals 0

    invoke-virtual {p0, p1}, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation;->setStartVelocity(F)Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation;

    move-result-object p1

    return-object p1
.end method

.method public setStartVelocity(F)Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation;
    .locals 0

    invoke-super {p0, p1}, Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;->setStartVelocity(F)Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation;

    return-object p0
.end method

.method setValueThreshold(F)V
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation;->mFlingForce:Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation$DragForce;

    goto/32 :goto_1

    nop

    :goto_1
    invoke-virtual {v0, p1}, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation$DragForce;->setValueThreshold(F)V

    goto/32 :goto_2

    nop

    :goto_2
    return-void
.end method

.method updateValueAndVelocity(J)Z
    .locals 3

    goto/32 :goto_17

    nop

    :goto_0
    iget p2, p1, Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation$MassState;->mValue:F

    goto/32 :goto_18

    nop

    :goto_1
    iget v0, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation;->mValue:F

    goto/32 :goto_12

    nop

    :goto_2
    iget v1, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation;->mValue:F

    goto/32 :goto_21

    nop

    :goto_3
    const/4 p1, 0x0

    goto/32 :goto_13

    nop

    :goto_4
    iget v0, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation;->mVelocity:F

    goto/32 :goto_16

    nop

    :goto_5
    iget p2, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation;->mMinValue:F

    goto/32 :goto_c

    nop

    :goto_6
    if-gtz p1, :cond_0

    goto/32 :goto_9

    :cond_0
    goto/32 :goto_20

    nop

    :goto_7
    if-ltz p1, :cond_1

    goto/32 :goto_24

    :cond_1
    goto/32 :goto_d

    nop

    :goto_8
    return p2

    :goto_9
    goto/32 :goto_1f

    nop

    :goto_a
    iget v0, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation;->mMaxValue:F

    goto/32 :goto_14

    nop

    :goto_b
    iput p1, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation;->mValue:F

    goto/32 :goto_8

    nop

    :goto_c
    cmpg-float p1, p1, p2

    goto/32 :goto_10

    nop

    :goto_d
    iget p1, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation;->mMinValue:F

    goto/32 :goto_11

    nop

    :goto_e
    iput p1, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation;->mVelocity:F

    goto/32 :goto_15

    nop

    :goto_f
    if-nez p1, :cond_2

    goto/32 :goto_1a

    :cond_2
    goto/32 :goto_1d

    nop

    :goto_10
    const/4 p2, 0x1

    goto/32 :goto_7

    nop

    :goto_11
    iput p1, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation;->mValue:F

    goto/32 :goto_23

    nop

    :goto_12
    float-to-int v0, v0

    goto/32 :goto_1c

    nop

    :goto_13
    return p1

    :goto_14
    cmpl-float p1, p1, v0

    goto/32 :goto_6

    nop

    :goto_15
    iget p1, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation;->mValue:F

    goto/32 :goto_5

    nop

    :goto_16
    invoke-virtual {p0, p1, v0}, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation;->isAtEquilibrium(FF)Z

    move-result p1

    goto/32 :goto_f

    nop

    :goto_17
    iget-object v0, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation;->mFlingForce:Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation$DragForce;

    goto/32 :goto_2

    nop

    :goto_18
    iput p2, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation;->mValue:F

    goto/32 :goto_22

    nop

    :goto_19
    return p2

    :goto_1a
    goto/32 :goto_3

    nop

    :goto_1b
    iget p1, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation;->mValue:F

    goto/32 :goto_a

    nop

    :goto_1c
    invoke-interface {p1, v0}, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation$FinalValueListener;->onFinalValueArrived(I)V

    goto/32 :goto_19

    nop

    :goto_1d
    iget-object p1, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation;->mFinalValueListener:Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation$FinalValueListener;

    goto/32 :goto_1

    nop

    :goto_1e
    invoke-virtual {v0, v1, v2, p1, p2}, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation$DragForce;->updateValueAndVelocity(FFJ)Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation$MassState;

    move-result-object p1

    goto/32 :goto_0

    nop

    :goto_1f
    iget p1, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation;->mValue:F

    goto/32 :goto_4

    nop

    :goto_20
    iget p1, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation;->mMaxValue:F

    goto/32 :goto_b

    nop

    :goto_21
    iget v2, p0, Lmiuix/overscroller/internal/dynamicanimation/animation/FlingAnimation;->mVelocity:F

    goto/32 :goto_1e

    nop

    :goto_22
    iget p1, p1, Lmiuix/overscroller/internal/dynamicanimation/animation/DynamicAnimation$MassState;->mVelocity:F

    goto/32 :goto_e

    nop

    :goto_23
    return p2

    :goto_24
    goto/32 :goto_1b

    nop
.end method
