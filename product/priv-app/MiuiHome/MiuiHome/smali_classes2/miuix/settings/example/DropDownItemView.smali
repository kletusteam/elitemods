.class public Lmiuix/settings/example/DropDownItemView;
.super Landroid/widget/LinearLayout;


# instance fields
.field private height:I

.field private width:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput v0, p0, Lmiuix/settings/example/DropDownItemView;->width:I

    iput v0, p0, Lmiuix/settings/example/DropDownItemView;->height:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    const/4 v0, -0x1

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput v0, p0, Lmiuix/settings/example/DropDownItemView;->width:I

    iput v0, p0, Lmiuix/settings/example/DropDownItemView;->height:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    const/4 v0, -0x1

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput v0, p0, Lmiuix/settings/example/DropDownItemView;->width:I

    iput v0, p0, Lmiuix/settings/example/DropDownItemView;->height:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    iput v0, p0, Lmiuix/settings/example/DropDownItemView;->width:I

    iput v0, p0, Lmiuix/settings/example/DropDownItemView;->height:I

    return-void
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 2

    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    iget v0, p0, Lmiuix/settings/example/DropDownItemView;->width:I

    if-gtz v0, :cond_0

    iget v0, p0, Lmiuix/settings/example/DropDownItemView;->height:I

    if-lez v0, :cond_1

    :cond_0
    iget v0, p0, Lmiuix/settings/example/DropDownItemView;->width:I

    if-lez v0, :cond_2

    iget v0, p0, Lmiuix/settings/example/DropDownItemView;->width:I

    :goto_0
    iget v1, p0, Lmiuix/settings/example/DropDownItemView;->height:I

    if-lez v1, :cond_3

    iget v1, p0, Lmiuix/settings/example/DropDownItemView;->height:I

    :goto_1
    invoke-virtual {p0, v0, v1}, Lmiuix/settings/example/DropDownItemView;->setMeasuredDimension(II)V

    :cond_1
    return-void

    :cond_2
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    goto :goto_0

    :cond_3
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    goto :goto_1
.end method

.method public setHeight(I)V
    .locals 0

    iput p1, p0, Lmiuix/settings/example/DropDownItemView;->height:I

    return-void
.end method

.method public setWidth(I)V
    .locals 0

    iput p1, p0, Lmiuix/settings/example/DropDownItemView;->width:I

    return-void
.end method
