.class public Lmiuix/recyclerview/widget/MiuiDefaultItemAnimator;
.super Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator;


# static fields
.field public static sAttachedListener:Landroid/view/View$OnAttachStateChangeListener;

.field public static sSpeedConfig:Lmiuix/animation/base/AnimConfig;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lmiuix/recyclerview/widget/MiuiDefaultItemAnimator$1;

    invoke-direct {v0}, Lmiuix/recyclerview/widget/MiuiDefaultItemAnimator$1;-><init>()V

    sput-object v0, Lmiuix/recyclerview/widget/MiuiDefaultItemAnimator;->sAttachedListener:Landroid/view/View$OnAttachStateChangeListener;

    new-instance v0, Lmiuix/animation/base/AnimConfig;

    invoke-direct {v0}, Lmiuix/animation/base/AnimConfig;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lmiuix/animation/base/AnimConfig;->setFromSpeed(F)Lmiuix/animation/base/AnimConfig;

    move-result-object v0

    sput-object v0, Lmiuix/recyclerview/widget/MiuiDefaultItemAnimator;->sSpeedConfig:Lmiuix/animation/base/AnimConfig;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator;-><init>()V

    return-void
.end method


# virtual methods
.method animateAddImpl(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V
    .locals 7

    goto/32 :goto_13

    nop

    :goto_0
    aput-object v2, v1, v3

    goto/32 :goto_3

    nop

    :goto_1
    aput-object v2, v1, v3

    goto/32 :goto_1e

    nop

    :goto_2
    aput-object v5, v2, v0

    goto/32 :goto_1d

    nop

    :goto_3
    invoke-static {v1}, Lmiuix/animation/Folme;->useAt([Landroid/view/View;)Lmiuix/animation/IFolme;

    move-result-object v1

    goto/32 :goto_15

    nop

    :goto_4
    const/4 v6, 0x2

    goto/32 :goto_c

    nop

    :goto_5
    const/high16 v4, 0x3f800000    # 1.0f

    goto/32 :goto_16

    nop

    :goto_6
    invoke-interface {v1}, Lmiuix/animation/IFolme;->state()Lmiuix/animation/IStateStyle;

    move-result-object v1

    goto/32 :goto_11

    nop

    :goto_7
    invoke-direct {v3, p0, p1}, Lmiuix/recyclerview/widget/MiuiDefaultItemAnimator$4;-><init>(Lmiuix/recyclerview/widget/MiuiDefaultItemAnimator;Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V

    goto/32 :goto_a

    nop

    :goto_8
    new-array v1, v0, [Landroid/view/View;

    goto/32 :goto_19

    nop

    :goto_9
    aput-object v5, v2, v3

    goto/32 :goto_e

    nop

    :goto_a
    invoke-virtual {v2, v3, v0, v1}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/32 :goto_1f

    nop

    :goto_b
    const/4 v0, 0x1

    goto/32 :goto_8

    nop

    :goto_c
    aput-object v5, v2, v6

    goto/32 :goto_20

    nop

    :goto_d
    sget-object v5, Lmiuix/animation/property/ViewProperty;->ALPHA:Lmiuix/animation/property/ViewProperty;

    goto/32 :goto_9

    nop

    :goto_e
    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    goto/32 :goto_f

    nop

    :goto_f
    aput-object v3, v2, v0

    goto/32 :goto_21

    nop

    :goto_10
    const/4 v3, 0x0

    goto/32 :goto_0

    nop

    :goto_11
    new-array v2, v6, [Ljava/lang/Object;

    goto/32 :goto_d

    nop

    :goto_12
    aput-object v4, v2, v3

    goto/32 :goto_5

    nop

    :goto_13
    invoke-virtual {p0, p1}, Lmiuix/recyclerview/widget/MiuiDefaultItemAnimator;->notifyAddStarting(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V

    goto/32 :goto_b

    nop

    :goto_14
    new-array v1, v0, [Landroid/view/View;

    goto/32 :goto_1a

    nop

    :goto_15
    invoke-interface {v1}, Lmiuix/animation/IFolme;->state()Lmiuix/animation/IStateStyle;

    move-result-object v1

    goto/32 :goto_1b

    nop

    :goto_16
    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    goto/32 :goto_2

    nop

    :goto_17
    sget-object v4, Lmiuix/animation/property/ViewProperty;->ALPHA:Lmiuix/animation/property/ViewProperty;

    goto/32 :goto_12

    nop

    :goto_18
    new-instance v3, Lmiuix/recyclerview/widget/MiuiDefaultItemAnimator$4;

    goto/32 :goto_7

    nop

    :goto_19
    iget-object v2, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_10

    nop

    :goto_1a
    iget-object v2, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_1

    nop

    :goto_1b
    const/4 v2, 0x3

    goto/32 :goto_1c

    nop

    :goto_1c
    new-array v2, v2, [Ljava/lang/Object;

    goto/32 :goto_17

    nop

    :goto_1d
    sget-object v5, Lmiuix/recyclerview/widget/MiuiDefaultItemAnimator;->sSpeedConfig:Lmiuix/animation/base/AnimConfig;

    goto/32 :goto_4

    nop

    :goto_1e
    invoke-static {v1}, Lmiuix/animation/Folme;->useAt([Landroid/view/View;)Lmiuix/animation/IFolme;

    move-result-object v1

    goto/32 :goto_6

    nop

    :goto_1f
    return-void

    :goto_20
    invoke-interface {v1, v2}, Lmiuix/animation/IStateStyle;->to([Ljava/lang/Object;)Lmiuix/animation/IStateStyle;

    goto/32 :goto_14

    nop

    :goto_21
    invoke-interface {v1, v2}, Lmiuix/animation/IStateStyle;->predictDuration([Ljava/lang/Object;)J

    move-result-wide v0

    goto/32 :goto_22

    nop

    :goto_22
    iget-object v2, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_18

    nop
.end method

.method animateChangeImpl(Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$ChangeInfo;)V
    .locals 14

    goto/32 :goto_10

    nop

    :goto_0
    aput-object v12, v11, v9

    goto/32 :goto_60

    nop

    :goto_1
    const/4 v1, 0x0

    goto/32 :goto_63

    nop

    :goto_2
    iget p1, p1, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$ChangeInfo;->fromY:I

    goto/32 :goto_27

    nop

    :goto_3
    invoke-interface {p1, v0}, Lmiuix/animation/IStateStyle;->to([Ljava/lang/Object;)Lmiuix/animation/IStateStyle;

    goto/32 :goto_5

    nop

    :goto_4
    sget-object v12, Lmiuix/animation/property/ViewProperty;->TRANSLATION_Y:Lmiuix/animation/property/ViewProperty;

    goto/32 :goto_4e

    nop

    :goto_5
    new-array p1, v8, [Landroid/view/View;

    goto/32 :goto_f

    nop

    :goto_6
    aput-object v4, v0, v8

    goto/32 :goto_50

    nop

    :goto_7
    sget-object v4, Lmiuix/recyclerview/widget/MiuiDefaultItemAnimator;->sSpeedConfig:Lmiuix/animation/base/AnimConfig;

    goto/32 :goto_20

    nop

    :goto_8
    sget-object v4, Lmiuix/animation/property/ViewProperty;->TRANSLATION_X:Lmiuix/animation/property/ViewProperty;

    goto/32 :goto_14

    nop

    :goto_9
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    goto/32 :goto_41

    nop

    :goto_a
    const/4 v5, 0x4

    goto/32 :goto_29

    nop

    :goto_b
    sget-object v10, Lmiuix/recyclerview/widget/MiuiDefaultItemAnimator;->sAttachedListener:Landroid/view/View$OnAttachStateChangeListener;

    goto/32 :goto_4f

    nop

    :goto_c
    new-array v11, v4, [Ljava/lang/Object;

    goto/32 :goto_49

    nop

    :goto_d
    aput-object p1, v11, v6

    goto/32 :goto_56

    nop

    :goto_e
    invoke-static {p1}, Lmiuix/animation/Folme;->useAt([Landroid/view/View;)Lmiuix/animation/IFolme;

    move-result-object p1

    goto/32 :goto_33

    nop

    :goto_f
    aput-object v1, p1, v9

    goto/32 :goto_43

    nop

    :goto_10
    iget-object v0, p1, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$ChangeInfo;->oldHolder:Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    goto/32 :goto_1

    nop

    :goto_11
    goto/16 :goto_5e

    :goto_12
    goto/32 :goto_5d

    nop

    :goto_13
    if-nez v1, :cond_0

    goto/32 :goto_59

    :cond_0
    goto/32 :goto_45

    nop

    :goto_14
    aput-object v4, v0, v9

    goto/32 :goto_51

    nop

    :goto_15
    iget-object v1, v3, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    :goto_16
    goto/32 :goto_2f

    nop

    :goto_17
    new-array v10, v8, [Landroid/view/View;

    goto/32 :goto_5a

    nop

    :goto_18
    const/4 v8, 0x1

    goto/32 :goto_53

    nop

    :goto_19
    iget v12, p1, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$ChangeInfo;->toY:I

    goto/32 :goto_2

    nop

    :goto_1a
    aput-object v4, v0, v8

    goto/32 :goto_3a

    nop

    :goto_1b
    sget-object v12, Lmiuix/recyclerview/widget/MiuiDefaultItemAnimator;->sSpeedConfig:Lmiuix/animation/base/AnimConfig;

    goto/32 :goto_4b

    nop

    :goto_1c
    aput-object v4, v0, v7

    goto/32 :goto_9

    nop

    :goto_1d
    sub-int/2addr v12, v13

    goto/32 :goto_40

    nop

    :goto_1e
    if-nez v2, :cond_1

    goto/32 :goto_26

    :cond_1
    goto/32 :goto_2c

    nop

    :goto_1f
    aput-object v12, v11, v8

    goto/32 :goto_5b

    nop

    :goto_20
    aput-object v4, v0, v5

    goto/32 :goto_3

    nop

    :goto_21
    aput-object v4, v0, v9

    goto/32 :goto_67

    nop

    :goto_22
    invoke-interface {v10, v11}, Lmiuix/animation/IStateStyle;->to([Ljava/lang/Object;)Lmiuix/animation/IStateStyle;

    goto/32 :goto_17

    nop

    :goto_23
    aput-object v1, p1, v9

    goto/32 :goto_e

    nop

    :goto_24
    new-array p1, v8, [Landroid/view/View;

    goto/32 :goto_23

    nop

    :goto_25
    invoke-virtual {v2, p1, v10, v11}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    :goto_26
    goto/32 :goto_13

    nop

    :goto_27
    sub-int/2addr v12, p1

    goto/32 :goto_35

    nop

    :goto_28
    invoke-interface {v10}, Lmiuix/animation/IFolme;->state()Lmiuix/animation/IStateStyle;

    move-result-object v10

    goto/32 :goto_3b

    nop

    :goto_29
    const/4 v6, 0x3

    goto/32 :goto_5c

    nop

    :goto_2a
    iget v13, p1, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$ChangeInfo;->fromX:I

    goto/32 :goto_31

    nop

    :goto_2b
    sget-object v12, Lmiuix/animation/property/ViewProperty;->TRANSLATION_X:Lmiuix/animation/property/ViewProperty;

    goto/32 :goto_47

    nop

    :goto_2c
    invoke-virtual {p0, v0, v8}, Lmiuix/recyclerview/widget/MiuiDefaultItemAnimator;->notifyChangeStarting(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;Z)V

    goto/32 :goto_b

    nop

    :goto_2d
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    goto/32 :goto_1f

    nop

    :goto_2e
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    goto/32 :goto_3e

    nop

    :goto_2f
    const/4 v4, 0x5

    goto/32 :goto_a

    nop

    :goto_30
    aput-object v2, v10, v9

    goto/32 :goto_37

    nop

    :goto_31
    sub-int/2addr v12, v13

    goto/32 :goto_2d

    nop

    :goto_32
    iget v13, p1, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$ChangeInfo;->fromY:I

    goto/32 :goto_3c

    nop

    :goto_33
    invoke-interface {p1}, Lmiuix/animation/IFolme;->state()Lmiuix/animation/IStateStyle;

    move-result-object p1

    goto/32 :goto_48

    nop

    :goto_34
    iget v12, p1, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$ChangeInfo;->toX:I

    goto/32 :goto_2a

    nop

    :goto_35
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    goto/32 :goto_d

    nop

    :goto_36
    invoke-interface {v10}, Lmiuix/animation/IFolme;->state()Lmiuix/animation/IStateStyle;

    move-result-object v10

    goto/32 :goto_c

    nop

    :goto_37
    invoke-static {v10}, Lmiuix/animation/Folme;->useAt([Landroid/view/View;)Lmiuix/animation/IFolme;

    move-result-object v10

    goto/32 :goto_36

    nop

    :goto_38
    invoke-direct {p1, p0, v2, v3}, Lmiuix/recyclerview/widget/MiuiDefaultItemAnimator$6;-><init>(Lmiuix/recyclerview/widget/MiuiDefaultItemAnimator;Landroid/view/View;Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V

    goto/32 :goto_58

    nop

    :goto_39
    invoke-interface {p1}, Lmiuix/animation/IFolme;->state()Lmiuix/animation/IStateStyle;

    move-result-object p1

    goto/32 :goto_5f

    nop

    :goto_3a
    sget-object v4, Lmiuix/animation/property/ViewProperty;->TRANSLATION_Y:Lmiuix/animation/property/ViewProperty;

    goto/32 :goto_1c

    nop

    :goto_3b
    new-array v11, v5, [Ljava/lang/Object;

    goto/32 :goto_2b

    nop

    :goto_3c
    sub-int/2addr v12, v13

    goto/32 :goto_3f

    nop

    :goto_3d
    new-array v10, v8, [Landroid/view/View;

    goto/32 :goto_30

    nop

    :goto_3e
    aput-object v4, v0, v6

    goto/32 :goto_7

    nop

    :goto_3f
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    goto/32 :goto_55

    nop

    :goto_40
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    goto/32 :goto_61

    nop

    :goto_41
    aput-object v4, v0, v6

    goto/32 :goto_66

    nop

    :goto_42
    iget-object v3, p1, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$ChangeInfo;->newHolder:Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    goto/32 :goto_4a

    nop

    :goto_43
    invoke-static {p1}, Lmiuix/animation/Folme;->useAt([Landroid/view/View;)Lmiuix/animation/IFolme;

    move-result-object p1

    goto/32 :goto_39

    nop

    :goto_44
    invoke-direct {p1, p0, v2, v0}, Lmiuix/recyclerview/widget/MiuiDefaultItemAnimator$5;-><init>(Lmiuix/recyclerview/widget/MiuiDefaultItemAnimator;Landroid/view/View;Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V

    goto/32 :goto_25

    nop

    :goto_45
    invoke-virtual {p0, v3, v9}, Lmiuix/recyclerview/widget/MiuiDefaultItemAnimator;->notifyChangeStarting(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;Z)V

    goto/32 :goto_24

    nop

    :goto_46
    move-object v2, v1

    goto/32 :goto_11

    nop

    :goto_47
    aput-object v12, v11, v9

    goto/32 :goto_34

    nop

    :goto_48
    new-array v0, v4, [Ljava/lang/Object;

    goto/32 :goto_8

    nop

    :goto_49
    sget-object v12, Lmiuix/animation/property/ViewProperty;->TRANSLATION_X:Lmiuix/animation/property/ViewProperty;

    goto/32 :goto_0

    nop

    :goto_4a
    if-nez v3, :cond_2

    goto/32 :goto_16

    :cond_2
    goto/32 :goto_15

    nop

    :goto_4b
    aput-object v12, v11, v5

    goto/32 :goto_22

    nop

    :goto_4c
    aput-object v4, v0, v7

    goto/32 :goto_2e

    nop

    :goto_4d
    invoke-static {v10}, Lmiuix/animation/Folme;->useAt([Landroid/view/View;)Lmiuix/animation/IFolme;

    move-result-object v10

    goto/32 :goto_28

    nop

    :goto_4e
    aput-object v12, v11, v7

    goto/32 :goto_57

    nop

    :goto_4f
    invoke-virtual {v2, v10}, Landroid/view/View;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    goto/32 :goto_3d

    nop

    :goto_50
    sget-object v4, Lmiuix/animation/property/ViewProperty;->TRANSLATION_Y:Lmiuix/animation/property/ViewProperty;

    goto/32 :goto_4c

    nop

    :goto_51
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    goto/32 :goto_6

    nop

    :goto_52
    iget v13, p1, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$ChangeInfo;->fromX:I

    goto/32 :goto_1d

    nop

    :goto_53
    const/4 v9, 0x0

    goto/32 :goto_1e

    nop

    :goto_54
    sget-object v4, Lmiuix/animation/property/ViewProperty;->TRANSLATION_X:Lmiuix/animation/property/ViewProperty;

    goto/32 :goto_21

    nop

    :goto_55
    aput-object v12, v11, v6

    goto/32 :goto_1b

    nop

    :goto_56
    invoke-interface {v10, v11}, Lmiuix/animation/IStateStyle;->predictDuration([Ljava/lang/Object;)J

    move-result-wide v10

    goto/32 :goto_62

    nop

    :goto_57
    iget v12, p1, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$ChangeInfo;->toY:I

    goto/32 :goto_32

    nop

    :goto_58
    invoke-virtual {v1, p1, v4, v5}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    :goto_59
    goto/32 :goto_68

    nop

    :goto_5a
    aput-object v2, v10, v9

    goto/32 :goto_4d

    nop

    :goto_5b
    sget-object v12, Lmiuix/animation/property/ViewProperty;->TRANSLATION_Y:Lmiuix/animation/property/ViewProperty;

    goto/32 :goto_64

    nop

    :goto_5c
    const/4 v7, 0x2

    goto/32 :goto_18

    nop

    :goto_5d
    iget-object v2, v0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    :goto_5e
    goto/32 :goto_42

    nop

    :goto_5f
    new-array v0, v5, [Ljava/lang/Object;

    goto/32 :goto_54

    nop

    :goto_60
    iget v12, p1, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$ChangeInfo;->toX:I

    goto/32 :goto_52

    nop

    :goto_61
    aput-object v12, v11, v8

    goto/32 :goto_4

    nop

    :goto_62
    new-instance p1, Lmiuix/recyclerview/widget/MiuiDefaultItemAnimator$5;

    goto/32 :goto_44

    nop

    :goto_63
    if-eqz v0, :cond_3

    goto/32 :goto_12

    :cond_3
    goto/32 :goto_46

    nop

    :goto_64
    aput-object v12, v11, v7

    goto/32 :goto_19

    nop

    :goto_65
    new-instance p1, Lmiuix/recyclerview/widget/MiuiDefaultItemAnimator$6;

    goto/32 :goto_38

    nop

    :goto_66
    invoke-interface {p1, v0}, Lmiuix/animation/IStateStyle;->predictDuration([Ljava/lang/Object;)J

    move-result-wide v4

    goto/32 :goto_65

    nop

    :goto_67
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    goto/32 :goto_1a

    nop

    :goto_68
    return-void
.end method

.method animateMoveImpl(Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$MoveInfo;)V
    .locals 9

    goto/32 :goto_f

    nop

    :goto_0
    iget-object v3, v3, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_2b

    nop

    :goto_1
    aput-object v5, v3, v7

    goto/32 :goto_1e

    nop

    :goto_2
    iget-object v0, p1, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$MoveInfo;->holder:Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    goto/32 :goto_3

    nop

    :goto_3
    const/4 v1, 0x1

    goto/32 :goto_9

    nop

    :goto_4
    aput-object v3, v2, v4

    goto/32 :goto_8

    nop

    :goto_5
    aput-object v5, v3, v8

    goto/32 :goto_c

    nop

    :goto_6
    new-array v3, v8, [Ljava/lang/Object;

    goto/32 :goto_13

    nop

    :goto_7
    const/4 v6, 0x2

    goto/32 :goto_2d

    nop

    :goto_8
    invoke-static {v2}, Lmiuix/animation/Folme;->useAt([Landroid/view/View;)Lmiuix/animation/IFolme;

    move-result-object v2

    goto/32 :goto_28

    nop

    :goto_9
    new-array v2, v1, [Landroid/view/View;

    goto/32 :goto_27

    nop

    :goto_a
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    goto/32 :goto_17

    nop

    :goto_b
    aput-object v1, v3, v7

    goto/32 :goto_2a

    nop

    :goto_c
    invoke-interface {v2, v3}, Lmiuix/animation/IStateStyle;->to([Ljava/lang/Object;)Lmiuix/animation/IStateStyle;

    goto/32 :goto_11

    nop

    :goto_d
    const/4 v8, 0x4

    goto/32 :goto_5

    nop

    :goto_e
    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_1a

    nop

    :goto_f
    iget-object v0, p1, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$MoveInfo;->holder:Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    goto/32 :goto_30

    nop

    :goto_10
    sget-object v1, Lmiuix/animation/property/ViewProperty;->TRANSLATION_Y:Lmiuix/animation/property/ViewProperty;

    goto/32 :goto_23

    nop

    :goto_11
    new-array v2, v1, [Landroid/view/View;

    goto/32 :goto_2f

    nop

    :goto_12
    aput-object v5, v3, v1

    goto/32 :goto_10

    nop

    :goto_13
    sget-object v5, Lmiuix/animation/property/ViewProperty;->TRANSLATION_X:Lmiuix/animation/property/ViewProperty;

    goto/32 :goto_20

    nop

    :goto_14
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    goto/32 :goto_12

    nop

    :goto_15
    sget-object v5, Lmiuix/animation/property/ViewProperty;->TRANSLATION_Y:Lmiuix/animation/property/ViewProperty;

    goto/32 :goto_7

    nop

    :goto_16
    invoke-virtual {p1, v3, v1, v2}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/32 :goto_24

    nop

    :goto_17
    aput-object v5, v3, v1

    goto/32 :goto_15

    nop

    :goto_18
    const/4 v3, 0x5

    goto/32 :goto_1f

    nop

    :goto_19
    iget-object p1, p1, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$MoveInfo;->holder:Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    goto/32 :goto_e

    nop

    :goto_1a
    new-instance v3, Lmiuix/recyclerview/widget/MiuiDefaultItemAnimator$3;

    goto/32 :goto_1b

    nop

    :goto_1b
    invoke-direct {v3, p0, v0}, Lmiuix/recyclerview/widget/MiuiDefaultItemAnimator$3;-><init>(Lmiuix/recyclerview/widget/MiuiDefaultItemAnimator;Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V

    goto/32 :goto_16

    nop

    :goto_1c
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto/32 :goto_b

    nop

    :goto_1d
    iget-object v3, v3, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_4

    nop

    :goto_1e
    sget-object v5, Lmiuix/recyclerview/widget/MiuiDefaultItemAnimator;->sSpeedConfig:Lmiuix/animation/base/AnimConfig;

    goto/32 :goto_d

    nop

    :goto_1f
    new-array v3, v3, [Ljava/lang/Object;

    goto/32 :goto_22

    nop

    :goto_20
    aput-object v5, v3, v4

    goto/32 :goto_14

    nop

    :goto_21
    invoke-interface {v2}, Lmiuix/animation/IFolme;->state()Lmiuix/animation/IStateStyle;

    move-result-object v2

    goto/32 :goto_18

    nop

    :goto_22
    sget-object v5, Lmiuix/animation/property/ViewProperty;->TRANSLATION_X:Lmiuix/animation/property/ViewProperty;

    goto/32 :goto_2c

    nop

    :goto_23
    aput-object v1, v3, v6

    goto/32 :goto_1c

    nop

    :goto_24
    return-void

    :goto_25
    aput-object v3, v2, v4

    goto/32 :goto_2e

    nop

    :goto_26
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    goto/32 :goto_29

    nop

    :goto_27
    iget-object v3, p1, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$MoveInfo;->holder:Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    goto/32 :goto_0

    nop

    :goto_28
    invoke-interface {v2}, Lmiuix/animation/IFolme;->state()Lmiuix/animation/IStateStyle;

    move-result-object v2

    goto/32 :goto_6

    nop

    :goto_29
    const/4 v7, 0x3

    goto/32 :goto_1

    nop

    :goto_2a
    invoke-interface {v2, v3}, Lmiuix/animation/IStateStyle;->predictDuration([Ljava/lang/Object;)J

    move-result-wide v1

    goto/32 :goto_19

    nop

    :goto_2b
    const/4 v4, 0x0

    goto/32 :goto_25

    nop

    :goto_2c
    aput-object v5, v3, v4

    goto/32 :goto_a

    nop

    :goto_2d
    aput-object v5, v3, v6

    goto/32 :goto_26

    nop

    :goto_2e
    invoke-static {v2}, Lmiuix/animation/Folme;->useAt([Landroid/view/View;)Lmiuix/animation/IFolme;

    move-result-object v2

    goto/32 :goto_21

    nop

    :goto_2f
    iget-object v3, p1, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$MoveInfo;->holder:Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    goto/32 :goto_1d

    nop

    :goto_30
    invoke-virtual {p0, v0}, Lmiuix/recyclerview/widget/MiuiDefaultItemAnimator;->notifyMoveStarting(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V

    goto/32 :goto_2

    nop
.end method

.method animateRemoveImpl(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V
    .locals 7

    goto/32 :goto_18

    nop

    :goto_0
    aput-object v2, v1, v3

    goto/32 :goto_4

    nop

    :goto_1
    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    goto/32 :goto_13

    nop

    :goto_2
    return-void

    :goto_3
    invoke-interface {v1}, Lmiuix/animation/IFolme;->state()Lmiuix/animation/IStateStyle;

    move-result-object v1

    goto/32 :goto_17

    nop

    :goto_4
    invoke-static {v1}, Lmiuix/animation/Folme;->useAt([Landroid/view/View;)Lmiuix/animation/IFolme;

    move-result-object v1

    goto/32 :goto_1a

    nop

    :goto_5
    invoke-direct {v3, p0, p1}, Lmiuix/recyclerview/widget/MiuiDefaultItemAnimator$2;-><init>(Lmiuix/recyclerview/widget/MiuiDefaultItemAnimator;Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V

    goto/32 :goto_f

    nop

    :goto_6
    const/4 v0, 0x1

    goto/32 :goto_10

    nop

    :goto_7
    const/4 v6, 0x2

    goto/32 :goto_15

    nop

    :goto_8
    sget-object v5, Lmiuix/recyclerview/widget/MiuiDefaultItemAnimator;->sSpeedConfig:Lmiuix/animation/base/AnimConfig;

    goto/32 :goto_7

    nop

    :goto_9
    invoke-interface {v1, v2}, Lmiuix/animation/IStateStyle;->predictDuration([Ljava/lang/Object;)J

    move-result-wide v0

    goto/32 :goto_14

    nop

    :goto_a
    aput-object v5, v2, v3

    goto/32 :goto_e

    nop

    :goto_b
    new-array v2, v6, [Ljava/lang/Object;

    goto/32 :goto_19

    nop

    :goto_c
    new-array v2, v2, [Ljava/lang/Object;

    goto/32 :goto_23

    nop

    :goto_d
    new-array v1, v0, [Landroid/view/View;

    goto/32 :goto_1b

    nop

    :goto_e
    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    goto/32 :goto_24

    nop

    :goto_f
    invoke-virtual {v2, v3, v0, v1}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/32 :goto_2

    nop

    :goto_10
    new-array v1, v0, [Landroid/view/View;

    goto/32 :goto_22

    nop

    :goto_11
    const/4 v3, 0x0

    goto/32 :goto_1e

    nop

    :goto_12
    const/4 v4, 0x0

    goto/32 :goto_1

    nop

    :goto_13
    aput-object v5, v2, v0

    goto/32 :goto_8

    nop

    :goto_14
    iget-object v2, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_1d

    nop

    :goto_15
    aput-object v5, v2, v6

    goto/32 :goto_21

    nop

    :goto_16
    invoke-virtual {v0, v1}, Landroid/view/View;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    goto/32 :goto_6

    nop

    :goto_17
    const/4 v2, 0x3

    goto/32 :goto_c

    nop

    :goto_18
    invoke-virtual {p0, p1}, Lmiuix/recyclerview/widget/MiuiDefaultItemAnimator;->notifyRemoveStarting(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V

    goto/32 :goto_20

    nop

    :goto_19
    sget-object v5, Lmiuix/animation/property/ViewProperty;->ALPHA:Lmiuix/animation/property/ViewProperty;

    goto/32 :goto_a

    nop

    :goto_1a
    invoke-interface {v1}, Lmiuix/animation/IFolme;->state()Lmiuix/animation/IStateStyle;

    move-result-object v1

    goto/32 :goto_b

    nop

    :goto_1b
    iget-object v2, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_0

    nop

    :goto_1c
    sget-object v1, Lmiuix/recyclerview/widget/MiuiDefaultItemAnimator;->sAttachedListener:Landroid/view/View$OnAttachStateChangeListener;

    goto/32 :goto_16

    nop

    :goto_1d
    new-instance v3, Lmiuix/recyclerview/widget/MiuiDefaultItemAnimator$2;

    goto/32 :goto_5

    nop

    :goto_1e
    aput-object v2, v1, v3

    goto/32 :goto_1f

    nop

    :goto_1f
    invoke-static {v1}, Lmiuix/animation/Folme;->useAt([Landroid/view/View;)Lmiuix/animation/IFolme;

    move-result-object v1

    goto/32 :goto_3

    nop

    :goto_20
    iget-object v0, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_1c

    nop

    :goto_21
    invoke-interface {v1, v2}, Lmiuix/animation/IStateStyle;->to([Ljava/lang/Object;)Lmiuix/animation/IStateStyle;

    goto/32 :goto_d

    nop

    :goto_22
    iget-object v2, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_11

    nop

    :goto_23
    sget-object v4, Lmiuix/animation/property/ViewProperty;->ALPHA:Lmiuix/animation/property/ViewProperty;

    goto/32 :goto_25

    nop

    :goto_24
    aput-object v3, v2, v0

    goto/32 :goto_9

    nop

    :goto_25
    aput-object v4, v2, v3

    goto/32 :goto_12

    nop
.end method

.method public getAddDuration()J
    .locals 2

    const-wide/16 v0, 0x12c

    return-wide v0
.end method

.method public getChangeDuration()J
    .locals 2

    const-wide/16 v0, 0x12c

    return-wide v0
.end method

.method public getMoveDuration()J
    .locals 2

    const-wide/16 v0, 0x12c

    return-wide v0
.end method

.method public getRemoveDuration()J
    .locals 2

    const-wide/16 v0, 0x12c

    return-wide v0
.end method

.method prepareAdd(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    invoke-virtual {p1, v0}, Landroid/view/View;->setAlpha(F)V

    goto/32 :goto_2

    nop

    :goto_1
    invoke-virtual {p0, p1}, Lmiuix/recyclerview/widget/MiuiDefaultItemAnimator;->resetAnimation(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V

    goto/32 :goto_4

    nop

    :goto_2
    return-void

    :goto_3
    const/4 v0, 0x0

    goto/32 :goto_0

    nop

    :goto_4
    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_3

    nop
.end method

.method prepareChange(Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$ChangeInfo;)V
    .locals 5

    goto/32 :goto_11

    nop

    :goto_0
    return-void

    :goto_1
    invoke-virtual {v1}, Landroid/view/View;->getTranslationY()F

    move-result v1

    goto/32 :goto_5

    nop

    :goto_2
    invoke-virtual {p1, v0}, Landroid/view/View;->setTranslationY(F)V

    :goto_3
    goto/32 :goto_0

    nop

    :goto_4
    iget-object v4, p1, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$ChangeInfo;->oldHolder:Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    goto/32 :goto_10

    nop

    :goto_5
    iget-object v2, p1, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$ChangeInfo;->oldHolder:Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    goto/32 :goto_22

    nop

    :goto_6
    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationX(F)V

    goto/32 :goto_25

    nop

    :goto_7
    sub-int/2addr v2, v3

    goto/32 :goto_16

    nop

    :goto_8
    sub-int/2addr v3, v4

    goto/32 :goto_17

    nop

    :goto_9
    iget-object v0, p1, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$ChangeInfo;->newHolder:Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    goto/32 :goto_24

    nop

    :goto_a
    iget v3, p1, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$ChangeInfo;->toY:I

    goto/32 :goto_18

    nop

    :goto_b
    sub-float/2addr v3, v1

    goto/32 :goto_d

    nop

    :goto_c
    int-to-float v0, v0

    goto/32 :goto_2

    nop

    :goto_d
    float-to-int v3, v3

    goto/32 :goto_4

    nop

    :goto_e
    iget-object v0, p1, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$ChangeInfo;->newHolder:Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    goto/32 :goto_23

    nop

    :goto_f
    invoke-virtual {p0, v0}, Lmiuix/recyclerview/widget/MiuiDefaultItemAnimator;->resetAnimation(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V

    goto/32 :goto_9

    nop

    :goto_10
    iget-object v4, v4, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_12

    nop

    :goto_11
    iget-object v0, p1, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$ChangeInfo;->oldHolder:Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    goto/32 :goto_20

    nop

    :goto_12
    invoke-virtual {v4, v0}, Landroid/view/View;->setTranslationX(F)V

    goto/32 :goto_1e

    nop

    :goto_13
    iget-object v1, v1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_1

    nop

    :goto_14
    iget-object v1, p1, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$ChangeInfo;->oldHolder:Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    goto/32 :goto_13

    nop

    :goto_15
    sub-float/2addr v2, v0

    goto/32 :goto_1d

    nop

    :goto_16
    int-to-float v2, v2

    goto/32 :goto_15

    nop

    :goto_17
    int-to-float v3, v3

    goto/32 :goto_b

    nop

    :goto_18
    iget v4, p1, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$ChangeInfo;->fromY:I

    goto/32 :goto_8

    nop

    :goto_19
    int-to-float v1, v1

    goto/32 :goto_6

    nop

    :goto_1a
    neg-int v0, v3

    goto/32 :goto_c

    nop

    :goto_1b
    iget v3, p1, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$ChangeInfo;->fromX:I

    goto/32 :goto_7

    nop

    :goto_1c
    neg-int v1, v2

    goto/32 :goto_19

    nop

    :goto_1d
    float-to-int v2, v2

    goto/32 :goto_a

    nop

    :goto_1e
    iget-object v0, p1, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$ChangeInfo;->oldHolder:Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    goto/32 :goto_26

    nop

    :goto_1f
    iget v2, p1, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$ChangeInfo;->toX:I

    goto/32 :goto_1b

    nop

    :goto_20
    iget-object v0, v0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_29

    nop

    :goto_21
    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_1a

    nop

    :goto_22
    invoke-virtual {p0, v2}, Lmiuix/recyclerview/widget/MiuiDefaultItemAnimator;->resetAnimation(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V

    goto/32 :goto_1f

    nop

    :goto_23
    if-nez v0, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_28

    nop

    :goto_24
    iget-object v0, v0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_1c

    nop

    :goto_25
    iget-object p1, p1, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$ChangeInfo;->newHolder:Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    goto/32 :goto_21

    nop

    :goto_26
    iget-object v0, v0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_27

    nop

    :goto_27
    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    goto/32 :goto_e

    nop

    :goto_28
    iget-object v0, p1, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$ChangeInfo;->newHolder:Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    goto/32 :goto_f

    nop

    :goto_29
    invoke-virtual {v0}, Landroid/view/View;->getTranslationX()F

    move-result v0

    goto/32 :goto_14

    nop
.end method

.method prepareMove(Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$MoveInfo;)V
    .locals 3

    goto/32 :goto_1

    nop

    :goto_0
    invoke-virtual {v0, p1}, Landroid/view/View;->setTranslationY(F)V

    goto/32 :goto_d

    nop

    :goto_1
    iget-object v0, p1, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$MoveInfo;->holder:Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    goto/32 :goto_a

    nop

    :goto_2
    int-to-float p1, v1

    goto/32 :goto_0

    nop

    :goto_3
    iget v2, p1, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$MoveInfo;->toX:I

    goto/32 :goto_b

    nop

    :goto_4
    iget v1, p1, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$MoveInfo;->fromX:I

    goto/32 :goto_3

    nop

    :goto_5
    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationX(F)V

    goto/32 :goto_c

    nop

    :goto_6
    sub-int/2addr v1, p1

    goto/32 :goto_2

    nop

    :goto_7
    iget p1, p1, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$MoveInfo;->toY:I

    goto/32 :goto_6

    nop

    :goto_8
    iget v1, p1, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$MoveInfo;->fromY:I

    goto/32 :goto_7

    nop

    :goto_9
    iget-object v0, v0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_8

    nop

    :goto_a
    iget-object v0, v0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_4

    nop

    :goto_b
    sub-int/2addr v1, v2

    goto/32 :goto_e

    nop

    :goto_c
    iget-object v0, p1, Lmiuix/recyclerview/widget/MiuiBaseDefaultItemAnimator$MoveInfo;->holder:Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    goto/32 :goto_9

    nop

    :goto_d
    return-void

    :goto_e
    int-to-float v1, v1

    goto/32 :goto_5

    nop
.end method

.method resetAnimation(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V
    .locals 5

    goto/32 :goto_e

    nop

    :goto_0
    invoke-interface {v1, v2}, Lmiuix/animation/IStateStyle;->end([Ljava/lang/Object;)V

    goto/32 :goto_1

    nop

    :goto_1
    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_f

    nop

    :goto_2
    const/4 v0, 0x2

    goto/32 :goto_d

    nop

    :goto_3
    const/4 v2, 0x3

    goto/32 :goto_b

    nop

    :goto_4
    new-array v1, v0, [Landroid/view/View;

    goto/32 :goto_c

    nop

    :goto_5
    aput-object v4, v2, v3

    goto/32 :goto_9

    nop

    :goto_6
    const/4 v3, 0x0

    goto/32 :goto_a

    nop

    :goto_7
    invoke-static {v1}, Lmiuix/animation/Folme;->useAt([Landroid/view/View;)Lmiuix/animation/IFolme;

    move-result-object v1

    goto/32 :goto_15

    nop

    :goto_8
    aput-object v3, v2, v0

    goto/32 :goto_2

    nop

    :goto_9
    sget-object v3, Lmiuix/animation/property/ViewProperty;->TRANSLATION_Y:Lmiuix/animation/property/ViewProperty;

    goto/32 :goto_8

    nop

    :goto_a
    aput-object v2, v1, v3

    goto/32 :goto_7

    nop

    :goto_b
    new-array v2, v2, [Ljava/lang/Object;

    goto/32 :goto_11

    nop

    :goto_c
    iget-object v2, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto/32 :goto_6

    nop

    :goto_d
    sget-object v3, Lmiuix/animation/property/ViewProperty;->ALPHA:Lmiuix/animation/property/ViewProperty;

    goto/32 :goto_14

    nop

    :goto_e
    if-nez p1, :cond_0

    goto/32 :goto_10

    :cond_0
    goto/32 :goto_13

    nop

    :goto_f
    invoke-static {p1}, Lmiuix/recyclerview/widget/MiuiDefaultItemAnimator;->resetAnimation(Landroid/view/View;)V

    :goto_10
    goto/32 :goto_12

    nop

    :goto_11
    sget-object v4, Lmiuix/animation/property/ViewProperty;->TRANSLATION_X:Lmiuix/animation/property/ViewProperty;

    goto/32 :goto_5

    nop

    :goto_12
    return-void

    :goto_13
    const/4 v0, 0x1

    goto/32 :goto_4

    nop

    :goto_14
    aput-object v3, v2, v0

    goto/32 :goto_0

    nop

    :goto_15
    invoke-interface {v1}, Lmiuix/animation/IFolme;->state()Lmiuix/animation/IStateStyle;

    move-result-object v1

    goto/32 :goto_3

    nop
.end method
