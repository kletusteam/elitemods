.class public final Lnet/lucode/hackware/magicindicator/R$id;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/lucode/hackware/magicindicator/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final action0:I = 0x7f0a0030

.field public static final action_bar:I = 0x7f0a0031

.field public static final action_bar_activity_content:I = 0x7f0a0032

.field public static final action_bar_container:I = 0x7f0a0036

.field public static final action_bar_root:I = 0x7f0a003d

.field public static final action_bar_spinner:I = 0x7f0a003e

.field public static final action_bar_subtitle:I = 0x7f0a003f

.field public static final action_bar_title:I = 0x7f0a0041

.field public static final action_context_bar:I = 0x7f0a0045

.field public static final action_divider:I = 0x7f0a0047

.field public static final action_menu_divider:I = 0x7f0a0049

.field public static final action_menu_presenter:I = 0x7f0a004c

.field public static final action_mode_bar:I = 0x7f0a004d

.field public static final action_mode_bar_stub:I = 0x7f0a004e

.field public static final action_mode_close_button:I = 0x7f0a004f

.field public static final activity_chooser_view_content:I = 0x7f0a0052

.field public static final alertTitle:I = 0x7f0a0057

.field public static final always:I = 0x7f0a0064

.field public static final beginning:I = 0x7f0a008a

.field public static final buttonPanel:I = 0x7f0a00a9

.field public static final cancel_action:I = 0x7f0a00ad

.field public static final checkbox:I = 0x7f0a00bd

.field public static final chronometer:I = 0x7f0a00ca

.field public static final collapseActionView:I = 0x7f0a00d7

.field public static final contentPanel:I = 0x7f0a00e0

.field public static final custom:I = 0x7f0a00eb

.field public static final customPanel:I = 0x7f0a00ec

.field public static final decor_content_parent:I = 0x7f0a00f6

.field public static final default_activity_button:I = 0x7f0a00f8

.field public static final disableHome:I = 0x7f0a0116

.field public static final edit_query:I = 0x7f0a0144

.field public static final end:I = 0x7f0a014c

.field public static final end_padder:I = 0x7f0a014f

.field public static final expand_activities_button:I = 0x7f0a0156

.field public static final expanded_menu:I = 0x7f0a0157

.field public static final home:I = 0x7f0a01aa

.field public static final homeAsUp:I = 0x7f0a01ab

.field public static final icon:I = 0x7f0a01b5

.field public static final ifRoom:I = 0x7f0a01c2

.field public static final image:I = 0x7f0a01c5

.field public static final indicator_container:I = 0x7f0a01c8

.field public static final info:I = 0x7f0a01cb

.field public static final line1:I = 0x7f0a01f6

.field public static final line3:I = 0x7f0a01f7

.field public static final listMode:I = 0x7f0a01f9

.field public static final list_item:I = 0x7f0a01fa

.field public static final media_actions:I = 0x7f0a0220

.field public static final middle:I = 0x7f0a0238

.field public static final multiply:I = 0x7f0a0268

.field public static final never:I = 0x7f0a0272

.field public static final none:I = 0x7f0a0275

.field public static final normal:I = 0x7f0a0276

.field public static final parentPanel:I = 0x7f0a0289

.field public static final progress_circular:I = 0x7f0a02a2

.field public static final progress_horizontal:I = 0x7f0a02a3

.field public static final radio:I = 0x7f0a02a9

.field public static final screen:I = 0x7f0a02dd

.field public static final scrollIndicatorDown:I = 0x7f0a02e1

.field public static final scrollIndicatorUp:I = 0x7f0a02e2

.field public static final scrollView:I = 0x7f0a02e3

.field public static final scroll_view:I = 0x7f0a02e4

.field public static final search_badge:I = 0x7f0a02e8

.field public static final search_bar:I = 0x7f0a02e9

.field public static final search_button:I = 0x7f0a02f6

.field public static final search_close_btn:I = 0x7f0a02f8

.field public static final search_edit_frame:I = 0x7f0a02fa

.field public static final search_go_btn:I = 0x7f0a02fb

.field public static final search_mag_icon:I = 0x7f0a0309

.field public static final search_plate:I = 0x7f0a030d

.field public static final search_src_text:I = 0x7f0a0310

.field public static final search_voice_btn:I = 0x7f0a0313

.field public static final select_dialog_listview:I = 0x7f0a031c

.field public static final shortcut:I = 0x7f0a0325

.field public static final showCustom:I = 0x7f0a0328

.field public static final showHome:I = 0x7f0a0329

.field public static final showTitle:I = 0x7f0a032a

.field public static final spacer:I = 0x7f0a0346

.field public static final split_action_bar:I = 0x7f0a034c

.field public static final src_atop:I = 0x7f0a0353

.field public static final src_in:I = 0x7f0a0354

.field public static final src_over:I = 0x7f0a0355

.field public static final status_bar_latest_event_content:I = 0x7f0a035e

.field public static final submit_area:I = 0x7f0a0363

.field public static final tabMode:I = 0x7f0a036e

.field public static final text:I = 0x7f0a038c

.field public static final text2:I = 0x7f0a038d

.field public static final textSpacerNoButtons:I = 0x7f0a0390

.field public static final time:I = 0x7f0a03a1

.field public static final title:I = 0x7f0a03a5

.field public static final title_container:I = 0x7f0a03a8

.field public static final title_template:I = 0x7f0a03ab

.field public static final topPanel:I = 0x7f0a03ae

.field public static final up:I = 0x7f0a03e0

.field public static final useLogo:I = 0x7f0a03e1

.field public static final withText:I = 0x7f0a0403

.field public static final wrap_content:I = 0x7f0a0414
