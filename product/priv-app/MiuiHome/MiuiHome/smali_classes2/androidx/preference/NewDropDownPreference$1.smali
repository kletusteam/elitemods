.class Landroidx/preference/NewDropDownPreference$1;
.super Ljava/lang/Object;
.source "NewDropDownPreference.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroidx/preference/NewDropDownPreference;->onBindViewHolder(Landroidx/preference/PreferenceViewHolder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroidx/preference/NewDropDownPreference;

.field final synthetic val$holder:Landroidx/preference/PreferenceViewHolder;


# direct methods
.method constructor <init>(Landroidx/preference/NewDropDownPreference;Landroidx/preference/PreferenceViewHolder;)V
    .locals 0

    iput-object p1, p0, Landroidx/preference/NewDropDownPreference$1;->this$0:Landroidx/preference/NewDropDownPreference;

    iput-object p2, p0, Landroidx/preference/NewDropDownPreference$1;->val$holder:Landroidx/preference/PreferenceViewHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ClickableViewAccessibility"
        }
    .end annotation

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return v5

    :pswitch_0
    new-array v1, v5, [Landroid/view/View;

    aput-object p1, v1, v4

    invoke-static {v1}, Lmiuix/animation/Folme;->useAt([Landroid/view/View;)Lmiuix/animation/IFolme;

    move-result-object v1

    invoke-interface {v1}, Lmiuix/animation/IFolme;->touch()Lmiuix/animation/ITouchStyle;

    move-result-object v1

    const/high16 v2, 0x3f800000    # 1.0f

    new-array v3, v4, [Lmiuix/animation/ITouchStyle$TouchType;

    invoke-interface {v1, v2, v3}, Lmiuix/animation/ITouchStyle;->setScale(F[Lmiuix/animation/ITouchStyle$TouchType;)Lmiuix/animation/ITouchStyle;

    move-result-object v1

    new-array v2, v5, [Lmiuix/animation/base/AnimConfig;

    new-instance v3, Lmiuix/animation/base/AnimConfig;

    invoke-direct {v3}, Lmiuix/animation/base/AnimConfig;-><init>()V

    aput-object v3, v2, v4

    invoke-interface {v1, v2}, Lmiuix/animation/ITouchStyle;->touchDown([Lmiuix/animation/base/AnimConfig;)V

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Landroidx/preference/NewDropDownPreference$1;->this$0:Landroidx/preference/NewDropDownPreference;

    invoke-static {v1}, Landroidx/preference/NewDropDownPreference;->access$300(Landroidx/preference/NewDropDownPreference;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Landroidx/preference/NewDropDownPreference$1;->this$0:Landroidx/preference/NewDropDownPreference;

    invoke-static {v1}, Landroidx/preference/NewDropDownPreference;->access$400(Landroidx/preference/NewDropDownPreference;)Landroid/widget/PopupWindow;

    move-result-object v1

    iget-object v2, p0, Landroidx/preference/NewDropDownPreference$1;->val$holder:Landroidx/preference/PreferenceViewHolder;

    iget-object v2, v2, Landroidx/preference/PreferenceViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/widget/PopupWindow;->showAsDropDown(Landroid/view/View;)V

    iget-object v1, p0, Landroidx/preference/NewDropDownPreference$1;->this$0:Landroidx/preference/NewDropDownPreference;

    invoke-static {v1}, Landroidx/preference/NewDropDownPreference;->access$500(Landroidx/preference/NewDropDownPreference;)Landroid/widget/PopupWindow;

    move-result-object v1

    iget-object v2, p0, Landroidx/preference/NewDropDownPreference$1;->val$holder:Landroidx/preference/PreferenceViewHolder;

    iget-object v2, v2, Landroidx/preference/PreferenceViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {v1, v2, v3, v4}, Landroid/widget/PopupWindow;->showAsDropDown(Landroid/view/View;II)V

    goto :goto_0

    :cond_1
    new-array v1, v5, [Landroid/view/View;

    aput-object p1, v1, v4

    invoke-static {v1}, Lmiuix/animation/Folme;->useAt([Landroid/view/View;)Lmiuix/animation/IFolme;

    move-result-object v1

    invoke-interface {v1}, Lmiuix/animation/IFolme;->touch()Lmiuix/animation/ITouchStyle;

    move-result-object v1

    new-array v2, v5, [Lmiuix/animation/base/AnimConfig;

    new-instance v3, Lmiuix/animation/base/AnimConfig;

    invoke-direct {v3}, Lmiuix/animation/base/AnimConfig;-><init>()V

    aput-object v3, v2, v4

    invoke-interface {v1, v2}, Lmiuix/animation/ITouchStyle;->touchUp([Lmiuix/animation/base/AnimConfig;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
