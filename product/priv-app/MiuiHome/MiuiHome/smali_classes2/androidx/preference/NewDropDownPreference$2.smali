.class Landroidx/preference/NewDropDownPreference$2;
.super Ljava/lang/Object;
.source "NewDropDownPreference.java"

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroidx/preference/NewDropDownPreference;->createPopUp()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroidx/preference/NewDropDownPreference;

.field final synthetic val$contentView:Landroid/widget/LinearLayout;

.field final synthetic val$k:I

.field final synthetic val$layoutHeight:I


# direct methods
.method constructor <init>(Landroidx/preference/NewDropDownPreference;Landroid/widget/LinearLayout;II)V
    .locals 0

    iput-object p1, p0, Landroidx/preference/NewDropDownPreference$2;->this$0:Landroidx/preference/NewDropDownPreference;

    iput-object p2, p0, Landroidx/preference/NewDropDownPreference$2;->val$contentView:Landroid/widget/LinearLayout;

    iput p3, p0, Landroidx/preference/NewDropDownPreference$2;->val$k:I

    iput p4, p0, Landroidx/preference/NewDropDownPreference$2;->val$layoutHeight:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 6

    iget-object v0, p0, Landroidx/preference/NewDropDownPreference$2;->val$contentView:Landroid/widget/LinearLayout;

    iget-object v1, p0, Landroidx/preference/NewDropDownPreference$2;->this$0:Landroidx/preference/NewDropDownPreference;

    iget-object v2, p0, Landroidx/preference/NewDropDownPreference$2;->val$contentView:Landroid/widget/LinearLayout;

    iget-object v3, p0, Landroidx/preference/NewDropDownPreference$2;->this$0:Landroidx/preference/NewDropDownPreference;

    invoke-static {v3}, Landroidx/preference/NewDropDownPreference;->access$000(Landroidx/preference/NewDropDownPreference;)I

    move-result v3

    iget v4, p0, Landroidx/preference/NewDropDownPreference$2;->val$k:I

    mul-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    iget v4, p0, Landroidx/preference/NewDropDownPreference$2;->val$layoutHeight:I

    iget v5, p0, Landroidx/preference/NewDropDownPreference$2;->val$k:I

    mul-int/lit8 v5, v5, 0x2

    add-int/2addr v4, v5

    invoke-static {v1, v2, v3, v4}, Landroidx/preference/NewDropDownPreference;->access$600(Landroidx/preference/NewDropDownPreference;Landroid/view/View;II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method
