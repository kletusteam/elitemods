.class Landroidx/preference/NewDropDownPreference$5;
.super Ljava/lang/Object;
.source "NewDropDownPreference.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroidx/preference/NewDropDownPreference;->createPopUp()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroidx/preference/NewDropDownPreference;

.field final synthetic val$popupWindow:Landroid/widget/PopupWindow;

.field final synthetic val$popupWindow1:Landroid/widget/PopupWindow;


# direct methods
.method constructor <init>(Landroidx/preference/NewDropDownPreference;Landroid/widget/PopupWindow;Landroid/widget/PopupWindow;)V
    .locals 0

    iput-object p1, p0, Landroidx/preference/NewDropDownPreference$5;->this$0:Landroidx/preference/NewDropDownPreference;

    iput-object p2, p0, Landroidx/preference/NewDropDownPreference$5;->val$popupWindow:Landroid/widget/PopupWindow;

    iput-object p3, p0, Landroidx/preference/NewDropDownPreference$5;->val$popupWindow1:Landroid/widget/PopupWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ClickableViewAccessibility"
        }
    .end annotation

    iget-object v0, p0, Landroidx/preference/NewDropDownPreference$5;->val$popupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroidx/preference/NewDropDownPreference$5;->val$popupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    :cond_0
    iget-object v0, p0, Landroidx/preference/NewDropDownPreference$5;->val$popupWindow1:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroidx/preference/NewDropDownPreference$5;->val$popupWindow1:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    :cond_1
    const/4 v0, 0x1

    return v0
.end method
