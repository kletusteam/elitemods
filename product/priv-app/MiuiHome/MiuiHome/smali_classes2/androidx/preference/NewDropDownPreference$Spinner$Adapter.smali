.class Landroidx/preference/NewDropDownPreference$Spinner$Adapter;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;
.source "NewDropDownPreference.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroidx/preference/NewDropDownPreference$Spinner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Adapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroidx/preference/NewDropDownPreference$Spinner$Adapter$Holder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$Adapter",
        "<",
        "Landroidx/preference/NewDropDownPreference$Spinner$Adapter$Holder;",
        ">;"
    }
.end annotation


# instance fields
.field entries:[Ljava/lang/CharSequence;

.field icons:[I

.field listener:Landroidx/preference/NewDropDownPreference$onItemClickListener;

.field final synthetic this$1:Landroidx/preference/NewDropDownPreference$Spinner;


# direct methods
.method public constructor <init>(Landroidx/preference/NewDropDownPreference$Spinner;[Ljava/lang/CharSequence;[ILandroidx/preference/NewDropDownPreference$onItemClickListener;)V
    .locals 0

    iput-object p1, p0, Landroidx/preference/NewDropDownPreference$Spinner$Adapter;->this$1:Landroidx/preference/NewDropDownPreference$Spinner;

    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    iput-object p2, p0, Landroidx/preference/NewDropDownPreference$Spinner$Adapter;->entries:[Ljava/lang/CharSequence;

    iput-object p3, p0, Landroidx/preference/NewDropDownPreference$Spinner$Adapter;->icons:[I

    iput-object p4, p0, Landroidx/preference/NewDropDownPreference$Spinner$Adapter;->listener:Landroidx/preference/NewDropDownPreference$onItemClickListener;

    return-void
.end method


# virtual methods
.method public getItemCount()I
    .locals 1

    iget-object v0, p0, Landroidx/preference/NewDropDownPreference$Spinner$Adapter;->entries:[Ljava/lang/CharSequence;

    array-length v0, v0

    return v0
.end method

.method public onBindViewHolder(Landroidx/preference/NewDropDownPreference$Spinner$Adapter$Holder;I)V
    .locals 5
    .param p1    # Landroidx/preference/NewDropDownPreference$Spinner$Adapter$Holder;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseCompatLoadingForDrawables"
        }
    .end annotation

    const/4 v3, 0x0

    iget-object v2, p1, Landroidx/preference/NewDropDownPreference$Spinner$Adapter$Holder;->title:Landroid/widget/TextView;

    iget-object v4, p0, Landroidx/preference/NewDropDownPreference$Spinner$Adapter;->entries:[Ljava/lang/CharSequence;

    aget-object v4, v4, p2

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p1, Landroidx/preference/NewDropDownPreference$Spinner$Adapter$Holder;->title:Landroid/widget/TextView;

    iget-object v2, p0, Landroidx/preference/NewDropDownPreference$Spinner$Adapter;->this$1:Landroidx/preference/NewDropDownPreference$Spinner;

    iget-object v2, v2, Landroidx/preference/NewDropDownPreference$Spinner;->this$0:Landroidx/preference/NewDropDownPreference;

    invoke-static {v2}, Landroidx/preference/NewDropDownPreference;->access$200(Landroidx/preference/NewDropDownPreference;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v2, v3

    :goto_0
    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v2, p1, Landroidx/preference/NewDropDownPreference$Spinner$Adapter$Holder;->icon:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v4, p0, Landroidx/preference/NewDropDownPreference$Spinner$Adapter;->icons:[I

    aget v4, v4, p2

    invoke-virtual {v2, v4}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget-object v2, p1, Landroidx/preference/NewDropDownPreference$Spinner$Adapter$Holder;->icon:Landroid/widget/ImageView;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    iget-object v2, p1, Landroidx/preference/NewDropDownPreference$Spinner$Adapter$Holder;->icon:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    instance-of v2, v0, Landroid/graphics/drawable/AnimationDrawable;

    if-eqz v2, :cond_0

    move-object v1, v0

    check-cast v1, Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v1, v3}, Landroid/graphics/drawable/AnimationDrawable;->setOneShot(Z)V

    invoke-virtual {v1}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    :cond_0
    iget-object v2, p1, Landroidx/preference/NewDropDownPreference$Spinner$Adapter$Holder;->itemView:Landroid/view/View;

    new-instance v4, Landroidx/preference/NewDropDownPreference$Spinner$Adapter$1;

    invoke-direct {v4, p0, p2}, Landroidx/preference/NewDropDownPreference$Spinner$Adapter$1;-><init>(Landroidx/preference/NewDropDownPreference$Spinner$Adapter;I)V

    invoke-virtual {v2, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p1, Landroidx/preference/NewDropDownPreference$Spinner$Adapter$Holder;->itemView:Landroid/view/View;

    iget-object v4, p0, Landroidx/preference/NewDropDownPreference$Spinner$Adapter;->this$1:Landroidx/preference/NewDropDownPreference$Spinner;

    iget-object v4, v4, Landroidx/preference/NewDropDownPreference$Spinner;->this$0:Landroidx/preference/NewDropDownPreference;

    invoke-virtual {v4}, Landroidx/preference/NewDropDownPreference;->getValueIndex()I

    move-result v4

    invoke-static {v4, v3}, Ljava/lang/Math;->max(II)I

    move-result v4

    if-ne p2, v4, :cond_1

    const/4 v3, 0x1

    :cond_1
    invoke-virtual {v2, v3}, Landroid/view/View;->setSelected(Z)V

    return-void

    :cond_2
    const/16 v2, 0x8

    goto :goto_0
.end method

.method public bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 0
    .param p1    # Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseCompatLoadingForDrawables"
        }
    .end annotation

    check-cast p1, Landroidx/preference/NewDropDownPreference$Spinner$Adapter$Holder;

    invoke-virtual {p0, p1, p2}, Landroidx/preference/NewDropDownPreference$Spinner$Adapter;->onBindViewHolder(Landroidx/preference/NewDropDownPreference$Spinner$Adapter$Holder;I)V

    return-void
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/preference/NewDropDownPreference$Spinner$Adapter$Holder;
    .locals 4
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "miuix_appcompat_dropdown_item_view"

    invoke-static {v2, v3}, Landroid/Utils/Utils;->LayoutToID(Landroid/content/Context;Ljava/lang/String;)I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiuix/settings/example/DropDownItemView;

    iget-object v1, p0, Landroidx/preference/NewDropDownPreference$Spinner$Adapter;->this$1:Landroidx/preference/NewDropDownPreference$Spinner;

    iget-object v1, v1, Landroidx/preference/NewDropDownPreference$Spinner;->this$0:Landroidx/preference/NewDropDownPreference;

    invoke-static {v1}, Landroidx/preference/NewDropDownPreference;->access$000(Landroidx/preference/NewDropDownPreference;)I

    move-result v1

    invoke-virtual {v0, v1}, Lmiuix/settings/example/DropDownItemView;->setWidth(I)V

    iget-object v1, p0, Landroidx/preference/NewDropDownPreference$Spinner$Adapter;->this$1:Landroidx/preference/NewDropDownPreference$Spinner;

    iget-object v1, v1, Landroidx/preference/NewDropDownPreference$Spinner;->this$0:Landroidx/preference/NewDropDownPreference;

    invoke-static {v1}, Landroidx/preference/NewDropDownPreference;->access$100(Landroidx/preference/NewDropDownPreference;)I

    move-result v1

    invoke-virtual {v0, v1}, Lmiuix/settings/example/DropDownItemView;->setHeight(I)V

    new-instance v1, Landroidx/preference/NewDropDownPreference$Spinner$Adapter$Holder;

    invoke-direct {v1, p0, v0}, Landroidx/preference/NewDropDownPreference$Spinner$Adapter$Holder;-><init>(Landroidx/preference/NewDropDownPreference$Spinner$Adapter;Landroid/view/View;)V

    return-object v1
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 1
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    invoke-virtual {p0, p1, p2}, Landroidx/preference/NewDropDownPreference$Spinner$Adapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/preference/NewDropDownPreference$Spinner$Adapter$Holder;

    move-result-object v0

    return-object v0
.end method
