.class Landroidx/preference/NewDropDownPreference$Spinner;
.super Landroidx/recyclerview/widget/RecyclerView;
.source "NewDropDownPreference.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroidx/preference/NewDropDownPreference;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Spinner"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroidx/preference/NewDropDownPreference$Spinner$Adapter;
    }
.end annotation


# instance fields
.field final synthetic this$0:Landroidx/preference/NewDropDownPreference;


# direct methods
.method public constructor <init>(Landroidx/preference/NewDropDownPreference;Landroid/content/Context;[Ljava/lang/CharSequence;[ILandroidx/preference/NewDropDownPreference$onItemClickListener;)V
    .locals 0
    .param p1    # Landroidx/preference/NewDropDownPreference;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iput-object p1, p0, Landroidx/preference/NewDropDownPreference$Spinner;->this$0:Landroidx/preference/NewDropDownPreference;

    invoke-direct {p0, p2}, Landroidx/recyclerview/widget/RecyclerView;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, p3, p4, p5}, Landroidx/preference/NewDropDownPreference$Spinner;->init([Ljava/lang/CharSequence;[ILandroidx/preference/NewDropDownPreference$onItemClickListener;)V

    return-void
.end method


# virtual methods
.method public init([Ljava/lang/CharSequence;[ILandroidx/preference/NewDropDownPreference$onItemClickListener;)V
    .locals 2

    new-instance v0, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {p0}, Landroidx/preference/NewDropDownPreference$Spinner;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/LinearLayoutManager;->setOrientation(I)V

    invoke-virtual {p0, v0}, Landroidx/preference/NewDropDownPreference$Spinner;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    new-instance v1, Landroidx/preference/NewDropDownPreference$Spinner$Adapter;

    invoke-direct {v1, p0, p1, p2, p3}, Landroidx/preference/NewDropDownPreference$Spinner$Adapter;-><init>(Landroidx/preference/NewDropDownPreference$Spinner;[Ljava/lang/CharSequence;[ILandroidx/preference/NewDropDownPreference$onItemClickListener;)V

    invoke-virtual {p0, v1}, Landroidx/preference/NewDropDownPreference$Spinner;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    return-void
.end method
