.class Landroidx/preference/NewDropDownPreference$3;
.super Ljava/lang/Object;
.source "NewDropDownPreference.java"

# interfaces
.implements Landroidx/preference/NewDropDownPreference$onItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroidx/preference/NewDropDownPreference;->createPopUp()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroidx/preference/NewDropDownPreference;


# direct methods
.method constructor <init>(Landroidx/preference/NewDropDownPreference;)V
    .locals 0

    iput-object p1, p0, Landroidx/preference/NewDropDownPreference$3;->this$0:Landroidx/preference/NewDropDownPreference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(I)V
    .locals 3

    iget-object v1, p0, Landroidx/preference/NewDropDownPreference$3;->this$0:Landroidx/preference/NewDropDownPreference;

    invoke-static {v1}, Landroidx/preference/NewDropDownPreference;->access$700(Landroidx/preference/NewDropDownPreference;)[Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    iget-object v1, p0, Landroidx/preference/NewDropDownPreference$3;->this$0:Landroidx/preference/NewDropDownPreference;

    invoke-static {v1}, Landroidx/preference/NewDropDownPreference;->access$800(Landroidx/preference/NewDropDownPreference;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Landroidx/preference/NewDropDownPreference$3;->this$0:Landroidx/preference/NewDropDownPreference;

    invoke-static {v1, v0}, Landroidx/preference/NewDropDownPreference;->access$802(Landroidx/preference/NewDropDownPreference;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    iget-object v1, p0, Landroidx/preference/NewDropDownPreference$3;->this$0:Landroidx/preference/NewDropDownPreference;

    invoke-static {v1}, Landroidx/preference/NewDropDownPreference;->access$900(Landroidx/preference/NewDropDownPreference;)Landroidx/preference/MiuiPreferenceHelper;

    move-result-object v1

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroidx/preference/MiuiPreferenceHelper;->putStr(Ljava/lang/String;)V

    iget-object v1, p0, Landroidx/preference/NewDropDownPreference$3;->this$0:Landroidx/preference/NewDropDownPreference;

    invoke-static {v1}, Landroidx/preference/NewDropDownPreference;->access$900(Landroidx/preference/NewDropDownPreference;)Landroidx/preference/MiuiPreferenceHelper;

    move-result-object v1

    invoke-virtual {v1}, Landroidx/preference/MiuiPreferenceHelper;->sendIntent()V

    iget-object v1, p0, Landroidx/preference/NewDropDownPreference$3;->this$0:Landroidx/preference/NewDropDownPreference;

    iget-object v2, p0, Landroidx/preference/NewDropDownPreference$3;->this$0:Landroidx/preference/NewDropDownPreference;

    invoke-virtual {v2}, Landroidx/preference/NewDropDownPreference;->getSummary()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroidx/preference/NewDropDownPreference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Landroidx/preference/NewDropDownPreference$3;->this$0:Landroidx/preference/NewDropDownPreference;

    invoke-static {v1}, Landroidx/preference/NewDropDownPreference;->access$500(Landroidx/preference/NewDropDownPreference;)Landroid/widget/PopupWindow;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Landroidx/preference/NewDropDownPreference$3;->this$0:Landroidx/preference/NewDropDownPreference;

    invoke-static {v1}, Landroidx/preference/NewDropDownPreference;->access$500(Landroidx/preference/NewDropDownPreference;)Landroid/widget/PopupWindow;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->dismiss()V

    :cond_0
    iget-object v1, p0, Landroidx/preference/NewDropDownPreference$3;->this$0:Landroidx/preference/NewDropDownPreference;

    invoke-static {v1}, Landroidx/preference/NewDropDownPreference;->access$400(Landroidx/preference/NewDropDownPreference;)Landroid/widget/PopupWindow;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Landroidx/preference/NewDropDownPreference$3;->this$0:Landroidx/preference/NewDropDownPreference;

    invoke-static {v1}, Landroidx/preference/NewDropDownPreference;->access$400(Landroidx/preference/NewDropDownPreference;)Landroid/widget/PopupWindow;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->dismiss()V

    :cond_1
    return-void
.end method
