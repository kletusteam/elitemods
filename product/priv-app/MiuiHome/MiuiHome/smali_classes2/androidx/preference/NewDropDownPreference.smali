.class public Landroidx/preference/NewDropDownPreference;
.super Lmiuix/preference/DropDownPreference;
.source "NewDropDownPreference.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroidx/preference/NewDropDownPreference$SavedState;,
        Landroidx/preference/NewDropDownPreference$Spinner;,
        Landroidx/preference/NewDropDownPreference$onItemClickListener;
    }
.end annotation


# static fields
.field private static final APP_RESOURCE_TAG:Ljava/lang/String; = "http://schemas.android.com/apk/res-auto"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private entries:[Ljava/lang/CharSequence;

.field private helper:Landroidx/preference/MiuiPreferenceHelper;

.field private icons:[I

.field private isValidPreferense:Z

.field private itemHeight:I

.field private itemTitleVisible:Z

.field private itemWidth:I

.field private mDefValue:Ljava/lang/CharSequence;

.field private popupWindow:Landroid/widget/PopupWindow;

.field private popupWindowBack:Landroid/widget/PopupWindow;

.field private value:Ljava/lang/CharSequence;

.field private values:[Ljava/lang/CharSequence;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Landroidx/preference/NewDropDownPreference;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Landroidx/preference/NewDropDownPreference;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Lmiuix/preference/DropDownPreference;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroidx/preference/NewDropDownPreference;->isValidPreferense:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lmiuix/preference/DropDownPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroidx/preference/NewDropDownPreference;->isValidPreferense:Z

    invoke-direct {p0, p2}, Landroidx/preference/NewDropDownPreference;->initial(Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lmiuix/preference/DropDownPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroidx/preference/NewDropDownPreference;->isValidPreferense:Z

    invoke-direct {p0, p2}, Landroidx/preference/NewDropDownPreference;->initial(Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 1

    invoke-direct {p0, p1, p2, p3, p4}, Lmiuix/preference/DropDownPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroidx/preference/NewDropDownPreference;->isValidPreferense:Z

    invoke-direct {p0, p2}, Landroidx/preference/NewDropDownPreference;->initial(Landroid/util/AttributeSet;)V

    return-void
.end method

.method static synthetic access$000(Landroidx/preference/NewDropDownPreference;)I
    .locals 1

    iget v0, p0, Landroidx/preference/NewDropDownPreference;->itemWidth:I

    return v0
.end method

.method static synthetic access$100(Landroidx/preference/NewDropDownPreference;)I
    .locals 1

    iget v0, p0, Landroidx/preference/NewDropDownPreference;->itemHeight:I

    return v0
.end method

.method static synthetic access$200(Landroidx/preference/NewDropDownPreference;)Z
    .locals 1

    iget-boolean v0, p0, Landroidx/preference/NewDropDownPreference;->itemTitleVisible:Z

    return v0
.end method

.method static synthetic access$300(Landroidx/preference/NewDropDownPreference;)Z
    .locals 1

    iget-boolean v0, p0, Landroidx/preference/NewDropDownPreference;->isValidPreferense:Z

    return v0
.end method

.method static synthetic access$400(Landroidx/preference/NewDropDownPreference;)Landroid/widget/PopupWindow;
    .locals 1

    iget-object v0, p0, Landroidx/preference/NewDropDownPreference;->popupWindowBack:Landroid/widget/PopupWindow;

    return-object v0
.end method

.method static synthetic access$500(Landroidx/preference/NewDropDownPreference;)Landroid/widget/PopupWindow;
    .locals 1

    iget-object v0, p0, Landroidx/preference/NewDropDownPreference;->popupWindow:Landroid/widget/PopupWindow;

    return-object v0
.end method

.method static synthetic access$600(Landroidx/preference/NewDropDownPreference;Landroid/view/View;II)Landroid/graphics/drawable/Drawable;
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Landroidx/preference/NewDropDownPreference;->getBackground(Landroid/view/View;II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Landroidx/preference/NewDropDownPreference;)[Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Landroidx/preference/NewDropDownPreference;->values:[Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic access$800(Landroidx/preference/NewDropDownPreference;)Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Landroidx/preference/NewDropDownPreference;->value:Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic access$802(Landroidx/preference/NewDropDownPreference;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 0

    iput-object p1, p0, Landroidx/preference/NewDropDownPreference;->value:Ljava/lang/CharSequence;

    return-object p1
.end method

.method static synthetic access$900(Landroidx/preference/NewDropDownPreference;)Landroidx/preference/MiuiPreferenceHelper;
    .locals 1

    iget-object v0, p0, Landroidx/preference/NewDropDownPreference;->helper:Landroidx/preference/MiuiPreferenceHelper;

    return-object v0
.end method

.method private createPopUp()V
    .locals 14

    const/4 v13, -0x1

    invoke-virtual {p0}, Landroidx/preference/NewDropDownPreference;->getContext()Landroid/content/Context;

    move-result-object v2

    new-instance v8, Lmiuix/springback/view/SpringBackLayout;

    invoke-direct {v8, v2}, Lmiuix/springback/view/SpringBackLayout;-><init>(Landroid/content/Context;)V

    new-instance v6, Landroid/widget/LinearLayout;

    invoke-direct {v6, v2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iget v1, p0, Landroidx/preference/NewDropDownPreference;->itemHeight:I

    iget-object v3, p0, Landroidx/preference/NewDropDownPreference;->entries:[Ljava/lang/CharSequence;

    array-length v3, v3

    const/4 v4, 0x5

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    mul-int v9, v1, v3

    new-instance v10, Landroid/widget/LinearLayout$LayoutParams;

    iget v1, p0, Landroidx/preference/NewDropDownPreference;->itemWidth:I

    invoke-direct {v10, v1, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/16 v1, 0xa

    invoke-static {v2, v1}, Landroid/Utils/ImageUtils;->convertDpToPx(Landroid/content/Context;I)I

    move-result v7

    invoke-virtual {v10, v7, v7, v7, v7}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    invoke-virtual {v6, v8, v10}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v1, Landroidx/preference/NewDropDownPreference$2;

    invoke-direct {v1, p0, v6, v7, v9}, Landroidx/preference/NewDropDownPreference$2;-><init>(Landroidx/preference/NewDropDownPreference;Landroid/widget/LinearLayout;II)V

    invoke-virtual {v6, v1}, Landroid/widget/LinearLayout;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    new-instance v0, Landroidx/preference/NewDropDownPreference$Spinner;

    iget-object v3, p0, Landroidx/preference/NewDropDownPreference;->entries:[Ljava/lang/CharSequence;

    iget-object v4, p0, Landroidx/preference/NewDropDownPreference;->icons:[I

    new-instance v5, Landroidx/preference/NewDropDownPreference$3;

    invoke-direct {v5, p0}, Landroidx/preference/NewDropDownPreference$3;-><init>(Landroidx/preference/NewDropDownPreference;)V

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Landroidx/preference/NewDropDownPreference$Spinner;-><init>(Landroidx/preference/NewDropDownPreference;Landroid/content/Context;[Ljava/lang/CharSequence;[ILandroidx/preference/NewDropDownPreference$onItemClickListener;)V

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    iget v3, p0, Landroidx/preference/NewDropDownPreference;->itemWidth:I

    invoke-direct {v1, v3, v9}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v8, v0, v1}, Lmiuix/springback/view/SpringBackLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v8, v0}, Lmiuix/springback/view/SpringBackLayout;->setTarget(Landroid/view/View;)V

    const/4 v1, 0x2

    invoke-virtual {v8, v1}, Lmiuix/springback/view/SpringBackLayout;->setScrollOrientation(I)V

    new-instance v11, Landroid/widget/PopupWindow;

    invoke-direct {v11}, Landroid/widget/PopupWindow;-><init>()V

    iget v1, p0, Landroidx/preference/NewDropDownPreference;->itemWidth:I

    mul-int/lit8 v3, v7, 0x2

    add-int/2addr v1, v3

    invoke-virtual {v11, v1}, Landroid/widget/PopupWindow;->setWidth(I)V

    mul-int/lit8 v1, v7, 0x2

    add-int/2addr v1, v9

    invoke-virtual {v11, v1}, Landroid/widget/PopupWindow;->setHeight(I)V

    const/4 v1, 0x1

    invoke-virtual {v11, v1}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    const-string v3, "#00ffffff"

    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v3

    invoke-direct {v1, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v11, v1}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v11, v6}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    new-instance v1, Landroidx/preference/NewDropDownPreference$4;

    invoke-direct {v1, p0}, Landroidx/preference/NewDropDownPreference$4;-><init>(Landroidx/preference/NewDropDownPreference;)V

    invoke-virtual {v11, v1}, Landroid/widget/PopupWindow;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    iput-object v11, p0, Landroidx/preference/NewDropDownPreference;->popupWindow:Landroid/widget/PopupWindow;

    new-instance v12, Landroid/widget/PopupWindow;

    invoke-direct {v12}, Landroid/widget/PopupWindow;-><init>()V

    invoke-virtual {v12, v13}, Landroid/widget/PopupWindow;->setHeight(I)V

    invoke-virtual {v12, v13}, Landroid/widget/PopupWindow;->setWidth(I)V

    new-instance v1, Landroidx/preference/NewDropDownPreference$5;

    invoke-direct {v1, p0, v11, v12}, Landroidx/preference/NewDropDownPreference$5;-><init>(Landroidx/preference/NewDropDownPreference;Landroid/widget/PopupWindow;Landroid/widget/PopupWindow;)V

    invoke-virtual {v12, v1}, Landroid/widget/PopupWindow;->setTouchInterceptor(Landroid/view/View$OnTouchListener;)V

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    const-string v3, "#00ffffff"

    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v3

    invoke-direct {v1, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v12, v1}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    new-instance v1, Landroid/widget/FrameLayout;

    invoke-direct {v1, v2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    invoke-virtual {v12, v1}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    iput-object v12, p0, Landroidx/preference/NewDropDownPreference;->popupWindowBack:Landroid/widget/PopupWindow;

    return-void
.end method

.method private getBackground(Landroid/view/View;II)Landroid/graphics/drawable/Drawable;
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x1e

    if-le v1, v2, :cond_0

    new-instance v0, Landroid/graphics/drawable/LayerDrawable;

    const/4 v1, 0x2

    new-array v1, v1, [Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, p1}, Landroidx/preference/NewDropDownPreference;->getBlurDrawable(Landroid/view/View;)Lcom/android/internal/graphics/drawable/BackgroundBlurDrawable;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-direct {p0, p2, p3}, Landroidx/preference/NewDropDownPreference;->getBgPopUpDrawable(II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-direct {v0, v1}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    new-array v2, v4, [I

    invoke-virtual {p0}, Landroidx/preference/NewDropDownPreference;->isNightMode()Z

    move-result v1

    if-eqz v1, :cond_1

    const/high16 v1, -0x1000000

    :goto_1
    aput v1, v2, v3

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/GradientDrawable;->setColors([I)V

    const/high16 v1, 0x41f00000    # 30.0f

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    goto :goto_0

    :cond_1
    const/4 v1, -0x1

    goto :goto_1
.end method

.method private getBgPopUpDrawable(II)Landroid/graphics/drawable/Drawable;
    .locals 13

    const/4 v0, 0x0

    const/4 v2, 0x0

    const-string v1, "#00ffffff"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    const-string v3, "#ff0000ff"

    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v5

    const/16 v7, 0x8

    const/16 v10, 0x4b

    move v3, v2

    move-object v4, v0

    move v6, v2

    move v8, p1

    move v9, p2

    invoke-static/range {v0 .. v10}, Landroid/Utils/BitmapHelper$GraphicalValuesData;->create(Landroid/graphics/Bitmap;IIILandroid/Utils/BitmapHelper$GradientDirection;IIIIII)Landroid/Utils/BitmapHelper$GraphicalValuesData;

    move-result-object v12

    invoke-static {v12}, Landroid/Utils/BitmapHelper;->createRectangleWithRoundedCorners(Landroid/Utils/BitmapHelper$GraphicalValuesData;)Landroid/graphics/Bitmap;

    move-result-object v11

    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Landroidx/preference/NewDropDownPreference;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1, v11}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    return-object v0
.end method

.method private getBlurDrawable(Landroid/view/View;)Lcom/android/internal/graphics/drawable/BackgroundBlurDrawable;
    .locals 3

    const/4 v0, 0x0

    invoke-direct {p0, p1}, Landroidx/preference/NewDropDownPreference;->getRoot(Landroid/view/View;)Landroid/view/ViewRootImpl;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/view/ViewRootImpl;->createBackgroundBlurDrawable()Lcom/android/internal/graphics/drawable/BackgroundBlurDrawable;

    move-result-object v0

    const/16 v2, 0x32

    invoke-virtual {v0, v2}, Lcom/android/internal/graphics/drawable/BackgroundBlurDrawable;->setBlurRadius(I)V

    const/high16 v2, 0x41f00000    # 30.0f

    invoke-virtual {v0, v2}, Lcom/android/internal/graphics/drawable/BackgroundBlurDrawable;->setCornerRadius(F)V

    :cond_0
    return-object v0
.end method

.method private getRoot(Landroid/view/View;)Landroid/view/ViewRootImpl;
    .locals 5

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v3, "getViewRootImpl"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v2, p1, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Landroid/view/ViewRootImpl;

    move-object v1, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v1

    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method private initial(Landroid/util/AttributeSet;)V
    .locals 12
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "RestrictedApi"
        }
    .end annotation

    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v11, -0x1

    invoke-virtual {p0}, Landroidx/preference/NewDropDownPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v9, Landroidx/preference/MiuiPreferenceHelper;

    invoke-virtual {p0}, Landroidx/preference/NewDropDownPreference;->getContext()Landroid/content/Context;

    move-result-object v10

    invoke-direct {v9, v10, p1}, Landroidx/preference/MiuiPreferenceHelper;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v9, p0, Landroidx/preference/NewDropDownPreference;->helper:Landroidx/preference/MiuiPreferenceHelper;

    const-string v9, "http://schemas.android.com/apk/res-auto"

    const-string v10, "entryValues"

    invoke-interface {p1, v9, v10, v11}, Landroid/util/AttributeSet;->getAttributeResourceValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v6

    const-string v9, "http://schemas.android.com/apk/res-auto"

    const-string v10, "entries"

    invoke-interface {p1, v9, v10, v11}, Landroid/util/AttributeSet;->getAttributeResourceValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v4

    const-string v9, "http://schemas.android.com/apk/res-auto"

    const-string v10, "entryIcons"

    invoke-interface {p1, v9, v10, v11}, Landroid/util/AttributeSet;->getAttributeResourceValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v5

    iget-object v9, p0, Landroidx/preference/NewDropDownPreference;->helper:Landroidx/preference/MiuiPreferenceHelper;

    const-string v10, "itemHeight"

    invoke-virtual {v9, v10, v11}, Landroidx/preference/MiuiPreferenceHelper;->getAttributeInt(Ljava/lang/String;I)I

    move-result v9

    iput v9, p0, Landroidx/preference/NewDropDownPreference;->itemHeight:I

    iget-object v9, p0, Landroidx/preference/NewDropDownPreference;->helper:Landroidx/preference/MiuiPreferenceHelper;

    const-string v10, "itemWidth"

    invoke-virtual {v9, v10, v11}, Landroidx/preference/MiuiPreferenceHelper;->getAttributeInt(Ljava/lang/String;I)I

    move-result v9

    iput v9, p0, Landroidx/preference/NewDropDownPreference;->itemWidth:I

    iget-object v9, p0, Landroidx/preference/NewDropDownPreference;->helper:Landroidx/preference/MiuiPreferenceHelper;

    const-string v10, "itemTitleVisible"

    invoke-virtual {v9, v10, v7}, Landroidx/preference/MiuiPreferenceHelper;->getAttributeBool(Ljava/lang/String;Z)Z

    move-result v9

    iput-boolean v9, p0, Landroidx/preference/NewDropDownPreference;->itemTitleVisible:Z

    iget v9, p0, Landroidx/preference/NewDropDownPreference;->itemWidth:I

    if-gez v9, :cond_0

    iget v9, p0, Landroidx/preference/NewDropDownPreference;->itemHeight:I

    if-lez v9, :cond_0

    iget v9, p0, Landroidx/preference/NewDropDownPreference;->itemHeight:I

    iput v9, p0, Landroidx/preference/NewDropDownPreference;->itemWidth:I

    :cond_0
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9, v6}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Landroidx/preference/NewDropDownPreference;->values:[Ljava/lang/CharSequence;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Landroidx/preference/NewDropDownPreference;->entries:[Ljava/lang/CharSequence;

    if-lez v5, :cond_2

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9, v5}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/TypedArray;->length()I

    move-result v9

    new-array v2, v9, [I

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->length()I

    move-result v9

    if-ge v1, v9, :cond_1

    invoke-virtual {v3, v1, v8}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v9

    aput v9, v2, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    iput-object v2, p0, Landroidx/preference/NewDropDownPreference;->icons:[I

    :cond_2
    iget-object v9, p0, Landroidx/preference/NewDropDownPreference;->values:[Ljava/lang/CharSequence;

    if-eqz v9, :cond_3

    iget-object v9, p0, Landroidx/preference/NewDropDownPreference;->entries:[Ljava/lang/CharSequence;

    if-eqz v9, :cond_3

    iget-object v9, p0, Landroidx/preference/NewDropDownPreference;->values:[Ljava/lang/CharSequence;

    array-length v9, v9

    iget-object v10, p0, Landroidx/preference/NewDropDownPreference;->entries:[Ljava/lang/CharSequence;

    array-length v10, v10

    if-ne v9, v10, :cond_3

    :goto_1
    iput-boolean v7, p0, Landroidx/preference/NewDropDownPreference;->isValidPreferense:Z

    const/4 v7, 0x0

    invoke-virtual {p0, v7}, Landroidx/preference/NewDropDownPreference;->setSummary(Ljava/lang/CharSequence;)V

    return-void

    :cond_3
    move v7, v8

    goto :goto_1
.end method


# virtual methods
.method public getSummary()Ljava/lang/CharSequence;
    .locals 1

    invoke-virtual {p0}, Landroidx/preference/NewDropDownPreference;->getentries()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getValueIndex()I
    .locals 2

    iget-object v0, p0, Landroidx/preference/NewDropDownPreference;->values:[Ljava/lang/CharSequence;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Landroidx/preference/NewDropDownPreference;->value:Ljava/lang/CharSequence;

    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public getentries()Ljava/lang/CharSequence;
    .locals 4

    const-string v2, ""

    iget-boolean v3, p0, Landroidx/preference/NewDropDownPreference;->isValidPreferense:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Landroidx/preference/NewDropDownPreference;->entries:[Ljava/lang/CharSequence;

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iget-object v3, p0, Landroidx/preference/NewDropDownPreference;->values:[Ljava/lang/CharSequence;

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    iget-object v3, p0, Landroidx/preference/NewDropDownPreference;->value:Ljava/lang/CharSequence;

    invoke-interface {v1, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Landroidx/preference/NewDropDownPreference;->value:Ljava/lang/CharSequence;

    invoke-interface {v1, v3}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v3

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    :cond_0
    return-object v2
.end method

.method public getvalue()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Landroidx/preference/NewDropDownPreference;->value:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public isNightMode()Z
    .locals 2

    invoke-virtual {p0}, Landroidx/preference/NewDropDownPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->uiMode:I

    and-int/lit8 v0, v0, 0x30

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onBindViewHolder(Landroidx/preference/PreferenceViewHolder;)V
    .locals 2

    invoke-super {p0, p1}, Lmiuix/preference/DropDownPreference;->onBindViewHolder(Landroidx/preference/PreferenceViewHolder;)V

    iget-object v0, p1, Landroidx/preference/PreferenceViewHolder;->itemView:Landroid/view/View;

    const v1, 0x1020018

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p1, Landroidx/preference/PreferenceViewHolder;->itemView:Landroid/view/View;

    new-instance v1, Landroidx/preference/NewDropDownPreference$1;

    invoke-direct {v1, p0, p1}, Landroidx/preference/NewDropDownPreference$1;-><init>(Landroidx/preference/NewDropDownPreference;Landroidx/preference/PreferenceViewHolder;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    invoke-direct {p0}, Landroidx/preference/NewDropDownPreference;->createPopUp()V

    return-void
.end method

.method protected onClick()V
    .locals 0

    return-void
.end method

.method protected onGetDefaultValue(Landroid/content/res/TypedArray;I)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroidx/preference/NewDropDownPreference;->mDefValue:Ljava/lang/CharSequence;

    iget-object v0, p0, Landroidx/preference/NewDropDownPreference;->mDefValue:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    const-string v0, "0"

    iput-object v0, p0, Landroidx/preference/NewDropDownPreference;->mDefValue:Ljava/lang/CharSequence;

    :cond_0
    iget-object v0, p0, Landroidx/preference/NewDropDownPreference;->mDefValue:Ljava/lang/CharSequence;

    return-object v0
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 3

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-class v2, Landroidx/preference/NewDropDownPreference$SavedState;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    invoke-super {p0, p1}, Lmiuix/preference/DropDownPreference;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    :goto_0
    return-void

    :cond_1
    move-object v0, p1

    check-cast v0, Landroidx/preference/NewDropDownPreference$SavedState;

    invoke-virtual {v0}, Landroidx/preference/NewDropDownPreference$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-super {p0, v1}, Lmiuix/preference/DropDownPreference;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    iget-object v1, v0, Landroidx/preference/NewDropDownPreference$SavedState;->mValue:Ljava/lang/String;

    invoke-virtual {p0, v1}, Landroidx/preference/NewDropDownPreference;->setValue(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    invoke-super {p0}, Lmiuix/preference/DropDownPreference;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-virtual {p0}, Landroidx/preference/NewDropDownPreference;->isPersistent()Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Landroidx/preference/NewDropDownPreference$SavedState;

    invoke-direct {v1, v0}, Landroidx/preference/NewDropDownPreference$SavedState;-><init>(Landroid/os/Parcelable;)V

    invoke-virtual {p0}, Landroidx/preference/NewDropDownPreference;->getvalue()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Landroidx/preference/NewDropDownPreference$SavedState;->mValue:Ljava/lang/String;

    move-object v0, v1

    goto :goto_0
.end method

.method protected onSetInitialValue(Ljava/lang/Object;)V
    .locals 2

    iget-object v0, p0, Landroidx/preference/NewDropDownPreference;->helper:Landroidx/preference/MiuiPreferenceHelper;

    invoke-virtual {v0}, Landroidx/preference/MiuiPreferenceHelper;->isValidateKey()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroidx/preference/NewDropDownPreference;->helper:Landroidx/preference/MiuiPreferenceHelper;

    invoke-virtual {v0}, Landroidx/preference/MiuiPreferenceHelper;->getStr()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroidx/preference/NewDropDownPreference;->value:Ljava/lang/CharSequence;

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Landroidx/preference/NewDropDownPreference;->mDefValue:Ljava/lang/CharSequence;

    iput-object v0, p0, Landroidx/preference/NewDropDownPreference;->value:Ljava/lang/CharSequence;

    iget-object v0, p0, Landroidx/preference/NewDropDownPreference;->helper:Landroidx/preference/MiuiPreferenceHelper;

    iget-object v1, p0, Landroidx/preference/NewDropDownPreference;->value:Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroidx/preference/MiuiPreferenceHelper;->putStr(Ljava/lang/String;)V

    iget-object v0, p0, Landroidx/preference/NewDropDownPreference;->helper:Landroidx/preference/MiuiPreferenceHelper;

    invoke-virtual {v0}, Landroidx/preference/MiuiPreferenceHelper;->sendIntent()V

    goto :goto_0
.end method

.method public onSetInitialValue(ZLjava/lang/Object;)V
    .locals 0

    invoke-virtual {p0, p2}, Landroidx/preference/NewDropDownPreference;->onSetInitialValue(Ljava/lang/Object;)V

    return-void
.end method

.method public setValue(Ljava/lang/CharSequence;)V
    .locals 0

    if-eqz p1, :cond_0

    iput-object p1, p0, Landroidx/preference/NewDropDownPreference;->value:Ljava/lang/CharSequence;

    :cond_0
    return-void
.end method
