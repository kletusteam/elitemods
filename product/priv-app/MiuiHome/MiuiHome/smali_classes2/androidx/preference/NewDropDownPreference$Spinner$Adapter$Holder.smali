.class Landroidx/preference/NewDropDownPreference$Spinner$Adapter$Holder;
.super Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
.source "NewDropDownPreference.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroidx/preference/NewDropDownPreference$Spinner$Adapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Holder"
.end annotation


# instance fields
.field icon:Landroid/widget/ImageView;

.field final synthetic this$2:Landroidx/preference/NewDropDownPreference$Spinner$Adapter;

.field title:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroidx/preference/NewDropDownPreference$Spinner$Adapter;Landroid/view/View;)V
    .locals 1
    .param p1    # Landroidx/preference/NewDropDownPreference$Spinner$Adapter;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    iput-object p1, p0, Landroidx/preference/NewDropDownPreference$Spinner$Adapter$Holder;->this$2:Landroidx/preference/NewDropDownPreference$Spinner$Adapter;

    invoke-direct {p0, p2}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    const v0, 0x1020006

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Landroidx/preference/NewDropDownPreference$Spinner$Adapter$Holder;->icon:Landroid/widget/ImageView;

    const v0, 0x1020016

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Landroidx/preference/NewDropDownPreference$Spinner$Adapter$Holder;->title:Landroid/widget/TextView;

    return-void
.end method
