.class public Lmiui/app/IActivityTaskManagerExpose;
.super Ljava/lang/Object;


# static fields
.field private static final CLASS:Lcom/miui/expose/utils/ClassHolder;

.field private static final cancelRecentsAnimation:Lcom/miui/expose/utils/MethodHolder;

.field private static final startActivityFromRecents:Lcom/miui/expose/utils/MethodHolder;


# instance fields
.field private final instance:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    new-instance v0, Lcom/miui/expose/utils/ClassHolder;

    const-string v1, "android.app.IActivityTaskManager"

    invoke-direct {v0, v1}, Lcom/miui/expose/utils/ClassHolder;-><init>(Ljava/lang/String;)V

    sput-object v0, Lmiui/app/IActivityTaskManagerExpose;->CLASS:Lcom/miui/expose/utils/ClassHolder;

    new-instance v0, Lcom/miui/expose/utils/MethodHolder;

    sget-object v1, Lmiui/app/IActivityTaskManagerExpose;->CLASS:Lcom/miui/expose/utils/ClassHolder;

    const-string v2, "startActivityFromRecents"

    const/4 v3, 0x1

    new-array v4, v3, [Ljava/lang/Class;

    const-class v5, Landroid/os/Bundle;

    const/4 v6, 0x0

    aput-object v5, v4, v6

    invoke-static {v4}, Lcom/miui/expose/utils/ParameterTypes;->of([Ljava/lang/Class;)Lcom/miui/expose/utils/ParameterTypes;

    move-result-object v4

    invoke-direct {v0, v1, v2, v4}, Lcom/miui/expose/utils/MethodHolder;-><init>(Lcom/miui/expose/utils/ClassHolder;Ljava/lang/String;Lcom/miui/expose/utils/ParameterTypes;)V

    sput-object v0, Lmiui/app/IActivityTaskManagerExpose;->startActivityFromRecents:Lcom/miui/expose/utils/MethodHolder;

    new-instance v0, Lcom/miui/expose/utils/MethodHolder;

    sget-object v1, Lmiui/app/IActivityTaskManagerExpose;->CLASS:Lcom/miui/expose/utils/ClassHolder;

    const-string v2, "cancelRecentsAnimation"

    new-array v3, v3, [Ljava/lang/Class;

    sget-object v4, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v4, v3, v6

    invoke-static {v3}, Lcom/miui/expose/utils/ParameterTypes;->of([Ljava/lang/Class;)Lcom/miui/expose/utils/ParameterTypes;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/miui/expose/utils/MethodHolder;-><init>(Lcom/miui/expose/utils/ClassHolder;Ljava/lang/String;Lcom/miui/expose/utils/ParameterTypes;)V

    sput-object v0, Lmiui/app/IActivityTaskManagerExpose;->cancelRecentsAnimation:Lcom/miui/expose/utils/MethodHolder;

    return-void
.end method

.method private constructor <init>(Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmiui/app/IActivityTaskManagerExpose;->instance:Ljava/lang/Object;

    return-void
.end method

.method public static box(Ljava/lang/Object;)Lmiui/app/IActivityTaskManagerExpose;
    .locals 1

    new-instance v0, Lmiui/app/IActivityTaskManagerExpose;

    invoke-direct {v0, p0}, Lmiui/app/IActivityTaskManagerExpose;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method


# virtual methods
.method public cancelRecentsAnimation(Z)V
    .locals 4

    sget-object v0, Lmiui/app/IActivityTaskManagerExpose;->cancelRecentsAnimation:Lcom/miui/expose/utils/MethodHolder;

    iget-object v1, p0, Lmiui/app/IActivityTaskManagerExpose;->instance:Ljava/lang/Object;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/miui/expose/utils/MethodHolder;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public unbox()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lmiui/app/IActivityTaskManagerExpose;->instance:Ljava/lang/Object;

    return-object v0
.end method
