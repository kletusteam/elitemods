.class public Lmiui/view/IRecentsAnimationControllerExpose;
.super Ljava/lang/Object;


# static fields
.field static final CLASS:Lcom/miui/expose/utils/ClassHolder;

.field private static final cleanupScreenshot:Lcom/miui/expose/utils/MethodHolder;


# instance fields
.field private final instance:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Lcom/miui/expose/utils/ClassHolder;

    const-string v1, "android.view.IRecentsAnimationController"

    invoke-direct {v0, v1}, Lcom/miui/expose/utils/ClassHolder;-><init>(Ljava/lang/String;)V

    sput-object v0, Lmiui/view/IRecentsAnimationControllerExpose;->CLASS:Lcom/miui/expose/utils/ClassHolder;

    new-instance v0, Lcom/miui/expose/utils/MethodHolder;

    sget-object v1, Lmiui/view/IRecentsAnimationControllerExpose;->CLASS:Lcom/miui/expose/utils/ClassHolder;

    const-string v2, "cleanupScreenshot"

    sget-object v3, Lcom/miui/expose/utils/ParameterTypes;->EMPTY:Lcom/miui/expose/utils/ParameterTypes;

    invoke-direct {v0, v1, v2, v3}, Lcom/miui/expose/utils/MethodHolder;-><init>(Lcom/miui/expose/utils/ClassHolder;Ljava/lang/String;Lcom/miui/expose/utils/ParameterTypes;)V

    sput-object v0, Lmiui/view/IRecentsAnimationControllerExpose;->cleanupScreenshot:Lcom/miui/expose/utils/MethodHolder;

    return-void
.end method

.method private constructor <init>(Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmiui/view/IRecentsAnimationControllerExpose;->instance:Ljava/lang/Object;

    return-void
.end method

.method public static box(Ljava/lang/Object;)Lmiui/view/IRecentsAnimationControllerExpose;
    .locals 1

    new-instance v0, Lmiui/view/IRecentsAnimationControllerExpose;

    invoke-direct {v0, p0}, Lmiui/view/IRecentsAnimationControllerExpose;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method


# virtual methods
.method public cleanupScreenshot()V
    .locals 3

    sget-object v0, Lmiui/view/IRecentsAnimationControllerExpose;->cleanupScreenshot:Lcom/miui/expose/utils/MethodHolder;

    iget-object v1, p0, Lmiui/view/IRecentsAnimationControllerExpose;->instance:Ljava/lang/Object;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/miui/expose/utils/MethodHolder;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
