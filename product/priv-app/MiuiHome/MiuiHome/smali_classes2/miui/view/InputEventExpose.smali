.class public Lmiui/view/InputEventExpose;
.super Ljava/lang/Object;


# static fields
.field static final CLASS:Lcom/miui/expose/utils/ClassHolder;

.field private static final setDisplayId:Lcom/miui/expose/utils/MethodHolder;


# instance fields
.field private final instance:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    new-instance v0, Lcom/miui/expose/utils/ClassHolder;

    const-string v1, "android.view.InputEvent"

    invoke-direct {v0, v1}, Lcom/miui/expose/utils/ClassHolder;-><init>(Ljava/lang/String;)V

    sput-object v0, Lmiui/view/InputEventExpose;->CLASS:Lcom/miui/expose/utils/ClassHolder;

    new-instance v0, Lcom/miui/expose/utils/MethodHolder;

    sget-object v1, Lmiui/view/InputEventExpose;->CLASS:Lcom/miui/expose/utils/ClassHolder;

    const-string v2, "setDisplayId"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Class;

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    invoke-static {v3}, Lcom/miui/expose/utils/ParameterTypes;->of([Ljava/lang/Class;)Lcom/miui/expose/utils/ParameterTypes;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/miui/expose/utils/MethodHolder;-><init>(Lcom/miui/expose/utils/ClassHolder;Ljava/lang/String;Lcom/miui/expose/utils/ParameterTypes;)V

    sput-object v0, Lmiui/view/InputEventExpose;->setDisplayId:Lcom/miui/expose/utils/MethodHolder;

    return-void
.end method

.method private constructor <init>(Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmiui/view/InputEventExpose;->instance:Ljava/lang/Object;

    return-void
.end method

.method public static box(Ljava/lang/Object;)Lmiui/view/InputEventExpose;
    .locals 1

    new-instance v0, Lmiui/view/InputEventExpose;

    invoke-direct {v0, p0}, Lmiui/view/InputEventExpose;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method


# virtual methods
.method public setDisplayId(I)V
    .locals 4

    sget-object v0, Lmiui/view/InputEventExpose;->setDisplayId:Lcom/miui/expose/utils/MethodHolder;

    iget-object v1, p0, Lmiui/view/InputEventExpose;->instance:Ljava/lang/Object;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/miui/expose/utils/MethodHolder;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
