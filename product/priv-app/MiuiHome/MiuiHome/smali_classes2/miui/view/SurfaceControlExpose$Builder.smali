.class public Lmiui/view/SurfaceControlExpose$Builder;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/view/SurfaceControlExpose;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# static fields
.field public static final CLASS:Lcom/miui/expose/utils/ClassHolder;

.field private static final setColorLayer:Lcom/miui/expose/utils/MethodHolder;


# instance fields
.field private final instance:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Lcom/miui/expose/utils/ClassHolder;

    const-string v1, "android.view.SurfaceControl$Builder"

    invoke-direct {v0, v1}, Lcom/miui/expose/utils/ClassHolder;-><init>(Ljava/lang/String;)V

    sput-object v0, Lmiui/view/SurfaceControlExpose$Builder;->CLASS:Lcom/miui/expose/utils/ClassHolder;

    new-instance v0, Lcom/miui/expose/utils/MethodHolder;

    sget-object v1, Lmiui/view/SurfaceControlExpose$Builder;->CLASS:Lcom/miui/expose/utils/ClassHolder;

    const-string v2, "setColorLayer"

    sget-object v3, Lcom/miui/expose/utils/ParameterTypes;->EMPTY:Lcom/miui/expose/utils/ParameterTypes;

    invoke-direct {v0, v1, v2, v3}, Lcom/miui/expose/utils/MethodHolder;-><init>(Lcom/miui/expose/utils/ClassHolder;Ljava/lang/String;Lcom/miui/expose/utils/ParameterTypes;)V

    sput-object v0, Lmiui/view/SurfaceControlExpose$Builder;->setColorLayer:Lcom/miui/expose/utils/MethodHolder;

    return-void
.end method

.method private constructor <init>(Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmiui/view/SurfaceControlExpose$Builder;->instance:Ljava/lang/Object;

    return-void
.end method

.method public static box(Ljava/lang/Object;)Lmiui/view/SurfaceControlExpose$Builder;
    .locals 1

    new-instance v0, Lmiui/view/SurfaceControlExpose$Builder;

    invoke-direct {v0, p0}, Lmiui/view/SurfaceControlExpose$Builder;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method


# virtual methods
.method public setColorLayer()Lmiui/view/SurfaceControlExpose$Builder;
    .locals 3

    sget-object v0, Lmiui/view/SurfaceControlExpose$Builder;->setColorLayer:Lcom/miui/expose/utils/MethodHolder;

    iget-object v1, p0, Lmiui/view/SurfaceControlExpose$Builder;->instance:Ljava/lang/Object;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/miui/expose/utils/MethodHolder;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public unbox()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lmiui/view/SurfaceControlExpose$Builder;->instance:Ljava/lang/Object;

    return-object v0
.end method
