.class Lmiui/home/lib/dialog/AlertController$AlertParams;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiui/home/lib/dialog/AlertController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "AlertParams"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiui/home/lib/dialog/AlertController$AlertParams$OnPrepareListViewListener;
    }
.end annotation


# instance fields
.field mAdapter:Landroid/widget/ListAdapter;

.field mCancelable:Z

.field mCheckBoxMessage:Ljava/lang/CharSequence;

.field mCheckedItem:I

.field mCheckedItems:[Z

.field mComment:Ljava/lang/CharSequence;

.field final mContext:Landroid/content/Context;

.field mCursor:Landroid/database/Cursor;

.field mCustomTitleView:Landroid/view/View;

.field mEnableDialogImmersive:Z

.field mEnableEnterAnim:Z

.field mExtraButtonList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lmiui/home/lib/dialog/AlertController$ButtonInfo;",
            ">;"
        }
    .end annotation
.end field

.field mHapticFeedbackEnabled:Z

.field mIcon:Landroid/graphics/drawable/Drawable;

.field mIconAttrId:I

.field mIconId:I

.field final mInflater:Landroid/view/LayoutInflater;

.field mIsChecked:Z

.field mIsCheckedColumn:Ljava/lang/String;

.field mIsMultiChoice:Z

.field mIsSingleChoice:Z

.field mItems:[Ljava/lang/CharSequence;

.field mLabelColumn:Ljava/lang/String;

.field mLiteVersion:I

.field mMessage:Ljava/lang/CharSequence;

.field mNegativeButtonListener:Landroid/content/DialogInterface$OnClickListener;

.field mNegativeButtonText:Ljava/lang/CharSequence;

.field mNeutralButtonListener:Landroid/content/DialogInterface$OnClickListener;

.field mNeutralButtonText:Ljava/lang/CharSequence;

.field mOnCancelListener:Landroid/content/DialogInterface$OnCancelListener;

.field mOnCheckboxClickListener:Landroid/content/DialogInterface$OnMultiChoiceClickListener;

.field mOnClickListener:Landroid/content/DialogInterface$OnClickListener;

.field mOnDialogShowAnimListener:Lmiui/home/lib/dialog/AlertDialog$OnDialogShowAnimListener;

.field mOnDismissListener:Landroid/content/DialogInterface$OnDismissListener;

.field mOnItemSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

.field mOnKeyListener:Landroid/content/DialogInterface$OnKeyListener;

.field mOnPrepareListViewListener:Lmiui/home/lib/dialog/AlertController$AlertParams$OnPrepareListViewListener;

.field mOnShowListener:Landroid/content/DialogInterface$OnShowListener;

.field mPositiveButtonListener:Landroid/content/DialogInterface$OnClickListener;

.field mPositiveButtonText:Ljava/lang/CharSequence;

.field mPreferLandscape:Z

.field mTitle:Ljava/lang/CharSequence;

.field mView:Landroid/view/View;

.field mViewLayoutResId:I


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lmiui/home/lib/dialog/AlertController$AlertParams;->mIconId:I

    iput v0, p0, Lmiui/home/lib/dialog/AlertController$AlertParams;->mIconAttrId:I

    const/4 v1, -0x1

    iput v1, p0, Lmiui/home/lib/dialog/AlertController$AlertParams;->mCheckedItem:I

    iput-object p1, p0, Lmiui/home/lib/dialog/AlertController$AlertParams;->mContext:Landroid/content/Context;

    const/4 v1, 0x1

    iput-boolean v1, p0, Lmiui/home/lib/dialog/AlertController$AlertParams;->mCancelable:Z

    invoke-static {}, Lmiuix/device/DeviceUtils;->isMiuiLiteV2()Z

    move-result v2

    xor-int/2addr v2, v1

    iput-boolean v2, p0, Lmiui/home/lib/dialog/AlertController$AlertParams;->mEnableDialogImmersive:Z

    invoke-static {}, Lmiuix/device/DeviceUtils;->getMiuiLiteVersion()I

    move-result v2

    iput v2, p0, Lmiui/home/lib/dialog/AlertController$AlertParams;->mLiteVersion:I

    iget v2, p0, Lmiui/home/lib/dialog/AlertController$AlertParams;->mLiteVersion:I

    if-gez v2, :cond_0

    iput v0, p0, Lmiui/home/lib/dialog/AlertController$AlertParams;->mLiteVersion:I

    :cond_0
    iput-boolean v1, p0, Lmiui/home/lib/dialog/AlertController$AlertParams;->mEnableEnterAnim:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiui/home/lib/dialog/AlertController$AlertParams;->mExtraButtonList:Ljava/util/List;

    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/LayoutInflater;

    iput-object p1, p0, Lmiui/home/lib/dialog/AlertController$AlertParams;->mInflater:Landroid/view/LayoutInflater;

    return-void
.end method

.method private createListView(Lmiui/home/lib/dialog/AlertController;)V
    .locals 11

    iget-object v0, p0, Lmiui/home/lib/dialog/AlertController$AlertParams;->mInflater:Landroid/view/LayoutInflater;

    iget v1, p1, Lmiui/home/lib/dialog/AlertController;->mListLayout:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Landroid/widget/ListView;

    iget-boolean v0, p0, Lmiui/home/lib/dialog/AlertController$AlertParams;->mIsMultiChoice:Z

    const/4 v8, 0x1

    if-eqz v0, :cond_1

    iget-object v3, p0, Lmiui/home/lib/dialog/AlertController$AlertParams;->mCursor:Landroid/database/Cursor;

    if-nez v3, :cond_0

    new-instance v9, Lmiui/home/lib/dialog/AlertController$AlertParams$1;

    iget-object v2, p0, Lmiui/home/lib/dialog/AlertController$AlertParams;->mContext:Landroid/content/Context;

    iget v3, p1, Lmiui/home/lib/dialog/AlertController;->mMultiChoiceItemLayout:I

    const v4, 0x1020014

    iget-object v5, p0, Lmiui/home/lib/dialog/AlertController$AlertParams;->mItems:[Ljava/lang/CharSequence;

    move-object v0, v9

    move-object v1, p0

    move-object v6, v7

    invoke-direct/range {v0 .. v6}, Lmiui/home/lib/dialog/AlertController$AlertParams$1;-><init>(Lmiui/home/lib/dialog/AlertController$AlertParams;Landroid/content/Context;II[Ljava/lang/CharSequence;Landroid/widget/ListView;)V

    goto :goto_1

    :cond_0
    new-instance v9, Lmiui/home/lib/dialog/AlertController$AlertParams$2;

    iget-object v2, p0, Lmiui/home/lib/dialog/AlertController$AlertParams;->mContext:Landroid/content/Context;

    const/4 v4, 0x0

    move-object v0, v9

    move-object v1, p0

    move-object v5, v7

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lmiui/home/lib/dialog/AlertController$AlertParams$2;-><init>(Lmiui/home/lib/dialog/AlertController$AlertParams;Landroid/content/Context;Landroid/database/Cursor;ZLandroid/widget/ListView;Lmiui/home/lib/dialog/AlertController;)V

    goto :goto_1

    :cond_1
    iget-boolean v0, p0, Lmiui/home/lib/dialog/AlertController$AlertParams;->mIsSingleChoice:Z

    if-eqz v0, :cond_2

    iget v0, p1, Lmiui/home/lib/dialog/AlertController;->mSingleChoiceItemLayout:I

    move v3, v0

    goto :goto_0

    :cond_2
    iget v0, p1, Lmiui/home/lib/dialog/AlertController;->mListItemLayout:I

    move v3, v0

    :goto_0
    iget-object v4, p0, Lmiui/home/lib/dialog/AlertController$AlertParams;->mCursor:Landroid/database/Cursor;

    const v0, 0x1020014

    if-eqz v4, :cond_3

    new-instance v9, Lmiui/home/lib/dialog/AlertController$AlertParams$3;

    iget-object v2, p0, Lmiui/home/lib/dialog/AlertController$AlertParams;->mContext:Landroid/content/Context;

    new-array v5, v8, [Ljava/lang/String;

    iget-object v1, p0, Lmiui/home/lib/dialog/AlertController$AlertParams;->mLabelColumn:Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v1, v5, v6

    new-array v10, v8, [I

    aput v0, v10, v6

    move-object v0, v9

    move-object v1, p0

    move-object v6, v10

    invoke-direct/range {v0 .. v6}, Lmiui/home/lib/dialog/AlertController$AlertParams$3;-><init>(Lmiui/home/lib/dialog/AlertController$AlertParams;Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[I)V

    goto :goto_1

    :cond_3
    iget-object v9, p0, Lmiui/home/lib/dialog/AlertController$AlertParams;->mAdapter:Landroid/widget/ListAdapter;

    if-eqz v9, :cond_4

    goto :goto_1

    :cond_4
    new-instance v9, Lmiui/home/lib/dialog/AlertController$CheckedItemAdapter;

    iget-object v1, p0, Lmiui/home/lib/dialog/AlertController$AlertParams;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lmiui/home/lib/dialog/AlertController$AlertParams;->mItems:[Ljava/lang/CharSequence;

    invoke-direct {v9, v1, v3, v0, v2}, Lmiui/home/lib/dialog/AlertController$CheckedItemAdapter;-><init>(Landroid/content/Context;II[Ljava/lang/CharSequence;)V

    :goto_1
    iget-object v0, p0, Lmiui/home/lib/dialog/AlertController$AlertParams;->mOnPrepareListViewListener:Lmiui/home/lib/dialog/AlertController$AlertParams$OnPrepareListViewListener;

    if-eqz v0, :cond_5

    invoke-interface {v0, v7}, Lmiui/home/lib/dialog/AlertController$AlertParams$OnPrepareListViewListener;->onPrepareListView(Landroid/widget/ListView;)V

    :cond_5
    iput-object v9, p1, Lmiui/home/lib/dialog/AlertController;->mAdapter:Landroid/widget/ListAdapter;

    iget v0, p0, Lmiui/home/lib/dialog/AlertController$AlertParams;->mCheckedItem:I

    iput v0, p1, Lmiui/home/lib/dialog/AlertController;->mCheckedItem:I

    iget-object v0, p0, Lmiui/home/lib/dialog/AlertController$AlertParams;->mOnClickListener:Landroid/content/DialogInterface$OnClickListener;

    if-eqz v0, :cond_6

    new-instance v0, Lmiui/home/lib/dialog/AlertController$AlertParams$4;

    invoke-direct {v0, p0, p1}, Lmiui/home/lib/dialog/AlertController$AlertParams$4;-><init>(Lmiui/home/lib/dialog/AlertController$AlertParams;Lmiui/home/lib/dialog/AlertController;)V

    invoke-virtual {v7, v0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    goto :goto_2

    :cond_6
    iget-object v0, p0, Lmiui/home/lib/dialog/AlertController$AlertParams;->mOnCheckboxClickListener:Landroid/content/DialogInterface$OnMultiChoiceClickListener;

    if-eqz v0, :cond_7

    new-instance v0, Lmiui/home/lib/dialog/AlertController$AlertParams$5;

    invoke-direct {v0, p0, v7, p1}, Lmiui/home/lib/dialog/AlertController$AlertParams$5;-><init>(Lmiui/home/lib/dialog/AlertController$AlertParams;Landroid/widget/ListView;Lmiui/home/lib/dialog/AlertController;)V

    invoke-virtual {v7, v0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    :cond_7
    :goto_2
    iget-object v0, p0, Lmiui/home/lib/dialog/AlertController$AlertParams;->mOnItemSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    if-eqz v0, :cond_8

    invoke-virtual {v7, v0}, Landroid/widget/ListView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    :cond_8
    iget-boolean v0, p0, Lmiui/home/lib/dialog/AlertController$AlertParams;->mIsSingleChoice:Z

    if-eqz v0, :cond_9

    invoke-virtual {v7, v8}, Landroid/widget/ListView;->setChoiceMode(I)V

    goto :goto_3

    :cond_9
    iget-boolean v0, p0, Lmiui/home/lib/dialog/AlertController$AlertParams;->mIsMultiChoice:Z

    if-eqz v0, :cond_a

    const/4 v0, 0x2

    invoke-virtual {v7, v0}, Landroid/widget/ListView;->setChoiceMode(I)V

    :cond_a
    :goto_3
    iput-object v7, p1, Lmiui/home/lib/dialog/AlertController;->mListView:Landroid/widget/ListView;

    return-void
.end method


# virtual methods
.method apply(Lmiui/home/lib/dialog/AlertController;)V
    .locals 4

    goto/32 :goto_3a

    nop

    :goto_0
    const/4 v1, 0x0

    goto/32 :goto_43

    nop

    :goto_1
    new-instance v1, Ljava/util/ArrayList;

    goto/32 :goto_23

    nop

    :goto_2
    if-nez v0, :cond_0

    goto/32 :goto_4f

    :cond_0
    :goto_3
    goto/32 :goto_4e

    nop

    :goto_4
    iget-object v0, p0, Lmiui/home/lib/dialog/AlertController$AlertParams;->mView:Landroid/view/View;

    goto/32 :goto_2a

    nop

    :goto_5
    if-eqz v0, :cond_1

    goto/32 :goto_3

    :cond_1
    goto/32 :goto_53

    nop

    :goto_6
    if-nez v0, :cond_2

    goto/32 :goto_1b

    :cond_2
    goto/32 :goto_1a

    nop

    :goto_7
    invoke-virtual {p1, v0}, Lmiui/home/lib/dialog/AlertController;->setTitle(Ljava/lang/CharSequence;)V

    :goto_8
    goto/32 :goto_36

    nop

    :goto_9
    if-nez v0, :cond_3

    goto/32 :goto_57

    :cond_3
    goto/32 :goto_4c

    nop

    :goto_a
    iget-object v0, p0, Lmiui/home/lib/dialog/AlertController$AlertParams;->mPositiveButtonText:Ljava/lang/CharSequence;

    goto/32 :goto_0

    nop

    :goto_b
    iget-object v0, p0, Lmiui/home/lib/dialog/AlertController$AlertParams;->mItems:[Ljava/lang/CharSequence;

    goto/32 :goto_51

    nop

    :goto_c
    invoke-virtual {p1, v1, v0}, Lmiui/home/lib/dialog/AlertController;->setCheckBox(ZLjava/lang/CharSequence;)V

    :goto_d
    goto/32 :goto_20

    nop

    :goto_e
    iget v0, p0, Lmiui/home/lib/dialog/AlertController$AlertParams;->mIconId:I

    goto/32 :goto_55

    nop

    :goto_f
    iget v0, p0, Lmiui/home/lib/dialog/AlertController$AlertParams;->mLiteVersion:I

    goto/32 :goto_2c

    nop

    :goto_10
    invoke-virtual {p1, v2, v0, v3, v1}, Lmiui/home/lib/dialog/AlertController;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;Landroid/os/Message;)V

    :goto_11
    goto/32 :goto_50

    nop

    :goto_12
    iget-object v0, p0, Lmiui/home/lib/dialog/AlertController$AlertParams;->mNeutralButtonText:Ljava/lang/CharSequence;

    goto/32 :goto_9

    nop

    :goto_13
    iget-object v0, p0, Lmiui/home/lib/dialog/AlertController$AlertParams;->mMessage:Ljava/lang/CharSequence;

    goto/32 :goto_6

    nop

    :goto_14
    if-nez v0, :cond_4

    goto/32 :goto_1e

    :cond_4
    goto/32 :goto_1d

    nop

    :goto_15
    invoke-virtual {p1, v2, v0, v3, v1}, Lmiui/home/lib/dialog/AlertController;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;Landroid/os/Message;)V

    :goto_16
    goto/32 :goto_12

    nop

    :goto_17
    goto :goto_2f

    :goto_18
    goto/32 :goto_19

    nop

    :goto_19
    iget v0, p0, Lmiui/home/lib/dialog/AlertController$AlertParams;->mViewLayoutResId:I

    goto/32 :goto_4b

    nop

    :goto_1a
    invoke-virtual {p1, v0}, Lmiui/home/lib/dialog/AlertController;->setMessage(Ljava/lang/CharSequence;)V

    :goto_1b
    goto/32 :goto_2b

    nop

    :goto_1c
    invoke-virtual {p1, v0}, Lmiui/home/lib/dialog/AlertController;->getIconAttributeResId(I)I

    move-result v0

    goto/32 :goto_31

    nop

    :goto_1d
    invoke-virtual {p1, v0}, Lmiui/home/lib/dialog/AlertController;->setIcon(Landroid/graphics/drawable/Drawable;)V

    :goto_1e
    goto/32 :goto_e

    nop

    :goto_1f
    const/4 v2, -0x2

    goto/32 :goto_45

    nop

    :goto_20
    iget-boolean v0, p0, Lmiui/home/lib/dialog/AlertController$AlertParams;->mHapticFeedbackEnabled:Z

    goto/32 :goto_37

    nop

    :goto_21
    invoke-virtual {p1, v0}, Lmiui/home/lib/dialog/AlertController;->setIcon(I)V

    :goto_22
    goto/32 :goto_44

    nop

    :goto_23
    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    goto/32 :goto_24

    nop

    :goto_24
    invoke-static {p1, v1}, Lmiui/home/lib/dialog/AlertController;->access$302(Lmiui/home/lib/dialog/AlertController;Ljava/util/List;)Ljava/util/List;

    :goto_25
    goto/32 :goto_b

    nop

    :goto_26
    if-nez v0, :cond_5

    goto/32 :goto_16

    :cond_5
    goto/32 :goto_1f

    nop

    :goto_27
    if-nez v0, :cond_6

    goto/32 :goto_d

    :cond_6
    goto/32 :goto_3d

    nop

    :goto_28
    invoke-virtual {p1, v0}, Lmiui/home/lib/dialog/AlertController;->setComment(Ljava/lang/CharSequence;)V

    :goto_29
    goto/32 :goto_a

    nop

    :goto_2a
    if-nez v0, :cond_7

    goto/32 :goto_18

    :cond_7
    goto/32 :goto_52

    nop

    :goto_2b
    iget-object v0, p0, Lmiui/home/lib/dialog/AlertController$AlertParams;->mComment:Ljava/lang/CharSequence;

    goto/32 :goto_39

    nop

    :goto_2c
    invoke-virtual {p1, v0}, Lmiui/home/lib/dialog/AlertController;->setLiteVersion(I)V

    goto/32 :goto_54

    nop

    :goto_2d
    iget-object v0, p0, Lmiui/home/lib/dialog/AlertController$AlertParams;->mExtraButtonList:Ljava/util/List;

    goto/32 :goto_40

    nop

    :goto_2e
    invoke-virtual {p1, v0}, Lmiui/home/lib/dialog/AlertController;->setView(I)V

    :goto_2f
    goto/32 :goto_33

    nop

    :goto_30
    iget-object v3, p0, Lmiui/home/lib/dialog/AlertController$AlertParams;->mPositiveButtonListener:Landroid/content/DialogInterface$OnClickListener;

    goto/32 :goto_10

    nop

    :goto_31
    invoke-virtual {p1, v0}, Lmiui/home/lib/dialog/AlertController;->setIcon(I)V

    :goto_32
    goto/32 :goto_13

    nop

    :goto_33
    iget-object v0, p0, Lmiui/home/lib/dialog/AlertController$AlertParams;->mCheckBoxMessage:Ljava/lang/CharSequence;

    goto/32 :goto_27

    nop

    :goto_34
    return-void

    :goto_35
    iget-boolean v0, p0, Lmiui/home/lib/dialog/AlertController$AlertParams;->mEnableEnterAnim:Z

    goto/32 :goto_38

    nop

    :goto_36
    iget-object v0, p0, Lmiui/home/lib/dialog/AlertController$AlertParams;->mIcon:Landroid/graphics/drawable/Drawable;

    goto/32 :goto_14

    nop

    :goto_37
    iput-boolean v0, p1, Lmiui/home/lib/dialog/AlertController;->mHapticFeedbackEnabled:Z

    goto/32 :goto_4a

    nop

    :goto_38
    invoke-virtual {p1, v0}, Lmiui/home/lib/dialog/AlertController;->setEnableEnterAnim(Z)V

    goto/32 :goto_34

    nop

    :goto_39
    if-nez v0, :cond_8

    goto/32 :goto_29

    :cond_8
    goto/32 :goto_28

    nop

    :goto_3a
    iget-object v0, p0, Lmiui/home/lib/dialog/AlertController$AlertParams;->mCustomTitleView:Landroid/view/View;

    goto/32 :goto_3f

    nop

    :goto_3b
    iget-object v0, p0, Lmiui/home/lib/dialog/AlertController$AlertParams;->mTitle:Ljava/lang/CharSequence;

    goto/32 :goto_4d

    nop

    :goto_3c
    if-nez v0, :cond_9

    goto/32 :goto_32

    :cond_9
    goto/32 :goto_1c

    nop

    :goto_3d
    iget-boolean v1, p0, Lmiui/home/lib/dialog/AlertController$AlertParams;->mIsChecked:Z

    goto/32 :goto_c

    nop

    :goto_3e
    const/4 v2, -0x1

    goto/32 :goto_30

    nop

    :goto_3f
    if-nez v0, :cond_a

    goto/32 :goto_42

    :cond_a
    goto/32 :goto_48

    nop

    :goto_40
    if-nez v0, :cond_b

    goto/32 :goto_25

    :cond_b
    goto/32 :goto_1

    nop

    :goto_41
    goto :goto_32

    :goto_42
    goto/32 :goto_3b

    nop

    :goto_43
    if-nez v0, :cond_c

    goto/32 :goto_11

    :cond_c
    goto/32 :goto_3e

    nop

    :goto_44
    iget v0, p0, Lmiui/home/lib/dialog/AlertController$AlertParams;->mIconAttrId:I

    goto/32 :goto_3c

    nop

    :goto_45
    iget-object v3, p0, Lmiui/home/lib/dialog/AlertController$AlertParams;->mNegativeButtonListener:Landroid/content/DialogInterface$OnClickListener;

    goto/32 :goto_15

    nop

    :goto_46
    invoke-virtual {p1, v0}, Lmiui/home/lib/dialog/AlertController;->setPreferLandscape(Z)V

    goto/32 :goto_35

    nop

    :goto_47
    invoke-virtual {p1, v0}, Lmiui/home/lib/dialog/AlertController;->setEnableImmersive(Z)V

    goto/32 :goto_f

    nop

    :goto_48
    invoke-virtual {p1, v0}, Lmiui/home/lib/dialog/AlertController;->setCustomTitle(Landroid/view/View;)V

    goto/32 :goto_41

    nop

    :goto_49
    iget-object v3, p0, Lmiui/home/lib/dialog/AlertController$AlertParams;->mNeutralButtonListener:Landroid/content/DialogInterface$OnClickListener;

    goto/32 :goto_56

    nop

    :goto_4a
    iget-boolean v0, p0, Lmiui/home/lib/dialog/AlertController$AlertParams;->mEnableDialogImmersive:Z

    goto/32 :goto_47

    nop

    :goto_4b
    if-nez v0, :cond_d

    goto/32 :goto_2f

    :cond_d
    goto/32 :goto_2e

    nop

    :goto_4c
    const/4 v2, -0x3

    goto/32 :goto_49

    nop

    :goto_4d
    if-nez v0, :cond_e

    goto/32 :goto_8

    :cond_e
    goto/32 :goto_7

    nop

    :goto_4e
    invoke-direct {p0, p1}, Lmiui/home/lib/dialog/AlertController$AlertParams;->createListView(Lmiui/home/lib/dialog/AlertController;)V

    :goto_4f
    goto/32 :goto_4

    nop

    :goto_50
    iget-object v0, p0, Lmiui/home/lib/dialog/AlertController$AlertParams;->mNegativeButtonText:Ljava/lang/CharSequence;

    goto/32 :goto_26

    nop

    :goto_51
    if-eqz v0, :cond_f

    goto/32 :goto_3

    :cond_f
    goto/32 :goto_58

    nop

    :goto_52
    invoke-virtual {p1, v0}, Lmiui/home/lib/dialog/AlertController;->setView(Landroid/view/View;)V

    goto/32 :goto_17

    nop

    :goto_53
    iget-object v0, p0, Lmiui/home/lib/dialog/AlertController$AlertParams;->mAdapter:Landroid/widget/ListAdapter;

    goto/32 :goto_2

    nop

    :goto_54
    iget-boolean v0, p0, Lmiui/home/lib/dialog/AlertController$AlertParams;->mPreferLandscape:Z

    goto/32 :goto_46

    nop

    :goto_55
    if-nez v0, :cond_10

    goto/32 :goto_22

    :cond_10
    goto/32 :goto_21

    nop

    :goto_56
    invoke-virtual {p1, v2, v0, v3, v1}, Lmiui/home/lib/dialog/AlertController;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;Landroid/os/Message;)V

    :goto_57
    goto/32 :goto_2d

    nop

    :goto_58
    iget-object v0, p0, Lmiui/home/lib/dialog/AlertController$AlertParams;->mCursor:Landroid/database/Cursor;

    goto/32 :goto_5

    nop
.end method
