.class public Lmiui/content/res/ConfigurationExpose;
.super Ljava/lang/Object;


# static fields
.field private static final CLASS:Lcom/miui/expose/utils/ClassHolder;

.field private static final windowConfiguration:Lcom/miui/expose/utils/FieldHolder;


# instance fields
.field private final instance:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Lcom/miui/expose/utils/ClassHolder;

    const-string v1, "android.content.res.Configuration"

    invoke-direct {v0, v1}, Lcom/miui/expose/utils/ClassHolder;-><init>(Ljava/lang/String;)V

    sput-object v0, Lmiui/content/res/ConfigurationExpose;->CLASS:Lcom/miui/expose/utils/ClassHolder;

    new-instance v0, Lcom/miui/expose/utils/FieldHolder;

    sget-object v1, Lmiui/content/res/ConfigurationExpose;->CLASS:Lcom/miui/expose/utils/ClassHolder;

    const-string v2, "windowConfiguration"

    sget-object v3, Lmiui/content/res/WindowConfigurationExpose;->CLASS:Lcom/miui/expose/utils/ClassHolder;

    invoke-direct {v0, v1, v2, v3}, Lcom/miui/expose/utils/FieldHolder;-><init>(Lcom/miui/expose/utils/ClassHolder;Ljava/lang/String;Lcom/miui/expose/utils/ClassHolder;)V

    sput-object v0, Lmiui/content/res/ConfigurationExpose;->windowConfiguration:Lcom/miui/expose/utils/FieldHolder;

    return-void
.end method

.method private constructor <init>(Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmiui/content/res/ConfigurationExpose;->instance:Ljava/lang/Object;

    return-void
.end method

.method public static box(Ljava/lang/Object;)Lmiui/content/res/ConfigurationExpose;
    .locals 1

    new-instance v0, Lmiui/content/res/ConfigurationExpose;

    invoke-direct {v0, p0}, Lmiui/content/res/ConfigurationExpose;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method


# virtual methods
.method public getWindowConfiguration()Lmiui/content/res/WindowConfigurationExpose;
    .locals 2

    sget-object v0, Lmiui/content/res/ConfigurationExpose;->windowConfiguration:Lcom/miui/expose/utils/FieldHolder;

    iget-object v1, p0, Lmiui/content/res/ConfigurationExpose;->instance:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/miui/expose/utils/FieldHolder;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lmiui/content/res/WindowConfigurationExpose;->box(Ljava/lang/Object;)Lmiui/content/res/WindowConfigurationExpose;

    move-result-object v0

    return-object v0
.end method
