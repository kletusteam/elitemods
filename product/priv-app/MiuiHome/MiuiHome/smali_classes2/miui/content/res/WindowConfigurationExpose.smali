.class public Lmiui/content/res/WindowConfigurationExpose;
.super Ljava/lang/Object;


# static fields
.field static final CLASS:Lcom/miui/expose/utils/ClassHolder;

.field private static final getActivityType:Lcom/miui/expose/utils/MethodHolder;

.field private static final getBounds:Lcom/miui/expose/utils/MethodHolder;

.field private static final getWindowingMode:Lcom/miui/expose/utils/MethodHolder;


# instance fields
.field private final instance:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Lcom/miui/expose/utils/ClassHolder;

    const-string v1, "android.app.WindowConfiguration"

    invoke-direct {v0, v1}, Lcom/miui/expose/utils/ClassHolder;-><init>(Ljava/lang/String;)V

    sput-object v0, Lmiui/content/res/WindowConfigurationExpose;->CLASS:Lcom/miui/expose/utils/ClassHolder;

    new-instance v0, Lcom/miui/expose/utils/MethodHolder;

    sget-object v1, Lmiui/content/res/WindowConfigurationExpose;->CLASS:Lcom/miui/expose/utils/ClassHolder;

    const-string v2, "getActivityType"

    sget-object v3, Lcom/miui/expose/utils/ParameterTypes;->EMPTY:Lcom/miui/expose/utils/ParameterTypes;

    invoke-direct {v0, v1, v2, v3}, Lcom/miui/expose/utils/MethodHolder;-><init>(Lcom/miui/expose/utils/ClassHolder;Ljava/lang/String;Lcom/miui/expose/utils/ParameterTypes;)V

    sput-object v0, Lmiui/content/res/WindowConfigurationExpose;->getActivityType:Lcom/miui/expose/utils/MethodHolder;

    new-instance v0, Lcom/miui/expose/utils/MethodHolder;

    sget-object v1, Lmiui/content/res/WindowConfigurationExpose;->CLASS:Lcom/miui/expose/utils/ClassHolder;

    const-string v2, "getWindowingMode"

    sget-object v3, Lcom/miui/expose/utils/ParameterTypes;->EMPTY:Lcom/miui/expose/utils/ParameterTypes;

    invoke-direct {v0, v1, v2, v3}, Lcom/miui/expose/utils/MethodHolder;-><init>(Lcom/miui/expose/utils/ClassHolder;Ljava/lang/String;Lcom/miui/expose/utils/ParameterTypes;)V

    sput-object v0, Lmiui/content/res/WindowConfigurationExpose;->getWindowingMode:Lcom/miui/expose/utils/MethodHolder;

    new-instance v0, Lcom/miui/expose/utils/MethodHolder;

    sget-object v1, Lmiui/content/res/WindowConfigurationExpose;->CLASS:Lcom/miui/expose/utils/ClassHolder;

    const-string v2, "getBounds"

    sget-object v3, Lcom/miui/expose/utils/ParameterTypes;->EMPTY:Lcom/miui/expose/utils/ParameterTypes;

    invoke-direct {v0, v1, v2, v3}, Lcom/miui/expose/utils/MethodHolder;-><init>(Lcom/miui/expose/utils/ClassHolder;Ljava/lang/String;Lcom/miui/expose/utils/ParameterTypes;)V

    sput-object v0, Lmiui/content/res/WindowConfigurationExpose;->getBounds:Lcom/miui/expose/utils/MethodHolder;

    return-void
.end method

.method private constructor <init>(Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmiui/content/res/WindowConfigurationExpose;->instance:Ljava/lang/Object;

    return-void
.end method

.method public static box(Ljava/lang/Object;)Lmiui/content/res/WindowConfigurationExpose;
    .locals 1

    new-instance v0, Lmiui/content/res/WindowConfigurationExpose;

    invoke-direct {v0, p0}, Lmiui/content/res/WindowConfigurationExpose;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method


# virtual methods
.method public getActivityType()I
    .locals 3

    sget-object v0, Lmiui/content/res/WindowConfigurationExpose;->getActivityType:Lcom/miui/expose/utils/MethodHolder;

    iget-object v1, p0, Lmiui/content/res/WindowConfigurationExpose;->instance:Ljava/lang/Object;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/miui/expose/utils/MethodHolder;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getWindowingMode()I
    .locals 3

    sget-object v0, Lmiui/content/res/WindowConfigurationExpose;->getWindowingMode:Lcom/miui/expose/utils/MethodHolder;

    iget-object v1, p0, Lmiui/content/res/WindowConfigurationExpose;->instance:Ljava/lang/Object;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/miui/expose/utils/MethodHolder;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public unbox()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lmiui/content/res/WindowConfigurationExpose;->instance:Ljava/lang/Object;

    return-object v0
.end method
