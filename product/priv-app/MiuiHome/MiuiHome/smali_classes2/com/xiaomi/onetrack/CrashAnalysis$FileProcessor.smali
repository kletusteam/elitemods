.class Lcom/xiaomi/onetrack/CrashAnalysis$FileProcessor;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/xiaomi/onetrack/CrashAnalysis;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FileProcessor"
.end annotation


# instance fields
.field final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field final b:Ljava/lang/String;

.field final c:Ljava/lang/String;

.field final synthetic d:Lcom/xiaomi/onetrack/CrashAnalysis;


# direct methods
.method constructor <init>(Lcom/xiaomi/onetrack/CrashAnalysis;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/xiaomi/onetrack/CrashAnalysis$FileProcessor;->d:Lcom/xiaomi/onetrack/CrashAnalysis;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/xiaomi/onetrack/CrashAnalysis$FileProcessor;->a:Ljava/util/List;

    iput-object p2, p0, Lcom/xiaomi/onetrack/CrashAnalysis$FileProcessor;->c:Ljava/lang/String;

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, ".xcrash"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/xiaomi/onetrack/CrashAnalysis$FileProcessor;->b:Ljava/lang/String;

    return-void
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "__"

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    array-length v0, p1

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    aget-object p1, p1, v0

    const-string v0, "_"

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    array-length v0, p1

    const/4 v2, 0x3

    if-ne v0, v2, :cond_0

    aget-object p1, p1, v1

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method


# virtual methods
.method a()V
    .locals 11

    goto/32 :goto_4

    nop

    :goto_0
    const-string v2, "CrashAnalysis"

    goto/32 :goto_3a

    nop

    :goto_1
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/32 :goto_15

    nop

    :goto_2
    if-nez v2, :cond_0

    goto/32 :goto_f

    :cond_0
    goto/32 :goto_d

    nop

    :goto_3
    if-lt v0, v1, :cond_1

    goto/32 :goto_11

    :cond_1
    goto/32 :goto_19

    nop

    :goto_4
    const/4 v0, 0x0

    :goto_5
    goto/32 :goto_44

    nop

    :goto_6
    new-instance v5, Ljava/lang/StringBuilder;

    goto/32 :goto_26

    nop

    :goto_7
    invoke-static {v2}, Lcom/xiaomi/onetrack/CrashAnalysis;->c(Lcom/xiaomi/onetrack/CrashAnalysis;)Lcom/xiaomi/onetrack/api/j;

    move-result-object v2

    goto/32 :goto_2

    nop

    :goto_8
    invoke-direct {p0, v1}, Lcom/xiaomi/onetrack/CrashAnalysis$FileProcessor;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    goto/32 :goto_1f

    nop

    :goto_9
    const-string v2, "remove reported crash file"

    goto/32 :goto_e

    nop

    :goto_a
    invoke-static {v2}, Lcom/xiaomi/onetrack/util/k;->a(Ljava/io/File;)V

    goto/32 :goto_45

    nop

    :goto_b
    invoke-virtual {v5, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    goto/32 :goto_22

    nop

    :goto_c
    invoke-static {v3, v2}, Lcom/xiaomi/onetrack/CrashAnalysis;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto/32 :goto_1c

    nop

    :goto_d
    iget-object v2, p0, Lcom/xiaomi/onetrack/CrashAnalysis$FileProcessor;->c:Ljava/lang/String;

    goto/32 :goto_3c

    nop

    :goto_e
    invoke-static {v1, v2}, Lcom/xiaomi/onetrack/util/p;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_f
    goto/32 :goto_23

    nop

    :goto_10
    goto/16 :goto_5

    :goto_11
    goto/32 :goto_18

    nop

    :goto_12
    invoke-static {v1, v2}, Lcom/xiaomi/onetrack/util/k;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    goto/32 :goto_39

    nop

    :goto_13
    const-string v10, "error: "

    goto/32 :goto_28

    nop

    :goto_14
    const-string v2, "CrashAnalysis"

    goto/32 :goto_43

    nop

    :goto_15
    invoke-static {v2, v5}, Lcom/xiaomi/onetrack/util/p;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/32 :goto_41

    nop

    :goto_16
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/32 :goto_2a

    nop

    :goto_17
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/32 :goto_38

    nop

    :goto_18
    return-void

    :goto_19
    iget-object v1, p0, Lcom/xiaomi/onetrack/CrashAnalysis$FileProcessor;->a:Ljava/util/List;

    goto/32 :goto_32

    nop

    :goto_1a
    invoke-virtual/range {v2 .. v9}, Lcom/xiaomi/onetrack/api/j;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    goto/32 :goto_3e

    nop

    :goto_1b
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_1

    nop

    :goto_1c
    invoke-static {v3}, Lcom/xiaomi/onetrack/CrashAnalysis;->a(Ljava/lang/String;)J

    move-result-wide v8

    goto/32 :goto_0

    nop

    :goto_1d
    const-string v2, "CrashAnalysis"

    goto/32 :goto_6

    nop

    :goto_1e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_13

    nop

    :goto_1f
    const v2, 0x19000

    goto/32 :goto_12

    nop

    :goto_20
    iget-object v5, p0, Lcom/xiaomi/onetrack/CrashAnalysis$FileProcessor;->c:Ljava/lang/String;

    goto/32 :goto_1a

    nop

    :goto_21
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    goto/32 :goto_3

    nop

    :goto_22
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/32 :goto_36

    nop

    :goto_23
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_10

    nop

    :goto_24
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_17

    nop

    :goto_25
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_3f

    nop

    :goto_26
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_2f

    nop

    :goto_27
    invoke-virtual {v1}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v1

    goto/32 :goto_2d

    nop

    :goto_28
    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_37

    nop

    :goto_29
    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_1b

    nop

    :goto_2a
    invoke-static {v2, v5}, Lcom/xiaomi/onetrack/util/p;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/32 :goto_1d

    nop

    :goto_2b
    check-cast v1, Ljava/io/File;

    goto/32 :goto_27

    nop

    :goto_2c
    iget-object v2, p0, Lcom/xiaomi/onetrack/CrashAnalysis$FileProcessor;->c:Ljava/lang/String;

    goto/32 :goto_c

    nop

    :goto_2d
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_8

    nop

    :goto_2e
    const-string v10, "feature id: "

    goto/32 :goto_29

    nop

    :goto_2f
    const-string v10, "crashTimeStamp: "

    goto/32 :goto_40

    nop

    :goto_30
    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    goto/32 :goto_a

    nop

    :goto_31
    new-instance v5, Ljava/lang/StringBuilder;

    goto/32 :goto_1e

    nop

    :goto_32
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_2b

    nop

    :goto_33
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_2e

    nop

    :goto_34
    iget-object v2, p0, Lcom/xiaomi/onetrack/CrashAnalysis$FileProcessor;->d:Lcom/xiaomi/onetrack/CrashAnalysis;

    goto/32 :goto_3d

    nop

    :goto_35
    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_24

    nop

    :goto_36
    invoke-static {v2, v5}, Lcom/xiaomi/onetrack/util/p;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/32 :goto_34

    nop

    :goto_37
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_16

    nop

    :goto_38
    invoke-static {v2, v5}, Lcom/xiaomi/onetrack/util/p;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/32 :goto_14

    nop

    :goto_39
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    goto/32 :goto_3b

    nop

    :goto_3a
    new-instance v5, Ljava/lang/StringBuilder;

    goto/32 :goto_25

    nop

    :goto_3b
    if-eqz v2, :cond_2

    goto/32 :goto_f

    :cond_2
    goto/32 :goto_42

    nop

    :goto_3c
    invoke-static {v3, v2}, Lcom/xiaomi/onetrack/CrashAnalysis;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    goto/32 :goto_2c

    nop

    :goto_3d
    invoke-static {v2}, Lcom/xiaomi/onetrack/CrashAnalysis;->c(Lcom/xiaomi/onetrack/CrashAnalysis;)Lcom/xiaomi/onetrack/api/j;

    move-result-object v2

    goto/32 :goto_20

    nop

    :goto_3e
    new-instance v2, Ljava/io/File;

    goto/32 :goto_30

    nop

    :goto_3f
    const-string v10, "fileName: "

    goto/32 :goto_35

    nop

    :goto_40
    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_b

    nop

    :goto_41
    const-string v2, "CrashAnalysis"

    goto/32 :goto_31

    nop

    :goto_42
    iget-object v2, p0, Lcom/xiaomi/onetrack/CrashAnalysis$FileProcessor;->d:Lcom/xiaomi/onetrack/CrashAnalysis;

    goto/32 :goto_7

    nop

    :goto_43
    new-instance v5, Ljava/lang/StringBuilder;

    goto/32 :goto_33

    nop

    :goto_44
    iget-object v1, p0, Lcom/xiaomi/onetrack/CrashAnalysis$FileProcessor;->a:Ljava/util/List;

    goto/32 :goto_21

    nop

    :goto_45
    const-string v1, "CrashAnalysis"

    goto/32 :goto_9

    nop
.end method

.method a(Ljava/io/File;)Z
    .locals 2

    goto/32 :goto_3

    nop

    :goto_0
    const/4 p1, 0x0

    goto/32 :goto_1

    nop

    :goto_1
    return p1

    :goto_2
    const/4 p1, 0x1

    goto/32 :goto_7

    nop

    :goto_3
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_5

    nop

    :goto_4
    iget-object v0, p0, Lcom/xiaomi/onetrack/CrashAnalysis$FileProcessor;->a:Ljava/util/List;

    goto/32 :goto_a

    nop

    :goto_5
    iget-object v1, p0, Lcom/xiaomi/onetrack/CrashAnalysis$FileProcessor;->b:Ljava/lang/String;

    goto/32 :goto_6

    nop

    :goto_6
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    goto/32 :goto_9

    nop

    :goto_7
    return p1

    :goto_8
    goto/32 :goto_0

    nop

    :goto_9
    if-nez v0, :cond_0

    goto/32 :goto_8

    :cond_0
    goto/32 :goto_4

    nop

    :goto_a
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/32 :goto_2

    nop
.end method
