.class public Lcom/xiaomi/onetrack/util/oaid/helpers/b;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/onetrack/util/oaid/helpers/b$a;
    }
.end annotation


# static fields
.field static a:Ljava/lang/String; = "b"


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static c()Ljava/lang/String;
    .locals 1

    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    const-string v0, ""

    :try_start_0
    invoke-static {}, Lcom/xiaomi/onetrack/util/oaid/helpers/b;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p1, v1}, Lcom/xiaomi/onetrack/util/oaid/helpers/b;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    sget-object v1, Lcom/xiaomi/onetrack/util/oaid/helpers/b;->a:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, Lcom/xiaomi/onetrack/util/p;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-object v0
.end method

.method a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    goto/32 :goto_37

    nop

    :goto_0
    new-instance p2, Lcom/xiaomi/onetrack/util/oaid/helpers/h;

    goto/32 :goto_1e

    nop

    :goto_1
    if-nez v2, :cond_0

    goto/32 :goto_2b

    :cond_0
    goto/32 :goto_2a

    nop

    :goto_2
    invoke-direct {p2}, Lcom/xiaomi/onetrack/util/oaid/helpers/m;-><init>()V

    goto/32 :goto_17

    nop

    :goto_3
    invoke-static {p1}, Lcom/xiaomi/onetrack/util/n;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_18

    nop

    :goto_4
    new-instance p2, Lcom/xiaomi/onetrack/util/oaid/helpers/l;

    goto/32 :goto_2c

    nop

    :goto_5
    invoke-virtual {p0}, Lcom/xiaomi/onetrack/util/oaid/helpers/b;->a()Z

    move-result v2

    goto/32 :goto_1

    nop

    :goto_6
    invoke-virtual {p2, p1}, Lcom/xiaomi/onetrack/util/oaid/helpers/j;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_1d

    nop

    :goto_7
    if-nez v2, :cond_1

    goto/32 :goto_42

    :cond_1
    goto/32 :goto_41

    nop

    :goto_8
    goto :goto_10

    :pswitch_0
    goto/32 :goto_3

    nop

    :goto_9
    goto :goto_10

    :pswitch_1
    goto/32 :goto_b

    nop

    :goto_a
    invoke-direct {p2}, Lcom/xiaomi/onetrack/util/oaid/helpers/i;-><init>()V

    goto/32 :goto_19

    nop

    :goto_b
    new-instance p2, Lcom/xiaomi/onetrack/util/oaid/helpers/k;

    goto/32 :goto_14

    nop

    :goto_c
    invoke-direct {p2}, Lcom/xiaomi/onetrack/util/oaid/helpers/f;-><init>()V

    goto/32 :goto_3d

    nop

    :goto_d
    goto :goto_10

    :pswitch_2
    goto/32 :goto_22

    nop

    :goto_e
    invoke-virtual {v1}, Lcom/xiaomi/onetrack/util/oaid/helpers/b$a;->ordinal()I

    move-result v1

    goto/32 :goto_40

    nop

    :goto_f
    invoke-virtual {p2, p1}, Lcom/xiaomi/onetrack/util/oaid/helpers/a;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    :goto_10
    goto/32 :goto_32

    nop

    :goto_11
    new-instance p2, Lcom/xiaomi/onetrack/util/oaid/helpers/i;

    goto/32 :goto_a

    nop

    :goto_12
    goto :goto_10

    :pswitch_3
    goto/32 :goto_27

    nop

    :goto_13
    goto :goto_10

    :pswitch_4
    goto/32 :goto_16

    nop

    :goto_14
    invoke-direct {p2}, Lcom/xiaomi/onetrack/util/oaid/helpers/k;-><init>()V

    goto/32 :goto_3f

    nop

    :goto_15
    throw p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_4
        :pswitch_3
        :pswitch_3
        :pswitch_6
        :pswitch_2
        :pswitch_1
        :pswitch_a
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_8
        :pswitch_8
        :pswitch_8
    .end packed-switch

    :goto_16
    new-instance p2, Lcom/xiaomi/onetrack/util/oaid/helpers/d;

    goto/32 :goto_2d

    nop

    :goto_17
    invoke-virtual {p2, p1}, Lcom/xiaomi/onetrack/util/oaid/helpers/m;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_30

    nop

    :goto_18
    goto :goto_10

    :pswitch_5
    goto/32 :goto_4

    nop

    :goto_19
    invoke-virtual {p2, p1}, Lcom/xiaomi/onetrack/util/oaid/helpers/i;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_8

    nop

    :goto_1a
    const/4 v0, 0x1

    goto/32 :goto_1b

    nop

    :goto_1b
    new-array v0, v0, [Ljava/lang/Object;

    goto/32 :goto_24

    nop

    :goto_1c
    invoke-static {p2, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    goto/32 :goto_3c

    nop

    :goto_1d
    goto :goto_10

    :pswitch_6
    goto/32 :goto_43

    nop

    :goto_1e
    invoke-direct {p2}, Lcom/xiaomi/onetrack/util/oaid/helpers/h;-><init>()V

    goto/32 :goto_28

    nop

    :goto_1f
    if-nez v1, :cond_2

    goto/32 :goto_33

    :cond_2
    goto/32 :goto_34

    nop

    :goto_20
    aput-object p2, v0, v1

    goto/32 :goto_39

    nop

    :goto_21
    invoke-virtual {p0}, Lcom/xiaomi/onetrack/util/oaid/helpers/b;->b()Z

    move-result v2

    goto/32 :goto_7

    nop

    :goto_22
    new-instance p2, Lcom/xiaomi/onetrack/util/oaid/helpers/j;

    goto/32 :goto_26

    nop

    :goto_23
    invoke-direct {p2}, Lcom/xiaomi/onetrack/util/oaid/helpers/e;-><init>()V

    goto/32 :goto_35

    nop

    :goto_24
    const/4 v1, 0x0

    goto/32 :goto_20

    nop

    :goto_25
    goto/16 :goto_10

    :pswitch_7
    goto/32 :goto_38

    nop

    :goto_26
    invoke-direct {p2}, Lcom/xiaomi/onetrack/util/oaid/helpers/j;-><init>()V

    goto/32 :goto_6

    nop

    :goto_27
    new-instance p2, Lcom/xiaomi/onetrack/util/oaid/helpers/e;

    goto/32 :goto_23

    nop

    :goto_28
    invoke-virtual {p2, p1}, Lcom/xiaomi/onetrack/util/oaid/helpers/h;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_9

    nop

    :goto_29
    new-instance p1, Ljava/lang/Exception;

    goto/32 :goto_1a

    nop

    :goto_2a
    sget-object v1, Lcom/xiaomi/onetrack/util/oaid/helpers/b$a;->o:Lcom/xiaomi/onetrack/util/oaid/helpers/b$a;

    :goto_2b
    goto/32 :goto_21

    nop

    :goto_2c
    invoke-direct {p2}, Lcom/xiaomi/onetrack/util/oaid/helpers/l;-><init>()V

    goto/32 :goto_36

    nop

    :goto_2d
    invoke-direct {p2}, Lcom/xiaomi/onetrack/util/oaid/helpers/d;-><init>()V

    goto/32 :goto_2f

    nop

    :goto_2e
    goto/16 :goto_10

    :pswitch_8
    goto/32 :goto_3b

    nop

    :goto_2f
    invoke-virtual {p2, p1}, Lcom/xiaomi/onetrack/util/oaid/helpers/d;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_25

    nop

    :goto_30
    goto/16 :goto_10

    :pswitch_9
    goto/32 :goto_11

    nop

    :goto_31
    goto/16 :goto_10

    :pswitch_a
    goto/32 :goto_0

    nop

    :goto_32
    return-object v0

    :goto_33
    goto/32 :goto_29

    nop

    :goto_34
    sget-object p2, Lcom/xiaomi/onetrack/util/oaid/helpers/c;->a:[I

    goto/32 :goto_e

    nop

    :goto_35
    invoke-virtual {p2, p1}, Lcom/xiaomi/onetrack/util/oaid/helpers/e;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_13

    nop

    :goto_36
    invoke-virtual {p2, p1}, Lcom/xiaomi/onetrack/util/oaid/helpers/l;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_31

    nop

    :goto_37
    const-string v0, ""

    goto/32 :goto_3e

    nop

    :goto_38
    new-instance p2, Lcom/xiaomi/onetrack/util/oaid/helpers/a;

    goto/32 :goto_3a

    nop

    :goto_39
    const-string p2, "undefined oaid method of manufacturer %s"

    goto/32 :goto_1c

    nop

    :goto_3a
    invoke-direct {p2}, Lcom/xiaomi/onetrack/util/oaid/helpers/a;-><init>()V

    goto/32 :goto_f

    nop

    :goto_3b
    new-instance p2, Lcom/xiaomi/onetrack/util/oaid/helpers/m;

    goto/32 :goto_2

    nop

    :goto_3c
    invoke-direct {p1, p2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    goto/32 :goto_15

    nop

    :goto_3d
    invoke-virtual {p2, p1}, Lcom/xiaomi/onetrack/util/oaid/helpers/f;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_12

    nop

    :goto_3e
    invoke-static {p2}, Lcom/xiaomi/onetrack/util/oaid/helpers/b$a;->b(Ljava/lang/String;)Lcom/xiaomi/onetrack/util/oaid/helpers/b$a;

    move-result-object v1

    goto/32 :goto_5

    nop

    :goto_3f
    invoke-virtual {p2, p1}, Lcom/xiaomi/onetrack/util/oaid/helpers/k;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_d

    nop

    :goto_40
    aget p2, p2, v1

    packed-switch p2, :pswitch_data_0

    goto/32 :goto_2e

    nop

    :goto_41
    sget-object v1, Lcom/xiaomi/onetrack/util/oaid/helpers/b$a;->p:Lcom/xiaomi/onetrack/util/oaid/helpers/b$a;

    :goto_42
    goto/32 :goto_1f

    nop

    :goto_43
    new-instance p2, Lcom/xiaomi/onetrack/util/oaid/helpers/f;

    goto/32 :goto_c

    nop
.end method

.method public a()Z
    .locals 2

    const-string v0, "ro.build.freeme.label"

    invoke-static {v0}, Lcom/xiaomi/onetrack/util/ab;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "FREEMEOS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public b()Z
    .locals 2

    const-string v0, "ro.ssui.product"

    invoke-static {v0}, Lcom/xiaomi/onetrack/util/ab;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "unknown"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method
