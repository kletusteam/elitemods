.class Lcom/miui/launcher/utils/BoostHelper$2;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/launcher/utils/BoostHelper;->boostThreadInternal(J[II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/launcher/utils/BoostHelper;

.field final synthetic val$duration:J

.field final synthetic val$mode:I

.field final synthetic val$threadIds:[I


# direct methods
.method constructor <init>(Lcom/miui/launcher/utils/BoostHelper;[IJI)V
    .locals 0

    iput-object p1, p0, Lcom/miui/launcher/utils/BoostHelper$2;->this$0:Lcom/miui/launcher/utils/BoostHelper;

    iput-object p2, p0, Lcom/miui/launcher/utils/BoostHelper$2;->val$threadIds:[I

    iput-wide p3, p0, Lcom/miui/launcher/utils/BoostHelper$2;->val$duration:J

    iput p5, p0, Lcom/miui/launcher/utils/BoostHelper$2;->val$mode:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    :try_start_0
    iget-object v0, p0, Lcom/miui/launcher/utils/BoostHelper$2;->val$threadIds:[I

    array-length v0, v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/miui/launcher/utils/BoostHelper$2;->this$0:Lcom/miui/launcher/utils/BoostHelper;

    invoke-static {v0}, Lcom/miui/launcher/utils/BoostHelper;->access$600(Lcom/miui/launcher/utils/BoostHelper;)Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x2

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/miui/launcher/utils/BoostHelper$2;->this$0:Lcom/miui/launcher/utils/BoostHelper;

    invoke-static {v0}, Lcom/miui/launcher/utils/BoostHelper;->access$700(Lcom/miui/launcher/utils/BoostHelper;)Ljava/lang/Class;

    move-result-object v0

    sget-object v4, Ljava/lang/Void;->TYPE:Ljava/lang/Class;

    const-string v5, "beginSchedThreads"

    const/4 v6, 0x3

    new-array v7, v6, [Ljava/lang/Class;

    const-string v8, "[I"

    invoke-static {v8}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v8

    aput-object v8, v7, v2

    sget-object v8, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v8, v7, v1

    sget-object v8, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v8, v7, v3

    new-array v6, v6, [Ljava/lang/Object;

    iget-object v8, p0, Lcom/miui/launcher/utils/BoostHelper$2;->val$threadIds:[I

    aput-object v8, v6, v2

    iget-wide v8, p0, Lcom/miui/launcher/utils/BoostHelper$2;->val$duration:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v6, v1

    iget v1, p0, Lcom/miui/launcher/utils/BoostHelper$2;->val$mode:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v6, v3

    invoke-static {v0, v4, v5, v7, v6}, Lcom/miui/launcher/utils/ReflectUtils;->callStaticMethod(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/miui/launcher/utils/BoostHelper$2;->this$0:Lcom/miui/launcher/utils/BoostHelper;

    invoke-static {v0}, Lcom/miui/launcher/utils/BoostHelper;->access$700(Lcom/miui/launcher/utils/BoostHelper;)Ljava/lang/Class;

    move-result-object v0

    sget-object v4, Ljava/lang/Void;->TYPE:Ljava/lang/Class;

    const-string v5, "beginSchedThreads"

    new-array v6, v3, [Ljava/lang/Class;

    const-string v7, "[I"

    invoke-static {v7}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v7

    aput-object v7, v6, v2

    sget-object v7, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v7, v6, v1

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/miui/launcher/utils/BoostHelper$2;->val$threadIds:[I

    aput-object v7, v3, v2

    iget-wide v7, p0, Lcom/miui/launcher/utils/BoostHelper$2;->val$duration:J

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v3, v1

    invoke-static {v0, v4, v5, v6, v3}, Lcom/miui/launcher/utils/ReflectUtils;->callStaticMethod(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_1
    :goto_0
    return-void
.end method
