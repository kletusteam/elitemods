.class public Lcom/miui/launcher/overlay/client/LauncherOverlayConnectionCompat;
.super Ljava/lang/Object;


# instance fields
.field private final mLauncherOverlayConnectionCompat:Lcom/miui/launcher/overlay/client/ServiceConnectionStrategy;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    if-ne p3, v0, :cond_0

    new-instance p3, Lcom/miui/launcher/overlay/client/WaivePriorityServiceConnectionStrategy;

    invoke-direct {p3, p1, p2}, Lcom/miui/launcher/overlay/client/WaivePriorityServiceConnectionStrategy;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object p3, p0, Lcom/miui/launcher/overlay/client/LauncherOverlayConnectionCompat;->mLauncherOverlayConnectionCompat:Lcom/miui/launcher/overlay/client/ServiceConnectionStrategy;

    goto :goto_0

    :cond_0
    new-instance p3, Lcom/miui/launcher/overlay/client/HighPriorityServiceConnectionStrategy;

    invoke-direct {p3, p1, p2}, Lcom/miui/launcher/overlay/client/HighPriorityServiceConnectionStrategy;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object p3, p0, Lcom/miui/launcher/overlay/client/LauncherOverlayConnectionCompat;->mLauncherOverlayConnectionCompat:Lcom/miui/launcher/overlay/client/ServiceConnectionStrategy;

    :goto_0
    return-void
.end method


# virtual methods
.method public bindClient(Lcom/miui/launcher/overlay/client/LauncherClient;)Lcom/miui/launcher/overlay/ILauncherOverlay;
    .locals 1

    iget-object v0, p0, Lcom/miui/launcher/overlay/client/LauncherOverlayConnectionCompat;->mLauncherOverlayConnectionCompat:Lcom/miui/launcher/overlay/client/ServiceConnectionStrategy;

    invoke-virtual {v0, p1}, Lcom/miui/launcher/overlay/client/ServiceConnectionStrategy;->bindClient(Lcom/miui/launcher/overlay/client/LauncherClient;)Lcom/miui/launcher/overlay/ILauncherOverlay;

    move-result-object p1

    return-object p1
.end method

.method public connect(Z)V
    .locals 1

    iget-object v0, p0, Lcom/miui/launcher/overlay/client/LauncherOverlayConnectionCompat;->mLauncherOverlayConnectionCompat:Lcom/miui/launcher/overlay/client/ServiceConnectionStrategy;

    invoke-virtual {v0, p1}, Lcom/miui/launcher/overlay/client/ServiceConnectionStrategy;->connect(Z)V

    return-void
.end method

.method public disconnect()V
    .locals 1

    iget-object v0, p0, Lcom/miui/launcher/overlay/client/LauncherOverlayConnectionCompat;->mLauncherOverlayConnectionCompat:Lcom/miui/launcher/overlay/client/ServiceConnectionStrategy;

    invoke-virtual {v0}, Lcom/miui/launcher/overlay/client/ServiceConnectionStrategy;->disconnect()V

    return-void
.end method

.method public hideOverlay()V
    .locals 1

    iget-object v0, p0, Lcom/miui/launcher/overlay/client/LauncherOverlayConnectionCompat;->mLauncherOverlayConnectionCompat:Lcom/miui/launcher/overlay/client/ServiceConnectionStrategy;

    invoke-virtual {v0}, Lcom/miui/launcher/overlay/client/ServiceConnectionStrategy;->hideOverlay()V

    return-void
.end method

.method public onDestroy(Lcom/miui/launcher/overlay/client/LauncherClient;Z)V
    .locals 1

    iget-object v0, p0, Lcom/miui/launcher/overlay/client/LauncherOverlayConnectionCompat;->mLauncherOverlayConnectionCompat:Lcom/miui/launcher/overlay/client/ServiceConnectionStrategy;

    invoke-virtual {v0, p1, p2}, Lcom/miui/launcher/overlay/client/ServiceConnectionStrategy;->onDestroy(Lcom/miui/launcher/overlay/client/LauncherClient;Z)V

    return-void
.end method

.method public onStart()V
    .locals 1

    iget-object v0, p0, Lcom/miui/launcher/overlay/client/LauncherOverlayConnectionCompat;->mLauncherOverlayConnectionCompat:Lcom/miui/launcher/overlay/client/ServiceConnectionStrategy;

    invoke-virtual {v0}, Lcom/miui/launcher/overlay/client/ServiceConnectionStrategy;->onStart()V

    return-void
.end method

.method public onStop()V
    .locals 1

    iget-object v0, p0, Lcom/miui/launcher/overlay/client/LauncherOverlayConnectionCompat;->mLauncherOverlayConnectionCompat:Lcom/miui/launcher/overlay/client/ServiceConnectionStrategy;

    invoke-virtual {v0}, Lcom/miui/launcher/overlay/client/ServiceConnectionStrategy;->onStop()V

    return-void
.end method

.method public showOverlay()V
    .locals 1

    iget-object v0, p0, Lcom/miui/launcher/overlay/client/LauncherOverlayConnectionCompat;->mLauncherOverlayConnectionCompat:Lcom/miui/launcher/overlay/client/ServiceConnectionStrategy;

    invoke-virtual {v0}, Lcom/miui/launcher/overlay/client/ServiceConnectionStrategy;->showOverlay()V

    return-void
.end method
