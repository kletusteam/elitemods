.class public Lcom/miui/launcher/overlay/client/HighPriorityServiceConnectionStrategy;
.super Lcom/miui/launcher/overlay/client/ServiceConnectionStrategy;


# instance fields
.field private final mClientService:Lcom/miui/launcher/overlay/client/LauncherClientService;


# direct methods
.method constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/miui/launcher/overlay/client/ServiceConnectionStrategy;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1, p2}, Lcom/miui/launcher/overlay/client/LauncherClientService;->getService(Landroid/content/Context;Ljava/lang/String;)Lcom/miui/launcher/overlay/client/LauncherClientService;

    move-result-object p1

    iput-object p1, p0, Lcom/miui/launcher/overlay/client/HighPriorityServiceConnectionStrategy;->mClientService:Lcom/miui/launcher/overlay/client/LauncherClientService;

    return-void
.end method


# virtual methods
.method public bindClient(Lcom/miui/launcher/overlay/client/LauncherClient;)Lcom/miui/launcher/overlay/ILauncherOverlay;
    .locals 1

    iget-object v0, p0, Lcom/miui/launcher/overlay/client/HighPriorityServiceConnectionStrategy;->mClientService:Lcom/miui/launcher/overlay/client/LauncherClientService;

    invoke-virtual {v0, p1}, Lcom/miui/launcher/overlay/client/LauncherClientService;->bindClient(Lcom/miui/launcher/overlay/client/LauncherClient;)Lcom/miui/launcher/overlay/ILauncherOverlay;

    move-result-object p1

    return-object p1
.end method

.method public connect(Z)V
    .locals 0

    iget-object p1, p0, Lcom/miui/launcher/overlay/client/HighPriorityServiceConnectionStrategy;->mClientService:Lcom/miui/launcher/overlay/client/LauncherClientService;

    invoke-virtual {p1}, Lcom/miui/launcher/overlay/client/LauncherClientService;->connect()V

    return-void
.end method

.method public disconnect()V
    .locals 1

    iget-object v0, p0, Lcom/miui/launcher/overlay/client/HighPriorityServiceConnectionStrategy;->mClientService:Lcom/miui/launcher/overlay/client/LauncherClientService;

    invoke-virtual {v0}, Lcom/miui/launcher/overlay/client/LauncherClientService;->disconnect()V

    return-void
.end method

.method public hideOverlay()V
    .locals 0

    return-void
.end method

.method public onDestroy(Lcom/miui/launcher/overlay/client/LauncherClient;Z)V
    .locals 1

    iget-object v0, p0, Lcom/miui/launcher/overlay/client/HighPriorityServiceConnectionStrategy;->mClientService:Lcom/miui/launcher/overlay/client/LauncherClientService;

    invoke-virtual {v0, p1, p2}, Lcom/miui/launcher/overlay/client/LauncherClientService;->unbindClient(Lcom/miui/launcher/overlay/client/LauncherClient;Z)V

    return-void
.end method

.method public onStart()V
    .locals 2

    iget-object v0, p0, Lcom/miui/launcher/overlay/client/HighPriorityServiceConnectionStrategy;->mClientService:Lcom/miui/launcher/overlay/client/LauncherClientService;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/miui/launcher/overlay/client/LauncherClientService;->setStopped(Z)V

    return-void
.end method

.method onStop()V
    .locals 2

    goto/32 :goto_2

    nop

    :goto_0
    invoke-virtual {v0, v1}, Lcom/miui/launcher/overlay/client/LauncherClientService;->setStopped(Z)V

    goto/32 :goto_1

    nop

    :goto_1
    return-void

    :goto_2
    iget-object v0, p0, Lcom/miui/launcher/overlay/client/HighPriorityServiceConnectionStrategy;->mClientService:Lcom/miui/launcher/overlay/client/LauncherClientService;

    goto/32 :goto_3

    nop

    :goto_3
    const/4 v1, 0x1

    goto/32 :goto_0

    nop
.end method

.method public showOverlay()V
    .locals 0

    return-void
.end method
