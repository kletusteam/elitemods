.class Lcom/miui/maml/elements/MusicControlScreenElement$AlbumInfo;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/maml/elements/MusicControlScreenElement;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AlbumInfo"
.end annotation


# instance fields
.field album:Ljava/lang/String;

.field artist:Ljava/lang/String;

.field title:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/miui/maml/elements/MusicControlScreenElement$1;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/maml/elements/MusicControlScreenElement$AlbumInfo;-><init>()V

    return-void
.end method


# virtual methods
.method update(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    if-nez p2, :cond_0

    goto/32 :goto_b

    :cond_0
    goto/32 :goto_a

    nop

    :goto_1
    invoke-static {p3, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    goto/32 :goto_14

    nop

    :goto_2
    if-nez p1, :cond_1

    goto/32 :goto_8

    :cond_1
    goto/32 :goto_7

    nop

    :goto_3
    iget-object v0, p0, Lcom/miui/maml/elements/MusicControlScreenElement$AlbumInfo;->artist:Ljava/lang/String;

    goto/32 :goto_1d

    nop

    :goto_4
    iput-object p3, p0, Lcom/miui/maml/elements/MusicControlScreenElement$AlbumInfo;->album:Ljava/lang/String;

    :goto_5
    goto/32 :goto_c

    nop

    :goto_6
    if-nez v0, :cond_2

    goto/32 :goto_13

    :cond_2
    goto/32 :goto_3

    nop

    :goto_7
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    :goto_8
    goto/32 :goto_0

    nop

    :goto_9
    iput-object p1, p0, Lcom/miui/maml/elements/MusicControlScreenElement$AlbumInfo;->title:Ljava/lang/String;

    goto/32 :goto_15

    nop

    :goto_a
    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p2

    :goto_b
    goto/32 :goto_1e

    nop

    :goto_c
    return v0

    :goto_d
    if-nez v0, :cond_3

    goto/32 :goto_13

    :cond_3
    goto/32 :goto_1c

    nop

    :goto_e
    goto :goto_13

    :goto_f
    goto/32 :goto_16

    nop

    :goto_10
    iget-object v0, p0, Lcom/miui/maml/elements/MusicControlScreenElement$AlbumInfo;->title:Ljava/lang/String;

    goto/32 :goto_11

    nop

    :goto_11
    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    goto/32 :goto_6

    nop

    :goto_12
    goto :goto_18

    :goto_13
    goto/32 :goto_17

    nop

    :goto_14
    if-eqz v0, :cond_4

    goto/32 :goto_f

    :cond_4
    goto/32 :goto_e

    nop

    :goto_15
    iput-object p2, p0, Lcom/miui/maml/elements/MusicControlScreenElement$AlbumInfo;->artist:Ljava/lang/String;

    goto/32 :goto_4

    nop

    :goto_16
    const/4 v0, 0x0

    goto/32 :goto_12

    nop

    :goto_17
    const/4 v0, 0x1

    :goto_18
    goto/32 :goto_1b

    nop

    :goto_19
    invoke-virtual {p3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p3

    :goto_1a
    goto/32 :goto_10

    nop

    :goto_1b
    if-nez v0, :cond_5

    goto/32 :goto_5

    :cond_5
    goto/32 :goto_9

    nop

    :goto_1c
    iget-object v0, p0, Lcom/miui/maml/elements/MusicControlScreenElement$AlbumInfo;->album:Ljava/lang/String;

    goto/32 :goto_1

    nop

    :goto_1d
    invoke-static {p2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    goto/32 :goto_d

    nop

    :goto_1e
    if-nez p3, :cond_6

    goto/32 :goto_1a

    :cond_6
    goto/32 :goto_19

    nop
.end method
