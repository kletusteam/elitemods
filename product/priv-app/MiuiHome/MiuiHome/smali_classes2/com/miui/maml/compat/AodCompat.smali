.class public Lcom/miui/maml/compat/AodCompat;
.super Ljava/lang/Object;


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "AodCompat"

.field private static final ROOT_TAG:Ljava/lang/String; = "aod"

.field private static final TYPE_ATTR_NAME:Ljava/lang/String; = "type"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static compatClock(Lorg/w3c/dom/Element;Lcom/miui/maml/ScreenElementRoot;)V
    .locals 0

    return-void
.end method

.method public static isAod(Lorg/w3c/dom/Element;Ljava/lang/String;)Z
    .locals 0

    const-string p0, "aod"

    invoke-virtual {p0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p0

    return p0
.end method
