.class Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLocator;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/maml/elements/MusicLyricParser$Lyric;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "LyricLocator"
.end annotation


# instance fields
.field final CRLF_LENGTH:I

.field mFullLyric:Ljava/lang/String;

.field mLyricLines:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLine;",
            ">;"
        }
    .end annotation
.end field

.field mTimeArr:[I

.field final synthetic this$0:Lcom/miui/maml/elements/MusicLyricParser$Lyric;


# direct methods
.method constructor <init>(Lcom/miui/maml/elements/MusicLyricParser$Lyric;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLocator;->this$0:Lcom/miui/maml/elements/MusicLyricParser$Lyric;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p1, 0x2

    iput p1, p0, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLocator;->CRLF_LENGTH:I

    return-void
.end method

.method private getLineNumber(J)I
    .locals 4

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLocator;->mTimeArr:[I

    array-length v2, v1

    if-ge v0, v2, :cond_2

    aget v2, v1, v0

    int-to-long v2, v2

    cmp-long v2, p1, v2

    if-ltz v2, :cond_1

    array-length v2, v1

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_0

    add-int/lit8 v2, v0, 0x1

    aget v1, v1, v2

    int-to-long v1, v1

    goto :goto_1

    :cond_0
    const-wide v1, 0x7fffffffffffffffL

    :goto_1
    cmp-long v1, p1, v1

    if-gez v1, :cond_1

    return v0

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    const/4 p1, -0x1

    return p1
.end method

.method private inflateLyricLines(Ljava/util/ArrayList;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/CharSequence;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLocator;->mTimeArr:[I

    const/4 v1, 0x0

    if-eqz v0, :cond_5

    if-eqz p1, :cond_5

    array-length v0, v0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-eq v0, v2, :cond_0

    goto/16 :goto_4

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLocator;->mLyricLines:Ljava/util/ArrayList;

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    iget-object v3, p0, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLocator;->mTimeArr:[I

    array-length v3, v3

    if-ge v2, v3, :cond_3

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    new-instance v4, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLine;

    iget-object v5, p0, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLocator;->this$0:Lcom/miui/maml/elements/MusicLyricParser$Lyric;

    invoke-direct {v4, v5}, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLine;-><init>(Lcom/miui/maml/elements/MusicLyricParser$Lyric;)V

    iput-object v3, v4, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLine;->lyric:Ljava/lang/CharSequence;

    if-lez v2, :cond_1

    iget-object v3, p0, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLocator;->mLyricLines:Ljava/util/ArrayList;

    add-int/lit8 v5, v2, -0x1

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLine;

    goto :goto_1

    :cond_1
    move-object v3, v1

    :goto_1
    if-eqz v3, :cond_2

    iget v5, v3, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLine;->pos:I

    iget-object v3, v3, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLine;->lyric:Ljava/lang/CharSequence;

    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v3

    add-int/2addr v5, v3

    iget v3, p0, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLocator;->CRLF_LENGTH:I

    add-int/2addr v3, v5

    goto :goto_2

    :cond_2
    move v3, v0

    :goto_2
    iput v3, v4, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLine;->pos:I

    iget-object v3, p0, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLocator;->mLyricLines:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    const-string p1, ""

    iput-object p1, p0, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLocator;->mFullLyric:Ljava/lang/String;

    :goto_3
    iget-object p1, p0, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLocator;->mLyricLines:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result p1

    if-ge v0, p1, :cond_4

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLocator;->mFullLyric:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLocator;->mLyricLines:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLine;

    iget-object v1, v1, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLine;->lyric:Ljava/lang/CharSequence;

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "\r\n"

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLocator;->mFullLyric:Ljava/lang/String;

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_4
    return-void

    :cond_5
    :goto_4
    iput-object v1, p0, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLocator;->mTimeArr:[I

    iput-object v1, p0, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLocator;->mLyricLines:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method getAfterLines(J)Ljava/lang/String;
    .locals 2

    goto/32 :goto_f

    nop

    :goto_0
    return-object v1

    :goto_1
    check-cast p1, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLine;

    goto/32 :goto_18

    nop

    :goto_2
    iget-object p1, p0, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLocator;->mFullLyric:Ljava/lang/String;

    goto/32 :goto_6

    nop

    :goto_3
    iget-object p2, p0, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLocator;->mTimeArr:[I

    goto/32 :goto_1d

    nop

    :goto_4
    add-int/lit8 p2, p2, -0x1

    goto/32 :goto_17

    nop

    :goto_5
    if-eqz v0, :cond_0

    goto/32 :goto_8

    :cond_0
    goto/32 :goto_7

    nop

    :goto_6
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    goto/32 :goto_1a

    nop

    :goto_7
    return-object v1

    :goto_8
    goto/32 :goto_14

    nop

    :goto_9
    const/4 v1, 0x0

    goto/32 :goto_5

    nop

    :goto_a
    return-object p1

    :goto_b
    goto/32 :goto_0

    nop

    :goto_c
    iget p1, p0, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLocator;->CRLF_LENGTH:I

    goto/32 :goto_1c

    nop

    :goto_d
    return-object p1

    :goto_e
    goto/32 :goto_3

    nop

    :goto_f
    iget-object v0, p0, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLocator;->mTimeArr:[I

    goto/32 :goto_9

    nop

    :goto_10
    add-int/2addr v0, p1

    goto/32 :goto_c

    nop

    :goto_11
    iget v0, p1, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLine;->pos:I

    goto/32 :goto_19

    nop

    :goto_12
    iget-object p1, p0, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLocator;->mFullLyric:Ljava/lang/String;

    goto/32 :goto_d

    nop

    :goto_13
    if-ltz p1, :cond_1

    goto/32 :goto_e

    :cond_1
    goto/32 :goto_12

    nop

    :goto_14
    invoke-direct {p0, p1, p2}, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLocator;->getLineNumber(J)I

    move-result p1

    goto/32 :goto_13

    nop

    :goto_15
    iget-object p2, p0, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLocator;->mLyricLines:Ljava/util/ArrayList;

    goto/32 :goto_16

    nop

    :goto_16
    invoke-virtual {p2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    goto/32 :goto_1

    nop

    :goto_17
    if-lt p1, p2, :cond_2

    goto/32 :goto_b

    :cond_2
    goto/32 :goto_15

    nop

    :goto_18
    iget-object p2, p0, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLocator;->mFullLyric:Ljava/lang/String;

    goto/32 :goto_11

    nop

    :goto_19
    iget-object p1, p1, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLine;->lyric:Ljava/lang/CharSequence;

    goto/32 :goto_1b

    nop

    :goto_1a
    invoke-virtual {p2, v0, p1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_a

    nop

    :goto_1b
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result p1

    goto/32 :goto_10

    nop

    :goto_1c
    add-int/2addr v0, p1

    goto/32 :goto_2

    nop

    :goto_1d
    array-length p2, p2

    goto/32 :goto_4

    nop
.end method

.method getBeforeLines(J)Ljava/lang/String;
    .locals 2

    goto/32 :goto_b

    nop

    :goto_0
    iget-object p2, p0, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLocator;->mLyricLines:Ljava/util/ArrayList;

    goto/32 :goto_7

    nop

    :goto_1
    const/4 v0, 0x0

    goto/32 :goto_a

    nop

    :goto_2
    if-eqz v0, :cond_0

    goto/32 :goto_10

    :cond_0
    goto/32 :goto_f

    nop

    :goto_3
    iget v1, p0, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLocator;->CRLF_LENGTH:I

    goto/32 :goto_11

    nop

    :goto_4
    if-gtz p1, :cond_1

    goto/32 :goto_e

    :cond_1
    goto/32 :goto_0

    nop

    :goto_5
    invoke-direct {p0, p1, p2}, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLocator;->getLineNumber(J)I

    move-result p1

    goto/32 :goto_4

    nop

    :goto_6
    check-cast p1, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLine;

    goto/32 :goto_8

    nop

    :goto_7
    invoke-virtual {p2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    goto/32 :goto_6

    nop

    :goto_8
    iget-object p2, p0, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLocator;->mFullLyric:Ljava/lang/String;

    goto/32 :goto_1

    nop

    :goto_9
    const/4 v1, 0x0

    goto/32 :goto_2

    nop

    :goto_a
    iget p1, p1, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLine;->pos:I

    goto/32 :goto_3

    nop

    :goto_b
    iget-object v0, p0, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLocator;->mTimeArr:[I

    goto/32 :goto_9

    nop

    :goto_c
    return-object v1

    :goto_d
    return-object p1

    :goto_e
    goto/32 :goto_c

    nop

    :goto_f
    return-object v1

    :goto_10
    goto/32 :goto_5

    nop

    :goto_11
    sub-int/2addr p1, v1

    goto/32 :goto_12

    nop

    :goto_12
    invoke-virtual {p2, v0, p1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_d

    nop
.end method

.method getLastLine(J)Ljava/lang/String;
    .locals 2

    goto/32 :goto_6

    nop

    :goto_0
    add-int/lit8 p1, p1, -0x1

    goto/32 :goto_14

    nop

    :goto_1
    if-gtz p1, :cond_0

    goto/32 :goto_11

    :cond_0
    goto/32 :goto_9

    nop

    :goto_2
    const/4 v1, 0x0

    goto/32 :goto_b

    nop

    :goto_3
    return-object v1

    :goto_4
    check-cast p1, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLine;

    goto/32 :goto_f

    nop

    :goto_5
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result p1

    goto/32 :goto_c

    nop

    :goto_6
    iget-object v0, p0, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLocator;->mTimeArr:[I

    goto/32 :goto_2

    nop

    :goto_7
    invoke-virtual {p2, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_10

    nop

    :goto_8
    invoke-direct {p0, p1, p2}, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLocator;->getLineNumber(J)I

    move-result p1

    goto/32 :goto_1

    nop

    :goto_9
    iget-object p2, p0, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLocator;->mLyricLines:Ljava/util/ArrayList;

    goto/32 :goto_0

    nop

    :goto_a
    iget v1, p1, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLine;->pos:I

    goto/32 :goto_13

    nop

    :goto_b
    if-eqz v0, :cond_1

    goto/32 :goto_e

    :cond_1
    goto/32 :goto_d

    nop

    :goto_c
    add-int/2addr v1, p1

    goto/32 :goto_7

    nop

    :goto_d
    return-object v1

    :goto_e
    goto/32 :goto_8

    nop

    :goto_f
    iget-object p2, p0, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLocator;->mFullLyric:Ljava/lang/String;

    goto/32 :goto_12

    nop

    :goto_10
    return-object p1

    :goto_11
    goto/32 :goto_3

    nop

    :goto_12
    iget v0, p1, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLine;->pos:I

    goto/32 :goto_a

    nop

    :goto_13
    iget-object p1, p1, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLine;->lyric:Ljava/lang/CharSequence;

    goto/32 :goto_5

    nop

    :goto_14
    invoke-virtual {p2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    goto/32 :goto_4

    nop
.end method

.method getLine(J)Ljava/lang/String;
    .locals 2

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLocator;->mTimeArr:[I

    goto/32 :goto_6

    nop

    :goto_1
    iget v1, p1, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLine;->pos:I

    goto/32 :goto_b

    nop

    :goto_2
    const/4 p2, -0x1

    goto/32 :goto_d

    nop

    :goto_3
    iget-object p2, p0, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLocator;->mFullLyric:Ljava/lang/String;

    goto/32 :goto_7

    nop

    :goto_4
    invoke-virtual {p2, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_11

    nop

    :goto_5
    iget-object p2, p0, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLocator;->mLyricLines:Ljava/util/ArrayList;

    goto/32 :goto_f

    nop

    :goto_6
    const/4 v1, 0x0

    goto/32 :goto_e

    nop

    :goto_7
    iget v0, p1, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLine;->pos:I

    goto/32 :goto_1

    nop

    :goto_8
    return-object v1

    :goto_9
    goto/32 :goto_a

    nop

    :goto_a
    invoke-direct {p0, p1, p2}, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLocator;->getLineNumber(J)I

    move-result p1

    goto/32 :goto_2

    nop

    :goto_b
    iget-object p1, p1, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLine;->lyric:Ljava/lang/CharSequence;

    goto/32 :goto_14

    nop

    :goto_c
    return-object v1

    :goto_d
    if-ne p1, p2, :cond_0

    goto/32 :goto_12

    :cond_0
    goto/32 :goto_5

    nop

    :goto_e
    if-eqz v0, :cond_1

    goto/32 :goto_9

    :cond_1
    goto/32 :goto_8

    nop

    :goto_f
    invoke-virtual {p2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    goto/32 :goto_13

    nop

    :goto_10
    add-int/2addr v1, p1

    goto/32 :goto_4

    nop

    :goto_11
    return-object p1

    :goto_12
    goto/32 :goto_c

    nop

    :goto_13
    check-cast p1, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLine;

    goto/32 :goto_3

    nop

    :goto_14
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result p1

    goto/32 :goto_10

    nop
.end method

.method getNextLine(J)Ljava/lang/String;
    .locals 2

    goto/32 :goto_9

    nop

    :goto_0
    iget-object p2, p0, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLocator;->mLyricLines:Ljava/util/ArrayList;

    goto/32 :goto_3

    nop

    :goto_1
    return-object p1

    :goto_2
    goto/32 :goto_19

    nop

    :goto_3
    add-int/lit8 p1, p1, 0x1

    goto/32 :goto_8

    nop

    :goto_4
    if-ge p1, p2, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_d

    nop

    :goto_5
    iget v0, p1, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLine;->pos:I

    goto/32 :goto_18

    nop

    :goto_6
    invoke-direct {p0, p1, p2}, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLocator;->getLineNumber(J)I

    move-result p1

    goto/32 :goto_15

    nop

    :goto_7
    check-cast p1, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLine;

    goto/32 :goto_17

    nop

    :goto_8
    invoke-virtual {p2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    goto/32 :goto_7

    nop

    :goto_9
    iget-object v0, p0, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLocator;->mTimeArr:[I

    goto/32 :goto_12

    nop

    :goto_a
    return-object v1

    :goto_b
    goto/32 :goto_6

    nop

    :goto_c
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result p1

    goto/32 :goto_10

    nop

    :goto_d
    iget-object p2, p0, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLocator;->mTimeArr:[I

    goto/32 :goto_f

    nop

    :goto_e
    add-int/lit8 p2, p2, -0x1

    goto/32 :goto_11

    nop

    :goto_f
    array-length p2, p2

    goto/32 :goto_e

    nop

    :goto_10
    add-int/2addr v1, p1

    goto/32 :goto_13

    nop

    :goto_11
    if-lt p1, p2, :cond_1

    goto/32 :goto_2

    :cond_1
    goto/32 :goto_0

    nop

    :goto_12
    const/4 v1, 0x0

    goto/32 :goto_16

    nop

    :goto_13
    invoke-virtual {p2, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_1

    nop

    :goto_14
    iget-object p1, p1, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLine;->lyric:Ljava/lang/CharSequence;

    goto/32 :goto_c

    nop

    :goto_15
    const/4 p2, -0x1

    goto/32 :goto_4

    nop

    :goto_16
    if-eqz v0, :cond_2

    goto/32 :goto_b

    :cond_2
    goto/32 :goto_a

    nop

    :goto_17
    iget-object p2, p0, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLocator;->mFullLyric:Ljava/lang/String;

    goto/32 :goto_5

    nop

    :goto_18
    iget v1, p1, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLine;->pos:I

    goto/32 :goto_14

    nop

    :goto_19
    return-object v1
.end method

.method set([ILjava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([I",
            "Ljava/util/ArrayList<",
            "Ljava/lang/CharSequence;",
            ">;)V"
        }
    .end annotation

    goto/32 :goto_2

    nop

    :goto_0
    return-void

    :goto_1
    invoke-direct {p0, p2}, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLocator;->inflateLyricLines(Ljava/util/ArrayList;)V

    goto/32 :goto_0

    nop

    :goto_2
    iput-object p1, p0, Lcom/miui/maml/elements/MusicLyricParser$Lyric$LyricLocator;->mTimeArr:[I

    goto/32 :goto_1

    nop
.end method
