.class public Lcom/miui/maml/animation/VariableAnimation;
.super Lcom/miui/maml/animation/BaseAnimation;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/maml/animation/VariableAnimation$Color;
    }
.end annotation


# static fields
.field public static final INNER_TAG_NAME:Ljava/lang/String; = "AniFrame"

.field public static final TAG_NAME:Ljava/lang/String; = "VariableAnimation"


# instance fields
.field private mArgbEvaluator:Landroid/animation/ArgbEvaluator;

.field private mCurValuesStr:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Lorg/w3c/dom/Element;Lcom/miui/maml/elements/ScreenElement;)V
    .locals 1

    const-string v0, "AniFrame"

    invoke-direct {p0, p1, v0, p2}, Lcom/miui/maml/animation/BaseAnimation;-><init>(Lorg/w3c/dom/Element;Ljava/lang/String;Lcom/miui/maml/elements/ScreenElement;)V

    const-string p1, "color"

    iget-object p2, p0, Lcom/miui/maml/animation/VariableAnimation;->mType:Ljava/lang/String;

    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/miui/maml/animation/VariableAnimation;->mAttrs:[Ljava/lang/String;

    array-length p1, p1

    new-array p1, p1, [Ljava/lang/String;

    iput-object p1, p0, Lcom/miui/maml/animation/VariableAnimation;->mCurValuesStr:[Ljava/lang/String;

    new-instance p1, Landroid/animation/ArgbEvaluator;

    invoke-direct {p1}, Landroid/animation/ArgbEvaluator;-><init>()V

    iput-object p1, p0, Lcom/miui/maml/animation/VariableAnimation;->mArgbEvaluator:Landroid/animation/ArgbEvaluator;

    :cond_0
    return-void
.end method


# virtual methods
.method public final getValue()D
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/miui/maml/animation/VariableAnimation;->getCurValue(I)D

    move-result-wide v0

    return-wide v0
.end method

.method public final getValueStr()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/miui/maml/animation/VariableAnimation;->mCurValuesStr:[Ljava/lang/String;

    if-eqz v0, :cond_0

    array-length v1, v0

    if-lez v1, :cond_0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    return-object v0

    :cond_0
    const-string v0, ""

    return-object v0
.end method

.method protected onCreateItem(Lcom/miui/maml/animation/BaseAnimation;Lorg/w3c/dom/Element;)Lcom/miui/maml/animation/BaseAnimation$AnimationItem;
    .locals 2

    const-string v0, "color"

    iget-object v1, p0, Lcom/miui/maml/animation/VariableAnimation;->mType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/miui/maml/animation/VariableAnimation$Color;

    invoke-direct {v0, p1, p2}, Lcom/miui/maml/animation/VariableAnimation$Color;-><init>(Lcom/miui/maml/animation/BaseAnimation;Lorg/w3c/dom/Element;)V

    return-object v0

    :cond_0
    new-instance v0, Lcom/miui/maml/animation/BaseAnimation$AnimationItem;

    invoke-direct {v0, p1, p2}, Lcom/miui/maml/animation/BaseAnimation$AnimationItem;-><init>(Lcom/miui/maml/animation/BaseAnimation;Lorg/w3c/dom/Element;)V

    return-object v0
.end method

.method protected onTick(Lcom/miui/maml/animation/BaseAnimation$AnimationItem;Lcom/miui/maml/animation/BaseAnimation$AnimationItem;F)V
    .locals 5

    const-string v0, "color"

    iget-object v1, p0, Lcom/miui/maml/animation/VariableAnimation;->mType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    if-eqz p1, :cond_1

    if-nez p2, :cond_0

    goto :goto_1

    :cond_0
    iget-object v0, p0, Lcom/miui/maml/animation/VariableAnimation;->mAttrs:[Ljava/lang/String;

    array-length v0, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_3

    move-object v2, p1

    check-cast v2, Lcom/miui/maml/animation/VariableAnimation$Color;

    iget-object v2, v2, Lcom/miui/maml/animation/VariableAnimation$Color;->color:Ljava/lang/String;

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    move-object v3, p2

    check-cast v3, Lcom/miui/maml/animation/VariableAnimation$Color;

    iget-object v3, v3, Lcom/miui/maml/animation/VariableAnimation$Color;->color:Ljava/lang/String;

    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v3

    iget-object v4, p0, Lcom/miui/maml/animation/VariableAnimation;->mArgbEvaluator:Landroid/animation/ArgbEvaluator;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v4, p3, v2, v3}, Landroid/animation/ArgbEvaluator;->evaluate(FLjava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/miui/maml/util/ColorUtils;->ColorToHex(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/miui/maml/animation/VariableAnimation;->mCurValuesStr:[Ljava/lang/String;

    aput-object v2, v3, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    return-void

    :cond_2
    invoke-super {p0, p1, p2, p3}, Lcom/miui/maml/animation/BaseAnimation;->onTick(Lcom/miui/maml/animation/BaseAnimation$AnimationItem;Lcom/miui/maml/animation/BaseAnimation$AnimationItem;F)V

    :cond_3
    return-void
.end method
