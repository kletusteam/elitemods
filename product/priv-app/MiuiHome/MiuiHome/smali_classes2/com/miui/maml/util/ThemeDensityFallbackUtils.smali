.class public Lcom/miui/maml/util/ThemeDensityFallbackUtils;
.super Ljava/lang/Object;


# static fields
.field private static final ALL_SUPPORT_DENSITY:[I

.field private static final DENSITIES:[I

.field private static final DENSITY_NONE:I = 0x1


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/miui/maml/util/ThemeDensityFallbackUtils;->DENSITIES:[I

    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/miui/maml/util/ThemeDensityFallbackUtils;->ALL_SUPPORT_DENSITY:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x280
        0x1e0
        0x140
        0xf0
        0x0
    .end array-data

    :array_1
    .array-data 4
        0x280
        0x1e0
        0x140
        0x1b8
        0xf0
        0xa0
        0x78
    .end array-data
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static findNearestDir(Ljava/lang/String;I)Ljava/lang/String;
    .locals 5

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    sget-object v3, Lcom/miui/maml/util/ThemeDensityFallbackUtils;->ALL_SUPPORT_DENSITY:[I

    array-length v4, v3

    if-ge v2, v4, :cond_0

    aget v3, v3, v2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    new-instance v2, Lcom/miui/maml/util/ThemeDensityFallbackUtils$1;

    invoke-direct {v2, p1}, Lcom/miui/maml/util/ThemeDensityFallbackUtils$1;-><init>(I)V

    invoke-static {v0, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    :goto_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result p1

    if-ge v1, p1, :cond_2

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "drawable"

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    invoke-static {v2}, Lcom/miui/maml/util/ThemeDensityFallbackUtils;->getScreenWidthSuffix(Landroid/content/res/Configuration;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/miui/maml/util/ThemeDensityFallbackUtils;->getDensitySuffix(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v2, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    return-object p1

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    const/4 p0, 0x0

    return-object p0
.end method

.method public static findNearestSupportDensity(I)I
    .locals 5

    const/16 v0, 0x1b8

    const v1, 0x7fffffff

    const/4 v2, 0x0

    :goto_0
    sget-object v3, Lcom/miui/maml/util/ThemeDensityFallbackUtils;->ALL_SUPPORT_DENSITY:[I

    array-length v4, v3

    if-ge v2, v4, :cond_1

    aget v3, v3, v2

    sub-int v3, p0, v3

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    if-ge v3, v1, :cond_0

    sget-object v0, Lcom/miui/maml/util/ThemeDensityFallbackUtils;->ALL_SUPPORT_DENSITY:[I

    aget v0, v0, v2

    move v1, v3

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return v0
.end method

.method public static getDensitySuffix(I)Ljava/lang/String;
    .locals 4

    const/16 v0, 0x78

    if-eq p0, v0, :cond_8

    const/16 v0, 0xa0

    if-eq p0, v0, :cond_7

    const/16 v0, 0xf0

    if-eq p0, v0, :cond_6

    const/16 v0, 0x140

    if-eq p0, v0, :cond_5

    const/16 v0, 0x1b8

    if-eq p0, v0, :cond_4

    const/16 v0, 0x1e0

    if-eq p0, v0, :cond_3

    const/16 v0, 0x280

    if-eq p0, v0, :cond_2

    packed-switch p0, :pswitch_data_0

    sget-object v0, Lcom/miui/maml/util/ThemeDensityFallbackUtils;->DENSITIES:[I

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    add-int/lit8 v1, v0, -0x1

    :goto_0
    if-ltz v1, :cond_1

    sget-object v2, Lcom/miui/maml/util/ThemeDensityFallbackUtils;->DENSITIES:[I

    aget v2, v2, v1

    sub-int/2addr v2, p0

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    sget-object v3, Lcom/miui/maml/util/ThemeDensityFallbackUtils;->DENSITIES:[I

    aget v3, v3, v0

    sub-int/2addr v3, p0

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    if-gt v2, v3, :cond_0

    move v0, v1

    :cond_0
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_1
    sget-object p0, Lcom/miui/maml/util/ThemeDensityFallbackUtils;->DENSITIES:[I

    aget p0, p0, v0

    invoke-static {p0}, Lcom/miui/maml/util/ThemeDensityFallbackUtils;->getDensitySuffix(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :pswitch_0
    const-string p0, "-nodpi"

    return-object p0

    :pswitch_1
    const-string p0, ""

    return-object p0

    :cond_2
    const-string p0, "-xxxhdpi"

    return-object p0

    :cond_3
    const-string p0, "-xxhdpi"

    return-object p0

    :cond_4
    const-string p0, "-nxhdpi"

    return-object p0

    :cond_5
    const-string p0, "-xhdpi"

    return-object p0

    :cond_6
    const-string p0, "-hdpi"

    return-object p0

    :cond_7
    const-string p0, "-mdpi"

    return-object p0

    :cond_8
    const-string p0, "-ldpi"

    return-object p0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static getScreenWidthSuffix(Landroid/content/res/Configuration;)Ljava/lang/String;
    .locals 1

    iget p0, p0, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    const/16 v0, 0x2d0

    if-lt p0, v0, :cond_0

    const-string p0, "-sw720dp"

    return-object p0

    :cond_0
    const-string p0, ""

    return-object p0
.end method
