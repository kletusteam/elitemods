.class Lcom/miui/home/settings/preference/DesktopModePreference$1;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/home/settings/preference/DesktopModePreference;->onBindViewHolder(Landroidx/preference/PreferenceViewHolder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/home/settings/preference/DesktopModePreference;


# direct methods
.method constructor <init>(Lcom/miui/home/settings/preference/DesktopModePreference;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/home/settings/preference/DesktopModePreference$1;->this$0:Lcom/miui/home/settings/preference/DesktopModePreference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 0

    invoke-virtual {p1, p0}, Landroid/view/View;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result p2

    invoke-virtual {p1}, Landroid/view/View;->getPaddingStart()I

    move-result p3

    sub-int/2addr p2, p3

    invoke-virtual {p1}, Landroid/view/View;->getPaddingEnd()I

    move-result p1

    sub-int/2addr p2, p1

    iget-object p1, p0, Lcom/miui/home/settings/preference/DesktopModePreference$1;->this$0:Lcom/miui/home/settings/preference/DesktopModePreference;

    invoke-static {p1}, Lcom/miui/home/settings/preference/DesktopModePreference;->access$000(Lcom/miui/home/settings/preference/DesktopModePreference;)Lmiuix/visual/check/VisualCheckBox;

    move-result-object p1

    invoke-virtual {p1}, Lmiuix/visual/check/VisualCheckBox;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    invoke-static {}, Lcom/miui/home/settings/preference/DesktopModePreference;->access$100()Z

    move-result p3

    if-eqz p3, :cond_0

    const/4 p3, 0x3

    goto :goto_0

    :cond_0
    const/4 p3, 0x2

    :goto_0
    div-int/2addr p2, p3

    iput p2, p1, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget-object p2, p0, Lcom/miui/home/settings/preference/DesktopModePreference$1;->this$0:Lcom/miui/home/settings/preference/DesktopModePreference;

    invoke-static {p2}, Lcom/miui/home/settings/preference/DesktopModePreference;->access$000(Lcom/miui/home/settings/preference/DesktopModePreference;)Lmiuix/visual/check/VisualCheckBox;

    move-result-object p2

    invoke-virtual {p2, p1}, Lmiuix/visual/check/VisualCheckBox;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object p2, p0, Lcom/miui/home/settings/preference/DesktopModePreference$1;->this$0:Lcom/miui/home/settings/preference/DesktopModePreference;

    invoke-static {p2}, Lcom/miui/home/settings/preference/DesktopModePreference;->access$200(Lcom/miui/home/settings/preference/DesktopModePreference;)Lmiuix/visual/check/VisualCheckBox;

    move-result-object p2

    invoke-virtual {p2, p1}, Lmiuix/visual/check/VisualCheckBox;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object p2, p0, Lcom/miui/home/settings/preference/DesktopModePreference$1;->this$0:Lcom/miui/home/settings/preference/DesktopModePreference;

    invoke-static {p2}, Lcom/miui/home/settings/preference/DesktopModePreference;->access$300(Lcom/miui/home/settings/preference/DesktopModePreference;)Lmiuix/visual/check/VisualCheckBox;

    move-result-object p2

    invoke-virtual {p2, p1}, Lmiuix/visual/check/VisualCheckBox;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method
