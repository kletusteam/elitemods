.class public abstract Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;
.super Landroid/graphics/drawable/Drawable;


# instance fields
.field private mApplyGravity:Z

.field final mBitmap:Landroid/graphics/Bitmap;

.field private mBitmapHeight:I

.field private final mBitmapShader:Landroid/graphics/BitmapShader;

.field private mBitmapWidth:I

.field private mCornerRadius:F

.field final mDstRect:Landroid/graphics/Rect;

.field private final mDstRectF:Landroid/graphics/RectF;

.field private mGravity:I

.field private mIsCircular:Z

.field private final mPaint:Landroid/graphics/Paint;

.field private final mShaderMatrix:Landroid/graphics/Matrix;

.field private mTargetDensity:I


# direct methods
.method constructor <init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V
    .locals 2

    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    const/16 v0, 0xa0

    iput v0, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mTargetDensity:I

    const/16 v0, 0x77

    iput v0, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mGravity:I

    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mPaint:Landroid/graphics/Paint;

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mShaderMatrix:Landroid/graphics/Matrix;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mDstRect:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mDstRectF:Landroid/graphics/RectF;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mApplyGravity:Z

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p1

    iget p1, p1, Landroid/util/DisplayMetrics;->densityDpi:I

    iput p1, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mTargetDensity:I

    :cond_0
    iput-object p2, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mBitmap:Landroid/graphics/Bitmap;

    iget-object p1, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz p1, :cond_1

    invoke-direct {p0}, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->computeBitmapSize()V

    new-instance p1, Landroid/graphics/BitmapShader;

    iget-object p2, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mBitmap:Landroid/graphics/Bitmap;

    sget-object v0, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    sget-object v1, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    invoke-direct {p1, p2, v0, v1}, Landroid/graphics/BitmapShader;-><init>(Landroid/graphics/Bitmap;Landroid/graphics/Shader$TileMode;Landroid/graphics/Shader$TileMode;)V

    iput-object p1, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mBitmapShader:Landroid/graphics/BitmapShader;

    goto :goto_0

    :cond_1
    const/4 p1, -0x1

    iput p1, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mBitmapHeight:I

    iput p1, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mBitmapWidth:I

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mBitmapShader:Landroid/graphics/BitmapShader;

    :goto_0
    return-void
.end method

.method private computeBitmapSize()V
    .locals 2

    iget-object v0, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mBitmap:Landroid/graphics/Bitmap;

    iget v1, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mTargetDensity:I

    invoke-virtual {v0, v1}, Landroid/graphics/Bitmap;->getScaledWidth(I)I

    move-result v0

    iput v0, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mBitmapWidth:I

    iget-object v0, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mBitmap:Landroid/graphics/Bitmap;

    iget v1, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mTargetDensity:I

    invoke-virtual {v0, v1}, Landroid/graphics/Bitmap;->getScaledHeight(I)I

    move-result v0

    iput v0, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mBitmapHeight:I

    return-void
.end method

.method private static isGreaterThanZero(F)Z
    .locals 1

    const v0, 0x3d4ccccd    # 0.05f

    cmpl-float p0, p0, v0

    if-lez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private updateCircularCornerRadius()V
    .locals 2

    iget v0, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mBitmapHeight:I

    iget v1, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mBitmapWidth:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    iput v0, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mCornerRadius:F

    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 4

    iget-object v0, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mBitmap:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->updateDstRect()V

    iget-object v1, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->getShader()Landroid/graphics/Shader;

    move-result-object v1

    if-nez v1, :cond_1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mDstRect:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mDstRectF:Landroid/graphics/RectF;

    iget v1, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mCornerRadius:F

    iget-object v2, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v1, v2}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    :goto_0
    return-void
.end method

.method public getAlpha()I
    .locals 1

    iget-object v0, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getAlpha()I

    move-result v0

    return v0
.end method

.method public getColorFilter()Landroid/graphics/ColorFilter;
    .locals 1

    iget-object v0, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColorFilter()Landroid/graphics/ColorFilter;

    move-result-object v0

    return-object v0
.end method

.method public getCornerRadius()F
    .locals 1

    iget v0, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mCornerRadius:F

    return v0
.end method

.method public getIntrinsicHeight()I
    .locals 1

    iget v0, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mBitmapHeight:I

    return v0
.end method

.method public getIntrinsicWidth()I
    .locals 1

    iget v0, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mBitmapWidth:I

    return v0
.end method

.method public getOpacity()I
    .locals 3

    iget v0, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mGravity:I

    const/4 v1, -0x3

    const/16 v2, 0x77

    if-ne v0, v2, :cond_3

    iget-boolean v0, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mIsCircular:Z

    if-eqz v0, :cond_0

    goto :goto_1

    :cond_0
    iget-object v0, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->hasAlpha()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getAlpha()I

    move-result v0

    const/16 v2, 0xff

    if-lt v0, v2, :cond_2

    iget v0, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mCornerRadius:F

    invoke-static {v0}, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->isGreaterThanZero(F)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, -0x1

    :cond_2
    :goto_0
    return v1

    :cond_3
    :goto_1
    return v1
.end method

.method gravityCompatApply(IIILandroid/graphics/Rect;Landroid/graphics/Rect;)V
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    goto/32 :goto_2

    nop

    :goto_1
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    goto/32 :goto_0

    nop

    :goto_2
    throw p1
.end method

.method protected onBoundsChange(Landroid/graphics/Rect;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/graphics/drawable/Drawable;->onBoundsChange(Landroid/graphics/Rect;)V

    iget-boolean p1, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mIsCircular:Z

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->updateCircularCornerRadius()V

    :cond_0
    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mApplyGravity:Z

    return-void
.end method

.method public setAlpha(I)V
    .locals 1

    iget-object v0, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getAlpha()I

    move-result v0

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    invoke-virtual {p0}, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->invalidateSelf()V

    :cond_0
    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1

    iget-object v0, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    invoke-virtual {p0}, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->invalidateSelf()V

    return-void
.end method

.method public setCornerRadius(F)V
    .locals 2

    iget v0, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mCornerRadius:F

    cmpl-float v0, v0, p1

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mIsCircular:Z

    invoke-static {p1}, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->isGreaterThanZero(F)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mBitmapShader:Landroid/graphics/BitmapShader;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mPaint:Landroid/graphics/Paint;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    :goto_0
    iput p1, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mCornerRadius:F

    invoke-virtual {p0}, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->invalidateSelf()V

    return-void
.end method

.method public setDither(Z)V
    .locals 1

    iget-object v0, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setDither(Z)V

    invoke-virtual {p0}, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->invalidateSelf()V

    return-void
.end method

.method public setFilterBitmap(Z)V
    .locals 1

    iget-object v0, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    invoke-virtual {p0}, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->invalidateSelf()V

    return-void
.end method

.method updateDstRect()V
    .locals 9

    goto/32 :goto_2

    nop

    :goto_0
    const/high16 v2, 0x3f000000    # 0.5f

    goto/32 :goto_9

    nop

    :goto_1
    if-nez v0, :cond_0

    goto/32 :goto_37

    :cond_0
    goto/32 :goto_14

    nop

    :goto_2
    iget-boolean v0, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mApplyGravity:Z

    goto/32 :goto_3d

    nop

    :goto_3
    iget-object v4, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mBitmap:Landroid/graphics/Bitmap;

    goto/32 :goto_41

    nop

    :goto_4
    move v5, v6

    goto/32 :goto_2f

    nop

    :goto_5
    if-nez v0, :cond_1

    goto/32 :goto_4c

    :cond_1
    goto/32 :goto_1c

    nop

    :goto_6
    iget v3, v3, Landroid/graphics/RectF;->top:F

    goto/32 :goto_d

    nop

    :goto_7
    return-void

    :goto_8
    iget-object v3, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mDstRectF:Landroid/graphics/RectF;

    goto/32 :goto_6

    nop

    :goto_9
    int-to-float v0, v0

    goto/32 :goto_32

    nop

    :goto_a
    invoke-virtual {v0, v2}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    goto/32 :goto_1b

    nop

    :goto_b
    sub-int/2addr v3, v0

    goto/32 :goto_31

    nop

    :goto_c
    iget-object v2, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mBitmapShader:Landroid/graphics/BitmapShader;

    goto/32 :goto_36

    nop

    :goto_d
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Matrix;->setTranslate(FF)V

    goto/32 :goto_39

    nop

    :goto_e
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    goto/32 :goto_33

    nop

    :goto_f
    iget-object v2, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mShaderMatrix:Landroid/graphics/Matrix;

    goto/32 :goto_3e

    nop

    :goto_10
    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    goto/32 :goto_4f

    nop

    :goto_11
    invoke-virtual/range {v2 .. v7}, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->gravityCompatApply(IIILandroid/graphics/Rect;Landroid/graphics/Rect;)V

    :goto_12
    goto/32 :goto_2e

    nop

    :goto_13
    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    goto/32 :goto_21

    nop

    :goto_14
    iget-object v0, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mShaderMatrix:Landroid/graphics/Matrix;

    goto/32 :goto_1d

    nop

    :goto_15
    iget-object v0, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mDstRect:Landroid/graphics/Rect;

    goto/32 :goto_e

    nop

    :goto_16
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    goto/32 :goto_2c

    nop

    :goto_17
    iget-object v3, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mDstRect:Landroid/graphics/Rect;

    goto/32 :goto_30

    nop

    :goto_18
    iget v2, v2, Landroid/graphics/RectF;->left:F

    goto/32 :goto_8

    nop

    :goto_19
    iget-object v0, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mPaint:Landroid/graphics/Paint;

    goto/32 :goto_c

    nop

    :goto_1a
    invoke-virtual {v4, v2, v3}, Landroid/graphics/Rect;->inset(II)V

    goto/32 :goto_0

    nop

    :goto_1b
    iget-object v0, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mBitmapShader:Landroid/graphics/BitmapShader;

    goto/32 :goto_1

    nop

    :goto_1c
    iget v0, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mBitmapWidth:I

    goto/32 :goto_4a

    nop

    :goto_1d
    iget-object v2, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mDstRectF:Landroid/graphics/RectF;

    goto/32 :goto_18

    nop

    :goto_1e
    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v2

    goto/32 :goto_24

    nop

    :goto_1f
    int-to-float v4, v4

    goto/32 :goto_3a

    nop

    :goto_20
    const/4 v1, 0x0

    goto/32 :goto_5

    nop

    :goto_21
    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto/32 :goto_48

    nop

    :goto_22
    iget-object v3, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mDstRectF:Landroid/graphics/RectF;

    goto/32 :goto_42

    nop

    :goto_23
    iget v4, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mBitmapWidth:I

    goto/32 :goto_49

    nop

    :goto_24
    iget-object v3, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mBitmap:Landroid/graphics/Bitmap;

    goto/32 :goto_16

    nop

    :goto_25
    iget-object v0, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mBitmapShader:Landroid/graphics/BitmapShader;

    goto/32 :goto_f

    nop

    :goto_26
    iput-boolean v1, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mApplyGravity:Z

    :goto_27
    goto/32 :goto_7

    nop

    :goto_28
    invoke-virtual {v0, v2, v3}, Landroid/graphics/Matrix;->preScale(FF)Z

    goto/32 :goto_25

    nop

    :goto_29
    iget-object v4, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mDstRect:Landroid/graphics/Rect;

    goto/32 :goto_1a

    nop

    :goto_2a
    iget v3, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mGravity:I

    goto/32 :goto_23

    nop

    :goto_2b
    iget-object v2, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mDstRectF:Landroid/graphics/RectF;

    goto/32 :goto_1e

    nop

    :goto_2c
    int-to-float v3, v3

    goto/32 :goto_4d

    nop

    :goto_2d
    iget-object v8, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mDstRect:Landroid/graphics/Rect;

    goto/32 :goto_43

    nop

    :goto_2e
    iget-object v0, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mDstRectF:Landroid/graphics/RectF;

    goto/32 :goto_45

    nop

    :goto_2f
    invoke-virtual/range {v3 .. v8}, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->gravityCompatApply(IIILandroid/graphics/Rect;Landroid/graphics/Rect;)V

    goto/32 :goto_15

    nop

    :goto_30
    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    goto/32 :goto_b

    nop

    :goto_31
    div-int/lit8 v3, v3, 0x2

    goto/32 :goto_38

    nop

    :goto_32
    mul-float/2addr v0, v2

    goto/32 :goto_3c

    nop

    :goto_33
    iget-object v2, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mDstRect:Landroid/graphics/Rect;

    goto/32 :goto_13

    nop

    :goto_34
    div-int/lit8 v2, v2, 0x2

    goto/32 :goto_47

    nop

    :goto_35
    invoke-virtual {p0}, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v6

    goto/32 :goto_46

    nop

    :goto_36
    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    :goto_37
    goto/32 :goto_26

    nop

    :goto_38
    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    goto/32 :goto_29

    nop

    :goto_39
    iget-object v0, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mShaderMatrix:Landroid/graphics/Matrix;

    goto/32 :goto_2b

    nop

    :goto_3a
    div-float/2addr v3, v4

    goto/32 :goto_28

    nop

    :goto_3b
    move-object v2, p0

    goto/32 :goto_11

    nop

    :goto_3c
    iput v0, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mCornerRadius:F

    goto/32 :goto_4b

    nop

    :goto_3d
    if-nez v0, :cond_2

    goto/32 :goto_27

    :cond_2
    goto/32 :goto_44

    nop

    :goto_3e
    invoke-virtual {v0, v2}, Landroid/graphics/BitmapShader;->setLocalMatrix(Landroid/graphics/Matrix;)V

    goto/32 :goto_19

    nop

    :goto_3f
    iget v4, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mGravity:I

    goto/32 :goto_40

    nop

    :goto_40
    invoke-virtual {p0}, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v7

    goto/32 :goto_2d

    nop

    :goto_41
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    goto/32 :goto_1f

    nop

    :goto_42
    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v3

    goto/32 :goto_3

    nop

    :goto_43
    move-object v3, p0

    goto/32 :goto_4

    nop

    :goto_44
    iget-boolean v0, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mIsCircular:Z

    goto/32 :goto_20

    nop

    :goto_45
    iget-object v2, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mDstRect:Landroid/graphics/Rect;

    goto/32 :goto_a

    nop

    :goto_46
    iget-object v7, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mDstRect:Landroid/graphics/Rect;

    goto/32 :goto_3b

    nop

    :goto_47
    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    goto/32 :goto_17

    nop

    :goto_48
    iget-object v2, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mDstRect:Landroid/graphics/Rect;

    goto/32 :goto_10

    nop

    :goto_49
    iget v5, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mBitmapHeight:I

    goto/32 :goto_35

    nop

    :goto_4a
    iget v2, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;->mBitmapHeight:I

    goto/32 :goto_4e

    nop

    :goto_4b
    goto/16 :goto_12

    :goto_4c
    goto/32 :goto_2a

    nop

    :goto_4d
    div-float/2addr v2, v3

    goto/32 :goto_22

    nop

    :goto_4e
    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v6

    goto/32 :goto_3f

    nop

    :goto_4f
    sub-int/2addr v2, v0

    goto/32 :goto_34

    nop
.end method
