.class public Lcom/miui/home/recents/views/MyTaskStackView;
.super Lcom/miui/home/recents/views/TaskStackView;

# interfaces
.implements Landroid/preference/CustomUpdater$CustomReceiver;


# static fields
.field static DEFAULT_ROTATE_CAMERA_DISTANCE:F

.field private static final TAG:Ljava/lang/String;

.field private static instance:Lcom/miui/home/recents/views/MyTaskStackView;


# instance fields
.field private firstStart:Z

.field private isNeedReset:Z

.field private isReload:Z

.field private isVertical:Z

.field private lastFakeEvent:J

.field private mDefCameraDistance:F

.field private final mFakeRun:Ljava/lang/Runnable;

.field private final mHorizontalGap:I

.field private mStyleAnim:I

.field private mTouchHandler:Lcom/miui/home/recents/views/TaskStackViewTouchHandler;

.field private final mVerticalGap:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-class v1, Lcom/miui/home/recents/views/MyTaskStackView;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/miui/home/recents/views/MyTaskStackView;->TAG:Ljava/lang/String;

    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    iget v1, v0, Landroid/util/DisplayMetrics;->density:F

    float-to-double v4, v1

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    iget v1, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-double v4, v1

    mul-double/2addr v2, v4

    iget v1, v0, Landroid/util/DisplayMetrics;->density:F

    float-to-double v4, v1

    div-double/2addr v2, v4

    const-wide/high16 v4, 0x4074000000000000L    # 320.0

    div-double/2addr v2, v4

    const-wide/high16 v4, 0x4094000000000000L    # 1280.0

    mul-double/2addr v2, v4

    double-to-float v1, v2

    sput v1, Lcom/miui/home/recents/views/MyTaskStackView;->DEFAULT_ROTATE_CAMERA_DISTANCE:F

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    invoke-direct {p0, p1}, Lcom/miui/home/recents/views/TaskStackView;-><init>(Landroid/content/Context;)V

    const/high16 v0, -0x40000000    # -2.0f

    iput v0, p0, Lcom/miui/home/recents/views/MyTaskStackView;->mDefCameraDistance:F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/home/recents/views/MyTaskStackView;->firstStart:Z

    new-instance v0, Lcom/miui/home/recents/views/MyTaskStackView$1;

    invoke-direct {v0, p0}, Lcom/miui/home/recents/views/MyTaskStackView$1;-><init>(Lcom/miui/home/recents/views/MyTaskStackView;)V

    iput-object v0, p0, Lcom/miui/home/recents/views/MyTaskStackView;->mFakeRun:Ljava/lang/Runnable;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/miui/home/recents/views/MyTaskStackView;->isReload:Z

    sput-object p0, Lcom/miui/home/recents/views/MyTaskStackView;->instance:Lcom/miui/home/recents/views/MyTaskStackView;

    invoke-virtual {p0}, Lcom/miui/home/recents/views/MyTaskStackView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "recents_task_view_header_height"

    invoke-static {p1, v1}, Landroid/Utils/Utils;->DimenToID(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/miui/home/recents/views/MyTaskStackView;->mVerticalGap:I

    invoke-virtual {p0}, Lcom/miui/home/recents/views/MyTaskStackView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "recents_horizontal_layout_task_view_gap"

    invoke-static {p1, v1}, Landroid/Utils/Utils;->DimenToID(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/miui/home/recents/views/MyTaskStackView;->mHorizontalGap:I

    invoke-direct {p0}, Lcom/miui/home/recents/views/MyTaskStackView;->updateSettings()V

    return-void
.end method

.method static synthetic access$000(Lcom/miui/home/recents/views/MyTaskStackView;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/home/recents/views/MyTaskStackView;->fakeEvent()V

    return-void
.end method

.method private fakeEvent()V
    .locals 8

    iget-object v4, p0, Lcom/miui/home/recents/views/MyTaskStackView;->mFakeRun:Ljava/lang/Runnable;

    invoke-virtual {p0, v4}, Lcom/miui/home/recents/views/MyTaskStackView;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-boolean v4, p0, Lcom/miui/home/recents/views/MyTaskStackView;->isVertical:Z

    if-nez v4, :cond_1

    iget v4, p0, Lcom/miui/home/recents/views/MyTaskStackView;->mStyleAnim:I

    const/4 v5, 0x2

    if-eq v4, v5, :cond_0

    iget v4, p0, Lcom/miui/home/recents/views/MyTaskStackView;->mStyleAnim:I

    const/4 v5, 0x3

    if-ne v4, v5, :cond_1

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/miui/home/recents/views/MyTaskStackView;->lastFakeEvent:J

    const-wide/16 v6, 0x3e8

    add-long/2addr v4, v6

    cmp-long v4, v2, v4

    if-gez v4, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    iput-wide v2, p0, Lcom/miui/home/recents/views/MyTaskStackView;->lastFakeEvent:J

    const-string v4, "mStackScroller"

    const/4 v5, 0x1

    invoke-static {p0, v4, v5}, Landroid/Utils/ReflectionUtil;->getFieldFromSuperClass(Ljava/lang/Object;Ljava/lang/String;I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/home/recents/views/TaskStackViewScroller;

    invoke-virtual {v1}, Lcom/miui/home/recents/views/TaskStackViewScroller;->getStackScroll()F

    move-result v0

    const v4, 0x3d4ccccd    # 0.05f

    sub-float v4, v0, v4

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5}, Lcom/miui/home/recents/views/TaskStackViewScroller;->animateScroll(FLjava/lang/Runnable;)V

    goto :goto_0
.end method

.method public static getInstance()Lcom/miui/home/recents/views/MyTaskStackView;
    .locals 1

    sget-object v0, Lcom/miui/home/recents/views/MyTaskStackView;->instance:Lcom/miui/home/recents/views/MyTaskStackView;

    return-object v0
.end method

.method private layoutTaskView(ZLcom/miui/home/recents/views/TaskView;)V
    .locals 5

    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "layoutTaskView"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    sget-object v4, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-class v4, Lcom/miui/home/recents/views/TaskView;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p2, v1, v2

    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private resetTransform()V
    .locals 7

    const/4 v6, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/miui/home/recents/views/MyTaskStackView;->getTaskViews()Ljava/util/List;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iget-object v3, p0, Lcom/miui/home/recents/views/MyTaskStackView;->mViewPool:Lcom/miui/home/recents/views/ViewPool;

    invoke-virtual {v3}, Lcom/miui/home/recents/views/ViewPool;->getViews()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/home/recents/views/TaskView;

    invoke-virtual {v2, v6}, Lcom/miui/home/recents/views/TaskView;->setRotationY(F)V

    invoke-virtual {v2, v6}, Lcom/miui/home/recents/views/TaskView;->setRotation(F)V

    invoke-virtual {v2, v6}, Lcom/miui/home/recents/views/TaskView;->setRotationX(F)V

    invoke-virtual {v2}, Lcom/miui/home/recents/views/TaskView;->resetPivot()V

    invoke-virtual {v2, v5}, Lcom/miui/home/recents/views/TaskView;->setScaleX(F)V

    invoke-virtual {v2, v5}, Lcom/miui/home/recents/views/TaskView;->setScaleY(F)V

    invoke-virtual {v2, v5}, Lcom/miui/home/recents/views/TaskView;->setAlpha(F)V

    goto :goto_0

    :cond_0
    iget-object v3, p0, Lcom/miui/home/recents/views/MyTaskStackView;->mCurrentTaskTransforms:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/home/recents/views/TaskViewTransform;

    const/4 v4, 0x1

    iput-boolean v4, v1, Lcom/miui/home/recents/views/TaskViewTransform;->visible:Z

    iput v5, v1, Lcom/miui/home/recents/views/TaskViewTransform;->alpha:F

    iput v5, v1, Lcom/miui/home/recents/views/TaskViewTransform;->scale:F

    iput v6, v1, Lcom/miui/home/recents/views/TaskViewTransform;->translationZ:F

    goto :goto_1

    :cond_1
    return-void
.end method

.method private updateSettings()V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/miui/home/recents/views/MyTaskStackView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/miui/home/launcher/RecentsAndFSGestureUtils;->getTaskStackViewLayoutStyle(Landroid/content/Context;)I

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/miui/home/recents/views/MyTaskStackView;->isVertical:Z

    invoke-virtual {p0}, Lcom/miui/home/recents/views/MyTaskStackView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    iget-boolean v0, p0, Lcom/miui/home/recents/views/MyTaskStackView;->isVertical:Z

    if-nez v0, :cond_2

    const-string v0, "recent_horizontal_style"

    :goto_1
    invoke-static {v3, v0, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/miui/home/recents/views/MyTaskStackView;->mStyleAnim:I

    iget v0, p0, Lcom/miui/home/recents/views/MyTaskStackView;->mStyleAnim:I

    const/4 v3, 0x2

    if-ne v0, v3, :cond_3

    move v0, v1

    :goto_2
    sput-boolean v0, Lcom/miui/home/recents/views/MyTaskStackViewsAlgorithmHorizontal;->isIosLeft:Z

    iget v0, p0, Lcom/miui/home/recents/views/MyTaskStackView;->mStyleAnim:I

    const/4 v3, 0x3

    if-ne v0, v3, :cond_4

    move v0, v1

    :goto_3
    sput-boolean v0, Lcom/miui/home/recents/views/MyTaskStackViewsAlgorithmHorizontal;->isIosRight:Z

    iget-boolean v0, p0, Lcom/miui/home/recents/views/MyTaskStackView;->firstStart:Z

    if-nez v0, :cond_0

    const-string v0, "updateLayoutStyle"

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {p0, v0, v1, v3}, Landroid/Utils/ReflectionUtil;->invokeInSuperClass(Ljava/lang/Object;Ljava/lang/String;I[Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/miui/home/recents/views/MyTaskStackView;->requestLayout()V

    iput-boolean v2, p0, Lcom/miui/home/recents/views/MyTaskStackView;->firstStart:Z

    :cond_0
    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    const-string v0, "recent_vertical_style"

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_2

    :cond_4
    move v0, v2

    goto :goto_3
.end method

.method private updateTransformation(Lcom/miui/home/recents/views/TaskView;Lcom/miui/home/recents/views/TaskViewTransform;)V
    .locals 3

    const/4 v2, 0x1

    iget v0, p0, Lcom/miui/home/recents/views/MyTaskStackView;->mStyleAnim:I

    iget-boolean v1, p0, Lcom/miui/home/recents/views/MyTaskStackView;->isNeedReset:Z

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/miui/home/recents/views/MyTaskStackView;->resetTransform()V

    :goto_0
    return-void

    :cond_0
    iget-boolean v1, p0, Lcom/miui/home/recents/views/MyTaskStackView;->isVertical:Z

    if-nez v1, :cond_5

    if-ne v0, v2, :cond_1

    invoke-direct {p0, p1, p2}, Lcom/miui/home/recents/views/MyTaskStackView;->updateTransformationHorizontalCub(Lcom/miui/home/recents/views/TaskView;Lcom/miui/home/recents/views/TaskViewTransform;)V

    goto :goto_0

    :cond_1
    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    invoke-direct {p0, p1, p2}, Lcom/miui/home/recents/views/MyTaskStackView;->updateTransformationOldSchool(Lcom/miui/home/recents/views/TaskView;Lcom/miui/home/recents/views/TaskViewTransform;)V

    goto :goto_0

    :cond_2
    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-ne v0, v1, :cond_4

    :cond_3
    invoke-direct {p0, p1, p2}, Lcom/miui/home/recents/views/MyTaskStackView;->updateTransformationIos(Lcom/miui/home/recents/views/TaskView;Lcom/miui/home/recents/views/TaskViewTransform;)V

    goto :goto_0

    :cond_4
    invoke-direct {p0, p1, p2}, Lcom/miui/home/recents/views/MyTaskStackView;->updateTransformationVertical(Lcom/miui/home/recents/views/TaskView;Lcom/miui/home/recents/views/TaskViewTransform;)V

    goto :goto_0

    :cond_5
    if-ne v0, v2, :cond_6

    invoke-direct {p0, p1, p2}, Lcom/miui/home/recents/views/MyTaskStackView;->updateTransformationVerticalCub(Lcom/miui/home/recents/views/TaskView;Lcom/miui/home/recents/views/TaskViewTransform;)V

    goto :goto_0

    :cond_6
    invoke-direct {p0, p1, p2}, Lcom/miui/home/recents/views/MyTaskStackView;->updateTransformationHorizontal(Lcom/miui/home/recents/views/TaskView;Lcom/miui/home/recents/views/TaskViewTransform;)V

    goto :goto_0
.end method

.method private updateTransformationHorizontal(Lcom/miui/home/recents/views/TaskView;Lcom/miui/home/recents/views/TaskViewTransform;)V
    .locals 8

    const/4 v7, 0x2

    const/high16 v6, 0x40000000    # 2.0f

    const/4 v5, 0x0

    iget-object v3, p2, Lcom/miui/home/recents/views/TaskViewTransform;->rect:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v3

    iget v4, p0, Lcom/miui/home/recents/views/MyTaskStackView;->mHorizontalGap:I

    int-to-float v4, v4

    div-float/2addr v4, v6

    add-float v0, v3, v4

    invoke-virtual {p1}, Lcom/miui/home/recents/views/TaskView;->getLayerType()I

    move-result v3

    if-eq v3, v7, :cond_0

    const/4 v3, 0x0

    check-cast v3, Landroid/graphics/Paint;

    invoke-virtual {p1, v7, v3}, Lcom/miui/home/recents/views/TaskView;->setLayerType(ILandroid/graphics/Paint;)V

    :cond_0
    invoke-virtual {p1}, Lcom/miui/home/recents/views/TaskView;->getMeasuredHeight()I

    move-result v3

    int-to-float v3, v3

    div-float v1, v3, v6

    invoke-virtual {p1, v1}, Lcom/miui/home/recents/views/TaskView;->setPivotY(F)V

    invoke-virtual {p1, v0}, Lcom/miui/home/recents/views/TaskView;->setPivotX(F)V

    invoke-virtual {p1, v5}, Lcom/miui/home/recents/views/TaskView;->setRotation(F)V

    invoke-virtual {p1, v5}, Lcom/miui/home/recents/views/TaskView;->setRotationX(F)V

    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Lcom/miui/home/recents/views/TaskView;->setRotationY(F)V

    sget v3, Lcom/miui/home/recents/views/MyTaskStackView;->DEFAULT_ROTATE_CAMERA_DISTANCE:F

    invoke-virtual {p1, v3}, Lcom/miui/home/recents/views/TaskView;->setCameraDistance(F)V

    return-void
.end method

.method private updateTransformationHorizontalCub(Lcom/miui/home/recents/views/TaskView;Lcom/miui/home/recents/views/TaskViewTransform;)V
    .locals 11

    const/4 v10, 0x2

    const/high16 v9, 0x42b40000    # 90.0f

    const/4 v8, 0x0

    const/high16 v7, 0x40000000    # 2.0f

    iget-object v5, p2, Lcom/miui/home/recents/views/TaskViewTransform;->rect:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->width()F

    move-result v5

    iget v6, p0, Lcom/miui/home/recents/views/MyTaskStackView;->mHorizontalGap:I

    int-to-float v6, v6

    div-float/2addr v6, v7

    add-float v1, v5, v6

    invoke-virtual {p1}, Lcom/miui/home/recents/views/TaskView;->getScrollX()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {p0}, Lcom/miui/home/recents/views/MyTaskStackView;->getMeasuredWidth()I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v6, v7

    add-float/2addr v5, v6

    invoke-virtual {p1}, Lcom/miui/home/recents/views/TaskView;->getLeft()I

    move-result v6

    int-to-float v6, v6

    sub-float v4, v5, v6

    div-float v5, v1, v7

    sub-float v5, v4, v5

    div-float v0, v5, v1

    invoke-virtual {p1}, Lcom/miui/home/recents/views/TaskView;->getLayerType()I

    move-result v5

    if-eq v5, v10, :cond_0

    const/4 v5, 0x0

    check-cast v5, Landroid/graphics/Paint;

    invoke-virtual {p1, v10, v5}, Lcom/miui/home/recents/views/TaskView;->setLayerType(ILandroid/graphics/Paint;)V

    :cond_0
    invoke-virtual {p1}, Lcom/miui/home/recents/views/TaskView;->getMeasuredHeight()I

    move-result v5

    int-to-float v5, v5

    div-float v2, v5, v7

    cmpg-float v5, v0, v8

    if-gez v5, :cond_1

    const/4 v1, 0x0

    :cond_1
    invoke-virtual {p1, v2}, Lcom/miui/home/recents/views/TaskView;->setPivotY(F)V

    invoke-virtual {p1, v1}, Lcom/miui/home/recents/views/TaskView;->setPivotX(F)V

    invoke-virtual {p1, v8}, Lcom/miui/home/recents/views/TaskView;->setRotation(F)V

    invoke-virtual {p1, v8}, Lcom/miui/home/recents/views/TaskView;->setRotationX(F)V

    mul-float v3, v0, v9

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v5

    cmpl-float v5, v5, v9

    if-lez v5, :cond_2

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v5

    div-float/2addr v5, v3

    mul-float v3, v5, v9

    :cond_2
    neg-float v5, v3

    invoke-virtual {p1, v5}, Lcom/miui/home/recents/views/TaskView;->setRotationY(F)V

    sget v5, Lcom/miui/home/recents/views/MyTaskStackView;->DEFAULT_ROTATE_CAMERA_DISTANCE:F

    invoke-virtual {p1, v5}, Lcom/miui/home/recents/views/TaskView;->setCameraDistance(F)V

    return-void
.end method

.method private updateTransformationIos(Lcom/miui/home/recents/views/TaskView;Lcom/miui/home/recents/views/TaskViewTransform;)V
    .locals 2

    iget v0, p2, Lcom/miui/home/recents/views/TaskViewTransform;->scale:F

    invoke-virtual {p1, v0}, Lcom/miui/home/recents/views/TaskView;->setScaleX(F)V

    invoke-virtual {p1, v0}, Lcom/miui/home/recents/views/TaskView;->setScaleY(F)V

    iget v1, p2, Lcom/miui/home/recents/views/TaskViewTransform;->alpha:F

    invoke-virtual {p1, v1}, Lcom/miui/home/recents/views/TaskView;->setAlpha(F)V

    iget v1, p2, Lcom/miui/home/recents/views/TaskViewTransform;->translationZ:F

    invoke-virtual {p1, v1}, Lcom/miui/home/recents/views/TaskView;->setTranslationZ(F)V

    return-void
.end method

.method private updateTransformationOldSchool(Lcom/miui/home/recents/views/TaskView;Lcom/miui/home/recents/views/TaskViewTransform;)V
    .locals 10

    const/4 v9, 0x2

    const/high16 v8, 0x42700000    # 60.0f

    const/4 v7, 0x0

    const/high16 v6, 0x40000000    # 2.0f

    iget-object v4, p2, Lcom/miui/home/recents/views/TaskViewTransform;->rect:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v4

    iget v5, p0, Lcom/miui/home/recents/views/MyTaskStackView;->mHorizontalGap:I

    int-to-float v5, v5

    div-float/2addr v5, v6

    add-float v1, v4, v5

    invoke-virtual {p1}, Lcom/miui/home/recents/views/TaskView;->getScrollX()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {p0}, Lcom/miui/home/recents/views/MyTaskStackView;->getMeasuredWidth()I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v5, v6

    add-float/2addr v4, v5

    invoke-virtual {p1}, Lcom/miui/home/recents/views/TaskView;->getLeft()I

    move-result v5

    int-to-float v5, v5

    sub-float v3, v4, v5

    div-float v4, v1, v6

    sub-float v4, v3, v4

    div-float v0, v4, v1

    invoke-virtual {p1}, Lcom/miui/home/recents/views/TaskView;->getLayerType()I

    move-result v4

    if-eq v4, v9, :cond_0

    const/4 v4, 0x0

    check-cast v4, Landroid/graphics/Paint;

    invoke-virtual {p1, v9, v4}, Lcom/miui/home/recents/views/TaskView;->setLayerType(ILandroid/graphics/Paint;)V

    :cond_0
    invoke-virtual {p1}, Lcom/miui/home/recents/views/TaskView;->getMeasuredHeight()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v4, v6

    invoke-virtual {p1, v4}, Lcom/miui/home/recents/views/TaskView;->setPivotY(F)V

    div-float v4, v1, v6

    invoke-virtual {p1, v4}, Lcom/miui/home/recents/views/TaskView;->setPivotX(F)V

    invoke-virtual {p1, v7}, Lcom/miui/home/recents/views/TaskView;->setRotation(F)V

    invoke-virtual {p1, v7}, Lcom/miui/home/recents/views/TaskView;->setRotationX(F)V

    mul-float v2, v0, v8

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v4

    const/high16 v5, 0x42b40000    # 90.0f

    cmpl-float v4, v4, v5

    if-lez v4, :cond_1

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v4

    div-float/2addr v4, v2

    mul-float v2, v4, v8

    :cond_1
    invoke-virtual {p1, v2}, Lcom/miui/home/recents/views/TaskView;->setRotationY(F)V

    sget v4, Lcom/miui/home/recents/views/MyTaskStackView;->DEFAULT_ROTATE_CAMERA_DISTANCE:F

    invoke-virtual {p1, v4}, Lcom/miui/home/recents/views/TaskView;->setCameraDistance(F)V

    return-void
.end method

.method private updateTransformationVertical(Lcom/miui/home/recents/views/TaskView;Lcom/miui/home/recents/views/TaskViewTransform;)V
    .locals 10

    const/4 v9, 0x2

    const/4 v8, 0x0

    const/high16 v7, 0x40000000    # 2.0f

    iget-object v5, p2, Lcom/miui/home/recents/views/TaskViewTransform;->rect:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v5

    iget v6, p0, Lcom/miui/home/recents/views/MyTaskStackView;->mVerticalGap:I

    int-to-float v6, v6

    add-float v1, v5, v6

    invoke-virtual {p1}, Lcom/miui/home/recents/views/TaskView;->getScrollY()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {p0}, Lcom/miui/home/recents/views/MyTaskStackView;->getMeasuredHeight()I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v6, v7

    add-float/2addr v5, v6

    invoke-virtual {p1}, Lcom/miui/home/recents/views/TaskView;->getTop()I

    move-result v6

    int-to-float v6, v6

    sub-float v4, v5, v6

    div-float v5, v1, v7

    sub-float v5, v4, v5

    div-float v0, v5, v1

    invoke-virtual {p1}, Lcom/miui/home/recents/views/TaskView;->getLayerType()I

    move-result v5

    if-eq v5, v9, :cond_0

    const/4 v5, 0x0

    check-cast v5, Landroid/graphics/Paint;

    invoke-virtual {p1, v9, v5}, Lcom/miui/home/recents/views/TaskView;->setLayerType(ILandroid/graphics/Paint;)V

    :cond_0
    invoke-virtual {p1}, Lcom/miui/home/recents/views/TaskView;->getMeasuredWidth()I

    move-result v5

    int-to-float v5, v5

    div-float v2, v5, v7

    invoke-virtual {p1, v2}, Lcom/miui/home/recents/views/TaskView;->setPivotX(F)V

    invoke-virtual {p1, v1}, Lcom/miui/home/recents/views/TaskView;->setPivotY(F)V

    invoke-virtual {p1, v8}, Lcom/miui/home/recents/views/TaskView;->setRotation(F)V

    invoke-virtual {p1, v8}, Lcom/miui/home/recents/views/TaskView;->setRotationY(F)V

    const/4 v3, 0x0

    invoke-virtual {p1, v3}, Lcom/miui/home/recents/views/TaskView;->setRotationX(F)V

    sget v5, Lcom/miui/home/recents/views/MyTaskStackView;->DEFAULT_ROTATE_CAMERA_DISTANCE:F

    invoke-virtual {p1, v5}, Lcom/miui/home/recents/views/TaskView;->setCameraDistance(F)V

    return-void
.end method

.method private updateTransformationVerticalCub(Lcom/miui/home/recents/views/TaskView;Lcom/miui/home/recents/views/TaskViewTransform;)V
    .locals 11

    const/4 v10, 0x2

    const/high16 v9, 0x42b40000    # 90.0f

    const/high16 v8, 0x40000000    # 2.0f

    const/4 v7, 0x0

    iget-object v5, p2, Lcom/miui/home/recents/views/TaskViewTransform;->rect:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v5

    iget v6, p0, Lcom/miui/home/recents/views/MyTaskStackView;->mVerticalGap:I

    int-to-float v6, v6

    add-float v1, v5, v6

    invoke-virtual {p1}, Lcom/miui/home/recents/views/TaskView;->getScrollY()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {p0}, Lcom/miui/home/recents/views/MyTaskStackView;->getMeasuredHeight()I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v6, v8

    add-float/2addr v5, v6

    invoke-virtual {p1}, Lcom/miui/home/recents/views/TaskView;->getTop()I

    move-result v6

    int-to-float v6, v6

    sub-float v4, v5, v6

    div-float v5, v1, v8

    sub-float v5, v4, v5

    div-float v0, v5, v1

    invoke-virtual {p1}, Lcom/miui/home/recents/views/TaskView;->getLayerType()I

    move-result v5

    if-eq v5, v10, :cond_0

    const/4 v5, 0x0

    check-cast v5, Landroid/graphics/Paint;

    invoke-virtual {p1, v10, v5}, Lcom/miui/home/recents/views/TaskView;->setLayerType(ILandroid/graphics/Paint;)V

    :cond_0
    invoke-virtual {p1}, Lcom/miui/home/recents/views/TaskView;->getMeasuredWidth()I

    move-result v5

    int-to-float v5, v5

    div-float v2, v5, v8

    cmpg-float v5, v0, v7

    if-gez v5, :cond_1

    const/4 v1, 0x0

    :cond_1
    invoke-virtual {p1, v2}, Lcom/miui/home/recents/views/TaskView;->setPivotX(F)V

    invoke-virtual {p1, v1}, Lcom/miui/home/recents/views/TaskView;->setPivotY(F)V

    invoke-virtual {p1, v7}, Lcom/miui/home/recents/views/TaskView;->setRotation(F)V

    invoke-virtual {p1, v7}, Lcom/miui/home/recents/views/TaskView;->setRotationY(F)V

    mul-float v3, v0, v9

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v5

    cmpl-float v5, v5, v9

    if-lez v5, :cond_2

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v5

    div-float/2addr v5, v3

    mul-float v3, v5, v9

    :cond_2
    invoke-virtual {p1, v3}, Lcom/miui/home/recents/views/TaskView;->setRotationX(F)V

    sget v5, Lcom/miui/home/recents/views/MyTaskStackView;->DEFAULT_ROTATE_CAMERA_DISTANCE:F

    invoke-virtual {p1, v5}, Lcom/miui/home/recents/views/TaskView;->setCameraDistance(F)V

    return-void
.end method


# virtual methods
.method public fakeEventAfterReload()V
    .locals 0

    return-void
.end method

.method public fakeEventWithDelay()V
    .locals 4

    iget-object v0, p0, Lcom/miui/home/recents/views/MyTaskStackView;->mFakeRun:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/miui/home/recents/views/MyTaskStackView;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lcom/miui/home/recents/views/MyTaskStackView;->mFakeRun:Ljava/lang/Runnable;

    const-wide/16 v2, 0xfa

    invoke-virtual {p0, v0, v2, v3}, Lcom/miui/home/recents/views/MyTaskStackView;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 2

    invoke-super {p0}, Lcom/miui/home/recents/views/TaskStackView;->onAttachedToWindow()V

    invoke-static {}, Landroid/preference/CustomUpdater;->getInstance()Landroid/preference/CustomUpdater;

    move-result-object v0

    const-string v1, "recent_panel_animation"

    invoke-virtual {v0, p0, v1}, Landroid/preference/CustomUpdater;->addCustomReceiver(Landroid/preference/CustomUpdater$CustomReceiver;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/miui/home/recents/views/MyTaskStackView;->updateSettings()V

    invoke-virtual {p0}, Lcom/miui/home/recents/views/MyTaskStackView;->registerEventBus()V

    return-void
.end method

.method public onCustomChanged(Ljava/lang/String;)V
    .locals 1

    const-string v0, "recent_panel_animation"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/miui/home/recents/views/MyTaskStackView;->resetTransform()V

    invoke-direct {p0}, Lcom/miui/home/recents/views/MyTaskStackView;->updateSettings()V

    :cond_0
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    invoke-super {p0}, Lcom/miui/home/recents/views/TaskStackView;->onDetachedFromWindow()V

    invoke-static {}, Landroid/preference/CustomUpdater;->getInstance()Landroid/preference/CustomUpdater;

    move-result-object v0

    const-string v1, "recent_panel_animation"

    invoke-virtual {v0, p0, v1}, Landroid/preference/CustomUpdater;->removeCustomReceiver(Landroid/preference/CustomUpdater$CustomReceiver;Ljava/lang/String;)V

    return-void
.end method

.method public onMessageEvent(Lcom/miui/home/recents/messages/FsGestureEnterRecentsCompleteEvent;)V
    .locals 0

    invoke-virtual {p0}, Lcom/miui/home/recents/views/MyTaskStackView;->fakeEventWithDelay()V

    return-void
.end method

.method public onMessageEvent(Lcom/miui/home/recents/messages/TaskViewDismissedEvent;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/miui/home/recents/views/TaskStackView;->onMessageEvent(Lcom/miui/home/recents/messages/TaskViewDismissedEvent;)V

    invoke-direct {p0}, Lcom/miui/home/recents/views/MyTaskStackView;->fakeEvent()V

    return-void
.end method

.method public registerEventBus()V
    .locals 1

    invoke-static {}, Lcom/miui/home/library/utils/AsyncTaskExecutorHelper;->getEventBus()Lorg/greenrobot/eventbus/EventBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lorg/greenrobot/eventbus/EventBus;->isRegistered(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/miui/home/library/utils/AsyncTaskExecutorHelper;->getEventBus()Lorg/greenrobot/eventbus/EventBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lorg/greenrobot/eventbus/EventBus;->register(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public setIsShowingMenu(Z)V
    .locals 0

    invoke-super {p0, p1}, Lcom/miui/home/recents/views/TaskStackView;->setIsShowingMenu(Z)V

    if-nez p1, :cond_0

    invoke-direct {p0}, Lcom/miui/home/recents/views/MyTaskStackView;->fakeEvent()V

    :cond_0
    return-void
.end method

.method public updateTaskViewToTransform(Lcom/miui/home/recents/views/TaskView;Lcom/miui/home/recents/views/TaskViewTransform;Lcom/android/systemui/shared/recents/utilities/AnimationProps;)V
    .locals 2

    invoke-virtual {p1}, Lcom/miui/home/recents/views/TaskView;->cancelTransformAnimation()V

    invoke-virtual {p1, p2, p3}, Lcom/miui/home/recents/views/TaskView;->updateViewPropertiesToTaskTransform(Lcom/miui/home/recents/views/TaskViewTransform;Lcom/android/systemui/shared/recents/utilities/AnimationProps;)V

    iget v0, p0, Lcom/miui/home/recents/views/MyTaskStackView;->mDefCameraDistance:F

    const/high16 v1, -0x40000000    # -2.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/miui/home/recents/views/TaskView;->getCameraDistance()F

    move-result v0

    iput v0, p0, Lcom/miui/home/recents/views/MyTaskStackView;->mDefCameraDistance:F

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/miui/home/recents/views/MyTaskStackView;->updateTransformation(Lcom/miui/home/recents/views/TaskView;Lcom/miui/home/recents/views/TaskViewTransform;)V

    return-void
.end method

.method public windowMesage(I)V
    .locals 2

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    const/4 v1, 0x2

    if-ne p1, v1, :cond_1

    :cond_0
    :goto_0
    iput-boolean v0, p0, Lcom/miui/home/recents/views/MyTaskStackView;->isNeedReset:Z

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
