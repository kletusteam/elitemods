.class public final Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$loadItemIcons$1;
.super Lmiuix/animation/listener/TransitionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->loadItemIcons(Lcom/miui/home/launcher/FolderInfo;Lcom/miui/home/launcher/IconCache;ZLcom/miui/home/library/utils/AsyncTaskExecutorHelper$SerialExecutor;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic $iconCache:Lcom/miui/home/launcher/IconCache;

.field final synthetic $info:Lcom/miui/home/launcher/FolderInfo;

.field final synthetic $serialExecutor:Lcom/miui/home/library/utils/AsyncTaskExecutorHelper$SerialExecutor;

.field final synthetic $usePredict:Z

.field final synthetic $whitAnim:Z

.field final synthetic this$0:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;


# direct methods
.method constructor <init>(Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;Lcom/miui/home/launcher/FolderInfo;Lcom/miui/home/launcher/IconCache;ZLcom/miui/home/library/utils/AsyncTaskExecutorHelper$SerialExecutor;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/miui/home/launcher/FolderInfo;",
            "Lcom/miui/home/launcher/IconCache;",
            "Z",
            "Lcom/miui/home/library/utils/AsyncTaskExecutorHelper$SerialExecutor;",
            "Z)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$loadItemIcons$1;->this$0:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    iput-object p2, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$loadItemIcons$1;->$info:Lcom/miui/home/launcher/FolderInfo;

    iput-object p3, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$loadItemIcons$1;->$iconCache:Lcom/miui/home/launcher/IconCache;

    iput-boolean p4, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$loadItemIcons$1;->$usePredict:Z

    iput-object p5, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$loadItemIcons$1;->$serialExecutor:Lcom/miui/home/library/utils/AsyncTaskExecutorHelper$SerialExecutor;

    iput-boolean p6, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$loadItemIcons$1;->$whitAnim:Z

    invoke-direct {p0}, Lmiuix/animation/listener/TransitionListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancel(Ljava/lang/Object;)V
    .locals 6

    invoke-super {p0, p1}, Lmiuix/animation/listener/TransitionListener;->onCancel(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$loadItemIcons$1;->this$0:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    iget-object v1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$loadItemIcons$1;->$info:Lcom/miui/home/launcher/FolderInfo;

    iget-object v2, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$loadItemIcons$1;->$iconCache:Lcom/miui/home/launcher/IconCache;

    iget-boolean v3, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$loadItemIcons$1;->$usePredict:Z

    iget-object v4, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$loadItemIcons$1;->$serialExecutor:Lcom/miui/home/library/utils/AsyncTaskExecutorHelper$SerialExecutor;

    iget-boolean v5, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$loadItemIcons$1;->$whitAnim:Z

    invoke-static/range {v0 .. v5}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->access$runLoadAction(Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;Lcom/miui/home/launcher/FolderInfo;Lcom/miui/home/launcher/IconCache;ZLcom/miui/home/library/utils/AsyncTaskExecutorHelper$SerialExecutor;Z)V

    return-void
.end method

.method public onComplete(Ljava/lang/Object;)V
    .locals 6

    invoke-super {p0, p1}, Lmiuix/animation/listener/TransitionListener;->onComplete(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$loadItemIcons$1;->this$0:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    iget-object v1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$loadItemIcons$1;->$info:Lcom/miui/home/launcher/FolderInfo;

    iget-object v2, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$loadItemIcons$1;->$iconCache:Lcom/miui/home/launcher/IconCache;

    iget-boolean v3, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$loadItemIcons$1;->$usePredict:Z

    iget-object v4, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$loadItemIcons$1;->$serialExecutor:Lcom/miui/home/library/utils/AsyncTaskExecutorHelper$SerialExecutor;

    iget-boolean v5, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$loadItemIcons$1;->$whitAnim:Z

    invoke-static/range {v0 .. v5}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->access$runLoadAction(Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;Lcom/miui/home/launcher/FolderInfo;Lcom/miui/home/launcher/IconCache;ZLcom/miui/home/library/utils/AsyncTaskExecutorHelper$SerialExecutor;Z)V

    return-void
.end method

.method public onUpdate(Ljava/lang/Object;Ljava/util/Collection;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/util/Collection<",
            "Lmiuix/animation/listener/UpdateInfo;",
            ">;)V"
        }
    .end annotation

    invoke-super {p0, p1, p2}, Lmiuix/animation/listener/TransitionListener;->onUpdate(Ljava/lang/Object;Ljava/util/Collection;)V

    return-void
.end method
