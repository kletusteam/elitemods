.class public Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;
.super Lcom/miui/home/smallwindow/SmallWindowStateHelper;


# instance fields
.field private mAllSmallWindowInfoSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mGetDisplayIdMethod:Ljava/lang/reflect/Method;

.field private mIsInSmallWindowState:Z

.field private mIsSupportMultipleSmallWindowFeature:Z

.field private mIsSupportMultipleSmallWindows:Z

.field private mIsSupportPin:Z

.field private mMiniSmallWindowInfoSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mNormalSmallWindowInfoSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mPipSmallWindowInfoSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/miui/home/smallwindow/SmallWindowStateHelper;-><init>()V

    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    iput-object v0, p0, Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;->mAllSmallWindowInfoSet:Ljava/util/Set;

    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    iput-object v0, p0, Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;->mMiniSmallWindowInfoSet:Ljava/util/Set;

    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    iput-object v0, p0, Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;->mNormalSmallWindowInfoSet:Ljava/util/Set;

    new-instance v0, Landroid/util/ArraySet;

    invoke-direct {v0}, Landroid/util/ArraySet;-><init>()V

    iput-object v0, p0, Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;->mPipSmallWindowInfoSet:Ljava/util/Set;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;->mIsSupportPin:Z

    iput-boolean v0, p0, Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;->mIsSupportMultipleSmallWindowFeature:Z

    iput-boolean v0, p0, Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;->mIsSupportMultipleSmallWindows:Z

    return-void
.end method

.method static synthetic access$000(Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;ZI)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;->updateState(ZI)V

    return-void
.end method

.method static synthetic access$100(Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;->mIsInSmallWindowState:Z

    return p0
.end method

.method static synthetic access$1000(Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;->addMiniSmallWindowInfoSet(Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;)V

    return-void
.end method

.method static synthetic access$102(Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;->mIsInSmallWindowState:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;->addPipSmallWindowInfoSet(Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;)V

    return-void
.end method

.method static synthetic access$1200(Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;->dispatchSmallWindowsStateEvent(I)V

    return-void
.end method

.method static synthetic access$200(Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;)I
    .locals 0

    invoke-direct {p0}, Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;->getDisplayId()I

    move-result p0

    return p0
.end method

.method static synthetic access$300(Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;)Z
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;->freeformCheck(Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;)Z

    move-result p0

    return p0
.end method

.method static synthetic access$400(Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;)Ljava/util/Set;
    .locals 0

    iget-object p0, p0, Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;->mAllSmallWindowInfoSet:Ljava/util/Set;

    return-object p0
.end method

.method static synthetic access$500(Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;)Ljava/util/Set;
    .locals 0

    iget-object p0, p0, Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;->mMiniSmallWindowInfoSet:Ljava/util/Set;

    return-object p0
.end method

.method static synthetic access$600(Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;)Ljava/util/Set;
    .locals 0

    iget-object p0, p0, Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;->mNormalSmallWindowInfoSet:Ljava/util/Set;

    return-object p0
.end method

.method static synthetic access$700(Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;)Ljava/util/Set;
    .locals 0

    iget-object p0, p0, Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;->mPipSmallWindowInfoSet:Ljava/util/Set;

    return-object p0
.end method

.method static synthetic access$800(Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;)Z
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;->isInPinMode(Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;)Z

    move-result p0

    return p0
.end method

.method static synthetic access$900(Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;->addNormalSmallWindowInfoSet(Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;)V

    return-void
.end method

.method private addMiniSmallWindowInfoSet(Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;)V
    .locals 3

    const-string v0, "SmallWindowStateHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "addMiniSmallWindowInfoSet: info = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p1, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;->mMiniSmallWindowInfoSet:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private addNormalSmallWindowInfoSet(Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;)V
    .locals 3

    const-string v0, "SmallWindowStateHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "addNormalSmallWindowInfoSet: info = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p1, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;->mNormalSmallWindowInfoSet:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private addPipSmallWindowInfoSet(Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;)V
    .locals 3

    const-string v0, "SmallWindowStateHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "addPipSmallWindowInfoSet: info = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p1, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;->mPipSmallWindowInfoSet:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private dispatchSmallWindowsStateEvent(I)V
    .locals 1

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    :cond_0
    invoke-static {}, Lcom/miui/home/library/utils/AsyncTaskExecutorHelper;->getEventBus()Lorg/greenrobot/eventbus/EventBus;

    move-result-object p1

    new-instance v0, Lcom/miui/home/smallwindow/messages/OpenSmallWindowsEvent;

    invoke-direct {v0}, Lcom/miui/home/smallwindow/messages/OpenSmallWindowsEvent;-><init>()V

    invoke-virtual {p1, v0}, Lorg/greenrobot/eventbus/EventBus;->post(Ljava/lang/Object;)V

    :cond_1
    return-void
.end method

.method private freeformCheck(Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;)Z
    .locals 1

    invoke-virtual {p1}, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->isInMiniFreeFormMode()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->isInFreeFormMode()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0, p1}, Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;->isInPinMode(Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method private getDisplayId()I
    .locals 4

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v1, 0x0

    const/16 v2, 0x1c

    if-gt v0, v2, :cond_0

    return v1

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;->mGetDisplayIdMethod:Ljava/lang/reflect/Method;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v2, "getDisplayId"

    new-array v3, v1, [Ljava/lang/Class;

    invoke-virtual {v0, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;->mGetDisplayIdMethod:Ljava/lang/reflect/Method;

    :cond_1
    iget-object v0, p0, Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;->mGetDisplayIdMethod:Ljava/lang/reflect/Method;

    iget-object v2, p0, Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;->mContext:Landroid/content/Context;

    new-array v3, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    :catch_0
    move-exception v0

    const-string v2, "SmallWindowStateHelper"

    const-string v3, "getDisplayId error"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    return v1
.end method

.method private isInPinMode(Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;->mIsSupportPin:Z

    if-eqz v0, :cond_0

    iget-boolean p1, p1, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->inPinMode:Z

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method private updateSmallWindowStateAndPackageName(I)V
    .locals 2

    iget-object v0, p0, Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;->mBackgroundExecutor:Lcom/miui/home/smallwindow/BackgroundExecutor;

    new-instance v1, Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager$2;

    invoke-direct {v1, p0, p1}, Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager$2;-><init>(Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;I)V

    invoke-virtual {v0, v1}, Lcom/miui/home/smallwindow/BackgroundExecutor;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    return-void
.end method

.method private updateState(ZI)V
    .locals 1

    invoke-direct {p0, p2}, Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;->updateSmallWindowStateAndPackageName(I)V

    iget-object p2, p0, Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;->mSmallWindowStateCallbacks:Ljava/util/Set;

    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/home/smallwindow/SmallWindowStateHelper$SmallWindowStateCallback;

    invoke-interface {v0, p1}, Lcom/miui/home/smallwindow/SmallWindowStateHelper$SmallWindowStateCallback;->onEnterOrExitSmallWindow(Z)V

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public addAllSmallWindowInfoSet(Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;)V
    .locals 1

    iget-object v0, p0, Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;->mAllSmallWindowInfoSet:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public canEnterMiniSmallWindow()Z
    .locals 4

    iget-boolean v0, p0, Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;->mIsSupportMultipleSmallWindowFeature:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;->mIsSupportMultipleSmallWindows:Z

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;->mMiniSmallWindowInfoSet:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;->mPipSmallWindowInfoSet:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    const/4 v3, 0x2

    if-ge v0, v3, :cond_0

    move v1, v2

    :cond_0
    return v1

    :cond_1
    iget-object v0, p0, Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;->mMiniSmallWindowInfoSet:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;->mPipSmallWindowInfoSet:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-ge v0, v2, :cond_2

    move v1, v2

    :cond_2
    return v1

    :cond_3
    iget-object v0, p0, Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;->mMiniSmallWindowInfoSet:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public canEnterSmallWindow(Ljava/lang/String;I)Z
    .locals 2

    iget-boolean v0, p0, Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;->mIsSupportMultipleSmallWindowFeature:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_3

    invoke-virtual {p0, p1, p2}, Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;->isPkgInSmallWindowMode(Ljava/lang/String;I)Z

    move-result p1

    iget-boolean p2, p0, Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;->mIsSupportMultipleSmallWindows:Z

    const/4 v0, 0x0

    if-eqz p2, :cond_1

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;->mAllSmallWindowInfoSet:Ljava/util/Set;

    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result p1

    const/4 p2, 0x2

    if-ge p1, p2, :cond_0

    move v0, v1

    :cond_0
    return v0

    :cond_1
    iget-object p1, p0, Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;->mAllSmallWindowInfoSet:Ljava/util/Set;

    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result p1

    if-ge p1, v1, :cond_2

    move v0, v1

    :cond_2
    return v0

    :cond_3
    invoke-virtual {p0}, Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;->isInSmallWindowMode()Z

    move-result p1

    xor-int/2addr p1, v1

    return p1
.end method

.method public getAllSmallWindows()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;->mAllSmallWindowInfoSet:Ljava/util/Set;

    return-object v0
.end method

.method public getNormalSmallWindows()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;->mNormalSmallWindowInfoSet:Ljava/util/Set;

    return-object v0
.end method

.method public init(Landroid/content/Context;ZZZ)V
    .locals 0

    invoke-super {p0, p1, p2, p3, p4}, Lcom/miui/home/smallwindow/SmallWindowStateHelper;->init(Landroid/content/Context;ZZZ)V

    new-instance p1, Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager$1;

    invoke-direct {p1, p0}, Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager$1;-><init>(Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;)V

    invoke-static {p1}, Lmiui/app/MiuiFreeFormManager;->registerFreeformCallback(Lmiui/app/IFreeformCallback;)V

    const/4 p1, -0x1

    invoke-direct {p0, p1}, Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;->updateSmallWindowStateAndPackageName(I)V

    iput-boolean p2, p0, Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;->mIsSupportMultipleSmallWindowFeature:Z

    iput-boolean p3, p0, Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;->mIsSupportMultipleSmallWindows:Z

    iput-boolean p4, p0, Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;->mIsSupportPin:Z

    return-void
.end method

.method public isInSmallWindowMode()Z
    .locals 1

    iget-boolean v0, p0, Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;->mIsInSmallWindowState:Z

    return v0
.end method

.method public isLastValidSmallWindowPackageName(Ljava/lang/String;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public isPkgInPipSmallWindowMode(Ljava/lang/String;I)Z
    .locals 3

    invoke-virtual {p0}, Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;->isInSmallWindowMode()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;->mPipSmallWindowInfoSet:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;

    iget-object v2, v1, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->packageName:Ljava/lang/String;

    invoke-static {v2, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget v1, v1, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->userId:I

    if-ne v1, p2, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_1
    const/4 p1, 0x0

    return p1
.end method

.method public isPkgInSmallWindowMode(Ljava/lang/String;I)Z
    .locals 3

    invoke-virtual {p0}, Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;->isInSmallWindowMode()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/miui/home/smallwindow/SmallWindowStateHelperUseManager;->mAllSmallWindowInfoSet:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;

    iget-object v2, v1, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->packageName:Ljava/lang/String;

    invoke-static {v2, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget v1, v1, Lmiui/app/MiuiFreeFormManager$MiuiFreeFormStackInfo;->userId:I

    if-ne v1, p2, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_1
    const/4 p1, 0x0

    return p1
.end method
