.class public final Lcom/miui/home/launcher/convertsize/FolderIconConvertSizeController;
.super Lcom/miui/home/launcher/convertsize/ItemIconConvertSizeController;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nFolderIconConvertSizeController.kt\nKotlin\n*S Kotlin\n*F\n+ 1 FolderIconConvertSizeController.kt\ncom/miui/home/launcher/convertsize/FolderIconConvertSizeController\n*L\n1#1,78:1\n*E\n"
.end annotation


# static fields
.field public static final INSTANCE:Lcom/miui/home/launcher/convertsize/FolderIconConvertSizeController;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/miui/home/launcher/convertsize/FolderIconConvertSizeController;

    invoke-direct {v0}, Lcom/miui/home/launcher/convertsize/FolderIconConvertSizeController;-><init>()V

    sput-object v0, Lcom/miui/home/launcher/convertsize/FolderIconConvertSizeController;->INSTANCE:Lcom/miui/home/launcher/convertsize/FolderIconConvertSizeController;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/miui/home/launcher/convertsize/ItemIconConvertSizeController;-><init>()V

    return-void
.end method

.method private final findCellLayoutPosition(Lcom/miui/home/launcher/CellLayout;Lcom/miui/home/launcher/ItemInfo;II)[I
    .locals 10

    iget v1, p2, Lcom/miui/home/launcher/ItemInfo;->cellX:I

    iget v2, p2, Lcom/miui/home/launcher/ItemInfo;->cellY:I

    move-object v0, p1

    move v3, p3

    move v4, p4

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/miui/home/launcher/CellLayout;->isCellOccupiedExceptItem(IIIILcom/miui/home/launcher/ItemInfo;)Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x1

    if-nez v0, :cond_0

    new-array p1, v2, [I

    iget p3, p2, Lcom/miui/home/launcher/ItemInfo;->cellX:I

    aput p3, p1, v1

    iget p2, p2, Lcom/miui/home/launcher/ItemInfo;->cellY:I

    aput p2, p1, v3

    return-object p1

    :cond_0
    iget v0, p2, Lcom/miui/home/launcher/ItemInfo;->cellX:I

    add-int/lit8 v5, v0, -0x1

    iget v6, p2, Lcom/miui/home/launcher/ItemInfo;->cellY:I

    move-object v4, p1

    move v7, p3

    move v8, p4

    move-object v9, p2

    invoke-virtual/range {v4 .. v9}, Lcom/miui/home/launcher/CellLayout;->isCellOccupiedExceptItem(IIIILcom/miui/home/launcher/ItemInfo;)Z

    move-result v0

    if-nez v0, :cond_1

    new-array p1, v2, [I

    iget p3, p2, Lcom/miui/home/launcher/ItemInfo;->cellX:I

    sub-int/2addr p3, v3

    aput p3, p1, v1

    iget p2, p2, Lcom/miui/home/launcher/ItemInfo;->cellY:I

    aput p2, p1, v3

    return-object p1

    :cond_1
    iget v0, p2, Lcom/miui/home/launcher/ItemInfo;->cellX:I

    add-int/lit8 v5, v0, -0x1

    iget v0, p2, Lcom/miui/home/launcher/ItemInfo;->cellY:I

    add-int/lit8 v6, v0, -0x1

    move-object v4, p1

    move v7, p3

    move v8, p4

    move-object v9, p2

    invoke-virtual/range {v4 .. v9}, Lcom/miui/home/launcher/CellLayout;->isCellOccupiedExceptItem(IIIILcom/miui/home/launcher/ItemInfo;)Z

    move-result v0

    if-nez v0, :cond_2

    new-array p1, v2, [I

    iget p3, p2, Lcom/miui/home/launcher/ItemInfo;->cellX:I

    sub-int/2addr p3, v3

    aput p3, p1, v1

    iget p2, p2, Lcom/miui/home/launcher/ItemInfo;->cellY:I

    sub-int/2addr p2, v3

    aput p2, p1, v3

    return-object p1

    :cond_2
    iget v5, p2, Lcom/miui/home/launcher/ItemInfo;->cellX:I

    iget v0, p2, Lcom/miui/home/launcher/ItemInfo;->cellY:I

    add-int/lit8 v6, v0, -0x1

    move-object v4, p1

    move v7, p3

    move v8, p4

    move-object v9, p2

    invoke-virtual/range {v4 .. v9}, Lcom/miui/home/launcher/CellLayout;->isCellOccupiedExceptItem(IIIILcom/miui/home/launcher/ItemInfo;)Z

    move-result v0

    if-nez v0, :cond_3

    new-array p1, v2, [I

    iget p3, p2, Lcom/miui/home/launcher/ItemInfo;->cellX:I

    aput p3, p1, v1

    iget p2, p2, Lcom/miui/home/launcher/ItemInfo;->cellY:I

    sub-int/2addr p2, v3

    aput p2, p1, v3

    return-object p1

    :cond_3
    invoke-virtual {p1, p3, p4}, Lcom/miui/home/launcher/CellLayout;->findFirstVacantArea(II)[I

    move-result-object p1

    return-object p1
.end method

.method private final insertFolderInNewScreen(Lcom/miui/home/launcher/FolderInfo;Lcom/miui/home/launcher/convertsize/ItemPositionInfo;)V
    .locals 4

    invoke-static {}, Lcom/miui/home/launcher/Application;->getLauncher()Lcom/miui/home/launcher/Launcher;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/miui/home/launcher/Launcher;->getWorkspace()Lcom/miui/home/launcher/Workspace;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    invoke-static {}, Lcom/miui/home/launcher/Application;->getLauncher()Lcom/miui/home/launcher/Launcher;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/miui/home/launcher/Workspace;->getScreenCount()I

    move-result v2

    new-instance v3, Lcom/miui/home/launcher/convertsize/FolderIconConvertSizeController$insertFolderInNewScreen$$inlined$let$lambda$1;

    invoke-direct {v3, v0, p2, p1}, Lcom/miui/home/launcher/convertsize/FolderIconConvertSizeController$insertFolderInNewScreen$$inlined$let$lambda$1;-><init>(Lcom/miui/home/launcher/Workspace;Lcom/miui/home/launcher/convertsize/ItemPositionInfo;Lcom/miui/home/launcher/FolderInfo;)V

    check-cast v3, Ljava/lang/Runnable;

    invoke-virtual {v1, v2, v3}, Lcom/miui/home/launcher/Launcher;->insertNewScreen(ILjava/lang/Runnable;)V

    :cond_1
    return-void
.end method


# virtual methods
.method public convertFolderSize(Lcom/miui/home/launcher/FolderInfo;I)V
    .locals 11

    const-string v0, "info"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, p2}, Lcom/miui/home/launcher/convertsize/FolderIconConvertSizeController;->getFolderSpanXFromType(I)I

    move-result v0

    invoke-virtual {p0, p2}, Lcom/miui/home/launcher/convertsize/FolderIconConvertSizeController;->getFolderSpanYFromType(I)I

    move-result v9

    new-instance v10, Lcom/miui/home/launcher/convertsize/ItemPositionInfo;

    iget v2, p1, Lcom/miui/home/launcher/FolderInfo;->cellX:I

    iget v3, p1, Lcom/miui/home/launcher/FolderInfo;->cellY:I

    iget-wide v6, p1, Lcom/miui/home/launcher/FolderInfo;->screenId:J

    move-object v1, v10

    move v4, v0

    move v5, v9

    move v8, p2

    invoke-direct/range {v1 .. v8}, Lcom/miui/home/launcher/convertsize/ItemPositionInfo;-><init>(IIIIJI)V

    iget p2, p1, Lcom/miui/home/launcher/FolderInfo;->spanX:I

    iget v1, p1, Lcom/miui/home/launcher/FolderInfo;->spanY:I

    invoke-virtual {p0, p2, v1, v0, v9}, Lcom/miui/home/launcher/convertsize/FolderIconConvertSizeController;->isIconBigger(IIII)Z

    move-result p2

    if-eqz p2, :cond_3

    invoke-static {}, Lcom/miui/home/launcher/Application;->getLauncher()Lcom/miui/home/launcher/Launcher;

    move-result-object p2

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/miui/home/launcher/Launcher;->getWorkspace()Lcom/miui/home/launcher/Workspace;

    move-result-object p2

    if-eqz p2, :cond_0

    iget-wide v1, p1, Lcom/miui/home/launcher/FolderInfo;->screenId:J

    invoke-virtual {p2, v1, v2}, Lcom/miui/home/launcher/Workspace;->getCellLayoutById(J)Lcom/miui/home/launcher/CellLayout;

    move-result-object p2

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    if-eqz p2, :cond_3

    sget-object v1, Lcom/miui/home/launcher/convertsize/FolderIconConvertSizeController;->INSTANCE:Lcom/miui/home/launcher/convertsize/FolderIconConvertSizeController;

    move-object v2, p1

    check-cast v2, Lcom/miui/home/launcher/ItemInfo;

    invoke-direct {v1, p2, v2, v0, v9}, Lcom/miui/home/launcher/convertsize/FolderIconConvertSizeController;->findCellLayoutPosition(Lcom/miui/home/launcher/CellLayout;Lcom/miui/home/launcher/ItemInfo;II)[I

    move-result-object p2

    if-nez p2, :cond_2

    sget-object p2, Lcom/miui/home/launcher/convertsize/FolderIconConvertSizeController;->INSTANCE:Lcom/miui/home/launcher/convertsize/FolderIconConvertSizeController;

    invoke-virtual {p2, v10}, Lcom/miui/home/launcher/convertsize/FolderIconConvertSizeController;->findAPositionOnLastScreen(Lcom/miui/home/launcher/convertsize/ItemPositionInfo;)Lcom/miui/home/launcher/convertsize/ItemPositionInfo;

    move-result-object p2

    if-eqz p2, :cond_1

    move-object v10, p2

    goto :goto_1

    :cond_1
    sget-object p2, Lcom/miui/home/launcher/convertsize/FolderIconConvertSizeController;->INSTANCE:Lcom/miui/home/launcher/convertsize/FolderIconConvertSizeController;

    invoke-direct {p2, p1, v10}, Lcom/miui/home/launcher/convertsize/FolderIconConvertSizeController;->insertFolderInNewScreen(Lcom/miui/home/launcher/FolderInfo;Lcom/miui/home/launcher/convertsize/ItemPositionInfo;)V

    return-void

    :cond_2
    const/4 v0, 0x0

    aget v0, p2, v0

    invoke-virtual {v10, v0}, Lcom/miui/home/launcher/convertsize/ItemPositionInfo;->setCellX(I)V

    const/4 v0, 0x1

    aget p2, p2, v0

    invoke-virtual {v10, p2}, Lcom/miui/home/launcher/convertsize/ItemPositionInfo;->setCellY(I)V

    :cond_3
    :goto_1
    invoke-static {}, Lcom/miui/home/launcher/Application;->getInstance()Lcom/miui/home/launcher/Application;

    move-result-object p2

    const-string v0, "Application.getInstance()"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/miui/home/launcher/Application;->getModel()Lcom/miui/home/launcher/LauncherModel;

    move-result-object p2

    if-eqz p2, :cond_4

    invoke-virtual {p2}, Lcom/miui/home/launcher/LauncherModel;->getCallback()Lcom/miui/home/launcher/LauncherModel$Callbacks;

    move-result-object p2

    if-eqz p2, :cond_4

    invoke-interface {p2, p1, v10}, Lcom/miui/home/launcher/LauncherModel$Callbacks;->bindFolderResize(Lcom/miui/home/launcher/FolderInfo;Lcom/miui/home/launcher/convertsize/ItemPositionInfo;)V

    :cond_4
    return-void
.end method

.method public final getFolderSpanXFromType(I)I
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x2

    if-eq p1, v1, :cond_1

    const/16 v2, 0x15

    if-eq p1, v2, :cond_0

    goto :goto_0

    :cond_0
    move v0, v1

    :cond_1
    :goto_0
    return v0
.end method

.method public final getFolderSpanYFromType(I)I
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x2

    if-eq p1, v1, :cond_1

    const/16 v2, 0x15

    if-eq p1, v2, :cond_0

    goto :goto_0

    :cond_0
    move v0, v1

    :cond_1
    :goto_0
    return v0
.end method
