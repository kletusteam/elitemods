.class public Lcom/miui/home/settings/preference/DesktopModePreference;
.super Landroidx/preference/Preference;

# interfaces
.implements Lmiuix/visual/check/VisualCheckGroup$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/home/settings/preference/DesktopModePreference$OnDesktopModeTemporaryChangeListener;
    }
.end annotation


# static fields
.field private static mSupportJeejen:Z


# instance fields
.field private isFirstBind:Z

.field private mClassicModeView:Lcom/miui/home/settings/FixedAspectRatioLottieAnimView;

.field private mDesktop:Lmiuix/visual/check/VisualCheckBox;

.field private mDrawer:Lmiuix/visual/check/VisualCheckBox;

.field private mDrawerAnimationName:Ljava/lang/String;

.field private mDrawerModeView:Lcom/miui/home/settings/FixedAspectRatioLottieAnimView;

.field private mEndIsDrawer:Z

.field private mJeejen:Lmiuix/visual/check/VisualCheckBox;

.field private mJeejenAnimationName:Ljava/lang/String;

.field private mJeejenModeView:Lcom/miui/home/settings/FixedAspectRatioLottieAnimView;

.field private mMode:I

.field private mOnDesktopModeTemporaryChangeListener:Lcom/miui/home/settings/preference/DesktopModePreference$OnDesktopModeTemporaryChangeListener;

.field private mStartIsDrawer:Z

.field private mVisualCheckGroup:Lmiuix/visual/check/VisualCheckGroup;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroidx/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->isFirstBind:Z

    const p1, 0x7f0d0064

    invoke-virtual {p0, p1}, Lcom/miui/home/settings/preference/DesktopModePreference;->setLayoutResource(I)V

    return-void
.end method

.method static synthetic access$000(Lcom/miui/home/settings/preference/DesktopModePreference;)Lmiuix/visual/check/VisualCheckBox;
    .locals 0

    iget-object p0, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->mDesktop:Lmiuix/visual/check/VisualCheckBox;

    return-object p0
.end method

.method static synthetic access$100()Z
    .locals 1

    sget-boolean v0, Lcom/miui/home/settings/preference/DesktopModePreference;->mSupportJeejen:Z

    return v0
.end method

.method static synthetic access$200(Lcom/miui/home/settings/preference/DesktopModePreference;)Lmiuix/visual/check/VisualCheckBox;
    .locals 0

    iget-object p0, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->mDrawer:Lmiuix/visual/check/VisualCheckBox;

    return-object p0
.end method

.method static synthetic access$300(Lcom/miui/home/settings/preference/DesktopModePreference;)Lmiuix/visual/check/VisualCheckBox;
    .locals 0

    iget-object p0, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->mJeejen:Lmiuix/visual/check/VisualCheckBox;

    return-object p0
.end method

.method private initAnimation()V
    .locals 2

    invoke-static {}, Lcom/miui/home/launcher/Application;->getInstance()Lcom/miui/home/launcher/Application;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/home/launcher/Application;->isInFoldLargeScreen()Z

    move-result v0

    if-nez v0, :cond_1

    sget-boolean v0, Lcom/miui/home/settings/preference/DesktopModePreference;->mSupportJeejen:Z

    if-eqz v0, :cond_0

    const-string v0, "drawer_mode_support_jeejen_animation.json"

    goto :goto_0

    :cond_0
    const-string v0, "drawer_mode_animation.json"

    :goto_0
    iput-object v0, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->mDrawerAnimationName:Ljava/lang/String;

    const-string v0, "jeejen_mode_animation.json"

    iput-object v0, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->mJeejenAnimationName:Ljava/lang/String;

    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->mJeejenModeView:Lcom/miui/home/settings/FixedAspectRatioLottieAnimView;

    iget-object v1, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->mJeejenAnimationName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/miui/home/settings/FixedAspectRatioLottieAnimView;->setAnimation(Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    invoke-static {}, Lcom/miui/home/launcher/DeviceConfig;->isScreenOrientationLandscape()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "drawer_mode_animation_fold_land.json"

    iput-object v0, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->mDrawerAnimationName:Ljava/lang/String;

    goto :goto_1

    :cond_2
    const-string v0, "drawer_mode_animation_fold_large.json"

    iput-object v0, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->mDrawerAnimationName:Ljava/lang/String;

    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->mDrawerModeView:Lcom/miui/home/settings/FixedAspectRatioLottieAnimView;

    iget-object v1, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->mDrawerAnimationName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/miui/home/settings/FixedAspectRatioLottieAnimView;->setAnimation(Ljava/lang/String;)V

    return-void
.end method

.method private setAnimation(I)V
    .locals 2

    iget-object v0, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->mDrawerModeView:Lcom/miui/home/settings/FixedAspectRatioLottieAnimView;

    invoke-virtual {v0}, Lcom/miui/home/settings/FixedAspectRatioLottieAnimView;->cancelAnimation()V

    iget-object v0, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->mDrawerModeView:Lcom/miui/home/settings/FixedAspectRatioLottieAnimView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/miui/home/settings/FixedAspectRatioLottieAnimView;->setProgress(F)V

    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->mJeejenModeView:Lcom/miui/home/settings/FixedAspectRatioLottieAnimView;

    invoke-virtual {v0}, Lcom/miui/home/settings/FixedAspectRatioLottieAnimView;->cancelAnimation()V

    iget-object v0, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->mJeejenModeView:Lcom/miui/home/settings/FixedAspectRatioLottieAnimView;

    invoke-virtual {v0, v1}, Lcom/miui/home/settings/FixedAspectRatioLottieAnimView;->setProgress(F)V

    :cond_0
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    sget-boolean p1, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-nez p1, :cond_1

    iget-object p1, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->mJeejenModeView:Lcom/miui/home/settings/FixedAspectRatioLottieAnimView;

    iget-object v0, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->mJeejenAnimationName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/miui/home/settings/FixedAspectRatioLottieAnimView;->setAnimation(Ljava/lang/String;)V

    iget-object p1, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->mJeejenModeView:Lcom/miui/home/settings/FixedAspectRatioLottieAnimView;

    invoke-virtual {p1}, Lcom/miui/home/settings/FixedAspectRatioLottieAnimView;->playAnimation()V

    goto :goto_0

    :pswitch_1
    iget-object p1, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->mDrawerModeView:Lcom/miui/home/settings/FixedAspectRatioLottieAnimView;

    iget-object v0, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->mDrawerAnimationName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/miui/home/settings/FixedAspectRatioLottieAnimView;->setAnimation(Ljava/lang/String;)V

    iget-object p1, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->mDrawerModeView:Lcom/miui/home/settings/FixedAspectRatioLottieAnimView;

    invoke-virtual {p1}, Lcom/miui/home/settings/FixedAspectRatioLottieAnimView;->playAnimation()V

    :cond_1
    :goto_0
    :pswitch_2
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private setChecked(I)V
    .locals 2

    invoke-direct {p0, p1}, Lcom/miui/home/settings/preference/DesktopModePreference;->setAnimation(I)V

    const/4 v0, 0x1

    const/4 v1, 0x0

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iget-object p1, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->mDesktop:Lmiuix/visual/check/VisualCheckBox;

    invoke-virtual {p1, v1}, Lmiuix/visual/check/VisualCheckBox;->setChecked(Z)V

    iget-object p1, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->mDrawer:Lmiuix/visual/check/VisualCheckBox;

    invoke-virtual {p1, v1}, Lmiuix/visual/check/VisualCheckBox;->setChecked(Z)V

    iget-object p1, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->mJeejen:Lmiuix/visual/check/VisualCheckBox;

    invoke-virtual {p1, v0}, Lmiuix/visual/check/VisualCheckBox;->setChecked(Z)V

    goto :goto_0

    :pswitch_1
    iget-object p1, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->mDesktop:Lmiuix/visual/check/VisualCheckBox;

    invoke-virtual {p1, v1}, Lmiuix/visual/check/VisualCheckBox;->setChecked(Z)V

    iget-object p1, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->mDrawer:Lmiuix/visual/check/VisualCheckBox;

    invoke-virtual {p1, v0}, Lmiuix/visual/check/VisualCheckBox;->setChecked(Z)V

    iget-object p1, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->mJeejen:Lmiuix/visual/check/VisualCheckBox;

    invoke-virtual {p1, v1}, Lmiuix/visual/check/VisualCheckBox;->setChecked(Z)V

    goto :goto_0

    :pswitch_2
    iget-object p1, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->mDesktop:Lmiuix/visual/check/VisualCheckBox;

    invoke-virtual {p1, v0}, Lmiuix/visual/check/VisualCheckBox;->setChecked(Z)V

    iget-object p1, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->mDrawer:Lmiuix/visual/check/VisualCheckBox;

    invoke-virtual {p1, v1}, Lmiuix/visual/check/VisualCheckBox;->setChecked(Z)V

    iget-object p1, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->mJeejen:Lmiuix/visual/check/VisualCheckBox;

    invoke-virtual {p1, v1}, Lmiuix/visual/check/VisualCheckBox;->setChecked(Z)V

    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public onBindViewHolder(Landroidx/preference/PreferenceViewHolder;)V
    .locals 2

    invoke-super {p0, p1}, Landroidx/preference/Preference;->onBindViewHolder(Landroidx/preference/PreferenceViewHolder;)V

    invoke-virtual {p0}, Lcom/miui/home/settings/preference/DesktopModePreference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/miui/home/launcher/DeviceConfig;->isNeedRemoveEasyMode(Landroid/content/Context;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    sput-boolean v0, Lcom/miui/home/settings/preference/DesktopModePreference;->mSupportJeejen:Z

    iget-object v0, p1, Landroidx/preference/PreferenceViewHolder;->itemView:Landroid/view/View;

    check-cast v0, Lmiuix/visual/check/VisualCheckGroup;

    iput-object v0, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->mVisualCheckGroup:Lmiuix/visual/check/VisualCheckGroup;

    const v0, 0x7f0a010b

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceViewHolder;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiuix/visual/check/VisualCheckBox;

    iput-object v0, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->mDesktop:Lmiuix/visual/check/VisualCheckBox;

    const v0, 0x7f0a012a

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceViewHolder;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiuix/visual/check/VisualCheckBox;

    iput-object v0, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->mDrawer:Lmiuix/visual/check/VisualCheckBox;

    const v0, 0x7f0a01ea

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceViewHolder;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiuix/visual/check/VisualCheckBox;

    iput-object v0, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->mJeejen:Lmiuix/visual/check/VisualCheckBox;

    iget-object v0, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->mVisualCheckGroup:Lmiuix/visual/check/VisualCheckGroup;

    new-instance v1, Lcom/miui/home/settings/preference/DesktopModePreference$1;

    invoke-direct {v1, p0}, Lcom/miui/home/settings/preference/DesktopModePreference$1;-><init>(Lcom/miui/home/settings/preference/DesktopModePreference;)V

    invoke-virtual {v0, v1}, Lmiuix/visual/check/VisualCheckGroup;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    const v0, 0x7f0a00cc

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceViewHolder;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/miui/home/settings/FixedAspectRatioLottieAnimView;

    iput-object v0, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->mClassicModeView:Lcom/miui/home/settings/FixedAspectRatioLottieAnimView;

    const v0, 0x7f0a012c

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceViewHolder;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/miui/home/settings/FixedAspectRatioLottieAnimView;

    iput-object v0, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->mDrawerModeView:Lcom/miui/home/settings/FixedAspectRatioLottieAnimView;

    const v0, 0x7f0a01eb

    invoke-virtual {p1, v0}, Landroidx/preference/PreferenceViewHolder;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/miui/home/settings/FixedAspectRatioLottieAnimView;

    iput-object p1, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->mJeejenModeView:Lcom/miui/home/settings/FixedAspectRatioLottieAnimView;

    sget-boolean p1, Lcom/miui/home/settings/preference/DesktopModePreference;->mSupportJeejen:Z

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->mClassicModeView:Lcom/miui/home/settings/FixedAspectRatioLottieAnimView;

    const v0, 0x7f0801dc

    invoke-virtual {p1, v0}, Lcom/miui/home/settings/FixedAspectRatioLottieAnimView;->setImageResource(I)V

    iget-object p1, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->mJeejen:Lmiuix/visual/check/VisualCheckBox;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lmiuix/visual/check/VisualCheckBox;->setVisibility(I)V

    :cond_0
    iget-boolean p1, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->isFirstBind:Z

    if-eqz p1, :cond_1

    invoke-direct {p0}, Lcom/miui/home/settings/preference/DesktopModePreference;->initAnimation()V

    iget p1, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->mMode:I

    invoke-direct {p0, p1}, Lcom/miui/home/settings/preference/DesktopModePreference;->setChecked(I)V

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->isFirstBind:Z

    :cond_1
    iget-object p1, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->mVisualCheckGroup:Lmiuix/visual/check/VisualCheckGroup;

    invoke-virtual {p1, p0}, Lmiuix/visual/check/VisualCheckGroup;->setOnCheckedChangeListener(Lmiuix/visual/check/VisualCheckGroup$OnCheckedChangeListener;)V

    return-void
.end method

.method public onCheckedChanged(Lmiuix/visual/check/VisualCheckGroup;I)V
    .locals 2

    iget-object p1, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->mDrawer:Lmiuix/visual/check/VisualCheckBox;

    invoke-virtual {p1}, Lmiuix/visual/check/VisualCheckBox;->getId()I

    move-result p1

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p2, p1, :cond_0

    move p1, v0

    goto :goto_0

    :cond_0
    move p1, v1

    :goto_0
    iput-boolean p1, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->mEndIsDrawer:Z

    iget-object p1, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->mDesktop:Lmiuix/visual/check/VisualCheckBox;

    invoke-virtual {p1}, Lmiuix/visual/check/VisualCheckBox;->getId()I

    move-result p1

    if-ne p2, p1, :cond_1

    iput v1, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->mMode:I

    goto :goto_1

    :cond_1
    iget-object p1, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->mDrawer:Lmiuix/visual/check/VisualCheckBox;

    invoke-virtual {p1}, Lmiuix/visual/check/VisualCheckBox;->getId()I

    move-result p1

    if-ne p2, p1, :cond_2

    iput v0, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->mMode:I

    goto :goto_1

    :cond_2
    iget-object p1, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->mJeejen:Lmiuix/visual/check/VisualCheckBox;

    invoke-virtual {p1}, Lmiuix/visual/check/VisualCheckBox;->getId()I

    move-result p1

    if-ne p2, p1, :cond_3

    const/4 p1, 0x2

    iput p1, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->mMode:I

    :cond_3
    :goto_1
    iget-object p1, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->mOnDesktopModeTemporaryChangeListener:Lcom/miui/home/settings/preference/DesktopModePreference$OnDesktopModeTemporaryChangeListener;

    if-eqz p1, :cond_4

    iget p2, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->mMode:I

    invoke-interface {p1, p2}, Lcom/miui/home/settings/preference/DesktopModePreference$OnDesktopModeTemporaryChangeListener;->OnDesktopModeTemporaryChange(I)V

    :cond_4
    iget p1, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->mMode:I

    invoke-direct {p0, p1}, Lcom/miui/home/settings/preference/DesktopModePreference;->setAnimation(I)V

    return-void
.end method

.method public setDesktopMode()V
    .locals 6

    iget-boolean v0, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->mEndIsDrawer:Z

    iget-boolean v1, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->mStartIsDrawer:Z

    const/4 v2, 0x1

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/miui/home/settings/preference/DesktopModePreference;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-boolean v1, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->mEndIsDrawer:Z

    invoke-static {v0, v1}, Lcom/miui/home/launcher/allapps/LauncherModeController;->setDrawerModeEnable(Landroid/content/Context;Z)V

    invoke-static {}, Lcom/mi/globallauncher/manager/BranchInterface;->getCommercialPref()Lcom/mi/globallauncher/branchInterface/ICommercialPreference;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/mi/globallauncher/branchInterface/ICommercialPreference;->setHasChangedDrawerMode(Z)V

    :cond_0
    iget v0, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->mMode:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    iget-boolean v0, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->mStartIsDrawer:Z

    if-eqz v0, :cond_1

    iput-boolean v2, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->mEndIsDrawer:Z

    invoke-virtual {p0}, Lcom/miui/home/settings/preference/DesktopModePreference;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-boolean v1, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->mEndIsDrawer:Z

    invoke-static {v0, v1}, Lcom/miui/home/launcher/allapps/LauncherModeController;->setDrawerModeEnable(Landroid/content/Context;Z)V

    :cond_1
    const-string v0, "DesktopModePreference"

    const-string v1, "start sending jeejen broadcast..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/miui/home/launcher/allapps/LauncherModeController;->getJeejenReceiverInfo()Ljava/util/List;

    move-result-object v0

    invoke-static {}, Lcom/miui/home/launcher/allapps/LauncherModeController;->getJeejenBroadcastIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    const/4 v3, 0x0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/pm/ResolveInfo;

    iget-object v4, v4, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v4, v4, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    const-string v5, "com.xiaomi.misettings"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {p0}, Lcom/miui/home/settings/preference/DesktopModePreference;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {}, Lcom/miui/launcher/utils/LauncherUtils;->getCurrentUserHandle()Landroid/os/UserHandle;

    move-result-object v5

    invoke-virtual {v4, v1, v5}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    invoke-static {v2}, Lcom/miui/home/launcher/allapps/LauncherModeController;->setJeejenBroadcastSend(Z)V

    const-string v1, "DesktopModePreference"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "jeejen broadcast is send to:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/miui/home/settings/preference/DesktopModePreference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "elderly_mode"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    invoke-virtual {p0}, Lcom/miui/home/settings/preference/DesktopModePreference;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "previous_font"

    invoke-static {}, Landroid/content/res/MiuiConfiguration;->getScaleMode()I

    move-result v2

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    :cond_3
    :goto_0
    invoke-static {}, Lcom/miui/home/launcher/common/PreferenceUtils;->getInstance()Lcom/miui/home/launcher/common/PreferenceUtils;

    move-result-object v0

    iget-boolean v1, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->mEndIsDrawer:Z

    invoke-virtual {v0, v1}, Lcom/miui/home/launcher/common/PreferenceUtils;->setDrawerModeEnable(Z)V

    return-void
.end method

.method public setInitValue(I)V
    .locals 1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->mEndIsDrawer:Z

    iput-boolean v0, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->mStartIsDrawer:Z

    iput p1, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->mMode:I

    iget-object p1, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->mDesktop:Lmiuix/visual/check/VisualCheckBox;

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->mDrawer:Lmiuix/visual/check/VisualCheckBox;

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->mJeejen:Lmiuix/visual/check/VisualCheckBox;

    if-eqz p1, :cond_1

    iget p1, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->mMode:I

    invoke-direct {p0, p1}, Lcom/miui/home/settings/preference/DesktopModePreference;->setChecked(I)V

    :cond_1
    return-void
.end method

.method public setOnDesktopModeTemporaryChangeListener(Lcom/miui/home/settings/preference/DesktopModePreference$OnDesktopModeTemporaryChangeListener;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/home/settings/preference/DesktopModePreference;->mOnDesktopModeTemporaryChangeListener:Lcom/miui/home/settings/preference/DesktopModePreference$OnDesktopModeTemporaryChangeListener;

    return-void
.end method
