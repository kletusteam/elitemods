.class public Lcom/miui/home/launcher/IconCustomizePreviewLayout;
.super Landroid/widget/LinearLayout;

# interfaces
.implements Landroid/preference/CustomUpdater$CustomReceiver;


# instance fields
.field private icon:Lcom/miui/home/launcher/LauncherIconImageView;

.field private final iconHelper:Lcom/miui/home/launcher/util/LauncherIconHelper;

.field private label:Lcom/miui/home/launcher/TitleTextView;

.field private mFirstDrawMark:Z

.field private mIconBitmap:Landroid/graphics/Bitmap;

.field private mIsDockMode:Z

.field private mIsHideTitle:Z

.field private titleContainer:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->mFirstDrawMark:Z

    invoke-static {}, Lcom/miui/home/launcher/util/LauncherIconHelper;->getInstance()Lcom/miui/home/launcher/util/LauncherIconHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->iconHelper:Lcom/miui/home/launcher/util/LauncherIconHelper;

    return-void
.end method

.method static synthetic access$000(Lcom/miui/home/launcher/IconCustomizePreviewLayout;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->updateLabel()V

    return-void
.end method

.method private createReflectionBitmap()Landroid/graphics/Bitmap;
    .locals 12

    const/high16 v5, 0x40000000    # 2.0f

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->getHeight()I

    move-result v2

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v9

    new-instance v8, Landroid/graphics/Canvas;

    invoke-direct {v8, v9}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    new-instance v10, Landroid/graphics/Matrix;

    invoke-direct {v10}, Landroid/graphics/Matrix;-><init>()V

    iget-object v0, p0, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->icon:Lcom/miui/home/launcher/LauncherIconImageView;

    invoke-virtual {v0}, Lcom/miui/home/launcher/LauncherIconImageView;->getWidth()I

    move-result v0

    int-to-float v0, v0

    iget-object v2, p0, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->mIconBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v0, v2

    iget-object v2, p0, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->icon:Lcom/miui/home/launcher/LauncherIconImageView;

    invoke-virtual {v2}, Lcom/miui/home/launcher/LauncherIconImageView;->getHeight()I

    move-result v2

    neg-int v2, v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->mIconBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    iget-object v3, p0, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->icon:Lcom/miui/home/launcher/LauncherIconImageView;

    invoke-virtual {v3}, Lcom/miui/home/launcher/LauncherIconImageView;->getWidth()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v5

    iget-object v4, p0, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->icon:Lcom/miui/home/launcher/LauncherIconImageView;

    invoke-virtual {v4}, Lcom/miui/home/launcher/LauncherIconImageView;->getHeight()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v4, v5

    invoke-virtual {v10, v0, v2, v3, v4}, Landroid/graphics/Matrix;->setScale(FFFF)V

    invoke-virtual {p0}, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->getWidth()I

    move-result v0

    iget-object v2, p0, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->mIconBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    sub-int/2addr v0, v2

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    iget-object v2, p0, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->icon:Lcom/miui/home/launcher/LauncherIconImageView;

    invoke-virtual {v2}, Lcom/miui/home/launcher/LauncherIconImageView;->getBottom()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->iconHelper:Lcom/miui/home/launcher/util/LauncherIconHelper;

    invoke-virtual {v3}, Lcom/miui/home/launcher/util/LauncherIconHelper;->getIconReflectionGap()F

    move-result v3

    add-float/2addr v2, v3

    invoke-virtual {v10, v0, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    iget-object v0, p0, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->mIconBitmap:Landroid/graphics/Bitmap;

    const/4 v2, 0x0

    invoke-virtual {v8, v0, v10, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    new-instance v11, Landroid/graphics/Paint;

    invoke-direct {v11}, Landroid/graphics/Paint;-><init>()V

    new-instance v0, Landroid/graphics/LinearGradient;

    iget-object v2, p0, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->icon:Lcom/miui/home/launcher/LauncherIconImageView;

    invoke-virtual {v2}, Lcom/miui/home/launcher/LauncherIconImageView;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p0}, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->getHeight()I

    move-result v3

    int-to-float v4, v3

    iget-object v3, p0, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->iconHelper:Lcom/miui/home/launcher/util/LauncherIconHelper;

    invoke-virtual {v3}, Lcom/miui/home/launcher/util/LauncherIconHelper;->getReflectionShadowColor()I

    move-result v5

    const v6, 0xffffff

    sget-object v7, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    move v3, v1

    invoke-direct/range {v0 .. v7}, Landroid/graphics/LinearGradient;-><init>(FFFFIILandroid/graphics/Shader$TileMode;)V

    invoke-virtual {v11, v0}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    new-instance v0, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->DST_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v0, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v11, v0}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    iget-object v0, p0, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->icon:Lcom/miui/home/launcher/LauncherIconImageView;

    invoke-virtual {v0}, Lcom/miui/home/launcher/LauncherIconImageView;->getHeight()I

    move-result v0

    int-to-float v2, v0

    invoke-virtual {p0}, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->getWidth()I

    move-result v0

    int-to-float v3, v0

    invoke-virtual {p0}, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->getHeight()I

    move-result v0

    int-to-float v4, v0

    move-object v0, v8

    move-object v5, v11

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    return-object v9
.end method

.method private drawDarkShadow(Landroid/graphics/Canvas;II)V
    .locals 10

    const/4 v6, 0x0

    const/4 v9, 0x0

    iget-object v5, p0, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->iconHelper:Lcom/miui/home/launcher/util/LauncherIconHelper;

    invoke-virtual {p0}, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->isDockViewMode()Z

    move-result v7

    invoke-virtual {v5, v7}, Lcom/miui/home/launcher/util/LauncherIconHelper;->isShadowState(Z)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-direct {p0}, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->getIconBitmap()Landroid/graphics/Bitmap;

    move-result-object v5

    if-eqz v5, :cond_0

    if-lez p2, :cond_0

    if-lez p3, :cond_0

    iget-object v5, p0, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->iconHelper:Lcom/miui/home/launcher/util/LauncherIconHelper;

    invoke-virtual {v5}, Lcom/miui/home/launcher/util/LauncherIconHelper;->getDarkShadowSize()F

    move-result v1

    invoke-virtual {p0}, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->getWidth()I

    move-result v5

    invoke-virtual {p0}, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->getHeight()I

    move-result v7

    sget-object v8, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v5, v7, v8}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    new-instance v4, Landroid/graphics/Canvas;

    invoke-direct {v4, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-eqz v2, :cond_0

    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    iget-object v5, p0, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->icon:Lcom/miui/home/launcher/LauncherIconImageView;

    invoke-virtual {v5}, Lcom/miui/home/launcher/LauncherIconImageView;->getLeft()I

    move-result v5

    iput v5, v3, Landroid/graphics/Rect;->left:I

    iget-object v5, p0, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->icon:Lcom/miui/home/launcher/LauncherIconImageView;

    invoke-virtual {v5}, Lcom/miui/home/launcher/LauncherIconImageView;->getTop()I

    move-result v5

    iput v5, v3, Landroid/graphics/Rect;->top:I

    iget v5, v3, Landroid/graphics/Rect;->left:I

    iget-object v7, p0, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->icon:Lcom/miui/home/launcher/LauncherIconImageView;

    invoke-virtual {v7}, Lcom/miui/home/launcher/LauncherIconImageView;->getWidth()I

    move-result v7

    add-int/2addr v5, v7

    iput v5, v3, Landroid/graphics/Rect;->right:I

    iget v5, v3, Landroid/graphics/Rect;->top:I

    iget-object v7, p0, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->icon:Lcom/miui/home/launcher/LauncherIconImageView;

    invoke-virtual {v7}, Lcom/miui/home/launcher/LauncherIconImageView;->getHeight()I

    move-result v7

    add-int/2addr v5, v7

    iput v5, v3, Landroid/graphics/Rect;->bottom:I

    iget-object v7, p0, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->mIconBitmap:Landroid/graphics/Bitmap;

    move-object v5, v6

    check-cast v5, Landroid/graphics/Rect;

    iget-object v8, p0, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->iconHelper:Lcom/miui/home/launcher/util/LauncherIconHelper;

    invoke-virtual {v8}, Lcom/miui/home/launcher/util/LauncherIconHelper;->getDarkShadowColor()I

    move-result v8

    invoke-static {v1, v8}, Lcom/miui/home/launcher/common/Utilities;->getIconDarkShadowPaint(FI)Landroid/graphics/Paint;

    move-result-object v8

    invoke-virtual {v4, v7, v5, v3, v8}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    check-cast v6, Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v9, v9, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    :cond_0
    return-void
.end method

.method private drawReflectionShadow(Landroid/graphics/Canvas;)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->iconHelper:Lcom/miui/home/launcher/util/LauncherIconHelper;

    invoke-virtual {p0}, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->isDockViewMode()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/miui/home/launcher/util/LauncherIconHelper;->isReflectState(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->getIconBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->createReflectionBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v2, v2, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method private getIconBitmap()Landroid/graphics/Bitmap;
    .locals 6

    iget-object v4, p0, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->mIconBitmap:Landroid/graphics/Bitmap;

    if-nez v4, :cond_0

    iget-object v2, p0, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->icon:Lcom/miui/home/launcher/LauncherIconImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v2}, Landroid/widget/ImageView;->getWidth()I

    move-result v3

    invoke-virtual {v2}, Landroid/widget/ImageView;->getHeight()I

    move-result v1

    if-lez v3, :cond_0

    if-lez v1, :cond_0

    invoke-static {v0}, Landroid/Utils/ImageUtils;->drawableToBitmap(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Bitmap;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v4, v3, v1, v5}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v4

    iput-object v4, p0, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->mIconBitmap:Landroid/graphics/Bitmap;

    :cond_0
    iget-object v4, p0, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->mIconBitmap:Landroid/graphics/Bitmap;

    return-object v4
.end method

.method private setIconTitle(Lcom/miui/home/launcher/TitleTextView;)V
    .locals 5

    const/4 v4, 0x1

    sget-object v1, Lcom/miui/home/launcher/IconCustomizePreviewLayout$2;->$SwitchMap$com$miui$home$launcher$util$LauncherIconHelper$IconLabelState:[I

    invoke-static {}, Lcom/miui/home/launcher/util/LauncherIconHelper;->getInstance()Lcom/miui/home/launcher/util/LauncherIconHelper;

    move-result-object v2

    invoke-virtual {p0}, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->isDockViewMode()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/miui/home/launcher/util/LauncherIconHelper;->getIconLabelState(Z)Lcom/miui/home/launcher/util/LauncherIconHelper$IconLabelState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/miui/home/launcher/util/LauncherIconHelper$IconLabelState;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    :goto_0
    invoke-virtual {p0}, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "launch_title_color"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {p0}, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "icon_title_text"

    invoke-static {v2, v3}, Landroid/Utils/Utils;->ColorToID(Landroid/content/Context;Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    :cond_0
    invoke-virtual {p1, v0}, Lcom/miui/home/launcher/TitleTextView;->setTextColor(I)V

    invoke-virtual {p0}, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "launch_title_size"

    const/16 v3, 0xc

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1, v1}, Lcom/miui/home/launcher/TitleTextView;->setTextSize(F)V

    return-void

    :pswitch_0
    invoke-virtual {p0, v4}, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->setIsHideTitle(Z)V

    goto :goto_0

    :pswitch_1
    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {p1, v1}, Lcom/miui/home/launcher/TitleTextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p1}, Lcom/miui/home/launcher/TitleTextView;->setSingleLine()V

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {p1, v1}, Lcom/miui/home/launcher/TitleTextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    invoke-virtual {p1, v4}, Lcom/miui/home/launcher/TitleTextView;->setHorizontalFadingEdgeEnabled(Z)V

    const/4 v1, -0x1

    invoke-virtual {p1, v1}, Lcom/miui/home/launcher/TitleTextView;->setMarqueeRepeatLimit(I)V

    invoke-virtual {p1, v4}, Lcom/miui/home/launcher/TitleTextView;->setSelected(Z)V

    invoke-virtual {p1, v4}, Lcom/miui/home/launcher/TitleTextView;->setHorizontallyScrolling(Z)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private updateLabel()V
    .locals 2

    iget-object v0, p0, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->label:Lcom/miui/home/launcher/TitleTextView;

    invoke-direct {p0, v0}, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->setIconTitle(Lcom/miui/home/launcher/TitleTextView;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->setIsHideTitle(Z)V

    iget-object v0, p0, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->label:Lcom/miui/home/launcher/TitleTextView;

    invoke-virtual {v0}, Lcom/miui/home/launcher/TitleTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->label:Lcom/miui/home/launcher/TitleTextView;

    iget-object v1, p0, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->label:Lcom/miui/home/launcher/TitleTextView;

    invoke-virtual {v1}, Lcom/miui/home/launcher/TitleTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/miui/home/launcher/TitleTextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    invoke-virtual {p0}, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->invalidate()V

    return-void
.end method


# virtual methods
.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->mFirstDrawMark:Z

    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method protected drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
    .locals 5

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->mFirstDrawMark:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->iconHelper:Lcom/miui/home/launcher/util/LauncherIconHelper;

    invoke-virtual {v2}, Lcom/miui/home/launcher/util/LauncherIconHelper;->isShadowEnable()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->drawReflectionShadow(Landroid/graphics/Canvas;)V

    invoke-virtual {p0}, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->getMeasuredWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->getMeasuredHeight()I

    move-result v3

    invoke-direct {p0, p1, v2, v3}, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->drawDarkShadow(Landroid/graphics/Canvas;II)V

    :cond_0
    iput-boolean v1, p0, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->mFirstDrawMark:Z

    :cond_1
    iget-boolean v2, p0, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->mIsHideTitle:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->titleContainer:Landroid/view/View;

    if-ne p2, v2, :cond_2

    :goto_0
    return v1

    :cond_2
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/LinearLayout;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v1

    goto :goto_0
.end method

.method public isDockViewMode()Z
    .locals 1

    iget-boolean v0, p0, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->mIsDockMode:Z

    return v0
.end method

.method protected onAttachedToWindow()V
    .locals 4

    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    invoke-static {}, Landroid/preference/CustomUpdater;->getInstance()Landroid/preference/CustomUpdater;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "launcher_icon_helper"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "launcher_label_icon_helper"

    aput-object v3, v1, v2

    invoke-virtual {v0, p0, v1}, Landroid/preference/CustomUpdater;->addCustomReceiver(Landroid/preference/CustomUpdater$CustomReceiver;[Ljava/lang/String;)V

    invoke-static {}, Landroid/preference/CustomUpdater;->getInstance()Landroid/preference/CustomUpdater;

    move-result-object v0

    const-string v1, "launcher_icon_helper"

    invoke-virtual {v0, v1}, Landroid/preference/CustomUpdater;->beginChange(Ljava/lang/String;)V

    return-void
.end method

.method public onCustomChanged(Ljava/lang/String;)V
    .locals 4

    const-string v0, "launcher_icon_helper"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/miui/home/launcher/common/Utilities;->resetResourceDependenceItem()V

    invoke-virtual {p0}, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->invalidate()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "launcher_label_icon_helper"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/miui/home/launcher/IconCustomizePreviewLayout$1;

    invoke-direct {v0, p0}, Lcom/miui/home/launcher/IconCustomizePreviewLayout$1;-><init>(Lcom/miui/home/launcher/IconCustomizePreviewLayout;)V

    const-wide/16 v2, 0xfa

    invoke-virtual {p0, v0, v2, v3}, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    invoke-static {}, Landroid/preference/CustomUpdater;->getInstance()Landroid/preference/CustomUpdater;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0, p0, v1}, Landroid/preference/CustomUpdater;->removeCustomReceiver(Landroid/preference/CustomUpdater$CustomReceiver;[Ljava/lang/String;)V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    invoke-virtual {p0}, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "icon_icon"

    invoke-static {v0, v1}, Landroid/Utils/Utils;->IDtoID(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/miui/home/launcher/LauncherIconImageView;

    iput-object v0, p0, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->icon:Lcom/miui/home/launcher/LauncherIconImageView;

    invoke-virtual {p0}, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "icon_title"

    invoke-static {v0, v1}, Landroid/Utils/Utils;->IDtoID(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/miui/home/launcher/TitleTextView;

    iput-object v0, p0, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->label:Lcom/miui/home/launcher/TitleTextView;

    invoke-virtual {p0}, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "icon_title_container"

    invoke-static {v0, v1}, Landroid/Utils/Utils;->IDtoID(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->titleContainer:Landroid/view/View;

    iget-object v0, p0, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->label:Lcom/miui/home/launcher/TitleTextView;

    invoke-direct {p0, v0}, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->setIconTitle(Lcom/miui/home/launcher/TitleTextView;)V

    return-void
.end method

.method public setDockViewMode(Z)V
    .locals 1

    iput-boolean p1, p0, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->mIsDockMode:Z

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->setIsHideTitle(Z)V

    iget-object v0, p0, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->label:Lcom/miui/home/launcher/TitleTextView;

    invoke-direct {p0, v0}, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->setIconTitle(Lcom/miui/home/launcher/TitleTextView;)V

    return-void
.end method

.method public setIsHideTitle(Z)V
    .locals 2

    invoke-static {}, Lcom/miui/home/launcher/util/LauncherIconHelper;->getInstance()Lcom/miui/home/launcher/util/LauncherIconHelper;

    move-result-object v0

    invoke-virtual {p0}, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->isDockViewMode()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/miui/home/launcher/util/LauncherIconHelper;->getIconLabelState(Z)Lcom/miui/home/launcher/util/LauncherIconHelper$IconLabelState;

    move-result-object v0

    sget-object v1, Lcom/miui/home/launcher/util/LauncherIconHelper$IconLabelState;->NOT_LABEL:Lcom/miui/home/launcher/util/LauncherIconHelper$IconLabelState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/miui/home/launcher/IconCustomizePreviewLayout;->mIsHideTitle:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
