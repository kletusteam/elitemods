.class Lcom/miui/home/recents/GestureStubView$MiuiSettingsObserver;
.super Landroid/database/ContentObserver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/home/recents/GestureStubView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MiuiSettingsObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/home/recents/GestureStubView;


# direct methods
.method constructor <init>(Lcom/miui/home/recents/GestureStubView;Landroid/os/Handler;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/home/recents/GestureStubView$MiuiSettingsObserver;->this$0:Lcom/miui/home/recents/GestureStubView;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 4

    iget-object p1, p0, Lcom/miui/home/recents/GestureStubView$MiuiSettingsObserver;->this$0:Lcom/miui/home/recents/GestureStubView;

    invoke-static {p1}, Lcom/miui/home/recents/GestureStubView;->access$1400(Lcom/miui/home/recents/GestureStubView;)Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "force_black_v2"

    invoke-static {v0, v1}, Lcom/miui/launcher/utils/MiuiSettingsUtils;->getGlobalBoolean(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/miui/home/recents/GestureStubView$MiuiSettingsObserver;->this$0:Lcom/miui/home/recents/GestureStubView;

    invoke-static {v0}, Lcom/miui/home/recents/GestureStubView;->access$1400(Lcom/miui/home/recents/GestureStubView;)Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "force_black"

    invoke-static {v0, v3}, Lcom/miui/launcher/utils/MiuiSettingsUtils;->getGlobalBoolean(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    move v0, v1

    goto :goto_1

    :cond_1
    :goto_0
    move v0, v2

    :goto_1
    invoke-static {p1, v0}, Lcom/miui/home/recents/GestureStubView;->access$3702(Lcom/miui/home/recents/GestureStubView;Z)Z

    iget-object p1, p0, Lcom/miui/home/recents/GestureStubView$MiuiSettingsObserver;->this$0:Lcom/miui/home/recents/GestureStubView;

    invoke-static {p1}, Lcom/miui/home/recents/GestureStubView;->access$3900(Lcom/miui/home/recents/GestureStubView;)I

    move-result v0

    if-eq v0, v2, :cond_2

    iget-object v0, p0, Lcom/miui/home/recents/GestureStubView$MiuiSettingsObserver;->this$0:Lcom/miui/home/recents/GestureStubView;

    invoke-static {v0}, Lcom/miui/home/recents/GestureStubView;->access$3900(Lcom/miui/home/recents/GestureStubView;)I

    move-result v0

    const/4 v3, 0x3

    if-ne v0, v3, :cond_3

    :cond_2
    move v1, v2

    :cond_3
    invoke-static {p1, v1}, Lcom/miui/home/recents/GestureStubView;->access$3802(Lcom/miui/home/recents/GestureStubView;Z)Z

    return-void
.end method

.method register()V
    .locals 3

    goto/32 :goto_d

    nop

    :goto_0
    invoke-static {v1}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    goto/32 :goto_6

    nop

    :goto_1
    const-string v1, "force_black"

    goto/32 :goto_0

    nop

    :goto_2
    iget-object v0, p0, Lcom/miui/home/recents/GestureStubView$MiuiSettingsObserver;->this$0:Lcom/miui/home/recents/GestureStubView;

    goto/32 :goto_5

    nop

    :goto_3
    invoke-static {v0, v1}, Lcom/miui/home/recents/GestureStubView;->access$1402(Lcom/miui/home/recents/GestureStubView;Landroid/content/ContentResolver;)Landroid/content/ContentResolver;

    goto/32 :goto_2

    nop

    :goto_4
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    goto/32 :goto_3

    nop

    :goto_5
    invoke-static {v0}, Lcom/miui/home/recents/GestureStubView;->access$1400(Lcom/miui/home/recents/GestureStubView;)Landroid/content/ContentResolver;

    move-result-object v0

    goto/32 :goto_f

    nop

    :goto_6
    invoke-virtual {v0, v1, v2, p0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    goto/32 :goto_c

    nop

    :goto_7
    iget-object v0, p0, Lcom/miui/home/recents/GestureStubView$MiuiSettingsObserver;->this$0:Lcom/miui/home/recents/GestureStubView;

    goto/32 :goto_9

    nop

    :goto_8
    invoke-static {v1}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    goto/32 :goto_e

    nop

    :goto_9
    invoke-static {v0}, Lcom/miui/home/recents/GestureStubView;->access$1400(Lcom/miui/home/recents/GestureStubView;)Landroid/content/ContentResolver;

    move-result-object v0

    goto/32 :goto_1

    nop

    :goto_a
    invoke-virtual {v0, v1, v2, p0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    goto/32 :goto_7

    nop

    :goto_b
    invoke-static {v0}, Lcom/miui/home/recents/GestureStubView;->access$1500(Lcom/miui/home/recents/GestureStubView;)Landroid/content/Context;

    move-result-object v1

    goto/32 :goto_4

    nop

    :goto_c
    return-void

    :goto_d
    iget-object v0, p0, Lcom/miui/home/recents/GestureStubView$MiuiSettingsObserver;->this$0:Lcom/miui/home/recents/GestureStubView;

    goto/32 :goto_b

    nop

    :goto_e
    const/4 v2, 0x0

    goto/32 :goto_a

    nop

    :goto_f
    const-string v1, "force_black_v2"

    goto/32 :goto_8

    nop
.end method

.method unregister()V
    .locals 2

    goto/32 :goto_2

    nop

    :goto_0
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    goto/32 :goto_1

    nop

    :goto_1
    invoke-static {v0, v1}, Lcom/miui/home/recents/GestureStubView;->access$1402(Lcom/miui/home/recents/GestureStubView;Landroid/content/ContentResolver;)Landroid/content/ContentResolver;

    goto/32 :goto_4

    nop

    :goto_2
    iget-object v0, p0, Lcom/miui/home/recents/GestureStubView$MiuiSettingsObserver;->this$0:Lcom/miui/home/recents/GestureStubView;

    goto/32 :goto_3

    nop

    :goto_3
    invoke-static {v0}, Lcom/miui/home/recents/GestureStubView;->access$1500(Lcom/miui/home/recents/GestureStubView;)Landroid/content/Context;

    move-result-object v1

    goto/32 :goto_0

    nop

    :goto_4
    iget-object v0, p0, Lcom/miui/home/recents/GestureStubView$MiuiSettingsObserver;->this$0:Lcom/miui/home/recents/GestureStubView;

    goto/32 :goto_6

    nop

    :goto_5
    invoke-virtual {v0, p0}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    goto/32 :goto_7

    nop

    :goto_6
    invoke-static {v0}, Lcom/miui/home/recents/GestureStubView;->access$1400(Lcom/miui/home/recents/GestureStubView;)Landroid/content/ContentResolver;

    move-result-object v0

    goto/32 :goto_5

    nop

    :goto_7
    return-void
.end method
