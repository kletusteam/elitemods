.class Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment$1;
.super Ljava/lang/Object;

# interfaces
.implements Landroidx/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment;->onCreatePreferences(Landroid/os/Bundle;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment;


# direct methods
.method constructor <init>(Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment$1;->this$0:Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroidx/preference/Preference;Ljava/lang/Object;)Z
    .locals 3

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment$1;->this$0:Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment;

    iget-object v0, p0, Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment$1;->this$0:Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment;

    invoke-virtual {v0}, Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment;->getTaskStackViewLayoutStyle(Landroid/content/Context;)I

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v2, v0}, Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment;->access$002(Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment;Z)Z

    iget-object v0, p0, Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment$1;->this$0:Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment;

    invoke-static {v0}, Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment;->access$100(Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment;)V

    invoke-static {}, Landroid/preference/CustomUpdater;->getInstance()Landroid/preference/CustomUpdater;

    move-result-object v0

    const-string v2, "recent_panel_animation"

    invoke-virtual {v0, v2}, Landroid/preference/CustomUpdater;->beginChange(Ljava/lang/String;)V

    return v1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
