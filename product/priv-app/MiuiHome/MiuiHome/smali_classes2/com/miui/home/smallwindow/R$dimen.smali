.class public final Lcom/miui/home/smallwindow/R$dimen;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/home/smallwindow/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final bottom_padding:I = 0x7f070086

.field public static final edit_bottom_padding:I = 0x7f07012d

.field public static final edit_left_padding:I = 0x7f07012e

.field public static final edit_right_padding:I = 0x7f070149

.field public static final edit_top_padding:I = 0x7f07014b

.field public static final left_padding:I = 0x7f0701ed

.field public static final recent_small_window_margin_top:I = 0x7f0705be

.field public static final recent_small_window_margin_top_horizontal_not_fixed_rotation:I = 0x7f0705bf

.field public static final recent_tv_small_window_margin_start:I = 0x7f0705c5

.field public static final recent_tv_small_window_margin_top:I = 0x7f0705c6

.field public static final recents_tv_small_window_text_size:I = 0x7f0705e5

.field public static final selected_bottom_padding:I = 0x7f070605

.field public static final selected_left_padding:I = 0x7f070606

.field public static final selected_right_padding:I = 0x7f070607

.field public static final selected_top_padding:I = 0x7f070608

.field public static final small_window_container_item_image_size:I = 0x7f070622

.field public static final small_window_container_title_top_margin:I = 0x7f070623

.field public static final small_window_edit_head_background_height:I = 0x7f070624

.field public static final small_window_edit_header_text_size:I = 0x7f070625

.field public static final small_window_edit_icon_badge_size:I = 0x7f070626

.field public static final small_window_edit_icon_height:I = 0x7f070627

.field public static final small_window_edit_icon_image_size:I = 0x7f070628

.field public static final small_window_edit_icon_width:I = 0x7f070629

.field public static final small_window_edit_title_text_size:I = 0x7f07062a

.field public static final small_window_edit_title_top_margin:I = 0x7f07062b

.field public static final small_window_gird_left_padding:I = 0x7f07062c

.field public static final small_window_gird_right_padding:I = 0x7f07062d

.field public static final small_window_item_title_margin_top:I = 0x7f07062e

.field public static final small_window_item_title_text_size:I = 0x7f07062f

.field public static final small_window_left_padding:I = 0x7f070630

.field public static final small_window_right_padding:I = 0x7f070631

.field public static final small_window_selected_icon_badge_size:I = 0x7f070632

.field public static final small_window_selected_icon_image_size:I = 0x7f070633

.field public static final small_window_selected_icon_size:I = 0x7f070634

.field public static final small_window_title_text_size:I = 0x7f070635

.field public static final space:I = 0x7f070640

.field public static final top_padding:I = 0x7f07067d
