.class Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment$2;
.super Ljava/lang/Object;

# interfaces
.implements Landroidx/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment;->onCreatePreferences(Landroid/os/Bundle;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment;


# direct methods
.method constructor <init>(Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment$2;->this$0:Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroidx/preference/Preference;Ljava/lang/Object;)Z
    .locals 2

    iget-object v0, p0, Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment$2;->this$0:Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment;

    invoke-static {v0}, Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment;->access$000(Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "recent_horizontal_style"

    :goto_0
    check-cast p2, Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v0, v1}, Landroid/preference/SettingsEliteHelper;->putIntinSettings(Ljava/lang/String;I)V

    invoke-static {}, Landroid/preference/CustomUpdater;->getInstance()Landroid/preference/CustomUpdater;

    move-result-object v0

    const-string v1, "recent_panel_animation"

    invoke-virtual {v0, v1}, Landroid/preference/CustomUpdater;->beginChange(Ljava/lang/String;)V

    const/4 v0, 0x1

    return v0

    :cond_0
    const-string v0, "recent_vertical_style"

    goto :goto_0
.end method
