.class public Lcom/miui/home/recents/views/TaskStackViewVerticalScroller;
.super Lcom/miui/home/recents/views/TaskStackViewScroller;


# instance fields
.field private mExitRecentVelocityThreshold:I

.field private mFlingDownY:F

.field private mScroller:Landroid/widget/OverScroller;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/miui/home/recents/views/TaskStackViewScroller$TaskStackViewScrollerCallback;Lcom/miui/home/recents/views/TaskStackLayoutAlgorithm;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/miui/home/recents/views/TaskStackViewScroller;-><init>(Landroid/content/Context;Lcom/miui/home/recents/views/TaskStackViewScroller$TaskStackViewScrollerCallback;Lcom/miui/home/recents/views/TaskStackLayoutAlgorithm;)V

    const/16 p2, 0x4b0

    iput p2, p0, Lcom/miui/home/recents/views/TaskStackViewVerticalScroller;->mExitRecentVelocityThreshold:I

    new-instance p2, Landroid/widget/OverScroller;

    invoke-direct {p2, p1}, Landroid/widget/OverScroller;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/miui/home/recents/views/TaskStackViewVerticalScroller;->mScroller:Landroid/widget/OverScroller;

    iget p2, p0, Lcom/miui/home/recents/views/TaskStackViewVerticalScroller;->mExitRecentVelocityThreshold:I

    int-to-float p2, p2

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p1

    iget p1, p1, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr p2, p1

    float-to-int p1, p2

    iput p1, p0, Lcom/miui/home/recents/views/TaskStackViewVerticalScroller;->mExitRecentVelocityThreshold:I

    return-void
.end method


# virtual methods
.method animateBoundScroll(I)V
    .locals 6

    goto/32 :goto_11

    nop

    :goto_0
    cmpg-double p1, v2, v4

    goto/32 :goto_7

    nop

    :goto_1
    if-gez v2, :cond_0

    goto/32 :goto_a

    :cond_0
    goto/32 :goto_13

    nop

    :goto_2
    invoke-virtual {p0, v1, p1}, Lcom/miui/home/recents/views/TaskStackViewVerticalScroller;->animateScroll(FLjava/lang/Runnable;)V

    goto/32 :goto_9

    nop

    :goto_3
    float-to-double v2, v0

    goto/32 :goto_18

    nop

    :goto_4
    invoke-static {v1, v0}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    goto/32 :goto_17

    nop

    :goto_5
    invoke-static {}, Lcom/miui/home/library/utils/AsyncTaskExecutorHelper;->getEventBus()Lorg/greenrobot/eventbus/EventBus;

    move-result-object p1

    goto/32 :goto_1c

    nop

    :goto_6
    cmpg-float v2, v0, v2

    goto/32 :goto_1

    nop

    :goto_7
    if-ltz p1, :cond_1

    goto/32 :goto_1a

    :cond_1
    goto/32 :goto_19

    nop

    :goto_8
    const/4 v2, 0x0

    goto/32 :goto_15

    nop

    :goto_9
    goto :goto_d

    :goto_a
    goto/32 :goto_b

    nop

    :goto_b
    const-string p1, "dropDown"

    goto/32 :goto_1b

    nop

    :goto_c
    invoke-virtual {p1, v0}, Lorg/greenrobot/eventbus/EventBus;->post(Ljava/lang/Object;)V

    :goto_d
    goto/32 :goto_16

    nop

    :goto_e
    const/4 v1, 0x1

    goto/32 :goto_8

    nop

    :goto_f
    const v2, -0x41b33333    # -0.2f

    goto/32 :goto_6

    nop

    :goto_10
    const/4 p1, 0x0

    goto/32 :goto_2

    nop

    :goto_11
    invoke-virtual {p0}, Lcom/miui/home/recents/views/TaskStackViewVerticalScroller;->getStackScroll()F

    move-result v0

    goto/32 :goto_12

    nop

    :goto_12
    invoke-virtual {p0, v0}, Lcom/miui/home/recents/views/TaskStackViewVerticalScroller;->getBoundedStackScroll(F)F

    move-result v1

    goto/32 :goto_4

    nop

    :goto_13
    iget v2, p0, Lcom/miui/home/recents/views/TaskStackViewVerticalScroller;->mExitRecentVelocityThreshold:I

    goto/32 :goto_14

    nop

    :goto_14
    if-gt p1, v2, :cond_2

    goto/32 :goto_1a

    :cond_2
    goto/32 :goto_3

    nop

    :goto_15
    invoke-direct {v0, v2, v1, v2, v1}, Lcom/miui/home/recents/messages/HideRecentsEvent;-><init>(ZZZZ)V

    goto/32 :goto_c

    nop

    :goto_16
    return-void

    :goto_17
    if-nez v2, :cond_3

    goto/32 :goto_d

    :cond_3
    goto/32 :goto_f

    nop

    :goto_18
    const-wide v4, -0x405147ae0ccccccdL    # -0.06000000089406967

    goto/32 :goto_0

    nop

    :goto_19
    goto :goto_a

    :goto_1a
    goto/32 :goto_10

    nop

    :goto_1b
    invoke-static {p1}, Lcom/miui/home/launcher/AnalyticalDataCollectorForRecents;->sendHideRecentsEvent(Ljava/lang/String;)V

    goto/32 :goto_5

    nop

    :goto_1c
    new-instance v0, Lcom/miui/home/recents/messages/HideRecentsEvent;

    goto/32 :goto_e

    nop
.end method

.method computeScroll()Z
    .locals 3

    goto/32 :goto_6

    nop

    :goto_0
    return v0

    :goto_1
    goto/32 :goto_4

    nop

    :goto_2
    iget v1, p0, Lcom/miui/home/recents/views/TaskStackViewVerticalScroller;->mFlingDownScrollP:F

    goto/32 :goto_3

    nop

    :goto_3
    invoke-virtual {p0, v1, v0}, Lcom/miui/home/recents/views/TaskStackViewVerticalScroller;->setDeltaStackScroll(FF)V

    goto/32 :goto_b

    nop

    :goto_4
    const/4 v0, 0x0

    goto/32 :goto_7

    nop

    :goto_5
    invoke-virtual {v0, v1, v2}, Lcom/miui/home/recents/views/TaskStackLayoutAlgorithm;->getDeltaPForY(FF)F

    move-result v0

    goto/32 :goto_2

    nop

    :goto_6
    iget-object v0, p0, Lcom/miui/home/recents/views/TaskStackViewVerticalScroller;->mScroller:Landroid/widget/OverScroller;

    goto/32 :goto_e

    nop

    :goto_7
    return v0

    :goto_8
    iget-object v0, p0, Lcom/miui/home/recents/views/TaskStackViewVerticalScroller;->mLayoutAlgorithm:Lcom/miui/home/recents/views/TaskStackLayoutAlgorithm;

    goto/32 :goto_c

    nop

    :goto_9
    int-to-float v2, v2

    goto/32 :goto_5

    nop

    :goto_a
    invoke-virtual {v2}, Landroid/widget/OverScroller;->getCurrY()I

    move-result v2

    goto/32 :goto_9

    nop

    :goto_b
    const/4 v0, 0x1

    goto/32 :goto_0

    nop

    :goto_c
    iget v1, p0, Lcom/miui/home/recents/views/TaskStackViewVerticalScroller;->mFlingDownY:F

    goto/32 :goto_f

    nop

    :goto_d
    if-nez v0, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_8

    nop

    :goto_e
    invoke-virtual {v0}, Landroid/widget/OverScroller;->computeScrollOffset()Z

    move-result v0

    goto/32 :goto_d

    nop

    :goto_f
    iget-object v2, p0, Lcom/miui/home/recents/views/TaskStackViewVerticalScroller;->mScroller:Landroid/widget/OverScroller;

    goto/32 :goto_a

    nop
.end method

.method public fling(FFFIIIIIIIII)V
    .locals 12

    move-object v0, p0

    move v1, p1

    iput v1, v0, Lcom/miui/home/recents/views/TaskStackViewVerticalScroller;->mFlingDownScrollP:F

    move v1, p3

    iput v1, v0, Lcom/miui/home/recents/views/TaskStackViewVerticalScroller;->mFlingDownY:F

    iget-object v1, v0, Lcom/miui/home/recents/views/TaskStackViewVerticalScroller;->mScroller:Landroid/widget/OverScroller;

    move/from16 v2, p4

    move/from16 v3, p5

    move/from16 v4, p6

    move/from16 v5, p7

    move/from16 v6, p8

    move/from16 v7, p9

    move/from16 v8, p10

    move/from16 v9, p11

    move/from16 v10, p12

    move/from16 v11, p12

    invoke-virtual/range {v1 .. v11}, Landroid/widget/OverScroller;->fling(IIIIIIIIII)V

    return-void
.end method

.method public getNearestMotionlessScrollP(F)F
    .locals 0

    return p1
.end method

.method public scrollToNearestMotionlessPosition()V
    .locals 0

    return-void
.end method

.method stopScroller()V
    .locals 1

    goto/32 :goto_6

    nop

    :goto_0
    invoke-virtual {v0}, Landroid/widget/OverScroller;->abortAnimation()V

    :goto_1
    goto/32 :goto_2

    nop

    :goto_2
    return-void

    :goto_3
    if-eqz v0, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_4

    nop

    :goto_4
    iget-object v0, p0, Lcom/miui/home/recents/views/TaskStackViewVerticalScroller;->mScroller:Landroid/widget/OverScroller;

    goto/32 :goto_0

    nop

    :goto_5
    invoke-virtual {v0}, Landroid/widget/OverScroller;->isFinished()Z

    move-result v0

    goto/32 :goto_3

    nop

    :goto_6
    iget-object v0, p0, Lcom/miui/home/recents/views/TaskStackViewVerticalScroller;->mScroller:Landroid/widget/OverScroller;

    goto/32 :goto_5

    nop
.end method
