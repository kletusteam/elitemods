.class public Lcom/miui/home/recents/views/TaskView$TaskIdAttributeContainer;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/home/recents/views/TaskView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "TaskIdAttributeContainer"
.end annotation


# instance fields
.field private final mIconView:Landroid/widget/ImageView;

.field private mStagePosition:I

.field private final mTask:Lcom/android/systemui/shared/recents/model/Task;

.field private final mThumbnailView:Lcom/miui/home/recents/views/TaskViewThumbnail;

.field final synthetic this$0:Lcom/miui/home/recents/views/TaskView;


# direct methods
.method public constructor <init>(Lcom/miui/home/recents/views/TaskView;Lcom/android/systemui/shared/recents/model/Task;Lcom/miui/home/recents/views/TaskViewThumbnail;Landroid/widget/ImageView;I)V
    .locals 0

    iput-object p1, p0, Lcom/miui/home/recents/views/TaskView$TaskIdAttributeContainer;->this$0:Lcom/miui/home/recents/views/TaskView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/miui/home/recents/views/TaskView$TaskIdAttributeContainer;->mTask:Lcom/android/systemui/shared/recents/model/Task;

    iput-object p3, p0, Lcom/miui/home/recents/views/TaskView$TaskIdAttributeContainer;->mThumbnailView:Lcom/miui/home/recents/views/TaskViewThumbnail;

    iput-object p4, p0, Lcom/miui/home/recents/views/TaskView$TaskIdAttributeContainer;->mIconView:Landroid/widget/ImageView;

    iput p5, p0, Lcom/miui/home/recents/views/TaskView$TaskIdAttributeContainer;->mStagePosition:I

    return-void
.end method


# virtual methods
.method public getIconView()Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/miui/home/recents/views/TaskView$TaskIdAttributeContainer;->mIconView:Landroid/widget/ImageView;

    return-object v0
.end method

.method public getTask()Lcom/android/systemui/shared/recents/model/Task;
    .locals 1

    iget-object v0, p0, Lcom/miui/home/recents/views/TaskView$TaskIdAttributeContainer;->mTask:Lcom/android/systemui/shared/recents/model/Task;

    return-object v0
.end method

.method public getThumbnailView()Lcom/miui/home/recents/views/TaskViewThumbnail;
    .locals 1

    iget-object v0, p0, Lcom/miui/home/recents/views/TaskView$TaskIdAttributeContainer;->mThumbnailView:Lcom/miui/home/recents/views/TaskViewThumbnail;

    return-object v0
.end method
