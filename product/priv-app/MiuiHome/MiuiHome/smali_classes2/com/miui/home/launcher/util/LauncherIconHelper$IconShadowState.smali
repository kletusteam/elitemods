.class public final enum Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/home/launcher/util/LauncherIconHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "IconShadowState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;

.field public static final enum NOT_SHADOW:Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;

.field public static final enum ONLY_REFLECT:Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;

.field public static final enum ONLY_SHADOW:Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;

.field public static final enum SHADOW_AND_REFLECT:Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;

    const-string v1, "NOT_SHADOW"

    invoke-direct {v0, v1, v2}, Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;->NOT_SHADOW:Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;

    new-instance v0, Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;

    const-string v1, "ONLY_SHADOW"

    invoke-direct {v0, v1, v3}, Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;->ONLY_SHADOW:Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;

    new-instance v0, Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;

    const-string v1, "ONLY_REFLECT"

    invoke-direct {v0, v1, v4}, Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;->ONLY_REFLECT:Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;

    new-instance v0, Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;

    const-string v1, "SHADOW_AND_REFLECT"

    invoke-direct {v0, v1, v5}, Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;->SHADOW_AND_REFLECT:Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;

    sget-object v1, Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;->NOT_SHADOW:Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;->ONLY_SHADOW:Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;->ONLY_REFLECT:Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;->SHADOW_AND_REFLECT:Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;

    aput-object v1, v0, v5

    sput-object v0, Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;->$VALUES:[Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;
    .locals 1

    const-class v0, Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;

    return-object v0
.end method

.method public static values()[Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;
    .locals 1

    sget-object v0, Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;->$VALUES:[Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;

    invoke-virtual {v0}, [Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;

    return-object v0
.end method
