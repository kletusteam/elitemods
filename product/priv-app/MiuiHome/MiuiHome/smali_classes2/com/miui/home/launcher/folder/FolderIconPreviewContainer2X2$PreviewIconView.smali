.class public final Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;
.super Landroidx/appcompat/widget/AppCompatImageView;

# interfaces
.implements Lcom/miui/home/launcher/LauncherAble;
.implements Lcom/miui/home/launcher/anim/LaunchAppAndBackHomeAnimTarget;
.implements Lcom/miui/home/launcher/folder/IItemDragAnim;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PreviewIconView"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nFolderIconPreviewContainer2X2.kt\nKotlin\n*S Kotlin\n*F\n+ 1 FolderIconPreviewContainer2X2.kt\ncom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView\n*L\n1#1,907:1\n*E\n"
.end annotation


# instance fields
.field private mBuddyInfo:Lcom/miui/home/launcher/ShortcutInfo;

.field private final mClickDelegate:Lcom/miui/home/launcher/folder/PreviewIconClickDelegate;

.field private mContext:Landroid/content/Context;

.field private mIconSizeProvider:Lcom/miui/home/launcher/common/IconSizeProvider;

.field private final mPreViewTouchDelegate:Lcom/miui/home/launcher/folder/PreViewTouchDelegate;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const-string v0, "mContext"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2, p3}, Landroidx/appcompat/widget/AppCompatImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance p2, Lcom/miui/home/launcher/folder/PreviewIconClickDelegate;

    invoke-direct {p2}, Lcom/miui/home/launcher/folder/PreviewIconClickDelegate;-><init>()V

    iput-object p2, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;->mClickDelegate:Lcom/miui/home/launcher/folder/PreviewIconClickDelegate;

    new-instance p2, Lcom/miui/home/launcher/folder/PreViewTouchDelegate;

    invoke-direct {p2}, Lcom/miui/home/launcher/folder/PreViewTouchDelegate;-><init>()V

    iput-object p2, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;->mPreViewTouchDelegate:Lcom/miui/home/launcher/folder/PreViewTouchDelegate;

    iput-object p1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;->mContext:Landroid/content/Context;

    sget-object p1, Lcom/miui/home/launcher/folder/LauncherFolderIconSizeProvider;->Companion:Lcom/miui/home/launcher/folder/LauncherFolderIconSizeProvider$Companion;

    invoke-virtual {p1}, Lcom/miui/home/launcher/folder/LauncherFolderIconSizeProvider$Companion;->getInstance()Lcom/miui/home/launcher/folder/LauncherFolderIconSizeProvider;

    move-result-object p1

    check-cast p1, Lcom/miui/home/launcher/common/IconSizeProvider;

    iput-object p1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;->mIconSizeProvider:Lcom/miui/home/launcher/common/IconSizeProvider;

    sget-object p1, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p0, p1}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    iget-object p1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;->mPreViewTouchDelegate:Lcom/miui/home/launcher/folder/PreViewTouchDelegate;

    move-object p2, p0

    check-cast p2, Landroid/view/View;

    invoke-virtual {p1, p2}, Lcom/miui/home/launcher/folder/PreViewTouchDelegate;->initTouchAnim(Landroid/view/View;)V

    return-void
.end method

.method private final setItemPadding()V
    .locals 1

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;->mIconSizeProvider:Lcom/miui/home/launcher/common/IconSizeProvider;

    invoke-interface {v0}, Lcom/miui/home/launcher/common/IconSizeProvider;->getFolderPreviewItemPadding()I

    move-result v0

    invoke-virtual {p0, v0, v0, v0, v0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;->setPadding(IIII)V

    return-void
.end method


# virtual methods
.method public getContentDrawable()Landroid/graphics/drawable/Drawable;
    .locals 6

    const/4 v0, 0x0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    :try_start_0
    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;->getIconImageView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;->getIconImageView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    new-instance v3, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;->getIconImageView()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v3, v4, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    move-object v1, v3

    check-cast v1, Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    const/4 v0, 0x0

    :try_start_1
    sget-object v3, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v2, v0, v3}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;->getIconImageView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-object v0, v1

    goto :goto_1

    :catch_0
    move-exception v0

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    goto :goto_0

    :catch_1
    move-exception v1

    :goto_0
    const-string v2, "WidgetTypeAnimTarget"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "get widget content drawable fail : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    return-object v0
.end method

.method public getIconImageView()Landroid/view/View;
    .locals 1

    move-object v0, p0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method public getIconImageViewOriginalLocation()Landroid/graphics/Rect;
    .locals 7

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;->getWidth()I

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;->getHeight()I

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    const/4 v0, 0x2

    new-array v0, v0, [F

    move-object v3, p0

    check-cast v3, Landroid/view/View;

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;->getRootView()Landroid/view/View;

    move-result-object v4

    invoke-static {v3, v4, v0, v2, v2}, Lcom/miui/home/launcher/common/Utilities;->getDescendantCoordRelativeToAncestor(Landroid/view/View;Landroid/view/View;[FZZ)F

    new-instance v3, Landroid/graphics/Rect;

    aget v4, v0, v2

    float-to-int v4, v4

    aget v5, v0, v1

    float-to-int v5, v5

    aget v2, v0, v2

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;->getWidth()I

    move-result v6

    int-to-float v6, v6

    add-float/2addr v2, v6

    float-to-int v2, v2

    aget v0, v0, v1

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;->getHeight()I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v0, v1

    float-to-int v0, v0

    invoke-direct {v3, v4, v5, v2, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v3

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "ItemIcon hasn\'t layout"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public getIconLocation()Landroid/graphics/Rect;
    .locals 7

    const/4 v0, 0x2

    new-array v0, v0, [I

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;->getLocationOnScreen([I)V

    const/4 v1, 0x0

    aget v2, v0, v1

    const/4 v3, 0x1

    if-nez v2, :cond_0

    aget v2, v0, v3

    if-nez v2, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    new-instance v2, Landroid/graphics/Rect;

    aget v4, v0, v1

    aget v5, v0, v3

    aget v1, v0, v1

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;->getWidth()I

    move-result v6

    add-int/2addr v1, v6

    aget v0, v0, v3

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;->getHeight()I

    move-result v3

    add-int/2addr v0, v3

    invoke-direct {v2, v4, v5, v1, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object v0, v2

    :goto_0
    return-object v0
.end method

.method public final getMBuddyInfo()Lcom/miui/home/launcher/ShortcutInfo;
    .locals 1

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;->mBuddyInfo:Lcom/miui/home/launcher/ShortcutInfo;

    return-object v0
.end method

.method public needChangeIconAlpha()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    const-string v0, "canvas"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;->mBuddyInfo:Lcom/miui/home/launcher/ShortcutInfo;

    if-eqz v0, :cond_5

    instance-of v1, v0, Lcom/miui/home/launcher/progress/ProgressShortcutInfo;

    if-eqz v1, :cond_5

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/miui/home/launcher/ShortcutInfo;->getIconBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_5

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;->getWidth()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;->mBuddyInfo:Lcom/miui/home/launcher/ShortcutInfo;

    if-nez v1, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    invoke-virtual {v1}, Lcom/miui/home/launcher/ShortcutInfo;->getIconBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    const-string v2, "mBuddyInfo!!.iconBitmap"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;->getHeight()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;->mBuddyInfo:Lcom/miui/home/launcher/ShortcutInfo;

    if-nez v2, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    invoke-virtual {v2}, Lcom/miui/home/launcher/ShortcutInfo;->getIconBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    const-string v3, "mBuddyInfo!!.iconBitmap"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->scale(FF)V

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;->mBuddyInfo:Lcom/miui/home/launcher/ShortcutInfo;

    if-nez v1, :cond_3

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_3
    invoke-virtual {v1}, Lcom/miui/home/launcher/ShortcutInfo;->getIconBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;->mBuddyInfo:Lcom/miui/home/launcher/ShortcutInfo;

    if-eqz v2, :cond_4

    check-cast v2, Lcom/miui/home/launcher/progress/ProgressShortcutInfo;

    iget v2, v2, Lcom/miui/home/launcher/progress/ProgressShortcutInfo;->mProgressPercent:I

    invoke-static {v0, p1, v1, v2}, Lcom/miui/home/launcher/progress/ApplicationProgressProcessor;->drawProgressIcon(Landroid/content/Context;Landroid/graphics/Canvas;Landroid/graphics/Bitmap;I)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto :goto_1

    :cond_4
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.miui.home.launcher.progress.ProgressShortcutInfo"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_5
    invoke-super {p0, p1}, Landroidx/appcompat/widget/AppCompatImageView;->onDraw(Landroid/graphics/Canvas;)V

    :goto_1
    return-void
.end method

.method public onEnterHomeAnimFinish()V
    .locals 2

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;->getIconImageView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getAlpha()F

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    cmpg-float v0, v0, v1

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;->getIconImageView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    :cond_0
    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;->getIconImageView()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;->setItemPadding()V

    invoke-super {p0, p1, p2}, Landroidx/appcompat/widget/AppCompatImageView;->onMeasure(II)V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    iget-object v1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;->mClickDelegate:Lcom/miui/home/launcher/folder/PreviewIconClickDelegate;

    invoke-virtual {v1, p1, p0}, Lcom/miui/home/launcher/folder/PreviewIconClickDelegate;->onViewTouchEvent(Landroid/view/MotionEvent;Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;)V

    invoke-static {}, Lcom/miui/home/launcher/folme/FolmeUtils;->isEnable()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;->mClickDelegate:Lcom/miui/home/launcher/folder/PreviewIconClickDelegate;

    invoke-virtual {v1}, Lcom/miui/home/launcher/folder/PreviewIconClickDelegate;->hasViewClickListeners()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    if-nez v0, :cond_0

    iget-object p1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;->mPreViewTouchDelegate:Lcom/miui/home/launcher/folder/PreViewTouchDelegate;

    invoke-virtual {p1}, Lcom/miui/home/launcher/folder/PreViewTouchDelegate;->onTouchDown()V

    return v1

    :cond_0
    const/4 v2, 0x3

    if-eq v0, v2, :cond_1

    if-ne v0, v1, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;->mPreViewTouchDelegate:Lcom/miui/home/launcher/folder/PreViewTouchDelegate;

    invoke-virtual {v0}, Lcom/miui/home/launcher/folder/PreViewTouchDelegate;->onTouchUp()V

    :cond_2
    invoke-super {p0, p1}, Landroidx/appcompat/widget/AppCompatImageView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1
.end method

.method public performPreViewItemHiddenAnim()V
    .locals 2

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    invoke-virtual {v0, p0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->isPreviewPlaceholder(Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public performPreViewItemShowAnim()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;->setVisibility(I)V

    return-void
.end method

.method public resetImageViewVisibility()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;->setVisibility(I)V

    return-void
.end method

.method public final setMBuddyInfo(Lcom/miui/home/launcher/ShortcutInfo;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;->mBuddyInfo:Lcom/miui/home/launcher/ShortcutInfo;

    return-void
.end method

.method public final setViewClickListener(Lcom/miui/home/launcher/folder/ListenerInfo;)V
    .locals 1

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;->mClickDelegate:Lcom/miui/home/launcher/folder/PreviewIconClickDelegate;

    invoke-virtual {v0, p0, p1}, Lcom/miui/home/launcher/folder/PreviewIconClickDelegate;->delegateViewClick(Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;Lcom/miui/home/launcher/folder/ListenerInfo;)V

    return-void
.end method
