.class Lcom/miui/home/recents/GestureStubView$OnBackInvokedCallbackWrapper;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/home/recents/GestureStubView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "OnBackInvokedCallbackWrapper"
.end annotation


# instance fields
.field private mBackNavigationInfo:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$2200(Lcom/miui/home/recents/GestureStubView$OnBackInvokedCallbackWrapper;)Ljava/lang/Object;
    .locals 0

    iget-object p0, p0, Lcom/miui/home/recents/GestureStubView$OnBackInvokedCallbackWrapper;->mBackNavigationInfo:Ljava/lang/Object;

    return-object p0
.end method


# virtual methods
.method onBackCancel()V
    .locals 2

    goto/32 :goto_7

    nop

    :goto_0
    return-void

    :goto_1
    if-nez v0, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_6

    nop

    :goto_2
    invoke-static {v0, v1}, Lcom/android/systemui/shared/recents/system/ActivityManagerWrapper;->invokeOnBackInvokedCallbackMethod(Ljava/lang/Object;Ljava/lang/String;)V

    goto/32 :goto_3

    nop

    :goto_3
    const/4 v0, 0x0

    goto/32 :goto_4

    nop

    :goto_4
    iput-object v0, p0, Lcom/miui/home/recents/GestureStubView$OnBackInvokedCallbackWrapper;->mBackNavigationInfo:Ljava/lang/Object;

    :goto_5
    goto/32 :goto_0

    nop

    :goto_6
    const-string v1, "onBackCancelled"

    goto/32 :goto_2

    nop

    :goto_7
    iget-object v0, p0, Lcom/miui/home/recents/GestureStubView$OnBackInvokedCallbackWrapper;->mBackNavigationInfo:Ljava/lang/Object;

    goto/32 :goto_1

    nop
.end method

.method onBackInvoke()V
    .locals 2

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Lcom/miui/home/recents/GestureStubView$OnBackInvokedCallbackWrapper;->mBackNavigationInfo:Ljava/lang/Object;

    goto/32 :goto_6

    nop

    :goto_1
    iput-object v0, p0, Lcom/miui/home/recents/GestureStubView$OnBackInvokedCallbackWrapper;->mBackNavigationInfo:Ljava/lang/Object;

    :goto_2
    goto/32 :goto_3

    nop

    :goto_3
    return-void

    :goto_4
    invoke-static {v0, v1}, Lcom/android/systemui/shared/recents/system/ActivityManagerWrapper;->invokeOnBackInvokedCallbackMethod(Ljava/lang/Object;Ljava/lang/String;)V

    goto/32 :goto_7

    nop

    :goto_5
    const-string v1, "onBackInvoked"

    goto/32 :goto_4

    nop

    :goto_6
    if-nez v0, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_5

    nop

    :goto_7
    const/4 v0, 0x0

    goto/32 :goto_1

    nop
.end method

.method onBackStart()V
    .locals 2

    goto/32 :goto_0

    nop

    :goto_0
    invoke-static {}, Lcom/android/systemui/shared/recents/system/ActivityManagerWrapper;->getBackNavigationInfo()Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_7

    nop

    :goto_1
    return-void

    :goto_2
    if-nez v0, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_6

    nop

    :goto_3
    invoke-static {v0, v1}, Lcom/android/systemui/shared/recents/system/ActivityManagerWrapper;->invokeOnBackInvokedCallbackMethod(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_4
    goto/32 :goto_1

    nop

    :goto_5
    iget-object v0, p0, Lcom/miui/home/recents/GestureStubView$OnBackInvokedCallbackWrapper;->mBackNavigationInfo:Ljava/lang/Object;

    goto/32 :goto_2

    nop

    :goto_6
    const-string v1, "onBackStarted"

    goto/32 :goto_3

    nop

    :goto_7
    iput-object v0, p0, Lcom/miui/home/recents/GestureStubView$OnBackInvokedCallbackWrapper;->mBackNavigationInfo:Ljava/lang/Object;

    goto/32 :goto_5

    nop
.end method
