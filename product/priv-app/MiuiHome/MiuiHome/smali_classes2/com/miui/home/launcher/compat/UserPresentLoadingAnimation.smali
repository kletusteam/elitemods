.class public Lcom/miui/home/launcher/compat/UserPresentLoadingAnimation;
.super Lcom/miui/home/launcher/compat/UserPresentAnimationCompatComplex;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/home/launcher/compat/UserPresentLoadingAnimation$LoadingAnimationListener;,
        Lcom/miui/home/launcher/compat/UserPresentLoadingAnimation$BackEaseOutInterpolator;
    }
.end annotation


# instance fields
.field private mAnimItemCount:I

.field private final mAnimatorListenerAdapter:Landroid/animation/AnimatorListenerAdapter;

.field private mIsLoading:Z

.field private mLoadingAnimation:Landroid/widget/ImageView;

.field private mLoadingAnimationListener:Lcom/miui/home/launcher/compat/UserPresentLoadingAnimation$LoadingAnimationListener;

.field private mScreenDiagonalDistance:I

.field private final mTmpLocation:[I

.field private mWorkspaceLoadingView:Landroid/widget/FrameLayout;


# direct methods
.method public constructor <init>(Lcom/miui/home/launcher/Launcher;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/home/launcher/compat/UserPresentAnimationCompatComplex;-><init>(Lcom/miui/home/launcher/Launcher;)V

    const/4 p1, 0x2

    new-array p1, p1, [I

    iput-object p1, p0, Lcom/miui/home/launcher/compat/UserPresentLoadingAnimation;->mTmpLocation:[I

    new-instance p1, Lcom/miui/home/launcher/compat/UserPresentLoadingAnimation$1;

    invoke-direct {p1, p0}, Lcom/miui/home/launcher/compat/UserPresentLoadingAnimation$1;-><init>(Lcom/miui/home/launcher/compat/UserPresentLoadingAnimation;)V

    iput-object p1, p0, Lcom/miui/home/launcher/compat/UserPresentLoadingAnimation;->mAnimatorListenerAdapter:Landroid/animation/AnimatorListenerAdapter;

    return-void
.end method

.method static synthetic access$006(Lcom/miui/home/launcher/compat/UserPresentLoadingAnimation;)I
    .locals 1

    iget v0, p0, Lcom/miui/home/launcher/compat/UserPresentLoadingAnimation;->mAnimItemCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/miui/home/launcher/compat/UserPresentLoadingAnimation;->mAnimItemCount:I

    return v0
.end method

.method static synthetic access$100(Lcom/miui/home/launcher/compat/UserPresentLoadingAnimation;)Lcom/miui/home/launcher/compat/UserPresentLoadingAnimation$LoadingAnimationListener;
    .locals 0

    iget-object p0, p0, Lcom/miui/home/launcher/compat/UserPresentLoadingAnimation;->mLoadingAnimationListener:Lcom/miui/home/launcher/compat/UserPresentLoadingAnimation$LoadingAnimationListener;

    return-object p0
.end method


# virtual methods
.method public prepareLoadingAnimation(Lcom/miui/home/launcher/compat/UserPresentLoadingAnimation$LoadingAnimationListener;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/home/launcher/compat/UserPresentLoadingAnimation;->mLoadingAnimationListener:Lcom/miui/home/launcher/compat/UserPresentLoadingAnimation$LoadingAnimationListener;

    const/4 p1, 0x0

    iput p1, p0, Lcom/miui/home/launcher/compat/UserPresentLoadingAnimation;->mAnimItemCount:I

    invoke-virtual {p0}, Lcom/miui/home/launcher/compat/UserPresentLoadingAnimation;->prepareAnimation()V

    return-void
.end method

.method prepareUserPresentAnimation(Landroid/view/View;)V
    .locals 1

    goto/32 :goto_9

    nop

    :goto_0
    iput p1, p0, Lcom/miui/home/launcher/compat/UserPresentLoadingAnimation;->mAnimItemCount:I

    goto/32 :goto_d

    nop

    :goto_1
    add-int/lit8 p1, p1, 0x1

    goto/32 :goto_0

    nop

    :goto_2
    if-nez v0, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_6

    nop

    :goto_3
    return-void

    :goto_4
    goto/32 :goto_7

    nop

    :goto_5
    invoke-interface {p1}, Lcom/miui/home/launcher/interfaces/UserPresentLoadingAnimIgnore;->needIgnore()Z

    move-result p1

    goto/32 :goto_f

    nop

    :goto_6
    check-cast p1, Lcom/miui/home/launcher/interfaces/UserPresentLoadingAnimIgnore;

    goto/32 :goto_5

    nop

    :goto_7
    iget p1, p0, Lcom/miui/home/launcher/compat/UserPresentLoadingAnimation;->mAnimItemCount:I

    goto/32 :goto_1

    nop

    :goto_8
    instance-of v0, p1, Lcom/miui/home/launcher/interfaces/UserPresentLoadingAnimIgnore;

    goto/32 :goto_2

    nop

    :goto_9
    if-eqz p1, :cond_1

    goto/32 :goto_b

    :cond_1
    goto/32 :goto_a

    nop

    :goto_a
    return-void

    :goto_b
    goto/32 :goto_e

    nop

    :goto_c
    invoke-virtual {p1, v0}, Landroid/view/View;->setAlpha(F)V

    goto/32 :goto_8

    nop

    :goto_d
    return-void

    :goto_e
    const/4 v0, 0x0

    goto/32 :goto_c

    nop

    :goto_f
    if-nez p1, :cond_2

    goto/32 :goto_4

    :cond_2
    goto/32 :goto_3

    nop
.end method

.method public showAnimation()V
    .locals 4

    iget-object v0, p0, Lcom/miui/home/launcher/compat/UserPresentLoadingAnimation;->mLauncher:Lcom/miui/home/launcher/Launcher;

    invoke-virtual {v0}, Lcom/miui/home/launcher/Launcher;->getWorkspace()Lcom/miui/home/launcher/Workspace;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/home/launcher/Workspace;->getCurrentScreenId()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/miui/home/launcher/compat/UserPresentLoadingAnimation;->mPreparedScreenId:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/miui/home/launcher/compat/UserPresentLoadingAnimation;->mLoadingAnimationListener:Lcom/miui/home/launcher/compat/UserPresentLoadingAnimation$LoadingAnimationListener;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/miui/home/launcher/compat/UserPresentLoadingAnimation$LoadingAnimationListener;->onAnimationFinish()V

    :cond_0
    invoke-super {p0}, Lcom/miui/home/launcher/compat/UserPresentAnimationCompatComplex;->showAnimation()V

    return-void
.end method

.method public showAnimationAndDismissLoading()V
    .locals 2

    iget-object v0, p0, Lcom/miui/home/launcher/compat/UserPresentLoadingAnimation;->mWorkspaceLoadingView:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/miui/home/launcher/compat/UserPresentLoadingAnimation;->mLauncher:Lcom/miui/home/launcher/Launcher;

    invoke-virtual {v0}, Lcom/miui/home/launcher/Launcher;->getShortcutMenuLayer()Lcom/miui/home/launcher/ShortcutMenuLayer;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v0, "Launcher.UserPresentAnimation"

    const-string v1, " Dismiss loading "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/miui/home/launcher/compat/UserPresentLoadingAnimation;->mLauncher:Lcom/miui/home/launcher/Launcher;

    invoke-virtual {v0}, Lcom/miui/home/launcher/Launcher;->getShortcutMenuLayer()Lcom/miui/home/launcher/ShortcutMenuLayer;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/home/launcher/compat/UserPresentLoadingAnimation;->mWorkspaceLoadingView:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Lcom/miui/home/launcher/ShortcutMenuLayer;->removeView(Landroid/view/View;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/miui/home/launcher/compat/UserPresentLoadingAnimation;->mWorkspaceLoadingView:Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/miui/home/launcher/compat/UserPresentLoadingAnimation;->mLoadingAnimation:Landroid/widget/ImageView;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/miui/home/launcher/compat/UserPresentLoadingAnimation;->mIsLoading:Z

    :cond_0
    invoke-virtual {p0}, Lcom/miui/home/launcher/compat/UserPresentLoadingAnimation;->showAnimation()V

    return-void
.end method

.method public showLoading()V
    .locals 4

    iget-boolean v0, p0, Lcom/miui/home/launcher/compat/UserPresentLoadingAnimation;->mIsLoading:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/miui/home/launcher/compat/UserPresentLoadingAnimation;->mLauncher:Lcom/miui/home/launcher/Launcher;

    invoke-virtual {v0}, Lcom/miui/home/launcher/Launcher;->getDragLayer()Lcom/miui/home/launcher/DragLayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/home/launcher/DragLayer;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/miui/home/launcher/compat/UserPresentLoadingAnimation;->mWorkspaceLoadingView:Landroid/widget/FrameLayout;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/miui/home/launcher/compat/UserPresentLoadingAnimation;->mLauncher:Lcom/miui/home/launcher/Launcher;

    invoke-virtual {v0}, Lcom/miui/home/launcher/Launcher;->getShortcutMenuLayer()Lcom/miui/home/launcher/ShortcutMenuLayer;

    move-result-object v0

    if-eqz v0, :cond_2

    const-string v0, "Launcher.UserPresentAnimation"

    const-string v1, " show loading "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/miui/home/launcher/compat/UserPresentLoadingAnimation;->mLauncher:Lcom/miui/home/launcher/Launcher;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0d019d

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/miui/home/launcher/compat/UserPresentLoadingAnimation;->mWorkspaceLoadingView:Landroid/widget/FrameLayout;

    iget-object v0, p0, Lcom/miui/home/launcher/compat/UserPresentLoadingAnimation;->mWorkspaceLoadingView:Landroid/widget/FrameLayout;

    const v1, 0x7f0a0202

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/miui/home/launcher/compat/UserPresentLoadingAnimation;->mLoadingAnimation:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/miui/home/launcher/compat/UserPresentLoadingAnimation;->mLauncher:Lcom/miui/home/launcher/Launcher;

    invoke-virtual {v0}, Lcom/miui/home/launcher/Launcher;->getShortcutMenuLayer()Lcom/miui/home/launcher/ShortcutMenuLayer;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/home/launcher/compat/UserPresentLoadingAnimation;->mWorkspaceLoadingView:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Lcom/miui/home/launcher/ShortcutMenuLayer;->addView(Landroid/view/View;)V

    invoke-static {}, Lcom/miui/home/launcher/WallpaperUtils;->getCurrentWallpaperColorMode()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/miui/home/launcher/compat/UserPresentLoadingAnimation;->mLauncher:Lcom/miui/home/launcher/Launcher;

    invoke-virtual {v0}, Lcom/miui/home/launcher/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0806fc

    invoke-virtual {v0, v1, v3}, Landroid/content/res/Resources;->getDrawable(ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/miui/home/launcher/compat/UserPresentLoadingAnimation;->mLauncher:Lcom/miui/home/launcher/Launcher;

    invoke-virtual {v0}, Lcom/miui/home/launcher/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0806fb

    invoke-virtual {v0, v1, v3}, Landroid/content/res/Resources;->getDrawable(ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_0
    instance-of v1, v0, Landroid/graphics/drawable/Animatable;

    if-eqz v1, :cond_1

    move-object v1, v0

    check-cast v1, Landroid/graphics/drawable/Animatable;

    invoke-interface {v1}, Landroid/graphics/drawable/Animatable;->start()V

    :cond_1
    iget-object v1, p0, Lcom/miui/home/launcher/compat/UserPresentLoadingAnimation;->mLoadingAnimation:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/home/launcher/compat/UserPresentLoadingAnimation;->mIsLoading:Z

    :cond_2
    return-void
.end method

.method showUserPresentAnimation(Landroid/view/View;)V
    .locals 8

    goto/32 :goto_50

    nop

    :goto_0
    const v5, 0x3e19999a    # 0.15f

    goto/32 :goto_2a

    nop

    :goto_1
    invoke-virtual {p1, v0}, Landroid/view/View;->setScaleX(F)V

    goto/32 :goto_5f

    nop

    :goto_2
    invoke-virtual {p1, v0, v1}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    goto/32 :goto_1d

    nop

    :goto_3
    const/high16 v0, 0x3f800000    # 1.0f

    goto/32 :goto_53

    nop

    :goto_4
    invoke-virtual {p1}, Landroid/view/ViewPropertyAnimator;->start()V

    goto/32 :goto_29

    nop

    :goto_5
    add-int/lit8 v1, v1, -0x32

    goto/32 :goto_26

    nop

    :goto_6
    int-to-float v5, v5

    goto/32 :goto_3c

    nop

    :goto_7
    move-object v5, p1

    goto/32 :goto_f

    nop

    :goto_8
    sub-int/2addr v4, v1

    goto/32 :goto_3d

    nop

    :goto_9
    add-float/2addr v2, v5

    goto/32 :goto_33

    nop

    :goto_a
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v4

    goto/32 :goto_35

    nop

    :goto_b
    iget v5, p0, Lcom/miui/home/launcher/compat/UserPresentLoadingAnimation;->mScreenDiagonalDistance:I

    goto/32 :goto_25

    nop

    :goto_c
    const/4 v3, 0x0

    goto/32 :goto_17

    nop

    :goto_d
    goto/16 :goto_2d

    :goto_e
    goto/32 :goto_16

    nop

    :goto_f
    check-cast v5, Lcom/miui/home/launcher/interfaces/PresentAnimationResettable;

    goto/32 :goto_37

    nop

    :goto_10
    invoke-static {v0, v1}, Lcom/miui/home/launcher/common/Utilities;->calcDistance(FF)I

    move-result v0

    goto/32 :goto_47

    nop

    :goto_11
    if-nez v5, :cond_0

    goto/32 :goto_55

    :cond_0
    goto/32 :goto_7

    nop

    :goto_12
    mul-int/lit16 v1, v0, 0x1f4

    goto/32 :goto_b

    nop

    :goto_13
    div-float/2addr v4, v7

    :goto_14
    goto/32 :goto_28

    nop

    :goto_15
    move v2, v6

    goto/32 :goto_d

    nop

    :goto_16
    neg-int v2, v2

    goto/32 :goto_39

    nop

    :goto_17
    aget v2, v2, v3

    goto/32 :goto_a

    nop

    :goto_18
    move v5, v6

    goto/32 :goto_21

    nop

    :goto_19
    if-nez v5, :cond_1

    goto/32 :goto_22

    :cond_1
    goto/32 :goto_18

    nop

    :goto_1a
    mul-float/2addr v7, v5

    goto/32 :goto_13

    nop

    :goto_1b
    invoke-static {}, Lcom/miui/home/launcher/DeviceConfig;->getScreenWidth()I

    move-result v0

    goto/32 :goto_5b

    nop

    :goto_1c
    add-int/2addr v2, v4

    goto/32 :goto_32

    nop

    :goto_1d
    invoke-virtual {p1, v5}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    goto/32 :goto_1e

    nop

    :goto_1e
    invoke-virtual {p1, v6}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    goto/32 :goto_3

    nop

    :goto_1f
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    goto/32 :goto_62

    nop

    :goto_20
    invoke-virtual {p1}, Landroid/view/View;->getTranslationX()F

    move-result v5

    goto/32 :goto_54

    nop

    :goto_21
    goto/16 :goto_52

    :goto_22
    goto/32 :goto_20

    nop

    :goto_23
    const-wide/16 v0, 0xfa

    goto/32 :goto_2

    nop

    :goto_24
    if-eqz v0, :cond_2

    goto/32 :goto_48

    :cond_2
    goto/32 :goto_42

    nop

    :goto_25
    div-int/2addr v1, v5

    goto/32 :goto_4e

    nop

    :goto_26
    invoke-static {v3, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto/32 :goto_1f

    nop

    :goto_27
    const v5, 0x3f19999a    # 0.6f

    goto/32 :goto_5c

    nop

    :goto_28
    instance-of v5, p1, Lcom/miui/home/launcher/interfaces/PresentAnimationResettable;

    goto/32 :goto_11

    nop

    :goto_29
    return-void

    :goto_2a
    const/4 v6, 0x0

    goto/32 :goto_4f

    nop

    :goto_2b
    if-eqz v1, :cond_3

    goto/32 :goto_44

    :cond_3
    goto/32 :goto_57

    nop

    :goto_2c
    div-float/2addr v2, v7

    :goto_2d
    goto/32 :goto_2b

    nop

    :goto_2e
    invoke-virtual {p1, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    goto/32 :goto_3e

    nop

    :goto_2f
    iget-object v2, p0, Lcom/miui/home/launcher/compat/UserPresentLoadingAnimation;->mTmpLocation:[I

    goto/32 :goto_c

    nop

    :goto_30
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v5

    goto/32 :goto_49

    nop

    :goto_31
    sub-int/2addr v2, v0

    goto/32 :goto_8

    nop

    :goto_32
    iget-object v4, p0, Lcom/miui/home/launcher/compat/UserPresentLoadingAnimation;->mTmpLocation:[I

    goto/32 :goto_40

    nop

    :goto_33
    invoke-virtual {p1, v2}, Landroid/view/View;->setTranslationX(F)V

    goto/32 :goto_5d

    nop

    :goto_34
    neg-int v4, v4

    goto/32 :goto_41

    nop

    :goto_35
    div-int/lit8 v4, v4, 0x2

    goto/32 :goto_1c

    nop

    :goto_36
    int-to-float v1, v4

    goto/32 :goto_61

    nop

    :goto_37
    invoke-interface {v5}, Lcom/miui/home/launcher/interfaces/PresentAnimationResettable;->needReset()Z

    move-result v5

    goto/32 :goto_19

    nop

    :goto_38
    invoke-static {}, Lcom/miui/home/launcher/DeviceConfig;->getScreenHeight()I

    move-result v1

    goto/32 :goto_3f

    nop

    :goto_39
    int-to-float v2, v2

    goto/32 :goto_56

    nop

    :goto_3a
    invoke-virtual {p1, v0}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    goto/32 :goto_4d

    nop

    :goto_3b
    invoke-virtual {p1, v0, v1}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    goto/32 :goto_23

    nop

    :goto_3c
    div-float/2addr v0, v5

    goto/32 :goto_27

    nop

    :goto_3d
    int-to-float v0, v2

    goto/32 :goto_36

    nop

    :goto_3e
    int-to-long v0, v0

    goto/32 :goto_3b

    nop

    :goto_3f
    int-to-float v1, v1

    goto/32 :goto_10

    nop

    :goto_40
    const/4 v5, 0x1

    goto/32 :goto_5e

    nop

    :goto_41
    int-to-float v4, v4

    goto/32 :goto_59

    nop

    :goto_42
    invoke-static {}, Lcom/miui/home/launcher/DeviceConfig;->getScreenWidth()I

    move-result v0

    goto/32 :goto_60

    nop

    :goto_43
    goto/16 :goto_14

    :goto_44
    goto/32 :goto_34

    nop

    :goto_45
    invoke-virtual {p1, v2}, Landroid/view/View;->getLocationInWindow([I)V

    goto/32 :goto_2f

    nop

    :goto_46
    invoke-virtual {p1, v0}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    goto/32 :goto_3a

    nop

    :goto_47
    iput v0, p0, Lcom/miui/home/launcher/compat/UserPresentLoadingAnimation;->mScreenDiagonalDistance:I

    :goto_48
    goto/32 :goto_1b

    nop

    :goto_49
    div-int/lit8 v5, v5, 0x2

    goto/32 :goto_4a

    nop

    :goto_4a
    add-int/2addr v4, v5

    goto/32 :goto_31

    nop

    :goto_4b
    iget-object v2, p0, Lcom/miui/home/launcher/compat/UserPresentLoadingAnimation;->mTmpLocation:[I

    goto/32 :goto_45

    nop

    :goto_4c
    invoke-static {}, Lcom/miui/home/launcher/DeviceConfig;->getScreenHeight()I

    move-result v1

    goto/32 :goto_5a

    nop

    :goto_4d
    iget-object v0, p0, Lcom/miui/home/launcher/compat/UserPresentLoadingAnimation;->mAnimatorListenerAdapter:Landroid/animation/AnimatorListenerAdapter;

    goto/32 :goto_63

    nop

    :goto_4e
    int-to-float v0, v0

    goto/32 :goto_6

    nop

    :goto_4f
    if-eqz v1, :cond_4

    goto/32 :goto_e

    :cond_4
    goto/32 :goto_15

    nop

    :goto_50
    iget v0, p0, Lcom/miui/home/launcher/compat/UserPresentLoadingAnimation;->mScreenDiagonalDistance:I

    goto/32 :goto_24

    nop

    :goto_51
    invoke-virtual {p1}, Landroid/view/View;->getTranslationX()F

    move-result v5

    :goto_52
    goto/32 :goto_9

    nop

    :goto_53
    invoke-virtual {p1, v0}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    goto/32 :goto_46

    nop

    :goto_54
    goto :goto_52

    :goto_55
    goto/32 :goto_51

    nop

    :goto_56
    int-to-float v7, v1

    goto/32 :goto_58

    nop

    :goto_57
    move v4, v6

    goto/32 :goto_43

    nop

    :goto_58
    mul-float/2addr v7, v5

    goto/32 :goto_2c

    nop

    :goto_59
    int-to-float v7, v1

    goto/32 :goto_1a

    nop

    :goto_5a
    div-int/lit8 v1, v1, 0x2

    goto/32 :goto_4b

    nop

    :goto_5b
    div-int/lit8 v0, v0, 0x2

    goto/32 :goto_4c

    nop

    :goto_5c
    add-float/2addr v0, v5

    goto/32 :goto_0

    nop

    :goto_5d
    invoke-virtual {p1, v4}, Landroid/view/View;->setTranslationY(F)V

    goto/32 :goto_1

    nop

    :goto_5e
    aget v4, v4, v5

    goto/32 :goto_30

    nop

    :goto_5f
    invoke-virtual {p1, v0}, Landroid/view/View;->setScaleY(F)V

    goto/32 :goto_5

    nop

    :goto_60
    int-to-float v0, v0

    goto/32 :goto_38

    nop

    :goto_61
    invoke-static {v0, v1}, Lcom/miui/home/launcher/common/Utilities;->calcDistance(FF)I

    move-result v0

    goto/32 :goto_12

    nop

    :goto_62
    sget-object v1, Lcom/miui/home/launcher/compat/UserPresentLoadingAnimation$BackEaseOutInterpolator;->sInstance:Lcom/miui/home/launcher/compat/UserPresentLoadingAnimation$BackEaseOutInterpolator;

    goto/32 :goto_2e

    nop

    :goto_63
    invoke-virtual {p1, v0}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    goto/32 :goto_4

    nop
.end method
