.class public final Lcom/miui/home/launcher/folder/FolderIcon2x2_4;
.super Lcom/miui/home/launcher/FolderIcon;

# interfaces
.implements Lcom/miui/home/launcher/folder/ListenerInfo;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nFolderIcon2x2_4.kt\nKotlin\n*S Kotlin\n*F\n+ 1 FolderIcon2x2_4.kt\ncom/miui/home/launcher/folder/FolderIcon2x2_4\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 ArraysJVM.kt\nkotlin/collections/ArraysKt__ArraysJVMKt\n*L\n1#1,442:1\n1828#2,3:443\n1828#2,3:448\n37#3,2:446\n*E\n*S KotlinDebug\n*F\n+ 1 FolderIcon2x2_4.kt\ncom/miui/home/launcher/folder/FolderIcon2x2_4\n*L\n88#1,3:443\n368#1,3:448\n245#1,2:446\n*E\n"
.end annotation


# instance fields
.field private _$_findViewCache:Ljava/util/HashMap;

.field private final mDefaultFolderBackground$delegate:Lkotlin/Lazy;

.field private mFolderCover:Landroid/widget/ImageView;

.field private mImageView:Lcom/miui/home/launcher/LauncherIconImageView;

.field private mPreViewLastRefreshTime:J

.field private mPreviewContainer:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

.field private mScreenRefreshRate:I

.field private final sTmpCanvas:Landroid/graphics/Canvas;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attr"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2}, Lcom/miui/home/launcher/FolderIcon;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance p1, Lcom/miui/home/launcher/folder/FolderIcon2x2_4$mDefaultFolderBackground$2;

    invoke-direct {p1, p0}, Lcom/miui/home/launcher/folder/FolderIcon2x2_4$mDefaultFolderBackground$2;-><init>(Lcom/miui/home/launcher/folder/FolderIcon2x2_4;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mDefaultFolderBackground$delegate:Lkotlin/Lazy;

    const/16 p1, 0x8

    iput p1, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mScreenRefreshRate:I

    new-instance p1, Landroid/graphics/Canvas;

    invoke-direct {p1}, Landroid/graphics/Canvas;-><init>()V

    iput-object p1, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->sTmpCanvas:Landroid/graphics/Canvas;

    invoke-static {}, Lcom/miui/home/launcher/Application;->getLauncherApplication()Lcom/miui/home/launcher/Application;

    move-result-object p1

    const-string p2, "Application.getLauncherApplication()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/miui/home/launcher/Application;->getIconCache()Lcom/miui/home/launcher/IconCache;

    move-result-object p1

    iput-object p1, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mIconCache:Lcom/miui/home/launcher/IconCache;

    invoke-static {}, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->access$getSLayerPaint$p$s1165083175()Landroid/graphics/Paint;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->setLayerPaint(Landroid/graphics/Paint;)V

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->enableDrawTouchMask(Z)V

    return-void
.end method

.method public static final synthetic access$getMInfo$p(Lcom/miui/home/launcher/folder/FolderIcon2x2_4;)Lcom/miui/home/launcher/FolderInfo;
    .locals 0

    iget-object p0, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mInfo:Lcom/miui/home/launcher/FolderInfo;

    return-object p0
.end method

.method public static final synthetic access$getMLauncher$p(Lcom/miui/home/launcher/folder/FolderIcon2x2_4;)Lcom/miui/home/launcher/Launcher;
    .locals 0

    iget-object p0, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mLauncher:Lcom/miui/home/launcher/Launcher;

    return-object p0
.end method

.method public static final synthetic access$getMPreViewLastRefreshTime$p(Lcom/miui/home/launcher/folder/FolderIcon2x2_4;)J
    .locals 2

    iget-wide v0, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mPreViewLastRefreshTime:J

    return-wide v0
.end method

.method public static final synthetic access$getMPreviewContainer$p(Lcom/miui/home/launcher/folder/FolderIcon2x2_4;)Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;
    .locals 1

    iget-object p0, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mPreviewContainer:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    if-nez p0, :cond_0

    const-string v0, "mPreviewContainer"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getMScreenRefreshRate$p(Lcom/miui/home/launcher/folder/FolderIcon2x2_4;)I
    .locals 0

    iget p0, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mScreenRefreshRate:I

    return p0
.end method

.method public static final synthetic access$getSLayerPaint$p$s1165083175()Landroid/graphics/Paint;
    .locals 1

    sget-object v0, Lcom/miui/home/launcher/FolderIcon;->sLayerPaint:Landroid/graphics/Paint;

    return-object v0
.end method

.method public static final synthetic access$setMPreViewLastRefreshTime$p(Lcom/miui/home/launcher/folder/FolderIcon2x2_4;J)V
    .locals 0

    iput-wide p1, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mPreViewLastRefreshTime:J

    return-void
.end method

.method private final adjustsAllIconContainerNotClipChildren()V
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->setClipChildren(Z)V

    sget v1, Lcom/miui/home/R$id;->icon_container:I

    invoke-virtual {p0, v1}, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->_$_findCachedViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    const-string v2, "icon_container"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-eqz v1, :cond_7

    check-cast v1, Landroid/view/ViewGroup;

    if-eqz v1, :cond_0

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setClipChildren(Z)V

    :cond_0
    sget v1, Lcom/miui/home/R$id;->icon_container:I

    invoke-virtual {p0, v1}, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->_$_findCachedViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    const-string v2, "icon_container"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-eqz v1, :cond_6

    check-cast v1, Landroid/view/ViewGroup;

    if-eqz v1, :cond_1

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setClipToPadding(Z)V

    :cond_1
    sget v1, Lcom/miui/home/R$id;->icon_container:I

    invoke-virtual {p0, v1}, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->_$_findCachedViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    if-eqz v1, :cond_2

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setClipChildren(Z)V

    :cond_2
    sget v1, Lcom/miui/home/R$id;->icon_container:I

    invoke-virtual {p0, v1}, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->_$_findCachedViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    if-eqz v1, :cond_3

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setClipToPadding(Z)V

    :cond_3
    iget-object v1, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mPreviewContainer:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    if-nez v1, :cond_4

    const-string v2, "mPreviewContainer"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    if-eqz v1, :cond_5

    invoke-virtual {v1, v0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->setClipChildren(Z)V

    :cond_5
    return-void

    :cond_6
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type android.view.ViewGroup"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_7
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type android.view.ViewGroup"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private final createOrRemoveView()V
    .locals 6

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mInfo:Lcom/miui/home/launcher/FolderInfo;

    invoke-virtual {v0}, Lcom/miui/home/launcher/FolderInfo;->count()I

    move-result v0

    sget v1, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->ITEMS_MAX_COUNT_2X2:I

    invoke-static {v0, v1}, Lkotlin/ranges/RangesKt;->coerceAtMost(II)I

    move-result v0

    iget-object v1, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mPreviewContainer:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    if-nez v1, :cond_0

    const-string v2, "mPreviewContainer"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v1}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->getMRealPvChildCount()I

    move-result v1

    sub-int v1, v0, v1

    if-lez v1, :cond_3

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_7

    iget-object v2, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mPreviewContainer:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    if-nez v2, :cond_1

    const-string v3, "mPreviewContainer"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    if-eqz v2, :cond_2

    new-instance v3, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "context"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v3, v4}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;-><init>(Landroid/content/Context;)V

    check-cast v3, Landroid/view/View;

    invoke-virtual {v2, v3}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->addPreView(Landroid/view/View;)V

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    if-gez v1, :cond_7

    :cond_4
    :goto_1
    iget-object v1, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mPreviewContainer:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    if-nez v1, :cond_5

    const-string v2, "mPreviewContainer"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {v1}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->getMRealPvChildCount()I

    move-result v1

    sub-int v1, v0, v1

    if-gez v1, :cond_7

    iget-object v1, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mPreviewContainer:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    if-nez v1, :cond_6

    const-string v2, "mPreviewContainer"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->removeLastPreView()V

    goto :goto_1

    :cond_7
    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->addItemOnclickListener()V

    return-void
.end method

.method private final getMDefaultFolderBackground()Lcom/miui/home/launcher/folder/FolderIcon4x4NormalBackgroundDrawable;
    .locals 1

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mDefaultFolderBackground$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/home/launcher/folder/FolderIcon4x4NormalBackgroundDrawable;

    return-object v0
.end method


# virtual methods
.method public _$_findCachedViewById(I)Landroid/view/View;
    .locals 2

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->_$_findViewCache:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->_$_findViewCache:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->_$_findViewCache:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->_$_findViewCache:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v0
.end method

.method public final addItemOnclickListener()V
    .locals 7

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mPreviewContainer:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    if-nez v0, :cond_0

    const-string v1, "mPreviewContainer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    if-eqz v0, :cond_6

    invoke-virtual {v0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->getMPvChildList()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_6

    check-cast v0, Ljava/lang/Iterable;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    add-int/lit8 v4, v2, 0x1

    if-gez v2, :cond_1

    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwIndexOverflow()V

    :cond_1
    check-cast v3, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;

    const/4 v5, 0x3

    if-eqz v3, :cond_3

    if-ge v2, v5, :cond_2

    move-object v6, p0

    goto :goto_1

    :cond_2
    const/4 v6, 0x0

    :goto_1
    check-cast v6, Lcom/miui/home/launcher/folder/ListenerInfo;

    invoke-virtual {v3, v6}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;->setViewClickListener(Lcom/miui/home/launcher/folder/ListenerInfo;)V

    :cond_3
    if-eqz v3, :cond_5

    if-ge v2, v5, :cond_4

    const/4 v2, 0x1

    goto :goto_2

    :cond_4
    move v2, v1

    :goto_2
    invoke-virtual {v3, v2}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;->setClickable(Z)V

    :cond_5
    move v2, v4

    goto :goto_0

    :cond_6
    return-void
.end method

.method protected drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
    .locals 3

    const-string v0, "child"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->isDrawingInThumbnailView()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mTitleContainer:Lcom/miui/home/launcher/ItemIconTitleContainer;

    if-ne p2, v0, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->isDrawingInThumbnailView()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mInfo:Lcom/miui/home/launcher/FolderInfo;

    const-string v1, "mInfo"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/miui/home/launcher/FolderInfo;->isOpened()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mIconContainer:Landroid/widget/FrameLayout;

    if-ne p2, v0, :cond_4

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mPreviewContainer:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    if-nez v0, :cond_1

    const-string v1, "mPreviewContainer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->getAlpha()F

    move-result v0

    iget-object v1, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mPreviewContainer:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    if-nez v1, :cond_2

    const-string v2, "mPreviewContainer"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->setAlpha(F)V

    invoke-super {p0, p1, p2, p3, p4}, Lcom/miui/home/launcher/FolderIcon;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result p1

    iget-object p2, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mPreviewContainer:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    if-nez p2, :cond_3

    const-string p3, "mPreviewContainer"

    invoke-static {p3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {p2, v0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->setAlpha(F)V

    goto :goto_0

    :cond_4
    invoke-super {p0, p1, p2, p3, p4}, Lcom/miui/home/launcher/FolderIcon;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result p1

    :goto_0
    return p1
.end method

.method protected dropIconIntoFolderIcon(Lcom/miui/home/launcher/DragObject;)V
    .locals 9

    const-string v0, "d"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mPreviewContainer:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    if-nez v0, :cond_0

    const-string v1, "mPreviewContainer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/4 v1, 0x0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->isPreViewContainerOverload()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    :cond_1
    move-object v0, v1

    :goto_0
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    new-instance v2, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "context"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v2, v3}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;-><init>(Landroid/content/Context;)V

    iget-object v3, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mPreviewContainer:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    if-nez v3, :cond_2

    const-string v4, "mPreviewContainer"

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    if-eqz v3, :cond_3

    move-object v4, v2

    check-cast v4, Landroid/view/View;

    invoke-virtual {v3, v4}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->addPreView(Landroid/view/View;)V

    :cond_3
    iget-object v3, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mPreviewContainer:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    if-nez v3, :cond_4

    const-string v4, "mPreviewContainer"

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    if-eqz v3, :cond_5

    invoke-virtual {v3}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->getLastVisibleView()Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;

    move-result-object v3

    if-eqz v3, :cond_5

    move-object v2, v3

    :cond_5
    iget-object v3, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mPreviewContainer:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    if-nez v3, :cond_6

    const-string v4, "mPreviewContainer"

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    const/4 v4, 0x1

    if-eqz v3, :cond_7

    invoke-virtual {v3, v4}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->getPreAddItemInfo(Z)Lcom/miui/home/launcher/folder/FolderIconPreviewInfo;

    move-result-object v1

    :cond_7
    const/4 v3, 0x0

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/miui/home/launcher/folder/FolderIconPreviewInfo;->getWidth()I

    move-result v1

    goto :goto_1

    :cond_8
    move v1, v3

    :goto_1
    invoke-virtual {p1}, Lcom/miui/home/launcher/DragObject;->getDragView()Lcom/miui/home/launcher/DragView;

    move-result-object v5

    const-string v6, "dragView"

    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v5}, Lcom/miui/home/launcher/DragView;->getContent()Landroid/view/View;

    move-result-object v6

    const-string v7, "dragView.content"

    invoke-static {v6, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v6}, Landroid/view/View;->getWidth()I

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-virtual {v5}, Lcom/miui/home/launcher/DragView;->getContent()Landroid/view/View;

    move-result-object v7

    instance-of v7, v7, Lcom/miui/home/launcher/ShortcutIcon;

    invoke-virtual {v5, v7}, Lcom/miui/home/launcher/DragView;->setFlagBigFolderAnimStyle(Z)V

    invoke-virtual {v5}, Lcom/miui/home/launcher/DragView;->getContent()Landroid/view/View;

    move-result-object v7

    instance-of v8, v7, Lcom/miui/home/launcher/ShortcutIcon;

    if-eqz v8, :cond_9

    check-cast v7, Lcom/miui/home/launcher/ShortcutIcon;

    invoke-virtual {v7}, Lcom/miui/home/launcher/ShortcutIcon;->getIconImageView()Lcom/miui/home/launcher/LauncherIconImageView;

    move-result-object v7

    if-eqz v7, :cond_9

    invoke-virtual {v7}, Lcom/miui/home/launcher/LauncherIconImageView;->getWidth()I

    move-result v6

    int-to-float v6, v6

    int-to-float v1, v1

    div-float v6, v1, v6

    sget-object v1, Lcom/miui/home/launcher/folder/BigFolderConfig;->Companion:Lcom/miui/home/launcher/folder/BigFolderConfig$Companion;

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v1, v7}, Lcom/miui/home/launcher/folder/BigFolderConfig$Companion;->bigFolderPaddingTop(Landroid/content/Context;)I

    move-result v1

    goto :goto_2

    :cond_9
    move v1, v3

    :goto_2
    invoke-virtual {v5, v3, v1}, Lcom/miui/home/launcher/DragView;->setDragVisualizeOffset(II)V

    invoke-virtual {v5, v6}, Lcom/miui/home/launcher/DragView;->setTargetScale(F)V

    if-eqz v0, :cond_a

    invoke-virtual {v5}, Lcom/miui/home/launcher/DragView;->setFakeTargetMode()V

    invoke-virtual {v5}, Lcom/miui/home/launcher/DragView;->setFadeoutAnimationMode()V

    :cond_a
    const/4 v0, 0x0

    invoke-virtual {v5, v0}, Lcom/miui/home/launcher/DragView;->setPivotX(F)V

    invoke-virtual {v5, v0}, Lcom/miui/home/launcher/DragView;->setPivotY(F)V

    check-cast v2, Landroid/view/View;

    invoke-virtual {v5, v2}, Lcom/miui/home/launcher/DragView;->setAnimateTarget(Landroid/view/View;)Z

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mInfo:Lcom/miui/home/launcher/FolderInfo;

    invoke-virtual {p1}, Lcom/miui/home/launcher/DragObject;->getDragInfo()Lcom/miui/home/launcher/ItemInfo;

    move-result-object v1

    if-eqz v1, :cond_c

    check-cast v1, Lcom/miui/home/launcher/ShortcutInfo;

    iget p1, p1, Lcom/miui/home/launcher/DragObject;->dropAction:I

    const/4 v2, 0x3

    if-eq p1, v2, :cond_b

    move v3, v4

    :cond_b
    iget-object p1, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mLauncher:Lcom/miui/home/launcher/Launcher;

    const-string v2, "mLauncher"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/miui/home/launcher/Launcher;->getLauncherMode()Lcom/miui/home/launcher/allapps/LauncherMode;

    move-result-object p1

    invoke-virtual {v0, v1, v3, p1}, Lcom/miui/home/launcher/FolderInfo;->add(Lcom/miui/home/launcher/ShortcutInfo;ZLcom/miui/home/launcher/allapps/LauncherMode;)V

    new-instance p1, Lcom/miui/home/launcher/folder/FolderIcon2x2_4$dropIconIntoFolderIcon$3;

    invoke-direct {p1, p0}, Lcom/miui/home/launcher/folder/FolderIcon2x2_4$dropIconIntoFolderIcon$3;-><init>(Lcom/miui/home/launcher/folder/FolderIcon2x2_4;)V

    check-cast p1, Ljava/lang/Runnable;

    invoke-virtual {v5, p1}, Lcom/miui/home/launcher/DragView;->setOnAnimationEndCallback(Ljava/lang/Runnable;)V

    return-void

    :cond_c
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.miui.home.launcher.ShortcutInfo"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public getBackAnimPreviewDrawable()Landroid/graphics/drawable/Drawable;
    .locals 9

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mIconImageView:Lcom/miui/home/launcher/LauncherIconImageView;

    const-string v1, "mIconImageView"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/miui/home/launcher/LauncherIconImageView;->getWidth()I

    move-result v0

    int-to-float v0, v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iget-object v1, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mIconImageView:Lcom/miui/home/launcher/LauncherIconImageView;

    const-string v2, "mIconImageView"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/miui/home/launcher/LauncherIconImageView;->getHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Lcom/miui/home/launcher/common/Utilities;->createBitmapSafely(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_3

    iget-object v2, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->sTmpCanvas:Landroid/graphics/Canvas;

    invoke-virtual {v2}, Landroid/graphics/Canvas;->save()I

    move-result v3

    invoke-virtual {v2, v0}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    iget-object v4, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mIconImageView:Lcom/miui/home/launcher/LauncherIconImageView;

    const-string v5, "mIconImageView"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/miui/home/launcher/LauncherIconImageView;->getWidth()I

    move-result v4

    iget-object v5, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mPreviewContainer:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    if-nez v5, :cond_0

    const-string v6, "mPreviewContainer"

    invoke-static {v6}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v5}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->getWidth()I

    move-result v5

    sub-int/2addr v4, v5

    int-to-float v4, v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    iget-object v6, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mIconImageView:Lcom/miui/home/launcher/LauncherIconImageView;

    const-string v7, "mIconImageView"

    invoke-static {v6, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v6}, Lcom/miui/home/launcher/LauncherIconImageView;->getHeight()I

    move-result v6

    iget-object v7, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mPreviewContainer:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    if-nez v7, :cond_1

    const-string v8, "mPreviewContainer"

    invoke-static {v8}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v7}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->getHeight()I

    move-result v7

    sub-int/2addr v6, v7

    int-to-float v6, v6

    div-float/2addr v6, v5

    invoke-virtual {v2, v4, v6}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v4, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mPreviewContainer:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    if-nez v4, :cond_2

    const-string v5, "mPreviewContainer"

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v4, v2}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {v2, v1}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    invoke-virtual {v2, v3}, Landroid/graphics/Canvas;->restoreToCount(I)V

    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    check-cast v1, Landroid/graphics/drawable/Drawable;

    return-object v1

    :cond_3
    return-object v1
.end method

.method public final getCover()Landroid/widget/ImageView;
    .locals 2

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mFolderCover:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    const-string v1, "mFolderCover"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public getFolderBackground()Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mFolderBackground:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getPreviewArray()[Landroid/widget/ImageView;
    .locals 2

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mPreviewContainer:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    if-nez v0, :cond_0

    const-string v1, "mPreviewContainer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->getMPvChildList()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    const/4 v1, 0x0

    new-array v1, v1, [Landroid/widget/ImageView;

    invoke-interface {v0, v1}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    check-cast v0, [Landroid/widget/ImageView;

    return-object v0

    :cond_1
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "null cannot be cast to non-null type kotlin.Array<T>"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final getPreviewByComponentName(Landroid/content/ComponentName;IJ)Lcom/miui/home/launcher/anim/LaunchAppAndBackHomeAnimTarget;
    .locals 10

    const/4 v0, 0x0

    move-object v1, v0

    check-cast v1, Lcom/miui/home/launcher/anim/LaunchAppAndBackHomeAnimTarget;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    check-cast v2, Ljava/util/List;

    const/4 v3, 0x0

    if-eqz p1, :cond_7

    iget-object v4, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mInfo:Lcom/miui/home/launcher/FolderInfo;

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mInfo:Lcom/miui/home/launcher/FolderInfo;

    invoke-virtual {v4}, Lcom/miui/home/launcher/FolderInfo;->count()I

    move-result v4

    if-gtz v4, :cond_0

    return-object v0

    :cond_0
    iget-object v4, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mInfo:Lcom/miui/home/launcher/FolderInfo;

    invoke-virtual {v4}, Lcom/miui/home/launcher/FolderInfo;->count()I

    move-result v4

    move-object v5, v1

    move v1, v3

    :goto_0
    if-ge v1, v4, :cond_6

    iget-object v6, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mPreviewContainer:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    if-nez v6, :cond_1

    const-string v7, "mPreviewContainer"

    invoke-static {v7}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v6, v1}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->getPreviewIconInfo(I)Lcom/miui/home/launcher/ShortcutInfo;

    move-result-object v6

    if-eqz v6, :cond_5

    invoke-virtual {v6}, Lcom/miui/home/launcher/ShortcutInfo;->getComponentName()Landroid/content/ComponentName;

    move-result-object v7

    if-eqz v7, :cond_5

    invoke-virtual {v7}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    invoke-virtual {v6}, Lcom/miui/home/launcher/ShortcutInfo;->getUserId()I

    move-result v8

    if-ne v8, p2, :cond_5

    iget-wide v8, v6, Lcom/miui/home/launcher/ShortcutInfo;->id:J

    cmp-long v6, v8, p3

    if-nez v6, :cond_5

    iget-object v6, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mPreviewContainer:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    if-nez v6, :cond_2

    const-string v8, "mPreviewContainer"

    invoke-static {v8}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v6}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->getMPvChildList()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-lt v6, v1, :cond_5

    iget-object v6, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mPreviewContainer:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    if-nez v6, :cond_3

    const-string v8, "mPreviewContainer"

    invoke-static {v8}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {v6}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->getMPvChildList()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v7}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    iget-object v5, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mPreviewContainer:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    if-nez v5, :cond_4

    const-string v6, "mPreviewContainer"

    invoke-static {v6}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {v5}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->getMPvChildList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/miui/home/launcher/anim/LaunchAppAndBackHomeAnimTarget;

    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_6
    move-object v1, v5

    :cond_7
    if-nez v1, :cond_9

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result p1

    if-lez p1, :cond_8

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    move-object v0, p1

    check-cast v0, Lcom/miui/home/launcher/anim/LaunchAppAndBackHomeAnimTarget;

    :cond_8
    move-object v1, v0

    :cond_9
    return-object v1
.end method

.method public getPreviewContainerSnapshot()Landroid/graphics/Bitmap;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getPreviewCount()I
    .locals 2

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mPreviewContainer:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    if-nez v0, :cond_0

    const-string v1, "mPreviewContainer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->getMPvChildList()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getPreviewIconHeight()F
    .locals 2

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mPreviewContainer:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    if-nez v0, :cond_0

    const-string v1, "mPreviewContainer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->getMPvChildList()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mPreviewContainer:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    if-nez v0, :cond_1

    const-string v1, "mPreviewContainer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->getMPvChildList()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mPreviewContainer:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    if-nez v0, :cond_2

    const-string v1, "mPreviewContainer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->getMPvChildList()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;

    invoke-virtual {v0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;->getHeight()I

    move-result v0

    int-to-float v0, v0

    goto :goto_0

    :cond_3
    const/4 v0, -0x1

    int-to-float v0, v0

    :goto_0
    return v0
.end method

.method public getPreviewItems()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mPreviewContainer:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    if-nez v0, :cond_0

    const-string v1, "mPreviewContainer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->getMPvChildList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getPreviewPosition(Landroid/graphics/Rect;)F
    .locals 10

    const-string v0, "rect"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mTmpPos:[F

    const/4 v1, 0x1

    const/4 v2, 0x0

    aput v2, v0, v1

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mTmpPos:[F

    iget-object v2, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mTmpPos:[F

    aget v2, v2, v1

    const/4 v3, 0x0

    aput v2, v0, v3

    sget v0, Lcom/miui/home/R$id;->preview_icons_container:I

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->_$_findCachedViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    invoke-virtual {v0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->getSmallItemsRectF()Landroid/graphics/RectF;

    move-result-object v0

    sget v2, Lcom/miui/home/R$id;->preview_icons_container:I

    invoke-virtual {p0, v2}, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->_$_findCachedViewById(I)Landroid/view/View;

    move-result-object v2

    move-object v4, v2

    check-cast v4, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    iget-object v2, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mLauncher:Lcom/miui/home/launcher/Launcher;

    const-string v5, "mLauncher"

    invoke-static {v2, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/miui/home/launcher/Launcher;->getDragLayer()Lcom/miui/home/launcher/DragLayer;

    move-result-object v2

    move-object v5, v2

    check-cast v5, Landroid/view/View;

    iget-object v6, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mTmpPos:[F

    new-instance v2, Lcom/miui/home/launcher/folder/FolderIcon2x2_4$getPreviewPosition$scale$1;

    invoke-direct {v2, p0}, Lcom/miui/home/launcher/folder/FolderIcon2x2_4$getPreviewPosition$scale$1;-><init>(Lcom/miui/home/launcher/folder/FolderIcon2x2_4;)V

    move-object v9, v2

    check-cast v9, Ljava/util/function/Predicate;

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-static/range {v4 .. v9}, Lcom/miui/home/launcher/common/Utilities;->getDescendantCoordRelativeToAncestor(Landroid/view/View;Landroid/view/View;[FZZLjava/util/function/Predicate;)F

    move-result v2

    iget-object v4, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mTmpPos:[F

    aget v4, v4, v3

    float-to-int v4, v4

    iget v5, v0, Landroid/graphics/RectF;->left:F

    float-to-int v5, v5

    add-int/2addr v4, v5

    iget-object v5, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mTmpPos:[F

    aget v5, v5, v1

    float-to-int v5, v5

    iget v6, v0, Landroid/graphics/RectF;->top:F

    float-to-int v6, v6

    add-int/2addr v5, v6

    iget-object v6, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mTmpPos:[F

    aget v3, v6, v3

    iget v6, v0, Landroid/graphics/RectF;->right:F

    float-to-int v6, v6

    int-to-float v6, v6

    mul-float/2addr v6, v2

    add-float/2addr v3, v6

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    iget-object v6, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mTmpPos:[F

    aget v1, v6, v1

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    float-to-int v0, v0

    int-to-float v0, v0

    mul-float/2addr v0, v2

    add-float/2addr v1, v0

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v0

    invoke-virtual {p1, v4, v5, v3, v0}, Landroid/graphics/Rect;->set(IIII)V

    return v2
.end method

.method protected initNoWordAdapter()Lcom/miui/home/launcher/util/noword/NoWordLauncherElementAdapter;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/miui/home/launcher/util/noword/NoWordLauncherElementAdapter<",
            "*>;"
        }
    .end annotation

    new-instance v0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4$initNoWordAdapter$1;

    move-object v1, p0

    check-cast v1, Lcom/miui/home/launcher/ItemIcon;

    invoke-direct {v0, p0, v1}, Lcom/miui/home/launcher/folder/FolderIcon2x2_4$initNoWordAdapter$1;-><init>(Lcom/miui/home/launcher/folder/FolderIcon2x2_4;Lcom/miui/home/launcher/ItemIcon;)V

    check-cast v0, Lcom/miui/home/launcher/util/noword/NoWordLauncherElementAdapter;

    return-object v0
.end method

.method public invalidatePreviews()V
    .locals 4

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mPreviewContainer:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    if-nez v0, :cond_0

    const-string v1, "mPreviewContainer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->getMPvChildList()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_2

    iget-object v2, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mPreviewContainer:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    if-nez v2, :cond_1

    const-string v3, "mPreviewContainer"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v2}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->getMPvChildList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;

    invoke-virtual {v2}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;->invalidate()V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method public loadItemIcons(Z)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->loadItemIcons(ZZ)V

    return-void
.end method

.method public loadItemIcons(ZZ)V
    .locals 6

    invoke-direct {p0}, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->createOrRemoveView()V

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mPreviewContainer:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    if-nez v0, :cond_0

    const-string p1, "mPreviewContainer"

    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    iget-object v1, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mInfo:Lcom/miui/home/launcher/FolderInfo;

    const-string p1, "mInfo"

    invoke-static {v1, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mIconCache:Lcom/miui/home/launcher/IconCache;

    const-string p1, "mIconCache"

    invoke-static {v2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mInfo:Lcom/miui/home/launcher/FolderInfo;

    const-string v3, "mInfo"

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/miui/home/launcher/FolderInfo;->isAppPredictOpen()Z

    move-result v3

    iget-object v4, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mSerialExecutor:Lcom/miui/home/library/utils/AsyncTaskExecutorHelper$SerialExecutor;

    const-string p1, "mSerialExecutor"

    invoke-static {v4, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->loadItemIcons(Lcom/miui/home/launcher/FolderInfo;Lcom/miui/home/launcher/IconCache;ZLcom/miui/home/library/utils/AsyncTaskExecutorHelper$SerialExecutor;Z)V

    iget-object p1, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mLauncher:Lcom/miui/home/launcher/Launcher;

    iget-object p2, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mInfo:Lcom/miui/home/launcher/FolderInfo;

    invoke-virtual {p1, p2}, Lcom/miui/home/launcher/Launcher;->updateFolderMessage(Lcom/miui/home/launcher/FolderInfo;)V

    return-void
.end method

.method protected needTransformedTouchPointInView(FFLandroid/view/View;Landroid/graphics/PointF;)Z
    .locals 0

    const/4 p1, 0x1

    return p1
.end method

.method public onBackAnimStart()V
    .locals 2

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mPreviewContainer:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    if-nez v0, :cond_0

    const-string v1, "mPreviewContainer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->setVisibility(I)V

    return-void
.end method

.method public onBackAnimStop()V
    .locals 2

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mPreviewContainer:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    if-nez v0, :cond_0

    const-string v1, "mPreviewContainer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->setVisibility(I)V

    return-void
.end method

.method public onDragEnter(Lcom/miui/home/launcher/DragObject;)V
    .locals 3

    const-string v0, "d"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/miui/home/launcher/DragObject;->getDragInfo()Lcom/miui/home/launcher/ItemInfo;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->isDropable(Lcom/miui/home/launcher/ItemInfo;)Z

    move-result p1

    if-eqz p1, :cond_3

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mIsDragingEnter:Z

    sget-object v0, Lcom/miui/home/launcher/folder/FolderAnimHelper;->Companion:Lcom/miui/home/launcher/folder/FolderAnimHelper$Companion;

    iget-object v1, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mPreviewContainer:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    if-nez v1, :cond_0

    const-string v2, "mPreviewContainer"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0, v1, p1}, Lcom/miui/home/launcher/folder/FolderAnimHelper$Companion;->scaleEachIcon(Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;Z)V

    iget-object p1, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mImageView:Lcom/miui/home/launcher/LauncherIconImageView;

    if-nez p1, :cond_1

    const-string v0, "mImageView"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    if-eqz p1, :cond_2

    instance-of v0, p1, Lcom/miui/home/launcher/folder/IFolderContainerAnimationAble;

    if-eqz v0, :cond_2

    check-cast p1, Lcom/miui/home/launcher/folder/IFolderContainerAnimationAble;

    invoke-interface {p1}, Lcom/miui/home/launcher/folder/IFolderContainerAnimationAble;->onDragEnter()V

    :cond_2
    iget-object p1, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mDragOpenFolder:Ljava/lang/Runnable;

    const-wide/16 v0, 0x640

    invoke-virtual {p0, p1, v0, v1}, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->postDelayed(Ljava/lang/Runnable;J)Z

    iget-object p1, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mPerformHapticRunnable:Ljava/lang/Runnable;

    const/16 v0, 0x96

    int-to-long v0, v0

    invoke-virtual {p0, p1, v0, v1}, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_3
    return-void
.end method

.method public onDragExit(Lcom/miui/home/launcher/DragObject;)V
    .locals 3

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mIsDragingEnter:Z

    sget-object v0, Lcom/miui/home/launcher/folder/FolderAnimHelper;->Companion:Lcom/miui/home/launcher/folder/FolderAnimHelper$Companion;

    iget-object v1, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mPreviewContainer:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    if-nez v1, :cond_0

    const-string v2, "mPreviewContainer"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0, v1, p1}, Lcom/miui/home/launcher/folder/FolderAnimHelper$Companion;->scaleEachIcon(Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;Z)V

    iget-object p1, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mImageView:Lcom/miui/home/launcher/LauncherIconImageView;

    if-nez p1, :cond_1

    const-string v0, "mImageView"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    if-eqz p1, :cond_2

    instance-of v0, p1, Lcom/miui/home/launcher/folder/IFolderContainerAnimationAble;

    if-eqz v0, :cond_2

    check-cast p1, Lcom/miui/home/launcher/folder/IFolderContainerAnimationAble;

    invoke-interface {p1}, Lcom/miui/home/launcher/folder/IFolderContainerAnimationAble;->onDragExit()V

    :cond_2
    iget-object p1, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mPerformHapticRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, p1}, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-object p1, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mDragOpenFolder:Ljava/lang/Runnable;

    invoke-virtual {p0, p1}, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->removeCallbacks(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public onDropStart(Lcom/miui/home/launcher/DragObject;)V
    .locals 0

    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    const v0, 0x7f0a029e

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.preview_icons_container)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    iput-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mPreviewContainer:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    invoke-super {p0}, Lcom/miui/home/launcher/FolderIcon;->onFinishInflate()V

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mIconCache:Lcom/miui/home/launcher/IconCache;

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->getFolderIcon(Lcom/miui/home/launcher/IconCache;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mFolderBackground:Landroid/graphics/drawable/Drawable;

    const-string v0, "folder_cover_grids_four_reserved.png"

    invoke-static {v0}, Lmiui/content/res/IconCustomizer;->getRawIconDrawable(Ljava/lang/String;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    const-string v0, "folder_cover_grids_four_01_reserved.png"

    invoke-static {v0}, Lmiui/content/res/IconCustomizer;->getRawIconDrawable(Ljava/lang/String;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    :cond_0
    const v1, 0x7f0a00ea

    invoke-virtual {p0, v1}, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_5

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mFolderCover:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mFolderCover:Landroid/widget/ImageView;

    if-nez v1, :cond_1

    const-string v2, "mFolderCover"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mFolderCover:Landroid/widget/ImageView;

    if-nez v0, :cond_3

    const-string v1, "mFolderCover"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    const v1, 0x7f08021d

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_0
    const v0, 0x7f0a01ba

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_4

    check-cast v0, Lcom/miui/home/launcher/LauncherIconImageView;

    iput-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mImageView:Lcom/miui/home/launcher/LauncherIconImageView;

    invoke-direct {p0}, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->adjustsAllIconContainerNotClipChildren()V

    return-void

    :cond_4
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type com.miui.home.launcher.LauncherIconImageView"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type android.widget.ImageView"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onIconRemoved()V
    .locals 4

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mInfo:Lcom/miui/home/launcher/FolderInfo;

    invoke-virtual {v0}, Lcom/miui/home/launcher/FolderInfo;->count()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    iget-object v2, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mPreviewContainer:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    if-nez v2, :cond_0

    const-string v3, "mPreviewContainer"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v2}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->getMPvChildList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_1

    iget-object v2, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mInfo:Lcom/miui/home/launcher/FolderInfo;

    iget-object v3, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mLauncher:Lcom/miui/home/launcher/Launcher;

    check-cast v3, Landroid/content/Context;

    invoke-virtual {v2, v3}, Lcom/miui/home/launcher/FolderInfo;->getAdapter(Landroid/content/Context;)Lcom/miui/home/launcher/ShortcutsAdapter;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/miui/home/launcher/ShortcutsAdapter;->getItem(I)Lcom/miui/home/launcher/ShortcutInfo;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->onIconRemoved(Lcom/miui/home/launcher/ShortcutInfo;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->createOrRemoveView()V

    return-void
.end method

.method public onPause()V
    .locals 2

    invoke-super {p0}, Lcom/miui/home/launcher/FolderIcon;->onPause()V

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mPreviewContainer:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    if-nez v0, :cond_0

    const-string v1, "mPreviewContainer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->onPause()V

    return-void
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Lcom/miui/home/launcher/FolderIcon;->onResume()V

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mPreviewContainer:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    if-nez v0, :cond_0

    const-string v1, "mPreviewContainer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->onResume()V

    return-void
.end method

.method public onViewClick(Landroid/view/View;)V
    .locals 5

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mPreviewContainer:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    if-nez v0, :cond_0

    const-string v1, "mPreviewContainer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->getMPvChildList()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_5

    check-cast v0, Ljava/lang/Iterable;

    const/4 v1, 0x0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    add-int/lit8 v3, v1, 0x1

    if-gez v1, :cond_1

    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwIndexOverflow()V

    :cond_1
    check-cast v2, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mPreviewContainer:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    if-nez v2, :cond_2

    const-string v4, "mPreviewContainer"

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v2, v1}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->getPreviewIconInfo(I)Lcom/miui/home/launcher/ShortcutInfo;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    if-eqz v1, :cond_3

    iget-object v2, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mLauncher:Lcom/miui/home/launcher/Launcher;

    invoke-virtual {v1, v2, p1}, Lcom/miui/home/launcher/ShortcutInfo;->handleClick(Lcom/miui/home/launcher/Launcher;Landroid/view/View;)Z

    :cond_3
    iget-object v1, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mLauncher:Lcom/miui/home/launcher/Launcher;

    sget-object v2, Lcom/miui/home/launcher/LauncherState;->ALL_APPS:Lcom/miui/home/launcher/LauncherState;

    invoke-virtual {v1, v2}, Lcom/miui/home/launcher/Launcher;->isInState(Lcom/miui/home/launcher/LauncherState;)Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    invoke-static {v1}, Lcom/miui/privacy/track/SensorsAnalyticsCollector;->trackLocalAppsClick(I)V

    :cond_4
    move v1, v3

    goto :goto_0

    :cond_5
    return-void
.end method

.method public onWallpaperColorChanged()V
    .locals 2

    invoke-super {p0}, Lcom/miui/home/launcher/FolderIcon;->onWallpaperColorChanged()V

    invoke-static {}, Lcom/miui/home/launcher/WallpaperUtils;->hasAppliedLightWallpaper()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mIconCache:Lcom/miui/home/launcher/IconCache;

    invoke-direct {p0}, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->getMDefaultFolderBackground()Lcom/miui/home/launcher/folder/FolderIcon4x4NormalBackgroundDrawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0, v1}, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->getFolderBgGridFourLight(Lcom/miui/home/launcher/IconCache;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mIconCache:Lcom/miui/home/launcher/IconCache;

    invoke-direct {p0}, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->getMDefaultFolderBackground()Lcom/miui/home/launcher/folder/FolderIcon4x4NormalBackgroundDrawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0, v1}, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->getFolderBgGridFourIcon(Lcom/miui/home/launcher/IconCache;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mFolderBackground:Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mFolderBackground:Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->setIconImageView(Landroid/graphics/drawable/Drawable;Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mPreviewContainer:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    if-nez v0, :cond_1

    const-string v1, "mPreviewContainer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->setFolderIconPlaceholderDrawableMatchingWallpaperColor()V

    return-void
.end method

.method public rebindInfo(Lcom/miui/home/launcher/ItemInfo;Landroid/view/ViewGroup;)V
    .locals 1

    const-string v0, "info"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1, p2}, Lcom/miui/home/launcher/FolderIcon;->rebindInfo(Lcom/miui/home/launcher/ItemInfo;Landroid/view/ViewGroup;)V

    invoke-direct {p0}, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->createOrRemoveView()V

    return-void
.end method

.method public resetBackAnim()V
    .locals 2

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mPreviewContainer:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    if-nez v0, :cond_0

    const-string v1, "mPreviewContainer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->setVisibility(I)V

    return-void
.end method

.method public showPreview(Z)V
    .locals 2

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mPreviewContainer:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    if-nez v0, :cond_0

    const-string v1, "mPreviewContainer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p0, p1}, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->getFolderPreviewAlpha(Z)F

    move-result p1

    invoke-virtual {v0, p1}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->setAlpha(F)V

    iget-object p1, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mPreviewContainer:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    if-nez p1, :cond_1

    const-string v0, "mPreviewContainer"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p1}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->invalidate()V

    iget-object p1, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mNoWordAdapter:Lcom/miui/home/launcher/util/noword/NoWordLauncherElementAdapter;

    invoke-virtual {p1}, Lcom/miui/home/launcher/util/noword/NoWordLauncherElementAdapter;->invalidateBindElementWhenLauncherInEditMode()V

    return-void
.end method

.method public updateBackAnim(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public updateSizeOnIconSizeChanged()V
    .locals 2

    invoke-super {p0}, Lcom/miui/home/launcher/FolderIcon;->updateSizeOnIconSizeChanged()V

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mPreviewContainer:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    if-nez v0, :cond_0

    const-string v1, "mPreviewContainer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->requestLayout()V

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mImageView:Lcom/miui/home/launcher/LauncherIconImageView;

    if-nez v0, :cond_1

    const-string v1, "mImageView"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0}, Lcom/miui/home/launcher/LauncherIconImageView;->requestLayout()V

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon2x2_4;->mFolderCover:Landroid/widget/ImageView;

    if-nez v0, :cond_2

    const-string v1, "mFolderCover"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v0}, Landroid/widget/ImageView;->requestLayout()V

    return-void
.end method
