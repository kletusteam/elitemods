.class Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$AnimatingLayer;
.super Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$StaticBitmapLayer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "AnimatingLayer"
.end annotation


# instance fields
.field private mCurrentDrawDrawable:Landroid/graphics/drawable/Drawable;

.field private mIsAnimating:Z


# direct methods
.method private constructor <init>(Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/Bitmap;I)V
    .locals 6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$StaticBitmapLayer;-><init>(Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/Bitmap;ILcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$1;)V

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$AnimatingLayer;->mIsAnimating:Z

    iput-object p2, p0, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$AnimatingLayer;->mCurrentDrawDrawable:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method synthetic constructor <init>(Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/Bitmap;ILcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$1;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$AnimatingLayer;-><init>(Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/Bitmap;I)V

    return-void
.end method

.method static synthetic access$901(Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$AnimatingLayer;)V
    .locals 0

    invoke-super {p0}, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$StaticBitmapLayer;->resetBackAnim()V

    return-void
.end method

.method public static synthetic lambda$resetBackAnim$0(Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$AnimatingLayer;)V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$AnimatingLayer;->mIsAnimating:Z

    invoke-static {p0}, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$AnimatingLayer;->access$901(Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$AnimatingLayer;)V

    invoke-virtual {p0}, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$AnimatingLayer;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-static {v0}, Lcom/miui/launcher/utils/MamlUtils;->clearDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p0}, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$AnimatingLayer;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$AnimatingLayer;->mCurrentDrawDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$AnimatingLayer;->invalidate()V

    return-void
.end method


# virtual methods
.method cancelAnimating()V
    .locals 1

    goto/32 :goto_3

    nop

    :goto_0
    iget-boolean v0, p0, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$AnimatingLayer;->mIsAnimating:Z

    goto/32 :goto_2

    nop

    :goto_1
    return-void

    :goto_2
    if-nez v0, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_4

    nop

    :goto_3
    invoke-super {p0}, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$StaticBitmapLayer;->cancelAnimating()V

    goto/32 :goto_0

    nop

    :goto_4
    invoke-virtual {p0}, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$AnimatingLayer;->resetBackAnim()V

    :goto_5
    goto/32 :goto_1

    nop
.end method

.method public getCurrentShowDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$AnimatingLayer;->mCurrentDrawDrawable:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method isAnimating()Z
    .locals 2

    goto/32 :goto_7

    nop

    :goto_0
    const/4 v0, 0x0

    goto/32 :goto_5

    nop

    :goto_1
    if-ne v0, v1, :cond_0

    goto/32 :goto_c

    :cond_0
    goto/32 :goto_b

    nop

    :goto_2
    invoke-virtual {p0}, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$AnimatingLayer;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    goto/32 :goto_1

    nop

    :goto_3
    if-eqz v0, :cond_1

    goto/32 :goto_6

    :cond_1
    goto/32 :goto_4

    nop

    :goto_4
    iget-object v0, p0, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$AnimatingLayer;->mCurrentDrawDrawable:Landroid/graphics/drawable/Drawable;

    goto/32 :goto_2

    nop

    :goto_5
    goto :goto_9

    :goto_6
    goto/32 :goto_8

    nop

    :goto_7
    invoke-super {p0}, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$StaticBitmapLayer;->isAnimating()Z

    move-result v0

    goto/32 :goto_3

    nop

    :goto_8
    const/4 v0, 0x1

    :goto_9
    goto/32 :goto_a

    nop

    :goto_a
    return v0

    :goto_b
    goto :goto_6

    :goto_c
    goto/32 :goto_0

    nop
.end method

.method onBoundsChange(Landroid/graphics/Rect;FF)V
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    invoke-super {p0, p1, p2, p3}, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$StaticBitmapLayer;->onBoundsChange(Landroid/graphics/Rect;FF)V

    goto/32 :goto_4

    nop

    :goto_2
    invoke-virtual {p0}, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$AnimatingLayer;->getDrawableBounds()Landroid/graphics/Rect;

    move-result-object p2

    goto/32 :goto_3

    nop

    :goto_3
    invoke-virtual {p1, p2}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    goto/32 :goto_0

    nop

    :goto_4
    iget-object p1, p0, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$AnimatingLayer;->mCurrentDrawDrawable:Landroid/graphics/drawable/Drawable;

    goto/32 :goto_2

    nop
.end method

.method onDraw(Landroid/graphics/Canvas;Landroid/graphics/Path;)V
    .locals 2

    goto/32 :goto_8

    nop

    :goto_0
    return-void

    :goto_1
    invoke-super {p0, p1, p2}, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$StaticBitmapLayer;->onDraw(Landroid/graphics/Canvas;Landroid/graphics/Path;)V

    :goto_2
    goto/32 :goto_0

    nop

    :goto_3
    invoke-virtual {p0}, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$AnimatingLayer;->getParentBounds()Landroid/graphics/Rect;

    move-result-object v1

    goto/32 :goto_b

    nop

    :goto_4
    goto :goto_2

    :goto_5
    goto/32 :goto_1

    nop

    :goto_6
    iget-object v0, p0, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$AnimatingLayer;->mCurrentDrawDrawable:Landroid/graphics/drawable/Drawable;

    goto/32 :goto_3

    nop

    :goto_7
    invoke-static {v0}, Lcom/miui/home/launcher/graphics/drawable/FancyDrawableCompat;->isInstance(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    goto/32 :goto_a

    nop

    :goto_8
    iget-object v0, p0, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$AnimatingLayer;->mCurrentDrawDrawable:Landroid/graphics/drawable/Drawable;

    goto/32 :goto_7

    nop

    :goto_9
    if-nez p2, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_6

    nop

    :goto_a
    if-nez v0, :cond_1

    goto/32 :goto_5

    :cond_1
    goto/32 :goto_9

    nop

    :goto_b
    invoke-static {v0, p1, v1, p2}, Lcom/miui/home/launcher/graphics/drawable/FancyDrawableCompat;->draw(Landroid/graphics/drawable/Drawable;Landroid/graphics/Canvas;Landroid/graphics/Rect;Landroid/graphics/Path;)V

    goto/32 :goto_4

    nop
.end method

.method public prepareBackAnim()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$AnimatingLayer;->mIsAnimating:Z

    invoke-super {p0}, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$StaticBitmapLayer;->prepareBackAnim()V

    invoke-static {}, Lcom/miui/home/launcher/DeviceConfig;->isSupportRecentsAndFsGesture()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$AnimatingLayer;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-static {v0}, Lcom/miui/home/launcher/graphics/drawable/AnimatingDrawableCompat;->getStartDrawable(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    iput-object v0, p0, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$AnimatingLayer;->mCurrentDrawDrawable:Landroid/graphics/drawable/Drawable;

    :cond_0
    return-void
.end method

.method public resetBackAnim()V
    .locals 2

    sget-object v0, Lcom/miui/home/recents/TouchInteractionService;->MAIN_THREAD_EXECUTOR:Lcom/miui/home/launcher/MainThreadExecutor;

    new-instance v1, Lcom/miui/home/launcher/graphics/drawable/-$$Lambda$LayerAdaptiveIconDrawable$AnimatingLayer$O6nDXo-PA8CuviST1k8LPh2j5-E;

    invoke-direct {v1, p0}, Lcom/miui/home/launcher/graphics/drawable/-$$Lambda$LayerAdaptiveIconDrawable$AnimatingLayer$O6nDXo-PA8CuviST1k8LPh2j5-E;-><init>(Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$AnimatingLayer;)V

    invoke-virtual {v0, v1}, Lcom/miui/home/launcher/MainThreadExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method updateBackAnim(Ljava/lang/String;F)V
    .locals 3

    goto/32 :goto_14

    nop

    :goto_0
    invoke-static {v0, p2}, Lcom/miui/home/launcher/graphics/drawable/FancyDrawableCompat;->updateRatio(Ljava/lang/Object;F)V

    goto/32 :goto_5

    nop

    :goto_1
    const/high16 v2, -0x40800000    # -1.0f

    goto/32 :goto_c

    nop

    :goto_2
    return-void

    :goto_3
    invoke-static {v1, p2}, Ljava/lang/Math;->min(FF)F

    move-result p2

    goto/32 :goto_0

    nop

    :goto_4
    invoke-static {v0}, Lcom/miui/home/launcher/graphics/drawable/AnimatingDrawableCompat;->getFancyDrawable(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto/32 :goto_8

    nop

    :goto_5
    iput-object v0, p0, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$AnimatingLayer;->mCurrentDrawDrawable:Landroid/graphics/drawable/Drawable;

    :goto_6
    goto/32 :goto_11

    nop

    :goto_7
    invoke-static {v0}, Lcom/miui/home/launcher/graphics/drawable/FancyDrawableCompat;->isInstance(Landroid/graphics/drawable/Drawable;)Z

    move-result v1

    goto/32 :goto_a

    nop

    :goto_8
    iget-boolean v1, p0, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$AnimatingLayer;->mIsAnimating:Z

    goto/32 :goto_16

    nop

    :goto_9
    invoke-static {p2, p1}, Lcom/miui/launcher/utils/MamlUtils;->notifyBackHome(Landroid/graphics/drawable/Drawable;Ljava/lang/String;)V

    goto/32 :goto_d

    nop

    :goto_a
    if-nez v1, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_15

    nop

    :goto_b
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    goto/32 :goto_17

    nop

    :goto_c
    invoke-static {v2, p2}, Ljava/lang/Math;->max(FF)F

    move-result p2

    goto/32 :goto_3

    nop

    :goto_d
    invoke-virtual {p0}, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$AnimatingLayer;->invalidate()V

    :goto_e
    goto/32 :goto_2

    nop

    :goto_f
    invoke-virtual {p0}, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$AnimatingLayer;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto/32 :goto_4

    nop

    :goto_10
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    goto/32 :goto_12

    nop

    :goto_11
    iget-object p2, p0, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$AnimatingLayer;->mCurrentDrawDrawable:Landroid/graphics/drawable/Drawable;

    goto/32 :goto_9

    nop

    :goto_12
    const/high16 v1, 0x3f800000    # 1.0f

    goto/32 :goto_1

    nop

    :goto_13
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getCallback()Landroid/graphics/drawable/Drawable$Callback;

    move-result-object v1

    goto/32 :goto_b

    nop

    :goto_14
    invoke-super {p0, p1, p2}, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$StaticBitmapLayer;->updateBackAnim(Ljava/lang/String;F)V

    goto/32 :goto_f

    nop

    :goto_15
    invoke-virtual {p0}, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$AnimatingLayer;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    goto/32 :goto_13

    nop

    :goto_16
    if-nez v1, :cond_1

    goto/32 :goto_e

    :cond_1
    goto/32 :goto_7

    nop

    :goto_17
    invoke-virtual {p0}, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$AnimatingLayer;->getDrawableBounds()Landroid/graphics/Rect;

    move-result-object v1

    goto/32 :goto_10

    nop
.end method
