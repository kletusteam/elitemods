.class public final Lcom/miui/home/launcher/folder/FolderIcon1x1;
.super Lcom/miui/home/launcher/FolderIcon;


# instance fields
.field private mAlphaAnimator:Landroid/animation/ValueAnimator;

.field private mDragOverAnimator:Landroid/animation/ValueAnimator;

.field private mFolderCover:Landroid/widget/ImageView;

.field private mImageView:Lcom/miui/home/launcher/LauncherIconImageView;

.field private mItemIcons:[Lcom/miui/home/launcher/folder/FolderIconPreviewContainer1X1$PreviewIconView;

.field private mPreviewContainer:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer1X1;

.field private final sTmpCanvas:Landroid/graphics/Canvas;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attr"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2}, Lcom/miui/home/launcher/FolderIcon;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p1, 0x0

    new-array p1, p1, [Lcom/miui/home/launcher/folder/FolderIconPreviewContainer1X1$PreviewIconView;

    iput-object p1, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mItemIcons:[Lcom/miui/home/launcher/folder/FolderIconPreviewContainer1X1$PreviewIconView;

    new-instance p1, Landroid/animation/ValueAnimator;

    invoke-direct {p1}, Landroid/animation/ValueAnimator;-><init>()V

    iput-object p1, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mAlphaAnimator:Landroid/animation/ValueAnimator;

    new-instance p1, Landroid/animation/ValueAnimator;

    invoke-direct {p1}, Landroid/animation/ValueAnimator;-><init>()V

    iput-object p1, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mDragOverAnimator:Landroid/animation/ValueAnimator;

    new-instance p1, Landroid/graphics/Canvas;

    invoke-direct {p1}, Landroid/graphics/Canvas;-><init>()V

    iput-object p1, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->sTmpCanvas:Landroid/graphics/Canvas;

    invoke-static {}, Lcom/miui/home/launcher/Application;->getLauncherApplication()Lcom/miui/home/launcher/Application;

    move-result-object p1

    const-string p2, "Application.getLauncherApplication()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/miui/home/launcher/Application;->getIconCache()Lcom/miui/home/launcher/IconCache;

    move-result-object p1

    iput-object p1, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mIconCache:Lcom/miui/home/launcher/IconCache;

    invoke-static {}, Lcom/miui/home/launcher/folder/FolderIcon1x1;->access$getSLayerPaint$p$s1165083175()Landroid/graphics/Paint;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/miui/home/launcher/folder/FolderIcon1x1;->setLayerPaint(Landroid/graphics/Paint;)V

    return-void
.end method

.method public static final synthetic access$getDEFAULT_DRAG_OVER_ANIM_SCALE$p$s1165083175()F
    .locals 1

    sget v0, Lcom/miui/home/launcher/FolderIcon;->DEFAULT_DRAG_OVER_ANIM_SCALE:F

    return v0
.end method

.method public static final synthetic access$getMAlphaAnimator$p(Lcom/miui/home/launcher/folder/FolderIcon1x1;)Landroid/animation/ValueAnimator;
    .locals 0

    iget-object p0, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mAlphaAnimator:Landroid/animation/ValueAnimator;

    return-object p0
.end method

.method public static final synthetic access$getMFolderCover$p(Lcom/miui/home/launcher/folder/FolderIcon1x1;)Landroid/widget/ImageView;
    .locals 1

    iget-object p0, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mFolderCover:Landroid/widget/ImageView;

    if-nez p0, :cond_0

    const-string v0, "mFolderCover"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getMIconImageView$p(Lcom/miui/home/launcher/folder/FolderIcon1x1;)Lcom/miui/home/launcher/LauncherIconImageView;
    .locals 0

    iget-object p0, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mIconImageView:Lcom/miui/home/launcher/LauncherIconImageView;

    return-object p0
.end method

.method public static final synthetic access$getMIsDragingEnter$p(Lcom/miui/home/launcher/folder/FolderIcon1x1;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mIsDragingEnter:Z

    return p0
.end method

.method public static final synthetic access$getMLauncher$p(Lcom/miui/home/launcher/folder/FolderIcon1x1;)Lcom/miui/home/launcher/Launcher;
    .locals 0

    iget-object p0, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mLauncher:Lcom/miui/home/launcher/Launcher;

    return-object p0
.end method

.method public static final synthetic access$getMPreviewContainer$p(Lcom/miui/home/launcher/folder/FolderIcon1x1;)Lcom/miui/home/launcher/folder/FolderIconPreviewContainer1X1;
    .locals 1

    iget-object p0, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mPreviewContainer:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer1X1;

    if-nez p0, :cond_0

    const-string v0, "mPreviewContainer"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getSLayerPaint$p$s1165083175()Landroid/graphics/Paint;
    .locals 1

    sget-object v0, Lcom/miui/home/launcher/FolderIcon;->sLayerPaint:Landroid/graphics/Paint;

    return-object v0
.end method


# virtual methods
.method protected drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
    .locals 3

    const-string v0, "child"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderIcon1x1;->isDrawingInThumbnailView()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mTitleContainer:Lcom/miui/home/launcher/ItemIconTitleContainer;

    if-ne p2, v0, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderIcon1x1;->isDrawingInThumbnailView()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mInfo:Lcom/miui/home/launcher/FolderInfo;

    const-string v1, "mInfo"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/miui/home/launcher/FolderInfo;->isOpened()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mIconContainer:Landroid/widget/FrameLayout;

    if-ne p2, v0, :cond_4

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mPreviewContainer:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer1X1;

    if-nez v0, :cond_1

    const-string v1, "mPreviewContainer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer1X1;->getAlpha()F

    move-result v0

    iget-object v1, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mPreviewContainer:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer1X1;

    if-nez v1, :cond_2

    const-string v2, "mPreviewContainer"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer1X1;->setAlpha(F)V

    invoke-super {p0, p1, p2, p3, p4}, Lcom/miui/home/launcher/FolderIcon;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result p1

    iget-object p2, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mPreviewContainer:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer1X1;

    if-nez p2, :cond_3

    const-string p3, "mPreviewContainer"

    invoke-static {p3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {p2, v0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer1X1;->setAlpha(F)V

    goto :goto_0

    :cond_4
    invoke-super {p0, p1, p2, p3, p4}, Lcom/miui/home/launcher/FolderIcon;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result p1

    :goto_0
    return p1
.end method

.method protected dropIconIntoFolderIcon(Lcom/miui/home/launcher/DragObject;)V
    .locals 8

    const-string v0, "d"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    check-cast v0, Landroid/view/View;

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mInfo:Lcom/miui/home/launcher/FolderInfo;

    invoke-virtual {v0}, Lcom/miui/home/launcher/FolderInfo;->count()I

    move-result v0

    iget v1, p1, Lcom/miui/home/launcher/DragObject;->dropAction:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    invoke-virtual {p1}, Lcom/miui/home/launcher/DragObject;->getDragInfo()Lcom/miui/home/launcher/ItemInfo;

    move-result-object v1

    iget v1, v1, Lcom/miui/home/launcher/ItemInfo;->cellX:I

    iget-object v3, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mItemIcons:[Lcom/miui/home/launcher/folder/FolderIconPreviewContainer1X1$PreviewIconView;

    array-length v3, v3

    if-ge v1, v3, :cond_0

    invoke-virtual {p1}, Lcom/miui/home/launcher/DragObject;->getDragInfo()Lcom/miui/home/launcher/ItemInfo;

    move-result-object v0

    iget v0, v0, Lcom/miui/home/launcher/ItemInfo;->cellX:I

    :cond_0
    iget-object v1, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mItemIcons:[Lcom/miui/home/launcher/folder/FolderIconPreviewContainer1X1$PreviewIconView;

    array-length v3, v1

    const/4 v4, 0x1

    sub-int/2addr v3, v4

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    aget-object v1, v1, v3

    check-cast v1, Landroid/view/View;

    invoke-virtual {p1}, Lcom/miui/home/launcher/DragObject;->getDragView()Lcom/miui/home/launcher/DragView;

    move-result-object v3

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderIcon1x1;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0701d2

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v5

    neg-int v5, v5

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderIcon1x1;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0701d3

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    neg-int v6, v6

    invoke-virtual {v3, v5, v6}, Lcom/miui/home/launcher/DragView;->setDragVisualizeOffset(II)V

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v5

    int-to-float v5, v5

    const-string v6, "dragView"

    invoke-static {v3, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/miui/home/launcher/DragView;->getContent()Landroid/view/View;

    move-result-object v6

    const-string v7, "dragView.content"

    invoke-static {v6, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v6}, Landroid/view/View;->getWidth()I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v5, v6

    invoke-virtual {v3, v5}, Lcom/miui/home/launcher/DragView;->setTargetScale(F)V

    iget-object v5, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mItemIcons:[Lcom/miui/home/launcher/folder/FolderIconPreviewContainer1X1$PreviewIconView;

    array-length v5, v5

    if-lt v0, v5, :cond_1

    invoke-virtual {v3}, Lcom/miui/home/launcher/DragView;->setFakeTargetMode()V

    invoke-virtual {v3}, Lcom/miui/home/launcher/DragView;->setFadeoutAnimationMode()V

    :cond_1
    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Lcom/miui/home/launcher/DragView;->setPivotX(F)V

    invoke-virtual {v3, v0}, Lcom/miui/home/launcher/DragView;->setPivotY(F)V

    invoke-virtual {v3, v1}, Lcom/miui/home/launcher/DragView;->setAnimateTarget(Landroid/view/View;)Z

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mInfo:Lcom/miui/home/launcher/FolderInfo;

    invoke-virtual {p1}, Lcom/miui/home/launcher/DragObject;->getDragInfo()Lcom/miui/home/launcher/ItemInfo;

    move-result-object v1

    if-eqz v1, :cond_3

    check-cast v1, Lcom/miui/home/launcher/ShortcutInfo;

    iget p1, p1, Lcom/miui/home/launcher/DragObject;->dropAction:I

    if-eq p1, v2, :cond_2

    goto :goto_0

    :cond_2
    const/4 v4, 0x0

    :goto_0
    iget-object p1, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mLauncher:Lcom/miui/home/launcher/Launcher;

    const-string v2, "mLauncher"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/miui/home/launcher/Launcher;->getLauncherMode()Lcom/miui/home/launcher/allapps/LauncherMode;

    move-result-object p1

    invoke-virtual {v0, v1, v4, p1}, Lcom/miui/home/launcher/FolderInfo;->add(Lcom/miui/home/launcher/ShortcutInfo;ZLcom/miui/home/launcher/allapps/LauncherMode;)V

    iget-object p1, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mInfo:Lcom/miui/home/launcher/FolderInfo;

    invoke-virtual {p1}, Lcom/miui/home/launcher/FolderInfo;->notifyDataSetChanged()V

    return-void

    :cond_3
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.miui.home.launcher.ShortcutInfo"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public getBackAnimPreviewDrawable()Landroid/graphics/drawable/Drawable;
    .locals 9

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mIconImageView:Lcom/miui/home/launcher/LauncherIconImageView;

    const-string v1, "mIconImageView"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/miui/home/launcher/LauncherIconImageView;->getWidth()I

    move-result v0

    int-to-float v0, v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iget-object v1, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mIconImageView:Lcom/miui/home/launcher/LauncherIconImageView;

    const-string v2, "mIconImageView"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/miui/home/launcher/LauncherIconImageView;->getHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Lcom/miui/home/launcher/common/Utilities;->createBitmapSafely(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_3

    iget-object v2, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->sTmpCanvas:Landroid/graphics/Canvas;

    invoke-virtual {v2}, Landroid/graphics/Canvas;->save()I

    move-result v3

    invoke-virtual {v2, v0}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    iget-object v4, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mIconImageView:Lcom/miui/home/launcher/LauncherIconImageView;

    const-string v5, "mIconImageView"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/miui/home/launcher/LauncherIconImageView;->getWidth()I

    move-result v4

    iget-object v5, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mPreviewContainer:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer1X1;

    if-nez v5, :cond_0

    const-string v6, "mPreviewContainer"

    invoke-static {v6}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v5}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer1X1;->getWidth()I

    move-result v5

    sub-int/2addr v4, v5

    int-to-float v4, v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    iget-object v6, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mIconImageView:Lcom/miui/home/launcher/LauncherIconImageView;

    const-string v7, "mIconImageView"

    invoke-static {v6, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v6}, Lcom/miui/home/launcher/LauncherIconImageView;->getHeight()I

    move-result v6

    iget-object v7, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mPreviewContainer:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer1X1;

    if-nez v7, :cond_1

    const-string v8, "mPreviewContainer"

    invoke-static {v8}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v7}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer1X1;->getHeight()I

    move-result v7

    sub-int/2addr v6, v7

    int-to-float v6, v6

    div-float/2addr v6, v5

    invoke-virtual {v2, v4, v6}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v4, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mPreviewContainer:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer1X1;

    if-nez v4, :cond_2

    const-string v5, "mPreviewContainer"

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v4, v2}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer1X1;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {v2, v1}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    invoke-virtual {v2, v3}, Landroid/graphics/Canvas;->restoreToCount(I)V

    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderIcon1x1;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    check-cast v1, Landroid/graphics/drawable/Drawable;

    return-object v1

    :cond_3
    return-object v1
.end method

.method public final getCover()Landroid/widget/ImageView;
    .locals 2

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mFolderCover:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    const-string v1, "mFolderCover"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method protected getDragEnterAnimatorValues()[F
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [F

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    aput v2, v0, v1

    invoke-static {}, Lcom/miui/home/launcher/folder/FolderIcon1x1;->access$getDEFAULT_DRAG_OVER_ANIM_SCALE$p$s1165083175()F

    move-result v1

    const/4 v2, 0x1

    aput v1, v0, v2

    return-object v0
.end method

.method protected getDragExitAnimatorValues()[F
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [F

    invoke-static {}, Lcom/miui/home/launcher/folder/FolderIcon1x1;->access$getDEFAULT_DRAG_OVER_ANIM_SCALE$p$s1165083175()F

    move-result v1

    const/4 v2, 0x0

    aput v1, v0, v2

    const/4 v1, 0x1

    const/high16 v2, 0x3f800000    # 1.0f

    aput v2, v0, v1

    return-object v0
.end method

.method public getFolderBackground()Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mFolderBackground:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getHitRect(Landroid/graphics/Rect;)V
    .locals 5

    const-string v0, "outRect"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderIcon1x1;->getLeft()I

    move-result v0

    iget-object v1, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mIconImageView:Lcom/miui/home/launcher/LauncherIconImageView;

    const-string v2, "mIconImageView"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/miui/home/launcher/LauncherIconImageView;->getLeft()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderIcon1x1;->getTop()I

    move-result v1

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderIcon1x1;->getLeft()I

    move-result v2

    iget-object v3, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mIconImageView:Lcom/miui/home/launcher/LauncherIconImageView;

    const-string v4, "mIconImageView"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/miui/home/launcher/LauncherIconImageView;->getWidth()I

    move-result v3

    add-int/2addr v2, v3

    iget-object v3, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mIconImageView:Lcom/miui/home/launcher/LauncherIconImageView;

    const-string v4, "mIconImageView"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/miui/home/launcher/LauncherIconImageView;->getLeft()I

    move-result v3

    add-int/2addr v2, v3

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderIcon1x1;->getBottom()I

    move-result v3

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    return-void
.end method

.method public getPreviewArray()[Landroid/widget/ImageView;
    .locals 2

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mItemIcons:[Lcom/miui/home/launcher/folder/FolderIconPreviewContainer1X1$PreviewIconView;

    if-eqz v0, :cond_0

    check-cast v0, [Landroid/widget/ImageView;

    return-object v0

    :cond_0
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.Array<android.widget.ImageView>"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getPreviewContainerSnapshot()Landroid/graphics/Bitmap;
    .locals 2

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mPreviewContainer:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer1X1;

    if-nez v0, :cond_0

    const-string v1, "mPreviewContainer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Landroid/view/View;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v0, v1}, Lcom/miui/home/launcher/DragController;->createViewBitmap(Landroid/view/View;F)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getPreviewCount()I
    .locals 1

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mItemIcons:[Lcom/miui/home/launcher/folder/FolderIconPreviewContainer1X1$PreviewIconView;

    array-length v0, v0

    return v0
.end method

.method public getPreviewIconHeight()F
    .locals 2

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mItemIcons:[Lcom/miui/home/launcher/folder/FolderIconPreviewContainer1X1$PreviewIconView;

    if-eqz v0, :cond_0

    array-length v1, v0

    if-lez v1, :cond_0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer1X1$PreviewIconView;->getHeight()I

    move-result v0

    int-to-float v0, v0

    goto :goto_0

    :cond_0
    const/4 v0, -0x1

    int-to-float v0, v0

    :goto_0
    return v0
.end method

.method public getPreviewPosition(Landroid/graphics/Rect;)F
    .locals 10

    const-string v0, "rect"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mTmpPos:[F

    const/4 v1, 0x1

    const/4 v2, 0x0

    aput v2, v0, v1

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mTmpPos:[F

    iget-object v2, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mTmpPos:[F

    aget v2, v2, v1

    const/4 v3, 0x0

    aput v2, v0, v3

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mPreviewContainer:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer1X1;

    if-nez v0, :cond_0

    const-string v2, "mPreviewContainer"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    move-object v4, v0

    check-cast v4, Landroid/view/View;

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mLauncher:Lcom/miui/home/launcher/Launcher;

    const-string v2, "mLauncher"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/miui/home/launcher/Launcher;->getDragLayer()Lcom/miui/home/launcher/DragLayer;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Landroid/view/View;

    iget-object v6, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mTmpPos:[F

    const/4 v7, 0x1

    const/4 v8, 0x0

    new-instance v0, Lcom/miui/home/launcher/folder/FolderIcon1x1$getPreviewPosition$scale$1;

    invoke-direct {v0, p0}, Lcom/miui/home/launcher/folder/FolderIcon1x1$getPreviewPosition$scale$1;-><init>(Lcom/miui/home/launcher/folder/FolderIcon1x1;)V

    move-object v9, v0

    check-cast v9, Ljava/util/function/Predicate;

    invoke-static/range {v4 .. v9}, Lcom/miui/home/launcher/common/Utilities;->getDescendantCoordRelativeToAncestor(Landroid/view/View;Landroid/view/View;[FZZLjava/util/function/Predicate;)F

    move-result v0

    iget-object v2, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mTmpPos:[F

    aget v2, v2, v3

    float-to-int v2, v2

    iget-object v4, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mTmpPos:[F

    aget v4, v4, v1

    float-to-int v4, v4

    iget-object v5, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mTmpPos:[F

    aget v3, v5, v3

    iget-object v5, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mPreviewContainer:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer1X1;

    if-nez v5, :cond_1

    const-string v6, "mPreviewContainer"

    invoke-static {v6}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v5}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer1X1;->getMeasuredWidth()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, v0

    add-float/2addr v3, v5

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    iget-object v5, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mTmpPos:[F

    aget v1, v5, v1

    iget-object v5, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mPreviewContainer:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer1X1;

    if-nez v5, :cond_2

    const-string v6, "mPreviewContainer"

    invoke-static {v6}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v5}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer1X1;->getMeasuredHeight()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, v0

    add-float/2addr v1, v5

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-virtual {p1, v2, v4, v3, v1}, Landroid/graphics/Rect;->set(IIII)V

    return v0
.end method

.method protected initNoWordAdapter()Lcom/miui/home/launcher/util/noword/NoWordLauncherElementAdapter;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/miui/home/launcher/util/noword/NoWordLauncherElementAdapter<",
            "*>;"
        }
    .end annotation

    new-instance v0, Lcom/miui/home/launcher/folder/FolderIcon1x1$initNoWordAdapter$1;

    move-object v1, p0

    check-cast v1, Lcom/miui/home/launcher/ItemIcon;

    invoke-direct {v0, p0, v1}, Lcom/miui/home/launcher/folder/FolderIcon1x1$initNoWordAdapter$1;-><init>(Lcom/miui/home/launcher/folder/FolderIcon1x1;Lcom/miui/home/launcher/ItemIcon;)V

    check-cast v0, Lcom/miui/home/launcher/util/noword/NoWordLauncherElementAdapter;

    return-object v0
.end method

.method public invalidatePreviews()V
    .locals 3

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mItemIcons:[Lcom/miui/home/launcher/folder/FolderIconPreviewContainer1X1$PreviewIconView;

    array-length v0, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    iget-object v2, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mItemIcons:[Lcom/miui/home/launcher/folder/FolderIconPreviewContainer1X1$PreviewIconView;

    aget-object v2, v2, v1

    invoke-virtual {v2}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer1X1$PreviewIconView;->invalidate()V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public loadItemIcons(Z)V
    .locals 4

    iget-object p1, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mPreviewContainer:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer1X1;

    if-nez p1, :cond_0

    const-string v0, "mPreviewContainer"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mInfo:Lcom/miui/home/launcher/FolderInfo;

    const-string v1, "mInfo"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mIconCache:Lcom/miui/home/launcher/IconCache;

    const-string v2, "mIconCache"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mSerialExecutor:Lcom/miui/home/library/utils/AsyncTaskExecutorHelper$SerialExecutor;

    const-string v3, "mSerialExecutor"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, v0, v1, v2}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer1X1;->loadItemIcons(Lcom/miui/home/launcher/FolderInfo;Lcom/miui/home/launcher/IconCache;Lcom/miui/home/library/utils/AsyncTaskExecutorHelper$SerialExecutor;)V

    iget-object p1, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mLauncher:Lcom/miui/home/launcher/Launcher;

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mInfo:Lcom/miui/home/launcher/FolderInfo;

    invoke-virtual {p1, v0}, Lcom/miui/home/launcher/Launcher;->updateFolderMessage(Lcom/miui/home/launcher/FolderInfo;)V

    return-void
.end method

.method public onBackAnimStart()V
    .locals 2

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mPreviewContainer:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer1X1;

    if-nez v0, :cond_0

    const-string v1, "mPreviewContainer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer1X1;->setVisibility(I)V

    return-void
.end method

.method public onBackAnimStop()V
    .locals 2

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mPreviewContainer:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer1X1;

    if-nez v0, :cond_0

    const-string v1, "mPreviewContainer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer1X1;->setVisibility(I)V

    return-void
.end method

.method public onDragEnter(Lcom/miui/home/launcher/DragObject;)V
    .locals 4

    const-string v0, "d"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/miui/home/launcher/DragObject;->getDragInfo()Lcom/miui/home/launcher/ItemInfo;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/miui/home/launcher/folder/FolderIcon1x1;->isDropable(Lcom/miui/home/launcher/ItemInfo;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mDragOverAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderIcon1x1;->getDragEnterAnimatorValues()[F

    move-result-object v0

    array-length v1, v0

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([FI)[F

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/animation/ValueAnimator;->setFloatValues([F)V

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mIsDragingEnter:Z

    iget-object p1, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mDragOverAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->start()V

    iget-object p1, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mPerformHapticRunnable:Ljava/lang/Runnable;

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mDragOverAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->getDuration()J

    move-result-wide v0

    const/4 v2, 0x2

    int-to-long v2, v2

    div-long/2addr v0, v2

    invoke-virtual {p0, p1, v0, v1}, Lcom/miui/home/launcher/folder/FolderIcon1x1;->postDelayed(Ljava/lang/Runnable;J)Z

    iget-object p1, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mDragOpenFolder:Ljava/lang/Runnable;

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mAlphaAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->getDuration()J

    move-result-wide v0

    mul-long/2addr v0, v2

    iget-object v2, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mDragOverAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->getDuration()J

    move-result-wide v2

    add-long/2addr v0, v2

    invoke-virtual {p0, p1, v0, v1}, Lcom/miui/home/launcher/folder/FolderIcon1x1;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    return-void
.end method

.method public onDragExit(Lcom/miui/home/launcher/DragObject;)V
    .locals 2

    iget-object p1, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mDragOverAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderIcon1x1;->getDragExitAnimatorValues()[F

    move-result-object v0

    array-length v1, v0

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([FI)[F

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/animation/ValueAnimator;->setFloatValues([F)V

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mIsDragingEnter:Z

    iget-object p1, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mDragOverAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->start()V

    iget-object p1, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mAlphaAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->cancel()V

    iget-object p1, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mPerformHapticRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, p1}, Lcom/miui/home/launcher/folder/FolderIcon1x1;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-object p1, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mDragOpenFolder:Ljava/lang/Runnable;

    invoke-virtual {p0, p1}, Lcom/miui/home/launcher/folder/FolderIcon1x1;->removeCallbacks(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public onDragOver(Lcom/miui/home/launcher/DragObject;)V
    .locals 0

    return-void
.end method

.method public onDropStart(Lcom/miui/home/launcher/DragObject;)V
    .locals 0

    iget-object p1, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mAlphaAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->cancel()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 4

    const v0, 0x7f0a029e

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/folder/FolderIcon1x1;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.preview_icons_container)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer1X1;

    iput-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mPreviewContainer:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer1X1;

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mPreviewContainer:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer1X1;

    if-nez v0, :cond_0

    const-string v1, "mPreviewContainer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer1X1;->getItemViews()[Lcom/miui/home/launcher/folder/FolderIconPreviewContainer1X1$PreviewIconView;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mItemIcons:[Lcom/miui/home/launcher/folder/FolderIconPreviewContainer1X1$PreviewIconView;

    invoke-super {p0}, Lcom/miui/home/launcher/FolderIcon;->onFinishInflate()V

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mContext:Landroid/content/Context;

    const-string v1, "mContext"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mIconCache:Lcom/miui/home/launcher/IconCache;

    invoke-virtual {p0, v1}, Lcom/miui/home/launcher/folder/FolderIcon1x1;->getFolderIcon(Lcom/miui/home/launcher/IconCache;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mFolderBackground:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mDragOverAnimator:Landroid/animation/ValueAnimator;

    const/high16 v2, 0x10e0000

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mDragOverAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/miui/home/launcher/folder/FolderIcon1x1$onFinishInflate$1;

    invoke-direct {v1, p0}, Lcom/miui/home/launcher/folder/FolderIcon1x1$onFinishInflate$1;-><init>(Lcom/miui/home/launcher/folder/FolderIcon1x1;)V

    check-cast v1, Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mDragOverAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/miui/home/launcher/folder/FolderIcon1x1$onFinishInflate$2;

    invoke-direct {v1, p0}, Lcom/miui/home/launcher/folder/FolderIcon1x1$onFinishInflate$2;-><init>(Lcom/miui/home/launcher/folder/FolderIcon1x1;)V

    check-cast v1, Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mAlphaAnimator:Landroid/animation/ValueAnimator;

    const/4 v1, 0x5

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setFloatValues([F)V

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mAlphaAnimator:Landroid/animation/ValueAnimator;

    const/16 v1, 0x12c

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mAlphaAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/miui/home/launcher/folder/FolderIcon1x1$onFinishInflate$3;

    invoke-direct {v1, p0}, Lcom/miui/home/launcher/folder/FolderIcon1x1$onFinishInflate$3;-><init>(Lcom/miui/home/launcher/folder/FolderIcon1x1;)V

    check-cast v1, Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mAlphaAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/miui/home/launcher/folder/FolderIcon1x1$onFinishInflate$4;

    invoke-direct {v1, p0}, Lcom/miui/home/launcher/folder/FolderIcon1x1$onFinishInflate$4;-><init>(Lcom/miui/home/launcher/folder/FolderIcon1x1;)V

    check-cast v1, Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mAlphaAnimator:Landroid/animation/ValueAnimator;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mAlphaAnimator:Landroid/animation/ValueAnimator;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setRepeatMode(I)V

    const-string v0, "folder_icon_cover.png"

    invoke-static {v0}, Lmiui/content/res/IconCustomizer;->getRawIconDrawable(Ljava/lang/String;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_1

    const-string v0, "folder_icon_cover_01.png"

    invoke-static {v0}, Lmiui/content/res/IconCustomizer;->getRawIconDrawable(Ljava/lang/String;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    :cond_1
    const v1, 0x7f0a00ea

    invoke-virtual {p0, v1}, Lcom/miui/home/launcher/folder/FolderIcon1x1;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_7

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mFolderCover:Landroid/widget/ImageView;

    if-eqz v0, :cond_3

    iget-object v1, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mFolderCover:Landroid/widget/ImageView;

    if-nez v1, :cond_2

    const-string v2, "mFolderCover"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mFolderCover:Landroid/widget/ImageView;

    if-nez v0, :cond_4

    const-string v1, "mFolderCover"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    const v1, 0x7f08021d

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_0
    const v0, 0x7f0a01ba

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/folder/FolderIcon1x1;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_6

    check-cast v0, Lcom/miui/home/launcher/LauncherIconImageView;

    iput-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mImageView:Lcom/miui/home/launcher/LauncherIconImageView;

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mPreviewContainer:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer1X1;

    if-nez v0, :cond_5

    const-string v1, "mPreviewContainer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderIcon1x1;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "resources"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer1X1;->calculateItemIconSize(Landroid/content/res/Resources;)V

    return-void

    :cond_6
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type com.miui.home.launcher.LauncherIconImageView"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_7
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type android.widget.ImageView"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x3f000000    # 0.5f
        0x0
        0x3f000000    # 0.5f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public onIconRemoved()V
    .locals 4

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mInfo:Lcom/miui/home/launcher/FolderInfo;

    invoke-virtual {v0}, Lcom/miui/home/launcher/FolderInfo;->count()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    iget-object v2, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mItemIcons:[Lcom/miui/home/launcher/folder/FolderIconPreviewContainer1X1$PreviewIconView;

    array-length v2, v2

    if-lez v2, :cond_0

    iget-object v2, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mInfo:Lcom/miui/home/launcher/FolderInfo;

    iget-object v3, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mLauncher:Lcom/miui/home/launcher/Launcher;

    check-cast v3, Landroid/content/Context;

    invoke-virtual {v2, v3}, Lcom/miui/home/launcher/FolderInfo;->getAdapter(Landroid/content/Context;)Lcom/miui/home/launcher/ShortcutsAdapter;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/miui/home/launcher/ShortcutsAdapter;->getItem(I)Lcom/miui/home/launcher/ShortcutInfo;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/miui/home/launcher/folder/FolderIcon1x1;->onIconRemoved(Lcom/miui/home/launcher/ShortcutInfo;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onWallpaperColorChanged()V
    .locals 2

    invoke-super {p0}, Lcom/miui/home/launcher/FolderIcon;->onWallpaperColorChanged()V

    invoke-static {}, Lcom/miui/home/launcher/WallpaperUtils;->hasAppliedLightWallpaper()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mIconCache:Lcom/miui/home/launcher/IconCache;

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/folder/FolderIcon1x1;->getFolderIconLight(Lcom/miui/home/launcher/IconCache;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mIconCache:Lcom/miui/home/launcher/IconCache;

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/folder/FolderIcon1x1;->getFolderIcon(Lcom/miui/home/launcher/IconCache;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mFolderBackground:Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mFolderBackground:Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/miui/home/launcher/folder/FolderIcon1x1;->setIconImageView(Landroid/graphics/drawable/Drawable;Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public resetBackAnim()V
    .locals 2

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mPreviewContainer:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer1X1;

    if-nez v0, :cond_0

    const-string v1, "mPreviewContainer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer1X1;->setVisibility(I)V

    return-void
.end method

.method public showPreview(Z)V
    .locals 2

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mPreviewContainer:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer1X1;

    if-nez v0, :cond_0

    const-string v1, "mPreviewContainer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p0, p1}, Lcom/miui/home/launcher/folder/FolderIcon1x1;->getFolderPreviewAlpha(Z)F

    move-result p1

    invoke-virtual {v0, p1}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer1X1;->setAlpha(F)V

    iget-object p1, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mPreviewContainer:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer1X1;

    if-nez p1, :cond_1

    const-string v0, "mPreviewContainer"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p1}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer1X1;->invalidate()V

    iget-object p1, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mNoWordAdapter:Lcom/miui/home/launcher/util/noword/NoWordLauncherElementAdapter;

    invoke-virtual {p1}, Lcom/miui/home/launcher/util/noword/NoWordLauncherElementAdapter;->invalidateBindElementWhenLauncherInEditMode()V

    return-void
.end method

.method public updateBackAnim(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public updateSizeOnIconSizeChanged()V
    .locals 2

    invoke-super {p0}, Lcom/miui/home/launcher/FolderIcon;->updateSizeOnIconSizeChanged()V

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mPreviewContainer:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer1X1;

    if-nez v0, :cond_0

    const-string v1, "mPreviewContainer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer1X1;->requestLayout()V

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mImageView:Lcom/miui/home/launcher/LauncherIconImageView;

    if-nez v0, :cond_1

    const-string v1, "mImageView"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0}, Lcom/miui/home/launcher/LauncherIconImageView;->requestLayout()V

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIcon1x1;->mFolderCover:Landroid/widget/ImageView;

    if-nez v0, :cond_2

    const-string v1, "mFolderCover"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v0}, Landroid/widget/ImageView;->requestLayout()V

    return-void
.end method
