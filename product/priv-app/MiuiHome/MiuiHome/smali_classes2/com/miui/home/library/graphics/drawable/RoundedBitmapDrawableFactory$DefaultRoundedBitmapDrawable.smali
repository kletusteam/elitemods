.class Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawableFactory$DefaultRoundedBitmapDrawable;
.super Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawableFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DefaultRoundedBitmapDrawable"
.end annotation


# direct methods
.method constructor <init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    return-void
.end method


# virtual methods
.method gravityCompatApply(IIILandroid/graphics/Rect;Landroid/graphics/Rect;)V
    .locals 6

    goto/32 :goto_4

    nop

    :goto_0
    move v1, p2

    goto/32 :goto_6

    nop

    :goto_1
    move v0, p1

    goto/32 :goto_0

    nop

    :goto_2
    return-void

    :goto_3
    move-object v4, p5

    goto/32 :goto_5

    nop

    :goto_4
    const/4 v5, 0x0

    goto/32 :goto_1

    nop

    :goto_5
    invoke-static/range {v0 .. v5}, Landroid/view/Gravity;->apply(IIILandroid/graphics/Rect;Landroid/graphics/Rect;I)V

    goto/32 :goto_2

    nop

    :goto_6
    move v2, p3

    goto/32 :goto_7

    nop

    :goto_7
    move-object v3, p4

    goto/32 :goto_3

    nop
.end method
