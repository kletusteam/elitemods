.class public final Lcom/miui/home/launcher/folder/LauncherFolder2x2IconImageView;
.super Lcom/miui/home/launcher/LauncherIconImageView;

# interfaces
.implements Lcom/miui/home/launcher/folder/IFolderContainerAnimationAble;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nLauncherFolder2x2IconImageView.kt\nKotlin\n*S Kotlin\n*F\n+ 1 LauncherFolder2x2IconImageView.kt\ncom/miui/home/launcher/folder/LauncherFolder2x2IconImageView\n*L\n1#1,178:1\n*E\n"
.end annotation


# instance fields
.field private final TAG:Ljava/lang/String;

.field private mAlphaAnimCount:I

.field private final mAlphaAnimDelayTime:J

.field private final mAlphaAnimHideAlpha:F

.field private final mAlphaAnimNormalAlpha:F

.field private final mAlphaAnimShowAlpha:F

.field private final mAlphaAnimStartDelayTime:J

.field private final mAnimMax:I

.field private final mIsDebug:Z

.field private mIsDragingEnter:Z

.field private mPreDrawable:Landroid/graphics/drawable/Drawable;

.field private final mViewEaseHideAlpha:Lmiuix/animation/utils/EaseManager$EaseStyle;

.field private final mViewEaseShowAlpha:Lmiuix/animation/utils/EaseManager$EaseStyle;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/miui/home/launcher/folder/LauncherFolder2x2IconImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/miui/home/launcher/folder/LauncherFolder2x2IconImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/miui/home/launcher/LauncherIconImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 p1, 0x0

    new-array p2, p1, [F

    const/4 p3, 0x6

    invoke-static {p3, p2}, Lmiuix/animation/utils/EaseManager;->getStyle(I[F)Lmiuix/animation/utils/EaseManager$EaseStyle;

    move-result-object p2

    iput-object p2, p0, Lcom/miui/home/launcher/folder/LauncherFolder2x2IconImageView;->mViewEaseHideAlpha:Lmiuix/animation/utils/EaseManager$EaseStyle;

    new-array p1, p1, [F

    const/16 p2, 0xf

    invoke-static {p2, p1}, Lmiuix/animation/utils/EaseManager;->getStyle(I[F)Lmiuix/animation/utils/EaseManager$EaseStyle;

    move-result-object p1

    iput-object p1, p0, Lcom/miui/home/launcher/folder/LauncherFolder2x2IconImageView;->mViewEaseShowAlpha:Lmiuix/animation/utils/EaseManager$EaseStyle;

    const/4 p1, 0x5

    iput p1, p0, Lcom/miui/home/launcher/folder/LauncherFolder2x2IconImageView;->mAnimMax:I

    const-string p1, "LauncherFolder2x2IconImageView"

    iput-object p1, p0, Lcom/miui/home/launcher/folder/LauncherFolder2x2IconImageView;->TAG:Ljava/lang/String;

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/miui/home/launcher/folder/LauncherFolder2x2IconImageView;->mIsDebug:Z

    const-wide/16 p1, 0x190

    iput-wide p1, p0, Lcom/miui/home/launcher/folder/LauncherFolder2x2IconImageView;->mAlphaAnimStartDelayTime:J

    const-wide/16 p1, 0xc8

    iput-wide p1, p0, Lcom/miui/home/launcher/folder/LauncherFolder2x2IconImageView;->mAlphaAnimDelayTime:J

    const p1, 0x3f59999a    # 0.85f

    iput p1, p0, Lcom/miui/home/launcher/folder/LauncherFolder2x2IconImageView;->mAlphaAnimHideAlpha:F

    const p1, 0x3f333333    # 0.7f

    iput p1, p0, Lcom/miui/home/launcher/folder/LauncherFolder2x2IconImageView;->mAlphaAnimShowAlpha:F

    const/high16 p1, 0x3f000000    # 0.5f

    iput p1, p0, Lcom/miui/home/launcher/folder/LauncherFolder2x2IconImageView;->mAlphaAnimNormalAlpha:F

    sget-object p1, Lcom/miui/home/launcher/folder/LauncherFolderIconSizeProvider;->Companion:Lcom/miui/home/launcher/folder/LauncherFolderIconSizeProvider$Companion;

    invoke-virtual {p1}, Lcom/miui/home/launcher/folder/LauncherFolderIconSizeProvider$Companion;->getInstance()Lcom/miui/home/launcher/folder/LauncherFolderIconSizeProvider;

    move-result-object p1

    check-cast p1, Lcom/miui/home/launcher/common/IconSizeProvider;

    iput-object p1, p0, Lcom/miui/home/launcher/folder/LauncherFolder2x2IconImageView;->mIconSizeProvider:Lcom/miui/home/launcher/common/IconSizeProvider;

    return-void
.end method

.method public static final synthetic access$getMAlphaAnimCount$p(Lcom/miui/home/launcher/folder/LauncherFolder2x2IconImageView;)I
    .locals 0

    iget p0, p0, Lcom/miui/home/launcher/folder/LauncherFolder2x2IconImageView;->mAlphaAnimCount:I

    return p0
.end method

.method public static final synthetic access$getMAnimMax$p(Lcom/miui/home/launcher/folder/LauncherFolder2x2IconImageView;)I
    .locals 0

    iget p0, p0, Lcom/miui/home/launcher/folder/LauncherFolder2x2IconImageView;->mAnimMax:I

    return p0
.end method

.method public static final synthetic access$getMIsDragingEnter$p(Lcom/miui/home/launcher/folder/LauncherFolder2x2IconImageView;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/miui/home/launcher/folder/LauncherFolder2x2IconImageView;->mIsDragingEnter:Z

    return p0
.end method

.method public static final synthetic access$preformAnimInternal(Lcom/miui/home/launcher/folder/LauncherFolder2x2IconImageView;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/home/launcher/folder/LauncherFolder2x2IconImageView;->preformAnimInternal()V

    return-void
.end method

.method public static final synthetic access$resetDrawableToNormalState(Lcom/miui/home/launcher/folder/LauncherFolder2x2IconImageView;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/home/launcher/folder/LauncherFolder2x2IconImageView;->resetDrawableToNormalState()V

    return-void
.end method

.method public static final synthetic access$setMAlphaAnimCount$p(Lcom/miui/home/launcher/folder/LauncherFolder2x2IconImageView;I)V
    .locals 0

    iput p1, p0, Lcom/miui/home/launcher/folder/LauncherFolder2x2IconImageView;->mAlphaAnimCount:I

    return-void
.end method

.method private final animEnd()V
    .locals 2

    const/4 v0, -0x1

    iput v0, p0, Lcom/miui/home/launcher/folder/LauncherFolder2x2IconImageView;->mAlphaAnimCount:I

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/miui/home/launcher/folder/LauncherFolder2x2IconImageView;->onDragContainerBgAnimAlpha(ZZ)V

    return-void
.end method

.method private final animStart()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/miui/home/launcher/folder/LauncherFolder2x2IconImageView;->mAlphaAnimCount:I

    invoke-direct {p0}, Lcom/miui/home/launcher/folder/LauncherFolder2x2IconImageView;->preformAnimInternal()V

    return-void
.end method

.method private final customTask(Lkotlin/jvm/functions/Function0;J)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;J)V"
        }
    .end annotation

    new-instance v0, Lcom/miui/home/launcher/folder/LauncherFolder2x2IconImageView$customTask$1;

    invoke-direct {v0, p0, p1}, Lcom/miui/home/launcher/folder/LauncherFolder2x2IconImageView$customTask$1;-><init>(Lcom/miui/home/launcher/folder/LauncherFolder2x2IconImageView;Lkotlin/jvm/functions/Function0;)V

    check-cast v0, Ljava/lang/Runnable;

    invoke-virtual {p0, v0, p2, p3}, Lcom/miui/home/launcher/folder/LauncherFolder2x2IconImageView;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method private final onDragContainerBgAnimAlpha(ZZ)V
    .locals 7

    iget-boolean v0, p0, Lcom/miui/home/launcher/folder/LauncherFolder2x2IconImageView;->mIsDebug:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/miui/home/launcher/folder/LauncherFolder2x2IconImageView;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onDragContainerBgAnimAlpha,mAlphaAnimCount = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/miui/home/launcher/folder/LauncherFolder2x2IconImageView;->mAlphaAnimCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ", time = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    if-eqz p1, :cond_1

    iget v0, p0, Lcom/miui/home/launcher/folder/LauncherFolder2x2IconImageView;->mAlphaAnimHideAlpha:F

    goto :goto_0

    :cond_1
    iget v0, p0, Lcom/miui/home/launcher/folder/LauncherFolder2x2IconImageView;->mAlphaAnimShowAlpha:F

    :goto_0
    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/Object;

    const-string v3, "LauncherFolder2x2IconImageView"

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-static {v2}, Lmiuix/animation/Folme;->useValue([Ljava/lang/Object;)Lmiuix/animation/IStateStyle;

    move-result-object v2

    const/4 v3, 0x2

    new-array v5, v3, [Ljava/lang/Object;

    const-string v6, "animAlpha"

    aput-object v6, v5, v4

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/LauncherFolder2x2IconImageView;->getAnimAlpha()F

    move-result v6

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-interface {v2, v5}, Lmiuix/animation/IStateStyle;->setTo([Ljava/lang/Object;)Lmiuix/animation/IStateStyle;

    move-result-object v2

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const-string v6, "animAlpha"

    aput-object v6, v5, v4

    if-eqz p2, :cond_2

    iget v0, p0, Lcom/miui/home/launcher/folder/LauncherFolder2x2IconImageView;->mAlphaAnimNormalAlpha:F

    :cond_2
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    aput-object v0, v5, v1

    new-instance v0, Lmiuix/animation/base/AnimConfig;

    invoke-direct {v0}, Lmiuix/animation/base/AnimConfig;-><init>()V

    if-eqz p1, :cond_3

    iget-object v6, p0, Lcom/miui/home/launcher/folder/LauncherFolder2x2IconImageView;->mViewEaseHideAlpha:Lmiuix/animation/utils/EaseManager$EaseStyle;

    goto :goto_1

    :cond_3
    iget-object v6, p0, Lcom/miui/home/launcher/folder/LauncherFolder2x2IconImageView;->mViewEaseShowAlpha:Lmiuix/animation/utils/EaseManager$EaseStyle;

    :goto_1
    iput-object v6, v0, Lmiuix/animation/base/AnimConfig;->ease:Lmiuix/animation/utils/EaseManager$EaseStyle;

    new-array v1, v1, [Lmiuix/animation/listener/TransitionListener;

    new-instance v6, Lcom/miui/home/launcher/folder/LauncherFolder2x2IconImageView$onDragContainerBgAnimAlpha$$inlined$let$lambda$1;

    invoke-direct {v6, p0, p1, p2}, Lcom/miui/home/launcher/folder/LauncherFolder2x2IconImageView$onDragContainerBgAnimAlpha$$inlined$let$lambda$1;-><init>(Lcom/miui/home/launcher/folder/LauncherFolder2x2IconImageView;ZZ)V

    check-cast v6, Lmiuix/animation/listener/TransitionListener;

    aput-object v6, v1, v4

    invoke-virtual {v0, v1}, Lmiuix/animation/base/AnimConfig;->addListeners([Lmiuix/animation/listener/TransitionListener;)Lmiuix/animation/base/AnimConfig;

    move-result-object p1

    aput-object p1, v5, v3

    invoke-interface {v2, v5}, Lmiuix/animation/IStateStyle;->to([Ljava/lang/Object;)Lmiuix/animation/IStateStyle;

    return-void
.end method

.method static synthetic onDragContainerBgAnimAlpha$default(Lcom/miui/home/launcher/folder/LauncherFolder2x2IconImageView;ZZILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/miui/home/launcher/folder/LauncherFolder2x2IconImageView;->onDragContainerBgAnimAlpha(ZZ)V

    return-void
.end method

.method private final preformAnimInternal()V
    .locals 3

    new-instance v0, Lcom/miui/home/launcher/folder/LauncherFolder2x2IconImageView$preformAnimInternal$1;

    invoke-direct {v0, p0}, Lcom/miui/home/launcher/folder/LauncherFolder2x2IconImageView$preformAnimInternal$1;-><init>(Lcom/miui/home/launcher/folder/LauncherFolder2x2IconImageView;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    iget v1, p0, Lcom/miui/home/launcher/folder/LauncherFolder2x2IconImageView;->mAlphaAnimCount:I

    if-nez v1, :cond_0

    iget-wide v1, p0, Lcom/miui/home/launcher/folder/LauncherFolder2x2IconImageView;->mAlphaAnimStartDelayTime:J

    goto :goto_0

    :cond_0
    iget-wide v1, p0, Lcom/miui/home/launcher/folder/LauncherFolder2x2IconImageView;->mAlphaAnimDelayTime:J

    :goto_0
    invoke-direct {p0, v0, v1, v2}, Lcom/miui/home/launcher/folder/LauncherFolder2x2IconImageView;->customTask(Lkotlin/jvm/functions/Function0;J)V

    return-void
.end method

.method private final resetDrawableToNormalState()V
    .locals 2

    iget-object v0, p0, Lcom/miui/home/launcher/folder/LauncherFolder2x2IconImageView;->mPreDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/miui/home/launcher/folder/FolderIcon4x4NormalBackgroundDrawable;

    if-eqz v1, :cond_1

    if-eqz v0, :cond_0

    check-cast v0, Lcom/miui/home/launcher/folder/FolderIcon4x4NormalBackgroundDrawable;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/miui/home/launcher/folder/FolderIcon4x4NormalBackgroundDrawable;->setAnimState(Z)V

    goto :goto_0

    :cond_0
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type com.miui.home.launcher.folder.FolderIcon4x4NormalBackgroundDrawable"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    :goto_0
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/folder/LauncherFolder2x2IconImageView;->setAnimAlpha(F)V

    return-void
.end method

.method private final settingDrawableToAnimState()V
    .locals 2

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/LauncherFolder2x2IconImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/home/launcher/folder/LauncherFolder2x2IconImageView;->mPreDrawable:Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/miui/home/launcher/folder/LauncherFolder2x2IconImageView;->mPreDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/miui/home/launcher/folder/FolderIcon4x4NormalBackgroundDrawable;

    if-eqz v1, :cond_1

    if-eqz v0, :cond_0

    check-cast v0, Lcom/miui/home/launcher/folder/FolderIcon4x4NormalBackgroundDrawable;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/miui/home/launcher/folder/FolderIcon4x4NormalBackgroundDrawable;->setAnimState(Z)V

    goto :goto_0

    :cond_0
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type com.miui.home.launcher.folder.FolderIcon4x4NormalBackgroundDrawable"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    :goto_0
    iget v0, p0, Lcom/miui/home/launcher/folder/LauncherFolder2x2IconImageView;->mAlphaAnimNormalAlpha:F

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/folder/LauncherFolder2x2IconImageView;->setAnimAlpha(F)V

    return-void
.end method


# virtual methods
.method public final getAnimAlpha()F
    .locals 1

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/LauncherFolder2x2IconImageView;->getAlpha()F

    move-result v0

    return v0
.end method

.method protected getHeightMeasureSpec(I)I
    .locals 0

    return p1
.end method

.method protected getWidthMeasureSpec(I)I
    .locals 0

    return p1
.end method

.method public onDragEnter()V
    .locals 3

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/home/launcher/folder/LauncherFolder2x2IconImageView;->mIsDragingEnter:Z

    invoke-direct {p0}, Lcom/miui/home/launcher/folder/LauncherFolder2x2IconImageView;->settingDrawableToAnimState()V

    sget-object v1, Lcom/miui/home/launcher/folder/FolderAnimHelper;->Companion:Lcom/miui/home/launcher/folder/FolderAnimHelper$Companion;

    move-object v2, p0

    check-cast v2, Landroid/view/View;

    invoke-virtual {v1, v2, v0}, Lcom/miui/home/launcher/folder/FolderAnimHelper$Companion;->scaleIconContainerBg(Landroid/view/View;Z)V

    invoke-direct {p0}, Lcom/miui/home/launcher/folder/LauncherFolder2x2IconImageView;->animStart()V

    return-void
.end method

.method public onDragExit()V
    .locals 3

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/miui/home/launcher/folder/LauncherFolder2x2IconImageView;->mIsDragingEnter:Z

    sget-object v1, Lcom/miui/home/launcher/folder/FolderAnimHelper;->Companion:Lcom/miui/home/launcher/folder/FolderAnimHelper$Companion;

    move-object v2, p0

    check-cast v2, Landroid/view/View;

    invoke-virtual {v1, v2, v0}, Lcom/miui/home/launcher/folder/FolderAnimHelper$Companion;->scaleIconContainerBg(Landroid/view/View;Z)V

    invoke-direct {p0}, Lcom/miui/home/launcher/folder/LauncherFolder2x2IconImageView;->animEnd()V

    return-void
.end method

.method public final setAnimAlpha(F)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/miui/home/launcher/folder/LauncherFolder2x2IconImageView;->setAlpha(F)V

    return-void
.end method
