.class public Lcom/miui/home/recents/views/MyTaskStackViewsAlgorithmHorizontal;
.super Lcom/miui/home/recents/views/TaskStackViewsAlgorithmHorizontal;


# static fields
.field public static isIosLeft:Z

.field public static isIosRight:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/home/recents/views/TaskStackViewsAlgorithmHorizontal;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public getTaskViewTransform(IFLcom/miui/home/recents/views/TaskViewTransform;)V
    .locals 11

    const/high16 v10, 0x42c80000    # 100.0f

    const/high16 v9, 0x41700000    # 15.0f

    const/4 v8, 0x1

    const/high16 v7, 0x3f800000    # 1.0f

    sget-boolean v5, Lcom/miui/home/recents/views/MyTaskStackViewsAlgorithmHorizontal;->isIosLeft:Z

    if-nez v5, :cond_0

    sget-boolean v5, Lcom/miui/home/recents/views/MyTaskStackViewsAlgorithmHorizontal;->isIosRight:Z

    if-nez v5, :cond_0

    invoke-super {p0, p1, p2, p3}, Lcom/miui/home/recents/views/TaskStackViewsAlgorithmHorizontal;->getTaskViewTransform(IFLcom/miui/home/recents/views/TaskViewTransform;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, p1}, Lcom/miui/home/recents/views/MyTaskStackViewsAlgorithmHorizontal;->calculateTaskViewOffsetXAndY(I)[I

    move-result-object v0

    const/4 v5, 0x0

    aget v5, v0, v5

    const/4 v6, 0x0

    invoke-virtual {p0, v6, p2}, Lcom/miui/home/recents/views/MyTaskStackViewsAlgorithmHorizontal;->getXForDeltaP(FF)I

    move-result v6

    add-int v4, v5, v6

    iget-object v5, p3, Lcom/miui/home/recents/views/TaskViewTransform;->rect:Landroid/graphics/RectF;

    iget-object v6, p0, Lcom/miui/home/recents/views/MyTaskStackViewsAlgorithmHorizontal;->mTaskViewRectF:Landroid/graphics/RectF;

    invoke-virtual {v5, v6}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    int-to-float v3, v4

    iget-object v5, p3, Lcom/miui/home/recents/views/TaskViewTransform;->rect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->left:F

    int-to-float v6, v4

    add-float v1, v5, v6

    const/high16 v2, 0x3f800000    # 1.0f

    sget-boolean v5, Lcom/miui/home/recents/views/MyTaskStackViewsAlgorithmHorizontal;->isIosLeft:Z

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/miui/home/recents/views/MyTaskStackViewsAlgorithmHorizontal;->mTaskViewRectF:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->left:F

    cmpg-float v5, v1, v5

    if-gez v5, :cond_1

    div-float/2addr v3, v9

    :cond_1
    iget-object v5, p3, Lcom/miui/home/recents/views/TaskViewTransform;->rect:Landroid/graphics/RectF;

    aget v6, v0, v8

    int-to-float v6, v6

    invoke-virtual {v5, v3, v6}, Landroid/graphics/RectF;->offset(FF)V

    iget-object v5, p3, Lcom/miui/home/recents/views/TaskViewTransform;->rect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->left:F

    iget-object v6, p0, Lcom/miui/home/recents/views/MyTaskStackViewsAlgorithmHorizontal;->mTaskViewRectF:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->left:F

    cmpg-float v5, v5, v6

    if-gez v5, :cond_2

    div-int/lit8 v5, v4, 0x19

    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v5

    int-to-float v5, v5

    iget-object v6, p0, Lcom/miui/home/recents/views/MyTaskStackViewsAlgorithmHorizontal;->mTaskViewRectF:Landroid/graphics/RectF;

    invoke-virtual {v6}, Landroid/graphics/RectF;->width()F

    move-result v6

    div-float/2addr v5, v6

    sub-float v2, v7, v5

    :cond_2
    cmpl-float v5, v2, v7

    if-lez v5, :cond_3

    const/high16 v2, 0x3f800000    # 1.0f

    :cond_3
    int-to-float v5, p1

    div-float/2addr v5, v10

    sub-float v5, v7, v5

    iput v5, p3, Lcom/miui/home/recents/views/TaskViewTransform;->translationZ:F

    :goto_1
    iput v2, p3, Lcom/miui/home/recents/views/TaskViewTransform;->scale:F

    iput v2, p3, Lcom/miui/home/recents/views/TaskViewTransform;->alpha:F

    iput-boolean v8, p3, Lcom/miui/home/recents/views/TaskViewTransform;->visible:Z

    goto :goto_0

    :cond_4
    iget-object v5, p0, Lcom/miui/home/recents/views/MyTaskStackViewsAlgorithmHorizontal;->mTaskViewRectF:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->left:F

    cmpl-float v5, v1, v5

    if-lez v5, :cond_5

    div-float/2addr v3, v9

    :cond_5
    iget-object v5, p3, Lcom/miui/home/recents/views/TaskViewTransform;->rect:Landroid/graphics/RectF;

    aget v6, v0, v8

    int-to-float v6, v6

    invoke-virtual {v5, v3, v6}, Landroid/graphics/RectF;->offset(FF)V

    iget-object v5, p3, Lcom/miui/home/recents/views/TaskViewTransform;->rect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->right:F

    iget-object v6, p0, Lcom/miui/home/recents/views/MyTaskStackViewsAlgorithmHorizontal;->mTaskViewRectF:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->right:F

    cmpl-float v5, v5, v6

    if-lez v5, :cond_6

    div-int/lit8 v5, v4, 0x19

    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v5

    int-to-float v5, v5

    iget-object v6, p0, Lcom/miui/home/recents/views/MyTaskStackViewsAlgorithmHorizontal;->mTaskViewRectF:Landroid/graphics/RectF;

    invoke-virtual {v6}, Landroid/graphics/RectF;->width()F

    move-result v6

    div-float/2addr v5, v6

    sub-float v2, v7, v5

    :cond_6
    cmpl-float v5, v2, v7

    if-lez v5, :cond_7

    const/high16 v2, 0x3f800000    # 1.0f

    :cond_7
    add-int/lit8 v5, p1, 0x1

    int-to-float v5, v5

    div-float/2addr v5, v10

    iput v5, p3, Lcom/miui/home/recents/views/TaskViewTransform;->translationZ:F

    goto :goto_1
.end method
