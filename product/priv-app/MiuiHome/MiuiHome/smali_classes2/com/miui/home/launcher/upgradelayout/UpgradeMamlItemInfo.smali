.class public Lcom/miui/home/launcher/upgradelayout/UpgradeMamlItemInfo;
.super Lcom/miui/home/launcher/upgradelayout/UpgradeLayoutItemInfo;


# instance fields
.field public mPickerId:Ljava/lang/String;

.field public mProductId:Ljava/lang/String;

.field public mUri:Ljava/lang/String;

.field public mVersionCode:I


# direct methods
.method public constructor <init>(IIIIJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V
    .locals 14

    move-object v13, p0

    const-wide/16 v7, -0x64

    const/4 v10, 0x0

    move-object v0, p0

    move v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move/from16 v4, p4

    move-wide/from16 v5, p5

    move-object/from16 v9, p7

    move-object/from16 v11, p13

    move/from16 v12, p11

    invoke-direct/range {v0 .. v12}, Lcom/miui/home/launcher/upgradelayout/UpgradeLayoutItemInfo;-><init>(IIIIJJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    move-object/from16 v0, p8

    iput-object v0, v13, Lcom/miui/home/launcher/upgradelayout/UpgradeMamlItemInfo;->mProductId:Ljava/lang/String;

    move-object/from16 v0, p9

    iput-object v0, v13, Lcom/miui/home/launcher/upgradelayout/UpgradeMamlItemInfo;->mUri:Ljava/lang/String;

    move/from16 v0, p10

    iput v0, v13, Lcom/miui/home/launcher/upgradelayout/UpgradeMamlItemInfo;->mVersionCode:I

    move-object/from16 v0, p12

    iput-object v0, v13, Lcom/miui/home/launcher/upgradelayout/UpgradeMamlItemInfo;->mPickerId:Ljava/lang/String;

    return-void
.end method

.method private addMamlWidget(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 4

    iget-object v0, p0, Lcom/miui/home/launcher/upgradelayout/UpgradeMamlItemInfo;->mValues:Landroid/content/ContentValues;

    const-string v1, "spanX"

    iget v2, p0, Lcom/miui/home/launcher/upgradelayout/UpgradeMamlItemInfo;->mSpanX:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    iget-object v0, p0, Lcom/miui/home/launcher/upgradelayout/UpgradeMamlItemInfo;->mValues:Landroid/content/ContentValues;

    const-string v1, "spanY"

    iget v2, p0, Lcom/miui/home/launcher/upgradelayout/UpgradeMamlItemInfo;->mSpanY:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    iget-object v0, p0, Lcom/miui/home/launcher/upgradelayout/UpgradeMamlItemInfo;->mValues:Landroid/content/ContentValues;

    const-string v1, "itemFlags"

    iget v2, p0, Lcom/miui/home/launcher/upgradelayout/UpgradeMamlItemInfo;->mItemFlags:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    iget-object v0, p0, Lcom/miui/home/launcher/upgradelayout/UpgradeMamlItemInfo;->mValues:Landroid/content/ContentValues;

    const-string v1, "itemType"

    const/16 v2, 0x13

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    iget-object v0, p0, Lcom/miui/home/launcher/upgradelayout/UpgradeMamlItemInfo;->mValues:Landroid/content/ContentValues;

    const-string v1, "product_id"

    iget-object v2, p0, Lcom/miui/home/launcher/upgradelayout/UpgradeMamlItemInfo;->mProductId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/miui/home/launcher/upgradelayout/UpgradeMamlItemInfo;->mValues:Landroid/content/ContentValues;

    const-string v1, "title"

    iget-object v2, p0, Lcom/miui/home/launcher/upgradelayout/UpgradeMamlItemInfo;->mTitle:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/miui/home/launcher/Application;->getInstance()Lcom/miui/home/launcher/Application;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/home/launcher/upgradelayout/UpgradeMamlItemInfo;->mUri:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/home/launcher/upgradelayout/UpgradeMamlItemInfo;->mProductId:Ljava/lang/String;

    iget v3, p0, Lcom/miui/home/launcher/upgradelayout/UpgradeMamlItemInfo;->mVersionCode:I

    invoke-static {v0, v1, v2, v3}, Lcom/miui/home/launcher/widget/MIUIWidgetCompat;->installMaMlFromExternal(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/miui/home/launcher/upgradelayout/UpgradeMamlItemInfo;->mValues:Landroid/content/ContentValues;

    const-string v1, "uri"

    invoke-direct {p0}, Lcom/miui/home/launcher/upgradelayout/UpgradeMamlItemInfo;->getMamlInfo()Lcom/miui/home/launcher/maml/MaMlWidgetInfo;

    move-result-object v2

    invoke-static {v2}, Lcom/miui/home/launcher/widget/MIUIWidgetCompat;->getMaMlResPath(Lcom/miui/home/launcher/maml/MaMlWidgetInfo;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const-string v0, "favorites"

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/miui/home/launcher/upgradelayout/UpgradeMamlItemInfo;->mValues:Landroid/content/ContentValues;

    invoke-static {p1, v0, v1, v2}, Lcom/miui/home/launcher/LauncherProvider;->safelyInsertDatabase(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    return-void
.end method

.method private getMamlInfo()Lcom/miui/home/launcher/maml/MaMlWidgetInfo;
    .locals 5

    new-instance v0, Lcom/miui/home/launcher/maml/MaMlWidgetInfo;

    invoke-direct {v0}, Lcom/miui/home/launcher/maml/MaMlWidgetInfo;-><init>()V

    iget v1, p0, Lcom/miui/home/launcher/upgradelayout/UpgradeMamlItemInfo;->mSpanX:I

    iput v1, v0, Lcom/miui/home/launcher/maml/MaMlWidgetInfo;->configSpanX:I

    iget v1, p0, Lcom/miui/home/launcher/upgradelayout/UpgradeMamlItemInfo;->mSpanY:I

    iput v1, v0, Lcom/miui/home/launcher/maml/MaMlWidgetInfo;->configSpanY:I

    iget-object v1, p0, Lcom/miui/home/launcher/upgradelayout/UpgradeMamlItemInfo;->mProductId:Ljava/lang/String;

    iput-object v1, v0, Lcom/miui/home/launcher/maml/MaMlWidgetInfo;->productId:Ljava/lang/String;

    iget-object v1, p0, Lcom/miui/home/launcher/upgradelayout/UpgradeMamlItemInfo;->mPickerId:Ljava/lang/String;

    iput-object v1, v0, Lcom/miui/home/launcher/maml/MaMlWidgetInfo;->pickerID:Ljava/lang/String;

    const/16 v1, 0x3f3

    iput v1, v0, Lcom/miui/home/launcher/maml/MaMlWidgetInfo;->addSource:I

    iget v1, p0, Lcom/miui/home/launcher/upgradelayout/UpgradeMamlItemInfo;->mVersionCode:I

    iput v1, v0, Lcom/miui/home/launcher/maml/MaMlWidgetInfo;->versionCode:I

    iget-object v1, p0, Lcom/miui/home/launcher/upgradelayout/UpgradeMamlItemInfo;->mTitle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/miui/home/launcher/maml/MaMlWidgetInfo;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/miui/home/launcher/upgradelayout/UpgradeMamlItemInfo;->mUri:Ljava/lang/String;

    iput-object v1, v0, Lcom/miui/home/launcher/maml/MaMlWidgetInfo;->maMlDownloadUrl:Ljava/lang/String;

    invoke-static {}, Lcom/miui/home/launcher/widget/MIUIWidgetCompat;->allocMaMlWidgetId()I

    move-result v1

    iput v1, v0, Lcom/miui/home/launcher/maml/MaMlWidgetInfo;->gadgetId:I

    iget-object v1, p0, Lcom/miui/home/launcher/upgradelayout/UpgradeMamlItemInfo;->mPackageName:Ljava/lang/String;

    iput-object v1, v0, Lcom/miui/home/launcher/maml/MaMlWidgetInfo;->appPackage:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/miui/home/launcher/maml/MaMlWidgetInfo;->getExtraIntentParams()Landroid/content/Intent;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/home/launcher/upgradelayout/UpgradeMamlItemInfo;->mValues:Landroid/content/ContentValues;

    const-string v3, "intent"

    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/miui/home/launcher/upgradelayout/UpgradeMamlItemInfo;->mValues:Landroid/content/ContentValues;

    const-string v2, "appWidgetId"

    iget v3, v0, Lcom/miui/home/launcher/maml/MaMlWidgetInfo;->gadgetId:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    return-object v0
.end method


# virtual methods
.method public parse(Lcom/miui/home/launcher/upgradelayout/UpgradeLayoutContext;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/miui/home/launcher/upgradelayout/UpgradeLayoutItemInfo;->parse(Lcom/miui/home/launcher/upgradelayout/UpgradeLayoutContext;)V

    iget-object p1, p1, Lcom/miui/home/launcher/upgradelayout/UpgradeLayoutContext;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-direct {p0, p1}, Lcom/miui/home/launcher/upgradelayout/UpgradeMamlItemInfo;->addMamlWidget(Landroid/database/sqlite/SQLiteDatabase;)V

    return-void
.end method
