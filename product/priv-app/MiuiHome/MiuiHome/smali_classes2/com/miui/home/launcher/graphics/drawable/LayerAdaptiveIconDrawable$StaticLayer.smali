.class abstract Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$StaticLayer;
.super Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$Layer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x408
    name = "StaticLayer"
.end annotation


# instance fields
.field private final mPaint:Landroid/graphics/Paint;


# direct methods
.method private constructor <init>(Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable;Landroid/graphics/drawable/Drawable;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$Layer;-><init>(Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable;Landroid/graphics/drawable/Drawable;ILcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$1;)V

    new-instance p1, Landroid/graphics/Paint;

    const/4 p2, 0x7

    invoke-direct {p1, p2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object p1, p0, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$StaticLayer;->mPaint:Landroid/graphics/Paint;

    return-void
.end method

.method synthetic constructor <init>(Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable;Landroid/graphics/drawable/Drawable;ILcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$1;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$StaticLayer;-><init>(Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable;Landroid/graphics/drawable/Drawable;I)V

    return-void
.end method


# virtual methods
.method abstract getShader()Landroid/graphics/Shader;
.end method

.method onDraw(Landroid/graphics/Canvas;Landroid/graphics/Path;)V
    .locals 3

    goto/32 :goto_6

    nop

    :goto_0
    iget-object v1, p0, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$StaticLayer;->mPaint:Landroid/graphics/Paint;

    goto/32 :goto_9

    nop

    :goto_1
    iget v0, v0, Landroid/graphics/Rect;->top:I

    goto/32 :goto_b

    nop

    :goto_2
    int-to-float v2, v2

    goto/32 :goto_14

    nop

    :goto_3
    iget v1, v0, Landroid/graphics/Rect;->left:I

    goto/32 :goto_13

    nop

    :goto_4
    invoke-virtual {p1, p2, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto/32 :goto_8

    nop

    :goto_5
    int-to-float p2, p2

    goto/32 :goto_1

    nop

    :goto_6
    invoke-virtual {p0}, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$StaticLayer;->getShader()Landroid/graphics/Shader;

    move-result-object v0

    goto/32 :goto_d

    nop

    :goto_7
    iget-object v1, p0, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$StaticLayer;->mPaint:Landroid/graphics/Paint;

    goto/32 :goto_4

    nop

    :goto_8
    iget p2, v0, Landroid/graphics/Rect;->left:I

    goto/32 :goto_c

    nop

    :goto_9
    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    :goto_a
    goto/32 :goto_17

    nop

    :goto_b
    neg-int v0, v0

    goto/32 :goto_f

    nop

    :goto_c
    neg-int p2, p2

    goto/32 :goto_5

    nop

    :goto_d
    if-eqz v0, :cond_0

    goto/32 :goto_16

    :cond_0
    goto/32 :goto_15

    nop

    :goto_e
    invoke-virtual {v1}, Landroid/graphics/Paint;->getShader()Landroid/graphics/Shader;

    move-result-object v1

    goto/32 :goto_1b

    nop

    :goto_f
    int-to-float v0, v0

    goto/32 :goto_18

    nop

    :goto_10
    return-void

    :goto_11
    iget v2, v0, Landroid/graphics/Rect;->top:I

    goto/32 :goto_2

    nop

    :goto_12
    invoke-virtual {p0}, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$StaticLayer;->getParentBounds()Landroid/graphics/Rect;

    move-result-object v0

    goto/32 :goto_3

    nop

    :goto_13
    int-to-float v1, v1

    goto/32 :goto_11

    nop

    :goto_14
    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    goto/32 :goto_7

    nop

    :goto_15
    return-void

    :goto_16
    goto/32 :goto_1a

    nop

    :goto_17
    if-nez p2, :cond_1

    goto/32 :goto_19

    :cond_1
    goto/32 :goto_12

    nop

    :goto_18
    invoke-virtual {p1, p2, v0}, Landroid/graphics/Canvas;->translate(FF)V

    :goto_19
    goto/32 :goto_10

    nop

    :goto_1a
    iget-object v1, p0, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$StaticLayer;->mPaint:Landroid/graphics/Paint;

    goto/32 :goto_e

    nop

    :goto_1b
    if-ne v1, v0, :cond_2

    goto/32 :goto_a

    :cond_2
    goto/32 :goto_0

    nop
.end method
