.class Lcom/miui/home/recents/util/SplitSelectStateController$RemoteSplitLaunchAnimationRunner;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/android/systemui/shared/recents/system/RemoteAnimationRunnerCompat;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/home/recents/util/SplitSelectStateController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RemoteSplitLaunchAnimationRunner"
.end annotation


# instance fields
.field private mAnimate:Z

.field private final mInitialTaskId:I

.field private final mInitialTaskPendingIntent:Landroid/app/PendingIntent;

.field private final mSecondTaskId:I

.field private final mSuccessCallback:Ljava/util/function/Consumer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/function/Consumer<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/miui/home/recents/util/SplitSelectStateController;


# direct methods
.method constructor <init>(Lcom/miui/home/recents/util/SplitSelectStateController;ZILandroid/app/PendingIntent;ILjava/util/function/Consumer;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZI",
            "Landroid/app/PendingIntent;",
            "I",
            "Ljava/util/function/Consumer<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/miui/home/recents/util/SplitSelectStateController$RemoteSplitLaunchAnimationRunner;->this$0:Lcom/miui/home/recents/util/SplitSelectStateController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p2, p0, Lcom/miui/home/recents/util/SplitSelectStateController$RemoteSplitLaunchAnimationRunner;->mAnimate:Z

    iput p3, p0, Lcom/miui/home/recents/util/SplitSelectStateController$RemoteSplitLaunchAnimationRunner;->mInitialTaskId:I

    iput-object p4, p0, Lcom/miui/home/recents/util/SplitSelectStateController$RemoteSplitLaunchAnimationRunner;->mInitialTaskPendingIntent:Landroid/app/PendingIntent;

    iput p5, p0, Lcom/miui/home/recents/util/SplitSelectStateController$RemoteSplitLaunchAnimationRunner;->mSecondTaskId:I

    iput-object p6, p0, Lcom/miui/home/recents/util/SplitSelectStateController$RemoteSplitLaunchAnimationRunner;->mSuccessCallback:Ljava/util/function/Consumer;

    return-void
.end method

.method public static synthetic lambda$onAnimationCancelled$2(Lcom/miui/home/recents/util/SplitSelectStateController$RemoteSplitLaunchAnimationRunner;)V
    .locals 2

    iget-object v0, p0, Lcom/miui/home/recents/util/SplitSelectStateController$RemoteSplitLaunchAnimationRunner;->mSuccessCallback:Ljava/util/function/Consumer;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/miui/home/recents/util/SplitSelectStateController$RemoteSplitLaunchAnimationRunner;->this$0:Lcom/miui/home/recents/util/SplitSelectStateController;

    invoke-static {v1}, Lcom/miui/home/recents/util/SplitSelectStateController;->access$100(Lcom/miui/home/recents/util/SplitSelectStateController;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/function/Consumer;->accept(Ljava/lang/Object;)V

    :cond_0
    iget-object v0, p0, Lcom/miui/home/recents/util/SplitSelectStateController$RemoteSplitLaunchAnimationRunner;->this$0:Lcom/miui/home/recents/util/SplitSelectStateController;

    invoke-virtual {v0}, Lcom/miui/home/recents/util/SplitSelectStateController;->resetState()V

    return-void
.end method

.method public static synthetic lambda$onAnimationStart$0(Lcom/miui/home/recents/util/SplitSelectStateController$RemoteSplitLaunchAnimationRunner;[Lcom/android/systemui/shared/recents/system/RemoteAnimationTargetCompat;Ljava/lang/Runnable;)V
    .locals 4

    invoke-static {}, Lcom/miui/home/launcher/Application;->getLauncher()Lcom/miui/home/launcher/Launcher;

    move-result-object v0

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    const/4 v2, 0x0

    invoke-virtual {v0}, Lcom/miui/home/launcher/Launcher;->getAppTransitionManager()Lcom/miui/home/recents/LauncherAppTransitionManager;

    move-result-object v3

    instance-of v3, v3, Lcom/miui/home/recents/QuickstepAppTransitionManagerImpl;

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lcom/miui/home/launcher/Launcher;->getAppTransitionManager()Lcom/miui/home/recents/LauncherAppTransitionManager;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/miui/home/recents/QuickstepAppTransitionManagerImpl;

    :cond_0
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/miui/home/recents/QuickstepAppTransitionManagerImpl;->isDoAnimationFinish()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "SplitSelectStateController"

    const-string v2, "showDockDivider = false"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    move v0, v1

    :goto_0
    if-eqz v0, :cond_2

    invoke-static {p1}, Lcom/miui/home/recents/TaskViewUtils;->showDockDivider([Lcom/android/systemui/shared/recents/system/RemoteAnimationTargetCompat;)V

    :cond_2
    invoke-interface {p2}, Ljava/lang/Runnable;->run()V

    iget-object p1, p0, Lcom/miui/home/recents/util/SplitSelectStateController$RemoteSplitLaunchAnimationRunner;->mSuccessCallback:Ljava/util/function/Consumer;

    if-eqz p1, :cond_3

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-interface {p1, p2}, Ljava/util/function/Consumer;->accept(Ljava/lang/Object;)V

    :cond_3
    iget-object p1, p0, Lcom/miui/home/recents/util/SplitSelectStateController$RemoteSplitLaunchAnimationRunner;->this$0:Lcom/miui/home/recents/util/SplitSelectStateController;

    invoke-virtual {p1}, Lcom/miui/home/recents/util/SplitSelectStateController;->resetState()V

    return-void
.end method

.method public static synthetic lambda$onAnimationStart$1(Lcom/miui/home/recents/util/SplitSelectStateController$RemoteSplitLaunchAnimationRunner;[Lcom/android/systemui/shared/recents/system/RemoteAnimationTargetCompat;[Lcom/android/systemui/shared/recents/system/RemoteAnimationTargetCompat;Ljava/lang/Runnable;I[Lcom/android/systemui/shared/recents/system/RemoteAnimationTargetCompat;)V
    .locals 11

    move-object v0, p0

    move-object v5, p1

    array-length v1, v5

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, v5, v2

    const-string v4, "SplitSelectStateController"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onAnimationStart:   mode="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v7, v3, Lcom/android/systemui/shared/recents/system/RemoteAnimationTargetCompat;->mode:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v7, "   taskId="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v7, v3, Lcom/android/systemui/shared/recents/system/RemoteAnimationTargetCompat;->taskId:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v7, "   isTranslucent="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v3, v3, Lcom/android/systemui/shared/recents/system/RemoteAnimationTargetCompat;->isTranslucent:Z

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    new-instance v9, Lcom/miui/home/recents/LauncherAnimationRunner$AnimationResult;

    new-instance v1, Lcom/miui/home/recents/util/-$$Lambda$SplitSelectStateController$RemoteSplitLaunchAnimationRunner$YhuCjQEXY0AmBTJ-J4rCAcjK1Yg;

    move-object v7, p2

    move-object v2, p3

    invoke-direct {v1, p0, p2, p3}, Lcom/miui/home/recents/util/-$$Lambda$SplitSelectStateController$RemoteSplitLaunchAnimationRunner$YhuCjQEXY0AmBTJ-J4rCAcjK1Yg;-><init>(Lcom/miui/home/recents/util/SplitSelectStateController$RemoteSplitLaunchAnimationRunner;[Lcom/android/systemui/shared/recents/system/RemoteAnimationTargetCompat;Ljava/lang/Runnable;)V

    invoke-direct {v9, v1}, Lcom/miui/home/recents/LauncherAnimationRunner$AnimationResult;-><init>(Ljava/lang/Runnable;)V

    iget-boolean v1, v0, Lcom/miui/home/recents/util/SplitSelectStateController$RemoteSplitLaunchAnimationRunner;->mAnimate:Z

    if-nez v1, :cond_1

    invoke-virtual {v9}, Lcom/miui/home/recents/LauncherAnimationRunner$AnimationResult;->finish()V

    return-void

    :cond_1
    invoke-static {}, Lcom/miui/home/launcher/Application;->getLauncher()Lcom/miui/home/launcher/Launcher;

    move-result-object v1

    if-eqz v1, :cond_4

    const/4 v2, 0x0

    invoke-virtual {v1}, Lcom/miui/home/launcher/Launcher;->getAppTransitionManager()Lcom/miui/home/recents/LauncherAppTransitionManager;

    move-result-object v3

    instance-of v3, v3, Lcom/miui/home/recents/QuickstepAppTransitionManagerImpl;

    if-eqz v3, :cond_2

    invoke-virtual {v1}, Lcom/miui/home/launcher/Launcher;->getAppTransitionManager()Lcom/miui/home/recents/LauncherAppTransitionManager;

    move-result-object v2

    check-cast v2, Lcom/miui/home/recents/QuickstepAppTransitionManagerImpl;

    :cond_2
    if-nez v2, :cond_3

    invoke-virtual {v9}, Lcom/miui/home/recents/LauncherAnimationRunner$AnimationResult;->finish()V

    const-string v1, "SplitSelectStateController"

    const-string v2, "onAnimationStart: appTransitionManagerImpl is null, return"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_3
    invoke-virtual {v1}, Lcom/miui/home/launcher/Launcher;->getRecentsView()Lcom/miui/home/recents/views/RecentsView;

    move-result-object v1

    iget v3, v0, Lcom/miui/home/recents/util/SplitSelectStateController$RemoteSplitLaunchAnimationRunner;->mInitialTaskId:I

    invoke-virtual {v1, v3}, Lcom/miui/home/recents/views/RecentsView;->getTaskView(I)Lcom/miui/home/recents/views/TaskView;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v8, 0x1

    iget-object v1, v0, Lcom/miui/home/recents/util/SplitSelectStateController$RemoteSplitLaunchAnimationRunner;->this$0:Lcom/miui/home/recents/util/SplitSelectStateController;

    invoke-static {v1}, Lcom/miui/home/recents/util/SplitSelectStateController;->access$200(Lcom/miui/home/recents/util/SplitSelectStateController;)I

    move-result v10

    move-object v1, v2

    move-object v2, v4

    move v4, p4

    move-object v5, p1

    move-object/from16 v6, p5

    move-object v7, p2

    invoke-virtual/range {v1 .. v10}, Lcom/miui/home/recents/QuickstepAppTransitionManagerImpl;->composeRecentsLaunchAnimator(Landroid/animation/AnimatorSet;Landroid/view/View;I[Lcom/android/systemui/shared/recents/system/RemoteAnimationTargetCompat;[Lcom/android/systemui/shared/recents/system/RemoteAnimationTargetCompat;[Lcom/android/systemui/shared/recents/system/RemoteAnimationTargetCompat;ZLcom/miui/home/recents/LauncherAnimationRunner$AnimationResult;I)V

    goto :goto_1

    :cond_4
    invoke-virtual {v9}, Lcom/miui/home/recents/LauncherAnimationRunner$AnimationResult;->finish()V

    const-string v1, "SplitSelectStateController"

    const-string v2, "onAnimationStart: laucher is null"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    return-void
.end method


# virtual methods
.method public onAnimationCancelled()V
    .locals 2

    iget-object v0, p0, Lcom/miui/home/recents/util/SplitSelectStateController$RemoteSplitLaunchAnimationRunner;->this$0:Lcom/miui/home/recents/util/SplitSelectStateController;

    invoke-static {v0}, Lcom/miui/home/recents/util/SplitSelectStateController;->access$000(Lcom/miui/home/recents/util/SplitSelectStateController;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/miui/home/recents/util/-$$Lambda$SplitSelectStateController$RemoteSplitLaunchAnimationRunner$1rJObht2r8sHE4aTEe0dsRAj9Po;

    invoke-direct {v1, p0}, Lcom/miui/home/recents/util/-$$Lambda$SplitSelectStateController$RemoteSplitLaunchAnimationRunner$1rJObht2r8sHE4aTEe0dsRAj9Po;-><init>(Lcom/miui/home/recents/util/SplitSelectStateController$RemoteSplitLaunchAnimationRunner;)V

    invoke-static {v0, v1}, Lcom/miui/home/recents/util/Utilities;->postAsyncCallback(Landroid/os/Handler;Ljava/lang/Runnable;)V

    return-void
.end method

.method public onAnimationStart(I[Lcom/android/systemui/shared/recents/system/RemoteAnimationTargetCompat;[Lcom/android/systemui/shared/recents/system/RemoteAnimationTargetCompat;[Lcom/android/systemui/shared/recents/system/RemoteAnimationTargetCompat;Ljava/lang/Runnable;)V
    .locals 9

    iget-object v0, p0, Lcom/miui/home/recents/util/SplitSelectStateController$RemoteSplitLaunchAnimationRunner;->this$0:Lcom/miui/home/recents/util/SplitSelectStateController;

    invoke-static {v0}, Lcom/miui/home/recents/util/SplitSelectStateController;->access$000(Lcom/miui/home/recents/util/SplitSelectStateController;)Landroid/os/Handler;

    move-result-object v0

    new-instance v8, Lcom/miui/home/recents/util/-$$Lambda$SplitSelectStateController$RemoteSplitLaunchAnimationRunner$tbATMRxfPzHA9fFWrd_BZdaW0Os;

    move-object v1, v8

    move-object v2, p0

    move-object v3, p2

    move-object v4, p4

    move-object v5, p5

    move v6, p1

    move-object v7, p3

    invoke-direct/range {v1 .. v7}, Lcom/miui/home/recents/util/-$$Lambda$SplitSelectStateController$RemoteSplitLaunchAnimationRunner$tbATMRxfPzHA9fFWrd_BZdaW0Os;-><init>(Lcom/miui/home/recents/util/SplitSelectStateController$RemoteSplitLaunchAnimationRunner;[Lcom/android/systemui/shared/recents/system/RemoteAnimationTargetCompat;[Lcom/android/systemui/shared/recents/system/RemoteAnimationTargetCompat;Ljava/lang/Runnable;I[Lcom/android/systemui/shared/recents/system/RemoteAnimationTargetCompat;)V

    invoke-static {v0, v8}, Lcom/miui/home/recents/util/Utilities;->postAsyncCallback(Landroid/os/Handler;Ljava/lang/Runnable;)V

    return-void
.end method
