.class public Lcom/miui/home/recents/views/TaskViewThumbnail;
.super Landroid/view/View;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;
    }
.end annotation


# instance fields
.field private mBgColor:I

.field private mBgColorForSmallWindow:I

.field private mBgFillPaint:Landroid/graphics/Paint;

.field private mBitmapShader:Landroid/graphics/BitmapShader;

.field private final mBitmapShaderDrawInfo:Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;

.field private mCornerRadius:I

.field private mCoverPaint:Landroid/graphics/Paint;

.field private mDimAlpha:F
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "recents"
    .end annotation
.end field

.field private mDisplay:Landroid/view/Display;

.field private mDrawPaint:Landroid/graphics/Paint;

.field private mFirstTaskViewRect:Landroid/graphics/Rect;

.field private mFullscreenThumbnailScale:F
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "recents"
    .end annotation
.end field

.field private mRealInsets:Landroid/graphics/Rect;

.field private mScaleMatrix:Landroid/graphics/Matrix;

.field private mScaledInsets:Landroid/graphics/Rect;

.field private mSecondBitmapShader:Landroid/graphics/BitmapShader;

.field private final mSecondBitmapShaderDrawInfo:Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;

.field private mSecondDrawPaint:Landroid/graphics/Paint;

.field private mSecondScaleMatrix:Landroid/graphics/Matrix;

.field private mSecondTaskViewRect:Landroid/graphics/Rect;

.field private mSecondThumbnailBitmapRect:Landroid/graphics/Rect;

.field private mSecondThumbnailDrawingRect:Landroid/graphics/RectF;

.field private mSecondThumbnailInfo:Lcom/android/systemui/shared/recents/model/TaskThumbnailInfo;

.field private mSplitBounds:Lcom/android/wm/shell/util/StagedSplitBounds;

.field public mSpringAnimationImpl:Lcom/miui/home/recents/util/SpringAnimationImpl;

.field private mTask:Lcom/android/systemui/shared/recents/model/Task;

.field private mTaskRatio:F

.field private mTaskViewWithoutHeaderRect:Landroid/graphics/Rect;
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "recents"
    .end annotation
.end field

.field private mThumbnailBitmapRect:Landroid/graphics/Rect;
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "recents"
    .end annotation
.end field

.field private mThumbnailDrawingRect:Landroid/graphics/RectF;

.field private mThumbnailInfo:Lcom/android/systemui/shared/recents/model/TaskThumbnailInfo;
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "recents"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/miui/home/recents/views/TaskViewThumbnail;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/miui/home/recents/views/TaskViewThumbnail;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/miui/home/recents/views/TaskViewThumbnail;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    new-instance p1, Landroid/graphics/Rect;

    invoke-direct {p1}, Landroid/graphics/Rect;-><init>()V

    iput-object p1, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mTaskViewWithoutHeaderRect:Landroid/graphics/Rect;

    new-instance p1, Landroid/graphics/Rect;

    invoke-direct {p1}, Landroid/graphics/Rect;-><init>()V

    iput-object p1, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mFirstTaskViewRect:Landroid/graphics/Rect;

    new-instance p1, Landroid/graphics/Rect;

    invoke-direct {p1}, Landroid/graphics/Rect;-><init>()V

    iput-object p1, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mSecondTaskViewRect:Landroid/graphics/Rect;

    new-instance p1, Landroid/graphics/Rect;

    invoke-direct {p1}, Landroid/graphics/Rect;-><init>()V

    iput-object p1, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mThumbnailBitmapRect:Landroid/graphics/Rect;

    new-instance p1, Landroid/graphics/Rect;

    invoke-direct {p1}, Landroid/graphics/Rect;-><init>()V

    iput-object p1, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mSecondThumbnailBitmapRect:Landroid/graphics/Rect;

    new-instance p1, Landroid/graphics/Matrix;

    invoke-direct {p1}, Landroid/graphics/Matrix;-><init>()V

    iput-object p1, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mScaleMatrix:Landroid/graphics/Matrix;

    new-instance p1, Landroid/graphics/Matrix;

    invoke-direct {p1}, Landroid/graphics/Matrix;-><init>()V

    iput-object p1, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mSecondScaleMatrix:Landroid/graphics/Matrix;

    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mDrawPaint:Landroid/graphics/Paint;

    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mSecondDrawPaint:Landroid/graphics/Paint;

    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mBgFillPaint:Landroid/graphics/Paint;

    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mCoverPaint:Landroid/graphics/Paint;

    new-instance p1, Landroid/graphics/Rect;

    invoke-direct {p1}, Landroid/graphics/Rect;-><init>()V

    iput-object p1, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mScaledInsets:Landroid/graphics/Rect;

    new-instance p1, Landroid/graphics/Rect;

    invoke-direct {p1}, Landroid/graphics/Rect;-><init>()V

    iput-object p1, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mRealInsets:Landroid/graphics/Rect;

    new-instance p1, Landroid/graphics/RectF;

    invoke-direct {p1}, Landroid/graphics/RectF;-><init>()V

    iput-object p1, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mThumbnailDrawingRect:Landroid/graphics/RectF;

    new-instance p1, Landroid/graphics/RectF;

    invoke-direct {p1}, Landroid/graphics/RectF;-><init>()V

    iput-object p1, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mSecondThumbnailDrawingRect:Landroid/graphics/RectF;

    const/high16 p1, 0x3f800000    # 1.0f

    iput p1, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mTaskRatio:F

    new-instance p1, Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;

    invoke-direct {p1}, Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;-><init>()V

    iput-object p1, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mBitmapShaderDrawInfo:Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;

    new-instance p1, Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;

    invoke-direct {p1}, Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;-><init>()V

    iput-object p1, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mSecondBitmapShaderDrawInfo:Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;

    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 p2, 0x1

    const/16 p3, 0x1c

    if-lt p1, p3, :cond_0

    iget-object p1, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mDrawPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    iget-object p1, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mDrawPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object p1, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mSecondDrawPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    iget-object p1, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mSecondDrawPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    :cond_0
    invoke-virtual {p0}, Lcom/miui/home/recents/views/TaskViewThumbnail;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p3, 0x7f06057c

    invoke-virtual {p1, p3}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    iput p1, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mBgColor:I

    invoke-virtual {p0}, Lcom/miui/home/recents/views/TaskViewThumbnail;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p3, 0x7f06057d

    invoke-virtual {p1, p3}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    iput p1, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mBgColorForSmallWindow:I

    iget-object p1, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mBgFillPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object p1, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mCoverPaint:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/miui/home/recents/views/TaskViewThumbnail;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    const p4, 0x7f060581

    invoke-virtual {p3, p4}, Landroid/content/res/Resources;->getColor(I)I

    move-result p3

    invoke-virtual {p1, p3}, Landroid/graphics/Paint;->setColor(I)V

    iget-object p1, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mCoverPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    const p1, 0x3f19999a    # 0.6f

    iput p1, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mFullscreenThumbnailScale:F

    invoke-virtual {p0}, Lcom/miui/home/recents/views/TaskViewThumbnail;->getContext()Landroid/content/Context;

    move-result-object p1

    const-string p2, "window"

    invoke-virtual {p1, p2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/WindowManager;

    invoke-interface {p1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object p1

    iput-object p1, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mDisplay:Landroid/view/Display;

    new-instance p1, Lcom/miui/home/recents/util/SpringAnimationImpl;

    invoke-direct {p1, p0}, Lcom/miui/home/recents/util/SpringAnimationImpl;-><init>(Landroid/view/View;)V

    iput-object p1, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mSpringAnimationImpl:Lcom/miui/home/recents/util/SpringAnimationImpl;

    return-void
.end method

.method private calculateInsets(Z)V
    .locals 3

    invoke-static {p1}, Lcom/miui/home/recents/util/Utilities;->isNeedRotate(Z)Z

    move-result p1

    if-nez p1, :cond_0

    invoke-static {}, Lcom/miui/home/launcher/DeviceConfig;->getScreenWidth()I

    move-result p1

    invoke-static {}, Lcom/miui/home/launcher/DeviceConfig;->getRealScreenHeight()I

    move-result v0

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result p1

    invoke-static {}, Lcom/miui/home/launcher/DeviceConfig;->getScreenWidth()I

    move-result v0

    invoke-static {}, Lcom/miui/home/launcher/DeviceConfig;->getRealScreenHeight()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/miui/home/launcher/DeviceConfig;->getScreenWidth()I

    move-result p1

    invoke-static {}, Lcom/miui/home/launcher/DeviceConfig;->getRealScreenHeight()I

    move-result v0

    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result p1

    invoke-static {}, Lcom/miui/home/launcher/DeviceConfig;->getScreenWidth()I

    move-result v0

    invoke-static {}, Lcom/miui/home/launcher/DeviceConfig;->getRealScreenHeight()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    :goto_0
    const/high16 v1, 0x3f800000    # 1.0f

    iget-object v2, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mTaskViewWithoutHeaderRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v1

    int-to-float p1, p1

    div-float/2addr v2, p1

    int-to-float p1, v0

    mul-float/2addr p1, v2

    iget-object v0, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mTaskViewWithoutHeaderRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    int-to-float v0, v0

    sub-float/2addr p1, v0

    float-to-int p1, p1

    new-instance v0, Landroid/graphics/Rect;

    const/4 v1, 0x0

    invoke-direct {v0, v1, v1, v1, p1}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mScaledInsets:Landroid/graphics/Rect;

    return-void
.end method

.method public static getScaleToTargetAreaWithLimits(IIIIIII)F
    .locals 1

    if-eqz p5, :cond_4

    if-nez p6, :cond_0

    goto :goto_0

    :cond_0
    int-to-float p5, p5

    const/high16 v0, 0x3f800000    # 1.0f

    mul-float/2addr p5, v0

    int-to-float p6, p6

    div-float/2addr p5, p6

    int-to-float p2, p2

    int-to-float p0, p0

    div-float/2addr p0, p5

    invoke-static {p2, p0}, Ljava/lang/Math;->max(FF)F

    move-result p0

    int-to-float p2, p3

    int-to-float p1, p1

    div-float/2addr p1, p5

    invoke-static {p2, p1}, Ljava/lang/Math;->min(FF)F

    move-result p1

    cmpg-float p2, p1, p0

    if-gez p2, :cond_1

    move p0, p1

    :cond_1
    int-to-float p2, p4

    div-float/2addr p2, p5

    float-to-double p2, p2

    invoke-static {p2, p3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide p2

    double-to-float p2, p2

    cmpg-float p3, p2, p0

    if-gez p3, :cond_2

    div-float/2addr p0, p6

    return p0

    :cond_2
    cmpl-float p0, p2, p1

    if-lez p0, :cond_3

    div-float/2addr p1, p6

    return p1

    :cond_3
    div-float/2addr p2, p6

    return p2

    :cond_4
    :goto_0
    const/4 p0, 0x0

    return p0
.end method

.method private isRecentsContainerLandscape()Z
    .locals 2

    invoke-virtual {p0}, Lcom/miui/home/recents/views/TaskViewThumbnail;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/miui/home/recents/OverviewComponentObserver;->getInstance(Landroid/content/Context;)Lcom/miui/home/recents/OverviewComponentObserver;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/home/recents/OverviewComponentObserver;->isHomeAndOverviewSame()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/miui/home/launcher/DeviceConfig;->isKeepRecentsViewPortrait()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/miui/home/launcher/Application;->getLauncher()Lcom/miui/home/launcher/Launcher;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/miui/home/launcher/Application;->getLauncher()Lcom/miui/home/launcher/Launcher;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/home/launcher/Launcher;->getRecentsContainer()Lcom/miui/home/recents/views/RecentsContainer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/home/recents/views/RecentsContainer;->getRecentsRotation()I

    move-result v0

    invoke-static {v0}, Lcom/miui/home/recents/util/RotationHelper;->isLandscapeRotation(I)Z

    move-result v0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/miui/home/recents/views/TaskViewThumbnail;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/miui/home/recents/util/Utilities;->getAppConfiguration(Landroid/content/Context;)Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private isSplitScreen()Z
    .locals 2

    iget-object v0, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mThumbnailInfo:Lcom/android/systemui/shared/recents/model/TaskThumbnailInfo;

    if-eqz v0, :cond_0

    iget v0, v0, Lcom/android/systemui/shared/recents/model/TaskThumbnailInfo;->mWindowMod:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private postTranslateToMatrix(ZLandroid/graphics/Matrix;Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;)V
    .locals 5

    invoke-static {p3}, Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;->access$500(Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;)I

    move-result v0

    const/4 v1, -0x1

    const/4 v2, 0x1

    const/16 v3, 0x5a

    if-ne v0, v3, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    move v0, v2

    :goto_0
    invoke-static {p3}, Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;->access$500(Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;)I

    move-result v3

    const/16 v4, -0x5a

    if-ne v3, v4, :cond_1

    goto :goto_1

    :cond_1
    move v1, v2

    :goto_1
    invoke-static {p3}, Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;->access$200(Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;)I

    move-result v2

    mul-int/2addr v2, v0

    invoke-static {p3}, Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;->access$300(Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;)I

    move-result v3

    mul-int/2addr v3, v1

    invoke-direct {p0}, Lcom/miui/home/recents/views/TaskViewThumbnail;->useCenterClipForThumbnail()Z

    move-result v4

    if-nez v4, :cond_2

    if-eqz p1, :cond_4

    iget-object p1, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mTask:Lcom/android/systemui/shared/recents/model/Task;

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/android/systemui/shared/recents/model/Task;->useMultipleThumbnail()Z

    move-result p1

    if-nez p1, :cond_4

    :cond_2
    invoke-static {p3}, Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;->access$000(Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;)I

    move-result p1

    iget-object v4, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mTaskViewWithoutHeaderRect:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    if-le p1, v4, :cond_3

    iget-object p1, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mTaskViewWithoutHeaderRect:Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result p1

    invoke-static {p3}, Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;->access$000(Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;)I

    move-result v2

    sub-int/2addr p1, v2

    mul-int/2addr p1, v0

    div-int/lit8 p1, p1, 0x2

    move v2, p1

    :cond_3
    invoke-static {p3}, Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;->access$100(Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;)I

    move-result p1

    iget-object v0, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mTaskViewWithoutHeaderRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    if-le p1, v0, :cond_4

    iget-object p1, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mTaskViewWithoutHeaderRect:Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result p1

    invoke-static {p3}, Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;->access$100(Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;)I

    move-result p3

    sub-int/2addr p1, p3

    mul-int/2addr p1, v1

    div-int/lit8 v3, p1, 0x2

    :cond_4
    const-string p1, "TaskViewThumbnail"

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "updateThumbnailDrawInfo: "

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, ", transY="

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-static {p1, p3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    int-to-float p1, v2

    int-to-float p3, v3

    invoke-virtual {p2, p1, p3}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    return-void
.end method

.method private splitVerticallyInRecents(Z)Z
    .locals 1

    invoke-static {}, Lcom/miui/home/launcher/Application;->getInstance()Lcom/miui/home/launcher/Application;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/home/launcher/Application;->isInFoldLargeScreenMode()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lcom/miui/home/launcher/common/Utilities;->isPadDevice()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    return p1
.end method

.method private updateBgColor()V
    .locals 2

    invoke-direct {p0}, Lcom/miui/home/recents/views/TaskViewThumbnail;->useCenterLayoutForThumbnail()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mBgFillPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mBgColorForSmallWindow:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mBgFillPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mBgColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    :goto_0
    return-void
.end method

.method private updateDrawingPropertiesAndRequestDraw()V
    .locals 2

    invoke-virtual {p0}, Lcom/miui/home/recents/views/TaskViewThumbnail;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f06057c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mBgColor:I

    invoke-virtual {p0}, Lcom/miui/home/recents/views/TaskViewThumbnail;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f06057d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mBgColorForSmallWindow:I

    invoke-direct {p0}, Lcom/miui/home/recents/views/TaskViewThumbnail;->updateBgColor()V

    invoke-virtual {p0}, Lcom/miui/home/recents/views/TaskViewThumbnail;->invalidate()V

    return-void
.end method

.method private updateOffset(Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;Landroid/graphics/Rect;)V
    .locals 2

    invoke-direct {p0}, Lcom/miui/home/recents/views/TaskViewThumbnail;->useCenterLayoutForThumbnail()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v0

    invoke-static {p1}, Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;->access$000(Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;)I

    move-result v1

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    invoke-static {p1, v0}, Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;->access$202(Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;I)I

    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result p2

    invoke-static {p1}, Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;->access$100(Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;)I

    move-result v0

    sub-int/2addr p2, v0

    div-int/lit8 p2, p2, 0x2

    invoke-static {p1, p2}, Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;->access$302(Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;I)I

    goto :goto_0

    :cond_0
    iget v0, p2, Landroid/graphics/Rect;->left:I

    invoke-static {p1, v0}, Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;->access$202(Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;I)I

    iget p2, p2, Landroid/graphics/Rect;->top:I

    iget-object v0, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mTaskViewWithoutHeaderRect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    sub-int/2addr p2, v0

    invoke-static {p1, p2}, Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;->access$302(Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;I)I

    :goto_0
    return-void
.end method

.method private updateRotation(Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;Landroid/graphics/Rect;Lcom/android/systemui/shared/recents/model/TaskThumbnailInfo;Landroid/graphics/Rect;ZZ)V
    .locals 4

    invoke-direct {p0}, Lcom/miui/home/recents/views/TaskViewThumbnail;->useCenterLayoutForThumbnail()Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/miui/home/launcher/DeviceConfig;->isRotatable()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-static {}, Lcom/miui/home/launcher/Application;->getInstance()Lcom/miui/home/launcher/Application;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/home/launcher/Application;->isInFoldLargeScreenMode()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    invoke-static {p1, v2}, Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;->access$502(Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;I)I

    goto/16 :goto_4

    :cond_2
    iget-object v0, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mTask:Lcom/android/systemui/shared/recents/model/Task;

    invoke-virtual {v0}, Lcom/android/systemui/shared/recents/model/Task;->isFreeformTask()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mTask:Lcom/android/systemui/shared/recents/model/Task;

    iget-object v0, v0, Lcom/android/systemui/shared/recents/model/Task;->bounds:Landroid/graphics/Rect;

    if-nez v0, :cond_3

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1

    :cond_4
    :goto_0
    move v0, v1

    :goto_1
    invoke-virtual {p2}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_9

    if-eqz p3, :cond_9

    iget v3, p3, Lcom/android/systemui/shared/recents/model/TaskThumbnailInfo;->taskWidth:I

    if-eqz v3, :cond_9

    iget p3, p3, Lcom/android/systemui/shared/recents/model/TaskThumbnailInfo;->taskHeight:I

    if-nez p3, :cond_5

    goto :goto_3

    :cond_5
    if-eqz v0, :cond_a

    xor-int p3, p5, p6

    if-eqz p3, :cond_8

    const/high16 p3, 0x40000000    # 2.0f

    if-eqz p5, :cond_7

    iget-object p5, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mDisplay:Landroid/view/Display;

    invoke-virtual {p5}, Landroid/view/Display;->getRotation()I

    move-result p5

    const/4 p6, 0x3

    if-ne p5, p6, :cond_6

    goto :goto_2

    :cond_6
    const/16 p5, -0x5a

    invoke-static {p1, p5}, Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;->access$502(Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;I)I

    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result p2

    int-to-float p2, p2

    div-float/2addr p2, p3

    invoke-static {p1, p2}, Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;->access$702(Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;F)F

    move-result p2

    invoke-static {p1, p2}, Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;->access$602(Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;F)F

    goto :goto_4

    :cond_7
    :goto_2
    const/16 p5, 0x5a

    invoke-static {p1, p5}, Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;->access$502(Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;I)I

    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result p2

    int-to-float p2, p2

    div-float/2addr p2, p3

    invoke-static {p1, p2}, Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;->access$702(Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;F)F

    move-result p2

    invoke-static {p1, p2}, Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;->access$602(Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;F)F

    goto :goto_4

    :cond_8
    invoke-static {p1, v2}, Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;->access$502(Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;I)I

    goto :goto_4

    :cond_9
    :goto_3
    invoke-static {p1, v2}, Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;->access$502(Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;I)I

    :cond_a
    :goto_4
    invoke-static {p1}, Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;->access$500(Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;)I

    move-result p2

    if-eqz p2, :cond_b

    goto :goto_5

    :cond_b
    move v1, v2

    :goto_5
    if-eqz v1, :cond_c

    invoke-virtual {p4}, Landroid/graphics/Rect;->height()I

    move-result p2

    goto :goto_6

    :cond_c
    invoke-virtual {p4}, Landroid/graphics/Rect;->width()I

    move-result p2

    :goto_6
    int-to-float p2, p2

    invoke-static {p1, p2}, Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;->access$802(Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;F)F

    if-eqz v1, :cond_d

    invoke-virtual {p4}, Landroid/graphics/Rect;->width()I

    move-result p2

    goto :goto_7

    :cond_d
    invoke-virtual {p4}, Landroid/graphics/Rect;->height()I

    move-result p2

    :goto_7
    int-to-float p2, p2

    invoke-static {p1, p2}, Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;->access$902(Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;F)F

    return-void
.end method

.method private updateScale(Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;Landroid/graphics/Rect;Lcom/android/systemui/shared/recents/model/TaskThumbnailInfo;Landroid/graphics/Rect;ZZ)V
    .locals 7

    iget-object p5, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mTask:Lcom/android/systemui/shared/recents/model/Task;

    invoke-virtual {p5}, Lcom/android/systemui/shared/recents/model/Task;->isFreeformTask()Z

    move-result p5

    const/4 p6, 0x0

    const/4 v0, 0x1

    if-eqz p5, :cond_1

    iget-object p5, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mTask:Lcom/android/systemui/shared/recents/model/Task;

    iget-object p5, p5, Lcom/android/systemui/shared/recents/model/Task;->bounds:Landroid/graphics/Rect;

    if-nez p5, :cond_0

    goto :goto_0

    :cond_0
    move p5, p6

    goto :goto_1

    :cond_1
    :goto_0
    move p5, v0

    :goto_1
    invoke-virtual {p2}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v1

    const/4 v2, 0x0

    if-nez v1, :cond_c

    if-eqz p3, :cond_c

    iget v1, p3, Lcom/android/systemui/shared/recents/model/TaskThumbnailInfo;->taskWidth:I

    if-eqz v1, :cond_c

    iget p3, p3, Lcom/android/systemui/shared/recents/model/TaskThumbnailInfo;->taskHeight:I

    if-nez p3, :cond_2

    goto/16 :goto_6

    :cond_2
    if-eqz p5, :cond_9

    invoke-static {p1}, Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;->access$800(Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;)F

    move-result p3

    cmpl-float p3, p3, v2

    if-eqz p3, :cond_8

    invoke-static {p1}, Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;->access$900(Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;)F

    move-result p3

    cmpl-float p3, p3, v2

    if-nez p3, :cond_3

    goto/16 :goto_4

    :cond_3
    invoke-direct {p0}, Lcom/miui/home/recents/views/TaskViewThumbnail;->useCenterLayoutForThumbnail()Z

    move-result p3

    invoke-direct {p0}, Lcom/miui/home/recents/views/TaskViewThumbnail;->isSplitScreen()Z

    move-result p4

    if-eqz p4, :cond_4

    sget-boolean p4, Lcom/miui/home/launcher/DeviceConfig;->IS_FOLD_DEVICE:Z

    if-nez p4, :cond_4

    move p6, v0

    :cond_4
    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result p4

    int-to-float p4, p4

    const/high16 p5, 0x3f800000    # 1.0f

    mul-float/2addr p4, p5

    invoke-static {p1}, Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;->access$800(Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;)F

    move-result v0

    div-float/2addr p4, v0

    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, p5

    invoke-static {p1}, Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;->access$900(Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;)F

    move-result p5

    div-float/2addr v0, p5

    if-eqz p3, :cond_5

    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result p3

    int-to-float p3, p3

    const p4, 0x3ecccccd    # 0.4f

    mul-float/2addr p3, p4

    float-to-int v0, p3

    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result p3

    int-to-float p3, p3

    const p5, 0x3f666666    # 0.9f

    mul-float/2addr p3, p5

    float-to-int v1, p3

    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result p3

    int-to-float p3, p3

    mul-float/2addr p3, p4

    float-to-int v2, p3

    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result p3

    int-to-float p3, p3

    mul-float/2addr p3, p5

    float-to-int v3, p3

    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result p3

    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result p2

    mul-int/2addr p3, p2

    int-to-float p2, p3

    const/high16 p3, 0x3f000000    # 0.5f

    mul-float/2addr p2, p3

    float-to-int v4, p2

    invoke-static {p1}, Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;->access$800(Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;)F

    move-result p2

    float-to-int v5, p2

    invoke-static {p1}, Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;->access$900(Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;)F

    move-result p2

    float-to-int v6, p2

    invoke-static/range {v0 .. v6}, Lcom/miui/home/recents/views/TaskViewThumbnail;->getScaleToTargetAreaWithLimits(IIIIIII)F

    move-result p2

    invoke-static {p1, p2}, Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;->access$402(Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;F)F

    goto :goto_3

    :cond_5
    if-nez p6, :cond_7

    cmpl-float p2, p4, v0

    if-lez p2, :cond_6

    goto :goto_2

    :cond_6
    invoke-static {p1, v0}, Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;->access$402(Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;F)F

    goto :goto_3

    :cond_7
    :goto_2
    invoke-static {p1, p4}, Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;->access$402(Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;F)F

    :goto_3
    invoke-static {p1}, Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;->access$800(Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;)F

    move-result p2

    invoke-static {p1}, Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;->access$400(Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;)F

    move-result p3

    mul-float/2addr p2, p3

    float-to-int p2, p2

    invoke-static {p1, p2}, Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;->access$002(Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;I)I

    invoke-static {p1}, Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;->access$900(Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;)F

    move-result p2

    invoke-static {p1}, Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;->access$400(Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;)F

    move-result p3

    mul-float/2addr p2, p3

    float-to-int p2, p2

    invoke-static {p1, p2}, Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;->access$102(Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;I)I

    const-string p2, "TaskViewThumbnail"

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string p4, "updateThumbnailScale, t="

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p4, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mTask:Lcom/android/systemui/shared/recents/model/Task;

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p4, ", mThumbnailBitmapSizeAfterScaled=("

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p1}, Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;->access$000(Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;)I

    move-result p4

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p4, ", "

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p1}, Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;->access$100(Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;)I

    move-result p1

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, ")"

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p2, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_7

    :cond_8
    :goto_4
    invoke-static {p1, v2}, Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;->access$402(Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;F)F

    return-void

    :cond_9
    invoke-virtual {p4}, Landroid/graphics/Rect;->width()I

    move-result p3

    if-eqz p3, :cond_b

    invoke-virtual {p4}, Landroid/graphics/Rect;->height()I

    move-result p3

    if-nez p3, :cond_a

    goto :goto_5

    :cond_a
    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result p3

    int-to-float p3, p3

    invoke-virtual {p4}, Landroid/graphics/Rect;->width()I

    move-result p5

    int-to-float p5, p5

    div-float/2addr p3, p5

    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result p2

    int-to-float p2, p2

    invoke-virtual {p4}, Landroid/graphics/Rect;->height()I

    move-result p4

    int-to-float p4, p4

    div-float/2addr p2, p4

    invoke-static {p3, p2}, Ljava/lang/Math;->min(FF)F

    move-result p2

    invoke-static {p1, p2}, Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;->access$402(Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;F)F

    goto :goto_7

    :cond_b
    :goto_5
    invoke-static {p1, v2}, Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;->access$402(Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;F)F

    goto :goto_7

    :cond_c
    :goto_6
    invoke-static {p1, v2}, Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;->access$402(Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;F)F

    :goto_7
    return-void
.end method

.method private updateThumbnailDrawInfo()V
    .locals 9

    invoke-direct {p0}, Lcom/miui/home/recents/views/TaskViewThumbnail;->isRecentsContainerLandscape()Z

    move-result v8

    invoke-direct {p0, v8}, Lcom/miui/home/recents/views/TaskViewThumbnail;->calculateInsets(Z)V

    xor-int/lit8 v0, v8, 0x1

    invoke-virtual {p0, v0}, Lcom/miui/home/recents/views/TaskViewThumbnail;->mapSplitBoundsToTaskViewRect(Z)V

    iget-object v0, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mTask:Lcom/android/systemui/shared/recents/model/Task;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/android/systemui/shared/recents/model/Task;->useMultipleThumbnail()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v4, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mBitmapShader:Landroid/graphics/BitmapShader;

    if-eqz v4, :cond_1

    iget-object v5, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mThumbnailInfo:Lcom/android/systemui/shared/recents/model/TaskThumbnailInfo;

    if-eqz v5, :cond_1

    iget-object v0, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mSecondBitmapShader:Landroid/graphics/BitmapShader;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mSecondThumbnailInfo:Lcom/android/systemui/shared/recents/model/TaskThumbnailInfo;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mSplitBounds:Lcom/android/wm/shell/util/StagedSplitBounds;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mFirstTaskViewRect:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mBitmapShaderDrawInfo:Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;

    iget-object v3, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mScaleMatrix:Landroid/graphics/Matrix;

    iget-object v6, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mThumbnailBitmapRect:Landroid/graphics/Rect;

    move-object v0, p0

    move v7, v8

    invoke-direct/range {v0 .. v7}, Lcom/miui/home/recents/views/TaskViewThumbnail;->updateThumbnailDrawInfoImpl(Landroid/graphics/Rect;Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;Landroid/graphics/Matrix;Landroid/graphics/BitmapShader;Lcom/android/systemui/shared/recents/model/TaskThumbnailInfo;Landroid/graphics/Rect;Z)V

    iget-object v1, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mSecondTaskViewRect:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mSecondBitmapShaderDrawInfo:Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;

    iget-object v3, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mSecondScaleMatrix:Landroid/graphics/Matrix;

    iget-object v4, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mSecondBitmapShader:Landroid/graphics/BitmapShader;

    iget-object v5, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mSecondThumbnailInfo:Lcom/android/systemui/shared/recents/model/TaskThumbnailInfo;

    iget-object v6, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mSecondThumbnailBitmapRect:Landroid/graphics/Rect;

    invoke-direct/range {v0 .. v7}, Lcom/miui/home/recents/views/TaskViewThumbnail;->updateThumbnailDrawInfoImpl(Landroid/graphics/Rect;Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;Landroid/graphics/Matrix;Landroid/graphics/BitmapShader;Lcom/android/systemui/shared/recents/model/TaskThumbnailInfo;Landroid/graphics/Rect;Z)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mBitmapShader:Landroid/graphics/BitmapShader;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mThumbnailInfo:Lcom/android/systemui/shared/recents/model/TaskThumbnailInfo;

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/miui/home/recents/views/TaskViewThumbnail;->updateBgColor()V

    iget-object v1, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mTaskViewWithoutHeaderRect:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mBitmapShaderDrawInfo:Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;

    iget-object v3, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mScaleMatrix:Landroid/graphics/Matrix;

    iget-object v4, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mBitmapShader:Landroid/graphics/BitmapShader;

    iget-object v5, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mThumbnailInfo:Lcom/android/systemui/shared/recents/model/TaskThumbnailInfo;

    iget-object v6, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mThumbnailBitmapRect:Landroid/graphics/Rect;

    move-object v0, p0

    move v7, v8

    invoke-direct/range {v0 .. v7}, Lcom/miui/home/recents/views/TaskViewThumbnail;->updateThumbnailDrawInfoImpl(Landroid/graphics/Rect;Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;Landroid/graphics/Matrix;Landroid/graphics/BitmapShader;Lcom/android/systemui/shared/recents/model/TaskThumbnailInfo;Landroid/graphics/Rect;Z)V

    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/miui/home/recents/views/TaskViewThumbnail;->invalidate()V

    return-void
.end method

.method private updateThumbnailDrawInfoImpl(Landroid/graphics/Rect;Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;Landroid/graphics/Matrix;Landroid/graphics/BitmapShader;Lcom/android/systemui/shared/recents/model/TaskThumbnailInfo;Landroid/graphics/Rect;Z)V
    .locals 8

    iput-boolean p7, p2, Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;->isRecentContainerLandscape:Z

    iget v0, p5, Lcom/android/systemui/shared/recents/model/TaskThumbnailInfo;->screenOrientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    move-object v1, p0

    move-object v2, p2

    move-object v3, p1

    move-object v4, p5

    move-object v5, p6

    move v6, p7

    move v7, v0

    invoke-direct/range {v1 .. v7}, Lcom/miui/home/recents/views/TaskViewThumbnail;->updateRotation(Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;Landroid/graphics/Rect;Lcom/android/systemui/shared/recents/model/TaskThumbnailInfo;Landroid/graphics/Rect;ZZ)V

    invoke-direct/range {v1 .. v7}, Lcom/miui/home/recents/views/TaskViewThumbnail;->updateScale(Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;Landroid/graphics/Rect;Lcom/android/systemui/shared/recents/model/TaskThumbnailInfo;Landroid/graphics/Rect;ZZ)V

    invoke-direct {p0, p2, p1}, Lcom/miui/home/recents/views/TaskViewThumbnail;->updateOffset(Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;Landroid/graphics/Rect;)V

    iget-object p1, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mThumbnailInfo:Lcom/android/systemui/shared/recents/model/TaskThumbnailInfo;

    if-eqz p1, :cond_1

    iget-object p1, p1, Lcom/android/systemui/shared/recents/model/TaskThumbnailInfo;->insets:Landroid/graphics/Rect;

    if-eqz p1, :cond_1

    const/4 p1, 0x0

    invoke-virtual {p3, p1, p1}, Landroid/graphics/Matrix;->setTranslate(FF)V

    :cond_1
    invoke-static {p2}, Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;->access$400(Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;)F

    move-result p1

    invoke-static {p2}, Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;->access$400(Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;)F

    move-result p5

    invoke-virtual {p3, p1, p5}, Landroid/graphics/Matrix;->postScale(FF)Z

    invoke-static {p2}, Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;->access$500(Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;)I

    move-result p1

    int-to-float p1, p1

    invoke-static {p2}, Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;->access$600(Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;)F

    move-result p5

    invoke-static {p2}, Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;->access$700(Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;)F

    move-result p6

    invoke-virtual {p3, p1, p5, p6}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    invoke-direct {p0, v0, p3, p2}, Lcom/miui/home/recents/views/TaskViewThumbnail;->postTranslateToMatrix(ZLandroid/graphics/Matrix;Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;)V

    if-eqz p4, :cond_2

    :try_start_0
    invoke-virtual {p4, p3}, Landroid/graphics/BitmapShader;->setLocalMatrix(Landroid/graphics/Matrix;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    const-string p2, "TaskViewThumbnail"

    const-string p3, "updateThumbnailDrawInfo mBitmapShader.setLocalMatrix error"

    invoke-static {p2, p3, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_2
    :goto_1
    return-void
.end method

.method private useCenterClipForThumbnail()Z
    .locals 1

    sget-boolean v0, Lcom/miui/home/launcher/DeviceConfig;->IS_FOLD_DEVICE:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mTask:Lcom/android/systemui/shared/recents/model/Task;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/systemui/shared/recents/model/Task;->hasMultipleTasks()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private useCenterLayoutForThumbnail()Z
    .locals 1

    invoke-virtual {p0}, Lcom/miui/home/recents/views/TaskViewThumbnail;->isFreeFormMode()Z

    move-result v0

    return v0
.end method


# virtual methods
.method bindToTask(Lcom/android/systemui/shared/recents/model/Task;Z)V
    .locals 0

    goto/32 :goto_2

    nop

    :goto_0
    return-void

    :goto_1
    invoke-direct {p0}, Lcom/miui/home/recents/views/TaskViewThumbnail;->updateDrawingPropertiesAndRequestDraw()V

    goto/32 :goto_0

    nop

    :goto_2
    iput-object p1, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mTask:Lcom/android/systemui/shared/recents/model/Task;

    goto/32 :goto_1

    nop
.end method

.method public clearThumbnail()V
    .locals 2

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mBitmapShader:Landroid/graphics/BitmapShader;

    iput-object v0, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mSecondBitmapShader:Landroid/graphics/BitmapShader;

    iput-object v0, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mSplitBounds:Lcom/android/wm/shell/util/StagedSplitBounds;

    iget-object v1, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mDrawPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    iget-object v1, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mSecondDrawPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    iget-object v1, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mThumbnailBitmapRect:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->setEmpty()V

    iget-object v1, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mSecondThumbnailBitmapRect:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->setEmpty()V

    iput-object v0, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mThumbnailInfo:Lcom/android/systemui/shared/recents/model/TaskThumbnailInfo;

    iput-object v0, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mSecondThumbnailInfo:Lcom/android/systemui/shared/recents/model/TaskThumbnailInfo;

    return-void
.end method

.method public getInsets()Landroid/graphics/Rect;
    .locals 1

    iget-object v0, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mScaledInsets:Landroid/graphics/Rect;

    return-object v0
.end method

.method public getSpringAnimationImpl()Lcom/miui/home/recents/util/SpringAnimationImpl;
    .locals 1

    iget-object v0, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mSpringAnimationImpl:Lcom/miui/home/recents/util/SpringAnimationImpl;

    return-object v0
.end method

.method public getSysUiStatusNavFlags()I
    .locals 2

    iget-object v0, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mThumbnailInfo:Lcom/android/systemui/shared/recents/model/TaskThumbnailInfo;

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    iget v0, v0, Lcom/android/systemui/shared/recents/model/TaskThumbnailInfo;->systemUiVisibility:I

    and-int/lit16 v0, v0, 0x2000

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    goto :goto_0

    :cond_0
    const/16 v0, 0x8

    :goto_0
    or-int/2addr v0, v1

    iget-object v1, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mThumbnailInfo:Lcom/android/systemui/shared/recents/model/TaskThumbnailInfo;

    iget v1, v1, Lcom/android/systemui/shared/recents/model/TaskThumbnailInfo;->systemUiVisibility:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    const/4 v1, 0x2

    :goto_1
    or-int/2addr v0, v1

    return v0

    :cond_2
    return v1
.end method

.method public getTaskRatio()F
    .locals 1

    iget v0, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mTaskRatio:F

    return v0
.end method

.method public getTaskViewHeaderHeight()I
    .locals 2

    invoke-virtual {p0}, Lcom/miui/home/recents/views/TaskViewThumbnail;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/miui/home/recents/OverviewComponentObserver;->getInstance(Landroid/content/Context;)Lcom/miui/home/recents/OverviewComponentObserver;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/home/recents/OverviewComponentObserver;->isHomeAndOverviewSame()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/miui/home/recents/util/Utilities;->isAddToLauncher(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/miui/home/launcher/Application;->getLauncher()Lcom/miui/home/launcher/Launcher;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/home/launcher/Launcher;->getRecentsView()Lcom/miui/home/recents/views/RecentsView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/home/recents/views/RecentsView;->getTaskStackView()Lcom/miui/home/recents/views/TaskStackView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/home/recents/views/TaskStackView;->getStackAlgorithm()Lcom/miui/home/recents/views/TaskStackLayoutAlgorithm;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/home/recents/views/TaskStackLayoutAlgorithm;->getRecentsTaskViewHeaderHeight()I

    move-result v0

    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/miui/home/recents/views/TaskViewThumbnail;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0705db

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    return v0
.end method

.method public isFreeFormMode()Z
    .locals 2

    iget-object v0, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mThumbnailInfo:Lcom/android/systemui/shared/recents/model/TaskThumbnailInfo;

    if-eqz v0, :cond_0

    iget v0, v0, Lcom/android/systemui/shared/recents/model/TaskThumbnailInfo;->mWindowMod:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method mapSplitBoundsToTaskViewRect(Z)V
    .locals 6

    goto/32 :goto_63

    nop

    :goto_0
    div-float/2addr v2, v1

    goto/32 :goto_5d

    nop

    :goto_1
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result p1

    goto/32 :goto_30

    nop

    :goto_2
    iget v3, v3, Landroid/graphics/Rect;->top:I

    goto/32 :goto_65

    nop

    :goto_3
    sub-int/2addr v1, p1

    goto/32 :goto_33

    nop

    :goto_4
    iget-object v5, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mTaskViewWithoutHeaderRect:Landroid/graphics/Rect;

    goto/32 :goto_18

    nop

    :goto_5
    if-eqz v0, :cond_0

    goto/32 :goto_13

    :cond_0
    goto/32 :goto_12

    nop

    :goto_6
    mul-float/2addr p1, v2

    goto/32 :goto_2c

    nop

    :goto_7
    iget v1, v1, Landroid/graphics/Rect;->right:I

    goto/32 :goto_44

    nop

    :goto_8
    iget-object v2, v0, Lcom/android/wm/shell/util/StagedSplitBounds;->rightBottomBounds:Landroid/graphics/Rect;

    goto/32 :goto_57

    nop

    :goto_9
    invoke-virtual {v0, v1, v2, p1, v3}, Landroid/graphics/Rect;->set(IIII)V

    goto/32 :goto_3e

    nop

    :goto_a
    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    goto/32 :goto_9

    nop

    :goto_b
    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    goto/32 :goto_c

    nop

    :goto_c
    sub-int/2addr v2, p1

    goto/32 :goto_4e

    nop

    :goto_d
    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    goto/32 :goto_40

    nop

    :goto_e
    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    goto/32 :goto_49

    nop

    :goto_f
    iget-object v1, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mTaskViewWithoutHeaderRect:Landroid/graphics/Rect;

    goto/32 :goto_59

    nop

    :goto_10
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result p1

    goto/32 :goto_1d

    nop

    :goto_11
    float-to-int p1, p1

    goto/32 :goto_38

    nop

    :goto_12
    return-void

    :goto_13
    goto/32 :goto_54

    nop

    :goto_14
    int-to-float v1, v1

    goto/32 :goto_0

    nop

    :goto_15
    sub-int/2addr p1, v0

    goto/32 :goto_4f

    nop

    :goto_16
    iget-object v2, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mTaskViewWithoutHeaderRect:Landroid/graphics/Rect;

    goto/32 :goto_47

    nop

    :goto_17
    iget-object v3, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mTaskViewWithoutHeaderRect:Landroid/graphics/Rect;

    goto/32 :goto_a

    nop

    :goto_18
    iget v5, v5, Landroid/graphics/Rect;->top:I

    goto/32 :goto_27

    nop

    :goto_19
    float-to-int p1, p1

    goto/32 :goto_24

    nop

    :goto_1a
    iget v1, v1, Landroid/graphics/Rect;->left:I

    goto/32 :goto_5f

    nop

    :goto_1b
    add-int/2addr v4, p1

    goto/32 :goto_5c

    nop

    :goto_1c
    iget v0, v0, Lcom/android/wm/shell/util/StagedSplitBounds;->leftTaskPercent:F

    goto/32 :goto_20

    nop

    :goto_1d
    iget-object v0, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mScaledInsets:Landroid/graphics/Rect;

    goto/32 :goto_23

    nop

    :goto_1e
    add-int/2addr p1, v1

    goto/32 :goto_3b

    nop

    :goto_1f
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result p1

    goto/32 :goto_4c

    nop

    :goto_20
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v2

    goto/32 :goto_50

    nop

    :goto_21
    iget v0, v0, Lcom/android/wm/shell/util/StagedSplitBounds;->topTaskPercent:F

    goto/32 :goto_46

    nop

    :goto_22
    iget p1, p1, Landroid/graphics/Rect;->top:I

    goto/32 :goto_16

    nop

    :goto_23
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    goto/32 :goto_28

    nop

    :goto_24
    iget-object v0, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mFirstTaskViewRect:Landroid/graphics/Rect;

    goto/32 :goto_61

    nop

    :goto_25
    mul-float/2addr p1, v2

    goto/32 :goto_56

    nop

    :goto_26
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result p1

    goto/32 :goto_66

    nop

    :goto_27
    add-int/2addr v5, p1

    goto/32 :goto_4d

    nop

    :goto_28
    add-int/2addr p1, v0

    goto/32 :goto_34

    nop

    :goto_29
    int-to-float v2, v2

    goto/32 :goto_62

    nop

    :goto_2a
    iget v1, v1, Landroid/graphics/Rect;->left:I

    goto/32 :goto_35

    nop

    :goto_2b
    if-nez p1, :cond_1

    goto/32 :goto_3f

    :cond_1
    goto/32 :goto_39

    nop

    :goto_2c
    float-to-int p1, p1

    goto/32 :goto_3d

    nop

    :goto_2d
    div-float/2addr v2, v1

    :goto_2e
    goto/32 :goto_4b

    nop

    :goto_2f
    iget-object v1, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mTaskViewWithoutHeaderRect:Landroid/graphics/Rect;

    goto/32 :goto_1a

    nop

    :goto_30
    int-to-float p1, p1

    goto/32 :goto_6

    nop

    :goto_31
    iget-object p1, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mTaskViewWithoutHeaderRect:Landroid/graphics/Rect;

    goto/32 :goto_26

    nop

    :goto_32
    iget v1, v1, Landroid/graphics/Rect;->left:I

    goto/32 :goto_43

    nop

    :goto_33
    iget-object p1, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mTaskViewWithoutHeaderRect:Landroid/graphics/Rect;

    goto/32 :goto_22

    nop

    :goto_34
    int-to-float p1, p1

    goto/32 :goto_25

    nop

    :goto_35
    iget-object v2, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mTaskViewWithoutHeaderRect:Landroid/graphics/Rect;

    goto/32 :goto_b

    nop

    :goto_36
    mul-float/2addr p1, v0

    goto/32 :goto_19

    nop

    :goto_37
    iget-boolean v2, v0, Lcom/android/wm/shell/util/StagedSplitBounds;->appsStackedVertically:Z

    goto/32 :goto_58

    nop

    :goto_38
    iget-object v0, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mFirstTaskViewRect:Landroid/graphics/Rect;

    goto/32 :goto_2f

    nop

    :goto_39
    iget-object p1, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mTaskViewWithoutHeaderRect:Landroid/graphics/Rect;

    goto/32 :goto_1f

    nop

    :goto_3a
    iget-object v1, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mTaskViewWithoutHeaderRect:Landroid/graphics/Rect;

    goto/32 :goto_2a

    nop

    :goto_3b
    int-to-float p1, p1

    goto/32 :goto_41

    nop

    :goto_3c
    iget-object v0, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mScaledInsets:Landroid/graphics/Rect;

    goto/32 :goto_55

    nop

    :goto_3d
    iget-object v0, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mSecondTaskViewRect:Landroid/graphics/Rect;

    goto/32 :goto_f

    nop

    :goto_3e
    goto :goto_4a

    :goto_3f
    goto/32 :goto_31

    nop

    :goto_40
    invoke-virtual {v0, v1, v3, v4, p1}, Landroid/graphics/Rect;->set(IIII)V

    goto/32 :goto_64

    nop

    :goto_41
    mul-float/2addr p1, v0

    goto/32 :goto_11

    nop

    :goto_42
    iget v4, v4, Landroid/graphics/Rect;->right:I

    goto/32 :goto_4

    nop

    :goto_43
    iget-object v3, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mTaskViewWithoutHeaderRect:Landroid/graphics/Rect;

    goto/32 :goto_48

    nop

    :goto_44
    int-to-float v1, v1

    goto/32 :goto_2d

    nop

    :goto_45
    iget-object v4, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mTaskViewWithoutHeaderRect:Landroid/graphics/Rect;

    goto/32 :goto_51

    nop

    :goto_46
    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v2

    goto/32 :goto_29

    nop

    :goto_47
    iget v2, v2, Landroid/graphics/Rect;->right:I

    goto/32 :goto_5a

    nop

    :goto_48
    iget v3, v3, Landroid/graphics/Rect;->top:I

    goto/32 :goto_45

    nop

    :goto_49
    invoke-virtual {v0, v1, p1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    :goto_4a
    goto/32 :goto_53

    nop

    :goto_4b
    invoke-direct {p0, p1}, Lcom/miui/home/recents/views/TaskViewThumbnail;->splitVerticallyInRecents(Z)Z

    move-result p1

    goto/32 :goto_2b

    nop

    :goto_4c
    iget-object v1, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mScaledInsets:Landroid/graphics/Rect;

    goto/32 :goto_60

    nop

    :goto_4d
    invoke-virtual {v0, v1, v3, v4, v5}, Landroid/graphics/Rect;->set(IIII)V

    goto/32 :goto_5b

    nop

    :goto_4e
    iget-object p1, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mTaskViewWithoutHeaderRect:Landroid/graphics/Rect;

    goto/32 :goto_52

    nop

    :goto_4f
    iget-object v0, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mSecondTaskViewRect:Landroid/graphics/Rect;

    goto/32 :goto_3a

    nop

    :goto_50
    int-to-float v2, v2

    goto/32 :goto_7

    nop

    :goto_51
    iget v4, v4, Landroid/graphics/Rect;->left:I

    goto/32 :goto_1b

    nop

    :goto_52
    iget p1, p1, Landroid/graphics/Rect;->right:I

    goto/32 :goto_17

    nop

    :goto_53
    return-void

    :goto_54
    new-instance v1, Landroid/graphics/Rect;

    goto/32 :goto_8

    nop

    :goto_55
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    goto/32 :goto_15

    nop

    :goto_56
    float-to-int p1, p1

    goto/32 :goto_3c

    nop

    :goto_57
    invoke-direct {v1, v2}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    goto/32 :goto_37

    nop

    :goto_58
    if-nez v2, :cond_2

    goto/32 :goto_5e

    :cond_2
    goto/32 :goto_21

    nop

    :goto_59
    iget v1, v1, Landroid/graphics/Rect;->right:I

    goto/32 :goto_3

    nop

    :goto_5a
    iget-object v3, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mTaskViewWithoutHeaderRect:Landroid/graphics/Rect;

    goto/32 :goto_e

    nop

    :goto_5b
    iget-object p1, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mTaskViewWithoutHeaderRect:Landroid/graphics/Rect;

    goto/32 :goto_10

    nop

    :goto_5c
    iget-object p1, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mTaskViewWithoutHeaderRect:Landroid/graphics/Rect;

    goto/32 :goto_d

    nop

    :goto_5d
    goto/16 :goto_2e

    :goto_5e
    goto/32 :goto_1c

    nop

    :goto_5f
    iget-object v3, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mTaskViewWithoutHeaderRect:Landroid/graphics/Rect;

    goto/32 :goto_2

    nop

    :goto_60
    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    goto/32 :goto_1e

    nop

    :goto_61
    iget-object v1, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mTaskViewWithoutHeaderRect:Landroid/graphics/Rect;

    goto/32 :goto_32

    nop

    :goto_62
    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    goto/32 :goto_14

    nop

    :goto_63
    iget-object v0, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mSplitBounds:Lcom/android/wm/shell/util/StagedSplitBounds;

    goto/32 :goto_5

    nop

    :goto_64
    iget-object p1, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mTaskViewWithoutHeaderRect:Landroid/graphics/Rect;

    goto/32 :goto_1

    nop

    :goto_65
    iget-object v4, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mTaskViewWithoutHeaderRect:Landroid/graphics/Rect;

    goto/32 :goto_42

    nop

    :goto_66
    int-to-float p1, p1

    goto/32 :goto_36

    nop
.end method

.method protected onAttachedToWindow()V
    .locals 1

    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    invoke-static {}, Lcom/miui/home/recents/util/WindowCornerRadiusUtil;->getTaskViewCornerRadius()I

    move-result v0

    iput v0, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mCornerRadius:I

    return-void
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/view/View;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    return-void
.end method

.method public onDarkModeChanged()V
    .locals 0

    invoke-direct {p0}, Lcom/miui/home/recents/views/TaskViewThumbnail;->updateDrawingPropertiesAndRequestDraw()V

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 16

    move-object/from16 v1, p0

    move-object/from16 v10, p1

    invoke-virtual/range {p0 .. p0}, Lcom/miui/home/recents/views/TaskViewThumbnail;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Lcom/miui/home/recents/views/TaskView;

    invoke-virtual {v0}, Lcom/miui/home/recents/views/TaskView;->getFullscreenProgress()F

    move-result v0

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    invoke-virtual/range {p0 .. p0}, Lcom/miui/home/recents/views/TaskViewThumbnail;->getWidth()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual/range {p0 .. p0}, Lcom/miui/home/recents/views/TaskViewThumbnail;->getHeight()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, v1, Lcom/miui/home/recents/views/TaskViewThumbnail;->mScaledInsets:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    int-to-float v4, v4

    mul-float/2addr v4, v0

    add-float/2addr v3, v4

    const/4 v11, 0x0

    invoke-virtual {v10, v11, v11, v2, v3}, Landroid/graphics/Canvas;->clipRect(FFFF)Z

    invoke-virtual/range {p0 .. p0}, Lcom/miui/home/recents/views/TaskViewThumbnail;->getTaskViewHeaderHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v10, v11, v2}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, v1, Lcom/miui/home/recents/views/TaskViewThumbnail;->mTask:Lcom/android/systemui/shared/recents/model/Task;

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/android/systemui/shared/recents/model/Task;->useMultipleThumbnail()Z

    move-result v2

    if-eqz v2, :cond_0

    move v12, v3

    goto :goto_0

    :cond_0
    move v12, v4

    :goto_0
    if-eqz v12, :cond_1

    iget-object v2, v1, Lcom/miui/home/recents/views/TaskViewThumbnail;->mFirstTaskViewRect:Landroid/graphics/Rect;

    goto :goto_1

    :cond_1
    iget-object v2, v1, Lcom/miui/home/recents/views/TaskViewThumbnail;->mTaskViewWithoutHeaderRect:Landroid/graphics/Rect;

    :goto_1
    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    if-eqz v12, :cond_2

    iget-object v5, v1, Lcom/miui/home/recents/views/TaskViewThumbnail;->mFirstTaskViewRect:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v5

    goto :goto_2

    :cond_2
    iget-object v5, v1, Lcom/miui/home/recents/views/TaskViewThumbnail;->mTaskViewWithoutHeaderRect:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v5

    int-to-float v5, v5

    iget v6, v1, Lcom/miui/home/recents/views/TaskViewThumbnail;->mTaskRatio:F

    mul-float/2addr v5, v6

    float-to-int v5, v5

    :goto_2
    iget-object v6, v1, Lcom/miui/home/recents/views/TaskViewThumbnail;->mBitmapShaderDrawInfo:Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;

    invoke-static {v6}, Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;->access$000(Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;)I

    move-result v6

    invoke-static {v2, v6}, Ljava/lang/Math;->min(II)I

    move-result v6

    iget-object v7, v1, Lcom/miui/home/recents/views/TaskViewThumbnail;->mBitmapShaderDrawInfo:Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;

    invoke-static {v7}, Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;->access$100(Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;)I

    move-result v7

    invoke-static {v5, v7}, Ljava/lang/Math;->min(II)I

    move-result v7

    if-eqz v12, :cond_3

    iget-object v8, v1, Lcom/miui/home/recents/views/TaskViewThumbnail;->mBitmapShaderDrawInfo:Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;

    iget-boolean v8, v8, Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;->isRecentContainerLandscape:Z

    if-nez v8, :cond_3

    goto :goto_3

    :cond_3
    move v3, v4

    :goto_3
    invoke-static {}, Lcom/miui/home/launcher/DeviceConfig;->isInMultiWindowMode()Z

    move-result v8

    if-nez v8, :cond_4

    if-nez v3, :cond_4

    int-to-float v3, v5

    iget-object v5, v1, Lcom/miui/home/recents/views/TaskViewThumbnail;->mScaledInsets:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    int-to-float v5, v5

    mul-float/2addr v5, v0

    add-float/2addr v3, v5

    float-to-int v5, v3

    invoke-direct/range {p0 .. p0}, Lcom/miui/home/recents/views/TaskViewThumbnail;->useCenterLayoutForThumbnail()Z

    move-result v3

    if-nez v3, :cond_4

    int-to-float v3, v7

    iget-object v7, v1, Lcom/miui/home/recents/views/TaskViewThumbnail;->mScaledInsets:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->bottom:I

    int-to-float v7, v7

    mul-float/2addr v7, v0

    add-float/2addr v3, v7

    float-to-int v7, v3

    :cond_4
    iget-object v3, v1, Lcom/miui/home/recents/views/TaskViewThumbnail;->mThumbnailDrawingRect:Landroid/graphics/RectF;

    iget-object v8, v1, Lcom/miui/home/recents/views/TaskViewThumbnail;->mBitmapShaderDrawInfo:Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;

    invoke-static {v8}, Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;->access$200(Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;)I

    move-result v8

    int-to-float v8, v8

    iget-object v9, v1, Lcom/miui/home/recents/views/TaskViewThumbnail;->mBitmapShaderDrawInfo:Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;

    invoke-static {v9}, Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;->access$300(Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;)I

    move-result v9

    int-to-float v9, v9

    iget-object v13, v1, Lcom/miui/home/recents/views/TaskViewThumbnail;->mBitmapShaderDrawInfo:Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;

    invoke-static {v13}, Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;->access$200(Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;)I

    move-result v13

    add-int/2addr v13, v6

    int-to-float v13, v13

    iget-object v14, v1, Lcom/miui/home/recents/views/TaskViewThumbnail;->mBitmapShaderDrawInfo:Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;

    invoke-static {v14}, Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;->access$300(Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;)I

    move-result v14

    add-int/2addr v14, v7

    int-to-float v14, v14

    invoke-virtual {v3, v8, v9, v13, v14}, Landroid/graphics/RectF;->set(FFFF)V

    if-eqz v12, :cond_5

    iget-object v3, v1, Lcom/miui/home/recents/views/TaskViewThumbnail;->mSecondTaskViewRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v4

    iget-object v3, v1, Lcom/miui/home/recents/views/TaskViewThumbnail;->mSecondTaskViewRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    iget-object v8, v1, Lcom/miui/home/recents/views/TaskViewThumbnail;->mSecondBitmapShaderDrawInfo:Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;

    invoke-static {v8}, Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;->access$000(Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;)I

    move-result v8

    invoke-static {v4, v8}, Ljava/lang/Math;->min(II)I

    move-result v8

    iget-object v9, v1, Lcom/miui/home/recents/views/TaskViewThumbnail;->mSecondBitmapShaderDrawInfo:Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;

    invoke-static {v9}, Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;->access$100(Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;)I

    move-result v9

    invoke-static {v3, v9}, Ljava/lang/Math;->min(II)I

    move-result v9

    int-to-float v9, v9

    iget-object v13, v1, Lcom/miui/home/recents/views/TaskViewThumbnail;->mScaledInsets:Landroid/graphics/Rect;

    iget v13, v13, Landroid/graphics/Rect;->bottom:I

    int-to-float v13, v13

    mul-float/2addr v13, v0

    add-float/2addr v9, v13

    float-to-int v9, v9

    int-to-float v3, v3

    iget-object v13, v1, Lcom/miui/home/recents/views/TaskViewThumbnail;->mScaledInsets:Landroid/graphics/Rect;

    iget v13, v13, Landroid/graphics/Rect;->bottom:I

    int-to-float v13, v13

    mul-float/2addr v13, v0

    add-float/2addr v3, v13

    float-to-int v0, v3

    iget-object v3, v1, Lcom/miui/home/recents/views/TaskViewThumbnail;->mSecondThumbnailDrawingRect:Landroid/graphics/RectF;

    iget-object v13, v1, Lcom/miui/home/recents/views/TaskViewThumbnail;->mSecondBitmapShaderDrawInfo:Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;

    invoke-static {v13}, Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;->access$200(Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;)I

    move-result v13

    int-to-float v13, v13

    iget-object v14, v1, Lcom/miui/home/recents/views/TaskViewThumbnail;->mSecondBitmapShaderDrawInfo:Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;

    invoke-static {v14}, Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;->access$300(Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;)I

    move-result v14

    int-to-float v14, v14

    iget-object v15, v1, Lcom/miui/home/recents/views/TaskViewThumbnail;->mSecondBitmapShaderDrawInfo:Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;

    invoke-static {v15}, Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;->access$200(Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;)I

    move-result v15

    add-int/2addr v8, v15

    int-to-float v8, v8

    iget-object v15, v1, Lcom/miui/home/recents/views/TaskViewThumbnail;->mSecondBitmapShaderDrawInfo:Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;

    invoke-static {v15}, Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;->access$300(Lcom/miui/home/recents/views/TaskViewThumbnail$BitmapShaderDrawInfo;)I

    move-result v15

    add-int/2addr v9, v15

    int-to-float v9, v9

    invoke-virtual {v3, v13, v14, v8, v9}, Landroid/graphics/RectF;->set(FFFF)V

    move v13, v0

    move v0, v4

    goto :goto_4

    :cond_5
    move v0, v4

    move v13, v0

    :goto_4
    iget-object v3, v1, Lcom/miui/home/recents/views/TaskViewThumbnail;->mBitmapShader:Landroid/graphics/BitmapShader;

    if-eqz v3, :cond_a

    if-lez v6, :cond_a

    if-lez v7, :cond_a

    sub-int v0, v2, v6

    sub-int v3, v5, v7

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v4

    const/4 v8, 0x2

    if-le v4, v8, :cond_6

    goto :goto_5

    :cond_6
    move v2, v6

    :goto_5
    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v4

    if-le v4, v8, :cond_7

    goto :goto_6

    :cond_7
    move v5, v7

    :goto_6
    iget-object v4, v1, Lcom/miui/home/recents/views/TaskViewThumbnail;->mTask:Lcom/android/systemui/shared/recents/model/Task;

    if-eqz v4, :cond_9

    invoke-virtual {v4}, Lcom/android/systemui/shared/recents/model/Task;->useMultipleThumbnail()Z

    move-result v4

    if-nez v4, :cond_9

    if-gt v0, v8, :cond_8

    if-le v3, v8, :cond_9

    :cond_8
    const/4 v3, 0x0

    const/4 v4, 0x0

    int-to-float v0, v2

    int-to-float v6, v5

    iget v2, v1, Lcom/miui/home/recents/views/TaskViewThumbnail;->mCornerRadius:I

    int-to-float v7, v2

    int-to-float v8, v2

    iget-object v9, v1, Lcom/miui/home/recents/views/TaskViewThumbnail;->mBgFillPaint:Landroid/graphics/Paint;

    move-object/from16 v2, p1

    move v5, v0

    invoke-virtual/range {v2 .. v9}, Landroid/graphics/Canvas;->drawRoundRect(FFFFFFLandroid/graphics/Paint;)V

    :cond_9
    :try_start_0
    iget-object v0, v1, Lcom/miui/home/recents/views/TaskViewThumbnail;->mThumbnailDrawingRect:Landroid/graphics/RectF;

    iget v2, v1, Lcom/miui/home/recents/views/TaskViewThumbnail;->mCornerRadius:I

    int-to-float v2, v2

    iget v3, v1, Lcom/miui/home/recents/views/TaskViewThumbnail;->mCornerRadius:I

    int-to-float v3, v3

    iget-object v4, v1, Lcom/miui/home/recents/views/TaskViewThumbnail;->mDrawPaint:Landroid/graphics/Paint;

    invoke-virtual {v10, v0, v2, v3, v4}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    if-eqz v12, :cond_b

    iget-object v0, v1, Lcom/miui/home/recents/views/TaskViewThumbnail;->mSecondThumbnailDrawingRect:Landroid/graphics/RectF;

    iget v2, v1, Lcom/miui/home/recents/views/TaskViewThumbnail;->mCornerRadius:I

    int-to-float v2, v2

    iget v3, v1, Lcom/miui/home/recents/views/TaskViewThumbnail;->mCornerRadius:I

    int-to-float v3, v3

    iget-object v4, v1, Lcom/miui/home/recents/views/TaskViewThumbnail;->mSecondDrawPaint:Landroid/graphics/Paint;

    invoke-virtual {v10, v0, v2, v3, v4}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_7

    :catch_0
    move-exception v0

    const-string v2, "TaskViewThumbnail"

    const-string v3, "ondraw"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_7

    :cond_a
    const/4 v3, 0x0

    const/4 v4, 0x0

    int-to-float v14, v2

    int-to-float v15, v5

    iget v2, v1, Lcom/miui/home/recents/views/TaskViewThumbnail;->mCornerRadius:I

    int-to-float v7, v2

    int-to-float v8, v2

    iget-object v9, v1, Lcom/miui/home/recents/views/TaskViewThumbnail;->mBgFillPaint:Landroid/graphics/Paint;

    move-object/from16 v2, p1

    move v5, v14

    move v6, v15

    invoke-virtual/range {v2 .. v9}, Landroid/graphics/Canvas;->drawRoundRect(FFFFFFLandroid/graphics/Paint;)V

    iget-object v2, v1, Lcom/miui/home/recents/views/TaskViewThumbnail;->mThumbnailDrawingRect:Landroid/graphics/RectF;

    invoke-virtual {v2, v11, v11, v14, v15}, Landroid/graphics/RectF;->set(FFFF)V

    if-eqz v12, :cond_b

    iget-object v2, v1, Lcom/miui/home/recents/views/TaskViewThumbnail;->mSecondThumbnailDrawingRect:Landroid/graphics/RectF;

    iget v3, v2, Landroid/graphics/RectF;->left:F

    iget-object v2, v1, Lcom/miui/home/recents/views/TaskViewThumbnail;->mSecondThumbnailDrawingRect:Landroid/graphics/RectF;

    iget v4, v2, Landroid/graphics/RectF;->top:F

    iget-object v2, v1, Lcom/miui/home/recents/views/TaskViewThumbnail;->mSecondThumbnailDrawingRect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    int-to-float v0, v0

    add-float v5, v2, v0

    iget-object v0, v1, Lcom/miui/home/recents/views/TaskViewThumbnail;->mSecondThumbnailDrawingRect:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->top:F

    int-to-float v2, v13

    add-float v6, v0, v2

    iget v0, v1, Lcom/miui/home/recents/views/TaskViewThumbnail;->mCornerRadius:I

    int-to-float v7, v0

    int-to-float v8, v0

    iget-object v9, v1, Lcom/miui/home/recents/views/TaskViewThumbnail;->mBgFillPaint:Landroid/graphics/Paint;

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v9}, Landroid/graphics/Canvas;->drawRoundRect(FFFFFFLandroid/graphics/Paint;)V

    :cond_b
    :goto_7
    iget-object v0, v1, Lcom/miui/home/recents/views/TaskViewThumbnail;->mTask:Lcom/android/systemui/shared/recents/model/Task;

    if-eqz v0, :cond_c

    invoke-virtual {v0}, Lcom/android/systemui/shared/recents/model/Task;->isCoverThumbnail()Z

    move-result v0

    if-eqz v0, :cond_c

    iget-object v0, v1, Lcom/miui/home/recents/views/TaskViewThumbnail;->mThumbnailDrawingRect:Landroid/graphics/RectF;

    iget v2, v1, Lcom/miui/home/recents/views/TaskViewThumbnail;->mCornerRadius:I

    int-to-float v3, v2

    int-to-float v2, v2

    iget-object v4, v1, Lcom/miui/home/recents/views/TaskViewThumbnail;->mCoverPaint:Landroid/graphics/Paint;

    invoke-virtual {v10, v0, v3, v2, v4}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    :cond_c
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    return-void
.end method

.method public onTaskViewSizeChanged(II)V
    .locals 3

    iget-object v0, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mTaskViewWithoutHeaderRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mTaskViewWithoutHeaderRect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    if-ne v0, p2, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mTaskViewWithoutHeaderRect:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/miui/home/recents/views/TaskViewThumbnail;->getTaskViewHeaderHeight()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1, p1, p2}, Landroid/graphics/Rect;->set(IIII)V

    invoke-static {p0, v2, v2, p1, p2}, Lcom/android/systemui/shared/recents/system/ViewWrapper;->set(Landroid/view/View;IIII)V

    invoke-direct {p0}, Lcom/miui/home/recents/views/TaskViewThumbnail;->updateThumbnailDrawInfo()V

    return-void
.end method

.method public reset()V
    .locals 2

    invoke-static {}, Lcom/miui/home/recents/util/SpringAnimationUtils;->getInstance()Lcom/miui/home/recents/util/SpringAnimationUtils;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mSpringAnimationImpl:Lcom/miui/home/recents/util/SpringAnimationImpl;

    invoke-virtual {v0, v1}, Lcom/miui/home/recents/util/SpringAnimationUtils;->cancelAllSpringAnimation(Lcom/miui/home/recents/util/SpringAnimationImpl;)V

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0, v0}, Lcom/miui/home/recents/views/TaskViewThumbnail;->setAlpha(F)V

    invoke-virtual {p0, v0}, Lcom/miui/home/recents/views/TaskViewThumbnail;->setScaleX(F)V

    invoke-virtual {p0, v0}, Lcom/miui/home/recents/views/TaskViewThumbnail;->setScaleY(F)V

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/miui/home/recents/views/TaskViewThumbnail;->setTranslationX(F)V

    invoke-virtual {p0, v1}, Lcom/miui/home/recents/views/TaskViewThumbnail;->setTranslationY(F)V

    invoke-static {}, Lcom/miui/home/recents/util/WindowCornerRadiusUtil;->getTaskViewCornerRadius()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/miui/home/recents/views/TaskViewThumbnail;->setCornerRadius(I)V

    invoke-virtual {p0, v0}, Lcom/miui/home/recents/views/TaskViewThumbnail;->setTaskRatio(F)V

    return-void
.end method

.method public setCornerRadius(I)V
    .locals 0

    iput p1, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mCornerRadius:I

    invoke-virtual {p0}, Lcom/miui/home/recents/views/TaskViewThumbnail;->invalidate()V

    return-void
.end method

.method public setDimAlpha(F)V
    .locals 0

    iput p1, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mDimAlpha:F

    invoke-virtual {p0}, Lcom/miui/home/recents/views/TaskViewThumbnail;->updateThumbnailPaintFilter()V

    return-void
.end method

.method setSecondThumbnail(Landroid/graphics/Bitmap;Lcom/android/systemui/shared/recents/model/TaskThumbnailInfo;Lcom/android/wm/shell/util/StagedSplitBounds;)V
    .locals 4

    goto/32 :goto_7

    nop

    :goto_0
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    goto/32 :goto_5

    nop

    :goto_1
    int-to-float v2, v2

    goto/32 :goto_21

    nop

    :goto_2
    return-void

    :goto_3
    goto/32 :goto_23

    nop

    :goto_4
    iput-object p3, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mSplitBounds:Lcom/android/wm/shell/util/StagedSplitBounds;

    goto/32 :goto_c

    nop

    :goto_5
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result p1

    goto/32 :goto_24

    nop

    :goto_6
    iget-object v2, v2, Lcom/android/systemui/shared/recents/model/TaskThumbnailInfo;->insets:Landroid/graphics/Rect;

    goto/32 :goto_2a

    nop

    :goto_7
    if-nez p1, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_9

    nop

    :goto_8
    float-to-int p1, p1

    goto/32 :goto_15

    nop

    :goto_9
    if-eqz p2, :cond_1

    goto/32 :goto_14

    :cond_1
    goto/32 :goto_13

    nop

    :goto_a
    int-to-float p1, p1

    goto/32 :goto_e

    nop

    :goto_b
    iget-object v0, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mSecondDrawPaint:Landroid/graphics/Paint;

    goto/32 :goto_17

    nop

    :goto_c
    invoke-direct {p0}, Lcom/miui/home/recents/views/TaskViewThumbnail;->updateThumbnailDrawInfo()V

    goto/32 :goto_2

    nop

    :goto_d
    invoke-direct {v0, p1, v1, v2}, Landroid/graphics/BitmapShader;-><init>(Landroid/graphics/Bitmap;Landroid/graphics/Shader$TileMode;Landroid/graphics/Shader$TileMode;)V

    goto/32 :goto_1e

    nop

    :goto_e
    iget-object v2, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mSecondThumbnailInfo:Lcom/android/systemui/shared/recents/model/TaskThumbnailInfo;

    goto/32 :goto_6

    nop

    :goto_f
    iget-object p2, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mSecondThumbnailInfo:Lcom/android/systemui/shared/recents/model/TaskThumbnailInfo;

    goto/32 :goto_10

    nop

    :goto_10
    iget-object p2, p2, Lcom/android/systemui/shared/recents/model/TaskThumbnailInfo;->insets:Landroid/graphics/Rect;

    goto/32 :goto_22

    nop

    :goto_11
    sget-object v1, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    goto/32 :goto_1f

    nop

    :goto_12
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result p1

    goto/32 :goto_a

    nop

    :goto_13
    goto :goto_3

    :goto_14
    goto/32 :goto_1a

    nop

    :goto_15
    invoke-virtual {p2, v0, v0, v1, p1}, Landroid/graphics/Rect;->set(IIII)V

    goto/32 :goto_18

    nop

    :goto_16
    iget-object p2, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mSecondThumbnailBitmapRect:Landroid/graphics/Rect;

    goto/32 :goto_26

    nop

    :goto_17
    iget-object v1, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mSecondBitmapShader:Landroid/graphics/BitmapShader;

    goto/32 :goto_1b

    nop

    :goto_18
    goto :goto_25

    :goto_19
    goto/32 :goto_27

    nop

    :goto_1a
    new-instance v0, Landroid/graphics/BitmapShader;

    goto/32 :goto_11

    nop

    :goto_1b
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    goto/32 :goto_1c

    nop

    :goto_1c
    iput-object p2, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mSecondThumbnailInfo:Lcom/android/systemui/shared/recents/model/TaskThumbnailInfo;

    goto/32 :goto_f

    nop

    :goto_1d
    return-void

    :goto_1e
    iput-object v0, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mSecondBitmapShader:Landroid/graphics/BitmapShader;

    goto/32 :goto_b

    nop

    :goto_1f
    sget-object v2, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    goto/32 :goto_d

    nop

    :goto_20
    sub-float/2addr p1, v2

    goto/32 :goto_8

    nop

    :goto_21
    iget v3, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mFullscreenThumbnailScale:F

    goto/32 :goto_29

    nop

    :goto_22
    const/4 v0, 0x0

    goto/32 :goto_28

    nop

    :goto_23
    invoke-virtual {p0}, Lcom/miui/home/recents/views/TaskViewThumbnail;->clearThumbnail()V

    goto/32 :goto_1d

    nop

    :goto_24
    invoke-virtual {p2, v0, v0, v1, p1}, Landroid/graphics/Rect;->set(IIII)V

    :goto_25
    goto/32 :goto_4

    nop

    :goto_26
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    goto/32 :goto_12

    nop

    :goto_27
    iget-object p2, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mSecondThumbnailBitmapRect:Landroid/graphics/Rect;

    goto/32 :goto_0

    nop

    :goto_28
    if-nez p2, :cond_2

    goto/32 :goto_19

    :cond_2
    goto/32 :goto_16

    nop

    :goto_29
    mul-float/2addr v2, v3

    goto/32 :goto_20

    nop

    :goto_2a
    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    goto/32 :goto_1

    nop
.end method

.method public setTaskRatio(F)V
    .locals 1

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-static {v0, p1}, Ljava/lang/Math;->min(FF)F

    move-result p1

    const/4 v0, 0x0

    invoke-static {v0, p1}, Ljava/lang/Math;->max(FF)F

    move-result p1

    iget v0, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mTaskRatio:F

    cmpl-float v0, p1, v0

    if-eqz v0, :cond_0

    iput p1, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mTaskRatio:F

    invoke-virtual {p0}, Lcom/miui/home/recents/views/TaskViewThumbnail;->invalidate()V

    :cond_0
    return-void
.end method

.method setThumbnail(Landroid/graphics/Bitmap;Lcom/android/systemui/shared/recents/model/TaskThumbnailInfo;)V
    .locals 4

    goto/32 :goto_11

    nop

    :goto_0
    sget-object v2, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    goto/32 :goto_23

    nop

    :goto_1
    iget p2, p2, Lcom/android/systemui/shared/recents/model/TaskThumbnailInfo;->scale:F

    goto/32 :goto_3

    nop

    :goto_2
    sget-object v1, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    goto/32 :goto_0

    nop

    :goto_3
    iput p2, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mFullscreenThumbnailScale:F

    goto/32 :goto_f

    nop

    :goto_4
    return-void

    :goto_5
    goto/32 :goto_b

    nop

    :goto_6
    iget-object v2, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mThumbnailInfo:Lcom/android/systemui/shared/recents/model/TaskThumbnailInfo;

    goto/32 :goto_13

    nop

    :goto_7
    invoke-virtual {p2, v0, v0, v1, p1}, Landroid/graphics/Rect;->set(IIII)V

    goto/32 :goto_20

    nop

    :goto_8
    iget-object v1, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mBitmapShader:Landroid/graphics/BitmapShader;

    goto/32 :goto_29

    nop

    :goto_9
    iget-object p2, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mThumbnailInfo:Lcom/android/systemui/shared/recents/model/TaskThumbnailInfo;

    goto/32 :goto_1

    nop

    :goto_a
    mul-float/2addr v2, v3

    goto/32 :goto_c

    nop

    :goto_b
    invoke-virtual {p0}, Lcom/miui/home/recents/views/TaskViewThumbnail;->clearThumbnail()V

    goto/32 :goto_1c

    nop

    :goto_c
    sub-float/2addr p1, v2

    goto/32 :goto_12

    nop

    :goto_d
    goto :goto_5

    :goto_e
    goto/32 :goto_2c

    nop

    :goto_f
    iget-object p2, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mThumbnailInfo:Lcom/android/systemui/shared/recents/model/TaskThumbnailInfo;

    goto/32 :goto_24

    nop

    :goto_10
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    goto/32 :goto_28

    nop

    :goto_11
    if-nez p1, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_1b

    nop

    :goto_12
    float-to-int p1, p1

    goto/32 :goto_7

    nop

    :goto_13
    iget-object v2, v2, Lcom/android/systemui/shared/recents/model/TaskThumbnailInfo;->insets:Landroid/graphics/Rect;

    goto/32 :goto_1a

    nop

    :goto_14
    iget-object v0, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mDrawPaint:Landroid/graphics/Paint;

    goto/32 :goto_8

    nop

    :goto_15
    int-to-float v2, v2

    goto/32 :goto_2b

    nop

    :goto_16
    iput-object p2, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mThumbnailInfo:Lcom/android/systemui/shared/recents/model/TaskThumbnailInfo;

    goto/32 :goto_9

    nop

    :goto_17
    if-nez p2, :cond_1

    goto/32 :goto_21

    :cond_1
    goto/32 :goto_25

    nop

    :goto_18
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result p1

    goto/32 :goto_22

    nop

    :goto_19
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    goto/32 :goto_18

    nop

    :goto_1a
    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    goto/32 :goto_15

    nop

    :goto_1b
    if-eqz p2, :cond_2

    goto/32 :goto_e

    :cond_2
    goto/32 :goto_d

    nop

    :goto_1c
    return-void

    :goto_1d
    invoke-virtual {p2, v0, v0, v1, p1}, Landroid/graphics/Rect;->set(IIII)V

    :goto_1e
    goto/32 :goto_26

    nop

    :goto_1f
    iget-object p2, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mThumbnailBitmapRect:Landroid/graphics/Rect;

    goto/32 :goto_10

    nop

    :goto_20
    goto :goto_1e

    :goto_21
    goto/32 :goto_1f

    nop

    :goto_22
    int-to-float p1, p1

    goto/32 :goto_6

    nop

    :goto_23
    invoke-direct {v0, p1, v1, v2}, Landroid/graphics/BitmapShader;-><init>(Landroid/graphics/Bitmap;Landroid/graphics/Shader$TileMode;Landroid/graphics/Shader$TileMode;)V

    goto/32 :goto_2a

    nop

    :goto_24
    iget-object p2, p2, Lcom/android/systemui/shared/recents/model/TaskThumbnailInfo;->insets:Landroid/graphics/Rect;

    goto/32 :goto_27

    nop

    :goto_25
    iget-object p2, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mThumbnailBitmapRect:Landroid/graphics/Rect;

    goto/32 :goto_19

    nop

    :goto_26
    invoke-direct {p0}, Lcom/miui/home/recents/views/TaskViewThumbnail;->updateThumbnailDrawInfo()V

    goto/32 :goto_4

    nop

    :goto_27
    const/4 v0, 0x0

    goto/32 :goto_17

    nop

    :goto_28
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result p1

    goto/32 :goto_1d

    nop

    :goto_29
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    goto/32 :goto_16

    nop

    :goto_2a
    iput-object v0, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mBitmapShader:Landroid/graphics/BitmapShader;

    goto/32 :goto_14

    nop

    :goto_2b
    iget v3, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mFullscreenThumbnailScale:F

    goto/32 :goto_a

    nop

    :goto_2c
    new-instance v0, Landroid/graphics/BitmapShader;

    goto/32 :goto_2

    nop
.end method

.method unbindFromTask()V
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    const/4 v0, 0x0

    goto/32 :goto_2

    nop

    :goto_1
    return-void

    :goto_2
    iput-object v0, p0, Lcom/miui/home/recents/views/TaskViewThumbnail;->mTask:Lcom/android/systemui/shared/recents/model/Task;

    goto/32 :goto_3

    nop

    :goto_3
    invoke-virtual {p0}, Lcom/miui/home/recents/views/TaskViewThumbnail;->clearThumbnail()V

    goto/32 :goto_1

    nop
.end method

.method updateThumbnailPaintFilter()V
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    invoke-virtual {p0}, Lcom/miui/home/recents/views/TaskViewThumbnail;->invalidate()V

    goto/32 :goto_1

    nop

    :goto_1
    return-void
.end method
