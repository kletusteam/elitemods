.class public Lcom/miui/home/launcher/maml/auth/MaMlAuthManager;
.super Ljava/lang/Object;


# instance fields
.field private mAuthFailureHandlers:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Lcom/miui/home/launcher/maml/auth/AuthFailureHandler;",
            ">;"
        }
    .end annotation
.end field

.field private mClientRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/miui/launcher/overlay/client/LauncherClient;",
            ">;"
        }
    .end annotation
.end field

.field private mLastCheckTime:J

.field private mLauncherRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/miui/home/launcher/Launcher;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/miui/home/launcher/Launcher;Lcom/miui/launcher/overlay/client/LauncherClient;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/miui/home/launcher/maml/auth/MaMlAuthManager;->mLauncherRef:Ljava/lang/ref/WeakReference;

    new-instance p1, Ljava/lang/ref/WeakReference;

    invoke-direct {p1, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/miui/home/launcher/maml/auth/MaMlAuthManager;->mClientRef:Ljava/lang/ref/WeakReference;

    new-instance p1, Landroid/util/SparseArray;

    sget-object p2, Lcom/miui/home/launcher/maml/auth/AuthFailureHandler;->STRATEGIES:[I

    array-length p2, p2

    invoke-direct {p1, p2}, Landroid/util/SparseArray;-><init>(I)V

    iput-object p1, p0, Lcom/miui/home/launcher/maml/auth/MaMlAuthManager;->mAuthFailureHandlers:Landroid/util/SparseArray;

    sget-object p1, Lcom/miui/home/launcher/maml/auth/AuthFailureHandler;->STRATEGIES:[I

    array-length p2, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p2, :cond_0

    aget v1, p1, v0

    iget-object v2, p0, Lcom/miui/home/launcher/maml/auth/MaMlAuthManager;->mAuthFailureHandlers:Landroid/util/SparseArray;

    invoke-static {v1}, Lcom/miui/home/launcher/maml/auth/AuthFailureHandler;->getFailureHandler(I)Lcom/miui/home/launcher/maml/auth/AuthFailureHandler;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private checkAuthority(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Lcom/miui/home/launcher/maml/MaMlWidgetInfo;",
            ">;)V"
        }
    .end annotation

    new-instance v0, Lcom/miui/home/launcher/maml/auth/-$$Lambda$MaMlAuthManager$p4J4Vs_4hM246VUo48Dbqg9aiYw;

    invoke-direct {v0, p0, p1}, Lcom/miui/home/launcher/maml/auth/-$$Lambda$MaMlAuthManager$p4J4Vs_4hM246VUo48Dbqg9aiYw;-><init>(Lcom/miui/home/launcher/maml/auth/MaMlAuthManager;Ljava/util/Set;)V

    invoke-static {v0}, Lcom/miui/home/library/utils/AsyncTaskExecutorHelper;->execParallel(Ljava/lang/Runnable;)V

    return-void
.end method

.method private getFailureHandler(Lcom/miui/home/launcher/maml/MaMlWidgetInfo;I)Lcom/miui/home/launcher/maml/auth/AuthFailureHandler;
    .locals 0

    invoke-virtual {p1}, Lcom/miui/home/launcher/maml/MaMlWidgetInfo;->supportAuth()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p2, 0x1

    :cond_0
    invoke-static {p2}, Lcom/miui/home/launcher/maml/auth/AuthFailureHandler;->getFailureHandler(I)Lcom/miui/home/launcher/maml/auth/AuthFailureHandler;

    move-result-object p1

    return-object p1
.end method

.method public static synthetic lambda$checkAuthority$0(Lcom/miui/home/launcher/maml/auth/MaMlAuthManager;Ljava/util/Set;)V
    .locals 8

    iget-object v0, p0, Lcom/miui/home/launcher/maml/auth/MaMlAuthManager;->mLauncherRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/home/launcher/Launcher;

    if-nez v0, :cond_0

    const-string p1, "MaMlAuthManager"

    const-string v0, "checkAuthority skip, Launcher is null"

    invoke-static {p1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    invoke-virtual {v0}, Lcom/miui/home/launcher/Launcher;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/miui/home/launcher/maml/auth/MaMlAuthManager;->shouldCheck(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    return-void

    :cond_1
    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/miui/home/launcher/maml/auth/MaMlAuthManager;->mLastCheckTime:J

    const-string v1, "MaMlAuthManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "start checkAuthority : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v3, p0, Lcom/miui/home/launcher/maml/auth/MaMlAuthManager;->mLastCheckTime:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/miui/home/launcher/maml/auth/MaMlAuthManager;->queryCloudStrategy()I

    move-result v1

    const-string v2, "MaMlAuthManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "checkAuthority strategy : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_2
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/home/launcher/maml/MaMlWidgetInfo;

    iget-object v3, v2, Lcom/miui/home/launcher/maml/MaMlWidgetInfo;->productId:Ljava/lang/String;

    iget v4, v2, Lcom/miui/home/launcher/maml/MaMlWidgetInfo;->versionCode:I

    invoke-static {v3, v4}, Lcom/miui/home/launcher/widget/MIUIWidgetCompat;->getMaMlDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v2, Lcom/miui/home/launcher/maml/MaMlWidgetInfo;->productId:Ljava/lang/String;

    invoke-static {v0, v4, v3}, Lcom/miui/maml/widget/edit/MamlDrmUtilKt;->isLegal(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lmiui/drm/DrmManager$DrmResult;

    move-result-object v4

    const-string v5, "MaMlAuthManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "checkAuthority result : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Lmiui/drm/DrmManager$DrmResult;->name()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, " for "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Lcom/miui/home/launcher/maml/MaMlWidgetInfo;->printDetail()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v5, Lmiui/drm/DrmManager$DrmResult;->DRM_SUCCESS:Lmiui/drm/DrmManager$DrmResult;

    if-eq v4, v5, :cond_2

    invoke-direct {p0, v2, v1, v3}, Lcom/miui/home/launcher/maml/auth/MaMlAuthManager;->recheckAuthority(Lcom/miui/home/launcher/maml/MaMlWidgetInfo;ILjava/lang/String;)V

    goto :goto_0

    :cond_3
    const-string p1, "key_last_maml_auth_check_time"

    iget-wide v1, p0, Lcom/miui/home/launcher/maml/auth/MaMlAuthManager;->mLastCheckTime:J

    invoke-static {v0, p1, v1, v2}, Lcom/miui/home/launcher/common/PreferenceUtils;->putLong(Landroid/content/Context;Ljava/lang/String;J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    const-string v0, "MaMlAuthManager"

    const-string v1, "checkAuthority error"

    invoke-static {v0, v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_1
    return-void
.end method

.method public static synthetic lambda$recheckAuthority$1(Lcom/miui/home/launcher/maml/auth/MaMlAuthManager;Lcom/miui/home/launcher/maml/MaMlWidgetInfo;Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 10

    iget-object v0, p0, Lcom/miui/home/launcher/maml/auth/MaMlAuthManager;->mLauncherRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/home/launcher/Launcher;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const-string p1, "MaMlAuthManager"

    const-string p2, "recheckAuthority skip, Launcher is null"

    invoke-static {p1, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1

    :cond_0
    invoke-virtual {v0}, Lcom/miui/home/launcher/Launcher;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v3, p1, Lcom/miui/home/launcher/maml/MaMlWidgetInfo;->productId:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/miui/home/launcher/maml/MaMlWidgetInfo;->getTitle()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p1, Lcom/miui/home/launcher/maml/MaMlWidgetInfo;->maMlDownloadUrl:Ljava/lang/String;

    iget v6, p1, Lcom/miui/home/launcher/maml/MaMlWidgetInfo;->versionCode:I

    iget-wide v7, p1, Lcom/miui/home/launcher/maml/MaMlWidgetInfo;->mtzSizeInKb:J

    long-to-int v7, v7

    const/4 v9, 0x0

    move-object v2, v0

    move-object v8, p2

    invoke-static/range {v2 .. v9}, Lcom/miui/maml/widget/edit/MamlutilKt;->downloadAndCopyRight(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;Z)I

    move-result v2

    const-string v3, "MaMlAuthManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "downloadAndCopyRight result : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v5, " for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v5, p1, Lcom/miui/home/launcher/maml/MaMlWidgetInfo;->id:J

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v2, :cond_1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1

    :cond_1
    iget-object v2, p1, Lcom/miui/home/launcher/maml/MaMlWidgetInfo;->productId:Ljava/lang/String;

    invoke-static {v0, v2, p2}, Lcom/miui/maml/widget/edit/MamlDrmUtilKt;->isLegal(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lmiui/drm/DrmManager$DrmResult;

    move-result-object p2

    const-string v0, "MaMlAuthManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "recheckAuthority result : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lmiui/drm/DrmManager$DrmResult;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v3, p1, Lcom/miui/home/launcher/maml/MaMlWidgetInfo;->id:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-object p1, Lmiui/drm/DrmManager$DrmResult;->DRM_SUCCESS:Lmiui/drm/DrmManager$DrmResult;

    if-ne p2, p1, :cond_2

    const/4 v1, 0x1

    :cond_2
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public static synthetic lambda$recheckAuthority$2(Lcom/miui/home/launcher/maml/auth/MaMlAuthManager;Lcom/miui/home/launcher/maml/MaMlWidgetInfo;ILjava/lang/Boolean;)V
    .locals 3

    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p3

    if-nez p3, :cond_0

    const-string p3, "MaMlAuthManager"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "recheckAuthority fail : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p1, Lcom/miui/home/launcher/maml/MaMlWidgetInfo;->id:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p3, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p1, p2}, Lcom/miui/home/launcher/maml/auth/MaMlAuthManager;->onAuthCheckFail(Lcom/miui/home/launcher/maml/MaMlWidgetInfo;I)V

    goto :goto_0

    :cond_0
    const-string p2, "MaMlAuthManager"

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "recheckAuthority pass : "

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v0, p1, Lcom/miui/home/launcher/maml/MaMlWidgetInfo;->id:J

    invoke-virtual {p3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p2, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method

.method private onAuthCheckFail(Lcom/miui/home/launcher/maml/MaMlWidgetInfo;I)V
    .locals 1

    iget-object v0, p0, Lcom/miui/home/launcher/maml/auth/MaMlAuthManager;->mLauncherRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/home/launcher/Launcher;

    if-eqz v0, :cond_0

    invoke-direct {p0, p1, p2}, Lcom/miui/home/launcher/maml/auth/MaMlAuthManager;->getFailureHandler(Lcom/miui/home/launcher/maml/MaMlWidgetInfo;I)Lcom/miui/home/launcher/maml/auth/AuthFailureHandler;

    move-result-object p2

    invoke-interface {p2, v0, p1}, Lcom/miui/home/launcher/maml/auth/AuthFailureHandler;->onFail(Lcom/miui/home/launcher/Launcher;Lcom/miui/home/launcher/maml/MaMlWidgetInfo;)V

    goto :goto_0

    :cond_0
    const-string p1, "MaMlAuthManager"

    const-string p2, "onAuthCheckFail skip, Launcher is null"

    invoke-static {p1, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method

.method private queryCloudStrategy()I
    .locals 4

    iget-object v0, p0, Lcom/miui/home/launcher/maml/auth/MaMlAuthManager;->mClientRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/launcher/overlay/client/LauncherClient;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const-string v0, "MaMlAuthManager"

    const-string v2, "queryCloudStrategy skip, LauncherClient is null"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :cond_0
    const-string v2, "authority_fail_maml_strategy"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3, v3}, Lcom/miui/launcher/overlay/client/LauncherClient;->callOverlay(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_1

    goto :goto_0

    :cond_1
    const-string v2, "authority_fail_maml_action"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    :goto_0
    return v1
.end method

.method private recheckAuthority(Lcom/miui/home/launcher/maml/MaMlWidgetInfo;ILjava/lang/String;)V
    .locals 1

    new-instance v0, Lcom/miui/home/launcher/maml/auth/-$$Lambda$MaMlAuthManager$f3PgdEiwLEtzSAfjdfeszASjaZ0;

    invoke-direct {v0, p0, p1, p3}, Lcom/miui/home/launcher/maml/auth/-$$Lambda$MaMlAuthManager$f3PgdEiwLEtzSAfjdfeszASjaZ0;-><init>(Lcom/miui/home/launcher/maml/auth/MaMlAuthManager;Lcom/miui/home/launcher/maml/MaMlWidgetInfo;Ljava/lang/String;)V

    new-instance p3, Lcom/miui/home/launcher/maml/auth/-$$Lambda$MaMlAuthManager$Txhxzd21hG6H_WBbDBE_bwKJRnc;

    invoke-direct {p3, p0, p1, p2}, Lcom/miui/home/launcher/maml/auth/-$$Lambda$MaMlAuthManager$Txhxzd21hG6H_WBbDBE_bwKJRnc;-><init>(Lcom/miui/home/launcher/maml/auth/MaMlAuthManager;Lcom/miui/home/launcher/maml/MaMlWidgetInfo;I)V

    invoke-static {v0, p3}, Lcom/miui/home/library/utils/AsyncTaskExecutorHelper;->execParallel(Ljava/util/function/Supplier;Ljava/util/function/Consumer;)V

    return-void
.end method

.method private shouldCheck(Landroid/content/Context;)Z
    .locals 4

    iget-wide v0, p0, Lcom/miui/home/launcher/maml/auth/MaMlAuthManager;->mLastCheckTime:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const-string v0, "key_last_maml_auth_check_time"

    invoke-static {p1, v0, v2, v3}, Lcom/miui/home/launcher/common/PreferenceUtils;->getLong(Landroid/content/Context;Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/miui/home/launcher/maml/auth/MaMlAuthManager;->mLastCheckTime:J

    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/miui/home/launcher/maml/auth/MaMlAuthManager;->mLastCheckTime:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x1499700

    cmp-long p1, v0, v2

    if-ltz p1, :cond_1

    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    return p1
.end method


# virtual methods
.method public onStop()V
    .locals 2

    iget-object v0, p0, Lcom/miui/home/launcher/maml/auth/MaMlAuthManager;->mLauncherRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/home/launcher/Launcher;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {v0}, Lcom/miui/home/launcher/Launcher;->getMaMlItems()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Lcom/miui/launcher/utils/CollectionUtils;->isEmpty(Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_1

    return-void

    :cond_1
    invoke-direct {p0, v0}, Lcom/miui/home/launcher/maml/auth/MaMlAuthManager;->checkAuthority(Ljava/util/Set;)V

    return-void
.end method
