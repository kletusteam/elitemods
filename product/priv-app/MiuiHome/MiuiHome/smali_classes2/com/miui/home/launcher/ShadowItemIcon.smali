.class public Lcom/miui/home/launcher/ShadowItemIcon;
.super Lcom/miui/home/launcher/ItemIcon;


# instance fields
.field protected isBackAnimazing:Z

.field private mFirstDrawMark:Z

.field private mIconBitmap:Landroid/graphics/Bitmap;

.field private mIconDarkShadow:Landroid/graphics/Bitmap;

.field private mIconReflectionShadow:Landroid/graphics/Bitmap;

.field private mIsHideShadow:Z

.field private mTitleColor:I

.field private originalIcon:Landroid/graphics/drawable/Drawable;

.field private recycleCount:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2}, Lcom/miui/home/launcher/ItemIcon;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-boolean v0, p0, Lcom/miui/home/launcher/ShadowItemIcon;->mIsHideShadow:Z

    iput v0, p0, Lcom/miui/home/launcher/ShadowItemIcon;->recycleCount:I

    iput-boolean v0, p0, Lcom/miui/home/launcher/ShadowItemIcon;->isBackAnimazing:Z

    return-void
.end method

.method private checkNeedRecycle()V
    .locals 2

    invoke-static {}, Lcom/miui/home/launcher/util/LauncherIconHelper;->getInstance()Lcom/miui/home/launcher/util/LauncherIconHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/miui/home/launcher/util/LauncherIconHelper;->getRecycleCount()I

    move-result v0

    iget v1, p0, Lcom/miui/home/launcher/ShadowItemIcon;->recycleCount:I

    if-eq v0, v1, :cond_0

    iput v0, p0, Lcom/miui/home/launcher/ShadowItemIcon;->recycleCount:I

    invoke-direct {p0}, Lcom/miui/home/launcher/ShadowItemIcon;->recycleBitmaps()V

    :cond_0
    return-void
.end method

.method private drawDarkShadow(Landroid/graphics/Canvas;II)V
    .locals 11

    const/4 v7, 0x0

    const/4 v10, 0x0

    invoke-static {}, Lcom/miui/home/launcher/util/LauncherIconHelper;->getInstance()Lcom/miui/home/launcher/util/LauncherIconHelper;

    move-result-object v4

    invoke-virtual {p0}, Lcom/miui/home/launcher/ShadowItemIcon;->isDockViewMode()Z

    move-result v6

    invoke-virtual {v4, v6}, Lcom/miui/home/launcher/util/LauncherIconHelper;->isShadowState(Z)Z

    move-result v6

    if-eqz v6, :cond_1

    iget-boolean v6, p0, Lcom/miui/home/launcher/ShadowItemIcon;->isBackAnimazing:Z

    if-nez v6, :cond_1

    invoke-direct {p0}, Lcom/miui/home/launcher/ShadowItemIcon;->getIconBitmap()Landroid/graphics/Bitmap;

    move-result-object v6

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/miui/home/launcher/ShadowItemIcon;->mIconDarkShadow:Landroid/graphics/Bitmap;

    if-nez v6, :cond_0

    if-lez p2, :cond_0

    if-lez p3, :cond_0

    invoke-virtual {v4}, Lcom/miui/home/launcher/util/LauncherIconHelper;->getDarkShadowSize()F

    move-result v3

    invoke-virtual {p0}, Lcom/miui/home/launcher/ShadowItemIcon;->getWidth()I

    move-result v6

    invoke-virtual {p0}, Lcom/miui/home/launcher/ShadowItemIcon;->getHeight()I

    move-result v8

    sget-object v9, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v6, v8, v9}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v6

    iput-object v6, p0, Lcom/miui/home/launcher/ShadowItemIcon;->mIconDarkShadow:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/miui/home/launcher/ShadowItemIcon;->mIconDarkShadow:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    iget-object v6, p0, Lcom/miui/home/launcher/ShadowItemIcon;->mIconContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v6}, Landroid/widget/FrameLayout;->getLeft()I

    move-result v6

    iget-object v8, p0, Lcom/miui/home/launcher/ShadowItemIcon;->mIconImageView:Lcom/miui/home/launcher/LauncherIconImageView;

    invoke-virtual {v8}, Lcom/miui/home/launcher/LauncherIconImageView;->getLeft()I

    move-result v8

    add-int/2addr v6, v8

    iput v6, v5, Landroid/graphics/Rect;->left:I

    iget-object v6, p0, Lcom/miui/home/launcher/ShadowItemIcon;->mIconContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v6}, Landroid/widget/FrameLayout;->getTop()I

    move-result v6

    iget-object v8, p0, Lcom/miui/home/launcher/ShadowItemIcon;->mIconImageView:Lcom/miui/home/launcher/LauncherIconImageView;

    invoke-virtual {v8}, Lcom/miui/home/launcher/LauncherIconImageView;->getTop()I

    move-result v8

    add-int/2addr v6, v8

    iput v6, v5, Landroid/graphics/Rect;->top:I

    iget v6, v5, Landroid/graphics/Rect;->left:I

    iget-object v8, p0, Lcom/miui/home/launcher/ShadowItemIcon;->mIconImageView:Lcom/miui/home/launcher/LauncherIconImageView;

    invoke-virtual {v8}, Lcom/miui/home/launcher/LauncherIconImageView;->getWidth()I

    move-result v8

    add-int/2addr v6, v8

    iput v6, v5, Landroid/graphics/Rect;->right:I

    iget v6, v5, Landroid/graphics/Rect;->top:I

    iget-object v8, p0, Lcom/miui/home/launcher/ShadowItemIcon;->mIconImageView:Lcom/miui/home/launcher/LauncherIconImageView;

    invoke-virtual {v8}, Lcom/miui/home/launcher/LauncherIconImageView;->getHeight()I

    move-result v8

    add-int/2addr v6, v8

    iput v6, v5, Landroid/graphics/Rect;->bottom:I

    iget-object v8, p0, Lcom/miui/home/launcher/ShadowItemIcon;->mIconBitmap:Landroid/graphics/Bitmap;

    move-object v6, v7

    check-cast v6, Landroid/graphics/Rect;

    invoke-virtual {v4}, Lcom/miui/home/launcher/util/LauncherIconHelper;->getDarkShadowColor()I

    move-result v9

    invoke-static {v3, v9}, Lcom/miui/home/launcher/common/Utilities;->getIconDarkShadowPaint(FI)Landroid/graphics/Paint;

    move-result-object v9

    invoke-virtual {v2, v8, v6, v5, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :cond_0
    iget-object v1, p0, Lcom/miui/home/launcher/ShadowItemIcon;->mIconDarkShadow:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_1

    check-cast v7, Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v10, v10, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    :cond_1
    return-void
.end method

.method private drawReflectionShadow(Landroid/graphics/Canvas;)V
    .locals 13

    const/4 v12, 0x0

    const/high16 v5, 0x40000000    # 2.0f

    const/4 v1, 0x0

    invoke-static {}, Lcom/miui/home/launcher/util/LauncherIconHelper;->getInstance()Lcom/miui/home/launcher/util/LauncherIconHelper;

    move-result-object v9

    invoke-virtual {p0}, Lcom/miui/home/launcher/ShadowItemIcon;->isDockViewMode()Z

    move-result v0

    invoke-virtual {v9, v0}, Lcom/miui/home/launcher/util/LauncherIconHelper;->isReflectState(Z)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/miui/home/launcher/ShadowItemIcon;->mIconReflectionShadow:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/miui/home/launcher/ShadowItemIcon;->getIconBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/miui/home/launcher/ShadowItemIcon;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/miui/home/launcher/ShadowItemIcon;->getHeight()I

    move-result v2

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/home/launcher/ShadowItemIcon;->mIconReflectionShadow:Landroid/graphics/Bitmap;

    new-instance v8, Landroid/graphics/Canvas;

    iget-object v0, p0, Lcom/miui/home/launcher/ShadowItemIcon;->mIconReflectionShadow:Landroid/graphics/Bitmap;

    invoke-direct {v8, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    new-instance v10, Landroid/graphics/Matrix;

    invoke-direct {v10}, Landroid/graphics/Matrix;-><init>()V

    iget-object v0, p0, Lcom/miui/home/launcher/ShadowItemIcon;->mIconImageView:Lcom/miui/home/launcher/LauncherIconImageView;

    invoke-virtual {v0}, Lcom/miui/home/launcher/LauncherIconImageView;->getWidth()I

    move-result v0

    int-to-float v0, v0

    iget-object v2, p0, Lcom/miui/home/launcher/ShadowItemIcon;->mIconBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v0, v2

    iget-object v2, p0, Lcom/miui/home/launcher/ShadowItemIcon;->mIconImageView:Lcom/miui/home/launcher/LauncherIconImageView;

    invoke-virtual {v2}, Lcom/miui/home/launcher/LauncherIconImageView;->getHeight()I

    move-result v2

    neg-int v2, v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/miui/home/launcher/ShadowItemIcon;->mIconBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    iget-object v3, p0, Lcom/miui/home/launcher/ShadowItemIcon;->mIconImageView:Lcom/miui/home/launcher/LauncherIconImageView;

    invoke-virtual {v3}, Lcom/miui/home/launcher/LauncherIconImageView;->getWidth()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v5

    iget-object v4, p0, Lcom/miui/home/launcher/ShadowItemIcon;->mIconImageView:Lcom/miui/home/launcher/LauncherIconImageView;

    invoke-virtual {v4}, Lcom/miui/home/launcher/LauncherIconImageView;->getHeight()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v4, v5

    invoke-virtual {v10, v0, v2, v3, v4}, Landroid/graphics/Matrix;->setScale(FFFF)V

    invoke-virtual {p0}, Lcom/miui/home/launcher/ShadowItemIcon;->getWidth()I

    move-result v0

    iget-object v2, p0, Lcom/miui/home/launcher/ShadowItemIcon;->mIconBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    sub-int/2addr v0, v2

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    iget-object v2, p0, Lcom/miui/home/launcher/ShadowItemIcon;->mIconImageView:Lcom/miui/home/launcher/LauncherIconImageView;

    invoke-virtual {v2}, Lcom/miui/home/launcher/LauncherIconImageView;->getBottom()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v9}, Lcom/miui/home/launcher/util/LauncherIconHelper;->getIconReflectionGap()F

    move-result v3

    add-float/2addr v2, v3

    invoke-virtual {v10, v0, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    iget-object v0, p0, Lcom/miui/home/launcher/ShadowItemIcon;->mIconBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v8, v0, v10, v12}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    new-instance v11, Landroid/graphics/Paint;

    invoke-direct {v11}, Landroid/graphics/Paint;-><init>()V

    new-instance v0, Landroid/graphics/LinearGradient;

    iget-object v2, p0, Lcom/miui/home/launcher/ShadowItemIcon;->mIconImageView:Lcom/miui/home/launcher/LauncherIconImageView;

    invoke-virtual {v2}, Lcom/miui/home/launcher/LauncherIconImageView;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p0}, Lcom/miui/home/launcher/ShadowItemIcon;->getHeight()I

    move-result v3

    int-to-float v4, v3

    invoke-virtual {v9}, Lcom/miui/home/launcher/util/LauncherIconHelper;->getReflectionShadowColor()I

    move-result v5

    const v6, 0xffffff

    sget-object v7, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    move v3, v1

    invoke-direct/range {v0 .. v7}, Landroid/graphics/LinearGradient;-><init>(FFFFIILandroid/graphics/Shader$TileMode;)V

    invoke-virtual {v11, v0}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    new-instance v0, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->DST_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v0, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v11, v0}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    iget-object v0, p0, Lcom/miui/home/launcher/ShadowItemIcon;->mIconImageView:Lcom/miui/home/launcher/LauncherIconImageView;

    invoke-virtual {v0}, Lcom/miui/home/launcher/LauncherIconImageView;->getHeight()I

    move-result v0

    int-to-float v2, v0

    invoke-virtual {p0}, Lcom/miui/home/launcher/ShadowItemIcon;->getWidth()I

    move-result v0

    int-to-float v3, v0

    invoke-virtual {p0}, Lcom/miui/home/launcher/ShadowItemIcon;->getHeight()I

    move-result v0

    int-to-float v4, v0

    move-object v0, v8

    move-object v5, v11

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    :cond_0
    iget-object v0, p0, Lcom/miui/home/launcher/ShadowItemIcon;->mIconReflectionShadow:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/miui/home/launcher/ShadowItemIcon;->mIconReflectionShadow:Landroid/graphics/Bitmap;

    invoke-virtual {p1, v0, v1, v1, v12}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    :cond_1
    return-void
.end method

.method public static drawableToBitmap(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Bitmap;
    .locals 8

    const/4 v7, 0x0

    const/4 v5, 0x1

    const/4 v0, 0x0

    if-eqz p0, :cond_4

    instance-of v4, p0, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v4, :cond_1

    move-object v2, p0

    check-cast v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v4

    if-nez v4, :cond_0

    :goto_0
    return-object v1

    :cond_0
    sget-object v4, Landroid/graphics/Bitmap$Config;->ALPHA_8:Landroid/graphics/Bitmap$Config;

    invoke-static {v5, v5, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    if-lez v4, :cond_2

    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    if-gtz v4, :cond_3

    :cond_2
    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v5, v5, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    :goto_1
    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {v3}, Landroid/graphics/Canvas;->getWidth()I

    move-result v4

    invoke-virtual {v3}, Landroid/graphics/Canvas;->getHeight()I

    move-result v5

    invoke-virtual {p0, v7, v7, v4, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    invoke-virtual {p0, v3}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :goto_2
    move-object v1, v0

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v4, v5, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_1

    :cond_4
    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v5, v5, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0, v7, v7, v7}, Landroid/graphics/Bitmap;->setPixel(III)V

    goto :goto_2
.end method

.method private getIconBitmap()Landroid/graphics/Bitmap;
    .locals 6

    iget-object v4, p0, Lcom/miui/home/launcher/ShadowItemIcon;->mIconBitmap:Landroid/graphics/Bitmap;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/miui/home/launcher/ShadowItemIcon;->mIconBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    iget-object v2, p0, Lcom/miui/home/launcher/ShadowItemIcon;->mIconImageView:Lcom/miui/home/launcher/LauncherIconImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v2}, Landroid/widget/ImageView;->getWidth()I

    move-result v3

    invoke-virtual {v2}, Landroid/widget/ImageView;->getHeight()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_1

    if-lez v3, :cond_1

    if-lez v1, :cond_1

    invoke-static {v0}, Lcom/miui/home/launcher/ShadowItemIcon;->drawableToBitmap(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Bitmap;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v4, v3, v1, v5}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v4

    iput-object v4, p0, Lcom/miui/home/launcher/ShadowItemIcon;->mIconBitmap:Landroid/graphics/Bitmap;

    :cond_1
    iget-object v4, p0, Lcom/miui/home/launcher/ShadowItemIcon;->mIconBitmap:Landroid/graphics/Bitmap;

    return-object v4
.end method

.method private recycleBitmaps()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/miui/home/launcher/ShadowItemIcon;->mIconBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iput-object v1, p0, Lcom/miui/home/launcher/ShadowItemIcon;->mIconBitmap:Landroid/graphics/Bitmap;

    :cond_0
    iget-object v0, p0, Lcom/miui/home/launcher/ShadowItemIcon;->mIconDarkShadow:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    iput-object v1, p0, Lcom/miui/home/launcher/ShadowItemIcon;->mIconDarkShadow:Landroid/graphics/Bitmap;

    :cond_1
    iget-object v0, p0, Lcom/miui/home/launcher/ShadowItemIcon;->mIconReflectionShadow:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2

    iput-object v1, p0, Lcom/miui/home/launcher/ShadowItemIcon;->mIconReflectionShadow:Landroid/graphics/Bitmap;

    :cond_2
    invoke-virtual {p0}, Lcom/miui/home/launcher/ShadowItemIcon;->invalidate()V

    return-void
.end method

.method private updateTextColor()V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/miui/home/launcher/ShadowItemIcon;->isDockViewMode()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/miui/home/launcher/ShadowItemIcon;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "launcher_hotseats_title_color"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/miui/home/launcher/ShadowItemIcon;->mTitleColor:I

    :goto_0
    iget v0, p0, Lcom/miui/home/launcher/ShadowItemIcon;->mTitleColor:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/miui/home/launcher/ShadowItemIcon;->mTitleColor:I

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/ShadowItemIcon;->setTextColor(I)V

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/miui/home/launcher/ShadowItemIcon;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "launcher_shorcut_title_color"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/miui/home/launcher/ShadowItemIcon;->mTitleColor:I

    goto :goto_0
.end method

.method private updateTitle()V
    .locals 7

    const/4 v6, 0x0

    const/4 v5, 0x1

    iget-object v1, p0, Lcom/miui/home/launcher/ShadowItemIcon;->mTitle:Lcom/miui/home/launcher/TitleTextView;

    sget-object v2, Lcom/miui/home/launcher/ShadowItemIcon$1;->$SwitchMap$com$miui$home$launcher$util$LauncherIconHelper$IconLabelState:[I

    invoke-static {}, Lcom/miui/home/launcher/util/LauncherIconHelper;->getInstance()Lcom/miui/home/launcher/util/LauncherIconHelper;

    move-result-object v3

    invoke-virtual {p0}, Lcom/miui/home/launcher/ShadowItemIcon;->isDockViewMode()Z

    move-result v4

    invoke-virtual {v3, v4}, Lcom/miui/home/launcher/util/LauncherIconHelper;->getIconLabelState(Z)Lcom/miui/home/launcher/util/LauncherIconHelper$IconLabelState;

    move-result-object v3

    invoke-virtual {v3}, Lcom/miui/home/launcher/util/LauncherIconHelper$IconLabelState;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    :goto_0
    invoke-virtual {p0}, Lcom/miui/home/launcher/ShadowItemIcon;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "launch_title_color"

    invoke-static {v2, v3, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_0
    invoke-virtual {p0}, Lcom/miui/home/launcher/ShadowItemIcon;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "launch_title_size"

    const/16 v4, 0xc

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextSize(F)V

    return-void

    :pswitch_0
    invoke-virtual {p0, v5}, Lcom/miui/home/launcher/ShadowItemIcon;->setIsHideTitle(Z)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {v1}, Landroid/widget/TextView;->setSingleLine()V

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setHorizontalFadingEdgeEnabled(Z)V

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setMarqueeRepeatLimit(I)V

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setSelected(Z)V

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setHorizontallyScrolling(Z)V

    goto :goto_0

    :pswitch_2
    invoke-virtual {v1}, Landroid/widget/TextView;->setSingleLine()V

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setMarqueeRepeatLimit(I)V

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setSelected(Z)V

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setHorizontallyScrolling(Z)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/home/launcher/ShadowItemIcon;->mFirstDrawMark:Z

    invoke-super {p0, p1}, Lcom/miui/home/launcher/ItemIcon;->dispatchDraw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method protected drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
    .locals 5

    const/4 v3, 0x0

    iget-boolean v0, p0, Lcom/miui/home/launcher/ShadowItemIcon;->mFirstDrawMark:Z

    if-eqz v0, :cond_1

    const-string v0, "mFirstDrawMark"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {p0, v0, v1, v2}, Landroid/Utils/ReflectionUtil;->setFieldForSuperClass(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;I)Z

    invoke-static {}, Lcom/miui/home/launcher/util/LauncherIconHelper;->getInstance()Lcom/miui/home/launcher/util/LauncherIconHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/home/launcher/util/LauncherIconHelper;->isShadowEnable()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/miui/home/launcher/ShadowItemIcon;->mIsHideShadow:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/miui/home/launcher/ShadowItemIcon;->checkNeedRecycle()V

    invoke-direct {p0, p1}, Lcom/miui/home/launcher/ShadowItemIcon;->drawReflectionShadow(Landroid/graphics/Canvas;)V

    invoke-virtual {p0}, Lcom/miui/home/launcher/ShadowItemIcon;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/miui/home/launcher/ShadowItemIcon;->getMeasuredHeight()I

    move-result v1

    invoke-direct {p0, p1, v0, v1}, Lcom/miui/home/launcher/ShadowItemIcon;->drawDarkShadow(Landroid/graphics/Canvas;II)V

    :cond_0
    :goto_0
    iput-boolean v3, p0, Lcom/miui/home/launcher/ShadowItemIcon;->mFirstDrawMark:Z

    :cond_1
    invoke-super {p0, p1, p2, p3, p4}, Lcom/miui/home/launcher/ItemIcon;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v0

    return v0

    :cond_2
    invoke-direct {p0}, Lcom/miui/home/launcher/ShadowItemIcon;->recycleBitmaps()V

    goto :goto_0
.end method

.method public getOriginalIcon()Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Lcom/miui/home/launcher/ShadowItemIcon;->originalIcon:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public setDockViewMode(Z)V
    .locals 0

    invoke-super {p0, p1}, Lcom/miui/home/launcher/ItemIcon;->setDockViewMode(Z)V

    invoke-direct {p0}, Lcom/miui/home/launcher/ShadowItemIcon;->updateTitle()V

    return-void
.end method

.method public setIconImageView(Landroid/graphics/drawable/Drawable;Landroid/graphics/Bitmap;)V
    .locals 0

    invoke-super {p0, p1, p2}, Lcom/miui/home/launcher/ItemIcon;->setIconImageView(Landroid/graphics/drawable/Drawable;Landroid/graphics/Bitmap;)V

    invoke-direct {p0}, Lcom/miui/home/launcher/ShadowItemIcon;->recycleBitmaps()V

    return-void
.end method

.method public setIsHideShadow(Z)V
    .locals 0

    invoke-super {p0, p1}, Lcom/miui/home/launcher/ItemIcon;->setIsHideShadow(Z)V

    iput-boolean p1, p0, Lcom/miui/home/launcher/ShadowItemIcon;->mIsHideShadow:Z

    return-void
.end method

.method public setIsHideTitle(Z)V
    .locals 2

    invoke-static {}, Lcom/miui/home/launcher/util/LauncherIconHelper;->getInstance()Lcom/miui/home/launcher/util/LauncherIconHelper;

    move-result-object v0

    invoke-virtual {p0}, Lcom/miui/home/launcher/ShadowItemIcon;->isDockViewMode()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/miui/home/launcher/util/LauncherIconHelper;->getIconLabelState(Z)Lcom/miui/home/launcher/util/LauncherIconHelper$IconLabelState;

    move-result-object v0

    sget-object v1, Lcom/miui/home/launcher/util/LauncherIconHelper$IconLabelState;->NOT_LABEL:Lcom/miui/home/launcher/util/LauncherIconHelper$IconLabelState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-super {p0, v0}, Lcom/miui/home/launcher/ItemIcon;->setIsHideTitle(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/miui/home/launcher/ItemIcon;->setTitle(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/miui/home/launcher/ShadowItemIcon;->updateTitle()V

    return-void
.end method
