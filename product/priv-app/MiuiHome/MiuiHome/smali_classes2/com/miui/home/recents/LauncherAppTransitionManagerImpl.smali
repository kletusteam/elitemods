.class public final Lcom/miui/home/recents/LauncherAppTransitionManagerImpl;
.super Lcom/miui/home/recents/QuickstepAppTransitionManagerImpl;


# instance fields
.field private isNeedResetLauncherState:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/home/recents/QuickstepAppTransitionManagerImpl;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$000(Lcom/miui/home/recents/LauncherAppTransitionManagerImpl;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/miui/home/recents/LauncherAppTransitionManagerImpl;->isNeedResetLauncherState:Z

    return p0
.end method

.method static synthetic access$002(Lcom/miui/home/recents/LauncherAppTransitionManagerImpl;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/miui/home/recents/LauncherAppTransitionManagerImpl;->isNeedResetLauncherState:Z

    return p1
.end method

.method private onAnimationListener(Lcom/miui/home/recents/util/RectFSpringAnim;Lcom/miui/home/launcher/Launcher;Lcom/miui/home/recents/views/RecentsView;[Lcom/android/systemui/shared/recents/system/RemoteAnimationTargetCompat;I)V
    .locals 7

    new-instance v6, Lcom/miui/home/recents/LauncherAppTransitionManagerImpl$1;

    move-object v0, v6

    move-object v1, p0

    move v2, p5

    move-object v3, p3

    move-object v4, p2

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/miui/home/recents/LauncherAppTransitionManagerImpl$1;-><init>(Lcom/miui/home/recents/LauncherAppTransitionManagerImpl;ILcom/miui/home/recents/views/RecentsView;Lcom/miui/home/launcher/Launcher;[Lcom/android/systemui/shared/recents/system/RemoteAnimationTargetCompat;)V

    invoke-virtual {p1, v6}, Lcom/miui/home/recents/util/RectFSpringAnim;->addAnimatorListener(Landroid/animation/Animator$AnimatorListener;)V

    return-void
.end method

.method private startAnim(Ljava/util/ArrayList;Lcom/miui/home/recents/LauncherAnimationRunner$AnimationResult;Lcom/miui/home/launcher/Launcher;Lcom/miui/home/recents/views/RecentsView;Lcom/miui/home/recents/views/TaskView;ZI)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/miui/home/recents/util/RectFSpringAnim;",
            ">;",
            "Lcom/miui/home/recents/LauncherAnimationRunner$AnimationResult;",
            "Lcom/miui/home/launcher/Launcher;",
            "Lcom/miui/home/recents/views/RecentsView;",
            "Lcom/miui/home/recents/views/TaskView;",
            "ZI)V"
        }
    .end annotation

    new-instance v0, Lcom/miui/home/recents/util/MultiAnimationEndDetector;

    invoke-direct {v0}, Lcom/miui/home/recents/util/MultiAnimationEndDetector;-><init>()V

    iput-object v0, p0, Lcom/miui/home/recents/LauncherAppTransitionManagerImpl;->mTaskLaunchEndDetector:Lcom/miui/home/recents/util/MultiAnimationEndDetector;

    invoke-virtual {p3}, Lcom/miui/home/launcher/Launcher;->getStateManager()Lcom/miui/home/launcher/LauncherStateManager;

    move-result-object p3

    iget-object v0, p0, Lcom/miui/home/recents/LauncherAppTransitionManagerImpl;->mTaskLaunchEndDetector:Lcom/miui/home/recents/util/MultiAnimationEndDetector;

    invoke-virtual {p3, v0}, Lcom/miui/home/launcher/LauncherStateManager;->setCurrentAnimation(Lcom/miui/home/recents/util/MultiAnimationEndDetector;)V

    if-nez p7, :cond_1

    const/4 p3, 0x1

    if-eqz p6, :cond_0

    const/4 p5, 0x0

    :cond_0
    iget-object p6, p0, Lcom/miui/home/recents/LauncherAppTransitionManagerImpl;->mTaskLaunchEndDetector:Lcom/miui/home/recents/util/MultiAnimationEndDetector;

    invoke-virtual {p0, p3, p4, p5, p6}, Lcom/miui/home/recents/LauncherAppTransitionManagerImpl;->startRecentsContentAnimator(ZLcom/miui/home/recents/views/RecentsView;Lcom/miui/home/recents/views/TaskView;Lcom/miui/home/recents/util/MultiAnimationEndDetector;)V

    :cond_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :goto_0
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result p4

    if-eqz p4, :cond_2

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Lcom/miui/home/recents/util/RectFSpringAnim;

    iget-object p5, p0, Lcom/miui/home/recents/LauncherAppTransitionManagerImpl;->mTaskLaunchEndDetector:Lcom/miui/home/recents/util/MultiAnimationEndDetector;

    invoke-virtual {p5, p4}, Lcom/miui/home/recents/util/MultiAnimationEndDetector;->addAnimation(Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    invoke-static {p2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance p3, Lcom/miui/home/recents/-$$Lambda$AlEeBxjHFsy1SRWRXeztzuuTMeo;

    invoke-direct {p3, p2}, Lcom/miui/home/recents/-$$Lambda$AlEeBxjHFsy1SRWRXeztzuuTMeo;-><init>(Lcom/miui/home/recents/LauncherAnimationRunner$AnimationResult;)V

    iput-object p3, p0, Lcom/miui/home/recents/LauncherAppTransitionManagerImpl;->mTaskLaunchFinishRunnable:Ljava/lang/Runnable;

    iget-object p2, p0, Lcom/miui/home/recents/LauncherAppTransitionManagerImpl;->mTaskLaunchEndDetector:Lcom/miui/home/recents/util/MultiAnimationEndDetector;

    iget-object p3, p0, Lcom/miui/home/recents/LauncherAppTransitionManagerImpl;->mTaskLaunchFinishRunnable:Ljava/lang/Runnable;

    invoke-virtual {p2, p3}, Lcom/miui/home/recents/util/MultiAnimationEndDetector;->addEndRunnable(Ljava/lang/Runnable;)V

    iget-object p2, p0, Lcom/miui/home/recents/LauncherAppTransitionManagerImpl;->mTaskLaunchEndDetector:Lcom/miui/home/recents/util/MultiAnimationEndDetector;

    invoke-virtual {p2}, Lcom/miui/home/recents/util/MultiAnimationEndDetector;->startDetect()V

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/miui/home/recents/util/RectFSpringAnim;

    invoke-virtual {p2}, Lcom/miui/home/recents/util/RectFSpringAnim;->startInGestureThread()V

    goto :goto_1

    :cond_3
    return-void
.end method


# virtual methods
.method public composeRecentsLaunchAnimator(Landroid/animation/AnimatorSet;Landroid/view/View;I[Lcom/android/systemui/shared/recents/system/RemoteAnimationTargetCompat;[Lcom/android/systemui/shared/recents/system/RemoteAnimationTargetCompat;[Lcom/android/systemui/shared/recents/system/RemoteAnimationTargetCompat;ZLcom/miui/home/recents/LauncherAnimationRunner$AnimationResult;I)V
    .locals 19

    invoke-virtual/range {p0 .. p0}, Lcom/miui/home/recents/LauncherAppTransitionManagerImpl;->getLauncher()Lcom/miui/home/launcher/Launcher;

    move-result-object v6

    if-nez v6, :cond_1

    if-eqz p8, :cond_0

    invoke-virtual/range {p8 .. p8}, Lcom/miui/home/recents/LauncherAnimationRunner$AnimationResult;->finish()V

    :cond_0
    return-void

    :cond_1
    invoke-virtual {v6}, Lcom/miui/home/launcher/Launcher;->getOverviewPanel()Landroid/view/View;

    move-result-object v0

    move-object/from16 v16, v0

    check-cast v16, Lcom/miui/home/recents/views/RecentsView;

    const/4 v0, 0x1

    xor-int/lit8 v1, p7, 0x1

    move-object/from16 v2, p2

    move-object/from16 v4, p4

    invoke-static {v6, v2, v4}, Lcom/miui/home/recents/TaskViewUtils;->findTaskViewToLaunch(Lcom/miui/home/launcher/Launcher;Landroid/view/View;[Lcom/android/systemui/shared/recents/system/RemoteAnimationTargetCompat;)Lcom/miui/home/recents/views/TaskView;

    move-result-object v17

    if-nez v1, :cond_3

    invoke-static {}, Lcom/miui/home/launcher/DeviceConfig;->isInMultiWindowMode()Z

    move-result v1

    if-nez v1, :cond_3

    move/from16 v5, p9

    if-ne v5, v0, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    move/from16 v5, p9

    :goto_0
    move/from16 v18, v0

    invoke-static {}, Lcom/miui/home/launcher/DeviceConfig;->isInSplitSelectState()Z

    move-result v0

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    if-eqz v0, :cond_7

    const/4 v15, 0x2

    move-object/from16 v7, v17

    move/from16 v8, v18

    move/from16 v9, p3

    move-object/from16 v10, p4

    move-object/from16 v11, p5

    move-object/from16 v12, p6

    move-object/from16 v13, v16

    move/from16 v14, p9

    invoke-static/range {v7 .. v15}, Lcom/miui/home/recents/TaskViewUtils;->getRecentsWindowAnimatorNew(Lcom/miui/home/recents/views/TaskView;ZI[Lcom/android/systemui/shared/recents/system/RemoteAnimationTargetCompat;[Lcom/android/systemui/shared/recents/system/RemoteAnimationTargetCompat;[Lcom/android/systemui/shared/recents/system/RemoteAnimationTargetCompat;Lcom/miui/home/recents/views/RecentsView;II)Lcom/miui/home/recents/util/RectFSpringAnim;

    move-result-object v2

    const/4 v15, 0x3

    invoke-static/range {v7 .. v15}, Lcom/miui/home/recents/TaskViewUtils;->getRecentsWindowAnimatorNew(Lcom/miui/home/recents/views/TaskView;ZI[Lcom/android/systemui/shared/recents/system/RemoteAnimationTargetCompat;[Lcom/android/systemui/shared/recents/system/RemoteAnimationTargetCompat;[Lcom/android/systemui/shared/recents/system/RemoteAnimationTargetCompat;Lcom/miui/home/recents/views/RecentsView;II)Lcom/miui/home/recents/util/RectFSpringAnim;

    move-result-object v7

    if-eqz v2, :cond_5

    if-nez v7, :cond_4

    goto :goto_1

    :cond_4
    move-object/from16 v0, p0

    move-object v1, v7

    move-object v8, v2

    move-object v2, v6

    move-object v15, v3

    move-object/from16 v3, v16

    move-object/from16 v4, p4

    move/from16 v5, p9

    invoke-direct/range {v0 .. v5}, Lcom/miui/home/recents/LauncherAppTransitionManagerImpl;->onAnimationListener(Lcom/miui/home/recents/util/RectFSpringAnim;Lcom/miui/home/launcher/Launcher;Lcom/miui/home/recents/views/RecentsView;[Lcom/android/systemui/shared/recents/system/RemoteAnimationTargetCompat;I)V

    invoke-virtual {v15, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v15, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v8, v15

    goto :goto_2

    :cond_5
    :goto_1
    if-eqz p8, :cond_6

    invoke-virtual/range {p8 .. p8}, Lcom/miui/home/recents/LauncherAnimationRunner$AnimationResult;->finish()V

    :cond_6
    return-void

    :cond_7
    move-object v15, v3

    const/4 v0, 0x1

    move-object/from16 v7, v17

    move/from16 v8, v18

    move/from16 v9, p3

    move-object/from16 v10, p4

    move-object/from16 v11, p5

    move-object/from16 v12, p6

    move-object/from16 v13, v16

    move/from16 v14, p9

    move-object v5, v15

    move v15, v0

    invoke-static/range {v7 .. v15}, Lcom/miui/home/recents/TaskViewUtils;->getRecentsWindowAnimatorNew(Lcom/miui/home/recents/views/TaskView;ZI[Lcom/android/systemui/shared/recents/system/RemoteAnimationTargetCompat;[Lcom/android/systemui/shared/recents/system/RemoteAnimationTargetCompat;[Lcom/android/systemui/shared/recents/system/RemoteAnimationTargetCompat;Lcom/miui/home/recents/views/RecentsView;II)Lcom/miui/home/recents/util/RectFSpringAnim;

    move-result-object v7

    if-nez v7, :cond_9

    if-eqz p8, :cond_8

    invoke-virtual/range {p8 .. p8}, Lcom/miui/home/recents/LauncherAnimationRunner$AnimationResult;->finish()V

    :cond_8
    return-void

    :cond_9
    move-object/from16 v0, p0

    move-object v1, v7

    move-object v2, v6

    move-object/from16 v3, v16

    move-object/from16 v4, p4

    move-object v8, v5

    move/from16 v5, p9

    invoke-direct/range {v0 .. v5}, Lcom/miui/home/recents/LauncherAppTransitionManagerImpl;->onAnimationListener(Lcom/miui/home/recents/util/RectFSpringAnim;Lcom/miui/home/launcher/Launcher;Lcom/miui/home/recents/views/RecentsView;[Lcom/android/systemui/shared/recents/system/RemoteAnimationTargetCompat;I)V

    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_2
    move-object/from16 v0, p0

    move-object v1, v8

    move-object/from16 v2, p8

    move-object v3, v6

    move-object/from16 v4, v16

    move-object/from16 v5, v17

    move/from16 v6, v18

    move/from16 v7, p9

    invoke-direct/range {v0 .. v7}, Lcom/miui/home/recents/LauncherAppTransitionManagerImpl;->startAnim(Ljava/util/ArrayList;Lcom/miui/home/recents/LauncherAnimationRunner$AnimationResult;Lcom/miui/home/launcher/Launcher;Lcom/miui/home/recents/views/RecentsView;Lcom/miui/home/recents/views/TaskView;ZI)V

    return-void
.end method

.method protected isLaunchingFromRecents(Landroid/view/View;[Lcom/android/systemui/shared/recents/system/RemoteAnimationTargetCompat;)Z
    .locals 3

    invoke-virtual {p0}, Lcom/miui/home/recents/LauncherAppTransitionManagerImpl;->getLauncher()Lcom/miui/home/launcher/Launcher;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/miui/home/launcher/Launcher;->getStateManager()Lcom/miui/home/launcher/LauncherStateManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/miui/home/launcher/LauncherStateManager;->getState()Lcom/miui/home/launcher/LauncherState;

    move-result-object v1

    sget-object v2, Lcom/miui/home/launcher/LauncherState;->OVERVIEW:Lcom/miui/home/recents/OverviewState;

    if-ne v1, v2, :cond_0

    invoke-static {v0, p1, p2}, Lcom/miui/home/recents/TaskViewUtils;->findTaskViewToLaunch(Lcom/miui/home/launcher/Launcher;Landroid/view/View;[Lcom/android/systemui/shared/recents/system/RemoteAnimationTargetCompat;)Lcom/miui/home/recents/views/TaskView;

    move-result-object p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public onEnterRecentsFromApp()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/miui/home/recents/LauncherAppTransitionManagerImpl;->isNeedResetLauncherState:Z

    return-void
.end method
