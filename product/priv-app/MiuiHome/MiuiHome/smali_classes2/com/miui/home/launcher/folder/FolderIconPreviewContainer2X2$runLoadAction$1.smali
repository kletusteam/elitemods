.class public final Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$runLoadAction$1;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/function/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->runLoadAction(Lcom/miui/home/launcher/FolderInfo;Lcom/miui/home/launcher/IconCache;ZLcom/miui/home/library/utils/AsyncTaskExecutorHelper$SerialExecutor;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/function/Function<",
        "Lcom/miui/home/launcher/ShortcutInfo;",
        "Landroid/graphics/drawable/Drawable;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nFolderIconPreviewContainer2X2.kt\nKotlin\n*S Kotlin\n*F\n+ 1 FolderIconPreviewContainer2X2.kt\ncom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$runLoadAction$1\n*L\n1#1,907:1\n*E\n"
.end annotation


# instance fields
.field final synthetic $currentIndex:I

.field final synthetic $iconCache:Lcom/miui/home/launcher/IconCache;

.field final synthetic $isToggle:Z

.field final synthetic $si:Lcom/miui/home/launcher/ShortcutInfo;

.field final synthetic this$0:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;


# direct methods
.method constructor <init>(Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;ILcom/miui/home/launcher/ShortcutInfo;Lcom/miui/home/launcher/IconCache;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/miui/home/launcher/ShortcutInfo;",
            "Lcom/miui/home/launcher/IconCache;",
            "Z)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$runLoadAction$1;->this$0:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    iput p2, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$runLoadAction$1;->$currentIndex:I

    iput-object p3, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$runLoadAction$1;->$si:Lcom/miui/home/launcher/ShortcutInfo;

    iput-object p4, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$runLoadAction$1;->$iconCache:Lcom/miui/home/launcher/IconCache;

    iput-boolean p5, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$runLoadAction$1;->$isToggle:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/miui/home/launcher/ShortcutInfo;)Landroid/graphics/drawable/Drawable;
    .locals 5

    iget-object p1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$runLoadAction$1;->this$0:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    invoke-virtual {p1}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->getMPvChildList()Ljava/util/List;

    move-result-object p1

    monitor-enter p1

    :try_start_0
    iget v0, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$runLoadAction$1;->$currentIndex:I

    iget-object v1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$runLoadAction$1;->this$0:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    invoke-virtual {v1}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->getMRealPvChildCount()I

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v2, 0x0

    if-lt v0, v1, :cond_0

    monitor-exit p1

    return-object v2

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$runLoadAction$1;->this$0:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    invoke-virtual {v0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->getMPvChildList()Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    iget v1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$runLoadAction$1;->$currentIndex:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p1

    iget-object p1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$runLoadAction$1;->$si:Lcom/miui/home/launcher/ShortcutInfo;

    iget-object v1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$runLoadAction$1;->this$0:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    invoke-virtual {v1}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v3, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$runLoadAction$1;->$iconCache:Lcom/miui/home/launcher/IconCache;

    iget-boolean v4, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$runLoadAction$1;->$isToggle:Z

    if-eqz v4, :cond_2

    move-object v0, v2

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_0
    invoke-virtual {p1, v1, v3, v0}, Lcom/miui/home/launcher/ShortcutInfo;->getIconDrawable(Landroid/content/Context;Lcom/miui/home/launcher/IconCache;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    if-eqz p1, :cond_3

    return-object p1

    :cond_3
    return-object v2

    :catchall_0
    move-exception v0

    monitor-exit p1

    throw v0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/miui/home/launcher/ShortcutInfo;

    invoke-virtual {p0, p1}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$runLoadAction$1;->apply(Lcom/miui/home/launcher/ShortcutInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    return-object p1
.end method
