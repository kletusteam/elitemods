.class public final synthetic Lcom/miui/home/recents/-$$Lambda$TaskViewUtils$dxH5vDCokQcmCCmoFRcEIc4D_pM;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/miui/home/recents/util/RectFSpringAnim$OnUpdateListener;


# instance fields
.field private final synthetic f$0:Lcom/miui/home/recents/util/ClipAnimationHelper$TransformParams;

.field private final synthetic f$1:Z

.field private final synthetic f$10:Landroid/graphics/RectF;

.field private final synthetic f$2:Z

.field private final synthetic f$3:Lcom/miui/home/recents/util/ClipAnimationHelper;

.field private final synthetic f$4:Lcom/miui/home/recents/util/RemoteAnimationTargetSet;

.field private final synthetic f$5:I

.field private final synthetic f$6:Z

.field private final synthetic f$7:Lcom/miui/home/recents/views/TaskView;

.field private final synthetic f$8:Z

.field private final synthetic f$9:Lcom/miui/home/recents/views/RecentsView;


# direct methods
.method public synthetic constructor <init>(Lcom/miui/home/recents/util/ClipAnimationHelper$TransformParams;ZZLcom/miui/home/recents/util/ClipAnimationHelper;Lcom/miui/home/recents/util/RemoteAnimationTargetSet;IZLcom/miui/home/recents/views/TaskView;ZLcom/miui/home/recents/views/RecentsView;Landroid/graphics/RectF;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/miui/home/recents/-$$Lambda$TaskViewUtils$dxH5vDCokQcmCCmoFRcEIc4D_pM;->f$0:Lcom/miui/home/recents/util/ClipAnimationHelper$TransformParams;

    iput-boolean p2, p0, Lcom/miui/home/recents/-$$Lambda$TaskViewUtils$dxH5vDCokQcmCCmoFRcEIc4D_pM;->f$1:Z

    iput-boolean p3, p0, Lcom/miui/home/recents/-$$Lambda$TaskViewUtils$dxH5vDCokQcmCCmoFRcEIc4D_pM;->f$2:Z

    iput-object p4, p0, Lcom/miui/home/recents/-$$Lambda$TaskViewUtils$dxH5vDCokQcmCCmoFRcEIc4D_pM;->f$3:Lcom/miui/home/recents/util/ClipAnimationHelper;

    iput-object p5, p0, Lcom/miui/home/recents/-$$Lambda$TaskViewUtils$dxH5vDCokQcmCCmoFRcEIc4D_pM;->f$4:Lcom/miui/home/recents/util/RemoteAnimationTargetSet;

    iput p6, p0, Lcom/miui/home/recents/-$$Lambda$TaskViewUtils$dxH5vDCokQcmCCmoFRcEIc4D_pM;->f$5:I

    iput-boolean p7, p0, Lcom/miui/home/recents/-$$Lambda$TaskViewUtils$dxH5vDCokQcmCCmoFRcEIc4D_pM;->f$6:Z

    iput-object p8, p0, Lcom/miui/home/recents/-$$Lambda$TaskViewUtils$dxH5vDCokQcmCCmoFRcEIc4D_pM;->f$7:Lcom/miui/home/recents/views/TaskView;

    iput-boolean p9, p0, Lcom/miui/home/recents/-$$Lambda$TaskViewUtils$dxH5vDCokQcmCCmoFRcEIc4D_pM;->f$8:Z

    iput-object p10, p0, Lcom/miui/home/recents/-$$Lambda$TaskViewUtils$dxH5vDCokQcmCCmoFRcEIc4D_pM;->f$9:Lcom/miui/home/recents/views/RecentsView;

    iput-object p11, p0, Lcom/miui/home/recents/-$$Lambda$TaskViewUtils$dxH5vDCokQcmCCmoFRcEIc4D_pM;->f$10:Landroid/graphics/RectF;

    return-void
.end method


# virtual methods
.method public final onUpdate(Landroid/graphics/RectF;FFF)V
    .locals 16

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/miui/home/recents/-$$Lambda$TaskViewUtils$dxH5vDCokQcmCCmoFRcEIc4D_pM;->f$0:Lcom/miui/home/recents/util/ClipAnimationHelper$TransformParams;

    iget-boolean v2, v0, Lcom/miui/home/recents/-$$Lambda$TaskViewUtils$dxH5vDCokQcmCCmoFRcEIc4D_pM;->f$1:Z

    iget-boolean v3, v0, Lcom/miui/home/recents/-$$Lambda$TaskViewUtils$dxH5vDCokQcmCCmoFRcEIc4D_pM;->f$2:Z

    iget-object v4, v0, Lcom/miui/home/recents/-$$Lambda$TaskViewUtils$dxH5vDCokQcmCCmoFRcEIc4D_pM;->f$3:Lcom/miui/home/recents/util/ClipAnimationHelper;

    iget-object v5, v0, Lcom/miui/home/recents/-$$Lambda$TaskViewUtils$dxH5vDCokQcmCCmoFRcEIc4D_pM;->f$4:Lcom/miui/home/recents/util/RemoteAnimationTargetSet;

    iget v6, v0, Lcom/miui/home/recents/-$$Lambda$TaskViewUtils$dxH5vDCokQcmCCmoFRcEIc4D_pM;->f$5:I

    iget-boolean v7, v0, Lcom/miui/home/recents/-$$Lambda$TaskViewUtils$dxH5vDCokQcmCCmoFRcEIc4D_pM;->f$6:Z

    iget-object v8, v0, Lcom/miui/home/recents/-$$Lambda$TaskViewUtils$dxH5vDCokQcmCCmoFRcEIc4D_pM;->f$7:Lcom/miui/home/recents/views/TaskView;

    iget-boolean v9, v0, Lcom/miui/home/recents/-$$Lambda$TaskViewUtils$dxH5vDCokQcmCCmoFRcEIc4D_pM;->f$8:Z

    iget-object v10, v0, Lcom/miui/home/recents/-$$Lambda$TaskViewUtils$dxH5vDCokQcmCCmoFRcEIc4D_pM;->f$9:Lcom/miui/home/recents/views/RecentsView;

    iget-object v11, v0, Lcom/miui/home/recents/-$$Lambda$TaskViewUtils$dxH5vDCokQcmCCmoFRcEIc4D_pM;->f$10:Landroid/graphics/RectF;

    move-object/from16 v12, p1

    move/from16 v13, p2

    move/from16 v14, p3

    move/from16 v15, p4

    invoke-static/range {v1 .. v15}, Lcom/miui/home/recents/TaskViewUtils;->lambda$createSpringAnim$1(Lcom/miui/home/recents/util/ClipAnimationHelper$TransformParams;ZZLcom/miui/home/recents/util/ClipAnimationHelper;Lcom/miui/home/recents/util/RemoteAnimationTargetSet;IZLcom/miui/home/recents/views/TaskView;ZLcom/miui/home/recents/views/RecentsView;Landroid/graphics/RectF;Landroid/graphics/RectF;FFF)V

    return-void
.end method
