.class Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$StaticGeneralLayer;
.super Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$StaticLayer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "StaticGeneralLayer"
.end annotation


# instance fields
.field private final mCanvas:Landroid/graphics/Canvas;

.field private mLayerBitmap:Landroid/graphics/Bitmap;

.field private mLayerShader:Landroid/graphics/BitmapShader;


# direct methods
.method private constructor <init>(Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable;Landroid/graphics/drawable/Drawable;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$StaticLayer;-><init>(Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable;Landroid/graphics/drawable/Drawable;ILcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$1;)V

    new-instance p1, Landroid/graphics/Canvas;

    invoke-direct {p1}, Landroid/graphics/Canvas;-><init>()V

    iput-object p1, p0, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$StaticGeneralLayer;->mCanvas:Landroid/graphics/Canvas;

    return-void
.end method

.method synthetic constructor <init>(Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable;Landroid/graphics/drawable/Drawable;ILcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$1;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$StaticGeneralLayer;-><init>(Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable;Landroid/graphics/drawable/Drawable;I)V

    return-void
.end method


# virtual methods
.method getShader()Landroid/graphics/Shader;
    .locals 4

    goto/32 :goto_5

    nop

    :goto_0
    return-object v0

    :goto_1
    iget-object v1, p0, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$StaticGeneralLayer;->mCanvas:Landroid/graphics/Canvas;

    goto/32 :goto_10

    nop

    :goto_2
    goto :goto_8

    :goto_3
    goto/32 :goto_14

    nop

    :goto_4
    if-eqz v0, :cond_0

    goto/32 :goto_1c

    :cond_0
    goto/32 :goto_11

    nop

    :goto_5
    iget-object v0, p0, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$StaticGeneralLayer;->mLayerBitmap:Landroid/graphics/Bitmap;

    goto/32 :goto_19

    nop

    :goto_6
    iget-object v1, p0, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$StaticGeneralLayer;->mLayerBitmap:Landroid/graphics/Bitmap;

    goto/32 :goto_b

    nop

    :goto_7
    return-object v0

    :goto_8
    goto/32 :goto_13

    nop

    :goto_9
    new-instance v0, Landroid/graphics/BitmapShader;

    goto/32 :goto_12

    nop

    :goto_a
    sget-object v3, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    goto/32 :goto_c

    nop

    :goto_b
    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    goto/32 :goto_18

    nop

    :goto_c
    invoke-direct {v0, v1, v2, v3}, Landroid/graphics/BitmapShader;-><init>(Landroid/graphics/Bitmap;Landroid/graphics/Shader$TileMode;Landroid/graphics/Shader$TileMode;)V

    goto/32 :goto_1b

    nop

    :goto_d
    invoke-virtual {p0}, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$StaticGeneralLayer;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto/32 :goto_f

    nop

    :goto_e
    sget-object v2, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    goto/32 :goto_a

    nop

    :goto_f
    if-eqz v0, :cond_1

    goto/32 :goto_3

    :cond_1
    goto/32 :goto_2

    nop

    :goto_10
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto/32 :goto_9

    nop

    :goto_11
    iget-object v0, p0, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$StaticGeneralLayer;->mCanvas:Landroid/graphics/Canvas;

    goto/32 :goto_1a

    nop

    :goto_12
    iget-object v1, p0, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$StaticGeneralLayer;->mLayerBitmap:Landroid/graphics/Bitmap;

    goto/32 :goto_e

    nop

    :goto_13
    const/4 v0, 0x0

    goto/32 :goto_0

    nop

    :goto_14
    iget-object v0, p0, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$StaticGeneralLayer;->mLayerShader:Landroid/graphics/BitmapShader;

    goto/32 :goto_4

    nop

    :goto_15
    iget-object v0, p0, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$StaticGeneralLayer;->mCanvas:Landroid/graphics/Canvas;

    goto/32 :goto_6

    nop

    :goto_16
    iget-object v0, p0, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$StaticGeneralLayer;->mLayerShader:Landroid/graphics/BitmapShader;

    goto/32 :goto_7

    nop

    :goto_17
    sget-object v2, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    goto/32 :goto_1d

    nop

    :goto_18
    invoke-virtual {p0}, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$StaticGeneralLayer;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto/32 :goto_1

    nop

    :goto_19
    if-nez v0, :cond_2

    goto/32 :goto_8

    :cond_2
    goto/32 :goto_d

    nop

    :goto_1a
    const/4 v1, 0x0

    goto/32 :goto_17

    nop

    :goto_1b
    iput-object v0, p0, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$StaticGeneralLayer;->mLayerShader:Landroid/graphics/BitmapShader;

    :goto_1c
    goto/32 :goto_16

    nop

    :goto_1d
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    goto/32 :goto_15

    nop
.end method

.method onBoundsChange(Landroid/graphics/Rect;FF)V
    .locals 0

    goto/32 :goto_6

    nop

    :goto_0
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result p3

    goto/32 :goto_8

    nop

    :goto_1
    if-gtz p2, :cond_0

    goto/32 :goto_16

    :cond_0
    goto/32 :goto_11

    nop

    :goto_2
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result p2

    goto/32 :goto_b

    nop

    :goto_3
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result p2

    goto/32 :goto_1

    nop

    :goto_4
    return-void

    :goto_5
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result p2

    goto/32 :goto_10

    nop

    :goto_6
    invoke-super {p0, p1, p2, p3}, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$StaticLayer;->onBoundsChange(Landroid/graphics/Rect;FF)V

    goto/32 :goto_f

    nop

    :goto_7
    const/4 p1, 0x0

    goto/32 :goto_9

    nop

    :goto_8
    if-eq p2, p3, :cond_1

    goto/32 :goto_e

    :cond_1
    goto/32 :goto_17

    nop

    :goto_9
    iput-object p1, p0, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$StaticGeneralLayer;->mLayerShader:Landroid/graphics/BitmapShader;

    goto/32 :goto_4

    nop

    :goto_a
    if-nez p2, :cond_2

    goto/32 :goto_e

    :cond_2
    goto/32 :goto_12

    nop

    :goto_b
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result p1

    goto/32 :goto_14

    nop

    :goto_c
    invoke-static {p2, p1, p3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object p1

    goto/32 :goto_15

    nop

    :goto_d
    if-ne p2, p3, :cond_3

    goto/32 :goto_16

    :cond_3
    :goto_e
    goto/32 :goto_3

    nop

    :goto_f
    iget-object p2, p0, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$StaticGeneralLayer;->mLayerBitmap:Landroid/graphics/Bitmap;

    goto/32 :goto_a

    nop

    :goto_10
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result p3

    goto/32 :goto_d

    nop

    :goto_11
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result p2

    goto/32 :goto_13

    nop

    :goto_12
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result p2

    goto/32 :goto_0

    nop

    :goto_13
    if-gtz p2, :cond_4

    goto/32 :goto_16

    :cond_4
    goto/32 :goto_2

    nop

    :goto_14
    sget-object p3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    goto/32 :goto_c

    nop

    :goto_15
    iput-object p1, p0, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$StaticGeneralLayer;->mLayerBitmap:Landroid/graphics/Bitmap;

    :goto_16
    goto/32 :goto_7

    nop

    :goto_17
    iget-object p2, p0, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$StaticGeneralLayer;->mLayerBitmap:Landroid/graphics/Bitmap;

    goto/32 :goto_5

    nop
.end method
