.class public abstract Lcom/miui/home/launcher/compat/UserPresentAnimationCompatV12Base;
.super Lcom/miui/home/launcher/compat/UserPresentAnimationCompatComplex;

# interfaces
.implements Lcom/miui/home/launcher/DeviceProfile$OnDeviceProfileChangeListener;


# instance fields
.field protected final LOCATION:[I

.field private final mAnimationValue:[F

.field private final mCameraTranslationZ:F

.field private final mDelayDistanceRatio:F

.field private final mDelayRandomRatio:F

.field protected mMinDelay:I

.field protected mPivot:[I


# direct methods
.method constructor <init>(Lcom/miui/home/launcher/Launcher;FFF)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/miui/home/launcher/compat/UserPresentAnimationCompatComplex;-><init>(Lcom/miui/home/launcher/Launcher;)V

    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/miui/home/launcher/compat/UserPresentAnimationCompatV12Base;->LOCATION:[I

    const v0, 0x7fffffff

    iput v0, p0, Lcom/miui/home/launcher/compat/UserPresentAnimationCompatV12Base;->mMinDelay:I

    const/4 v0, 0x3

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/miui/home/launcher/compat/UserPresentAnimationCompatV12Base;->mAnimationValue:[F

    invoke-direct {p0}, Lcom/miui/home/launcher/compat/UserPresentAnimationCompatV12Base;->changePivot()V

    iput p2, p0, Lcom/miui/home/launcher/compat/UserPresentAnimationCompatV12Base;->mCameraTranslationZ:F

    iput p3, p0, Lcom/miui/home/launcher/compat/UserPresentAnimationCompatV12Base;->mDelayDistanceRatio:F

    iput p4, p0, Lcom/miui/home/launcher/compat/UserPresentAnimationCompatV12Base;->mDelayRandomRatio:F

    invoke-virtual {p1, p0}, Lcom/miui/home/launcher/Launcher;->addOnDeviceProfileChangeListener(Lcom/miui/home/launcher/DeviceProfile$OnDeviceProfileChangeListener;)V

    return-void
.end method

.method private changePivot()V
    .locals 6

    const/4 v0, 0x2

    new-array v1, v0, [I

    invoke-static {}, Lcom/miui/home/launcher/DeviceConfig;->getScreenWidth()I

    move-result v2

    div-int/2addr v2, v0

    const/4 v0, 0x0

    aput v2, v1, v0

    invoke-static {}, Lcom/miui/home/launcher/DeviceConfig;->getScreenHeight()I

    move-result v0

    int-to-double v2, v0

    const-wide/high16 v4, 0x4004000000000000L    # 2.5

    div-double/2addr v2, v4

    double-to-int v0, v2

    const/4 v2, 0x1

    aput v0, v1, v2

    iput-object v1, p0, Lcom/miui/home/launcher/compat/UserPresentAnimationCompatV12Base;->mPivot:[I

    return-void
.end method


# virtual methods
.method protected conversionValueFrom3DTo2D(IIF)[F
    .locals 2

    int-to-float p1, p1

    int-to-float p2, p2

    invoke-static {p1, p2, p3}, Lcom/miui/home/launcher/util/CameraLite;->to2D(FFF)Landroid/graphics/PointF;

    move-result-object v0

    iget v1, v0, Landroid/graphics/PointF;->x:F

    sub-float/2addr v1, p1

    iget p1, v0, Landroid/graphics/PointF;->y:F

    sub-float/2addr p1, p2

    const/high16 p2, 0x3f800000    # 1.0f

    invoke-static {p2, p3}, Lcom/miui/home/launcher/util/CameraLite;->valueTo2D(FF)F

    move-result p2

    iget-object p3, p0, Lcom/miui/home/launcher/compat/UserPresentAnimationCompatV12Base;->mAnimationValue:[F

    const/4 v0, 0x0

    aput v1, p3, v0

    const/4 v0, 0x1

    aput p1, p3, v0

    const/4 p1, 0x2

    aput p2, p3, p1

    return-object p3
.end method

.method public abstract endAnimation(Landroid/view/View;)V
.end method

.method public onDeviceProfileChanged(Lcom/miui/home/launcher/DeviceProfile;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/home/launcher/compat/UserPresentAnimationCompatV12Base;->changePivot()V

    return-void
.end method

.method public prepareAnimation()V
    .locals 4

    iget-wide v0, p0, Lcom/miui/home/launcher/compat/UserPresentAnimationCompatV12Base;->mPreparedScreenId:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const v0, 0x7fffffff

    iput v0, p0, Lcom/miui/home/launcher/compat/UserPresentAnimationCompatV12Base;->mMinDelay:I

    :cond_0
    invoke-super {p0}, Lcom/miui/home/launcher/compat/UserPresentAnimationCompatComplex;->prepareAnimation()V

    return-void
.end method

.method protected prepareUserPresentAnimation(Landroid/view/View;)V
    .locals 13

    if-eqz p1, :cond_0

    const v0, 0x7f0a03e4

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-virtual {p0, p1}, Lcom/miui/home/launcher/compat/UserPresentAnimationCompatV12Base;->endAnimation(Landroid/view/View;)V

    iget-object v1, p0, Lcom/miui/home/launcher/compat/UserPresentAnimationCompatV12Base;->LOCATION:[I

    invoke-virtual {p1, v1}, Landroid/view/View;->getLocationInWindow([I)V

    iget-object v1, p0, Lcom/miui/home/launcher/compat/UserPresentAnimationCompatV12Base;->LOCATION:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v3

    const/4 v4, 0x2

    div-int/2addr v3, v4

    add-int/2addr v1, v3

    iget-object v3, p0, Lcom/miui/home/launcher/compat/UserPresentAnimationCompatV12Base;->LOCATION:[I

    const/4 v5, 0x1

    aget v3, v3, v5

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v6

    div-int/2addr v6, v4

    add-int/2addr v3, v6

    iget-object v6, p0, Lcom/miui/home/launcher/compat/UserPresentAnimationCompatV12Base;->mPivot:[I

    aget v7, v6, v2

    sub-int v7, v1, v7

    int-to-float v7, v7

    aget v6, v6, v5

    sub-int v6, v3, v6

    int-to-float v6, v6

    invoke-static {v7, v6}, Lcom/miui/home/launcher/common/Utilities;->calcDistance(FF)I

    move-result v6

    int-to-float v6, v6

    iget v7, p0, Lcom/miui/home/launcher/compat/UserPresentAnimationCompatV12Base;->mDelayDistanceRatio:F

    mul-float/2addr v6, v7

    float-to-double v6, v6

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v8

    iget v10, p0, Lcom/miui/home/launcher/compat/UserPresentAnimationCompatV12Base;->mDelayRandomRatio:F

    float-to-double v10, v10

    mul-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->floor(D)D

    move-result-wide v8

    add-double/2addr v6, v8

    double-to-int v6, v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {p1, v0, v7}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    iget v0, p0, Lcom/miui/home/launcher/compat/UserPresentAnimationCompatV12Base;->mMinDelay:I

    invoke-static {v6, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/miui/home/launcher/compat/UserPresentAnimationCompatV12Base;->mMinDelay:I

    iget-object v0, p0, Lcom/miui/home/launcher/compat/UserPresentAnimationCompatV12Base;->mPivot:[I

    aget v6, v0, v2

    sub-int/2addr v1, v6

    aget v0, v0, v5

    sub-int/2addr v3, v0

    const v0, 0x7f0a03e2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {p1, v0, v6}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    const v0, 0x7f0a03e3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {p1, v0, v6}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    iget v0, p0, Lcom/miui/home/launcher/compat/UserPresentAnimationCompatV12Base;->mCameraTranslationZ:F

    invoke-virtual {p0, v1, v3, v0}, Lcom/miui/home/launcher/compat/UserPresentAnimationCompatV12Base;->conversionValueFrom3DTo2D(IIF)[F

    move-result-object v0

    aget v8, v0, v4

    aget v9, v0, v4

    const v10, 0x3e19999a    # 0.15f

    aget v11, v0, v2

    aget v12, v0, v5

    move-object v6, p0

    move-object v7, p1

    invoke-virtual/range {v6 .. v12}, Lcom/miui/home/launcher/compat/UserPresentAnimationCompatV12Base;->setViewPrepareInfo(Landroid/view/View;FFFFF)V

    :cond_0
    return-void
.end method

.method protected setViewPrepareInfo(Landroid/view/View;FFFFF)V
    .locals 0

    invoke-virtual {p1, p4}, Landroid/view/View;->setAlpha(F)V

    invoke-virtual {p1, p2}, Landroid/view/View;->setScaleX(F)V

    invoke-virtual {p1, p3}, Landroid/view/View;->setScaleY(F)V

    invoke-virtual {p0, p1}, Lcom/miui/home/launcher/compat/UserPresentAnimationCompatV12Base;->ignoreTranslation(Landroid/view/View;)Z

    move-result p2

    if-nez p2, :cond_0

    invoke-virtual {p1, p5}, Landroid/view/View;->setTranslationX(F)V

    :cond_0
    invoke-virtual {p1, p6}, Landroid/view/View;->setTranslationY(F)V

    return-void
.end method

.method final showUserPresentAnimation(Landroid/view/View;)V
    .locals 4

    goto/32 :goto_a

    nop

    :goto_0
    const/4 v3, 0x0

    goto/32 :goto_14

    nop

    :goto_1
    invoke-virtual {p0, p1}, Lcom/miui/home/launcher/compat/UserPresentAnimationCompatV12Base;->prepareUserPresentAnimation(Landroid/view/View;)V

    :goto_2
    goto/32 :goto_b

    nop

    :goto_3
    if-nez v1, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_8

    nop

    :goto_4
    goto :goto_17

    :goto_5
    goto/32 :goto_16

    nop

    :goto_6
    invoke-virtual {p0, p1}, Lcom/miui/home/launcher/compat/UserPresentAnimationCompatV12Base;->ignoreTranslation(Landroid/view/View;)Z

    move-result v1

    goto/32 :goto_1b

    nop

    :goto_7
    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_d

    nop

    :goto_8
    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_19

    nop

    :goto_9
    const/4 v2, 0x0

    goto/32 :goto_3

    nop

    :goto_a
    const v0, 0x7f0a03e4

    goto/32 :goto_7

    nop

    :goto_b
    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_15

    nop

    :goto_c
    return-void

    :goto_d
    if-eqz v1, :cond_1

    goto/32 :goto_2

    :cond_1
    goto/32 :goto_1

    nop

    :goto_e
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto/32 :goto_4

    nop

    :goto_f
    if-eq v0, v3, :cond_2

    goto/32 :goto_1e

    :cond_2
    goto/32 :goto_1d

    nop

    :goto_10
    sub-int/2addr v1, v0

    goto/32 :goto_12

    nop

    :goto_11
    const/16 v0, 0x12c

    goto/32 :goto_18

    nop

    :goto_12
    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    :goto_13
    goto/32 :goto_11

    nop

    :goto_14
    invoke-virtual {p1, v0, v3}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    goto/32 :goto_1a

    nop

    :goto_15
    instance-of v1, v1, Ljava/lang/Integer;

    goto/32 :goto_9

    nop

    :goto_16
    move v1, v2

    :goto_17
    goto/32 :goto_0

    nop

    :goto_18
    invoke-static {v1, v2, v0}, Lcom/miui/home/launcher/common/Utilities;->boundToRange(III)I

    move-result v0

    goto/32 :goto_6

    nop

    :goto_19
    check-cast v1, Ljava/lang/Integer;

    goto/32 :goto_e

    nop

    :goto_1a
    iget v0, p0, Lcom/miui/home/launcher/compat/UserPresentAnimationCompatV12Base;->mMinDelay:I

    goto/32 :goto_1c

    nop

    :goto_1b
    invoke-virtual {p0, p1, v0, v1}, Lcom/miui/home/launcher/compat/UserPresentAnimationCompatV12Base;->showUserPresentAnimation(Landroid/view/View;IZ)V

    goto/32 :goto_c

    nop

    :goto_1c
    const v3, 0x7fffffff

    goto/32 :goto_f

    nop

    :goto_1d
    goto :goto_13

    :goto_1e
    goto/32 :goto_10

    nop
.end method

.method abstract showUserPresentAnimation(Landroid/view/View;IZ)V
.end method
