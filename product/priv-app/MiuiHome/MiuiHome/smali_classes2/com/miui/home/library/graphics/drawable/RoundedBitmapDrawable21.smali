.class Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable21;
.super Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;


# direct methods
.method protected constructor <init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    return-void
.end method


# virtual methods
.method public getOutline(Landroid/graphics/Outline;)V
    .locals 2

    invoke-virtual {p0}, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable21;->updateDstRect()V

    iget-object v0, p0, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable21;->mDstRect:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/miui/home/library/graphics/drawable/RoundedBitmapDrawable21;->getCornerRadius()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Outline;->setRoundRect(Landroid/graphics/Rect;F)V

    return-void
.end method

.method gravityCompatApply(IIILandroid/graphics/Rect;Landroid/graphics/Rect;)V
    .locals 6

    goto/32 :goto_3

    nop

    :goto_0
    return-void

    :goto_1
    move v1, p2

    goto/32 :goto_5

    nop

    :goto_2
    invoke-static/range {v0 .. v5}, Landroid/view/Gravity;->apply(IIILandroid/graphics/Rect;Landroid/graphics/Rect;I)V

    goto/32 :goto_0

    nop

    :goto_3
    const/4 v5, 0x0

    goto/32 :goto_6

    nop

    :goto_4
    move-object v3, p4

    goto/32 :goto_7

    nop

    :goto_5
    move v2, p3

    goto/32 :goto_4

    nop

    :goto_6
    move v0, p1

    goto/32 :goto_1

    nop

    :goto_7
    move-object v4, p5

    goto/32 :goto_2

    nop
.end method
