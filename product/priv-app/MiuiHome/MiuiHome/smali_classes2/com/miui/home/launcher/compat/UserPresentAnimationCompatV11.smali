.class public Lcom/miui/home/launcher/compat/UserPresentAnimationCompatV11;
.super Lcom/miui/home/launcher/compat/UserPresentAnimationCompatComplex;


# instance fields
.field private final mPoivt:[I

.field private final mTmpLocation:[I


# direct methods
.method constructor <init>(Lcom/miui/home/launcher/Launcher;)V
    .locals 2

    invoke-direct {p0, p1}, Lcom/miui/home/launcher/compat/UserPresentAnimationCompatComplex;-><init>(Lcom/miui/home/launcher/Launcher;)V

    const/4 p1, 0x2

    new-array v0, p1, [I

    iput-object v0, p0, Lcom/miui/home/launcher/compat/UserPresentAnimationCompatV11;->mTmpLocation:[I

    new-array v0, p1, [I

    invoke-static {}, Lcom/miui/home/launcher/DeviceConfig;->getScreenWidth()I

    move-result v1

    div-int/2addr v1, p1

    const/4 p1, 0x0

    aput v1, v0, p1

    const/4 p1, 0x1

    const/16 v1, -0x64

    aput v1, v0, p1

    iput-object v0, p0, Lcom/miui/home/launcher/compat/UserPresentAnimationCompatV11;->mPoivt:[I

    return-void
.end method

.method static synthetic lambda$showUserPresentAnimation$0(Landroidx/dynamicanimation/animation/SpringAnimation;Landroidx/dynamicanimation/animation/SpringAnimation;Landroidx/dynamicanimation/animation/SpringAnimation;Landroidx/dynamicanimation/animation/SpringAnimation;)V
    .locals 0

    invoke-virtual {p0}, Landroidx/dynamicanimation/animation/SpringAnimation;->start()V

    invoke-virtual {p1}, Landroidx/dynamicanimation/animation/SpringAnimation;->start()V

    invoke-virtual {p2}, Landroidx/dynamicanimation/animation/SpringAnimation;->start()V

    invoke-virtual {p3}, Landroidx/dynamicanimation/animation/SpringAnimation;->start()V

    return-void
.end method

.method static synthetic lambda$showUserPresentAnimation$1(Landroidx/dynamicanimation/animation/SpringAnimation;)V
    .locals 0

    invoke-virtual {p0}, Landroidx/dynamicanimation/animation/SpringAnimation;->start()V

    return-void
.end method


# virtual methods
.method prepareUserPresentAnimation(Landroid/view/View;)V
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    if-nez p1, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_1

    nop

    :goto_1
    const/4 v0, 0x0

    goto/32 :goto_2

    nop

    :goto_2
    invoke-virtual {p1, v0}, Landroid/view/View;->setAlpha(F)V

    :goto_3
    goto/32 :goto_4

    nop

    :goto_4
    return-void
.end method

.method showUserPresentAnimation(Landroid/view/View;)V
    .locals 12

    goto/32 :goto_2a

    nop

    :goto_0
    add-int/2addr v2, v4

    goto/32 :goto_1

    nop

    :goto_1
    iget-object v4, p0, Lcom/miui/home/launcher/compat/UserPresentAnimationCompatV11;->mPoivt:[I

    goto/32 :goto_52

    nop

    :goto_2
    sub-float v7, v8, v7

    goto/32 :goto_2b

    nop

    :goto_3
    const/high16 v10, 0x45fa0000    # 8000.0f

    goto/32 :goto_46

    nop

    :goto_4
    invoke-direct {v2, p1, v3}, Landroidx/dynamicanimation/animation/SpringAnimation;-><init>(Ljava/lang/Object;Landroidx/dynamicanimation/animation/FloatPropertyCompat;)V

    goto/32 :goto_5d

    nop

    :goto_5
    invoke-static {v8, v7, v4}, Lcom/miui/home/launcher/animate/SpringAnimator;->getSpringForce(FFF)Landroidx/dynamicanimation/animation/SpringForce;

    move-result-object v4

    goto/32 :goto_45

    nop

    :goto_6
    invoke-virtual {v6, v4}, Landroidx/dynamicanimation/animation/SpringAnimation;->setSpring(Landroidx/dynamicanimation/animation/SpringForce;)Landroidx/dynamicanimation/animation/SpringAnimation;

    move-result-object v6

    goto/32 :goto_26

    nop

    :goto_7
    invoke-direct {v7, p1, v10}, Landroidx/dynamicanimation/animation/SpringAnimation;-><init>(Ljava/lang/Object;Landroidx/dynamicanimation/animation/FloatPropertyCompat;)V

    goto/32 :goto_5b

    nop

    :goto_8
    const/high16 v3, 0x3fc00000    # 1.5f

    goto/32 :goto_21

    nop

    :goto_9
    invoke-static {v4}, Lcom/miui/home/launcher/animate/SpringAnimator;->stiffnessConvert(F)F

    move-result v4

    goto/32 :goto_10

    nop

    :goto_a
    invoke-static {v8, p1, v3}, Lcom/miui/home/launcher/animate/SpringAnimator;->getSpringForce(FFF)Landroidx/dynamicanimation/animation/SpringForce;

    move-result-object p1

    goto/32 :goto_1a

    nop

    :goto_b
    invoke-static {v5, v0, v1}, Lcom/miui/home/launcher/common/Utilities;->useViewToPostDelay(Ljava/lang/Runnable;J)V

    goto/32 :goto_28

    nop

    :goto_c
    aget v2, v2, v3

    goto/32 :goto_2c

    nop

    :goto_d
    sget-object v9, Landroidx/dynamicanimation/animation/DynamicAnimation;->TRANSLATION_Y:Landroidx/dynamicanimation/animation/DynamicAnimation$ViewProperty;

    goto/32 :goto_5c

    nop

    :goto_e
    sget-object v3, Landroidx/dynamicanimation/animation/DynamicAnimation;->ALPHA:Landroidx/dynamicanimation/animation/DynamicAnimation$ViewProperty;

    goto/32 :goto_4

    nop

    :goto_f
    check-cast v4, Landroidx/dynamicanimation/animation/SpringAnimation;

    goto/32 :goto_62

    nop

    :goto_10
    new-instance v6, Landroidx/dynamicanimation/animation/SpringAnimation;

    goto/32 :goto_d

    nop

    :goto_11
    const/4 v9, 0x0

    goto/32 :goto_39

    nop

    :goto_12
    invoke-static {v7, v4}, Ljava/lang/Math;->min(FF)F

    move-result v7

    goto/32 :goto_2

    nop

    :goto_13
    int-to-float v2, v2

    goto/32 :goto_4a

    nop

    :goto_14
    const/high16 v8, 0x3f800000    # 1.0f

    goto/32 :goto_53

    nop

    :goto_15
    invoke-direct {v2, p1}, Lcom/miui/home/launcher/compat/-$$Lambda$UserPresentAnimationCompatV11$L8HysBjpritXs5Z-_RiQzoKalAo;-><init>(Landroidx/dynamicanimation/animation/SpringAnimation;)V

    goto/32 :goto_18

    nop

    :goto_16
    sget-object v10, Landroidx/dynamicanimation/animation/DynamicAnimation;->SCALE_X:Landroidx/dynamicanimation/animation/DynamicAnimation$ViewProperty;

    goto/32 :goto_7

    nop

    :goto_17
    int-to-float v6, v0

    goto/32 :goto_56

    nop

    :goto_18
    const-wide/16 v3, 0x2

    goto/32 :goto_34

    nop

    :goto_19
    aget v0, v0, v1

    goto/32 :goto_25

    nop

    :goto_1a
    invoke-virtual {v2, p1}, Landroidx/dynamicanimation/animation/SpringAnimation;->setSpring(Landroidx/dynamicanimation/animation/SpringForce;)Landroidx/dynamicanimation/animation/SpringAnimation;

    move-result-object p1

    goto/32 :goto_1e

    nop

    :goto_1b
    instance-of v3, p1, Lcom/miui/home/launcher/ItemIcon;

    goto/32 :goto_32

    nop

    :goto_1c
    check-cast v6, Landroidx/dynamicanimation/animation/SpringAnimation;

    goto/32 :goto_5a

    nop

    :goto_1d
    sget-object v11, Landroidx/dynamicanimation/animation/DynamicAnimation;->SCALE_Y:Landroidx/dynamicanimation/animation/DynamicAnimation$ViewProperty;

    goto/32 :goto_2e

    nop

    :goto_1e
    invoke-virtual {p1, v9}, Landroidx/dynamicanimation/animation/SpringAnimation;->setStartValue(F)Landroidx/dynamicanimation/animation/DynamicAnimation;

    move-result-object p1

    goto/32 :goto_38

    nop

    :goto_1f
    invoke-virtual {v10, v4}, Landroidx/dynamicanimation/animation/SpringAnimation;->setSpring(Landroidx/dynamicanimation/animation/SpringForce;)Landroidx/dynamicanimation/animation/SpringAnimation;

    move-result-object v4

    goto/32 :goto_58

    nop

    :goto_20
    sub-int/2addr v2, v3

    goto/32 :goto_13

    nop

    :goto_21
    mul-float/2addr v2, v3

    goto/32 :goto_1b

    nop

    :goto_22
    iget-object v2, p0, Lcom/miui/home/launcher/compat/UserPresentAnimationCompatV11;->mTmpLocation:[I

    goto/32 :goto_23

    nop

    :goto_23
    const/4 v3, 0x1

    goto/32 :goto_c

    nop

    :goto_24
    add-int/2addr v0, v2

    goto/32 :goto_22

    nop

    :goto_25
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v2

    goto/32 :goto_3a

    nop

    :goto_26
    invoke-virtual {v6, v9}, Landroidx/dynamicanimation/animation/SpringAnimation;->setStartValue(F)Landroidx/dynamicanimation/animation/DynamicAnimation;

    move-result-object v6

    goto/32 :goto_1c

    nop

    :goto_27
    invoke-static {v3}, Lcom/miui/home/launcher/animate/SpringAnimator;->stiffnessConvert(F)F

    move-result v3

    goto/32 :goto_a

    nop

    :goto_28
    if-nez v3, :cond_0

    goto/32 :goto_66

    :cond_0
    goto/32 :goto_61

    nop

    :goto_29
    invoke-static {v6, v4}, Ljava/lang/Math;->min(FF)F

    move-result v4

    goto/32 :goto_67

    nop

    :goto_2a
    iget-object v0, p0, Lcom/miui/home/launcher/compat/UserPresentAnimationCompatV11;->mTmpLocation:[I

    goto/32 :goto_4f

    nop

    :goto_2b
    const/high16 v9, 0x3f000000    # 0.5f

    goto/32 :goto_3

    nop

    :goto_2c
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v4

    goto/32 :goto_33

    nop

    :goto_2d
    invoke-direct {v6, p1, v7}, Landroidx/dynamicanimation/animation/SpringAnimation;-><init>(Ljava/lang/Object;Landroidx/dynamicanimation/animation/FloatPropertyCompat;)V

    goto/32 :goto_6

    nop

    :goto_2e
    invoke-direct {v10, p1, v11}, Landroidx/dynamicanimation/animation/SpringAnimation;-><init>(Ljava/lang/Object;Landroidx/dynamicanimation/animation/FloatPropertyCompat;)V

    goto/32 :goto_1f

    nop

    :goto_2f
    const v2, 0x493e0

    goto/32 :goto_36

    nop

    :goto_30
    int-to-float v0, v0

    goto/32 :goto_6b

    nop

    :goto_31
    int-to-long v0, v0

    goto/32 :goto_40

    nop

    :goto_32
    const v4, 0x3e4ccccd    # 0.2f

    goto/32 :goto_54

    nop

    :goto_33
    div-int/lit8 v4, v4, 0x2

    goto/32 :goto_0

    nop

    :goto_34
    mul-long/2addr v0, v3

    goto/32 :goto_65

    nop

    :goto_35
    check-cast v2, Landroidx/dynamicanimation/animation/SpringAnimation;

    goto/32 :goto_6c

    nop

    :goto_36
    div-int/2addr v2, v0

    goto/32 :goto_5e

    nop

    :goto_37
    div-float v7, v6, v7

    goto/32 :goto_14

    nop

    :goto_38
    check-cast p1, Landroidx/dynamicanimation/animation/SpringAnimation;

    goto/32 :goto_59

    nop

    :goto_39
    invoke-static {v9, v7, v4}, Lcom/miui/home/launcher/animate/SpringAnimator;->getSpringForce(FFF)Landroidx/dynamicanimation/animation/SpringForce;

    move-result-object v10

    goto/32 :goto_49

    nop

    :goto_3a
    div-int/lit8 v2, v2, 0x2

    goto/32 :goto_24

    nop

    :goto_3b
    const v7, 0x45bb8000    # 6000.0f

    goto/32 :goto_55

    nop

    :goto_3c
    goto :goto_4c

    :goto_3d
    goto/32 :goto_4b

    nop

    :goto_3e
    return-void

    :goto_3f
    add-int/lit8 v0, v0, -0x1e

    goto/32 :goto_51

    nop

    :goto_40
    new-instance v5, Lcom/miui/home/launcher/compat/-$$Lambda$UserPresentAnimationCompatV11$nUrXB9ilpTMYTW2uGhqn7F0GcAU;

    goto/32 :goto_60

    nop

    :goto_41
    invoke-virtual {v6, v2}, Landroidx/dynamicanimation/animation/SpringAnimation;->setStartValue(F)Landroidx/dynamicanimation/animation/DynamicAnimation;

    move-result-object v2

    goto/32 :goto_35

    nop

    :goto_42
    const/4 v1, 0x0

    goto/32 :goto_19

    nop

    :goto_43
    invoke-virtual {v7, v5}, Landroidx/dynamicanimation/animation/SpringAnimation;->setStartValue(F)Landroidx/dynamicanimation/animation/DynamicAnimation;

    move-result-object v7

    goto/32 :goto_68

    nop

    :goto_44
    const v3, 0x3f333333    # 0.7f

    goto/32 :goto_27

    nop

    :goto_45
    new-instance v6, Landroidx/dynamicanimation/animation/SpringAnimation;

    goto/32 :goto_5f

    nop

    :goto_46
    div-float/2addr v6, v10

    goto/32 :goto_29

    nop

    :goto_47
    invoke-virtual {v2, v6}, Landroidx/dynamicanimation/animation/SpringAnimation;->setStartVelocity(F)Landroidx/dynamicanimation/animation/DynamicAnimation;

    goto/32 :goto_5

    nop

    :goto_48
    invoke-virtual {p1}, Lcom/miui/home/launcher/ItemIcon;->getTitleContainer()Landroid/view/View;

    move-result-object p1

    goto/32 :goto_69

    nop

    :goto_49
    invoke-virtual {v6, v10}, Landroidx/dynamicanimation/animation/SpringAnimation;->setSpring(Landroidx/dynamicanimation/animation/SpringForce;)Landroidx/dynamicanimation/animation/SpringAnimation;

    move-result-object v6

    goto/32 :goto_41

    nop

    :goto_4a
    invoke-static {v0, v2}, Lcom/miui/home/launcher/common/Utilities;->calcDistance(FF)I

    move-result v0

    goto/32 :goto_2f

    nop

    :goto_4b
    const v5, 0x3f4ccccd    # 0.8f

    :goto_4c
    goto/32 :goto_17

    nop

    :goto_4d
    invoke-static {v7, v5}, Ljava/lang/Math;->max(FF)F

    move-result v5

    goto/32 :goto_3b

    nop

    :goto_4e
    new-instance v10, Landroidx/dynamicanimation/animation/SpringAnimation;

    goto/32 :goto_1d

    nop

    :goto_4f
    invoke-virtual {p1, v0}, Landroid/view/View;->getLocationInWindow([I)V

    goto/32 :goto_64

    nop

    :goto_50
    move v5, v4

    goto/32 :goto_3c

    nop

    :goto_51
    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto/32 :goto_31

    nop

    :goto_52
    aget v5, v4, v1

    goto/32 :goto_57

    nop

    :goto_53
    sub-float v7, v8, v7

    goto/32 :goto_4d

    nop

    :goto_54
    if-nez v3, :cond_1

    goto/32 :goto_3d

    :cond_1
    goto/32 :goto_50

    nop

    :goto_55
    div-float v7, v6, v7

    goto/32 :goto_12

    nop

    :goto_56
    const v7, 0x45124000    # 2340.0f

    goto/32 :goto_37

    nop

    :goto_57
    sub-int/2addr v0, v5

    goto/32 :goto_30

    nop

    :goto_58
    invoke-virtual {v4, v5}, Landroidx/dynamicanimation/animation/SpringAnimation;->setStartValue(F)Landroidx/dynamicanimation/animation/DynamicAnimation;

    move-result-object v4

    goto/32 :goto_f

    nop

    :goto_59
    new-instance v2, Lcom/miui/home/launcher/compat/-$$Lambda$UserPresentAnimationCompatV11$L8HysBjpritXs5Z-_RiQzoKalAo;

    goto/32 :goto_15

    nop

    :goto_5a
    new-instance v7, Landroidx/dynamicanimation/animation/SpringAnimation;

    goto/32 :goto_16

    nop

    :goto_5b
    invoke-virtual {v7, v4}, Landroidx/dynamicanimation/animation/SpringAnimation;->setSpring(Landroidx/dynamicanimation/animation/SpringForce;)Landroidx/dynamicanimation/animation/SpringAnimation;

    move-result-object v7

    goto/32 :goto_43

    nop

    :goto_5c
    invoke-direct {v6, p1, v9}, Landroidx/dynamicanimation/animation/SpringAnimation;-><init>(Ljava/lang/Object;Landroidx/dynamicanimation/animation/FloatPropertyCompat;)V

    goto/32 :goto_11

    nop

    :goto_5d
    const p1, 0x3f666666    # 0.9f

    goto/32 :goto_44

    nop

    :goto_5e
    int-to-float v2, v2

    goto/32 :goto_8

    nop

    :goto_5f
    sget-object v7, Landroidx/dynamicanimation/animation/DynamicAnimation;->ALPHA:Landroidx/dynamicanimation/animation/DynamicAnimation$ViewProperty;

    goto/32 :goto_2d

    nop

    :goto_60
    invoke-direct {v5, v2, v6, v7, v4}, Lcom/miui/home/launcher/compat/-$$Lambda$UserPresentAnimationCompatV11$nUrXB9ilpTMYTW2uGhqn7F0GcAU;-><init>(Landroidx/dynamicanimation/animation/SpringAnimation;Landroidx/dynamicanimation/animation/SpringAnimation;Landroidx/dynamicanimation/animation/SpringAnimation;Landroidx/dynamicanimation/animation/SpringAnimation;)V

    goto/32 :goto_b

    nop

    :goto_61
    check-cast p1, Lcom/miui/home/launcher/ItemIcon;

    goto/32 :goto_48

    nop

    :goto_62
    div-int/lit8 v0, v0, 0x10

    goto/32 :goto_3f

    nop

    :goto_63
    int-to-float v6, v6

    goto/32 :goto_47

    nop

    :goto_64
    iget-object v0, p0, Lcom/miui/home/launcher/compat/UserPresentAnimationCompatV11;->mTmpLocation:[I

    goto/32 :goto_42

    nop

    :goto_65
    invoke-static {v2, v0, v1}, Lcom/miui/home/launcher/common/Utilities;->useViewToPostDelay(Ljava/lang/Runnable;J)V

    :goto_66
    goto/32 :goto_3e

    nop

    :goto_67
    add-float/2addr v4, v9

    goto/32 :goto_9

    nop

    :goto_68
    check-cast v7, Landroidx/dynamicanimation/animation/SpringAnimation;

    goto/32 :goto_4e

    nop

    :goto_69
    new-instance v2, Landroidx/dynamicanimation/animation/SpringAnimation;

    goto/32 :goto_e

    nop

    :goto_6a
    div-int/2addr v6, v0

    goto/32 :goto_63

    nop

    :goto_6b
    aget v3, v4, v3

    goto/32 :goto_20

    nop

    :goto_6c
    const v6, -0x4c4b40

    goto/32 :goto_6a

    nop
.end method
