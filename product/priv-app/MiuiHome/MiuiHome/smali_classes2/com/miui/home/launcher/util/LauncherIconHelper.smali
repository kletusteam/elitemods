.class public Lcom/miui/home/launcher/util/LauncherIconHelper;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/preference/CustomUpdater$CustomReceiver;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/home/launcher/util/LauncherIconHelper$IconLabelState;,
        Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;
    }
.end annotation


# static fields
.field private static final HELPER:Lcom/miui/home/launcher/util/LauncherIconHelper;


# instance fields
.field private darkShadowColor:I

.field private darkShadowSize:F

.field private final handler:Landroid/os/Handler;

.field private hotSeatsLabelState:Lcom/miui/home/launcher/util/LauncherIconHelper$IconLabelState;

.field private hotSeatsShadowState:Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;

.field private iconLabelState:Lcom/miui/home/launcher/util/LauncherIconHelper$IconLabelState;

.field private iconReflectionGap:F

.field private iconShadowState:Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;

.field private final keys:[Ljava/lang/String;

.field private recycleCount:I

.field private reflectionShadowColor:I

.field private regionRunable:Ljava/lang/Runnable;

.field private titleColor:I

.field private titleSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/miui/home/launcher/util/LauncherIconHelper;

    invoke-direct {v0}, Lcom/miui/home/launcher/util/LauncherIconHelper;-><init>()V

    sput-object v0, Lcom/miui/home/launcher/util/LauncherIconHelper;->HELPER:Lcom/miui/home/launcher/util/LauncherIconHelper;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const/4 v3, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v3, p0, Lcom/miui/home/launcher/util/LauncherIconHelper;->recycleCount:I

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "launcher_icon_helper"

    aput-object v1, v0, v3

    const/4 v1, 0x1

    const-string v2, "launcher_label_icon_helper"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/miui/home/launcher/util/LauncherIconHelper;->keys:[Ljava/lang/String;

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/miui/home/launcher/util/LauncherIconHelper;->handler:Landroid/os/Handler;

    new-instance v0, Lcom/miui/home/launcher/util/LauncherIconHelper$1;

    invoke-direct {v0, p0}, Lcom/miui/home/launcher/util/LauncherIconHelper$1;-><init>(Lcom/miui/home/launcher/util/LauncherIconHelper;)V

    iput-object v0, p0, Lcom/miui/home/launcher/util/LauncherIconHelper;->regionRunable:Ljava/lang/Runnable;

    invoke-static {}, Landroid/preference/CustomUpdater;->getInstance()Landroid/preference/CustomUpdater;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/home/launcher/util/LauncherIconHelper;->keys:[Ljava/lang/String;

    invoke-virtual {v0, p0, v1}, Landroid/preference/CustomUpdater;->addCustomReceiver(Landroid/preference/CustomUpdater$CustomReceiver;[Ljava/lang/String;)V

    iget-object v0, p0, Lcom/miui/home/launcher/util/LauncherIconHelper;->keys:[Ljava/lang/String;

    aget-object v0, v0, v3

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/util/LauncherIconHelper;->onCustomChanged(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$000(Lcom/miui/home/launcher/util/LauncherIconHelper;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/home/launcher/util/LauncherIconHelper;->updateRegion()V

    return-void
.end method

.method public static declared-synchronized getInstance()Lcom/miui/home/launcher/util/LauncherIconHelper;
    .locals 4

    const-class v2, Lcom/miui/home/launcher/util/LauncherIconHelper;

    monitor-enter v2

    :try_start_0
    const-class v3, Lcom/miui/home/launcher/util/LauncherIconHelper;

    monitor-enter v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    sget-object v0, Lcom/miui/home/launcher/util/LauncherIconHelper;->HELPER:Lcom/miui/home/launcher/util/LauncherIconHelper;

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v2

    return-object v0

    :catchall_0
    move-exception v1

    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method private updateRegion()V
    .locals 3

    invoke-static {}, Landroid/preference/SettingsEliteHelper;->getCon()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "miui.intent.action.MIUI_REGION_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method private updateVariables()V
    .locals 4

    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v0, v1, Landroid/util/DisplayMetrics;->density:F

    const-string v1, "launch_dark_sh_color"

    invoke-static {v1}, Landroid/preference/SettingsEliteHelper;->getIntofSettings(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/miui/home/launcher/util/LauncherIconHelper;->darkShadowColor:I

    const-string v1, "launch_dark_sh_size"

    invoke-static {v1}, Landroid/preference/SettingsEliteHelper;->getIntofSettings(Ljava/lang/String;)I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v0

    iput v1, p0, Lcom/miui/home/launcher/util/LauncherIconHelper;->darkShadowSize:F

    const-string v1, "launch_refl_sh_color"

    invoke-static {v1}, Landroid/preference/SettingsEliteHelper;->getIntofSettings(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/miui/home/launcher/util/LauncherIconHelper;->reflectionShadowColor:I

    const-string v1, "launch_icon_reflection_gap"

    invoke-static {v1}, Landroid/preference/SettingsEliteHelper;->getIntofSettings(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, -0x19

    int-to-float v1, v1

    mul-float/2addr v1, v0

    iput v1, p0, Lcom/miui/home/launcher/util/LauncherIconHelper;->iconReflectionGap:F

    invoke-static {}, Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;->values()[Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;

    move-result-object v1

    const-string v2, "launch_icon_shadow_state"

    invoke-static {v2}, Landroid/preference/SettingsEliteHelper;->getIntofSettings(Ljava/lang/String;)I

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, p0, Lcom/miui/home/launcher/util/LauncherIconHelper;->iconShadowState:Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;

    invoke-static {}, Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;->values()[Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;

    move-result-object v1

    const-string v2, "launch_hot_seats_shadow_state"

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/preference/SettingsEliteHelper;->getIntofSettings(Ljava/lang/String;I)I

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, p0, Lcom/miui/home/launcher/util/LauncherIconHelper;->hotSeatsShadowState:Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;

    invoke-static {}, Lcom/miui/home/launcher/util/LauncherIconHelper$IconLabelState;->values()[Lcom/miui/home/launcher/util/LauncherIconHelper$IconLabelState;

    move-result-object v1

    const/4 v3, 0x1

    const-string v2, "launch_icon_label_state"

    invoke-static {v2, v3}, Landroid/preference/SettingsEliteHelper;->getIntofSettings(Ljava/lang/String;I)I

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, p0, Lcom/miui/home/launcher/util/LauncherIconHelper;->iconLabelState:Lcom/miui/home/launcher/util/LauncherIconHelper$IconLabelState;

    invoke-static {}, Lcom/miui/home/launcher/util/LauncherIconHelper$IconLabelState;->values()[Lcom/miui/home/launcher/util/LauncherIconHelper$IconLabelState;

    move-result-object v1

    const-string v2, "launch_hot_seats_label_state"

    invoke-static {v2, v3}, Landroid/preference/SettingsEliteHelper;->getIntofSettings(Ljava/lang/String;I)I

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, p0, Lcom/miui/home/launcher/util/LauncherIconHelper;->hotSeatsLabelState:Lcom/miui/home/launcher/util/LauncherIconHelper$IconLabelState;

    const-string v1, "launch_title_color"

    invoke-static {v1}, Landroid/preference/SettingsEliteHelper;->getIntofSettings(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/miui/home/launcher/util/LauncherIconHelper;->titleColor:I

    const-string v1, "launch_title_size"

    const/16 v2, 0xc

    invoke-static {v1, v2}, Landroid/preference/SettingsEliteHelper;->getIntofSettings(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/miui/home/launcher/util/LauncherIconHelper;->titleSize:I

    return-void
.end method


# virtual methods
.method public getDarkShadowColor()I
    .locals 1

    iget v0, p0, Lcom/miui/home/launcher/util/LauncherIconHelper;->darkShadowColor:I

    return v0
.end method

.method public getDarkShadowSize()F
    .locals 1

    iget v0, p0, Lcom/miui/home/launcher/util/LauncherIconHelper;->darkShadowSize:F

    return v0
.end method

.method public getHotSeatsLabelState()Lcom/miui/home/launcher/util/LauncherIconHelper$IconLabelState;
    .locals 1

    iget-object v0, p0, Lcom/miui/home/launcher/util/LauncherIconHelper;->hotSeatsLabelState:Lcom/miui/home/launcher/util/LauncherIconHelper$IconLabelState;

    return-object v0
.end method

.method public getHotSeatsShadowState()Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;
    .locals 1

    iget-object v0, p0, Lcom/miui/home/launcher/util/LauncherIconHelper;->hotSeatsShadowState:Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;

    return-object v0
.end method

.method public getIconLabelState(Z)Lcom/miui/home/launcher/util/LauncherIconHelper$IconLabelState;
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/miui/home/launcher/util/LauncherIconHelper;->hotSeatsLabelState:Lcom/miui/home/launcher/util/LauncherIconHelper$IconLabelState;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/miui/home/launcher/util/LauncherIconHelper;->iconLabelState:Lcom/miui/home/launcher/util/LauncherIconHelper$IconLabelState;

    goto :goto_0
.end method

.method public getIconReflectionGap()F
    .locals 1

    iget v0, p0, Lcom/miui/home/launcher/util/LauncherIconHelper;->iconReflectionGap:F

    return v0
.end method

.method public getIconShadowState()Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;
    .locals 1

    iget-object v0, p0, Lcom/miui/home/launcher/util/LauncherIconHelper;->iconShadowState:Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;

    return-object v0
.end method

.method public getRecycleCount()I
    .locals 1

    iget v0, p0, Lcom/miui/home/launcher/util/LauncherIconHelper;->recycleCount:I

    return v0
.end method

.method public getReflectionShadowColor()I
    .locals 2

    const/16 v1, 0xff

    iget v0, p0, Lcom/miui/home/launcher/util/LauncherIconHelper;->reflectionShadowColor:I

    invoke-static {v0, v1, v1, v1}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    return v0
.end method

.method public getTitleColor()I
    .locals 1

    iget v0, p0, Lcom/miui/home/launcher/util/LauncherIconHelper;->titleColor:I

    return v0
.end method

.method public getTitleSize()I
    .locals 1

    iget v0, p0, Lcom/miui/home/launcher/util/LauncherIconHelper;->titleSize:I

    return v0
.end method

.method public isReflectState(Z)Z
    .locals 2

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/miui/home/launcher/util/LauncherIconHelper;->hotSeatsShadowState:Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;

    :goto_0
    sget-object v1, Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;->ONLY_REFLECT:Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;->SHADOW_AND_REFLECT:Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;

    if-ne v0, v1, :cond_2

    :cond_0
    const/4 v1, 0x1

    :goto_1
    return v1

    :cond_1
    iget-object v0, p0, Lcom/miui/home/launcher/util/LauncherIconHelper;->iconShadowState:Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public isShadowEnable()Z
    .locals 2

    iget-object v0, p0, Lcom/miui/home/launcher/util/LauncherIconHelper;->iconShadowState:Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;

    sget-object v1, Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;->NOT_SHADOW:Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/miui/home/launcher/util/LauncherIconHelper;->hotSeatsShadowState:Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;

    sget-object v1, Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;->NOT_SHADOW:Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;

    if-eq v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isShadowState(Z)Z
    .locals 2

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/miui/home/launcher/util/LauncherIconHelper;->hotSeatsShadowState:Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;

    :goto_0
    sget-object v1, Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;->ONLY_SHADOW:Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;->SHADOW_AND_REFLECT:Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;

    if-ne v0, v1, :cond_2

    :cond_0
    const/4 v1, 0x1

    :goto_1
    return v1

    :cond_1
    iget-object v0, p0, Lcom/miui/home/launcher/util/LauncherIconHelper;->iconShadowState:Lcom/miui/home/launcher/util/LauncherIconHelper$IconShadowState;

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public onCustomChanged(Ljava/lang/String;)V
    .locals 4

    iget-object v0, p0, Lcom/miui/home/launcher/util/LauncherIconHelper;->keys:[Ljava/lang/String;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/miui/home/launcher/util/LauncherIconHelper;->updateVariables()V

    invoke-static {}, Lcom/miui/home/launcher/common/Utilities;->resetResourceDependenceItem()V

    iget v0, p0, Lcom/miui/home/launcher/util/LauncherIconHelper;->recycleCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/miui/home/launcher/util/LauncherIconHelper;->recycleCount:I

    iget-object v0, p0, Lcom/miui/home/launcher/util/LauncherIconHelper;->keys:[Ljava/lang/String;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/miui/home/launcher/util/LauncherIconHelper;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/miui/home/launcher/util/LauncherIconHelper;->regionRunable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/miui/home/launcher/util/LauncherIconHelper;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/miui/home/launcher/util/LauncherIconHelper;->regionRunable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    return-void
.end method
