.class public final Lcom/miui/home/launcher/common/BackgroundThread;
.super Landroid/os/HandlerThread;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/home/launcher/common/BackgroundThread$BackgroundThreadFactory;,
        Lcom/miui/home/launcher/common/BackgroundThread$BackgroundHandler;
    }
.end annotation


# static fields
.field private static sHandler:Lcom/miui/home/launcher/common/BackgroundThread$BackgroundHandler;

.field private static sSingleThreadExecutor:Ljava/util/concurrent/ExecutorService;


# direct methods
.method private constructor <init>()V
    .locals 2

    const-string v0, "Launcher.Background"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lcom/miui/home/launcher/common/BackgroundThread$BackgroundThreadFactory;

    invoke-direct {v0}, Lcom/miui/home/launcher/common/BackgroundThread$BackgroundThreadFactory;-><init>()V

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    sput-object v0, Lcom/miui/home/launcher/common/BackgroundThread;->sSingleThreadExecutor:Ljava/util/concurrent/ExecutorService;

    return-void
.end method

.method static synthetic access$000()Ljava/util/concurrent/ExecutorService;
    .locals 1

    sget-object v0, Lcom/miui/home/launcher/common/BackgroundThread;->sSingleThreadExecutor:Ljava/util/concurrent/ExecutorService;

    return-object v0
.end method

.method public static getHandler()Landroid/os/Handler;
    .locals 3

    const-class v0, Lcom/miui/home/launcher/common/BackgroundThread;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/miui/home/launcher/common/BackgroundThread;->sHandler:Lcom/miui/home/launcher/common/BackgroundThread$BackgroundHandler;

    if-nez v1, :cond_0

    new-instance v1, Lcom/miui/home/launcher/common/BackgroundThread;

    invoke-direct {v1}, Lcom/miui/home/launcher/common/BackgroundThread;-><init>()V

    invoke-virtual {v1}, Lcom/miui/home/launcher/common/BackgroundThread;->start()V

    new-instance v2, Lcom/miui/home/launcher/common/BackgroundThread$BackgroundHandler;

    invoke-virtual {v1}, Lcom/miui/home/launcher/common/BackgroundThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v2, v1}, Lcom/miui/home/launcher/common/BackgroundThread$BackgroundHandler;-><init>(Landroid/os/Looper;)V

    sput-object v2, Lcom/miui/home/launcher/common/BackgroundThread;->sHandler:Lcom/miui/home/launcher/common/BackgroundThread$BackgroundHandler;

    :cond_0
    sget-object v1, Lcom/miui/home/launcher/common/BackgroundThread;->sHandler:Lcom/miui/home/launcher/common/BackgroundThread$BackgroundHandler;

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public static post(Ljava/lang/Runnable;)V
    .locals 1

    invoke-static {}, Lcom/miui/home/launcher/common/BackgroundThread;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public static postAtFrontOfQueue(Ljava/lang/Runnable;)V
    .locals 1

    invoke-static {}, Lcom/miui/home/launcher/common/BackgroundThread;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/os/Handler;->postAtFrontOfQueue(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public static postDelayed(Ljava/lang/Runnable;J)V
    .locals 1

    invoke-static {}, Lcom/miui/home/launcher/common/BackgroundThread;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, p0, p1, p2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public static removeCallbacks(Ljava/lang/Runnable;)V
    .locals 1

    invoke-static {}, Lcom/miui/home/launcher/common/BackgroundThread;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    return-void
.end method
