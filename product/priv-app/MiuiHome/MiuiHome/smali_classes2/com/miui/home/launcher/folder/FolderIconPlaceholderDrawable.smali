.class public final Lcom/miui/home/launcher/folder/FolderIconPlaceholderDrawable;
.super Landroid/graphics/drawable/Drawable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/home/launcher/folder/FolderIconPlaceholderDrawable$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/miui/home/launcher/folder/FolderIconPlaceholderDrawable$Companion;

# The value of this static final field might be set in the static constructor
.field private static final PLACEHOLDER_BACKGROUND_RADIUS:F = 0.21459228f

# The value of this static final field might be set in the static constructor
.field private static final PLACEHOLDER_TRANSPARENT_EDGE_SCALE:F = 0.037974f


# instance fields
.field private final LAYOUT_DEBUGABLE:Z

.field private final TAG:Ljava/lang/String;

.field private mBackgroundColor:I

.field private mBackgroundColorDark:I

.field private mBackgroundColorLight:I

.field private mBoundRect:Landroid/graphics/RectF;

.field private final mPaint:Landroid/graphics/Paint;

.field private mRadius:F


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/miui/home/launcher/folder/FolderIconPlaceholderDrawable$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/miui/home/launcher/folder/FolderIconPlaceholderDrawable$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/miui/home/launcher/folder/FolderIconPlaceholderDrawable;->Companion:Lcom/miui/home/launcher/folder/FolderIconPlaceholderDrawable$Companion;

    const v0, 0x3d1b8aa0    # 0.037974f

    sput v0, Lcom/miui/home/launcher/folder/FolderIconPlaceholderDrawable;->PLACEHOLDER_TRANSPARENT_EDGE_SCALE:F

    const v0, 0x3e5bbe14

    sput v0, Lcom/miui/home/launcher/folder/FolderIconPlaceholderDrawable;->PLACEHOLDER_BACKGROUND_RADIUS:F

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    const-string v0, "#4DFFFFFF"

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/miui/home/launcher/folder/FolderIconPlaceholderDrawable;->mBackgroundColor:I

    const-string v0, "#4DFFFFFF"

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/miui/home/launcher/folder/FolderIconPlaceholderDrawable;->mBackgroundColorDark:I

    const-string v0, "#F0000000"

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/miui/home/launcher/folder/FolderIconPlaceholderDrawable;->mBackgroundColorLight:I

    const/high16 v0, 0x41000000    # 8.0f

    iput v0, p0, Lcom/miui/home/launcher/folder/FolderIconPlaceholderDrawable;->mRadius:F

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/miui/home/launcher/folder/FolderIconPlaceholderDrawable;->mPaint:Landroid/graphics/Paint;

    const-string v0, "FolderIconPlaceholderDrawable"

    iput-object v0, p0, Lcom/miui/home/launcher/folder/FolderIconPlaceholderDrawable;->TAG:Ljava/lang/String;

    const v0, 0x7f0600f8

    invoke-static {p1, v0}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    iput v0, p0, Lcom/miui/home/launcher/folder/FolderIconPlaceholderDrawable;->mBackgroundColorDark:I

    const v0, 0x7f0600f9

    invoke-static {p1, v0}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    iput v0, p0, Lcom/miui/home/launcher/folder/FolderIconPlaceholderDrawable;->mBackgroundColorLight:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const v0, 0x7f07016e

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result p1

    iput p1, p0, Lcom/miui/home/launcher/folder/FolderIconPlaceholderDrawable;->mRadius:F

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderIconPlaceholderDrawable;->initPaint()V

    return-void
.end method

.method private final getRectFSafe()Landroid/graphics/RectF;
    .locals 2

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIconPlaceholderDrawable;->mBoundRect:Landroid/graphics/RectF;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Landroid/graphics/RectF;

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderIconPlaceholderDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    :goto_0
    return-object v0
.end method

.method private final setCorners(F)V
    .locals 0

    iput p1, p0, Lcom/miui/home/launcher/folder/FolderIconPlaceholderDrawable;->mRadius:F

    return-void
.end method

.method private final setRectF(Landroid/graphics/RectF;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/home/launcher/folder/FolderIconPlaceholderDrawable;->mBoundRect:Landroid/graphics/RectF;

    return-void
.end method


# virtual methods
.method public final calcDrawableParams()V
    .locals 5

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderIconPlaceholderDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderIconPlaceholderDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    int-to-float v0, v0

    sget v2, Lcom/miui/home/launcher/folder/FolderIconPlaceholderDrawable;->PLACEHOLDER_TRANSPARENT_EDGE_SCALE:F

    mul-float/2addr v2, v0

    sget v3, Lcom/miui/home/launcher/folder/FolderIconPlaceholderDrawable;->PLACEHOLDER_BACKGROUND_RADIUS:F

    mul-float/2addr v3, v0

    new-instance v4, Landroid/graphics/RectF;

    sub-float/2addr v0, v2

    int-to-float v1, v1

    sub-float/2addr v1, v2

    invoke-direct {v4, v2, v2, v0, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-direct {p0, v4}, Lcom/miui/home/launcher/folder/FolderIconPlaceholderDrawable;->setRectF(Landroid/graphics/RectF;)V

    invoke-direct {p0, v3}, Lcom/miui/home/launcher/folder/FolderIconPlaceholderDrawable;->setCorners(F)V

    return-void
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 4

    const-string v0, "canvas"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/miui/home/launcher/folder/FolderIconPlaceholderDrawable;->getRectFSafe()Landroid/graphics/RectF;

    move-result-object v0

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderIconPlaceholderDrawable;->initPaint()V

    iget v1, p0, Lcom/miui/home/launcher/folder/FolderIconPlaceholderDrawable;->mRadius:F

    iget-object v2, p0, Lcom/miui/home/launcher/folder/FolderIconPlaceholderDrawable;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v1, v2}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    iget-boolean p1, p0, Lcom/miui/home/launcher/folder/FolderIconPlaceholderDrawable;->LAYOUT_DEBUGABLE:Z

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/miui/home/launcher/folder/FolderIconPlaceholderDrawable;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "draw rect["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, v0, Landroid/graphics/RectF;->left:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const/16 v2, 0x2c

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget v3, v0, Landroid/graphics/RectF;->top:F

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget v3, v0, Landroid/graphics/RectF;->right:F

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const/16 v0, 0x5d

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method public getOpacity()I
    .locals 1

    const/4 v0, -0x3

    return v0
.end method

.method public final initPaint()V
    .locals 2

    invoke-static {}, Lcom/miui/home/launcher/WallpaperUtils;->hasAppliedLightWallpaper()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/miui/home/launcher/folder/FolderIconPlaceholderDrawable;->mBackgroundColorLight:I

    goto :goto_0

    :cond_0
    iget v0, p0, Lcom/miui/home/launcher/folder/FolderIconPlaceholderDrawable;->mBackgroundColorDark:I

    :goto_0
    iput v0, p0, Lcom/miui/home/launcher/folder/FolderIconPlaceholderDrawable;->mBackgroundColor:I

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIconPlaceholderDrawable;->mPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/miui/home/launcher/folder/FolderIconPlaceholderDrawable;->mBackgroundColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIconPlaceholderDrawable;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIconPlaceholderDrawable;->mPaint:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    return-void
.end method

.method protected onBoundsChange(Landroid/graphics/Rect;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/graphics/drawable/Drawable;->onBoundsChange(Landroid/graphics/Rect;)V

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderIconPlaceholderDrawable;->calcDrawableParams()V

    return-void
.end method

.method public setAlpha(I)V
    .locals 0

    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIconPlaceholderDrawable;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderIconPlaceholderDrawable;->invalidateSelf()V

    return-void
.end method
