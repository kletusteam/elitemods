.class public Lcom/miui/home/recents/views/ViewPool;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/home/recents/views/ViewPool$ViewPoolConsumer;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        "T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mPool:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList<",
            "TV;>;"
        }
    .end annotation
.end field

.field private mViewCreator:Lcom/miui/home/recents/views/ViewPool$ViewPoolConsumer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/miui/home/recents/views/ViewPool$ViewPoolConsumer<",
            "TV;TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/miui/home/recents/views/ViewPool$ViewPoolConsumer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/miui/home/recents/views/ViewPool$ViewPoolConsumer<",
            "TV;TT;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/miui/home/recents/views/ViewPool;->mPool:Ljava/util/LinkedList;

    iput-object p1, p0, Lcom/miui/home/recents/views/ViewPool;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/miui/home/recents/views/ViewPool;->mViewCreator:Lcom/miui/home/recents/views/ViewPool$ViewPoolConsumer;

    return-void
.end method


# virtual methods
.method getViews()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "TV;>;"
        }
    .end annotation

    goto/32 :goto_1

    nop

    :goto_0
    return-object v0

    :goto_1
    iget-object v0, p0, Lcom/miui/home/recents/views/ViewPool;->mPool:Ljava/util/LinkedList;

    goto/32 :goto_0

    nop
.end method

.method pickUpViewFromPool(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;TT;)TV;"
        }
    .end annotation

    goto/32 :goto_1a

    nop

    :goto_0
    if-nez v3, :cond_0

    goto/32 :goto_18

    :cond_0
    goto/32 :goto_1b

    nop

    :goto_1
    goto :goto_8

    :goto_2
    goto/32 :goto_7

    nop

    :goto_3
    const/4 v1, 0x1

    goto/32 :goto_1d

    nop

    :goto_4
    invoke-virtual {p1}, Ljava/util/LinkedList;->pop()Ljava/lang/Object;

    move-result-object p1

    :goto_5
    goto/32 :goto_12

    nop

    :goto_6
    invoke-interface {p1, v0}, Lcom/miui/home/recents/views/ViewPool$ViewPoolConsumer;->createView(Landroid/content/Context;)Ljava/lang/Object;

    move-result-object p1

    goto/32 :goto_3

    nop

    :goto_7
    const/4 p1, 0x0

    :goto_8
    goto/32 :goto_1c

    nop

    :goto_9
    if-nez v2, :cond_1

    goto/32 :goto_2

    :cond_1
    goto/32 :goto_a

    nop

    :goto_a
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_d

    nop

    :goto_b
    return-object p1

    :goto_c
    invoke-interface {v3, v2, p1}, Lcom/miui/home/recents/views/ViewPool$ViewPoolConsumer;->hasPreferredData(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    goto/32 :goto_0

    nop

    :goto_d
    iget-object v3, p0, Lcom/miui/home/recents/views/ViewPool;->mViewCreator:Lcom/miui/home/recents/views/ViewPool$ViewPoolConsumer;

    goto/32 :goto_c

    nop

    :goto_e
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    goto/32 :goto_9

    nop

    :goto_f
    move-object p1, v2

    goto/32 :goto_1

    nop

    :goto_10
    invoke-interface {v0, p1, p2, v1}, Lcom/miui/home/recents/views/ViewPool$ViewPoolConsumer;->onPickUpViewFromPool(Ljava/lang/Object;Ljava/lang/Object;Z)V

    goto/32 :goto_b

    nop

    :goto_11
    iget-object v0, p0, Lcom/miui/home/recents/views/ViewPool;->mPool:Ljava/util/LinkedList;

    goto/32 :goto_17

    nop

    :goto_12
    iget-object v0, p0, Lcom/miui/home/recents/views/ViewPool;->mViewCreator:Lcom/miui/home/recents/views/ViewPool$ViewPoolConsumer;

    goto/32 :goto_10

    nop

    :goto_13
    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    goto/32 :goto_19

    nop

    :goto_14
    if-nez v0, :cond_2

    goto/32 :goto_1e

    :cond_2
    goto/32 :goto_16

    nop

    :goto_15
    iget-object p1, p0, Lcom/miui/home/recents/views/ViewPool;->mPool:Ljava/util/LinkedList;

    goto/32 :goto_4

    nop

    :goto_16
    iget-object p1, p0, Lcom/miui/home/recents/views/ViewPool;->mViewCreator:Lcom/miui/home/recents/views/ViewPool$ViewPoolConsumer;

    goto/32 :goto_1f

    nop

    :goto_17
    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_18
    goto/32 :goto_e

    nop

    :goto_19
    const/4 v1, 0x0

    goto/32 :goto_14

    nop

    :goto_1a
    iget-object v0, p0, Lcom/miui/home/recents/views/ViewPool;->mPool:Ljava/util/LinkedList;

    goto/32 :goto_13

    nop

    :goto_1b
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    goto/32 :goto_f

    nop

    :goto_1c
    if-eqz p1, :cond_3

    goto/32 :goto_5

    :cond_3
    goto/32 :goto_15

    nop

    :goto_1d
    goto/16 :goto_5

    :goto_1e
    goto/32 :goto_11

    nop

    :goto_1f
    iget-object v0, p0, Lcom/miui/home/recents/views/ViewPool;->mContext:Landroid/content/Context;

    goto/32 :goto_6

    nop
.end method

.method returnViewToPool(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    goto/32 :goto_3

    nop

    :goto_0
    return-void

    :goto_1
    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->push(Ljava/lang/Object;)V

    goto/32 :goto_0

    nop

    :goto_2
    invoke-interface {v0, p1}, Lcom/miui/home/recents/views/ViewPool$ViewPoolConsumer;->onReturnViewToPool(Ljava/lang/Object;)V

    goto/32 :goto_4

    nop

    :goto_3
    iget-object v0, p0, Lcom/miui/home/recents/views/ViewPool;->mViewCreator:Lcom/miui/home/recents/views/ViewPool$ViewPoolConsumer;

    goto/32 :goto_2

    nop

    :goto_4
    iget-object v0, p0, Lcom/miui/home/recents/views/ViewPool;->mPool:Ljava/util/LinkedList;

    goto/32 :goto_1

    nop
.end method
