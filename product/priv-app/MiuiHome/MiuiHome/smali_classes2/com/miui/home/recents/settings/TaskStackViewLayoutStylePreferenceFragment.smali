.class public Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment;
.super Lmiuix/preference/PreferenceFragment;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private isVertical:Z

.field private mEntries:[Ljava/lang/CharSequence;

.field private mPrefs:Lmiuix/preference/DropDownPreference;

.field private mStyleContainer:Lcom/miui/home/recents/settings/TaskStackViewLayoutStyleContainerPreference;

.field private mValues:[Ljava/lang/CharSequence;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lmiuix/preference/PreferenceFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment;->isVertical:Z

    return v0
.end method

.method static synthetic access$002(Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment;->isVertical:Z

    return p1
.end method

.method static synthetic access$100(Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment;->updateValuesAndEntries()V

    return-void
.end method

.method private getArray(Ljava/lang/String;)I
    .locals 3

    invoke-virtual {p0}, Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "array"

    invoke-virtual {p0}, Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static getTaskStackViewLayoutStyle(Landroid/content/Context;)I
    .locals 3

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "task_stack_view_layout_style"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method private updateValuesAndEntries()V
    .locals 6

    iget-boolean v4, p0, Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment;->isVertical:Z

    if-eqz v4, :cond_0

    const/4 v0, 0x2

    :goto_0
    new-array v1, v0, [Ljava/lang/CharSequence;

    new-array v2, v0, [Ljava/lang/CharSequence;

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v0, :cond_1

    iget-object v4, p0, Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment;->mValues:[Ljava/lang/CharSequence;

    aget-object v4, v4, v3

    aput-object v4, v1, v3

    iget-object v4, p0, Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment;->mEntries:[Ljava/lang/CharSequence;

    aget-object v4, v4, v3

    aput-object v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_0
    const/4 v0, 0x5

    goto :goto_0

    :cond_1
    iget-object v4, p0, Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment;->mPrefs:Lmiuix/preference/DropDownPreference;

    invoke-virtual {v4, v2}, Lmiuix/preference/DropDownPreference;->setEntries([Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment;->mPrefs:Lmiuix/preference/DropDownPreference;

    invoke-virtual {v4, v1}, Lmiuix/preference/DropDownPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    iget-object v5, p0, Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment;->mPrefs:Lmiuix/preference/DropDownPreference;

    iget-boolean v4, p0, Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment;->isVertical:Z

    if-nez v4, :cond_2

    const-string v4, "recent_horizontal_style"

    :goto_2
    invoke-static {v4}, Landroid/preference/SettingsEliteHelper;->getIntofSettings(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v5, v4}, Lmiuix/preference/DropDownPreference;->setValueIndex(I)V

    return-void

    :cond_2
    const-string v4, "recent_vertical_style"

    goto :goto_2
.end method


# virtual methods
.method public onCreatePreferences(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 4

    invoke-virtual {p0}, Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "task_stack_view_layout_style_fragment"

    const-string v2, "xml"

    invoke-virtual {p0}, Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0, p2}, Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment;->setPreferencesFromResource(ILjava/lang/String;)V

    const-string v0, "animation_style"

    invoke-virtual {p0, v0}, Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Lmiuix/preference/DropDownPreference;

    iput-object v0, p0, Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment;->mPrefs:Lmiuix/preference/DropDownPreference;

    const-string v0, "task_stack_view_layout_style_container_preference"

    invoke-virtual {p0, v0}, Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/miui/home/recents/settings/TaskStackViewLayoutStyleContainerPreference;

    iput-object v0, p0, Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment;->mStyleContainer:Lcom/miui/home/recents/settings/TaskStackViewLayoutStyleContainerPreference;

    sget-boolean v0, Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment;->mStyleContainer:Lcom/miui/home/recents/settings/TaskStackViewLayoutStyleContainerPreference;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment;->mStyleContainer:Lcom/miui/home/recents/settings/TaskStackViewLayoutStyleContainerPreference;

    new-instance v1, Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment$1;

    invoke-direct {v1, p0}, Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment$1;-><init>(Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment;)V

    invoke-virtual {v0, v1}, Lcom/miui/home/recents/settings/TaskStackViewLayoutStyleContainerPreference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    invoke-virtual {p0}, Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "recent_anim_style_entries"

    invoke-direct {p0, v1}, Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment;->getArray(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment;->mEntries:[Ljava/lang/CharSequence;

    invoke-virtual {p0}, Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "recent_anim_style_values"

    invoke-direct {p0, v1}, Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment;->getArray(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment;->mValues:[Ljava/lang/CharSequence;

    invoke-virtual {p0}, Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment;->getTaskStackViewLayoutStyle(Landroid/content/Context;)I

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment;->isVertical:Z

    iget-object v0, p0, Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment;->mPrefs:Lmiuix/preference/DropDownPreference;

    new-instance v1, Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment$2;

    invoke-direct {v1, p0}, Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment$2;-><init>(Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment;)V

    invoke-virtual {v0, v1}, Lmiuix/preference/DropDownPreference;->setOnPreferenceChangeListener(Landroidx/preference/Preference$OnPreferenceChangeListener;)V

    invoke-direct {p0}, Lcom/miui/home/recents/settings/TaskStackViewLayoutStylePreferenceFragment;->updateValuesAndEntries()V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
