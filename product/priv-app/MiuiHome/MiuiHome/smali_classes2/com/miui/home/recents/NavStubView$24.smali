.class Lcom/miui/home/recents/NavStubView$24;
.super Landroid/animation/AnimatorListenerAdapter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/home/recents/NavStubView;->startAppToWorldCirculate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/home/recents/NavStubView;


# direct methods
.method constructor <init>(Lcom/miui/home/recents/NavStubView;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/home/recents/NavStubView$24;->this$0:Lcom/miui/home/recents/NavStubView;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 2

    sget-object p1, Lcom/miui/home/recents/NavStubView;->TAG:Ljava/lang/String;

    const-string v0, "NavStubView::startAppToWorldCirculate = onAnimationCancel"

    invoke-static {p1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object p1, p0, Lcom/miui/home/recents/NavStubView$24;->this$0:Lcom/miui/home/recents/NavStubView;

    new-instance v0, Lcom/miui/home/recents/NavStubView$24$2;

    invoke-direct {v0, p0}, Lcom/miui/home/recents/NavStubView$24$2;-><init>(Lcom/miui/home/recents/NavStubView$24;)V

    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, Lcom/miui/home/recents/NavStubView;->finish(ZLjava/lang/Runnable;)V

    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2

    iget-object p1, p0, Lcom/miui/home/recents/NavStubView$24;->this$0:Lcom/miui/home/recents/NavStubView;

    new-instance v0, Lcom/miui/home/recents/NavStubView$24$1;

    invoke-direct {v0, p0}, Lcom/miui/home/recents/NavStubView$24$1;-><init>(Lcom/miui/home/recents/NavStubView$24;)V

    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, Lcom/miui/home/recents/NavStubView;->finish(ZLjava/lang/Runnable;)V

    invoke-static {}, Lcom/miui/home/recents/util/TraceUtils;->endSection()V

    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0

    const-string p1, "appToWorldCirculateAim"

    invoke-static {p1}, Lcom/miui/home/recents/util/TraceUtils;->beginSection(Ljava/lang/String;)V

    return-void
.end method
