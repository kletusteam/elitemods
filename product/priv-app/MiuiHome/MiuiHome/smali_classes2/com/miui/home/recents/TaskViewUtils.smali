.class public final Lcom/miui/home/recents/TaskViewUtils;
.super Ljava/lang/Object;


# direct methods
.method private static createSpringAnim(Lcom/miui/home/recents/views/RecentsView;Lcom/miui/home/recents/views/TaskView;Lcom/miui/home/recents/util/ClipAnimationHelper$TransformParams;Lcom/miui/home/recents/util/ClipAnimationHelper;Lcom/miui/home/recents/util/RemoteAnimationTargetSet;Landroid/graphics/RectF;Landroid/graphics/RectF;FFZI)Lcom/miui/home/recents/util/RectFSpringAnim;
    .locals 20

    new-instance v7, Lcom/miui/home/recents/util/RectFSpringAnim;

    const v0, 0x3dcccccd    # 0.1f

    move-object/from16 v15, p5

    move-object/from16 v2, p6

    invoke-static {v15, v2, v0}, Lcom/miui/home/recents/util/RectUtil;->generateLeastWrapBoundWithRatio(Landroid/graphics/RectF;Landroid/graphics/RectF;F)Landroid/graphics/RectF;

    move-result-object v1

    const/4 v5, 0x0

    const/high16 v6, 0x3fa00000    # 1.25f

    move-object v0, v7

    move/from16 v3, p7

    move/from16 v4, p8

    invoke-direct/range {v0 .. v6}, Lcom/miui/home/recents/util/RectFSpringAnim;-><init>(Landroid/graphics/RectF;Landroid/graphics/RectF;FFFF)V

    sget-object v0, Lcom/miui/home/recents/util/RectFSpringAnim$AnimType;->OPEN_FROM_RECENTS:Lcom/miui/home/recents/util/RectFSpringAnim$AnimType;

    invoke-virtual {v7, v0}, Lcom/miui/home/recents/util/RectFSpringAnim;->setAnimParamByType(Lcom/miui/home/recents/util/RectFSpringAnim$AnimType;)V

    invoke-virtual/range {p0 .. p0}, Lcom/miui/home/recents/views/RecentsView;->getHeight()I

    move-result v0

    invoke-virtual/range {p0 .. p0}, Lcom/miui/home/recents/views/RecentsView;->getWidth()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Lcom/miui/home/launcher/RecentsAndFSGestureUtils;->isUseTaskStackLayoutStyleHorizontalAnim(Z)Z

    move-result v17

    invoke-static/range {p10 .. p10}, Lcom/miui/home/recents/TaskViewUtils;->isVerticalClip(I)Z

    move-result v10

    invoke-static/range {p10 .. p10}, Lcom/miui/home/recents/TaskViewUtils;->isClipFromLeftOrTop(I)Z

    move-result v11

    new-instance v0, Lcom/miui/home/recents/-$$Lambda$TaskViewUtils$dxH5vDCokQcmCCmoFRcEIc4D_pM;

    move-object v8, v0

    move-object/from16 v9, p2

    move-object/from16 v12, p3

    move-object/from16 v13, p4

    move/from16 v14, p10

    move/from16 v15, p9

    move-object/from16 v16, p1

    move-object/from16 v18, p0

    move-object/from16 v19, p5

    invoke-direct/range {v8 .. v19}, Lcom/miui/home/recents/-$$Lambda$TaskViewUtils$dxH5vDCokQcmCCmoFRcEIc4D_pM;-><init>(Lcom/miui/home/recents/util/ClipAnimationHelper$TransformParams;ZZLcom/miui/home/recents/util/ClipAnimationHelper;Lcom/miui/home/recents/util/RemoteAnimationTargetSet;IZLcom/miui/home/recents/views/TaskView;ZLcom/miui/home/recents/views/RecentsView;Landroid/graphics/RectF;)V

    invoke-virtual {v7, v0}, Lcom/miui/home/recents/util/RectFSpringAnim;->addOnUpdateListener(Lcom/miui/home/recents/util/RectFSpringAnim$OnUpdateListener;)V

    new-instance v0, Lcom/miui/home/recents/TaskViewUtils$1;

    move-object/from16 v1, p1

    move-object/from16 v2, p4

    move/from16 v3, p9

    invoke-direct {v0, v2, v3, v1}, Lcom/miui/home/recents/TaskViewUtils$1;-><init>(Lcom/miui/home/recents/util/RemoteAnimationTargetSet;ZLcom/miui/home/recents/views/TaskView;)V

    invoke-virtual {v7, v0}, Lcom/miui/home/recents/util/RectFSpringAnim;->addAnimatorListener(Landroid/animation/Animator$AnimatorListener;)V

    return-object v7
.end method

.method public static findTaskViewToLaunch(Lcom/miui/home/launcher/Launcher;Landroid/view/View;[Lcom/android/systemui/shared/recents/system/RemoteAnimationTargetCompat;)Lcom/miui/home/recents/views/TaskView;
    .locals 5

    invoke-virtual {p0}, Lcom/miui/home/launcher/Launcher;->getOverviewPanel()Landroid/view/View;

    move-result-object p0

    check-cast p0, Lcom/miui/home/recents/views/RecentsView;

    instance-of v0, p1, Lcom/miui/home/recents/views/TaskView;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/miui/home/recents/views/TaskView;

    return-object p1

    :cond_0
    const/4 p1, 0x0

    if-nez p2, :cond_1

    return-object p1

    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    array-length v1, p2

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_3

    aget-object v3, p2, v2

    iget v4, v3, Lcom/android/systemui/shared/recents/system/RemoteAnimationTargetCompat;->mode:I

    if-nez v4, :cond_2

    iget v3, v3, Lcom/android/systemui/shared/recents/system/RemoteAnimationTargetCompat;->taskId:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result p2

    if-eqz p2, :cond_4

    return-object p1

    :cond_4
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p2

    move-object v0, p1

    :cond_5
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/miui/home/recents/views/RecentsView;->getTaskView(I)Lcom/miui/home/recents/views/TaskView;

    move-result-object v0

    if-eqz v0, :cond_5

    :cond_6
    if-nez v0, :cond_7

    return-object p1

    :cond_7
    return-object v0
.end method

.method public static getEndRadius(Lcom/miui/home/recents/util/RemoteAnimationTargetSet;)I
    .locals 1

    invoke-static {}, Lcom/miui/home/launcher/DeviceConfig;->isInMultiWindowMode()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p0, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/miui/home/recents/util/RemoteAnimationTargetSet;->hasMultiTask()Z

    move-result p0

    if-eqz p0, :cond_1

    const/16 p0, 0x16

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/miui/home/recents/util/WindowCornerRadiusUtil;->getCornerRadius()I

    move-result p0

    :goto_0
    return p0
.end method

.method public static getEndRectF(Lcom/miui/home/recents/util/ClipAnimationHelper;I)Landroid/graphics/RectF;
    .locals 1

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/miui/home/recents/util/ClipAnimationHelper;->getTargetRect()Landroid/graphics/RectF;

    move-result-object p1

    invoke-virtual {p0}, Lcom/miui/home/recents/util/ClipAnimationHelper;->getSourceStackBounds()Landroid/graphics/Rect;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Rect;->left:I

    neg-int v0, v0

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/miui/home/recents/util/ClipAnimationHelper;->getSourceStackBounds()Landroid/graphics/Rect;

    move-result-object p0

    iget p0, p0, Landroid/graphics/Rect;->top:I

    neg-int p0, p0

    int-to-float p0, p0

    invoke-virtual {p1, v0, p0}, Landroid/graphics/RectF;->offset(FF)V

    goto :goto_0

    :cond_0
    new-instance p1, Landroid/graphics/RectF;

    invoke-virtual {p0}, Lcom/miui/home/recents/util/ClipAnimationHelper;->getSplitSingleSourceStackBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-direct {p1, v0}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    invoke-virtual {p0}, Lcom/miui/home/recents/util/ClipAnimationHelper;->getSplitSingleSourceStackBounds()Landroid/graphics/Rect;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Rect;->left:I

    neg-int v0, v0

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/miui/home/recents/util/ClipAnimationHelper;->getSplitSingleSourceStackBounds()Landroid/graphics/Rect;

    move-result-object p0

    iget p0, p0, Landroid/graphics/Rect;->top:I

    neg-int p0, p0

    int-to-float p0, p0

    invoke-virtual {p1, v0, p0}, Landroid/graphics/RectF;->offset(FF)V

    :goto_0
    return-object p1
.end method

.method public static getRecentsWindowAnimatorNew(Lcom/miui/home/recents/views/TaskView;ZI[Lcom/android/systemui/shared/recents/system/RemoteAnimationTargetCompat;[Lcom/android/systemui/shared/recents/system/RemoteAnimationTargetCompat;[Lcom/android/systemui/shared/recents/system/RemoteAnimationTargetCompat;Lcom/miui/home/recents/views/RecentsView;II)Lcom/miui/home/recents/util/RectFSpringAnim;
    .locals 15

    move-object/from16 v0, p3

    move/from16 v10, p8

    new-instance v6, Lcom/miui/home/recents/util/ClipAnimationHelper$TransformParams;

    invoke-direct {v6}, Lcom/miui/home/recents/util/ClipAnimationHelper$TransformParams;-><init>()V

    new-instance v7, Lcom/miui/home/recents/util/RemoteAnimationTargetSet;

    const/4 v1, 0x0

    invoke-direct {v7, v0, v1}, Lcom/miui/home/recents/util/RemoteAnimationTargetSet;-><init>([Lcom/android/systemui/shared/recents/system/RemoteAnimationTargetCompat;I)V

    const/4 v2, 0x1

    const/4 v11, 0x0

    move/from16 v4, p7

    if-eq v4, v2, :cond_0

    if-nez p0, :cond_0

    const-string v0, "TaskViewUtils"

    const-string v1, "getRecentsWindowAnimatorNew error:taskView == null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-object v11

    :cond_0
    invoke-static {}, Lcom/miui/home/launcher/Application;->getLauncher()Lcom/miui/home/launcher/Launcher;

    move-result-object v3

    if-eqz v3, :cond_5

    iget-object v5, v7, Lcom/miui/home/recents/util/RemoteAnimationTargetSet;->apps:[Lcom/android/systemui/shared/recents/system/RemoteAnimationTargetCompat;

    array-length v5, v5

    if-nez v5, :cond_1

    goto/16 :goto_1

    :cond_1
    new-instance v8, Lcom/miui/home/recents/util/ClipAnimationHelper;

    invoke-direct {v8}, Lcom/miui/home/recents/util/ClipAnimationHelper;-><init>()V

    invoke-virtual {v8, v2}, Lcom/miui/home/recents/util/ClipAnimationHelper;->prepareAnimation(Z)V

    invoke-virtual {v3}, Lcom/miui/home/launcher/Launcher;->getRootViewRect()Landroid/graphics/Rect;

    move-result-object v5

    iget-object v9, v7, Lcom/miui/home/recents/util/RemoteAnimationTargetSet;->apps:[Lcom/android/systemui/shared/recents/system/RemoteAnimationTargetCompat;

    aget-object v9, v9, v1

    invoke-virtual {v8, v9}, Lcom/miui/home/recents/util/ClipAnimationHelper;->updateSourceStack(Lcom/android/systemui/shared/recents/system/RemoteAnimationTargetCompat;)V

    invoke-virtual {v8, v7}, Lcom/miui/home/recents/util/ClipAnimationHelper;->updateSourceStackBounds(Lcom/miui/home/recents/util/RemoteAnimationTargetSet;)V

    invoke-virtual {v8, v5}, Lcom/miui/home/recents/util/ClipAnimationHelper;->updateHomeStack(Landroid/graphics/Rect;)V

    invoke-virtual {v8}, Lcom/miui/home/recents/util/ClipAnimationHelper;->getSourceStackBounds()Landroid/graphics/Rect;

    move-result-object v5

    invoke-virtual {v8, v5}, Lcom/miui/home/recents/util/ClipAnimationHelper;->updateTargetRect(Landroid/graphics/Rect;)V

    if-ne v10, v2, :cond_2

    move-object v9, v7

    goto :goto_0

    :cond_2
    new-instance v2, Lcom/miui/home/recents/util/RemoteAnimationTargetSet;

    invoke-direct {v2, v0, v1, v10}, Lcom/miui/home/recents/util/RemoteAnimationTargetSet;-><init>([Lcom/android/systemui/shared/recents/system/RemoteAnimationTargetCompat;II)V

    move-object v9, v2

    :goto_0
    iget-object v0, v9, Lcom/miui/home/recents/util/RemoteAnimationTargetSet;->unfilteredApps:[Lcom/android/systemui/shared/recents/system/RemoteAnimationTargetCompat;

    if-nez v0, :cond_3

    const-string v0, "TaskViewUtils"

    const-string v1, "getRecentsWindowAnimatorNew: filterOpeningTargets.unfilteredApps == null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-object v11

    :cond_3
    invoke-virtual {v8, v9, v10}, Lcom/miui/home/recents/util/ClipAnimationHelper;->updateSplitSourceStackBounds(Lcom/miui/home/recents/util/RemoteAnimationTargetSet;I)V

    invoke-static {v8, v10}, Lcom/miui/home/recents/TaskViewUtils;->getEndRectF(Lcom/miui/home/recents/util/ClipAnimationHelper;I)Landroid/graphics/RectF;

    move-result-object v12

    move-object v0, v3

    move-object/from16 v1, p6

    move-object v2, p0

    move-object v3, v12

    move/from16 v4, p7

    move/from16 v5, p8

    invoke-static/range {v0 .. v5}, Lcom/miui/home/recents/TaskViewUtils;->initStartRectF(Lcom/miui/home/launcher/Launcher;Lcom/miui/home/recents/views/RecentsView;Lcom/miui/home/recents/views/TaskView;Landroid/graphics/RectF;II)Landroid/graphics/RectF;

    move-result-object v5

    if-nez v5, :cond_4

    const-string v0, "TaskViewUtils"

    const-string v1, "getRecentsWindowAnimatorNew: startRectF is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-object v11

    :cond_4
    invoke-static {}, Lcom/miui/home/recents/util/WindowCornerRadiusUtil;->getTaskViewCornerRadius()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x3f800000    # 1.0f

    mul-float/2addr v0, v1

    invoke-virtual {v12}, Landroid/graphics/RectF;->width()F

    move-result v1

    mul-float/2addr v0, v1

    invoke-virtual {v5}, Landroid/graphics/RectF;->width()F

    move-result v1

    div-float v13, v0, v1

    invoke-static {v7}, Lcom/miui/home/recents/TaskViewUtils;->getEndRadius(Lcom/miui/home/recents/util/RemoteAnimationTargetSet;)I

    move-result v0

    int-to-float v14, v0

    move-object/from16 v0, p6

    move-object v1, p0

    move-object v2, v6

    move-object v3, v8

    move-object v4, v9

    move-object v6, v12

    move v7, v13

    move v8, v14

    move/from16 v9, p1

    move/from16 v10, p8

    invoke-static/range {v0 .. v10}, Lcom/miui/home/recents/TaskViewUtils;->createSpringAnim(Lcom/miui/home/recents/views/RecentsView;Lcom/miui/home/recents/views/TaskView;Lcom/miui/home/recents/util/ClipAnimationHelper$TransformParams;Lcom/miui/home/recents/util/ClipAnimationHelper;Lcom/miui/home/recents/util/RemoteAnimationTargetSet;Landroid/graphics/RectF;Landroid/graphics/RectF;FFZI)Lcom/miui/home/recents/util/RectFSpringAnim;

    move-result-object v0

    invoke-static {}, Lcom/miui/home/recents/breakableAnim/IconAndTaskBreakableAnimManager;->getInstance()Lcom/miui/home/recents/breakableAnim/IconAndTaskBreakableAnimManager;

    move-result-object v1

    invoke-virtual {v1, v0, v11}, Lcom/miui/home/recents/breakableAnim/IconAndTaskBreakableAnimManager;->setupAnimAndBreakLast(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0

    :cond_5
    :goto_1
    return-object v11
.end method

.method private static getTaskViewStartRectF(Lcom/miui/home/launcher/Launcher;Lcom/miui/home/recents/views/RecentsView;Lcom/miui/home/recents/views/TaskView;Landroid/graphics/RectF;)Landroid/graphics/RectF;
    .locals 4

    invoke-virtual {p1}, Lcom/miui/home/recents/views/RecentsView;->getTaskStackView()Lcom/miui/home/recents/views/TaskStackView;

    move-result-object p1

    invoke-virtual {p2}, Lcom/miui/home/recents/views/TaskView;->getTask()Lcom/android/systemui/shared/recents/model/Task;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/miui/home/recents/views/TaskStackView;->getTaskViewWithoutHeaderRectF(Lcom/android/systemui/shared/recents/model/Task;)Landroid/graphics/RectF;

    move-result-object p1

    const/4 p2, 0x0

    if-nez p1, :cond_0

    const-string p0, "TaskViewUtils"

    const-string p1, "getTaskViewStartRectF: startRectF is null"

    invoke-static {p0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-object p2

    :cond_0
    invoke-static {}, Lcom/miui/home/launcher/DeviceConfig;->isKeepRecentsViewPortrait()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/miui/home/launcher/Launcher;->getRecentsContainer()Lcom/miui/home/recents/views/RecentsContainer;

    move-result-object p0

    invoke-virtual {p0}, Lcom/miui/home/recents/views/RecentsContainer;->getRecentsRotation()I

    move-result p0

    const/4 p2, 0x0

    invoke-static {p0, p2, p1}, Lcom/miui/home/launcher/util/CoordinateTransforms;->transformCoordinate(IILandroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object p0

    invoke-virtual {p1, p0}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/miui/home/launcher/DeviceConfig;->getLastRotation()I

    move-result p0

    invoke-static {}, Lcom/miui/home/launcher/DeviceConfig;->getRotation()I

    move-result v0

    if-eq p0, v0, :cond_2

    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    iget v2, p3, Landroid/graphics/RectF;->right:F

    iget v3, p3, Landroid/graphics/RectF;->left:F

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    iget v3, p3, Landroid/graphics/RectF;->bottom:F

    iget p3, p3, Landroid/graphics/RectF;->top:F

    sub-float/2addr v3, p3

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result p3

    invoke-static {p0, v0, v2, p3, v1}, Lcom/miui/home/launcher/util/CoordinateTransforms;->transformToRotation(IIIILandroid/graphics/Matrix;)V

    new-instance p0, Landroid/graphics/Rect;

    iget p3, p1, Landroid/graphics/RectF;->left:F

    invoke-static {p3}, Ljava/lang/Math;->round(F)I

    move-result p3

    iget v0, p1, Landroid/graphics/RectF;->top:F

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iget v2, p1, Landroid/graphics/RectF;->right:F

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    iget v3, p1, Landroid/graphics/RectF;->bottom:F

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-direct {p0, p3, v0, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-static {v1, p0, p2}, Lcom/miui/home/launcher/util/CoordinateTransforms;->transformRect(Landroid/graphics/Matrix;Landroid/graphics/Rect;Landroid/graphics/RectF;)V

    invoke-virtual {p1, p0}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    :cond_2
    :goto_0
    return-object p1
.end method

.method public static hideDockDivider([Lcom/android/systemui/shared/recents/system/RemoteAnimationTargetCompat;)V
    .locals 5

    if-eqz p0, :cond_1

    array-length v0, p0

    if-lez v0, :cond_1

    array-length v0, p0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    aget-object v2, p0, v1

    iget v3, v2, Lcom/android/systemui/shared/recents/system/RemoteAnimationTargetCompat;->windowType:I

    const/16 v4, 0x7f2

    if-ne v3, v4, :cond_0

    new-instance v3, Lcom/android/systemui/shared/recents/system/TransactionCompat;

    invoke-direct {v3}, Lcom/android/systemui/shared/recents/system/TransactionCompat;-><init>()V

    iget-object v2, v2, Lcom/android/systemui/shared/recents/system/RemoteAnimationTargetCompat;->leash:Lcom/android/systemui/shared/recents/system/SurfaceControlCompat;

    invoke-virtual {v3, v2}, Lcom/android/systemui/shared/recents/system/TransactionCompat;->hide(Lcom/android/systemui/shared/recents/system/SurfaceControlCompat;)Lcom/android/systemui/shared/recents/system/TransactionCompat;

    invoke-virtual {v3}, Lcom/android/systemui/shared/recents/system/TransactionCompat;->apply()V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private static initStartRectF(Lcom/miui/home/launcher/Launcher;Lcom/miui/home/recents/views/RecentsView;Lcom/miui/home/recents/views/TaskView;Landroid/graphics/RectF;II)Landroid/graphics/RectF;
    .locals 4

    const/4 v0, 0x1

    const/high16 v1, 0x3f000000    # 0.5f

    const/high16 v2, 0x42200000    # 40.0f

    const/4 v3, 0x2

    if-ne p5, v3, :cond_3

    invoke-static {}, Lcom/miui/home/launcher/common/Utilities;->isPadDevice()Z

    move-result p5

    if-nez p5, :cond_0

    invoke-static {}, Lcom/miui/home/launcher/Application;->getInstance()Lcom/miui/home/launcher/Application;

    move-result-object p5

    invoke-virtual {p5}, Lcom/miui/home/launcher/Application;->isInFoldLargeScreenMode()Z

    move-result p5

    if-eqz p5, :cond_2

    :cond_0
    invoke-static {}, Lcom/miui/home/recents/views/FloatingTaskView;->isFloatingTaskViewOnRight()Z

    move-result p5

    if-eqz p5, :cond_2

    if-ne p4, v0, :cond_1

    invoke-static {}, Lcom/miui/home/launcher/DeviceConfig;->getScreenWidth()I

    move-result p0

    int-to-float p0, p0

    const/high16 p1, 0x3e800000    # 0.25f

    mul-float/2addr p0, p1

    invoke-static {}, Lcom/miui/home/launcher/DeviceConfig;->getRealScreenHeight()I

    move-result p1

    int-to-float p1, p1

    mul-float/2addr p1, v1

    new-instance p2, Landroid/graphics/RectF;

    sub-float p3, p0, v2

    sub-float p4, p1, v2

    add-float/2addr p0, v2

    add-float/2addr p1, v2

    invoke-direct {p2, p3, p4, p0, p1}, Landroid/graphics/RectF;-><init>(FFFF)V

    return-object p2

    :cond_1
    invoke-static {p0, p1, p2, p3}, Lcom/miui/home/recents/TaskViewUtils;->getTaskViewStartRectF(Lcom/miui/home/launcher/Launcher;Lcom/miui/home/recents/views/RecentsView;Lcom/miui/home/recents/views/TaskView;Landroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object p0

    return-object p0

    :cond_2
    invoke-static {}, Lcom/miui/home/recents/views/FloatingTaskView;->getFloatingTaskViewRectF()Landroid/graphics/RectF;

    move-result-object p0

    return-object p0

    :cond_3
    const/4 v3, 0x3

    if-ne p5, v3, :cond_8

    invoke-static {}, Lcom/miui/home/launcher/common/Utilities;->isPadDevice()Z

    move-result p5

    const/high16 v3, 0x3f400000    # 0.75f

    if-nez p5, :cond_5

    invoke-static {}, Lcom/miui/home/launcher/Application;->getInstance()Lcom/miui/home/launcher/Application;

    move-result-object p5

    invoke-virtual {p5}, Lcom/miui/home/launcher/Application;->isInFoldLargeScreenMode()Z

    move-result p5

    if-eqz p5, :cond_4

    goto :goto_0

    :cond_4
    invoke-static {}, Lcom/miui/home/launcher/DeviceConfig;->getScreenShortSize()I

    move-result p0

    int-to-float p0, p0

    mul-float/2addr p0, v1

    invoke-static {}, Lcom/miui/home/launcher/DeviceConfig;->getScreenLongSize()I

    move-result p1

    int-to-float p1, p1

    mul-float/2addr p1, v3

    new-instance p2, Landroid/graphics/RectF;

    sub-float p3, p0, v2

    sub-float p4, p1, v2

    add-float/2addr p0, v2

    add-float/2addr p1, v2

    invoke-direct {p2, p3, p4, p0, p1}, Landroid/graphics/RectF;-><init>(FFFF)V

    return-object p2

    :cond_5
    :goto_0
    invoke-static {}, Lcom/miui/home/recents/views/FloatingTaskView;->isFloatingTaskViewOnRight()Z

    move-result p5

    if-eqz p5, :cond_6

    invoke-static {}, Lcom/miui/home/recents/views/FloatingTaskView;->getFloatingTaskViewRectF()Landroid/graphics/RectF;

    move-result-object p0

    return-object p0

    :cond_6
    if-ne p4, v0, :cond_7

    invoke-static {}, Lcom/miui/home/launcher/DeviceConfig;->getScreenWidth()I

    move-result p0

    int-to-float p0, p0

    mul-float/2addr p0, v3

    invoke-static {}, Lcom/miui/home/launcher/DeviceConfig;->getRealScreenHeight()I

    move-result p1

    int-to-float p1, p1

    mul-float/2addr p1, v1

    new-instance p2, Landroid/graphics/RectF;

    sub-float p3, p0, v2

    sub-float p4, p1, v2

    add-float/2addr p0, v2

    add-float/2addr p1, v2

    invoke-direct {p2, p3, p4, p0, p1}, Landroid/graphics/RectF;-><init>(FFFF)V

    return-object p2

    :cond_7
    invoke-static {p0, p1, p2, p3}, Lcom/miui/home/recents/TaskViewUtils;->getTaskViewStartRectF(Lcom/miui/home/launcher/Launcher;Lcom/miui/home/recents/views/RecentsView;Lcom/miui/home/recents/views/TaskView;Landroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object p0

    return-object p0

    :cond_8
    invoke-static {p0, p1, p2, p3}, Lcom/miui/home/recents/TaskViewUtils;->getTaskViewStartRectF(Lcom/miui/home/launcher/Launcher;Lcom/miui/home/recents/views/RecentsView;Lcom/miui/home/recents/views/TaskView;Landroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object p0

    return-object p0
.end method

.method private static isClipFromLeftOrTop(I)Z
    .locals 1

    invoke-static {}, Lcom/miui/home/launcher/common/Utilities;->isPadDevice()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/miui/home/launcher/Application;->getInstance()Lcom/miui/home/launcher/Application;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/home/launcher/Application;->isInFoldLargeScreenMode()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x3

    if-ne p0, v0, :cond_1

    invoke-static {}, Lcom/miui/home/recents/views/FloatingTaskView;->isFloatingTaskViewOnRight()Z

    move-result p0

    if-eqz p0, :cond_1

    const/4 p0, 0x0

    return p0

    :cond_1
    const/4 p0, 0x1

    return p0
.end method

.method private static isVerticalClip(I)Z
    .locals 1

    invoke-static {}, Lcom/miui/home/launcher/common/Utilities;->isPadDevice()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/miui/home/launcher/Application;->getInstance()Lcom/miui/home/launcher/Application;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/home/launcher/Application;->isInFoldLargeScreenMode()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x2

    if-eq p0, v0, :cond_2

    const/4 v0, 0x3

    if-ne p0, v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 p0, 0x1

    return p0

    :cond_2
    :goto_0
    const/4 p0, 0x0

    return p0
.end method

.method static synthetic lambda$createSpringAnim$0(Lcom/miui/home/recents/views/TaskView;FZLcom/miui/home/recents/views/RecentsView;Landroid/graphics/RectF;Landroid/graphics/RectF;)V
    .locals 9

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/miui/home/recents/views/TaskView;->setChildrenViewAlpha(F)V

    const v1, 0x3ecccccd    # 0.4f

    sub-float v2, v1, p1

    div-float/2addr v2, v1

    invoke-static {v0, v2}, Ljava/lang/Math;->max(FF)F

    move-result v1

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-static {v2, v1}, Ljava/lang/Math;->min(FF)F

    move-result v8

    invoke-virtual {p0, v0, v0, v8}, Lcom/miui/home/recents/views/TaskView;->setHeaderTranslationAndAlpha(FFF)V

    if-eqz p2, :cond_0

    move-object v3, p3

    move-object v4, p0

    move-object v5, p4

    move-object v6, p5

    move v7, p1

    invoke-virtual/range {v3 .. v8}, Lcom/miui/home/recents/views/RecentsView;->alignTaskViewWhenTaskLaunch(Lcom/miui/home/recents/views/TaskView;Landroid/graphics/RectF;Landroid/graphics/RectF;FF)V

    :cond_0
    return-void
.end method

.method static synthetic lambda$createSpringAnim$1(Lcom/miui/home/recents/util/ClipAnimationHelper$TransformParams;ZZLcom/miui/home/recents/util/ClipAnimationHelper;Lcom/miui/home/recents/util/RemoteAnimationTargetSet;IZLcom/miui/home/recents/views/TaskView;ZLcom/miui/home/recents/views/RecentsView;Landroid/graphics/RectF;Landroid/graphics/RectF;FFF)V
    .locals 12

    move-object v0, p0

    move-object/from16 v1, p4

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {p0, v2}, Lcom/miui/home/recents/util/ClipAnimationHelper$TransformParams;->setTargetAlpha(F)Lcom/miui/home/recents/util/ClipAnimationHelper$TransformParams;

    move-result-object v3

    move-object/from16 v10, p11

    invoke-virtual {v3, v10}, Lcom/miui/home/recents/util/ClipAnimationHelper$TransformParams;->setRect(Landroid/graphics/RectF;)Lcom/miui/home/recents/util/ClipAnimationHelper$TransformParams;

    move-result-object v3

    const/4 v11, 0x0

    invoke-virtual {v3, v11}, Lcom/miui/home/recents/util/ClipAnimationHelper$TransformParams;->setClipProgress(F)Lcom/miui/home/recents/util/ClipAnimationHelper$TransformParams;

    move-result-object v3

    move/from16 v4, p13

    invoke-virtual {v3, v4}, Lcom/miui/home/recents/util/ClipAnimationHelper$TransformParams;->setRadius(F)Lcom/miui/home/recents/util/ClipAnimationHelper$TransformParams;

    move-result-object v3

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/miui/home/recents/util/ClipAnimationHelper$TransformParams;->setVerticalClip(Ljava/lang/Boolean;)Lcom/miui/home/recents/util/ClipAnimationHelper$TransformParams;

    move-result-object v3

    move v4, p2

    invoke-virtual {v3, p2}, Lcom/miui/home/recents/util/ClipAnimationHelper$TransformParams;->setClipFromLeftOrTop(Z)Lcom/miui/home/recents/util/ClipAnimationHelper$TransformParams;

    const/4 v3, 0x0

    move-object v4, p3

    move/from16 v5, p5

    invoke-virtual {p3, v1, p0, v3, v5}, Lcom/miui/home/recents/util/ClipAnimationHelper;->applyTransformNew(Lcom/miui/home/recents/util/RemoteAnimationTargetSet;Lcom/miui/home/recents/util/ClipAnimationHelper$TransformParams;Lcom/miui/home/recents/util/ClipAnimationHelper$TransformParams;I)Landroid/graphics/RectF;

    if-nez p6, :cond_0

    sget-object v0, Lcom/miui/home/recents/TouchInteractionService;->MAIN_THREAD_EXECUTOR:Lcom/miui/home/launcher/MainThreadExecutor;

    new-instance v3, Lcom/miui/home/recents/-$$Lambda$TaskViewUtils$JFzKlJnt07nalq4rgxfKVPzuF6M;

    move-object v4, v3

    move-object/from16 v5, p7

    move/from16 v6, p12

    move/from16 v7, p8

    move-object/from16 v8, p9

    move-object/from16 v9, p10

    move-object/from16 v10, p11

    invoke-direct/range {v4 .. v10}, Lcom/miui/home/recents/-$$Lambda$TaskViewUtils$JFzKlJnt07nalq4rgxfKVPzuF6M;-><init>(Lcom/miui/home/recents/views/TaskView;FZLcom/miui/home/recents/views/RecentsView;Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    invoke-virtual {v0, v3}, Lcom/miui/home/launcher/MainThreadExecutor;->execute(Ljava/lang/Runnable;)V

    :cond_0
    if-eqz v1, :cond_1

    invoke-virtual/range {p4 .. p4}, Lcom/miui/home/recents/util/RemoteAnimationTargetSet;->isTranslucent()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lcom/miui/home/launcher/DeviceConfig;->isInMultiWindowModeCompatAndroidT()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lcom/miui/home/recents/DimLayer;->getInstance()Lcom/miui/home/recents/DimLayer;

    move-result-object v0

    move/from16 v3, p12

    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v2

    invoke-static {v11, v2}, Ljava/lang/Math;->max(FF)F

    move-result v2

    invoke-virtual/range {p4 .. p4}, Lcom/miui/home/recents/util/RemoteAnimationTargetSet;->getHomeSurfaceControlCompat()Lcom/android/systemui/shared/recents/system/SurfaceControlCompat;

    move-result-object v1

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v1, v3}, Lcom/miui/home/recents/DimLayer;->dim(FLcom/android/systemui/shared/recents/system/SurfaceControlCompat;Z)V

    :cond_1
    return-void
.end method

.method public static showDockDivider([Lcom/android/systemui/shared/recents/system/RemoteAnimationTargetCompat;)V
    .locals 5

    if-eqz p0, :cond_1

    array-length v0, p0

    if-lez v0, :cond_1

    array-length v0, p0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    aget-object v2, p0, v1

    iget v3, v2, Lcom/android/systemui/shared/recents/system/RemoteAnimationTargetCompat;->windowType:I

    const/16 v4, 0x7f2

    if-ne v3, v4, :cond_0

    new-instance v3, Lcom/android/systemui/shared/recents/system/TransactionCompat;

    invoke-direct {v3}, Lcom/android/systemui/shared/recents/system/TransactionCompat;-><init>()V

    iget-object v2, v2, Lcom/android/systemui/shared/recents/system/RemoteAnimationTargetCompat;->leash:Lcom/android/systemui/shared/recents/system/SurfaceControlCompat;

    invoke-virtual {v3, v2}, Lcom/android/systemui/shared/recents/system/TransactionCompat;->show(Lcom/android/systemui/shared/recents/system/SurfaceControlCompat;)Lcom/android/systemui/shared/recents/system/TransactionCompat;

    invoke-virtual {v3}, Lcom/android/systemui/shared/recents/system/TransactionCompat;->apply()V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method
