.class public Lcom/miui/home/library/utils/DeferredHandler;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/home/library/utils/DeferredHandler$IdleRunnable;,
        Lcom/miui/home/library/utils/DeferredHandler$Impl;
    }
.end annotation


# instance fields
.field private mDeferedDelay:J

.field private mHandler:Lcom/miui/home/library/utils/DeferredHandler$Impl;

.field private mMessageQueue:Landroid/os/MessageQueue;

.field private mQueue:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/miui/home/library/utils/DeferredHandler;->mQueue:Ljava/util/LinkedList;

    invoke-static {}, Landroid/os/Looper;->myQueue()Landroid/os/MessageQueue;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/home/library/utils/DeferredHandler;->mMessageQueue:Landroid/os/MessageQueue;

    new-instance v0, Lcom/miui/home/library/utils/DeferredHandler$Impl;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/miui/home/library/utils/DeferredHandler$Impl;-><init>(Lcom/miui/home/library/utils/DeferredHandler;Lcom/miui/home/library/utils/DeferredHandler$1;)V

    iput-object v0, p0, Lcom/miui/home/library/utils/DeferredHandler;->mHandler:Lcom/miui/home/library/utils/DeferredHandler$Impl;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/miui/home/library/utils/DeferredHandler;->mDeferedDelay:J

    return-void
.end method

.method static synthetic access$100(Lcom/miui/home/library/utils/DeferredHandler;)Ljava/util/LinkedList;
    .locals 0

    iget-object p0, p0, Lcom/miui/home/library/utils/DeferredHandler;->mQueue:Ljava/util/LinkedList;

    return-object p0
.end method


# virtual methods
.method public cancel()V
    .locals 2

    iget-object v0, p0, Lcom/miui/home/library/utils/DeferredHandler;->mQueue:Ljava/util/LinkedList;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Lcom/miui/home/library/utils/DeferredHandler;->mQueue:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->clear()V

    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public post(Ljava/lang/Runnable;)V
    .locals 2

    iget-object v0, p0, Lcom/miui/home/library/utils/DeferredHandler;->mQueue:Ljava/util/LinkedList;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Lcom/miui/home/library/utils/DeferredHandler;->mQueue:Ljava/util/LinkedList;

    invoke-virtual {v1, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    iget-object p1, p0, Lcom/miui/home/library/utils/DeferredHandler;->mQueue:Ljava/util/LinkedList;

    invoke-virtual {p1}, Ljava/util/LinkedList;->size()I

    move-result p1

    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    invoke-virtual {p0}, Lcom/miui/home/library/utils/DeferredHandler;->scheduleNextLocked()V

    :cond_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public postIdle(Ljava/lang/Runnable;)V
    .locals 1

    new-instance v0, Lcom/miui/home/library/utils/DeferredHandler$IdleRunnable;

    invoke-direct {v0, p0, p1}, Lcom/miui/home/library/utils/DeferredHandler$IdleRunnable;-><init>(Lcom/miui/home/library/utils/DeferredHandler;Ljava/lang/Runnable;)V

    invoke-virtual {p0, v0}, Lcom/miui/home/library/utils/DeferredHandler;->post(Ljava/lang/Runnable;)V

    return-void
.end method

.method scheduleNextLocked()V
    .locals 4

    goto/32 :goto_4

    nop

    :goto_0
    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    goto/32 :goto_c

    nop

    :goto_1
    check-cast v0, Ljava/lang/Runnable;

    goto/32 :goto_b

    nop

    :goto_2
    invoke-virtual {v0}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_1

    nop

    :goto_3
    const/4 v3, 0x1

    goto/32 :goto_5

    nop

    :goto_4
    iget-object v0, p0, Lcom/miui/home/library/utils/DeferredHandler;->mQueue:Ljava/util/LinkedList;

    goto/32 :goto_0

    nop

    :goto_5
    if-eqz v2, :cond_0

    goto/32 :goto_e

    :cond_0
    goto/32 :goto_13

    nop

    :goto_6
    cmp-long v2, v0, v2

    goto/32 :goto_3

    nop

    :goto_7
    invoke-virtual {v0, v3}, Lcom/miui/home/library/utils/DeferredHandler$Impl;->sendEmptyMessage(I)Z

    goto/32 :goto_d

    nop

    :goto_8
    const-wide/16 v2, -0x1

    goto/32 :goto_6

    nop

    :goto_9
    return-void

    :goto_a
    iget-object v2, p0, Lcom/miui/home/library/utils/DeferredHandler;->mHandler:Lcom/miui/home/library/utils/DeferredHandler$Impl;

    goto/32 :goto_17

    nop

    :goto_b
    instance-of v0, v0, Lcom/miui/home/library/utils/DeferredHandler$IdleRunnable;

    goto/32 :goto_19

    nop

    :goto_c
    if-gtz v0, :cond_1

    goto/32 :goto_18

    :cond_1
    goto/32 :goto_10

    nop

    :goto_d
    goto :goto_18

    :goto_e
    goto/32 :goto_a

    nop

    :goto_f
    iget-object v1, p0, Lcom/miui/home/library/utils/DeferredHandler;->mHandler:Lcom/miui/home/library/utils/DeferredHandler$Impl;

    goto/32 :goto_11

    nop

    :goto_10
    iget-object v0, p0, Lcom/miui/home/library/utils/DeferredHandler;->mQueue:Ljava/util/LinkedList;

    goto/32 :goto_2

    nop

    :goto_11
    invoke-virtual {v0, v1}, Landroid/os/MessageQueue;->addIdleHandler(Landroid/os/MessageQueue$IdleHandler;)V

    goto/32 :goto_14

    nop

    :goto_12
    iget-wide v0, p0, Lcom/miui/home/library/utils/DeferredHandler;->mDeferedDelay:J

    goto/32 :goto_8

    nop

    :goto_13
    iget-object v0, p0, Lcom/miui/home/library/utils/DeferredHandler;->mHandler:Lcom/miui/home/library/utils/DeferredHandler$Impl;

    goto/32 :goto_7

    nop

    :goto_14
    goto :goto_18

    :goto_15
    goto/32 :goto_12

    nop

    :goto_16
    iget-object v0, p0, Lcom/miui/home/library/utils/DeferredHandler;->mMessageQueue:Landroid/os/MessageQueue;

    goto/32 :goto_f

    nop

    :goto_17
    invoke-virtual {v2, v3, v0, v1}, Lcom/miui/home/library/utils/DeferredHandler$Impl;->sendEmptyMessageDelayed(IJ)Z

    :goto_18
    goto/32 :goto_9

    nop

    :goto_19
    if-nez v0, :cond_2

    goto/32 :goto_15

    :cond_2
    goto/32 :goto_16

    nop
.end method

.method public setDeferedDelay(J)V
    .locals 0

    iput-wide p1, p0, Lcom/miui/home/library/utils/DeferredHandler;->mDeferedDelay:J

    return-void
.end method
