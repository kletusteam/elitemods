.class Lcom/miui/home/launcher/compat/UserPresentAnimationCompatV12Phone;
.super Lcom/miui/home/launcher/compat/UserPresentAnimationCompatV12Base;


# instance fields
.field private final mAnimatorListener:Landroid/animation/AnimatorListenerAdapter;


# direct methods
.method constructor <init>(Lcom/miui/home/launcher/Launcher;)V
    .locals 3

    const v0, -0x3b448000    # -1500.0f

    const v1, 0x3e4ccccd    # 0.2f

    const/high16 v2, 0x41a00000    # 20.0f

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/miui/home/launcher/compat/UserPresentAnimationCompatV12Base;-><init>(Lcom/miui/home/launcher/Launcher;FFF)V

    new-instance p1, Lcom/miui/home/launcher/compat/UserPresentAnimationCompatV12Phone$1;

    invoke-direct {p1, p0}, Lcom/miui/home/launcher/compat/UserPresentAnimationCompatV12Phone$1;-><init>(Lcom/miui/home/launcher/compat/UserPresentAnimationCompatV12Phone;)V

    iput-object p1, p0, Lcom/miui/home/launcher/compat/UserPresentAnimationCompatV12Phone;->mAnimatorListener:Landroid/animation/AnimatorListenerAdapter;

    return-void
.end method

.method private endAnimation(Landroid/view/View;I)V
    .locals 1

    invoke-virtual {p1, p2}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/miui/home/launcher/animate/SpringAnimator;

    if-eqz v0, :cond_0

    invoke-virtual {p1, p2}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/miui/home/launcher/animate/SpringAnimator;

    invoke-virtual {p1}, Lcom/miui/home/launcher/animate/SpringAnimator;->cancel()V

    :cond_0
    return-void
.end method

.method private getSpringAnimator(Landroid/view/View;IFFFF)Lcom/miui/home/launcher/animate/SpringAnimator;
    .locals 1

    invoke-virtual {p1, p2}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/miui/home/launcher/animate/SpringAnimator;

    if-eqz v0, :cond_0

    invoke-virtual {p1, p2}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/miui/home/launcher/animate/SpringAnimator;

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/miui/home/launcher/animate/SpringAnimator;

    invoke-direct {v0, p5, p6, p3, p4}, Lcom/miui/home/launcher/animate/SpringAnimator;-><init>(FFFF)V

    invoke-virtual {p1, p2, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    move-object p1, v0

    :goto_0
    return-object p1
.end method

.method public static synthetic lambda$showUserPresentAnimation$0(Lcom/miui/home/launcher/compat/UserPresentAnimationCompatV12Phone;IILandroid/view/View;F)V
    .locals 0

    invoke-virtual {p0, p1, p2, p4}, Lcom/miui/home/launcher/compat/UserPresentAnimationCompatV12Phone;->conversionValueFrom3DTo2D(IIF)[F

    move-result-object p1

    invoke-virtual {p0, p3}, Lcom/miui/home/launcher/compat/UserPresentAnimationCompatV12Phone;->ignoreTranslation(Landroid/view/View;)Z

    move-result p2

    if-nez p2, :cond_0

    const/4 p2, 0x0

    aget p2, p1, p2

    invoke-virtual {p3, p2}, Landroid/view/View;->setTranslationX(F)V

    :cond_0
    const/4 p2, 0x1

    aget p2, p1, p2

    invoke-virtual {p3, p2}, Landroid/view/View;->setTranslationY(F)V

    const/4 p2, 0x2

    aget p4, p1, p2

    invoke-virtual {p3, p4}, Landroid/view/View;->setScaleX(F)V

    aget p1, p1, p2

    invoke-virtual {p3, p1}, Landroid/view/View;->setScaleY(F)V

    return-void
.end method


# virtual methods
.method public endAnimation(Landroid/view/View;)V
    .locals 1

    const v0, 0x7f0a03e8

    invoke-direct {p0, p1, v0}, Lcom/miui/home/launcher/compat/UserPresentAnimationCompatV12Phone;->endAnimation(Landroid/view/View;I)V

    const v0, 0x7f0a03ed

    invoke-direct {p0, p1, v0}, Lcom/miui/home/launcher/compat/UserPresentAnimationCompatV12Phone;->endAnimation(Landroid/view/View;I)V

    invoke-virtual {p0, p1}, Lcom/miui/home/launcher/compat/UserPresentAnimationCompatV12Phone;->resetView(Landroid/view/View;)V

    return-void
.end method

.method showUserPresentAnimation(Landroid/view/View;IZ)V
    .locals 10

    goto/32 :goto_13

    nop

    :goto_0
    move-object v2, p0

    goto/32 :goto_3d

    nop

    :goto_1
    return-void

    :goto_2
    new-instance v1, Lcom/miui/home/launcher/compat/-$$Lambda$UserPresentAnimationCompatV12Phone$C3nE-FWrW5QotTQ-MBzJLLRXqsk;

    goto/32 :goto_11

    nop

    :goto_3
    check-cast v2, Ljava/lang/Integer;

    goto/32 :goto_3a

    nop

    :goto_4
    const/high16 v6, 0x3f800000    # 1.0f

    goto/32 :goto_2f

    nop

    :goto_5
    invoke-virtual {p1, p3}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v3

    goto/32 :goto_33

    nop

    :goto_6
    const/4 v1, 0x0

    goto/32 :goto_27

    nop

    :goto_7
    const v6, -0x3b448000    # -1500.0f

    goto/32 :goto_2d

    nop

    :goto_8
    invoke-static {p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    goto/32 :goto_21

    nop

    :goto_9
    invoke-virtual {p3, v1}, Lcom/miui/home/launcher/animate/SpringAnimator;->setStartVelocity(F)V

    goto/32 :goto_2

    nop

    :goto_a
    if-nez v0, :cond_0

    goto/32 :goto_e

    :cond_0
    goto/32 :goto_20

    nop

    :goto_b
    const/4 v1, 0x0

    goto/32 :goto_36

    nop

    :goto_c
    iput p3, p0, Lcom/miui/home/launcher/compat/UserPresentAnimationCompatV12Phone;->mAllAnimationViewNum:I

    goto/32 :goto_3c

    nop

    :goto_d
    goto/16 :goto_39

    :goto_e
    goto/32 :goto_38

    nop

    :goto_f
    if-nez v1, :cond_1

    goto/32 :goto_28

    :cond_1
    goto/32 :goto_6

    nop

    :goto_10
    invoke-virtual {p1, p3}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_26

    nop

    :goto_11
    invoke-direct {v1, p0, v0, v2, p1}, Lcom/miui/home/launcher/compat/-$$Lambda$UserPresentAnimationCompatV12Phone$C3nE-FWrW5QotTQ-MBzJLLRXqsk;-><init>(Lcom/miui/home/launcher/compat/UserPresentAnimationCompatV12Phone;IILandroid/view/View;)V

    goto/32 :goto_31

    nop

    :goto_12
    const v8, 0x3dcccccd    # 0.1f

    goto/32 :goto_0

    nop

    :goto_13
    iget p3, p0, Lcom/miui/home/launcher/compat/UserPresentAnimationCompatV12Phone;->mAllAnimationViewNum:I

    goto/32 :goto_3b

    nop

    :goto_14
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto/32 :goto_34

    nop

    :goto_15
    const/4 v5, 0x0

    goto/32 :goto_4

    nop

    :goto_16
    invoke-direct/range {v3 .. v9}, Lcom/miui/home/launcher/compat/UserPresentAnimationCompatV12Phone;->getSpringAnimator(Landroid/view/View;IFFFF)Lcom/miui/home/launcher/animate/SpringAnimator;

    move-result-object p3

    goto/32 :goto_17

    nop

    :goto_17
    invoke-static {}, Lcom/miui/home/launcher/common/DeviceLevelUtils;->isHighAnimationRate()Z

    move-result v1

    goto/32 :goto_f

    nop

    :goto_18
    invoke-direct {p3, p1}, Lcom/miui/home/launcher/compat/-$$Lambda$Ha1Zt_z4-25WcwQGPzDUEZW5QkY;-><init>(Landroid/view/View;)V

    goto/32 :goto_22

    nop

    :goto_19
    invoke-virtual {p3, v0}, Lcom/miui/home/launcher/animate/SpringAnimator;->setAnimatorListenerAdapter(Landroid/animation/AnimatorListenerAdapter;)V

    goto/32 :goto_37

    nop

    :goto_1a
    const p3, 0x7f0a03e3

    goto/32 :goto_5

    nop

    :goto_1b
    move-object v3, p0

    goto/32 :goto_1f

    nop

    :goto_1c
    invoke-direct/range {v2 .. v8}, Lcom/miui/home/launcher/compat/UserPresentAnimationCompatV12Phone;->getSpringAnimator(Landroid/view/View;IFFFF)Lcom/miui/home/launcher/animate/SpringAnimator;

    move-result-object p2

    goto/32 :goto_8

    nop

    :goto_1d
    invoke-virtual {p1, p3, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    :goto_1e
    goto/32 :goto_24

    nop

    :goto_1f
    move-object v4, p1

    goto/32 :goto_16

    nop

    :goto_20
    invoke-virtual {p1, p3}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_2c

    nop

    :goto_21
    new-instance p3, Lcom/miui/home/launcher/compat/-$$Lambda$Ha1Zt_z4-25WcwQGPzDUEZW5QkY;

    goto/32 :goto_18

    nop

    :goto_22
    invoke-virtual {p2, p3}, Lcom/miui/home/launcher/animate/SpringAnimator;->setUpdateListener(Lcom/miui/home/launcher/animate/SpringAnimator$UpdateListener;)V

    goto/32 :goto_2e

    nop

    :goto_23
    const v9, 0x3eb33333    # 0.35f

    goto/32 :goto_1b

    nop

    :goto_24
    const v5, 0x7f0a03ed

    goto/32 :goto_7

    nop

    :goto_25
    const v8, 0x3f47ae14    # 0.78f

    goto/32 :goto_23

    nop

    :goto_26
    instance-of v0, v0, Ljava/lang/Integer;

    goto/32 :goto_b

    nop

    :goto_27
    goto :goto_2a

    :goto_28
    goto/32 :goto_29

    nop

    :goto_29
    const/high16 v1, 0x41200000    # 10.0f

    :goto_2a
    goto/32 :goto_9

    nop

    :goto_2b
    iget-object v0, p0, Lcom/miui/home/launcher/compat/UserPresentAnimationCompatV12Phone;->mAnimatorListener:Landroid/animation/AnimatorListenerAdapter;

    goto/32 :goto_19

    nop

    :goto_2c
    check-cast v0, Ljava/lang/Integer;

    goto/32 :goto_14

    nop

    :goto_2d
    const/4 v7, 0x0

    goto/32 :goto_25

    nop

    :goto_2e
    invoke-virtual {p2, v0, v1}, Lcom/miui/home/launcher/animate/SpringAnimator;->startDelay(J)V

    goto/32 :goto_1

    nop

    :goto_2f
    const/high16 v7, 0x3f400000    # 0.75f

    goto/32 :goto_12

    nop

    :goto_30
    if-nez v3, :cond_2

    goto/32 :goto_1e

    :cond_2
    goto/32 :goto_32

    nop

    :goto_31
    invoke-virtual {p3, v1}, Lcom/miui/home/launcher/animate/SpringAnimator;->setUpdateListener(Lcom/miui/home/launcher/animate/SpringAnimator$UpdateListener;)V

    goto/32 :goto_2b

    nop

    :goto_32
    invoke-virtual {p1, p3}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_3

    nop

    :goto_33
    instance-of v3, v3, Ljava/lang/Integer;

    goto/32 :goto_30

    nop

    :goto_34
    invoke-virtual {p1, p3, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    goto/32 :goto_d

    nop

    :goto_35
    const v4, 0x7f0a03e8

    goto/32 :goto_15

    nop

    :goto_36
    const/4 v2, 0x0

    goto/32 :goto_a

    nop

    :goto_37
    int-to-long v0, p2

    goto/32 :goto_3e

    nop

    :goto_38
    move v0, v2

    :goto_39
    goto/32 :goto_1a

    nop

    :goto_3a
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    goto/32 :goto_1d

    nop

    :goto_3b
    add-int/lit8 p3, p3, 0x1

    goto/32 :goto_c

    nop

    :goto_3c
    const p3, 0x7f0a03e2

    goto/32 :goto_10

    nop

    :goto_3d
    move-object v3, p1

    goto/32 :goto_1c

    nop

    :goto_3e
    invoke-virtual {p3, v0, v1}, Lcom/miui/home/launcher/animate/SpringAnimator;->startDelay(J)V

    goto/32 :goto_35

    nop
.end method
