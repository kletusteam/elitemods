.class Lcom/miui/home/recents/SystemUiProxyWrapper$1;
.super Lcom/android/wm/shell/recents/IRecentTasksListener$Stub;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/home/recents/SystemUiProxyWrapper;->registerRecentTasksListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/home/recents/SystemUiProxyWrapper;


# direct methods
.method constructor <init>(Lcom/miui/home/recents/SystemUiProxyWrapper;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/home/recents/SystemUiProxyWrapper$1;->this$0:Lcom/miui/home/recents/SystemUiProxyWrapper;

    invoke-direct {p0}, Lcom/android/wm/shell/recents/IRecentTasksListener$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onRecentTasksChanged()V
    .locals 2

    invoke-static {}, Lcom/miui/home/launcher/Application;->getInstance()Lcom/miui/home/launcher/Application;

    move-result-object v0

    invoke-static {v0}, Lcom/miui/home/recents/RecentsModel;->getInstance(Landroid/content/Context;)Lcom/miui/home/recents/RecentsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/home/recents/RecentsModel;->onTaskStackChangedBackground()V

    invoke-static {}, Lcom/miui/home/library/utils/AsyncTaskExecutorHelper;->getEventBus()Lorg/greenrobot/eventbus/EventBus;

    move-result-object v0

    new-instance v1, Lcom/miui/home/recents/messages/RecentTasksChangedEvent;

    invoke-direct {v1}, Lcom/miui/home/recents/messages/RecentTasksChangedEvent;-><init>()V

    invoke-virtual {v0, v1}, Lorg/greenrobot/eventbus/EventBus;->post(Ljava/lang/Object;)V

    return-void
.end method
