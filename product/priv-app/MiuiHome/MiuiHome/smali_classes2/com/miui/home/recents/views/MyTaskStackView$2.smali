.class Lcom/miui/home/recents/views/MyTaskStackView$2;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/miui/home/launcher/LauncherStateManager$StateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/home/recents/views/MyTaskStackView;->onAttachedToWindow()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/miui/home/recents/views/MyTaskStackView;


# direct methods
.method constructor <init>(Lcom/miui/home/recents/views/MyTaskStackView;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/home/recents/views/MyTaskStackView$2;->this$0:Lcom/miui/home/recents/views/MyTaskStackView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onStateTransitionComplete(Lcom/miui/home/launcher/LauncherState;)V
    .locals 2

    sget-object v0, Lcom/miui/home/launcher/LauncherState;->OVERVIEW:Lcom/miui/home/recents/OverviewState;

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/miui/home/recents/views/MyTaskStackView$2;->this$0:Lcom/miui/home/recents/views/MyTaskStackView;

    invoke-static {v0}, Lcom/miui/home/recents/views/MyTaskStackView;->access$100(Lcom/miui/home/recents/views/MyTaskStackView;)V

    :cond_0
    invoke-static {}, Lcom/miui/home/recents/views/MyTaskStackView;->access$200()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onStateTransitionComplete: "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onStateTransitionStart(Lcom/miui/home/launcher/LauncherState;)V
    .locals 2

    sget-object v0, Lcom/miui/home/launcher/LauncherState;->OVERVIEW:Lcom/miui/home/recents/OverviewState;

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/miui/home/recents/views/MyTaskStackView$2;->this$0:Lcom/miui/home/recents/views/MyTaskStackView;

    invoke-static {v0}, Lcom/miui/home/recents/views/MyTaskStackView;->access$100(Lcom/miui/home/recents/views/MyTaskStackView;)V

    :cond_0
    invoke-static {}, Lcom/miui/home/recents/views/MyTaskStackView;->access$200()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onStateTransitionStart: "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
