.class public Lcom/miui/home/recents/views/TaskStackViewHorizontalScroller;
.super Lcom/miui/home/recents/views/TaskStackViewScroller;


# instance fields
.field private mFlingDownX:F

.field private mPhysicalScroller:Lcom/miui/home/recents/util/PhysicalScroller;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/miui/home/recents/views/TaskStackViewScroller$TaskStackViewScrollerCallback;Lcom/miui/home/recents/views/TaskStackLayoutAlgorithm;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/miui/home/recents/views/TaskStackViewScroller;-><init>(Landroid/content/Context;Lcom/miui/home/recents/views/TaskStackViewScroller$TaskStackViewScrollerCallback;Lcom/miui/home/recents/views/TaskStackLayoutAlgorithm;)V

    new-instance p1, Lcom/miui/home/recents/util/PhysicalScroller;

    invoke-direct {p1}, Lcom/miui/home/recents/util/PhysicalScroller;-><init>()V

    iput-object p1, p0, Lcom/miui/home/recents/views/TaskStackViewHorizontalScroller;->mPhysicalScroller:Lcom/miui/home/recents/util/PhysicalScroller;

    return-void
.end method

.method private adjustEndScrollPix(F)F
    .locals 3

    iget v0, p0, Lcom/miui/home/recents/views/TaskStackViewHorizontalScroller;->mFlingDownScrollP:F

    iget-object v1, p0, Lcom/miui/home/recents/views/TaskStackViewHorizontalScroller;->mLayoutAlgorithm:Lcom/miui/home/recents/views/TaskStackLayoutAlgorithm;

    iget v2, p0, Lcom/miui/home/recents/views/TaskStackViewHorizontalScroller;->mFlingDownX:F

    invoke-virtual {v1, v2, p1}, Lcom/miui/home/recents/views/TaskStackLayoutAlgorithm;->getDeltaPForX(FF)F

    move-result p1

    add-float/2addr v0, p1

    invoke-virtual {p0, v0}, Lcom/miui/home/recents/views/TaskStackViewHorizontalScroller;->getNearestMotionlessScrollP(F)F

    move-result p1

    iget v0, p0, Lcom/miui/home/recents/views/TaskStackViewHorizontalScroller;->mFlingDownX:F

    iget-object v1, p0, Lcom/miui/home/recents/views/TaskStackViewHorizontalScroller;->mLayoutAlgorithm:Lcom/miui/home/recents/views/TaskStackLayoutAlgorithm;

    iget v2, p0, Lcom/miui/home/recents/views/TaskStackViewHorizontalScroller;->mFlingDownScrollP:F

    invoke-virtual {v1, v2, p1}, Lcom/miui/home/recents/views/TaskStackLayoutAlgorithm;->getXForDeltaP(FF)I

    move-result p1

    int-to-float p1, p1

    add-float/2addr v0, p1

    return v0
.end method


# virtual methods
.method animateBoundScroll(I)V
    .locals 11

    goto/32 :goto_12

    nop

    :goto_0
    move-wide v5, v9

    goto/32 :goto_a

    nop

    :goto_1
    float-to-double v1, v1

    goto/32 :goto_e

    nop

    :goto_2
    iget-object v0, p0, Lcom/miui/home/recents/views/TaskStackViewHorizontalScroller;->mPhysicalScroller:Lcom/miui/home/recents/util/PhysicalScroller;

    goto/32 :goto_9

    nop

    :goto_3
    invoke-virtual/range {v0 .. v10}, Lcom/miui/home/recents/util/PhysicalScroller;->startAnim(DDDDD)V

    goto/32 :goto_f

    nop

    :goto_4
    int-to-float p1, p1

    goto/32 :goto_2

    nop

    :goto_5
    check-cast p1, Lcom/miui/home/recents/views/TaskStackView;

    goto/32 :goto_13

    nop

    :goto_6
    iput p1, p0, Lcom/miui/home/recents/views/TaskStackViewHorizontalScroller;->mFlingDownX:F

    goto/32 :goto_b

    nop

    :goto_7
    iget v1, p0, Lcom/miui/home/recents/views/TaskStackViewHorizontalScroller;->mFlingDownScrollP:F

    goto/32 :goto_c

    nop

    :goto_8
    if-nez v1, :cond_0

    goto/32 :goto_14

    :cond_0
    goto/32 :goto_15

    nop

    :goto_9
    iget v1, p0, Lcom/miui/home/recents/views/TaskStackViewHorizontalScroller;->mFlingDownX:F

    goto/32 :goto_1

    nop

    :goto_a
    move-wide v7, v9

    goto/32 :goto_3

    nop

    :goto_b
    iget-object p1, p0, Lcom/miui/home/recents/views/TaskStackViewHorizontalScroller;->mLayoutAlgorithm:Lcom/miui/home/recents/views/TaskStackLayoutAlgorithm;

    goto/32 :goto_7

    nop

    :goto_c
    invoke-virtual {p1, v1, v0}, Lcom/miui/home/recents/views/TaskStackLayoutAlgorithm;->getXForDeltaP(FF)I

    move-result p1

    goto/32 :goto_4

    nop

    :goto_d
    float-to-double v9, p1

    goto/32 :goto_0

    nop

    :goto_e
    const-wide/16 v3, 0x0

    goto/32 :goto_d

    nop

    :goto_f
    iget-object p1, p0, Lcom/miui/home/recents/views/TaskStackViewHorizontalScroller;->mScrollerCallback:Lcom/miui/home/recents/views/TaskStackViewScroller$TaskStackViewScrollerCallback;

    goto/32 :goto_5

    nop

    :goto_10
    return-void

    :goto_11
    const/4 p1, 0x0

    goto/32 :goto_6

    nop

    :goto_12
    invoke-virtual {p0}, Lcom/miui/home/recents/views/TaskStackViewHorizontalScroller;->getStackScroll()F

    move-result p1

    goto/32 :goto_16

    nop

    :goto_13
    invoke-virtual {p1}, Lcom/miui/home/recents/views/TaskStackView;->invalidate()V

    :goto_14
    goto/32 :goto_10

    nop

    :goto_15
    iput p1, p0, Lcom/miui/home/recents/views/TaskStackViewHorizontalScroller;->mFlingDownScrollP:F

    goto/32 :goto_11

    nop

    :goto_16
    invoke-virtual {p0, p1}, Lcom/miui/home/recents/views/TaskStackViewHorizontalScroller;->getBoundedStackScroll(F)F

    move-result v0

    goto/32 :goto_17

    nop

    :goto_17
    invoke-static {v0, p1}, Ljava/lang/Float;->compare(FF)I

    move-result v1

    goto/32 :goto_8

    nop
.end method

.method computeScroll()Z
    .locals 4

    goto/32 :goto_1

    nop

    :goto_0
    if-nez v0, :cond_0

    goto/32 :goto_9

    :cond_0
    goto/32 :goto_b

    nop

    :goto_1
    iget-object v0, p0, Lcom/miui/home/recents/views/TaskStackViewHorizontalScroller;->mPhysicalScroller:Lcom/miui/home/recents/util/PhysicalScroller;

    goto/32 :goto_e

    nop

    :goto_2
    double-to-float v2, v2

    goto/32 :goto_c

    nop

    :goto_3
    iget-object v2, p0, Lcom/miui/home/recents/views/TaskStackViewHorizontalScroller;->mPhysicalScroller:Lcom/miui/home/recents/util/PhysicalScroller;

    goto/32 :goto_7

    nop

    :goto_4
    return v0

    :goto_5
    iget v1, p0, Lcom/miui/home/recents/views/TaskStackViewHorizontalScroller;->mFlingDownX:F

    goto/32 :goto_3

    nop

    :goto_6
    const/4 v0, 0x0

    goto/32 :goto_4

    nop

    :goto_7
    invoke-virtual {v2}, Lcom/miui/home/recents/util/PhysicalScroller;->getPosition()D

    move-result-wide v2

    goto/32 :goto_2

    nop

    :goto_8
    return v0

    :goto_9
    goto/32 :goto_6

    nop

    :goto_a
    const/4 v0, 0x1

    goto/32 :goto_8

    nop

    :goto_b
    iget-object v0, p0, Lcom/miui/home/recents/views/TaskStackViewHorizontalScroller;->mLayoutAlgorithm:Lcom/miui/home/recents/views/TaskStackLayoutAlgorithm;

    goto/32 :goto_5

    nop

    :goto_c
    invoke-virtual {v0, v1, v2}, Lcom/miui/home/recents/views/TaskStackLayoutAlgorithm;->getDeltaPForX(FF)F

    move-result v0

    goto/32 :goto_d

    nop

    :goto_d
    iget v1, p0, Lcom/miui/home/recents/views/TaskStackViewHorizontalScroller;->mFlingDownScrollP:F

    goto/32 :goto_f

    nop

    :goto_e
    invoke-virtual {v0}, Lcom/miui/home/recents/util/PhysicalScroller;->computeScrollOffset()Z

    move-result v0

    goto/32 :goto_0

    nop

    :goto_f
    invoke-virtual {p0, v1, v0}, Lcom/miui/home/recents/views/TaskStackViewHorizontalScroller;->setDeltaStackScroll(FF)V

    goto/32 :goto_a

    nop
.end method

.method public fling(FFFIIIIIIIII)V
    .locals 13

    move-object v0, p0

    move v1, p1

    iput v1, v0, Lcom/miui/home/recents/views/TaskStackViewHorizontalScroller;->mFlingDownScrollP:F

    move v1, p2

    iput v1, v0, Lcom/miui/home/recents/views/TaskStackViewHorizontalScroller;->mFlingDownX:F

    move/from16 v1, p4

    int-to-double v1, v1

    move/from16 v3, p6

    int-to-double v3, v3

    invoke-static {v1, v2, v3, v4}, Lcom/miui/home/recents/util/PhysicalScroller;->getEndPosition(DD)D

    move-result-wide v5

    double-to-float v5, v5

    invoke-direct {p0, v5}, Lcom/miui/home/recents/views/TaskStackViewHorizontalScroller;->adjustEndScrollPix(F)F

    move-result v5

    iget-object v6, v0, Lcom/miui/home/recents/views/TaskStackViewHorizontalScroller;->mPhysicalScroller:Lcom/miui/home/recents/util/PhysicalScroller;

    float-to-double v7, v5

    move/from16 v5, p8

    int-to-double v9, v5

    move/from16 v5, p9

    int-to-double v11, v5

    move-object p1, v6

    move-wide p2, v1

    move-wide/from16 p4, v3

    move-wide/from16 p6, v7

    move-wide/from16 p8, v9

    move-wide/from16 p10, v11

    invoke-virtual/range {p1 .. p11}, Lcom/miui/home/recents/util/PhysicalScroller;->startAnim(DDDDD)V

    return-void
.end method

.method public getNearestMotionlessScrollP(F)F
    .locals 4

    iget-object v0, p0, Lcom/miui/home/recents/views/TaskStackViewHorizontalScroller;->mLayoutAlgorithm:Lcom/miui/home/recents/views/TaskStackLayoutAlgorithm;

    invoke-virtual {v0}, Lcom/miui/home/recents/views/TaskStackLayoutAlgorithm;->getScrollPGap()F

    move-result v0

    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    rem-float/2addr v1, v0

    const/4 v2, 0x0

    cmpl-float v2, p1, v2

    const/high16 v3, 0x40000000    # 2.0f

    if-lez v2, :cond_0

    sub-float/2addr p1, v1

    div-float v2, v0, v3

    cmpl-float v1, v1, v2

    if-lez v1, :cond_1

    add-float/2addr p1, v0

    goto :goto_0

    :cond_0
    add-float/2addr p1, v1

    div-float v2, v0, v3

    cmpl-float v1, v1, v2

    if-lez v1, :cond_1

    sub-float/2addr p1, v0

    :cond_1
    :goto_0
    return p1
.end method

.method public scrollToNearestMotionlessPosition()V
    .locals 12

    invoke-virtual {p0}, Lcom/miui/home/recents/views/TaskStackViewHorizontalScroller;->getStackScroll()F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/miui/home/recents/views/TaskStackViewHorizontalScroller;->getNearestMotionlessScrollP(F)F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_0

    iput v0, p0, Lcom/miui/home/recents/views/TaskStackViewHorizontalScroller;->mFlingDownScrollP:F

    const/4 v0, 0x0

    iput v0, p0, Lcom/miui/home/recents/views/TaskStackViewHorizontalScroller;->mFlingDownX:F

    iget-object v0, p0, Lcom/miui/home/recents/views/TaskStackViewHorizontalScroller;->mLayoutAlgorithm:Lcom/miui/home/recents/views/TaskStackLayoutAlgorithm;

    iget v2, p0, Lcom/miui/home/recents/views/TaskStackViewHorizontalScroller;->mFlingDownScrollP:F

    invoke-virtual {v0, v2, v1}, Lcom/miui/home/recents/views/TaskStackLayoutAlgorithm;->getXForDeltaP(FF)I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/miui/home/recents/views/TaskStackViewHorizontalScroller;->mPhysicalScroller:Lcom/miui/home/recents/util/PhysicalScroller;

    iget v2, p0, Lcom/miui/home/recents/views/TaskStackViewHorizontalScroller;->mFlingDownX:F

    float-to-double v2, v2

    const-wide/16 v4, 0x0

    float-to-double v10, v0

    move-wide v6, v10

    move-wide v8, v10

    invoke-virtual/range {v1 .. v11}, Lcom/miui/home/recents/util/PhysicalScroller;->startAnim(DDDDD)V

    iget-object v0, p0, Lcom/miui/home/recents/views/TaskStackViewHorizontalScroller;->mScrollerCallback:Lcom/miui/home/recents/views/TaskStackViewScroller$TaskStackViewScrollerCallback;

    check-cast v0, Lcom/miui/home/recents/views/TaskStackView;

    invoke-virtual {v0}, Lcom/miui/home/recents/views/TaskStackView;->invalidate()V

    :cond_0
    return-void
.end method

.method stopScroller()V
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    if-eqz v0, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_2

    nop

    :goto_1
    iget-object v0, p0, Lcom/miui/home/recents/views/TaskStackViewHorizontalScroller;->mPhysicalScroller:Lcom/miui/home/recents/util/PhysicalScroller;

    goto/32 :goto_3

    nop

    :goto_2
    iget-object v0, p0, Lcom/miui/home/recents/views/TaskStackViewHorizontalScroller;->mPhysicalScroller:Lcom/miui/home/recents/util/PhysicalScroller;

    goto/32 :goto_5

    nop

    :goto_3
    invoke-virtual {v0}, Lcom/miui/home/recents/util/PhysicalScroller;->isFinish()Z

    move-result v0

    goto/32 :goto_0

    nop

    :goto_4
    return-void

    :goto_5
    invoke-virtual {v0}, Lcom/miui/home/recents/util/PhysicalScroller;->stopScroll()V

    :goto_6
    goto/32 :goto_4

    nop
.end method
