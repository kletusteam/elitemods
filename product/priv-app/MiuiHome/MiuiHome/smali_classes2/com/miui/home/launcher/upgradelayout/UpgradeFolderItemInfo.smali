.class public Lcom/miui/home/launcher/upgradelayout/UpgradeFolderItemInfo;
.super Lcom/miui/home/launcher/upgradelayout/UpgradeLayoutItemInfo;


# direct methods
.method public constructor <init>(IIIIJLjava/lang/String;)V
    .locals 13

    const-wide/16 v7, -0x64

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move/from16 v3, p3

    move/from16 v4, p4

    move-wide/from16 v5, p5

    move-object/from16 v9, p7

    invoke-direct/range {v0 .. v12}, Lcom/miui/home/launcher/upgradelayout/UpgradeLayoutItemInfo;-><init>(IIIIJJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method private addFolder(Lcom/miui/home/launcher/upgradelayout/UpgradeLayoutContext;)V
    .locals 6

    iget-object v0, p1, Lcom/miui/home/launcher/upgradelayout/UpgradeLayoutContext;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    iget-object v1, p0, Lcom/miui/home/launcher/upgradelayout/UpgradeFolderItemInfo;->mValues:Landroid/content/ContentValues;

    iget-object v2, p0, Lcom/miui/home/launcher/upgradelayout/UpgradeFolderItemInfo;->mTitle:Ljava/lang/String;

    const/16 v3, 0x15

    const/4 v4, 0x2

    const/4 v5, 0x2

    invoke-static/range {v0 .. v5}, Lcom/miui/home/launcher/LauncherProvider$DatabaseHelper;->addFolder(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Ljava/lang/String;III)J

    move-result-wide v0

    iput-wide v0, p1, Lcom/miui/home/launcher/upgradelayout/UpgradeLayoutContext;->newFolderId:J

    return-void
.end method

.method private updateOldToolsFolder(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;)V
    .locals 6

    const-string v0, "com.miui.home:string/default_folder_title_tools"

    invoke-static {p1, v0}, Lcom/miui/home/launcher/LauncherProvider$DatabaseHelper;->queryIdByTitle(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "title"

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f11016d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "label"

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    invoke-virtual {p2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v2, v3, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string p2, "favorites"

    const-string v3, "_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-virtual {p1, p2, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    :cond_0
    return-void
.end method


# virtual methods
.method public parse(Lcom/miui/home/launcher/upgradelayout/UpgradeLayoutContext;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/miui/home/launcher/upgradelayout/UpgradeLayoutItemInfo;->parse(Lcom/miui/home/launcher/upgradelayout/UpgradeLayoutContext;)V

    iget-object v0, p1, Lcom/miui/home/launcher/upgradelayout/UpgradeLayoutContext;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    iget-object v1, p1, Lcom/miui/home/launcher/upgradelayout/UpgradeLayoutContext;->mContext:Landroid/content/Context;

    invoke-direct {p0, v0, v1}, Lcom/miui/home/launcher/upgradelayout/UpgradeFolderItemInfo;->updateOldToolsFolder(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;)V

    invoke-direct {p0, p1}, Lcom/miui/home/launcher/upgradelayout/UpgradeFolderItemInfo;->addFolder(Lcom/miui/home/launcher/upgradelayout/UpgradeLayoutContext;)V

    return-void
.end method
