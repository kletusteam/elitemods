.class final Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$runLoadAction$2;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/function/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->runLoadAction(Lcom/miui/home/launcher/FolderInfo;Lcom/miui/home/launcher/IconCache;ZLcom/miui/home/library/utils/AsyncTaskExecutorHelper$SerialExecutor;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/function/Consumer<",
        "Landroid/graphics/drawable/Drawable;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic $currentIndex:I

.field final synthetic $si:Lcom/miui/home/launcher/ShortcutInfo;

.field final synthetic this$0:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;


# direct methods
.method constructor <init>(Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;Lcom/miui/home/launcher/ShortcutInfo;I)V
    .locals 0

    iput-object p1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$runLoadAction$2;->this$0:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    iput-object p2, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$runLoadAction$2;->$si:Lcom/miui/home/launcher/ShortcutInfo;

    iput p3, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$runLoadAction$2;->$currentIndex:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Landroid/graphics/drawable/Drawable;)V
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$runLoadAction$2;->$si:Lcom/miui/home/launcher/ShortcutInfo;

    invoke-virtual {v0}, Lcom/miui/home/launcher/ShortcutInfo;->getColorFilter()Landroid/graphics/ColorFilter;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    :cond_0
    iget v0, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$runLoadAction$2;->$currentIndex:I

    iget-object v1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$runLoadAction$2;->this$0:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    invoke-virtual {v1}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->getMRealPvChildCount()I

    move-result v1

    if-ge v0, v1, :cond_6

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$runLoadAction$2;->this$0:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    invoke-virtual {v0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->getMPvChildList()Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    iget v1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$runLoadAction$2;->$currentIndex:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;

    invoke-virtual {v0, p1}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object p1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$runLoadAction$2;->this$0:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    invoke-virtual {p1}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->getMPvChildList()Ljava/util/List;

    move-result-object p1

    if-nez p1, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    iget v0, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$runLoadAction$2;->$currentIndex:I

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$runLoadAction$2;->$si:Lcom/miui/home/launcher/ShortcutInfo;

    invoke-virtual {p1, v0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;->setMBuddyInfo(Lcom/miui/home/launcher/ShortcutInfo;)V

    iget-object p1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$runLoadAction$2;->this$0:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    invoke-virtual {p1}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->getMPvChildList()Ljava/util/List;

    move-result-object p1

    if-nez p1, :cond_3

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_3
    iget v0, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$runLoadAction$2;->$currentIndex:I

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$runLoadAction$2;->$si:Lcom/miui/home/launcher/ShortcutInfo;

    invoke-virtual {v0}, Lcom/miui/home/launcher/ShortcutInfo;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object p1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$runLoadAction$2;->this$0:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    invoke-virtual {p1}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->getMPvChildList()Ljava/util/List;

    move-result-object p1

    if-nez p1, :cond_4

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_4
    iget v0, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$runLoadAction$2;->$currentIndex:I

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;

    invoke-virtual {p1}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;->getMBuddyInfo()Lcom/miui/home/launcher/ShortcutInfo;

    move-result-object p1

    instance-of p1, p1, Lcom/miui/home/launcher/progress/ProgressShortcutInfo;

    if-eqz p1, :cond_6

    iget-object p1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$runLoadAction$2;->this$0:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    invoke-virtual {p1}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->getMPvChildList()Ljava/util/List;

    move-result-object p1

    if-nez p1, :cond_5

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_5
    iget v0, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$runLoadAction$2;->$currentIndex:I

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;

    invoke-virtual {p1}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;->invalidate()V

    :cond_6
    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, p1}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$runLoadAction$2;->accept(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method
