.class public Lcom/miui/home/launcher/gadget/NormalClearButton;
.super Lcom/miui/home/launcher/gadget/ClearButton;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/home/launcher/gadget/ClearButton;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method doClear()V
    .locals 2

    goto/32 :goto_3

    nop

    :goto_0
    const-string v1, "com.android.systemui.taskmanager.Clear"

    goto/32 :goto_5

    nop

    :goto_1
    iget-object v1, p0, Lcom/miui/home/launcher/gadget/NormalClearButton;->mContext:Landroid/content/Context;

    goto/32 :goto_2

    nop

    :goto_2
    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/32 :goto_4

    nop

    :goto_3
    new-instance v0, Landroid/content/Intent;

    goto/32 :goto_0

    nop

    :goto_4
    return-void

    :goto_5
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    goto/32 :goto_1

    nop
.end method

.method initClearIcon()V
    .locals 2

    goto/32 :goto_2

    nop

    :goto_0
    invoke-virtual {v0, v1}, Lcom/miui/home/launcher/common/ClearIconImageView;->setImageResource(I)V

    goto/32 :goto_1

    nop

    :goto_1
    return-void

    :goto_2
    iget-object v0, p0, Lcom/miui/home/launcher/gadget/NormalClearButton;->mClearIcon:Lcom/miui/home/launcher/common/ClearIconImageView;

    goto/32 :goto_3

    nop

    :goto_3
    const v1, 0x7f080248

    goto/32 :goto_0

    nop
.end method

.method protected setButtonAniamtor(ILandroid/animation/Animator$AnimatorListener;)V
    .locals 0

    invoke-super {p0, p1, p2}, Lcom/miui/home/launcher/gadget/ClearButton;->setButtonAniamtor(ILandroid/animation/Animator$AnimatorListener;)V

    iget-object p1, p0, Lcom/miui/home/launcher/gadget/NormalClearButton;->mClearIcon:Lcom/miui/home/launcher/common/ClearIconImageView;

    invoke-virtual {p1}, Lcom/miui/home/launcher/common/ClearIconImageView;->setClearByAnimator()V

    return-void
.end method
