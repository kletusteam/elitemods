.class Lcom/miui/home/launcher/search/SearchEdgeEffect$1;
.super Lcom/miui/home/launcher/search/SearchEdgeEffect$Position;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/home/launcher/search/SearchEdgeEffect;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/miui/home/launcher/search/SearchEdgeEffect$Position;-><init>()V

    return-void
.end method


# virtual methods
.method getCurveLimitAndOffset(FF)Landroid/util/Pair;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(FF)",
            "Landroid/util/Pair<",
            "Ljava/lang/Float;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    goto/32 :goto_1

    nop

    :goto_0
    new-instance v0, Landroid/util/Pair;

    goto/32 :goto_5

    nop

    :goto_1
    invoke-static {}, Lcom/miui/home/launcher/Application;->getInstance()Lcom/miui/home/launcher/Application;

    move-result-object p2

    goto/32 :goto_4

    nop

    :goto_2
    add-float/2addr p1, v0

    :goto_3
    goto/32 :goto_0

    nop

    :goto_4
    invoke-static {p2}, Lcom/miui/home/launcher/common/Utilities;->getStatusBarHeight(Landroid/content/Context;)I

    move-result p2

    goto/32 :goto_b

    nop

    :goto_5
    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p1

    goto/32 :goto_a

    nop

    :goto_6
    invoke-static {}, Lcom/miui/home/launcher/DeviceConfig;->doLauncherHavePaddingTopForStatusBarAndNotch()Z

    move-result v0

    goto/32 :goto_7

    nop

    :goto_7
    if-nez v0, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_8

    nop

    :goto_8
    const/high16 v0, 0x40800000    # 4.0f

    goto/32 :goto_d

    nop

    :goto_9
    sub-float v0, p2, v0

    goto/32 :goto_2

    nop

    :goto_a
    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p2

    goto/32 :goto_c

    nop

    :goto_b
    int-to-float p2, p2

    goto/32 :goto_6

    nop

    :goto_c
    invoke-direct {v0, p1, p2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto/32 :goto_e

    nop

    :goto_d
    div-float v0, p2, v0

    goto/32 :goto_9

    nop

    :goto_e
    return-object v0
.end method

.method public getDeltaDistance(F)F
    .locals 0

    return p1
.end method

.method public getDrawRotate()F
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getVelocity(I)I
    .locals 0

    return p1
.end method
