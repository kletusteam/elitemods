.class Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$StaticBitmapLayer;
.super Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$StaticLayer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "StaticBitmapLayer"
.end annotation


# instance fields
.field private final mBitmap:Landroid/graphics/Bitmap;

.field private final mLayerShader:Landroid/graphics/BitmapShader;


# direct methods
.method private constructor <init>(Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/Bitmap;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p4, v0}, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$StaticLayer;-><init>(Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable;Landroid/graphics/drawable/Drawable;ILcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$1;)V

    iput-object p3, p0, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$StaticBitmapLayer;->mBitmap:Landroid/graphics/Bitmap;

    new-instance p1, Landroid/graphics/BitmapShader;

    sget-object p2, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    sget-object p4, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    invoke-direct {p1, p3, p2, p4}, Landroid/graphics/BitmapShader;-><init>(Landroid/graphics/Bitmap;Landroid/graphics/Shader$TileMode;Landroid/graphics/Shader$TileMode;)V

    iput-object p1, p0, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$StaticBitmapLayer;->mLayerShader:Landroid/graphics/BitmapShader;

    return-void
.end method

.method synthetic constructor <init>(Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/Bitmap;ILcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$1;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$StaticBitmapLayer;-><init>(Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/Bitmap;I)V

    return-void
.end method


# virtual methods
.method getShader()Landroid/graphics/Shader;
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return-object v0

    :goto_1
    iget-object v0, p0, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$StaticBitmapLayer;->mLayerShader:Landroid/graphics/BitmapShader;

    goto/32 :goto_0

    nop
.end method

.method onBoundsChange(Landroid/graphics/Rect;FF)V
    .locals 1

    goto/32 :goto_4

    nop

    :goto_0
    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result p2

    goto/32 :goto_13

    nop

    :goto_1
    int-to-float p2, p2

    goto/32 :goto_12

    nop

    :goto_2
    int-to-float p3, p3

    goto/32 :goto_1a

    nop

    :goto_3
    invoke-virtual {p2, p1}, Landroid/graphics/BitmapShader;->setLocalMatrix(Landroid/graphics/Matrix;)V

    goto/32 :goto_18

    nop

    :goto_4
    invoke-super {p0, p1, p2, p3}, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$StaticLayer;->onBoundsChange(Landroid/graphics/Rect;FF)V

    goto/32 :goto_d

    nop

    :goto_5
    invoke-virtual {p1, p2, v0}, Landroid/graphics/Matrix;->preScale(FF)Z

    goto/32 :goto_14

    nop

    :goto_6
    invoke-virtual {p0}, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$StaticBitmapLayer;->getDrawableBounds()Landroid/graphics/Rect;

    move-result-object v0

    goto/32 :goto_10

    nop

    :goto_7
    const/high16 p3, 0x3f800000    # 1.0f

    goto/32 :goto_17

    nop

    :goto_8
    int-to-float v0, v0

    goto/32 :goto_19

    nop

    :goto_9
    invoke-direct {p1}, Landroid/graphics/Matrix;-><init>()V

    goto/32 :goto_15

    nop

    :goto_a
    iget p2, p2, Landroid/graphics/Rect;->left:I

    goto/32 :goto_1

    nop

    :goto_b
    iget p3, p3, Landroid/graphics/Rect;->top:I

    goto/32 :goto_e

    nop

    :goto_c
    iget-object v0, p0, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$StaticBitmapLayer;->mBitmap:Landroid/graphics/Bitmap;

    goto/32 :goto_11

    nop

    :goto_d
    new-instance p1, Landroid/graphics/Matrix;

    goto/32 :goto_9

    nop

    :goto_e
    int-to-float p3, p3

    goto/32 :goto_1d

    nop

    :goto_f
    int-to-float v0, v0

    goto/32 :goto_16

    nop

    :goto_10
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    goto/32 :goto_f

    nop

    :goto_11
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    goto/32 :goto_8

    nop

    :goto_12
    invoke-virtual {p0}, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$StaticBitmapLayer;->getDrawableBounds()Landroid/graphics/Rect;

    move-result-object p3

    goto/32 :goto_b

    nop

    :goto_13
    int-to-float p2, p2

    goto/32 :goto_7

    nop

    :goto_14
    invoke-virtual {p0}, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$StaticBitmapLayer;->getDrawableBounds()Landroid/graphics/Rect;

    move-result-object p2

    goto/32 :goto_a

    nop

    :goto_15
    invoke-virtual {p0}, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$StaticBitmapLayer;->getDrawableBounds()Landroid/graphics/Rect;

    move-result-object p2

    goto/32 :goto_0

    nop

    :goto_16
    mul-float/2addr v0, p3

    goto/32 :goto_1b

    nop

    :goto_17
    mul-float/2addr p2, p3

    goto/32 :goto_c

    nop

    :goto_18
    return-void

    :goto_19
    div-float/2addr p2, v0

    goto/32 :goto_6

    nop

    :goto_1a
    div-float/2addr v0, p3

    goto/32 :goto_5

    nop

    :goto_1b
    iget-object p3, p0, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$StaticBitmapLayer;->mBitmap:Landroid/graphics/Bitmap;

    goto/32 :goto_1e

    nop

    :goto_1c
    iget-object p2, p0, Lcom/miui/home/launcher/graphics/drawable/LayerAdaptiveIconDrawable$StaticBitmapLayer;->mLayerShader:Landroid/graphics/BitmapShader;

    goto/32 :goto_3

    nop

    :goto_1d
    invoke-virtual {p1, p2, p3}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    goto/32 :goto_1c

    nop

    :goto_1e
    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result p3

    goto/32 :goto_2

    nop
.end method
