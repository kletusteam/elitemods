.class public Lcom/miui/home/launcher/debug/UsbDebugSwitchObserver;
.super Landroid/database/ContentObserver;


# static fields
.field private static volatile mIsUsbTurnedOn:Z


# instance fields
.field private mContext:Landroid/content/Context;

.field private mListener:Lcom/miui/home/launcher/debug/OnUsbSwitchListener;


# direct methods
.method public constructor <init>(Landroid/os/Handler;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    invoke-static {}, Lcom/miui/home/launcher/Application;->getInstance()Lcom/miui/home/launcher/Application;

    move-result-object p1

    iput-object p1, p0, Lcom/miui/home/launcher/debug/UsbDebugSwitchObserver;->mContext:Landroid/content/Context;

    invoke-direct {p0}, Lcom/miui/home/launcher/debug/UsbDebugSwitchObserver;->updateUsbSwitchState()V

    return-void
.end method

.method private addUsbSwitchListener(Lcom/miui/home/launcher/debug/OnUsbSwitchListener;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/home/launcher/debug/UsbDebugSwitchObserver;->mListener:Lcom/miui/home/launcher/debug/OnUsbSwitchListener;

    return-void
.end method

.method private getListener()Lcom/miui/home/launcher/debug/OnUsbSwitchListener;
    .locals 3

    iget-object v0, p0, Lcom/miui/home/launcher/debug/UsbDebugSwitchObserver;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    new-instance v0, Lcom/miui/home/launcher/debug/LauncherFinishReceiver;

    sget-boolean v1, Lcom/miui/home/launcher/debug/UsbDebugSwitchObserver;->mIsUsbTurnedOn:Z

    iget-object v2, p0, Lcom/miui/home/launcher/debug/UsbDebugSwitchObserver;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, v2}, Lcom/miui/home/launcher/debug/LauncherFinishReceiver;-><init>(ZLandroid/content/Context;)V

    return-object v0
.end method

.method public static isUsbTurnedOn()Z
    .locals 1

    sget-boolean v0, Lcom/miui/home/launcher/debug/UsbDebugSwitchObserver;->mIsUsbTurnedOn:Z

    return v0
.end method

.method private removeUsbSwitchListener(Landroid/content/Context;)V
    .locals 1

    iget-object v0, p0, Lcom/miui/home/launcher/debug/UsbDebugSwitchObserver;->mListener:Lcom/miui/home/launcher/debug/OnUsbSwitchListener;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-interface {v0, p1}, Lcom/miui/home/launcher/debug/OnUsbSwitchListener;->onCancelListener(Landroid/content/Context;)V

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/miui/home/launcher/debug/UsbDebugSwitchObserver;->mListener:Lcom/miui/home/launcher/debug/OnUsbSwitchListener;

    return-void
.end method

.method private updateUsbSwitchState()V
    .locals 3

    iget-object v0, p0, Lcom/miui/home/launcher/debug/UsbDebugSwitchObserver;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "adb_enabled"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v2, 0x1

    :cond_0
    sput-boolean v2, Lcom/miui/home/launcher/debug/UsbDebugSwitchObserver;->mIsUsbTurnedOn:Z

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 2

    invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V

    iget-object p1, p0, Lcom/miui/home/launcher/debug/UsbDebugSwitchObserver;->mListener:Lcom/miui/home/launcher/debug/OnUsbSwitchListener;

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/miui/home/launcher/debug/UsbDebugSwitchObserver;->mContext:Landroid/content/Context;

    if-nez p1, :cond_0

    goto :goto_1

    :cond_0
    invoke-direct {p0}, Lcom/miui/home/launcher/debug/UsbDebugSwitchObserver;->updateUsbSwitchState()V

    const-string p1, " UsbDebugSwitchObserver "

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " isUsbTurnedOn = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-boolean v1, Lcom/miui/home/launcher/debug/UsbDebugSwitchObserver;->mIsUsbTurnedOn:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-boolean p1, Lcom/miui/home/launcher/debug/UsbDebugSwitchObserver;->mIsUsbTurnedOn:Z

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/miui/home/launcher/debug/UsbDebugSwitchObserver;->mListener:Lcom/miui/home/launcher/debug/OnUsbSwitchListener;

    iget-object v0, p0, Lcom/miui/home/launcher/debug/UsbDebugSwitchObserver;->mContext:Landroid/content/Context;

    invoke-interface {p1, v0}, Lcom/miui/home/launcher/debug/OnUsbSwitchListener;->onUsbSwitchOn(Landroid/content/Context;)V

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lcom/miui/home/launcher/debug/UsbDebugSwitchObserver;->mListener:Lcom/miui/home/launcher/debug/OnUsbSwitchListener;

    iget-object v0, p0, Lcom/miui/home/launcher/debug/UsbDebugSwitchObserver;->mContext:Landroid/content/Context;

    invoke-interface {p1, v0}, Lcom/miui/home/launcher/debug/OnUsbSwitchListener;->onUsbSwitchOff(Landroid/content/Context;)V

    :goto_0
    return-void

    :cond_2
    :goto_1
    return-void
.end method

.method public registerUsbSwitchObserver()V
    .locals 3

    iget-object v0, p0, Lcom/miui/home/launcher/debug/UsbDebugSwitchObserver;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "adb_enabled"

    invoke-static {v1}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, p0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    invoke-direct {p0}, Lcom/miui/home/launcher/debug/UsbDebugSwitchObserver;->getListener()Lcom/miui/home/launcher/debug/OnUsbSwitchListener;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/miui/home/launcher/debug/UsbDebugSwitchObserver;->addUsbSwitchListener(Lcom/miui/home/launcher/debug/OnUsbSwitchListener;)V

    return-void
.end method

.method public unregisterUsbSwitchObserver()V
    .locals 1

    iget-object v0, p0, Lcom/miui/home/launcher/debug/UsbDebugSwitchObserver;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    iget-object v0, p0, Lcom/miui/home/launcher/debug/UsbDebugSwitchObserver;->mContext:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/miui/home/launcher/debug/UsbDebugSwitchObserver;->removeUsbSwitchListener(Landroid/content/Context;)V

    return-void
.end method
