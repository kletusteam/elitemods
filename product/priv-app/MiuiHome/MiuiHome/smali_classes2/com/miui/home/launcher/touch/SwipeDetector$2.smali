.class Lcom/miui/home/launcher/touch/SwipeDetector$2;
.super Lcom/miui/home/launcher/touch/SwipeDetector$Direction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/home/launcher/touch/SwipeDetector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/miui/home/launcher/touch/SwipeDetector$Direction;-><init>()V

    return-void
.end method


# virtual methods
.method getActiveTouchSlop(Landroid/view/MotionEvent;ILandroid/graphics/PointF;)F
    .locals 0

    goto/32 :goto_2

    nop

    :goto_0
    iget p2, p3, Landroid/graphics/PointF;->y:F

    goto/32 :goto_1

    nop

    :goto_1
    sub-float/2addr p1, p2

    goto/32 :goto_4

    nop

    :goto_2
    invoke-virtual {p1, p2}, Landroid/view/MotionEvent;->getY(I)F

    move-result p1

    goto/32 :goto_0

    nop

    :goto_3
    return p1

    :goto_4
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result p1

    goto/32 :goto_3

    nop
.end method

.method getDisplacement(Landroid/view/MotionEvent;ILandroid/graphics/PointF;)F
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    invoke-virtual {p1, p2}, Landroid/view/MotionEvent;->getX(I)F

    move-result p1

    goto/32 :goto_3

    nop

    :goto_1
    return p1

    :goto_2
    sub-float/2addr p1, p2

    goto/32 :goto_1

    nop

    :goto_3
    iget p2, p3, Landroid/graphics/PointF;->x:F

    goto/32 :goto_2

    nop
.end method
