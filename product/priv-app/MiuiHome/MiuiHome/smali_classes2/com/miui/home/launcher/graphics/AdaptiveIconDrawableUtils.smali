.class public Lcom/miui/home/launcher/graphics/AdaptiveIconDrawableUtils;
.super Ljava/lang/Object;


# static fields
.field private static mContext:Landroid/content/Context;

.field private static sCurrentColor:I

.field private static sMonoEnable:Z


# direct methods
.method public static getColor()I
    .locals 1

    sget v0, Lcom/miui/home/launcher/graphics/AdaptiveIconDrawableUtils;->sCurrentColor:I

    return v0
.end method

.method public static getMonochrome(Landroid/graphics/drawable/AdaptiveIconDrawable;)Landroid/graphics/drawable/Drawable;
    .locals 6

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v2, "getMonochrome"

    const-class v3, Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x0

    new-array v4, v1, [Ljava/lang/Class;

    new-array v5, v1, [Ljava/lang/Object;

    move-object v1, p0

    invoke-static/range {v0 .. v5}, Lcom/miui/launcher/utils/ReflectUtils;->invokeObject(Ljava/lang/Class;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Class;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/graphics/drawable/Drawable;

    return-object p0
.end method

.method public static init(Landroid/content/Context;)V
    .locals 2

    sput-object p0, Lcom/miui/home/launcher/graphics/AdaptiveIconDrawableUtils;->mContext:Landroid/content/Context;

    sget-object p0, Lcom/miui/home/launcher/graphics/AdaptiveIconDrawableUtils;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string v0, "key_monochrome"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/miui/launcher/utils/MiuiSettingsUtils;->getBooleanFromSystem(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result p0

    sput-boolean p0, Lcom/miui/home/launcher/graphics/AdaptiveIconDrawableUtils;->sMonoEnable:Z

    sget-object p0, Lcom/miui/home/launcher/graphics/AdaptiveIconDrawableUtils;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string v0, "key_monochrome_color"

    const/4 v1, -0x1

    invoke-static {p0, v0, v1}, Lcom/miui/launcher/utils/MiuiSettingsUtils;->getIntFromSystem(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result p0

    sput p0, Lcom/miui/home/launcher/graphics/AdaptiveIconDrawableUtils;->sCurrentColor:I

    return-void
.end method

.method public static isMonoEnable()Z
    .locals 1

    sget-boolean v0, Lcom/miui/home/launcher/graphics/AdaptiveIconDrawableUtils;->sMonoEnable:Z

    return v0
.end method

.method public static setCurrentColor(I)V
    .locals 0

    sput p0, Lcom/miui/home/launcher/graphics/AdaptiveIconDrawableUtils;->sCurrentColor:I

    return-void
.end method

.method public static setMonoEnable(Z)V
    .locals 0

    sput-boolean p0, Lcom/miui/home/launcher/graphics/AdaptiveIconDrawableUtils;->sMonoEnable:Z

    return-void
.end method
