.class public abstract Lcom/miui/home/launcher/convertsize/ItemIconConvertSizeController;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/miui/home/launcher/convertsize/ConvertSizeController;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nItemIconConvertSizeController.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ItemIconConvertSizeController.kt\ncom/miui/home/launcher/convertsize/ItemIconConvertSizeController\n*L\n1#1,24:1\n*E\n"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public convertFolderSize(Lcom/miui/home/launcher/FolderInfo;I)V
    .locals 1

    const-string v0, "info"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0, p1, p2}, Lcom/miui/home/launcher/convertsize/ConvertSizeController$DefaultImpls;->convertFolderSize(Lcom/miui/home/launcher/convertsize/ConvertSizeController;Lcom/miui/home/launcher/FolderInfo;I)V

    return-void
.end method

.method public final findAPositionOnLastScreen(Lcom/miui/home/launcher/convertsize/ItemPositionInfo;)Lcom/miui/home/launcher/convertsize/ItemPositionInfo;
    .locals 12

    const-string v0, "positionInfo"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Lcom/miui/home/launcher/Application;->getLauncher()Lcom/miui/home/launcher/Launcher;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/miui/home/launcher/Launcher;->getWorkspace()Lcom/miui/home/launcher/Workspace;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/miui/home/launcher/Workspace;->getScreenCount()I

    move-result v2

    const/4 v3, 0x1

    sub-int/2addr v2, v3

    invoke-virtual {v0, v2}, Lcom/miui/home/launcher/Workspace;->getCellLayout(I)Lcom/miui/home/launcher/CellLayout;

    move-result-object v2

    invoke-virtual {p1}, Lcom/miui/home/launcher/convertsize/ItemPositionInfo;->getSpanX()I

    move-result v4

    invoke-virtual {p1}, Lcom/miui/home/launcher/convertsize/ItemPositionInfo;->getSpanY()I

    move-result v5

    invoke-virtual {v2, v4, v5}, Lcom/miui/home/launcher/CellLayout;->findFirstVacantArea(II)[I

    move-result-object v2

    if-eqz v2, :cond_1

    new-instance v1, Lcom/miui/home/launcher/convertsize/ItemPositionInfo;

    const/4 v4, 0x0

    aget v5, v2, v4

    aget v6, v2, v3

    invoke-virtual {p1}, Lcom/miui/home/launcher/convertsize/ItemPositionInfo;->getSpanX()I

    move-result v7

    invoke-virtual {p1}, Lcom/miui/home/launcher/convertsize/ItemPositionInfo;->getSpanY()I

    move-result v8

    invoke-virtual {v0}, Lcom/miui/home/launcher/Workspace;->getScreenCount()I

    move-result v2

    sub-int/2addr v2, v3

    invoke-virtual {v0, v2}, Lcom/miui/home/launcher/Workspace;->getScreenIdByIndex(I)J

    move-result-wide v9

    invoke-virtual {p1}, Lcom/miui/home/launcher/convertsize/ItemPositionInfo;->getItemType()I

    move-result v11

    move-object v4, v1

    invoke-direct/range {v4 .. v11}, Lcom/miui/home/launcher/convertsize/ItemPositionInfo;-><init>(IIIIJI)V

    return-object v1

    :cond_1
    move-object p1, v1

    check-cast p1, Ljava/lang/Void;

    :cond_2
    return-object v1
.end method

.method public final isIconBigger(IIII)Z
    .locals 0

    if-gt p3, p1, :cond_1

    if-le p4, p2, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method
