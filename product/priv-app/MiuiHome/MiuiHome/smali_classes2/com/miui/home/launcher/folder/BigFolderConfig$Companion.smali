.class public final Lcom/miui/home/launcher/folder/BigFolderConfig$Companion;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/home/launcher/folder/BigFolderConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBigFolderConfig.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BigFolderConfig.kt\ncom/miui/home/launcher/folder/BigFolderConfig$Companion\n*L\n1#1,34:1\n*E\n"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/home/launcher/folder/BigFolderConfig$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final bigFolderIconHeight(Landroid/content/Context;II)I
    .locals 1

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    invoke-static {p2, p3, v0}, Lcom/miui/home/launcher/DeviceConfig;->getMiuiWidgetSizeSpec(IIZ)J

    move-result-wide p2

    invoke-static {p1}, Lcom/miui/home/launcher/MIUIWidgetUtil;->getMiuiWidgetPadding(Landroid/content/Context;)Landroid/graphics/Rect;

    move-result-object p1

    long-to-int p2, p2

    iget p3, p1, Landroid/graphics/Rect;->top:I

    sub-int/2addr p2, p3

    iget p1, p1, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr p2, p1

    return p2

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public final bigFolderPaddingTop(Landroid/content/Context;)I
    .locals 2

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0700ab

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const v1, 0x7f0701d2

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result p1

    add-float/2addr v0, p1

    float-to-int p1, v0

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method
