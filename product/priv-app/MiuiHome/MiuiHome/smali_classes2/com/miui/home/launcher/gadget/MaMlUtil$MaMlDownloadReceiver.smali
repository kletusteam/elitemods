.class Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver;
.super Landroid/content/BroadcastReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/home/launcher/gadget/MaMlUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MaMlDownloadReceiver"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver$_lancet;
    }
.end annotation


# static fields
.field private static volatile sReceiver:Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver;


# instance fields
.field private volatile mDownloadStatus:I

.field private final maMlDownloadListener:Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadListener;

.field private final maMlDownloadParams:Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadParams;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadListener;Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadParams;)V
    .locals 0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    iput-object p1, p0, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver;->maMlDownloadListener:Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadListener;

    iput-object p2, p0, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver;->maMlDownloadParams:Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadParams;

    iget-object p1, p0, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver;->maMlDownloadListener:Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadListener;

    const/4 p2, 0x1

    invoke-interface {p1, p2}, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadListener;->onDownloadStatusChange(I)V

    return-void
.end method

.method static synthetic access$000(Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver;Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver;->onReceive$___twin___(Landroid/content/Context;Landroid/content/Intent;)V

    return-void
.end method

.method private fromError(Ljava/lang/Exception;)Lcom/miui/maml/widget/edit/MamlDownloadStatus;
    .locals 7

    new-instance v6, Lcom/miui/maml/widget/edit/MamlDownloadStatus;

    iget-object v0, p0, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver;->maMlDownloadParams:Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadParams;

    invoke-static {v0}, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadParams;->access$300(Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadParams;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver;->maMlDownloadParams:Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadParams;

    invoke-static {v0}, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadParams;->access$400(Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadParams;)I

    move-result v2

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    const/4 v3, 0x3

    const/4 v4, -0x1

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/miui/maml/widget/edit/MamlDownloadStatus;-><init>(Ljava/lang/String;IIILjava/lang/String;)V

    return-object v6
.end method

.method private handle(Landroid/content/Context;Lcom/miui/maml/widget/edit/MamlDownloadStatus;)V
    .locals 2

    invoke-virtual {p2}, Lcom/miui/maml/widget/edit/MamlDownloadStatus;->getState()I

    move-result p1

    packed-switch p1, :pswitch_data_0

    goto/16 :goto_0

    :pswitch_0
    const-string p1, "MaMlUtil"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MaMlDownload "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver;->maMlDownloadParams:Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadParams;

    invoke-static {v1}, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadParams;->access$100(Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadParams;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " err:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lcom/miui/maml/widget/edit/MamlDownloadStatus;->getErrorMsg()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget p1, p0, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver;->mDownloadStatus:I

    const/4 p2, 0x5

    if-eq p1, p2, :cond_1

    iput p2, p0, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver;->mDownloadStatus:I

    iget-object p1, p0, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver;->maMlDownloadListener:Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadListener;

    iget p2, p0, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver;->mDownloadStatus:I

    invoke-interface {p1, p2}, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadListener;->onDownloadStatusChange(I)V

    goto :goto_0

    :pswitch_1
    const-string p1, "MaMlUtil"

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "MaMlDownload done"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver;->maMlDownloadParams:Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadParams;

    invoke-static {v0}, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadParams;->access$100(Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadParams;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget p1, p0, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver;->mDownloadStatus:I

    const/4 p2, 0x4

    if-eq p1, p2, :cond_1

    iput p2, p0, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver;->mDownloadStatus:I

    iget-object p1, p0, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver;->maMlDownloadListener:Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadListener;

    iget p2, p0, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver;->mDownloadStatus:I

    invoke-interface {p1, p2}, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadListener;->onDownloadStatusChange(I)V

    goto :goto_0

    :pswitch_2
    iget p1, p0, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver;->mDownloadStatus:I

    const/4 p2, 0x3

    if-eq p1, p2, :cond_1

    iput p2, p0, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver;->mDownloadStatus:I

    iget-object p1, p0, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver;->maMlDownloadListener:Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadListener;

    iget p2, p0, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver;->mDownloadStatus:I

    invoke-interface {p1, p2}, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadListener;->onDownloadStatusChange(I)V

    goto :goto_0

    :pswitch_3
    iget p1, p0, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver;->mDownloadStatus:I

    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    iput v0, p0, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver;->mDownloadStatus:I

    iget-object p1, p0, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver;->maMlDownloadListener:Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadListener;

    iget v0, p0, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver;->mDownloadStatus:I

    invoke-interface {p1, v0}, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadListener;->onDownloadStatusChange(I)V

    :cond_0
    iget-object p1, p0, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver;->maMlDownloadListener:Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadListener;

    invoke-virtual {p2}, Lcom/miui/maml/widget/edit/MamlDownloadStatus;->getProgressPercent()I

    move-result p2

    invoke-interface {p1, p2}, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadListener;->onDownloadProgress(I)V

    :cond_1
    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static synthetic lambda$download$0(Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver;Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver;->timeOut(Landroid/content/Context;)V

    return-void
.end method

.method private onReceive(Landroid/content/Context;Lcom/miui/maml/widget/edit/MamlDownloadStatus;)V
    .locals 5

    const-string v0, "MaMlUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onReceive product = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lcom/miui/maml/widget/edit/MamlDownloadStatus;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " status = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lcom/miui/maml/widget/edit/MamlDownloadStatus;->getState()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {}, Lcom/miui/home/launcher/gadget/MaMlUtil;->access$000()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    invoke-static {}, Lcom/miui/home/launcher/gadget/MaMlUtil;->access$000()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v2

    invoke-virtual {p2}, Lcom/miui/maml/widget/edit/MamlDownloadStatus;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    if-eqz v2, :cond_0

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_7

    invoke-virtual {p2}, Lcom/miui/maml/widget/edit/MamlDownloadStatus;->getState()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver;->maMlDownloadParams:Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadParams;

    invoke-static {v1}, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadParams;->access$200(Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadParams;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "file://"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p2}, Lcom/miui/maml/widget/edit/MamlDownloadStatus;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/miui/maml/widget/edit/MamlDownloadStatus;->getMamlVersion()I

    move-result v3

    invoke-static {p1, v1, v3}, Lcom/miui/home/launcher/widget/MIUIWidgetCompat;->installMaMlFromThemeApp(Landroid/content/Context;Ljava/lang/String;I)Z

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver;->maMlDownloadParams:Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadParams;

    invoke-static {v1}, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadParams;->access$200(Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadParams;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v3, p0, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver;->maMlDownloadParams:Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadParams;

    invoke-static {v3}, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadParams;->access$300(Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadParams;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver;->maMlDownloadParams:Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadParams;

    invoke-static {v4}, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadParams;->access$400(Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadParams;)I

    move-result v4

    invoke-static {p1, v1, v3, v4}, Lcom/miui/home/launcher/widget/MIUIWidgetCompat;->installMaMlFromExternal(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;I)Z

    :cond_2
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver;

    invoke-direct {v1, p1, p2}, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver;->handle(Landroid/content/Context;Lcom/miui/maml/widget/edit/MamlDownloadStatus;)V

    goto :goto_1

    :cond_3
    invoke-virtual {p2}, Lcom/miui/maml/widget/edit/MamlDownloadStatus;->getState()I

    move-result v0

    if-eq v0, v2, :cond_4

    invoke-virtual {p2}, Lcom/miui/maml/widget/edit/MamlDownloadStatus;->getState()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_5

    :cond_4
    invoke-static {}, Lcom/miui/home/launcher/gadget/MaMlUtil;->access$000()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v0

    invoke-virtual {p2}, Lcom/miui/maml/widget/edit/MamlDownloadStatus;->getId()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p2}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_5
    invoke-static {}, Lcom/miui/home/launcher/gadget/MaMlUtil;->access$000()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object p2

    monitor-enter p2

    :try_start_1
    invoke-static {}, Lcom/miui/home/launcher/gadget/MaMlUtil;->access$000()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    sget-object v0, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver;->sReceiver:Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_6

    :try_start_2
    sget-object v0, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver;->sReceiver:Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver;

    invoke-virtual {p1, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catch_0
    const/4 p1, 0x0

    :try_start_3
    sput-object p1, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver;->sReceiver:Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver;

    :cond_6
    monitor-exit p2

    goto :goto_2

    :catchall_0
    move-exception p1

    monitor-exit p2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw p1

    :cond_7
    :goto_2
    return-void

    :catchall_1
    move-exception p1

    :try_start_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw p1
.end method

.method private onReceive$___twin___(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0

    invoke-static {p2}, Lcom/miui/maml/widget/edit/MamlDownloadStatusKt;->createDownloadStatus(Landroid/content/Intent;)Lcom/miui/maml/widget/edit/MamlDownloadStatus;

    move-result-object p2

    invoke-direct {p0, p1, p2}, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver;->onReceive(Landroid/content/Context;Lcom/miui/maml/widget/edit/MamlDownloadStatus;)V

    return-void
.end method

.method private timeOut(Landroid/content/Context;)V
    .locals 2

    iget v0, p0, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver;->mDownloadStatus:I

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/concurrent/TimeoutException;

    const-string v1, "Maml Download timeout"

    invoke-direct {v0, v1}, Ljava/util/concurrent/TimeoutException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver;->fromError(Ljava/lang/Exception;)Lcom/miui/maml/widget/edit/MamlDownloadStatus;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver;->onReceive(Landroid/content/Context;Lcom/miui/maml/widget/edit/MamlDownloadStatus;)V

    :cond_0
    return-void
.end method


# virtual methods
.method download(Landroid/content/Context;)V
    .locals 13

    goto/32 :goto_1c

    nop

    :goto_0
    invoke-static {v1}, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadParams;->access$400(Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadParams;)I

    move-result v9

    goto/32 :goto_10

    nop

    :goto_1
    move-object v1, p1

    goto/32 :goto_22

    nop

    :goto_2
    return-void

    :goto_3
    invoke-static {v0}, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadParams;->access$200(Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadParams;)Ljava/lang/String;

    move-result-object v4

    goto/32 :goto_1b

    nop

    :goto_4
    const-string v1, "file://"

    goto/32 :goto_23

    nop

    :goto_5
    iget-object v1, p0, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver;->maMlDownloadParams:Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadParams;

    goto/32 :goto_0

    nop

    :goto_6
    invoke-direct {p0, p1, v0}, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver;->onReceive(Landroid/content/Context;Lcom/miui/maml/widget/edit/MamlDownloadStatus;)V

    :goto_7
    goto/32 :goto_1d

    nop

    :goto_8
    const/16 v11, 0x64

    goto/32 :goto_e

    nop

    :goto_9
    invoke-direct/range {v7 .. v12}, Lcom/miui/maml/widget/edit/MamlDownloadStatus;-><init>(Ljava/lang/String;IIILjava/lang/String;)V

    goto/32 :goto_6

    nop

    :goto_a
    iget-object v1, p0, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver;->maMlDownloadParams:Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadParams;

    goto/32 :goto_13

    nop

    :goto_b
    if-eqz v0, :cond_0

    goto/32 :goto_1a

    :cond_0
    goto/32 :goto_14

    nop

    :goto_c
    move-object v7, v0

    goto/32 :goto_9

    nop

    :goto_d
    invoke-direct {v0, p0, p1}, Lcom/miui/home/launcher/gadget/-$$Lambda$MaMlUtil$MaMlDownloadReceiver$iZ32Zf3J2yQG4uhuQG9Jec6rFps;-><init>(Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver;Landroid/content/Context;)V

    goto/32 :goto_f

    nop

    :goto_e
    const-string v12, ""

    goto/32 :goto_c

    nop

    :goto_f
    const-wide/16 v1, 0x7530

    goto/32 :goto_1f

    nop

    :goto_10
    const/4 v10, 0x2

    goto/32 :goto_8

    nop

    :goto_11
    iget-object v0, p0, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver;->maMlDownloadParams:Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadParams;

    goto/32 :goto_3

    nop

    :goto_12
    invoke-static {v0}, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadParams;->access$200(Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadParams;)Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_4

    nop

    :goto_13
    invoke-static {v1}, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadParams;->access$300(Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadParams;)Ljava/lang/String;

    move-result-object v8

    goto/32 :goto_5

    nop

    :goto_14
    iget-object v0, p0, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver;->maMlDownloadParams:Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadParams;

    goto/32 :goto_21

    nop

    :goto_15
    new-instance v0, Lcom/miui/maml/widget/edit/MamlDownloadStatus;

    goto/32 :goto_a

    nop

    :goto_16
    invoke-static {v0}, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadParams;->access$400(Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadParams;)I

    move-result v5

    goto/32 :goto_20

    nop

    :goto_17
    invoke-static {v0}, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadParams;->access$100(Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadParams;)Ljava/lang/String;

    move-result-object v3

    goto/32 :goto_11

    nop

    :goto_18
    iget-object v0, p0, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver;->maMlDownloadParams:Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadParams;

    goto/32 :goto_17

    nop

    :goto_19
    goto :goto_7

    :goto_1a
    goto/32 :goto_15

    nop

    :goto_1b
    iget-object v0, p0, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver;->maMlDownloadParams:Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadParams;

    goto/32 :goto_16

    nop

    :goto_1c
    iget-object v0, p0, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver;->maMlDownloadParams:Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadParams;

    goto/32 :goto_12

    nop

    :goto_1d
    new-instance v0, Lcom/miui/home/launcher/gadget/-$$Lambda$MaMlUtil$MaMlDownloadReceiver$iZ32Zf3J2yQG4uhuQG9Jec6rFps;

    goto/32 :goto_d

    nop

    :goto_1e
    invoke-static {v0}, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadParams;->access$500(Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadParams;)I

    move-result v6

    goto/32 :goto_1

    nop

    :goto_1f
    invoke-static {v0, v1, v2}, Lcom/miui/home/launcher/common/BackgroundThread;->postDelayed(Ljava/lang/Runnable;J)V

    goto/32 :goto_2

    nop

    :goto_20
    iget-object v0, p0, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver;->maMlDownloadParams:Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadParams;

    goto/32 :goto_1e

    nop

    :goto_21
    invoke-static {v0}, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadParams;->access$300(Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadParams;)Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_18

    nop

    :goto_22
    invoke-static/range {v1 .. v6}, Lcom/miui/maml/widget/edit/MamlutilKt;->startDownloadMamlAndRight(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)I

    goto/32 :goto_19

    nop

    :goto_23
    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    goto/32 :goto_b

    nop
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver$_lancet;->com_miui_home_launcher_aop_BroadcastReceiverHooker_onReceive(Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver;Landroid/content/Context;Landroid/content/Intent;)V

    return-void
.end method

.method startMaMlDownload(Landroid/content/Context;)V
    .locals 4

    goto/32 :goto_e

    nop

    :goto_0
    return-void

    :catchall_0
    move-exception p1

    :try_start_0
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/32 :goto_2

    nop

    :goto_1
    const-string v1, "MaMlUtil"

    goto/32 :goto_10

    nop

    :goto_2
    throw p1

    :goto_3
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_5

    nop

    :goto_4
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_a

    nop

    :goto_5
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_4

    nop

    :goto_6
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_f

    nop

    :goto_7
    invoke-direct {p0, p1, v0}, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver;->onReceive(Landroid/content/Context;Lcom/miui/maml/widget/edit/MamlDownloadStatus;)V

    :goto_8
    goto/32 :goto_0

    nop

    :goto_9
    const-string v3, "download err :"

    goto/32 :goto_6

    nop

    :goto_a
    invoke-direct {p0, v0}, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver;->fromError(Ljava/lang/Exception;)Lcom/miui/maml/widget/edit/MamlDownloadStatus;

    move-result-object v0

    goto/32 :goto_7

    nop

    :goto_b
    monitor-enter v0

    :try_start_1
    sget-object v1, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver;->sReceiver:Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver;

    if-nez v1, :cond_0

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "com.android.thememanager.ACTION.maml.download"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, p0, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    sput-object p0, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver;->sReceiver:Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver;

    :cond_0
    const-string v1, "MaMlUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "startMaMlDownload "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver;->maMlDownloadParams:Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadParams;

    invoke-static {v3}, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadParams;->access$100(Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadParams;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " url = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver;->maMlDownloadParams:Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadParams;

    invoke-static {v3}, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadParams;->access$200(Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadParams;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/miui/home/launcher/gadget/MaMlUtil;->access$000()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver;->maMlDownloadParams:Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadParams;

    invoke-static {v2}, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadParams;->access$300(Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadParams;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-static {}, Lcom/miui/home/launcher/gadget/MaMlUtil;->access$000()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v2

    iget-object v3, p0, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver;->maMlDownloadParams:Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadParams;

    invoke-static {v3}, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadParams;->access$300(Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadParams;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    invoke-interface {v1, p0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string p1, "MaMlUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "download "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver;->maMlDownloadParams:Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadParams;

    invoke-static {v2}, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadParams;->access$100(Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadParams;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "already started"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit v0

    return-void

    :cond_2
    invoke-interface {v1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-virtual {p0, p1}, Lcom/miui/home/launcher/gadget/MaMlUtil$MaMlDownloadReceiver;->download(Landroid/content/Context;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto/32 :goto_d

    nop

    :goto_c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_9

    nop

    :goto_d
    goto/16 :goto_8

    :catch_0
    move-exception v0

    goto/32 :goto_1

    nop

    :goto_e
    invoke-static {}, Lcom/miui/home/launcher/gadget/MaMlUtil;->access$000()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v0

    goto/32 :goto_b

    nop

    :goto_f
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    goto/32 :goto_3

    nop

    :goto_10
    new-instance v2, Ljava/lang/StringBuilder;

    goto/32 :goto_c

    nop
.end method
