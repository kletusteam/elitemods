.class public final Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;
.super Landroid/view/ViewGroup;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;,
        Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nFolderIconPreviewContainer2X2.kt\nKotlin\n*S Kotlin\n*F\n+ 1 FolderIconPreviewContainer2X2.kt\ncom/miui/home/launcher/folder/FolderIconPreviewContainer2X2\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,907:1\n1819#2,2:908\n1819#2,2:910\n1828#2,3:912\n1828#2,3:915\n*E\n*S KotlinDebug\n*F\n+ 1 FolderIconPreviewContainer2X2.kt\ncom/miui/home/launcher/folder/FolderIconPreviewContainer2X2\n*L\n851#1,2:908\n858#1,2:910\n880#1,3:912\n896#1,3:915\n*E\n"
.end annotation


# static fields
.field public static final Companion:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$Companion;

# The value of this static final field might be set in the static constructor
.field public static final ITEMS_MAX_COUNT_2X2:I = 0x7


# instance fields
.field private final LAYOUT_DEBUGABLE:Z

.field private final MAX_LARGE_ICON_NUM:I

.field private final TAG:Ljava/lang/String;

.field private final m2x2LargeItemMergeEdgePercent:F

.field private final m2x2LargeItemMergeInnerPercent:F

.field private final m2x2SmallItemMergeInnerPercent:F

.field private mFolderIconPlaceholderDrawable:Lcom/miui/home/launcher/folder/FolderIconPlaceholderDrawable;

.field private mHeightMeasureSpec:I

.field private final mHideAnimConfig:Lmiuix/animation/base/AnimConfig;

.field private mLarge2x2ItemMergeEdgeHor:I

.field private mLarge2x2ItemMergeEdgeVer:I

.field private mLarge2x2ItemMergeInnerHor:I

.field private mLarge2x2ItemMergeInnerVer:I

.field private mLargeItemHeight:I

.field private mLargeItemWith:I

.field private mPvChildList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;",
            ">;"
        }
    .end annotation
.end field

.field private mPvItemLocationInfoList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/miui/home/launcher/folder/FolderIconPreviewInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mRealPvChildCount:I

.field private final mShowAnimAlphaConfig:Lmiuix/animation/base/AnimSpecialConfig;

.field private final mShowAnimScaleConfig:Lmiuix/animation/base/AnimSpecialConfig;

.field private mSmall2x2ItemMergeInner:I

.field private mSmallItemHeight:I

.field private mSmallItemWith:I

.field private mWidthMeasureSpec:I

.field private previewPlaceholderShouldAnim:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->Companion:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$Companion;

    const/4 v0, 0x7

    sput v0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->ITEMS_MAX_COUNT_2X2:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const-string p1, "FolderIconPreviewContainer2X2"

    iput-object p1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->TAG:Ljava/lang/String;

    const/4 p1, 0x3

    iput p1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->MAX_LARGE_ICON_NUM:I

    const p1, 0x3db43958    # 0.088f

    iput p1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->m2x2LargeItemMergeEdgePercent:F

    const p1, 0x3d99999a    # 0.075f

    iput p1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->m2x2LargeItemMergeInnerPercent:F

    const p1, 0x3ccccccd    # 0.025f

    iput p1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->m2x2SmallItemMergeInnerPercent:F

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    check-cast p1, Ljava/util/List;

    iput-object p1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mPvChildList:Ljava/util/List;

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    check-cast p1, Ljava/util/List;

    iput-object p1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mPvItemLocationInfoList:Ljava/util/List;

    invoke-static {}, Lcom/miui/home/launcher/DeviceConfig;->isDefaultIcon()Z

    move-result p1

    if-eqz p1, :cond_0

    new-instance p1, Lcom/miui/home/launcher/folder/FolderIconPlaceholderDrawable;

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->getContext()Landroid/content/Context;

    move-result-object p2

    const-string p3, "context"

    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p1, p2}, Lcom/miui/home/launcher/folder/FolderIconPlaceholderDrawable;-><init>(Landroid/content/Context;)V

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput-object p1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mFolderIconPlaceholderDrawable:Lcom/miui/home/launcher/folder/FolderIconPlaceholderDrawable;

    new-instance p1, Lmiuix/animation/base/AnimSpecialConfig;

    invoke-direct {p1}, Lmiuix/animation/base/AnimSpecialConfig;-><init>()V

    const/4 p2, 0x2

    new-array p3, p2, [F

    fill-array-data p3, :array_0

    const/4 v0, -0x2

    invoke-virtual {p1, v0, p3}, Lmiuix/animation/base/AnimSpecialConfig;->setEase(I[F)Lmiuix/animation/base/AnimConfig;

    move-result-object p1

    if-eqz p1, :cond_2

    check-cast p1, Lmiuix/animation/base/AnimSpecialConfig;

    iput-object p1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mShowAnimAlphaConfig:Lmiuix/animation/base/AnimSpecialConfig;

    new-instance p1, Lmiuix/animation/base/AnimSpecialConfig;

    invoke-direct {p1}, Lmiuix/animation/base/AnimSpecialConfig;-><init>()V

    new-array p3, p2, [F

    fill-array-data p3, :array_1

    invoke-virtual {p1, v0, p3}, Lmiuix/animation/base/AnimSpecialConfig;->setEase(I[F)Lmiuix/animation/base/AnimConfig;

    move-result-object p1

    if-eqz p1, :cond_1

    check-cast p1, Lmiuix/animation/base/AnimSpecialConfig;

    iput-object p1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mShowAnimScaleConfig:Lmiuix/animation/base/AnimSpecialConfig;

    new-instance p1, Lmiuix/animation/base/AnimConfig;

    invoke-direct {p1}, Lmiuix/animation/base/AnimConfig;-><init>()V

    new-array p2, p2, [F

    fill-array-data p2, :array_2

    invoke-virtual {p1, v0, p2}, Lmiuix/animation/base/AnimConfig;->setEase(I[F)Lmiuix/animation/base/AnimConfig;

    move-result-object p1

    const-string p2, "AnimConfig()\n        .se\u2026.SPRING_PHY, 0.95f, 0.2f)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mHideAnimConfig:Lmiuix/animation/base/AnimConfig;

    return-void

    :cond_1
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type miuix.animation.base.AnimSpecialConfig"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type miuix.animation.base.AnimSpecialConfig"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :array_0
    .array-data 4
        0x3f666666    # 0.9f
        0x3e99999a    # 0.3f
    .end array-data

    :array_1
    .array-data 4
        0x3f266666    # 0.65f
        0x3e99999a    # 0.3f
    .end array-data

    :array_2
    .array-data 4
        0x3f733333    # 0.95f
        0x3e4ccccd    # 0.2f
    .end array-data
.end method

.method public static final synthetic access$getMFolderIconPlaceholderDrawable$p(Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;)Lcom/miui/home/launcher/folder/FolderIconPlaceholderDrawable;
    .locals 0

    iget-object p0, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mFolderIconPlaceholderDrawable:Lcom/miui/home/launcher/folder/FolderIconPlaceholderDrawable;

    return-object p0
.end method

.method public static final synthetic access$getTAG$p(Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->TAG:Ljava/lang/String;

    return-object p0
.end method

.method public static final synthetic access$runLoadAction(Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;Lcom/miui/home/launcher/FolderInfo;Lcom/miui/home/launcher/IconCache;ZLcom/miui/home/library/utils/AsyncTaskExecutorHelper$SerialExecutor;Z)V
    .locals 0

    invoke-direct/range {p0 .. p5}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->runLoadAction(Lcom/miui/home/launcher/FolderInfo;Lcom/miui/home/launcher/IconCache;ZLcom/miui/home/library/utils/AsyncTaskExecutorHelper$SerialExecutor;Z)V

    return-void
.end method

.method public static final synthetic access$showPreviewIcon(Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->showPreviewIcon()V

    return-void
.end method

.method private final addPlaceholderInternal()V
    .locals 3

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mPvChildList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sget v1, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->ITEMS_MAX_COUNT_2X2:I

    if-lt v0, v1, :cond_0

    return-void

    :cond_0
    new-instance v0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "context"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mPvChildList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    check-cast v0, Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->addView(Landroid/view/View;)V

    return-void
.end method

.method private final executePlaceholderAddAnim(Z)V
    .locals 4

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    iput-boolean v0, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->previewPlaceholderShouldAnim:Z

    goto :goto_2

    :cond_0
    iget-boolean p1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->previewPlaceholderShouldAnim:Z

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mPvChildList:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    sget v1, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->ITEMS_MAX_COUNT_2X2:I

    if-ne p1, v1, :cond_1

    const/4 p1, 0x0

    check-cast p1, Landroid/widget/ImageView;

    iput-boolean v0, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->previewPlaceholderShouldAnim:Z

    :try_start_0
    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mPvChildList:Ljava/util/List;

    const/4 v1, 0x4

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    iget-object v1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mPvChildList:Ljava/util/List;

    const/4 v2, 0x5

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :try_start_2
    iget-object v2, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mPvChildList:Ljava/util/List;

    const/4 v3, 0x6

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    move-object p1, v2

    goto :goto_1

    :catch_0
    move-object v1, p1

    goto :goto_0

    :catch_1
    move-object v0, p1

    move-object v1, v0

    :catch_2
    :goto_0
    iget-object v2, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->TAG:Ljava/lang/String;

    const-string v3, "an error is occurred on placeholder anim showing"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    invoke-direct {p0, v0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->executePlaceholderAddAnimInternal(Landroid/widget/ImageView;)V

    invoke-direct {p0, v1}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->executePlaceholderAddAnimInternal(Landroid/widget/ImageView;)V

    invoke-direct {p0, p1}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->executePlaceholderAddAnimInternal(Landroid/widget/ImageView;)V

    :cond_1
    :goto_2
    return-void
.end method

.method private final executePlaceholderAddAnimInternal(Landroid/widget/ImageView;)V
    .locals 6

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setAlpha(F)V

    invoke-virtual {p1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mFolderIconPlaceholderDrawable:Lcom/miui/home/launcher/folder/FolderIconPlaceholderDrawable;

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x1

    xor-int/2addr v1, v2

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mFolderIconPlaceholderDrawable:Lcom/miui/home/launcher/folder/FolderIconPlaceholderDrawable;

    check-cast v1, Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    new-array v1, v2, [Landroid/view/View;

    check-cast p1, Landroid/view/View;

    const/4 v3, 0x0

    aput-object p1, v1, v3

    invoke-static {v1}, Lmiuix/animation/Folme;->useAt([Landroid/view/View;)Lmiuix/animation/IFolme;

    move-result-object p1

    invoke-interface {p1}, Lmiuix/animation/IFolme;->state()Lmiuix/animation/IStateStyle;

    move-result-object p1

    const/4 v1, 0x2

    new-array v4, v1, [Ljava/lang/Object;

    sget-object v5, Lmiuix/animation/property/ViewProperty;->ALPHA:Lmiuix/animation/property/ViewProperty;

    aput-object v5, v4, v3

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    aput-object v0, v4, v2

    invoke-interface {p1, v4}, Lmiuix/animation/IStateStyle;->setTo([Ljava/lang/Object;)Lmiuix/animation/IStateStyle;

    move-result-object p1

    new-array v0, v1, [Ljava/lang/Object;

    sget-object v1, Lmiuix/animation/property/ViewProperty;->ALPHA:Lmiuix/animation/property/ViewProperty;

    aput-object v1, v0, v3

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-interface {p1, v0}, Lmiuix/animation/IStateStyle;->to([Ljava/lang/Object;)Lmiuix/animation/IStateStyle;

    :cond_1
    return-void
.end method

.method private final folderIconPlaceholderDrawableMeasure()V
    .locals 2

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mPvItemLocationInfoList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sget v1, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->ITEMS_MAX_COUNT_2X2:I

    add-int/lit8 v1, v1, -0x1

    if-gt v0, v1, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mFolderIconPlaceholderDrawable:Lcom/miui/home/launcher/folder/FolderIconPlaceholderDrawable;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/miui/home/launcher/folder/FolderIconPlaceholderDrawable;->calcDrawableParams()V

    :cond_1
    return-void
.end method

.method private final getShortcutInfoSafety(Lcom/miui/home/launcher/FolderInfo;I)Lcom/miui/home/launcher/ShortcutInfo;
    .locals 2

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    :try_start_0
    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/miui/home/launcher/FolderInfo;->getAdapter(Landroid/content/Context;)Lcom/miui/home/launcher/ShortcutsAdapter;

    move-result-object p1

    invoke-virtual {p1, p2}, Lcom/miui/home/launcher/ShortcutsAdapter;->getItem(I)Lcom/miui/home/launcher/ShortcutInfo;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-object p1, v0

    :goto_0
    if-eqz p1, :cond_0

    move-object v0, p1

    :cond_0
    return-object v0
.end method

.method private final hidePreviewIcon(Lmiuix/animation/listener/TransitionListener;)V
    .locals 8

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mPvChildList:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    add-int/lit8 v4, v2, 0x1

    if-gez v2, :cond_0

    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwIndexOverflow()V

    :cond_0
    check-cast v3, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;

    iget-object v5, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mHideAnimConfig:Lmiuix/animation/base/AnimConfig;

    iget v6, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mRealPvChildCount:I

    const/4 v7, 0x1

    sub-int/2addr v6, v7

    if-ne v2, v6, :cond_1

    new-instance v2, Lmiuix/animation/base/AnimConfig;

    invoke-direct {v2}, Lmiuix/animation/base/AnimConfig;-><init>()V

    const/4 v5, -0x2

    const/4 v6, 0x2

    new-array v6, v6, [F

    fill-array-data v6, :array_0

    invoke-virtual {v2, v5, v6}, Lmiuix/animation/base/AnimConfig;->setEase(I[F)Lmiuix/animation/base/AnimConfig;

    move-result-object v2

    new-array v5, v7, [Lmiuix/animation/listener/TransitionListener;

    aput-object p1, v5, v1

    invoke-virtual {v2, v5}, Lmiuix/animation/base/AnimConfig;->addListeners([Lmiuix/animation/listener/TransitionListener;)Lmiuix/animation/base/AnimConfig;

    move-result-object v5

    const-string v2, "AnimConfig()\n           \u2026eners(transitionListener)"

    invoke-static {v5, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_1
    new-array v2, v7, [Landroid/view/View;

    check-cast v3, Landroid/view/View;

    aput-object v3, v2, v1

    invoke-static {v2}, Lmiuix/animation/Folme;->useAt([Landroid/view/View;)Lmiuix/animation/IFolme;

    move-result-object v2

    invoke-interface {v2}, Lmiuix/animation/IFolme;->state()Lmiuix/animation/IStateStyle;

    move-result-object v2

    sget-object v3, Lmiuix/animation/property/ViewProperty;->ALPHA:Lmiuix/animation/property/ViewProperty;

    check-cast v3, Lmiuix/animation/property/FloatProperty;

    const/4 v6, 0x0

    invoke-interface {v2, v3, v6}, Lmiuix/animation/IStateStyle;->add(Lmiuix/animation/property/FloatProperty;F)Lmiuix/animation/IStateStyle;

    move-result-object v2

    sget-object v3, Lmiuix/animation/property/ViewProperty;->SCALE_X:Lmiuix/animation/property/ViewProperty;

    check-cast v3, Lmiuix/animation/property/FloatProperty;

    const v6, 0x3f666666    # 0.9f

    invoke-interface {v2, v3, v6}, Lmiuix/animation/IStateStyle;->add(Lmiuix/animation/property/FloatProperty;F)Lmiuix/animation/IStateStyle;

    move-result-object v2

    sget-object v3, Lmiuix/animation/property/ViewProperty;->SCALE_Y:Lmiuix/animation/property/ViewProperty;

    check-cast v3, Lmiuix/animation/property/FloatProperty;

    invoke-interface {v2, v3, v6}, Lmiuix/animation/IStateStyle;->add(Lmiuix/animation/property/FloatProperty;F)Lmiuix/animation/IStateStyle;

    move-result-object v2

    new-array v3, v7, [Lmiuix/animation/base/AnimConfig;

    aput-object v5, v3, v1

    invoke-interface {v2, v3}, Lmiuix/animation/IStateStyle;->to([Lmiuix/animation/base/AnimConfig;)Lmiuix/animation/IStateStyle;

    move v2, v4

    goto :goto_0

    :cond_2
    return-void

    :array_0
    .array-data 4
        0x3f733333    # 0.95f
        0x3e4ccccd    # 0.2f
    .end array-data
.end method

.method private final largeViewPreSetup2x2(IIIIIII)[I
    .locals 17

    move-object/from16 v0, p0

    move/from16 v1, p1

    move/from16 v2, p2

    move/from16 v10, p5

    move/from16 v11, p6

    move/from16 v12, p7

    iget-object v13, v0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mPvItemLocationInfoList:Ljava/util/List;

    new-instance v14, Lcom/miui/home/launcher/folder/FolderIconPreviewInfo;

    new-instance v8, Landroid/graphics/Rect;

    const/4 v15, 0x0

    invoke-direct {v8, v15, v15, v10, v11}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v9, Landroid/graphics/Rect;

    add-int v7, v10, v1

    add-int v6, v11, v2

    invoke-direct {v9, v1, v2, v7, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v3, v14

    move v15, v6

    move/from16 v6, p5

    move/from16 v16, v7

    move/from16 v7, p6

    invoke-direct/range {v3 .. v9}, Lcom/miui/home/launcher/folder/FolderIconPreviewInfo;-><init>(IIIILandroid/graphics/Rect;Landroid/graphics/Rect;)V

    invoke-interface {v13, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-boolean v3, v0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->LAYOUT_DEBUGABLE:Z

    if-eqz v3, :cond_0

    iget-object v3, v0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "largeViewPreSetup2x2 , index = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v5, " , layout = ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v5, 0x2c

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move/from16 v6, v16

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v5, 0x5d

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget v3, v0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mLarge2x2ItemMergeInnerHor:I

    add-int/2addr v3, v10

    rem-int/lit8 v4, v12, 0x2

    if-nez v4, :cond_2

    invoke-static {}, Lcom/miui/home/launcher/DeviceConfig;->isLayoutRtl()Z

    move-result v4

    if-eqz v4, :cond_1

    neg-int v3, v3

    :cond_1
    add-int/2addr v1, v3

    goto :goto_0

    :cond_2
    move/from16 v1, p3

    :goto_0
    const/4 v3, 0x1

    if-ne v12, v3, :cond_3

    iget v4, v0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mLarge2x2ItemMergeInnerVer:I

    add-int/2addr v4, v11

    add-int/2addr v2, v4

    :cond_3
    const/4 v4, 0x2

    new-array v4, v4, [I

    const/4 v5, 0x0

    aput v1, v4, v5

    aput v2, v4, v3

    return-object v4
.end method

.method private final onLayout2x2(ZIIII)V
    .locals 3

    const/4 p1, 0x0

    :try_start_0
    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->getChildCount()I

    move-result p2

    sget p3, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->ITEMS_MAX_COUNT_2X2:I

    invoke-static {p2, p3}, Lkotlin/ranges/RangesKt;->coerceAtMost(II)I

    move-result p2

    :goto_0
    if-ge p1, p2, :cond_1

    iget-object p3, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mPvItemLocationInfoList:Ljava/util/List;

    invoke-interface {p3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/miui/home/launcher/folder/FolderIconPreviewInfo;

    invoke-virtual {p3}, Lcom/miui/home/launcher/folder/FolderIconPreviewInfo;->getGroupRect()Landroid/graphics/Rect;

    move-result-object p3

    iget-object p4, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mPvChildList:Ljava/util/List;

    invoke-interface {p4, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;

    iget p5, p3, Landroid/graphics/Rect;->left:I

    iget v0, p3, Landroid/graphics/Rect;->top:I

    iget v1, p3, Landroid/graphics/Rect;->right:I

    iget v2, p3, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p4, p5, v0, v1, v2}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;->layout(IIII)V

    iget-boolean p4, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->LAYOUT_DEBUGABLE:Z

    if-eqz p4, :cond_0

    iget-object p4, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->TAG:Ljava/lang/String;

    new-instance p5, Ljava/lang/StringBuilder;

    invoke-direct {p5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "\n"

    invoke-virtual {p5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "largeViewLayout2x2 , index = "

    invoke-virtual {p5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, " , layout = ["

    invoke-virtual {p5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v0, p3, Landroid/graphics/Rect;->left:I

    invoke-virtual {p5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v0, 0x2c

    invoke-virtual {p5, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget v1, p3, Landroid/graphics/Rect;->top:I

    invoke-virtual {p5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p5, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget v1, p3, Landroid/graphics/Rect;->right:I

    invoke-virtual {p5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p5, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget p3, p3, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 p3, 0x5d

    invoke-virtual {p5, p3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string p3, ""

    invoke-virtual {p5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-static {p4, p3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :catch_0
    move-exception p1

    iget-object p2, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->TAG:Ljava/lang/String;

    const-string p3, "onLayout2x2 indexOutOfBoundsException, This usually occurs when threads are concurrent ~"

    check-cast p1, Ljava/lang/Throwable;

    invoke-static {p2, p3, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_1
    return-void
.end method

.method private final onMeasureChild2x2()V
    .locals 6

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->getChildCount()I

    move-result v0

    sget v1, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->ITEMS_MAX_COUNT_2X2:I

    invoke-static {v0, v1}, Lkotlin/ranges/RangesKt;->coerceAtMost(II)I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    const/4 v2, 0x3

    const/high16 v3, 0x40000000    # 2.0f

    if-ge v1, v2, :cond_0

    iget-object v2, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mPvChildList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;

    iget v4, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mLargeItemWith:I

    invoke-static {v4, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    iget v5, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mLargeItemHeight:I

    invoke-static {v5, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v2, v4, v3}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;->measure(II)V

    goto :goto_1

    :cond_0
    iget-object v2, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mPvChildList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;

    iget v4, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mSmallItemWith:I

    invoke-static {v4, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    iget v5, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mSmallItemHeight:I

    invoke-static {v5, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v2, v4, v3}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;->measure(II)V

    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private final onMeasureChildCustom(II)V
    .locals 1

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->getChildCount()I

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mPvChildList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    if-eqz p1, :cond_2

    if-nez p2, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mPvItemLocationInfoList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    invoke-direct {p0, p1, p2}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->preMeasure2x2(II)V

    invoke-direct {p0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->preSetup2x2()V

    iget-boolean p1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->LAYOUT_DEBUGABLE:Z

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->TAG:Ljava/lang/String;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "mPvItemLocationInfoList >>> "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mPvItemLocationInfoList:Ljava/util/List;

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object p1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->TAG:Ljava/lang/String;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "mPvItemLocationInfoList.size  >>> "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mPvItemLocationInfoList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    invoke-direct {p0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->onMeasureChild2x2()V

    return-void

    :cond_2
    :goto_0
    return-void
.end method

.method private final preMeasure2x2(II)V
    .locals 4

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p1

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->getPaddingStart()I

    move-result v0

    sub-int/2addr p1, v0

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->getPaddingEnd()I

    move-result v0

    sub-int/2addr p1, v0

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p2

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->getPaddingTop()I

    move-result v0

    sub-int/2addr p2, v0

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->getPaddingBottom()I

    move-result v0

    sub-int/2addr p2, v0

    if-gt p1, p2, :cond_0

    int-to-float v0, p1

    iget v1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->m2x2LargeItemMergeEdgePercent:F

    mul-float/2addr v1, v0

    float-to-int v1, v1

    iput v1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mLarge2x2ItemMergeEdgeHor:I

    iget v1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->m2x2LargeItemMergeInnerPercent:F

    mul-float/2addr v1, v0

    float-to-int v1, v1

    iput v1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mLarge2x2ItemMergeInnerHor:I

    iget v1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mLarge2x2ItemMergeInnerHor:I

    iput v1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mLarge2x2ItemMergeInnerVer:I

    iget v2, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mLarge2x2ItemMergeEdgeHor:I

    mul-int/lit8 v2, v2, 0x2

    sub-int v2, p1, v2

    sub-int/2addr v2, v1

    div-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mLargeItemWith:I

    iget v1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mLargeItemWith:I

    iput v1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mLargeItemHeight:I

    iget v2, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mLargeItemHeight:I

    mul-int/lit8 v2, v2, 0x2

    sub-int v2, p2, v2

    iget v3, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mLarge2x2ItemMergeInnerVer:I

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mLarge2x2ItemMergeEdgeVer:I

    iget v2, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->m2x2SmallItemMergeInnerPercent:F

    mul-float/2addr v0, v2

    float-to-int v0, v0

    iput v0, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mSmall2x2ItemMergeInner:I

    iget v0, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mSmall2x2ItemMergeInner:I

    sub-int/2addr v1, v0

    div-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mSmallItemWith:I

    iget v0, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mSmallItemWith:I

    iput v0, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mSmallItemHeight:I

    goto :goto_0

    :cond_0
    int-to-float v0, p2

    iget v1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->m2x2LargeItemMergeEdgePercent:F

    mul-float/2addr v1, v0

    float-to-int v1, v1

    iput v1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mLarge2x2ItemMergeEdgeVer:I

    iget v1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->m2x2LargeItemMergeInnerPercent:F

    mul-float/2addr v1, v0

    float-to-int v1, v1

    iput v1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mLarge2x2ItemMergeInnerVer:I

    iget v1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mLarge2x2ItemMergeInnerVer:I

    iput v1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mLarge2x2ItemMergeInnerHor:I

    iget v2, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mLarge2x2ItemMergeEdgeVer:I

    mul-int/lit8 v2, v2, 0x2

    sub-int v2, p2, v2

    sub-int/2addr v2, v1

    div-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mLargeItemHeight:I

    iget v1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mLargeItemHeight:I

    iput v1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mLargeItemWith:I

    iget v1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mLargeItemWith:I

    mul-int/lit8 v2, v1, 0x2

    sub-int v2, p1, v2

    iget v3, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mLarge2x2ItemMergeInnerHor:I

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mLarge2x2ItemMergeEdgeHor:I

    iget v2, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->m2x2SmallItemMergeInnerPercent:F

    mul-float/2addr v0, v2

    float-to-int v0, v0

    iput v0, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mSmall2x2ItemMergeInner:I

    iget v0, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mSmall2x2ItemMergeInner:I

    sub-int/2addr v1, v0

    div-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mSmallItemWith:I

    iget v0, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mSmallItemWith:I

    iput v0, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mSmallItemHeight:I

    :goto_0
    iget-boolean v0, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->LAYOUT_DEBUGABLE:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " \n\nvalidWidth = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " , validHeight = "

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 p1, 0xa

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string p2, "padding = ["

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->getPaddingStart()I

    move-result p2

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 p2, 0x2c

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->getPaddingTop()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->getPaddingRight()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->getPaddingBottom()I

    move-result p2

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, "]\n"

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, "large2x2ItemMergeEdgeVer = "

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p2, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mLarge2x2ItemMergeEdgeVer:I

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, " ,     large2x2ItemMergeEdgeHor = "

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p2, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mLarge2x2ItemMergeEdgeHor:I

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string p2, "large2x2ItemMergeInnerHor = "

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p2, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mLarge2x2ItemMergeInnerHor:I

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, " ,   large2x2ItemMergeInnerVer = "

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p2, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mLarge2x2ItemMergeInnerVer:I

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string p2, "largeItemWith = "

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p2, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mLargeItemWith:I

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, " ,                  largeItemHeight = "

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p2, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mLargeItemHeight:I

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string p2, "small2x2ItemMergeInner = "

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p2, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mSmall2x2ItemMergeInner:I

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string p2, "smallItemWith = "

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p2, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mSmallItemWith:I

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, " ,      smallItemHeight = "

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p2, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mSmallItemHeight:I

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return-void
.end method

.method private final preSetup2x2()V
    .locals 13

    invoke-static {}, Lcom/miui/home/launcher/DeviceConfig;->isLayoutRtl()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->getPaddingEnd()I

    move-result v0

    iget v1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mLarge2x2ItemMergeEdgeHor:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mLarge2x2ItemMergeInnerHor:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mLargeItemWith:I

    add-int/2addr v0, v1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->getPaddingStart()I

    move-result v0

    iget v1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mLarge2x2ItemMergeEdgeHor:I

    add-int/2addr v0, v1

    :goto_0
    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->getPaddingTop()I

    move-result v1

    iget v2, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mLarge2x2ItemMergeEdgeVer:I

    add-int/2addr v1, v2

    sget v9, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->ITEMS_MAX_COUNT_2X2:I

    const/4 v10, 0x0

    move v3, v0

    move v4, v1

    move v1, v10

    move v11, v1

    :goto_1
    if-ge v11, v9, :cond_3

    const/4 v2, 0x3

    const/4 v12, 0x1

    if-ge v11, v2, :cond_2

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->getPaddingTop()I

    move-result v1

    iget v2, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mLarge2x2ItemMergeEdgeVer:I

    add-int v5, v1, v2

    iget v6, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mLargeItemWith:I

    iget v7, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mLargeItemHeight:I

    move-object v1, p0

    move v2, v3

    move v3, v4

    move v4, v0

    move v8, v11

    invoke-direct/range {v1 .. v8}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->largeViewPreSetup2x2(IIIIIII)[I

    move-result-object v1

    aget v2, v1, v10

    aget v1, v1, v12

    invoke-static {}, Lcom/miui/home/launcher/DeviceConfig;->isLayoutRtl()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x2

    if-ne v11, v3, :cond_1

    iget v3, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mSmallItemWith:I

    iget v4, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mSmall2x2ItemMergeInner:I

    add-int/2addr v3, v4

    add-int/2addr v2, v3

    move v4, v1

    move v1, v2

    move v3, v1

    goto :goto_2

    :cond_1
    move v4, v1

    move v1, v2

    move v3, v1

    goto :goto_2

    :cond_2
    iget v6, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mSmallItemWith:I

    iget v7, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mSmallItemHeight:I

    move-object v2, p0

    move v5, v1

    move v8, v11

    invoke-direct/range {v2 .. v8}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->smallViewPreSetup2x2(IIIIII)[I

    move-result-object v2

    aget v3, v2, v10

    aget v2, v2, v12

    move v4, v2

    :goto_2
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    :cond_3
    return-void
.end method

.method private final removePlaceholderIconItem()V
    .locals 2

    iget v0, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mRealPvChildCount:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    :goto_0
    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mPvChildList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget v1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mRealPvChildCount:I

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mPvChildList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;

    iget-object v1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mPvChildList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    check-cast v0, Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->removeView(Landroid/view/View;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private final runLoadAction(Lcom/miui/home/launcher/FolderInfo;Lcom/miui/home/launcher/IconCache;ZLcom/miui/home/library/utils/AsyncTaskExecutorHelper$SerialExecutor;Z)V
    .locals 16

    move-object/from16 v6, p0

    move-object/from16 v7, p1

    move-object/from16 v8, p4

    if-nez v7, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/miui/home/launcher/FolderInfo;->count()I

    move-result v9

    const/4 v10, 0x0

    if-eqz p3, :cond_3

    invoke-virtual/range {p1 .. p1}, Lcom/miui/home/launcher/FolderInfo;->getAppPredictList()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual/range {p1 .. p1}, Lcom/miui/home/launcher/FolderInfo;->getAppPredictList()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, v6, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mPvChildList:Ljava/util/List;

    if-nez v1, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v9, v1}, Lkotlin/ranges/RangesKt;->coerceAtMost(II)I

    move-result v1

    if-ge v0, v1, :cond_3

    :cond_2
    move v11, v10

    move v12, v11

    move v13, v12

    goto :goto_0

    :cond_3
    move/from16 v13, p3

    move v11, v10

    move v12, v11

    :goto_0
    if-ge v11, v9, :cond_9

    iget-object v0, v6, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mPvChildList:Ljava/util/List;

    if-nez v0, :cond_4

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_4
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v12, v0, :cond_9

    if-eqz v13, :cond_5

    invoke-virtual/range {p1 .. p1}, Lcom/miui/home/launcher/FolderInfo;->getAppPredictList()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/home/launcher/ShortcutInfo;

    goto :goto_1

    :cond_5
    invoke-direct {v6, v7, v11}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->getShortcutInfoSafety(Lcom/miui/home/launcher/FolderInfo;I)Lcom/miui/home/launcher/ShortcutInfo;

    move-result-object v0

    :goto_1
    move-object v14, v0

    if-eqz v14, :cond_8

    if-nez v14, :cond_6

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_6
    iget v0, v14, Lcom/miui/home/launcher/ShortcutInfo;->mIconType:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_7

    const/4 v0, 0x1

    move v5, v0

    goto :goto_2

    :cond_7
    move v5, v10

    :goto_2
    new-instance v15, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$runLoadAction$1;

    move-object v0, v15

    move-object/from16 v1, p0

    move v2, v12

    move-object v3, v14

    move-object/from16 v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$runLoadAction$1;-><init>(Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;ILcom/miui/home/launcher/ShortcutInfo;Lcom/miui/home/launcher/IconCache;Z)V

    check-cast v15, Ljava/util/function/Function;

    new-instance v0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$runLoadAction$2;

    invoke-direct {v0, v6, v14, v12}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$runLoadAction$2;-><init>(Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;Lcom/miui/home/launcher/ShortcutInfo;I)V

    check-cast v0, Ljava/util/function/Consumer;

    move-object v1, v8

    check-cast v1, Ljava/util/concurrent/Executor;

    invoke-static {v15, v0, v14, v1}, Lcom/miui/home/library/utils/AsyncTaskExecutorHelper;->execSerial(Ljava/util/function/Function;Ljava/util/function/Consumer;Ljava/lang/Object;Ljava/util/concurrent/Executor;)V

    add-int/lit8 v12, v12, 0x1

    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    :cond_8
    return-void

    :cond_9
    new-instance v0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$runLoadAction$3;

    move/from16 v1, p5

    invoke-direct {v0, v6, v12, v1}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$runLoadAction$3;-><init>(Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;IZ)V

    check-cast v0, Ljava/util/function/Consumer;

    invoke-static {v0, v8}, Lcom/miui/home/library/utils/AsyncTaskExecutorHelper;->doUIConsumerSerialized(Ljava/util/function/Consumer;Lcom/miui/home/library/utils/AsyncTaskExecutorHelper$SerialExecutor;)V

    return-void
.end method

.method private final showPreviewIcon()V
    .locals 8

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mPvChildList:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    add-int/lit8 v4, v2, 0x1

    if-gez v2, :cond_0

    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwIndexOverflow()V

    :cond_0
    check-cast v3, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;

    new-instance v2, Lmiuix/animation/base/AnimConfig;

    invoke-direct {v2}, Lmiuix/animation/base/AnimConfig;-><init>()V

    sget-object v5, Lmiuix/animation/property/ViewProperty;->SCALE_X:Lmiuix/animation/property/ViewProperty;

    check-cast v5, Lmiuix/animation/property/FloatProperty;

    iget-object v6, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mShowAnimScaleConfig:Lmiuix/animation/base/AnimSpecialConfig;

    invoke-virtual {v2, v5, v6}, Lmiuix/animation/base/AnimConfig;->setSpecial(Lmiuix/animation/property/FloatProperty;Lmiuix/animation/base/AnimSpecialConfig;)Lmiuix/animation/base/AnimConfig;

    move-result-object v2

    sget-object v5, Lmiuix/animation/property/ViewProperty;->SCALE_Y:Lmiuix/animation/property/ViewProperty;

    check-cast v5, Lmiuix/animation/property/FloatProperty;

    iget-object v6, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mShowAnimScaleConfig:Lmiuix/animation/base/AnimSpecialConfig;

    invoke-virtual {v2, v5, v6}, Lmiuix/animation/base/AnimConfig;->setSpecial(Lmiuix/animation/property/FloatProperty;Lmiuix/animation/base/AnimSpecialConfig;)Lmiuix/animation/base/AnimConfig;

    move-result-object v2

    sget-object v5, Lmiuix/animation/property/ViewProperty;->ALPHA:Lmiuix/animation/property/ViewProperty;

    check-cast v5, Lmiuix/animation/property/FloatProperty;

    iget-object v6, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mShowAnimAlphaConfig:Lmiuix/animation/base/AnimSpecialConfig;

    invoke-virtual {v2, v5, v6}, Lmiuix/animation/base/AnimConfig;->setSpecial(Lmiuix/animation/property/FloatProperty;Lmiuix/animation/base/AnimSpecialConfig;)Lmiuix/animation/base/AnimConfig;

    move-result-object v2

    const/4 v5, 0x1

    new-array v6, v5, [Landroid/view/View;

    check-cast v3, Landroid/view/View;

    aput-object v3, v6, v1

    invoke-static {v6}, Lmiuix/animation/Folme;->useAt([Landroid/view/View;)Lmiuix/animation/IFolme;

    move-result-object v3

    invoke-interface {v3}, Lmiuix/animation/IFolme;->state()Lmiuix/animation/IStateStyle;

    move-result-object v3

    sget-object v6, Lmiuix/animation/property/ViewProperty;->SCALE_X:Lmiuix/animation/property/ViewProperty;

    check-cast v6, Lmiuix/animation/property/FloatProperty;

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-interface {v3, v6, v7}, Lmiuix/animation/IStateStyle;->add(Lmiuix/animation/property/FloatProperty;F)Lmiuix/animation/IStateStyle;

    move-result-object v3

    sget-object v6, Lmiuix/animation/property/ViewProperty;->SCALE_Y:Lmiuix/animation/property/ViewProperty;

    check-cast v6, Lmiuix/animation/property/FloatProperty;

    invoke-interface {v3, v6, v7}, Lmiuix/animation/IStateStyle;->add(Lmiuix/animation/property/FloatProperty;F)Lmiuix/animation/IStateStyle;

    move-result-object v3

    sget-object v6, Lmiuix/animation/property/ViewProperty;->ALPHA:Lmiuix/animation/property/ViewProperty;

    check-cast v6, Lmiuix/animation/property/FloatProperty;

    invoke-interface {v3, v6, v7}, Lmiuix/animation/IStateStyle;->add(Lmiuix/animation/property/FloatProperty;F)Lmiuix/animation/IStateStyle;

    move-result-object v3

    new-array v5, v5, [Lmiuix/animation/base/AnimConfig;

    aput-object v2, v5, v1

    invoke-interface {v3, v5}, Lmiuix/animation/IStateStyle;->to([Lmiuix/animation/base/AnimConfig;)Lmiuix/animation/IStateStyle;

    move v2, v4

    goto :goto_0

    :cond_1
    return-void
.end method

.method private final smallViewPreSetup2x2(IIIIII)[I
    .locals 17

    move-object/from16 v0, p0

    move/from16 v1, p1

    move/from16 v2, p2

    move/from16 v10, p4

    move/from16 v11, p5

    move/from16 v12, p6

    iget-object v13, v0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mPvItemLocationInfoList:Ljava/util/List;

    new-instance v14, Lcom/miui/home/launcher/folder/FolderIconPreviewInfo;

    new-instance v8, Landroid/graphics/Rect;

    const/4 v15, 0x0

    invoke-direct {v8, v15, v15, v10, v11}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v9, Landroid/graphics/Rect;

    add-int v7, v10, v1

    add-int v6, v11, v2

    invoke-direct {v9, v1, v2, v7, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v3, v14

    move v15, v6

    move/from16 v6, p4

    move/from16 v16, v7

    move/from16 v7, p5

    invoke-direct/range {v3 .. v9}, Lcom/miui/home/launcher/folder/FolderIconPreviewInfo;-><init>(IIIILandroid/graphics/Rect;Landroid/graphics/Rect;)V

    invoke-interface {v13, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-boolean v3, v0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->LAYOUT_DEBUGABLE:Z

    if-eqz v3, :cond_0

    iget-object v3, v0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "smallViewPreSetup2x2 , index = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v5, " , layout = ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v5, 0x2c

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move/from16 v6, v16

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v5, 0x5d

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget v3, v0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mSmall2x2ItemMergeInner:I

    add-int/2addr v3, v10

    add-int/lit8 v4, v12, -0x3

    rem-int/lit8 v5, v4, 0x2

    if-nez v5, :cond_2

    invoke-static {}, Lcom/miui/home/launcher/DeviceConfig;->isLayoutRtl()Z

    move-result v5

    if-eqz v5, :cond_1

    neg-int v3, v3

    :cond_1
    add-int/2addr v1, v3

    goto :goto_0

    :cond_2
    move/from16 v1, p3

    :goto_0
    const/4 v3, 0x1

    if-ne v4, v3, :cond_3

    iget v4, v0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mSmall2x2ItemMergeInner:I

    add-int/2addr v4, v11

    add-int/2addr v2, v4

    :cond_3
    const/4 v4, 0x2

    new-array v4, v4, [I

    const/4 v5, 0x0

    aput v1, v4, v5

    aput v2, v4, v3

    return-object v4
.end method


# virtual methods
.method public final addPlaceholderIconItem(Landroid/content/Context;)V
    .locals 1

    if-eqz p1, :cond_0

    iget p1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mRealPvChildCount:I

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->previewPlaceholderShouldAnim:Z

    :goto_0
    iget-object p1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mPvChildList:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    sget v0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->ITEMS_MAX_COUNT_2X2:I

    if-ge p1, v0, :cond_0

    invoke-direct {p0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->addPlaceholderInternal()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final addPreView(Landroid/view/View;)V
    .locals 2

    if-eqz p1, :cond_4

    instance-of v0, p1, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mPvChildList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sget v1, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->ITEMS_MAX_COUNT_2X2:I

    if-ge v0, v1, :cond_1

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mPvChildList:Ljava/util/List;

    if-eqz p1, :cond_0

    move-object v1, p1

    check-cast v1, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0, p1}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->addView(Landroid/view/View;)V

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.miui.home.launcher.folder.FolderIconPreviewContainer2X2.PreviewIconView"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    :goto_0
    iget v0, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mRealPvChildCount:I

    sget v1, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->ITEMS_MAX_COUNT_2X2:I

    if-ge v0, v1, :cond_2

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mRealPvChildCount:I

    :cond_2
    check-cast p1, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;

    invoke-virtual {p1}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->addPlaceholderIconItem(Landroid/content/Context;)V

    goto :goto_1

    :cond_3
    new-instance v0, Ljava/lang/IllegalAccessException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, " should be a PreviewIconView or a subclass of PreviewIconView!"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalAccessException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_4
    :goto_1
    return-void
.end method

.method public addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .locals 0

    invoke-super {p0, p1, p2, p3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public getItemInfo(I)Lcom/miui/home/launcher/folder/FolderIconPreviewInfo;
    .locals 1

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mPvItemLocationInfoList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mPvItemLocationInfoList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/miui/home/launcher/folder/FolderIconPreviewInfo;

    return-object p1

    :cond_0
    iget-object p1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mPvItemLocationInfoList:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    if-lez p1, :cond_1

    iget-object p1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mPvItemLocationInfoList:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/miui/home/launcher/folder/FolderIconPreviewInfo;

    return-object p1

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method public final getLastView()Landroid/view/View;
    .locals 2

    iget v0, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mRealPvChildCount:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mPvChildList:Ljava/util/List;

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    :goto_0
    return-object v0
.end method

.method public final getLastVisibleView()Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;
    .locals 3

    iget v0, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mRealPvChildCount:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mPvChildList:Ljava/util/List;

    add-int/lit8 v0, v0, -0x1

    sget v2, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->ITEMS_MAX_COUNT_2X2:I

    add-int/lit8 v2, v2, -0x1

    invoke-static {v0, v2}, Lkotlin/ranges/RangesKt;->coerceAtMost(II)I

    move-result v0

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;

    :goto_0
    return-object v0
.end method

.method public final getMPvChildList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mPvChildList:Ljava/util/List;

    return-object v0
.end method

.method public final getMRealPvChildCount()I
    .locals 1

    iget v0, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mRealPvChildCount:I

    return v0
.end method

.method public getPreAddItemInfo(Z)Lcom/miui/home/launcher/folder/FolderIconPreviewInfo;
    .locals 0

    if-eqz p1, :cond_0

    iget p1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mRealPvChildCount:I

    add-int/lit8 p1, p1, -0x1

    goto :goto_0

    :cond_0
    iget p1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mRealPvChildCount:I

    :goto_0
    invoke-virtual {p0, p1}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->getItemInfo(I)Lcom/miui/home/launcher/folder/FolderIconPreviewInfo;

    move-result-object p1

    return-object p1
.end method

.method public final getPreviewIconInfo(I)Lcom/miui/home/launcher/ShortcutInfo;
    .locals 1

    iget v0, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mRealPvChildCount:I

    add-int/lit8 v0, v0, -0x1

    if-lt v0, p1, :cond_0

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mPvChildList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;

    invoke-virtual {p1}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;->getMBuddyInfo()Lcom/miui/home/launcher/ShortcutInfo;

    move-result-object p1

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public getSmallItemsRectF()Landroid/graphics/RectF;
    .locals 6

    invoke-static {}, Lcom/miui/home/launcher/DeviceConfig;->isLayoutRtl()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->getPaddingStart()I

    move-result v0

    iget v1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mLarge2x2ItemMergeEdgeHor:I

    add-int/2addr v0, v1

    int-to-float v0, v0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->getPaddingStart()I

    move-result v0

    iget v1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mLarge2x2ItemMergeEdgeHor:I

    add-int/2addr v0, v1

    iget v2, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mLargeItemWith:I

    add-int/2addr v0, v2

    add-int/2addr v0, v1

    int-to-float v0, v0

    :goto_0
    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->getPaddingTop()I

    move-result v1

    iget v2, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mLarge2x2ItemMergeEdgeVer:I

    add-int/2addr v1, v2

    iget v2, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mLargeItemHeight:I

    add-int/2addr v1, v2

    iget v2, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mLarge2x2ItemMergeInnerVer:I

    add-int/2addr v1, v2

    int-to-float v1, v1

    new-instance v2, Landroid/graphics/RectF;

    iget v3, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mSmallItemWith:I

    mul-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    add-float/2addr v3, v0

    iget v4, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mSmall2x2ItemMergeInner:I

    int-to-float v5, v4

    add-float/2addr v3, v5

    iget v5, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mSmallItemHeight:I

    mul-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    add-float/2addr v5, v1

    int-to-float v4, v4

    add-float/2addr v5, v4

    invoke-direct {v2, v0, v1, v3, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    return-object v2
.end method

.method public isPreViewContainerOverload()Z
    .locals 2

    iget v0, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mRealPvChildCount:I

    sget v1, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->ITEMS_MAX_COUNT_2X2:I

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final isPreviewPlaceholder(Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;)Z
    .locals 1

    const-string v0, "viewParent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object p1

    instance-of p1, p1, Lcom/miui/home/launcher/folder/FolderIconPlaceholderDrawable;

    return p1
.end method

.method public final loadItemIcons(Lcom/miui/home/launcher/FolderInfo;Lcom/miui/home/launcher/IconCache;ZLcom/miui/home/library/utils/AsyncTaskExecutorHelper$SerialExecutor;Z)V
    .locals 8

    const-string v0, "info"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "iconCache"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "serialExecutor"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p5, :cond_0

    new-instance v0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$loadItemIcons$1;

    move-object v1, v0

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move v5, p3

    move-object v6, p4

    move v7, p5

    invoke-direct/range {v1 .. v7}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$loadItemIcons$1;-><init>(Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;Lcom/miui/home/launcher/FolderInfo;Lcom/miui/home/launcher/IconCache;ZLcom/miui/home/library/utils/AsyncTaskExecutorHelper$SerialExecutor;Z)V

    check-cast v0, Lmiuix/animation/listener/TransitionListener;

    invoke-direct {p0, v0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->hidePreviewIcon(Lmiuix/animation/listener/TransitionListener;)V

    goto :goto_0

    :cond_0
    invoke-direct {p0, p3}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->executePlaceholderAddAnim(Z)V

    invoke-direct/range {p0 .. p5}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->runLoadAction(Lcom/miui/home/launcher/FolderInfo;Lcom/miui/home/launcher/IconCache;ZLcom/miui/home/library/utils/AsyncTaskExecutorHelper$SerialExecutor;Z)V

    :goto_0
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 2

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->getChildCount()I

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mPvChildList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    iget v0, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mWidthMeasureSpec:I

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mHeightMeasureSpec:I

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mPvItemLocationInfoList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    iget v0, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mWidthMeasureSpec:I

    iget v1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mHeightMeasureSpec:I

    invoke-direct {p0, v0, v1}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->onMeasureChildCustom(II)V

    :cond_1
    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mPvItemLocationInfoList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_2

    return-void

    :cond_2
    invoke-direct/range {p0 .. p5}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->onLayout2x2(ZIIII)V

    return-void

    :cond_3
    :goto_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 0

    iput p1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mWidthMeasureSpec:I

    iput p2, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mHeightMeasureSpec:I

    invoke-direct {p0, p1, p2}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->onMeasureChildCustom(II)V

    invoke-direct {p0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->folderIconPlaceholderDrawableMeasure()V

    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    return-void
.end method

.method public final onPause()V
    .locals 2

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mPvChildList:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    iget v1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->MAX_LARGE_ICON_NUM:I

    invoke-static {v0, v1}, Lkotlin/collections/CollectionsKt;->take(Ljava/lang/Iterable;I)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;

    invoke-virtual {v1}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-static {v1}, Lcom/miui/home/launcher/graphics/drawable/MamlCompat;->onPause(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final onResume()V
    .locals 3

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mPvChildList:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    iget v1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->MAX_LARGE_ICON_NUM:I

    invoke-static {v0, v1}, Lkotlin/collections/CollectionsKt;->take(Ljava/lang/Iterable;I)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;

    invoke-virtual {v1}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-static {v2}, Lcom/miui/home/launcher/graphics/drawable/MamlCompat;->onResume(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v1}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;->invalidate()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final removeLastPreView()V
    .locals 3

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->getLastView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->removeView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mPvChildList:Ljava/util/List;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mPvChildList:Ljava/util/List;

    iget v2, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mRealPvChildCount:I

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v1, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    iget v1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mRealPvChildCount:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mRealPvChildCount:I

    iget v1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mRealPvChildCount:I

    iget v2, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->MAX_LARGE_ICON_NUM:I

    if-le v1, v2, :cond_0

    invoke-direct {p0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->addPlaceholderInternal()V

    :cond_0
    invoke-direct {p0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->removePlaceholderIconItem()V

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1

    :cond_1
    :goto_0
    return-void
.end method

.method public removeView(Landroid/view/View;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    return-void
.end method

.method public removeViewAt(I)V
    .locals 0

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->removeViewAt(I)V

    return-void
.end method

.method public final setFolderIconPlaceholderDrawableMatchingWallpaperColor()V
    .locals 3

    invoke-static {}, Lcom/miui/home/launcher/DeviceConfig;->isDefaultIcon()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/miui/home/launcher/folder/FolderIconPlaceholderDrawable;

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "context"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lcom/miui/home/launcher/folder/FolderIconPlaceholderDrawable;-><init>(Landroid/content/Context;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mFolderIconPlaceholderDrawable:Lcom/miui/home/launcher/folder/FolderIconPlaceholderDrawable;

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mFolderIconPlaceholderDrawable:Lcom/miui/home/launcher/folder/FolderIconPlaceholderDrawable;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/miui/home/launcher/folder/FolderIconPlaceholderDrawable;->invalidateSelf()V

    :cond_1
    invoke-direct {p0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->folderIconPlaceholderDrawableMeasure()V

    return-void
.end method

.method public final setMPvChildList(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mPvChildList:Ljava/util/List;

    return-void
.end method

.method public final setMRealPvChildCount(I)V
    .locals 0

    iput p1, p0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->mRealPvChildCount:I

    return-void
.end method
