.class public Lcom/miui/home/launcher/MyTitleTextView;
.super Lcom/miui/home/launcher/TitleTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/miui/home/launcher/TitleTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public setTextAppearance(Landroid/content/Context;I)V
    .locals 3

    invoke-super {p0, p1, p2}, Lcom/miui/home/launcher/TitleTextView;->setTextAppearance(Landroid/content/Context;I)V

    invoke-virtual {p0}, Lcom/miui/home/launcher/MyTitleTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p0}, Lcom/miui/home/launcher/MyTitleTextView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "icon_title_text"

    invoke-static {v1, v2}, Landroid/Utils/Utils;->ColorToID(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/MyTitleTextView;->setTextColor(I)V

    return-void
.end method

.method public setTextColor(I)V
    .locals 4

    invoke-virtual {p0}, Lcom/miui/home/launcher/MyTitleTextView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "launch_title_color"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_0

    move p1, v0

    :cond_0
    invoke-super {p0, p1}, Lcom/miui/home/launcher/TitleTextView;->setTextColor(I)V

    return-void
.end method

.method public setTextSize(IF)V
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/miui/home/launcher/MyTitleTextView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "launch_title_size"

    const/16 v3, 0xc

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    int-to-float v1, v1

    invoke-super {p0, v0, v1}, Lcom/miui/home/launcher/TitleTextView;->setTextSize(IF)V

    return-void
.end method
