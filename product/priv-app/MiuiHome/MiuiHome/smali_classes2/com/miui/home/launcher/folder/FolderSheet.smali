.class public Lcom/miui/home/launcher/folder/FolderSheet;
.super Lcom/miui/home/launcher/AbstractFloatingView;

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnTouchListener;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# static fields
.field public static isFolderSheetViewShow:Z


# instance fields
.field private final TAG:Ljava/lang/String;

.field private downY:F

.field private eventY:F

.field listener:Lcom/miui/home/launcher/LauncherStateManager$StateListener;

.field private mAppPredictSlidingButton:Lmiuix/slidingwidget/widget/SlidingButton;

.field private mBeforeAppPredictButtonStatus:I

.field private mBigFolderCheckBox:Lmiuix/visual/check/VisualCheckBox;

.field private mBigFolderName:Lmiuix/visual/check/VisualCheckedTextView;

.field private mBigFolderSelectBorder:Lcom/miui/home/settings/FixedAspectRatioLottieAnimView;

.field private mDefaultFolderCheckBox:Lmiuix/visual/check/VisualCheckBox;

.field private mDefaultFolderName:Lmiuix/visual/check/VisualCheckedTextView;

.field private mDefaultFolderSelectBorder:Lcom/miui/home/settings/FixedAspectRatioLottieAnimView;

.field private mFolderInfo:Lcom/miui/home/launcher/FolderInfo;

.field private mFolderPickerAppPredictExposed:Landroid/widget/RelativeLayout;

.field private mFolderPickerAppPredictSummary:Landroid/widget/TextView;

.field private mFolderPickerAppPredictTitle:Landroid/widget/TextView;

.field private mFolderPickerBody:Landroid/widget/ScrollView;

.field private mFolderPickerCancel:Landroid/widget/ImageView;

.field private mFolderPickerDragLine:Landroid/widget/ImageView;

.field private mFolderPickerOk:Landroid/widget/ImageView;

.field private mFolderPickerSelectBigFolderBg:Landroid/widget/ImageView;

.field private mFolderPickerSelectBigFolderImg:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

.field private mFolderPickerSelectDefaultFolderBg:Landroid/widget/ImageView;

.field private mFolderPickerSelectDefaultFolderImg:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer1X1;

.field private mFolderPickerSelectWallpaperBg:Landroid/widget/ImageView;

.field private mFolderPickerTitle:Landroid/widget/TextView;

.field private mFolderType:I

.field private mSerialExecutor:Lcom/miui/home/library/utils/AsyncTaskExecutorHelper$SerialExecutor;

.field private mTranslationY:F

.field private moveY:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/miui/home/launcher/AbstractFloatingView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const-string p1, "FolderSheet"

    iput-object p1, p0, Lcom/miui/home/launcher/folder/FolderSheet;->TAG:Ljava/lang/String;

    new-instance p1, Lcom/miui/home/library/utils/AsyncTaskExecutorHelper$SerialExecutor;

    const-string p2, "Folder Sheet"

    invoke-direct {p1, p2}, Lcom/miui/home/library/utils/AsyncTaskExecutorHelper$SerialExecutor;-><init>(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mSerialExecutor:Lcom/miui/home/library/utils/AsyncTaskExecutorHelper$SerialExecutor;

    new-instance p1, Lcom/miui/home/launcher/folder/FolderSheet$2;

    invoke-direct {p1, p0}, Lcom/miui/home/launcher/folder/FolderSheet$2;-><init>(Lcom/miui/home/launcher/folder/FolderSheet;)V

    iput-object p1, p0, Lcom/miui/home/launcher/folder/FolderSheet;->listener:Lcom/miui/home/launcher/LauncherStateManager$StateListener;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/miui/home/launcher/AbstractFloatingView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const-string p1, "FolderSheet"

    iput-object p1, p0, Lcom/miui/home/launcher/folder/FolderSheet;->TAG:Ljava/lang/String;

    new-instance p1, Lcom/miui/home/library/utils/AsyncTaskExecutorHelper$SerialExecutor;

    const-string p2, "Folder Sheet"

    invoke-direct {p1, p2}, Lcom/miui/home/library/utils/AsyncTaskExecutorHelper$SerialExecutor;-><init>(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mSerialExecutor:Lcom/miui/home/library/utils/AsyncTaskExecutorHelper$SerialExecutor;

    new-instance p1, Lcom/miui/home/launcher/folder/FolderSheet$2;

    invoke-direct {p1, p0}, Lcom/miui/home/launcher/folder/FolderSheet$2;-><init>(Lcom/miui/home/launcher/folder/FolderSheet;)V

    iput-object p1, p0, Lcom/miui/home/launcher/folder/FolderSheet;->listener:Lcom/miui/home/launcher/LauncherStateManager$StateListener;

    return-void
.end method

.method static synthetic access$000(Lcom/miui/home/launcher/folder/FolderSheet;Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/home/launcher/folder/FolderSheet;->setNavigationBarOrStatusBarTransparent(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$102(Lcom/miui/home/launcher/folder/FolderSheet;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mIsOpen:Z

    return p1
.end method

.method static synthetic access$200(Lcom/miui/home/launcher/folder/FolderSheet;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/home/launcher/folder/FolderSheet;->restoreLauncherBlurWithAnim()V

    return-void
.end method

.method static synthetic access$300(Lcom/miui/home/launcher/folder/FolderSheet;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/home/launcher/folder/FolderSheet;->trackQuitFolderPicker()V

    return-void
.end method

.method private closeOrOpenPickerByTouch(FI)V
    .locals 0

    int-to-float p2, p2

    cmpl-float p1, p1, p2

    if-lez p1, :cond_0

    const/4 p1, 0x1

    invoke-virtual {p0, p1}, Lcom/miui/home/launcher/folder/FolderSheet;->handleClose(Z)V

    goto :goto_0

    :cond_0
    iget p1, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mTranslationY:F

    invoke-virtual {p0, p1}, Lcom/miui/home/launcher/folder/FolderSheet;->setTranslationY(F)V

    invoke-direct {p0}, Lcom/miui/home/launcher/folder/FolderSheet;->handleOpen()V

    :goto_0
    return-void
.end method

.method private getColorFromTheme(Landroid/content/Context;II)I
    .locals 1

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderSheet;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/content/Context;->getColor(I)I

    move-result p3

    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/res/Resources$Theme;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result p3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const-string p1, "FolderSheet"

    const-string p2, "Color find error"

    invoke-static {p1, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return p3
.end method

.method private getFolderPickerDragLineDrawable(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;
    .locals 1

    invoke-static {}, Lcom/miui/home/launcher/DeviceConfig;->isDarkMode()Z

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x7f0802b1

    invoke-static {p1, v0}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    return-object p1

    :cond_0
    const v0, 0x7f0802b0

    invoke-static {p1, v0}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    return-object p1
.end method

.method private getFolderPickerSummaryText(Landroid/content/Context;)I
    .locals 1

    invoke-static {}, Lcom/miui/home/launcher/DeviceConfig;->isDarkMode()Z

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x7f0605e9

    invoke-static {p1, v0}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p1

    return p1

    :cond_0
    const v0, 0x7f060057

    invoke-static {p1, v0}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p1

    return p1
.end method

.method private getFolderPickerTitleColor(Landroid/content/Context;)I
    .locals 1

    invoke-static {}, Lcom/miui/home/launcher/DeviceConfig;->isDarkMode()Z

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x7f0605ef

    invoke-static {p1, v0}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p1

    return p1

    :cond_0
    const v0, 0x7f060064

    invoke-static {p1, v0}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p1

    return p1
.end method

.method public static getFolderSheetType()I
    .locals 1

    const/16 v0, 0x1000

    return v0
.end method

.method private getFolderSizeByType()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderInfo:Lcom/miui/home/launcher/FolderInfo;

    iget v0, v0, Lcom/miui/home/launcher/FolderInfo;->itemType:I

    iget v1, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderType:I

    const/16 v2, 0x15

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderInfo:Lcom/miui/home/launcher/FolderInfo;

    iget v0, v0, Lcom/miui/home/launcher/FolderInfo;->itemType:I

    if-ne v0, v2, :cond_0

    const-string v0, "2*2"

    goto :goto_0

    :cond_0
    const-string v0, "1*1"

    :goto_0
    return-object v0

    :cond_1
    if-ne v1, v2, :cond_2

    const-string v0, "2*2"

    goto :goto_1

    :cond_2
    const-string v0, "1*1"

    :goto_1
    return-object v0
.end method

.method private handleLauncherIconVisible(Z)V
    .locals 5

    invoke-static {}, Lcom/miui/home/launcher/Application;->getLauncher()Lcom/miui/home/launcher/Launcher;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/home/launcher/Launcher;->getWorkspace()Lcom/miui/home/launcher/Workspace;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x4

    if-eqz p1, :cond_0

    move v4, v2

    goto :goto_0

    :cond_0
    move v4, v3

    :goto_0
    invoke-virtual {v1, v4}, Lcom/miui/home/launcher/Workspace;->setVisibility(I)V

    invoke-virtual {v0}, Lcom/miui/home/launcher/Launcher;->getHotSeats()Lcom/miui/home/launcher/hotseats/HotSeats;

    move-result-object v1

    if-eqz p1, :cond_1

    move v4, v2

    goto :goto_1

    :cond_1
    move v4, v3

    :goto_1
    invoke-virtual {v1, v4}, Lcom/miui/home/launcher/hotseats/HotSeats;->setVisibility(I)V

    invoke-virtual {v0}, Lcom/miui/home/launcher/Launcher;->getSearchBarContainer()Lcom/miui/home/launcher/SearchBarContainer;

    move-result-object v0

    if-eqz p1, :cond_2

    goto :goto_2

    :cond_2
    move v2, v3

    :goto_2
    invoke-virtual {v0, v2}, Lcom/miui/home/launcher/SearchBarContainer;->setVisibility(I)V

    return-void
.end method

.method private handleOpen()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mIsOpen:Z

    sput-boolean v0, Lcom/miui/home/launcher/folder/FolderSheet;->isFolderSheetViewShow:Z

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderSheet;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/miui/home/launcher/Launcher;->getLauncher(Landroid/content/Context;)Lcom/miui/home/launcher/Launcher;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/home/launcher/Launcher;->notifyBackGestureStatus()V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/miui/home/launcher/folder/FolderSheet;->handleLauncherIconVisible(Z)V

    invoke-direct {p0}, Lcom/miui/home/launcher/folder/FolderSheet;->setLauncherBlurWithAnim()V

    return-void
.end method

.method private handleSlidingButtonColorDarkModeSyncInternal()V
    .locals 4

    :try_start_0
    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mAppPredictSlidingButton:Lmiuix/slidingwidget/widget/SlidingButton;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "mHelper"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    iget-object v1, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mAppPredictSlidingButton:Lmiuix/slidingwidget/widget/SlidingButton;

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiuix/slidingwidget/widget/SlidingButtonHelper;

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderSheet;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const v2, 0x7f1205d8

    sget-object v3, Lmiuix/slidingwidget/R$styleable;->SlidingButton:[I

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v1

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderSheet;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Lmiuix/slidingwidget/widget/SlidingButtonHelper;->initResource(Landroid/content/Context;Landroid/content/res/TypedArray;)V

    invoke-virtual {v0}, Lmiuix/slidingwidget/widget/SlidingButtonHelper;->initDrawable()V

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mAppPredictSlidingButton:Lmiuix/slidingwidget/widget/SlidingButton;

    invoke-virtual {v0}, Lmiuix/slidingwidget/widget/SlidingButton;->requestLayout()V

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mAppPredictSlidingButton:Lmiuix/slidingwidget/widget/SlidingButton;

    invoke-virtual {v0}, Lmiuix/slidingwidget/widget/SlidingButton;->invalidate()V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/ReflectiveOperationException;->printStackTrace()V

    :goto_0
    return-void
.end method

.method private handleVisualCheckedTextViewColorDarkModeSyncInternal(ZLmiuix/visual/check/VisualCheckedTextView;)V
    .locals 5

    :try_start_0
    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderSheet;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0605f8

    const v2, 0x7f060101

    invoke-direct {p0, v0, v1, v2}, Lcom/miui/home/launcher/folder/FolderSheet;->getColorFromTheme(Landroid/content/Context;II)I

    move-result v0

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderSheet;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0605f9

    const v3, 0x7f060102

    invoke-direct {p0, v1, v2, v3}, Lcom/miui/home/launcher/folder/FolderSheet;->getColorFromTheme(Landroid/content/Context;II)I

    move-result v1

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v3, "mCheckedColor"

    invoke-virtual {v2, v3}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, p2, v4}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v4, "mUncheckedColor"

    invoke-virtual {v2, v4}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, p2, v3}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_0
    invoke-virtual {p2, v0}, Lmiuix/visual/check/VisualCheckedTextView;->setTextColor(I)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :goto_1
    return-void
.end method

.method private init(Lcom/miui/home/launcher/FolderInfo;)V
    .locals 2

    iput-object p1, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderInfo:Lcom/miui/home/launcher/FolderInfo;

    iget v0, p1, Lcom/miui/home/launcher/FolderInfo;->itemType:I

    iput v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderType:I

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderSheet;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f010043

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/folder/FolderSheet;->setAnimation(Landroid/view/animation/Animation;)V

    invoke-virtual {p0, p1}, Lcom/miui/home/launcher/folder/FolderSheet;->initListener(Lcom/miui/home/launcher/FolderInfo;)V

    invoke-direct {p0}, Lcom/miui/home/launcher/folder/FolderSheet;->initPreviewIcon()V

    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderPickerAppPredictExposed:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    :cond_0
    invoke-direct {p0}, Lcom/miui/home/launcher/folder/FolderSheet;->setFolderPickerDarkModeColor()V

    invoke-virtual {p1}, Lcom/miui/home/launcher/FolderInfo;->isBigFolder()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    invoke-direct {p0, p1}, Lcom/miui/home/launcher/folder/FolderSheet;->isDefaultFolder(Z)V

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderSheet;->setFolderPickerLocation()V

    return-void
.end method

.method private initFolderPreviewIcon2x2()V
    .locals 7

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderInfo:Lcom/miui/home/launcher/FolderInfo;

    invoke-virtual {v0}, Lcom/miui/home/launcher/FolderInfo;->count()I

    move-result v0

    const/4 v1, 0x7

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    move-object v3, v1

    :goto_0
    if-ge v2, v0, :cond_2

    if-nez v3, :cond_0

    new-instance v3, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderSheet;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2$PreviewIconView;-><init>(Landroid/content/Context;)V

    :cond_0
    if-eqz v3, :cond_1

    iget-object v4, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderPickerSelectBigFolderImg:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    invoke-virtual {v4, v3}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->addPreView(Landroid/view/View;)V

    move-object v3, v1

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderPickerSelectBigFolderImg:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    invoke-virtual {v0}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->setFolderIconPlaceholderDrawableMatchingWallpaperColor()V

    iget-object v1, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderPickerSelectBigFolderImg:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    iget-object v2, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderInfo:Lcom/miui/home/launcher/FolderInfo;

    invoke-static {}, Lcom/miui/home/launcher/Application;->getInstance()Lcom/miui/home/launcher/Application;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/home/launcher/Application;->getIconCache()Lcom/miui/home/launcher/IconCache;

    move-result-object v3

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mAppPredictSlidingButton:Lmiuix/slidingwidget/widget/SlidingButton;

    invoke-virtual {v0}, Lmiuix/slidingwidget/widget/SlidingButton;->isChecked()Z

    move-result v4

    iget-object v5, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mSerialExecutor:Lcom/miui/home/library/utils/AsyncTaskExecutorHelper$SerialExecutor;

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->loadItemIcons(Lcom/miui/home/launcher/FolderInfo;Lcom/miui/home/launcher/IconCache;ZLcom/miui/home/library/utils/AsyncTaskExecutorHelper$SerialExecutor;Z)V

    return-void
.end method

.method private initPreviewIcon()V
    .locals 4

    invoke-direct {p0}, Lcom/miui/home/launcher/folder/FolderSheet;->initFolderPreviewIcon2x2()V

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderPickerSelectDefaultFolderImg:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer1X1;

    iget-object v1, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderInfo:Lcom/miui/home/launcher/FolderInfo;

    invoke-static {}, Lcom/miui/home/launcher/Application;->getInstance()Lcom/miui/home/launcher/Application;

    move-result-object v2

    invoke-virtual {v2}, Lcom/miui/home/launcher/Application;->getIconCache()Lcom/miui/home/launcher/IconCache;

    move-result-object v2

    iget-object v3, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mSerialExecutor:Lcom/miui/home/library/utils/AsyncTaskExecutorHelper$SerialExecutor;

    invoke-virtual {v0, v1, v2, v3}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer1X1;->loadItemIcons(Lcom/miui/home/launcher/FolderInfo;Lcom/miui/home/launcher/IconCache;Lcom/miui/home/library/utils/AsyncTaskExecutorHelper$SerialExecutor;)V

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderPickerSelectBigFolderBg:Landroid/widget/ImageView;

    new-instance v1, Lcom/miui/home/launcher/folder/FolderIcon4x4NormalBackgroundDrawable;

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderSheet;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/miui/home/launcher/folder/FolderIcon4x4NormalBackgroundDrawable;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderInfo:Lcom/miui/home/launcher/FolderInfo;

    iget v0, v0, Lcom/miui/home/launcher/FolderInfo;->itemType:I

    const/16 v1, 0x15

    if-eq v0, v1, :cond_0

    invoke-direct {p0}, Lcom/miui/home/launcher/folder/FolderSheet;->setDefaultFolderVisible()V

    invoke-direct {p0}, Lcom/miui/home/launcher/folder/FolderSheet;->setBigFolderGone()V

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/miui/home/launcher/folder/FolderSheet;->setBigFolderVisible()V

    invoke-direct {p0}, Lcom/miui/home/launcher/folder/FolderSheet;->setDefaultFolderGone()V

    :goto_0
    return-void
.end method

.method private internationalNotSupportAppPredict()V
    .locals 3

    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mAppPredictSlidingButton:Lmiuix/slidingwidget/widget/SlidingButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lmiuix/slidingwidget/widget/SlidingButton;->setChecked(Z)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mAppPredictSlidingButton:Lmiuix/slidingwidget/widget/SlidingButton;

    iget-object v1, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderInfo:Lcom/miui/home/launcher/FolderInfo;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/miui/home/launcher/FolderInfo;->isAppPredictOpen(Z)Z

    move-result v1

    invoke-virtual {v0, v1}, Lmiuix/slidingwidget/widget/SlidingButton;->setChecked(Z)V

    :goto_0
    return-void
.end method

.method private isDefaultFolder(Z)V
    .locals 1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderPickerAppPredictExposed:Landroid/widget/RelativeLayout;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/RelativeLayout;->setClickable(Z)V

    iget-object p1, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mAppPredictSlidingButton:Lmiuix/slidingwidget/widget/SlidingButton;

    invoke-virtual {p1, v0}, Lmiuix/slidingwidget/widget/SlidingButton;->setEnabled(Z)V

    iget-object p1, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mAppPredictSlidingButton:Lmiuix/slidingwidget/widget/SlidingButton;

    invoke-virtual {p1, v0}, Lmiuix/slidingwidget/widget/SlidingButton;->setChecked(Z)V

    :cond_0
    return-void
.end method

.method static synthetic lambda$show$0(Lcom/miui/home/launcher/FolderInfo;Lcom/miui/home/launcher/Launcher;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 0

    instance-of p3, p2, Lcom/miui/home/launcher/folder/FolderSheet;

    if-nez p3, :cond_0

    return-void

    :cond_0
    check-cast p2, Lcom/miui/home/launcher/folder/FolderSheet;

    invoke-direct {p2, p0}, Lcom/miui/home/launcher/folder/FolderSheet;->init(Lcom/miui/home/launcher/FolderInfo;)V

    invoke-virtual {p1}, Lcom/miui/home/launcher/Launcher;->getDragLayer()Lcom/miui/home/launcher/DragLayer;

    move-result-object p0

    invoke-virtual {p0, p2}, Lcom/miui/home/launcher/DragLayer;->addView(Landroid/view/View;)V

    invoke-direct {p2}, Lcom/miui/home/launcher/folder/FolderSheet;->handleOpen()V

    return-void
.end method

.method private restoreLauncherBlurWithAnim()V
    .locals 4

    invoke-static {}, Lcom/miui/home/launcher/Application;->getLauncher()Lcom/miui/home/launcher/Launcher;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/16 v3, 0x118

    invoke-static {v0, v1, v2, v3}, Lcom/miui/home/launcher/common/BlurUtils;->fastBlurWhenExitFolderPicker(Lcom/miui/home/launcher/Launcher;FZI)V

    return-void
.end method

.method private setBigFolderGone()V
    .locals 2

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderPickerSelectBigFolderBg:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderPickerSelectBigFolderImg:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    invoke-virtual {v0, v1}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->setVisibility(I)V

    return-void
.end method

.method private setBigFolderVisible()V
    .locals 2

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderPickerSelectBigFolderBg:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderPickerSelectBigFolderImg:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    invoke-virtual {v0, v1}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->setVisibility(I)V

    return-void
.end method

.method private setDefaultFolderGone()V
    .locals 2

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderPickerSelectDefaultFolderBg:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderPickerSelectDefaultFolderImg:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer1X1;

    invoke-virtual {v0, v1}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer1X1;->setVisibility(I)V

    return-void
.end method

.method private setDefaultFolderVisible()V
    .locals 2

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderPickerSelectDefaultFolderBg:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderPickerSelectDefaultFolderImg:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer1X1;

    invoke-virtual {v0, v1}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer1X1;->setVisibility(I)V

    return-void
.end method

.method private setFolderPickerDarkModeColor()V
    .locals 2

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderPickerDragLine:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderSheet;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/miui/home/launcher/folder/FolderSheet;->getFolderPickerDragLineDrawable(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderPickerSelectWallpaperBg:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderSheet;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/miui/home/launcher/folder/FolderSheet;->getFolderSheetWallpaperBg(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mDefaultFolderSelectBorder:Lcom/miui/home/settings/FixedAspectRatioLottieAnimView;

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderSheet;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/miui/home/launcher/folder/FolderSheet;->getFolderSheetDefaultFolderBg(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/miui/home/settings/FixedAspectRatioLottieAnimView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mBigFolderSelectBorder:Lcom/miui/home/settings/FixedAspectRatioLottieAnimView;

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderSheet;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/miui/home/launcher/folder/FolderSheet;->getFolderSheetBigFolderBg(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/miui/home/settings/FixedAspectRatioLottieAnimView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderSheet;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/folder/FolderSheet;->getFolderSheetBackgroundDrawable(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/folder/FolderSheet;->setBackground(Landroid/graphics/drawable/Drawable;)V

    invoke-static {}, Lcom/miui/home/launcher/Application;->getInstance()Lcom/miui/home/launcher/Application;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/home/launcher/Application;->isInFoldLargeScreenMode()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/miui/home/launcher/DeviceConfig;->isHasNavigationBar()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderSheet;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/miui/home/launcher/Launcher;->getLauncher(Landroid/content/Context;)Lcom/miui/home/launcher/Launcher;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/home/launcher/Launcher;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderSheet;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/miui/home/launcher/folder/FolderSheet;->getNavigationBarColor(Landroid/content/Context;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/Window;->setNavigationBarColor(I)V

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/miui/home/launcher/Application;->getInstance()Lcom/miui/home/launcher/Application;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/home/launcher/Application;->isInFoldLargeScreenMode()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lcom/miui/home/launcher/DeviceConfig;->isHasNavigationBar()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderSheet;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/miui/home/launcher/Launcher;->getLauncher(Landroid/content/Context;)Lcom/miui/home/launcher/Launcher;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/home/launcher/Launcher;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderSheet;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/miui/home/launcher/folder/FolderSheet;->getNavigationBarColor(Landroid/content/Context;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/Window;->setStatusBarColor(I)V

    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderPickerOk:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderSheet;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/miui/home/launcher/folder/FolderSheet;->getFolderPickerTitleColor(Landroid/content/Context;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setColorFilter(I)V

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderPickerCancel:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderSheet;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/miui/home/launcher/folder/FolderSheet;->getFolderPickerTitleColor(Landroid/content/Context;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setColorFilter(I)V

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderPickerTitle:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderSheet;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/miui/home/launcher/folder/FolderSheet;->getFolderPickerTitleColor(Landroid/content/Context;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderPickerAppPredictTitle:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderSheet;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/miui/home/launcher/folder/FolderSheet;->getFolderPickerTitleColor(Landroid/content/Context;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderPickerAppPredictSummary:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderSheet;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/miui/home/launcher/folder/FolderSheet;->getFolderPickerSummaryText(Landroid/content/Context;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method

.method private setLauncherBlurWithAnim()V
    .locals 4

    invoke-static {}, Lcom/miui/home/launcher/Application;->getLauncher()Lcom/miui/home/launcher/Launcher;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x1

    const/16 v3, 0x118

    invoke-static {v0, v1, v2, v3}, Lcom/miui/home/launcher/common/BlurUtils;->fastBlurWhenEnterFolderPicker(Lcom/miui/home/launcher/Launcher;FZI)V

    return-void
.end method

.method private setNavigationBarOrStatusBarColor(Landroid/content/Context;)V
    .locals 1

    invoke-static {}, Lcom/miui/home/launcher/DeviceConfig;->isHasNavigationBar()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/miui/home/launcher/Launcher;->getLauncher(Landroid/content/Context;)Lcom/miui/home/launcher/Launcher;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/home/launcher/Launcher;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {p0, p1}, Lcom/miui/home/launcher/folder/FolderSheet;->getNavigationBarColor(Landroid/content/Context;)I

    move-result p1

    invoke-virtual {v0, p1}, Landroid/view/Window;->setNavigationBarColor(I)V

    goto :goto_0

    :cond_0
    invoke-static {p1}, Lcom/miui/home/launcher/Launcher;->getLauncher(Landroid/content/Context;)Lcom/miui/home/launcher/Launcher;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/home/launcher/Launcher;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {p0, p1}, Lcom/miui/home/launcher/folder/FolderSheet;->getNavigationBarColor(Landroid/content/Context;)I

    move-result p1

    invoke-virtual {v0, p1}, Landroid/view/Window;->setStatusBarColor(I)V

    :goto_0
    return-void
.end method

.method private setNavigationBarOrStatusBarTransparent(Landroid/content/Context;)V
    .locals 2

    invoke-static {}, Lcom/miui/home/launcher/DeviceConfig;->isHasNavigationBar()Z

    move-result v0

    const v1, 0x7f0605e3

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/miui/home/launcher/Launcher;->getLauncher(Landroid/content/Context;)Lcom/miui/home/launcher/Launcher;

    move-result-object p1

    invoke-virtual {p1}, Lcom/miui/home/launcher/Launcher;->getWindow()Landroid/view/Window;

    move-result-object p1

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderSheet;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/Window;->setNavigationBarColor(I)V

    goto :goto_0

    :cond_0
    invoke-static {p1}, Lcom/miui/home/launcher/Launcher;->getLauncher(Landroid/content/Context;)Lcom/miui/home/launcher/Launcher;

    move-result-object p1

    invoke-virtual {p1}, Lcom/miui/home/launcher/Launcher;->getWindow()Landroid/view/Window;

    move-result-object p1

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderSheet;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/Window;->setStatusBarColor(I)V

    :goto_0
    return-void
.end method

.method public static show(Lcom/miui/home/launcher/Launcher;Lcom/miui/home/launcher/FolderInfo;)V
    .locals 3

    new-instance v0, Landroidx/asynclayoutinflater/view/AsyncLayoutInflater;

    invoke-direct {v0, p0}, Landroidx/asynclayoutinflater/view/AsyncLayoutInflater;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/miui/home/launcher/Launcher;->getDragLayer()Lcom/miui/home/launcher/DragLayer;

    move-result-object v1

    new-instance v2, Lcom/miui/home/launcher/folder/-$$Lambda$FolderSheet$2NNobqWbcYT9JXh_r6Rv6qUg0u4;

    invoke-direct {v2, p1, p0}, Lcom/miui/home/launcher/folder/-$$Lambda$FolderSheet$2NNobqWbcYT9JXh_r6Rv6qUg0u4;-><init>(Lcom/miui/home/launcher/FolderInfo;Lcom/miui/home/launcher/Launcher;)V

    const p0, 0x7f0d0076

    invoke-virtual {v0, p0, v1, v2}, Landroidx/asynclayoutinflater/view/AsyncLayoutInflater;->inflate(ILandroid/view/ViewGroup;Landroidx/asynclayoutinflater/view/AsyncLayoutInflater$OnInflateFinishedListener;)V

    return-void
.end method

.method private trackQuitFolderPicker()V
    .locals 5

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderInfo:Lcom/miui/home/launcher/FolderInfo;

    invoke-virtual {v0}, Lcom/miui/home/launcher/FolderInfo;->getFolderGridSize()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0}, Lcom/miui/home/launcher/folder/FolderSheet;->getFolderSizeByType()Ljava/lang/String;

    move-result-object v1

    sget-boolean v2, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v2, :cond_0

    const-string v2, "false"

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/miui/home/launcher/Application;->getLauncher()Lcom/miui/home/launcher/Launcher;

    move-result-object v2

    iget-object v3, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderInfo:Lcom/miui/home/launcher/FolderInfo;

    invoke-virtual {v3}, Lcom/miui/home/launcher/FolderInfo;->getAppPredictPreferenceKey()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/miui/home/launcher/FolderInfo$AppPredictStatus;->APP_PREDICT_UNINITIALIZED:Lcom/miui/home/launcher/FolderInfo$AppPredictStatus;

    invoke-virtual {v4}, Lcom/miui/home/launcher/FolderInfo$AppPredictStatus;->getValue()I

    move-result v4

    invoke-static {v2, v3, v4}, Lcom/miui/home/launcher/common/PreferenceUtils;->getInt(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v2

    iget v3, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mBeforeAppPredictButtonStatus:I

    if-ne v2, v3, :cond_1

    const-string v2, "false"

    goto :goto_0

    :cond_1
    const-string v2, "true"

    :goto_0
    invoke-static {v0, v1, v2}, Lcom/miui/home/launcher/AnalyticalDataCollector;->trackQuitFolderPicker(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public getFolderSheetBackgroundDrawable(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;
    .locals 1

    invoke-static {}, Lcom/miui/home/launcher/DeviceConfig;->isDarkMode()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lcom/miui/home/launcher/Application;->getInstance()Lcom/miui/home/launcher/Application;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/home/launcher/Application;->isInFoldLargeScreenMode()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f080219

    invoke-static {p1, v0}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    return-object p1

    :cond_0
    const v0, 0x7f08021f

    invoke-static {p1, v0}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    return-object p1

    :cond_1
    invoke-static {}, Lcom/miui/home/launcher/Application;->getInstance()Lcom/miui/home/launcher/Application;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/home/launcher/Application;->isInFoldLargeScreenMode()Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f080218

    invoke-static {p1, v0}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    return-object p1

    :cond_2
    const v0, 0x7f08021e

    invoke-static {p1, v0}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    return-object p1
.end method

.method public getFolderSheetBigFolderBg(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;
    .locals 1

    invoke-static {}, Lcom/miui/home/launcher/DeviceConfig;->isDarkMode()Z

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x7f080296

    invoke-static {p1, v0}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    return-object p1

    :cond_0
    const v0, 0x7f080297

    invoke-static {p1, v0}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    return-object p1
.end method

.method public getFolderSheetDefaultFolderBg(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;
    .locals 1

    invoke-static {}, Lcom/miui/home/launcher/DeviceConfig;->isDarkMode()Z

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x7f0802a3

    invoke-static {p1, v0}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    return-object p1

    :cond_0
    const v0, 0x7f0802a4

    invoke-static {p1, v0}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    return-object p1
.end method

.method public getFolderSheetWallpaperBg(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;
    .locals 1

    invoke-static {}, Lcom/miui/home/launcher/DeviceConfig;->isDarkMode()Z

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x7f0802b4

    invoke-static {p1, v0}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    return-object p1

    :cond_0
    const v0, 0x7f0802b3

    invoke-static {p1, v0}, Landroidx/core/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    return-object p1
.end method

.method public getNavigationBarColor(Landroid/content/Context;)I
    .locals 1

    invoke-static {}, Lcom/miui/home/launcher/DeviceConfig;->isDarkMode()Z

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x7f060100

    invoke-static {p1, v0}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p1

    return p1

    :cond_0
    const v0, 0x7f0600ff

    invoke-static {p1, v0}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p1

    return p1
.end method

.method protected handleClose(Z)V
    .locals 1

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mIsOpen:Z

    sput-boolean p1, Lcom/miui/home/launcher/folder/FolderSheet;->isFolderSheetViewShow:Z

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderSheet;->getContext()Landroid/content/Context;

    move-result-object p1

    const v0, 0x7f010037

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/miui/home/launcher/folder/FolderSheet;->setAnimation(Landroid/view/animation/Animation;)V

    new-instance v0, Lcom/miui/home/launcher/folder/FolderSheet$1;

    invoke-direct {v0, p0}, Lcom/miui/home/launcher/folder/FolderSheet$1;-><init>(Lcom/miui/home/launcher/folder/FolderSheet;)V

    invoke-virtual {p1, v0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderSheet;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/miui/home/launcher/Launcher;->getLauncher(Landroid/content/Context;)Lcom/miui/home/launcher/Launcher;

    move-result-object p1

    invoke-virtual {p1}, Lcom/miui/home/launcher/Launcher;->notifyBackGestureStatus()V

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderSheet;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/miui/home/launcher/Launcher;->getLauncher(Landroid/content/Context;)Lcom/miui/home/launcher/Launcher;

    move-result-object p1

    invoke-virtual {p1}, Lcom/miui/home/launcher/Launcher;->getDragLayer()Lcom/miui/home/launcher/DragLayer;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/miui/home/launcher/DragLayer;->removeView(Landroid/view/View;)V

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lcom/miui/home/launcher/folder/FolderSheet;->handleLauncherIconVisible(Z)V

    invoke-direct {p0}, Lcom/miui/home/launcher/folder/FolderSheet;->restoreLauncherBlurWithAnim()V

    invoke-direct {p0}, Lcom/miui/home/launcher/folder/FolderSheet;->trackQuitFolderPicker()V

    return-void
.end method

.method public initListener(Lcom/miui/home/launcher/FolderInfo;)V
    .locals 3

    const v0, 0x7f0a0177

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/folder/FolderSheet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderPickerBody:Landroid/widget/ScrollView;

    const v0, 0x7f0a0178

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/folder/FolderSheet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderPickerCancel:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderPickerCancel:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0a017a

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/folder/FolderSheet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderPickerOk:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderPickerOk:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0a0181

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/folder/FolderSheet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderPickerTitle:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderPickerTitle:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderSheet;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/miui/home/launcher/FolderInfo;->getTitle(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0a0179

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/folder/FolderSheet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderPickerDragLine:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderPickerDragLine:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    const v0, 0x7f0a0180

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/folder/FolderSheet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderPickerSelectWallpaperBg:Landroid/widget/ImageView;

    const v0, 0x7f0a00fa

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/folder/FolderSheet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiuix/visual/check/VisualCheckedTextView;

    iput-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mDefaultFolderName:Lmiuix/visual/check/VisualCheckedTextView;

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mDefaultFolderName:Lmiuix/visual/check/VisualCheckedTextView;

    invoke-virtual {v0, p0}, Lmiuix/visual/check/VisualCheckedTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0a008c

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/folder/FolderSheet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiuix/visual/check/VisualCheckedTextView;

    iput-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mBigFolderName:Lmiuix/visual/check/VisualCheckedTextView;

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mBigFolderName:Lmiuix/visual/check/VisualCheckedTextView;

    invoke-virtual {v0, p0}, Lmiuix/visual/check/VisualCheckedTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0a00f9

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/folder/FolderSheet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiuix/visual/check/VisualCheckBox;

    iput-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mDefaultFolderCheckBox:Lmiuix/visual/check/VisualCheckBox;

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mDefaultFolderCheckBox:Lmiuix/visual/check/VisualCheckBox;

    invoke-virtual {v0, p0}, Lmiuix/visual/check/VisualCheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0a008b

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/folder/FolderSheet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiuix/visual/check/VisualCheckBox;

    iput-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mBigFolderCheckBox:Lmiuix/visual/check/VisualCheckBox;

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mBigFolderCheckBox:Lmiuix/visual/check/VisualCheckBox;

    invoke-virtual {v0, p0}, Lmiuix/visual/check/VisualCheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0a00fb

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/folder/FolderSheet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/miui/home/settings/FixedAspectRatioLottieAnimView;

    iput-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mDefaultFolderSelectBorder:Lcom/miui/home/settings/FixedAspectRatioLottieAnimView;

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mDefaultFolderSelectBorder:Lcom/miui/home/settings/FixedAspectRatioLottieAnimView;

    invoke-virtual {v0, p0}, Lcom/miui/home/settings/FixedAspectRatioLottieAnimView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0a008d

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/folder/FolderSheet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/miui/home/settings/FixedAspectRatioLottieAnimView;

    iput-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mBigFolderSelectBorder:Lcom/miui/home/settings/FixedAspectRatioLottieAnimView;

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mBigFolderSelectBorder:Lcom/miui/home/settings/FixedAspectRatioLottieAnimView;

    invoke-virtual {v0, p0}, Lcom/miui/home/settings/FixedAspectRatioLottieAnimView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0a017d

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/folder/FolderSheet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    iput-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderPickerSelectBigFolderImg:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    const v0, 0x7f0a017c

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/folder/FolderSheet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderPickerSelectBigFolderBg:Landroid/widget/ImageView;

    const v0, 0x7f0a017f

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/folder/FolderSheet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer1X1;

    iput-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderPickerSelectDefaultFolderImg:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer1X1;

    const v0, 0x7f0a017e

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/folder/FolderSheet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderPickerSelectDefaultFolderBg:Landroid/widget/ImageView;

    const v0, 0x7f0a0174

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/folder/FolderSheet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderPickerAppPredictExposed:Landroid/widget/RelativeLayout;

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderPickerAppPredictExposed:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0a0176

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/folder/FolderSheet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderPickerAppPredictTitle:Landroid/widget/TextView;

    const v0, 0x7f0a0175

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/folder/FolderSheet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderPickerAppPredictSummary:Landroid/widget/TextView;

    const v0, 0x7f0a0070

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/folder/FolderSheet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiuix/slidingwidget/widget/SlidingButton;

    iput-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mAppPredictSlidingButton:Lmiuix/slidingwidget/widget/SlidingButton;

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mAppPredictSlidingButton:Lmiuix/slidingwidget/widget/SlidingButton;

    invoke-virtual {v0, p0}, Lmiuix/slidingwidget/widget/SlidingButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mAppPredictSlidingButton:Lmiuix/slidingwidget/widget/SlidingButton;

    iget-object v1, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderInfo:Lcom/miui/home/launcher/FolderInfo;

    invoke-virtual {v1}, Lcom/miui/home/launcher/FolderInfo;->isAppPredictOpen()Z

    move-result v1

    invoke-virtual {v0, v1}, Lmiuix/slidingwidget/widget/SlidingButton;->setChecked(Z)V

    invoke-static {}, Lcom/miui/home/launcher/Application;->getLauncher()Lcom/miui/home/launcher/Launcher;

    move-result-object v0

    invoke-virtual {p1}, Lcom/miui/home/launcher/FolderInfo;->getAppPredictPreferenceKey()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/miui/home/launcher/FolderInfo$AppPredictStatus;->APP_PREDICT_UNINITIALIZED:Lcom/miui/home/launcher/FolderInfo$AppPredictStatus;

    invoke-virtual {v2}, Lcom/miui/home/launcher/FolderInfo$AppPredictStatus;->getValue()I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/miui/home/launcher/common/PreferenceUtils;->getInt(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mBeforeAppPredictButtonStatus:I

    iget p1, p1, Lcom/miui/home/launcher/FolderInfo;->itemType:I

    const/16 v0, 0x15

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-virtual {p0, p1}, Lcom/miui/home/launcher/folder/FolderSheet;->setCheckedBoxByBigFolder(Z)V

    return-void
.end method

.method protected isOfType(I)Z
    .locals 0

    and-int/lit16 p1, p1, 0x1000

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method protected onAttachedToWindow()V
    .locals 2

    invoke-super {p0}, Lcom/miui/home/launcher/AbstractFloatingView;->onAttachedToWindow()V

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderSheet;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/miui/home/launcher/Launcher;->getLauncher(Landroid/content/Context;)Lcom/miui/home/launcher/Launcher;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/home/launcher/Launcher;->getStateManager()Lcom/miui/home/launcher/LauncherStateManager;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/home/launcher/folder/FolderSheet;->listener:Lcom/miui/home/launcher/LauncherStateManager$StateListener;

    invoke-virtual {v0, v1}, Lcom/miui/home/launcher/LauncherStateManager;->addStateListener(Lcom/miui/home/launcher/LauncherStateManager$StateListener;)V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    invoke-super {p0}, Lcom/miui/home/launcher/AbstractFloatingView;->onBackPressed()Z

    const/4 v0, 0x1

    return v0
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 6

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mAppPredictSlidingButton:Lmiuix/slidingwidget/widget/SlidingButton;

    if-ne p1, v0, :cond_1

    if-eqz p2, :cond_0

    iget-object p1, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderInfo:Lcom/miui/home/launcher/FolderInfo;

    invoke-virtual {p1}, Lcom/miui/home/launcher/FolderInfo;->updateAppPredictList()Z

    :cond_0
    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderPickerSelectBigFolderImg:Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;

    iget-object v1, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderInfo:Lcom/miui/home/launcher/FolderInfo;

    invoke-static {}, Lcom/miui/home/launcher/Application;->getInstance()Lcom/miui/home/launcher/Application;

    move-result-object p1

    invoke-virtual {p1}, Lcom/miui/home/launcher/Application;->getIconCache()Lcom/miui/home/launcher/IconCache;

    move-result-object v2

    iget-object v4, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mSerialExecutor:Lcom/miui/home/library/utils/AsyncTaskExecutorHelper$SerialExecutor;

    const/4 v5, 0x1

    move v3, p2

    invoke-virtual/range {v0 .. v5}, Lcom/miui/home/launcher/folder/FolderIconPreviewContainer2X2;->loadItemIcons(Lcom/miui/home/launcher/FolderInfo;Lcom/miui/home/launcher/IconCache;ZLcom/miui/home/library/utils/AsyncTaskExecutorHelper$SerialExecutor;Z)V

    :cond_1
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result p1

    const/4 v0, 0x1

    sparse-switch p1, :sswitch_data_0

    goto/16 :goto_1

    :sswitch_0
    iget-object p1, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderInfo:Lcom/miui/home/launcher/FolderInfo;

    iget-object v1, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mAppPredictSlidingButton:Lmiuix/slidingwidget/widget/SlidingButton;

    invoke-virtual {v1}, Lmiuix/slidingwidget/widget/SlidingButton;->isChecked()Z

    move-result v1

    invoke-virtual {p1, v1}, Lcom/miui/home/launcher/FolderInfo;->switchAppPredict(Z)V

    iget-object p1, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mAppPredictSlidingButton:Lmiuix/slidingwidget/widget/SlidingButton;

    invoke-virtual {p1}, Lmiuix/slidingwidget/widget/SlidingButton;->isChecked()Z

    move-result p1

    if-eqz p1, :cond_0

    sget-object p1, Lcom/miui/home/launcher/folder/AppPredictHelper;->INSTANCE:Lcom/miui/home/launcher/folder/AppPredictHelper;

    iget-object v1, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderInfo:Lcom/miui/home/launcher/FolderInfo;

    invoke-virtual {p1, v1}, Lcom/miui/home/launcher/folder/AppPredictHelper;->addAppPredictListener(Lcom/miui/home/launcher/folder/AppPredictUpdateListener;)V

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/miui/home/launcher/folder/AppPredictHelper;->INSTANCE:Lcom/miui/home/launcher/folder/AppPredictHelper;

    iget-object v1, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderInfo:Lcom/miui/home/launcher/FolderInfo;

    invoke-virtual {p1, v1}, Lcom/miui/home/launcher/folder/AppPredictHelper;->removeAppPredictListener(Lcom/miui/home/launcher/folder/AppPredictUpdateListener;)Z

    :goto_0
    iget-object p1, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderInfo:Lcom/miui/home/launcher/FolderInfo;

    invoke-virtual {p1}, Lcom/miui/home/launcher/FolderInfo;->refreshPreviewIcons()V

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/folder/FolderSheet;->handleClose(Z)V

    iget-object p1, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderInfo:Lcom/miui/home/launcher/FolderInfo;

    iget p1, p1, Lcom/miui/home/launcher/FolderInfo;->itemType:I

    iget v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderType:I

    if-eq p1, v0, :cond_1

    iget-object p1, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderInfo:Lcom/miui/home/launcher/FolderInfo;

    invoke-virtual {p1, v0}, Lcom/miui/home/launcher/FolderInfo;->convertIconSize(I)V

    goto :goto_1

    :sswitch_1
    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/folder/FolderSheet;->handleClose(Z)V

    goto :goto_1

    :sswitch_2
    iget-object p1, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mAppPredictSlidingButton:Lmiuix/slidingwidget/widget/SlidingButton;

    invoke-virtual {p1}, Lmiuix/slidingwidget/widget/SlidingButton;->isChecked()Z

    move-result v1

    xor-int/2addr v0, v1

    invoke-virtual {p1, v0}, Lmiuix/slidingwidget/widget/SlidingButton;->setChecked(Z)V

    goto :goto_1

    :sswitch_3
    invoke-direct {p0}, Lcom/miui/home/launcher/folder/FolderSheet;->setDefaultFolderVisible()V

    invoke-direct {p0}, Lcom/miui/home/launcher/folder/FolderSheet;->setBigFolderGone()V

    iget-object p1, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderPickerAppPredictExposed:Landroid/widget/RelativeLayout;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/RelativeLayout;->setClickable(Z)V

    iget-object p1, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mAppPredictSlidingButton:Lmiuix/slidingwidget/widget/SlidingButton;

    invoke-virtual {p1, v0}, Lmiuix/slidingwidget/widget/SlidingButton;->setEnabled(Z)V

    iget-object p1, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mAppPredictSlidingButton:Lmiuix/slidingwidget/widget/SlidingButton;

    invoke-virtual {p1, v0}, Lmiuix/slidingwidget/widget/SlidingButton;->setChecked(Z)V

    const/4 p1, 0x2

    iput p1, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderType:I

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/folder/FolderSheet;->setCheckedBoxByBigFolder(Z)V

    goto :goto_1

    :sswitch_4
    invoke-direct {p0}, Lcom/miui/home/launcher/folder/FolderSheet;->setBigFolderVisible()V

    invoke-direct {p0}, Lcom/miui/home/launcher/folder/FolderSheet;->setDefaultFolderGone()V

    iget-object p1, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderPickerAppPredictExposed:Landroid/widget/RelativeLayout;

    invoke-virtual {p1, v0}, Landroid/widget/RelativeLayout;->setClickable(Z)V

    iget-object p1, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mAppPredictSlidingButton:Lmiuix/slidingwidget/widget/SlidingButton;

    invoke-virtual {p1, v0}, Lmiuix/slidingwidget/widget/SlidingButton;->setEnabled(Z)V

    invoke-direct {p0}, Lcom/miui/home/launcher/folder/FolderSheet;->internationalNotSupportAppPredict()V

    const/16 p1, 0x15

    iput p1, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderType:I

    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/folder/FolderSheet;->setCheckedBoxByBigFolder(Z)V

    :cond_1
    :goto_1
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0a008b -> :sswitch_4
        0x7f0a008c -> :sswitch_4
        0x7f0a008d -> :sswitch_4
        0x7f0a00f9 -> :sswitch_3
        0x7f0a00fa -> :sswitch_3
        0x7f0a00fb -> :sswitch_3
        0x7f0a0174 -> :sswitch_2
        0x7f0a0178 -> :sswitch_1
        0x7f0a017a -> :sswitch_0
    .end sparse-switch
.end method

.method public onControllerInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public onDarkModeChange()V
    .locals 2

    invoke-direct {p0}, Lcom/miui/home/launcher/folder/FolderSheet;->setFolderPickerDarkModeColor()V

    invoke-direct {p0}, Lcom/miui/home/launcher/folder/FolderSheet;->handleSlidingButtonColorDarkModeSyncInternal()V

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mDefaultFolderCheckBox:Lmiuix/visual/check/VisualCheckBox;

    invoke-virtual {v0}, Lmiuix/visual/check/VisualCheckBox;->isChecked()Z

    move-result v0

    iget-object v1, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mDefaultFolderName:Lmiuix/visual/check/VisualCheckedTextView;

    invoke-direct {p0, v0, v1}, Lcom/miui/home/launcher/folder/FolderSheet;->handleVisualCheckedTextViewColorDarkModeSyncInternal(ZLmiuix/visual/check/VisualCheckedTextView;)V

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mBigFolderCheckBox:Lmiuix/visual/check/VisualCheckBox;

    invoke-virtual {v0}, Lmiuix/visual/check/VisualCheckBox;->isChecked()Z

    move-result v0

    iget-object v1, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mBigFolderName:Lmiuix/visual/check/VisualCheckedTextView;

    invoke-direct {p0, v0, v1}, Lcom/miui/home/launcher/folder/FolderSheet;->handleVisualCheckedTextViewColorDarkModeSyncInternal(ZLmiuix/visual/check/VisualCheckedTextView;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    invoke-super {p0}, Lcom/miui/home/launcher/AbstractFloatingView;->onDetachedFromWindow()V

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderSheet;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/miui/home/launcher/Launcher;->getLauncher(Landroid/content/Context;)Lcom/miui/home/launcher/Launcher;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/home/launcher/Launcher;->getStateManager()Lcom/miui/home/launcher/LauncherStateManager;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/home/launcher/folder/FolderSheet;->listener:Lcom/miui/home/launcher/LauncherStateManager$StateListener;

    invoke-virtual {v0, v1}, Lcom/miui/home/launcher/LauncherStateManager;->removeStateListener(Lcom/miui/home/launcher/LauncherStateManager$StateListener;)V

    return-void
.end method

.method public onScreenSizeChange()V
    .locals 0

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderSheet;->setFolderPickerLocation()V

    invoke-direct {p0}, Lcom/miui/home/launcher/folder/FolderSheet;->setLauncherBlurWithAnim()V

    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1

    invoke-static {}, Lcom/miui/home/launcher/DeviceConfig;->getDeviceHeight()I

    move-result p1

    div-int/lit8 p1, p1, 0x2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    sget-boolean p2, Lcom/miui/home/launcher/folder/FolderSheet;->isFolderSheetViewShow:Z

    if-eqz p2, :cond_1

    iget p2, p0, Lcom/miui/home/launcher/folder/FolderSheet;->moveY:F

    invoke-direct {p0, p2, p1}, Lcom/miui/home/launcher/folder/FolderSheet;->closeOrOpenPickerByTouch(FI)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result p1

    iput p1, p0, Lcom/miui/home/launcher/folder/FolderSheet;->moveY:F

    iget p1, p0, Lcom/miui/home/launcher/folder/FolderSheet;->moveY:F

    iget v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->downY:F

    sub-float/2addr p1, v0

    iget v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->eventY:F

    sub-float/2addr p1, v0

    const/4 v0, 0x0

    cmpl-float p1, p1, v0

    if-lez p1, :cond_0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result p1

    iget p2, p0, Lcom/miui/home/launcher/folder/FolderSheet;->downY:F

    sub-float/2addr p1, p2

    iget p2, p0, Lcom/miui/home/launcher/folder/FolderSheet;->eventY:F

    sub-float/2addr p1, p2

    invoke-virtual {p0, p1}, Lcom/miui/home/launcher/folder/FolderSheet;->setTranslationY(F)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/folder/FolderSheet;->setTranslationY(F)V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result p2

    invoke-direct {p0, p2, p1}, Lcom/miui/home/launcher/folder/FolderSheet;->closeOrOpenPickerByTouch(FI)V

    goto :goto_0

    :pswitch_3
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result p1

    iput p1, p0, Lcom/miui/home/launcher/folder/FolderSheet;->downY:F

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result p1

    iput p1, p0, Lcom/miui/home/launcher/folder/FolderSheet;->eventY:F

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderSheet;->getTranslationY()F

    move-result p1

    iput p1, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mTranslationY:F

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public setCheckedBoxByBigFolder(Z)V
    .locals 2

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mDefaultFolderCheckBox:Lmiuix/visual/check/VisualCheckBox;

    xor-int/lit8 v1, p1, 0x1

    invoke-virtual {v0, v1}, Lmiuix/visual/check/VisualCheckBox;->setChecked(Z)V

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mBigFolderCheckBox:Lmiuix/visual/check/VisualCheckBox;

    invoke-virtual {v0, p1}, Lmiuix/visual/check/VisualCheckBox;->setChecked(Z)V

    return-void
.end method

.method public setFolderPickerLocation()V
    .locals 4

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderSheet;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-static {}, Lcom/miui/home/launcher/Application;->getInstance()Lcom/miui/home/launcher/Application;

    move-result-object v1

    invoke-virtual {v1}, Lcom/miui/home/launcher/Application;->isInFoldLargeScreenMode()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderSheet;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f07016d

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    invoke-static {}, Lcom/miui/home/launcher/DeviceConfig;->getScreenWidth()I

    move-result v1

    iget v3, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    sub-int/2addr v1, v3

    div-int/lit8 v1, v1, 0x2

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    iget-object v1, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderPickerDragLine:Landroid/widget/ImageView;

    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderSheet;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/miui/home/launcher/folder/FolderSheet;->setNavigationBarOrStatusBarTransparent(Landroid/content/Context;)V

    invoke-static {}, Lcom/miui/home/launcher/DeviceConfig;->isScreenOrientationLandscape()Z

    move-result v1

    if-eqz v1, :cond_0

    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    goto :goto_0

    :cond_0
    const/high16 v1, 0x41a00000    # 20.0f

    invoke-static {v1}, Lcom/miui/home/launcher/common/Utilities;->dp2px(F)I

    move-result v1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    goto :goto_0

    :cond_1
    const/4 v1, -0x1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    iget-object v1, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderPickerDragLine:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderSheet;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/miui/home/launcher/folder/FolderSheet;->setNavigationBarOrStatusBarColor(Landroid/content/Context;)V

    :goto_0
    invoke-virtual {p0, v0}, Lcom/miui/home/launcher/folder/FolderSheet;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderSheet;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070195

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0}, Lcom/miui/home/launcher/folder/FolderSheet;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f07019b

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iget-object v3, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderPickerBody:Landroid/widget/ScrollView;

    invoke-virtual {v3, v0, v2, v0, v2}, Landroid/widget/ScrollView;->setPadding(IIII)V

    iget-object v0, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderPickerSelectWallpaperBg:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getPaddingTop()I

    move-result v2

    iget-object v3, p0, Lcom/miui/home/launcher/folder/FolderSheet;->mFolderPickerSelectWallpaperBg:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getPaddingBottom()I

    move-result v3

    invoke-virtual {v0, v1, v2, v1, v3}, Landroid/widget/ImageView;->setPadding(IIII)V

    return-void
.end method
