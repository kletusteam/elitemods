.class public final Lcom/miui/home/launcher/folder/AppPredictHelper;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAppPredictHelper.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AppPredictHelper.kt\ncom/miui/home/launcher/folder/AppPredictHelper\n+ 2 _Arrays.kt\nkotlin/collections/ArraysKt___ArraysKt\n+ 3 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,191:1\n13571#2,3:192\n734#3:195\n825#3,2:196\n1819#3,2:198\n1819#3,2:200\n*E\n*S KotlinDebug\n*F\n+ 1 AppPredictHelper.kt\ncom/miui/home/launcher/folder/AppPredictHelper\n*L\n99#1,3:192\n99#1:195\n99#1,2:196\n126#1,2:198\n152#1,2:200\n*E\n"
.end annotation


# static fields
.field public static final INSTANCE:Lcom/miui/home/launcher/folder/AppPredictHelper;

.field private static mAppPredictService:Lcom/miui/apppredict/IAppPredict;

.field private static final mAppPredictServiceConnection:Lcom/miui/home/launcher/folder/AppPredictHelper$mAppPredictServiceConnection$1;

.field private static mListenerList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/ref/WeakReference<",
            "Lcom/miui/home/launcher/folder/AppPredictUpdateListener;",
            ">;>;"
        }
    .end annotation
.end field

.field private static mPredictInfoList:Lcom/miui/home/launcher/folder/AppPredictList;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/miui/home/launcher/folder/AppPredictHelper;

    invoke-direct {v0}, Lcom/miui/home/launcher/folder/AppPredictHelper;-><init>()V

    sput-object v0, Lcom/miui/home/launcher/folder/AppPredictHelper;->INSTANCE:Lcom/miui/home/launcher/folder/AppPredictHelper;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/miui/home/launcher/folder/AppPredictHelper;->mListenerList:Ljava/util/ArrayList;

    new-instance v0, Lcom/miui/home/launcher/folder/AppPredictHelper$mAppPredictServiceConnection$1;

    invoke-direct {v0}, Lcom/miui/home/launcher/folder/AppPredictHelper$mAppPredictServiceConnection$1;-><init>()V

    sput-object v0, Lcom/miui/home/launcher/folder/AppPredictHelper;->mAppPredictServiceConnection:Lcom/miui/home/launcher/folder/AppPredictHelper$mAppPredictServiceConnection$1;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final synthetic access$getAppPredictFromServiceInternal(Lcom/miui/home/launcher/folder/AppPredictHelper;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/home/launcher/folder/AppPredictHelper;->getAppPredictFromServiceInternal(Z)V

    return-void
.end method

.method public static final synthetic access$getMPredictInfoList$p(Lcom/miui/home/launcher/folder/AppPredictHelper;)Lcom/miui/home/launcher/folder/AppPredictList;
    .locals 0

    sget-object p0, Lcom/miui/home/launcher/folder/AppPredictHelper;->mPredictInfoList:Lcom/miui/home/launcher/folder/AppPredictList;

    return-object p0
.end method

.method public static final synthetic access$setMAppPredictService$p(Lcom/miui/home/launcher/folder/AppPredictHelper;Lcom/miui/apppredict/IAppPredict;)V
    .locals 0

    sput-object p1, Lcom/miui/home/launcher/folder/AppPredictHelper;->mAppPredictService:Lcom/miui/apppredict/IAppPredict;

    return-void
.end method

.method public static final synthetic access$setMPredictInfoList$p(Lcom/miui/home/launcher/folder/AppPredictHelper;Lcom/miui/home/launcher/folder/AppPredictList;)V
    .locals 0

    sput-object p1, Lcom/miui/home/launcher/folder/AppPredictHelper;->mPredictInfoList:Lcom/miui/home/launcher/folder/AppPredictList;

    return-void
.end method

.method public static final synthetic access$updateAppPredict(Lcom/miui/home/launcher/folder/AppPredictHelper;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/home/launcher/folder/AppPredictHelper;->updateAppPredict()V

    return-void
.end method

.method private final addUnFindShortcutInfoToList(Ljava/util/List;Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/miui/home/launcher/ShortcutInfo;",
            ">;",
            "Ljava/util/ArrayList<",
            "Lcom/miui/home/launcher/ShortcutInfo;",
            ">;)V"
        }
    .end annotation

    check-cast p1, Ljava/lang/Iterable;

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/home/launcher/ShortcutInfo;

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-void
.end method

.method private final connectToService(Landroid/content/Context;)V
    .locals 4

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.miui.securitycenter"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.miui.securitycenter"

    const-string v3, "com.miui.apppredict.service.AiService"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    sget-object v1, Lcom/miui/home/launcher/folder/AppPredictHelper;->mAppPredictServiceConnection:Lcom/miui/home/launcher/folder/AppPredictHelper$mAppPredictServiceConnection$1;

    check-cast v1, Landroid/content/ServiceConnection;

    const/4 v2, 0x1

    invoke-virtual {p1, v0, v1, v2}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    return-void
.end method

.method private final filterPredictAppSorted(Ljava/util/List;)Ljava/util/List;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/miui/home/launcher/ShortcutInfo;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/miui/home/launcher/ShortcutInfo;",
            ">;"
        }
    .end annotation

    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    :try_start_0
    sget-object v1, Lcom/miui/home/launcher/folder/AppPredictHelper;->mPredictInfoList:Lcom/miui/home/launcher/folder/AppPredictList;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/miui/home/launcher/folder/AppPredictList;->getApp_list()[Lcom/miui/home/launcher/folder/AppPredictItem;

    move-result-object v1

    if-eqz v1, :cond_5

    array-length v2, v1

    const/4 v3, 0x0

    move v4, v3

    move v5, v4

    :goto_0
    if-ge v4, v2, :cond_0

    aget-object v6, v1, v4

    add-int/lit8 v7, v5, 0x1

    move-object v8, v0

    check-cast v8, Ljava/util/Map;

    new-instance v9, Lkotlin/Pair;

    invoke-virtual {v6}, Lcom/miui/home/launcher/folder/AppPredictItem;->getPackage_name()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6}, Lcom/miui/home/launcher/folder/AppPredictItem;->is_xspace()Z

    move-result v6

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-direct {v9, v10, v6}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v8, v9, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v4, v4, 0x1

    move v5, v7

    goto :goto_0

    :cond_0
    if-eqz p1, :cond_4

    move-object v1, p1

    check-cast v1, Ljava/lang/Iterable;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    check-cast v2, Ljava/util/Collection;

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object v5, v4

    check-cast v5, Lcom/miui/home/launcher/ShortcutInfo;

    new-instance v6, Lkotlin/Pair;

    invoke-virtual {v5}, Lcom/miui/home/launcher/ShortcutInfo;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5}, Lcom/miui/home/launcher/ShortcutInfo;->getUser()Landroid/os/UserHandle;

    move-result-object v5

    invoke-static {v5}, Lcom/miui/launcher/utils/LauncherUtils;->isXSpaceUser(Landroid/os/UserHandle;)Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-direct {v6, v7, v5}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v6}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_2

    const/4 v5, 0x1

    goto :goto_2

    :cond_2
    move v5, v3

    :goto_2
    if-eqz v5, :cond_1

    invoke-interface {v2, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    check-cast v2, Ljava/util/List;

    goto :goto_3

    :cond_4
    const/4 v2, 0x0

    :goto_3
    check-cast v2, Ljava/lang/Iterable;

    new-instance v1, Lcom/miui/home/launcher/folder/AppPredictHelper$filterPredictAppSorted$$inlined$apply$lambda$1;

    invoke-direct {v1, v0, p1}, Lcom/miui/home/launcher/folder/AppPredictHelper$filterPredictAppSorted$$inlined$apply$lambda$1;-><init>(Landroid/util/ArrayMap;Ljava/util/List;)V

    check-cast v1, Ljava/util/Comparator;

    invoke-static {v2, v1}, Lkotlin/collections/CollectionsKt;->sortedWith(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    const-string p1, "AppPredictHelper"

    const-string v0, "error occur while run filterPredictAppSorted !"

    invoke-static {p1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method private final getAppPredictFromServiceInternal(Z)V
    .locals 2

    sget-object v0, Lcom/miui/home/launcher/folder/AppPredictHelper;->mAppPredictService:Lcom/miui/apppredict/IAppPredict;

    if-eqz v0, :cond_2

    if-nez v0, :cond_0

    :try_start_0
    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    const-string p1, "com.miui.home"

    invoke-interface {v0, p1}, Lcom/miui/apppredict/IAppPredict;->register(Ljava/lang/String;)V

    sget-object p1, Lcom/miui/home/launcher/folder/AppPredictHelper;->mAppPredictService:Lcom/miui/apppredict/IAppPredict;

    if-nez p1, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    const-string v0, "com.miui.home"

    new-instance v1, Lcom/miui/home/launcher/folder/AppPredictHelper$getAppPredictFromServiceInternal$1;

    invoke-direct {v1}, Lcom/miui/home/launcher/folder/AppPredictHelper$getAppPredictFromServiceInternal$1;-><init>()V

    check-cast v1, Lcom/miui/apppredict/IAppPredictCallBack;

    invoke-interface {p1, v0, v1}, Lcom/miui/apppredict/IAppPredict;->getAppPredictList(Ljava/lang/String;Lcom/miui/apppredict/IAppPredictCallBack;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    :cond_2
    if-eqz p1, :cond_3

    invoke-static {}, Lcom/miui/home/launcher/Application;->getInstance()Lcom/miui/home/launcher/Application;

    move-result-object p1

    const-string v0, "Application.getInstance()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/miui/home/launcher/folder/AppPredictHelper;->connectToService(Landroid/content/Context;)V

    :cond_3
    :goto_0
    return-void
.end method

.method private final updateAppPredict()V
    .locals 2

    sget-object v0, Lcom/miui/home/launcher/folder/AppPredictHelper;->mListenerList:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    check-cast v0, Ljava/lang/Iterable;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/home/launcher/folder/AppPredictUpdateListener;

    if-eqz v1, :cond_0

    invoke-interface {v1}, Lcom/miui/home/launcher/folder/AppPredictUpdateListener;->onAppPredictUpdate()V

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method public final addAppPredictListener(Lcom/miui/home/launcher/folder/AppPredictUpdateListener;)V
    .locals 1

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/miui/home/launcher/folder/AppPredictHelper;->containsOrNot(Lcom/miui/home/launcher/folder/AppPredictUpdateListener;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    sget-object p1, Lcom/miui/home/launcher/folder/AppPredictHelper;->mListenerList:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public final containsOrNot(Lcom/miui/home/launcher/folder/AppPredictUpdateListener;)Ljava/lang/ref/WeakReference;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/miui/home/launcher/folder/AppPredictUpdateListener;",
            ")",
            "Ljava/lang/ref/WeakReference<",
            "Lcom/miui/home/launcher/folder/AppPredictUpdateListener;",
            ">;"
        }
    .end annotation

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/miui/home/launcher/folder/AppPredictHelper;->mListenerList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/home/launcher/folder/AppPredictUpdateListener;

    invoke-static {v2, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    return-object v1

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method public final getAppPredictForFolder(Ljava/util/List;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/miui/home/launcher/ShortcutInfo;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/miui/home/launcher/ShortcutInfo;",
            ">;"
        }
    .end annotation

    const-string v0, "folderItemList"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {p0, p1}, Lcom/miui/home/launcher/folder/AppPredictHelper;->filterPredictAppSorted(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    invoke-direct {p0, p1, v0}, Lcom/miui/home/launcher/folder/AppPredictHelper;->addUnFindShortcutInfoToList(Ljava/util/List;Ljava/util/ArrayList;)V

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public final getAppPredictFromService(Z)V
    .locals 1

    new-instance v0, Lcom/miui/home/launcher/folder/AppPredictHelper$getAppPredictFromService$1;

    invoke-direct {v0, p1}, Lcom/miui/home/launcher/folder/AppPredictHelper$getAppPredictFromService$1;-><init>(Z)V

    check-cast v0, Ljava/lang/Runnable;

    invoke-static {v0}, Lcom/miui/home/launcher/common/BackgroundThread;->post(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final removeAppPredictListener(Lcom/miui/home/launcher/folder/AppPredictUpdateListener;)Z
    .locals 1

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/miui/home/launcher/folder/AppPredictHelper;->containsOrNot(Lcom/miui/home/launcher/folder/AppPredictUpdateListener;)Ljava/lang/ref/WeakReference;

    move-result-object p1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/miui/home/launcher/folder/AppPredictHelper;->mListenerList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    const/4 p1, 0x1

    :goto_0
    return p1
.end method
