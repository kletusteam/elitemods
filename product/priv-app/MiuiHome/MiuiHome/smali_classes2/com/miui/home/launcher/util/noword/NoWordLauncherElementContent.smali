.class public interface abstract Lcom/miui/home/launcher/util/noword/NoWordLauncherElementContent;
.super Ljava/lang/Object;


# virtual methods
.method public canDrawForegroundTitle()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public iconContainerId()I
    .locals 1

    const/4 v0, -0x1

    return v0
.end method

.method public iconView()Landroid/view/View;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public isNoWordModel()Z
    .locals 1

    invoke-static {}, Lcom/miui/home/launcher/common/Utilities;->isNoWordModel()Z

    move-result v0

    return v0
.end method

.method public titleViewContainerId()I
    .locals 1

    const v0, 0x7f0a01c0

    return v0
.end method

.method public titleViewId()I
    .locals 1

    const/4 v0, -0x1

    return v0
.end method

.method public viewChangeAlphaWhenEditModeChange()Landroid/view/View;
    .locals 1

    invoke-interface {p0}, Lcom/miui/home/launcher/util/noword/NoWordLauncherElementContent;->iconView()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
