.class Lcom/miui/home/launcher/search/SearchEdgeEffect$2;
.super Lcom/miui/home/launcher/search/SearchEdgeEffect$Position;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/home/launcher/search/SearchEdgeEffect;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/miui/home/launcher/search/SearchEdgeEffect$Position;-><init>()V

    return-void
.end method


# virtual methods
.method getCurveLimitAndOffset(FF)Landroid/util/Pair;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(FF)",
            "Landroid/util/Pair<",
            "Ljava/lang/Float;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    goto/32 :goto_2

    nop

    :goto_0
    new-instance v0, Landroid/util/Pair;

    goto/32 :goto_d

    nop

    :goto_1
    invoke-static {}, Lcom/miui/home/launcher/DeviceConfig;->isShowNavigationBar()Z

    move-result v1

    goto/32 :goto_c

    nop

    :goto_2
    invoke-static {}, Lcom/miui/home/launcher/Application;->getInstance()Lcom/miui/home/launcher/Application;

    move-result-object v0

    goto/32 :goto_f

    nop

    :goto_3
    add-float/2addr p1, p2

    goto/32 :goto_8

    nop

    :goto_4
    const/high16 p2, 0x40800000    # 4.0f

    goto/32 :goto_e

    nop

    :goto_5
    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p2

    goto/32 :goto_7

    nop

    :goto_6
    sub-float p2, v0, p2

    goto/32 :goto_3

    nop

    :goto_7
    invoke-direct {v0, p1, p2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto/32 :goto_a

    nop

    :goto_8
    move p2, v0

    :goto_9
    goto/32 :goto_0

    nop

    :goto_a
    return-object v0

    :goto_b
    int-to-float v0, v0

    goto/32 :goto_1

    nop

    :goto_c
    if-nez v1, :cond_0

    goto/32 :goto_9

    :cond_0
    goto/32 :goto_4

    nop

    :goto_d
    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p1

    goto/32 :goto_5

    nop

    :goto_e
    div-float p2, p1, p2

    goto/32 :goto_6

    nop

    :goto_f
    invoke-static {v0}, Lcom/miui/home/launcher/common/Utilities;->getNavigationBarHeight(Landroid/content/Context;)I

    move-result v0

    goto/32 :goto_b

    nop
.end method

.method public getDeltaDistance(F)F
    .locals 0

    neg-float p1, p1

    return p1
.end method

.method public getDrawRotate()F
    .locals 1

    const/high16 v0, 0x43340000    # 180.0f

    return v0
.end method

.method public getVelocity(I)I
    .locals 0

    neg-int p1, p1

    return p1
.end method
