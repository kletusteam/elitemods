.class public Lcom/miui/home/recents/util/SplitSelectStateController;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/home/recents/util/SplitSelectStateController$RemoteSplitLaunchAnimationRunner;
    }
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mHandler:Landroid/os/Handler;

.field private mInitialTaskId:I

.field private mInitialTaskIntent:Landroid/content/Intent;

.field private mInitialTaskPackageName:Ljava/lang/String;

.field private mInitialTaskUserId:I

.field private mRecentsAnimationRunning:Z

.field private mSecondIntent:Landroid/content/Intent;

.field private mSecondPackageName:Ljava/lang/String;

.field private mSecondTaskId:I

.field private mSecondTaskStagePosition:I

.field private mSecondUser:Landroid/os/UserHandle;

.field private mSecondUserId:I

.field private final mStateManager:Lcom/miui/home/launcher/LauncherStateManager;

.field private mType:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;Lcom/miui/home/launcher/LauncherStateManager;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/miui/home/recents/util/SplitSelectStateController;->mInitialTaskId:I

    iput v0, p0, Lcom/miui/home/recents/util/SplitSelectStateController;->mInitialTaskUserId:I

    iput v0, p0, Lcom/miui/home/recents/util/SplitSelectStateController;->mSecondTaskId:I

    iput v0, p0, Lcom/miui/home/recents/util/SplitSelectStateController;->mSecondUserId:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/miui/home/recents/util/SplitSelectStateController;->mType:I

    iput-object p1, p0, Lcom/miui/home/recents/util/SplitSelectStateController;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/miui/home/recents/util/SplitSelectStateController;->mHandler:Landroid/os/Handler;

    iput-object p3, p0, Lcom/miui/home/recents/util/SplitSelectStateController;->mStateManager:Lcom/miui/home/launcher/LauncherStateManager;

    return-void
.end method

.method static synthetic access$000(Lcom/miui/home/recents/util/SplitSelectStateController;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/miui/home/recents/util/SplitSelectStateController;->mHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static synthetic access$100(Lcom/miui/home/recents/util/SplitSelectStateController;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/miui/home/recents/util/SplitSelectStateController;->mRecentsAnimationRunning:Z

    return p0
.end method

.method static synthetic access$200(Lcom/miui/home/recents/util/SplitSelectStateController;)I
    .locals 0

    iget p0, p0, Lcom/miui/home/recents/util/SplitSelectStateController;->mType:I

    return p0
.end method

.method private isSameApp()Z
    .locals 2

    iget-object v0, p0, Lcom/miui/home/recents/util/SplitSelectStateController;->mInitialTaskPackageName:Ljava/lang/String;

    iget-object v1, p0, Lcom/miui/home/recents/util/SplitSelectStateController;->mSecondPackageName:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/miui/home/recents/util/SplitSelectStateController;->mInitialTaskUserId:I

    iget v1, p0, Lcom/miui/home/recents/util/SplitSelectStateController;->mSecondUserId:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method private isSecondTaskIntentSet()Z
    .locals 2

    iget v0, p0, Lcom/miui/home/recents/util/SplitSelectStateController;->mSecondTaskId:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/miui/home/recents/util/SplitSelectStateController;->mSecondIntent:Landroid/content/Intent;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method


# virtual methods
.method public getInitialTaskPackageName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/home/recents/util/SplitSelectStateController;->mInitialTaskPackageName:Ljava/lang/String;

    return-object v0
.end method

.method public getInitialTaskUserId()I
    .locals 1

    iget v0, p0, Lcom/miui/home/recents/util/SplitSelectStateController;->mInitialTaskUserId:I

    return v0
.end method

.method public isBothSplitAppsConfirmed()Z
    .locals 2

    iget v0, p0, Lcom/miui/home/recents/util/SplitSelectStateController;->mInitialTaskId:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    invoke-direct {p0}, Lcom/miui/home/recents/util/SplitSelectStateController;->isSecondTaskIntentSet()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public launchSplitTasks(Ljava/util/function/Consumer;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/function/Consumer<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    iget v0, p0, Lcom/miui/home/recents/util/SplitSelectStateController;->mType:I

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    invoke-direct {p0}, Lcom/miui/home/recents/util/SplitSelectStateController;->isSameApp()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/miui/home/recents/util/SplitSelectStateController;->resetSecondState()V

    return-void

    :cond_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    iget-object v2, p0, Lcom/miui/home/recents/util/SplitSelectStateController;->mContext:Landroid/content/Context;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/miui/home/recents/util/SplitSelectStateController;->mSecondIntent:Landroid/content/Intent;

    const/high16 v5, 0x2000000

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/miui/home/recents/util/SplitSelectStateController;->mSecondUser:Landroid/os/UserHandle;

    invoke-static/range {v2 .. v7}, Lcom/android/systemui/shared/recents/system/ActivityManagerWrapper;->getActivityAsUser(Landroid/content/Context;ILandroid/content/Intent;ILandroid/os/Bundle;Landroid/os/UserHandle;)Landroid/app/PendingIntent;

    move-result-object v0

    move-object v5, v0

    move-object v6, v1

    goto :goto_0

    :cond_1
    move-object v5, v1

    move-object v6, v5

    :goto_0
    const/4 v3, 0x1

    iget v4, p0, Lcom/miui/home/recents/util/SplitSelectStateController;->mInitialTaskId:I

    iget v7, p0, Lcom/miui/home/recents/util/SplitSelectStateController;->mSecondTaskId:I

    const/4 v9, 0x0

    const/high16 v10, 0x3f000000    # 0.5f

    move-object v2, p0

    move-object v8, p1

    invoke-virtual/range {v2 .. v10}, Lcom/miui/home/recents/util/SplitSelectStateController;->launchTasks(ZILandroid/app/PendingIntent;Landroid/content/Intent;ILjava/util/function/Consumer;ZF)V

    return-void
.end method

.method public launchTasks(ZIILjava/util/function/Consumer;ZF)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZII",
            "Ljava/util/function/Consumer<",
            "Ljava/lang/Boolean;",
            ">;ZF)V"
        }
    .end annotation

    const/4 v0, 0x0

    move-object v10, p0

    iput v0, v10, Lcom/miui/home/recents/util/SplitSelectStateController;->mType:I

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v6, p3

    move-object v7, p4

    move/from16 v8, p5

    move/from16 v9, p6

    invoke-virtual/range {v1 .. v9}, Lcom/miui/home/recents/util/SplitSelectStateController;->launchTasks(ZILandroid/app/PendingIntent;Landroid/content/Intent;ILjava/util/function/Consumer;ZF)V

    return-void
.end method

.method public launchTasks(ZILandroid/app/PendingIntent;Landroid/content/Intent;ILjava/util/function/Consumer;ZF)V
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZI",
            "Landroid/app/PendingIntent;",
            "Landroid/content/Intent;",
            "I",
            "Ljava/util/function/Consumer<",
            "Ljava/lang/Boolean;",
            ">;ZF)V"
        }
    .end annotation

    move-object/from16 v7, p0

    new-instance v8, Lcom/miui/home/recents/util/SplitSelectStateController$RemoteSplitLaunchAnimationRunner;

    move-object v0, v8

    move-object/from16 v1, p0

    move/from16 v2, p1

    move/from16 v3, p2

    move-object/from16 v4, p3

    move/from16 v5, p5

    move-object/from16 v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/miui/home/recents/util/SplitSelectStateController$RemoteSplitLaunchAnimationRunner;-><init>(Lcom/miui/home/recents/util/SplitSelectStateController;ZILandroid/app/PendingIntent;ILjava/util/function/Consumer;)V

    new-instance v17, Lcom/android/systemui/shared/recents/system/RemoteAnimationAdapterCompat;

    const-wide/16 v2, 0x12c

    const-wide/16 v4, 0x96

    move-object/from16 v0, v17

    move-object v1, v8

    invoke-direct/range {v0 .. v5}, Lcom/android/systemui/shared/recents/system/RemoteAnimationAdapterCompat;-><init>(Lcom/android/systemui/shared/recents/system/RemoteAnimationRunnerCompat;JJ)V

    invoke-static {}, Landroid/app/ActivityOptions;->makeBasic()Landroid/app/ActivityOptions;

    move-result-object v0

    if-eqz p7, :cond_0

    invoke-static {v0}, Lcom/android/systemui/shared/recents/system/ActivityOptionsCompat;->setFreezeRecentTasksReordering(Landroid/app/ActivityOptions;)V

    :cond_0
    sget-object v1, Lcom/miui/home/recents/SystemUiProxyWrapper;->INSTANCE:Lcom/miui/home/recents/util/MainThreadInitializedObject;

    invoke-virtual {v1}, Lcom/miui/home/recents/util/MainThreadInitializedObject;->getNoCreate()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/miui/home/recents/SystemUiProxyWrapper;

    if-eqz v9, :cond_2

    iget v1, v7, Lcom/miui/home/recents/util/SplitSelectStateController;->mType:I

    if-nez v1, :cond_1

    invoke-virtual {v0}, Landroid/app/ActivityOptions;->toBundle()Landroid/os/Bundle;

    move-result-object v11

    const/4 v13, 0x0

    iget v14, v7, Lcom/miui/home/recents/util/SplitSelectStateController;->mSecondTaskStagePosition:I

    move/from16 v10, p2

    move/from16 v12, p5

    move/from16 v15, p8

    move-object/from16 v16, v17

    invoke-virtual/range {v9 .. v16}, Lcom/miui/home/recents/SystemUiProxyWrapper;->startTasksWithLegacyTransition(ILandroid/os/Bundle;ILandroid/os/Bundle;IFLcom/android/systemui/shared/recents/system/RemoteAnimationAdapterCompat;)V

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Landroid/app/ActivityOptions;->toBundle()Landroid/os/Bundle;

    move-result-object v13

    const/4 v14, 0x0

    iget v15, v7, Lcom/miui/home/recents/util/SplitSelectStateController;->mSecondTaskStagePosition:I

    move-object/from16 v10, p3

    move-object/from16 v11, p4

    move/from16 v12, p2

    move/from16 v16, p8

    invoke-virtual/range {v9 .. v17}, Lcom/miui/home/recents/SystemUiProxyWrapper;->startIntentAndTaskWithLegacyTransition(Landroid/app/PendingIntent;Landroid/content/Intent;ILandroid/os/Bundle;Landroid/os/Bundle;IFLcom/android/systemui/shared/recents/system/RemoteAnimationAdapterCompat;)V

    :cond_2
    :goto_0
    return-void
.end method

.method public resetSecondState()V
    .locals 2

    const/4 v0, -0x1

    iput v0, p0, Lcom/miui/home/recents/util/SplitSelectStateController;->mSecondTaskId:I

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/miui/home/recents/util/SplitSelectStateController;->mSecondIntent:Landroid/content/Intent;

    iput-object v1, p0, Lcom/miui/home/recents/util/SplitSelectStateController;->mSecondPackageName:Ljava/lang/String;

    iput v0, p0, Lcom/miui/home/recents/util/SplitSelectStateController;->mSecondUserId:I

    iput-object v1, p0, Lcom/miui/home/recents/util/SplitSelectStateController;->mSecondUser:Landroid/os/UserHandle;

    return-void
.end method

.method public resetState()V
    .locals 2

    const/4 v0, -0x1

    iput v0, p0, Lcom/miui/home/recents/util/SplitSelectStateController;->mInitialTaskId:I

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/miui/home/recents/util/SplitSelectStateController;->mInitialTaskIntent:Landroid/content/Intent;

    iput-object v1, p0, Lcom/miui/home/recents/util/SplitSelectStateController;->mInitialTaskPackageName:Ljava/lang/String;

    iput v0, p0, Lcom/miui/home/recents/util/SplitSelectStateController;->mInitialTaskUserId:I

    iput v0, p0, Lcom/miui/home/recents/util/SplitSelectStateController;->mSecondTaskId:I

    iput-object v1, p0, Lcom/miui/home/recents/util/SplitSelectStateController;->mSecondIntent:Landroid/content/Intent;

    iput-object v1, p0, Lcom/miui/home/recents/util/SplitSelectStateController;->mSecondPackageName:Ljava/lang/String;

    iput v0, p0, Lcom/miui/home/recents/util/SplitSelectStateController;->mSecondUserId:I

    iput-object v1, p0, Lcom/miui/home/recents/util/SplitSelectStateController;->mSecondUser:Landroid/os/UserHandle;

    const/4 v1, 0x0

    iput v1, p0, Lcom/miui/home/recents/util/SplitSelectStateController;->mType:I

    iput v0, p0, Lcom/miui/home/recents/util/SplitSelectStateController;->mSecondTaskStagePosition:I

    iput-boolean v1, p0, Lcom/miui/home/recents/util/SplitSelectStateController;->mRecentsAnimationRunning:Z

    return-void
.end method

.method public setInitialTaskSelect(Lcom/android/systemui/shared/recents/model/Task;I)V
    .locals 1

    invoke-virtual {p0}, Lcom/miui/home/recents/util/SplitSelectStateController;->resetState()V

    iget-object v0, p1, Lcom/android/systemui/shared/recents/model/Task;->key:Lcom/android/systemui/shared/recents/model/Task$TaskKey;

    iget v0, v0, Lcom/android/systemui/shared/recents/model/Task$TaskKey;->id:I

    iput v0, p0, Lcom/miui/home/recents/util/SplitSelectStateController;->mInitialTaskId:I

    iget-object v0, p1, Lcom/android/systemui/shared/recents/model/Task;->key:Lcom/android/systemui/shared/recents/model/Task$TaskKey;

    iget-object v0, v0, Lcom/android/systemui/shared/recents/model/Task$TaskKey;->baseIntent:Landroid/content/Intent;

    iput-object v0, p0, Lcom/miui/home/recents/util/SplitSelectStateController;->mInitialTaskIntent:Landroid/content/Intent;

    iget-object v0, p1, Lcom/android/systemui/shared/recents/model/Task;->key:Lcom/android/systemui/shared/recents/model/Task$TaskKey;

    invoke-virtual {v0}, Lcom/android/systemui/shared/recents/model/Task$TaskKey;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/home/recents/util/SplitSelectStateController;->mInitialTaskPackageName:Ljava/lang/String;

    iget-object p1, p1, Lcom/android/systemui/shared/recents/model/Task;->key:Lcom/android/systemui/shared/recents/model/Task$TaskKey;

    iget p1, p1, Lcom/android/systemui/shared/recents/model/Task$TaskKey;->userId:I

    iput p1, p0, Lcom/miui/home/recents/util/SplitSelectStateController;->mInitialTaskUserId:I

    iput p2, p0, Lcom/miui/home/recents/util/SplitSelectStateController;->mSecondTaskStagePosition:I

    return-void
.end method

.method public setSecondIntent(Landroid/content/Intent;Landroid/os/UserHandle;)V
    .locals 1

    iput-object p1, p0, Lcom/miui/home/recents/util/SplitSelectStateController;->mSecondIntent:Landroid/content/Intent;

    invoke-static {p2}, Lcom/miui/launcher/utils/LauncherUtils;->getUserId(Landroid/os/UserHandle;)I

    move-result v0

    iput v0, p0, Lcom/miui/home/recents/util/SplitSelectStateController;->mSecondUserId:I

    iput-object p2, p0, Lcom/miui/home/recents/util/SplitSelectStateController;->mSecondUser:Landroid/os/UserHandle;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object p2

    if-eqz p2, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput-object p1, p0, Lcom/miui/home/recents/util/SplitSelectStateController;->mSecondPackageName:Ljava/lang/String;

    const/4 p1, 0x1

    iput p1, p0, Lcom/miui/home/recents/util/SplitSelectStateController;->mType:I

    return-void
.end method

.method public setSecondTask(Lcom/android/systemui/shared/recents/model/Task;)V
    .locals 0

    iget-object p1, p1, Lcom/android/systemui/shared/recents/model/Task;->key:Lcom/android/systemui/shared/recents/model/Task$TaskKey;

    iget p1, p1, Lcom/android/systemui/shared/recents/model/Task$TaskKey;->id:I

    iput p1, p0, Lcom/miui/home/recents/util/SplitSelectStateController;->mSecondTaskId:I

    const/4 p1, 0x0

    iput p1, p0, Lcom/miui/home/recents/util/SplitSelectStateController;->mType:I

    return-void
.end method
