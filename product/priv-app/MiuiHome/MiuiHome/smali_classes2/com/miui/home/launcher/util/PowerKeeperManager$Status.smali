.class public final enum Lcom/miui/home/launcher/util/PowerKeeperManager$Status;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/home/launcher/util/PowerKeeperManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Status"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/miui/home/launcher/util/PowerKeeperManager$Status;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/miui/home/launcher/util/PowerKeeperManager$Status;

.field public static final enum GESTURE_END:Lcom/miui/home/launcher/util/PowerKeeperManager$Status;

.field public static final enum GESTURE_START:Lcom/miui/home/launcher/util/PowerKeeperManager$Status;

.field public static final enum TO_APP:Lcom/miui/home/launcher/util/PowerKeeperManager$Status;

.field public static final enum TO_ASSISTANT:Lcom/miui/home/launcher/util/PowerKeeperManager$Status;

.field public static final enum TO_HOME:Lcom/miui/home/launcher/util/PowerKeeperManager$Status;

.field public static final enum TO_ONE_HAND:Lcom/miui/home/launcher/util/PowerKeeperManager$Status;

.field public static final enum TO_RECENT:Lcom/miui/home/launcher/util/PowerKeeperManager$Status;


# instance fields
.field private value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    new-instance v0, Lcom/miui/home/launcher/util/PowerKeeperManager$Status;

    const-string v1, "GESTURE_START"

    const-string v2, "gesture_start"

    const/4 v3, 0x0

    invoke-direct {v0, v1, v3, v2}, Lcom/miui/home/launcher/util/PowerKeeperManager$Status;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/miui/home/launcher/util/PowerKeeperManager$Status;->GESTURE_START:Lcom/miui/home/launcher/util/PowerKeeperManager$Status;

    new-instance v0, Lcom/miui/home/launcher/util/PowerKeeperManager$Status;

    const-string v1, "GESTURE_END"

    const-string v2, "gesture_end"

    const/4 v4, 0x1

    invoke-direct {v0, v1, v4, v2}, Lcom/miui/home/launcher/util/PowerKeeperManager$Status;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/miui/home/launcher/util/PowerKeeperManager$Status;->GESTURE_END:Lcom/miui/home/launcher/util/PowerKeeperManager$Status;

    new-instance v0, Lcom/miui/home/launcher/util/PowerKeeperManager$Status;

    const-string v1, "TO_HOME"

    const-string v2, "to_home"

    const/4 v5, 0x2

    invoke-direct {v0, v1, v5, v2}, Lcom/miui/home/launcher/util/PowerKeeperManager$Status;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/miui/home/launcher/util/PowerKeeperManager$Status;->TO_HOME:Lcom/miui/home/launcher/util/PowerKeeperManager$Status;

    new-instance v0, Lcom/miui/home/launcher/util/PowerKeeperManager$Status;

    const-string v1, "TO_RECENT"

    const-string v2, "to_recent"

    const/4 v6, 0x3

    invoke-direct {v0, v1, v6, v2}, Lcom/miui/home/launcher/util/PowerKeeperManager$Status;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/miui/home/launcher/util/PowerKeeperManager$Status;->TO_RECENT:Lcom/miui/home/launcher/util/PowerKeeperManager$Status;

    new-instance v0, Lcom/miui/home/launcher/util/PowerKeeperManager$Status;

    const-string v1, "TO_APP"

    const-string v2, "to_app"

    const/4 v7, 0x4

    invoke-direct {v0, v1, v7, v2}, Lcom/miui/home/launcher/util/PowerKeeperManager$Status;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/miui/home/launcher/util/PowerKeeperManager$Status;->TO_APP:Lcom/miui/home/launcher/util/PowerKeeperManager$Status;

    new-instance v0, Lcom/miui/home/launcher/util/PowerKeeperManager$Status;

    const-string v1, "TO_ASSISTANT"

    const-string v2, "to_assistant"

    const/4 v8, 0x5

    invoke-direct {v0, v1, v8, v2}, Lcom/miui/home/launcher/util/PowerKeeperManager$Status;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/miui/home/launcher/util/PowerKeeperManager$Status;->TO_ASSISTANT:Lcom/miui/home/launcher/util/PowerKeeperManager$Status;

    new-instance v0, Lcom/miui/home/launcher/util/PowerKeeperManager$Status;

    const-string v1, "TO_ONE_HAND"

    const-string v2, "to_one_hand"

    const/4 v9, 0x6

    invoke-direct {v0, v1, v9, v2}, Lcom/miui/home/launcher/util/PowerKeeperManager$Status;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/miui/home/launcher/util/PowerKeeperManager$Status;->TO_ONE_HAND:Lcom/miui/home/launcher/util/PowerKeeperManager$Status;

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/miui/home/launcher/util/PowerKeeperManager$Status;

    sget-object v1, Lcom/miui/home/launcher/util/PowerKeeperManager$Status;->GESTURE_START:Lcom/miui/home/launcher/util/PowerKeeperManager$Status;

    aput-object v1, v0, v3

    sget-object v1, Lcom/miui/home/launcher/util/PowerKeeperManager$Status;->GESTURE_END:Lcom/miui/home/launcher/util/PowerKeeperManager$Status;

    aput-object v1, v0, v4

    sget-object v1, Lcom/miui/home/launcher/util/PowerKeeperManager$Status;->TO_HOME:Lcom/miui/home/launcher/util/PowerKeeperManager$Status;

    aput-object v1, v0, v5

    sget-object v1, Lcom/miui/home/launcher/util/PowerKeeperManager$Status;->TO_RECENT:Lcom/miui/home/launcher/util/PowerKeeperManager$Status;

    aput-object v1, v0, v6

    sget-object v1, Lcom/miui/home/launcher/util/PowerKeeperManager$Status;->TO_APP:Lcom/miui/home/launcher/util/PowerKeeperManager$Status;

    aput-object v1, v0, v7

    sget-object v1, Lcom/miui/home/launcher/util/PowerKeeperManager$Status;->TO_ASSISTANT:Lcom/miui/home/launcher/util/PowerKeeperManager$Status;

    aput-object v1, v0, v8

    sget-object v1, Lcom/miui/home/launcher/util/PowerKeeperManager$Status;->TO_ONE_HAND:Lcom/miui/home/launcher/util/PowerKeeperManager$Status;

    aput-object v1, v0, v9

    sput-object v0, Lcom/miui/home/launcher/util/PowerKeeperManager$Status;->$VALUES:[Lcom/miui/home/launcher/util/PowerKeeperManager$Status;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/miui/home/launcher/util/PowerKeeperManager$Status;->value:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/miui/home/launcher/util/PowerKeeperManager$Status;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/miui/home/launcher/util/PowerKeeperManager$Status;->value:Ljava/lang/String;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/miui/home/launcher/util/PowerKeeperManager$Status;
    .locals 1

    const-class v0, Lcom/miui/home/launcher/util/PowerKeeperManager$Status;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/miui/home/launcher/util/PowerKeeperManager$Status;

    return-object p0
.end method

.method public static values()[Lcom/miui/home/launcher/util/PowerKeeperManager$Status;
    .locals 1

    sget-object v0, Lcom/miui/home/launcher/util/PowerKeeperManager$Status;->$VALUES:[Lcom/miui/home/launcher/util/PowerKeeperManager$Status;

    invoke-virtual {v0}, [Lcom/miui/home/launcher/util/PowerKeeperManager$Status;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/miui/home/launcher/util/PowerKeeperManager$Status;

    return-object v0
.end method
