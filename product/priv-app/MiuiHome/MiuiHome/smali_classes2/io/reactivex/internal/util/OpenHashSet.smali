.class public final Lio/reactivex/internal/util/OpenHashSet;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field keys:[Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[TT;"
        }
    .end annotation
.end field

.field final loadFactor:F

.field mask:I

.field maxSize:I

.field size:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/16 v0, 0x10

    const/high16 v1, 0x3f400000    # 0.75f

    invoke-direct {p0, v0, v1}, Lio/reactivex/internal/util/OpenHashSet;-><init>(IF)V

    return-void
.end method

.method public constructor <init>(IF)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Lio/reactivex/internal/util/OpenHashSet;->loadFactor:F

    invoke-static {p1}, Lio/reactivex/internal/util/Pow2;->roundToPowerOfTwo(I)I

    move-result p1

    add-int/lit8 v0, p1, -0x1

    iput v0, p0, Lio/reactivex/internal/util/OpenHashSet;->mask:I

    int-to-float v0, p1

    mul-float/2addr p2, v0

    float-to-int p2, p2

    iput p2, p0, Lio/reactivex/internal/util/OpenHashSet;->maxSize:I

    new-array p1, p1, [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Object;

    iput-object p1, p0, Lio/reactivex/internal/util/OpenHashSet;->keys:[Ljava/lang/Object;

    return-void
.end method

.method static mix(I)I
    .locals 1

    const v0, -0x61c88647

    mul-int/2addr p0, v0

    ushr-int/lit8 v0, p0, 0x10

    xor-int/2addr p0, v0

    return p0
.end method


# virtual methods
.method public add(Ljava/lang/Object;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    iget-object v0, p0, Lio/reactivex/internal/util/OpenHashSet;->keys:[Ljava/lang/Object;

    iget v1, p0, Lio/reactivex/internal/util/OpenHashSet;->mask:I

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-static {v2}, Lio/reactivex/internal/util/OpenHashSet;->mix(I)I

    move-result v2

    and-int/2addr v2, v1

    aget-object v3, v0, v2

    const/4 v4, 0x1

    if-eqz v3, :cond_2

    invoke-virtual {v3, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    const/4 v5, 0x0

    if-eqz v3, :cond_0

    return v5

    :cond_0
    add-int/2addr v2, v4

    and-int/2addr v2, v1

    aget-object v3, v0, v2

    if-nez v3, :cond_1

    goto :goto_0

    :cond_1
    invoke-virtual {v3, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    return v5

    :cond_2
    :goto_0
    aput-object p1, v0, v2

    iget p1, p0, Lio/reactivex/internal/util/OpenHashSet;->size:I

    add-int/2addr p1, v4

    iput p1, p0, Lio/reactivex/internal/util/OpenHashSet;->size:I

    iget v0, p0, Lio/reactivex/internal/util/OpenHashSet;->maxSize:I

    if-lt p1, v0, :cond_3

    invoke-virtual {p0}, Lio/reactivex/internal/util/OpenHashSet;->rehash()V

    :cond_3
    return v4
.end method

.method public keys()[Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lio/reactivex/internal/util/OpenHashSet;->keys:[Ljava/lang/Object;

    return-object v0
.end method

.method rehash()V
    .locals 8

    goto/32 :goto_25

    nop

    :goto_0
    and-int/2addr v5, v3

    goto/32 :goto_26

    nop

    :goto_1
    aget-object v7, v4, v5

    goto/32 :goto_2

    nop

    :goto_2
    if-nez v7, :cond_0

    goto/32 :goto_1b

    :cond_0
    :goto_3
    goto/32 :goto_23

    nop

    :goto_4
    iput v0, p0, Lio/reactivex/internal/util/OpenHashSet;->maxSize:I

    goto/32 :goto_1e

    nop

    :goto_5
    aget-object v7, v0, v1

    goto/32 :goto_7

    nop

    :goto_6
    invoke-static {v5}, Lio/reactivex/internal/util/OpenHashSet;->mix(I)I

    move-result v5

    goto/32 :goto_16

    nop

    :goto_7
    aput-object v7, v4, v5

    goto/32 :goto_20

    nop

    :goto_8
    goto :goto_1d

    :goto_9
    goto/32 :goto_24

    nop

    :goto_a
    iget v5, p0, Lio/reactivex/internal/util/OpenHashSet;->size:I

    :goto_b
    goto/32 :goto_10

    nop

    :goto_c
    check-cast v4, [Ljava/lang/Object;

    goto/32 :goto_a

    nop

    :goto_d
    shl-int/lit8 v2, v1, 0x1

    goto/32 :goto_18

    nop

    :goto_e
    goto :goto_b

    :goto_f
    goto/32 :goto_21

    nop

    :goto_10
    add-int/lit8 v6, v5, -0x1

    goto/32 :goto_1c

    nop

    :goto_11
    int-to-float v0, v2

    goto/32 :goto_28

    nop

    :goto_12
    add-int/lit8 v1, v1, -0x1

    goto/32 :goto_14

    nop

    :goto_13
    array-length v1, v0

    goto/32 :goto_d

    nop

    :goto_14
    aget-object v5, v0, v1

    goto/32 :goto_17

    nop

    :goto_15
    mul-float/2addr v0, v1

    goto/32 :goto_22

    nop

    :goto_16
    and-int/2addr v5, v3

    goto/32 :goto_1

    nop

    :goto_17
    if-eqz v5, :cond_1

    goto/32 :goto_9

    :cond_1
    goto/32 :goto_8

    nop

    :goto_18
    add-int/lit8 v3, v2, -0x1

    goto/32 :goto_1f

    nop

    :goto_19
    return-void

    :goto_1a
    if-eqz v7, :cond_2

    goto/32 :goto_3

    :cond_2
    :goto_1b
    goto/32 :goto_5

    nop

    :goto_1c
    if-nez v5, :cond_3

    goto/32 :goto_f

    :cond_3
    :goto_1d
    goto/32 :goto_12

    nop

    :goto_1e
    iput-object v4, p0, Lio/reactivex/internal/util/OpenHashSet;->keys:[Ljava/lang/Object;

    goto/32 :goto_19

    nop

    :goto_1f
    new-array v4, v2, [Ljava/lang/Object;

    goto/32 :goto_c

    nop

    :goto_20
    move v5, v6

    goto/32 :goto_e

    nop

    :goto_21
    iput v3, p0, Lio/reactivex/internal/util/OpenHashSet;->mask:I

    goto/32 :goto_11

    nop

    :goto_22
    float-to-int v0, v0

    goto/32 :goto_4

    nop

    :goto_23
    add-int/lit8 v5, v5, 0x1

    goto/32 :goto_0

    nop

    :goto_24
    aget-object v5, v0, v1

    goto/32 :goto_27

    nop

    :goto_25
    iget-object v0, p0, Lio/reactivex/internal/util/OpenHashSet;->keys:[Ljava/lang/Object;

    goto/32 :goto_13

    nop

    :goto_26
    aget-object v7, v4, v5

    goto/32 :goto_1a

    nop

    :goto_27
    invoke-virtual {v5}, Ljava/lang/Object;->hashCode()I

    move-result v5

    goto/32 :goto_6

    nop

    :goto_28
    iget v1, p0, Lio/reactivex/internal/util/OpenHashSet;->loadFactor:F

    goto/32 :goto_15

    nop
.end method

.method public remove(Ljava/lang/Object;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    iget-object v0, p0, Lio/reactivex/internal/util/OpenHashSet;->keys:[Ljava/lang/Object;

    iget v1, p0, Lio/reactivex/internal/util/OpenHashSet;->mask:I

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-static {v2}, Lio/reactivex/internal/util/OpenHashSet;->mix(I)I

    move-result v2

    and-int/2addr v2, v1

    aget-object v3, v0, v2

    const/4 v4, 0x0

    if-nez v3, :cond_0

    return v4

    :cond_0
    invoke-virtual {v3, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p0, v2, v0, v1}, Lio/reactivex/internal/util/OpenHashSet;->removeEntry(I[Ljava/lang/Object;I)Z

    move-result p1

    return p1

    :cond_1
    add-int/lit8 v2, v2, 0x1

    and-int/2addr v2, v1

    aget-object v3, v0, v2

    if-nez v3, :cond_2

    return v4

    :cond_2
    invoke-virtual {v3, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p0, v2, v0, v1}, Lio/reactivex/internal/util/OpenHashSet;->removeEntry(I[Ljava/lang/Object;I)Z

    move-result p1

    return p1
.end method

.method removeEntry(I[Ljava/lang/Object;I)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I[TT;I)Z"
        }
    .end annotation

    goto/32 :goto_e

    nop

    :goto_0
    const/4 p3, 0x0

    goto/32 :goto_6

    nop

    :goto_1
    if-le p1, v0, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_7

    nop

    :goto_2
    goto :goto_9

    :goto_3
    goto/32 :goto_1e

    nop

    :goto_4
    and-int/2addr v0, p3

    goto/32 :goto_14

    nop

    :goto_5
    if-eqz v2, :cond_1

    goto/32 :goto_1c

    :cond_1
    goto/32 :goto_0

    nop

    :goto_6
    aput-object p3, p2, p1

    goto/32 :goto_1b

    nop

    :goto_7
    if-lt p1, v3, :cond_2

    goto/32 :goto_9

    :cond_2
    goto/32 :goto_c

    nop

    :goto_8
    if-gt v3, v0, :cond_3

    goto/32 :goto_13

    :cond_3
    :goto_9
    goto/32 :goto_f

    nop

    :goto_a
    iput v0, p0, Lio/reactivex/internal/util/OpenHashSet;->size:I

    :goto_b
    goto/32 :goto_1a

    nop

    :goto_c
    if-gt v3, v0, :cond_4

    goto/32 :goto_13

    :cond_4
    goto/32 :goto_2

    nop

    :goto_d
    aget-object v2, p2, v0

    goto/32 :goto_5

    nop

    :goto_e
    iget v0, p0, Lio/reactivex/internal/util/OpenHashSet;->size:I

    goto/32 :goto_11

    nop

    :goto_f
    aput-object v2, p2, p1

    goto/32 :goto_10

    nop

    :goto_10
    move p1, v0

    goto/32 :goto_12

    nop

    :goto_11
    const/4 v1, 0x1

    goto/32 :goto_18

    nop

    :goto_12
    goto :goto_b

    :goto_13
    goto/32 :goto_1f

    nop

    :goto_14
    goto :goto_16

    :goto_15
    and-int/2addr v0, p3

    :goto_16
    goto/32 :goto_d

    nop

    :goto_17
    invoke-static {v3}, Lio/reactivex/internal/util/OpenHashSet;->mix(I)I

    move-result v3

    goto/32 :goto_19

    nop

    :goto_18
    sub-int/2addr v0, v1

    goto/32 :goto_a

    nop

    :goto_19
    and-int/2addr v3, p3

    goto/32 :goto_1

    nop

    :goto_1a
    add-int/lit8 v0, p1, 0x1

    goto/32 :goto_15

    nop

    :goto_1b
    return v1

    :goto_1c
    goto/32 :goto_1d

    nop

    :goto_1d
    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v3

    goto/32 :goto_17

    nop

    :goto_1e
    if-ge p1, v3, :cond_5

    goto/32 :goto_13

    :cond_5
    goto/32 :goto_8

    nop

    :goto_1f
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_4

    nop
.end method
