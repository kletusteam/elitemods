.class public final Lio/branch/search/R$dimen;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/branch/search/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final branch_deepview_app_icon_corners:I = 0x7f070087

.field public static final branch_deepview_app_icon_size:I = 0x7f070088

.field public static final branch_deepview_app_name_text:I = 0x7f070089

.field public static final branch_deepview_button_corners:I = 0x7f07008a

.field public static final branch_deepview_button_description_text:I = 0x7f07008b

.field public static final branch_deepview_button_elevation:I = 0x7f07008c

.field public static final branch_deepview_button_margin_bottom:I = 0x7f07008d

.field public static final branch_deepview_button_margin_horizontal:I = 0x7f07008e

.field public static final branch_deepview_button_margin_top:I = 0x7f07008f

.field public static final branch_deepview_button_text:I = 0x7f070090

.field public static final branch_deepview_corners:I = 0x7f070091

.field public static final branch_deepview_description_text:I = 0x7f070092

.field public static final branch_deepview_image_corners:I = 0x7f070093

.field public static final branch_deepview_image_padding:I = 0x7f070094

.field public static final branch_deepview_loading_radius:I = 0x7f070095

.field public static final branch_deepview_loading_stroke:I = 0x7f070096

.field public static final branch_deepview_title_text:I = 0x7f070097

.field public static final compat_button_inset_horizontal_material:I = 0x7f0700a2

.field public static final compat_button_inset_vertical_material:I = 0x7f0700a3

.field public static final compat_button_padding_horizontal_material:I = 0x7f0700a4

.field public static final compat_button_padding_vertical_material:I = 0x7f0700a5

.field public static final compat_control_corner_material:I = 0x7f0700a6

.field public static final compat_notification_large_icon_max_height:I = 0x7f0700a7

.field public static final compat_notification_large_icon_max_width:I = 0x7f0700a8

.field public static final notification_action_icon_size:I = 0x7f070593

.field public static final notification_action_text_size:I = 0x7f070594

.field public static final notification_big_circle_margin:I = 0x7f070595

.field public static final notification_content_margin_start:I = 0x7f070596

.field public static final notification_large_icon_height:I = 0x7f070597

.field public static final notification_large_icon_width:I = 0x7f070598

.field public static final notification_main_column_padding_top:I = 0x7f070599

.field public static final notification_media_narrow_margin:I = 0x7f07059a

.field public static final notification_right_icon_size:I = 0x7f07059b

.field public static final notification_right_side_padding_top:I = 0x7f07059c

.field public static final notification_small_icon_background_padding:I = 0x7f07059d

.field public static final notification_small_icon_size_as_large:I = 0x7f07059e

.field public static final notification_subtext_size:I = 0x7f07059f

.field public static final notification_top_pad:I = 0x7f0705a0

.field public static final notification_top_pad_large_text:I = 0x7f0705a1
