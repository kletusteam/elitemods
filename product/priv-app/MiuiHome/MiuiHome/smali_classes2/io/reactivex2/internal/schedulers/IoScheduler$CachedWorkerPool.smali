.class final Lio/reactivex2/internal/schedulers/IoScheduler$CachedWorkerPool;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/reactivex2/internal/schedulers/IoScheduler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "CachedWorkerPool"
.end annotation


# instance fields
.field final allWorkers:Lio/reactivex2/disposables/CompositeDisposable;

.field private final evictorService:Ljava/util/concurrent/ScheduledExecutorService;

.field private final evictorTask:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future<",
            "*>;"
        }
    .end annotation
.end field

.field private final expiringWorkerQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue<",
            "Lio/reactivex2/internal/schedulers/IoScheduler$ThreadWorker;",
            ">;"
        }
    .end annotation
.end field

.field private final keepAliveTime:J

.field private final threadFactory:Ljava/util/concurrent/ThreadFactory;


# direct methods
.method constructor <init>(JLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/ThreadFactory;)V
    .locals 7

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p3, :cond_0

    invoke-virtual {p3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide p1

    goto :goto_0

    :cond_0
    const-wide/16 p1, 0x0

    :goto_0
    iput-wide p1, p0, Lio/reactivex2/internal/schedulers/IoScheduler$CachedWorkerPool;->keepAliveTime:J

    new-instance p1, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object p1, p0, Lio/reactivex2/internal/schedulers/IoScheduler$CachedWorkerPool;->expiringWorkerQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    new-instance p1, Lio/reactivex2/disposables/CompositeDisposable;

    invoke-direct {p1}, Lio/reactivex2/disposables/CompositeDisposable;-><init>()V

    iput-object p1, p0, Lio/reactivex2/internal/schedulers/IoScheduler$CachedWorkerPool;->allWorkers:Lio/reactivex2/disposables/CompositeDisposable;

    iput-object p4, p0, Lio/reactivex2/internal/schedulers/IoScheduler$CachedWorkerPool;->threadFactory:Ljava/util/concurrent/ThreadFactory;

    const/4 p1, 0x0

    if-eqz p3, :cond_1

    const/4 p1, 0x1

    sget-object p2, Lio/reactivex2/internal/schedulers/IoScheduler;->EVICTOR_THREAD_FACTORY:Lio/reactivex2/internal/schedulers/RxThreadFactory;

    invoke-static {p1, p2}, Ljava/util/concurrent/Executors;->newScheduledThreadPool(ILjava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object p1

    iget-wide v4, p0, Lio/reactivex2/internal/schedulers/IoScheduler$CachedWorkerPool;->keepAliveTime:J

    sget-object v6, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    move-object v0, p1

    move-object v1, p0

    move-wide v2, v4

    invoke-interface/range {v0 .. v6}, Ljava/util/concurrent/ScheduledExecutorService;->scheduleWithFixedDelay(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object p2

    goto :goto_1

    :cond_1
    move-object p2, p1

    :goto_1
    iput-object p1, p0, Lio/reactivex2/internal/schedulers/IoScheduler$CachedWorkerPool;->evictorService:Ljava/util/concurrent/ScheduledExecutorService;

    iput-object p2, p0, Lio/reactivex2/internal/schedulers/IoScheduler$CachedWorkerPool;->evictorTask:Ljava/util/concurrent/Future;

    return-void
.end method


# virtual methods
.method evictExpiredWorkers()V
    .locals 6

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Lio/reactivex2/internal/schedulers/IoScheduler$CachedWorkerPool;->expiringWorkerQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    goto/32 :goto_a

    nop

    :goto_1
    if-nez v3, :cond_0

    goto/32 :goto_8

    :cond_0
    goto/32 :goto_3

    nop

    :goto_2
    invoke-virtual {v4, v3}, Lio/reactivex2/disposables/CompositeDisposable;->remove(Lio/reactivex2/disposables/Disposable;)Z

    goto/32 :goto_7

    nop

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    goto/32 :goto_f

    nop

    :goto_4
    if-eqz v0, :cond_1

    goto/32 :goto_8

    :cond_1
    goto/32 :goto_10

    nop

    :goto_5
    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_6
    goto/32 :goto_12

    nop

    :goto_7
    goto :goto_6

    :goto_8
    goto/32 :goto_13

    nop

    :goto_9
    invoke-virtual {v4, v3}, Ljava/util/concurrent/ConcurrentLinkedQueue;->remove(Ljava/lang/Object;)Z

    move-result v4

    goto/32 :goto_e

    nop

    :goto_a
    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->isEmpty()Z

    move-result v0

    goto/32 :goto_4

    nop

    :goto_b
    iget-object v4, p0, Lio/reactivex2/internal/schedulers/IoScheduler$CachedWorkerPool;->allWorkers:Lio/reactivex2/disposables/CompositeDisposable;

    goto/32 :goto_2

    nop

    :goto_c
    invoke-virtual {v3}, Lio/reactivex2/internal/schedulers/IoScheduler$ThreadWorker;->getExpirationTime()J

    move-result-wide v4

    goto/32 :goto_14

    nop

    :goto_d
    if-lez v4, :cond_2

    goto/32 :goto_8

    :cond_2
    goto/32 :goto_15

    nop

    :goto_e
    if-nez v4, :cond_3

    goto/32 :goto_6

    :cond_3
    goto/32 :goto_b

    nop

    :goto_f
    check-cast v3, Lio/reactivex2/internal/schedulers/IoScheduler$ThreadWorker;

    goto/32 :goto_c

    nop

    :goto_10
    invoke-virtual {p0}, Lio/reactivex2/internal/schedulers/IoScheduler$CachedWorkerPool;->now()J

    move-result-wide v0

    goto/32 :goto_11

    nop

    :goto_11
    iget-object v2, p0, Lio/reactivex2/internal/schedulers/IoScheduler$CachedWorkerPool;->expiringWorkerQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    goto/32 :goto_5

    nop

    :goto_12
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    goto/32 :goto_1

    nop

    :goto_13
    return-void

    :goto_14
    cmp-long v4, v4, v0

    goto/32 :goto_d

    nop

    :goto_15
    iget-object v4, p0, Lio/reactivex2/internal/schedulers/IoScheduler$CachedWorkerPool;->expiringWorkerQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    goto/32 :goto_9

    nop
.end method

.method get()Lio/reactivex2/internal/schedulers/IoScheduler$ThreadWorker;
    .locals 2

    goto/32 :goto_1

    nop

    :goto_0
    iget-object v1, p0, Lio/reactivex2/internal/schedulers/IoScheduler$CachedWorkerPool;->threadFactory:Ljava/util/concurrent/ThreadFactory;

    goto/32 :goto_d

    nop

    :goto_1
    iget-object v0, p0, Lio/reactivex2/internal/schedulers/IoScheduler$CachedWorkerPool;->allWorkers:Lio/reactivex2/disposables/CompositeDisposable;

    goto/32 :goto_c

    nop

    :goto_2
    return-object v0

    :goto_3
    goto/32 :goto_e

    nop

    :goto_4
    invoke-virtual {v1, v0}, Lio/reactivex2/disposables/CompositeDisposable;->add(Lio/reactivex2/disposables/Disposable;)Z

    goto/32 :goto_13

    nop

    :goto_5
    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_12

    nop

    :goto_6
    return-object v0

    :goto_7
    goto/32 :goto_10

    nop

    :goto_8
    sget-object v0, Lio/reactivex2/internal/schedulers/IoScheduler;->SHUTDOWN_THREAD_WORKER:Lio/reactivex2/internal/schedulers/IoScheduler$ThreadWorker;

    goto/32 :goto_6

    nop

    :goto_9
    iget-object v0, p0, Lio/reactivex2/internal/schedulers/IoScheduler$CachedWorkerPool;->expiringWorkerQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    goto/32 :goto_5

    nop

    :goto_a
    if-nez v0, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_8

    nop

    :goto_b
    iget-object v1, p0, Lio/reactivex2/internal/schedulers/IoScheduler$CachedWorkerPool;->allWorkers:Lio/reactivex2/disposables/CompositeDisposable;

    goto/32 :goto_4

    nop

    :goto_c
    invoke-virtual {v0}, Lio/reactivex2/disposables/CompositeDisposable;->isDisposed()Z

    move-result v0

    goto/32 :goto_a

    nop

    :goto_d
    invoke-direct {v0, v1}, Lio/reactivex2/internal/schedulers/IoScheduler$ThreadWorker;-><init>(Ljava/util/concurrent/ThreadFactory;)V

    goto/32 :goto_b

    nop

    :goto_e
    new-instance v0, Lio/reactivex2/internal/schedulers/IoScheduler$ThreadWorker;

    goto/32 :goto_0

    nop

    :goto_f
    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->isEmpty()Z

    move-result v0

    goto/32 :goto_11

    nop

    :goto_10
    iget-object v0, p0, Lio/reactivex2/internal/schedulers/IoScheduler$CachedWorkerPool;->expiringWorkerQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    goto/32 :goto_f

    nop

    :goto_11
    if-eqz v0, :cond_1

    goto/32 :goto_3

    :cond_1
    goto/32 :goto_9

    nop

    :goto_12
    check-cast v0, Lio/reactivex2/internal/schedulers/IoScheduler$ThreadWorker;

    goto/32 :goto_14

    nop

    :goto_13
    return-object v0

    :goto_14
    if-nez v0, :cond_2

    goto/32 :goto_7

    :cond_2
    goto/32 :goto_2

    nop
.end method

.method now()J
    .locals 2

    goto/32 :goto_1

    nop

    :goto_0
    return-wide v0

    :goto_1
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    goto/32 :goto_0

    nop
.end method

.method release(Lio/reactivex2/internal/schedulers/IoScheduler$ThreadWorker;)V
    .locals 4

    goto/32 :goto_2

    nop

    :goto_0
    return-void

    :goto_1
    invoke-virtual {p1, v0, v1}, Lio/reactivex2/internal/schedulers/IoScheduler$ThreadWorker;->setExpirationTime(J)V

    goto/32 :goto_3

    nop

    :goto_2
    invoke-virtual {p0}, Lio/reactivex2/internal/schedulers/IoScheduler$CachedWorkerPool;->now()J

    move-result-wide v0

    goto/32 :goto_6

    nop

    :goto_3
    iget-object v0, p0, Lio/reactivex2/internal/schedulers/IoScheduler$CachedWorkerPool;->expiringWorkerQueue:Ljava/util/concurrent/ConcurrentLinkedQueue;

    goto/32 :goto_5

    nop

    :goto_4
    add-long/2addr v0, v2

    goto/32 :goto_1

    nop

    :goto_5
    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->offer(Ljava/lang/Object;)Z

    goto/32 :goto_0

    nop

    :goto_6
    iget-wide v2, p0, Lio/reactivex2/internal/schedulers/IoScheduler$CachedWorkerPool;->keepAliveTime:J

    goto/32 :goto_4

    nop
.end method

.method public run()V
    .locals 0

    invoke-virtual {p0}, Lio/reactivex2/internal/schedulers/IoScheduler$CachedWorkerPool;->evictExpiredWorkers()V

    return-void
.end method

.method shutdown()V
    .locals 2

    goto/32 :goto_a

    nop

    :goto_0
    invoke-virtual {v0}, Lio/reactivex2/disposables/CompositeDisposable;->dispose()V

    goto/32 :goto_6

    nop

    :goto_1
    if-nez v0, :cond_0

    goto/32 :goto_9

    :cond_0
    goto/32 :goto_2

    nop

    :goto_2
    const/4 v1, 0x1

    goto/32 :goto_8

    nop

    :goto_3
    return-void

    :goto_4
    invoke-interface {v0}, Ljava/util/concurrent/ScheduledExecutorService;->shutdownNow()Ljava/util/List;

    :goto_5
    goto/32 :goto_3

    nop

    :goto_6
    iget-object v0, p0, Lio/reactivex2/internal/schedulers/IoScheduler$CachedWorkerPool;->evictorTask:Ljava/util/concurrent/Future;

    goto/32 :goto_1

    nop

    :goto_7
    if-nez v0, :cond_1

    goto/32 :goto_5

    :cond_1
    goto/32 :goto_4

    nop

    :goto_8
    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    :goto_9
    goto/32 :goto_b

    nop

    :goto_a
    iget-object v0, p0, Lio/reactivex2/internal/schedulers/IoScheduler$CachedWorkerPool;->allWorkers:Lio/reactivex2/disposables/CompositeDisposable;

    goto/32 :goto_0

    nop

    :goto_b
    iget-object v0, p0, Lio/reactivex2/internal/schedulers/IoScheduler$CachedWorkerPool;->evictorService:Ljava/util/concurrent/ScheduledExecutorService;

    goto/32 :goto_7

    nop
.end method
