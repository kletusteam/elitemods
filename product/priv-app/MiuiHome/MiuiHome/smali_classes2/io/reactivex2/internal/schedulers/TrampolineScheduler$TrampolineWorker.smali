.class final Lio/reactivex2/internal/schedulers/TrampolineScheduler$TrampolineWorker;
.super Lio/reactivex2/Scheduler$Worker;

# interfaces
.implements Lio/reactivex2/disposables/Disposable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/reactivex2/internal/schedulers/TrampolineScheduler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "TrampolineWorker"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/reactivex2/internal/schedulers/TrampolineScheduler$TrampolineWorker$AppendToQueueTask;
    }
.end annotation


# instance fields
.field final counter:Ljava/util/concurrent/atomic/AtomicInteger;

.field volatile disposed:Z

.field final queue:Ljava/util/concurrent/PriorityBlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/PriorityBlockingQueue<",
            "Lio/reactivex2/internal/schedulers/TrampolineScheduler$TimedRunnable;",
            ">;"
        }
    .end annotation
.end field

.field private final wip:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lio/reactivex2/Scheduler$Worker;-><init>()V

    new-instance v0, Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/PriorityBlockingQueue;-><init>()V

    iput-object v0, p0, Lio/reactivex2/internal/schedulers/TrampolineScheduler$TrampolineWorker;->queue:Ljava/util/concurrent/PriorityBlockingQueue;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lio/reactivex2/internal/schedulers/TrampolineScheduler$TrampolineWorker;->wip:Ljava/util/concurrent/atomic/AtomicInteger;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lio/reactivex2/internal/schedulers/TrampolineScheduler$TrampolineWorker;->counter:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method


# virtual methods
.method public dispose()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/reactivex2/internal/schedulers/TrampolineScheduler$TrampolineWorker;->disposed:Z

    return-void
.end method

.method enqueue(Ljava/lang/Runnable;J)Lio/reactivex2/disposables/Disposable;
    .locals 1

    goto/32 :goto_25

    nop

    :goto_0
    new-instance v0, Lio/reactivex2/internal/schedulers/TrampolineScheduler$TimedRunnable;

    goto/32 :goto_26

    nop

    :goto_1
    if-eqz p1, :cond_0

    goto/32 :goto_14

    :cond_0
    goto/32 :goto_20

    nop

    :goto_2
    iget-object p2, p0, Lio/reactivex2/internal/schedulers/TrampolineScheduler$TrampolineWorker;->wip:Ljava/util/concurrent/atomic/AtomicInteger;

    goto/32 :goto_22

    nop

    :goto_3
    goto :goto_14

    :goto_4
    goto/32 :goto_21

    nop

    :goto_5
    if-eqz p3, :cond_1

    goto/32 :goto_14

    :cond_1
    goto/32 :goto_17

    nop

    :goto_6
    return-object p1

    :goto_7
    goto/32 :goto_29

    nop

    :goto_8
    iget-object p1, p0, Lio/reactivex2/internal/schedulers/TrampolineScheduler$TrampolineWorker;->queue:Ljava/util/concurrent/PriorityBlockingQueue;

    goto/32 :goto_2a

    nop

    :goto_9
    iget-object p2, p0, Lio/reactivex2/internal/schedulers/TrampolineScheduler$TrampolineWorker;->queue:Ljava/util/concurrent/PriorityBlockingQueue;

    goto/32 :goto_c

    nop

    :goto_a
    invoke-direct {p1, p0, v0}, Lio/reactivex2/internal/schedulers/TrampolineScheduler$TrampolineWorker$AppendToQueueTask;-><init>(Lio/reactivex2/internal/schedulers/TrampolineScheduler$TrampolineWorker;Lio/reactivex2/internal/schedulers/TrampolineScheduler$TimedRunnable;)V

    goto/32 :goto_12

    nop

    :goto_b
    if-eqz p1, :cond_2

    goto/32 :goto_4

    :cond_2
    goto/32 :goto_13

    nop

    :goto_c
    invoke-virtual {p2}, Ljava/util/concurrent/PriorityBlockingQueue;->poll()Ljava/lang/Object;

    move-result-object p2

    goto/32 :goto_1b

    nop

    :goto_d
    iget-object p3, p0, Lio/reactivex2/internal/schedulers/TrampolineScheduler$TrampolineWorker;->counter:Ljava/util/concurrent/atomic/AtomicInteger;

    goto/32 :goto_f

    nop

    :goto_e
    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result p1

    goto/32 :goto_b

    nop

    :goto_f
    invoke-virtual {p3}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result p3

    goto/32 :goto_1d

    nop

    :goto_10
    if-nez v0, :cond_3

    goto/32 :goto_2c

    :cond_3
    goto/32 :goto_1f

    nop

    :goto_11
    invoke-interface {p2}, Ljava/lang/Runnable;->run()V

    goto/32 :goto_3

    nop

    :goto_12
    invoke-static {p1}, Lio/reactivex2/disposables/Disposables;->fromRunnable(Ljava/lang/Runnable;)Lio/reactivex2/disposables/Disposable;

    move-result-object p1

    goto/32 :goto_18

    nop

    :goto_13
    const/4 p1, 0x1

    :goto_14
    goto/32 :goto_1a

    nop

    :goto_15
    iget-object p1, p0, Lio/reactivex2/internal/schedulers/TrampolineScheduler$TrampolineWorker;->wip:Ljava/util/concurrent/atomic/AtomicInteger;

    goto/32 :goto_e

    nop

    :goto_16
    invoke-virtual {p2, p1}, Ljava/util/concurrent/atomic/AtomicInteger;->addAndGet(I)I

    move-result p1

    goto/32 :goto_1

    nop

    :goto_17
    iget-object p2, p2, Lio/reactivex2/internal/schedulers/TrampolineScheduler$TimedRunnable;->run:Ljava/lang/Runnable;

    goto/32 :goto_11

    nop

    :goto_18
    return-object p1

    :goto_19
    sget-object p1, Lio/reactivex2/internal/disposables/EmptyDisposable;->INSTANCE:Lio/reactivex2/internal/disposables/EmptyDisposable;

    goto/32 :goto_27

    nop

    :goto_1a
    iget-boolean p2, p0, Lio/reactivex2/internal/schedulers/TrampolineScheduler$TrampolineWorker;->disposed:Z

    goto/32 :goto_1c

    nop

    :goto_1b
    check-cast p2, Lio/reactivex2/internal/schedulers/TrampolineScheduler$TimedRunnable;

    goto/32 :goto_1e

    nop

    :goto_1c
    if-nez p2, :cond_4

    goto/32 :goto_28

    :cond_4
    goto/32 :goto_8

    nop

    :goto_1d
    invoke-direct {v0, p1, p2, p3}, Lio/reactivex2/internal/schedulers/TrampolineScheduler$TimedRunnable;-><init>(Ljava/lang/Runnable;Ljava/lang/Long;I)V

    goto/32 :goto_24

    nop

    :goto_1e
    if-eqz p2, :cond_5

    goto/32 :goto_7

    :cond_5
    goto/32 :goto_2

    nop

    :goto_1f
    sget-object p1, Lio/reactivex2/internal/disposables/EmptyDisposable;->INSTANCE:Lio/reactivex2/internal/disposables/EmptyDisposable;

    goto/32 :goto_2b

    nop

    :goto_20
    sget-object p1, Lio/reactivex2/internal/disposables/EmptyDisposable;->INSTANCE:Lio/reactivex2/internal/disposables/EmptyDisposable;

    goto/32 :goto_6

    nop

    :goto_21
    new-instance p1, Lio/reactivex2/internal/schedulers/TrampolineScheduler$TrampolineWorker$AppendToQueueTask;

    goto/32 :goto_a

    nop

    :goto_22
    neg-int p1, p1

    goto/32 :goto_16

    nop

    :goto_23
    invoke-virtual {p1, v0}, Ljava/util/concurrent/PriorityBlockingQueue;->add(Ljava/lang/Object;)Z

    goto/32 :goto_15

    nop

    :goto_24
    iget-object p1, p0, Lio/reactivex2/internal/schedulers/TrampolineScheduler$TrampolineWorker;->queue:Ljava/util/concurrent/PriorityBlockingQueue;

    goto/32 :goto_23

    nop

    :goto_25
    iget-boolean v0, p0, Lio/reactivex2/internal/schedulers/TrampolineScheduler$TrampolineWorker;->disposed:Z

    goto/32 :goto_10

    nop

    :goto_26
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    goto/32 :goto_d

    nop

    :goto_27
    return-object p1

    :goto_28
    goto/32 :goto_9

    nop

    :goto_29
    iget-boolean p3, p2, Lio/reactivex2/internal/schedulers/TrampolineScheduler$TimedRunnable;->disposed:Z

    goto/32 :goto_5

    nop

    :goto_2a
    invoke-virtual {p1}, Ljava/util/concurrent/PriorityBlockingQueue;->clear()V

    goto/32 :goto_19

    nop

    :goto_2b
    return-object p1

    :goto_2c
    goto/32 :goto_0

    nop
.end method

.method public isDisposed()Z
    .locals 1

    iget-boolean v0, p0, Lio/reactivex2/internal/schedulers/TrampolineScheduler$TrampolineWorker;->disposed:Z

    return v0
.end method

.method public schedule(Ljava/lang/Runnable;)Lio/reactivex2/disposables/Disposable;
    .locals 2

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p0, v0}, Lio/reactivex2/internal/schedulers/TrampolineScheduler$TrampolineWorker;->now(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    invoke-virtual {p0, p1, v0, v1}, Lio/reactivex2/internal/schedulers/TrampolineScheduler$TrampolineWorker;->enqueue(Ljava/lang/Runnable;J)Lio/reactivex2/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Lio/reactivex2/disposables/Disposable;
    .locals 2

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p0, v0}, Lio/reactivex2/internal/schedulers/TrampolineScheduler$TrampolineWorker;->now(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    invoke-virtual {p4, p2, p3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide p2

    add-long/2addr v0, p2

    new-instance p2, Lio/reactivex2/internal/schedulers/TrampolineScheduler$SleepingRunnable;

    invoke-direct {p2, p1, p0, v0, v1}, Lio/reactivex2/internal/schedulers/TrampolineScheduler$SleepingRunnable;-><init>(Ljava/lang/Runnable;Lio/reactivex2/internal/schedulers/TrampolineScheduler$TrampolineWorker;J)V

    invoke-virtual {p0, p2, v0, v1}, Lio/reactivex2/internal/schedulers/TrampolineScheduler$TrampolineWorker;->enqueue(Ljava/lang/Runnable;J)Lio/reactivex2/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method
