.class public final Lio/reactivex2/internal/disposables/ListCompositeDisposable;
.super Ljava/lang/Object;

# interfaces
.implements Lio/reactivex2/disposables/Disposable;
.implements Lio/reactivex2/internal/disposables/DisposableContainer;


# instance fields
.field volatile disposed:Z

.field resources:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lio/reactivex2/disposables/Disposable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public add(Lio/reactivex2/disposables/Disposable;)Z
    .locals 1

    const-string v0, "d is null"

    invoke-static {p1, v0}, Lio/reactivex2/internal/functions/ObjectHelper;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    iget-boolean v0, p0, Lio/reactivex2/internal/disposables/ListCompositeDisposable;->disposed:Z

    if-nez v0, :cond_2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lio/reactivex2/internal/disposables/ListCompositeDisposable;->disposed:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lio/reactivex2/internal/disposables/ListCompositeDisposable;->resources:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lio/reactivex2/internal/disposables/ListCompositeDisposable;->resources:Ljava/util/List;

    :cond_0
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 p1, 0x1

    monitor-exit p0

    return p1

    :cond_1
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    :cond_2
    :goto_0
    invoke-interface {p1}, Lio/reactivex2/disposables/Disposable;->dispose()V

    const/4 p1, 0x0

    return p1
.end method

.method public delete(Lio/reactivex2/disposables/Disposable;)Z
    .locals 2

    const-string v0, "Disposable item is null"

    invoke-static {p1, v0}, Lio/reactivex2/internal/functions/ObjectHelper;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    iget-boolean v0, p0, Lio/reactivex2/internal/disposables/ListCompositeDisposable;->disposed:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    :cond_0
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lio/reactivex2/internal/disposables/ListCompositeDisposable;->disposed:Z

    if-eqz v0, :cond_1

    monitor-exit p0

    return v1

    :cond_1
    iget-object v0, p0, Lio/reactivex2/internal/disposables/ListCompositeDisposable;->resources:Ljava/util/List;

    if-eqz v0, :cond_3

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_2

    goto :goto_0

    :cond_2
    monitor-exit p0

    const/4 p1, 0x1

    return p1

    :cond_3
    :goto_0
    monitor-exit p0

    return v1

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public dispose()V
    .locals 2

    iget-boolean v0, p0, Lio/reactivex2/internal/disposables/ListCompositeDisposable;->disposed:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lio/reactivex2/internal/disposables/ListCompositeDisposable;->disposed:Z

    if-eqz v0, :cond_1

    monitor-exit p0

    return-void

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/reactivex2/internal/disposables/ListCompositeDisposable;->disposed:Z

    iget-object v0, p0, Lio/reactivex2/internal/disposables/ListCompositeDisposable;->resources:Ljava/util/List;

    const/4 v1, 0x0

    iput-object v1, p0, Lio/reactivex2/internal/disposables/ListCompositeDisposable;->resources:Ljava/util/List;

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0, v0}, Lio/reactivex2/internal/disposables/ListCompositeDisposable;->dispose(Ljava/util/List;)V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method dispose(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lio/reactivex2/disposables/Disposable;",
            ">;)V"
        }
    .end annotation

    goto/32 :goto_1c

    nop

    :goto_0
    goto :goto_f

    :catch_0
    move-exception v1

    goto/32 :goto_d

    nop

    :goto_1
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/32 :goto_1d

    nop

    :goto_2
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    goto/32 :goto_18

    nop

    :goto_3
    return-void

    :goto_4
    goto/32 :goto_1b

    nop

    :goto_5
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result p1

    goto/32 :goto_10

    nop

    :goto_6
    throw p1

    :goto_7
    goto/32 :goto_13

    nop

    :goto_8
    new-instance v0, Ljava/util/ArrayList;

    goto/32 :goto_16

    nop

    :goto_9
    invoke-static {p1}, Lio/reactivex2/internal/util/ExceptionHelper;->wrapOrThrow(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object p1

    goto/32 :goto_6

    nop

    :goto_a
    if-eqz v0, :cond_0

    goto/32 :goto_17

    :cond_0
    goto/32 :goto_8

    nop

    :goto_b
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_c

    nop

    :goto_c
    if-nez v1, :cond_1

    goto/32 :goto_1e

    :cond_1
    goto/32 :goto_1a

    nop

    :goto_d
    invoke-static {v1}, Lio/reactivex2/exceptions/Exceptions;->throwIfFatal(Ljava/lang/Throwable;)V

    goto/32 :goto_a

    nop

    :goto_e
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_f
    goto/32 :goto_b

    nop

    :goto_10
    const/4 v1, 0x1

    goto/32 :goto_20

    nop

    :goto_11
    check-cast v1, Lio/reactivex2/disposables/Disposable;

    :try_start_0
    invoke-interface {v1}, Lio/reactivex2/disposables/Disposable;->dispose()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_0

    nop

    :goto_12
    invoke-direct {p1, v0}, Lio/reactivex2/exceptions/CompositeException;-><init>(Ljava/lang/Iterable;)V

    goto/32 :goto_14

    nop

    :goto_13
    new-instance p1, Lio/reactivex2/exceptions/CompositeException;

    goto/32 :goto_12

    nop

    :goto_14
    throw p1

    :goto_15
    goto/32 :goto_19

    nop

    :goto_16
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :goto_17
    goto/32 :goto_1

    nop

    :goto_18
    check-cast p1, Ljava/lang/Throwable;

    goto/32 :goto_9

    nop

    :goto_19
    return-void

    :goto_1a
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_11

    nop

    :goto_1b
    const/4 v0, 0x0

    goto/32 :goto_e

    nop

    :goto_1c
    if-eqz p1, :cond_2

    goto/32 :goto_4

    :cond_2
    goto/32 :goto_3

    nop

    :goto_1d
    goto :goto_f

    :goto_1e
    goto/32 :goto_1f

    nop

    :goto_1f
    if-nez v0, :cond_3

    goto/32 :goto_15

    :cond_3
    goto/32 :goto_5

    nop

    :goto_20
    if-eq p1, v1, :cond_4

    goto/32 :goto_7

    :cond_4
    goto/32 :goto_21

    nop

    :goto_21
    const/4 p1, 0x0

    goto/32 :goto_2

    nop
.end method

.method public isDisposed()Z
    .locals 1

    iget-boolean v0, p0, Lio/reactivex2/internal/disposables/ListCompositeDisposable;->disposed:Z

    return v0
.end method

.method public remove(Lio/reactivex2/disposables/Disposable;)Z
    .locals 1

    invoke-virtual {p0, p1}, Lio/reactivex2/internal/disposables/ListCompositeDisposable;->delete(Lio/reactivex2/disposables/Disposable;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lio/reactivex2/disposables/Disposable;->dispose()V

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method
