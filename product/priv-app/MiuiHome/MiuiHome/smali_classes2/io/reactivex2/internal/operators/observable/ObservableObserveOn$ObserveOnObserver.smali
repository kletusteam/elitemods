.class final Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;
.super Lio/reactivex2/internal/observers/BasicIntQueueDisposable;

# interfaces
.implements Lio/reactivex2/Observer;
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/reactivex2/internal/operators/observable/ObservableObserveOn;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "ObserveOnObserver"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lio/reactivex2/internal/observers/BasicIntQueueDisposable<",
        "TT;>;",
        "Lio/reactivex2/Observer<",
        "TT;>;",
        "Ljava/lang/Runnable;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x5b45d4a143741ca0L


# instance fields
.field final bufferSize:I

.field final delayError:Z

.field volatile disposed:Z

.field volatile done:Z

.field final downstream:Lio/reactivex2/Observer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex2/Observer<",
            "-TT;>;"
        }
    .end annotation
.end field

.field error:Ljava/lang/Throwable;

.field outputFused:Z

.field queue:Lio/reactivex2/internal/fuseable/SimpleQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex2/internal/fuseable/SimpleQueue<",
            "TT;>;"
        }
    .end annotation
.end field

.field sourceMode:I

.field upstream:Lio/reactivex2/disposables/Disposable;

.field final worker:Lio/reactivex2/Scheduler$Worker;


# direct methods
.method constructor <init>(Lio/reactivex2/Observer;Lio/reactivex2/Scheduler$Worker;ZI)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex2/Observer<",
            "-TT;>;",
            "Lio/reactivex2/Scheduler$Worker;",
            "ZI)V"
        }
    .end annotation

    invoke-direct {p0}, Lio/reactivex2/internal/observers/BasicIntQueueDisposable;-><init>()V

    iput-object p1, p0, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->downstream:Lio/reactivex2/Observer;

    iput-object p2, p0, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->worker:Lio/reactivex2/Scheduler$Worker;

    iput-boolean p3, p0, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->delayError:Z

    iput p4, p0, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->bufferSize:I

    return-void
.end method


# virtual methods
.method checkTerminated(ZZLio/reactivex2/Observer;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ",
            "Lio/reactivex2/Observer<",
            "-TT;>;)Z"
        }
    .end annotation

    goto/32 :goto_23

    nop

    :goto_0
    goto/16 :goto_1e

    :goto_1
    goto/32 :goto_1d

    nop

    :goto_2
    return v1

    :goto_3
    goto/32 :goto_21

    nop

    :goto_4
    invoke-virtual {p1}, Lio/reactivex2/Scheduler$Worker;->dispose()V

    goto/32 :goto_c

    nop

    :goto_5
    invoke-interface {p1}, Lio/reactivex2/internal/fuseable/SimpleQueue;->clear()V

    goto/32 :goto_18

    nop

    :goto_6
    iget-object p2, p0, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->queue:Lio/reactivex2/internal/fuseable/SimpleQueue;

    goto/32 :goto_20

    nop

    :goto_7
    iput-boolean v1, p0, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->disposed:Z

    goto/32 :goto_6

    nop

    :goto_8
    iget-object p1, p0, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->worker:Lio/reactivex2/Scheduler$Worker;

    goto/32 :goto_4

    nop

    :goto_9
    iget-object p1, p0, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->queue:Lio/reactivex2/internal/fuseable/SimpleQueue;

    goto/32 :goto_5

    nop

    :goto_a
    invoke-interface {p3, p1}, Lio/reactivex2/Observer;->onError(Ljava/lang/Throwable;)V

    goto/32 :goto_0

    nop

    :goto_b
    if-nez p1, :cond_0

    goto/32 :goto_10

    :cond_0
    goto/32 :goto_24

    nop

    :goto_c
    return v1

    :goto_d
    goto/32 :goto_17

    nop

    :goto_e
    if-nez p2, :cond_1

    goto/32 :goto_10

    :cond_1
    goto/32 :goto_27

    nop

    :goto_f
    return v1

    :goto_10
    goto/32 :goto_12

    nop

    :goto_11
    if-nez v0, :cond_2

    goto/32 :goto_19

    :cond_2
    goto/32 :goto_9

    nop

    :goto_12
    const/4 p1, 0x0

    goto/32 :goto_14

    nop

    :goto_13
    iget-object p1, p0, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->worker:Lio/reactivex2/Scheduler$Worker;

    goto/32 :goto_1b

    nop

    :goto_14
    return p1

    :goto_15
    invoke-interface {p3}, Lio/reactivex2/Observer;->onComplete()V

    goto/32 :goto_13

    nop

    :goto_16
    invoke-virtual {p1}, Lio/reactivex2/Scheduler$Worker;->dispose()V

    goto/32 :goto_2

    nop

    :goto_17
    if-nez p2, :cond_3

    goto/32 :goto_10

    :cond_3
    goto/32 :goto_1f

    nop

    :goto_18
    return v1

    :goto_19
    goto/32 :goto_b

    nop

    :goto_1a
    iget-boolean v0, p0, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->delayError:Z

    goto/32 :goto_25

    nop

    :goto_1b
    invoke-virtual {p1}, Lio/reactivex2/Scheduler$Worker;->dispose()V

    goto/32 :goto_f

    nop

    :goto_1c
    const/4 v1, 0x1

    goto/32 :goto_11

    nop

    :goto_1d
    invoke-interface {p3}, Lio/reactivex2/Observer;->onComplete()V

    :goto_1e
    goto/32 :goto_22

    nop

    :goto_1f
    iput-boolean v1, p0, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->disposed:Z

    goto/32 :goto_15

    nop

    :goto_20
    invoke-interface {p2}, Lio/reactivex2/internal/fuseable/SimpleQueue;->clear()V

    goto/32 :goto_28

    nop

    :goto_21
    if-nez p1, :cond_4

    goto/32 :goto_d

    :cond_4
    goto/32 :goto_7

    nop

    :goto_22
    iget-object p1, p0, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->worker:Lio/reactivex2/Scheduler$Worker;

    goto/32 :goto_16

    nop

    :goto_23
    iget-boolean v0, p0, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->disposed:Z

    goto/32 :goto_1c

    nop

    :goto_24
    iget-object p1, p0, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->error:Ljava/lang/Throwable;

    goto/32 :goto_1a

    nop

    :goto_25
    if-nez v0, :cond_5

    goto/32 :goto_3

    :cond_5
    goto/32 :goto_e

    nop

    :goto_26
    if-nez p1, :cond_6

    goto/32 :goto_1

    :cond_6
    goto/32 :goto_a

    nop

    :goto_27
    iput-boolean v1, p0, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->disposed:Z

    goto/32 :goto_26

    nop

    :goto_28
    invoke-interface {p3, p1}, Lio/reactivex2/Observer;->onError(Ljava/lang/Throwable;)V

    goto/32 :goto_8

    nop
.end method

.method public clear()V
    .locals 1

    iget-object v0, p0, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->queue:Lio/reactivex2/internal/fuseable/SimpleQueue;

    invoke-interface {v0}, Lio/reactivex2/internal/fuseable/SimpleQueue;->clear()V

    return-void
.end method

.method public dispose()V
    .locals 1

    iget-boolean v0, p0, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->disposed:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->disposed:Z

    iget-object v0, p0, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->upstream:Lio/reactivex2/disposables/Disposable;

    invoke-interface {v0}, Lio/reactivex2/disposables/Disposable;->dispose()V

    iget-object v0, p0, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->worker:Lio/reactivex2/Scheduler$Worker;

    invoke-virtual {v0}, Lio/reactivex2/Scheduler$Worker;->dispose()V

    invoke-virtual {p0}, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->getAndIncrement()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->queue:Lio/reactivex2/internal/fuseable/SimpleQueue;

    invoke-interface {v0}, Lio/reactivex2/internal/fuseable/SimpleQueue;->clear()V

    :cond_0
    return-void
.end method

.method drainFused()V
    .locals 5

    goto/32 :goto_18

    nop

    :goto_0
    invoke-interface {v0, v3}, Lio/reactivex2/Observer;->onError(Ljava/lang/Throwable;)V

    goto/32 :goto_25

    nop

    :goto_1
    invoke-interface {v1, v0}, Lio/reactivex2/Observer;->onError(Ljava/lang/Throwable;)V

    goto/32 :goto_1e

    nop

    :goto_2
    iput-boolean v0, p0, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->disposed:Z

    goto/32 :goto_1c

    nop

    :goto_3
    iget-object v1, p0, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->downstream:Lio/reactivex2/Observer;

    goto/32 :goto_1

    nop

    :goto_4
    return-void

    :goto_5
    invoke-virtual {v0}, Lio/reactivex2/Scheduler$Worker;->dispose()V

    goto/32 :goto_16

    nop

    :goto_6
    if-nez v0, :cond_0

    goto/32 :goto_1f

    :cond_0
    goto/32 :goto_3

    nop

    :goto_7
    iget-boolean v2, p0, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->done:Z

    goto/32 :goto_23

    nop

    :goto_8
    move v1, v0

    :goto_9
    goto/32 :goto_e

    nop

    :goto_a
    return-void

    :goto_b
    goto/32 :goto_22

    nop

    :goto_c
    iget-boolean v4, p0, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->delayError:Z

    goto/32 :goto_d

    nop

    :goto_d
    if-eqz v4, :cond_1

    goto/32 :goto_17

    :cond_1
    goto/32 :goto_1b

    nop

    :goto_e
    iget-boolean v2, p0, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->disposed:Z

    goto/32 :goto_11

    nop

    :goto_f
    return-void

    :goto_10
    goto/32 :goto_7

    nop

    :goto_11
    if-nez v2, :cond_2

    goto/32 :goto_10

    :cond_2
    goto/32 :goto_f

    nop

    :goto_12
    invoke-virtual {v0}, Lio/reactivex2/Scheduler$Worker;->dispose()V

    goto/32 :goto_a

    nop

    :goto_13
    invoke-interface {v0}, Lio/reactivex2/Observer;->onComplete()V

    :goto_14
    goto/32 :goto_1d

    nop

    :goto_15
    iget-object v3, p0, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->downstream:Lio/reactivex2/Observer;

    goto/32 :goto_24

    nop

    :goto_16
    return-void

    :goto_17
    goto/32 :goto_15

    nop

    :goto_18
    const/4 v0, 0x1

    goto/32 :goto_8

    nop

    :goto_19
    if-eqz v1, :cond_3

    goto/32 :goto_9

    :cond_3
    goto/32 :goto_4

    nop

    :goto_1a
    iput-boolean v0, p0, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->disposed:Z

    goto/32 :goto_21

    nop

    :goto_1b
    if-nez v2, :cond_4

    goto/32 :goto_17

    :cond_4
    goto/32 :goto_28

    nop

    :goto_1c
    iget-object v0, p0, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->downstream:Lio/reactivex2/Observer;

    goto/32 :goto_0

    nop

    :goto_1d
    iget-object v0, p0, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->worker:Lio/reactivex2/Scheduler$Worker;

    goto/32 :goto_12

    nop

    :goto_1e
    goto :goto_14

    :goto_1f
    goto/32 :goto_26

    nop

    :goto_20
    if-nez v2, :cond_5

    goto/32 :goto_b

    :cond_5
    goto/32 :goto_1a

    nop

    :goto_21
    iget-object v0, p0, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->error:Ljava/lang/Throwable;

    goto/32 :goto_6

    nop

    :goto_22
    neg-int v1, v1

    goto/32 :goto_29

    nop

    :goto_23
    iget-object v3, p0, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->error:Ljava/lang/Throwable;

    goto/32 :goto_c

    nop

    :goto_24
    const/4 v4, 0x0

    goto/32 :goto_27

    nop

    :goto_25
    iget-object v0, p0, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->worker:Lio/reactivex2/Scheduler$Worker;

    goto/32 :goto_5

    nop

    :goto_26
    iget-object v0, p0, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->downstream:Lio/reactivex2/Observer;

    goto/32 :goto_13

    nop

    :goto_27
    invoke-interface {v3, v4}, Lio/reactivex2/Observer;->onNext(Ljava/lang/Object;)V

    goto/32 :goto_20

    nop

    :goto_28
    if-nez v3, :cond_6

    goto/32 :goto_17

    :cond_6
    goto/32 :goto_2

    nop

    :goto_29
    invoke-virtual {p0, v1}, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->addAndGet(I)I

    move-result v1

    goto/32 :goto_19

    nop
.end method

.method drainNormal()V
    .locals 7

    goto/32 :goto_3

    nop

    :goto_0
    invoke-virtual {p0, v4, v6, v1}, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->checkTerminated(ZZLio/reactivex2/Observer;)Z

    move-result v4

    goto/32 :goto_21

    nop

    :goto_1
    return-void

    :goto_2
    goto/32 :goto_5

    nop

    :goto_3
    iget-object v0, p0, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->queue:Lio/reactivex2/internal/fuseable/SimpleQueue;

    goto/32 :goto_11

    nop

    :goto_4
    iget-object v0, p0, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->worker:Lio/reactivex2/Scheduler$Worker;

    goto/32 :goto_1d

    nop

    :goto_5
    if-nez v6, :cond_0

    goto/32 :goto_16

    :cond_0
    goto/32 :goto_b

    nop

    :goto_6
    const/4 v6, 0x0

    :goto_7
    goto/32 :goto_0

    nop

    :goto_8
    if-eqz v3, :cond_1

    goto/32 :goto_f

    :cond_1
    goto/32 :goto_15

    nop

    :goto_9
    invoke-static {v3}, Lio/reactivex2/exceptions/Exceptions;->throwIfFatal(Ljava/lang/Throwable;)V

    goto/32 :goto_18

    nop

    :goto_a
    invoke-interface {v1, v3}, Lio/reactivex2/Observer;->onError(Ljava/lang/Throwable;)V

    goto/32 :goto_4

    nop

    :goto_b
    neg-int v3, v3

    goto/32 :goto_12

    nop

    :goto_c
    goto :goto_7

    :goto_d
    goto/32 :goto_6

    nop

    :goto_e
    move v3, v2

    :goto_f
    goto/32 :goto_13

    nop

    :goto_10
    const/4 v2, 0x1

    goto/32 :goto_e

    nop

    :goto_11
    iget-object v1, p0, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->downstream:Lio/reactivex2/Observer;

    goto/32 :goto_10

    nop

    :goto_12
    invoke-virtual {p0, v3}, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->addAndGet(I)I

    move-result v3

    goto/32 :goto_8

    nop

    :goto_13
    iget-boolean v4, p0, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->done:Z

    goto/32 :goto_20

    nop

    :goto_14
    return-void

    :goto_15
    return-void

    :goto_16
    goto/32 :goto_1c

    nop

    :goto_17
    iget-boolean v4, p0, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->done:Z

    :try_start_0
    invoke-interface {v0}, Lio/reactivex2/internal/fuseable/SimpleQueue;->poll()Ljava/lang/Object;

    move-result-object v5
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_26

    nop

    :goto_18
    iput-boolean v2, p0, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->disposed:Z

    goto/32 :goto_1b

    nop

    :goto_19
    move v6, v2

    goto/32 :goto_c

    nop

    :goto_1a
    goto :goto_24

    :catch_0
    move-exception v3

    goto/32 :goto_9

    nop

    :goto_1b
    iget-object v2, p0, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->upstream:Lio/reactivex2/disposables/Disposable;

    goto/32 :goto_1e

    nop

    :goto_1c
    invoke-interface {v1, v5}, Lio/reactivex2/Observer;->onNext(Ljava/lang/Object;)V

    goto/32 :goto_1a

    nop

    :goto_1d
    invoke-virtual {v0}, Lio/reactivex2/Scheduler$Worker;->dispose()V

    goto/32 :goto_14

    nop

    :goto_1e
    invoke-interface {v2}, Lio/reactivex2/disposables/Disposable;->dispose()V

    goto/32 :goto_1f

    nop

    :goto_1f
    invoke-interface {v0}, Lio/reactivex2/internal/fuseable/SimpleQueue;->clear()V

    goto/32 :goto_a

    nop

    :goto_20
    invoke-interface {v0}, Lio/reactivex2/internal/fuseable/SimpleQueue;->isEmpty()Z

    move-result v5

    goto/32 :goto_25

    nop

    :goto_21
    if-nez v4, :cond_2

    goto/32 :goto_2

    :cond_2
    goto/32 :goto_1

    nop

    :goto_22
    if-nez v4, :cond_3

    goto/32 :goto_24

    :cond_3
    goto/32 :goto_23

    nop

    :goto_23
    return-void

    :goto_24
    goto/32 :goto_17

    nop

    :goto_25
    invoke-virtual {p0, v4, v5, v1}, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->checkTerminated(ZZLio/reactivex2/Observer;)Z

    move-result v4

    goto/32 :goto_22

    nop

    :goto_26
    if-eqz v5, :cond_4

    goto/32 :goto_d

    :cond_4
    goto/32 :goto_19

    nop
.end method

.method public isDisposed()Z
    .locals 1

    iget-boolean v0, p0, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->disposed:Z

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    iget-object v0, p0, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->queue:Lio/reactivex2/internal/fuseable/SimpleQueue;

    invoke-interface {v0}, Lio/reactivex2/internal/fuseable/SimpleQueue;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public onComplete()V
    .locals 1

    iget-boolean v0, p0, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->done:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->done:Z

    invoke-virtual {p0}, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->schedule()V

    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    iget-boolean v0, p0, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->done:Z

    if-eqz v0, :cond_0

    invoke-static {p1}, Lio/reactivex2/plugins/RxJavaPlugins;->onError(Ljava/lang/Throwable;)V

    return-void

    :cond_0
    iput-object p1, p0, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->error:Ljava/lang/Throwable;

    const/4 p1, 0x1

    iput-boolean p1, p0, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->done:Z

    invoke-virtual {p0}, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->schedule()V

    return-void
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    iget-boolean v0, p0, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->done:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget v0, p0, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->sourceMode:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->queue:Lio/reactivex2/internal/fuseable/SimpleQueue;

    invoke-interface {v0, p1}, Lio/reactivex2/internal/fuseable/SimpleQueue;->offer(Ljava/lang/Object;)Z

    :cond_1
    invoke-virtual {p0}, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->schedule()V

    return-void
.end method

.method public onSubscribe(Lio/reactivex2/disposables/Disposable;)V
    .locals 2

    iget-object v0, p0, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->upstream:Lio/reactivex2/disposables/Disposable;

    invoke-static {v0, p1}, Lio/reactivex2/internal/disposables/DisposableHelper;->validate(Lio/reactivex2/disposables/Disposable;Lio/reactivex2/disposables/Disposable;)Z

    move-result v0

    if-eqz v0, :cond_2

    iput-object p1, p0, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->upstream:Lio/reactivex2/disposables/Disposable;

    instance-of v0, p1, Lio/reactivex2/internal/fuseable/QueueDisposable;

    if-eqz v0, :cond_1

    check-cast p1, Lio/reactivex2/internal/fuseable/QueueDisposable;

    const/4 v0, 0x7

    invoke-interface {p1, v0}, Lio/reactivex2/internal/fuseable/QueueDisposable;->requestFusion(I)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iput v0, p0, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->sourceMode:I

    iput-object p1, p0, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->queue:Lio/reactivex2/internal/fuseable/SimpleQueue;

    iput-boolean v1, p0, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->done:Z

    iget-object p1, p0, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->downstream:Lio/reactivex2/Observer;

    invoke-interface {p1, p0}, Lio/reactivex2/Observer;->onSubscribe(Lio/reactivex2/disposables/Disposable;)V

    invoke-virtual {p0}, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->schedule()V

    return-void

    :cond_0
    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    iput v0, p0, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->sourceMode:I

    iput-object p1, p0, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->queue:Lio/reactivex2/internal/fuseable/SimpleQueue;

    iget-object p1, p0, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->downstream:Lio/reactivex2/Observer;

    invoke-interface {p1, p0}, Lio/reactivex2/Observer;->onSubscribe(Lio/reactivex2/disposables/Disposable;)V

    return-void

    :cond_1
    new-instance p1, Lio/reactivex2/internal/queue/SpscLinkedArrayQueue;

    iget v0, p0, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->bufferSize:I

    invoke-direct {p1, v0}, Lio/reactivex2/internal/queue/SpscLinkedArrayQueue;-><init>(I)V

    iput-object p1, p0, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->queue:Lio/reactivex2/internal/fuseable/SimpleQueue;

    iget-object p1, p0, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->downstream:Lio/reactivex2/Observer;

    invoke-interface {p1, p0}, Lio/reactivex2/Observer;->onSubscribe(Lio/reactivex2/disposables/Disposable;)V

    :cond_2
    return-void
.end method

.method public poll()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-object v0, p0, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->queue:Lio/reactivex2/internal/fuseable/SimpleQueue;

    invoke-interface {v0}, Lio/reactivex2/internal/fuseable/SimpleQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public requestFusion(I)I
    .locals 1

    const/4 v0, 0x2

    and-int/2addr p1, v0

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    iput-boolean p1, p0, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->outputFused:Z

    return v0

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public run()V
    .locals 1

    iget-boolean v0, p0, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->outputFused:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->drainFused()V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->drainNormal()V

    :goto_0
    return-void
.end method

.method schedule()V
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    invoke-virtual {p0}, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->getAndIncrement()I

    move-result v0

    goto/32 :goto_3

    nop

    :goto_1
    invoke-virtual {v0, p0}, Lio/reactivex2/Scheduler$Worker;->schedule(Ljava/lang/Runnable;)Lio/reactivex2/disposables/Disposable;

    :goto_2
    goto/32 :goto_5

    nop

    :goto_3
    if-eqz v0, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_4

    nop

    :goto_4
    iget-object v0, p0, Lio/reactivex2/internal/operators/observable/ObservableObserveOn$ObserveOnObserver;->worker:Lio/reactivex2/Scheduler$Worker;

    goto/32 :goto_1

    nop

    :goto_5
    return-void
.end method
