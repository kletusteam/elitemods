.class final Lio/reactivex2/internal/operators/observable/ObservableCreate$SerializedEmitter;
.super Ljava/util/concurrent/atomic/AtomicInteger;

# interfaces
.implements Lio/reactivex2/ObservableEmitter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/reactivex2/internal/operators/observable/ObservableCreate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "SerializedEmitter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/concurrent/atomic/AtomicInteger;",
        "Lio/reactivex2/ObservableEmitter<",
        "TT;>;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x43c4fdd95fbcd5c6L


# instance fields
.field volatile done:Z

.field final emitter:Lio/reactivex2/ObservableEmitter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex2/ObservableEmitter<",
            "TT;>;"
        }
    .end annotation
.end field

.field final error:Lio/reactivex2/internal/util/AtomicThrowable;

.field final queue:Lio/reactivex2/internal/queue/SpscLinkedArrayQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex2/internal/queue/SpscLinkedArrayQueue<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lio/reactivex2/ObservableEmitter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex2/ObservableEmitter<",
            "TT;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object p1, p0, Lio/reactivex2/internal/operators/observable/ObservableCreate$SerializedEmitter;->emitter:Lio/reactivex2/ObservableEmitter;

    new-instance p1, Lio/reactivex2/internal/util/AtomicThrowable;

    invoke-direct {p1}, Lio/reactivex2/internal/util/AtomicThrowable;-><init>()V

    iput-object p1, p0, Lio/reactivex2/internal/operators/observable/ObservableCreate$SerializedEmitter;->error:Lio/reactivex2/internal/util/AtomicThrowable;

    new-instance p1, Lio/reactivex2/internal/queue/SpscLinkedArrayQueue;

    const/16 v0, 0x10

    invoke-direct {p1, v0}, Lio/reactivex2/internal/queue/SpscLinkedArrayQueue;-><init>(I)V

    iput-object p1, p0, Lio/reactivex2/internal/operators/observable/ObservableCreate$SerializedEmitter;->queue:Lio/reactivex2/internal/queue/SpscLinkedArrayQueue;

    return-void
.end method


# virtual methods
.method drain()V
    .locals 1

    goto/32 :goto_4

    nop

    :goto_0
    return-void

    :goto_1
    invoke-virtual {p0}, Lio/reactivex2/internal/operators/observable/ObservableCreate$SerializedEmitter;->drainLoop()V

    :goto_2
    goto/32 :goto_0

    nop

    :goto_3
    if-eqz v0, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_1

    nop

    :goto_4
    invoke-virtual {p0}, Lio/reactivex2/internal/operators/observable/ObservableCreate$SerializedEmitter;->getAndIncrement()I

    move-result v0

    goto/32 :goto_3

    nop
.end method

.method drainLoop()V
    .locals 8

    goto/32 :goto_d

    nop

    :goto_0
    invoke-interface {v0, v6}, Lio/reactivex2/ObservableEmitter;->onNext(Ljava/lang/Object;)V

    goto/32 :goto_1f

    nop

    :goto_1
    if-nez v5, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_25

    nop

    :goto_2
    invoke-interface {v0}, Lio/reactivex2/ObservableEmitter;->isDisposed()Z

    move-result v5

    goto/32 :goto_1

    nop

    :goto_3
    invoke-interface {v0, v1}, Lio/reactivex2/ObservableEmitter;->onError(Ljava/lang/Throwable;)V

    goto/32 :goto_10

    nop

    :goto_4
    const/4 v7, 0x0

    :goto_5
    goto/32 :goto_8

    nop

    :goto_6
    return-void

    :goto_7
    goto/32 :goto_26

    nop

    :goto_8
    if-nez v5, :cond_1

    goto/32 :goto_f

    :cond_1
    goto/32 :goto_c

    nop

    :goto_9
    iget-boolean v5, p0, Lio/reactivex2/internal/operators/observable/ObservableCreate$SerializedEmitter;->done:Z

    goto/32 :goto_1c

    nop

    :goto_a
    iget-object v1, p0, Lio/reactivex2/internal/operators/observable/ObservableCreate$SerializedEmitter;->queue:Lio/reactivex2/internal/queue/SpscLinkedArrayQueue;

    goto/32 :goto_15

    nop

    :goto_b
    if-eqz v4, :cond_2

    goto/32 :goto_21

    :cond_2
    goto/32 :goto_13

    nop

    :goto_c
    if-nez v7, :cond_3

    goto/32 :goto_f

    :cond_3
    goto/32 :goto_1e

    nop

    :goto_d
    iget-object v0, p0, Lio/reactivex2/internal/operators/observable/ObservableCreate$SerializedEmitter;->emitter:Lio/reactivex2/ObservableEmitter;

    goto/32 :goto_a

    nop

    :goto_e
    return-void

    :goto_f
    goto/32 :goto_1b

    nop

    :goto_10
    return-void

    :goto_11
    goto/32 :goto_9

    nop

    :goto_12
    if-nez v5, :cond_4

    goto/32 :goto_11

    :cond_4
    goto/32 :goto_22

    nop

    :goto_13
    return-void

    :goto_14
    goto/32 :goto_0

    nop

    :goto_15
    iget-object v2, p0, Lio/reactivex2/internal/operators/observable/ObservableCreate$SerializedEmitter;->error:Lio/reactivex2/internal/util/AtomicThrowable;

    goto/32 :goto_1a

    nop

    :goto_16
    invoke-virtual {p0, v4}, Lio/reactivex2/internal/operators/observable/ObservableCreate$SerializedEmitter;->addAndGet(I)I

    move-result v4

    goto/32 :goto_b

    nop

    :goto_17
    neg-int v4, v4

    goto/32 :goto_16

    nop

    :goto_18
    goto :goto_5

    :goto_19
    goto/32 :goto_4

    nop

    :goto_1a
    const/4 v3, 0x1

    goto/32 :goto_20

    nop

    :goto_1b
    if-nez v7, :cond_5

    goto/32 :goto_14

    :cond_5
    goto/32 :goto_17

    nop

    :goto_1c
    invoke-virtual {v1}, Lio/reactivex2/internal/queue/SpscLinkedArrayQueue;->poll()Ljava/lang/Object;

    move-result-object v6

    goto/32 :goto_1d

    nop

    :goto_1d
    if-eqz v6, :cond_6

    goto/32 :goto_19

    :cond_6
    goto/32 :goto_24

    nop

    :goto_1e
    invoke-interface {v0}, Lio/reactivex2/ObservableEmitter;->onComplete()V

    goto/32 :goto_e

    nop

    :goto_1f
    goto :goto_21

    :goto_20
    move v4, v3

    :goto_21
    goto/32 :goto_2

    nop

    :goto_22
    invoke-virtual {v1}, Lio/reactivex2/internal/queue/SpscLinkedArrayQueue;->clear()V

    goto/32 :goto_23

    nop

    :goto_23
    invoke-virtual {v2}, Lio/reactivex2/internal/util/AtomicThrowable;->terminate()Ljava/lang/Throwable;

    move-result-object v1

    goto/32 :goto_3

    nop

    :goto_24
    move v7, v3

    goto/32 :goto_18

    nop

    :goto_25
    invoke-virtual {v1}, Lio/reactivex2/internal/queue/SpscLinkedArrayQueue;->clear()V

    goto/32 :goto_6

    nop

    :goto_26
    invoke-virtual {v2}, Lio/reactivex2/internal/util/AtomicThrowable;->get()Ljava/lang/Object;

    move-result-object v5

    goto/32 :goto_12

    nop
.end method

.method public isDisposed()Z
    .locals 1

    iget-object v0, p0, Lio/reactivex2/internal/operators/observable/ObservableCreate$SerializedEmitter;->emitter:Lio/reactivex2/ObservableEmitter;

    invoke-interface {v0}, Lio/reactivex2/ObservableEmitter;->isDisposed()Z

    move-result v0

    return v0
.end method

.method public onComplete()V
    .locals 1

    iget-object v0, p0, Lio/reactivex2/internal/operators/observable/ObservableCreate$SerializedEmitter;->emitter:Lio/reactivex2/ObservableEmitter;

    invoke-interface {v0}, Lio/reactivex2/ObservableEmitter;->isDisposed()Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lio/reactivex2/internal/operators/observable/ObservableCreate$SerializedEmitter;->done:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/reactivex2/internal/operators/observable/ObservableCreate$SerializedEmitter;->done:Z

    invoke-virtual {p0}, Lio/reactivex2/internal/operators/observable/ObservableCreate$SerializedEmitter;->drain()V

    return-void

    :cond_1
    :goto_0
    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    invoke-virtual {p0, p1}, Lio/reactivex2/internal/operators/observable/ObservableCreate$SerializedEmitter;->tryOnError(Ljava/lang/Throwable;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, Lio/reactivex2/plugins/RxJavaPlugins;->onError(Ljava/lang/Throwable;)V

    :cond_0
    return-void
.end method

.method public onNext(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    iget-object v0, p0, Lio/reactivex2/internal/operators/observable/ObservableCreate$SerializedEmitter;->emitter:Lio/reactivex2/ObservableEmitter;

    invoke-interface {v0}, Lio/reactivex2/ObservableEmitter;->isDisposed()Z

    move-result v0

    if-nez v0, :cond_4

    iget-boolean v0, p0, Lio/reactivex2/internal/operators/observable/ObservableCreate$SerializedEmitter;->done:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    if-nez p1, :cond_1

    new-instance p1, Ljava/lang/NullPointerException;

    const-string v0, "onNext called with null. Null values are generally not allowed in 2.x operators and sources."

    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lio/reactivex2/internal/operators/observable/ObservableCreate$SerializedEmitter;->onError(Ljava/lang/Throwable;)V

    return-void

    :cond_1
    invoke-virtual {p0}, Lio/reactivex2/internal/operators/observable/ObservableCreate$SerializedEmitter;->get()I

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lio/reactivex2/internal/operators/observable/ObservableCreate$SerializedEmitter;->compareAndSet(II)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lio/reactivex2/internal/operators/observable/ObservableCreate$SerializedEmitter;->emitter:Lio/reactivex2/ObservableEmitter;

    invoke-interface {v0, p1}, Lio/reactivex2/ObservableEmitter;->onNext(Ljava/lang/Object;)V

    invoke-virtual {p0}, Lio/reactivex2/internal/operators/observable/ObservableCreate$SerializedEmitter;->decrementAndGet()I

    move-result p1

    if-nez p1, :cond_3

    return-void

    :cond_2
    iget-object v0, p0, Lio/reactivex2/internal/operators/observable/ObservableCreate$SerializedEmitter;->queue:Lio/reactivex2/internal/queue/SpscLinkedArrayQueue;

    monitor-enter v0

    :try_start_0
    invoke-interface {v0, p1}, Lio/reactivex2/internal/fuseable/SimpleQueue;->offer(Ljava/lang/Object;)Z

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Lio/reactivex2/internal/operators/observable/ObservableCreate$SerializedEmitter;->getAndIncrement()I

    move-result p1

    if-eqz p1, :cond_3

    return-void

    :cond_3
    invoke-virtual {p0}, Lio/reactivex2/internal/operators/observable/ObservableCreate$SerializedEmitter;->drainLoop()V

    return-void

    :catchall_0
    move-exception p1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1

    :cond_4
    :goto_0
    return-void
.end method

.method public serialize()Lio/reactivex2/ObservableEmitter;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex2/ObservableEmitter<",
            "TT;>;"
        }
    .end annotation

    return-object p0
.end method

.method public setCancellable(Lio/reactivex2/functions/Cancellable;)V
    .locals 1

    iget-object v0, p0, Lio/reactivex2/internal/operators/observable/ObservableCreate$SerializedEmitter;->emitter:Lio/reactivex2/ObservableEmitter;

    invoke-interface {v0, p1}, Lio/reactivex2/ObservableEmitter;->setCancellable(Lio/reactivex2/functions/Cancellable;)V

    return-void
.end method

.method public setDisposable(Lio/reactivex2/disposables/Disposable;)V
    .locals 1

    iget-object v0, p0, Lio/reactivex2/internal/operators/observable/ObservableCreate$SerializedEmitter;->emitter:Lio/reactivex2/ObservableEmitter;

    invoke-interface {v0, p1}, Lio/reactivex2/ObservableEmitter;->setDisposable(Lio/reactivex2/disposables/Disposable;)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lio/reactivex2/internal/operators/observable/ObservableCreate$SerializedEmitter;->emitter:Lio/reactivex2/ObservableEmitter;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public tryOnError(Ljava/lang/Throwable;)Z
    .locals 2

    iget-object v0, p0, Lio/reactivex2/internal/operators/observable/ObservableCreate$SerializedEmitter;->emitter:Lio/reactivex2/ObservableEmitter;

    invoke-interface {v0}, Lio/reactivex2/ObservableEmitter;->isDisposed()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lio/reactivex2/internal/operators/observable/ObservableCreate$SerializedEmitter;->done:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    if-nez p1, :cond_1

    new-instance p1, Ljava/lang/NullPointerException;

    const-string v0, "onError called with null. Null values are generally not allowed in 2.x operators and sources."

    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lio/reactivex2/internal/operators/observable/ObservableCreate$SerializedEmitter;->error:Lio/reactivex2/internal/util/AtomicThrowable;

    invoke-virtual {v0, p1}, Lio/reactivex2/internal/util/AtomicThrowable;->addThrowable(Ljava/lang/Throwable;)Z

    move-result p1

    if-eqz p1, :cond_2

    const/4 p1, 0x1

    iput-boolean p1, p0, Lio/reactivex2/internal/operators/observable/ObservableCreate$SerializedEmitter;->done:Z

    invoke-virtual {p0}, Lio/reactivex2/internal/operators/observable/ObservableCreate$SerializedEmitter;->drain()V

    return p1

    :cond_2
    return v1

    :cond_3
    :goto_0
    return v1
.end method
