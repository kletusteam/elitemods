.class public final Lio/reactivex2/disposables/CompositeDisposable;
.super Ljava/lang/Object;

# interfaces
.implements Lio/reactivex2/disposables/Disposable;
.implements Lio/reactivex2/internal/disposables/DisposableContainer;


# instance fields
.field volatile disposed:Z

.field resources:Lio/reactivex2/internal/util/OpenHashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex2/internal/util/OpenHashSet<",
            "Lio/reactivex2/disposables/Disposable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public add(Lio/reactivex2/disposables/Disposable;)Z
    .locals 1

    const-string v0, "disposable is null"

    invoke-static {p1, v0}, Lio/reactivex2/internal/functions/ObjectHelper;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    iget-boolean v0, p0, Lio/reactivex2/disposables/CompositeDisposable;->disposed:Z

    if-nez v0, :cond_2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lio/reactivex2/disposables/CompositeDisposable;->disposed:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lio/reactivex2/disposables/CompositeDisposable;->resources:Lio/reactivex2/internal/util/OpenHashSet;

    if-nez v0, :cond_0

    new-instance v0, Lio/reactivex2/internal/util/OpenHashSet;

    invoke-direct {v0}, Lio/reactivex2/internal/util/OpenHashSet;-><init>()V

    iput-object v0, p0, Lio/reactivex2/disposables/CompositeDisposable;->resources:Lio/reactivex2/internal/util/OpenHashSet;

    :cond_0
    invoke-virtual {v0, p1}, Lio/reactivex2/internal/util/OpenHashSet;->add(Ljava/lang/Object;)Z

    const/4 p1, 0x1

    monitor-exit p0

    return p1

    :cond_1
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    :cond_2
    :goto_0
    invoke-interface {p1}, Lio/reactivex2/disposables/Disposable;->dispose()V

    const/4 p1, 0x0

    return p1
.end method

.method public delete(Lio/reactivex2/disposables/Disposable;)Z
    .locals 2

    const-string v0, "disposables is null"

    invoke-static {p1, v0}, Lio/reactivex2/internal/functions/ObjectHelper;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    iget-boolean v0, p0, Lio/reactivex2/disposables/CompositeDisposable;->disposed:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    :cond_0
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lio/reactivex2/disposables/CompositeDisposable;->disposed:Z

    if-eqz v0, :cond_1

    monitor-exit p0

    return v1

    :cond_1
    iget-object v0, p0, Lio/reactivex2/disposables/CompositeDisposable;->resources:Lio/reactivex2/internal/util/OpenHashSet;

    if-eqz v0, :cond_3

    invoke-virtual {v0, p1}, Lio/reactivex2/internal/util/OpenHashSet;->remove(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_2

    goto :goto_0

    :cond_2
    monitor-exit p0

    const/4 p1, 0x1

    return p1

    :cond_3
    :goto_0
    monitor-exit p0

    return v1

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public dispose()V
    .locals 2

    iget-boolean v0, p0, Lio/reactivex2/disposables/CompositeDisposable;->disposed:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lio/reactivex2/disposables/CompositeDisposable;->disposed:Z

    if-eqz v0, :cond_1

    monitor-exit p0

    return-void

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/reactivex2/disposables/CompositeDisposable;->disposed:Z

    iget-object v0, p0, Lio/reactivex2/disposables/CompositeDisposable;->resources:Lio/reactivex2/internal/util/OpenHashSet;

    const/4 v1, 0x0

    iput-object v1, p0, Lio/reactivex2/disposables/CompositeDisposable;->resources:Lio/reactivex2/internal/util/OpenHashSet;

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0, v0}, Lio/reactivex2/disposables/CompositeDisposable;->dispose(Lio/reactivex2/internal/util/OpenHashSet;)V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method dispose(Lio/reactivex2/internal/util/OpenHashSet;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex2/internal/util/OpenHashSet<",
            "Lio/reactivex2/disposables/Disposable;",
            ">;)V"
        }
    .end annotation

    goto/32 :goto_15

    nop

    :goto_0
    invoke-virtual {p1}, Lio/reactivex2/internal/util/OpenHashSet;->keys()[Ljava/lang/Object;

    move-result-object p1

    goto/32 :goto_5

    nop

    :goto_1
    if-eq p1, v0, :cond_0

    goto/32 :goto_26

    :cond_0
    goto/32 :goto_b

    nop

    :goto_2
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result p1

    goto/32 :goto_1b

    nop

    :goto_3
    goto :goto_e

    :goto_4
    goto/32 :goto_10

    nop

    :goto_5
    array-length v1, p1

    goto/32 :goto_13

    nop

    :goto_6
    new-instance v3, Ljava/util/ArrayList;

    goto/32 :goto_23

    nop

    :goto_7
    if-eqz v3, :cond_1

    goto/32 :goto_24

    :cond_1
    goto/32 :goto_6

    nop

    :goto_8
    invoke-direct {p1, v3}, Lio/reactivex2/exceptions/CompositeException;-><init>(Ljava/lang/Iterable;)V

    goto/32 :goto_20

    nop

    :goto_9
    check-cast p1, Ljava/lang/Throwable;

    goto/32 :goto_1f

    nop

    :goto_a
    aget-object v4, p1, v0

    goto/32 :goto_1e

    nop

    :goto_b
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    goto/32 :goto_9

    nop

    :goto_c
    if-lt v0, v1, :cond_2

    goto/32 :goto_4

    :cond_2
    goto/32 :goto_a

    nop

    :goto_d
    move v0, v2

    :goto_e
    goto/32 :goto_c

    nop

    :goto_f
    const/4 v0, 0x0

    goto/32 :goto_0

    nop

    :goto_10
    if-nez v3, :cond_3

    goto/32 :goto_21

    :cond_3
    goto/32 :goto_2

    nop

    :goto_11
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_12
    goto/32 :goto_14

    nop

    :goto_13
    const/4 v2, 0x0

    goto/32 :goto_18

    nop

    :goto_14
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_3

    nop

    :goto_15
    if-eqz p1, :cond_4

    goto/32 :goto_1d

    :cond_4
    goto/32 :goto_1c

    nop

    :goto_16
    new-instance p1, Lio/reactivex2/exceptions/CompositeException;

    goto/32 :goto_8

    nop

    :goto_17
    invoke-static {v4}, Lio/reactivex2/exceptions/Exceptions;->throwIfFatal(Ljava/lang/Throwable;)V

    goto/32 :goto_7

    nop

    :goto_18
    move-object v3, v0

    goto/32 :goto_d

    nop

    :goto_19
    goto :goto_12

    :catch_0
    move-exception v4

    goto/32 :goto_17

    nop

    :goto_1a
    if-nez v5, :cond_5

    goto/32 :goto_12

    :cond_5
    :try_start_0
    check-cast v4, Lio/reactivex2/disposables/Disposable;

    invoke-interface {v4}, Lio/reactivex2/disposables/Disposable;->dispose()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_19

    nop

    :goto_1b
    const/4 v0, 0x1

    goto/32 :goto_1

    nop

    :goto_1c
    return-void

    :goto_1d
    goto/32 :goto_f

    nop

    :goto_1e
    instance-of v5, v4, Lio/reactivex2/disposables/Disposable;

    goto/32 :goto_1a

    nop

    :goto_1f
    invoke-static {p1}, Lio/reactivex2/internal/util/ExceptionHelper;->wrapOrThrow(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object p1

    goto/32 :goto_25

    nop

    :goto_20
    throw p1

    :goto_21
    goto/32 :goto_22

    nop

    :goto_22
    return-void

    :goto_23
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    :goto_24
    goto/32 :goto_11

    nop

    :goto_25
    throw p1

    :goto_26
    goto/32 :goto_16

    nop
.end method

.method public isDisposed()Z
    .locals 1

    iget-boolean v0, p0, Lio/reactivex2/disposables/CompositeDisposable;->disposed:Z

    return v0
.end method

.method public remove(Lio/reactivex2/disposables/Disposable;)Z
    .locals 1

    invoke-virtual {p0, p1}, Lio/reactivex2/disposables/CompositeDisposable;->delete(Lio/reactivex2/disposables/Disposable;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lio/reactivex2/disposables/Disposable;->dispose()V

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method
