.class final Lio/reactivex2/exceptions/CompositeException$WrappedPrintStream;
.super Lio/reactivex2/exceptions/CompositeException$PrintStreamOrWriter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/reactivex2/exceptions/CompositeException;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "WrappedPrintStream"
.end annotation


# instance fields
.field private final printStream:Ljava/io/PrintStream;


# direct methods
.method constructor <init>(Ljava/io/PrintStream;)V
    .locals 0

    invoke-direct {p0}, Lio/reactivex2/exceptions/CompositeException$PrintStreamOrWriter;-><init>()V

    iput-object p1, p0, Lio/reactivex2/exceptions/CompositeException$WrappedPrintStream;->printStream:Ljava/io/PrintStream;

    return-void
.end method


# virtual methods
.method println(Ljava/lang/Object;)V
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    invoke-virtual {v0, p1}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    goto/32 :goto_1

    nop

    :goto_1
    return-void

    :goto_2
    iget-object v0, p0, Lio/reactivex2/exceptions/CompositeException$WrappedPrintStream;->printStream:Ljava/io/PrintStream;

    goto/32 :goto_0

    nop
.end method
