.class Ly2/c$b;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ly2/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation


# instance fields
.field private final a:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue<",
            "Ly2/c$a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v0, p0, Ly2/c$b;->a:Ljava/util/Queue;

    return-void
.end method


# virtual methods
.method a()Ly2/c$a;
    .locals 2

    goto/32 :goto_3

    nop

    :goto_0
    invoke-direct {v1}, Ly2/c$a;-><init>()V

    :goto_1
    goto/32 :goto_2

    nop

    :goto_2
    return-object v1

    :catchall_0
    move-exception v1

    :try_start_0
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/32 :goto_4

    nop

    :goto_3
    iget-object v0, p0, Ly2/c$b;->a:Ljava/util/Queue;

    goto/32 :goto_5

    nop

    :goto_4
    throw v1

    :goto_5
    monitor-enter v0

    :try_start_1
    iget-object v1, p0, Ly2/c$b;->a:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ly2/c$a;

    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/32 :goto_7

    nop

    :goto_6
    new-instance v1, Ly2/c$a;

    goto/32 :goto_0

    nop

    :goto_7
    if-eqz v1, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_6

    nop
.end method

.method b(Ly2/c$a;)V
    .locals 3

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Ly2/c$b;->a:Ljava/util/Queue;

    goto/32 :goto_1

    nop

    :goto_1
    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Ly2/c$b;->a:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->size()I

    move-result v1

    const/16 v2, 0xa

    if-ge v1, v2, :cond_0

    iget-object v1, p0, Ly2/c$b;->a:Ljava/util/Queue;

    invoke-interface {v1, p1}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    :cond_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/32 :goto_2

    nop

    :goto_2
    throw p1
.end method
