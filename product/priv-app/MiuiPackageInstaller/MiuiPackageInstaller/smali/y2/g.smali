.class public Ly2/g;
.super Lq3/g;

# interfaces
.implements Ly2/h;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lq3/g<",
        "Lu2/f;",
        "Lw2/v<",
        "*>;>;",
        "Ly2/h;"
    }
.end annotation


# instance fields
.field private e:Ly2/h$a;


# direct methods
.method public constructor <init>(J)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lq3/g;-><init>(J)V

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "InlinedApi"
        }
    .end annotation

    const/16 v0, 0x28

    if-lt p1, v0, :cond_0

    invoke-virtual {p0}, Lq3/g;->b()V

    goto :goto_0

    :cond_0
    const/16 v0, 0x14

    if-ge p1, v0, :cond_1

    const/16 v0, 0xf

    if-ne p1, v0, :cond_2

    :cond_1
    invoke-virtual {p0}, Lq3/g;->h()J

    move-result-wide v0

    const-wide/16 v2, 0x2

    div-long/2addr v0, v2

    invoke-virtual {p0, v0, v1}, Lq3/g;->m(J)V

    :cond_2
    :goto_0
    return-void
.end method

.method public bridge synthetic c(Lu2/f;)Lw2/v;
    .locals 0

    invoke-super {p0, p1}, Lq3/g;->l(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lw2/v;

    return-object p1
.end method

.method public bridge synthetic d(Lu2/f;Lw2/v;)Lw2/v;
    .locals 0

    invoke-super {p0, p1, p2}, Lq3/g;->k(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lw2/v;

    return-object p1
.end method

.method public e(Ly2/h$a;)V
    .locals 0

    iput-object p1, p0, Ly2/g;->e:Ly2/h$a;

    return-void
.end method

.method protected bridge synthetic i(Ljava/lang/Object;)I
    .locals 0

    check-cast p1, Lw2/v;

    invoke-virtual {p0, p1}, Ly2/g;->n(Lw2/v;)I

    move-result p1

    return p1
.end method

.method protected bridge synthetic j(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lu2/f;

    check-cast p2, Lw2/v;

    invoke-virtual {p0, p1, p2}, Ly2/g;->o(Lu2/f;Lw2/v;)V

    return-void
.end method

.method protected n(Lw2/v;)I
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lw2/v<",
            "*>;)I"
        }
    .end annotation

    if-nez p1, :cond_0

    const/4 p1, 0x0

    invoke-super {p0, p1}, Lq3/g;->i(Ljava/lang/Object;)I

    move-result p1

    return p1

    :cond_0
    invoke-interface {p1}, Lw2/v;->c()I

    move-result p1

    return p1
.end method

.method protected o(Lu2/f;Lw2/v;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lu2/f;",
            "Lw2/v<",
            "*>;)V"
        }
    .end annotation

    iget-object p1, p0, Ly2/g;->e:Ly2/h$a;

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-interface {p1, p2}, Ly2/h$a;->c(Lw2/v;)V

    :cond_0
    return-void
.end method
