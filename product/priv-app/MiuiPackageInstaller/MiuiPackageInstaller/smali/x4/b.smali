.class public final enum Lx4/b;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lx4/b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lx4/b;

.field public static final enum b:Lx4/b;

.field public static final enum c:Lx4/b;

.field public static final enum d:Lx4/b;

.field public static final enum e:Lx4/b;

.field public static final enum f:Lx4/b;

.field public static final enum g:Lx4/b;

.field public static final enum h:Lx4/b;

.field public static final enum i:Lx4/b;

.field public static final enum j:Lx4/b;

.field private static final synthetic k:[Lx4/b;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    new-instance v0, Lx4/b;

    const-string v1, "BEGIN_ARRAY"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lx4/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lx4/b;->a:Lx4/b;

    new-instance v1, Lx4/b;

    const-string v3, "END_ARRAY"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lx4/b;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lx4/b;->b:Lx4/b;

    new-instance v3, Lx4/b;

    const-string v5, "BEGIN_OBJECT"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lx4/b;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lx4/b;->c:Lx4/b;

    new-instance v5, Lx4/b;

    const-string v7, "END_OBJECT"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8}, Lx4/b;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lx4/b;->d:Lx4/b;

    new-instance v7, Lx4/b;

    const-string v9, "NAME"

    const/4 v10, 0x4

    invoke-direct {v7, v9, v10}, Lx4/b;-><init>(Ljava/lang/String;I)V

    sput-object v7, Lx4/b;->e:Lx4/b;

    new-instance v9, Lx4/b;

    const-string v11, "STRING"

    const/4 v12, 0x5

    invoke-direct {v9, v11, v12}, Lx4/b;-><init>(Ljava/lang/String;I)V

    sput-object v9, Lx4/b;->f:Lx4/b;

    new-instance v11, Lx4/b;

    const-string v13, "NUMBER"

    const/4 v14, 0x6

    invoke-direct {v11, v13, v14}, Lx4/b;-><init>(Ljava/lang/String;I)V

    sput-object v11, Lx4/b;->g:Lx4/b;

    new-instance v13, Lx4/b;

    const-string v15, "BOOLEAN"

    const/4 v14, 0x7

    invoke-direct {v13, v15, v14}, Lx4/b;-><init>(Ljava/lang/String;I)V

    sput-object v13, Lx4/b;->h:Lx4/b;

    new-instance v15, Lx4/b;

    const-string v14, "NULL"

    const/16 v12, 0x8

    invoke-direct {v15, v14, v12}, Lx4/b;-><init>(Ljava/lang/String;I)V

    sput-object v15, Lx4/b;->i:Lx4/b;

    new-instance v14, Lx4/b;

    const-string v12, "END_DOCUMENT"

    const/16 v10, 0x9

    invoke-direct {v14, v12, v10}, Lx4/b;-><init>(Ljava/lang/String;I)V

    sput-object v14, Lx4/b;->j:Lx4/b;

    const/16 v12, 0xa

    new-array v12, v12, [Lx4/b;

    aput-object v0, v12, v2

    aput-object v1, v12, v4

    aput-object v3, v12, v6

    aput-object v5, v12, v8

    const/4 v0, 0x4

    aput-object v7, v12, v0

    const/4 v0, 0x5

    aput-object v9, v12, v0

    const/4 v0, 0x6

    aput-object v11, v12, v0

    const/4 v0, 0x7

    aput-object v13, v12, v0

    const/16 v0, 0x8

    aput-object v15, v12, v0

    aput-object v14, v12, v10

    sput-object v12, Lx4/b;->k:[Lx4/b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lx4/b;
    .locals 1

    const-class v0, Lx4/b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lx4/b;

    return-object p0
.end method

.method public static values()[Lx4/b;
    .locals 1

    sget-object v0, Lx4/b;->k:[Lx4/b;

    invoke-virtual {v0}, [Lx4/b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lx4/b;

    return-object v0
.end method
