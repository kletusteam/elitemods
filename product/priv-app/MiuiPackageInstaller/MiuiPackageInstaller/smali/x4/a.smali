.class public Lx4/a;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field private final a:Ljava/io/Reader;

.field private b:Z

.field private final c:[C

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field h:I

.field private i:J

.field private j:I

.field private k:Ljava/lang/String;

.field private l:[I

.field private m:I

.field private n:[Ljava/lang/String;

.field private o:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lx4/a$a;

    invoke-direct {v0}, Lx4/a$a;-><init>()V

    sput-object v0, Lr4/f;->a:Lr4/f;

    return-void
.end method

.method public constructor <init>(Ljava/io/Reader;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lx4/a;->b:Z

    const/16 v1, 0x400

    new-array v1, v1, [C

    iput-object v1, p0, Lx4/a;->c:[C

    iput v0, p0, Lx4/a;->d:I

    iput v0, p0, Lx4/a;->e:I

    iput v0, p0, Lx4/a;->f:I

    iput v0, p0, Lx4/a;->g:I

    iput v0, p0, Lx4/a;->h:I

    const/16 v1, 0x20

    new-array v2, v1, [I

    iput-object v2, p0, Lx4/a;->l:[I

    add-int/lit8 v3, v0, 0x1

    iput v3, p0, Lx4/a;->m:I

    const/4 v3, 0x6

    aput v3, v2, v0

    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, Lx4/a;->n:[Ljava/lang/String;

    new-array v0, v1, [I

    iput-object v0, p0, Lx4/a;->o:[I

    const-string v0, "in == null"

    invoke-static {p1, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    iput-object p1, p0, Lx4/a;->a:Ljava/io/Reader;

    return-void
.end method

.method private D(C)Z
    .locals 1

    const/16 v0, 0x9

    if-eq p1, v0, :cond_1

    const/16 v0, 0xa

    if-eq p1, v0, :cond_1

    const/16 v0, 0xc

    if-eq p1, v0, :cond_1

    const/16 v0, 0xd

    if-eq p1, v0, :cond_1

    const/16 v0, 0x20

    if-eq p1, v0, :cond_1

    const/16 v0, 0x23

    if-eq p1, v0, :cond_0

    const/16 v0, 0x2c

    if-eq p1, v0, :cond_1

    const/16 v0, 0x2f

    if-eq p1, v0, :cond_0

    const/16 v0, 0x3d

    if-eq p1, v0, :cond_0

    const/16 v0, 0x7b

    if-eq p1, v0, :cond_1

    const/16 v0, 0x7d

    if-eq p1, v0, :cond_1

    const/16 v0, 0x3a

    if-eq p1, v0, :cond_1

    const/16 v0, 0x3b

    if-eq p1, v0, :cond_0

    packed-switch p1, :pswitch_data_0

    const/4 p1, 0x1

    return p1

    :cond_0
    :pswitch_0
    invoke-direct {p0}, Lx4/a;->m()V

    :cond_1
    :pswitch_1
    const/4 p1, 0x0

    return p1

    :pswitch_data_0
    .packed-switch 0x5b
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private V(Z)I
    .locals 7

    iget-object v0, p0, Lx4/a;->c:[C

    :goto_0
    iget v1, p0, Lx4/a;->d:I

    :goto_1
    iget v2, p0, Lx4/a;->e:I

    :goto_2
    const/4 v3, 0x1

    if-ne v1, v2, :cond_2

    iput v1, p0, Lx4/a;->d:I

    invoke-direct {p0, v3}, Lx4/a;->y(I)Z

    move-result v1

    if-nez v1, :cond_1

    if-nez p1, :cond_0

    const/4 p1, -0x1

    return p1

    :cond_0
    new-instance p1, Ljava/io/EOFException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "End of input"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lx4/a;->F()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    iget v1, p0, Lx4/a;->d:I

    iget v2, p0, Lx4/a;->e:I

    :cond_2
    add-int/lit8 v4, v1, 0x1

    aget-char v1, v0, v1

    const/16 v5, 0xa

    if-ne v1, v5, :cond_3

    iget v1, p0, Lx4/a;->f:I

    add-int/2addr v1, v3

    iput v1, p0, Lx4/a;->f:I

    iput v4, p0, Lx4/a;->g:I

    goto :goto_4

    :cond_3
    const/16 v5, 0x20

    if-eq v1, v5, :cond_b

    const/16 v5, 0xd

    if-eq v1, v5, :cond_b

    const/16 v5, 0x9

    if-ne v1, v5, :cond_4

    goto :goto_4

    :cond_4
    const/16 v5, 0x2f

    if-ne v1, v5, :cond_9

    iput v4, p0, Lx4/a;->d:I

    const/4 v6, 0x2

    if-ne v4, v2, :cond_5

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Lx4/a;->d:I

    invoke-direct {p0, v6}, Lx4/a;->y(I)Z

    move-result v2

    iget v4, p0, Lx4/a;->d:I

    add-int/2addr v4, v3

    iput v4, p0, Lx4/a;->d:I

    if-nez v2, :cond_5

    return v1

    :cond_5
    invoke-direct {p0}, Lx4/a;->m()V

    iget v2, p0, Lx4/a;->d:I

    aget-char v3, v0, v2

    const/16 v4, 0x2a

    if-eq v3, v4, :cond_7

    if-eq v3, v5, :cond_6

    return v1

    :cond_6
    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lx4/a;->d:I

    :goto_3
    invoke-direct {p0}, Lx4/a;->i0()V

    goto :goto_0

    :cond_7
    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lx4/a;->d:I

    const-string v1, "*/"

    invoke-direct {p0, v1}, Lx4/a;->h0(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    iget v1, p0, Lx4/a;->d:I

    add-int/2addr v1, v6

    goto/16 :goto_1

    :cond_8
    const-string p1, "Unterminated comment"

    invoke-direct {p0, p1}, Lx4/a;->l0(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object p1

    throw p1

    :cond_9
    const/16 v2, 0x23

    iput v4, p0, Lx4/a;->d:I

    if-ne v1, v2, :cond_a

    invoke-direct {p0}, Lx4/a;->m()V

    goto :goto_3

    :cond_a
    return v1

    :cond_b
    :goto_4
    move v1, v4

    goto/16 :goto_2
.end method

.method private X(C)Ljava/lang/String;
    .locals 9

    iget-object v0, p0, Lx4/a;->c:[C

    const/4 v1, 0x0

    :goto_0
    iget v2, p0, Lx4/a;->d:I

    iget v3, p0, Lx4/a;->e:I

    move v4, v3

    move v3, v2

    :goto_1
    const/16 v5, 0x10

    const/4 v6, 0x1

    if-ge v2, v4, :cond_5

    add-int/lit8 v7, v2, 0x1

    aget-char v2, v0, v2

    if-ne v2, p1, :cond_1

    iput v7, p0, Lx4/a;->d:I

    sub-int/2addr v7, v3

    sub-int/2addr v7, v6

    if-nez v1, :cond_0

    new-instance p1, Ljava/lang/String;

    invoke-direct {p1, v0, v3, v7}, Ljava/lang/String;-><init>([CII)V

    return-object p1

    :cond_0
    invoke-virtual {v1, v0, v3, v7}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_1
    const/16 v8, 0x5c

    if-ne v2, v8, :cond_3

    iput v7, p0, Lx4/a;->d:I

    sub-int/2addr v7, v3

    sub-int/2addr v7, v6

    if-nez v1, :cond_2

    add-int/lit8 v1, v7, 0x1

    mul-int/lit8 v1, v1, 0x2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1, v5}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    move-object v1, v2

    :cond_2
    invoke-virtual {v1, v0, v3, v7}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    invoke-direct {p0}, Lx4/a;->e0()C

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_3
    const/16 v5, 0xa

    if-ne v2, v5, :cond_4

    iget v2, p0, Lx4/a;->f:I

    add-int/2addr v2, v6

    iput v2, p0, Lx4/a;->f:I

    iput v7, p0, Lx4/a;->g:I

    :cond_4
    move v2, v7

    goto :goto_1

    :cond_5
    if-nez v1, :cond_6

    sub-int v1, v2, v3

    mul-int/lit8 v1, v1, 0x2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v1, v5}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-direct {v4, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    move-object v1, v4

    :cond_6
    sub-int v4, v2, v3

    invoke-virtual {v1, v0, v3, v4}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    iput v2, p0, Lx4/a;->d:I

    invoke-direct {p0, v6}, Lx4/a;->y(I)Z

    move-result v2

    if-eqz v2, :cond_7

    goto :goto_0

    :cond_7
    const-string p1, "Unterminated string"

    invoke-direct {p0, p1}, Lx4/a;->l0(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object p1

    throw p1
.end method

.method private Z()Ljava/lang/String;
    .locals 6

    const/4 v0, 0x0

    const/4 v1, 0x0

    :cond_0
    move v2, v0

    :goto_0
    iget v3, p0, Lx4/a;->d:I

    add-int v4, v3, v2

    iget v5, p0, Lx4/a;->e:I

    if-ge v4, v5, :cond_2

    iget-object v4, p0, Lx4/a;->c:[C

    add-int/2addr v3, v2

    aget-char v3, v4, v3

    const/16 v4, 0x9

    if-eq v3, v4, :cond_3

    const/16 v4, 0xa

    if-eq v3, v4, :cond_3

    const/16 v4, 0xc

    if-eq v3, v4, :cond_3

    const/16 v4, 0xd

    if-eq v3, v4, :cond_3

    const/16 v4, 0x20

    if-eq v3, v4, :cond_3

    const/16 v4, 0x23

    if-eq v3, v4, :cond_1

    const/16 v4, 0x2c

    if-eq v3, v4, :cond_3

    const/16 v4, 0x2f

    if-eq v3, v4, :cond_1

    const/16 v4, 0x3d

    if-eq v3, v4, :cond_1

    const/16 v4, 0x7b

    if-eq v3, v4, :cond_3

    const/16 v4, 0x7d

    if-eq v3, v4, :cond_3

    const/16 v4, 0x3a

    if-eq v3, v4, :cond_3

    const/16 v4, 0x3b

    if-eq v3, v4, :cond_1

    packed-switch v3, :pswitch_data_0

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    :pswitch_0
    invoke-direct {p0}, Lx4/a;->m()V

    goto :goto_1

    :cond_2
    iget-object v3, p0, Lx4/a;->c:[C

    array-length v3, v3

    if-ge v2, v3, :cond_4

    add-int/lit8 v3, v2, 0x1

    invoke-direct {p0, v3}, Lx4/a;->y(I)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_0

    :cond_3
    :goto_1
    :pswitch_1
    move v0, v2

    goto :goto_2

    :cond_4
    if-nez v1, :cond_5

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v3, 0x10

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    :cond_5
    iget-object v3, p0, Lx4/a;->c:[C

    iget v4, p0, Lx4/a;->d:I

    invoke-virtual {v1, v3, v4, v2}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    iget v3, p0, Lx4/a;->d:I

    add-int/2addr v3, v2

    iput v3, p0, Lx4/a;->d:I

    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lx4/a;->y(I)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_2
    if-nez v1, :cond_6

    new-instance v1, Ljava/lang/String;

    iget-object v2, p0, Lx4/a;->c:[C

    iget v3, p0, Lx4/a;->d:I

    invoke-direct {v1, v2, v3, v0}, Ljava/lang/String;-><init>([CII)V

    goto :goto_3

    :cond_6
    iget-object v2, p0, Lx4/a;->c:[C

    iget v3, p0, Lx4/a;->d:I

    invoke-virtual {v1, v2, v3, v0}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_3
    iget v2, p0, Lx4/a;->d:I

    add-int/2addr v2, v0

    iput v2, p0, Lx4/a;->d:I

    return-object v1

    nop

    :pswitch_data_0
    .packed-switch 0x5b
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private b0()I
    .locals 8

    iget-object v0, p0, Lx4/a;->c:[C

    iget v1, p0, Lx4/a;->d:I

    aget-char v0, v0, v1

    const/4 v1, 0x0

    const/16 v2, 0x74

    if-eq v0, v2, :cond_5

    const/16 v2, 0x54

    if-ne v0, v2, :cond_0

    goto :goto_2

    :cond_0
    const/16 v2, 0x66

    if-eq v0, v2, :cond_4

    const/16 v2, 0x46

    if-ne v0, v2, :cond_1

    goto :goto_1

    :cond_1
    const/16 v2, 0x6e

    if-eq v0, v2, :cond_3

    const/16 v2, 0x4e

    if-ne v0, v2, :cond_2

    goto :goto_0

    :cond_2
    return v1

    :cond_3
    :goto_0
    const/4 v0, 0x7

    const-string v2, "null"

    const-string v3, "NULL"

    goto :goto_3

    :cond_4
    :goto_1
    const/4 v0, 0x6

    const-string v2, "false"

    const-string v3, "FALSE"

    goto :goto_3

    :cond_5
    :goto_2
    const/4 v0, 0x5

    const-string v2, "true"

    const-string v3, "TRUE"

    :goto_3
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    const/4 v5, 0x1

    :goto_4
    if-ge v5, v4, :cond_8

    iget v6, p0, Lx4/a;->d:I

    add-int/2addr v6, v5

    iget v7, p0, Lx4/a;->e:I

    if-lt v6, v7, :cond_6

    add-int/lit8 v6, v5, 0x1

    invoke-direct {p0, v6}, Lx4/a;->y(I)Z

    move-result v6

    if-nez v6, :cond_6

    return v1

    :cond_6
    iget-object v6, p0, Lx4/a;->c:[C

    iget v7, p0, Lx4/a;->d:I

    add-int/2addr v7, v5

    aget-char v6, v6, v7

    invoke-virtual {v2, v5}, Ljava/lang/String;->charAt(I)C

    move-result v7

    if-eq v6, v7, :cond_7

    invoke-virtual {v3, v5}, Ljava/lang/String;->charAt(I)C

    move-result v7

    if-eq v6, v7, :cond_7

    return v1

    :cond_7
    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    :cond_8
    iget v2, p0, Lx4/a;->d:I

    add-int/2addr v2, v4

    iget v3, p0, Lx4/a;->e:I

    if-lt v2, v3, :cond_9

    add-int/lit8 v2, v4, 0x1

    invoke-direct {p0, v2}, Lx4/a;->y(I)Z

    move-result v2

    if-eqz v2, :cond_a

    :cond_9
    iget-object v2, p0, Lx4/a;->c:[C

    iget v3, p0, Lx4/a;->d:I

    add-int/2addr v3, v4

    aget-char v2, v2, v3

    invoke-direct {p0, v2}, Lx4/a;->D(C)Z

    move-result v2

    if-eqz v2, :cond_a

    return v1

    :cond_a
    iget v1, p0, Lx4/a;->d:I

    add-int/2addr v1, v4

    iput v1, p0, Lx4/a;->d:I

    iput v0, p0, Lx4/a;->h:I

    return v0
.end method

.method private c0()I
    .locals 18

    move-object/from16 v0, p0

    iget-object v1, v0, Lx4/a;->c:[C

    iget v2, v0, Lx4/a;->d:I

    iget v3, v0, Lx4/a;->e:I

    const/4 v6, 0x1

    const/4 v7, 0x0

    move v10, v6

    move v8, v7

    move v9, v8

    move v13, v9

    const-wide/16 v11, 0x0

    :goto_0
    add-int v14, v2, v8

    const/4 v15, 0x2

    if-ne v14, v3, :cond_2

    array-length v2, v1

    if-ne v8, v2, :cond_0

    return v7

    :cond_0
    add-int/lit8 v2, v8, 0x1

    invoke-direct {v0, v2}, Lx4/a;->y(I)Z

    move-result v2

    if-nez v2, :cond_1

    goto/16 :goto_7

    :cond_1
    iget v2, v0, Lx4/a;->d:I

    iget v3, v0, Lx4/a;->e:I

    :cond_2
    add-int v14, v2, v8

    aget-char v14, v1, v14

    const/16 v7, 0x2b

    const/4 v4, 0x3

    const/4 v5, 0x5

    if-eq v14, v7, :cond_1d

    const/16 v7, 0x45

    if-eq v14, v7, :cond_1a

    const/16 v7, 0x65

    if-eq v14, v7, :cond_1a

    const/16 v7, 0x2d

    if-eq v14, v7, :cond_17

    const/16 v7, 0x2e

    if-eq v14, v7, :cond_15

    const/16 v7, 0x30

    if-lt v14, v7, :cond_d

    const/16 v7, 0x39

    if-le v14, v7, :cond_3

    goto :goto_6

    :cond_3
    if-eq v9, v6, :cond_b

    if-nez v9, :cond_4

    goto :goto_3

    :cond_4
    if-ne v9, v15, :cond_8

    const-wide/16 v16, 0x0

    cmp-long v4, v11, v16

    if-nez v4, :cond_5

    const/4 v4, 0x0

    return v4

    :cond_5
    const-wide/16 v4, 0xa

    mul-long/2addr v4, v11

    add-int/lit8 v14, v14, -0x30

    int-to-long v14, v14

    sub-long/2addr v4, v14

    const-wide v14, -0xcccccccccccccccL

    cmp-long v7, v11, v14

    if-gtz v7, :cond_7

    if-nez v7, :cond_6

    cmp-long v7, v4, v11

    if-gez v7, :cond_6

    goto :goto_1

    :cond_6
    const/4 v7, 0x0

    goto :goto_2

    :cond_7
    :goto_1
    move v7, v6

    :goto_2
    and-int/2addr v10, v7

    move-wide v11, v4

    goto :goto_4

    :cond_8
    if-ne v9, v4, :cond_9

    const/4 v7, 0x0

    const/4 v9, 0x4

    goto :goto_5

    :cond_9
    if-eq v9, v5, :cond_a

    const/4 v4, 0x6

    if-ne v9, v4, :cond_c

    :cond_a
    const/4 v7, 0x0

    const/4 v9, 0x7

    goto :goto_5

    :cond_b
    :goto_3
    add-int/lit8 v14, v14, -0x30

    neg-int v4, v14

    int-to-long v11, v4

    move v9, v15

    :cond_c
    :goto_4
    const/4 v7, 0x0

    :goto_5
    const-wide/16 v16, 0x0

    goto/16 :goto_d

    :cond_d
    :goto_6
    invoke-direct {v0, v14}, Lx4/a;->D(C)Z

    move-result v1

    if-nez v1, :cond_14

    :goto_7
    if-ne v9, v15, :cond_11

    if-eqz v10, :cond_11

    const-wide/high16 v1, -0x8000000000000000L

    cmp-long v1, v11, v1

    if-nez v1, :cond_e

    if-eqz v13, :cond_11

    :cond_e
    const-wide/16 v16, 0x0

    cmp-long v1, v11, v16

    if-nez v1, :cond_f

    if-nez v13, :cond_11

    :cond_f
    if-eqz v13, :cond_10

    goto :goto_8

    :cond_10
    neg-long v11, v11

    :goto_8
    iput-wide v11, v0, Lx4/a;->i:J

    iget v1, v0, Lx4/a;->d:I

    add-int/2addr v1, v8

    iput v1, v0, Lx4/a;->d:I

    const/16 v1, 0xf

    :goto_9
    iput v1, v0, Lx4/a;->h:I

    return v1

    :cond_11
    if-eq v9, v15, :cond_13

    const/4 v1, 0x4

    if-eq v9, v1, :cond_13

    const/4 v1, 0x7

    if-ne v9, v1, :cond_12

    goto :goto_a

    :cond_12
    const/4 v7, 0x0

    return v7

    :cond_13
    :goto_a
    iput v8, v0, Lx4/a;->j:I

    const/16 v1, 0x10

    goto :goto_9

    :cond_14
    const/4 v7, 0x0

    return v7

    :cond_15
    const/4 v7, 0x0

    const-wide/16 v16, 0x0

    if-ne v9, v15, :cond_16

    goto :goto_c

    :cond_16
    return v7

    :cond_17
    const/4 v4, 0x6

    const/4 v7, 0x0

    const-wide/16 v16, 0x0

    if-nez v9, :cond_18

    move v9, v6

    move v13, v9

    goto :goto_d

    :cond_18
    if-ne v9, v5, :cond_19

    goto :goto_c

    :cond_19
    return v7

    :cond_1a
    const/4 v7, 0x0

    const-wide/16 v16, 0x0

    if-eq v9, v15, :cond_1c

    const/4 v4, 0x4

    if-ne v9, v4, :cond_1b

    goto :goto_b

    :cond_1b
    return v7

    :cond_1c
    :goto_b
    move v9, v5

    goto :goto_d

    :cond_1d
    const/4 v4, 0x6

    const/4 v7, 0x0

    const-wide/16 v16, 0x0

    if-ne v9, v5, :cond_1e

    :goto_c
    move v9, v4

    :goto_d
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_0

    :cond_1e
    return v7
.end method

.method private d0(I)V
    .locals 3

    iget v0, p0, Lx4/a;->m:I

    iget-object v1, p0, Lx4/a;->l:[I

    array-length v2, v1

    if-ne v0, v2, :cond_0

    mul-int/lit8 v0, v0, 0x2

    invoke-static {v1, v0}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v1

    iput-object v1, p0, Lx4/a;->l:[I

    iget-object v1, p0, Lx4/a;->o:[I

    invoke-static {v1, v0}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v1

    iput-object v1, p0, Lx4/a;->o:[I

    iget-object v1, p0, Lx4/a;->n:[Ljava/lang/String;

    invoke-static {v1, v0}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, p0, Lx4/a;->n:[Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lx4/a;->l:[I

    iget v1, p0, Lx4/a;->m:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lx4/a;->m:I

    aput p1, v0, v1

    return-void
.end method

.method private e0()C
    .locals 8

    iget v0, p0, Lx4/a;->d:I

    iget v1, p0, Lx4/a;->e:I

    const-string v2, "Unterminated escape sequence"

    const/4 v3, 0x1

    if-ne v0, v1, :cond_1

    invoke-direct {p0, v3}, Lx4/a;->y(I)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-direct {p0, v2}, Lx4/a;->l0(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_1
    :goto_0
    iget-object v0, p0, Lx4/a;->c:[C

    iget v1, p0, Lx4/a;->d:I

    add-int/lit8 v4, v1, 0x1

    iput v4, p0, Lx4/a;->d:I

    aget-char v0, v0, v1

    const/16 v1, 0xa

    if-eq v0, v1, :cond_e

    const/16 v3, 0x22

    if-eq v0, v3, :cond_f

    const/16 v3, 0x27

    if-eq v0, v3, :cond_f

    const/16 v3, 0x2f

    if-eq v0, v3, :cond_f

    const/16 v3, 0x5c

    if-eq v0, v3, :cond_f

    const/16 v3, 0x62

    if-eq v0, v3, :cond_d

    const/16 v3, 0x66

    if-eq v0, v3, :cond_c

    const/16 v5, 0x6e

    if-eq v0, v5, :cond_b

    const/16 v5, 0x72

    if-eq v0, v5, :cond_a

    const/16 v5, 0x74

    if-eq v0, v5, :cond_9

    const/16 v5, 0x75

    if-ne v0, v5, :cond_8

    const/4 v0, 0x4

    add-int/2addr v4, v0

    iget v5, p0, Lx4/a;->e:I

    if-le v4, v5, :cond_3

    invoke-direct {p0, v0}, Lx4/a;->y(I)Z

    move-result v4

    if-eqz v4, :cond_2

    goto :goto_1

    :cond_2
    invoke-direct {p0, v2}, Lx4/a;->l0(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_3
    :goto_1
    const/4 v2, 0x0

    iget v4, p0, Lx4/a;->d:I

    add-int/lit8 v5, v4, 0x4

    :goto_2
    if-ge v4, v5, :cond_7

    iget-object v6, p0, Lx4/a;->c:[C

    aget-char v6, v6, v4

    shl-int/lit8 v2, v2, 0x4

    int-to-char v2, v2

    const/16 v7, 0x30

    if-lt v6, v7, :cond_4

    const/16 v7, 0x39

    if-gt v6, v7, :cond_4

    add-int/lit8 v6, v6, -0x30

    :goto_3
    add-int/2addr v2, v6

    int-to-char v2, v2

    goto :goto_5

    :cond_4
    const/16 v7, 0x61

    if-lt v6, v7, :cond_5

    if-gt v6, v3, :cond_5

    add-int/lit8 v6, v6, -0x61

    :goto_4
    add-int/2addr v6, v1

    goto :goto_3

    :cond_5
    const/16 v7, 0x41

    if-lt v6, v7, :cond_6

    const/16 v7, 0x46

    if-gt v6, v7, :cond_6

    add-int/lit8 v6, v6, -0x41

    goto :goto_4

    :goto_5
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_6
    new-instance v1, Ljava/lang/NumberFormatException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\\u"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v3, Ljava/lang/String;

    iget-object v4, p0, Lx4/a;->c:[C

    iget v5, p0, Lx4/a;->d:I

    invoke-direct {v3, v4, v5, v0}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_7
    iget v1, p0, Lx4/a;->d:I

    add-int/2addr v1, v0

    iput v1, p0, Lx4/a;->d:I

    return v2

    :cond_8
    const-string v0, "Invalid escape sequence"

    invoke-direct {p0, v0}, Lx4/a;->l0(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_9
    const/16 v0, 0x9

    return v0

    :cond_a
    const/16 v0, 0xd

    return v0

    :cond_b
    return v1

    :cond_c
    const/16 v0, 0xc

    return v0

    :cond_d
    const/16 v0, 0x8

    return v0

    :cond_e
    iget v1, p0, Lx4/a;->f:I

    add-int/2addr v1, v3

    iput v1, p0, Lx4/a;->f:I

    iput v4, p0, Lx4/a;->g:I

    :cond_f
    return v0
.end method

.method private g0(C)V
    .locals 6

    iget-object v0, p0, Lx4/a;->c:[C

    :goto_0
    iget v1, p0, Lx4/a;->d:I

    iget v2, p0, Lx4/a;->e:I

    :goto_1
    const/4 v3, 0x1

    if-ge v1, v2, :cond_3

    add-int/lit8 v4, v1, 0x1

    aget-char v1, v0, v1

    if-ne v1, p1, :cond_0

    iput v4, p0, Lx4/a;->d:I

    return-void

    :cond_0
    const/16 v5, 0x5c

    if-ne v1, v5, :cond_1

    iput v4, p0, Lx4/a;->d:I

    invoke-direct {p0}, Lx4/a;->e0()C

    goto :goto_0

    :cond_1
    const/16 v5, 0xa

    if-ne v1, v5, :cond_2

    iget v1, p0, Lx4/a;->f:I

    add-int/2addr v1, v3

    iput v1, p0, Lx4/a;->f:I

    iput v4, p0, Lx4/a;->g:I

    :cond_2
    move v1, v4

    goto :goto_1

    :cond_3
    iput v1, p0, Lx4/a;->d:I

    invoke-direct {p0, v3}, Lx4/a;->y(I)Z

    move-result v1

    if-eqz v1, :cond_4

    goto :goto_0

    :cond_4
    const-string p1, "Unterminated string"

    invoke-direct {p0, p1}, Lx4/a;->l0(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object p1

    throw p1
.end method

.method private h0(Ljava/lang/String;)Z
    .locals 6

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    :goto_0
    iget v1, p0, Lx4/a;->d:I

    add-int/2addr v1, v0

    iget v2, p0, Lx4/a;->e:I

    const/4 v3, 0x0

    if-le v1, v2, :cond_1

    invoke-direct {p0, v0}, Lx4/a;->y(I)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_1

    :cond_0
    return v3

    :cond_1
    :goto_1
    iget-object v1, p0, Lx4/a;->c:[C

    iget v2, p0, Lx4/a;->d:I

    aget-char v1, v1, v2

    const/16 v4, 0xa

    const/4 v5, 0x1

    if-ne v1, v4, :cond_2

    iget v1, p0, Lx4/a;->f:I

    add-int/2addr v1, v5

    iput v1, p0, Lx4/a;->f:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lx4/a;->g:I

    goto :goto_3

    :cond_2
    :goto_2
    if-ge v3, v0, :cond_4

    iget-object v1, p0, Lx4/a;->c:[C

    iget v2, p0, Lx4/a;->d:I

    add-int/2addr v2, v3

    aget-char v1, v1, v2

    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v2

    if-eq v1, v2, :cond_3

    :goto_3
    iget v1, p0, Lx4/a;->d:I

    add-int/2addr v1, v5

    iput v1, p0, Lx4/a;->d:I

    goto :goto_0

    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_4
    return v5
.end method

.method private i0()V
    .locals 4

    :cond_0
    iget v0, p0, Lx4/a;->d:I

    iget v1, p0, Lx4/a;->e:I

    const/4 v2, 0x1

    if-lt v0, v1, :cond_1

    invoke-direct {p0, v2}, Lx4/a;->y(I)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_1
    iget-object v0, p0, Lx4/a;->c:[C

    iget v1, p0, Lx4/a;->d:I

    add-int/lit8 v3, v1, 0x1

    iput v3, p0, Lx4/a;->d:I

    aget-char v0, v0, v1

    const/16 v1, 0xa

    if-ne v0, v1, :cond_2

    iget v0, p0, Lx4/a;->f:I

    add-int/2addr v0, v2

    iput v0, p0, Lx4/a;->f:I

    iput v3, p0, Lx4/a;->g:I

    goto :goto_0

    :cond_2
    const/16 v1, 0xd

    if-ne v0, v1, :cond_0

    :cond_3
    :goto_0
    return-void
.end method

.method private j0()V
    .locals 4

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lx4/a;->d:I

    add-int v2, v1, v0

    iget v3, p0, Lx4/a;->e:I

    if-ge v2, v3, :cond_3

    iget-object v2, p0, Lx4/a;->c:[C

    add-int/2addr v1, v0

    aget-char v1, v2, v1

    const/16 v2, 0x9

    if-eq v1, v2, :cond_2

    const/16 v2, 0xa

    if-eq v1, v2, :cond_2

    const/16 v2, 0xc

    if-eq v1, v2, :cond_2

    const/16 v2, 0xd

    if-eq v1, v2, :cond_2

    const/16 v2, 0x20

    if-eq v1, v2, :cond_2

    const/16 v2, 0x23

    if-eq v1, v2, :cond_1

    const/16 v2, 0x2c

    if-eq v1, v2, :cond_2

    const/16 v2, 0x2f

    if-eq v1, v2, :cond_1

    const/16 v2, 0x3d

    if-eq v1, v2, :cond_1

    const/16 v2, 0x7b

    if-eq v1, v2, :cond_2

    const/16 v2, 0x7d

    if-eq v1, v2, :cond_2

    const/16 v2, 0x3a

    if-eq v1, v2, :cond_2

    const/16 v2, 0x3b

    if-eq v1, v2, :cond_1

    packed-switch v1, :pswitch_data_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    :pswitch_0
    invoke-direct {p0}, Lx4/a;->m()V

    :cond_2
    :pswitch_1
    iget v1, p0, Lx4/a;->d:I

    add-int/2addr v1, v0

    iput v1, p0, Lx4/a;->d:I

    return-void

    :cond_3
    add-int/2addr v1, v0

    iput v1, p0, Lx4/a;->d:I

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lx4/a;->y(I)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :pswitch_data_0
    .packed-switch 0x5b
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private l0(Ljava/lang/String;)Ljava/io/IOException;
    .locals 2

    new-instance v0, Lx4/d;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lx4/a;->F()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lx4/d;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private m()V
    .locals 1

    iget-boolean v0, p0, Lx4/a;->b:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const-string v0, "Use JsonReader.setLenient(true) to accept malformed JSON"

    invoke-direct {p0, v0}, Lx4/a;->l0(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0
.end method

.method private n()V
    .locals 5

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lx4/a;->V(Z)I

    iget v1, p0, Lx4/a;->d:I

    sub-int/2addr v1, v0

    iput v1, p0, Lx4/a;->d:I

    add-int/lit8 v0, v1, 0x5

    iget v2, p0, Lx4/a;->e:I

    const/4 v3, 0x5

    if-le v0, v2, :cond_0

    invoke-direct {p0, v3}, Lx4/a;->y(I)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lx4/a;->c:[C

    aget-char v2, v0, v1

    const/16 v4, 0x29

    if-ne v2, v4, :cond_2

    add-int/lit8 v2, v1, 0x1

    aget-char v2, v0, v2

    const/16 v4, 0x5d

    if-ne v2, v4, :cond_2

    add-int/lit8 v2, v1, 0x2

    aget-char v2, v0, v2

    const/16 v4, 0x7d

    if-ne v2, v4, :cond_2

    add-int/lit8 v2, v1, 0x3

    aget-char v2, v0, v2

    const/16 v4, 0x27

    if-ne v2, v4, :cond_2

    add-int/lit8 v1, v1, 0x4

    aget-char v0, v0, v1

    const/16 v1, 0xa

    if-eq v0, v1, :cond_1

    goto :goto_0

    :cond_1
    iget v0, p0, Lx4/a;->d:I

    add-int/2addr v0, v3

    iput v0, p0, Lx4/a;->d:I

    :cond_2
    :goto_0
    return-void
.end method

.method private y(I)Z
    .locals 7

    iget-object v0, p0, Lx4/a;->c:[C

    iget v1, p0, Lx4/a;->g:I

    iget v2, p0, Lx4/a;->d:I

    sub-int/2addr v1, v2

    iput v1, p0, Lx4/a;->g:I

    iget v1, p0, Lx4/a;->e:I

    const/4 v3, 0x0

    if-eq v1, v2, :cond_0

    sub-int/2addr v1, v2

    iput v1, p0, Lx4/a;->e:I

    invoke-static {v0, v2, v0, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0

    :cond_0
    iput v3, p0, Lx4/a;->e:I

    :goto_0
    iput v3, p0, Lx4/a;->d:I

    :cond_1
    iget-object v1, p0, Lx4/a;->a:Ljava/io/Reader;

    iget v2, p0, Lx4/a;->e:I

    array-length v4, v0

    sub-int/2addr v4, v2

    invoke-virtual {v1, v0, v2, v4}, Ljava/io/Reader;->read([CII)I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_3

    iget v2, p0, Lx4/a;->e:I

    add-int/2addr v2, v1

    iput v2, p0, Lx4/a;->e:I

    iget v1, p0, Lx4/a;->f:I

    const/4 v4, 0x1

    if-nez v1, :cond_2

    iget v1, p0, Lx4/a;->g:I

    if-nez v1, :cond_2

    if-lez v2, :cond_2

    aget-char v5, v0, v3

    const v6, 0xfeff

    if-ne v5, v6, :cond_2

    iget v5, p0, Lx4/a;->d:I

    add-int/2addr v5, v4

    iput v5, p0, Lx4/a;->d:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lx4/a;->g:I

    add-int/lit8 p1, p1, 0x1

    :cond_2
    if-lt v2, p1, :cond_1

    return v4

    :cond_3
    return v3
.end method


# virtual methods
.method public B()Z
    .locals 2

    iget v0, p0, Lx4/a;->h:I

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lx4/a;->p()I

    move-result v0

    :cond_0
    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final C()Z
    .locals 1

    iget-boolean v0, p0, Lx4/a;->b:Z

    return v0
.end method

.method F()Ljava/lang/String;
    .locals 4

    goto/32 :goto_2

    nop

    :goto_0
    const-string v3, " at line "

    goto/32 :goto_10

    nop

    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_6

    nop

    :goto_2
    iget v0, p0, Lx4/a;->f:I

    goto/32 :goto_5

    nop

    :goto_3
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_0

    nop

    :goto_4
    invoke-virtual {p0}, Lx4/a;->z()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_11

    nop

    :goto_5
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_a

    nop

    :goto_6
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/32 :goto_f

    nop

    :goto_7
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_4

    nop

    :goto_8
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/32 :goto_c

    nop

    :goto_9
    iget v2, p0, Lx4/a;->g:I

    goto/32 :goto_12

    nop

    :goto_a
    iget v1, p0, Lx4/a;->d:I

    goto/32 :goto_9

    nop

    :goto_b
    return-object v0

    :goto_c
    const-string v0, " column "

    goto/32 :goto_1

    nop

    :goto_d
    new-instance v2, Ljava/lang/StringBuilder;

    goto/32 :goto_3

    nop

    :goto_e
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_d

    nop

    :goto_f
    const-string v0, " path "

    goto/32 :goto_7

    nop

    :goto_10
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_8

    nop

    :goto_11
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_13

    nop

    :goto_12
    sub-int/2addr v1, v2

    goto/32 :goto_e

    nop

    :goto_13
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_b

    nop
.end method

.method public J()Z
    .locals 5

    iget v0, p0, Lx4/a;->h:I

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lx4/a;->p()I

    move-result v0

    :cond_0
    const/4 v1, 0x5

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-ne v0, v1, :cond_1

    iput v2, p0, Lx4/a;->h:I

    iget-object v0, p0, Lx4/a;->o:[I

    iget v1, p0, Lx4/a;->m:I

    sub-int/2addr v1, v3

    aget v2, v0, v1

    add-int/2addr v2, v3

    aput v2, v0, v1

    return v3

    :cond_1
    const/4 v1, 0x6

    if-ne v0, v1, :cond_2

    iput v2, p0, Lx4/a;->h:I

    iget-object v0, p0, Lx4/a;->o:[I

    iget v1, p0, Lx4/a;->m:I

    sub-int/2addr v1, v3

    aget v4, v0, v1

    add-int/2addr v4, v3

    aput v4, v0, v1

    return v2

    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected a boolean but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lx4/a;->a0()Lx4/b;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lx4/a;->F()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public K()D
    .locals 6

    iget v0, p0, Lx4/a;->h:I

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lx4/a;->p()I

    move-result v0

    :cond_0
    const/16 v1, 0xf

    const/4 v2, 0x0

    if-ne v0, v1, :cond_1

    iput v2, p0, Lx4/a;->h:I

    iget-object v0, p0, Lx4/a;->o:[I

    iget v1, p0, Lx4/a;->m:I

    add-int/lit8 v1, v1, -0x1

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    iget-wide v0, p0, Lx4/a;->i:J

    long-to-double v0, v0

    return-wide v0

    :cond_1
    const/16 v1, 0x10

    const/16 v3, 0xb

    if-ne v0, v1, :cond_2

    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lx4/a;->c:[C

    iget v4, p0, Lx4/a;->d:I

    iget v5, p0, Lx4/a;->j:I

    invoke-direct {v0, v1, v4, v5}, Ljava/lang/String;-><init>([CII)V

    iput-object v0, p0, Lx4/a;->k:Ljava/lang/String;

    iget v0, p0, Lx4/a;->d:I

    iget v1, p0, Lx4/a;->j:I

    add-int/2addr v0, v1

    iput v0, p0, Lx4/a;->d:I

    goto :goto_3

    :cond_2
    const/16 v1, 0x8

    if-eq v0, v1, :cond_6

    const/16 v4, 0x9

    if-ne v0, v4, :cond_3

    goto :goto_0

    :cond_3
    const/16 v1, 0xa

    if-ne v0, v1, :cond_4

    invoke-direct {p0}, Lx4/a;->Z()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_4
    if-ne v0, v3, :cond_5

    goto :goto_3

    :cond_5
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected a double but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lx4/a;->a0()Lx4/b;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lx4/a;->F()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    :goto_0
    if-ne v0, v1, :cond_7

    const/16 v0, 0x27

    goto :goto_1

    :cond_7
    const/16 v0, 0x22

    :goto_1
    invoke-direct {p0, v0}, Lx4/a;->X(C)Ljava/lang/String;

    move-result-object v0

    :goto_2
    iput-object v0, p0, Lx4/a;->k:Ljava/lang/String;

    :goto_3
    iput v3, p0, Lx4/a;->h:I

    iget-object v0, p0, Lx4/a;->k:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    iget-boolean v3, p0, Lx4/a;->b:Z

    if-nez v3, :cond_9

    invoke-static {v0, v1}, Ljava/lang/Double;->isNaN(D)Z

    move-result v3

    if-nez v3, :cond_8

    invoke-static {v0, v1}, Ljava/lang/Double;->isInfinite(D)Z

    move-result v3

    if-nez v3, :cond_8

    goto :goto_4

    :cond_8
    new-instance v2, Lx4/d;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "JSON forbids NaN and infinities: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lx4/a;->F()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lx4/d;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_9
    :goto_4
    const/4 v3, 0x0

    iput-object v3, p0, Lx4/a;->k:Ljava/lang/String;

    iput v2, p0, Lx4/a;->h:I

    iget-object v2, p0, Lx4/a;->o:[I

    iget v3, p0, Lx4/a;->m:I

    add-int/lit8 v3, v3, -0x1

    aget v4, v2, v3

    add-int/lit8 v4, v4, 0x1

    aput v4, v2, v3

    return-wide v0
.end method

.method public L()I
    .locals 7

    iget v0, p0, Lx4/a;->h:I

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lx4/a;->p()I

    move-result v0

    :cond_0
    const/16 v1, 0xf

    const-string v2, "Expected an int but was "

    const/4 v3, 0x0

    if-ne v0, v1, :cond_2

    iget-wide v0, p0, Lx4/a;->i:J

    long-to-int v4, v0

    int-to-long v5, v4

    cmp-long v0, v0, v5

    if-nez v0, :cond_1

    iput v3, p0, Lx4/a;->h:I

    iget-object v0, p0, Lx4/a;->o:[I

    iget v1, p0, Lx4/a;->m:I

    add-int/lit8 v1, v1, -0x1

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    return v4

    :cond_1
    new-instance v0, Ljava/lang/NumberFormatException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, p0, Lx4/a;->i:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lx4/a;->F()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    const/16 v1, 0x10

    if-ne v0, v1, :cond_3

    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lx4/a;->c:[C

    iget v4, p0, Lx4/a;->d:I

    iget v5, p0, Lx4/a;->j:I

    invoke-direct {v0, v1, v4, v5}, Ljava/lang/String;-><init>([CII)V

    iput-object v0, p0, Lx4/a;->k:Ljava/lang/String;

    iget v0, p0, Lx4/a;->d:I

    iget v1, p0, Lx4/a;->j:I

    add-int/2addr v0, v1

    iput v0, p0, Lx4/a;->d:I

    goto :goto_4

    :cond_3
    const/16 v1, 0xa

    const/16 v4, 0x8

    if-eq v0, v4, :cond_5

    const/16 v5, 0x9

    if-eq v0, v5, :cond_5

    if-ne v0, v1, :cond_4

    goto :goto_0

    :cond_4
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lx4/a;->a0()Lx4/b;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lx4/a;->F()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    :goto_0
    if-ne v0, v1, :cond_6

    invoke-direct {p0}, Lx4/a;->Z()Ljava/lang/String;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lx4/a;->k:Ljava/lang/String;

    goto :goto_3

    :cond_6
    if-ne v0, v4, :cond_7

    const/16 v0, 0x27

    goto :goto_2

    :cond_7
    const/16 v0, 0x22

    :goto_2
    invoke-direct {p0, v0}, Lx4/a;->X(C)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :goto_3
    :try_start_0
    iget-object v0, p0, Lx4/a;->k:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v3, p0, Lx4/a;->h:I

    iget-object v1, p0, Lx4/a;->o:[I

    iget v4, p0, Lx4/a;->m:I

    add-int/lit8 v4, v4, -0x1

    aget v5, v1, v4

    add-int/lit8 v5, v5, 0x1

    aput v5, v1, v4
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    :catch_0
    :goto_4
    const/16 v0, 0xb

    iput v0, p0, Lx4/a;->h:I

    iget-object v0, p0, Lx4/a;->k:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    double-to-int v4, v0

    int-to-double v5, v4

    cmpl-double v0, v5, v0

    if-nez v0, :cond_8

    const/4 v0, 0x0

    iput-object v0, p0, Lx4/a;->k:Ljava/lang/String;

    iput v3, p0, Lx4/a;->h:I

    iget-object v0, p0, Lx4/a;->o:[I

    iget v1, p0, Lx4/a;->m:I

    add-int/lit8 v1, v1, -0x1

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    return v4

    :cond_8
    new-instance v0, Ljava/lang/NumberFormatException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lx4/a;->k:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lx4/a;->F()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public M()J
    .locals 8

    iget v0, p0, Lx4/a;->h:I

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lx4/a;->p()I

    move-result v0

    :cond_0
    const/16 v1, 0xf

    const/4 v2, 0x0

    if-ne v0, v1, :cond_1

    iput v2, p0, Lx4/a;->h:I

    iget-object v0, p0, Lx4/a;->o:[I

    iget v1, p0, Lx4/a;->m:I

    add-int/lit8 v1, v1, -0x1

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    iget-wide v0, p0, Lx4/a;->i:J

    return-wide v0

    :cond_1
    const/16 v1, 0x10

    const-string v3, "Expected a long but was "

    if-ne v0, v1, :cond_2

    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lx4/a;->c:[C

    iget v4, p0, Lx4/a;->d:I

    iget v5, p0, Lx4/a;->j:I

    invoke-direct {v0, v1, v4, v5}, Ljava/lang/String;-><init>([CII)V

    iput-object v0, p0, Lx4/a;->k:Ljava/lang/String;

    iget v0, p0, Lx4/a;->d:I

    iget v1, p0, Lx4/a;->j:I

    add-int/2addr v0, v1

    iput v0, p0, Lx4/a;->d:I

    goto :goto_4

    :cond_2
    const/16 v1, 0xa

    const/16 v4, 0x8

    if-eq v0, v4, :cond_4

    const/16 v5, 0x9

    if-eq v0, v5, :cond_4

    if-ne v0, v1, :cond_3

    goto :goto_0

    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lx4/a;->a0()Lx4/b;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lx4/a;->F()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    :goto_0
    if-ne v0, v1, :cond_5

    invoke-direct {p0}, Lx4/a;->Z()Ljava/lang/String;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lx4/a;->k:Ljava/lang/String;

    goto :goto_3

    :cond_5
    if-ne v0, v4, :cond_6

    const/16 v0, 0x27

    goto :goto_2

    :cond_6
    const/16 v0, 0x22

    :goto_2
    invoke-direct {p0, v0}, Lx4/a;->X(C)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :goto_3
    :try_start_0
    iget-object v0, p0, Lx4/a;->k:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iput v2, p0, Lx4/a;->h:I

    iget-object v4, p0, Lx4/a;->o:[I

    iget v5, p0, Lx4/a;->m:I

    add-int/lit8 v5, v5, -0x1

    aget v6, v4, v5

    add-int/lit8 v6, v6, 0x1

    aput v6, v4, v5
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    return-wide v0

    :catch_0
    :goto_4
    const/16 v0, 0xb

    iput v0, p0, Lx4/a;->h:I

    iget-object v0, p0, Lx4/a;->k:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    double-to-long v4, v0

    long-to-double v6, v4

    cmpl-double v0, v6, v0

    if-nez v0, :cond_7

    const/4 v0, 0x0

    iput-object v0, p0, Lx4/a;->k:Ljava/lang/String;

    iput v2, p0, Lx4/a;->h:I

    iget-object v0, p0, Lx4/a;->o:[I

    iget v1, p0, Lx4/a;->m:I

    add-int/lit8 v1, v1, -0x1

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    return-wide v4

    :cond_7
    new-instance v0, Ljava/lang/NumberFormatException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lx4/a;->k:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lx4/a;->F()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public U()Ljava/lang/String;
    .locals 3

    iget v0, p0, Lx4/a;->h:I

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lx4/a;->p()I

    move-result v0

    :cond_0
    const/16 v1, 0xe

    if-ne v0, v1, :cond_1

    invoke-direct {p0}, Lx4/a;->Z()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_1
    const/16 v1, 0xc

    if-ne v0, v1, :cond_2

    const/16 v0, 0x27

    :goto_0
    invoke-direct {p0, v0}, Lx4/a;->X(C)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    const/16 v1, 0xd

    if-ne v0, v1, :cond_3

    const/16 v0, 0x22

    goto :goto_0

    :goto_1
    const/4 v1, 0x0

    iput v1, p0, Lx4/a;->h:I

    iget-object v1, p0, Lx4/a;->n:[Ljava/lang/String;

    iget v2, p0, Lx4/a;->m:I

    add-int/lit8 v2, v2, -0x1

    aput-object v0, v1, v2

    return-object v0

    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected a name but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lx4/a;->a0()Lx4/b;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lx4/a;->F()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public W()V
    .locals 3

    iget v0, p0, Lx4/a;->h:I

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lx4/a;->p()I

    move-result v0

    :cond_0
    const/4 v1, 0x7

    if-ne v0, v1, :cond_1

    const/4 v0, 0x0

    iput v0, p0, Lx4/a;->h:I

    iget-object v0, p0, Lx4/a;->o:[I

    iget v1, p0, Lx4/a;->m:I

    add-int/lit8 v1, v1, -0x1

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    return-void

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected null but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lx4/a;->a0()Lx4/b;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lx4/a;->F()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public Y()Ljava/lang/String;
    .locals 4

    iget v0, p0, Lx4/a;->h:I

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lx4/a;->p()I

    move-result v0

    :cond_0
    const/16 v1, 0xa

    if-ne v0, v1, :cond_1

    invoke-direct {p0}, Lx4/a;->Z()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_1
    const/16 v1, 0x8

    if-ne v0, v1, :cond_2

    const/16 v0, 0x27

    :goto_0
    invoke-direct {p0, v0}, Lx4/a;->X(C)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    const/16 v1, 0x9

    if-ne v0, v1, :cond_3

    const/16 v0, 0x22

    goto :goto_0

    :cond_3
    const/16 v1, 0xb

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lx4/a;->k:Ljava/lang/String;

    const/4 v1, 0x0

    iput-object v1, p0, Lx4/a;->k:Ljava/lang/String;

    goto :goto_1

    :cond_4
    const/16 v1, 0xf

    if-ne v0, v1, :cond_5

    iget-wide v0, p0, Lx4/a;->i:J

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_5
    const/16 v1, 0x10

    if-ne v0, v1, :cond_6

    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lx4/a;->c:[C

    iget v2, p0, Lx4/a;->d:I

    iget v3, p0, Lx4/a;->j:I

    invoke-direct {v0, v1, v2, v3}, Ljava/lang/String;-><init>([CII)V

    iget v1, p0, Lx4/a;->d:I

    iget v2, p0, Lx4/a;->j:I

    add-int/2addr v1, v2

    iput v1, p0, Lx4/a;->d:I

    :goto_1
    const/4 v1, 0x0

    iput v1, p0, Lx4/a;->h:I

    iget-object v1, p0, Lx4/a;->o:[I

    iget v2, p0, Lx4/a;->m:I

    add-int/lit8 v2, v2, -0x1

    aget v3, v1, v2

    add-int/lit8 v3, v3, 0x1

    aput v3, v1, v2

    return-object v0

    :cond_6
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected a string but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lx4/a;->a0()Lx4/b;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lx4/a;->F()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a0()Lx4/b;
    .locals 1

    iget v0, p0, Lx4/a;->h:I

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lx4/a;->p()I

    move-result v0

    :cond_0
    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :pswitch_0
    sget-object v0, Lx4/b;->j:Lx4/b;

    return-object v0

    :pswitch_1
    sget-object v0, Lx4/b;->g:Lx4/b;

    return-object v0

    :pswitch_2
    sget-object v0, Lx4/b;->e:Lx4/b;

    return-object v0

    :pswitch_3
    sget-object v0, Lx4/b;->f:Lx4/b;

    return-object v0

    :pswitch_4
    sget-object v0, Lx4/b;->i:Lx4/b;

    return-object v0

    :pswitch_5
    sget-object v0, Lx4/b;->h:Lx4/b;

    return-object v0

    :pswitch_6
    sget-object v0, Lx4/b;->b:Lx4/b;

    return-object v0

    :pswitch_7
    sget-object v0, Lx4/b;->a:Lx4/b;

    return-object v0

    :pswitch_8
    sget-object v0, Lx4/b;->d:Lx4/b;

    return-object v0

    :pswitch_9
    sget-object v0, Lx4/b;->c:Lx4/b;

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public b()V
    .locals 3

    iget v0, p0, Lx4/a;->h:I

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lx4/a;->p()I

    move-result v0

    :cond_0
    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lx4/a;->d0(I)V

    iget-object v1, p0, Lx4/a;->o:[I

    iget v2, p0, Lx4/a;->m:I

    sub-int/2addr v2, v0

    const/4 v0, 0x0

    aput v0, v1, v2

    iput v0, p0, Lx4/a;->h:I

    return-void

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected BEGIN_ARRAY but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lx4/a;->a0()Lx4/b;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lx4/a;->F()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public close()V
    .locals 3

    const/4 v0, 0x0

    iput v0, p0, Lx4/a;->h:I

    iget-object v1, p0, Lx4/a;->l:[I

    const/16 v2, 0x8

    aput v2, v1, v0

    const/4 v0, 0x1

    iput v0, p0, Lx4/a;->m:I

    iget-object v0, p0, Lx4/a;->a:Ljava/io/Reader;

    invoke-virtual {v0}, Ljava/io/Reader;->close()V

    return-void
.end method

.method public final f0(Z)V
    .locals 0

    iput-boolean p1, p0, Lx4/a;->b:Z

    return-void
.end method

.method public g()V
    .locals 3

    iget v0, p0, Lx4/a;->h:I

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lx4/a;->p()I

    move-result v0

    :cond_0
    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lx4/a;->d0(I)V

    const/4 v0, 0x0

    iput v0, p0, Lx4/a;->h:I

    return-void

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected BEGIN_OBJECT but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lx4/a;->a0()Lx4/b;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lx4/a;->F()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public k0()V
    .locals 5

    const/4 v0, 0x0

    move v1, v0

    :cond_0
    iget v2, p0, Lx4/a;->h:I

    if-nez v2, :cond_1

    invoke-virtual {p0}, Lx4/a;->p()I

    move-result v2

    :cond_1
    const/4 v3, 0x3

    const/4 v4, 0x1

    if-ne v2, v3, :cond_2

    invoke-direct {p0, v4}, Lx4/a;->d0(I)V

    :goto_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    :cond_2
    if-ne v2, v4, :cond_3

    invoke-direct {p0, v3}, Lx4/a;->d0(I)V

    goto :goto_0

    :cond_3
    const/4 v3, 0x4

    if-ne v2, v3, :cond_4

    :goto_1
    iget v2, p0, Lx4/a;->m:I

    sub-int/2addr v2, v4

    iput v2, p0, Lx4/a;->m:I

    add-int/lit8 v1, v1, -0x1

    goto :goto_6

    :cond_4
    const/4 v3, 0x2

    if-ne v2, v3, :cond_5

    goto :goto_1

    :cond_5
    const/16 v3, 0xe

    if-eq v2, v3, :cond_b

    const/16 v3, 0xa

    if-ne v2, v3, :cond_6

    goto :goto_5

    :cond_6
    const/16 v3, 0x8

    if-eq v2, v3, :cond_a

    const/16 v3, 0xc

    if-ne v2, v3, :cond_7

    goto :goto_3

    :cond_7
    const/16 v3, 0x9

    if-eq v2, v3, :cond_9

    const/16 v3, 0xd

    if-ne v2, v3, :cond_8

    goto :goto_2

    :cond_8
    const/16 v3, 0x10

    if-ne v2, v3, :cond_c

    iget v2, p0, Lx4/a;->d:I

    iget v3, p0, Lx4/a;->j:I

    add-int/2addr v2, v3

    iput v2, p0, Lx4/a;->d:I

    goto :goto_6

    :cond_9
    :goto_2
    const/16 v2, 0x22

    goto :goto_4

    :cond_a
    :goto_3
    const/16 v2, 0x27

    :goto_4
    invoke-direct {p0, v2}, Lx4/a;->g0(C)V

    goto :goto_6

    :cond_b
    :goto_5
    invoke-direct {p0}, Lx4/a;->j0()V

    :cond_c
    :goto_6
    iput v0, p0, Lx4/a;->h:I

    if-nez v1, :cond_0

    iget-object v0, p0, Lx4/a;->o:[I

    iget v1, p0, Lx4/a;->m:I

    add-int/lit8 v2, v1, -0x1

    aget v3, v0, v2

    add-int/2addr v3, v4

    aput v3, v0, v2

    iget-object v0, p0, Lx4/a;->n:[Ljava/lang/String;

    sub-int/2addr v1, v4

    const-string v2, "null"

    aput-object v2, v0, v1

    return-void
.end method

.method p()I
    .locals 15

    goto/32 :goto_4e

    nop

    :goto_0
    if-ne v0, v6, :cond_0

    goto/32 :goto_93

    :cond_0
    goto/32 :goto_cf

    nop

    :goto_1
    if-ne v1, v4, :cond_1

    goto/32 :goto_58

    :cond_1
    goto/32 :goto_96

    nop

    :goto_2
    const/4 v14, 0x5

    goto/32 :goto_c0

    nop

    :goto_3
    if-ne v0, v10, :cond_2

    goto/32 :goto_ca

    :cond_2
    goto/32 :goto_35

    nop

    :goto_4
    goto/16 :goto_a3

    :goto_5
    goto/32 :goto_ad

    nop

    :goto_6
    return v8

    :goto_7
    goto/32 :goto_4f

    nop

    :goto_8
    const/16 v0, 0xe

    goto/32 :goto_c2

    nop

    :goto_9
    const/16 v14, 0x3e

    goto/32 :goto_a1

    nop

    :goto_a
    if-eq v2, v13, :cond_3

    goto/32 :goto_7b

    :cond_3
    goto/32 :goto_99

    nop

    :goto_b
    sub-int/2addr v0, v13

    goto/32 :goto_e

    nop

    :goto_c
    invoke-direct {p0, v0}, Lx4/a;->l0(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    goto/32 :goto_80

    nop

    :goto_d
    iget-object v0, p0, Lx4/a;->c:[C

    goto/32 :goto_c8

    nop

    :goto_e
    iput v0, p0, Lx4/a;->d:I

    goto/32 :goto_7f

    nop

    :goto_f
    if-ne v0, v10, :cond_4

    goto/32 :goto_7b

    :cond_4
    goto/32 :goto_6a

    nop

    :goto_10
    goto/16 :goto_6d

    :goto_11
    goto/32 :goto_21

    nop

    :goto_12
    iput v0, p0, Lx4/a;->h:I

    goto/32 :goto_47

    nop

    :goto_13
    const/16 v6, 0x5d

    goto/32 :goto_36

    nop

    :goto_14
    invoke-direct {p0}, Lx4/a;->m()V

    :goto_15
    goto/32 :goto_88

    nop

    :goto_16
    const/4 v8, 0x7

    goto/32 :goto_31

    nop

    :goto_17
    iput v3, p0, Lx4/a;->h:I

    goto/32 :goto_1d

    nop

    :goto_18
    iput v13, p0, Lx4/a;->h:I

    goto/32 :goto_92

    nop

    :goto_19
    int-to-char v0, v1

    goto/32 :goto_8e

    nop

    :goto_1a
    throw v0

    :goto_1b
    goto/32 :goto_9c

    nop

    :goto_1c
    if-nez v0, :cond_5

    goto/32 :goto_c6

    :cond_5
    goto/32 :goto_c5

    nop

    :goto_1d
    return v3

    :goto_1e
    goto/32 :goto_32

    nop

    :goto_1f
    if-ne v1, v9, :cond_6

    goto/32 :goto_81

    :cond_6
    goto/32 :goto_42

    nop

    :goto_20
    if-eq v0, v1, :cond_7

    goto/32 :goto_48

    :cond_7
    goto/32 :goto_a2

    nop

    :goto_21
    if-eq v2, v11, :cond_8

    goto/32 :goto_74

    :cond_8
    goto/32 :goto_b1

    nop

    :goto_22
    iget v0, p0, Lx4/a;->d:I

    goto/32 :goto_51

    nop

    :goto_23
    iput v12, p0, Lx4/a;->h:I

    goto/32 :goto_4c

    nop

    :goto_24
    return v11

    :goto_25
    goto/32 :goto_8f

    nop

    :goto_26
    const/16 v1, 0x3a

    goto/32 :goto_c1

    nop

    :goto_27
    invoke-direct {p0}, Lx4/a;->m()V

    goto/32 :goto_38

    nop

    :goto_28
    sub-int/2addr v1, v13

    goto/32 :goto_75

    nop

    :goto_29
    invoke-direct {p0}, Lx4/a;->m()V

    goto/32 :goto_90

    nop

    :goto_2a
    sub-int/2addr v0, v13

    goto/32 :goto_9e

    nop

    :goto_2b
    add-int/lit8 v2, v1, -0x1

    goto/32 :goto_7e

    nop

    :goto_2c
    iget v1, p0, Lx4/a;->m:I

    goto/32 :goto_2b

    nop

    :goto_2d
    goto/16 :goto_ca

    :goto_2e
    goto/32 :goto_5a

    nop

    :goto_2f
    if-ne v2, v14, :cond_9

    goto/32 :goto_ce

    :cond_9
    goto/32 :goto_d1

    nop

    :goto_30
    invoke-direct {p0}, Lx4/a;->c0()I

    move-result v0

    goto/32 :goto_8a

    nop

    :goto_31
    const/16 v9, 0x3b

    goto/32 :goto_43

    nop

    :goto_32
    const/16 v0, 0x9

    goto/32 :goto_4

    nop

    :goto_33
    iget v1, p0, Lx4/a;->e:I

    goto/32 :goto_4b

    nop

    :goto_34
    iput v0, p0, Lx4/a;->d:I

    goto/32 :goto_c7

    nop

    :goto_35
    if-ne v0, v9, :cond_a

    goto/32 :goto_3d

    :cond_a
    goto/32 :goto_cc

    nop

    :goto_36
    const/4 v7, 0x3

    goto/32 :goto_16

    nop

    :goto_37
    iget v1, p0, Lx4/a;->m:I

    goto/32 :goto_61

    nop

    :goto_38
    goto/16 :goto_ca

    :goto_39
    goto/32 :goto_2

    nop

    :goto_3a
    iget-boolean v0, p0, Lx4/a;->b:Z

    goto/32 :goto_1c

    nop

    :goto_3b
    iput v11, p0, Lx4/a;->h:I

    goto/32 :goto_24

    nop

    :goto_3c
    throw v0

    :goto_3d
    goto/32 :goto_27

    nop

    :goto_3e
    iget-object v0, p0, Lx4/a;->c:[C

    goto/32 :goto_98

    nop

    :goto_3f
    return v0

    :goto_40
    goto/32 :goto_d

    nop

    :goto_41
    const/4 v11, 0x4

    goto/32 :goto_d4

    nop

    :goto_42
    if-eq v1, v0, :cond_b

    goto/32 :goto_4d

    :cond_b
    goto/32 :goto_23

    nop

    :goto_43
    const/16 v10, 0x2c

    goto/32 :goto_41

    nop

    :goto_44
    const-string v0, "Expected \':\'"

    goto/32 :goto_91

    nop

    :goto_45
    iget v0, p0, Lx4/a;->d:I

    goto/32 :goto_33

    nop

    :goto_46
    aput v11, v0, v1

    goto/32 :goto_6f

    nop

    :goto_47
    return v0

    :goto_48
    goto/32 :goto_60

    nop

    :goto_49
    throw v0

    :goto_4a
    goto/32 :goto_2f

    nop

    :goto_4b
    if-ge v0, v1, :cond_c

    goto/32 :goto_64

    :cond_c
    goto/32 :goto_59

    nop

    :goto_4c
    return v12

    :goto_4d
    goto/32 :goto_6e

    nop

    :goto_4e
    iget-object v0, p0, Lx4/a;->l:[I

    goto/32 :goto_2c

    nop

    :goto_4f
    invoke-direct {p0}, Lx4/a;->m()V

    goto/32 :goto_17

    nop

    :goto_50
    const/4 v0, 0x6

    goto/32 :goto_a7

    nop

    :goto_51
    sub-int/2addr v0, v13

    goto/32 :goto_34

    nop

    :goto_52
    const-string v1, "JsonReader is closed"

    goto/32 :goto_85

    nop

    :goto_53
    invoke-direct {p0, v0}, Lx4/a;->l0(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    goto/32 :goto_a8

    nop

    :goto_54
    aget-char v0, v0, v1

    goto/32 :goto_ac

    nop

    :goto_55
    sub-int/2addr v1, v13

    goto/32 :goto_46

    nop

    :goto_56
    aget-char v0, v0, v1

    goto/32 :goto_9

    nop

    :goto_57
    throw v0

    :goto_58
    goto/32 :goto_29

    nop

    :goto_59
    invoke-direct {p0, v13}, Lx4/a;->y(I)Z

    move-result v0

    goto/32 :goto_63

    nop

    :goto_5a
    if-eq v2, v12, :cond_d

    goto/32 :goto_39

    :cond_d
    goto/32 :goto_d0

    nop

    :goto_5b
    iget v0, p0, Lx4/a;->d:I

    goto/32 :goto_ab

    nop

    :goto_5c
    invoke-direct {p0, v0}, Lx4/a;->l0(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    goto/32 :goto_3c

    nop

    :goto_5d
    goto/16 :goto_a3

    :goto_5e
    if-eq v2, v14, :cond_e

    goto/32 :goto_15

    :cond_e
    goto/32 :goto_d5

    nop

    :goto_5f
    const/4 v13, 0x1

    goto/32 :goto_af

    nop

    :goto_60
    invoke-direct {p0}, Lx4/a;->m()V

    goto/32 :goto_5b

    nop

    :goto_61
    sub-int/2addr v1, v13

    goto/32 :goto_b2

    nop

    :goto_62
    if-ne v1, v0, :cond_f

    goto/32 :goto_4a

    :cond_f
    goto/32 :goto_a5

    nop

    :goto_63
    if-nez v0, :cond_10

    goto/32 :goto_ca

    :cond_10
    :goto_64
    goto/32 :goto_3e

    nop

    :goto_65
    invoke-direct {p0, v3}, Lx4/a;->l0(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    goto/32 :goto_57

    nop

    :goto_66
    const/4 v0, 0x0

    goto/32 :goto_d6

    nop

    :goto_67
    const/16 v1, 0x3d

    goto/32 :goto_b3

    nop

    :goto_68
    invoke-direct {p0}, Lx4/a;->m()V

    goto/32 :goto_c4

    nop

    :goto_69
    if-nez v0, :cond_11

    goto/32 :goto_b8

    :cond_11
    goto/32 :goto_b7

    nop

    :goto_6a
    if-ne v0, v9, :cond_12

    goto/32 :goto_7b

    :cond_12
    goto/32 :goto_b4

    nop

    :goto_6b
    if-ne v0, v1, :cond_13

    goto/32 :goto_ba

    :cond_13
    goto/32 :goto_0

    nop

    :goto_6c
    throw v0

    :goto_6d
    goto/32 :goto_55

    nop

    :goto_6e
    const-string v0, "Unterminated object"

    goto/32 :goto_c

    nop

    :goto_6f
    const/16 v0, 0x7d

    goto/32 :goto_5e

    nop

    :goto_70
    if-ne v2, v13, :cond_14

    goto/32 :goto_1b

    :cond_14
    goto/32 :goto_7d

    nop

    :goto_71
    goto/16 :goto_a3

    :goto_72
    goto/32 :goto_97

    nop

    :goto_73
    throw v0

    :goto_74
    goto/32 :goto_50

    nop

    :goto_75
    aput v12, v0, v1

    goto/32 :goto_2d

    nop

    :goto_76
    if-nez v0, :cond_15

    goto/32 :goto_a0

    :cond_15
    goto/32 :goto_68

    nop

    :goto_77
    iget v0, p0, Lx4/a;->d:I

    goto/32 :goto_b

    nop

    :goto_78
    const/4 v1, -0x1

    goto/32 :goto_20

    nop

    :goto_79
    invoke-direct {p0, v0}, Lx4/a;->l0(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    goto/32 :goto_1a

    nop

    :goto_7a
    return v7

    :goto_7b
    goto/32 :goto_70

    nop

    :goto_7c
    if-eq v2, v14, :cond_16

    goto/32 :goto_11

    :cond_16
    goto/32 :goto_10

    nop

    :goto_7d
    if-eq v2, v12, :cond_17

    goto/32 :goto_83

    :cond_17
    goto/32 :goto_82

    nop

    :goto_7e
    aget v2, v0, v2

    goto/32 :goto_cb

    nop

    :goto_7f
    invoke-direct {p0}, Lx4/a;->b0()I

    move-result v0

    goto/32 :goto_69

    nop

    :goto_80
    throw v0

    :goto_81
    goto/32 :goto_14

    nop

    :goto_82
    goto/16 :goto_1b

    :goto_83
    goto/32 :goto_89

    nop

    :goto_84
    invoke-direct {p0}, Lx4/a;->m()V

    goto/32 :goto_45

    nop

    :goto_85
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_6c

    nop

    :goto_86
    if-ne v0, v5, :cond_18

    goto/32 :goto_1e

    :cond_18
    goto/32 :goto_87

    nop

    :goto_87
    if-ne v0, v4, :cond_19

    goto/32 :goto_7

    :cond_19
    goto/32 :goto_f

    nop

    :goto_88
    invoke-direct {p0, v13}, Lx4/a;->V(Z)I

    move-result v1

    goto/32 :goto_aa

    nop

    :goto_89
    const-string v0, "Unexpected value"

    goto/32 :goto_79

    nop

    :goto_8a
    if-nez v0, :cond_1a

    goto/32 :goto_40

    :cond_1a
    goto/32 :goto_3f

    nop

    :goto_8b
    if-ne v1, v10, :cond_1b

    goto/32 :goto_15

    :cond_1b
    goto/32 :goto_1f

    nop

    :goto_8c
    invoke-direct {p0, v3}, Lx4/a;->l0(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    goto/32 :goto_49

    nop

    :goto_8d
    iget-object v0, p0, Lx4/a;->l:[I

    goto/32 :goto_37

    nop

    :goto_8e
    invoke-direct {p0, v0}, Lx4/a;->D(C)Z

    move-result v0

    goto/32 :goto_95

    nop

    :goto_8f
    const-string v0, "Unterminated array"

    goto/32 :goto_5c

    nop

    :goto_90
    const/16 v0, 0xc

    goto/32 :goto_71

    nop

    :goto_91
    invoke-direct {p0, v0}, Lx4/a;->l0(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    goto/32 :goto_73

    nop

    :goto_92
    return v13

    :goto_93
    goto/32 :goto_a

    nop

    :goto_94
    iget v0, p0, Lx4/a;->d:I

    goto/32 :goto_2a

    nop

    :goto_95
    if-nez v0, :cond_1c

    goto/32 :goto_c3

    :cond_1c
    goto/32 :goto_8

    nop

    :goto_96
    const-string v3, "Expected name"

    goto/32 :goto_62

    nop

    :goto_97
    const/16 v0, 0xd

    goto/32 :goto_5d

    nop

    :goto_98
    iget v1, p0, Lx4/a;->d:I

    goto/32 :goto_56

    nop

    :goto_99
    iput v11, p0, Lx4/a;->h:I

    goto/32 :goto_b9

    nop

    :goto_9a
    goto/16 :goto_ca

    :goto_9b
    goto/32 :goto_44

    nop

    :goto_9c
    invoke-direct {p0}, Lx4/a;->m()V

    goto/32 :goto_22

    nop

    :goto_9d
    const-string v0, "Expected value"

    goto/32 :goto_53

    nop

    :goto_9e
    iput v0, p0, Lx4/a;->d:I

    goto/32 :goto_19

    nop

    :goto_9f
    goto :goto_a3

    :goto_a0
    goto/32 :goto_9d

    nop

    :goto_a1
    if-eq v0, v14, :cond_1d

    goto/32 :goto_ca

    :cond_1d
    goto/32 :goto_a6

    nop

    :goto_a2
    const/16 v0, 0x11

    :goto_a3
    goto/32 :goto_12

    nop

    :goto_a4
    if-ne v0, v1, :cond_1e

    goto/32 :goto_a9

    :cond_1e
    goto/32 :goto_77

    nop

    :goto_a5
    invoke-direct {p0}, Lx4/a;->m()V

    goto/32 :goto_94

    nop

    :goto_a6
    add-int/2addr v1, v13

    goto/32 :goto_d2

    nop

    :goto_a7
    if-eq v2, v0, :cond_1f

    goto/32 :goto_bd

    :cond_1f
    goto/32 :goto_3a

    nop

    :goto_a8
    throw v0

    :goto_a9
    goto/32 :goto_18

    nop

    :goto_aa
    if-ne v1, v5, :cond_20

    goto/32 :goto_72

    :cond_20
    goto/32 :goto_1

    nop

    :goto_ab
    sub-int/2addr v0, v13

    goto/32 :goto_b0

    nop

    :goto_ac
    invoke-direct {p0, v0}, Lx4/a;->D(C)Z

    move-result v0

    goto/32 :goto_76

    nop

    :goto_ad
    new-instance v0, Ljava/lang/IllegalStateException;

    goto/32 :goto_52

    nop

    :goto_ae
    invoke-direct {p0, v13}, Lx4/a;->V(Z)I

    move-result v0

    goto/32 :goto_86

    nop

    :goto_af
    if-eq v2, v13, :cond_21

    goto/32 :goto_2e

    :cond_21
    goto/32 :goto_28

    nop

    :goto_b0
    iput v0, p0, Lx4/a;->d:I

    goto/32 :goto_b5

    nop

    :goto_b1
    sub-int/2addr v1, v13

    goto/32 :goto_d8

    nop

    :goto_b2
    aput v8, v0, v1

    goto/32 :goto_bc

    nop

    :goto_b3
    if-eq v0, v1, :cond_22

    goto/32 :goto_9b

    :cond_22
    goto/32 :goto_84

    nop

    :goto_b4
    const/16 v1, 0x5b

    goto/32 :goto_6b

    nop

    :goto_b5
    goto :goto_ca

    :goto_b6
    goto/32 :goto_c9

    nop

    :goto_b7
    return v0

    :goto_b8
    goto/32 :goto_30

    nop

    :goto_b9
    return v11

    :goto_ba
    goto/32 :goto_d7

    nop

    :goto_bb
    invoke-direct {p0, v13}, Lx4/a;->V(Z)I

    move-result v0

    goto/32 :goto_26

    nop

    :goto_bc
    goto :goto_ca

    :goto_bd
    goto/32 :goto_d3

    nop

    :goto_be
    const/16 v5, 0x22

    goto/32 :goto_13

    nop

    :goto_bf
    const/16 v4, 0x27

    goto/32 :goto_be

    nop

    :goto_c0
    if-ne v2, v7, :cond_23

    goto/32 :goto_6d

    :cond_23
    goto/32 :goto_7c

    nop

    :goto_c1
    if-ne v0, v1, :cond_24

    goto/32 :goto_ca

    :cond_24
    goto/32 :goto_67

    nop

    :goto_c2
    goto/16 :goto_a3

    :goto_c3
    goto/32 :goto_8c

    nop

    :goto_c4
    const/16 v0, 0xa

    goto/32 :goto_9f

    nop

    :goto_c5
    invoke-direct {p0}, Lx4/a;->n()V

    :goto_c6
    goto/32 :goto_8d

    nop

    :goto_c7
    iput v8, p0, Lx4/a;->h:I

    goto/32 :goto_6

    nop

    :goto_c8
    iget v1, p0, Lx4/a;->d:I

    goto/32 :goto_54

    nop

    :goto_c9
    if-ne v2, v3, :cond_25

    goto/32 :goto_5

    :cond_25
    :goto_ca
    goto/32 :goto_ae

    nop

    :goto_cb
    const/16 v3, 0x8

    goto/32 :goto_bf

    nop

    :goto_cc
    if-eq v0, v6, :cond_26

    goto/32 :goto_25

    :cond_26
    goto/32 :goto_3b

    nop

    :goto_cd
    return v12

    :goto_ce
    goto/32 :goto_65

    nop

    :goto_cf
    const/16 v1, 0x7b

    goto/32 :goto_a4

    nop

    :goto_d0
    invoke-direct {p0, v13}, Lx4/a;->V(Z)I

    move-result v0

    goto/32 :goto_3

    nop

    :goto_d1
    iput v12, p0, Lx4/a;->h:I

    goto/32 :goto_cd

    nop

    :goto_d2
    iput v1, p0, Lx4/a;->d:I

    goto/32 :goto_9a

    nop

    :goto_d3
    if-eq v2, v8, :cond_27

    goto/32 :goto_b6

    :cond_27
    goto/32 :goto_66

    nop

    :goto_d4
    const/4 v12, 0x2

    goto/32 :goto_5f

    nop

    :goto_d5
    invoke-direct {p0, v13}, Lx4/a;->V(Z)I

    move-result v1

    goto/32 :goto_8b

    nop

    :goto_d6
    invoke-direct {p0, v0}, Lx4/a;->V(Z)I

    move-result v0

    goto/32 :goto_78

    nop

    :goto_d7
    iput v7, p0, Lx4/a;->h:I

    goto/32 :goto_7a

    nop

    :goto_d8
    aput v14, v0, v1

    goto/32 :goto_bb

    nop
.end method

.method public r()V
    .locals 3

    iget v0, p0, Lx4/a;->h:I

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lx4/a;->p()I

    move-result v0

    :cond_0
    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    iget v0, p0, Lx4/a;->m:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lx4/a;->m:I

    iget-object v1, p0, Lx4/a;->o:[I

    add-int/lit8 v0, v0, -0x1

    aget v2, v1, v0

    add-int/lit8 v2, v2, 0x1

    aput v2, v1, v0

    const/4 v0, 0x0

    iput v0, p0, Lx4/a;->h:I

    return-void

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected END_ARRAY but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lx4/a;->a0()Lx4/b;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lx4/a;->F()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public s()V
    .locals 3

    iget v0, p0, Lx4/a;->h:I

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lx4/a;->p()I

    move-result v0

    :cond_0
    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    iget v0, p0, Lx4/a;->m:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lx4/a;->m:I

    iget-object v1, p0, Lx4/a;->n:[Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v2, v1, v0

    iget-object v1, p0, Lx4/a;->o:[I

    add-int/lit8 v0, v0, -0x1

    aget v2, v1, v0

    add-int/lit8 v2, v2, 0x1

    aput v2, v1, v0

    const/4 v0, 0x0

    iput v0, p0, Lx4/a;->h:I

    return-void

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected END_OBJECT but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lx4/a;->a0()Lx4/b;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lx4/a;->F()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lx4/a;->F()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public z()Ljava/lang/String;
    .locals 5

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget v1, p0, Lx4/a;->m:I

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_3

    iget-object v3, p0, Lx4/a;->l:[I

    aget v3, v3, v2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    const/4 v4, 0x4

    if-eq v3, v4, :cond_0

    const/4 v4, 0x5

    if-eq v3, v4, :cond_0

    goto :goto_1

    :cond_0
    const/16 v3, 0x2e

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lx4/a;->n:[Ljava/lang/String;

    aget-object v4, v3, v2

    if-eqz v4, :cond_2

    aget-object v3, v3, v2

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_1
    const/16 v3, 0x5b

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lx4/a;->o:[I

    aget v3, v3, v2

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v3, 0x5d

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_2
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
