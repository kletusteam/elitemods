.class public Le2/a;
.super Ljava/lang/Object;


# static fields
.field static final l:Le2/a;


# instance fields
.field public a:Landroid/content/Context;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/Boolean;

.field public j:Ljava/lang/String;

.field private k:Le2/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Le2/a;

    invoke-direct {v0}, Le2/a;-><init>()V

    sput-object v0, Le2/a;->l:Le2/a;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Le2/a;->a:Landroid/content/Context;

    iput-object v0, p0, Le2/a;->b:Ljava/lang/String;

    iput-object v0, p0, Le2/a;->c:Ljava/lang/String;

    iput-object v0, p0, Le2/a;->d:Ljava/lang/String;

    iput-object v0, p0, Le2/a;->e:Ljava/lang/String;

    iput-object v0, p0, Le2/a;->f:Ljava/lang/String;

    iput-object v0, p0, Le2/a;->g:Ljava/lang/String;

    iput-object v0, p0, Le2/a;->h:Ljava/lang/String;

    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    iput-object v1, p0, Le2/a;->i:Ljava/lang/Boolean;

    iput-object v0, p0, Le2/a;->j:Ljava/lang/String;

    new-instance v0, Le2/b;

    invoke-direct {v0}, Le2/b;-><init>()V

    iput-object v0, p0, Le2/a;->k:Le2/b;

    return-void
.end method

.method private a()Ljava/lang/Boolean;
    .locals 2

    iget-object v0, p0, Le2/a;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Le2/a;->e:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Le2/a;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Le2/a;->a:Landroid/content/Context;

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    return-object v0

    :cond_1
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "have send args is null\uff0cyou must init first. appId "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Le2/a;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " appVersion "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Le2/a;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " appKey "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Le2/a;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Li2/e;->b(Ljava/lang/String;)V

    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    return-object v0
.end method


# virtual methods
.method public b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Le2/a;->a:Landroid/content/Context;

    iput-object p2, p0, Le2/a;->b:Ljava/lang/String;

    iput-object p3, p0, Le2/a;->c:Ljava/lang/String;

    iput-object p4, p0, Le2/a;->e:Ljava/lang/String;

    iput-object p5, p0, Le2/a;->f:Ljava/lang/String;

    iput-object p6, p0, Le2/a;->g:Ljava/lang/String;

    return-void
.end method

.method public c(Ljava/lang/String;JLjava/lang/String;ILjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Map;)Ljava/lang/Boolean;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "J",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/Boolean;"
        }
    .end annotation

    move-object v12, p0

    invoke-direct {p0}, Le2/a;->a()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    if-nez p1, :cond_1

    iget-object v0, v12, Le2/a;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, "h-adashx.ut.taobao.com"

    :goto_0
    move-object v3, v0

    goto :goto_1

    :cond_1
    move-object v3, p1

    :goto_1
    iget-object v1, v12, Le2/a;->c:Ljava/lang/String;

    iget-object v2, v12, Le2/a;->a:Landroid/content/Context;

    move-object v0, p0

    move-wide v4, p2

    move-object/from16 v6, p4

    move/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    move-object/from16 v11, p9

    invoke-static/range {v0 .. v11}, Lh2/c;->a(Le2/a;Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;JLjava/lang/String;ILjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Map;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_2
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    return-object v0
.end method
