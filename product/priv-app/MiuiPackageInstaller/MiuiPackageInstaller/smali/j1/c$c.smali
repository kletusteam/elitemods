.class final Lj1/c$c;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lj1/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lj1/c$c$a;
    }
.end annotation


# instance fields
.field final a:Ljava/lang/String;

.field final b:Ljava/lang/String;

.field final c:Ljava/lang/String;

.field final d:Ljava/lang/String;

.field final e:Ljava/lang/String;

.field final f:Ljava/lang/String;

.field final g:Ljava/lang/String;

.field final h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final i:Ljava/lang/String;

.field final j:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lj1/c$c$a;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lj1/c$c;->j:Ljava/util/Map;

    iget-object v0, p1, Lj1/c$c$a;->a:Ljava/lang/String;

    iput-object v0, p0, Lj1/c$c;->a:Ljava/lang/String;

    iget-object v0, p1, Lj1/c$c$a;->b:Ljava/lang/String;

    iput-object v0, p0, Lj1/c$c;->b:Ljava/lang/String;

    iget-object v0, p1, Lj1/c$c$a;->c:Ljava/lang/String;

    iput-object v0, p0, Lj1/c$c;->c:Ljava/lang/String;

    iget-object v0, p1, Lj1/c$c$a;->d:Ljava/lang/String;

    iput-object v0, p0, Lj1/c$c;->d:Ljava/lang/String;

    iget-object v0, p1, Lj1/c$c$a;->e:Ljava/lang/String;

    iput-object v0, p0, Lj1/c$c;->e:Ljava/lang/String;

    iget-object v0, p1, Lj1/c$c$a;->f:Ljava/lang/String;

    iput-object v0, p0, Lj1/c$c;->f:Ljava/lang/String;

    iget-object v0, p1, Lj1/c$c$a;->g:Ljava/lang/String;

    iput-object v0, p0, Lj1/c$c;->g:Ljava/lang/String;

    iget-object p1, p1, Lj1/c$c$a;->h:Ljava/util/Map;

    iput-object p1, p0, Lj1/c$c;->h:Ljava/util/Map;

    invoke-direct {p0}, Lj1/c$c;->a()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lj1/c$c;->i:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lj1/c$c$a;Lj1/c$a;)V
    .locals 0

    invoke-direct {p0, p1}, Lj1/c$c;-><init>(Lj1/c$c$a;)V

    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 4

    iget-object v0, p0, Lj1/c$c;->j:Ljava/util/Map;

    iget-object v1, p0, Lj1/c$c;->a:Ljava/lang/String;

    const-string v2, "appKey"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lj1/c$c;->j:Ljava/util/Map;

    iget-object v1, p0, Lj1/c$c;->c:Ljava/lang/String;

    const-string v2, "appVer"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lj1/c$c;->j:Ljava/util/Map;

    iget-object v1, p0, Lj1/c$c;->d:Ljava/lang/String;

    const-string v2, "osType"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lj1/c$c;->j:Ljava/util/Map;

    iget-object v1, p0, Lj1/c$c;->e:Ljava/lang/String;

    const-string v2, "osVer"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lj1/c$c;->j:Ljava/util/Map;

    iget-object v1, p0, Lj1/c$c;->f:Ljava/lang/String;

    const-string v2, "deviceId"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lj1/c$c;->j:Ljava/util/Map;

    iget-object v1, p0, Lj1/c$c;->g:Ljava/lang/String;

    const-string v2, "beaconVer"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lj1/c$c;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v2, p0, Lj1/c$c;->j:Ljava/util/Map;

    iget-object v3, p0, Lj1/c$c;->h:Ljava/util/Map;

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lj1/c$c;->j:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lj1/c$c;->j:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_1
    iget-object v1, p0, Lj1/c$c;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lj1/d;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lj1/c$c;->j:Ljava/util/Map;

    const-string v2, "sign"

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0
.end method
