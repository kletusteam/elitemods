.class final Lj1/c;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lj1/c$c;,
        Lj1/c$b;
    }
.end annotation


# static fields
.field private static final d:Ljava/lang/String;

.field private static final e:Ljava/lang/String;


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lj1/a$d;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lj1/a;

.field private final c:Lj1/c$b;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    sget-boolean v0, Lj1/b;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "100.67.64.54"

    goto :goto_0

    :cond_0
    const-string v0, "beacon-api.aliyuncs.com"

    :goto_0
    sput-object v0, Lj1/c;->d:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "https://"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "/beacon/fetch/config"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lj1/c;->e:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Lj1/a;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lj1/c;->a:Ljava/util/List;

    iput-object p1, p0, Lj1/c;->b:Lj1/a;

    new-instance p1, Lj1/c$b;

    const/4 v0, 0x0

    invoke-direct {p1, p0, v0}, Lj1/c$b;-><init>(Lj1/c;Lj1/c$a;)V

    iput-object p1, p0, Lj1/c;->c:Lj1/c$b;

    return-void
.end method

.method private a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)Lj1/c$c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lj1/c$c;"
        }
    .end annotation

    new-instance v0, Lj1/c$c$a;

    invoke-direct {v0}, Lj1/c$c$a;-><init>()V

    invoke-virtual {v0, p2}, Lj1/c$c$a;->a(Ljava/lang/String;)Lj1/c$c$a;

    move-result-object p2

    invoke-virtual {p2, p3}, Lj1/c$c$a;->d(Ljava/lang/String;)Lj1/c$c$a;

    move-result-object p2

    invoke-static {p1}, Lj1/d;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Lj1/c$c$a;->e(Ljava/lang/String;)Lj1/c$c$a;

    move-result-object p2

    const-string p3, "Android"

    invoke-virtual {p2, p3}, Lj1/c$c$a;->f(Ljava/lang/String;)Lj1/c$c$a;

    move-result-object p2

    sget p3, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Lj1/c$c$a;->g(Ljava/lang/String;)Lj1/c$c$a;

    move-result-object p2

    invoke-static {p1}, La7/a;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Lj1/c$c$a;->h(Ljava/lang/String;)Lj1/c$c$a;

    move-result-object p1

    const-string p2, "1.0.5"

    invoke-virtual {p1, p2}, Lj1/c$c$a;->i(Ljava/lang/String;)Lj1/c$c$a;

    move-result-object p1

    invoke-virtual {p1, p4}, Lj1/c$c$a;->b(Ljava/util/Map;)Lj1/c$c$a;

    move-result-object p1

    invoke-virtual {p1}, Lj1/c$c$a;->c()Lj1/c$c;

    move-result-object p1

    return-object p1
.end method

.method private b(Lj1/c$c;)Ljava/lang/String;
    .locals 4

    iget-object p1, p1, Lj1/c$c;->j:Ljava/util/Map;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-direct {p0, v2}, Lj1/c;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-direct {p0, v2}, Lj1/c;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "&"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result p1

    if-lez p1, :cond_1

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result p1

    add-int/lit8 p1, p1, -0x1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method static synthetic e(Lj1/c;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lj1/c;->g(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private f(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lj1/c;->h(Ljava/lang/String;)V

    return-void
.end method

.method private g(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lj1/c;->b:Lj1/a;

    new-instance v1, Lj1/a$e;

    invoke-direct {v1, p1, p2}, Lj1/a$e;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lj1/a;->c(Lj1/a$e;)V

    return-void
.end method

.method private h(Ljava/lang/String;)V
    .locals 4

    :try_start_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string p1, "result"

    invoke-virtual {v0, p1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object p1

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lj1/c;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v1

    if-ge v0, v1, :cond_1

    invoke-virtual {p1, v0}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/json/JSONObject;

    const-string v2, "key"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "value"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Lj1/a$d;

    invoke-direct {v3, v2, v1}, Lj1/a$d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lj1/c;->a:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catch_0
    :cond_1
    return-void
.end method

.method private i(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    :try_start_0
    const-string v0, "UTF-8"

    invoke-static {p1, v0}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    const-string p1, ""

    return-object p1
.end method


# virtual methods
.method c()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lj1/a$d;",
            ">;"
        }
    .end annotation

    goto/32 :goto_2

    nop

    :goto_0
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    goto/32 :goto_1

    nop

    :goto_1
    return-object v0

    :goto_2
    iget-object v0, p0, Lj1/c;->a:Ljava/util/List;

    goto/32 :goto_0

    nop
.end method

.method d(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    goto/32 :goto_b

    nop

    :goto_0
    new-instance p3, Ljava/lang/StringBuilder;

    goto/32 :goto_1c

    nop

    :goto_1
    invoke-direct {p0, p1}, Lj1/c;->f(Ljava/lang/String;)V

    goto/32 :goto_5

    nop

    :goto_2
    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_8

    nop

    :goto_3
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object p1

    goto/32 :goto_17

    nop

    :goto_4
    iget-object p3, p0, Lj1/c;->c:Lj1/c$b;

    goto/32 :goto_10

    nop

    :goto_5
    return-void

    :goto_6
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_1d

    nop

    :goto_7
    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    goto/32 :goto_16

    nop

    :goto_8
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    goto/32 :goto_9

    nop

    :goto_9
    invoke-static {p4, p2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_1

    nop

    :goto_a
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_18

    nop

    :goto_b
    invoke-direct {p0, p1, p2, p3, p4}, Lj1/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)Lj1/c$c;

    move-result-object p1

    goto/32 :goto_f

    nop

    :goto_c
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_2

    nop

    :goto_d
    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_14

    nop

    :goto_e
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    goto/32 :goto_0

    nop

    :goto_f
    new-instance p2, Ljava/lang/StringBuilder;

    goto/32 :goto_a

    nop

    :goto_10
    invoke-direct {p0, p1}, Lj1/c;->b(Lj1/c$c;)Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_3

    nop

    :goto_11
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_e

    nop

    :goto_12
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_15

    nop

    :goto_13
    const-string p3, "byappkey"

    goto/32 :goto_11

    nop

    :goto_14
    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_7

    nop

    :goto_15
    const-string p3, "[fetchByAppKey] result: "

    goto/32 :goto_c

    nop

    :goto_16
    const-string p4, "beacon"

    goto/32 :goto_1a

    nop

    :goto_17
    invoke-virtual {p3, p2, p1}, Lj1/c$b;->a(Ljava/lang/String;[B)Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_1e

    nop

    :goto_18
    sget-object p3, Lj1/c;->e:Ljava/lang/String;

    goto/32 :goto_6

    nop

    :goto_19
    const-string p4, "url="

    goto/32 :goto_d

    nop

    :goto_1a
    invoke-static {p4, p3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_4

    nop

    :goto_1b
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_13

    nop

    :goto_1c
    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_19

    nop

    :goto_1d
    const-string p3, "/"

    goto/32 :goto_1b

    nop

    :goto_1e
    new-instance p2, Ljava/lang/StringBuilder;

    goto/32 :goto_12

    nop
.end method
