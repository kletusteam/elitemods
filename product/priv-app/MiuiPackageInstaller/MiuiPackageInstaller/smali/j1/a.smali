.class public final Lj1/a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lj1/a$d;,
        Lj1/a$e;,
        Lj1/a$f;,
        Lj1/a$g;,
        Lj1/a$c;,
        Lj1/a$b;
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Landroid/os/HandlerThread;

.field private final e:Lj1/c;

.field private final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lj1/a$g;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lj1/a$f;",
            ">;"
        }
    .end annotation
.end field

.field private h:Landroid/os/Handler;

.field private i:J

.field private j:Z

.field private k:I


# direct methods
.method private constructor <init>(Lj1/a$c;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lj1/a;->f:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lj1/a;->g:Ljava/util/List;

    const/16 v0, 0xff

    iput v0, p0, Lj1/a;->k:I

    iget-object v0, p1, Lj1/a$c;->a:Ljava/lang/String;

    iput-object v0, p0, Lj1/a;->a:Ljava/lang/String;

    iget-object v0, p1, Lj1/a$c;->b:Ljava/lang/String;

    iput-object v0, p0, Lj1/a;->b:Ljava/lang/String;

    iget-object v0, p1, Lj1/a$c;->c:Ljava/util/Map;

    iput-object v0, p0, Lj1/a;->c:Ljava/util/Map;

    iget-wide v0, p1, Lj1/a$c;->d:J

    iput-wide v0, p0, Lj1/a;->i:J

    iget-boolean p1, p1, Lj1/a$c;->e:Z

    iput-boolean p1, p0, Lj1/a;->j:Z

    new-instance p1, Lj1/c;

    invoke-direct {p1, p0}, Lj1/c;-><init>(Lj1/a;)V

    iput-object p1, p0, Lj1/a;->e:Lj1/c;

    new-instance p1, Landroid/os/HandlerThread;

    const-string v0, "Beacon Daemon"

    invoke-direct {p1, v0}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object p1, p0, Lj1/a;->d:Landroid/os/HandlerThread;

    invoke-virtual {p1}, Landroid/os/HandlerThread;->start()V

    invoke-direct {p0}, Lj1/a;->a()V

    return-void
.end method

.method synthetic constructor <init>(Lj1/a$c;Lj1/a$a;)V
    .locals 0

    invoke-direct {p0, p1}, Lj1/a;-><init>(Lj1/a$c;)V

    return-void
.end method

.method private a()V
    .locals 2

    new-instance v0, Lj1/a$b;

    iget-object v1, p0, Lj1/a;->d:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lj1/a$b;-><init>(Lj1/a;Landroid/os/Looper;)V

    iput-object v0, p0, Lj1/a;->h:Landroid/os/Handler;

    return-void
.end method

.method private b(Landroid/content/Context;)V
    .locals 2

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    const/4 v1, 0x1

    iput v1, v0, Landroid/os/Message;->what:I

    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object p1, p0, Lj1/a;->h:Landroid/os/Handler;

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method private d(Lj1/a$f;)V
    .locals 1

    iget-object v0, p0, Lj1/a;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private e(Lj1/a$g;)V
    .locals 1

    iget-object v0, p0, Lj1/a;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method static synthetic f(Lj1/a;)V
    .locals 0

    invoke-direct {p0}, Lj1/a;->m()V

    return-void
.end method

.method static synthetic g(Lj1/a;Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lj1/a;->s(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic h(Lj1/a;Lj1/a$e;)V
    .locals 0

    invoke-direct {p0, p1}, Lj1/a;->o(Lj1/a$e;)V

    return-void
.end method

.method static synthetic i(Lj1/a;Lj1/a$f;)V
    .locals 0

    invoke-direct {p0, p1}, Lj1/a;->d(Lj1/a$f;)V

    return-void
.end method

.method static synthetic j(Lj1/a;Lj1/a$g;)V
    .locals 0

    invoke-direct {p0, p1}, Lj1/a;->e(Lj1/a$g;)V

    return-void
.end method

.method private m()V
    .locals 1

    iget-object v0, p0, Lj1/a;->h:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quitSafely()V

    invoke-direct {p0}, Lj1/a;->a()V

    return-void
.end method

.method private n(Landroid/content/Context;)V
    .locals 2

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    const/4 v1, 0x2

    iput v1, v0, Landroid/os/Message;->what:I

    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object p1, p0, Lj1/a;->h:Landroid/os/Handler;

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method private o(Lj1/a$e;)V
    .locals 2

    iget-object v0, p0, Lj1/a;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lj1/a$f;

    invoke-interface {v1, p1}, Lj1/a$f;->a(Lj1/a$e;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private p(Lj1/a$g;)V
    .locals 1

    iget-object v0, p0, Lj1/a;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method static synthetic q(Lj1/a;Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lj1/a;->u(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic r(Lj1/a;Lj1/a$g;)V
    .locals 0

    invoke-direct {p0, p1}, Lj1/a;->p(Lj1/a$g;)V

    return-void
.end method

.method private s(Landroid/content/Context;)V
    .locals 2

    iget-boolean v0, p0, Lj1/a;->j:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lj1/a;->n(Landroid/content/Context;)V

    iput v1, p0, Lj1/a;->k:I

    goto :goto_0

    :cond_0
    iput v1, p0, Lj1/a;->k:I

    invoke-direct {p0, p1}, Lj1/a;->b(Landroid/content/Context;)V

    invoke-virtual {p0}, Lj1/a;->y()V

    const/16 p1, 0xff

    iput p1, p0, Lj1/a;->k:I

    :goto_0
    return-void
.end method

.method static synthetic t(Lj1/a;Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lj1/a;->v(Landroid/content/Context;)V

    return-void
.end method

.method private u(Landroid/content/Context;)V
    .locals 4

    iget-object v0, p0, Lj1/a;->e:Lj1/c;

    iget-object v1, p0, Lj1/a;->a:Ljava/lang/String;

    iget-object v2, p0, Lj1/a;->b:Ljava/lang/String;

    iget-object v3, p0, Lj1/a;->c:Ljava/util/Map;

    invoke-virtual {v0, p1, v1, v2, v3}, Lj1/c;->d(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    iget-object p1, p0, Lj1/a;->e:Lj1/c;

    invoke-virtual {p1}, Lj1/c;->c()Ljava/util/List;

    move-result-object p1

    iget-object v0, p0, Lj1/a;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lj1/a$g;

    invoke-interface {v1, p1}, Lj1/a$g;->a(Ljava/util/List;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private v(Landroid/content/Context;)V
    .locals 4

    iget-object v0, p0, Lj1/a;->h:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lj1/a;->h:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    :cond_0
    invoke-direct {p0, p1}, Lj1/a;->b(Landroid/content/Context;)V

    iget-object p1, p0, Lj1/a;->h:Landroid/os/Handler;

    iget-wide v2, p0, Lj1/a;->i:J

    invoke-virtual {p1, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    return-void
.end method

.method private w()Z
    .locals 2

    iget v0, p0, Lj1/a;->k:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method


# virtual methods
.method c(Lj1/a$e;)V
    .locals 2

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    goto/32 :goto_3

    nop

    :goto_2
    iput v1, v0, Landroid/os/Message;->what:I

    goto/32 :goto_4

    nop

    :goto_3
    const/4 v1, 0x7

    goto/32 :goto_2

    nop

    :goto_4
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    goto/32 :goto_5

    nop

    :goto_5
    iget-object p1, p0, Lj1/a;->h:Landroid/os/Handler;

    goto/32 :goto_6

    nop

    :goto_6
    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/32 :goto_0

    nop
.end method

.method public k(Lj1/a$f;)V
    .locals 2

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    const/4 v1, 0x6

    iput v1, v0, Landroid/os/Message;->what:I

    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object p1, p0, Lj1/a;->h:Landroid/os/Handler;

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public l(Lj1/a$g;)V
    .locals 2

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    const/4 v1, 0x4

    iput v1, v0, Landroid/os/Message;->what:I

    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object p1, p0, Lj1/a;->h:Landroid/os/Handler;

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public x(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0}, Lj1/a;->w()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    const/4 v1, 0x0

    iput v1, v0, Landroid/os/Message;->what:I

    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object p1, p0, Lj1/a;->h:Landroid/os/Handler;

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    return-void
.end method

.method public y()V
    .locals 2

    invoke-direct {p0}, Lj1/a;->w()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    const/4 v1, 0x3

    iput v1, v0, Landroid/os/Message;->what:I

    iget-object v1, p0, Lj1/a;->h:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    return-void
.end method
