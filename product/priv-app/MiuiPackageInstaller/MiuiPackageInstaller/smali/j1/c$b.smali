.class final Lj1/c$b;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lj1/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lj1/c;


# direct methods
.method private constructor <init>(Lj1/c;)V
    .locals 0

    iput-object p1, p0, Lj1/c$b;->a:Lj1/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lj1/c;Lj1/c$a;)V
    .locals 0

    invoke-direct {p0, p1}, Lj1/c$b;-><init>(Lj1/c;)V

    return-void
.end method


# virtual methods
.method a(Ljava/lang/String;[B)Ljava/lang/String;
    .locals 6

    goto/32 :goto_7

    nop

    :goto_0
    goto :goto_2

    :catch_0
    move-exception p1

    goto/32 :goto_5

    nop

    :goto_1
    return-object p1

    :catchall_0
    move-exception p1

    :goto_2
    goto/32 :goto_11

    nop

    :goto_3
    move-object v3, v0

    :goto_4
    goto/32 :goto_10

    nop

    :goto_5
    move-object v3, v0

    :goto_6
    :try_start_0
    const-string p2, "beacon"

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {p2, v1, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    iget-object p2, p0, Lj1/c$b;->a:Lj1/c;

    const-string v1, "-100"

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-static {p2, v1, p1}, Lj1/c;->e(Lj1/c;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/32 :goto_c

    nop

    :goto_7
    const/4 v0, 0x0

    :try_start_1
    new-instance v1, Ljava/net/URL;

    invoke-direct {v1, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object p1

    check-cast p1, Ljava/net/HttpURLConnection;

    const/16 v1, 0x2710

    invoke-virtual {p1, v1}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    invoke-virtual {p1, v1}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    const-string v1, "POST"

    invoke-virtual {p1, v1}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    invoke-virtual {p1, v1}, Ljava/net/HttpURLConnection;->setDoInput(Z)V

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Ljava/net/HttpURLConnection;->setUseCaches(Z)V

    sget-boolean v1, Lj1/b;->a:Z

    if-eqz v1, :cond_0

    const-string v1, "Host"

    const-string v2, "beacon-api.aliyuncs.com"

    invoke-virtual {p1, v1, v2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    invoke-virtual {v1, p2}, Ljava/io/OutputStream;->write([B)V

    invoke-virtual {v1}, Ljava/io/OutputStream;->flush()V

    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result p2

    invoke-virtual {p0, p2}, Lj1/c$b;->b(I)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object p1

    goto :goto_8

    :cond_1
    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getErrorStream()Ljava/io/InputStream;

    move-result-object p1

    :goto_8
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/InputStreamReader;

    const-string v5, "UTF-8"

    invoke-direct {v4, p1, v5}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-direct {v3, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    :try_start_3
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    :goto_9
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_9

    :cond_2
    if-nez v2, :cond_3

    iget-object v0, p0, Lj1/c$b;->a:Lj1/c;

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, p2, v2}, Lj1/c;->e(Lj1/c;Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_5
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    :try_start_4
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    :catch_1
    goto/32 :goto_17

    nop

    :goto_a
    throw p1

    :goto_b
    move-object v0, v1

    goto/32 :goto_f

    nop

    :goto_c
    if-nez v0, :cond_4

    goto/32 :goto_d

    :cond_4
    :try_start_5
    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    :goto_d
    if-eqz v3, :cond_5

    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    :catch_2
    :cond_5
    goto/32 :goto_15

    nop

    :goto_e
    goto/16 :goto_2

    :catch_3
    move-exception p1

    goto/32 :goto_18

    nop

    :goto_f
    goto/16 :goto_6

    :catchall_1
    move-exception p1

    goto/32 :goto_16

    nop

    :goto_10
    move-object v0, v1

    goto/32 :goto_e

    nop

    :goto_11
    if-nez v0, :cond_6

    goto/32 :goto_12

    :cond_6
    :try_start_6
    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    :goto_12
    if-eqz v3, :cond_7

    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    :catch_4
    :cond_7
    goto/32 :goto_a

    nop

    :goto_13
    goto/16 :goto_4

    :catch_5
    move-exception p1

    goto/32 :goto_14

    nop

    :goto_14
    goto :goto_19

    :catchall_2
    move-exception p1

    goto/32 :goto_3

    nop

    :goto_15
    const-string p1, ""

    goto/32 :goto_1

    nop

    :goto_16
    move-object v3, v0

    goto/32 :goto_0

    nop

    :goto_17
    return-object p1

    :catchall_3
    move-exception p1

    goto/32 :goto_13

    nop

    :goto_18
    move-object v3, v0

    :goto_19
    goto/32 :goto_b

    nop
.end method

.method b(I)Z
    .locals 1

    goto/32 :goto_5

    nop

    :goto_0
    const/4 p1, 0x0

    :goto_1
    goto/32 :goto_3

    nop

    :goto_2
    if-ge p1, v0, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_8

    nop

    :goto_3
    return p1

    :goto_4
    if-lt p1, v0, :cond_1

    goto/32 :goto_7

    :cond_1
    goto/32 :goto_9

    nop

    :goto_5
    const/16 v0, 0xc8

    goto/32 :goto_2

    nop

    :goto_6
    goto :goto_1

    :goto_7
    goto/32 :goto_0

    nop

    :goto_8
    const/16 v0, 0x12c

    goto/32 :goto_4

    nop

    :goto_9
    const/4 p1, 0x1

    goto/32 :goto_6

    nop
.end method
