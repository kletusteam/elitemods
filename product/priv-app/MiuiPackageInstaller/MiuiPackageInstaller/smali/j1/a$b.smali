.class final Lj1/a$b;
.super Landroid/os/Handler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lj1/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lj1/a;


# direct methods
.method constructor <init>(Lj1/a;Landroid/os/Looper;)V
    .locals 0

    iput-object p1, p0, Lj1/a$b;->a:Lj1/a;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    :try_start_0
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Lj1/a$e;

    iget-object v0, p0, Lj1/a$b;->a:Lj1/a;

    invoke-static {v0, p1}, Lj1/a;->h(Lj1/a;Lj1/a$e;)V

    goto :goto_0

    :pswitch_1
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Lj1/a$f;

    iget-object v0, p0, Lj1/a$b;->a:Lj1/a;

    invoke-static {v0, p1}, Lj1/a;->i(Lj1/a;Lj1/a$f;)V

    goto :goto_0

    :pswitch_2
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Lj1/a$g;

    iget-object v0, p0, Lj1/a$b;->a:Lj1/a;

    invoke-static {v0, p1}, Lj1/a;->r(Lj1/a;Lj1/a$g;)V

    goto :goto_0

    :pswitch_3
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Lj1/a$g;

    iget-object v0, p0, Lj1/a$b;->a:Lj1/a;

    invoke-static {v0, p1}, Lj1/a;->j(Lj1/a;Lj1/a$g;)V

    goto :goto_0

    :pswitch_4
    iget-object p1, p0, Lj1/a$b;->a:Lj1/a;

    invoke-static {p1}, Lj1/a;->f(Lj1/a;)V

    goto :goto_0

    :pswitch_5
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Landroid/content/Context;

    iget-object v0, p0, Lj1/a$b;->a:Lj1/a;

    invoke-static {v0, p1}, Lj1/a;->t(Lj1/a;Landroid/content/Context;)V

    goto :goto_0

    :pswitch_6
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Landroid/content/Context;

    iget-object v0, p0, Lj1/a$b;->a:Lj1/a;

    invoke-static {v0, p1}, Lj1/a;->q(Lj1/a;Landroid/content/Context;)V

    goto :goto_0

    :pswitch_7
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Landroid/content/Context;

    iget-object v0, p0, Lj1/a$b;->a:Lj1/a;

    invoke-static {v0, p1}, Lj1/a;->g(Lj1/a;Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    const-string v1, "beacon"

    invoke-static {v1, v0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
