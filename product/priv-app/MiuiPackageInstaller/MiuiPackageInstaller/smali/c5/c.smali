.class public final enum Lc5/c;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lc5/c;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum b:Lc5/c;

.field public static final enum c:Lc5/c;

.field public static final enum d:Lc5/c;

.field public static final enum e:Lc5/c;

.field public static final enum f:Lc5/c;

.field private static final synthetic g:[Lc5/c;


# instance fields
.field public a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 12

    new-instance v0, Lc5/c;

    const-string v1, "ROLLBACK"

    const/4 v2, 0x0

    const-string v3, " ROLLBACK "

    invoke-direct {v0, v1, v2, v3}, Lc5/c;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lc5/c;->b:Lc5/c;

    new-instance v1, Lc5/c;

    const-string v3, "ABORT"

    const/4 v4, 0x1

    const-string v5, " ABORT "

    invoke-direct {v1, v3, v4, v5}, Lc5/c;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lc5/c;->c:Lc5/c;

    new-instance v3, Lc5/c;

    const-string v5, "FAIL"

    const/4 v6, 0x2

    const-string v7, " FAIL "

    invoke-direct {v3, v5, v6, v7}, Lc5/c;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lc5/c;->d:Lc5/c;

    new-instance v5, Lc5/c;

    const-string v7, "IGNORE"

    const/4 v8, 0x3

    const-string v9, " IGNORE "

    invoke-direct {v5, v7, v8, v9}, Lc5/c;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lc5/c;->e:Lc5/c;

    new-instance v7, Lc5/c;

    const-string v9, "REPLACE"

    const/4 v10, 0x4

    const-string v11, " REPLACE "

    invoke-direct {v7, v9, v10, v11}, Lc5/c;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lc5/c;->f:Lc5/c;

    const/4 v9, 0x5

    new-array v9, v9, [Lc5/c;

    aput-object v0, v9, v2

    aput-object v1, v9, v4

    aput-object v3, v9, v6

    aput-object v5, v9, v8

    aput-object v7, v9, v10

    sput-object v9, Lc5/c;->g:[Lc5/c;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lc5/c;->a:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lc5/c;
    .locals 1

    const-class v0, Lc5/c;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lc5/c;

    return-object p0
.end method

.method public static values()[Lc5/c;
    .locals 1

    sget-object v0, Lc5/c;->g:[Lc5/c;

    invoke-virtual {v0}, [Lc5/c;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lc5/c;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lc5/c;->a:Ljava/lang/String;

    return-object v0
.end method
