.class public final Lw8/b;
.super Lw8/c;


# instance fields
.field private volatile _immediate:Lw8/b;

.field private final b:Landroid/os/Handler;

.field private final c:Ljava/lang/String;

.field private final d:Z

.field private final e:Lw8/b;


# direct methods
.method public constructor <init>(Landroid/os/Handler;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lw8/b;-><init>(Landroid/os/Handler;Ljava/lang/String;Z)V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/os/Handler;Ljava/lang/String;ILm8/g;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    :cond_0
    invoke-direct {p0, p1, p2}, Lw8/b;-><init>(Landroid/os/Handler;Ljava/lang/String;)V

    return-void
.end method

.method private constructor <init>(Landroid/os/Handler;Ljava/lang/String;Z)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lw8/c;-><init>(Lm8/g;)V

    iput-object p1, p0, Lw8/b;->b:Landroid/os/Handler;

    iput-object p2, p0, Lw8/b;->c:Ljava/lang/String;

    iput-boolean p3, p0, Lw8/b;->d:Z

    if-eqz p3, :cond_0

    move-object v0, p0

    :cond_0
    iput-object v0, p0, Lw8/b;->_immediate:Lw8/b;

    iget-object p3, p0, Lw8/b;->_immediate:Lw8/b;

    if-nez p3, :cond_1

    new-instance p3, Lw8/b;

    const/4 v0, 0x1

    invoke-direct {p3, p1, p2, v0}, Lw8/b;-><init>(Landroid/os/Handler;Ljava/lang/String;Z)V

    iput-object p3, p0, Lw8/b;->_immediate:Lw8/b;

    :cond_1
    iput-object p3, p0, Lw8/b;->e:Lw8/b;

    return-void
.end method

.method public static synthetic Z(Lw8/b;Ljava/lang/Runnable;)V
    .locals 0

    invoke-static {p0, p1}, Lw8/b;->d0(Lw8/b;Ljava/lang/Runnable;)V

    return-void
.end method

.method public static final synthetic a0(Lw8/b;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lw8/b;->b:Landroid/os/Handler;

    return-object p0
.end method

.method private final b0(Ld8/g;Ljava/lang/Runnable;)V
    .locals 3

    new-instance v0, Ljava/util/concurrent/CancellationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "The task was rejected, the handler underlying the dispatcher \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, "\' was closed"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CancellationException;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v0}, Lv8/o1;->c(Ld8/g;Ljava/util/concurrent/CancellationException;)V

    invoke-static {}, Lv8/t0;->b()Lv8/a0;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lv8/a0;->U(Ld8/g;Ljava/lang/Runnable;)V

    return-void
.end method

.method private static final d0(Lw8/b;Ljava/lang/Runnable;)V
    .locals 0

    iget-object p0, p0, Lw8/b;->b:Landroid/os/Handler;

    invoke-virtual {p0, p1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    return-void
.end method


# virtual methods
.method public D(JLv8/j;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lv8/j<",
            "-",
            "La8/v;",
            ">;)V"
        }
    .end annotation

    new-instance v0, Lw8/b$a;

    invoke-direct {v0, p3, p0}, Lw8/b$a;-><init>(Lv8/j;Lw8/b;)V

    iget-object v1, p0, Lw8/b;->b:Landroid/os/Handler;

    const-wide v2, 0x3fffffffffffffffL    # 1.9999999999999998

    invoke-static {p1, p2, v2, v3}, Lr8/d;->e(JJ)J

    move-result-wide p1

    invoke-virtual {v1, v0, p1, p2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    move-result p1

    if-eqz p1, :cond_0

    new-instance p1, Lw8/b$b;

    invoke-direct {p1, p0, v0}, Lw8/b$b;-><init>(Lw8/b;Ljava/lang/Runnable;)V

    invoke-interface {p3, p1}, Lv8/j;->k(Ll8/l;)V

    goto :goto_0

    :cond_0
    invoke-interface {p3}, Ld8/d;->c()Ld8/g;

    move-result-object p1

    invoke-direct {p0, p1, v0}, Lw8/b;->b0(Ld8/g;Ljava/lang/Runnable;)V

    :goto_0
    return-void
.end method

.method public U(Ld8/g;Ljava/lang/Runnable;)V
    .locals 1

    iget-object v0, p0, Lw8/b;->b:Landroid/os/Handler;

    invoke-virtual {v0, p2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1, p2}, Lw8/b;->b0(Ld8/g;Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.method public V(Ld8/g;)Z
    .locals 1

    iget-boolean p1, p0, Lw8/b;->d:Z

    if-eqz p1, :cond_1

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object p1

    iget-object v0, p0, Lw8/b;->b:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {p1, v0}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method public bridge synthetic X()Lv8/v1;
    .locals 1

    invoke-virtual {p0}, Lw8/b;->c0()Lw8/b;

    move-result-object v0

    return-object v0
.end method

.method public c0()Lw8/b;
    .locals 1

    iget-object v0, p0, Lw8/b;->e:Lw8/b;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    instance-of v0, p1, Lw8/b;

    if-eqz v0, :cond_0

    check-cast p1, Lw8/b;

    iget-object p1, p1, Lw8/b;->b:Landroid/os/Handler;

    iget-object v0, p0, Lw8/b;->b:Landroid/os/Handler;

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lw8/b;->b:Landroid/os/Handler;

    invoke-static {v0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public s(JLjava/lang/Runnable;Ld8/g;)Lv8/u0;
    .locals 3

    iget-object v0, p0, Lw8/b;->b:Landroid/os/Handler;

    const-wide v1, 0x3fffffffffffffffL    # 1.9999999999999998

    invoke-static {p1, p2, v1, v2}, Lr8/d;->e(JJ)J

    move-result-wide p1

    invoke-virtual {v0, p3, p1, p2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    move-result p1

    if-eqz p1, :cond_0

    new-instance p1, Lw8/a;

    invoke-direct {p1, p0, p3}, Lw8/a;-><init>(Lw8/b;Ljava/lang/Runnable;)V

    return-object p1

    :cond_0
    invoke-direct {p0, p4, p3}, Lw8/b;->b0(Ld8/g;Ljava/lang/Runnable;)V

    sget-object p1, Lv8/x1;->a:Lv8/x1;

    return-object p1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lv8/v1;->Y()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lw8/b;->c:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lw8/b;->b:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    iget-boolean v1, p0, Lw8/b;->d:Z

    if-eqz v1, :cond_1

    const-string v1, ".immediate"

    invoke-static {v0, v1}, Lm8/i;->l(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :cond_1
    return-object v0
.end method
