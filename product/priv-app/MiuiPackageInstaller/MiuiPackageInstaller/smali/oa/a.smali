.class Loa/a;
.super Loa/c$a;

# interfaces
.implements Lna/c$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Loa/a$b;
    }
.end annotation


# instance fields
.field private u:Lna/e;

.field private v:Lna/f;

.field private w:Lna/c;

.field private x:Loa/a$b;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 3

    invoke-direct {p0, p1}, Loa/c$a;-><init>(Landroid/content/Context;)V

    new-instance p1, Lna/e;

    invoke-direct {p1}, Lna/e;-><init>()V

    iput-object p1, p0, Loa/a;->u:Lna/e;

    new-instance p1, Lna/f;

    iget-object v0, p0, Loa/a;->u:Lna/e;

    invoke-direct {p1, v0}, Lna/f;-><init>(Lna/e;)V

    iput-object p1, p0, Loa/a;->v:Lna/f;

    new-instance v0, Lna/g;

    invoke-direct {v0}, Lna/g;-><init>()V

    invoke-virtual {p1, v0}, Lna/f;->x(Lna/g;)Lna/f;

    iget-object p1, p0, Loa/a;->v:Lna/f;

    const/high16 v0, 0x3f000000    # 0.5f

    invoke-virtual {p1, v0}, Lna/b;->m(F)Lna/b;

    iget-object p1, p0, Loa/a;->v:Lna/f;

    invoke-virtual {p1}, Lna/f;->u()Lna/g;

    move-result-object p1

    const v1, 0x3f7851ec    # 0.97f

    invoke-virtual {p1, v1}, Lna/g;->d(F)Lna/g;

    iget-object p1, p0, Loa/a;->v:Lna/f;

    invoke-virtual {p1}, Lna/f;->u()Lna/g;

    move-result-object p1

    const v1, 0x43028000    # 130.5f

    invoke-virtual {p1, v1}, Lna/g;->f(F)Lna/g;

    iget-object p1, p0, Loa/a;->v:Lna/f;

    invoke-virtual {p1}, Lna/f;->u()Lna/g;

    move-result-object p1

    const-wide v1, 0x408f400000000000L    # 1000.0

    invoke-virtual {p1, v1, v2}, Lna/g;->g(D)Lna/g;

    new-instance p1, Lna/c;

    iget-object v1, p0, Loa/a;->u:Lna/e;

    invoke-direct {p1, v1, p0}, Lna/c;-><init>(Lna/e;Lna/c$b;)V

    iput-object p1, p0, Loa/a;->w:Lna/c;

    invoke-virtual {p1, v0}, Lna/b;->m(F)Lna/b;

    iget-object p1, p0, Loa/a;->w:Lna/c;

    const v0, 0x3ef3cf3e

    invoke-virtual {p1, v0}, Lna/c;->z(F)Lna/c;

    return-void
.end method

.method static synthetic I(Loa/a;)Loa/a$b;
    .locals 0

    iget-object p0, p0, Loa/a;->x:Loa/a$b;

    return-object p0
.end method

.method static synthetic J(Loa/a;)Lna/c;
    .locals 0

    iget-object p0, p0, Loa/a;->w:Lna/c;

    return-object p0
.end method

.method static synthetic K(Loa/a;)V
    .locals 0

    invoke-direct {p0}, Loa/a;->O()V

    return-void
.end method

.method static synthetic L(Loa/a;IIFII)V
    .locals 0

    invoke-direct/range {p0 .. p5}, Loa/a;->N(IIFII)V

    return-void
.end method

.method private M(IIIII)V
    .locals 6

    iget-object v0, p0, Loa/a;->w:Lna/c;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lna/b;->o(F)Lna/b;

    iget-object v0, p0, Loa/a;->w:Lna/c;

    int-to-float p2, p2

    invoke-virtual {v0, p2}, Lna/c;->C(F)Lna/c;

    int-to-long v0, p1

    iget-object v2, p0, Loa/a;->w:Lna/c;

    invoke-virtual {v2}, Lna/c;->w()F

    move-result v2

    float-to-long v2, v2

    add-long/2addr v0, v2

    int-to-long v2, p4

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    iget-object v0, p0, Loa/a;->w:Lna/c;

    sub-int v1, p4, p1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lna/c;->x(F)F

    move-result v0

    float-to-int v0, v0

    move v1, p4

    goto :goto_0

    :cond_0
    int-to-long v2, p3

    cmp-long v2, v0, v2

    if-gez v2, :cond_1

    iget-object v0, p0, Loa/a;->w:Lna/c;

    sub-int v1, p3, p1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lna/c;->x(F)F

    move-result v0

    float-to-int v0, v0

    move v1, p3

    goto :goto_0

    :cond_1
    long-to-int v0, v0

    iget-object v1, p0, Loa/a;->w:Lna/c;

    invoke-virtual {v1}, Lna/c;->v()F

    move-result v1

    float-to-int v1, v1

    move v5, v1

    move v1, v0

    move v0, v5

    :goto_0
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Loa/c$a;->z(Z)V

    invoke-virtual {p0, p2}, Loa/c$a;->u(F)V

    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v3

    invoke-virtual {p0, v3, v4}, Loa/c$a;->B(J)V

    invoke-virtual {p0, p1}, Loa/c$a;->v(I)V

    invoke-virtual {p0, p1}, Loa/c$a;->A(I)V

    invoke-virtual {p0, v0}, Loa/c$a;->w(I)V

    invoke-virtual {p0, v1}, Loa/c$a;->x(I)V

    invoke-virtual {p0, v2}, Loa/c$a;->C(I)V

    invoke-static {p3, p1}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {p4, p1}, Ljava/lang/Math;->max(II)I

    move-result v1

    new-instance v2, Loa/a$b;

    iget-object v3, p0, Loa/a;->w:Lna/c;

    invoke-direct {v2, v3, p1, p2}, Loa/a$b;-><init>(Lna/b;IF)V

    iput-object v2, p0, Loa/a;->x:Loa/a$b;

    new-instance p1, Loa/a$a;

    invoke-direct {p1, p0, p3, p4, p5}, Loa/a$a;-><init>(Loa/a;III)V

    invoke-virtual {v2, p1}, Loa/a$b;->i(Loa/a$b$b;)V

    iget-object p1, p0, Loa/a;->x:Loa/a$b;

    invoke-virtual {p1, v0}, Loa/a$b;->h(I)V

    iget-object p1, p0, Loa/a;->x:Loa/a$b;

    invoke-virtual {p1, v1}, Loa/a$b;->g(I)V

    iget-object p1, p0, Loa/a;->x:Loa/a$b;

    invoke-virtual {p1}, Loa/a$b;->j()V

    return-void
.end method

.method private N(IIFII)V
    .locals 3

    const/high16 v0, 0x45fa0000    # 8000.0f

    cmpl-float v1, p3, v0

    const/4 v2, 0x0

    if-lez v1, :cond_0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p3

    aput-object p3, v1, v2

    const-string p3, "%f is too fast for spring, slow down"

    invoke-static {p3, v1}, Loa/b;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    move p3, v0

    :cond_0
    invoke-virtual {p0, v2}, Loa/c$a;->z(Z)V

    invoke-virtual {p0, p3}, Loa/c$a;->u(F)V

    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Loa/c$a;->B(J)V

    invoke-virtual {p0, p2}, Loa/c$a;->v(I)V

    invoke-virtual {p0, p2}, Loa/c$a;->A(I)V

    const v0, 0x7fffffff

    invoke-virtual {p0, v0}, Loa/c$a;->w(I)V

    invoke-virtual {p0, p4}, Loa/c$a;->x(I)V

    invoke-virtual {p0, p1}, Loa/c$a;->C(I)V

    new-instance p1, Loa/a$b;

    iget-object v0, p0, Loa/a;->v:Lna/f;

    invoke-direct {p1, v0, p2, p3}, Loa/a$b;-><init>(Lna/b;IF)V

    iput-object p1, p0, Loa/a;->x:Loa/a$b;

    iget-object p1, p0, Loa/a;->v:Lna/f;

    invoke-virtual {p1}, Lna/f;->u()Lna/g;

    move-result-object p1

    iget-object v0, p0, Loa/a;->x:Loa/a$b;

    invoke-virtual {v0, p4}, Loa/a$b;->f(I)I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p1, v0}, Lna/g;->e(F)Lna/g;

    if-eqz p5, :cond_2

    const/4 p1, 0x0

    cmpg-float p1, p3, p1

    if-gez p1, :cond_1

    iget-object p1, p0, Loa/a;->x:Loa/a$b;

    sub-int p3, p4, p5

    invoke-virtual {p1, p3}, Loa/a$b;->h(I)V

    iget-object p1, p0, Loa/a;->x:Loa/a$b;

    invoke-static {p4, p2}, Ljava/lang/Math;->max(II)I

    move-result p2

    invoke-virtual {p1, p2}, Loa/a$b;->g(I)V

    goto :goto_0

    :cond_1
    iget-object p1, p0, Loa/a;->x:Loa/a$b;

    invoke-static {p4, p2}, Ljava/lang/Math;->min(II)I

    move-result p2

    invoke-virtual {p1, p2}, Loa/a$b;->h(I)V

    iget-object p1, p0, Loa/a;->x:Loa/a$b;

    add-int/2addr p4, p5

    invoke-virtual {p1, p4}, Loa/a$b;->g(I)V

    :cond_2
    :goto_0
    iget-object p1, p0, Loa/a;->x:Loa/a$b;

    invoke-virtual {p1}, Loa/a$b;->j()V

    return-void
.end method

.method private O()V
    .locals 3

    iget-object v0, p0, Loa/a;->x:Loa/a$b;

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {p0}, Loa/c$a;->q()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Loa/a;->x:Loa/a$b;

    invoke-virtual {v2}, Loa/a$b;->e()Lna/b;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Loa/a;->x:Loa/a$b;

    iget v2, v2, Loa/a$b;->f:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Loa/a;->x:Loa/a$b;

    iget v2, v2, Loa/a$b;->e:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const-string v1, "resetting current handler: state(%d), anim(%s), value(%d), velocity(%f)"

    invoke-static {v1, v0}, Loa/b;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Loa/a;->x:Loa/a$b;

    invoke-virtual {v0}, Loa/a$b;->c()V

    const/4 v0, 0x0

    iput-object v0, p0, Loa/a;->x:Loa/a$b;

    :cond_0
    return-void
.end method

.method private P(IIIII)V
    .locals 10

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v3, 0x1

    aput-object v1, v0, v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v4, 0x2

    aput-object v1, v0, v4

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v4, 0x3

    aput-object v1, v0, v4

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v4, 0x4

    aput-object v1, v0, v4

    const-string v1, "startAfterEdge: start(%d) velocity(%d) boundary(%d, %d) over(%d)"

    invoke-static {v1, v0}, Loa/b;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    if-le p1, p2, :cond_0

    if-ge p1, p3, :cond_0

    invoke-virtual {p0, v3}, Loa/c$a;->z(Z)V

    return-void

    :cond_0
    if-le p1, p3, :cond_1

    move v0, v3

    goto :goto_0

    :cond_1
    move v0, v2

    :goto_0
    if-eqz v0, :cond_2

    move v8, p3

    goto :goto_1

    :cond_2
    move v8, p2

    :goto_1
    sub-int v1, p1, v8

    if-eqz p4, :cond_3

    invoke-static {v1}, Ljava/lang/Integer;->signum(I)I

    move-result v1

    mul-int/2addr v1, p4

    if-ltz v1, :cond_3

    move v2, v3

    :cond_3
    if-eqz v2, :cond_4

    const-string p2, "spring forward"

    invoke-static {p2}, Loa/b;->a(Ljava/lang/String;)V

    const/4 v5, 0x2

    int-to-float v7, p4

    :goto_2
    move-object v4, p0

    move v6, p1

    move v9, p5

    invoke-direct/range {v4 .. v9}, Loa/a;->N(IIFII)V

    goto :goto_3

    :cond_4
    iget-object v1, p0, Loa/a;->w:Lna/c;

    int-to-float v2, p1

    invoke-virtual {v1, v2}, Lna/b;->o(F)Lna/b;

    iget-object v1, p0, Loa/a;->w:Lna/c;

    int-to-float v7, p4

    invoke-virtual {v1, v7}, Lna/c;->C(F)Lna/c;

    iget-object v1, p0, Loa/a;->w:Lna/c;

    invoke-virtual {v1}, Lna/c;->w()F

    move-result v1

    if-eqz v0, :cond_5

    int-to-float v2, p3

    cmpg-float v2, v1, v2

    if-ltz v2, :cond_6

    :cond_5
    if-nez v0, :cond_7

    int-to-float v0, p2

    cmpl-float v0, v1, v0

    if-lez v0, :cond_7

    :cond_6
    const-string v0, "fling to content"

    invoke-static {v0}, Loa/b;->a(Ljava/lang/String;)V

    move-object v0, p0

    move v1, p1

    move v2, p4

    move v3, p2

    move v4, p3

    move v5, p5

    invoke-direct/range {v0 .. v5}, Loa/a;->M(IIIII)V

    goto :goto_3

    :cond_7
    const-string p2, "spring backward"

    invoke-static {p2}, Loa/b;->a(Ljava/lang/String;)V

    const/4 v5, 0x1

    goto :goto_2

    :goto_3
    return-void
.end method


# virtual methods
.method D(III)Z
    .locals 7

    goto/32 :goto_10

    nop

    :goto_0
    goto/16 :goto_27

    :goto_1
    goto/32 :goto_2d

    nop

    :goto_2
    const/4 v4, 0x2

    goto/32 :goto_4

    nop

    :goto_3
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto/32 :goto_2

    nop

    :goto_4
    aput-object v1, v0, v4

    goto/32 :goto_9

    nop

    :goto_5
    invoke-direct {p0}, Loa/a;->O()V

    :goto_6
    goto/32 :goto_29

    nop

    :goto_7
    const/4 v5, 0x0

    goto/32 :goto_15

    nop

    :goto_8
    invoke-static {v1, v0}, Loa/b;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/32 :goto_12

    nop

    :goto_9
    const-string v1, "SPRING_BACK start(%d) boundary(%d, %d)"

    goto/32 :goto_8

    nop

    :goto_a
    invoke-virtual {p0, p1}, Loa/c$a;->x(I)V

    goto/32 :goto_24

    nop

    :goto_b
    move v2, p1

    goto/32 :goto_14

    nop

    :goto_c
    invoke-virtual {p0, p1}, Loa/c$a;->v(I)V

    goto/32 :goto_19

    nop

    :goto_d
    aput-object v1, v0, v6

    goto/32 :goto_3

    nop

    :goto_e
    aput-object v1, v0, v3

    goto/32 :goto_11

    nop

    :goto_f
    const/4 v3, 0x0

    goto/32 :goto_2c

    nop

    :goto_10
    const/4 v0, 0x3

    goto/32 :goto_1c

    nop

    :goto_11
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto/32 :goto_16

    nop

    :goto_12
    iget-object v0, p0, Loa/a;->x:Loa/a$b;

    goto/32 :goto_20

    nop

    :goto_13
    move-object v0, p0

    goto/32 :goto_b

    nop

    :goto_14
    move v4, p3

    goto/32 :goto_21

    nop

    :goto_15
    move-object v0, p0

    goto/32 :goto_25

    nop

    :goto_16
    const/4 v6, 0x1

    goto/32 :goto_d

    nop

    :goto_17
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto/32 :goto_23

    nop

    :goto_18
    return v0

    :goto_19
    invoke-virtual {p0, p1}, Loa/c$a;->A(I)V

    goto/32 :goto_a

    nop

    :goto_1a
    invoke-virtual {p0}, Loa/c$a;->s()Z

    move-result v0

    goto/32 :goto_2b

    nop

    :goto_1b
    const/4 v1, 0x1

    goto/32 :goto_28

    nop

    :goto_1c
    new-array v0, v0, [Ljava/lang/Object;

    goto/32 :goto_17

    nop

    :goto_1d
    move v4, p2

    :goto_1e
    goto/32 :goto_1f

    nop

    :goto_1f
    invoke-direct/range {v0 .. v5}, Loa/a;->N(IIFII)V

    goto/32 :goto_0

    nop

    :goto_20
    if-nez v0, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_5

    nop

    :goto_21
    goto :goto_1e

    :goto_22
    goto/32 :goto_c

    nop

    :goto_23
    const/4 v3, 0x0

    goto/32 :goto_e

    nop

    :goto_24
    invoke-virtual {p0, v3}, Loa/c$a;->w(I)V

    goto/32 :goto_26

    nop

    :goto_25
    move v2, p1

    goto/32 :goto_1d

    nop

    :goto_26
    invoke-virtual {p0, v6}, Loa/c$a;->z(Z)V

    :goto_27
    goto/32 :goto_1a

    nop

    :goto_28
    const/4 v3, 0x0

    goto/32 :goto_7

    nop

    :goto_29
    if-lt p1, p2, :cond_1

    goto/32 :goto_1

    :cond_1
    goto/32 :goto_1b

    nop

    :goto_2a
    const/4 v1, 0x1

    goto/32 :goto_f

    nop

    :goto_2b
    xor-int/2addr v0, v6

    goto/32 :goto_18

    nop

    :goto_2c
    const/4 v5, 0x0

    goto/32 :goto_13

    nop

    :goto_2d
    if-gt p1, p3, :cond_2

    goto/32 :goto_22

    :cond_2
    goto/32 :goto_2a

    nop
.end method

.method G()Z
    .locals 4

    goto/32 :goto_19

    nop

    :goto_0
    if-ltz v1, :cond_0

    goto/32 :goto_12

    :cond_0
    goto/32 :goto_5

    nop

    :goto_1
    return v0

    :goto_2
    goto/32 :goto_c

    nop

    :goto_3
    if-eq v1, v2, :cond_1

    goto/32 :goto_12

    :cond_1
    goto/32 :goto_d

    nop

    :goto_4
    iget v2, v2, Loa/a$b;->e:F

    goto/32 :goto_9

    nop

    :goto_5
    const-string v1, "State Changed: BALLISTIC -> CUBIC"

    goto/32 :goto_22

    nop

    :goto_6
    cmpg-float v1, v1, v2

    goto/32 :goto_0

    nop

    :goto_7
    if-eqz v0, :cond_2

    goto/32 :goto_2

    :cond_2
    goto/32 :goto_18

    nop

    :goto_8
    xor-int/2addr v0, v3

    goto/32 :goto_1f

    nop

    :goto_9
    invoke-static {v2}, Ljava/lang/Math;->signum(F)F

    move-result v2

    goto/32 :goto_21

    nop

    :goto_a
    invoke-virtual {p0, v1}, Loa/c$a;->v(I)V

    goto/32 :goto_17

    nop

    :goto_b
    iget v1, v1, Loa/a$b;->f:I

    goto/32 :goto_a

    nop

    :goto_c
    invoke-virtual {v0}, Loa/a$b;->k()Z

    move-result v0

    goto/32 :goto_16

    nop

    :goto_d
    iget-object v1, p0, Loa/a;->x:Loa/a$b;

    goto/32 :goto_1e

    nop

    :goto_e
    const/4 v2, 0x2

    goto/32 :goto_1c

    nop

    :goto_f
    const/4 v0, 0x0

    goto/32 :goto_1

    nop

    :goto_10
    int-to-float v1, v1

    goto/32 :goto_13

    nop

    :goto_11
    invoke-virtual {p0, v3}, Loa/c$a;->C(I)V

    :goto_12
    goto/32 :goto_8

    nop

    :goto_13
    invoke-static {v1}, Ljava/lang/Math;->signum(F)F

    move-result v1

    goto/32 :goto_15

    nop

    :goto_14
    invoke-static {v0}, Loa/b;->a(Ljava/lang/String;)V

    goto/32 :goto_f

    nop

    :goto_15
    iget-object v2, p0, Loa/a;->x:Loa/a$b;

    goto/32 :goto_4

    nop

    :goto_16
    iget-object v1, p0, Loa/a;->x:Loa/a$b;

    goto/32 :goto_b

    nop

    :goto_17
    iget-object v1, p0, Loa/a;->x:Loa/a$b;

    goto/32 :goto_1b

    nop

    :goto_18
    const-string v0, "no handler found, aborting"

    goto/32 :goto_14

    nop

    :goto_19
    iget-object v0, p0, Loa/a;->x:Loa/a$b;

    goto/32 :goto_7

    nop

    :goto_1a
    invoke-virtual {p0, v1}, Loa/c$a;->u(F)V

    goto/32 :goto_1d

    nop

    :goto_1b
    iget v1, v1, Loa/a$b;->e:F

    goto/32 :goto_1a

    nop

    :goto_1c
    const/4 v3, 0x1

    goto/32 :goto_3

    nop

    :goto_1d
    invoke-virtual {p0}, Loa/c$a;->q()I

    move-result v1

    goto/32 :goto_e

    nop

    :goto_1e
    iget v1, v1, Loa/a$b;->f:I

    goto/32 :goto_10

    nop

    :goto_1f
    return v0

    :goto_20
    const/4 v2, 0x0

    goto/32 :goto_6

    nop

    :goto_21
    mul-float/2addr v1, v2

    goto/32 :goto_20

    nop

    :goto_22
    invoke-static {v1}, Loa/b;->a(Ljava/lang/String;)V

    goto/32 :goto_11

    nop
.end method

.method public Q(D)V
    .locals 2

    invoke-static {p1, p2}, Ljava/lang/Math;->abs(D)D

    move-result-wide p1

    const-wide v0, 0x40b3880000000000L    # 5000.0

    cmpg-double p1, p1, v0

    if-gtz p1, :cond_0

    iget-object p1, p0, Loa/a;->v:Lna/f;

    invoke-virtual {p1}, Lna/f;->u()Lna/g;

    move-result-object p1

    const p2, 0x4376b333    # 246.7f

    goto :goto_0

    :cond_0
    iget-object p1, p0, Loa/a;->v:Lna/f;

    invoke-virtual {p1}, Lna/f;->u()Lna/g;

    move-result-object p1

    const p2, 0x43028000    # 130.5f

    :goto_0
    invoke-virtual {p1, p2}, Lna/g;->f(F)Lna/g;

    return-void
.end method

.method public a(I)V
    .locals 1

    invoke-virtual {p0}, Loa/c$a;->p()I

    move-result v0

    add-int/2addr v0, p1

    invoke-virtual {p0, v0}, Loa/a;->y(I)V

    return-void
.end method

.method j()Z
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    invoke-static {v0}, Loa/b;->a(Ljava/lang/String;)V

    goto/32 :goto_8

    nop

    :goto_1
    return v0

    :goto_2
    iget-object v0, p0, Loa/a;->x:Loa/a$b;

    goto/32 :goto_9

    nop

    :goto_3
    return v0

    :goto_4
    goto/32 :goto_a

    nop

    :goto_5
    const/4 v0, 0x1

    goto/32 :goto_3

    nop

    :goto_6
    const-string v0, "checking have more work when finish"

    goto/32 :goto_0

    nop

    :goto_7
    invoke-virtual {v0}, Loa/a$b;->d()Z

    move-result v0

    goto/32 :goto_b

    nop

    :goto_8
    invoke-virtual {p0}, Loa/a;->G()Z

    goto/32 :goto_5

    nop

    :goto_9
    if-nez v0, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_7

    nop

    :goto_a
    const/4 v0, 0x0

    goto/32 :goto_1

    nop

    :goto_b
    if-nez v0, :cond_1

    goto/32 :goto_4

    :cond_1
    goto/32 :goto_6

    nop
.end method

.method k()V
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    const-string v0, "finish scroller"

    goto/32 :goto_5

    nop

    :goto_1
    const/4 v0, 0x1

    goto/32 :goto_7

    nop

    :goto_2
    invoke-virtual {p0, v0}, Loa/c$a;->v(I)V

    goto/32 :goto_1

    nop

    :goto_3
    return-void

    :goto_4
    invoke-virtual {p0}, Loa/c$a;->o()I

    move-result v0

    goto/32 :goto_2

    nop

    :goto_5
    invoke-static {v0}, Loa/b;->a(Ljava/lang/String;)V

    goto/32 :goto_4

    nop

    :goto_6
    invoke-direct {p0}, Loa/a;->O()V

    goto/32 :goto_3

    nop

    :goto_7
    invoke-virtual {p0, v0}, Loa/c$a;->z(Z)V

    goto/32 :goto_6

    nop
.end method

.method l(IIIII)V
    .locals 6

    goto/32 :goto_18

    nop

    :goto_0
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto/32 :goto_4

    nop

    :goto_1
    move v2, p3

    goto/32 :goto_2b

    nop

    :goto_2
    if-eqz p2, :cond_0

    goto/32 :goto_26

    :cond_0
    goto/32 :goto_15

    nop

    :goto_3
    aput-object v1, v0, v4

    goto/32 :goto_21

    nop

    :goto_4
    const/4 v3, 0x1

    goto/32 :goto_23

    nop

    :goto_5
    invoke-static {v1, v0}, Loa/b;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/32 :goto_16

    nop

    :goto_6
    const/4 v4, 0x3

    goto/32 :goto_1c

    nop

    :goto_7
    int-to-double v0, p2

    goto/32 :goto_12

    nop

    :goto_8
    if-le p1, p4, :cond_1

    goto/32 :goto_1e

    :cond_1
    goto/32 :goto_2a

    nop

    :goto_9
    invoke-virtual {p0, v3}, Loa/c$a;->z(Z)V

    goto/32 :goto_25

    nop

    :goto_a
    invoke-direct/range {p0 .. p5}, Loa/a;->M(IIIII)V

    goto/32 :goto_1d

    nop

    :goto_b
    new-array v0, v0, [Ljava/lang/Object;

    goto/32 :goto_2c

    nop

    :goto_c
    const-string v1, "FLING: start(%d) velocity(%d) boundary(%d, %d) over(%d)"

    goto/32 :goto_5

    nop

    :goto_d
    const/4 v4, 0x4

    goto/32 :goto_11

    nop

    :goto_e
    goto :goto_1e

    :goto_f
    goto/32 :goto_a

    nop

    :goto_10
    move v5, p5

    goto/32 :goto_14

    nop

    :goto_11
    aput-object v1, v0, v4

    goto/32 :goto_c

    nop

    :goto_12
    invoke-virtual {p0, v0, v1}, Loa/a;->Q(D)V

    goto/32 :goto_8

    nop

    :goto_13
    move v1, p1

    goto/32 :goto_1

    nop

    :goto_14
    invoke-direct/range {v0 .. v5}, Loa/a;->P(IIIII)V

    goto/32 :goto_1f

    nop

    :goto_15
    invoke-virtual {p0, p1}, Loa/c$a;->v(I)V

    goto/32 :goto_17

    nop

    :goto_16
    invoke-direct {p0}, Loa/a;->O()V

    goto/32 :goto_2

    nop

    :goto_17
    invoke-virtual {p0, p1}, Loa/c$a;->A(I)V

    goto/32 :goto_28

    nop

    :goto_18
    const/4 v0, 0x5

    goto/32 :goto_b

    nop

    :goto_19
    invoke-virtual {p0, v2}, Loa/c$a;->w(I)V

    goto/32 :goto_9

    nop

    :goto_1a
    const/4 v4, 0x2

    goto/32 :goto_3

    nop

    :goto_1b
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto/32 :goto_1a

    nop

    :goto_1c
    aput-object v1, v0, v4

    goto/32 :goto_29

    nop

    :goto_1d
    return-void

    :goto_1e
    goto/32 :goto_22

    nop

    :goto_1f
    return-void

    :goto_20
    aput-object v1, v0, v2

    goto/32 :goto_0

    nop

    :goto_21
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto/32 :goto_6

    nop

    :goto_22
    move-object v0, p0

    goto/32 :goto_13

    nop

    :goto_23
    aput-object v1, v0, v3

    goto/32 :goto_1b

    nop

    :goto_24
    move v4, p2

    goto/32 :goto_10

    nop

    :goto_25
    return-void

    :goto_26
    goto/32 :goto_7

    nop

    :goto_27
    const/4 v2, 0x0

    goto/32 :goto_20

    nop

    :goto_28
    invoke-virtual {p0, p1}, Loa/c$a;->x(I)V

    goto/32 :goto_19

    nop

    :goto_29
    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto/32 :goto_d

    nop

    :goto_2a
    if-lt p1, p3, :cond_2

    goto/32 :goto_f

    :cond_2
    goto/32 :goto_e

    nop

    :goto_2b
    move v3, p4

    goto/32 :goto_24

    nop

    :goto_2c
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto/32 :goto_27

    nop
.end method

.method t(III)V
    .locals 7

    goto/32 :goto_f

    nop

    :goto_0
    move v3, p2

    goto/32 :goto_c

    nop

    :goto_1
    invoke-virtual {p0}, Loa/c$a;->m()F

    move-result v0

    goto/32 :goto_9

    nop

    :goto_2
    move v6, p3

    goto/32 :goto_a

    nop

    :goto_3
    return-void

    :goto_4
    move-object v1, p0

    goto/32 :goto_d

    nop

    :goto_5
    if-eqz v0, :cond_0

    goto/32 :goto_b

    :cond_0
    goto/32 :goto_e

    nop

    :goto_6
    if-nez v0, :cond_1

    goto/32 :goto_8

    :cond_1
    goto/32 :goto_7

    nop

    :goto_7
    invoke-direct {p0}, Loa/a;->O()V

    :goto_8
    goto/32 :goto_1

    nop

    :goto_9
    float-to-int v5, v0

    goto/32 :goto_4

    nop

    :goto_a
    invoke-direct/range {v1 .. v6}, Loa/a;->P(IIIII)V

    :goto_b
    goto/32 :goto_3

    nop

    :goto_c
    move v4, p2

    goto/32 :goto_2

    nop

    :goto_d
    move v2, p1

    goto/32 :goto_0

    nop

    :goto_e
    iget-object v0, p0, Loa/a;->x:Loa/a$b;

    goto/32 :goto_6

    nop

    :goto_f
    invoke-virtual {p0}, Loa/c$a;->q()I

    move-result v0

    goto/32 :goto_5

    nop
.end method

.method y(I)V
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    invoke-super {p0, p1}, Loa/c$a;->y(I)V

    goto/32 :goto_0

    nop
.end method
