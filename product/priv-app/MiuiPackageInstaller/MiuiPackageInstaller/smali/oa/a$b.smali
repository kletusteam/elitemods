.class Loa/a$b;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Loa/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Loa/a$b$b;,
        Loa/a$b$a;
    }
.end annotation


# instance fields
.field a:Lna/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lna/b<",
            "*>;"
        }
    .end annotation
.end field

.field b:I

.field private final c:I

.field private final d:I

.field e:F

.field f:I

.field private g:Loa/a$b$b;

.field private h:F

.field private i:F

.field private j:J

.field private k:Loa/a$b$a;


# direct methods
.method constructor <init>(Lna/b;IF)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lna/b<",
            "*>;IF)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Loa/a$b$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Loa/a$b$a;-><init>(Loa/a$b;Loa/a$a;)V

    iput-object v0, p0, Loa/a$b;->k:Loa/a$b$a;

    iput-object p1, p0, Loa/a$b;->a:Lna/b;

    const v0, -0x800001

    invoke-virtual {p1, v0}, Lna/b;->l(F)Lna/b;

    iget-object p1, p0, Loa/a$b;->a:Lna/b;

    const v0, 0x7f7fffff    # Float.MAX_VALUE

    invoke-virtual {p1, v0}, Lna/b;->k(F)Lna/b;

    iput p2, p0, Loa/a$b;->b:I

    iput p3, p0, Loa/a$b;->e:F

    const p1, 0x7fffffff

    const/high16 v0, -0x80000000

    if-lez p2, :cond_0

    add-int/2addr v0, p2

    goto :goto_0

    :cond_0
    if-gez p2, :cond_1

    add-int/2addr p1, p2

    :cond_1
    :goto_0
    iput v0, p0, Loa/a$b;->c:I

    iput p1, p0, Loa/a$b;->d:I

    iget-object p1, p0, Loa/a$b;->a:Lna/b;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Lna/b;->o(F)Lna/b;

    iget-object p1, p0, Loa/a$b;->a:Lna/b;

    invoke-virtual {p1, p3}, Lna/b;->p(F)Lna/b;

    return-void
.end method

.method static synthetic a(Loa/a$b;)F
    .locals 0

    iget p0, p0, Loa/a$b;->h:F

    return p0
.end method

.method static synthetic b(Loa/a$b;)F
    .locals 0

    iget p0, p0, Loa/a$b;->i:F

    return p0
.end method


# virtual methods
.method c()V
    .locals 2

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    const-wide/16 v0, 0x0

    goto/32 :goto_2

    nop

    :goto_2
    iput-wide v0, p0, Loa/a$b;->j:J

    goto/32 :goto_7

    nop

    :goto_3
    invoke-virtual {v0, v1}, Lna/b;->j(Lna/b$r;)V

    goto/32 :goto_0

    nop

    :goto_4
    invoke-virtual {v0}, Lna/b;->c()V

    goto/32 :goto_6

    nop

    :goto_5
    iget-object v1, p0, Loa/a$b;->k:Loa/a$b$a;

    goto/32 :goto_3

    nop

    :goto_6
    iget-object v0, p0, Loa/a$b;->a:Lna/b;

    goto/32 :goto_5

    nop

    :goto_7
    iget-object v0, p0, Loa/a$b;->a:Lna/b;

    goto/32 :goto_4

    nop
.end method

.method d()Z
    .locals 3

    goto/32 :goto_2

    nop

    :goto_0
    iget v1, p0, Loa/a$b;->f:I

    goto/32 :goto_8

    nop

    :goto_1
    const/4 v0, 0x0

    goto/32 :goto_4

    nop

    :goto_2
    iget-object v0, p0, Loa/a$b;->g:Loa/a$b$b;

    goto/32 :goto_3

    nop

    :goto_3
    if-nez v0, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_0

    nop

    :goto_4
    return v0

    :goto_5
    iget v2, p0, Loa/a$b;->e:F

    goto/32 :goto_9

    nop

    :goto_6
    return v0

    :goto_7
    goto/32 :goto_1

    nop

    :goto_8
    int-to-float v1, v1

    goto/32 :goto_5

    nop

    :goto_9
    invoke-interface {v0, v1, v2}, Loa/a$b$b;->a(FF)Z

    move-result v0

    goto/32 :goto_6

    nop
.end method

.method e()Lna/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lna/b<",
            "*>;"
        }
    .end annotation

    goto/32 :goto_1

    nop

    :goto_0
    return-object v0

    :goto_1
    iget-object v0, p0, Loa/a$b;->a:Lna/b;

    goto/32 :goto_0

    nop
.end method

.method f(I)I
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget v0, p0, Loa/a$b;->b:I

    goto/32 :goto_1

    nop

    :goto_1
    sub-int/2addr p1, v0

    goto/32 :goto_2

    nop

    :goto_2
    return p1
.end method

.method g(I)V
    .locals 1

    goto/32 :goto_7

    nop

    :goto_0
    sub-int/2addr p1, v0

    goto/32 :goto_a

    nop

    :goto_1
    iput p1, p0, Loa/a$b;->i:F

    goto/32 :goto_8

    nop

    :goto_2
    invoke-virtual {v0, p1}, Lna/b;->k(F)Lna/b;

    goto/32 :goto_1

    nop

    :goto_3
    iget-object v0, p0, Loa/a$b;->a:Lna/b;

    goto/32 :goto_2

    nop

    :goto_4
    iget v0, p0, Loa/a$b;->b:I

    goto/32 :goto_0

    nop

    :goto_5
    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result p1

    goto/32 :goto_9

    nop

    :goto_6
    if-gt p1, v0, :cond_0

    goto/32 :goto_c

    :cond_0
    goto/32 :goto_b

    nop

    :goto_7
    iget v0, p0, Loa/a$b;->d:I

    goto/32 :goto_6

    nop

    :goto_8
    return-void

    :goto_9
    int-to-float p1, p1

    goto/32 :goto_3

    nop

    :goto_a
    const/4 v0, 0x0

    goto/32 :goto_5

    nop

    :goto_b
    move p1, v0

    :goto_c
    goto/32 :goto_4

    nop
.end method

.method h(I)V
    .locals 1

    goto/32 :goto_b

    nop

    :goto_0
    sub-int/2addr p1, v0

    goto/32 :goto_7

    nop

    :goto_1
    invoke-virtual {v0, p1}, Lna/b;->l(F)Lna/b;

    goto/32 :goto_a

    nop

    :goto_2
    iget v0, p0, Loa/a$b;->b:I

    goto/32 :goto_0

    nop

    :goto_3
    move p1, v0

    :goto_4
    goto/32 :goto_2

    nop

    :goto_5
    iget-object v0, p0, Loa/a$b;->a:Lna/b;

    goto/32 :goto_1

    nop

    :goto_6
    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result p1

    goto/32 :goto_9

    nop

    :goto_7
    const/4 v0, 0x0

    goto/32 :goto_6

    nop

    :goto_8
    return-void

    :goto_9
    int-to-float p1, p1

    goto/32 :goto_5

    nop

    :goto_a
    iput p1, p0, Loa/a$b;->h:F

    goto/32 :goto_8

    nop

    :goto_b
    iget v0, p0, Loa/a$b;->c:I

    goto/32 :goto_c

    nop

    :goto_c
    if-lt p1, v0, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_3

    nop
.end method

.method i(Loa/a$b$b;)V
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    iput-object p1, p0, Loa/a$b;->g:Loa/a$b$b;

    goto/32 :goto_0

    nop
.end method

.method j()V
    .locals 2

    goto/32 :goto_8

    nop

    :goto_0
    const/4 v1, 0x1

    goto/32 :goto_3

    nop

    :goto_1
    iput-wide v0, p0, Loa/a$b;->j:J

    goto/32 :goto_4

    nop

    :goto_2
    iget-object v0, p0, Loa/a$b;->a:Lna/b;

    goto/32 :goto_0

    nop

    :goto_3
    invoke-virtual {v0, v1}, Lna/b;->r(Z)V

    goto/32 :goto_6

    nop

    :goto_4
    return-void

    :goto_5
    invoke-virtual {v0, v1}, Lna/b;->b(Lna/b$r;)Lna/b;

    goto/32 :goto_2

    nop

    :goto_6
    const-wide/16 v0, 0x0

    goto/32 :goto_1

    nop

    :goto_7
    iget-object v1, p0, Loa/a$b;->k:Loa/a$b$a;

    goto/32 :goto_5

    nop

    :goto_8
    iget-object v0, p0, Loa/a$b;->a:Lna/b;

    goto/32 :goto_7

    nop
.end method

.method k()Z
    .locals 7

    goto/32 :goto_a

    nop

    :goto_0
    return v0

    :goto_1
    invoke-virtual {v0, v2, v3}, Lna/b;->a(J)Z

    move-result v0

    goto/32 :goto_e

    nop

    :goto_2
    new-array v4, v4, [Ljava/lang/Object;

    goto/32 :goto_3

    nop

    :goto_3
    const/4 v5, 0x0

    goto/32 :goto_1d

    nop

    :goto_4
    iget-object v4, p0, Loa/a$b;->k:Loa/a$b$a;

    goto/32 :goto_18

    nop

    :goto_5
    const/4 v4, 0x3

    goto/32 :goto_2

    nop

    :goto_6
    xor-int/2addr v0, v1

    goto/32 :goto_1e

    nop

    :goto_7
    invoke-virtual {v0}, Lna/b;->g()Z

    move-result v0

    goto/32 :goto_6

    nop

    :goto_8
    iput-wide v4, p0, Loa/a$b;->j:J

    :goto_9
    goto/32 :goto_11

    nop

    :goto_a
    iget-wide v0, p0, Loa/a$b;->j:J

    goto/32 :goto_10

    nop

    :goto_b
    iget v5, p0, Loa/a$b;->f:I

    goto/32 :goto_23

    nop

    :goto_c
    invoke-static {v0}, Loa/b;->c(Ljava/lang/String;)V

    goto/32 :goto_20

    nop

    :goto_d
    aput-object v6, v4, v5

    goto/32 :goto_b

    nop

    :goto_e
    if-nez v0, :cond_0

    goto/32 :goto_9

    :cond_0
    goto/32 :goto_5

    nop

    :goto_f
    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    goto/32 :goto_19

    nop

    :goto_10
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v2

    goto/32 :goto_14

    nop

    :goto_11
    iput-wide v2, p0, Loa/a$b;->j:J

    goto/32 :goto_0

    nop

    :goto_12
    iget v5, p0, Loa/a$b;->e:F

    goto/32 :goto_25

    nop

    :goto_13
    const-wide/16 v4, 0x0

    goto/32 :goto_8

    nop

    :goto_14
    cmp-long v0, v2, v0

    goto/32 :goto_21

    nop

    :goto_15
    iget-object v0, p0, Loa/a$b;->a:Lna/b;

    goto/32 :goto_1

    nop

    :goto_16
    const-string v0, "update done in this frame, dropping current update request"

    goto/32 :goto_c

    nop

    :goto_17
    if-eqz v0, :cond_1

    goto/32 :goto_1f

    :cond_1
    goto/32 :goto_16

    nop

    :goto_18
    invoke-virtual {v1, v4}, Lna/b;->j(Lna/b$r;)V

    goto/32 :goto_13

    nop

    :goto_19
    invoke-virtual {v6}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v6

    goto/32 :goto_d

    nop

    :goto_1a
    const-string v1, "%s finishing value(%d) velocity(%f)"

    goto/32 :goto_22

    nop

    :goto_1b
    aput-object v5, v4, v1

    goto/32 :goto_1c

    nop

    :goto_1c
    const/4 v1, 0x2

    goto/32 :goto_12

    nop

    :goto_1d
    iget-object v6, p0, Loa/a$b;->a:Lna/b;

    goto/32 :goto_f

    nop

    :goto_1e
    return v0

    :goto_1f
    goto/32 :goto_15

    nop

    :goto_20
    iget-object v0, p0, Loa/a$b;->a:Lna/b;

    goto/32 :goto_7

    nop

    :goto_21
    const/4 v1, 0x1

    goto/32 :goto_17

    nop

    :goto_22
    invoke-static {v1, v4}, Loa/b;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/32 :goto_26

    nop

    :goto_23
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    goto/32 :goto_1b

    nop

    :goto_24
    aput-object v5, v4, v1

    goto/32 :goto_1a

    nop

    :goto_25
    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    goto/32 :goto_24

    nop

    :goto_26
    iget-object v1, p0, Loa/a$b;->a:Lna/b;

    goto/32 :goto_4

    nop
.end method
