.class Loa/c$a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Loa/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "a"
.end annotation


# static fields
.field private static r:F

.field private static final s:[F

.field private static final t:[F


# instance fields
.field private a:Landroid/content/Context;

.field private b:D

.field private c:D

.field private d:D

.field private e:D

.field private f:D

.field private g:F

.field private h:J

.field private i:I

.field private j:Z

.field private k:F

.field private l:I

.field private m:F

.field private n:Lg9/j;

.field private o:Z

.field private p:D

.field private q:[D


# direct methods
.method static constructor <clinit>()V
    .locals 19

    const-wide v0, 0x3fe8f5c28f5c28f6L    # 0.78

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    const-wide v2, 0x3feccccccccccccdL    # 0.9

    invoke-static {v2, v3}, Ljava/lang/Math;->log(D)D

    move-result-wide v2

    div-double/2addr v0, v2

    double-to-float v0, v0

    sput v0, Loa/c$a;->r:F

    const/16 v0, 0x65

    new-array v1, v0, [F

    sput-object v1, Loa/c$a;->s:[F

    new-array v0, v0, [F

    sput-object v0, Loa/c$a;->t:[F

    const/4 v0, 0x0

    const/4 v1, 0x0

    move v2, v1

    move v1, v0

    :goto_0
    const/16 v3, 0x64

    const/high16 v4, 0x3f800000    # 1.0f

    if-ge v2, v3, :cond_4

    int-to-float v3, v2

    const/high16 v5, 0x42c80000    # 100.0f

    div-float v5, v3, v5

    move v3, v4

    :goto_1
    sub-float v6, v3, v0

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    add-float/2addr v6, v0

    const/high16 v8, 0x40400000    # 3.0f

    mul-float v9, v6, v8

    sub-float v10, v4, v6

    mul-float/2addr v9, v10

    const v11, 0x3e333333    # 0.175f

    mul-float v12, v10, v11

    const v13, 0x3eb33334    # 0.35000002f

    mul-float v14, v6, v13

    add-float/2addr v12, v14

    mul-float/2addr v12, v9

    mul-float v14, v6, v6

    mul-float/2addr v14, v6

    add-float/2addr v12, v14

    sub-float v15, v12, v5

    invoke-static {v15}, Ljava/lang/Math;->abs(F)F

    move-result v15

    move/from16 v16, v12

    float-to-double v11, v15

    const-wide v17, 0x3ee4f8b588e368f1L    # 1.0E-5

    cmpg-double v11, v11, v17

    if-gez v11, :cond_2

    sget-object v3, Loa/c$a;->s:[F

    const/high16 v11, 0x3f000000    # 0.5f

    mul-float/2addr v10, v11

    add-float/2addr v10, v6

    mul-float/2addr v9, v10

    add-float/2addr v9, v14

    aput v9, v3, v2

    move v3, v4

    :goto_2
    sub-float v6, v3, v1

    div-float/2addr v6, v7

    add-float/2addr v6, v1

    mul-float v9, v6, v8

    sub-float v10, v4, v6

    mul-float/2addr v9, v10

    mul-float v12, v10, v11

    add-float/2addr v12, v6

    mul-float/2addr v12, v9

    mul-float v14, v6, v6

    mul-float/2addr v14, v6

    add-float/2addr v12, v14

    sub-float v15, v12, v5

    invoke-static {v15}, Ljava/lang/Math;->abs(F)F

    move-result v15

    float-to-double v7, v15

    cmpg-double v7, v7, v17

    if-gez v7, :cond_0

    sget-object v3, Loa/c$a;->t:[F

    const v7, 0x3e333333    # 0.175f

    mul-float/2addr v10, v7

    mul-float/2addr v6, v13

    add-float/2addr v10, v6

    mul-float/2addr v9, v10

    add-float/2addr v9, v14

    aput v9, v3, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    const v7, 0x3e333333    # 0.175f

    cmpl-float v8, v12, v5

    if-lez v8, :cond_1

    move v3, v6

    goto :goto_3

    :cond_1
    move v1, v6

    :goto_3
    const/high16 v7, 0x40000000    # 2.0f

    const/high16 v8, 0x40400000    # 3.0f

    goto :goto_2

    :cond_2
    cmpl-float v7, v16, v5

    if-lez v7, :cond_3

    move v3, v6

    goto :goto_1

    :cond_3
    move v0, v6

    goto :goto_1

    :cond_4
    sget-object v0, Loa/c$a;->s:[F

    sget-object v1, Loa/c$a;->t:[F

    aput v4, v1, v3

    aput v4, v0, v3

    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Landroid/view/ViewConfiguration;->getScrollFriction()F

    move-result v0

    iput v0, p0, Loa/c$a;->k:F

    const/4 v0, 0x0

    iput v0, p0, Loa/c$a;->l:I

    iput-object p1, p0, Loa/c$a;->a:Landroid/content/Context;

    const/4 v0, 0x1

    iput-boolean v0, p0, Loa/c$a;->j:Z

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p1

    iget p1, p1, Landroid/util/DisplayMetrics;->density:F

    const/high16 v0, 0x43200000    # 160.0f

    mul-float/2addr p1, v0

    const v0, 0x43c10b3d

    mul-float/2addr p1, v0

    const v0, 0x3f570a3d    # 0.84f

    mul-float/2addr p1, v0

    iput p1, p0, Loa/c$a;->m:F

    return-void
.end method

.method static synthetic b(Loa/c$a;)Z
    .locals 0

    iget-boolean p0, p0, Loa/c$a;->j:Z

    return p0
.end method

.method static synthetic c(Loa/c$a;)D
    .locals 2

    iget-wide v0, p0, Loa/c$a;->c:D

    return-wide v0
.end method

.method static synthetic d(Loa/c$a;)D
    .locals 2

    iget-wide v0, p0, Loa/c$a;->f:D

    return-wide v0
.end method

.method static synthetic e(Loa/c$a;)D
    .locals 2

    iget-wide v0, p0, Loa/c$a;->b:D

    return-wide v0
.end method

.method static synthetic f(Loa/c$a;)D
    .locals 2

    iget-wide v0, p0, Loa/c$a;->d:D

    return-wide v0
.end method

.method static synthetic g(Loa/c$a;)I
    .locals 0

    iget p0, p0, Loa/c$a;->i:I

    return p0
.end method

.method static synthetic h(Loa/c$a;)J
    .locals 2

    iget-wide v0, p0, Loa/c$a;->h:J

    return-wide v0
.end method


# virtual methods
.method final A(I)V
    .locals 2

    goto/32 :goto_2

    nop

    :goto_0
    return-void

    :goto_1
    iput-wide v0, p0, Loa/c$a;->b:D

    goto/32 :goto_0

    nop

    :goto_2
    int-to-double v0, p1

    goto/32 :goto_1

    nop
.end method

.method final B(J)V
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    iput-wide p1, p0, Loa/c$a;->h:J

    goto/32 :goto_0

    nop
.end method

.method final C(I)V
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    iput p1, p0, Loa/c$a;->l:I

    goto/32 :goto_1

    nop

    :goto_1
    return-void
.end method

.method D(III)Z
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    const/4 p0, 0x0

    goto/32 :goto_1

    nop

    :goto_1
    throw p0
.end method

.method E(III)V
    .locals 2

    goto/32 :goto_3

    nop

    :goto_0
    const/4 p1, 0x0

    goto/32 :goto_5

    nop

    :goto_1
    iput-wide v0, p0, Loa/c$a;->c:D

    goto/32 :goto_8

    nop

    :goto_2
    iput p3, p0, Loa/c$a;->i:I

    goto/32 :goto_0

    nop

    :goto_3
    const/4 v0, 0x0

    goto/32 :goto_6

    nop

    :goto_4
    int-to-double p1, p1

    goto/32 :goto_9

    nop

    :goto_5
    iput p1, p0, Loa/c$a;->g:F

    goto/32 :goto_c

    nop

    :goto_6
    iput-boolean v0, p0, Loa/c$a;->j:Z

    goto/32 :goto_b

    nop

    :goto_7
    iput-wide v0, p0, Loa/c$a;->b:D

    goto/32 :goto_1

    nop

    :goto_8
    add-int/2addr p1, p2

    goto/32 :goto_4

    nop

    :goto_9
    iput-wide p1, p0, Loa/c$a;->d:D

    goto/32 :goto_f

    nop

    :goto_a
    return-void

    :goto_b
    int-to-double v0, p1

    goto/32 :goto_7

    nop

    :goto_c
    const-wide/16 p1, 0x0

    goto/32 :goto_e

    nop

    :goto_d
    iput-wide p1, p0, Loa/c$a;->h:J

    goto/32 :goto_2

    nop

    :goto_e
    iput-wide p1, p0, Loa/c$a;->e:D

    goto/32 :goto_a

    nop

    :goto_f
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide p1

    goto/32 :goto_d

    nop
.end method

.method F(FII)V
    .locals 2

    goto/32 :goto_3

    nop

    :goto_0
    float-to-double v0, p1

    goto/32 :goto_f

    nop

    :goto_1
    int-to-float p2, p2

    goto/32 :goto_9

    nop

    :goto_2
    iput-wide p1, p0, Loa/c$a;->d:D

    goto/32 :goto_10

    nop

    :goto_3
    const/4 v0, 0x0

    goto/32 :goto_11

    nop

    :goto_4
    iput-boolean v0, p0, Loa/c$a;->o:Z

    goto/32 :goto_e

    nop

    :goto_5
    invoke-direct {p1}, Lg9/j;-><init>()V

    goto/32 :goto_18

    nop

    :goto_6
    iput-object p3, p0, Loa/c$a;->q:[D

    goto/32 :goto_8

    nop

    :goto_7
    const/4 p2, 0x2

    goto/32 :goto_a

    nop

    :goto_8
    new-array p2, p2, [F

    fill-array-data p2, :array_0

    goto/32 :goto_15

    nop

    :goto_9
    add-float/2addr p1, p2

    goto/32 :goto_c

    nop

    :goto_a
    new-array p3, p2, [D

    goto/32 :goto_6

    nop

    :goto_b
    int-to-double p1, p3

    goto/32 :goto_d

    nop

    :goto_c
    float-to-double p1, p1

    goto/32 :goto_2

    nop

    :goto_d
    iput-wide p1, p0, Loa/c$a;->e:D

    goto/32 :goto_13

    nop

    :goto_e
    invoke-virtual {p0, v0}, Loa/c$a;->C(I)V

    goto/32 :goto_0

    nop

    :goto_f
    iput-wide v0, p0, Loa/c$a;->p:D

    goto/32 :goto_19

    nop

    :goto_10
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide p1

    goto/32 :goto_17

    nop

    :goto_11
    iput-boolean v0, p0, Loa/c$a;->j:Z

    goto/32 :goto_4

    nop

    :goto_12
    new-instance p1, Lg9/j;

    goto/32 :goto_5

    nop

    :goto_13
    iput-wide p1, p0, Loa/c$a;->f:D

    goto/32 :goto_12

    nop

    :goto_14
    iput-wide v0, p0, Loa/c$a;->c:D

    goto/32 :goto_1

    nop

    :goto_15
    invoke-virtual {p1, p2, p3}, Lg9/j;->b([F[D)V

    goto/32 :goto_16

    nop

    :goto_16
    return-void

    :array_0
    .array-data 4
        0x3f7d70a4    # 0.99f
        0x3ecccccd    # 0.4f
    .end array-data

    :goto_17
    iput-wide p1, p0, Loa/c$a;->h:J

    goto/32 :goto_b

    nop

    :goto_18
    iput-object p1, p0, Loa/c$a;->n:Lg9/j;

    goto/32 :goto_7

    nop

    :goto_19
    iput-wide v0, p0, Loa/c$a;->b:D

    goto/32 :goto_14

    nop
.end method

.method G()Z
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    const/4 p0, 0x0

    goto/32 :goto_1

    nop

    :goto_1
    throw p0
.end method

.method H(F)V
    .locals 6

    goto/32 :goto_0

    nop

    :goto_0
    iget-wide v0, p0, Loa/c$a;->b:D

    goto/32 :goto_6

    nop

    :goto_1
    add-double/2addr v0, v2

    goto/32 :goto_4

    nop

    :goto_2
    return-void

    :goto_3
    sub-double/2addr v4, v0

    goto/32 :goto_8

    nop

    :goto_4
    iput-wide v0, p0, Loa/c$a;->c:D

    goto/32 :goto_2

    nop

    :goto_5
    long-to-double v2, v2

    goto/32 :goto_1

    nop

    :goto_6
    float-to-double v2, p1

    goto/32 :goto_9

    nop

    :goto_7
    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    goto/32 :goto_5

    nop

    :goto_8
    mul-double/2addr v2, v4

    goto/32 :goto_7

    nop

    :goto_9
    iget-wide v4, p0, Loa/c$a;->d:D

    goto/32 :goto_3

    nop
.end method

.method i()Z
    .locals 20

    goto/32 :goto_11

    nop

    :goto_0
    const/4 v2, 0x0

    goto/32 :goto_e

    nop

    :goto_1
    iput-wide v1, v0, Loa/c$a;->c:D

    goto/32 :goto_36

    nop

    :goto_2
    iput-wide v1, v0, Loa/c$a;->b:D

    :goto_3
    goto/32 :goto_1f

    nop

    :goto_4
    if-eqz v1, :cond_0

    goto/32 :goto_13

    :cond_0
    goto/32 :goto_12

    nop

    :goto_5
    if-nez v1, :cond_1

    goto/32 :goto_37

    :cond_1
    goto/32 :goto_39

    nop

    :goto_6
    iget-wide v11, v0, Loa/c$a;->f:D

    goto/32 :goto_35

    nop

    :goto_7
    iput-wide v4, v0, Loa/c$a;->c:D

    goto/32 :goto_31

    nop

    :goto_8
    if-nez v1, :cond_2

    goto/32 :goto_28

    :cond_2
    goto/32 :goto_2b

    nop

    :goto_9
    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->min(DD)D

    move-result-wide v6

    goto/32 :goto_2e

    nop

    :goto_a
    goto :goto_20

    :goto_b
    goto/32 :goto_10

    nop

    :goto_c
    invoke-virtual/range {v10 .. v19}, Lg9/j;->a(DDDD[D)D

    move-result-wide v1

    goto/32 :goto_14

    nop

    :goto_d
    mul-double/2addr v8, v1

    goto/32 :goto_1c

    nop

    :goto_e
    if-nez v1, :cond_3

    goto/32 :goto_20

    :cond_3
    goto/32 :goto_2c

    nop

    :goto_f
    invoke-virtual {v0, v4, v5, v1, v2}, Loa/c$a;->r(DD)Z

    move-result v1

    goto/32 :goto_8

    nop

    :goto_10
    iget-boolean v1, v0, Loa/c$a;->o:Z

    goto/32 :goto_3c

    nop

    :goto_11
    move-object/from16 v0, p0

    goto/32 :goto_19

    nop

    :goto_12
    goto :goto_23

    :goto_13
    goto/32 :goto_22

    nop

    :goto_14
    iget-wide v4, v0, Loa/c$a;->b:D

    goto/32 :goto_d

    nop

    :goto_15
    new-array v1, v1, [D

    goto/32 :goto_24

    nop

    :goto_16
    iget-object v10, v0, Loa/c$a;->n:Lg9/j;

    goto/32 :goto_6

    nop

    :goto_17
    div-float/2addr v1, v6

    goto/32 :goto_3b

    nop

    :goto_18
    cmpl-double v1, v6, v10

    goto/32 :goto_4

    nop

    :goto_19
    iget-object v1, v0, Loa/c$a;->n:Lg9/j;

    goto/32 :goto_0

    nop

    :goto_1a
    const/4 v1, 0x2

    goto/32 :goto_15

    nop

    :goto_1b
    iput-wide v4, v0, Loa/c$a;->h:J

    goto/32 :goto_16

    nop

    :goto_1c
    add-double/2addr v4, v8

    goto/32 :goto_7

    nop

    :goto_1d
    return v2

    :goto_1e
    move-wide/from16 v17, v8

    goto/32 :goto_32

    nop

    :goto_1f
    return v3

    :goto_20
    goto/32 :goto_1d

    nop

    :goto_21
    iget-wide v6, v0, Loa/c$a;->h:J

    goto/32 :goto_2f

    nop

    :goto_22
    move-wide v8, v6

    :goto_23
    goto/32 :goto_1b

    nop

    :goto_24
    iget-wide v4, v0, Loa/c$a;->d:D

    goto/32 :goto_3d

    nop

    :goto_25
    iget-wide v4, v0, Loa/c$a;->b:D

    goto/32 :goto_2a

    nop

    :goto_26
    long-to-float v1, v6

    goto/32 :goto_3f

    nop

    :goto_27
    goto/16 :goto_3

    :goto_28
    goto/32 :goto_33

    nop

    :goto_29
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v4

    goto/32 :goto_21

    nop

    :goto_2a
    aput-wide v4, v1, v3

    goto/32 :goto_1e

    nop

    :goto_2b
    iput-boolean v3, v0, Loa/c$a;->o:Z

    goto/32 :goto_27

    nop

    :goto_2c
    iget-boolean v1, v0, Loa/c$a;->j:Z

    goto/32 :goto_2d

    nop

    :goto_2d
    if-nez v1, :cond_4

    goto/32 :goto_b

    :cond_4
    goto/32 :goto_a

    nop

    :goto_2e
    const-wide/16 v10, 0x0

    goto/32 :goto_18

    nop

    :goto_2f
    sub-long v6, v4, v6

    goto/32 :goto_26

    nop

    :goto_30
    aget-wide v15, v1, v3

    goto/32 :goto_1a

    nop

    :goto_31
    iput-wide v1, v0, Loa/c$a;->f:D

    goto/32 :goto_34

    nop

    :goto_32
    move-object/from16 v19, v1

    goto/32 :goto_c

    nop

    :goto_33
    iget-wide v1, v0, Loa/c$a;->c:D

    goto/32 :goto_2

    nop

    :goto_34
    iget-wide v1, v0, Loa/c$a;->d:D

    goto/32 :goto_f

    nop

    :goto_35
    iget-object v1, v0, Loa/c$a;->q:[D

    goto/32 :goto_3a

    nop

    :goto_36
    return v3

    :goto_37
    goto/32 :goto_29

    nop

    :goto_38
    iget-wide v1, v0, Loa/c$a;->d:D

    goto/32 :goto_1

    nop

    :goto_39
    iput-boolean v3, v0, Loa/c$a;->j:Z

    goto/32 :goto_38

    nop

    :goto_3a
    aget-wide v13, v1, v2

    goto/32 :goto_30

    nop

    :goto_3b
    float-to-double v6, v1

    goto/32 :goto_3e

    nop

    :goto_3c
    const/4 v3, 0x1

    goto/32 :goto_5

    nop

    :goto_3d
    aput-wide v4, v1, v2

    goto/32 :goto_25

    nop

    :goto_3e
    const-wide v8, 0x3f90624de0000000L    # 0.01600000075995922

    goto/32 :goto_9

    nop

    :goto_3f
    const/high16 v6, 0x447a0000    # 1000.0f

    goto/32 :goto_17

    nop
.end method

.method j()Z
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    throw p0

    :goto_1
    const/4 p0, 0x0

    goto/32 :goto_0

    nop
.end method

.method k()V
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    const/4 p0, 0x0

    goto/32 :goto_1

    nop

    :goto_1
    throw p0
.end method

.method l(IIIII)V
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    const/4 p0, 0x0

    goto/32 :goto_1

    nop

    :goto_1
    throw p0
.end method

.method final m()F
    .locals 2

    goto/32 :goto_1

    nop

    :goto_0
    return v0

    :goto_1
    iget-wide v0, p0, Loa/c$a;->f:D

    goto/32 :goto_2

    nop

    :goto_2
    double-to-float v0, v0

    goto/32 :goto_0

    nop
.end method

.method final n()I
    .locals 2

    goto/32 :goto_1

    nop

    :goto_0
    return v0

    :goto_1
    iget-wide v0, p0, Loa/c$a;->c:D

    goto/32 :goto_2

    nop

    :goto_2
    double-to-int v0, v0

    goto/32 :goto_0

    nop
.end method

.method final o()I
    .locals 2

    goto/32 :goto_2

    nop

    :goto_0
    double-to-int v0, v0

    goto/32 :goto_1

    nop

    :goto_1
    return v0

    :goto_2
    iget-wide v0, p0, Loa/c$a;->d:D

    goto/32 :goto_0

    nop
.end method

.method final p()I
    .locals 2

    goto/32 :goto_2

    nop

    :goto_0
    return v0

    :goto_1
    double-to-int v0, v0

    goto/32 :goto_0

    nop

    :goto_2
    iget-wide v0, p0, Loa/c$a;->b:D

    goto/32 :goto_1

    nop
.end method

.method final q()I
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return v0

    :goto_1
    iget v0, p0, Loa/c$a;->l:I

    goto/32 :goto_0

    nop
.end method

.method r(DD)Z
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    sub-double/2addr p1, p3

    goto/32 :goto_a

    nop

    :goto_1
    const/4 p1, 0x1

    goto/32 :goto_2

    nop

    :goto_2
    goto :goto_6

    :goto_3
    goto/32 :goto_5

    nop

    :goto_4
    return p1

    :goto_5
    const/4 p1, 0x0

    :goto_6
    goto/32 :goto_4

    nop

    :goto_7
    const-wide/high16 p3, 0x3ff0000000000000L    # 1.0

    goto/32 :goto_8

    nop

    :goto_8
    cmpg-double p1, p1, p3

    goto/32 :goto_9

    nop

    :goto_9
    if-ltz p1, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_1

    nop

    :goto_a
    invoke-static {p1, p2}, Ljava/lang/Math;->abs(D)D

    move-result-wide p1

    goto/32 :goto_7

    nop
.end method

.method final s()Z
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-boolean v0, p0, Loa/c$a;->j:Z

    goto/32 :goto_1

    nop

    :goto_1
    return v0
.end method

.method t(III)V
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    const/4 p0, 0x0

    goto/32 :goto_1

    nop

    :goto_1
    throw p0
.end method

.method final u(F)V
    .locals 2

    goto/32 :goto_0

    nop

    :goto_0
    float-to-double v0, p1

    goto/32 :goto_1

    nop

    :goto_1
    iput-wide v0, p0, Loa/c$a;->f:D

    goto/32 :goto_2

    nop

    :goto_2
    return-void
.end method

.method final v(I)V
    .locals 2

    goto/32 :goto_0

    nop

    :goto_0
    int-to-double v0, p1

    goto/32 :goto_2

    nop

    :goto_1
    return-void

    :goto_2
    iput-wide v0, p0, Loa/c$a;->c:D

    goto/32 :goto_1

    nop
.end method

.method final w(I)V
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    iput p1, p0, Loa/c$a;->i:I

    goto/32 :goto_0

    nop
.end method

.method final x(I)V
    .locals 2

    goto/32 :goto_1

    nop

    :goto_0
    iput-wide v0, p0, Loa/c$a;->d:D

    goto/32 :goto_2

    nop

    :goto_1
    int-to-double v0, p1

    goto/32 :goto_0

    nop

    :goto_2
    return-void
.end method

.method y(I)V
    .locals 2

    goto/32 :goto_4

    nop

    :goto_0
    iput-wide v0, p0, Loa/c$a;->d:D

    goto/32 :goto_2

    nop

    :goto_1
    return-void

    :goto_2
    const/4 p1, 0x0

    goto/32 :goto_3

    nop

    :goto_3
    iput-boolean p1, p0, Loa/c$a;->j:Z

    goto/32 :goto_1

    nop

    :goto_4
    int-to-double v0, p1

    goto/32 :goto_0

    nop
.end method

.method final z(Z)V
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    iput-boolean p1, p0, Loa/c$a;->j:Z

    goto/32 :goto_0

    nop
.end method
