.class public Lo/b;
.super Ljava/lang/Object;

# interfaces
.implements Lo/d$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lo/b$a;
    }
.end annotation


# instance fields
.field a:Lo/i;

.field b:F

.field c:Z

.field d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lo/i;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lo/b$a;

.field f:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lo/b;->a:Lo/i;

    const/4 v0, 0x0

    iput v0, p0, Lo/b;->b:F

    const/4 v0, 0x0

    iput-boolean v0, p0, Lo/b;->c:Z

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lo/b;->d:Ljava/util/ArrayList;

    iput-boolean v0, p0, Lo/b;->f:Z

    return-void
.end method

.method public constructor <init>(Lo/c;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lo/b;->a:Lo/i;

    const/4 v0, 0x0

    iput v0, p0, Lo/b;->b:F

    const/4 v0, 0x0

    iput-boolean v0, p0, Lo/b;->c:Z

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lo/b;->d:Ljava/util/ArrayList;

    iput-boolean v0, p0, Lo/b;->f:Z

    new-instance v0, Lo/a;

    invoke-direct {v0, p0, p1}, Lo/a;-><init>(Lo/b;Lo/c;)V

    iput-object v0, p0, Lo/b;->e:Lo/b$a;

    return-void
.end method

.method private u(Lo/i;Lo/d;)Z
    .locals 0

    iget p1, p1, Lo/i;->m:I

    const/4 p2, 0x1

    if-gt p1, p2, :cond_0

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    return p2
.end method

.method private w([ZLo/i;)Lo/i;
    .locals 9

    iget-object v0, p0, Lo/b;->e:Lo/b$a;

    invoke-interface {v0}, Lo/b$a;->d()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    move v4, v1

    :goto_0
    if-ge v3, v0, :cond_3

    iget-object v5, p0, Lo/b;->e:Lo/b$a;

    invoke-interface {v5, v3}, Lo/b$a;->a(I)F

    move-result v5

    cmpg-float v6, v5, v1

    if-gez v6, :cond_2

    iget-object v6, p0, Lo/b;->e:Lo/b$a;

    invoke-interface {v6, v3}, Lo/b$a;->h(I)Lo/i;

    move-result-object v6

    if-eqz p1, :cond_0

    iget v7, v6, Lo/i;->c:I

    aget-boolean v7, p1, v7

    if-nez v7, :cond_2

    :cond_0
    if-eq v6, p2, :cond_2

    iget-object v7, v6, Lo/i;->j:Lo/i$a;

    sget-object v8, Lo/i$a;->c:Lo/i$a;

    if-eq v7, v8, :cond_1

    sget-object v8, Lo/i$a;->d:Lo/i$a;

    if-ne v7, v8, :cond_2

    :cond_1
    cmpg-float v7, v5, v4

    if-gez v7, :cond_2

    move v4, v5

    move-object v2, v6

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_3
    return-object v2
.end method


# virtual methods
.method public A(Lo/d;Lo/i;Z)V
    .locals 3

    if-eqz p2, :cond_2

    iget-boolean v0, p2, Lo/i;->g:Z

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lo/b;->e:Lo/b$a;

    invoke-interface {v0, p2}, Lo/b$a;->g(Lo/i;)F

    move-result v0

    iget v1, p0, Lo/b;->b:F

    iget v2, p2, Lo/i;->f:F

    mul-float/2addr v2, v0

    add-float/2addr v1, v2

    iput v1, p0, Lo/b;->b:F

    iget-object v0, p0, Lo/b;->e:Lo/b$a;

    invoke-interface {v0, p2, p3}, Lo/b$a;->j(Lo/i;Z)F

    if-eqz p3, :cond_1

    invoke-virtual {p2, p0}, Lo/i;->d(Lo/b;)V

    :cond_1
    sget-boolean p2, Lo/d;->t:Z

    if-eqz p2, :cond_2

    iget-object p2, p0, Lo/b;->e:Lo/b$a;

    invoke-interface {p2}, Lo/b$a;->d()I

    move-result p2

    if-nez p2, :cond_2

    const/4 p2, 0x1

    iput-boolean p2, p0, Lo/b;->f:Z

    iput-boolean p2, p1, Lo/d;->a:Z

    :cond_2
    :goto_0
    return-void
.end method

.method public B(Lo/d;Lo/b;Z)V
    .locals 3

    iget-object v0, p0, Lo/b;->e:Lo/b$a;

    invoke-interface {v0, p2, p3}, Lo/b$a;->f(Lo/b;Z)F

    move-result v0

    iget v1, p0, Lo/b;->b:F

    iget v2, p2, Lo/b;->b:F

    mul-float/2addr v2, v0

    add-float/2addr v1, v2

    iput v1, p0, Lo/b;->b:F

    if-eqz p3, :cond_0

    iget-object p2, p2, Lo/b;->a:Lo/i;

    invoke-virtual {p2, p0}, Lo/i;->d(Lo/b;)V

    :cond_0
    sget-boolean p2, Lo/d;->t:Z

    if-eqz p2, :cond_1

    iget-object p2, p0, Lo/b;->a:Lo/i;

    if-eqz p2, :cond_1

    iget-object p2, p0, Lo/b;->e:Lo/b$a;

    invoke-interface {p2}, Lo/b$a;->d()I

    move-result p2

    if-nez p2, :cond_1

    const/4 p2, 0x1

    iput-boolean p2, p0, Lo/b;->f:Z

    iput-boolean p2, p1, Lo/d;->a:Z

    :cond_1
    return-void
.end method

.method public C(Lo/d;Lo/i;Z)V
    .locals 3

    if-eqz p2, :cond_2

    iget-boolean v0, p2, Lo/i;->n:Z

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lo/b;->e:Lo/b$a;

    invoke-interface {v0, p2}, Lo/b$a;->g(Lo/i;)F

    move-result v0

    iget v1, p0, Lo/b;->b:F

    iget v2, p2, Lo/i;->p:F

    mul-float/2addr v2, v0

    add-float/2addr v1, v2

    iput v1, p0, Lo/b;->b:F

    iget-object v1, p0, Lo/b;->e:Lo/b$a;

    invoke-interface {v1, p2, p3}, Lo/b$a;->j(Lo/i;Z)F

    if-eqz p3, :cond_1

    invoke-virtual {p2, p0}, Lo/i;->d(Lo/b;)V

    :cond_1
    iget-object v1, p0, Lo/b;->e:Lo/b$a;

    iget-object v2, p1, Lo/d;->n:Lo/c;

    iget-object v2, v2, Lo/c;->d:[Lo/i;

    iget p2, p2, Lo/i;->o:I

    aget-object p2, v2, p2

    invoke-interface {v1, p2, v0, p3}, Lo/b$a;->b(Lo/i;FZ)V

    sget-boolean p2, Lo/d;->t:Z

    if-eqz p2, :cond_2

    iget-object p2, p0, Lo/b;->e:Lo/b$a;

    invoke-interface {p2}, Lo/b$a;->d()I

    move-result p2

    if-nez p2, :cond_2

    const/4 p2, 0x1

    iput-boolean p2, p0, Lo/b;->f:Z

    iput-boolean p2, p1, Lo/d;->a:Z

    :cond_2
    :goto_0
    return-void
.end method

.method public D(Lo/d;)V
    .locals 8

    iget-object v0, p1, Lo/d;->g:[Lo/b;

    array-length v0, v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    const/4 v2, 0x1

    if-nez v1, :cond_8

    iget-object v3, p0, Lo/b;->e:Lo/b$a;

    invoke-interface {v3}, Lo/b$a;->d()I

    move-result v3

    move v4, v0

    :goto_1
    if-ge v4, v3, :cond_3

    iget-object v5, p0, Lo/b;->e:Lo/b$a;

    invoke-interface {v5, v4}, Lo/b$a;->h(I)Lo/i;

    move-result-object v5

    iget v6, v5, Lo/i;->d:I

    const/4 v7, -0x1

    if-ne v6, v7, :cond_1

    iget-boolean v6, v5, Lo/i;->g:Z

    if-nez v6, :cond_1

    iget-boolean v6, v5, Lo/i;->n:Z

    if-eqz v6, :cond_2

    :cond_1
    iget-object v6, p0, Lo/b;->d:Ljava/util/ArrayList;

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_3
    iget-object v3, p0, Lo/b;->d:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_7

    move v4, v0

    :goto_2
    if-ge v4, v3, :cond_6

    iget-object v5, p0, Lo/b;->d:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lo/i;

    iget-boolean v6, v5, Lo/i;->g:Z

    if-eqz v6, :cond_4

    invoke-virtual {p0, p1, v5, v2}, Lo/b;->A(Lo/d;Lo/i;Z)V

    goto :goto_3

    :cond_4
    iget-boolean v6, v5, Lo/i;->n:Z

    if-eqz v6, :cond_5

    invoke-virtual {p0, p1, v5, v2}, Lo/b;->C(Lo/d;Lo/i;Z)V

    goto :goto_3

    :cond_5
    iget-object v6, p1, Lo/d;->g:[Lo/b;

    iget v5, v5, Lo/i;->d:I

    aget-object v5, v6, v5

    invoke-virtual {p0, p1, v5, v2}, Lo/b;->B(Lo/d;Lo/b;Z)V

    :goto_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lo/b;->d:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    goto :goto_0

    :cond_7
    move v1, v2

    goto :goto_0

    :cond_8
    sget-boolean v0, Lo/d;->t:Z

    if-eqz v0, :cond_9

    iget-object v0, p0, Lo/b;->a:Lo/i;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lo/b;->e:Lo/b$a;

    invoke-interface {v0}, Lo/b$a;->d()I

    move-result v0

    if-nez v0, :cond_9

    iput-boolean v2, p0, Lo/b;->f:Z

    iput-boolean v2, p1, Lo/d;->a:Z

    :cond_9
    return-void
.end method

.method public a(Lo/i;)V
    .locals 3

    iget v0, p1, Lo/i;->e:I

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x2

    if-ne v0, v2, :cond_1

    const/high16 v1, 0x447a0000    # 1000.0f

    goto :goto_0

    :cond_1
    const/4 v2, 0x3

    if-ne v0, v2, :cond_2

    const v1, 0x49742400    # 1000000.0f

    goto :goto_0

    :cond_2
    const/4 v2, 0x4

    if-ne v0, v2, :cond_3

    const v1, 0x4e6e6b28    # 1.0E9f

    goto :goto_0

    :cond_3
    const/4 v2, 0x5

    if-ne v0, v2, :cond_4

    const v1, 0x5368d4a5    # 1.0E12f

    :cond_4
    :goto_0
    iget-object v0, p0, Lo/b;->e:Lo/b$a;

    invoke-interface {v0, p1, v1}, Lo/b$a;->c(Lo/i;F)V

    return-void
.end method

.method public b(Lo/d;[Z)Lo/i;
    .locals 0

    const/4 p1, 0x0

    invoke-direct {p0, p2, p1}, Lo/b;->w([ZLo/i;)Lo/i;

    move-result-object p1

    return-object p1
.end method

.method public c(Lo/d$a;)V
    .locals 5

    instance-of v0, p1, Lo/b;

    if-eqz v0, :cond_0

    check-cast p1, Lo/b;

    const/4 v0, 0x0

    iput-object v0, p0, Lo/b;->a:Lo/i;

    iget-object v0, p0, Lo/b;->e:Lo/b$a;

    invoke-interface {v0}, Lo/b$a;->clear()V

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p1, Lo/b;->e:Lo/b$a;

    invoke-interface {v1}, Lo/b$a;->d()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p1, Lo/b;->e:Lo/b$a;

    invoke-interface {v1, v0}, Lo/b$a;->h(I)Lo/i;

    move-result-object v1

    iget-object v2, p1, Lo/b;->e:Lo/b$a;

    invoke-interface {v2, v0}, Lo/b$a;->a(I)F

    move-result v2

    iget-object v3, p0, Lo/b;->e:Lo/b$a;

    const/4 v4, 0x1

    invoke-interface {v3, v1, v2, v4}, Lo/b$a;->b(Lo/i;FZ)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public clear()V
    .locals 1

    iget-object v0, p0, Lo/b;->e:Lo/b$a;

    invoke-interface {v0}, Lo/b$a;->clear()V

    const/4 v0, 0x0

    iput-object v0, p0, Lo/b;->a:Lo/i;

    const/4 v0, 0x0

    iput v0, p0, Lo/b;->b:F

    return-void
.end method

.method public d(Lo/d;I)Lo/b;
    .locals 3

    iget-object v0, p0, Lo/b;->e:Lo/b$a;

    const-string v1, "ep"

    invoke-virtual {p1, p2, v1}, Lo/d;->o(ILjava/lang/String;)Lo/i;

    move-result-object v1

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-interface {v0, v1, v2}, Lo/b$a;->c(Lo/i;F)V

    iget-object v0, p0, Lo/b;->e:Lo/b$a;

    const-string v1, "em"

    invoke-virtual {p1, p2, v1}, Lo/d;->o(ILjava/lang/String;)Lo/i;

    move-result-object p1

    const/high16 p2, -0x40800000    # -1.0f

    invoke-interface {v0, p1, p2}, Lo/b$a;->c(Lo/i;F)V

    return-object p0
.end method

.method e(Lo/i;I)Lo/b;
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Lo/b;->e:Lo/b$a;

    goto/32 :goto_2

    nop

    :goto_1
    invoke-interface {v0, p1, p2}, Lo/b$a;->c(Lo/i;F)V

    goto/32 :goto_3

    nop

    :goto_2
    int-to-float p2, p2

    goto/32 :goto_1

    nop

    :goto_3
    return-object p0
.end method

.method f(Lo/d;)Z
    .locals 2

    goto/32 :goto_7

    nop

    :goto_0
    goto :goto_9

    :goto_1
    goto/32 :goto_3

    nop

    :goto_2
    invoke-interface {v1}, Lo/b$a;->d()I

    move-result v1

    goto/32 :goto_a

    nop

    :goto_3
    invoke-virtual {p0, p1}, Lo/b;->x(Lo/i;)V

    goto/32 :goto_8

    nop

    :goto_4
    move p1, v0

    goto/32 :goto_0

    nop

    :goto_5
    if-eqz p1, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_4

    nop

    :goto_6
    const/4 v0, 0x1

    goto/32 :goto_5

    nop

    :goto_7
    invoke-virtual {p0, p1}, Lo/b;->g(Lo/d;)Lo/i;

    move-result-object p1

    goto/32 :goto_6

    nop

    :goto_8
    const/4 p1, 0x0

    :goto_9
    goto/32 :goto_e

    nop

    :goto_a
    if-eqz v1, :cond_1

    goto/32 :goto_c

    :cond_1
    goto/32 :goto_b

    nop

    :goto_b
    iput-boolean v0, p0, Lo/b;->f:Z

    :goto_c
    goto/32 :goto_d

    nop

    :goto_d
    return p1

    :goto_e
    iget-object v1, p0, Lo/b;->e:Lo/b$a;

    goto/32 :goto_2

    nop
.end method

.method g(Lo/d;)Lo/i;
    .locals 14

    goto/32 :goto_22

    nop

    :goto_0
    iget-object v11, v10, Lo/i;->j:Lo/i$a;

    goto/32 :goto_19

    nop

    :goto_1
    move-object v1, v10

    goto/32 :goto_43

    nop

    :goto_2
    move v5, v4

    goto/32 :goto_17

    nop

    :goto_3
    iget-object v9, p0, Lo/b;->e:Lo/b$a;

    goto/32 :goto_27

    nop

    :goto_4
    move v4, v2

    goto/32 :goto_2

    nop

    :goto_5
    if-ltz v11, :cond_0

    goto/32 :goto_21

    :cond_0
    goto/32 :goto_e

    nop

    :goto_6
    if-nez v11, :cond_1

    goto/32 :goto_21

    :cond_1
    goto/32 :goto_25

    nop

    :goto_7
    invoke-interface {v0}, Lo/b$a;->d()I

    move-result v0

    goto/32 :goto_36

    nop

    :goto_8
    move-object v2, v10

    goto/32 :goto_b

    nop

    :goto_9
    goto :goto_1c

    :goto_a
    goto/32 :goto_40

    nop

    :goto_b
    goto :goto_21

    :goto_c
    goto/32 :goto_45

    nop

    :goto_d
    if-eqz v1, :cond_2

    goto/32 :goto_21

    :cond_2
    goto/32 :goto_2f

    nop

    :goto_e
    if-eqz v2, :cond_3

    goto/32 :goto_c

    :cond_3
    :goto_f
    goto/32 :goto_28

    nop

    :goto_10
    return-object v1

    :goto_11
    goto/32 :goto_3a

    nop

    :goto_12
    move v8, v7

    goto/32 :goto_1b

    nop

    :goto_13
    iget-object v10, p0, Lo/b;->e:Lo/b$a;

    goto/32 :goto_39

    nop

    :goto_14
    move-object v1, v10

    goto/32 :goto_38

    nop

    :goto_15
    if-eq v11, v12, :cond_4

    goto/32 :goto_33

    :cond_4
    goto/32 :goto_30

    nop

    :goto_16
    if-nez v11, :cond_5

    goto/32 :goto_21

    :cond_5
    goto/32 :goto_1d

    nop

    :goto_17
    move v6, v5

    goto/32 :goto_3c

    nop

    :goto_18
    if-gtz v11, :cond_6

    goto/32 :goto_2a

    :cond_6
    goto/32 :goto_29

    nop

    :goto_19
    sget-object v12, Lo/i$a;->a:Lo/i$a;

    goto/32 :goto_35

    nop

    :goto_1a
    move v6, v2

    goto/32 :goto_23

    nop

    :goto_1b
    move-object v2, v1

    :goto_1c
    goto/32 :goto_24

    nop

    :goto_1d
    move v8, v9

    goto/32 :goto_42

    nop

    :goto_1e
    goto :goto_31

    :goto_1f
    goto/32 :goto_3e

    nop

    :goto_20
    move v6, v13

    :goto_21
    goto/32 :goto_2e

    nop

    :goto_22
    iget-object v0, p0, Lo/b;->e:Lo/b$a;

    goto/32 :goto_7

    nop

    :goto_23
    move v8, v9

    goto/32 :goto_8

    nop

    :goto_24
    if-lt v4, v0, :cond_7

    goto/32 :goto_a

    :cond_7
    goto/32 :goto_3

    nop

    :goto_25
    move v7, v9

    goto/32 :goto_14

    nop

    :goto_26
    move v5, v1

    goto/32 :goto_3f

    nop

    :goto_27
    invoke-interface {v9, v4}, Lo/b$a;->a(I)F

    move-result v9

    goto/32 :goto_13

    nop

    :goto_28
    invoke-direct {p0, v10, p1}, Lo/b;->u(Lo/i;Lo/d;)Z

    move-result v2

    goto/32 :goto_1a

    nop

    :goto_29
    goto/16 :goto_f

    :goto_2a
    goto/32 :goto_2d

    nop

    :goto_2b
    invoke-direct {p0, v10, p1}, Lo/b;->u(Lo/i;Lo/d;)Z

    move-result v11

    goto/32 :goto_6

    nop

    :goto_2c
    const/4 v2, 0x0

    goto/32 :goto_3d

    nop

    :goto_2d
    if-eqz v6, :cond_8

    goto/32 :goto_21

    :cond_8
    goto/32 :goto_37

    nop

    :goto_2e
    add-int/lit8 v4, v4, 0x1

    goto/32 :goto_9

    nop

    :goto_2f
    cmpg-float v11, v9, v3

    goto/32 :goto_5

    nop

    :goto_30
    if-eqz v1, :cond_9

    goto/32 :goto_44

    :cond_9
    :goto_31
    goto/32 :goto_34

    nop

    :goto_32
    goto :goto_21

    :goto_33
    goto/32 :goto_d

    nop

    :goto_34
    invoke-direct {p0, v10, p1}, Lo/b;->u(Lo/i;Lo/d;)Z

    move-result v1

    goto/32 :goto_26

    nop

    :goto_35
    const/4 v13, 0x1

    goto/32 :goto_15

    nop

    :goto_36
    const/4 v1, 0x0

    goto/32 :goto_2c

    nop

    :goto_37
    invoke-direct {p0, v10, p1}, Lo/b;->u(Lo/i;Lo/d;)Z

    move-result v11

    goto/32 :goto_16

    nop

    :goto_38
    move v5, v13

    goto/32 :goto_32

    nop

    :goto_39
    invoke-interface {v10, v4}, Lo/b$a;->h(I)Lo/i;

    move-result-object v10

    goto/32 :goto_0

    nop

    :goto_3a
    return-object v2

    :goto_3b
    cmpl-float v11, v7, v9

    goto/32 :goto_41

    nop

    :goto_3c
    move v7, v3

    goto/32 :goto_12

    nop

    :goto_3d
    const/4 v3, 0x0

    goto/32 :goto_4

    nop

    :goto_3e
    if-eqz v5, :cond_a

    goto/32 :goto_21

    :cond_a
    goto/32 :goto_2b

    nop

    :goto_3f
    move v7, v9

    goto/32 :goto_1

    nop

    :goto_40
    if-nez v1, :cond_b

    goto/32 :goto_11

    :cond_b
    goto/32 :goto_10

    nop

    :goto_41
    if-gtz v11, :cond_c

    goto/32 :goto_1f

    :cond_c
    goto/32 :goto_1e

    nop

    :goto_42
    move-object v2, v10

    goto/32 :goto_20

    nop

    :goto_43
    goto/16 :goto_21

    :goto_44
    goto/32 :goto_3b

    nop

    :goto_45
    cmpl-float v11, v8, v9

    goto/32 :goto_18

    nop
.end method

.method public getKey()Lo/i;
    .locals 1

    iget-object v0, p0, Lo/b;->a:Lo/i;

    return-object v0
.end method

.method h(Lo/i;Lo/i;IFLo/i;Lo/i;I)Lo/b;
    .locals 5

    goto/32 :goto_1c

    nop

    :goto_0
    iget-object p1, p0, Lo/b;->e:Lo/b$a;

    goto/32 :goto_49

    nop

    :goto_1
    iget-object p1, p0, Lo/b;->e:Lo/b$a;

    goto/32 :goto_7

    nop

    :goto_2
    if-lez p3, :cond_0

    goto/32 :goto_34

    :cond_0
    goto/32 :goto_33

    nop

    :goto_3
    invoke-interface {p4, p1, v0}, Lo/b$a;->c(Lo/i;F)V

    goto/32 :goto_24

    nop

    :goto_4
    invoke-interface {p1, p2, v1}, Lo/b$a;->c(Lo/i;F)V

    goto/32 :goto_47

    nop

    :goto_5
    return-object p0

    :goto_6
    goto/32 :goto_3e

    nop

    :goto_7
    invoke-interface {p1, p5, v2}, Lo/b$a;->c(Lo/i;F)V

    goto/32 :goto_2a

    nop

    :goto_8
    invoke-interface {p1, p6, v0}, Lo/b$a;->c(Lo/i;F)V

    goto/32 :goto_19

    nop

    :goto_9
    if-eqz v1, :cond_1

    goto/32 :goto_12

    :cond_1
    goto/32 :goto_f

    nop

    :goto_a
    cmpl-float v1, p4, v1

    goto/32 :goto_c

    nop

    :goto_b
    return-object p0

    :goto_c
    const/high16 v2, -0x40800000    # -1.0f

    goto/32 :goto_9

    nop

    :goto_d
    mul-float/2addr p1, v3

    goto/32 :goto_46

    nop

    :goto_e
    iget-object p3, p0, Lo/b;->e:Lo/b$a;

    goto/32 :goto_14

    nop

    :goto_f
    iget-object p4, p0, Lo/b;->e:Lo/b$a;

    goto/32 :goto_3

    nop

    :goto_10
    neg-int p1, p3

    goto/32 :goto_2e

    nop

    :goto_11
    goto :goto_21

    :goto_12
    goto/32 :goto_1e

    nop

    :goto_13
    sub-float v3, v0, p4

    goto/32 :goto_39

    nop

    :goto_14
    invoke-interface {p3, p1, v0}, Lo/b$a;->c(Lo/i;F)V

    goto/32 :goto_2d

    nop

    :goto_15
    cmpg-float v1, p4, v1

    goto/32 :goto_18

    nop

    :goto_16
    iput p1, p0, Lo/b;->b:F

    goto/32 :goto_2b

    nop

    :goto_17
    const/high16 p3, -0x40000000    # -2.0f

    goto/32 :goto_48

    nop

    :goto_18
    if-lez v1, :cond_2

    goto/32 :goto_2c

    :cond_2
    goto/32 :goto_40

    nop

    :goto_19
    if-lez p3, :cond_3

    goto/32 :goto_3b

    :cond_3
    goto/32 :goto_3a

    nop

    :goto_1a
    iget-object p1, p0, Lo/b;->e:Lo/b$a;

    goto/32 :goto_28

    nop

    :goto_1b
    int-to-float p1, p1

    goto/32 :goto_d

    nop

    :goto_1c
    const/high16 v0, 0x3f800000    # 1.0f

    goto/32 :goto_30

    nop

    :goto_1d
    iget-object p1, p0, Lo/b;->e:Lo/b$a;

    goto/32 :goto_3c

    nop

    :goto_1e
    const/4 v1, 0x0

    goto/32 :goto_15

    nop

    :goto_1f
    iget-object v1, p0, Lo/b;->e:Lo/b$a;

    goto/32 :goto_13

    nop

    :goto_20
    int-to-float p1, p3

    :goto_21
    goto/32 :goto_16

    nop

    :goto_22
    invoke-interface {p1, p5, v0}, Lo/b$a;->c(Lo/i;F)V

    goto/32 :goto_26

    nop

    :goto_23
    invoke-interface {p1, p6, v0}, Lo/b$a;->c(Lo/i;F)V

    goto/32 :goto_2

    nop

    :goto_24
    iget-object p1, p0, Lo/b;->e:Lo/b$a;

    goto/32 :goto_27

    nop

    :goto_25
    iget-object p1, p0, Lo/b;->e:Lo/b$a;

    goto/32 :goto_42

    nop

    :goto_26
    neg-int p1, p7

    goto/32 :goto_4c

    nop

    :goto_27
    invoke-interface {p1, p2, v2}, Lo/b$a;->c(Lo/i;F)V

    goto/32 :goto_1

    nop

    :goto_28
    invoke-interface {p1, p6, v2}, Lo/b$a;->c(Lo/i;F)V

    goto/32 :goto_45

    nop

    :goto_29
    neg-int p1, p3

    goto/32 :goto_1b

    nop

    :goto_2a
    iget-object p1, p0, Lo/b;->e:Lo/b$a;

    goto/32 :goto_23

    nop

    :goto_2b
    goto :goto_32

    :goto_2c
    goto/32 :goto_41

    nop

    :goto_2d
    iget-object p1, p0, Lo/b;->e:Lo/b$a;

    goto/32 :goto_44

    nop

    :goto_2e
    add-int/2addr p1, p7

    :goto_2f
    goto/32 :goto_37

    nop

    :goto_30
    if-eq p2, p5, :cond_4

    goto/32 :goto_6

    :cond_4
    goto/32 :goto_e

    nop

    :goto_31
    goto :goto_21

    :goto_32
    goto/32 :goto_b

    nop

    :goto_33
    if-gtz p7, :cond_5

    goto/32 :goto_32

    :cond_5
    :goto_34
    goto/32 :goto_10

    nop

    :goto_35
    invoke-interface {v1, p1, v4}, Lo/b$a;->c(Lo/i;F)V

    goto/32 :goto_1d

    nop

    :goto_36
    mul-float/2addr v2, p4

    goto/32 :goto_3f

    nop

    :goto_37
    int-to-float p1, p1

    goto/32 :goto_11

    nop

    :goto_38
    add-float/2addr p1, p2

    goto/32 :goto_31

    nop

    :goto_39
    mul-float v4, v3, v0

    goto/32 :goto_35

    nop

    :goto_3a
    if-gtz p7, :cond_6

    goto/32 :goto_32

    :cond_6
    :goto_3b
    goto/32 :goto_29

    nop

    :goto_3c
    mul-float v1, v3, v2

    goto/32 :goto_4

    nop

    :goto_3d
    invoke-interface {p4, p1, v2}, Lo/b$a;->c(Lo/i;F)V

    goto/32 :goto_25

    nop

    :goto_3e
    const/high16 v1, 0x3f000000    # 0.5f

    goto/32 :goto_a

    nop

    :goto_3f
    invoke-interface {p1, p5, v2}, Lo/b$a;->c(Lo/i;F)V

    goto/32 :goto_0

    nop

    :goto_40
    iget-object p4, p0, Lo/b;->e:Lo/b$a;

    goto/32 :goto_3d

    nop

    :goto_41
    cmpl-float v1, p4, v0

    goto/32 :goto_4a

    nop

    :goto_42
    invoke-interface {p1, p2, v0}, Lo/b$a;->c(Lo/i;F)V

    goto/32 :goto_20

    nop

    :goto_43
    mul-float/2addr p2, p4

    goto/32 :goto_38

    nop

    :goto_44
    invoke-interface {p1, p6, v0}, Lo/b$a;->c(Lo/i;F)V

    goto/32 :goto_4b

    nop

    :goto_45
    iget-object p1, p0, Lo/b;->e:Lo/b$a;

    goto/32 :goto_22

    nop

    :goto_46
    int-to-float p2, p7

    goto/32 :goto_43

    nop

    :goto_47
    iget-object p1, p0, Lo/b;->e:Lo/b$a;

    goto/32 :goto_36

    nop

    :goto_48
    invoke-interface {p1, p2, p3}, Lo/b$a;->c(Lo/i;F)V

    goto/32 :goto_5

    nop

    :goto_49
    mul-float/2addr v0, p4

    goto/32 :goto_8

    nop

    :goto_4a
    if-gez v1, :cond_7

    goto/32 :goto_4d

    :cond_7
    goto/32 :goto_1a

    nop

    :goto_4b
    iget-object p1, p0, Lo/b;->e:Lo/b$a;

    goto/32 :goto_17

    nop

    :goto_4c
    goto/16 :goto_2f

    :goto_4d
    goto/32 :goto_1f

    nop
.end method

.method i(Lo/i;I)Lo/b;
    .locals 0

    goto/32 :goto_4

    nop

    :goto_0
    int-to-float p2, p2

    goto/32 :goto_5

    nop

    :goto_1
    iput p2, p0, Lo/b;->b:F

    goto/32 :goto_2

    nop

    :goto_2
    const/4 p1, 0x1

    goto/32 :goto_6

    nop

    :goto_3
    return-object p0

    :goto_4
    iput-object p1, p0, Lo/b;->a:Lo/i;

    goto/32 :goto_0

    nop

    :goto_5
    iput p2, p1, Lo/i;->f:F

    goto/32 :goto_1

    nop

    :goto_6
    iput-boolean p1, p0, Lo/b;->f:Z

    goto/32 :goto_3

    nop
.end method

.method public isEmpty()Z
    .locals 2

    iget-object v0, p0, Lo/b;->a:Lo/i;

    if-nez v0, :cond_0

    iget v0, p0, Lo/b;->b:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    iget-object v0, p0, Lo/b;->e:Lo/b$a;

    invoke-interface {v0}, Lo/b$a;->d()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method j(Lo/i;Lo/i;F)Lo/b;
    .locals 2

    goto/32 :goto_3

    nop

    :goto_0
    return-object p0

    :goto_1
    const/high16 v1, -0x40800000    # -1.0f

    goto/32 :goto_2

    nop

    :goto_2
    invoke-interface {v0, p1, v1}, Lo/b$a;->c(Lo/i;F)V

    goto/32 :goto_5

    nop

    :goto_3
    iget-object v0, p0, Lo/b;->e:Lo/b$a;

    goto/32 :goto_1

    nop

    :goto_4
    invoke-interface {p1, p2, p3}, Lo/b$a;->c(Lo/i;F)V

    goto/32 :goto_0

    nop

    :goto_5
    iget-object p1, p0, Lo/b;->e:Lo/b$a;

    goto/32 :goto_4

    nop
.end method

.method public k(Lo/i;Lo/i;Lo/i;Lo/i;F)Lo/b;
    .locals 2

    iget-object v0, p0, Lo/b;->e:Lo/b$a;

    const/high16 v1, -0x40800000    # -1.0f

    invoke-interface {v0, p1, v1}, Lo/b$a;->c(Lo/i;F)V

    iget-object p1, p0, Lo/b;->e:Lo/b$a;

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-interface {p1, p2, v0}, Lo/b$a;->c(Lo/i;F)V

    iget-object p1, p0, Lo/b;->e:Lo/b$a;

    invoke-interface {p1, p3, p5}, Lo/b$a;->c(Lo/i;F)V

    iget-object p1, p0, Lo/b;->e:Lo/b$a;

    neg-float p2, p5

    invoke-interface {p1, p4, p2}, Lo/b$a;->c(Lo/i;F)V

    return-object p0
.end method

.method public l(FFFLo/i;Lo/i;Lo/i;Lo/i;)Lo/b;
    .locals 4

    const/4 v0, 0x0

    iput v0, p0, Lo/b;->b:F

    cmpl-float v1, p2, v0

    const/high16 v2, -0x40800000    # -1.0f

    const/high16 v3, 0x3f800000    # 1.0f

    if-eqz v1, :cond_3

    cmpl-float v1, p1, p3

    if-nez v1, :cond_0

    goto :goto_0

    :cond_0
    cmpl-float v1, p1, v0

    if-nez v1, :cond_1

    iget-object p1, p0, Lo/b;->e:Lo/b$a;

    invoke-interface {p1, p4, v3}, Lo/b$a;->c(Lo/i;F)V

    iget-object p1, p0, Lo/b;->e:Lo/b$a;

    invoke-interface {p1, p5, v2}, Lo/b$a;->c(Lo/i;F)V

    goto :goto_1

    :cond_1
    cmpl-float v0, p3, v0

    if-nez v0, :cond_2

    iget-object p1, p0, Lo/b;->e:Lo/b$a;

    invoke-interface {p1, p6, v3}, Lo/b$a;->c(Lo/i;F)V

    iget-object p1, p0, Lo/b;->e:Lo/b$a;

    invoke-interface {p1, p7, v2}, Lo/b$a;->c(Lo/i;F)V

    goto :goto_1

    :cond_2
    div-float/2addr p1, p2

    div-float/2addr p3, p2

    div-float/2addr p1, p3

    iget-object p2, p0, Lo/b;->e:Lo/b$a;

    invoke-interface {p2, p4, v3}, Lo/b$a;->c(Lo/i;F)V

    iget-object p2, p0, Lo/b;->e:Lo/b$a;

    invoke-interface {p2, p5, v2}, Lo/b$a;->c(Lo/i;F)V

    iget-object p2, p0, Lo/b;->e:Lo/b$a;

    invoke-interface {p2, p7, p1}, Lo/b$a;->c(Lo/i;F)V

    iget-object p2, p0, Lo/b;->e:Lo/b$a;

    neg-float p1, p1

    invoke-interface {p2, p6, p1}, Lo/b$a;->c(Lo/i;F)V

    goto :goto_1

    :cond_3
    :goto_0
    iget-object p1, p0, Lo/b;->e:Lo/b$a;

    invoke-interface {p1, p4, v3}, Lo/b$a;->c(Lo/i;F)V

    iget-object p1, p0, Lo/b;->e:Lo/b$a;

    invoke-interface {p1, p5, v2}, Lo/b$a;->c(Lo/i;F)V

    iget-object p1, p0, Lo/b;->e:Lo/b$a;

    invoke-interface {p1, p7, v3}, Lo/b$a;->c(Lo/i;F)V

    iget-object p1, p0, Lo/b;->e:Lo/b$a;

    invoke-interface {p1, p6, v2}, Lo/b$a;->c(Lo/i;F)V

    :goto_1
    return-object p0
.end method

.method public m(Lo/i;I)Lo/b;
    .locals 1

    if-gez p2, :cond_0

    mul-int/lit8 p2, p2, -0x1

    int-to-float p2, p2

    iput p2, p0, Lo/b;->b:F

    iget-object p2, p0, Lo/b;->e:Lo/b$a;

    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0

    :cond_0
    int-to-float p2, p2

    iput p2, p0, Lo/b;->b:F

    iget-object p2, p0, Lo/b;->e:Lo/b$a;

    const/high16 v0, -0x40800000    # -1.0f

    :goto_0
    invoke-interface {p2, p1, v0}, Lo/b$a;->c(Lo/i;F)V

    return-object p0
.end method

.method public n(Lo/i;Lo/i;I)Lo/b;
    .locals 2

    const/4 v0, 0x0

    if-eqz p3, :cond_1

    if-gez p3, :cond_0

    mul-int/lit8 p3, p3, -0x1

    const/4 v0, 0x1

    :cond_0
    int-to-float p3, p3

    iput p3, p0, Lo/b;->b:F

    :cond_1
    const/high16 p3, -0x40800000    # -1.0f

    const/high16 v1, 0x3f800000    # 1.0f

    if-nez v0, :cond_2

    iget-object v0, p0, Lo/b;->e:Lo/b$a;

    invoke-interface {v0, p1, p3}, Lo/b$a;->c(Lo/i;F)V

    iget-object p1, p0, Lo/b;->e:Lo/b$a;

    invoke-interface {p1, p2, v1}, Lo/b$a;->c(Lo/i;F)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lo/b;->e:Lo/b$a;

    invoke-interface {v0, p1, v1}, Lo/b$a;->c(Lo/i;F)V

    iget-object p1, p0, Lo/b;->e:Lo/b$a;

    invoke-interface {p1, p2, p3}, Lo/b$a;->c(Lo/i;F)V

    :goto_0
    return-object p0
.end method

.method public o(Lo/i;Lo/i;Lo/i;I)Lo/b;
    .locals 2

    const/4 v0, 0x0

    if-eqz p4, :cond_1

    if-gez p4, :cond_0

    mul-int/lit8 p4, p4, -0x1

    const/4 v0, 0x1

    :cond_0
    int-to-float p4, p4

    iput p4, p0, Lo/b;->b:F

    :cond_1
    const/high16 p4, -0x40800000    # -1.0f

    const/high16 v1, 0x3f800000    # 1.0f

    if-nez v0, :cond_2

    iget-object v0, p0, Lo/b;->e:Lo/b$a;

    invoke-interface {v0, p1, p4}, Lo/b$a;->c(Lo/i;F)V

    iget-object p1, p0, Lo/b;->e:Lo/b$a;

    invoke-interface {p1, p2, v1}, Lo/b$a;->c(Lo/i;F)V

    iget-object p1, p0, Lo/b;->e:Lo/b$a;

    invoke-interface {p1, p3, v1}, Lo/b$a;->c(Lo/i;F)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lo/b;->e:Lo/b$a;

    invoke-interface {v0, p1, v1}, Lo/b$a;->c(Lo/i;F)V

    iget-object p1, p0, Lo/b;->e:Lo/b$a;

    invoke-interface {p1, p2, p4}, Lo/b$a;->c(Lo/i;F)V

    iget-object p1, p0, Lo/b;->e:Lo/b$a;

    invoke-interface {p1, p3, p4}, Lo/b$a;->c(Lo/i;F)V

    :goto_0
    return-object p0
.end method

.method public p(Lo/i;Lo/i;Lo/i;I)Lo/b;
    .locals 2

    const/4 v0, 0x0

    if-eqz p4, :cond_1

    if-gez p4, :cond_0

    mul-int/lit8 p4, p4, -0x1

    const/4 v0, 0x1

    :cond_0
    int-to-float p4, p4

    iput p4, p0, Lo/b;->b:F

    :cond_1
    const/high16 p4, -0x40800000    # -1.0f

    const/high16 v1, 0x3f800000    # 1.0f

    if-nez v0, :cond_2

    iget-object v0, p0, Lo/b;->e:Lo/b$a;

    invoke-interface {v0, p1, p4}, Lo/b$a;->c(Lo/i;F)V

    iget-object p1, p0, Lo/b;->e:Lo/b$a;

    invoke-interface {p1, p2, v1}, Lo/b$a;->c(Lo/i;F)V

    iget-object p1, p0, Lo/b;->e:Lo/b$a;

    invoke-interface {p1, p3, p4}, Lo/b$a;->c(Lo/i;F)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lo/b;->e:Lo/b$a;

    invoke-interface {v0, p1, v1}, Lo/b$a;->c(Lo/i;F)V

    iget-object p1, p0, Lo/b;->e:Lo/b$a;

    invoke-interface {p1, p2, p4}, Lo/b$a;->c(Lo/i;F)V

    iget-object p1, p0, Lo/b;->e:Lo/b$a;

    invoke-interface {p1, p3, v1}, Lo/b$a;->c(Lo/i;F)V

    :goto_0
    return-object p0
.end method

.method public q(Lo/i;Lo/i;Lo/i;Lo/i;F)Lo/b;
    .locals 2

    iget-object v0, p0, Lo/b;->e:Lo/b$a;

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-interface {v0, p3, v1}, Lo/b$a;->c(Lo/i;F)V

    iget-object p3, p0, Lo/b;->e:Lo/b$a;

    invoke-interface {p3, p4, v1}, Lo/b$a;->c(Lo/i;F)V

    iget-object p3, p0, Lo/b;->e:Lo/b$a;

    const/high16 p4, -0x41000000    # -0.5f

    invoke-interface {p3, p1, p4}, Lo/b$a;->c(Lo/i;F)V

    iget-object p1, p0, Lo/b;->e:Lo/b$a;

    invoke-interface {p1, p2, p4}, Lo/b$a;->c(Lo/i;F)V

    neg-float p1, p5

    iput p1, p0, Lo/b;->b:F

    return-object p0
.end method

.method r()V
    .locals 2

    goto/32 :goto_a

    nop

    :goto_0
    iput v0, p0, Lo/b;->b:F

    goto/32 :goto_9

    nop

    :goto_1
    if-ltz v1, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_8

    nop

    :goto_2
    mul-float/2addr v0, v1

    goto/32 :goto_0

    nop

    :goto_3
    invoke-interface {v0}, Lo/b$a;->k()V

    :goto_4
    goto/32 :goto_5

    nop

    :goto_5
    return-void

    :goto_6
    const/4 v1, 0x0

    goto/32 :goto_7

    nop

    :goto_7
    cmpg-float v1, v0, v1

    goto/32 :goto_1

    nop

    :goto_8
    const/high16 v1, -0x40800000    # -1.0f

    goto/32 :goto_2

    nop

    :goto_9
    iget-object v0, p0, Lo/b;->e:Lo/b$a;

    goto/32 :goto_3

    nop

    :goto_a
    iget v0, p0, Lo/b;->b:F

    goto/32 :goto_6

    nop
.end method

.method s()Z
    .locals 2

    goto/32 :goto_e

    nop

    :goto_0
    goto :goto_4

    :goto_1
    goto/32 :goto_3

    nop

    :goto_2
    if-ne v0, v1, :cond_0

    goto/32 :goto_c

    :cond_0
    goto/32 :goto_7

    nop

    :goto_3
    const/4 v0, 0x0

    :goto_4
    goto/32 :goto_a

    nop

    :goto_5
    if-nez v0, :cond_1

    goto/32 :goto_1

    :cond_1
    goto/32 :goto_6

    nop

    :goto_6
    iget-object v0, v0, Lo/i;->j:Lo/i$a;

    goto/32 :goto_d

    nop

    :goto_7
    iget v0, p0, Lo/b;->b:F

    goto/32 :goto_9

    nop

    :goto_8
    const/4 v0, 0x1

    goto/32 :goto_0

    nop

    :goto_9
    const/4 v1, 0x0

    goto/32 :goto_f

    nop

    :goto_a
    return v0

    :goto_b
    if-gez v0, :cond_2

    goto/32 :goto_1

    :cond_2
    :goto_c
    goto/32 :goto_8

    nop

    :goto_d
    sget-object v1, Lo/i$a;->a:Lo/i$a;

    goto/32 :goto_2

    nop

    :goto_e
    iget-object v0, p0, Lo/b;->a:Lo/i;

    goto/32 :goto_5

    nop

    :goto_f
    cmpg-float v0, v0, v1

    goto/32 :goto_b

    nop
.end method

.method t(Lo/i;)Z
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    return p1

    :goto_1
    invoke-interface {v0, p1}, Lo/b$a;->e(Lo/i;)Z

    move-result p1

    goto/32 :goto_0

    nop

    :goto_2
    iget-object v0, p0, Lo/b;->e:Lo/b$a;

    goto/32 :goto_1

    nop
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lo/b;->z()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public v(Lo/i;)Lo/i;
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lo/b;->w([ZLo/i;)Lo/i;

    move-result-object p1

    return-object p1
.end method

.method x(Lo/i;)V
    .locals 3

    goto/32 :goto_19

    nop

    :goto_0
    if-eqz p1, :cond_0

    goto/32 :goto_10

    :cond_0
    goto/32 :goto_f

    nop

    :goto_1
    iput v2, v0, Lo/i;->d:I

    goto/32 :goto_7

    nop

    :goto_2
    div-float/2addr p1, v0

    goto/32 :goto_17

    nop

    :goto_3
    invoke-interface {v2, v0, v1}, Lo/b$a;->c(Lo/i;F)V

    goto/32 :goto_8

    nop

    :goto_4
    iget-object p1, p0, Lo/b;->e:Lo/b$a;

    goto/32 :goto_c

    nop

    :goto_5
    const/4 v2, -0x1

    goto/32 :goto_1

    nop

    :goto_6
    const/high16 v1, -0x40800000    # -1.0f

    goto/32 :goto_13

    nop

    :goto_7
    const/4 v0, 0x0

    goto/32 :goto_d

    nop

    :goto_8
    iget-object v0, p0, Lo/b;->a:Lo/i;

    goto/32 :goto_5

    nop

    :goto_9
    const/4 v2, 0x1

    goto/32 :goto_a

    nop

    :goto_a
    invoke-interface {v0, p1, v2}, Lo/b$a;->j(Lo/i;Z)F

    move-result v0

    goto/32 :goto_16

    nop

    :goto_b
    cmpl-float p1, v0, p1

    goto/32 :goto_0

    nop

    :goto_c
    invoke-interface {p1, v0}, Lo/b$a;->i(F)V

    goto/32 :goto_18

    nop

    :goto_d
    iput-object v0, p0, Lo/b;->a:Lo/i;

    :goto_e
    goto/32 :goto_11

    nop

    :goto_f
    return-void

    :goto_10
    goto/32 :goto_15

    nop

    :goto_11
    iget-object v0, p0, Lo/b;->e:Lo/b$a;

    goto/32 :goto_9

    nop

    :goto_12
    iget-object v2, p0, Lo/b;->e:Lo/b$a;

    goto/32 :goto_3

    nop

    :goto_13
    if-nez v0, :cond_1

    goto/32 :goto_e

    :cond_1
    goto/32 :goto_12

    nop

    :goto_14
    iput-object p1, p0, Lo/b;->a:Lo/i;

    goto/32 :goto_1a

    nop

    :goto_15
    iget p1, p0, Lo/b;->b:F

    goto/32 :goto_2

    nop

    :goto_16
    mul-float/2addr v0, v1

    goto/32 :goto_14

    nop

    :goto_17
    iput p1, p0, Lo/b;->b:F

    goto/32 :goto_4

    nop

    :goto_18
    return-void

    :goto_19
    iget-object v0, p0, Lo/b;->a:Lo/i;

    goto/32 :goto_6

    nop

    :goto_1a
    const/high16 p1, 0x3f800000    # 1.0f

    goto/32 :goto_b

    nop
.end method

.method public y()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lo/b;->a:Lo/i;

    iget-object v0, p0, Lo/b;->e:Lo/b$a;

    invoke-interface {v0}, Lo/b$a;->clear()V

    const/4 v0, 0x0

    iput v0, p0, Lo/b;->b:F

    const/4 v0, 0x0

    iput-boolean v0, p0, Lo/b;->f:Z

    return-void
.end method

.method z()Ljava/lang/String;
    .locals 10

    goto/32 :goto_55

    nop

    :goto_0
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_34

    nop

    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_4e

    nop

    :goto_2
    move v1, v4

    goto/32 :goto_37

    nop

    :goto_3
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_3b

    nop

    :goto_4
    goto/16 :goto_6b

    :goto_5
    goto/32 :goto_4b

    nop

    :goto_6
    const-string v0, "0.0"

    goto/32 :goto_51

    nop

    :goto_7
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_17

    nop

    :goto_8
    add-int/lit8 v3, v3, 0x1

    goto/32 :goto_2e

    nop

    :goto_9
    if-lt v3, v5, :cond_0

    goto/32 :goto_2f

    :cond_0
    goto/32 :goto_a

    nop

    :goto_a
    iget-object v6, p0, Lo/b;->e:Lo/b$a;

    goto/32 :goto_28

    nop

    :goto_b
    const-string v0, " = "

    goto/32 :goto_23

    nop

    :goto_c
    return-object v0

    :goto_d
    invoke-interface {v7, v3}, Lo/b$a;->a(I)F

    move-result v7

    goto/32 :goto_21

    nop

    :goto_e
    invoke-virtual {v6}, Lo/i;->toString()Ljava/lang/String;

    move-result-object v6

    goto/32 :goto_67

    nop

    :goto_f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_53

    nop

    :goto_10
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_42

    nop

    :goto_11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_6a

    nop

    :goto_12
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_15

    nop

    :goto_13
    goto/16 :goto_66

    :goto_14
    goto/32 :goto_69

    nop

    :goto_15
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_5e

    nop

    :goto_16
    if-eqz v1, :cond_1

    goto/32 :goto_1f

    :cond_1
    goto/32 :goto_5b

    nop

    :goto_17
    iget v0, p0, Lo/b;->b:F

    goto/32 :goto_3c

    nop

    :goto_18
    move v1, v3

    :goto_19
    goto/32 :goto_1a

    nop

    :goto_1a
    iget-object v5, p0, Lo/b;->e:Lo/b$a;

    goto/32 :goto_47

    nop

    :goto_1b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_5a

    nop

    :goto_1c
    iget v1, p0, Lo/b;->b:F

    goto/32 :goto_6e

    nop

    :goto_1d
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    goto/32 :goto_49

    nop

    :goto_1e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1f
    goto/32 :goto_c

    nop

    :goto_20
    if-eqz v1, :cond_2

    goto/32 :goto_36

    :cond_2
    goto/32 :goto_45

    nop

    :goto_21
    cmpl-float v8, v7, v2

    goto/32 :goto_4f

    nop

    :goto_22
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_1c

    nop

    :goto_23
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_22

    nop

    :goto_24
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_2a

    nop

    :goto_25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :goto_26
    goto/32 :goto_29

    nop

    :goto_27
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_65

    nop

    :goto_28
    invoke-interface {v6, v3}, Lo/b$a;->h(I)Lo/i;

    move-result-object v6

    goto/32 :goto_60

    nop

    :goto_29
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_43

    nop

    :goto_2a
    goto :goto_26

    :goto_2b
    goto/32 :goto_1

    nop

    :goto_2c
    const-string v0, "- "

    goto/32 :goto_13

    nop

    :goto_2d
    if-eqz v1, :cond_3

    goto/32 :goto_14

    :cond_3
    goto/32 :goto_54

    nop

    :goto_2e
    goto/16 :goto_48

    :goto_2f
    goto/32 :goto_16

    nop

    :goto_30
    const-string v1, ""

    goto/32 :goto_63

    nop

    :goto_31
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_35

    nop

    :goto_32
    goto/16 :goto_5f

    :goto_33
    goto/32 :goto_56

    nop

    :goto_34
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_41

    nop

    :goto_35
    goto :goto_4a

    :goto_36
    goto/32 :goto_40

    nop

    :goto_37
    goto/16 :goto_19

    :goto_38
    goto/32 :goto_18

    nop

    :goto_39
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_61

    nop

    :goto_3a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_32

    nop

    :goto_3b
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_6

    nop

    :goto_3c
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    goto/32 :goto_59

    nop

    :goto_3d
    const/4 v4, 0x1

    goto/32 :goto_3f

    nop

    :goto_3e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_46

    nop

    :goto_3f
    if-nez v1, :cond_4

    goto/32 :goto_38

    :cond_4
    goto/32 :goto_6f

    nop

    :goto_40
    new-instance v1, Ljava/lang/StringBuilder;

    goto/32 :goto_3e

    nop

    :goto_41
    const-string v0, " + "

    goto/32 :goto_50

    nop

    :goto_42
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_2c

    nop

    :goto_43
    new-instance v1, Ljava/lang/StringBuilder;

    goto/32 :goto_f

    nop

    :goto_44
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_4d

    nop

    :goto_45
    new-instance v1, Ljava/lang/StringBuilder;

    goto/32 :goto_31

    nop

    :goto_46
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_1d

    nop

    :goto_47
    invoke-interface {v5}, Lo/b$a;->d()I

    move-result v5

    :goto_48
    goto/32 :goto_9

    nop

    :goto_49
    const-string v0, " "

    :goto_4a
    goto/32 :goto_44

    nop

    :goto_4b
    iget-object v7, p0, Lo/b;->e:Lo/b$a;

    goto/32 :goto_d

    nop

    :goto_4c
    cmpl-float v1, v7, v1

    goto/32 :goto_20

    nop

    :goto_4d
    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_11

    nop

    :goto_4e
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_39

    nop

    :goto_4f
    if-eqz v8, :cond_5

    goto/32 :goto_6d

    :cond_5
    goto/32 :goto_6c

    nop

    :goto_50
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_3a

    nop

    :goto_51
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_1e

    nop

    :goto_52
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_62

    nop

    :goto_53
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_b

    nop

    :goto_54
    cmpg-float v1, v7, v2

    goto/32 :goto_5c

    nop

    :goto_55
    iget-object v0, p0, Lo/b;->a:Lo/i;

    goto/32 :goto_30

    nop

    :goto_56
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_27

    nop

    :goto_57
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_7

    nop

    :goto_58
    new-instance v1, Ljava/lang/StringBuilder;

    goto/32 :goto_10

    nop

    :goto_59
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_2

    nop

    :goto_5a
    const-string v1, "0"

    goto/32 :goto_24

    nop

    :goto_5b
    new-instance v1, Ljava/lang/StringBuilder;

    goto/32 :goto_3

    nop

    :goto_5c
    if-ltz v1, :cond_6

    goto/32 :goto_5f

    :cond_6
    goto/32 :goto_58

    nop

    :goto_5d
    const/4 v3, 0x0

    goto/32 :goto_3d

    nop

    :goto_5e
    mul-float/2addr v7, v9

    :goto_5f
    goto/32 :goto_70

    nop

    :goto_60
    if-eqz v6, :cond_7

    goto/32 :goto_5

    :cond_7
    goto/32 :goto_4

    nop

    :goto_61
    iget-object v1, p0, Lo/b;->a:Lo/i;

    goto/32 :goto_25

    nop

    :goto_62
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_1b

    nop

    :goto_63
    if-eqz v0, :cond_8

    goto/32 :goto_2b

    :cond_8
    goto/32 :goto_52

    nop

    :goto_64
    if-gtz v8, :cond_9

    goto/32 :goto_33

    :cond_9
    goto/32 :goto_0

    nop

    :goto_65
    const-string v0, " - "

    :goto_66
    goto/32 :goto_12

    nop

    :goto_67
    const/high16 v9, -0x40800000    # -1.0f

    goto/32 :goto_2d

    nop

    :goto_68
    cmpl-float v1, v1, v2

    goto/32 :goto_5d

    nop

    :goto_69
    new-instance v1, Ljava/lang/StringBuilder;

    goto/32 :goto_64

    nop

    :goto_6a
    move v1, v4

    :goto_6b
    goto/32 :goto_8

    nop

    :goto_6c
    goto :goto_6b

    :goto_6d
    goto/32 :goto_e

    nop

    :goto_6e
    const/4 v2, 0x0

    goto/32 :goto_68

    nop

    :goto_6f
    new-instance v1, Ljava/lang/StringBuilder;

    goto/32 :goto_57

    nop

    :goto_70
    const/high16 v1, 0x3f800000    # 1.0f

    goto/32 :goto_4c

    nop
.end method
