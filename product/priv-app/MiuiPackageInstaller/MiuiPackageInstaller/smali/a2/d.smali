.class public La2/d;
.super Ljava/lang/Object;

# interfaces
.implements La2/b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        La2/d$c;,
        La2/d$b;
    }
.end annotation


# instance fields
.field private a:Landroid/content/Context;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "La2/d$c;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/util/concurrent/ExecutorService;


# direct methods
.method private constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "None_Network"

    iput-object v0, p0, La2/d;->b:Ljava/lang/String;

    const-string v1, "unknown"

    iput-object v1, p0, La2/d;->c:Ljava/lang/String;

    iput-object v0, p0, La2/d;->d:Ljava/lang/String;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, La2/d;->e:Ljava/util/ArrayList;

    const-string v0, "network"

    invoke-static {v0}, Ly1/c;->a(Ljava/lang/String;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, La2/d;->f:Ljava/util/concurrent/ExecutorService;

    return-void
.end method

.method synthetic constructor <init>(La2/d$a;)V
    .locals 0

    invoke-direct {p0}, La2/d;-><init>()V

    return-void
.end method

.method private static c(Landroid/content/Context;Ljava/lang/String;)I
    .locals 2

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    invoke-virtual {p0, p1, v0, v1}, Landroid/content/Context;->checkPermission(Ljava/lang/String;II)I

    move-result p0

    return p0
.end method

.method public static d()La2/d;
    .locals 1

    invoke-static {}, La2/d$b;->a()La2/d;

    move-result-object v0

    return-object v0
.end method

.method static synthetic e(La2/d;)Ljava/lang/String;
    .locals 0

    invoke-direct {p0}, La2/d;->s()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static synthetic f(La2/d;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, La2/d;->d:Ljava/lang/String;

    return-object p1
.end method

.method private static g(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    const/4 v0, 0x0

    :try_start_0
    const-string v1, "android.permission.ACCESS_FINE_LOCATION"

    invoke-static {p0, v1}, La2/d;->c(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p0

    const-string v1, "wifi"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/net/wifi/WifiManager;

    invoke-virtual {p0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object p0

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    return-object v0

    :catchall_0
    move-exception p0

    const-string v1, "get ssid fail"

    invoke-static {v1, p0}, Lz1/a;->j(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-object v0
.end method

.method static synthetic h(La2/d;)Ljava/util/ArrayList;
    .locals 0

    iget-object p0, p0, La2/d;->e:Ljava/util/ArrayList;

    return-object p0
.end method

.method static synthetic i(La2/d;)Ljava/util/concurrent/ExecutorService;
    .locals 0

    iget-object p0, p0, La2/d;->f:Ljava/util/concurrent/ExecutorService;

    return-object p0
.end method

.method static synthetic k(La2/d;Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, La2/d;->p(Landroid/content/Context;)V

    return-void
.end method

.method private static m(Landroid/content/Context;)Z
    .locals 2

    const/4 v0, 0x0

    :try_start_0
    const-string v1, "android.permission.ACCESS_NETWORK_STATE"

    invoke-static {p0, v1}, La2/d;->c(Landroid/content/Context;Ljava/lang/String;)I

    move-result p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez p0, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0

    :catchall_0
    move-exception p0

    const-string v1, "check network info permission fail"

    invoke-static {v1, p0}, Lz1/a;->j(Ljava/lang/String;Ljava/lang/Throwable;)V

    return v0
.end method

.method static synthetic n(La2/d;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, La2/d;->d:Ljava/lang/String;

    return-object p0
.end method

.method private static o(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    :try_start_0
    const-string v0, "android.permission.READ_PHONE_STATE"

    invoke-static {p0, v0}, La2/d;->c(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "phone"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/telephony/TelephonyManager;

    invoke-virtual {p0}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    return-object p0

    :catchall_0
    move-exception p0

    const-string v0, "getCellSP fail"

    invoke-static {v0, p0}, Lz1/a;->j(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    const-string p0, "unknown"

    return-object p0
.end method

.method private p(Landroid/content/Context;)V
    .locals 4

    const-string v0, "unknown"

    iput-object v0, p0, La2/d;->c:Ljava/lang/String;

    iput-object v0, p0, La2/d;->b:Ljava/lang/String;

    invoke-static {}, Lo1/e;->b()Z

    move-result v1

    if-nez v1, :cond_0

    return-void

    :cond_0
    :try_start_0
    invoke-static {p1}, La2/d;->m(Landroid/content/Context;)Z

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_3

    iget-object p1, p0, La2/d;->c:Ljava/lang/String;

    if-nez p1, :cond_1

    iput-object v0, p0, La2/d;->c:Ljava/lang/String;

    :cond_1
    iget-object p1, p0, La2/d;->b:Ljava/lang/String;

    if-nez p1, :cond_2

    iput-object v0, p0, La2/d;->b:Ljava/lang/String;

    :cond_2
    return-void

    :cond_3
    :try_start_1
    const-string v1, "connectivity"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v1, :cond_6

    iget-object p1, p0, La2/d;->c:Ljava/lang/String;

    if-nez p1, :cond_4

    iput-object v0, p0, La2/d;->c:Ljava/lang/String;

    :cond_4
    iget-object p1, p0, La2/d;->b:Ljava/lang/String;

    if-nez p1, :cond_5

    iput-object v0, p0, La2/d;->b:Ljava/lang/String;

    :cond_5
    return-void

    :cond_6
    :try_start_2
    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-nez v1, :cond_9

    iget-object p1, p0, La2/d;->c:Ljava/lang/String;

    if-nez p1, :cond_7

    iput-object v0, p0, La2/d;->c:Ljava/lang/String;

    :cond_7
    iget-object p1, p0, La2/d;->b:Ljava/lang/String;

    if-nez p1, :cond_8

    iput-object v0, p0, La2/d;->b:Ljava/lang/String;

    :cond_8
    return-void

    :cond_9
    :try_start_3
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isAvailable()Z

    move-result v2

    if-eqz v2, :cond_1a

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    if-nez v2, :cond_a

    goto/16 :goto_0

    :cond_a
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getType()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_d

    invoke-static {p1}, La2/d;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, La2/d;->c:Ljava/lang/String;

    if-nez p1, :cond_b

    iput-object v0, p0, La2/d;->c:Ljava/lang/String;

    :cond_b
    const-string p1, "wifi"

    iput-object p1, p0, La2/d;->b:Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    iget-object p1, p0, La2/d;->c:Ljava/lang/String;

    if-nez p1, :cond_c

    iput-object v0, p0, La2/d;->c:Ljava/lang/String;

    :cond_c
    return-void

    :cond_d
    :try_start_4
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getType()I

    move-result v2

    if-nez v2, :cond_17

    invoke-static {p1}, La2/d;->o(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, La2/d;->c:Ljava/lang/String;

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result p1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    const/16 v1, 0xd

    if-eq p1, v1, :cond_15

    const/16 v1, 0xf

    if-eq p1, v1, :cond_13

    const/16 v1, 0x14

    if-eq p1, v1, :cond_11

    packed-switch p1, :pswitch_data_0

    iget-object p1, p0, La2/d;->c:Ljava/lang/String;

    if-nez p1, :cond_e

    iput-object v0, p0, La2/d;->c:Ljava/lang/String;

    :cond_e
    iget-object p1, p0, La2/d;->b:Ljava/lang/String;

    if-nez p1, :cond_f

    iput-object v0, p0, La2/d;->b:Ljava/lang/String;

    :cond_f
    return-void

    :pswitch_0
    :try_start_5
    const-string p1, "2g"

    iput-object p1, p0, La2/d;->b:Ljava/lang/String;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    iget-object p1, p0, La2/d;->c:Ljava/lang/String;

    if-nez p1, :cond_10

    iput-object v0, p0, La2/d;->c:Ljava/lang/String;

    :cond_10
    return-void

    :cond_11
    :try_start_6
    const-string p1, "5g"

    iput-object p1, p0, La2/d;->b:Ljava/lang/String;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    iget-object p1, p0, La2/d;->c:Ljava/lang/String;

    if-nez p1, :cond_12

    iput-object v0, p0, La2/d;->c:Ljava/lang/String;

    :cond_12
    return-void

    :cond_13
    :pswitch_1
    :try_start_7
    const-string p1, "3g"

    iput-object p1, p0, La2/d;->b:Ljava/lang/String;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    iget-object p1, p0, La2/d;->c:Ljava/lang/String;

    if-nez p1, :cond_14

    iput-object v0, p0, La2/d;->c:Ljava/lang/String;

    :cond_14
    return-void

    :cond_15
    :try_start_8
    const-string p1, "4g"

    iput-object p1, p0, La2/d;->b:Ljava/lang/String;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    iget-object p1, p0, La2/d;->c:Ljava/lang/String;

    if-nez p1, :cond_16

    iput-object v0, p0, La2/d;->c:Ljava/lang/String;

    :cond_16
    return-void

    :cond_17
    iget-object p1, p0, La2/d;->c:Ljava/lang/String;

    if-nez p1, :cond_18

    iput-object v0, p0, La2/d;->c:Ljava/lang/String;

    :cond_18
    iget-object p1, p0, La2/d;->b:Ljava/lang/String;

    if-nez p1, :cond_19

    iput-object v0, p0, La2/d;->b:Ljava/lang/String;

    :cond_19
    return-void

    :cond_1a
    :goto_0
    iget-object p1, p0, La2/d;->c:Ljava/lang/String;

    if-nez p1, :cond_1b

    iput-object v0, p0, La2/d;->c:Ljava/lang/String;

    :cond_1b
    iget-object p1, p0, La2/d;->b:Ljava/lang/String;

    if-nez p1, :cond_1c

    iput-object v0, p0, La2/d;->b:Ljava/lang/String;

    :cond_1c
    return-void

    :catchall_0
    move-exception p1

    :try_start_9
    const-string v1, "getNetType fail"

    invoke-static {v1, p1}, Lz1/a;->j(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    iget-object p1, p0, La2/d;->c:Ljava/lang/String;

    if-nez p1, :cond_1d

    iput-object v0, p0, La2/d;->c:Ljava/lang/String;

    :cond_1d
    iget-object p1, p0, La2/d;->b:Ljava/lang/String;

    if-nez p1, :cond_1e

    iput-object v0, p0, La2/d;->b:Ljava/lang/String;

    :cond_1e
    return-void

    :catchall_1
    move-exception p1

    iget-object v1, p0, La2/d;->c:Ljava/lang/String;

    if-nez v1, :cond_1f

    iput-object v0, p0, La2/d;->c:Ljava/lang/String;

    :cond_1f
    iget-object v1, p0, La2/d;->b:Ljava/lang/String;

    if-nez v1, :cond_20

    iput-object v0, p0, La2/d;->b:Ljava/lang/String;

    :cond_20
    throw p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic q(Landroid/content/Context;)Z
    .locals 0

    invoke-static {p0}, La2/d;->m(Landroid/content/Context;)Z

    move-result p0

    return p0
.end method

.method private s()Ljava/lang/String;
    .locals 5

    const-string v0, "None_Network"

    :try_start_0
    iget-object v1, p0, La2/d;->a:Landroid/content/Context;

    const-string v2, "connectivity"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isAvailable()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[detectCurrentNetwork] - Network name:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, " subType name: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getSubtypeName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lz1/a;->b(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v2, :cond_0

    goto :goto_0

    :cond_0
    move-object v0, v2

    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    :cond_1
    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, La2/d;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "$"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, La2/d;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b()Z
    .locals 2

    iget-object v0, p0, La2/d;->b:Ljava/lang/String;

    const-string v1, "unknown"

    if-eq v0, v1, :cond_0

    const-string v1, "wifi"

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public f()Z
    .locals 2

    iget-object v0, p0, La2/d;->b:Ljava/lang/String;

    const-string v1, "wifi"

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public j(La2/d$c;)V
    .locals 1

    iget-object v0, p0, La2/d;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public l(Landroid/content/Context;)V
    .locals 2

    if-eqz p1, :cond_2

    iget-object v0, p0, La2/d;->a:Landroid/content/Context;

    if-eqz v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, La2/d;->a:Landroid/content/Context;

    new-instance p1, La2/d$a;

    invoke-direct {p1, p0}, La2/d$a;-><init>(La2/d;)V

    :try_start_0
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, La2/d;->a:Landroid/content/Context;

    invoke-virtual {v1, p1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    iget-object p1, p0, La2/d;->a:Landroid/content/Context;

    invoke-direct {p0, p1}, La2/d;->p(Landroid/content/Context;)V

    invoke-static {}, Lz1/a;->e()Z

    move-result p1

    if-eqz p1, :cond_1

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "NetworkStateManager init "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, La2/d;->t()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lz1/a;->g(Ljava/lang/String;)V

    :cond_1
    return-void

    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Context can\'t be null"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public r()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, La2/d;->b:Ljava/lang/String;

    return-object v0
.end method

.method public t()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, La2/d;->c:Ljava/lang/String;

    return-object v0
.end method
