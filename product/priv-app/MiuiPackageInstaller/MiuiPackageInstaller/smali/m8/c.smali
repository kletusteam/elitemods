.class public abstract Lm8/c;
.super Ljava/lang/Object;

# interfaces
.implements Ls8/a;
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lm8/c$a;
    }
.end annotation


# static fields
.field public static final g:Ljava/lang/Object;


# instance fields
.field private transient a:Ls8/a;

.field protected final b:Ljava/lang/Object;

.field private final c:Ljava/lang/Class;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lm8/c$a;->a()Lm8/c$a;

    move-result-object v0

    sput-object v0, Lm8/c;->g:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    sget-object v0, Lm8/c;->g:Ljava/lang/Object;

    invoke-direct {p0, v0}, Lm8/c;-><init>(Ljava/lang/Object;)V

    return-void
.end method

.method protected constructor <init>(Ljava/lang/Object;)V
    .locals 6

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lm8/c;-><init>(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method

.method protected constructor <init>(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lm8/c;->b:Ljava/lang/Object;

    iput-object p2, p0, Lm8/c;->c:Ljava/lang/Class;

    iput-object p3, p0, Lm8/c;->d:Ljava/lang/String;

    iput-object p4, p0, Lm8/c;->e:Ljava/lang/String;

    iput-boolean p5, p0, Lm8/c;->f:Z

    return-void
.end method


# virtual methods
.method public c()Ls8/a;
    .locals 1

    iget-object v0, p0, Lm8/c;->a:Ls8/a;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lm8/c;->e()Ls8/a;

    move-result-object v0

    iput-object v0, p0, Lm8/c;->a:Ls8/a;

    :cond_0
    return-object v0
.end method

.method protected abstract e()Ls8/a;
.end method

.method public f()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lm8/c;->b:Ljava/lang/Object;

    return-object v0
.end method

.method public i()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lm8/c;->d:Ljava/lang/String;

    return-object v0
.end method

.method public k()Ls8/c;
    .locals 2

    iget-object v0, p0, Lm8/c;->c:Ljava/lang/Class;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    iget-boolean v1, p0, Lm8/c;->f:Z

    if-eqz v1, :cond_1

    invoke-static {v0}, Lm8/u;->b(Ljava/lang/Class;)Ls8/c;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-static {v0}, Lm8/u;->a(Ljava/lang/Class;)Ls8/b;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method protected l()Ls8/a;
    .locals 1

    invoke-virtual {p0}, Lm8/c;->c()Ls8/a;

    move-result-object v0

    if-eq v0, p0, :cond_0

    return-object v0

    :cond_0
    new-instance v0, Lk8/b;

    invoke-direct {v0}, Lk8/b;-><init>()V

    throw v0
.end method

.method public m()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lm8/c;->e:Ljava/lang/String;

    return-object v0
.end method
