.class public Lm8/u;
.super Ljava/lang/Object;


# static fields
.field private static final a:Lm8/v;

.field private static final b:[Ls8/b;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v0, 0x0

    :try_start_0
    const-string v1, "kotlin.reflect.jvm.internal.ReflectionFactoryImpl"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lm8/v;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    :catch_0
    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Lm8/v;

    invoke-direct {v0}, Lm8/v;-><init>()V

    :goto_0
    sput-object v0, Lm8/u;->a:Lm8/v;

    const/4 v0, 0x0

    new-array v0, v0, [Ls8/b;

    sput-object v0, Lm8/u;->b:[Ls8/b;

    return-void
.end method

.method public static a(Ljava/lang/Class;)Ls8/b;
    .locals 1

    sget-object v0, Lm8/u;->a:Lm8/v;

    invoke-virtual {v0, p0}, Lm8/v;->a(Ljava/lang/Class;)Ls8/b;

    move-result-object p0

    return-object p0
.end method

.method public static b(Ljava/lang/Class;)Ls8/c;
    .locals 2

    sget-object v0, Lm8/u;->a:Lm8/v;

    const-string v1, ""

    invoke-virtual {v0, p0, v1}, Lm8/v;->b(Ljava/lang/Class;Ljava/lang/String;)Ls8/c;

    move-result-object p0

    return-object p0
.end method

.method public static c(Lm8/l;)Ls8/d;
    .locals 1

    sget-object v0, Lm8/u;->a:Lm8/v;

    invoke-virtual {v0, p0}, Lm8/v;->c(Lm8/l;)Ls8/d;

    move-result-object p0

    return-object p0
.end method

.method public static d(Lm8/n;)Ls8/e;
    .locals 1

    sget-object v0, Lm8/u;->a:Lm8/v;

    invoke-virtual {v0, p0}, Lm8/v;->d(Lm8/n;)Ls8/e;

    move-result-object p0

    return-object p0
.end method

.method public static e(Lm8/h;)Ljava/lang/String;
    .locals 1

    sget-object v0, Lm8/u;->a:Lm8/v;

    invoke-virtual {v0, p0}, Lm8/v;->e(Lm8/h;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static f(Lm8/j;)Ljava/lang/String;
    .locals 1

    sget-object v0, Lm8/u;->a:Lm8/v;

    invoke-virtual {v0, p0}, Lm8/v;->f(Lm8/j;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method
