.class public Lt9/d;
.super Lja/e;

# interfaces
.implements Lt9/c;


# instance fields
.field private A:Landroid/view/ViewGroup;

.field private x:Lmiuix/appcompat/app/c;

.field private y:Lt9/b;

.field private z:Landroid/view/View;


# direct methods
.method public constructor <init>(Lmiuix/appcompat/app/c;Landroid/view/Menu;)V
    .locals 1

    invoke-virtual {p1}, Lmiuix/appcompat/app/c;->o()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Lja/e;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1}, Lmiuix/appcompat/app/c;->o()Landroid/content/Context;

    move-result-object v0

    iput-object p1, p0, Lt9/d;->x:Lmiuix/appcompat/app/c;

    new-instance p1, Lt9/b;

    invoke-direct {p1, v0, p2}, Lt9/b;-><init>(Landroid/content/Context;Landroid/view/Menu;)V

    iput-object p1, p0, Lt9/d;->y:Lt9/b;

    invoke-virtual {p0, p1}, Lja/e;->i(Landroid/widget/ListAdapter;)V

    new-instance p1, Lt9/d$a;

    invoke-direct {p1, p0}, Lt9/d$a;-><init>(Lt9/d;)V

    invoke-virtual {p0, p1}, Lja/e;->Q(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method

.method static synthetic W(Lt9/d;)Lt9/b;
    .locals 0

    iget-object p0, p0, Lt9/d;->y:Lt9/b;

    return-object p0
.end method

.method static synthetic X(Lt9/d;)Landroid/view/View;
    .locals 0

    iget-object p0, p0, Lt9/d;->z:Landroid/view/View;

    return-object p0
.end method

.method static synthetic Y(Lt9/d;)Landroid/view/ViewGroup;
    .locals 0

    iget-object p0, p0, Lt9/d;->A:Landroid/view/ViewGroup;

    return-object p0
.end method

.method static synthetic Z(Lt9/d;)Lmiuix/appcompat/app/c;
    .locals 0

    iget-object p0, p0, Lt9/d;->x:Lmiuix/appcompat/app/c;

    return-object p0
.end method

.method private a0(Landroid/view/View;Landroid/view/ViewGroup;)V
    .locals 4

    if-nez p2, :cond_0

    const-string p1, "ImmersionMenu"

    const-string p2, "ImmersionMenuPopupWindow offset can\'t be adjusted without parent"

    invoke-static {p1, p2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    const/4 v0, 0x2

    new-array v1, v0, [I

    invoke-virtual {p2, v1}, Landroid/view/ViewGroup;->getLocationInWindow([I)V

    new-array v0, v0, [I

    invoke-virtual {p1, v0}, Landroid/view/View;->getLocationInWindow([I)V

    const/4 v2, 0x1

    aget v3, v0, v2

    aget v2, v1, v2

    sub-int/2addr v3, v2

    invoke-virtual {p0}, Lja/e;->E()I

    move-result v2

    sub-int/2addr v3, v2

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    add-int/2addr v2, v3

    neg-int v2, v2

    invoke-virtual {p0, v2}, Lja/e;->d(I)V

    invoke-static {p2}, Lia/i;->a(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lja/e;->D()I

    move-result p1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    aget v0, v0, v2

    aget v1, v1, v2

    sub-int/2addr v0, v1

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result p1

    add-int/2addr v0, p1

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getWidth()I

    move-result p1

    sub-int/2addr p1, v0

    invoke-virtual {p0}, Lja/e;->D()I

    move-result p2

    sub-int/2addr p1, p2

    :goto_0
    invoke-virtual {p0, p1}, Lja/e;->f(I)V

    return-void
.end method


# virtual methods
.method public a(Z)V
    .locals 0

    invoke-virtual {p0}, Lja/e;->dismiss()V

    return-void
.end method

.method public k(Landroid/view/View;Landroid/view/ViewGroup;)V
    .locals 0

    iput-object p1, p0, Lt9/d;->z:Landroid/view/View;

    iput-object p2, p0, Lt9/d;->A:Landroid/view/ViewGroup;

    invoke-direct {p0, p1, p2}, Lt9/d;->a0(Landroid/view/View;Landroid/view/ViewGroup;)V

    invoke-super {p0, p1, p2}, Lja/e;->k(Landroid/view/View;Landroid/view/ViewGroup;)V

    return-void
.end method

.method public l(Landroid/view/Menu;)V
    .locals 1

    iget-object v0, p0, Lt9/d;->y:Lt9/b;

    invoke-virtual {v0, p1}, Lt9/b;->d(Landroid/view/Menu;)V

    return-void
.end method
