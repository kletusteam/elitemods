.class public Lt9/a;
.super Landroid/widget/FrameLayout;


# instance fields
.field private a:Lz9/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lt9/a;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0, p1}, Lt9/a;->a(Landroid/content/Context;)V

    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 1

    new-instance v0, Lz9/a;

    invoke-direct {v0, p1}, Lz9/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lt9/a;->a:Lz9/a;

    new-instance p1, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v0, -0x1

    invoke-direct {p1, v0, v0}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    iget-object v0, p0, Lt9/a;->a:Lz9/a;

    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object p1, p0, Lt9/a;->a:Lz9/a;

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;I)V

    invoke-virtual {p0, v0}, Lt9/a;->b(Z)Z

    return-void
.end method


# virtual methods
.method public b(Z)Z
    .locals 1

    iget-object v0, p0, Lt9/a;->a:Lz9/a;

    invoke-virtual {v0, p1}, Lz9/a;->c(Z)Z

    move-result p1

    return p1
.end method
