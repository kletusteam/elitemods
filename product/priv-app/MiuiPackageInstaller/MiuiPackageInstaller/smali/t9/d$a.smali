.class Lt9/d$a;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lt9/d;-><init>(Lmiuix/appcompat/app/c;Landroid/view/Menu;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lt9/d;


# direct methods
.method constructor <init>(Lt9/d;)V
    .locals 0

    iput-object p1, p0, Lt9/d$a;->a:Lt9/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView<",
            "*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iget-object p1, p0, Lt9/d$a;->a:Lt9/d;

    invoke-static {p1}, Lt9/d;->W(Lt9/d;)Lt9/b;

    move-result-object p1

    invoke-virtual {p1, p3}, Lt9/b;->c(I)Landroid/view/MenuItem;

    move-result-object p1

    invoke-interface {p1}, Landroid/view/MenuItem;->hasSubMenu()Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-interface {p1}, Landroid/view/MenuItem;->getSubMenu()Landroid/view/SubMenu;

    move-result-object p1

    iget-object p2, p0, Lt9/d$a;->a:Lt9/d;

    new-instance p3, Lt9/d$a$a;

    invoke-direct {p3, p0, p1}, Lt9/d$a$a;-><init>(Lt9/d$a;Landroid/view/SubMenu;)V

    invoke-virtual {p2, p3}, Lja/e;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    goto :goto_0

    :cond_0
    iget-object p2, p0, Lt9/d$a;->a:Lt9/d;

    invoke-static {p2}, Lt9/d;->Z(Lt9/d;)Lmiuix/appcompat/app/c;

    move-result-object p2

    const/4 p3, 0x0

    invoke-virtual {p2, p3, p1}, Lmiuix/appcompat/app/c;->u(ILandroid/view/MenuItem;)Z

    :goto_0
    iget-object p1, p0, Lt9/d$a;->a:Lt9/d;

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Lt9/d;->a(Z)V

    return-void
.end method
