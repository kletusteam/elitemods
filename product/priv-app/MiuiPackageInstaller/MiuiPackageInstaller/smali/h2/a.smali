.class public final enum Lh2/a;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lh2/a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum A:Lh2/a;

.field public static final enum B:Lh2/a;

.field public static final enum C:Lh2/a;

.field public static final enum D:Lh2/a;

.field public static final enum E:Lh2/a;

.field public static final enum F:Lh2/a;

.field public static final enum G:Lh2/a;

.field public static final enum H:Lh2/a;

.field private static final synthetic I:[Lh2/a;

.field public static final enum a:Lh2/a;

.field public static final enum b:Lh2/a;

.field public static final enum c:Lh2/a;

.field public static final enum d:Lh2/a;

.field public static final enum e:Lh2/a;

.field public static final enum f:Lh2/a;

.field public static final enum g:Lh2/a;

.field public static final enum h:Lh2/a;

.field public static final enum i:Lh2/a;

.field public static final enum j:Lh2/a;

.field public static final enum k:Lh2/a;

.field public static final enum l:Lh2/a;

.field public static final enum m:Lh2/a;

.field public static final enum n:Lh2/a;

.field public static final enum o:Lh2/a;

.field public static final enum p:Lh2/a;

.field public static final enum q:Lh2/a;

.field public static final enum r:Lh2/a;

.field public static final enum s:Lh2/a;

.field public static final enum t:Lh2/a;

.field public static final enum u:Lh2/a;

.field public static final enum v:Lh2/a;

.field public static final enum w:Lh2/a;

.field public static final enum x:Lh2/a;

.field public static final enum y:Lh2/a;

.field public static final enum z:Lh2/a;


# direct methods
.method static constructor <clinit>()V
    .locals 36

    new-instance v0, Lh2/a;

    const-string v1, "IMEI"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lh2/a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lh2/a;->a:Lh2/a;

    new-instance v1, Lh2/a;

    const-string v3, "IMSI"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lh2/a;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lh2/a;->b:Lh2/a;

    new-instance v3, Lh2/a;

    const-string v5, "BRAND"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lh2/a;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lh2/a;->c:Lh2/a;

    new-instance v5, Lh2/a;

    const-string v7, "DEVICE_MODEL"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8}, Lh2/a;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lh2/a;->d:Lh2/a;

    new-instance v7, Lh2/a;

    const-string v9, "RESOLUTION"

    const/4 v10, 0x4

    invoke-direct {v7, v9, v10}, Lh2/a;-><init>(Ljava/lang/String;I)V

    sput-object v7, Lh2/a;->e:Lh2/a;

    new-instance v9, Lh2/a;

    const-string v11, "CARRIER"

    const/4 v12, 0x5

    invoke-direct {v9, v11, v12}, Lh2/a;-><init>(Ljava/lang/String;I)V

    sput-object v9, Lh2/a;->f:Lh2/a;

    new-instance v11, Lh2/a;

    const-string v13, "ACCESS"

    const/4 v14, 0x6

    invoke-direct {v11, v13, v14}, Lh2/a;-><init>(Ljava/lang/String;I)V

    sput-object v11, Lh2/a;->g:Lh2/a;

    new-instance v13, Lh2/a;

    const-string v15, "ACCESS_SUBTYPE"

    const/4 v14, 0x7

    invoke-direct {v13, v15, v14}, Lh2/a;-><init>(Ljava/lang/String;I)V

    sput-object v13, Lh2/a;->h:Lh2/a;

    new-instance v15, Lh2/a;

    const-string v14, "CHANNEL"

    const/16 v12, 0x8

    invoke-direct {v15, v14, v12}, Lh2/a;-><init>(Ljava/lang/String;I)V

    sput-object v15, Lh2/a;->i:Lh2/a;

    new-instance v14, Lh2/a;

    const-string v12, "APPKEY"

    const/16 v10, 0x9

    invoke-direct {v14, v12, v10}, Lh2/a;-><init>(Ljava/lang/String;I)V

    sput-object v14, Lh2/a;->j:Lh2/a;

    new-instance v12, Lh2/a;

    const-string v10, "APPVERSION"

    const/16 v8, 0xa

    invoke-direct {v12, v10, v8}, Lh2/a;-><init>(Ljava/lang/String;I)V

    sput-object v12, Lh2/a;->k:Lh2/a;

    new-instance v10, Lh2/a;

    const-string v8, "LL_USERNICK"

    const/16 v6, 0xb

    invoke-direct {v10, v8, v6}, Lh2/a;-><init>(Ljava/lang/String;I)V

    sput-object v10, Lh2/a;->l:Lh2/a;

    new-instance v8, Lh2/a;

    const-string v6, "USERNICK"

    const/16 v4, 0xc

    invoke-direct {v8, v6, v4}, Lh2/a;-><init>(Ljava/lang/String;I)V

    sput-object v8, Lh2/a;->m:Lh2/a;

    new-instance v6, Lh2/a;

    const-string v4, "LL_USERID"

    const/16 v2, 0xd

    invoke-direct {v6, v4, v2}, Lh2/a;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lh2/a;->n:Lh2/a;

    new-instance v4, Lh2/a;

    const-string v2, "USERID"

    move-object/from16 v16, v6

    const/16 v6, 0xe

    invoke-direct {v4, v2, v6}, Lh2/a;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lh2/a;->o:Lh2/a;

    new-instance v2, Lh2/a;

    const-string v6, "LANGUAGE"

    move-object/from16 v17, v4

    const/16 v4, 0xf

    invoke-direct {v2, v6, v4}, Lh2/a;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lh2/a;->p:Lh2/a;

    new-instance v6, Lh2/a;

    const-string v4, "OS"

    move-object/from16 v18, v2

    const/16 v2, 0x10

    invoke-direct {v6, v4, v2}, Lh2/a;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lh2/a;->q:Lh2/a;

    new-instance v4, Lh2/a;

    const-string v2, "OSVERSION"

    move-object/from16 v19, v6

    const/16 v6, 0x11

    invoke-direct {v4, v2, v6}, Lh2/a;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lh2/a;->r:Lh2/a;

    new-instance v2, Lh2/a;

    const-string v6, "SDKVERSION"

    move-object/from16 v20, v4

    const/16 v4, 0x12

    invoke-direct {v2, v6, v4}, Lh2/a;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lh2/a;->s:Lh2/a;

    new-instance v6, Lh2/a;

    const-string v4, "START_SESSION_TIMESTAMP"

    move-object/from16 v21, v2

    const/16 v2, 0x13

    invoke-direct {v6, v4, v2}, Lh2/a;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lh2/a;->t:Lh2/a;

    new-instance v4, Lh2/a;

    const-string v2, "UTDID"

    move-object/from16 v22, v6

    const/16 v6, 0x14

    invoke-direct {v4, v2, v6}, Lh2/a;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lh2/a;->u:Lh2/a;

    new-instance v2, Lh2/a;

    const-string v6, "SDKTYPE"

    move-object/from16 v23, v4

    const/16 v4, 0x15

    invoke-direct {v2, v6, v4}, Lh2/a;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lh2/a;->v:Lh2/a;

    new-instance v6, Lh2/a;

    const-string v4, "RESERVE2"

    move-object/from16 v24, v2

    const/16 v2, 0x16

    invoke-direct {v6, v4, v2}, Lh2/a;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lh2/a;->w:Lh2/a;

    new-instance v2, Lh2/a;

    const-string v4, "RESERVE3"

    move-object/from16 v25, v6

    const/16 v6, 0x17

    invoke-direct {v2, v4, v6}, Lh2/a;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lh2/a;->x:Lh2/a;

    new-instance v4, Lh2/a;

    const-string v6, "RESERVE4"

    move-object/from16 v26, v2

    const/16 v2, 0x18

    invoke-direct {v4, v6, v2}, Lh2/a;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lh2/a;->y:Lh2/a;

    new-instance v2, Lh2/a;

    const-string v6, "RESERVE5"

    move-object/from16 v27, v4

    const/16 v4, 0x19

    invoke-direct {v2, v6, v4}, Lh2/a;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lh2/a;->z:Lh2/a;

    new-instance v4, Lh2/a;

    const-string v6, "RESERVES"

    move-object/from16 v28, v2

    const/16 v2, 0x1a

    invoke-direct {v4, v6, v2}, Lh2/a;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lh2/a;->A:Lh2/a;

    new-instance v2, Lh2/a;

    const-string v6, "RECORD_TIMESTAMP"

    move-object/from16 v29, v4

    const/16 v4, 0x1b

    invoke-direct {v2, v6, v4}, Lh2/a;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lh2/a;->B:Lh2/a;

    new-instance v4, Lh2/a;

    const-string v6, "PAGE"

    move-object/from16 v30, v2

    const/16 v2, 0x1c

    invoke-direct {v4, v6, v2}, Lh2/a;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lh2/a;->C:Lh2/a;

    new-instance v2, Lh2/a;

    const-string v6, "EVENTID"

    move-object/from16 v31, v4

    const/16 v4, 0x1d

    invoke-direct {v2, v6, v4}, Lh2/a;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lh2/a;->D:Lh2/a;

    new-instance v4, Lh2/a;

    const-string v6, "ARG1"

    move-object/from16 v32, v2

    const/16 v2, 0x1e

    invoke-direct {v4, v6, v2}, Lh2/a;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lh2/a;->E:Lh2/a;

    new-instance v2, Lh2/a;

    const-string v6, "ARG2"

    move-object/from16 v33, v4

    const/16 v4, 0x1f

    invoke-direct {v2, v6, v4}, Lh2/a;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lh2/a;->F:Lh2/a;

    new-instance v4, Lh2/a;

    const-string v6, "ARG3"

    move-object/from16 v34, v2

    const/16 v2, 0x20

    invoke-direct {v4, v6, v2}, Lh2/a;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lh2/a;->G:Lh2/a;

    new-instance v2, Lh2/a;

    const-string v6, "ARGS"

    move-object/from16 v35, v4

    const/16 v4, 0x21

    invoke-direct {v2, v6, v4}, Lh2/a;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lh2/a;->H:Lh2/a;

    const/16 v4, 0x22

    new-array v4, v4, [Lh2/a;

    const/4 v6, 0x0

    aput-object v0, v4, v6

    const/4 v0, 0x1

    aput-object v1, v4, v0

    const/4 v0, 0x2

    aput-object v3, v4, v0

    const/4 v0, 0x3

    aput-object v5, v4, v0

    const/4 v0, 0x4

    aput-object v7, v4, v0

    const/4 v0, 0x5

    aput-object v9, v4, v0

    const/4 v0, 0x6

    aput-object v11, v4, v0

    const/4 v0, 0x7

    aput-object v13, v4, v0

    const/16 v0, 0x8

    aput-object v15, v4, v0

    const/16 v0, 0x9

    aput-object v14, v4, v0

    const/16 v0, 0xa

    aput-object v12, v4, v0

    const/16 v0, 0xb

    aput-object v10, v4, v0

    const/16 v0, 0xc

    aput-object v8, v4, v0

    const/16 v0, 0xd

    aput-object v16, v4, v0

    const/16 v0, 0xe

    aput-object v17, v4, v0

    const/16 v0, 0xf

    aput-object v18, v4, v0

    const/16 v0, 0x10

    aput-object v19, v4, v0

    const/16 v0, 0x11

    aput-object v20, v4, v0

    const/16 v0, 0x12

    aput-object v21, v4, v0

    const/16 v0, 0x13

    aput-object v22, v4, v0

    const/16 v0, 0x14

    aput-object v23, v4, v0

    const/16 v0, 0x15

    aput-object v24, v4, v0

    const/16 v0, 0x16

    aput-object v25, v4, v0

    const/16 v0, 0x17

    aput-object v26, v4, v0

    const/16 v0, 0x18

    aput-object v27, v4, v0

    const/16 v0, 0x19

    aput-object v28, v4, v0

    const/16 v0, 0x1a

    aput-object v29, v4, v0

    const/16 v0, 0x1b

    aput-object v30, v4, v0

    const/16 v0, 0x1c

    aput-object v31, v4, v0

    const/16 v0, 0x1d

    aput-object v32, v4, v0

    const/16 v0, 0x1e

    aput-object v33, v4, v0

    const/16 v0, 0x1f

    aput-object v34, v4, v0

    const/16 v0, 0x20

    aput-object v35, v4, v0

    const/16 v0, 0x21

    aput-object v2, v4, v0

    sput-object v4, Lh2/a;->I:[Lh2/a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lh2/a;
    .locals 1

    const-class v0, Lh2/a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lh2/a;

    return-object p0
.end method

.method public static values()[Lh2/a;
    .locals 1

    sget-object v0, Lh2/a;->I:[Lh2/a;

    invoke-virtual {v0}, [Lh2/a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lh2/a;

    return-object v0
.end method
