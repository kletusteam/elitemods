.class public Lh2/c;
.super Ljava/lang/Object;


# direct methods
.method public static a(Le2/a;Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;JLjava/lang/String;ILjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Map;)Z
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Le2/a;",
            "Ljava/lang/String;",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "J",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    move-object v11, p0

    :try_start_0
    const-string v0, "RestAPI start send log!"

    invoke-static {v0}, Li2/e;->d(Ljava/lang/String;)V

    move-object v1, p0

    move-object v2, p1

    move-wide/from16 v3, p4

    move-object/from16 v5, p6

    move/from16 v6, p7

    move-object/from16 v7, p8

    move-object/from16 v8, p9

    move-object/from16 v9, p10

    move-object/from16 v10, p11

    invoke-static/range {v1 .. v10}, Lh2/b;->a(Le2/a;Ljava/lang/String;JLjava/lang/String;ILjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Li2/h;->e(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "RestAPI build data succ!"

    invoke-static {v1}, Li2/e;->d(Ljava/lang/String;)V

    new-instance v1, Ljava/util/HashMap;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Ljava/util/HashMap;-><init>(I)V

    invoke-static/range {p7 .. p7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v2, 0x0

    move-object v0, p1

    move-object v3, p2

    :try_start_1
    invoke-static {p0, p1, p2, v1}, Lg2/a;->b(Le2/a;Ljava/lang/String;Landroid/content/Context;Ljava/util/Map;)[B

    move-result-object v2
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v1, v0

    :try_start_2
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Li2/e;->b(Ljava/lang/String;)V

    :goto_0
    if-eqz v2, :cond_1

    const-string v0, "packRequest success!"

    invoke-static {v0}, Li2/e;->d(Ljava/lang/String;)V

    move-object v1, p3

    invoke-static {p0, p3, v2}, Lg2/c;->b(Le2/a;Ljava/lang/String;[B)Lg2/b;

    move-result-object v0

    invoke-virtual {v0}, Lg2/b;->a()Z

    move-result v0

    return v0

    :cond_0
    const-string v0, "UTRestAPI build data failure!"

    invoke-static {v0}, Li2/e;->d(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    const-string v1, "system error!"

    invoke-static {v1, v0}, Li2/e;->c(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_1
    :goto_1
    const/4 v0, 0x0

    return v0
.end method
