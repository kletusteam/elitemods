.class public Lc2/a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lc2/a$d;,
        Lc2/a$e;,
        Lc2/a$b;,
        Lc2/a$c;
    }
.end annotation


# static fields
.field private static final h:Lc2/d;

.field private static final i:Lc2/c;


# instance fields
.field private a:Z

.field private b:Lc2/d;

.field private c:Lc2/c;

.field private d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lc2/c;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lc2/a$c;

.field private f:Ljava/lang/String;

.field private g:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    sget-object v0, Lc2/d;->c:Lc2/d;

    sput-object v0, Lc2/a;->h:Lc2/d;

    new-instance v0, Lc2/a$d;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lc2/a$d;-><init>(Lc2/a$a;)V

    sput-object v0, Lc2/a;->i:Lc2/c;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lc2/a;->a:Z

    sget-object v0, Lc2/a;->h:Lc2/d;

    iput-object v0, p0, Lc2/a;->b:Lc2/d;

    sget-object v0, Lc2/a;->i:Lc2/c;

    iput-object v0, p0, Lc2/a;->c:Lc2/c;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lc2/a;->d:Ljava/util/ArrayList;

    new-instance v0, Lc2/a$c;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lc2/a$c;-><init>(Lc2/a;Lc2/a$a;)V

    iput-object v0, p0, Lc2/a;->e:Lc2/a$c;

    iput-object p1, p0, Lc2/a;->f:Ljava/lang/String;

    if-nez p1, :cond_0

    const-string p1, "default"

    iput-object p1, p0, Lc2/a;->f:Ljava/lang/String;

    :cond_0
    iput-boolean p2, p0, Lc2/a;->g:Z

    if-eqz p2, :cond_1

    sget-object p1, Lc2/d;->a:Lc2/d;

    iput-object p1, p0, Lc2/a;->b:Lc2/d;

    :cond_1
    return-void
.end method

.method static synthetic a(Lc2/a;)Lc2/c;
    .locals 0

    iget-object p0, p0, Lc2/a;->c:Lc2/c;

    return-object p0
.end method

.method private b(Ljava/lang/Object;)Ljava/lang/String;
    .locals 2

    if-nez p1, :cond_0

    const-string p1, ""

    goto :goto_0

    :cond_0
    instance-of v0, p1, Ljava/lang/Class;

    if-eqz v0, :cond_1

    check-cast p1, Ljava/lang/Class;

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_1
    instance-of v0, p1, Ljava/lang/String;

    if-eqz v0, :cond_2

    check-cast p1, Ljava/lang/String;

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lc2/a;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method static synthetic c(Lc2/a;Lc2/d;)Z
    .locals 0

    invoke-direct {p0, p1}, Lc2/a;->d(Lc2/d;)Z

    move-result p0

    return p0
.end method

.method private d(Lc2/d;)Z
    .locals 1

    iget-boolean v0, p0, Lc2/a;->a:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    iget-object v0, p0, Lc2/a;->b:Lc2/d;

    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    if-lt p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method static synthetic e(Lc2/a;)Ljava/util/ArrayList;
    .locals 0

    iget-object p0, p0, Lc2/a;->d:Ljava/util/ArrayList;

    return-object p0
.end method


# virtual methods
.method public f(Ljava/lang/Object;)Lc2/b;
    .locals 5

    new-instance v0, Lc2/a$e;

    invoke-direct {p0, p1}, Lc2/a;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    new-instance v1, Lc2/a$b;

    iget-object v2, p0, Lc2/a;->e:Lc2/a$c;

    iget-boolean v3, p0, Lc2/a;->g:Z

    const/4 v4, 0x0

    invoke-direct {v1, v2, v3, v4}, Lc2/a$b;-><init>(Lc2/c;ZLc2/a$a;)V

    invoke-direct {v0, p1, v1}, Lc2/a$e;-><init>(Ljava/lang/String;Lc2/c;)V

    return-object v0
.end method
