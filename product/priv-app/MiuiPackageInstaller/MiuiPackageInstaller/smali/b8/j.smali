.class public final Lb8/j;
.super Lb8/t;


# direct methods
.method public static bridge synthetic A(Ljava/lang/Iterable;)Ljava/lang/Object;
    .locals 0

    invoke-static {p0}, Lb8/t;->A(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method public static bridge synthetic D(Ljava/lang/Iterable;)Ljava/util/List;
    .locals 0

    invoke-static {p0}, Lb8/t;->D(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static bridge synthetic F(Ljava/util/Collection;)Ljava/util/List;
    .locals 0

    invoke-static {p0}, Lb8/t;->F(Ljava/util/Collection;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static bridge synthetic G(Ljava/lang/Iterable;)Ljava/util/Set;
    .locals 0

    invoke-static {p0}, Lb8/t;->G(Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object p0

    return-object p0
.end method

.method public static bridge synthetic H(Ljava/lang/Iterable;)Ljava/lang/Iterable;
    .locals 0

    invoke-static {p0}, Lb8/t;->H(Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object p0

    return-object p0
.end method

.method public static bridge synthetic b(Ljava/lang/Object;)Ljava/util/List;
    .locals 0

    invoke-static {p0}, Lb8/k;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static bridge synthetic e(Ljava/util/List;Ljava/lang/Comparable;IIILjava/lang/Object;)I
    .locals 0

    invoke-static/range {p0 .. p5}, Lb8/l;->e(Ljava/util/List;Ljava/lang/Comparable;IIILjava/lang/Object;)I

    move-result p0

    return p0
.end method

.method public static bridge synthetic f()Ljava/util/List;
    .locals 1

    invoke-static {}, Lb8/l;->f()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static bridge synthetic g(Ljava/util/List;)I
    .locals 0

    invoke-static {p0}, Lb8/l;->g(Ljava/util/List;)I

    move-result p0

    return p0
.end method

.method public static bridge varargs synthetic h([Ljava/lang/Object;)Ljava/util/List;
    .locals 0

    invoke-static {p0}, Lb8/l;->h([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static bridge varargs synthetic i([Ljava/lang/Object;)Ljava/util/List;
    .locals 0

    invoke-static {p0}, Lb8/l;->i([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static bridge varargs synthetic j([Ljava/lang/Object;)Ljava/util/List;
    .locals 0

    invoke-static {p0}, Lb8/l;->j([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static bridge synthetic k(Ljava/util/List;)Ljava/util/List;
    .locals 0

    invoke-static {p0}, Lb8/l;->k(Ljava/util/List;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static bridge synthetic m()V
    .locals 0

    invoke-static {}, Lb8/l;->m()V

    return-void
.end method

.method public static bridge synthetic n(Ljava/lang/Iterable;I)I
    .locals 0

    invoke-static {p0, p1}, Lb8/m;->n(Ljava/lang/Iterable;I)I

    move-result p0

    return p0
.end method

.method public static bridge synthetic o(Ljava/util/Enumeration;)Ljava/util/Iterator;
    .locals 0

    invoke-static {p0}, Lb8/n;->o(Ljava/util/Enumeration;)Ljava/util/Iterator;

    move-result-object p0

    return-object p0
.end method

.method public static bridge synthetic p(Ljava/util/List;)V
    .locals 0

    invoke-static {p0}, Lb8/p;->p(Ljava/util/List;)V

    return-void
.end method

.method public static bridge synthetic q(Ljava/util/List;Ljava/util/Comparator;)V
    .locals 0

    invoke-static {p0, p1}, Lb8/p;->q(Ljava/util/List;Ljava/util/Comparator;)V

    return-void
.end method

.method public static bridge synthetic r(Ljava/util/Collection;Ljava/lang/Iterable;)Z
    .locals 0

    invoke-static {p0, p1}, Lb8/q;->r(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    move-result p0

    return p0
.end method

.method public static bridge synthetic s(Ljava/util/Collection;[Ljava/lang/Object;)Z
    .locals 0

    invoke-static {p0, p1}, Lb8/q;->s(Ljava/util/Collection;[Ljava/lang/Object;)Z

    move-result p0

    return p0
.end method

.method public static bridge synthetic t(Ljava/lang/Iterable;)Lt8/f;
    .locals 0

    invoke-static {p0}, Lb8/t;->t(Ljava/lang/Iterable;)Lt8/f;

    move-result-object p0

    return-object p0
.end method

.method public static bridge synthetic v(Ljava/lang/Iterable;Ljava/lang/Appendable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Ll8/l;ILjava/lang/Object;)Ljava/lang/Appendable;
    .locals 0

    invoke-static/range {p0 .. p9}, Lb8/t;->v(Ljava/lang/Iterable;Ljava/lang/Appendable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Ll8/l;ILjava/lang/Object;)Ljava/lang/Appendable;

    move-result-object p0

    return-object p0
.end method

.method public static bridge synthetic y(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;
    .locals 0

    invoke-static {p0, p1}, Lb8/t;->y(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static bridge synthetic z(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/List;
    .locals 0

    invoke-static {p0, p1}, Lb8/t;->z(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method
