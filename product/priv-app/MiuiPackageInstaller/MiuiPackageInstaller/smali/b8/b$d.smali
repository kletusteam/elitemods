.class final Lb8/b$d;
.super Lb8/b;

# interfaces
.implements Ljava/util/RandomAccess;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lb8/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "d"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Lb8/b<",
        "TE;>;",
        "Ljava/util/RandomAccess;"
    }
.end annotation


# instance fields
.field private final b:Lb8/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb8/b<",
            "TE;>;"
        }
    .end annotation
.end field

.field private final c:I

.field private d:I


# direct methods
.method public constructor <init>(Lb8/b;II)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lb8/b<",
            "+TE;>;II)V"
        }
    .end annotation

    const-string v0, "list"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lb8/b;-><init>()V

    iput-object p1, p0, Lb8/b$d;->b:Lb8/b;

    iput p2, p0, Lb8/b$d;->c:I

    sget-object v0, Lb8/b;->a:Lb8/b$a;

    invoke-virtual {p1}, Lb8/a;->size()I

    move-result p1

    invoke-virtual {v0, p2, p3, p1}, Lb8/b$a;->c(III)V

    sub-int/2addr p3, p2

    iput p3, p0, Lb8/b$d;->d:I

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lb8/b$d;->d:I

    return v0
.end method

.method public get(I)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TE;"
        }
    .end annotation

    sget-object v0, Lb8/b;->a:Lb8/b$a;

    iget v1, p0, Lb8/b$d;->d:I

    invoke-virtual {v0, p1, v1}, Lb8/b$a;->a(II)V

    iget-object v0, p0, Lb8/b$d;->b:Lb8/b;

    iget v1, p0, Lb8/b$d;->c:I

    add-int/2addr v1, p1

    invoke-virtual {v0, v1}, Lb8/b;->get(I)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method
