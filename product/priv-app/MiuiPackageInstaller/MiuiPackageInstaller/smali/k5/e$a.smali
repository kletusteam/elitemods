.class public final enum Lk5/e$a;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lk5/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lk5/e$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum c:Lk5/e$a;

.field private static final synthetic d:[Lk5/e$a;


# instance fields
.field public final a:Ljava/lang/String;

.field public b:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Lk5/e$a;

    const-string v1, "DEFAULT"

    const/4 v2, 0x0

    const-string v3, "com.xiaomi.market.sdk_pref"

    invoke-direct {v0, v1, v2, v3, v2}, Lk5/e$a;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    sput-object v0, Lk5/e$a;->c:Lk5/e$a;

    const/4 v1, 0x1

    new-array v1, v1, [Lk5/e$a;

    aput-object v0, v1, v2

    sput-object v1, Lk5/e$a;->d:[Lk5/e$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lk5/e$a;->a:Ljava/lang/String;

    iput-boolean p4, p0, Lk5/e$a;->b:Z

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lk5/e$a;
    .locals 1

    const-class v0, Lk5/e$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lk5/e$a;

    return-object p0
.end method

.method public static values()[Lk5/e$a;
    .locals 1

    sget-object v0, Lk5/e$a;->d:[Lk5/e$a;

    invoke-virtual {v0}, [Lk5/e$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lk5/e$a;

    return-object v0
.end method
