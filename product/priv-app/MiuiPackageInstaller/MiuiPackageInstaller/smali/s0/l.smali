.class public abstract Ls0/l;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ls0/l$e;,
        Ls0/l$d;,
        Ls0/l$f;
    }
.end annotation


# static fields
.field private static final G:[I

.field private static final H:Ls0/g;

.field private static I:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal<",
            "Ln/a<",
            "Landroid/animation/Animator;",
            "Ls0/l$d;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field private A:Z

.field private B:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ls0/l$f;",
            ">;"
        }
    .end annotation
.end field

.field private C:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Landroid/animation/Animator;",
            ">;"
        }
    .end annotation
.end field

.field private D:Ls0/l$e;

.field private E:Ln/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ln/a<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private F:Ls0/g;

.field private a:Ljava/lang/String;

.field private b:J

.field c:J

.field private d:Landroid/animation/TimeInterpolator;

.field e:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/Class<",
            "*>;>;"
        }
    .end annotation
.end field

.field private i:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/Class<",
            "*>;>;"
        }
    .end annotation
.end field

.field private l:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private m:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private n:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private o:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/Class<",
            "*>;>;"
        }
    .end annotation
.end field

.field private p:Ls0/s;

.field private q:Ls0/s;

.field r:Ls0/p;

.field private s:[I

.field private t:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ls0/r;",
            ">;"
        }
    .end annotation
.end field

.field private u:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ls0/r;",
            ">;"
        }
    .end annotation
.end field

.field private v:Landroid/view/ViewGroup;

.field w:Z

.field x:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Landroid/animation/Animator;",
            ">;"
        }
    .end annotation
.end field

.field private y:I

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Ls0/l;->G:[I

    new-instance v0, Ls0/l$a;

    invoke-direct {v0}, Ls0/l$a;-><init>()V

    sput-object v0, Ls0/l;->H:Ls0/g;

    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    sput-object v0, Ls0/l;->I:Ljava/lang/ThreadLocal;

    return-void

    nop

    :array_0
    .array-data 4
        0x2
        0x1
        0x3
        0x4
    .end array-data
.end method

.method public constructor <init>()V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ls0/l;->a:Ljava/lang/String;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Ls0/l;->b:J

    iput-wide v0, p0, Ls0/l;->c:J

    const/4 v0, 0x0

    iput-object v0, p0, Ls0/l;->d:Landroid/animation/TimeInterpolator;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ls0/l;->e:Ljava/util/ArrayList;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ls0/l;->f:Ljava/util/ArrayList;

    iput-object v0, p0, Ls0/l;->g:Ljava/util/ArrayList;

    iput-object v0, p0, Ls0/l;->h:Ljava/util/ArrayList;

    iput-object v0, p0, Ls0/l;->i:Ljava/util/ArrayList;

    iput-object v0, p0, Ls0/l;->j:Ljava/util/ArrayList;

    iput-object v0, p0, Ls0/l;->k:Ljava/util/ArrayList;

    iput-object v0, p0, Ls0/l;->l:Ljava/util/ArrayList;

    iput-object v0, p0, Ls0/l;->m:Ljava/util/ArrayList;

    iput-object v0, p0, Ls0/l;->n:Ljava/util/ArrayList;

    iput-object v0, p0, Ls0/l;->o:Ljava/util/ArrayList;

    new-instance v1, Ls0/s;

    invoke-direct {v1}, Ls0/s;-><init>()V

    iput-object v1, p0, Ls0/l;->p:Ls0/s;

    new-instance v1, Ls0/s;

    invoke-direct {v1}, Ls0/s;-><init>()V

    iput-object v1, p0, Ls0/l;->q:Ls0/s;

    iput-object v0, p0, Ls0/l;->r:Ls0/p;

    sget-object v1, Ls0/l;->G:[I

    iput-object v1, p0, Ls0/l;->s:[I

    iput-object v0, p0, Ls0/l;->v:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    iput-boolean v1, p0, Ls0/l;->w:Z

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Ls0/l;->x:Ljava/util/ArrayList;

    iput v1, p0, Ls0/l;->y:I

    iput-boolean v1, p0, Ls0/l;->z:Z

    iput-boolean v1, p0, Ls0/l;->A:Z

    iput-object v0, p0, Ls0/l;->B:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ls0/l;->C:Ljava/util/ArrayList;

    sget-object v0, Ls0/l;->H:Ls0/g;

    iput-object v0, p0, Ls0/l;->F:Ls0/g;

    return-void
.end method

.method private static H(Ls0/r;Ls0/r;Ljava/lang/String;)Z
    .locals 0

    iget-object p0, p0, Ls0/r;->a:Ljava/util/Map;

    invoke-interface {p0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    iget-object p1, p1, Ls0/r;->a:Ljava/util/Map;

    invoke-interface {p1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    const/4 p2, 0x1

    if-nez p0, :cond_0

    if-nez p1, :cond_0

    const/4 p2, 0x0

    goto :goto_0

    :cond_0
    if-eqz p0, :cond_2

    if-nez p1, :cond_1

    goto :goto_0

    :cond_1
    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p0

    xor-int/2addr p2, p0

    :cond_2
    :goto_0
    return p2
.end method

.method private I(Ln/a;Ln/a;Landroid/util/SparseArray;Landroid/util/SparseArray;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ln/a<",
            "Landroid/view/View;",
            "Ls0/r;",
            ">;",
            "Ln/a<",
            "Landroid/view/View;",
            "Ls0/r;",
            ">;",
            "Landroid/util/SparseArray<",
            "Landroid/view/View;",
            ">;",
            "Landroid/util/SparseArray<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p3}, Landroid/util/SparseArray;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    invoke-virtual {p3, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    if-eqz v2, :cond_0

    invoke-virtual {p0, v2}, Ls0/l;->G(Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p3, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v3

    invoke-virtual {p4, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    if-eqz v3, :cond_0

    invoke-virtual {p0, v3}, Ls0/l;->G(Landroid/view/View;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p1, v2}, Ln/g;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ls0/r;

    invoke-virtual {p2, v3}, Ln/g;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ls0/r;

    if-eqz v4, :cond_0

    if-eqz v5, :cond_0

    iget-object v6, p0, Ls0/l;->t:Ljava/util/ArrayList;

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v4, p0, Ls0/l;->u:Ljava/util/ArrayList;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p1, v2}, Ln/g;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p2, v3}, Ln/g;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private N(Ln/a;Ln/a;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ln/a<",
            "Landroid/view/View;",
            "Ls0/r;",
            ">;",
            "Ln/a<",
            "Landroid/view/View;",
            "Ls0/r;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p1}, Ln/g;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    invoke-virtual {p1, v0}, Ln/g;->i(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    if-eqz v1, :cond_0

    invoke-virtual {p0, v1}, Ls0/l;->G(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p2, v1}, Ln/g;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ls0/r;

    if-eqz v1, :cond_0

    iget-object v2, v1, Ls0/r;->b:Landroid/view/View;

    invoke-virtual {p0, v2}, Ls0/l;->G(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p1, v0}, Ln/g;->k(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ls0/r;

    iget-object v3, p0, Ls0/l;->t:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Ls0/l;->u:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private O(Ln/a;Ln/a;Ln/d;Ln/d;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ln/a<",
            "Landroid/view/View;",
            "Ls0/r;",
            ">;",
            "Ln/a<",
            "Landroid/view/View;",
            "Ls0/r;",
            ">;",
            "Ln/d<",
            "Landroid/view/View;",
            ">;",
            "Ln/d<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p3}, Ln/d;->m()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    invoke-virtual {p3, v1}, Ln/d;->n(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    if-eqz v2, :cond_0

    invoke-virtual {p0, v2}, Ls0/l;->G(Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p3, v1}, Ln/d;->i(I)J

    move-result-wide v3

    invoke-virtual {p4, v3, v4}, Ln/d;->f(J)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    if-eqz v3, :cond_0

    invoke-virtual {p0, v3}, Ls0/l;->G(Landroid/view/View;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p1, v2}, Ln/g;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ls0/r;

    invoke-virtual {p2, v3}, Ln/g;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ls0/r;

    if-eqz v4, :cond_0

    if-eqz v5, :cond_0

    iget-object v6, p0, Ls0/l;->t:Ljava/util/ArrayList;

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v4, p0, Ls0/l;->u:Ljava/util/ArrayList;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p1, v2}, Ln/g;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p2, v3}, Ln/g;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private P(Ln/a;Ln/a;Ln/a;Ln/a;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ln/a<",
            "Landroid/view/View;",
            "Ls0/r;",
            ">;",
            "Ln/a<",
            "Landroid/view/View;",
            "Ls0/r;",
            ">;",
            "Ln/a<",
            "Ljava/lang/String;",
            "Landroid/view/View;",
            ">;",
            "Ln/a<",
            "Ljava/lang/String;",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p3}, Ln/g;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    invoke-virtual {p3, v1}, Ln/g;->m(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    if-eqz v2, :cond_0

    invoke-virtual {p0, v2}, Ls0/l;->G(Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p3, v1}, Ln/g;->i(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {p4, v3}, Ln/g;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    if-eqz v3, :cond_0

    invoke-virtual {p0, v3}, Ls0/l;->G(Landroid/view/View;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p1, v2}, Ln/g;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ls0/r;

    invoke-virtual {p2, v3}, Ln/g;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ls0/r;

    if-eqz v4, :cond_0

    if-eqz v5, :cond_0

    iget-object v6, p0, Ls0/l;->t:Ljava/util/ArrayList;

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v4, p0, Ls0/l;->u:Ljava/util/ArrayList;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p1, v2}, Ln/g;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p2, v3}, Ln/g;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private Q(Ls0/s;Ls0/s;)V
    .locals 5

    new-instance v0, Ln/a;

    iget-object v1, p1, Ls0/s;->a:Ln/a;

    invoke-direct {v0, v1}, Ln/a;-><init>(Ln/g;)V

    new-instance v1, Ln/a;

    iget-object v2, p2, Ls0/s;->a:Ln/a;

    invoke-direct {v1, v2}, Ln/a;-><init>(Ln/g;)V

    const/4 v2, 0x0

    :goto_0
    iget-object v3, p0, Ls0/l;->s:[I

    array-length v4, v3

    if-ge v2, v4, :cond_4

    aget v3, v3, v2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x2

    if-eq v3, v4, :cond_2

    const/4 v4, 0x3

    if-eq v3, v4, :cond_1

    const/4 v4, 0x4

    if-eq v3, v4, :cond_0

    goto :goto_1

    :cond_0
    iget-object v3, p1, Ls0/s;->c:Ln/d;

    iget-object v4, p2, Ls0/s;->c:Ln/d;

    invoke-direct {p0, v0, v1, v3, v4}, Ls0/l;->O(Ln/a;Ln/a;Ln/d;Ln/d;)V

    goto :goto_1

    :cond_1
    iget-object v3, p1, Ls0/s;->b:Landroid/util/SparseArray;

    iget-object v4, p2, Ls0/s;->b:Landroid/util/SparseArray;

    invoke-direct {p0, v0, v1, v3, v4}, Ls0/l;->I(Ln/a;Ln/a;Landroid/util/SparseArray;Landroid/util/SparseArray;)V

    goto :goto_1

    :cond_2
    iget-object v3, p1, Ls0/s;->d:Ln/a;

    iget-object v4, p2, Ls0/s;->d:Ln/a;

    invoke-direct {p0, v0, v1, v3, v4}, Ls0/l;->P(Ln/a;Ln/a;Ln/a;Ln/a;)V

    goto :goto_1

    :cond_3
    invoke-direct {p0, v0, v1}, Ls0/l;->N(Ln/a;Ln/a;)V

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_4
    invoke-direct {p0, v0, v1}, Ls0/l;->d(Ln/a;Ln/a;)V

    return-void
.end method

.method private W(Landroid/animation/Animator;Ln/a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/animation/Animator;",
            "Ln/a<",
            "Landroid/animation/Animator;",
            "Ls0/l$d;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_0

    new-instance v0, Ls0/l$b;

    invoke-direct {v0, p0, p2}, Ls0/l$b;-><init>(Ls0/l;Ln/a;)V

    invoke-virtual {p1, v0}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {p0, p1}, Ls0/l;->f(Landroid/animation/Animator;)V

    :cond_0
    return-void
.end method

.method private d(Ln/a;Ln/a;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ln/a<",
            "Landroid/view/View;",
            "Ls0/r;",
            ">;",
            "Ln/a<",
            "Landroid/view/View;",
            "Ls0/r;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p1}, Ln/g;->size()I

    move-result v2

    const/4 v3, 0x0

    if-ge v1, v2, :cond_1

    invoke-virtual {p1, v1}, Ln/g;->m(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ls0/r;

    iget-object v4, v2, Ls0/r;->b:Landroid/view/View;

    invoke-virtual {p0, v4}, Ls0/l;->G(Landroid/view/View;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Ls0/l;->t:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Ls0/l;->u:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    invoke-virtual {p2}, Ln/g;->size()I

    move-result p1

    if-ge v0, p1, :cond_3

    invoke-virtual {p2, v0}, Ln/g;->m(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ls0/r;

    iget-object v1, p1, Ls0/r;->b:Landroid/view/View;

    invoke-virtual {p0, v1}, Ls0/l;->G(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Ls0/l;->u:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object p1, p0, Ls0/l;->t:Ljava/util/ArrayList;

    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    return-void
.end method

.method private static e(Ls0/s;Landroid/view/View;Ls0/r;)V
    .locals 3

    iget-object v0, p0, Ls0/s;->a:Ln/a;

    invoke-virtual {v0, p1, p2}, Ln/g;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result p2

    const/4 v0, 0x0

    if-ltz p2, :cond_1

    iget-object v1, p0, Ls0/s;->b:Landroid/util/SparseArray;

    invoke-virtual {v1, p2}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v1

    if-ltz v1, :cond_0

    iget-object v1, p0, Ls0/s;->b:Landroid/util/SparseArray;

    invoke-virtual {v1, p2, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_0

    :cond_0
    iget-object v1, p0, Ls0/s;->b:Landroid/util/SparseArray;

    invoke-virtual {v1, p2, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    :cond_1
    :goto_0
    invoke-static {p1}, Landroidx/core/view/t;->F(Landroid/view/View;)Ljava/lang/String;

    move-result-object p2

    if-eqz p2, :cond_3

    iget-object v1, p0, Ls0/s;->d:Ln/a;

    invoke-virtual {v1, p2}, Ln/g;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Ls0/s;->d:Ln/a;

    invoke-virtual {v1, p2, v0}, Ln/g;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_2
    iget-object v1, p0, Ls0/s;->d:Ln/a;

    invoke-virtual {v1, p2, p1}, Ln/g;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    :goto_1
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object p2

    instance-of p2, p2, Landroid/widget/ListView;

    if-eqz p2, :cond_5

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object p2

    check-cast p2, Landroid/widget/ListView;

    invoke-virtual {p2}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    invoke-interface {v1}, Landroid/widget/ListAdapter;->hasStableIds()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-virtual {p2, p1}, Landroid/widget/ListView;->getPositionForView(Landroid/view/View;)I

    move-result v1

    invoke-virtual {p2, v1}, Landroid/widget/ListView;->getItemIdAtPosition(I)J

    move-result-wide v1

    iget-object p2, p0, Ls0/s;->c:Ln/d;

    invoke-virtual {p2, v1, v2}, Ln/d;->h(J)I

    move-result p2

    if-ltz p2, :cond_4

    iget-object p1, p0, Ls0/s;->c:Ln/d;

    invoke-virtual {p1, v1, v2}, Ln/d;->f(J)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    if-eqz p1, :cond_5

    const/4 p2, 0x0

    invoke-static {p1, p2}, Landroidx/core/view/t;->q0(Landroid/view/View;Z)V

    iget-object p0, p0, Ls0/s;->c:Ln/d;

    invoke-virtual {p0, v1, v2, v0}, Ln/d;->j(JLjava/lang/Object;)V

    goto :goto_2

    :cond_4
    const/4 p2, 0x1

    invoke-static {p1, p2}, Landroidx/core/view/t;->q0(Landroid/view/View;Z)V

    iget-object p0, p0, Ls0/s;->c:Ln/d;

    invoke-virtual {p0, v1, v2, p1}, Ln/d;->j(JLjava/lang/Object;)V

    :cond_5
    :goto_2
    return-void
.end method

.method private h(Landroid/view/View;Z)V
    .locals 5

    if-nez p1, :cond_0

    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    iget-object v1, p0, Ls0/l;->i:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    return-void

    :cond_1
    iget-object v1, p0, Ls0/l;->j:Ljava/util/ArrayList;

    if-eqz v1, :cond_2

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    return-void

    :cond_2
    iget-object v1, p0, Ls0/l;->k:Ljava/util/ArrayList;

    const/4 v2, 0x0

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    move v3, v2

    :goto_0
    if-ge v3, v1, :cond_4

    iget-object v4, p0, Ls0/l;->k:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Class;

    invoke-virtual {v4, p1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    return-void

    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_4
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    instance-of v1, v1, Landroid/view/ViewGroup;

    if-eqz v1, :cond_7

    new-instance v1, Ls0/r;

    invoke-direct {v1, p1}, Ls0/r;-><init>(Landroid/view/View;)V

    if-eqz p2, :cond_5

    invoke-virtual {p0, v1}, Ls0/l;->j(Ls0/r;)V

    goto :goto_1

    :cond_5
    invoke-virtual {p0, v1}, Ls0/l;->g(Ls0/r;)V

    :goto_1
    iget-object v3, v1, Ls0/r;->c:Ljava/util/ArrayList;

    invoke-virtual {v3, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0, v1}, Ls0/l;->i(Ls0/r;)V

    if-eqz p2, :cond_6

    iget-object v3, p0, Ls0/l;->p:Ls0/s;

    goto :goto_2

    :cond_6
    iget-object v3, p0, Ls0/l;->q:Ls0/s;

    :goto_2
    invoke-static {v3, p1, v1}, Ls0/l;->e(Ls0/s;Landroid/view/View;Ls0/r;)V

    :cond_7
    instance-of v1, p1, Landroid/view/ViewGroup;

    if-eqz v1, :cond_c

    iget-object v1, p0, Ls0/l;->m:Ljava/util/ArrayList;

    if-eqz v1, :cond_8

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    return-void

    :cond_8
    iget-object v0, p0, Ls0/l;->n:Ljava/util/ArrayList;

    if-eqz v0, :cond_9

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    return-void

    :cond_9
    iget-object v0, p0, Ls0/l;->o:Ljava/util/ArrayList;

    if-eqz v0, :cond_b

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    move v1, v2

    :goto_3
    if-ge v1, v0, :cond_b

    iget-object v3, p0, Ls0/l;->o:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Class;

    invoke-virtual {v3, p1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    return-void

    :cond_a
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_b
    check-cast p1, Landroid/view/ViewGroup;

    :goto_4
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ge v2, v0, :cond_c

    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Ls0/l;->h(Landroid/view/View;Z)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_c
    return-void
.end method

.method private static x()Ln/a;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ln/a<",
            "Landroid/animation/Animator;",
            "Ls0/l$d;",
            ">;"
        }
    .end annotation

    sget-object v0, Ls0/l;->I:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ln/a;

    if-nez v0, :cond_0

    new-instance v0, Ln/a;

    invoke-direct {v0}, Ln/a;-><init>()V

    sget-object v1, Ls0/l;->I:Ljava/lang/ThreadLocal;

    invoke-virtual {v1, v0}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    :cond_0
    return-object v0
.end method


# virtual methods
.method public A()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Ls0/l;->g:Ljava/util/ArrayList;

    return-object v0
.end method

.method public B()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Class<",
            "*>;>;"
        }
    .end annotation

    iget-object v0, p0, Ls0/l;->h:Ljava/util/ArrayList;

    return-object v0
.end method

.method public C()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Ls0/l;->f:Ljava/util/ArrayList;

    return-object v0
.end method

.method public D()[Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public E(Landroid/view/View;Z)Ls0/r;
    .locals 1

    iget-object v0, p0, Ls0/l;->r:Ls0/p;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1, p2}, Ls0/l;->E(Landroid/view/View;Z)Ls0/r;

    move-result-object p1

    return-object p1

    :cond_0
    if-eqz p2, :cond_1

    iget-object p2, p0, Ls0/l;->p:Ls0/s;

    goto :goto_0

    :cond_1
    iget-object p2, p0, Ls0/l;->q:Ls0/s;

    :goto_0
    iget-object p2, p2, Ls0/s;->a:Ln/a;

    invoke-virtual {p2, p1}, Ln/g;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ls0/r;

    return-object p1
.end method

.method public F(Ls0/r;Ls0/r;)Z
    .locals 6

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-eqz p1, :cond_3

    if-eqz p2, :cond_3

    invoke-virtual {p0}, Ls0/l;->D()[Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    array-length v3, v2

    move v4, v0

    :goto_0
    if-ge v4, v3, :cond_3

    aget-object v5, v2, v4

    invoke-static {p1, p2, v5}, Ls0/l;->H(Ls0/r;Ls0/r;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_1
    iget-object v2, p1, Ls0/r;->a:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {p1, p2, v3}, Ls0/l;->H(Ls0/r;Ls0/r;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    :goto_1
    move v0, v1

    :cond_3
    return v0
.end method

.method G(Landroid/view/View;)Z
    .locals 5

    goto/32 :goto_1f

    nop

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/32 :goto_44

    nop

    :goto_1
    iget-object v1, p0, Ls0/l;->h:Ljava/util/ArrayList;

    goto/32 :goto_43

    nop

    :goto_2
    if-eqz v0, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_5f

    nop

    :goto_3
    move v0, v2

    :goto_4
    goto/32 :goto_42

    nop

    :goto_5
    if-nez v1, :cond_1

    goto/32 :goto_32

    :cond_1
    goto/32 :goto_30

    nop

    :goto_6
    return v2

    :goto_7
    goto/32 :goto_53

    nop

    :goto_8
    if-eqz v1, :cond_2

    goto/32 :goto_3a

    :cond_2
    goto/32 :goto_17

    nop

    :goto_9
    if-nez v1, :cond_3

    goto/32 :goto_46

    :cond_3
    goto/32 :goto_18

    nop

    :goto_a
    iget-object v1, p0, Ls0/l;->e:Ljava/util/ArrayList;

    goto/32 :goto_3b

    nop

    :goto_b
    iget-object v1, p0, Ls0/l;->j:Ljava/util/ArrayList;

    goto/32 :goto_2f

    nop

    :goto_c
    if-lt v0, v1, :cond_4

    goto/32 :goto_57

    :cond_4
    goto/32 :goto_1

    nop

    :goto_d
    move v3, v2

    :goto_e
    goto/32 :goto_1b

    nop

    :goto_f
    if-nez v0, :cond_5

    goto/32 :goto_4f

    :cond_5
    goto/32 :goto_4e

    nop

    :goto_10
    iget-object v1, p0, Ls0/l;->i:Ljava/util/ArrayList;

    goto/32 :goto_52

    nop

    :goto_11
    invoke-virtual {v4, p1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v4

    goto/32 :goto_23

    nop

    :goto_12
    iget-object v1, p0, Ls0/l;->e:Ljava/util/ArrayList;

    goto/32 :goto_0

    nop

    :goto_13
    if-nez v1, :cond_6

    goto/32 :goto_38

    :cond_6
    goto/32 :goto_37

    nop

    :goto_14
    return v2

    :goto_15
    goto/32 :goto_a

    nop

    :goto_16
    invoke-static {p1}, Landroidx/core/view/t;->F(Landroid/view/View;)Ljava/lang/String;

    move-result-object v3

    goto/32 :goto_48

    nop

    :goto_17
    iget-object v1, p0, Ls0/l;->f:Ljava/util/ArrayList;

    goto/32 :goto_35

    nop

    :goto_18
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    goto/32 :goto_d

    nop

    :goto_19
    iget-object v4, p0, Ls0/l;->k:Ljava/util/ArrayList;

    goto/32 :goto_5b

    nop

    :goto_1a
    check-cast v4, Ljava/lang/Class;

    goto/32 :goto_11

    nop

    :goto_1b
    if-lt v3, v1, :cond_7

    goto/32 :goto_46

    :cond_7
    goto/32 :goto_19

    nop

    :goto_1c
    if-nez v1, :cond_8

    goto/32 :goto_26

    :cond_8
    goto/32 :goto_41

    nop

    :goto_1d
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    goto/32 :goto_33

    nop

    :goto_1e
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto/32 :goto_2e

    nop

    :goto_1f
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    goto/32 :goto_10

    nop

    :goto_20
    if-nez v1, :cond_9

    goto/32 :goto_28

    :cond_9
    goto/32 :goto_21

    nop

    :goto_21
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    goto/32 :goto_1d

    nop

    :goto_22
    const/4 v3, 0x1

    goto/32 :goto_8

    nop

    :goto_23
    if-nez v4, :cond_a

    goto/32 :goto_5d

    :cond_a
    goto/32 :goto_5c

    nop

    :goto_24
    iget-object v1, p0, Ls0/l;->l:Ljava/util/ArrayList;

    goto/32 :goto_50

    nop

    :goto_25
    if-nez v1, :cond_b

    goto/32 :goto_3a

    :cond_b
    :goto_26
    goto/32 :goto_51

    nop

    :goto_27
    return v2

    :goto_28
    goto/32 :goto_b

    nop

    :goto_29
    iget-object v1, p0, Ls0/l;->h:Ljava/util/ArrayList;

    goto/32 :goto_1c

    nop

    :goto_2a
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    goto/32 :goto_c

    nop

    :goto_2b
    if-nez v0, :cond_c

    goto/32 :goto_4f

    :cond_c
    goto/32 :goto_47

    nop

    :goto_2c
    if-nez v1, :cond_d

    goto/32 :goto_3e

    :cond_d
    goto/32 :goto_3d

    nop

    :goto_2d
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto/32 :goto_f

    nop

    :goto_2e
    if-nez v0, :cond_e

    goto/32 :goto_4d

    :cond_e
    goto/32 :goto_4c

    nop

    :goto_2f
    if-nez v1, :cond_f

    goto/32 :goto_3e

    :cond_f
    goto/32 :goto_59

    nop

    :goto_30
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    goto/32 :goto_31

    nop

    :goto_31
    if-nez v1, :cond_10

    goto/32 :goto_3a

    :cond_10
    :goto_32
    goto/32 :goto_39

    nop

    :goto_33
    if-nez v1, :cond_11

    goto/32 :goto_28

    :cond_11
    goto/32 :goto_27

    nop

    :goto_34
    iget-object v1, p0, Ls0/l;->k:Ljava/util/ArrayList;

    goto/32 :goto_9

    nop

    :goto_35
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    goto/32 :goto_5a

    nop

    :goto_36
    invoke-static {p1}, Landroidx/core/view/t;->F(Landroid/view/View;)Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_54

    nop

    :goto_37
    return v3

    :goto_38
    goto/32 :goto_3c

    nop

    :goto_39
    return v3

    :goto_3a
    goto/32 :goto_12

    nop

    :goto_3b
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    goto/32 :goto_22

    nop

    :goto_3c
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_56

    nop

    :goto_3d
    return v2

    :goto_3e
    goto/32 :goto_34

    nop

    :goto_3f
    invoke-virtual {v1, p1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v1

    goto/32 :goto_13

    nop

    :goto_40
    check-cast v1, Ljava/lang/Class;

    goto/32 :goto_3f

    nop

    :goto_41
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    goto/32 :goto_25

    nop

    :goto_42
    iget-object v1, p0, Ls0/l;->h:Ljava/util/ArrayList;

    goto/32 :goto_2a

    nop

    :goto_43
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_40

    nop

    :goto_44
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto/32 :goto_2

    nop

    :goto_45
    goto/16 :goto_e

    :goto_46
    goto/32 :goto_24

    nop

    :goto_47
    invoke-static {p1}, Landroidx/core/view/t;->F(Landroid/view/View;)Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_2d

    nop

    :goto_48
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    goto/32 :goto_4b

    nop

    :goto_49
    iget-object v1, p0, Ls0/l;->l:Ljava/util/ArrayList;

    goto/32 :goto_16

    nop

    :goto_4a
    iget-object v0, p0, Ls0/l;->h:Ljava/util/ArrayList;

    goto/32 :goto_58

    nop

    :goto_4b
    if-nez v1, :cond_12

    goto/32 :goto_15

    :cond_12
    goto/32 :goto_14

    nop

    :goto_4c
    goto/16 :goto_7

    :goto_4d
    goto/32 :goto_55

    nop

    :goto_4e
    return v3

    :goto_4f
    goto/32 :goto_4a

    nop

    :goto_50
    if-nez v1, :cond_13

    goto/32 :goto_15

    :cond_13
    goto/32 :goto_36

    nop

    :goto_51
    iget-object v1, p0, Ls0/l;->g:Ljava/util/ArrayList;

    goto/32 :goto_5

    nop

    :goto_52
    const/4 v2, 0x0

    goto/32 :goto_20

    nop

    :goto_53
    return v3

    :goto_54
    if-nez v1, :cond_14

    goto/32 :goto_15

    :cond_14
    goto/32 :goto_49

    nop

    :goto_55
    iget-object v0, p0, Ls0/l;->g:Ljava/util/ArrayList;

    goto/32 :goto_2b

    nop

    :goto_56
    goto/16 :goto_4

    :goto_57
    goto/32 :goto_6

    nop

    :goto_58
    if-nez v0, :cond_15

    goto/32 :goto_57

    :cond_15
    goto/32 :goto_3

    nop

    :goto_59
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    goto/32 :goto_2c

    nop

    :goto_5a
    if-eqz v1, :cond_16

    goto/32 :goto_3a

    :cond_16
    goto/32 :goto_29

    nop

    :goto_5b
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    goto/32 :goto_1a

    nop

    :goto_5c
    return v2

    :goto_5d
    goto/32 :goto_5e

    nop

    :goto_5e
    add-int/lit8 v3, v3, 0x1

    goto/32 :goto_45

    nop

    :goto_5f
    iget-object v0, p0, Ls0/l;->f:Ljava/util/ArrayList;

    goto/32 :goto_1e

    nop
.end method

.method public R(Landroid/view/View;)V
    .locals 5

    iget-boolean v0, p0, Ls0/l;->A:Z

    if-nez v0, :cond_3

    invoke-static {}, Ls0/l;->x()Ln/a;

    move-result-object v0

    invoke-virtual {v0}, Ln/g;->size()I

    move-result v1

    invoke-static {p1}, Ls0/y;->d(Landroid/view/View;)Ls0/h0;

    move-result-object p1

    const/4 v2, 0x1

    sub-int/2addr v1, v2

    :goto_0
    if-ltz v1, :cond_1

    invoke-virtual {v0, v1}, Ln/g;->m(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ls0/l$d;

    iget-object v4, v3, Ls0/l$d;->a:Landroid/view/View;

    if-eqz v4, :cond_0

    iget-object v3, v3, Ls0/l$d;->d:Ls0/h0;

    invoke-virtual {p1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0, v1}, Ln/g;->i(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/animation/Animator;

    invoke-static {v3}, Ls0/a;->b(Landroid/animation/Animator;)V

    :cond_0
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_1
    iget-object p1, p0, Ls0/l;->B:Ljava/util/ArrayList;

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result p1

    if-lez p1, :cond_2

    iget-object p1, p0, Ls0/l;->B:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v0, :cond_2

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ls0/l$f;

    invoke-interface {v3, p0}, Ls0/l$f;->a(Ls0/l;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    iput-boolean v2, p0, Ls0/l;->z:Z

    :cond_3
    return-void
.end method

.method S(Landroid/view/ViewGroup;)V
    .locals 10

    goto/32 :goto_48

    nop

    :goto_0
    invoke-virtual/range {v4 .. v9}, Ls0/l;->o(Landroid/view/ViewGroup;Ls0/s;Ls0/s;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    goto/32 :goto_43

    nop

    :goto_1
    invoke-virtual {v2, v6}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    goto/32 :goto_12

    nop

    :goto_2
    iget-object v8, p0, Ls0/l;->t:Ljava/util/ArrayList;

    goto/32 :goto_18

    nop

    :goto_3
    check-cast v5, Ls0/l$d;

    goto/32 :goto_30

    nop

    :goto_4
    invoke-direct {p0, v0, v1}, Ls0/l;->Q(Ls0/s;Ls0/s;)V

    goto/32 :goto_13

    nop

    :goto_5
    check-cast v4, Landroid/animation/Animator;

    goto/32 :goto_c

    nop

    :goto_6
    invoke-virtual {v0}, Ln/g;->size()I

    move-result v1

    goto/32 :goto_27

    nop

    :goto_7
    invoke-virtual {v5, v6, v9}, Ls0/l;->F(Ls0/r;Ls0/r;)Z

    move-result v5

    goto/32 :goto_19

    nop

    :goto_8
    check-cast v9, Ls0/r;

    :goto_9
    goto/32 :goto_15

    nop

    :goto_a
    goto/16 :goto_3c

    :goto_b
    goto/32 :goto_3b

    nop

    :goto_c
    if-nez v4, :cond_0

    goto/32 :goto_46

    :cond_0
    goto/32 :goto_35

    nop

    :goto_d
    if-nez v9, :cond_1

    goto/32 :goto_b

    :cond_1
    :goto_e
    goto/32 :goto_2b

    nop

    :goto_f
    iget-object v6, v5, Ls0/l$d;->d:Ls0/h0;

    goto/32 :goto_1

    nop

    :goto_10
    iget-object v9, p0, Ls0/l;->q:Ls0/s;

    goto/32 :goto_3d

    nop

    :goto_11
    new-instance v0, Ljava/util/ArrayList;

    goto/32 :goto_2c

    nop

    :goto_12
    if-nez v6, :cond_2

    goto/32 :goto_46

    :cond_2
    goto/32 :goto_21

    nop

    :goto_13
    invoke-static {}, Ls0/l;->x()Ln/a;

    move-result-object v0

    goto/32 :goto_6

    nop

    :goto_14
    invoke-virtual {v4}, Landroid/animation/Animator;->isRunning()Z

    move-result v5

    goto/32 :goto_1c

    nop

    :goto_15
    if-eqz v8, :cond_3

    goto/32 :goto_e

    :cond_3
    goto/32 :goto_d

    nop

    :goto_16
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto/32 :goto_44

    nop

    :goto_17
    invoke-virtual {p0, v7, v3}, Ls0/l;->E(Landroid/view/View;Z)Ls0/r;

    move-result-object v8

    goto/32 :goto_3e

    nop

    :goto_18
    iget-object v9, p0, Ls0/l;->u:Ljava/util/ArrayList;

    goto/32 :goto_3f

    nop

    :goto_19
    if-nez v5, :cond_4

    goto/32 :goto_b

    :cond_4
    goto/32 :goto_25

    nop

    :goto_1a
    move-object v9, v7

    goto/32 :goto_8

    nop

    :goto_1b
    move-object v5, p1

    goto/32 :goto_0

    nop

    :goto_1c
    if-eqz v5, :cond_5

    goto/32 :goto_24

    :cond_5
    goto/32 :goto_47

    nop

    :goto_1d
    if-nez v5, :cond_6

    goto/32 :goto_46

    :cond_6
    goto/32 :goto_14

    nop

    :goto_1e
    goto/16 :goto_39

    :goto_1f
    goto/32 :goto_36

    nop

    :goto_20
    add-int/lit8 v1, v1, -0x1

    goto/32 :goto_1e

    nop

    :goto_21
    iget-object v6, v5, Ls0/l$d;->c:Ls0/r;

    goto/32 :goto_42

    nop

    :goto_22
    invoke-virtual {v0, v1}, Ln/g;->i(I)Ljava/lang/Object;

    move-result-object v4

    goto/32 :goto_5

    nop

    :goto_23
    goto/16 :goto_46

    :goto_24
    goto/32 :goto_45

    nop

    :goto_25
    move v5, v3

    goto/32 :goto_a

    nop

    :goto_26
    iget-object v7, p0, Ls0/l;->q:Ls0/s;

    goto/32 :goto_2

    nop

    :goto_27
    invoke-static {p1}, Ls0/y;->d(Landroid/view/View;)Ls0/h0;

    move-result-object v2

    goto/32 :goto_29

    nop

    :goto_28
    return-void

    :goto_29
    const/4 v3, 0x1

    goto/32 :goto_38

    nop

    :goto_2a
    if-eqz v8, :cond_7

    goto/32 :goto_9

    :cond_7
    goto/32 :goto_2f

    nop

    :goto_2b
    iget-object v5, v5, Ls0/l$d;->e:Ls0/l;

    goto/32 :goto_7

    nop

    :goto_2c
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto/32 :goto_33

    nop

    :goto_2d
    if-nez v5, :cond_8

    goto/32 :goto_32

    :cond_8
    goto/32 :goto_31

    nop

    :goto_2e
    iget-object v1, p0, Ls0/l;->q:Ls0/s;

    goto/32 :goto_4

    nop

    :goto_2f
    if-eqz v9, :cond_9

    goto/32 :goto_9

    :cond_9
    goto/32 :goto_10

    nop

    :goto_30
    if-nez v5, :cond_a

    goto/32 :goto_46

    :cond_a
    goto/32 :goto_49

    nop

    :goto_31
    goto :goto_24

    :goto_32
    goto/32 :goto_40

    nop

    :goto_33
    iput-object v0, p0, Ls0/l;->u:Ljava/util/ArrayList;

    goto/32 :goto_34

    nop

    :goto_34
    iget-object v0, p0, Ls0/l;->p:Ls0/s;

    goto/32 :goto_2e

    nop

    :goto_35
    invoke-virtual {v0, v4}, Ln/g;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    goto/32 :goto_3

    nop

    :goto_36
    iget-object v6, p0, Ls0/l;->p:Ls0/s;

    goto/32 :goto_26

    nop

    :goto_37
    invoke-virtual {v9, v7}, Ln/g;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    goto/32 :goto_1a

    nop

    :goto_38
    sub-int/2addr v1, v3

    :goto_39
    goto/32 :goto_41

    nop

    :goto_3a
    if-nez v6, :cond_b

    goto/32 :goto_46

    :cond_b
    goto/32 :goto_f

    nop

    :goto_3b
    const/4 v5, 0x0

    :goto_3c
    goto/32 :goto_1d

    nop

    :goto_3d
    iget-object v9, v9, Ls0/s;->a:Ln/a;

    goto/32 :goto_37

    nop

    :goto_3e
    invoke-virtual {p0, v7, v3}, Ls0/l;->t(Landroid/view/View;Z)Ls0/r;

    move-result-object v9

    goto/32 :goto_2a

    nop

    :goto_3f
    move-object v4, p0

    goto/32 :goto_1b

    nop

    :goto_40
    invoke-virtual {v0, v4}, Ln/g;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto/32 :goto_23

    nop

    :goto_41
    if-gez v1, :cond_c

    goto/32 :goto_1f

    :cond_c
    goto/32 :goto_22

    nop

    :goto_42
    iget-object v7, v5, Ls0/l$d;->a:Landroid/view/View;

    goto/32 :goto_17

    nop

    :goto_43
    invoke-virtual {p0}, Ls0/l;->X()V

    goto/32 :goto_28

    nop

    :goto_44
    iput-object v0, p0, Ls0/l;->t:Ljava/util/ArrayList;

    goto/32 :goto_11

    nop

    :goto_45
    invoke-virtual {v4}, Landroid/animation/Animator;->cancel()V

    :goto_46
    goto/32 :goto_20

    nop

    :goto_47
    invoke-virtual {v4}, Landroid/animation/Animator;->isStarted()Z

    move-result v5

    goto/32 :goto_2d

    nop

    :goto_48
    new-instance v0, Ljava/util/ArrayList;

    goto/32 :goto_16

    nop

    :goto_49
    iget-object v6, v5, Ls0/l$d;->a:Landroid/view/View;

    goto/32 :goto_3a

    nop
.end method

.method public T(Ls0/l$f;)Ls0/l;
    .locals 1

    iget-object v0, p0, Ls0/l;->B:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    return-object p0

    :cond_0
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    iget-object p1, p0, Ls0/l;->B:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result p1

    if-nez p1, :cond_1

    const/4 p1, 0x0

    iput-object p1, p0, Ls0/l;->B:Ljava/util/ArrayList;

    :cond_1
    return-object p0
.end method

.method public U(Landroid/view/View;)Ls0/l;
    .locals 1

    iget-object v0, p0, Ls0/l;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public V(Landroid/view/View;)V
    .locals 5

    iget-boolean v0, p0, Ls0/l;->z:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Ls0/l;->A:Z

    const/4 v1, 0x0

    if-nez v0, :cond_2

    invoke-static {}, Ls0/l;->x()Ln/a;

    move-result-object v0

    invoke-virtual {v0}, Ln/g;->size()I

    move-result v2

    invoke-static {p1}, Ls0/y;->d(Landroid/view/View;)Ls0/h0;

    move-result-object p1

    add-int/lit8 v2, v2, -0x1

    :goto_0
    if-ltz v2, :cond_1

    invoke-virtual {v0, v2}, Ln/g;->m(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ls0/l$d;

    iget-object v4, v3, Ls0/l$d;->a:Landroid/view/View;

    if-eqz v4, :cond_0

    iget-object v3, v3, Ls0/l$d;->d:Ls0/h0;

    invoke-virtual {p1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0, v2}, Ln/g;->i(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/animation/Animator;

    invoke-static {v3}, Ls0/a;->c(Landroid/animation/Animator;)V

    :cond_0
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    :cond_1
    iget-object p1, p0, Ls0/l;->B:Ljava/util/ArrayList;

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result p1

    if-lez p1, :cond_2

    iget-object p1, p0, Ls0/l;->B:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    move v2, v1

    :goto_1
    if-ge v2, v0, :cond_2

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ls0/l$f;

    invoke-interface {v3, p0}, Ls0/l$f;->e(Ls0/l;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    iput-boolean v1, p0, Ls0/l;->z:Z

    :cond_3
    return-void
.end method

.method protected X()V
    .locals 4

    invoke-virtual {p0}, Ls0/l;->e0()V

    invoke-static {}, Ls0/l;->x()Ln/a;

    move-result-object v0

    iget-object v1, p0, Ls0/l;->C:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/animation/Animator;

    invoke-virtual {v0, v2}, Ln/g;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Ls0/l;->e0()V

    invoke-direct {p0, v2, v0}, Ls0/l;->W(Landroid/animation/Animator;Ln/a;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Ls0/l;->C:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    invoke-virtual {p0}, Ls0/l;->p()V

    return-void
.end method

.method public Y(J)Ls0/l;
    .locals 0

    iput-wide p1, p0, Ls0/l;->c:J

    return-object p0
.end method

.method public Z(Ls0/l$e;)V
    .locals 0

    iput-object p1, p0, Ls0/l;->D:Ls0/l$e;

    return-void
.end method

.method public a(Ls0/l$f;)Ls0/l;
    .locals 1

    iget-object v0, p0, Ls0/l;->B:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ls0/l;->B:Ljava/util/ArrayList;

    :cond_0
    iget-object v0, p0, Ls0/l;->B:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public a0(Landroid/animation/TimeInterpolator;)Ls0/l;
    .locals 0

    iput-object p1, p0, Ls0/l;->d:Landroid/animation/TimeInterpolator;

    return-object p0
.end method

.method public b0(Ls0/g;)V
    .locals 0

    if-nez p1, :cond_0

    sget-object p1, Ls0/l;->H:Ls0/g;

    :cond_0
    iput-object p1, p0, Ls0/l;->F:Ls0/g;

    return-void
.end method

.method public c(Landroid/view/View;)Ls0/l;
    .locals 1

    iget-object v0, p0, Ls0/l;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public c0(Ls0/o;)V
    .locals 0

    return-void
.end method

.method protected cancel()V
    .locals 4

    iget-object v0, p0, Ls0/l;->x:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_0

    iget-object v1, p0, Ls0/l;->x:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/animation/Animator;

    invoke-virtual {v1}, Landroid/animation/Animator;->cancel()V

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Ls0/l;->B:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Ls0/l;->B:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v1, :cond_1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ls0/l$f;

    invoke-interface {v3, p0}, Ls0/l$f;->b(Ls0/l;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    return-void
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Ls0/l;->m()Ls0/l;

    move-result-object v0

    return-object v0
.end method

.method public d0(J)Ls0/l;
    .locals 0

    iput-wide p1, p0, Ls0/l;->b:J

    return-object p0
.end method

.method protected e0()V
    .locals 5

    iget v0, p0, Ls0/l;->y:I

    if-nez v0, :cond_1

    iget-object v0, p0, Ls0/l;->B:Ljava/util/ArrayList;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Ls0/l;->B:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v3, v1

    :goto_0
    if-ge v3, v2, :cond_0

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ls0/l$f;

    invoke-interface {v4, p0}, Ls0/l$f;->d(Ls0/l;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    iput-boolean v1, p0, Ls0/l;->A:Z

    :cond_1
    iget v0, p0, Ls0/l;->y:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ls0/l;->y:I

    return-void
.end method

.method protected f(Landroid/animation/Animator;)V
    .locals 4

    if-nez p1, :cond_0

    invoke-virtual {p0}, Ls0/l;->p()V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Ls0/l;->q()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_1

    invoke-virtual {p0}, Ls0/l;->q()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    :cond_1
    invoke-virtual {p0}, Ls0/l;->y()J

    move-result-wide v0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_2

    invoke-virtual {p0}, Ls0/l;->y()J

    move-result-wide v0

    invoke-virtual {p1}, Landroid/animation/Animator;->getStartDelay()J

    move-result-wide v2

    add-long/2addr v0, v2

    invoke-virtual {p1, v0, v1}, Landroid/animation/Animator;->setStartDelay(J)V

    :cond_2
    invoke-virtual {p0}, Ls0/l;->s()Landroid/animation/TimeInterpolator;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Ls0/l;->s()Landroid/animation/TimeInterpolator;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    :cond_3
    new-instance v0, Ls0/l$c;

    invoke-direct {v0, p0}, Ls0/l$c;-><init>(Ls0/l;)V

    invoke-virtual {p1, v0}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {p1}, Landroid/animation/Animator;->start()V

    :goto_0
    return-void
.end method

.method f0(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    goto/32 :goto_75

    nop

    :goto_0
    cmp-long v0, v0, v2

    goto/32 :goto_1e

    nop

    :goto_1
    if-gtz v0, :cond_0

    goto/32 :goto_4f

    :cond_0
    :goto_2
    goto/32 :goto_7b

    nop

    :goto_3
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_7

    nop

    :goto_4
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_3

    nop

    :goto_5
    iget-wide v0, p0, Ls0/l;->c:J

    goto/32 :goto_74

    nop

    :goto_6
    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_16

    nop

    :goto_7
    const-string p1, "tgts("

    goto/32 :goto_59

    nop

    :goto_8
    iget-object v0, p0, Ls0/l;->f:Ljava/util/ArrayList;

    goto/32 :goto_30

    nop

    :goto_9
    const-string p1, "dur("

    goto/32 :goto_54

    nop

    :goto_a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :goto_b
    goto/32 :goto_1d

    nop

    :goto_c
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_6a

    nop

    :goto_d
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_3a

    nop

    :goto_e
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result p1

    goto/32 :goto_6

    nop

    :goto_f
    const-string p1, "interp("

    goto/32 :goto_5f

    nop

    :goto_10
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_13

    nop

    :goto_11
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_6d

    nop

    :goto_12
    if-lt v2, v0, :cond_1

    goto/32 :goto_4f

    :cond_1
    goto/32 :goto_46

    nop

    :goto_13
    iget-wide v2, p0, Ls0/l;->b:J

    goto/32 :goto_1f

    nop

    :goto_14
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_51

    nop

    :goto_15
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_40

    nop

    :goto_16
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_45

    nop

    :goto_17
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_25

    nop

    :goto_18
    iget-wide v4, p0, Ls0/l;->c:J

    goto/32 :goto_69

    nop

    :goto_19
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :goto_1a
    goto/32 :goto_67

    nop

    :goto_1b
    iget-object v0, p0, Ls0/l;->e:Ljava/util/ArrayList;

    goto/32 :goto_28

    nop

    :goto_1c
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto/32 :goto_5b

    nop

    :goto_1d
    new-instance v3, Ljava/lang/StringBuilder;

    goto/32 :goto_65

    nop

    :goto_1e
    const-string v1, ") "

    goto/32 :goto_21

    nop

    :goto_1f
    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    goto/32 :goto_6e

    nop

    :goto_20
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_22

    nop

    :goto_21
    if-nez v0, :cond_2

    goto/32 :goto_1a

    :cond_2
    goto/32 :goto_20

    nop

    :goto_22
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_24

    nop

    :goto_23
    const/4 v2, 0x0

    goto/32 :goto_7c

    nop

    :goto_24
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_9

    nop

    :goto_25
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_38

    nop

    :goto_26
    if-lt v0, v3, :cond_3

    goto/32 :goto_3b

    :cond_3
    goto/32 :goto_7d

    nop

    :goto_27
    add-int/lit8 v2, v2, 0x1

    goto/32 :goto_4e

    nop

    :goto_28
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto/32 :goto_2d

    nop

    :goto_29
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :goto_2a
    goto/32 :goto_64

    nop

    :goto_2b
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_3c

    nop

    :goto_2c
    iget-object v0, p0, Ls0/l;->f:Ljava/util/ArrayList;

    goto/32 :goto_73

    nop

    :goto_2d
    const-string v1, ", "

    goto/32 :goto_23

    nop

    :goto_2e
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_2b

    nop

    :goto_2f
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    goto/32 :goto_7a

    nop

    :goto_30
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto/32 :goto_1

    nop

    :goto_31
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    goto/32 :goto_3d

    nop

    :goto_32
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_42

    nop

    :goto_33
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_6b

    nop

    :goto_34
    iget-object p1, p0, Ls0/l;->d:Landroid/animation/TimeInterpolator;

    goto/32 :goto_4a

    nop

    :goto_35
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_78

    nop

    :goto_36
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :goto_37
    goto/32 :goto_2e

    nop

    :goto_38
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_f

    nop

    :goto_39
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_1b

    nop

    :goto_3a
    goto/16 :goto_77

    :goto_3b
    goto/32 :goto_8

    nop

    :goto_3c
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_47

    nop

    :goto_3d
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto/32 :goto_3e

    nop

    :goto_3e
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_27

    nop

    :goto_3f
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_66

    nop

    :goto_40
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_53

    nop

    :goto_41
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_4

    nop

    :goto_42
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_5

    nop

    :goto_43
    if-nez v0, :cond_4

    goto/32 :goto_4c

    :cond_4
    goto/32 :goto_6c

    nop

    :goto_44
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    goto/32 :goto_26

    nop

    :goto_45
    const-string p1, ": "

    goto/32 :goto_32

    nop

    :goto_46
    if-gtz v2, :cond_5

    goto/32 :goto_37

    :cond_5
    goto/32 :goto_c

    nop

    :goto_47
    iget-object p1, p0, Ls0/l;->f:Ljava/util/ArrayList;

    goto/32 :goto_31

    nop

    :goto_48
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_11

    nop

    :goto_49
    if-nez v0, :cond_6

    goto/32 :goto_5e

    :cond_6
    goto/32 :goto_17

    nop

    :goto_4a
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto/32 :goto_71

    nop

    :goto_4b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :goto_4c
    goto/32 :goto_70

    nop

    :goto_4d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_19

    nop

    :goto_4e
    goto/16 :goto_2

    :goto_4f
    goto/32 :goto_15

    nop

    :goto_50
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_29

    nop

    :goto_51
    iget-object p1, p0, Ls0/l;->e:Ljava/util/ArrayList;

    goto/32 :goto_2f

    nop

    :goto_52
    cmp-long v0, v4, v2

    goto/32 :goto_43

    nop

    :goto_53
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_5c

    nop

    :goto_54
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_18

    nop

    :goto_55
    new-instance v3, Ljava/lang/StringBuilder;

    goto/32 :goto_33

    nop

    :goto_56
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_a

    nop

    :goto_57
    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_35

    nop

    :goto_58
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto/32 :goto_12

    nop

    :goto_59
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_39

    nop

    :goto_5a
    iget-object v3, p0, Ls0/l;->e:Ljava/util/ArrayList;

    goto/32 :goto_44

    nop

    :goto_5b
    if-lez v0, :cond_7

    goto/32 :goto_61

    :cond_7
    goto/32 :goto_2c

    nop

    :goto_5c
    const-string p1, ")"

    goto/32 :goto_50

    nop

    :goto_5d
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :goto_5e
    goto/32 :goto_79

    nop

    :goto_5f
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_34

    nop

    :goto_60
    if-gtz v0, :cond_8

    goto/32 :goto_2a

    :cond_8
    :goto_61
    goto/32 :goto_41

    nop

    :goto_62
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_e

    nop

    :goto_63
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_36

    nop

    :goto_64
    return-object p1

    :goto_65
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_14

    nop

    :goto_66
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    goto/32 :goto_57

    nop

    :goto_67
    iget-wide v4, p0, Ls0/l;->b:J

    goto/32 :goto_52

    nop

    :goto_68
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_63

    nop

    :goto_69
    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    goto/32 :goto_4d

    nop

    :goto_6a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_68

    nop

    :goto_6b
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_56

    nop

    :goto_6c
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_48

    nop

    :goto_6d
    const-string p1, "dly("

    goto/32 :goto_10

    nop

    :goto_6e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_4b

    nop

    :goto_6f
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_3f

    nop

    :goto_70
    iget-object v0, p0, Ls0/l;->d:Landroid/animation/TimeInterpolator;

    goto/32 :goto_49

    nop

    :goto_71
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_5d

    nop

    :goto_72
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_d

    nop

    :goto_73
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto/32 :goto_60

    nop

    :goto_74
    const-wide/16 v2, -0x1

    goto/32 :goto_0

    nop

    :goto_75
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_6f

    nop

    :goto_76
    move v0, v2

    :goto_77
    goto/32 :goto_5a

    nop

    :goto_78
    const-string p1, "@"

    goto/32 :goto_62

    nop

    :goto_79
    iget-object v0, p0, Ls0/l;->e:Ljava/util/ArrayList;

    goto/32 :goto_1c

    nop

    :goto_7a
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto/32 :goto_72

    nop

    :goto_7b
    iget-object v0, p0, Ls0/l;->f:Ljava/util/ArrayList;

    goto/32 :goto_58

    nop

    :goto_7c
    if-gtz v0, :cond_9

    goto/32 :goto_3b

    :cond_9
    goto/32 :goto_76

    nop

    :goto_7d
    if-gtz v0, :cond_a

    goto/32 :goto_b

    :cond_a
    goto/32 :goto_55

    nop
.end method

.method public abstract g(Ls0/r;)V
.end method

.method i(Ls0/r;)V
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    return-void
.end method

.method public abstract j(Ls0/r;)V
.end method

.method k(Landroid/view/ViewGroup;Z)V
    .locals 5

    goto/32 :goto_5

    nop

    :goto_0
    check-cast v2, Ljava/lang/String;

    goto/32 :goto_6

    nop

    :goto_1
    const/4 v1, 0x0

    goto/32 :goto_13

    nop

    :goto_2
    invoke-virtual {v3, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/32 :goto_5f

    nop

    :goto_3
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    goto/32 :goto_56

    nop

    :goto_4
    if-lt v0, p1, :cond_0

    goto/32 :goto_40

    :cond_0
    goto/32 :goto_35

    nop

    :goto_5
    invoke-virtual {p0, p2}, Ls0/l;->l(Z)V

    goto/32 :goto_57

    nop

    :goto_6
    iget-object v3, p0, Ls0/l;->p:Ls0/s;

    goto/32 :goto_67

    nop

    :goto_7
    invoke-virtual {v2, v0}, Ln/g;->i(I)Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_64

    nop

    :goto_8
    move p1, v1

    :goto_9
    goto/32 :goto_3e

    nop

    :goto_a
    iget-object p1, p0, Ls0/l;->E:Ln/a;

    goto/32 :goto_12

    nop

    :goto_b
    iget-object v4, p0, Ls0/l;->q:Ls0/s;

    :goto_c
    goto/32 :goto_3c

    nop

    :goto_d
    goto/16 :goto_54

    :goto_e
    goto/32 :goto_53

    nop

    :goto_f
    new-instance v3, Ls0/r;

    goto/32 :goto_46

    nop

    :goto_10
    if-nez v0, :cond_1

    goto/32 :goto_50

    :cond_1
    goto/32 :goto_1c

    nop

    :goto_11
    check-cast v0, Landroid/view/View;

    goto/32 :goto_29

    nop

    :goto_12
    if-nez p1, :cond_2

    goto/32 :goto_61

    :cond_2
    goto/32 :goto_34

    nop

    :goto_13
    if-lez v0, :cond_3

    goto/32 :goto_23

    :cond_3
    goto/32 :goto_6f

    nop

    :goto_14
    invoke-virtual {v3, v2, v0}, Ln/g;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_15
    goto/32 :goto_2e

    nop

    :goto_16
    check-cast v2, Ljava/lang/Integer;

    goto/32 :goto_3

    nop

    :goto_17
    invoke-virtual {v4, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/32 :goto_1d

    nop

    :goto_18
    if-nez p2, :cond_4

    goto/32 :goto_42

    :cond_4
    goto/32 :goto_1f

    nop

    :goto_19
    invoke-virtual {v2, v1}, Ln/g;->m(I)Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_0

    nop

    :goto_1a
    goto :goto_c

    :goto_1b
    goto/32 :goto_b

    nop

    :goto_1c
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    goto/32 :goto_4f

    nop

    :goto_1d
    invoke-virtual {p0, v3}, Ls0/l;->i(Ls0/r;)V

    goto/32 :goto_68

    nop

    :goto_1e
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    goto/32 :goto_69

    nop

    :goto_1f
    iget-object v3, p0, Ls0/l;->p:Ls0/s;

    goto/32 :goto_41

    nop

    :goto_20
    goto/16 :goto_44

    :goto_21
    goto/32 :goto_30

    nop

    :goto_22
    if-gtz v0, :cond_5

    goto/32 :goto_21

    :cond_5
    :goto_23
    goto/32 :goto_4c

    nop

    :goto_24
    goto/16 :goto_75

    :goto_25
    goto/32 :goto_74

    nop

    :goto_26
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto/32 :goto_22

    nop

    :goto_27
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_47

    nop

    :goto_28
    if-nez v0, :cond_6

    goto/32 :goto_15

    :cond_6
    goto/32 :goto_73

    nop

    :goto_29
    new-instance v2, Ls0/r;

    goto/32 :goto_45

    nop

    :goto_2a
    iget-object v0, p0, Ls0/l;->f:Ljava/util/ArrayList;

    goto/32 :goto_5d

    nop

    :goto_2b
    iget-object v4, p0, Ls0/l;->p:Ls0/s;

    goto/32 :goto_1a

    nop

    :goto_2c
    invoke-virtual {p0, v2}, Ls0/l;->j(Ls0/r;)V

    goto/32 :goto_d

    nop

    :goto_2d
    if-nez v0, :cond_7

    goto/32 :goto_44

    :cond_7
    goto/32 :goto_1e

    nop

    :goto_2e
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_60

    nop

    :goto_2f
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_3f

    nop

    :goto_30
    invoke-direct {p0, p1, p2}, Ls0/l;->h(Landroid/view/View;Z)V

    goto/32 :goto_43

    nop

    :goto_31
    iget-object v0, p0, Ls0/l;->h:Ljava/util/ArrayList;

    goto/32 :goto_2d

    nop

    :goto_32
    new-instance p2, Ljava/util/ArrayList;

    goto/32 :goto_55

    nop

    :goto_33
    if-lt p1, v0, :cond_8

    goto/32 :goto_71

    :cond_8
    goto/32 :goto_2a

    nop

    :goto_34
    invoke-virtual {p1}, Ln/g;->size()I

    move-result p1

    goto/32 :goto_32

    nop

    :goto_35
    iget-object v2, p0, Ls0/l;->E:Ln/a;

    goto/32 :goto_7

    nop

    :goto_36
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto/32 :goto_1

    nop

    :goto_37
    iget-object v2, p0, Ls0/l;->e:Ljava/util/ArrayList;

    goto/32 :goto_5e

    nop

    :goto_38
    invoke-virtual {v3, v2}, Ln/g;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_58

    nop

    :goto_39
    if-nez v2, :cond_9

    goto/32 :goto_3d

    :cond_9
    goto/32 :goto_f

    nop

    :goto_3a
    iget-object v3, v2, Ls0/r;->c:Ljava/util/ArrayList;

    goto/32 :goto_2

    nop

    :goto_3b
    iget-object v3, v3, Ls0/s;->d:Ln/a;

    goto/32 :goto_38

    nop

    :goto_3c
    invoke-static {v4, v2, v3}, Ls0/l;->e(Ls0/s;Landroid/view/View;Ls0/r;)V

    :goto_3d
    goto/32 :goto_27

    nop

    :goto_3e
    iget-object v0, p0, Ls0/l;->f:Ljava/util/ArrayList;

    goto/32 :goto_5a

    nop

    :goto_3f
    goto/16 :goto_66

    :goto_40
    goto/32 :goto_63

    nop

    :goto_41
    goto/16 :goto_6d

    :goto_42
    goto/32 :goto_6c

    nop

    :goto_43
    goto/16 :goto_71

    :goto_44
    goto/32 :goto_5b

    nop

    :goto_45
    invoke-direct {v2, v0}, Ls0/r;-><init>(Landroid/view/View;)V

    goto/32 :goto_52

    nop

    :goto_46
    invoke-direct {v3, v2}, Ls0/r;-><init>(Landroid/view/View;)V

    goto/32 :goto_4e

    nop

    :goto_47
    goto :goto_5c

    :goto_48
    goto/32 :goto_8

    nop

    :goto_49
    if-lt v0, v2, :cond_a

    goto/32 :goto_48

    :cond_a
    goto/32 :goto_37

    nop

    :goto_4a
    add-int/lit8 p1, p1, 0x1

    goto/32 :goto_70

    nop

    :goto_4b
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    goto/32 :goto_49

    nop

    :goto_4c
    iget-object v0, p0, Ls0/l;->g:Ljava/util/ArrayList;

    goto/32 :goto_10

    nop

    :goto_4d
    iget-object v2, p0, Ls0/l;->e:Ljava/util/ArrayList;

    goto/32 :goto_4b

    nop

    :goto_4e
    if-nez p2, :cond_b

    goto/32 :goto_25

    :cond_b
    goto/32 :goto_62

    nop

    :goto_4f
    if-nez v0, :cond_c

    goto/32 :goto_21

    :cond_c
    :goto_50
    goto/32 :goto_31

    nop

    :goto_51
    if-eqz p2, :cond_d

    goto/32 :goto_61

    :cond_d
    goto/32 :goto_a

    nop

    :goto_52
    if-nez p2, :cond_e

    goto/32 :goto_e

    :cond_e
    goto/32 :goto_2c

    nop

    :goto_53
    invoke-virtual {p0, v2}, Ls0/l;->g(Ls0/r;)V

    :goto_54
    goto/32 :goto_3a

    nop

    :goto_55
    invoke-direct {p2, p1}, Ljava/util/ArrayList;-><init>(I)V

    goto/32 :goto_65

    nop

    :goto_56
    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    goto/32 :goto_39

    nop

    :goto_57
    iget-object v0, p0, Ls0/l;->e:Ljava/util/ArrayList;

    goto/32 :goto_36

    nop

    :goto_58
    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/32 :goto_2f

    nop

    :goto_59
    invoke-static {v3, v0, v2}, Ls0/l;->e(Ls0/s;Landroid/view/View;Ls0/r;)V

    goto/32 :goto_4a

    nop

    :goto_5a
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto/32 :goto_33

    nop

    :goto_5b
    move v0, v1

    :goto_5c
    goto/32 :goto_4d

    nop

    :goto_5d
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_11

    nop

    :goto_5e
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_16

    nop

    :goto_5f
    invoke-virtual {p0, v2}, Ls0/l;->i(Ls0/r;)V

    goto/32 :goto_18

    nop

    :goto_60
    goto/16 :goto_40

    :goto_61
    goto/32 :goto_72

    nop

    :goto_62
    invoke-virtual {p0, v3}, Ls0/l;->j(Ls0/r;)V

    goto/32 :goto_24

    nop

    :goto_63
    if-lt v1, p1, :cond_f

    goto/32 :goto_61

    :cond_f
    goto/32 :goto_76

    nop

    :goto_64
    check-cast v2, Ljava/lang/String;

    goto/32 :goto_6a

    nop

    :goto_65
    move v0, v1

    :goto_66
    goto/32 :goto_4

    nop

    :goto_67
    iget-object v3, v3, Ls0/s;->d:Ln/a;

    goto/32 :goto_14

    nop

    :goto_68
    if-nez p2, :cond_10

    goto/32 :goto_1b

    :cond_10
    goto/32 :goto_2b

    nop

    :goto_69
    if-nez v0, :cond_11

    goto/32 :goto_21

    :cond_11
    goto/32 :goto_20

    nop

    :goto_6a
    iget-object v3, p0, Ls0/l;->p:Ls0/s;

    goto/32 :goto_3b

    nop

    :goto_6b
    check-cast v0, Landroid/view/View;

    goto/32 :goto_28

    nop

    :goto_6c
    iget-object v3, p0, Ls0/l;->q:Ls0/s;

    :goto_6d
    goto/32 :goto_59

    nop

    :goto_6e
    iget-object v4, v3, Ls0/r;->c:Ljava/util/ArrayList;

    goto/32 :goto_17

    nop

    :goto_6f
    iget-object v0, p0, Ls0/l;->f:Ljava/util/ArrayList;

    goto/32 :goto_26

    nop

    :goto_70
    goto/16 :goto_9

    :goto_71
    goto/32 :goto_51

    nop

    :goto_72
    return-void

    :goto_73
    iget-object v2, p0, Ls0/l;->E:Ln/a;

    goto/32 :goto_19

    nop

    :goto_74
    invoke-virtual {p0, v3}, Ls0/l;->g(Ls0/r;)V

    :goto_75
    goto/32 :goto_6e

    nop

    :goto_76
    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_6b

    nop
.end method

.method l(Z)V
    .locals 0

    goto/32 :goto_8

    nop

    :goto_0
    iget-object p1, p0, Ls0/l;->p:Ls0/s;

    goto/32 :goto_c

    nop

    :goto_1
    invoke-virtual {p1}, Ln/g;->clear()V

    goto/32 :goto_10

    nop

    :goto_2
    iget-object p1, p0, Ls0/l;->q:Ls0/s;

    goto/32 :goto_14

    nop

    :goto_3
    goto :goto_e

    :goto_4
    goto/32 :goto_f

    nop

    :goto_5
    invoke-virtual {p1}, Landroid/util/SparseArray;->clear()V

    goto/32 :goto_11

    nop

    :goto_6
    return-void

    :goto_7
    invoke-virtual {p1}, Ln/d;->a()V

    goto/32 :goto_6

    nop

    :goto_8
    if-nez p1, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_0

    nop

    :goto_9
    invoke-virtual {p1}, Landroid/util/SparseArray;->clear()V

    goto/32 :goto_d

    nop

    :goto_a
    iget-object p1, p1, Ls0/s;->b:Landroid/util/SparseArray;

    goto/32 :goto_5

    nop

    :goto_b
    iget-object p1, p1, Ls0/s;->a:Ln/a;

    goto/32 :goto_12

    nop

    :goto_c
    iget-object p1, p1, Ls0/s;->a:Ln/a;

    goto/32 :goto_1

    nop

    :goto_d
    iget-object p1, p0, Ls0/l;->q:Ls0/s;

    :goto_e
    goto/32 :goto_13

    nop

    :goto_f
    iget-object p1, p0, Ls0/l;->q:Ls0/s;

    goto/32 :goto_b

    nop

    :goto_10
    iget-object p1, p0, Ls0/l;->p:Ls0/s;

    goto/32 :goto_a

    nop

    :goto_11
    iget-object p1, p0, Ls0/l;->p:Ls0/s;

    goto/32 :goto_3

    nop

    :goto_12
    invoke-virtual {p1}, Ln/g;->clear()V

    goto/32 :goto_2

    nop

    :goto_13
    iget-object p1, p1, Ls0/s;->c:Ln/d;

    goto/32 :goto_7

    nop

    :goto_14
    iget-object p1, p1, Ls0/s;->b:Landroid/util/SparseArray;

    goto/32 :goto_9

    nop
.end method

.method public m()Ls0/l;
    .locals 3

    const/4 v0, 0x0

    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ls0/l;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, v1, Ls0/l;->C:Ljava/util/ArrayList;

    new-instance v2, Ls0/s;

    invoke-direct {v2}, Ls0/s;-><init>()V

    iput-object v2, v1, Ls0/l;->p:Ls0/s;

    new-instance v2, Ls0/s;

    invoke-direct {v2}, Ls0/s;-><init>()V

    iput-object v2, v1, Ls0/l;->q:Ls0/s;

    iput-object v0, v1, Ls0/l;->t:Ljava/util/ArrayList;

    iput-object v0, v1, Ls0/l;->u:Ljava/util/ArrayList;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    :catch_0
    return-object v0
.end method

.method public n(Landroid/view/ViewGroup;Ls0/r;Ls0/r;)Landroid/animation/Animator;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method protected o(Landroid/view/ViewGroup;Ls0/s;Ls0/s;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "Ls0/s;",
            "Ls0/s;",
            "Ljava/util/ArrayList<",
            "Ls0/r;",
            ">;",
            "Ljava/util/ArrayList<",
            "Ls0/r;",
            ">;)V"
        }
    .end annotation

    move-object/from16 v6, p0

    invoke-static {}, Ls0/l;->x()Ln/a;

    move-result-object v7

    new-instance v8, Landroid/util/SparseIntArray;

    invoke-direct {v8}, Landroid/util/SparseIntArray;-><init>()V

    invoke-virtual/range {p4 .. p4}, Ljava/util/ArrayList;->size()I

    move-result v9

    const/4 v11, 0x0

    :goto_0
    const-wide v0, 0x7fffffffffffffffL

    if-ge v11, v9, :cond_d

    move-object/from16 v12, p4

    invoke-virtual {v12, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ls0/r;

    move-object/from16 v13, p5

    invoke-virtual {v13, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ls0/r;

    if-eqz v0, :cond_0

    iget-object v3, v0, Ls0/r;->c:Ljava/util/ArrayList;

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const/4 v0, 0x0

    :cond_0
    if-eqz v1, :cond_1

    iget-object v3, v1, Ls0/r;->c:Ljava/util/ArrayList;

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    const/4 v1, 0x0

    :cond_1
    if-nez v0, :cond_4

    if-nez v1, :cond_4

    :cond_2
    move-object/from16 v14, p1

    :cond_3
    move-object/from16 v15, p3

    move/from16 v17, v9

    goto/16 :goto_7

    :cond_4
    if-eqz v0, :cond_6

    if-eqz v1, :cond_6

    invoke-virtual {v6, v0, v1}, Ls0/l;->F(Ls0/r;Ls0/r;)Z

    move-result v3

    if-eqz v3, :cond_5

    goto :goto_1

    :cond_5
    const/4 v3, 0x0

    goto :goto_2

    :cond_6
    :goto_1
    const/4 v3, 0x1

    :goto_2
    if-eqz v3, :cond_2

    move-object/from16 v14, p1

    invoke-virtual {v6, v14, v0, v1}, Ls0/l;->n(Landroid/view/ViewGroup;Ls0/r;Ls0/r;)Landroid/animation/Animator;

    move-result-object v3

    if-eqz v3, :cond_3

    if-eqz v1, :cond_b

    iget-object v0, v1, Ls0/r;->b:Landroid/view/View;

    invoke-virtual/range {p0 .. p0}, Ls0/l;->D()[Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_a

    array-length v4, v1

    if-lez v4, :cond_a

    new-instance v4, Ls0/r;

    invoke-direct {v4, v0}, Ls0/r;-><init>(Landroid/view/View;)V

    move-object/from16 v15, p3

    iget-object v5, v15, Ls0/s;->a:Ln/a;

    invoke-virtual {v5, v0}, Ln/g;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ls0/r;

    if-eqz v5, :cond_7

    const/4 v2, 0x0

    :goto_3
    array-length v10, v1

    if-ge v2, v10, :cond_7

    iget-object v10, v4, Ls0/r;->a:Ljava/util/Map;

    move-object/from16 v16, v3

    aget-object v3, v1, v2

    move/from16 v17, v9

    iget-object v9, v5, Ls0/r;->a:Ljava/util/Map;

    move-object/from16 v18, v5

    aget-object v5, v1, v2

    invoke-interface {v9, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v10, v3, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v3, v16

    move/from16 v9, v17

    move-object/from16 v5, v18

    goto :goto_3

    :cond_7
    move-object/from16 v16, v3

    move/from16 v17, v9

    invoke-virtual {v7}, Ln/g;->size()I

    move-result v1

    const/4 v2, 0x0

    :goto_4
    if-ge v2, v1, :cond_9

    invoke-virtual {v7, v2}, Ln/g;->i(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/animation/Animator;

    invoke-virtual {v7, v3}, Ln/g;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ls0/l$d;

    iget-object v5, v3, Ls0/l$d;->c:Ls0/r;

    if-eqz v5, :cond_8

    iget-object v5, v3, Ls0/l$d;->a:Landroid/view/View;

    if-ne v5, v0, :cond_8

    iget-object v5, v3, Ls0/l$d;->b:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Ls0/l;->u()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    iget-object v3, v3, Ls0/l$d;->c:Ls0/r;

    invoke-virtual {v3, v4}, Ls0/r;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    const/4 v2, 0x0

    goto :goto_5

    :cond_8
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_9
    move-object/from16 v2, v16

    goto :goto_5

    :cond_a
    move-object/from16 v15, p3

    move-object/from16 v16, v3

    move/from16 v17, v9

    move-object/from16 v2, v16

    const/4 v4, 0x0

    :goto_5
    move-object v1, v0

    move-object v9, v2

    move-object v5, v4

    goto :goto_6

    :cond_b
    move-object/from16 v15, p3

    move-object/from16 v16, v3

    move/from16 v17, v9

    iget-object v0, v0, Ls0/r;->b:Landroid/view/View;

    move-object v1, v0

    move-object/from16 v9, v16

    const/4 v5, 0x0

    :goto_6
    if-eqz v9, :cond_c

    new-instance v10, Ls0/l$d;

    invoke-virtual/range {p0 .. p0}, Ls0/l;->u()Ljava/lang/String;

    move-result-object v2

    invoke-static/range {p1 .. p1}, Ls0/y;->d(Landroid/view/View;)Ls0/h0;

    move-result-object v4

    move-object v0, v10

    move-object/from16 v3, p0

    invoke-direct/range {v0 .. v5}, Ls0/l$d;-><init>(Landroid/view/View;Ljava/lang/String;Ls0/l;Ls0/h0;Ls0/r;)V

    invoke-virtual {v7, v9, v10}, Ln/g;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, v6, Ls0/l;->C:Ljava/util/ArrayList;

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_c
    :goto_7
    add-int/lit8 v11, v11, 0x1

    move/from16 v9, v17

    goto/16 :goto_0

    :cond_d
    invoke-virtual {v8}, Landroid/util/SparseIntArray;->size()I

    move-result v2

    if-eqz v2, :cond_e

    const/4 v10, 0x0

    :goto_8
    invoke-virtual {v8}, Landroid/util/SparseIntArray;->size()I

    move-result v2

    if-ge v10, v2, :cond_e

    invoke-virtual {v8, v10}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v2

    iget-object v3, v6, Ls0/l;->C:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/animation/Animator;

    invoke-virtual {v8, v10}, Landroid/util/SparseIntArray;->valueAt(I)I

    move-result v3

    int-to-long v3, v3

    sub-long/2addr v3, v0

    invoke-virtual {v2}, Landroid/animation/Animator;->getStartDelay()J

    move-result-wide v11

    add-long/2addr v3, v11

    invoke-virtual {v2, v3, v4}, Landroid/animation/Animator;->setStartDelay(J)V

    add-int/lit8 v10, v10, 0x1

    goto :goto_8

    :cond_e
    return-void
.end method

.method protected p()V
    .locals 6

    iget v0, p0, Ls0/l;->y:I

    const/4 v1, 0x1

    sub-int/2addr v0, v1

    iput v0, p0, Ls0/l;->y:I

    if-nez v0, :cond_5

    iget-object v0, p0, Ls0/l;->B:Ljava/util/ArrayList;

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Ls0/l;->B:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v4, v2

    :goto_0
    if-ge v4, v3, :cond_0

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ls0/l$f;

    invoke-interface {v5, p0}, Ls0/l$f;->c(Ls0/l;)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_0
    move v0, v2

    :goto_1
    iget-object v3, p0, Ls0/l;->p:Ls0/s;

    iget-object v3, v3, Ls0/s;->c:Ln/d;

    invoke-virtual {v3}, Ln/d;->m()I

    move-result v3

    if-ge v0, v3, :cond_2

    iget-object v3, p0, Ls0/l;->p:Ls0/s;

    iget-object v3, v3, Ls0/s;->c:Ln/d;

    invoke-virtual {v3, v0}, Ln/d;->n(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    if-eqz v3, :cond_1

    invoke-static {v3, v2}, Landroidx/core/view/t;->q0(Landroid/view/View;Z)V

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move v0, v2

    :goto_2
    iget-object v3, p0, Ls0/l;->q:Ls0/s;

    iget-object v3, v3, Ls0/s;->c:Ln/d;

    invoke-virtual {v3}, Ln/d;->m()I

    move-result v3

    if-ge v0, v3, :cond_4

    iget-object v3, p0, Ls0/l;->q:Ls0/s;

    iget-object v3, v3, Ls0/s;->c:Ln/d;

    invoke-virtual {v3, v0}, Ln/d;->n(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    if-eqz v3, :cond_3

    invoke-static {v3, v2}, Landroidx/core/view/t;->q0(Landroid/view/View;Z)V

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iput-boolean v1, p0, Ls0/l;->A:Z

    :cond_5
    return-void
.end method

.method public q()J
    .locals 2

    iget-wide v0, p0, Ls0/l;->c:J

    return-wide v0
.end method

.method public r()Ls0/l$e;
    .locals 1

    iget-object v0, p0, Ls0/l;->D:Ls0/l$e;

    return-object v0
.end method

.method public s()Landroid/animation/TimeInterpolator;
    .locals 1

    iget-object v0, p0, Ls0/l;->d:Landroid/animation/TimeInterpolator;

    return-object v0
.end method

.method t(Landroid/view/View;Z)Ls0/r;
    .locals 6

    goto/32 :goto_2a

    nop

    :goto_0
    return-object v1

    :goto_1
    return-object v1

    :goto_2
    goto/32 :goto_10

    nop

    :goto_3
    check-cast v5, Ls0/r;

    goto/32 :goto_9

    nop

    :goto_4
    check-cast v1, Ls0/r;

    :goto_5
    goto/32 :goto_0

    nop

    :goto_6
    goto/16 :goto_20

    :goto_7
    goto/32 :goto_1a

    nop

    :goto_8
    invoke-virtual {v0, p1, p2}, Ls0/l;->t(Landroid/view/View;Z)Ls0/r;

    move-result-object p1

    goto/32 :goto_18

    nop

    :goto_9
    if-eqz v5, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_1

    nop

    :goto_a
    const/4 v1, 0x0

    goto/32 :goto_1e

    nop

    :goto_b
    const/4 v3, -0x1

    goto/32 :goto_1f

    nop

    :goto_c
    if-nez p2, :cond_1

    goto/32 :goto_12

    :cond_1
    goto/32 :goto_14

    nop

    :goto_d
    add-int/lit8 v4, v4, 0x1

    goto/32 :goto_6

    nop

    :goto_e
    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    goto/32 :goto_27

    nop

    :goto_f
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    goto/32 :goto_b

    nop

    :goto_10
    iget-object v5, v5, Ls0/r;->b:Landroid/view/View;

    goto/32 :goto_1b

    nop

    :goto_11
    goto :goto_1d

    :goto_12
    goto/32 :goto_1c

    nop

    :goto_13
    if-lt v4, v2, :cond_2

    goto/32 :goto_7

    :cond_2
    goto/32 :goto_17

    nop

    :goto_14
    iget-object v0, p0, Ls0/l;->t:Ljava/util/ArrayList;

    goto/32 :goto_11

    nop

    :goto_15
    return-object v1

    :goto_16
    goto/32 :goto_f

    nop

    :goto_17
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    goto/32 :goto_3

    nop

    :goto_18
    return-object p1

    :goto_19
    goto/32 :goto_c

    nop

    :goto_1a
    if-gez v3, :cond_3

    goto/32 :goto_5

    :cond_3
    goto/32 :goto_22

    nop

    :goto_1b
    if-eq v5, p1, :cond_4

    goto/32 :goto_29

    :cond_4
    goto/32 :goto_26

    nop

    :goto_1c
    iget-object v0, p0, Ls0/l;->u:Ljava/util/ArrayList;

    :goto_1d
    goto/32 :goto_a

    nop

    :goto_1e
    if-eqz v0, :cond_5

    goto/32 :goto_16

    :cond_5
    goto/32 :goto_15

    nop

    :goto_1f
    const/4 v4, 0x0

    :goto_20
    goto/32 :goto_13

    nop

    :goto_21
    if-nez v0, :cond_6

    goto/32 :goto_19

    :cond_6
    goto/32 :goto_8

    nop

    :goto_22
    if-nez p2, :cond_7

    goto/32 :goto_24

    :cond_7
    goto/32 :goto_25

    nop

    :goto_23
    goto :goto_2c

    :goto_24
    goto/32 :goto_2b

    nop

    :goto_25
    iget-object p1, p0, Ls0/l;->u:Ljava/util/ArrayList;

    goto/32 :goto_23

    nop

    :goto_26
    move v3, v4

    goto/32 :goto_28

    nop

    :goto_27
    move-object v1, p1

    goto/32 :goto_4

    nop

    :goto_28
    goto/16 :goto_7

    :goto_29
    goto/32 :goto_d

    nop

    :goto_2a
    iget-object v0, p0, Ls0/l;->r:Ls0/p;

    goto/32 :goto_21

    nop

    :goto_2b
    iget-object p1, p0, Ls0/l;->t:Ljava/util/ArrayList;

    :goto_2c
    goto/32 :goto_e

    nop
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    const-string v0, ""

    invoke-virtual {p0, v0}, Ls0/l;->f0(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public u()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ls0/l;->a:Ljava/lang/String;

    return-object v0
.end method

.method public v()Ls0/g;
    .locals 1

    iget-object v0, p0, Ls0/l;->F:Ls0/g;

    return-object v0
.end method

.method public w()Ls0/o;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public y()J
    .locals 2

    iget-wide v0, p0, Ls0/l;->b:J

    return-wide v0
.end method

.method public z()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Ls0/l;->e:Ljava/util/ArrayList;

    return-object v0
.end method
