.class public Ls0/p;
.super Ls0/l;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ls0/p$b;
    }
.end annotation


# instance fields
.field private J:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ls0/l;",
            ">;"
        }
    .end annotation
.end field

.field private K:Z

.field L:I

.field M:Z

.field private N:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ls0/l;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ls0/p;->J:Ljava/util/ArrayList;

    const/4 v0, 0x1

    iput-boolean v0, p0, Ls0/p;->K:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Ls0/p;->M:Z

    iput v0, p0, Ls0/p;->N:I

    return-void
.end method

.method private j0(Ls0/l;)V
    .locals 1

    iget-object v0, p0, Ls0/p;->J:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iput-object p0, p1, Ls0/l;->r:Ls0/p;

    return-void
.end method

.method private s0()V
    .locals 3

    new-instance v0, Ls0/p$b;

    invoke-direct {v0, p0}, Ls0/p$b;-><init>(Ls0/p;)V

    iget-object v1, p0, Ls0/p;->J:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ls0/l;

    invoke-virtual {v2, v0}, Ls0/l;->a(Ls0/l$f;)Ls0/l;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Ls0/p;->J:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, p0, Ls0/p;->L:I

    return-void
.end method


# virtual methods
.method public R(Landroid/view/View;)V
    .locals 3

    invoke-super {p0, p1}, Ls0/l;->R(Landroid/view/View;)V

    iget-object v0, p0, Ls0/p;->J:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    iget-object v2, p0, Ls0/p;->J:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ls0/l;

    invoke-virtual {v2, p1}, Ls0/l;->R(Landroid/view/View;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public bridge synthetic T(Ls0/l$f;)Ls0/l;
    .locals 0

    invoke-virtual {p0, p1}, Ls0/p;->m0(Ls0/l$f;)Ls0/p;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic U(Landroid/view/View;)Ls0/l;
    .locals 0

    invoke-virtual {p0, p1}, Ls0/p;->n0(Landroid/view/View;)Ls0/p;

    move-result-object p1

    return-object p1
.end method

.method public V(Landroid/view/View;)V
    .locals 3

    invoke-super {p0, p1}, Ls0/l;->V(Landroid/view/View;)V

    iget-object v0, p0, Ls0/p;->J:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    iget-object v2, p0, Ls0/p;->J:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ls0/l;

    invoke-virtual {v2, p1}, Ls0/l;->V(Landroid/view/View;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected X()V
    .locals 4

    iget-object v0, p0, Ls0/p;->J:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ls0/l;->e0()V

    invoke-virtual {p0}, Ls0/l;->p()V

    return-void

    :cond_0
    invoke-direct {p0}, Ls0/p;->s0()V

    iget-boolean v0, p0, Ls0/p;->K:Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Ls0/p;->J:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Ls0/p;->J:Ljava/util/ArrayList;

    add-int/lit8 v2, v0, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ls0/l;

    iget-object v2, p0, Ls0/p;->J:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ls0/l;

    new-instance v3, Ls0/p$a;

    invoke-direct {v3, p0, v2}, Ls0/p$a;-><init>(Ls0/p;Ls0/l;)V

    invoke-virtual {v1, v3}, Ls0/l;->a(Ls0/l$f;)Ls0/l;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Ls0/p;->J:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ls0/l;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ls0/l;->X()V

    goto :goto_2

    :cond_2
    iget-object v0, p0, Ls0/p;->J:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ls0/l;

    invoke-virtual {v1}, Ls0/l;->X()V

    goto :goto_1

    :cond_3
    :goto_2
    return-void
.end method

.method public bridge synthetic Y(J)Ls0/l;
    .locals 0

    invoke-virtual {p0, p1, p2}, Ls0/p;->o0(J)Ls0/p;

    move-result-object p1

    return-object p1
.end method

.method public Z(Ls0/l$e;)V
    .locals 3

    invoke-super {p0, p1}, Ls0/l;->Z(Ls0/l$e;)V

    iget v0, p0, Ls0/p;->N:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Ls0/p;->N:I

    iget-object v0, p0, Ls0/p;->J:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    iget-object v2, p0, Ls0/p;->J:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ls0/l;

    invoke-virtual {v2, p1}, Ls0/l;->Z(Ls0/l$e;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public bridge synthetic a(Ls0/l$f;)Ls0/l;
    .locals 0

    invoke-virtual {p0, p1}, Ls0/p;->g0(Ls0/l$f;)Ls0/p;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic a0(Landroid/animation/TimeInterpolator;)Ls0/l;
    .locals 0

    invoke-virtual {p0, p1}, Ls0/p;->p0(Landroid/animation/TimeInterpolator;)Ls0/p;

    move-result-object p1

    return-object p1
.end method

.method public b0(Ls0/g;)V
    .locals 2

    invoke-super {p0, p1}, Ls0/l;->b0(Ls0/g;)V

    iget v0, p0, Ls0/p;->N:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Ls0/p;->N:I

    iget-object v0, p0, Ls0/p;->J:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ls0/p;->J:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Ls0/p;->J:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ls0/l;

    invoke-virtual {v1, p1}, Ls0/l;->b0(Ls0/g;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public bridge synthetic c(Landroid/view/View;)Ls0/l;
    .locals 0

    invoke-virtual {p0, p1}, Ls0/p;->h0(Landroid/view/View;)Ls0/p;

    move-result-object p1

    return-object p1
.end method

.method public c0(Ls0/o;)V
    .locals 3

    invoke-super {p0, p1}, Ls0/l;->c0(Ls0/o;)V

    iget v0, p0, Ls0/p;->N:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Ls0/p;->N:I

    iget-object v0, p0, Ls0/p;->J:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    iget-object v2, p0, Ls0/p;->J:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ls0/l;

    invoke-virtual {v2, p1}, Ls0/l;->c0(Ls0/o;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Ls0/p;->m()Ls0/l;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic d0(J)Ls0/l;
    .locals 0

    invoke-virtual {p0, p1, p2}, Ls0/p;->r0(J)Ls0/p;

    move-result-object p1

    return-object p1
.end method

.method f0(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    goto/32 :goto_b

    nop

    :goto_0
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/32 :goto_f

    nop

    :goto_1
    iget-object v2, p0, Ls0/p;->J:Ljava/util/ArrayList;

    goto/32 :goto_d

    nop

    :goto_2
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_4

    nop

    :goto_3
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_13

    nop

    :goto_4
    iget-object v0, p0, Ls0/p;->J:Ljava/util/ArrayList;

    goto/32 :goto_19

    nop

    :goto_5
    new-instance v2, Ljava/lang/StringBuilder;

    goto/32 :goto_1a

    nop

    :goto_6
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_15

    nop

    :goto_7
    const/4 v1, 0x0

    :goto_8
    goto/32 :goto_1

    nop

    :goto_9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_0

    nop

    :goto_a
    const-string v0, "\n"

    goto/32 :goto_2

    nop

    :goto_b
    invoke-super {p0, p1}, Ls0/l;->f0(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_7

    nop

    :goto_c
    return-object v0

    :goto_d
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    goto/32 :goto_16

    nop

    :goto_e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_3

    nop

    :goto_f
    invoke-virtual {v0, v3}, Ls0/l;->f0(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_12

    nop

    :goto_10
    goto :goto_8

    :goto_11
    goto/32 :goto_c

    nop

    :goto_12
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_6

    nop

    :goto_13
    const-string v4, "  "

    goto/32 :goto_9

    nop

    :goto_14
    check-cast v0, Ls0/l;

    goto/32 :goto_17

    nop

    :goto_15
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_10

    nop

    :goto_16
    if-lt v1, v2, :cond_0

    goto/32 :goto_11

    :cond_0
    goto/32 :goto_5

    nop

    :goto_17
    new-instance v3, Ljava/lang/StringBuilder;

    goto/32 :goto_e

    nop

    :goto_18
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_a

    nop

    :goto_19
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_14

    nop

    :goto_1a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_18

    nop
.end method

.method public g(Ls0/r;)V
    .locals 3

    iget-object v0, p1, Ls0/r;->b:Landroid/view/View;

    invoke-virtual {p0, v0}, Ls0/l;->G(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Ls0/p;->J:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ls0/l;

    iget-object v2, p1, Ls0/r;->b:Landroid/view/View;

    invoke-virtual {v1, v2}, Ls0/l;->G(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, p1}, Ls0/l;->g(Ls0/r;)V

    iget-object v2, p1, Ls0/r;->c:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-void
.end method

.method public g0(Ls0/l$f;)Ls0/p;
    .locals 0

    invoke-super {p0, p1}, Ls0/l;->a(Ls0/l$f;)Ls0/l;

    move-result-object p1

    check-cast p1, Ls0/p;

    return-object p1
.end method

.method public h0(Landroid/view/View;)Ls0/p;
    .locals 2

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ls0/p;->J:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Ls0/p;->J:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ls0/l;

    invoke-virtual {v1, p1}, Ls0/l;->c(Landroid/view/View;)Ls0/l;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-super {p0, p1}, Ls0/l;->c(Landroid/view/View;)Ls0/l;

    move-result-object p1

    check-cast p1, Ls0/p;

    return-object p1
.end method

.method i(Ls0/r;)V
    .locals 3

    goto/32 :goto_6

    nop

    :goto_0
    iget-object v0, p0, Ls0/p;->J:Ljava/util/ArrayList;

    goto/32 :goto_d

    nop

    :goto_1
    iget-object v2, p0, Ls0/p;->J:Ljava/util/ArrayList;

    goto/32 :goto_5

    nop

    :goto_2
    return-void

    :goto_3
    goto :goto_9

    :goto_4
    goto/32 :goto_2

    nop

    :goto_5
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_7

    nop

    :goto_6
    invoke-super {p0, p1}, Ls0/l;->i(Ls0/r;)V

    goto/32 :goto_0

    nop

    :goto_7
    check-cast v2, Ls0/l;

    goto/32 :goto_b

    nop

    :goto_8
    const/4 v1, 0x0

    :goto_9
    goto/32 :goto_c

    nop

    :goto_a
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_3

    nop

    :goto_b
    invoke-virtual {v2, p1}, Ls0/l;->i(Ls0/r;)V

    goto/32 :goto_a

    nop

    :goto_c
    if-lt v1, v0, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_1

    nop

    :goto_d
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto/32 :goto_8

    nop
.end method

.method public i0(Ls0/l;)Ls0/p;
    .locals 4

    invoke-direct {p0, p1}, Ls0/p;->j0(Ls0/l;)V

    iget-wide v0, p0, Ls0/l;->c:J

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-ltz v2, :cond_0

    invoke-virtual {p1, v0, v1}, Ls0/l;->Y(J)Ls0/l;

    :cond_0
    iget v0, p0, Ls0/p;->N:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Ls0/l;->s()Landroid/animation/TimeInterpolator;

    move-result-object v0

    invoke-virtual {p1, v0}, Ls0/l;->a0(Landroid/animation/TimeInterpolator;)Ls0/l;

    :cond_1
    iget v0, p0, Ls0/p;->N:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Ls0/l;->w()Ls0/o;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ls0/l;->c0(Ls0/o;)V

    :cond_2
    iget v0, p0, Ls0/p;->N:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Ls0/l;->v()Ls0/g;

    move-result-object v0

    invoke-virtual {p1, v0}, Ls0/l;->b0(Ls0/g;)V

    :cond_3
    iget v0, p0, Ls0/p;->N:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Ls0/l;->r()Ls0/l$e;

    move-result-object v0

    invoke-virtual {p1, v0}, Ls0/l;->Z(Ls0/l$e;)V

    :cond_4
    return-object p0
.end method

.method public j(Ls0/r;)V
    .locals 3

    iget-object v0, p1, Ls0/r;->b:Landroid/view/View;

    invoke-virtual {p0, v0}, Ls0/l;->G(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Ls0/p;->J:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ls0/l;

    iget-object v2, p1, Ls0/r;->b:Landroid/view/View;

    invoke-virtual {v1, v2}, Ls0/l;->G(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, p1}, Ls0/l;->j(Ls0/r;)V

    iget-object v2, p1, Ls0/r;->c:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-void
.end method

.method public k0(I)Ls0/l;
    .locals 1

    if-ltz p1, :cond_1

    iget-object v0, p0, Ls0/p;->J:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Ls0/p;->J:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ls0/l;

    return-object p1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public l0()I
    .locals 1

    iget-object v0, p0, Ls0/p;->J:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public m()Ls0/l;
    .locals 4

    invoke-super {p0}, Ls0/l;->m()Ls0/l;

    move-result-object v0

    check-cast v0, Ls0/p;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Ls0/p;->J:Ljava/util/ArrayList;

    iget-object v1, p0, Ls0/p;->J:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    iget-object v3, p0, Ls0/p;->J:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ls0/l;

    invoke-virtual {v3}, Ls0/l;->m()Ls0/l;

    move-result-object v3

    invoke-direct {v0, v3}, Ls0/p;->j0(Ls0/l;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public m0(Ls0/l$f;)Ls0/p;
    .locals 0

    invoke-super {p0, p1}, Ls0/l;->T(Ls0/l$f;)Ls0/l;

    move-result-object p1

    check-cast p1, Ls0/p;

    return-object p1
.end method

.method public n0(Landroid/view/View;)Ls0/p;
    .locals 2

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ls0/p;->J:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Ls0/p;->J:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ls0/l;

    invoke-virtual {v1, p1}, Ls0/l;->U(Landroid/view/View;)Ls0/l;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-super {p0, p1}, Ls0/l;->U(Landroid/view/View;)Ls0/l;

    move-result-object p1

    check-cast p1, Ls0/p;

    return-object p1
.end method

.method protected o(Landroid/view/ViewGroup;Ls0/s;Ls0/s;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "Ls0/s;",
            "Ls0/s;",
            "Ljava/util/ArrayList<",
            "Ls0/r;",
            ">;",
            "Ljava/util/ArrayList<",
            "Ls0/r;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    invoke-virtual {p0}, Ls0/l;->y()J

    move-result-wide v1

    iget-object v3, v0, Ls0/p;->J:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v3, :cond_3

    iget-object v5, v0, Ls0/p;->J:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    move-object v6, v5

    check-cast v6, Ls0/l;

    const-wide/16 v7, 0x0

    cmp-long v5, v1, v7

    if-lez v5, :cond_2

    iget-boolean v5, v0, Ls0/p;->K:Z

    if-nez v5, :cond_0

    if-nez v4, :cond_2

    :cond_0
    invoke-virtual {v6}, Ls0/l;->y()J

    move-result-wide v9

    cmp-long v5, v9, v7

    if-lez v5, :cond_1

    add-long/2addr v9, v1

    invoke-virtual {v6, v9, v10}, Ls0/l;->d0(J)Ls0/l;

    goto :goto_1

    :cond_1
    invoke-virtual {v6, v1, v2}, Ls0/l;->d0(J)Ls0/l;

    :cond_2
    :goto_1
    move-object v7, p1

    move-object v8, p2

    move-object v9, p3

    move-object/from16 v10, p4

    move-object/from16 v11, p5

    invoke-virtual/range {v6 .. v11}, Ls0/l;->o(Landroid/view/ViewGroup;Ls0/s;Ls0/s;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_3
    return-void
.end method

.method public o0(J)Ls0/p;
    .locals 4

    invoke-super {p0, p1, p2}, Ls0/l;->Y(J)Ls0/l;

    iget-wide v0, p0, Ls0/l;->c:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    iget-object v0, p0, Ls0/p;->J:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    iget-object v2, p0, Ls0/p;->J:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ls0/l;

    invoke-virtual {v2, p1, p2}, Ls0/l;->Y(J)Ls0/l;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object p0
.end method

.method public p0(Landroid/animation/TimeInterpolator;)Ls0/p;
    .locals 3

    iget v0, p0, Ls0/p;->N:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Ls0/p;->N:I

    iget-object v0, p0, Ls0/p;->J:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    iget-object v2, p0, Ls0/p;->J:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ls0/l;

    invoke-virtual {v2, p1}, Ls0/l;->a0(Landroid/animation/TimeInterpolator;)Ls0/l;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    invoke-super {p0, p1}, Ls0/l;->a0(Landroid/animation/TimeInterpolator;)Ls0/l;

    move-result-object p1

    check-cast p1, Ls0/p;

    return-object p1
.end method

.method public q0(I)Ls0/p;
    .locals 3

    const/4 v0, 0x1

    if-eqz p1, :cond_1

    if-ne p1, v0, :cond_0

    const/4 p1, 0x0

    iput-boolean p1, p0, Ls0/p;->K:Z

    goto :goto_0

    :cond_0
    new-instance v0, Landroid/util/AndroidRuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid parameter for TransitionSet ordering: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Landroid/util/AndroidRuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iput-boolean v0, p0, Ls0/p;->K:Z

    :goto_0
    return-object p0
.end method

.method public r0(J)Ls0/p;
    .locals 0

    invoke-super {p0, p1, p2}, Ls0/l;->d0(J)Ls0/l;

    move-result-object p1

    check-cast p1, Ls0/p;

    return-object p1
.end method
