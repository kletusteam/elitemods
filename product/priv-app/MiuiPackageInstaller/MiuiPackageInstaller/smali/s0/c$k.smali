.class Ls0/c$k;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ls0/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "k"
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:I

.field private e:Landroid/view/View;

.field private f:I

.field private g:I


# direct methods
.method constructor <init>(Landroid/view/View;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Ls0/c$k;->e:Landroid/view/View;

    return-void
.end method

.method private b()V
    .locals 5

    iget-object v0, p0, Ls0/c$k;->e:Landroid/view/View;

    iget v1, p0, Ls0/c$k;->a:I

    iget v2, p0, Ls0/c$k;->b:I

    iget v3, p0, Ls0/c$k;->c:I

    iget v4, p0, Ls0/c$k;->d:I

    invoke-static {v0, v1, v2, v3, v4}, Ls0/y;->f(Landroid/view/View;IIII)V

    const/4 v0, 0x0

    iput v0, p0, Ls0/c$k;->f:I

    iput v0, p0, Ls0/c$k;->g:I

    return-void
.end method


# virtual methods
.method a(Landroid/graphics/PointF;)V
    .locals 1

    goto/32 :goto_3

    nop

    :goto_0
    iget v0, p0, Ls0/c$k;->f:I

    goto/32 :goto_d

    nop

    :goto_1
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    goto/32 :goto_b

    nop

    :goto_2
    invoke-static {p1}, Ljava/lang/Math;->round(F)I

    move-result p1

    goto/32 :goto_4

    nop

    :goto_3
    iget v0, p1, Landroid/graphics/PointF;->x:F

    goto/32 :goto_1

    nop

    :goto_4
    iput p1, p0, Ls0/c$k;->d:I

    goto/32 :goto_a

    nop

    :goto_5
    invoke-direct {p0}, Ls0/c$k;->b()V

    :goto_6
    goto/32 :goto_c

    nop

    :goto_7
    add-int/lit8 p1, p1, 0x1

    goto/32 :goto_8

    nop

    :goto_8
    iput p1, p0, Ls0/c$k;->g:I

    goto/32 :goto_0

    nop

    :goto_9
    iget p1, p1, Landroid/graphics/PointF;->y:F

    goto/32 :goto_2

    nop

    :goto_a
    iget p1, p0, Ls0/c$k;->g:I

    goto/32 :goto_7

    nop

    :goto_b
    iput v0, p0, Ls0/c$k;->c:I

    goto/32 :goto_9

    nop

    :goto_c
    return-void

    :goto_d
    if-eq v0, p1, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_5

    nop
.end method

.method c(Landroid/graphics/PointF;)V
    .locals 1

    goto/32 :goto_9

    nop

    :goto_0
    return-void

    :goto_1
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    goto/32 :goto_2

    nop

    :goto_2
    iput v0, p0, Ls0/c$k;->a:I

    goto/32 :goto_5

    nop

    :goto_3
    if-eq p1, v0, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_6

    nop

    :goto_4
    iput p1, p0, Ls0/c$k;->f:I

    goto/32 :goto_a

    nop

    :goto_5
    iget p1, p1, Landroid/graphics/PointF;->y:F

    goto/32 :goto_b

    nop

    :goto_6
    invoke-direct {p0}, Ls0/c$k;->b()V

    :goto_7
    goto/32 :goto_0

    nop

    :goto_8
    iput p1, p0, Ls0/c$k;->b:I

    goto/32 :goto_c

    nop

    :goto_9
    iget v0, p1, Landroid/graphics/PointF;->x:F

    goto/32 :goto_1

    nop

    :goto_a
    iget v0, p0, Ls0/c$k;->g:I

    goto/32 :goto_3

    nop

    :goto_b
    invoke-static {p1}, Ljava/lang/Math;->round(F)I

    move-result p1

    goto/32 :goto_8

    nop

    :goto_c
    iget p1, p0, Ls0/c$k;->f:I

    goto/32 :goto_d

    nop

    :goto_d
    add-int/lit8 p1, p1, 0x1

    goto/32 :goto_4

    nop
.end method
