.class final Lkotlinx/coroutines/internal/s;
.super Lv8/v1;

# interfaces
.implements Lv8/n0;


# instance fields
.field private final b:Ljava/lang/Throwable;

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/Throwable;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Lv8/v1;-><init>()V

    iput-object p1, p0, Lkotlinx/coroutines/internal/s;->b:Ljava/lang/Throwable;

    iput-object p2, p0, Lkotlinx/coroutines/internal/s;->c:Ljava/lang/String;

    return-void
.end method

.method private final a0()Ljava/lang/Void;
    .locals 3

    iget-object v0, p0, Lkotlinx/coroutines/internal/s;->b:Ljava/lang/Throwable;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lkotlinx/coroutines/internal/s;->c:Ljava/lang/String;

    const-string v1, ""

    if-eqz v0, :cond_1

    const-string v2, ". "

    invoke-static {v2, v0}, Lm8/i;->l(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    move-object v1, v0

    :cond_1
    :goto_0
    const-string v0, "Module with the Main dispatcher had failed to initialize"

    invoke-static {v0, v1}, Lm8/i;->l(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/IllegalStateException;

    iget-object v2, p0, Lkotlinx/coroutines/internal/s;->b:Ljava/lang/Throwable;

    invoke-direct {v1, v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :cond_2
    invoke-static {}, Lkotlinx/coroutines/internal/r;->d()Ljava/lang/Void;

    new-instance v0, La8/d;

    invoke-direct {v0}, La8/d;-><init>()V

    throw v0
.end method


# virtual methods
.method public bridge synthetic D(JLv8/j;)V
    .locals 0

    invoke-virtual {p0, p1, p2, p3}, Lkotlinx/coroutines/internal/s;->b0(JLv8/j;)Ljava/lang/Void;

    return-void
.end method

.method public bridge synthetic U(Ld8/g;Ljava/lang/Runnable;)V
    .locals 0

    invoke-virtual {p0, p1, p2}, Lkotlinx/coroutines/internal/s;->Z(Ld8/g;Ljava/lang/Runnable;)Ljava/lang/Void;

    return-void
.end method

.method public V(Ld8/g;)Z
    .locals 0

    invoke-direct {p0}, Lkotlinx/coroutines/internal/s;->a0()Ljava/lang/Void;

    new-instance p1, La8/d;

    invoke-direct {p1}, La8/d;-><init>()V

    throw p1
.end method

.method public X()Lv8/v1;
    .locals 0

    return-object p0
.end method

.method public Z(Ld8/g;Ljava/lang/Runnable;)Ljava/lang/Void;
    .locals 0

    invoke-direct {p0}, Lkotlinx/coroutines/internal/s;->a0()Ljava/lang/Void;

    new-instance p1, La8/d;

    invoke-direct {p1}, La8/d;-><init>()V

    throw p1
.end method

.method public b0(JLv8/j;)Ljava/lang/Void;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lv8/j<",
            "-",
            "La8/v;",
            ">;)",
            "Ljava/lang/Void;"
        }
    .end annotation

    invoke-direct {p0}, Lkotlinx/coroutines/internal/s;->a0()Ljava/lang/Void;

    new-instance p1, La8/d;

    invoke-direct {p1}, La8/d;-><init>()V

    throw p1
.end method

.method public s(JLjava/lang/Runnable;Ld8/g;)Lv8/u0;
    .locals 0

    invoke-direct {p0}, Lkotlinx/coroutines/internal/s;->a0()Ljava/lang/Void;

    new-instance p1, La8/d;

    invoke-direct {p1}, La8/d;-><init>()V

    throw p1
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Dispatchers.Main[missing"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lkotlinx/coroutines/internal/s;->b:Ljava/lang/Throwable;

    if-eqz v1, :cond_0

    const-string v2, ", cause="

    invoke-static {v2, v1}, Lm8/i;->l(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_0
    const-string v1, ""

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x5d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
