.class public Lkotlinx/coroutines/internal/w;
.super Lv8/a;

# interfaces
.implements Lf8/e;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lv8/a<",
        "TT;>;",
        "Lf8/e;"
    }
.end annotation


# instance fields
.field public final c:Ld8/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ld8/d<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ld8/g;Ld8/d;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ld8/g;",
            "Ld8/d<",
            "-TT;>;)V"
        }
    .end annotation

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0, v0}, Lv8/a;-><init>(Ld8/g;ZZ)V

    iput-object p2, p0, Lkotlinx/coroutines/internal/w;->c:Ld8/d;

    return-void
.end method


# virtual methods
.method protected final Z()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final e()Lf8/e;
    .locals 2

    iget-object v0, p0, Lkotlinx/coroutines/internal/w;->c:Ld8/d;

    instance-of v1, v0, Lf8/e;

    if-eqz v1, :cond_0

    check-cast v0, Lf8/e;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method protected o(Ljava/lang/Object;)V
    .locals 3

    iget-object v0, p0, Lkotlinx/coroutines/internal/w;->c:Ld8/d;

    invoke-static {v0}, Le8/b;->b(Ld8/d;)Ld8/d;

    move-result-object v0

    iget-object v1, p0, Lkotlinx/coroutines/internal/w;->c:Ld8/d;

    invoke-static {p1, v1}, Lv8/x;->a(Ljava/lang/Object;Ld8/d;)Ljava/lang/Object;

    move-result-object p1

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-static {v0, p1, v1, v2, v1}, Lkotlinx/coroutines/internal/f;->c(Ld8/d;Ljava/lang/Object;Ll8/l;ILjava/lang/Object;)V

    return-void
.end method

.method protected y0(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lkotlinx/coroutines/internal/w;->c:Ld8/d;

    invoke-static {p1, v0}, Lv8/x;->a(Ljava/lang/Object;Ld8/d;)Ljava/lang/Object;

    move-result-object p1

    invoke-interface {v0, p1}, Ld8/d;->i(Ljava/lang/Object;)V

    return-void
.end method
