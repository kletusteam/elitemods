.class public final Lkotlinx/coroutines/internal/b0;
.super Ljava/lang/Object;


# static fields
.field public static final a:Lkotlinx/coroutines/internal/x;

.field private static final b:Ll8/p;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ll8/p<",
            "Ljava/lang/Object;",
            "Ld8/g$b;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Ll8/p;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ll8/p<",
            "Lv8/e2<",
            "*>;",
            "Ld8/g$b;",
            "Lv8/e2<",
            "*>;>;"
        }
    .end annotation
.end field

.field private static final d:Ll8/p;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ll8/p<",
            "Lkotlinx/coroutines/internal/e0;",
            "Ld8/g$b;",
            "Lkotlinx/coroutines/internal/e0;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lkotlinx/coroutines/internal/x;

    const-string v1, "NO_THREAD_ELEMENTS"

    invoke-direct {v0, v1}, Lkotlinx/coroutines/internal/x;-><init>(Ljava/lang/String;)V

    sput-object v0, Lkotlinx/coroutines/internal/b0;->a:Lkotlinx/coroutines/internal/x;

    sget-object v0, Lkotlinx/coroutines/internal/b0$a;->b:Lkotlinx/coroutines/internal/b0$a;

    sput-object v0, Lkotlinx/coroutines/internal/b0;->b:Ll8/p;

    sget-object v0, Lkotlinx/coroutines/internal/b0$b;->b:Lkotlinx/coroutines/internal/b0$b;

    sput-object v0, Lkotlinx/coroutines/internal/b0;->c:Ll8/p;

    sget-object v0, Lkotlinx/coroutines/internal/b0$c;->b:Lkotlinx/coroutines/internal/b0$c;

    sput-object v0, Lkotlinx/coroutines/internal/b0;->d:Ll8/p;

    return-void
.end method

.method public static final a(Ld8/g;Ljava/lang/Object;)V
    .locals 2

    sget-object v0, Lkotlinx/coroutines/internal/b0;->a:Lkotlinx/coroutines/internal/x;

    if-ne p1, v0, :cond_0

    return-void

    :cond_0
    instance-of v0, p1, Lkotlinx/coroutines/internal/e0;

    if-eqz v0, :cond_1

    check-cast p1, Lkotlinx/coroutines/internal/e0;

    invoke-virtual {p1, p0}, Lkotlinx/coroutines/internal/e0;->b(Ld8/g;)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    sget-object v1, Lkotlinx/coroutines/internal/b0;->c:Ll8/p;

    invoke-interface {p0, v0, v1}, Ld8/g;->fold(Ljava/lang/Object;Ll8/p;)Ljava/lang/Object;

    move-result-object v0

    const-string v1, "null cannot be cast to non-null type kotlinx.coroutines.ThreadContextElement<kotlin.Any?>"

    invoke-static {v0, v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    check-cast v0, Lv8/e2;

    invoke-interface {v0, p0, p1}, Lv8/e2;->m(Ld8/g;Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method public static final b(Ld8/g;)Ljava/lang/Object;
    .locals 2

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v1, Lkotlinx/coroutines/internal/b0;->b:Ll8/p;

    invoke-interface {p0, v0, v1}, Ld8/g;->fold(Ljava/lang/Object;Ll8/p;)Ljava/lang/Object;

    move-result-object p0

    invoke-static {p0}, Lm8/i;->c(Ljava/lang/Object;)V

    return-object p0
.end method

.method public static final c(Ld8/g;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    if-nez p1, :cond_0

    invoke-static {p0}, Lkotlinx/coroutines/internal/b0;->b(Ld8/g;)Ljava/lang/Object;

    move-result-object p1

    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    if-ne p1, v0, :cond_1

    sget-object p0, Lkotlinx/coroutines/internal/b0;->a:Lkotlinx/coroutines/internal/x;

    goto :goto_0

    :cond_1
    instance-of v0, p1, Ljava/lang/Integer;

    if-eqz v0, :cond_2

    new-instance v0, Lkotlinx/coroutines/internal/e0;

    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    invoke-direct {v0, p0, p1}, Lkotlinx/coroutines/internal/e0;-><init>(Ld8/g;I)V

    sget-object p1, Lkotlinx/coroutines/internal/b0;->d:Ll8/p;

    invoke-interface {p0, v0, p1}, Ld8/g;->fold(Ljava/lang/Object;Ll8/p;)Ljava/lang/Object;

    move-result-object p0

    goto :goto_0

    :cond_2
    check-cast p1, Lv8/e2;

    invoke-interface {p1, p0}, Lv8/e2;->g(Ld8/g;)Ljava/lang/Object;

    move-result-object p0

    :goto_0
    return-object p0
.end method
