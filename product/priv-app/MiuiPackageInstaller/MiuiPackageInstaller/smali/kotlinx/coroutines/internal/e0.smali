.class final Lkotlinx/coroutines/internal/e0;
.super Ljava/lang/Object;


# instance fields
.field public final a:Ld8/g;

.field private final b:[Ljava/lang/Object;

.field private final c:[Lv8/e2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lv8/e2<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private d:I


# direct methods
.method public constructor <init>(Ld8/g;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lkotlinx/coroutines/internal/e0;->a:Ld8/g;

    new-array p1, p2, [Ljava/lang/Object;

    iput-object p1, p0, Lkotlinx/coroutines/internal/e0;->b:[Ljava/lang/Object;

    new-array p1, p2, [Lv8/e2;

    iput-object p1, p0, Lkotlinx/coroutines/internal/e0;->c:[Lv8/e2;

    return-void
.end method


# virtual methods
.method public final a(Lv8/e2;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lv8/e2<",
            "*>;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    iget-object v0, p0, Lkotlinx/coroutines/internal/e0;->b:[Ljava/lang/Object;

    iget v1, p0, Lkotlinx/coroutines/internal/e0;->d:I

    aput-object p2, v0, v1

    iget-object p2, p0, Lkotlinx/coroutines/internal/e0;->c:[Lv8/e2;

    add-int/lit8 v0, v1, 0x1

    iput v0, p0, Lkotlinx/coroutines/internal/e0;->d:I

    aput-object p1, p2, v1

    return-void
.end method

.method public final b(Ld8/g;)V
    .locals 4

    iget-object v0, p0, Lkotlinx/coroutines/internal/e0;->c:[Lv8/e2;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    if-ltz v0, :cond_1

    :goto_0
    add-int/lit8 v1, v0, -0x1

    iget-object v2, p0, Lkotlinx/coroutines/internal/e0;->c:[Lv8/e2;

    aget-object v2, v2, v0

    invoke-static {v2}, Lm8/i;->c(Ljava/lang/Object;)V

    iget-object v3, p0, Lkotlinx/coroutines/internal/e0;->b:[Ljava/lang/Object;

    aget-object v0, v3, v0

    invoke-interface {v2, p1, v0}, Lv8/e2;->m(Ld8/g;Ljava/lang/Object;)V

    if-gez v1, :cond_0

    goto :goto_1

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    :goto_1
    return-void
.end method
