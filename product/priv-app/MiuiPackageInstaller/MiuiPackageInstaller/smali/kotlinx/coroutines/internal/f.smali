.class public final Lkotlinx/coroutines/internal/f;
.super Ljava/lang/Object;


# static fields
.field private static final a:Lkotlinx/coroutines/internal/x;

.field public static final b:Lkotlinx/coroutines/internal/x;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lkotlinx/coroutines/internal/x;

    const-string v1, "UNDEFINED"

    invoke-direct {v0, v1}, Lkotlinx/coroutines/internal/x;-><init>(Ljava/lang/String;)V

    sput-object v0, Lkotlinx/coroutines/internal/f;->a:Lkotlinx/coroutines/internal/x;

    new-instance v0, Lkotlinx/coroutines/internal/x;

    const-string v1, "REUSABLE_CLAIMED"

    invoke-direct {v0, v1}, Lkotlinx/coroutines/internal/x;-><init>(Ljava/lang/String;)V

    sput-object v0, Lkotlinx/coroutines/internal/f;->b:Lkotlinx/coroutines/internal/x;

    return-void
.end method

.method public static final synthetic a()Lkotlinx/coroutines/internal/x;
    .locals 1

    sget-object v0, Lkotlinx/coroutines/internal/f;->a:Lkotlinx/coroutines/internal/x;

    return-object v0
.end method

.method public static final b(Ld8/d;Ljava/lang/Object;Ll8/l;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ld8/d<",
            "-TT;>;",
            "Ljava/lang/Object;",
            "Ll8/l<",
            "-",
            "Ljava/lang/Throwable;",
            "La8/v;",
            ">;)V"
        }
    .end annotation

    instance-of v0, p0, Lkotlinx/coroutines/internal/e;

    if-eqz v0, :cond_8

    check-cast p0, Lkotlinx/coroutines/internal/e;

    invoke-static {p1, p2}, Lv8/x;->b(Ljava/lang/Object;Ll8/l;)Ljava/lang/Object;

    move-result-object p2

    iget-object v0, p0, Lkotlinx/coroutines/internal/e;->d:Lv8/a0;

    invoke-virtual {p0}, Lkotlinx/coroutines/internal/e;->c()Ld8/g;

    move-result-object v1

    invoke-virtual {v0, v1}, Lv8/a0;->V(Ld8/g;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    iput-object p2, p0, Lkotlinx/coroutines/internal/e;->f:Ljava/lang/Object;

    iput v1, p0, Lv8/r0;->c:I

    iget-object p1, p0, Lkotlinx/coroutines/internal/e;->d:Lv8/a0;

    invoke-virtual {p0}, Lkotlinx/coroutines/internal/e;->c()Ld8/g;

    move-result-object p2

    invoke-virtual {p1, p2, p0}, Lv8/a0;->U(Ld8/g;Ljava/lang/Runnable;)V

    goto/16 :goto_4

    :cond_0
    sget-object v0, Lv8/f2;->a:Lv8/f2;

    invoke-virtual {v0}, Lv8/f2;->a()Lv8/y0;

    move-result-object v0

    invoke-virtual {v0}, Lv8/y0;->d0()Z

    move-result v2

    if-eqz v2, :cond_1

    iput-object p2, p0, Lkotlinx/coroutines/internal/e;->f:Ljava/lang/Object;

    iput v1, p0, Lv8/r0;->c:I

    invoke-virtual {v0, p0}, Lv8/y0;->Z(Lv8/r0;)V

    goto/16 :goto_4

    :cond_1
    invoke-virtual {v0, v1}, Lv8/y0;->b0(Z)V

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {p0}, Lkotlinx/coroutines/internal/e;->c()Ld8/g;

    move-result-object v3

    sget-object v4, Lv8/k1;->d0:Lv8/k1$b;

    invoke-interface {v3, v4}, Ld8/g;->get(Ld8/g$c;)Ld8/g$b;

    move-result-object v3

    check-cast v3, Lv8/k1;

    if-eqz v3, :cond_2

    invoke-interface {v3}, Lv8/k1;->b()Z

    move-result v4

    if-nez v4, :cond_2

    invoke-interface {v3}, Lv8/k1;->C()Ljava/util/concurrent/CancellationException;

    move-result-object v3

    invoke-virtual {p0, p2, v3}, Lkotlinx/coroutines/internal/e;->a(Ljava/lang/Object;Ljava/lang/Throwable;)V

    sget-object p2, La8/m;->a:La8/m$a;

    invoke-static {v3}, La8/n;->a(Ljava/lang/Throwable;)Ljava/lang/Object;

    move-result-object p2

    invoke-static {p2}, La8/m;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    invoke-interface {p0, p2}, Ld8/d;->i(Ljava/lang/Object;)V

    move p2, v1

    goto :goto_0

    :cond_2
    const/4 p2, 0x0

    :goto_0
    if-nez p2, :cond_7

    iget-object p2, p0, Lkotlinx/coroutines/internal/e;->e:Ld8/d;

    iget-object v3, p0, Lkotlinx/coroutines/internal/e;->g:Ljava/lang/Object;

    invoke-interface {p2}, Ld8/d;->c()Ld8/g;

    move-result-object v4

    invoke-static {v4, v3}, Lkotlinx/coroutines/internal/b0;->c(Ld8/g;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    sget-object v5, Lkotlinx/coroutines/internal/b0;->a:Lkotlinx/coroutines/internal/x;

    if-eq v3, v5, :cond_3

    invoke-static {p2, v4, v3}, Lv8/z;->g(Ld8/d;Ld8/g;Ljava/lang/Object;)Lv8/k2;

    move-result-object p2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_1

    :cond_3
    move-object p2, v2

    :goto_1
    :try_start_1
    iget-object v5, p0, Lkotlinx/coroutines/internal/e;->e:Ld8/d;

    invoke-interface {v5, p1}, Ld8/d;->i(Ljava/lang/Object;)V

    sget-object p1, La8/v;->a:La8/v;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz p2, :cond_4

    :try_start_2
    invoke-virtual {p2}, Lv8/k2;->C0()Z

    move-result p1

    if-eqz p1, :cond_7

    :cond_4
    invoke-static {v4, v3}, Lkotlinx/coroutines/internal/b0;->a(Ld8/g;Ljava/lang/Object;)V

    goto :goto_2

    :catchall_0
    move-exception p1

    if-eqz p2, :cond_5

    invoke-virtual {p2}, Lv8/k2;->C0()Z

    move-result p2

    if-eqz p2, :cond_6

    :cond_5
    invoke-static {v4, v3}, Lkotlinx/coroutines/internal/b0;->a(Ld8/g;Ljava/lang/Object;)V

    :cond_6
    throw p1

    :cond_7
    :goto_2
    invoke-virtual {v0}, Lv8/y0;->f0()Z

    move-result p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    if-nez p1, :cond_7

    goto :goto_3

    :catchall_1
    move-exception p1

    :try_start_3
    invoke-virtual {p0, p1, v2}, Lv8/r0;->h(Ljava/lang/Throwable;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    :goto_3
    invoke-virtual {v0, v1}, Lv8/y0;->X(Z)V

    goto :goto_4

    :catchall_2
    move-exception p0

    invoke-virtual {v0, v1}, Lv8/y0;->X(Z)V

    throw p0

    :cond_8
    invoke-interface {p0, p1}, Ld8/d;->i(Ljava/lang/Object;)V

    :goto_4
    return-void
.end method

.method public static synthetic c(Ld8/d;Ljava/lang/Object;Ll8/l;ILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    :cond_0
    invoke-static {p0, p1, p2}, Lkotlinx/coroutines/internal/f;->b(Ld8/d;Ljava/lang/Object;Ll8/l;)V

    return-void
.end method
