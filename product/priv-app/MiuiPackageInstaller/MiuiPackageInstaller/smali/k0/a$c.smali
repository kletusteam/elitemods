.class Lk0/a$c;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lk0/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "c"
.end annotation


# instance fields
.field public final a:I

.field public final b:I

.field public final c:J

.field public final d:[B


# direct methods
.method constructor <init>(IIJ[B)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lk0/a$c;->a:I

    iput p2, p0, Lk0/a$c;->b:I

    iput-wide p3, p0, Lk0/a$c;->c:J

    iput-object p5, p0, Lk0/a$c;->d:[B

    return-void
.end method

.method constructor <init>(II[B)V
    .locals 6

    const-wide/16 v3, -0x1

    move-object v0, p0

    move v1, p1

    move v2, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lk0/a$c;-><init>(IIJ[B)V

    return-void
.end method

.method public static a(Ljava/lang/String;)Lk0/a$c;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 p0, 0x0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    sget-object v0, Lk0/a;->s0:Ljava/nio/charset/Charset;

    invoke-virtual {p0, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object p0

    new-instance v0, Lk0/a$c;

    array-length v1, p0

    const/4 v2, 0x2

    invoke-direct {v0, v2, v1, p0}, Lk0/a$c;-><init>(II[B)V

    return-object v0
.end method

.method public static b(JLjava/nio/ByteOrder;)Lk0/a$c;
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [J

    const/4 v1, 0x0

    aput-wide p0, v0, v1

    invoke-static {v0, p2}, Lk0/a$c;->c([JLjava/nio/ByteOrder;)Lk0/a$c;

    move-result-object p0

    return-object p0
.end method

.method public static c([JLjava/nio/ByteOrder;)Lk0/a$c;
    .locals 5

    sget-object v0, Lk0/a;->Y:[I

    const/4 v1, 0x4

    aget v0, v0, v1

    array-length v2, p0

    mul-int/2addr v0, v2

    new-array v0, v0, [B

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    array-length p1, p0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, p1, :cond_0

    aget-wide v3, p0, v2

    long-to-int v3, v3

    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    new-instance p1, Lk0/a$c;

    array-length p0, p0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    invoke-direct {p1, v1, p0, v0}, Lk0/a$c;-><init>(II[B)V

    return-object p1
.end method

.method public static d(Lk0/a$e;Ljava/nio/ByteOrder;)Lk0/a$c;
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [Lk0/a$e;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    invoke-static {v0, p1}, Lk0/a$c;->e([Lk0/a$e;Ljava/nio/ByteOrder;)Lk0/a$c;

    move-result-object p0

    return-object p0
.end method

.method public static e([Lk0/a$e;Ljava/nio/ByteOrder;)Lk0/a$c;
    .locals 6

    sget-object v0, Lk0/a;->Y:[I

    const/4 v1, 0x5

    aget v0, v0, v1

    array-length v2, p0

    mul-int/2addr v0, v2

    new-array v0, v0, [B

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    array-length p1, p0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, p1, :cond_0

    aget-object v3, p0, v2

    iget-wide v4, v3, Lk0/a$e;->a:J

    long-to-int v4, v4

    invoke-virtual {v0, v4}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    iget-wide v3, v3, Lk0/a$e;->b:J

    long-to-int v3, v3

    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    new-instance p1, Lk0/a$c;

    array-length p0, p0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    invoke-direct {p1, v1, p0, v0}, Lk0/a$c;-><init>(II[B)V

    return-object p1
.end method

.method public static f(ILjava/nio/ByteOrder;)Lk0/a$c;
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    aput p0, v0, v1

    invoke-static {v0, p1}, Lk0/a$c;->g([ILjava/nio/ByteOrder;)Lk0/a$c;

    move-result-object p0

    return-object p0
.end method

.method public static g([ILjava/nio/ByteOrder;)Lk0/a$c;
    .locals 4

    sget-object v0, Lk0/a;->Y:[I

    const/4 v1, 0x3

    aget v0, v0, v1

    array-length v2, p0

    mul-int/2addr v0, v2

    new-array v0, v0, [B

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    array-length p1, p0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, p1, :cond_0

    aget v3, p0, v2

    int-to-short v3, v3

    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    new-instance p1, Lk0/a$c;

    array-length p0, p0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    invoke-direct {p1, v1, p0, v0}, Lk0/a$c;-><init>(II[B)V

    return-object p1
.end method


# virtual methods
.method public h(Ljava/nio/ByteOrder;)D
    .locals 4

    invoke-virtual {p0, p1}, Lk0/a$c;->k(Ljava/nio/ByteOrder;)Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_9

    instance-of v0, p1, Ljava/lang/String;

    if-eqz v0, :cond_0

    check-cast p1, Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    return-wide v0

    :cond_0
    instance-of v0, p1, [J

    const/4 v1, 0x0

    const-string v2, "There are more than one component"

    const/4 v3, 0x1

    if-eqz v0, :cond_2

    check-cast p1, [J

    array-length v0, p1

    if-ne v0, v3, :cond_1

    aget-wide v0, p1, v1

    long-to-double v0, v0

    return-wide v0

    :cond_1
    new-instance p1, Ljava/lang/NumberFormatException;

    invoke-direct {p1, v2}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    instance-of v0, p1, [I

    if-eqz v0, :cond_4

    check-cast p1, [I

    array-length v0, p1

    if-ne v0, v3, :cond_3

    aget p1, p1, v1

    int-to-double v0, p1

    return-wide v0

    :cond_3
    new-instance p1, Ljava/lang/NumberFormatException;

    invoke-direct {p1, v2}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_4
    instance-of v0, p1, [D

    if-eqz v0, :cond_6

    check-cast p1, [D

    array-length v0, p1

    if-ne v0, v3, :cond_5

    aget-wide v0, p1, v1

    return-wide v0

    :cond_5
    new-instance p1, Ljava/lang/NumberFormatException;

    invoke-direct {p1, v2}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_6
    instance-of v0, p1, [Lk0/a$e;

    if-eqz v0, :cond_8

    check-cast p1, [Lk0/a$e;

    array-length v0, p1

    if-ne v0, v3, :cond_7

    aget-object p1, p1, v1

    invoke-virtual {p1}, Lk0/a$e;->a()D

    move-result-wide v0

    return-wide v0

    :cond_7
    new-instance p1, Ljava/lang/NumberFormatException;

    invoke-direct {p1, v2}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_8
    new-instance p1, Ljava/lang/NumberFormatException;

    const-string v0, "Couldn\'t find a double value"

    invoke-direct {p1, v0}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_9
    new-instance p1, Ljava/lang/NumberFormatException;

    const-string v0, "NULL can\'t be converted to a double value"

    invoke-direct {p1, v0}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public i(Ljava/nio/ByteOrder;)I
    .locals 4

    invoke-virtual {p0, p1}, Lk0/a$c;->k(Ljava/nio/ByteOrder;)Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_5

    instance-of v0, p1, Ljava/lang/String;

    if-eqz v0, :cond_0

    check-cast p1, Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p1

    return p1

    :cond_0
    instance-of v0, p1, [J

    const/4 v1, 0x0

    const-string v2, "There are more than one component"

    const/4 v3, 0x1

    if-eqz v0, :cond_2

    check-cast p1, [J

    array-length v0, p1

    if-ne v0, v3, :cond_1

    aget-wide v0, p1, v1

    long-to-int p1, v0

    return p1

    :cond_1
    new-instance p1, Ljava/lang/NumberFormatException;

    invoke-direct {p1, v2}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    instance-of v0, p1, [I

    if-eqz v0, :cond_4

    check-cast p1, [I

    array-length v0, p1

    if-ne v0, v3, :cond_3

    aget p1, p1, v1

    return p1

    :cond_3
    new-instance p1, Ljava/lang/NumberFormatException;

    invoke-direct {p1, v2}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_4
    new-instance p1, Ljava/lang/NumberFormatException;

    const-string v0, "Couldn\'t find a integer value"

    invoke-direct {p1, v0}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_5
    new-instance p1, Ljava/lang/NumberFormatException;

    const-string v0, "NULL can\'t be converted to a integer value"

    invoke-direct {p1, v0}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public j(Ljava/nio/ByteOrder;)Ljava/lang/String;
    .locals 7

    invoke-virtual {p0, p1}, Lk0/a$c;->k(Ljava/nio/ByteOrder;)Ljava/lang/Object;

    move-result-object p1

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return-object v0

    :cond_0
    instance-of v1, p1, Ljava/lang/String;

    if-eqz v1, :cond_1

    check-cast p1, Ljava/lang/String;

    return-object p1

    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    instance-of v2, p1, [J

    const-string v3, ","

    const/4 v4, 0x0

    if-eqz v2, :cond_4

    check-cast p1, [J

    :cond_2
    :goto_0
    array-length v0, p1

    if-ge v4, v0, :cond_3

    aget-wide v5, p1, v4

    invoke-virtual {v1, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    add-int/lit8 v4, v4, 0x1

    array-length v0, p1

    if-eq v4, v0, :cond_2

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_3
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_4
    instance-of v2, p1, [I

    if-eqz v2, :cond_7

    check-cast p1, [I

    :cond_5
    :goto_1
    array-length v0, p1

    if-ge v4, v0, :cond_6

    aget v0, p1, v4

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    add-int/lit8 v4, v4, 0x1

    array-length v0, p1

    if-eq v4, v0, :cond_5

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_6
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_7
    instance-of v2, p1, [D

    if-eqz v2, :cond_a

    check-cast p1, [D

    :cond_8
    :goto_2
    array-length v0, p1

    if-ge v4, v0, :cond_9

    aget-wide v5, p1, v4

    invoke-virtual {v1, v5, v6}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    add-int/lit8 v4, v4, 0x1

    array-length v0, p1

    if-eq v4, v0, :cond_8

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_9
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_a
    instance-of v2, p1, [Lk0/a$e;

    if-eqz v2, :cond_d

    check-cast p1, [Lk0/a$e;

    :cond_b
    :goto_3
    array-length v0, p1

    if-ge v4, v0, :cond_c

    aget-object v0, p1, v4

    iget-wide v5, v0, Lk0/a$e;->a:J

    invoke-virtual {v1, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const/16 v0, 0x2f

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    aget-object v0, p1, v4

    iget-wide v5, v0, Lk0/a$e;->b:J

    invoke-virtual {v1, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    add-int/lit8 v4, v4, 0x1

    array-length v0, p1

    if-eq v4, v0, :cond_b

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    :cond_c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_d
    return-object v0
.end method

.method k(Ljava/nio/ByteOrder;)Ljava/lang/Object;
    .locals 10

    goto/32 :goto_11

    nop

    :goto_0
    invoke-static {v1, v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_1
    goto/32 :goto_2a

    nop

    :goto_2
    invoke-static {v1, v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_3
    goto/32 :goto_36

    nop

    :goto_4
    goto/16 :goto_25

    :catch_0
    move-exception p1

    goto/32 :goto_24

    nop

    :goto_5
    add-int/lit8 v5, v5, 0x1

    goto/32 :goto_54

    nop

    :goto_6
    if-nez v3, :cond_0

    goto/32 :goto_1

    :cond_0
    :try_start_0
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    goto/32 :goto_1c

    nop

    :goto_7
    add-int/lit8 v5, v5, 0x1

    goto/32 :goto_32

    nop

    :goto_8
    add-int/lit8 v5, v5, 0x1

    goto/32 :goto_15

    nop

    :goto_9
    return-object p1

    :pswitch_0
    :try_start_1
    iget p1, p0, Lk0/a$c;->b:I

    new-array p1, p1, [Lk0/a$e;

    :goto_a
    iget v4, p0, Lk0/a$c;->b:I

    if-ge v5, v4, :cond_f

    invoke-virtual {v3}, Lk0/a$b;->m()J

    move-result-wide v6

    invoke-virtual {v3}, Lk0/a$b;->m()J

    move-result-wide v8

    new-instance v4, Lk0/a$e;

    invoke-direct {v4, v6, v7, v8, v9}, Lk0/a$e;-><init>(JJ)V

    aput-object v4, p1, v5
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_a
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto/32 :goto_5

    nop

    :goto_b
    const/4 v2, 0x0

    :try_start_2
    new-instance v3, Lk0/a$b;

    iget-object v4, p0, Lk0/a$c;->d:[B

    invoke-direct {v3, v4}, Lk0/a$b;-><init>([B)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_b
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    invoke-virtual {v3, p1}, Lk0/a$b;->p(Ljava/nio/ByteOrder;)V

    iget p1, p0, Lk0/a$c;->a:I

    const/4 v4, 0x1

    const/4 v5, 0x0

    packed-switch p1, :pswitch_data_0

    goto/16 :goto_17

    :pswitch_1
    iget p1, p0, Lk0/a$c;->b:I

    new-array p1, p1, [D

    :goto_c
    iget v4, p0, Lk0/a$c;->b:I

    if-ge v5, v4, :cond_d

    invoke-virtual {v3}, Lk0/a$b;->readDouble()D

    move-result-wide v6

    aput-wide v6, p1, v5
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_a
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto/32 :goto_5a

    nop

    :goto_d
    return-object p1

    :pswitch_2
    :try_start_4
    iget p1, p0, Lk0/a$c;->b:I

    new-array p1, p1, [I

    :goto_e
    iget v4, p0, Lk0/a$c;->b:I

    if-ge v5, v4, :cond_5

    invoke-virtual {v3}, Lk0/a$b;->readUnsignedShort()I

    move-result v4

    aput v4, p1, v5
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_a
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto/32 :goto_7

    nop

    :goto_f
    move-object v2, v3

    :goto_10
    goto/32 :goto_5b

    nop

    :goto_11
    const-string v0, "IOException occurred while closing InputStream"

    goto/32 :goto_2c

    nop

    :goto_12
    return-object p1

    :pswitch_3
    :try_start_5
    iget p1, p0, Lk0/a$c;->b:I

    new-array p1, p1, [J

    :goto_13
    iget v4, p0, Lk0/a$c;->b:I

    if-ge v5, v4, :cond_1

    invoke-virtual {v3}, Lk0/a$b;->m()J

    move-result-wide v6

    aput-wide v6, p1, v5
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_a
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto/32 :goto_8

    nop

    :goto_14
    goto/16 :goto_46

    :catchall_0
    move-exception p1

    goto/32 :goto_53

    nop

    :goto_15
    goto :goto_13

    :cond_1
    :try_start_6
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_f

    goto/32 :goto_60

    nop

    :goto_16
    return-object v4

    :goto_17
    :try_start_7
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_0

    goto/32 :goto_4

    nop

    :goto_18
    add-int/lit8 v5, v5, 0x1

    goto/32 :goto_4b

    nop

    :goto_19
    goto/16 :goto_29

    :catch_1
    move-exception v2

    goto/32 :goto_28

    nop

    :goto_1a
    throw p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_6
        :pswitch_5
        :pswitch_7
        :pswitch_9
        :pswitch_4
        :pswitch_8
        :pswitch_1
    .end packed-switch

    :goto_1b
    add-int/lit8 v5, v5, 0x1

    goto/32 :goto_3c

    nop

    :goto_1c
    goto/16 :goto_1

    :catch_2
    move-exception p1

    goto/32 :goto_0

    nop

    :goto_1d
    return-object v6

    :cond_2
    :try_start_8
    new-instance v4, Ljava/lang/String;

    sget-object v5, Lk0/a;->s0:Ljava/nio/charset/Charset;

    invoke-direct {v4, p1, v5}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_a
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    :try_start_9
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_4

    goto/32 :goto_2d

    nop

    :goto_1e
    invoke-static {v1, v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_1f
    goto/32 :goto_26

    nop

    :goto_20
    goto/16 :goto_4d

    :cond_3
    :try_start_a
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_e

    goto/32 :goto_5f

    nop

    :goto_21
    invoke-static {v1, v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_22
    goto/32 :goto_1d

    nop

    :goto_23
    goto/16 :goto_56

    :cond_4
    :try_start_b
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_5

    goto/32 :goto_35

    nop

    :goto_24
    invoke-static {v1, v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_25
    goto/32 :goto_52

    nop

    :goto_26
    return-object p1

    :pswitch_4
    :try_start_c
    iget p1, p0, Lk0/a$c;->b:I

    new-array p1, p1, [Lk0/a$e;

    :goto_27
    iget v4, p0, Lk0/a$c;->b:I

    if-ge v5, v4, :cond_c

    invoke-virtual {v3}, Lk0/a$b;->readInt()I

    move-result v4

    int-to-long v6, v4

    invoke-virtual {v3}, Lk0/a$b;->readInt()I

    move-result v4

    int-to-long v8, v4

    new-instance v4, Lk0/a$e;

    invoke-direct {v4, v6, v7, v8, v9}, Lk0/a$e;-><init>(JJ)V

    aput-object v4, p1, v5
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_a
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    goto/32 :goto_1b

    nop

    :goto_28
    invoke-static {v1, v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_29
    goto/32 :goto_3e

    nop

    :goto_2a
    return-object v2

    :catchall_1
    move-exception p1

    goto/32 :goto_f

    nop

    :goto_2b
    goto/16 :goto_5d

    :catch_3
    move-exception v2

    goto/32 :goto_5c

    nop

    :goto_2c
    const-string v1, "ExifInterface"

    goto/32 :goto_b

    nop

    :goto_2d
    goto :goto_31

    :catch_4
    move-exception p1

    goto/32 :goto_30

    nop

    :goto_2e
    invoke-static {v1, v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_2f
    goto/32 :goto_55

    nop

    :goto_30
    invoke-static {v1, v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_31
    goto/32 :goto_16

    nop

    :goto_32
    goto/16 :goto_e

    :cond_5
    :try_start_d
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_c

    goto/32 :goto_57

    nop

    :goto_33
    invoke-static {v1, v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_34
    goto/32 :goto_4c

    nop

    :goto_35
    goto/16 :goto_48

    :catch_5
    move-exception v2

    goto/32 :goto_47

    nop

    :goto_36
    return-object p1

    :pswitch_5
    :try_start_e
    iget p1, p0, Lk0/a$c;->b:I

    sget-object v6, Lk0/a;->Z:[B

    array-length v6, v6

    if-lt p1, v6, :cond_8

    move p1, v5

    :goto_37
    sget-object v6, Lk0/a;->Z:[B

    array-length v7, v6

    if-ge p1, v7, :cond_7

    iget-object v7, p0, Lk0/a$c;->d:[B

    aget-byte v7, v7, p1

    aget-byte v8, v6, p1

    if-eq v7, v8, :cond_6

    move v4, v5

    goto :goto_38

    :cond_6
    add-int/lit8 p1, p1, 0x1

    goto :goto_37

    :cond_7
    :goto_38
    if-eqz v4, :cond_8

    array-length v5, v6

    :cond_8
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    :goto_39
    iget v4, p0, Lk0/a$c;->b:I

    if-ge v5, v4, :cond_b

    iget-object v4, p0, Lk0/a$c;->d:[B

    aget-byte v4, v4, v5

    if-nez v4, :cond_9

    goto :goto_3b

    :cond_9
    const/16 v6, 0x20

    if-lt v4, v6, :cond_a

    int-to-char v4, v4

    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_3a

    :cond_a
    const/16 v4, 0x3f

    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :goto_3a
    add-int/lit8 v5, v5, 0x1

    goto :goto_39

    :cond_b
    :goto_3b
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_a
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    :try_start_f
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_1

    goto/32 :goto_19

    nop

    :goto_3c
    goto/16 :goto_27

    :cond_c
    :try_start_10
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_8

    goto/32 :goto_4a

    nop

    :goto_3d
    add-int/lit8 v5, v5, 0x1

    goto/32 :goto_23

    nop

    :goto_3e
    return-object p1

    :pswitch_6
    :try_start_11
    iget-object p1, p0, Lk0/a$c;->d:[B

    array-length v6, p1

    if-ne v6, v4, :cond_2

    aget-byte v6, p1, v5

    if-ltz v6, :cond_2

    aget-byte v6, p1, v5

    if-gt v6, v4, :cond_2

    new-instance v6, Ljava/lang/String;

    new-array v4, v4, [C

    aget-byte p1, p1, v5

    add-int/lit8 p1, p1, 0x30

    int-to-char p1, p1

    aput-char p1, v4, v5

    invoke-direct {v6, v4}, Ljava/lang/String;-><init>([C)V
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_a
    .catchall {:try_start_11 .. :try_end_11} :catchall_1

    :try_start_12
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_7

    goto/32 :goto_44

    nop

    :goto_3f
    return-object p1

    :pswitch_7
    :try_start_13
    iget p1, p0, Lk0/a$c;->b:I

    new-array p1, p1, [I

    :goto_40
    iget v4, p0, Lk0/a$c;->b:I

    if-ge v5, v4, :cond_e

    invoke-virtual {v3}, Lk0/a$b;->readShort()S

    move-result v4

    aput v4, p1, v5
    :try_end_13
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_13} :catch_a
    .catchall {:try_start_13 .. :try_end_13} :catchall_1

    goto/32 :goto_18

    nop

    :goto_41
    goto/16 :goto_34

    :catch_6
    move-exception v2

    goto/32 :goto_33

    nop

    :goto_42
    invoke-static {v1, v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_43
    goto/32 :goto_9

    nop

    :goto_44
    goto/16 :goto_22

    :catch_7
    move-exception p1

    goto/32 :goto_21

    nop

    :goto_45
    move-object v3, v2

    :goto_46
    :try_start_14
    const-string v4, "IOException occurred during reading a value"

    invoke-static {v1, v4, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_1

    goto/32 :goto_6

    nop

    :goto_47
    invoke-static {v1, v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_48
    goto/32 :goto_3f

    nop

    :goto_49
    goto/16 :goto_c

    :cond_d
    :try_start_15
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_15
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_15} :catch_6

    goto/32 :goto_41

    nop

    :goto_4a
    goto/16 :goto_2f

    :catch_8
    move-exception v2

    goto/32 :goto_2e

    nop

    :goto_4b
    goto :goto_40

    :cond_e
    :try_start_16
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_16
    .catch Ljava/io/IOException; {:try_start_16 .. :try_end_16} :catch_d

    goto/32 :goto_5e

    nop

    :goto_4c
    return-object p1

    :pswitch_8
    :try_start_17
    iget p1, p0, Lk0/a$c;->b:I

    new-array p1, p1, [D

    :goto_4d
    iget v4, p0, Lk0/a$c;->b:I

    if-ge v5, v4, :cond_3

    invoke-virtual {v3}, Lk0/a$b;->readFloat()F

    move-result v4

    float-to-double v6, v4

    aput-wide v6, p1, v5
    :try_end_17
    .catch Ljava/io/IOException; {:try_start_17 .. :try_end_17} :catch_a
    .catchall {:try_start_17 .. :try_end_17} :catchall_1

    goto/32 :goto_4e

    nop

    :goto_4e
    add-int/lit8 v5, v5, 0x1

    goto/32 :goto_20

    nop

    :goto_4f
    goto :goto_51

    :catch_9
    move-exception v2

    goto/32 :goto_50

    nop

    :goto_50
    invoke-static {v1, v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_51
    goto/32 :goto_1a

    nop

    :goto_52
    return-object v2

    :catch_a
    move-exception p1

    goto/32 :goto_14

    nop

    :goto_53
    goto/16 :goto_10

    :catch_b
    move-exception p1

    goto/32 :goto_45

    nop

    :goto_54
    goto/16 :goto_a

    :cond_f
    :try_start_18
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_18
    .catch Ljava/io/IOException; {:try_start_18 .. :try_end_18} :catch_3

    goto/32 :goto_2b

    nop

    :goto_55
    return-object p1

    :pswitch_9
    :try_start_19
    iget p1, p0, Lk0/a$c;->b:I

    new-array p1, p1, [I

    :goto_56
    iget v4, p0, Lk0/a$c;->b:I

    if-ge v5, v4, :cond_4

    invoke-virtual {v3}, Lk0/a$b;->readInt()I

    move-result v4

    aput v4, p1, v5
    :try_end_19
    .catch Ljava/io/IOException; {:try_start_19 .. :try_end_19} :catch_a
    .catchall {:try_start_19 .. :try_end_19} :catchall_1

    goto/32 :goto_3d

    nop

    :goto_57
    goto/16 :goto_3

    :catch_c
    move-exception v2

    goto/32 :goto_2

    nop

    :goto_58
    invoke-static {v1, v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_59
    goto/32 :goto_d

    nop

    :goto_5a
    add-int/lit8 v5, v5, 0x1

    goto/32 :goto_49

    nop

    :goto_5b
    if-nez v2, :cond_10

    goto/32 :goto_51

    :cond_10
    :try_start_1a
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_1a
    .catch Ljava/io/IOException; {:try_start_1a .. :try_end_1a} :catch_9

    goto/32 :goto_4f

    nop

    :goto_5c
    invoke-static {v1, v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_5d
    goto/32 :goto_12

    nop

    :goto_5e
    goto/16 :goto_43

    :catch_d
    move-exception v2

    goto/32 :goto_42

    nop

    :goto_5f
    goto/16 :goto_1f

    :catch_e
    move-exception v2

    goto/32 :goto_1e

    nop

    :goto_60
    goto :goto_59

    :catch_f
    move-exception v2

    goto/32 :goto_58

    nop
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Lk0/a;->X:[Ljava/lang/String;

    iget v2, p0, Lk0/a$c;->a:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", data length:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lk0/a$c;->d:[B

    array-length v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
