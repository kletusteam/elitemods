.class public Lw6/g;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field private static volatile b:Z


# instance fields
.field private a:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lw6/g;->a:Landroid/content/Context;

    return-void
.end method

.method private a()V
    .locals 1

    invoke-static {}, Lx6/h;->i()V

    iget-object v0, p0, Lw6/g;->a:Landroid/content/Context;

    invoke-static {v0}, Lx6/f;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    sget-boolean v0, Lw6/g;->b:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    sput-boolean v0, Lw6/g;->b:Z

    const/4 v0, 0x0

    :try_start_0
    invoke-direct {p0}, Lw6/g;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    sput-boolean v0, Lw6/g;->b:Z

    :cond_1
    return-void
.end method

.method private b(Ljava/lang/String;)Z
    .locals 2

    const-string v0, "https://mpush-api.aliyun.com/v2.0/a/audid/req/"

    const/4 v1, 0x1

    invoke-static {v0, p1, v1}, Lw6/b;->a(Ljava/lang/String;Ljava/lang/String;Z)Lw6/a;

    move-result-object p1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    invoke-static {p1}, La7/g;->a(Lw6/a;)Z

    move-result p1

    return p1
.end method

.method private c()V
    .locals 4

    invoke-static {}, Lx6/h;->i()V

    invoke-direct {p0}, Lw6/g;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    new-array v0, v2, [Ljava/lang/Object;

    const-string v1, "postData is empty"

    invoke-static {v1, v0}, Lx6/h;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    :cond_0
    invoke-direct {p0, v0}, Lw6/g;->b(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x1

    const-string v3, ""

    if-eqz v0, :cond_1

    new-array v0, v1, [Ljava/lang/Object;

    const-string v1, "upload success"

    aput-object v1, v0, v2

    invoke-static {v3, v0}, Lx6/h;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    new-array v0, v1, [Ljava/lang/Object;

    const-string v1, "upload fail"

    aput-object v1, v0, v2

    invoke-static {v3, v0}, Lx6/h;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method private d()Ljava/lang/String;
    .locals 3

    invoke-static {}, La7/b;->a()La7/b;

    move-result-object v0

    invoke-virtual {v0}, La7/b;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    invoke-static {v0}, Lv6/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lx6/h;->h()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    const-string v2, ""

    invoke-static {v2, v1}, Lx6/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_1
    invoke-static {v0}, Lv6/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public run()V
    .locals 3

    :try_start_0
    invoke-direct {p0}, Lw6/g;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, ""

    invoke-static {v2, v0, v1}, Lx6/h;->d(Ljava/lang/String;Ljava/lang/Throwable;[Ljava/lang/Object;)V

    :goto_0
    return-void
.end method
