.class public abstract Lc3/a;
.super Ljava/lang/Object;

# interfaces
.implements Lu2/j;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lu2/j<",
        "Landroid/graphics/ImageDecoder$Source;",
        "TT;>;"
    }
.end annotation


# instance fields
.field final a:Ld3/s;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Ld3/s;->b()Ld3/s;

    move-result-object v0

    iput-object v0, p0, Lc3/a;->a:Ld3/s;

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;Lu2/h;)Z
    .locals 0

    check-cast p1, Landroid/graphics/ImageDecoder$Source;

    invoke-virtual {p0, p1, p2}, Lc3/a;->e(Landroid/graphics/ImageDecoder$Source;Lu2/h;)Z

    move-result p1

    return p1
.end method

.method public bridge synthetic b(Ljava/lang/Object;IILu2/h;)Lw2/v;
    .locals 0

    check-cast p1, Landroid/graphics/ImageDecoder$Source;

    invoke-virtual {p0, p1, p2, p3, p4}, Lc3/a;->d(Landroid/graphics/ImageDecoder$Source;IILu2/h;)Lw2/v;

    move-result-object p1

    return-object p1
.end method

.method protected abstract c(Landroid/graphics/ImageDecoder$Source;IILandroid/graphics/ImageDecoder$OnHeaderDecodedListener;)Lw2/v;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/ImageDecoder$Source;",
            "II",
            "Landroid/graphics/ImageDecoder$OnHeaderDecodedListener;",
            ")",
            "Lw2/v<",
            "TT;>;"
        }
    .end annotation
.end method

.method public final d(Landroid/graphics/ImageDecoder$Source;IILu2/h;)Lw2/v;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/ImageDecoder$Source;",
            "II",
            "Lu2/h;",
            ")",
            "Lw2/v<",
            "TT;>;"
        }
    .end annotation

    sget-object v0, Ld3/n;->f:Lu2/g;

    invoke-virtual {p4, v0}, Lu2/h;->c(Lu2/g;)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lu2/b;

    sget-object v0, Ld3/m;->h:Lu2/g;

    invoke-virtual {p4, v0}, Lu2/h;->c(Lu2/g;)Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Ld3/m;

    sget-object v0, Ld3/n;->j:Lu2/g;

    invoke-virtual {p4, v0}, Lu2/h;->c(Lu2/g;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p4, v0}, Lu2/h;->c(Lu2/g;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    move v5, v0

    sget-object v0, Ld3/n;->g:Lu2/g;

    invoke-virtual {p4, v0}, Lu2/h;->c(Lu2/g;)Ljava/lang/Object;

    move-result-object p4

    move-object v8, p4

    check-cast v8, Lu2/i;

    new-instance p4, Lc3/a$a;

    move-object v1, p4

    move-object v2, p0

    move v3, p2

    move v4, p3

    invoke-direct/range {v1 .. v8}, Lc3/a$a;-><init>(Lc3/a;IIZLu2/b;Ld3/m;Lu2/i;)V

    invoke-virtual {p0, p1, p2, p3, p4}, Lc3/a;->c(Landroid/graphics/ImageDecoder$Source;IILandroid/graphics/ImageDecoder$OnHeaderDecodedListener;)Lw2/v;

    move-result-object p1

    return-object p1
.end method

.method public final e(Landroid/graphics/ImageDecoder$Source;Lu2/h;)Z
    .locals 0

    const/4 p1, 0x1

    return p1
.end method
