.class final Lm5/z0$c;
.super Lf8/k;

# interfaces
.implements Ll8/p;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lm5/z0;->P1()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf8/k;",
        "Ll8/p<",
        "Lv8/e0;",
        "Ld8/d<",
        "-",
        "La8/v;",
        ">;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation runtime Lf8/f;
    c = "com.miui.packageInstaller.InstallerPrepareActivity$loadApk$1"
    f = "InstallerPrepareActivity.kt"
    l = {
        0x14d
    }
    m = "invokeSuspend"
.end annotation


# instance fields
.field e:Ljava/lang/Object;

.field f:I

.field final synthetic g:Lm5/z0;


# direct methods
.method constructor <init>(Lm5/z0;Ld8/d;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lm5/z0;",
            "Ld8/d<",
            "-",
            "Lm5/z0$c;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lm5/z0$c;->g:Lm5/z0;

    const/4 p1, 0x2

    invoke-direct {p0, p1, p2}, Lf8/k;-><init>(ILd8/d;)V

    return-void
.end method


# virtual methods
.method public final b(Ljava/lang/Object;Ld8/d;)Ld8/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ld8/d<",
            "*>;)",
            "Ld8/d<",
            "La8/v;",
            ">;"
        }
    .end annotation

    new-instance p1, Lm5/z0$c;

    iget-object v0, p0, Lm5/z0$c;->g:Lm5/z0;

    invoke-direct {p1, v0, p2}, Lm5/z0$c;-><init>(Lm5/z0;Ld8/d;)V

    return-object p1
.end method

.method public bridge synthetic g(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lv8/e0;

    check-cast p2, Ld8/d;

    invoke-virtual {p0, p1, p2}, Lm5/z0$c;->q(Lv8/e0;Ld8/d;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final n(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    invoke-static {}, Le8/b;->c()Ljava/lang/Object;

    move-result-object v0

    iget v1, p0, Lm5/z0$c;->f:I

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-eqz v1, :cond_1

    if-ne v1, v3, :cond_0

    iget-object v0, p0, Lm5/z0$c;->e:Ljava/lang/Object;

    check-cast v0, Lm5/z0;

    :try_start_0
    invoke-static {p1}, La8/n;->b(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    :catch_0
    move-exception p1

    goto/16 :goto_2

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    invoke-static {p1}, La8/n;->b(Ljava/lang/Object;)V

    iget-object p1, p0, Lm5/z0$c;->g:Lm5/z0;

    invoke-static {p1}, Lm5/z0;->X0(Lm5/z0;)Z

    move-result p1

    if-eqz p1, :cond_4

    iget-object p1, p0, Lm5/z0$c;->g:Lm5/z0;

    invoke-virtual {p1}, Lm5/z0;->q1()Lcom/miui/packageInstaller/model/ApkInfo;

    move-result-object p1

    if-eqz p1, :cond_4

    iget-object p1, p0, Lm5/z0$c;->g:Lm5/z0;

    invoke-virtual {p1}, Lm5/z0;->q1()Lcom/miui/packageInstaller/model/ApkInfo;

    move-result-object p1

    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lcom/miui/packageInstaller/model/ApkInfo;->getPackageInfo()Landroid/content/pm/PackageInfo;

    move-result-object p1

    if-eqz p1, :cond_3

    iget-object p1, p1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    if-eqz p1, :cond_3

    iget-object v0, p0, Lm5/z0$c;->g:Lm5/z0;

    invoke-virtual {v0}, Lm5/z0;->q1()Lcom/miui/packageInstaller/model/ApkInfo;

    move-result-object v1

    if-nez v1, :cond_2

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/pm/PackageManager;->getApplicationIcon(Landroid/content/pm/ApplicationInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/miui/packageInstaller/model/ApkInfo;->setIcon(Landroid/graphics/drawable/Drawable;)V

    :cond_3
    :goto_0
    iget-object p1, p0, Lm5/z0$c;->g:Lm5/z0;

    invoke-virtual {p1}, Lm5/z0;->q1()Lcom/miui/packageInstaller/model/ApkInfo;

    move-result-object v0

    invoke-virtual {p1, v0, v2}, Lm5/z0;->Y1(Lcom/miui/packageInstaller/model/ApkInfo;I)V

    sget-object p1, La8/v;->a:La8/v;

    return-object p1

    :cond_4
    iget-object p1, p0, Lm5/z0$c;->g:Lm5/z0;

    invoke-virtual {p1}, Lm5/z0;->v1()Landroid/net/Uri;

    move-result-object p1

    if-eqz p1, :cond_7

    new-instance p1, Lv5/a;

    iget-object v1, p0, Lm5/z0$c;->g:Lm5/z0;

    invoke-virtual {v1}, Lm5/z0;->r1()Lm5/e;

    move-result-object v1

    iget-object v5, p0, Lm5/z0$c;->g:Lm5/z0;

    invoke-virtual {v5}, Lm5/z0;->v1()Landroid/net/Uri;

    move-result-object v5

    invoke-static {v5}, Lm8/i;->c(Ljava/lang/Object;)V

    invoke-direct {p1, v1, v5}, Lv5/a;-><init>(Lm5/e;Landroid/net/Uri;)V

    :try_start_1
    iget-object v1, p0, Lm5/z0$c;->g:Lm5/z0;

    iput-object v1, p0, Lm5/z0$c;->e:Ljava/lang/Object;

    iput v3, p0, Lm5/z0$c;->f:I

    invoke-virtual {p1, p0}, Lv5/a;->n(Ld8/d;)Ljava/lang/Object;

    move-result-object p1

    if-ne p1, v0, :cond_5

    return-object v0

    :cond_5
    move-object v0, v1

    :goto_1
    check-cast p1, Lcom/miui/packageInstaller/model/ApkInfo;

    invoke-virtual {v0, p1}, Lm5/z0;->f2(Lcom/miui/packageInstaller/model/ApkInfo;)V

    iget-object p1, p0, Lm5/z0$c;->g:Lm5/z0;

    invoke-virtual {p1}, Lm5/z0;->q1()Lcom/miui/packageInstaller/model/ApkInfo;

    move-result-object v0

    invoke-virtual {p1, v0, v2}, Lm5/z0;->Y1(Lcom/miui/packageInstaller/model/ApkInfo;I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_4

    :goto_2
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    instance-of v0, p1, Lv5/a$a;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lm5/z0$c;->g:Lm5/z0;

    check-cast p1, Lv5/a$a;

    invoke-virtual {p1}, Lv5/a$a;->a()I

    move-result p1

    invoke-virtual {v0, v4, p1}, Lm5/z0;->Y1(Lcom/miui/packageInstaller/model/ApkInfo;I)V

    goto :goto_4

    :cond_6
    iget-object p1, p0, Lm5/z0$c;->g:Lm5/z0;

    const/16 v0, 0x21

    goto :goto_3

    :cond_7
    iget-object p1, p0, Lm5/z0$c;->g:Lm5/z0;

    const/16 v0, 0x22

    :goto_3
    invoke-virtual {p1, v4, v0}, Lm5/z0;->Y1(Lcom/miui/packageInstaller/model/ApkInfo;I)V

    :goto_4
    sget-object p1, La8/v;->a:La8/v;

    return-object p1
.end method

.method public final q(Lv8/e0;Ld8/d;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lv8/e0;",
            "Ld8/d<",
            "-",
            "La8/v;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lm5/z0$c;->b(Ljava/lang/Object;Ld8/d;)Ld8/d;

    move-result-object p1

    check-cast p1, Lm5/z0$c;

    sget-object p2, La8/v;->a:La8/v;

    invoke-virtual {p1, p2}, Lm5/z0$c;->n(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method
