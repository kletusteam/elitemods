.class public abstract Lm5/z0;
.super Lq2/b;

# interfaces
.implements Lw5/a$a;
.implements Le6/a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lm5/z0$a;
    }
.end annotation


# static fields
.field public static final U:Lm5/z0$a;


# instance fields
.field private A:Z

.field private B:Lcom/miui/packageInstaller/model/Virus;

.field private C:Lcom/miui/packageInstaller/model/CloudParams;

.field private D:Z

.field private E:Z

.field private F:Z

.field private G:Lw5/a;

.field private H:Z

.field private I:Z

.field private J:Z

.field private K:I

.field private L:Z

.field private M:Lo5/c;

.field private N:Ljava/lang/String;

.field private O:Ll6/d;

.field private P:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lz5/a;",
            ">;"
        }
    .end annotation
.end field

.field private Q:Lcom/miui/packageInstaller/model/ApkInfo;

.field private R:I

.field private S:I

.field private T:Ljava/lang/String;

.field private final u:Ljava/lang/String;

.field private v:Landroid/content/pm/PackageInstaller;

.field private w:I

.field private x:Landroid/net/Uri;

.field private y:Lm5/e;

.field public z:Lt5/x;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lm5/z0$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lm5/z0$a;-><init>(Lm8/g;)V

    sput-object v0, Lm5/z0;->U:Lm5/z0$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lq2/b;-><init>()V

    const-string v0, "IPA"

    iput-object v0, p0, Lm5/z0;->u:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lm5/z0;->w:I

    new-instance v0, Lm5/e;

    invoke-direct {v0}, Lm5/e;-><init>()V

    iput-object v0, p0, Lm5/z0;->y:Lm5/e;

    new-instance v0, Lo5/c;

    invoke-direct {v0}, Lo5/c;-><init>()V

    iput-object v0, p0, Lm5/z0;->M:Lo5/c;

    new-instance v0, Ll6/d;

    invoke-direct {v0}, Ll6/d;-><init>()V

    iput-object v0, p0, Lm5/z0;->O:Ll6/d;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lm5/z0;->P:Ljava/util/ArrayList;

    const-string v0, ""

    iput-object v0, p0, Lm5/z0;->T:Ljava/lang/String;

    return-void
.end method

.method private final A1()V
    .locals 5

    iget-object v0, p0, Lm5/z0;->y:Lm5/e;

    iget-object v1, p0, Lm5/z0;->Q:Lcom/miui/packageInstaller/model/ApkInfo;

    if-eqz v0, :cond_2

    if-nez v1, :cond_0

    goto/16 :goto_1

    :cond_0
    new-instance v2, Lp5/b;

    const-string v3, "report_btn"

    const-string v4, "button"

    invoke-direct {v2, v3, v4, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v2}, Lp5/f;->c()Z

    new-instance v2, Ljava/lang/StringBuilder;

    sget-boolean v3, Lq2/c;->a:Z

    if-eqz v3, :cond_1

    const-string v3, "http://fe.market.pt.xiaomi.com/hd/apm-h5-cdn/cdn-feedbackV1.html"

    goto :goto_0

    :cond_1
    const-string v3, "https://app.market.xiaomi.com/hd/apm-h5-cdn/cdn-feedbackV1.html"

    :goto_0
    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "?pName="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/ApkInfo;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "&appName="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/ApkInfo;->getLabel()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "&appVersionCode="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/ApkInfo;->getVersionCode()I

    move-result v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "&pageRef="

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "com.miui.packageinstaller"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "&installSource="

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lm5/e;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "&a_hide="

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "true"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "StringBuilder(if (Config\u2026              .toString()"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mimarket://browse?url="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "&back=true"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lq2/d;->b(Landroid/content/Context;Ljava/lang/String;)Z

    :cond_2
    :goto_1
    return-void
.end method

.method private final C1()V
    .locals 3

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "from_type"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lm5/z0;->R:I

    invoke-direct {p0}, Lm5/z0;->K1()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "apk_info"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/miui/packageInstaller/model/ApkInfo;

    iput-object v0, p0, Lm5/z0;->Q:Lcom/miui/packageInstaller/model/ApkInfo;

    :cond_0
    return-void
.end method

.method private static final G1(Lm5/z0;Landroid/view/View;)V
    .locals 2

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Lp5/b;

    const-string v0, "page_back_btn"

    const-string v1, "button"

    invoke-direct {p1, v0, v1, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    const-string v0, "back_type"

    const-string v1, "click_icon"

    invoke-virtual {p1, v0, v1}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p1

    invoke-virtual {p1}, Lp5/f;->c()Z

    invoke-virtual {p0}, Lm5/z0;->g1()V

    return-void
.end method

.method private static final H1(Lm5/z0;Landroid/view/View;)V
    .locals 2

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Lp5/b;

    const-string v0, "setting_btn"

    const-string v1, "button"

    invoke-direct {p1, v0, v1, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p1}, Lp5/f;->c()Z

    new-instance p1, Landroid/content/Intent;

    const-class v0, Lcom/android/packageinstaller/SettingsActivity;

    invoke-direct {p1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v0, p0, Lm5/z0;->Q:Lcom/miui/packageInstaller/model/ApkInfo;

    const-string v1, "apk_info"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-virtual {p0}, Lm5/z0;->c()Lm5/e;

    move-result-object v0

    const-string v1, "caller"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    iget-object v0, p0, Lq2/b;->q:Lo5/b;

    const-string v1, "fromPage"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    invoke-virtual {p0, p1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private static final I1(Lm5/z0;Landroid/view/View;)V
    .locals 0

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lm5/z0;->A1()V

    return-void
.end method

.method public static synthetic J0(Lm5/z0;)V
    .locals 0

    invoke-static {p0}, Lm5/z0;->c1(Lm5/z0;)V

    return-void
.end method

.method public static synthetic K0(Lm5/z0;Ljava/util/HashMap;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lm5/z0;->q2(Lm5/z0;Ljava/util/HashMap;Landroid/view/View;)V

    return-void
.end method

.method private final K1()Z
    .locals 2

    iget v0, p0, Lm5/z0;->R:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public static synthetic L0(Lm5/z0;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lm5/z0;->U1(Lm5/z0;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic M0(Lm5/z0;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lm5/z0;->t2(Lm5/z0;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic N0(Lm5/z0;)V
    .locals 0

    invoke-static {p0}, Lm5/z0;->Z1(Lm5/z0;)V

    return-void
.end method

.method public static synthetic O0(Lm5/z0;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lm5/z0;->G1(Lm5/z0;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic P0(Lm5/z0;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lm5/z0;->V1(Lm5/z0;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic Q0(Lm5/z0;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lm5/z0;->n2(Lm5/z0;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic R0(Lm5/z0;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lm5/z0;->l2(Lm5/z0;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic S0(Lm5/z0;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lm5/z0;->I1(Lm5/z0;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic T0(Lm5/z0;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lm5/z0;->s2(Lm5/z0;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic U0(Lm5/z0;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lm5/z0;->H1(Lm5/z0;Landroid/view/View;)V

    return-void
.end method

.method private static final U1(Lm5/z0;Landroid/view/View;)V
    .locals 2

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p0, Lm5/z0;->C:Lcom/miui/packageInstaller/model/CloudParams;

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    iget-object p1, p1, Lcom/miui/packageInstaller/model/CloudParams;->positiveButtonTip:Lcom/miui/packageInstaller/model/PositiveButtonRules;

    if-eqz p1, :cond_0

    iget-object p1, p1, Lcom/miui/packageInstaller/model/PositiveButtonRules;->actionUrl:Ljava/lang/String;

    goto :goto_0

    :cond_0
    move-object p1, v0

    :goto_0
    invoke-static {p0, p1}, Lq2/d;->b(Landroid/content/Context;Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_2

    iget-object p1, p0, Lm5/z0;->C:Lcom/miui/packageInstaller/model/CloudParams;

    if-eqz p1, :cond_1

    iget-object p1, p1, Lcom/miui/packageInstaller/model/CloudParams;->positiveButtonTip:Lcom/miui/packageInstaller/model/PositiveButtonRules;

    if-eqz p1, :cond_1

    iget-object v0, p1, Lcom/miui/packageInstaller/model/PositiveButtonRules;->actionUrlBackup:Ljava/lang/String;

    :cond_1
    invoke-static {p0, v0}, Lq2/d;->b(Landroid/content/Context;Ljava/lang/String;)Z

    move-result p1

    :cond_2
    if-eqz p1, :cond_3

    invoke-virtual {p0}, Lm5/z0;->g1()V

    :cond_3
    new-instance p1, Lp5/b;

    const-string v0, "search_similar_app_btn"

    const-string v1, "button"

    invoke-direct {p1, v0, v1, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p1}, Lp5/f;->c()Z

    return-void
.end method

.method public static final synthetic V0(Lm5/z0;)Lo5/b;
    .locals 0

    iget-object p0, p0, Lq2/b;->q:Lo5/b;

    return-object p0
.end method

.method private static final V1(Lm5/z0;Landroid/view/View;)V
    .locals 2

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Lp5/b;

    const-string v0, "cancel_install_btn"

    const-string v1, "button"

    invoke-direct {p1, v0, v1, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p1}, Lp5/f;->c()Z

    invoke-virtual {p0}, Lm5/z0;->g1()V

    return-void
.end method

.method public static final synthetic W0(Lm5/z0;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lm5/z0;->u:Ljava/lang/String;

    return-object p0
.end method

.method private final W1()V
    .locals 4

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_CALLING_PACKAGE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lm5/e;

    if-nez v0, :cond_0

    :try_start_0
    new-instance v1, Lp4/e;

    invoke-direct {v1}, Lp4/e;-><init>()V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "EXTRA_CALLING_PACKAGE_STRING"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-class v3, Lm5/e;

    invoke-virtual {v1, v2, v3}, Lp4/e;->h(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lm5/e;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    :catch_0
    :cond_0
    if-eqz v0, :cond_1

    iput-object v0, p0, Lm5/z0;->y:Lm5/e;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lm5/z0;->y:Lm5/e;

    const-string v1, ""

    iput-object v1, v0, Lm5/e;->e:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lm5/e;->p(I)V

    iget-object v0, p0, Lm5/z0;->y:Lm5/e;

    invoke-virtual {v0, v1}, Lm5/e;->n(Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method public static final synthetic X0(Lm5/z0;)Z
    .locals 0

    invoke-direct {p0}, Lm5/z0;->K1()Z

    move-result p0

    return p0
.end method

.method private static final Z1(Lm5/z0;)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p0, p0, Lm5/z0;->G:Lw5/a;

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lw5/a;->e()V

    :cond_0
    return-void
.end method

.method private final b1()V
    .locals 3

    iget-boolean v0, p0, Lm5/z0;->F:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lm5/z0;->Q:Lcom/miui/packageInstaller/model/ApkInfo;

    if-eqz v0, :cond_2

    iget v1, p0, Lm5/z0;->w:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_2

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/ApkInfo;->getFileUri()Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    iget-object v2, p0, Lm5/z0;->Q:Lcom/miui/packageInstaller/model/ApkInfo;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/miui/packageInstaller/model/ApkInfo;->getOriginalUri()Landroid/net/Uri;

    move-result-object v1

    :cond_1
    invoke-static {v0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object v0

    new-instance v1, Lm5/o0;

    invoke-direct {v1, p0}, Lm5/o0;-><init>(Lm5/z0;)V

    invoke-virtual {v0, v1}, Lf6/z;->g(Ljava/lang/Runnable;)V

    :cond_2
    return-void
.end method

.method private final b2()V
    .locals 3

    new-instance v0, Lp5/g;

    const-string v1, "page_back_btn"

    const-string v2, "button"

    invoke-direct {v0, v1, v2, p0}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    new-instance v0, Lp5/g;

    const-string v1, "setting_btn"

    invoke-direct {v0, v1, v2, p0}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    new-instance v0, Lp5/g;

    const-string v1, "report_btn"

    invoke-direct {v0, v1, v2, p0}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    return-void
.end method

.method private static final c1(Lm5/z0;)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p0, p0, Lm5/z0;->Q:Lcom/miui/packageInstaller/model/ApkInfo;

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/miui/packageInstaller/model/ApkInfo;->getFileUri()Landroid/net/Uri;

    move-result-object p0

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object p0

    if-eqz p0, :cond_0

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    :cond_0
    return-void
.end method

.method private final d1()V
    .locals 3

    invoke-static {}, Lm5/b;->m()Ljava/util/List;

    move-result-object v0

    const-string v1, "getAliveActivities()"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    instance-of v2, v1, Lm5/z0;

    if-eqz v2, :cond_0

    invoke-static {v1, p0}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public static synthetic k2(Lm5/z0;Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;Ljava/lang/CharSequence;ILjava/lang/Object;)V
    .locals 0

    if-nez p4, :cond_3

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_2

    iget-object p2, p0, Lm5/z0;->Q:Lcom/miui/packageInstaller/model/ApkInfo;

    const/4 p3, 0x0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/miui/packageInstaller/model/ApkInfo;->isOtherVersionInstalled()Z

    move-result p2

    if-nez p2, :cond_0

    const/4 p3, 0x1

    :cond_0
    if-eqz p3, :cond_1

    const p2, 0x7f110055

    invoke-virtual {p0, p2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object p2

    const-string p3, "{\n            getString(\u2026cancel_install)\n        }"

    goto :goto_0

    :cond_1
    const p2, 0x7f110056

    invoke-virtual {p0, p2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object p2

    const-string p3, "{\n            getString(\u2026.cancel_update)\n        }"

    :goto_0
    invoke-static {p2, p3}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_2
    invoke-virtual {p0, p1, p2}, Lm5/z0;->j2(Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;Ljava/lang/CharSequence;)V

    return-void

    :cond_3
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    const-string p1, "Super calls with default arguments not supported in this target, function: setupCancelInstallButton"

    invoke-direct {p0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method private static final l2(Lm5/z0;Landroid/view/View;)V
    .locals 2

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Lp5/b;

    const-string v0, "cancel_install_btn"

    const-string v1, "button"

    invoke-direct {p1, v0, v1, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p1}, Lp5/f;->c()Z

    invoke-virtual {p0}, Lm5/z0;->g1()V

    return-void
.end method

.method private final n1()Ljava/lang/String;
    .locals 4

    iget v0, p0, Lm5/z0;->S:I

    const v1, 0x7f110055

    const/4 v2, 0x1

    if-eq v0, v2, :cond_3

    const/4 v3, 0x2

    if-eq v0, v3, :cond_2

    const/4 v3, 0x3

    if-eq v0, v3, :cond_2

    iget-object v0, p0, Lm5/z0;->Q:Lcom/miui/packageInstaller/model/ApkInfo;

    const/4 v3, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/ApkInfo;->isOtherVersionInstalled()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    move v2, v3

    :goto_0
    if-eqz v2, :cond_1

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "{\n            getString(\u2026cancel_install)\n        }"

    goto :goto_1

    :cond_1
    const v0, 0x7f110056

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "{\n            getString(\u2026.cancel_update)\n        }"

    :goto_1
    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0

    :cond_2
    const v0, 0x7f1100ad

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "getString(R.string.close\u2026kscreen_authorize_cancel)"

    :goto_2
    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0

    :cond_3
    invoke-virtual {p0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "getString(R.string.cancel_install)"

    goto :goto_2
.end method

.method private static final n2(Lm5/z0;Landroid/view/View;)V
    .locals 3

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p0, Lm5/z0;->C:Lcom/miui/packageInstaller/model/CloudParams;

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    iget-object p1, p1, Lcom/miui/packageInstaller/model/CloudParams;->positiveButtonTip:Lcom/miui/packageInstaller/model/PositiveButtonRules;

    if-eqz p1, :cond_0

    iget-object p1, p1, Lcom/miui/packageInstaller/model/PositiveButtonRules;->method:Ljava/lang/String;

    goto :goto_0

    :cond_0
    move-object p1, v0

    :goto_0
    const-string v1, "install"

    invoke-static {p1, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    const-string v2, "button"

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lm5/z0;->i1()V

    iget-object p1, p0, Lm5/z0;->C:Lcom/miui/packageInstaller/model/CloudParams;

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_1

    iget-boolean p1, p1, Lcom/miui/packageInstaller/model/CloudParams;->backgroundInstall:Z

    if-ne p1, v0, :cond_1

    goto :goto_1

    :cond_1
    move v0, v1

    :goto_1
    if-eqz v0, :cond_2

    const-string p1, "background"

    goto :goto_2

    :cond_2
    const-string p1, "front_desk"

    :goto_2
    new-instance v0, Lp5/b;

    const-string v1, "xiaomi_market_install_btn"

    invoke-direct {v0, v1, v2, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    const-string v1, "appstore_install_type"

    invoke-virtual {v0, v1, p1}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p1

    invoke-virtual {p1}, Lp5/f;->c()Z

    const-string p1, "CLICK"

    invoke-virtual {p0, p1}, Lm5/z0;->R1(Ljava/lang/String;)V

    goto :goto_4

    :cond_3
    const-string v1, "jump"

    invoke-static {p1, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_8

    iget-object p1, p0, Lm5/z0;->C:Lcom/miui/packageInstaller/model/CloudParams;

    if-eqz p1, :cond_4

    iget-object p1, p1, Lcom/miui/packageInstaller/model/CloudParams;->positiveButtonTip:Lcom/miui/packageInstaller/model/PositiveButtonRules;

    if-eqz p1, :cond_4

    iget-object p1, p1, Lcom/miui/packageInstaller/model/PositiveButtonRules;->actionUrl:Ljava/lang/String;

    goto :goto_3

    :cond_4
    move-object p1, v0

    :goto_3
    invoke-static {p0, p1}, Lq2/d;->b(Landroid/content/Context;Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_6

    iget-object p1, p0, Lm5/z0;->C:Lcom/miui/packageInstaller/model/CloudParams;

    if-eqz p1, :cond_5

    iget-object p1, p1, Lcom/miui/packageInstaller/model/CloudParams;->positiveButtonTip:Lcom/miui/packageInstaller/model/PositiveButtonRules;

    if-eqz p1, :cond_5

    iget-object v0, p1, Lcom/miui/packageInstaller/model/PositiveButtonRules;->actionUrlBackup:Ljava/lang/String;

    :cond_5
    invoke-static {p0, v0}, Lq2/d;->b(Landroid/content/Context;Ljava/lang/String;)Z

    move-result p1

    :cond_6
    if-eqz p1, :cond_7

    invoke-virtual {p0}, Lm5/z0;->g1()V

    :cond_7
    new-instance p1, Lp5/b;

    const-string v0, "search_similar_app_btn"

    invoke-direct {p1, v0, v2, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p1}, Lp5/f;->c()Z

    :cond_8
    :goto_4
    return-void
.end method

.method private final o1()Ljava/lang/String;
    .locals 6

    iget v0, p0, Lm5/z0;->S:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_5

    const/4 v2, 0x2

    if-eq v0, v2, :cond_5

    const/4 v2, 0x3

    const v3, 0x7f110381

    const v4, 0x7f1103dc

    const/4 v5, 0x0

    if-eq v0, v2, :cond_2

    iget-object v0, p0, Lm5/z0;->Q:Lcom/miui/packageInstaller/model/ApkInfo;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/ApkInfo;->isOtherVersionInstalled()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    move v1, v5

    :goto_0
    if-eqz v1, :cond_1

    invoke-virtual {p0, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "{\n            getString(\u2026.start_install)\n        }"

    goto :goto_1

    :cond_1
    invoke-virtual {p0, v4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "{\n            getString(\u2026g.update_start)\n        }"

    :goto_1
    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0

    :cond_2
    iget-object v0, p0, Lm5/z0;->Q:Lcom/miui/packageInstaller/model/ApkInfo;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/ApkInfo;->isOtherVersionInstalled()Z

    move-result v0

    if-nez v0, :cond_3

    goto :goto_2

    :cond_3
    move v1, v5

    :goto_2
    if-eqz v1, :cond_4

    invoke-virtual {p0, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "{\n                    ge\u2026nstall)\n                }"

    goto :goto_3

    :cond_4
    invoke-virtual {p0, v4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "{\n                    ge\u2026_start)\n                }"

    :goto_3
    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0

    :cond_5
    const v0, 0x7f110106

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "getString(R.string.experiment_continue)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public static synthetic p2(Lm5/z0;Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;Ljava/lang/String;ZILjava/lang/Object;)V
    .locals 0

    if-nez p5, :cond_2

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const p2, 0x7f110381

    invoke-virtual {p0, p2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object p2

    const-string p5, "getString(R.string.start_install)"

    invoke-static {p2, p5}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    :cond_1
    invoke-virtual {p0, p1, p2, p3}, Lm5/z0;->o2(Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;Ljava/lang/String;Z)V

    return-void

    :cond_2
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    const-string p1, "Super calls with default arguments not supported in this target, function: setupInstallButton"

    invoke-direct {p0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method private static final q2(Lm5/z0;Ljava/util/HashMap;Landroid/view/View;)V
    .locals 2

    const-string p2, "this$0"

    invoke-static {p0, p2}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "$map"

    invoke-static {p1, p2}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lm5/z0;->a2()V

    new-instance p2, Lp5/b;

    const-string v0, "install_btn"

    const-string v1, "button"

    invoke-direct {p2, v0, v1, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p2, p1}, Lp5/f;->g(Ljava/util/Map;)Lp5/f;

    move-result-object p0

    invoke-virtual {p0}, Lp5/f;->c()Z

    return-void
.end method

.method private static final s2(Lm5/z0;Landroid/view/View;)V
    .locals 3

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p0, Lm5/z0;->C:Lcom/miui/packageInstaller/model/CloudParams;

    if-eqz p1, :cond_0

    iget-object p1, p1, Lcom/miui/packageInstaller/model/CloudParams;->positiveButtonTip:Lcom/miui/packageInstaller/model/PositiveButtonRules;

    if-eqz p1, :cond_0

    iget-object p1, p1, Lcom/miui/packageInstaller/model/PositiveButtonRules;->method:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    const-string v0, "install"

    invoke-static {p1, v0}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    invoke-virtual {p0}, Lm5/z0;->i1()V

    iget-object p1, p0, Lm5/z0;->C:Lcom/miui/packageInstaller/model/CloudParams;

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_1

    iget-boolean p1, p1, Lcom/miui/packageInstaller/model/CloudParams;->backgroundInstall:Z

    if-ne p1, v0, :cond_1

    goto :goto_1

    :cond_1
    move v0, v1

    :goto_1
    if-eqz v0, :cond_2

    const-string p1, "background"

    goto :goto_2

    :cond_2
    const-string p1, "front_desk"

    :goto_2
    new-instance v0, Lp5/b;

    const-string v1, "xiaomi_market_install_btn"

    const-string v2, "button"

    invoke-direct {v0, v1, v2, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    const-string v1, "appstore_install_type"

    invoke-virtual {v0, v1, p1}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p1

    invoke-virtual {p1}, Lp5/f;->c()Z

    const-string p1, "CLICK"

    invoke-virtual {p0, p1}, Lm5/z0;->R1(Ljava/lang/String;)V

    :cond_3
    return-void
.end method

.method private static final t2(Lm5/z0;Landroid/view/View;)V
    .locals 2

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Lp5/b;

    const-string v0, "cancel_install_btn"

    const-string v1, "button"

    invoke-direct {p1, v0, v1, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p1}, Lp5/f;->c()Z

    invoke-virtual {p0}, Lm5/z0;->g1()V

    return-void
.end method


# virtual methods
.method public final B1(Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;Z)Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;
    .locals 4

    const-string v0, "<this>"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lm5/z0;->C:Lcom/miui/packageInstaller/model/CloudParams;

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    iget-boolean v3, v0, Lcom/miui/packageInstaller/model/CloudParams;->bundleApp:Z

    if-ne v3, v2, :cond_0

    move v1, v2

    :cond_0
    const/4 v3, 0x0

    if-eqz v1, :cond_3

    if-eqz v0, :cond_1

    iget-object v0, v0, Lcom/miui/packageInstaller/model/CloudParams;->positiveButtonTip:Lcom/miui/packageInstaller/model/PositiveButtonRules;

    if-eqz v0, :cond_1

    iget-object v0, v0, Lcom/miui/packageInstaller/model/PositiveButtonRules;->method:Ljava/lang/String;

    goto :goto_0

    :cond_1
    move-object v0, v3

    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lm5/z0;->C:Lcom/miui/packageInstaller/model/CloudParams;

    if-eqz v0, :cond_2

    iget-object v0, v0, Lcom/miui/packageInstaller/model/CloudParams;->positiveButtonTip:Lcom/miui/packageInstaller/model/PositiveButtonRules;

    if-eqz v0, :cond_2

    iget-object v0, v0, Lcom/miui/packageInstaller/model/PositiveButtonRules;->actionUrl:Ljava/lang/String;

    goto :goto_1

    :cond_2
    move-object v0, v3

    :goto_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p0, p1, p2, p3}, Lm5/z0;->a1(Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;Z)Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;

    move-result-object p1

    return-object p1

    :cond_3
    iget-object p2, p0, Lm5/z0;->C:Lcom/miui/packageInstaller/model/CloudParams;

    if-eqz p2, :cond_5

    if-eqz p2, :cond_4

    iget p2, p2, Lcom/miui/packageInstaller/model/CloudParams;->verifyAccount:I

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    goto :goto_2

    :cond_4
    move-object p2, v3

    :goto_2
    invoke-static {p2}, Lm8/i;->c(Ljava/lang/Object;)V

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p2

    if-lez p2, :cond_5

    iget-boolean p2, p0, Lm5/z0;->H:Z

    if-eqz p2, :cond_5

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->getMFirstButton()Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;

    move-result-object p2

    if-eqz p2, :cond_6

    const p3, 0x7f1103e5

    invoke-virtual {p0, p3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object p3

    const-string v0, "getString(R.string.verify_and_install)"

    goto :goto_3

    :cond_5
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->getMFirstButton()Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;

    move-result-object p2

    if-eqz p2, :cond_6

    const p3, 0x7f110381

    invoke-virtual {p0, p3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object p3

    const-string v0, "getString(R.string.start_install)"

    :goto_3
    invoke-static {p3, v0}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, p2, p3, v2}, Lm5/z0;->o2(Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;Ljava/lang/String;Z)V

    :cond_6
    iget-object p2, p0, Lm5/z0;->C:Lcom/miui/packageInstaller/model/CloudParams;

    if-eqz p2, :cond_7

    iget-object p2, p2, Lcom/miui/packageInstaller/model/CloudParams;->positiveButtonTip:Lcom/miui/packageInstaller/model/PositiveButtonRules;

    if-eqz p2, :cond_7

    iget-object p2, p2, Lcom/miui/packageInstaller/model/PositiveButtonRules;->method:Ljava/lang/String;

    goto :goto_4

    :cond_7
    move-object p2, v3

    :goto_4
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p2

    const/4 p3, 0x2

    if-nez p2, :cond_b

    iget-object p2, p0, Lm5/z0;->C:Lcom/miui/packageInstaller/model/CloudParams;

    if-eqz p2, :cond_8

    iget-object p2, p2, Lcom/miui/packageInstaller/model/CloudParams;->positiveButtonTip:Lcom/miui/packageInstaller/model/PositiveButtonRules;

    if-eqz p2, :cond_8

    iget-object p2, p2, Lcom/miui/packageInstaller/model/PositiveButtonRules;->actionUrl:Ljava/lang/String;

    goto :goto_5

    :cond_8
    move-object p2, v3

    :goto_5
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p2

    if-nez p2, :cond_b

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->getMSecondButton()Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;

    move-result-object p2

    if-eqz p2, :cond_a

    iget-object v0, p0, Lm5/z0;->C:Lcom/miui/packageInstaller/model/CloudParams;

    if-eqz v0, :cond_9

    iget-object v0, v0, Lcom/miui/packageInstaller/model/CloudParams;->positiveButtonTip:Lcom/miui/packageInstaller/model/PositiveButtonRules;

    goto :goto_6

    :cond_9
    move-object v0, v3

    :goto_6
    invoke-static {v0}, Lm8/i;->c(Ljava/lang/Object;)V

    invoke-virtual {p0, p2, v0}, Lm5/z0;->m2(Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;Lcom/miui/packageInstaller/model/PositiveButtonRules;)V

    :cond_a
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->getMThirdButton()Lcom/miui/packageInstaller/view/InstallerActionButton;

    move-result-object p2

    if-eqz p2, :cond_e

    invoke-static {p0, p2, v3, p3, v3}, Lm5/z0;->k2(Lm5/z0;Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;Ljava/lang/CharSequence;ILjava/lang/Object;)V

    goto :goto_7

    :cond_b
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->getMSecondButton()Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;

    move-result-object p2

    if-eqz p2, :cond_c

    invoke-static {p0, p2, v3, p3, v3}, Lm5/z0;->k2(Lm5/z0;Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;Ljava/lang/CharSequence;ILjava/lang/Object;)V

    :cond_c
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->getMThirdButton()Lcom/miui/packageInstaller/view/InstallerActionButton;

    move-result-object p2

    if-nez p2, :cond_d

    goto :goto_7

    :cond_d
    const/16 p3, 0x8

    invoke-virtual {p2, p3}, Landroid/widget/Button;->setVisibility(I)V

    :cond_e
    :goto_7
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->getMSecondButton()Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;

    move-result-object p2

    iget-boolean p3, p0, Lm5/z0;->L:Z

    if-eqz p3, :cond_f

    if-eqz p2, :cond_f

    invoke-virtual {p1, p2}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->b(Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;)V

    :cond_f
    return-object p2
.end method

.method public D0()Ljava/lang/String;
    .locals 1

    const-string v0, "install_prepare"

    return-object v0
.end method

.method public D1()V
    .locals 2

    new-instance v0, Lt5/x;

    invoke-direct {v0, p0}, Lt5/x;-><init>(Lm5/z0;)V

    invoke-virtual {p0, v0}, Lm5/z0;->g2(Lt5/x;)V

    new-instance v0, Lw5/a;

    iget-object v1, p0, Lm5/z0;->O:Ll6/d;

    invoke-direct {v0, p0, v1, p0}, Lw5/a;-><init>(Landroid/content/Context;Ll6/d;Lw5/a$a;)V

    iput-object v0, p0, Lm5/z0;->G:Lw5/a;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PI_DI_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Ln5/e;->a:Ln5/e;

    invoke-virtual {v1}, Ln5/e;->d()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lm5/z0;->N:Ljava/lang/String;

    return-void
.end method

.method public E0()Ljava/lang/String;
    .locals 1

    const-string v0, "install_before"

    return-object v0
.end method

.method public final E1()V
    .locals 4

    iget-object v0, p0, Lq2/b;->q:Lo5/b;

    invoke-virtual {p0}, Lm5/z0;->c()Lm5/e;

    move-result-object v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lm5/e;->j()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_0
    move-object v1, v2

    :goto_0
    const-string v3, ""

    if-nez v1, :cond_1

    move-object v1, v3

    :cond_1
    invoke-virtual {v0, v1}, Lo5/b;->q(Ljava/lang/String;)V

    iget-object v0, p0, Lq2/b;->q:Lo5/b;

    invoke-virtual {p0}, Lm5/z0;->c()Lm5/e;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v2, v1, Lm5/e;->e:Ljava/lang/String;

    :cond_2
    if-nez v2, :cond_3

    goto :goto_1

    :cond_3
    move-object v3, v2

    :goto_1
    invoke-virtual {v0, v3}, Lo5/b;->u(Ljava/lang/String;)V

    iget-object v0, p0, Lm5/z0;->M:Lo5/c;

    const-string v1, "item_type"

    const-string v2, "null"

    invoke-virtual {v0, v1, v2}, Lo5/c;->l(Ljava/lang/String;Ljava/lang/Object;)V

    iget-object v0, p0, Lm5/z0;->M:Lo5/c;

    const-string v1, "apk_channel"

    invoke-virtual {v0, v1, v2}, Lo5/c;->l(Ljava/lang/String;Ljava/lang/Object;)V

    iget-object v0, p0, Lm5/z0;->M:Lo5/c;

    const-string v1, "block_scene"

    invoke-virtual {v0, v1, v2}, Lo5/c;->l(Ljava/lang/String;Ljava/lang/Object;)V

    iget-object v0, p0, Lm5/z0;->M:Lo5/c;

    const-string v1, "category"

    invoke-virtual {v0, v1, v2}, Lo5/c;->l(Ljava/lang/String;Ljava/lang/Object;)V

    iget-object v0, p0, Lm5/z0;->M:Lo5/c;

    const-string v1, "subcategory"

    invoke-virtual {v0, v1, v2}, Lo5/c;->l(Ljava/lang/String;Ljava/lang/Object;)V

    iget-object v0, p0, Lm5/z0;->M:Lo5/c;

    const-string v1, "company"

    invoke-virtual {v0, v1, v2}, Lo5/c;->l(Ljava/lang/String;Ljava/lang/Object;)V

    iget-object v0, p0, Lm5/z0;->M:Lo5/c;

    const-string v1, "item_id"

    invoke-virtual {v0, v1, v2}, Lo5/c;->l(Ljava/lang/String;Ljava/lang/Object;)V

    iget-object v0, p0, Lm5/z0;->M:Lo5/c;

    const-string v1, "package_name"

    invoke-virtual {v0, v1, v2}, Lo5/c;->l(Ljava/lang/String;Ljava/lang/Object;)V

    iget-object v0, p0, Lm5/z0;->M:Lo5/c;

    const-string v1, "install_type"

    invoke-virtual {v0, v1, v2}, Lo5/c;->l(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method public F1()V
    .locals 7

    invoke-virtual {p0}, Lm5/z0;->E1()V

    invoke-virtual {p0}, Lm5/z0;->D1()V

    invoke-static {p0}, Lm2/b;->g(Landroid/content/Context;)Lm2/b;

    move-result-object v0

    invoke-virtual {v0}, Lm2/b;->h()Z

    move-result v0

    iput-boolean v0, p0, Lm5/z0;->H:Z

    const v0, 0x7f0a0089

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lm5/r0;

    invoke-direct {v1, p0}, Lm5/r0;-><init>(Lm5/z0;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f0a008a

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    const/4 v2, 0x1

    if-nez v1, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setAutoMirrored(Z)V

    :goto_1
    new-array v1, v2, [Landroid/view/View;

    const/4 v3, 0x0

    aput-object v0, v1, v3

    invoke-static {v1}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object v1

    invoke-interface {v1}, Lmiuix/animation/e;->c()Lmiuix/animation/j;

    move-result-object v1

    new-array v4, v3, [Lmiuix/animation/j$b;

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-interface {v1, v5, v4}, Lmiuix/animation/j;->G(F[Lmiuix/animation/j$b;)Lmiuix/animation/j;

    move-result-object v1

    new-array v4, v3, [Lmiuix/animation/j$b;

    const/high16 v6, 0x3f000000    # 0.5f

    invoke-interface {v1, v6, v4}, Lmiuix/animation/j;->d(F[Lmiuix/animation/j$b;)Lmiuix/animation/j;

    move-result-object v1

    new-array v4, v3, [Lc9/a;

    invoke-interface {v1, v0, v4}, Lmiuix/animation/j;->z(Landroid/view/View;[Lc9/a;)V

    const v0, 0x7f0a030c

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lm5/x0;

    invoke-direct {v1, p0}, Lm5/x0;-><init>(Lm5/z0;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-array v1, v2, [Landroid/view/View;

    aput-object v0, v1, v3

    invoke-static {v1}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object v1

    invoke-interface {v1}, Lmiuix/animation/e;->c()Lmiuix/animation/j;

    move-result-object v1

    new-array v4, v3, [Lmiuix/animation/j$b;

    invoke-interface {v1, v5, v4}, Lmiuix/animation/j;->G(F[Lmiuix/animation/j$b;)Lmiuix/animation/j;

    move-result-object v1

    new-array v4, v3, [Lmiuix/animation/j$b;

    invoke-interface {v1, v6, v4}, Lmiuix/animation/j;->d(F[Lmiuix/animation/j$b;)Lmiuix/animation/j;

    move-result-object v1

    new-array v4, v3, [Lc9/a;

    invoke-interface {v1, v0, v4}, Lmiuix/animation/j;->z(Landroid/view/View;[Lc9/a;)V

    const v0, 0x7f0a0162

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_3

    const-string v1, "com.xiaomi.market"

    invoke-static {p0, v1}, Lj2/f;->o(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    new-instance v1, Lm5/v0;

    invoke-direct {v1, p0}, Lm5/v0;-><init>(Lm5/z0;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-array v1, v2, [Landroid/view/View;

    aput-object v0, v1, v3

    invoke-static {v1}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object v1

    invoke-interface {v1}, Lmiuix/animation/e;->c()Lmiuix/animation/j;

    move-result-object v1

    new-array v2, v3, [Lmiuix/animation/j$b;

    invoke-interface {v1, v5, v2}, Lmiuix/animation/j;->G(F[Lmiuix/animation/j$b;)Lmiuix/animation/j;

    move-result-object v1

    new-array v2, v3, [Lmiuix/animation/j$b;

    invoke-interface {v1, v6, v2}, Lmiuix/animation/j;->d(F[Lmiuix/animation/j$b;)Lmiuix/animation/j;

    move-result-object v1

    new-array v2, v3, [Lc9/a;

    invoke-interface {v1, v0, v2}, Lmiuix/animation/j;->z(Landroid/view/View;[Lc9/a;)V

    :cond_3
    :goto_2
    return-void
.end method

.method public G()Lcom/miui/packageInstaller/model/ApkInfo;
    .locals 1

    iget-object v0, p0, Lm5/z0;->Q:Lcom/miui/packageInstaller/model/ApkInfo;

    return-object v0
.end method

.method public final J1(Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;)V
    .locals 2

    const-string p2, "<this>"

    invoke-static {p1, p2}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->getMFirstButton()Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;

    move-result-object p2

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    invoke-interface {p2}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;->a()Landroid/view/View;

    move-result-object p2

    goto :goto_0

    :cond_0
    move-object p2, v0

    :goto_0
    if-nez p2, :cond_1

    goto :goto_1

    :cond_1
    const/16 v1, 0x8

    invoke-virtual {p2, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_1
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->getMSecondButton()Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;

    move-result-object p1

    if-eqz p1, :cond_2

    const/4 p2, 0x2

    invoke-static {p0, p1, v0, p2, v0}, Lm5/z0;->k2(Lm5/z0;Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;Ljava/lang/CharSequence;ILjava/lang/Object;)V

    :cond_2
    const/4 p1, 0x1

    iput-boolean p1, p0, Lm5/z0;->A:Z

    iput-boolean p1, p0, Lm5/z0;->L:Z

    return-void
.end method

.method public final L1()Z
    .locals 1

    iget-boolean v0, p0, Lm5/z0;->D:Z

    return v0
.end method

.method public M(Lz5/a;)V
    .locals 1

    const-string v0, "listener"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lm5/z0;->P:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public final M1()Z
    .locals 1

    iget-boolean v0, p0, Lm5/z0;->A:Z

    return v0
.end method

.method public N(Ljava/lang/String;)Lo5/c;
    .locals 0

    iget-object p1, p0, Lm5/z0;->M:Lo5/c;

    return-object p1
.end method

.method public final N1(I)V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/miui/packageInstaller/AppDesImageActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lm5/z0;->C:Lcom/miui/packageInstaller/model/CloudParams;

    if-eqz v1, :cond_0

    iget-object v1, v1, Lcom/miui/packageInstaller/model/CloudParams;->appInfo:Lcom/miui/packageInstaller/model/MarketAppInfo;

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    const-string v2, "appImageUrls"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v1, "image_position"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public O()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lm5/z0;->A:Z

    return-void
.end method

.method public final O1(Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;)V
    .locals 4

    const-string v0, "<this>"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lm5/z0;->C:Lcom/miui/packageInstaller/model/CloudParams;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    iget-boolean v3, v0, Lcom/miui/packageInstaller/model/CloudParams;->storeListed:Z

    if-ne v3, v1, :cond_0

    move v3, v1

    goto :goto_0

    :cond_0
    move v3, v2

    :goto_0
    if-eqz v3, :cond_3

    sget-object v0, Lt5/n;->b:Lt5/n$a;

    invoke-virtual {v0}, Lt5/n$a;->b()Lt5/n;

    move-result-object v0

    const-string v3, "installOption"

    invoke-virtual {v0, v3}, Lt5/n;->d(Ljava/lang/String;)Lt5/l;

    move-result-object v0

    const-string v3, "installOptionStyle"

    invoke-virtual {v0, v3, v2}, Lt5/l;->a(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lm5/z0;->S:I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "experimentType = "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p0, Lm5/z0;->S:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v3, "LBB"

    invoke-static {v3, v0}, Lf6/o;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    iget-object v0, p0, Lm5/z0;->C:Lcom/miui/packageInstaller/model/CloudParams;

    if-eqz v0, :cond_1

    iget-boolean v0, v0, Lcom/miui/packageInstaller/model/CloudParams;->bundleApp:Z

    if-ne v0, v1, :cond_1

    goto :goto_1

    :cond_1
    move v1, v2

    :goto_1
    if-eqz v1, :cond_2

    goto :goto_3

    :cond_2
    iget v0, p0, Lm5/z0;->S:I

    invoke-virtual {p1, v0}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->d(I)V

    goto :goto_4

    :cond_3
    if-eqz v0, :cond_4

    iget-boolean v0, v0, Lcom/miui/packageInstaller/model/CloudParams;->bundleApp:Z

    if-ne v0, v1, :cond_4

    goto :goto_2

    :cond_4
    move v1, v2

    :goto_2
    if-eqz v1, :cond_5

    :goto_3
    invoke-virtual {p0, p1}, Lm5/z0;->Q1(Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;)V

    goto :goto_4

    :cond_5
    invoke-virtual {p1, v2}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->d(I)V

    :goto_4
    return-void
.end method

.method public final P1()V
    .locals 6

    invoke-static {p0}, Landroidx/lifecycle/k;->a(Landroidx/lifecycle/j;)Landroidx/lifecycle/e;

    move-result-object v0

    invoke-static {}, Lv8/t0;->c()Lv8/v1;

    move-result-object v1

    new-instance v3, Lm5/z0$c;

    const/4 v2, 0x0

    invoke-direct {v3, p0, v2}, Lm5/z0$c;-><init>(Lm5/z0;Ld8/d;)V

    const/4 v4, 0x2

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lv8/f;->d(Lv8/e0;Ld8/g;Lv8/g0;Ll8/p;ILjava/lang/Object;)Lv8/k1;

    return-void
.end method

.method public final Q1(Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;)V
    .locals 3

    const-string v0, "<this>"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const v0, 0x7f0d0065

    goto :goto_0

    :cond_0
    const v0, 0x7f0d0064

    goto :goto_0

    :cond_1
    const v0, 0x7f0d0063

    :goto_0
    invoke-virtual {p1}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    const-string v1, "from(context).inflate(resourceId, null)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->e(Landroid/view/View;)V

    return-void
.end method

.method public final R1(Ljava/lang/String;)V
    .locals 3

    const-string v0, "event"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lm5/z0;->C:Lcom/miui/packageInstaller/model/CloudParams;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/miui/packageInstaller/model/CloudParams;->incrementPackageInfo:Lcom/miui/packageInstaller/model/HasIncrement;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/HasIncrement;->getEx()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    new-instance v0, Lcom/miui/packageInstaller/model/AdData;

    invoke-direct {v0}, Lcom/miui/packageInstaller/model/AdData;-><init>()V

    iget-object v2, p0, Lm5/z0;->C:Lcom/miui/packageInstaller/model/CloudParams;

    if-eqz v2, :cond_1

    iget-object v2, v2, Lcom/miui/packageInstaller/model/CloudParams;->incrementPackageInfo:Lcom/miui/packageInstaller/model/HasIncrement;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/miui/packageInstaller/model/HasIncrement;->getEx()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_1
    move-object v2, v1

    :goto_1
    invoke-static {v2}, Lm8/i;->c(Ljava/lang/Object;)V

    invoke-virtual {v0, v2}, Lcom/miui/packageInstaller/model/AdData;->setEx(Ljava/lang/String;)V

    iget-object v2, p0, Lm5/z0;->C:Lcom/miui/packageInstaller/model/CloudParams;

    if-eqz v2, :cond_2

    iget-object v2, v2, Lcom/miui/packageInstaller/model/CloudParams;->incrementPackageInfo:Lcom/miui/packageInstaller/model/HasIncrement;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/miui/packageInstaller/model/HasIncrement;->getClickMonitorUrls()[Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v0, v2}, Lcom/miui/packageInstaller/model/AdData;->setClickMonitorUrls([Ljava/lang/String;)V

    :cond_2
    iget-object v2, p0, Lm5/z0;->C:Lcom/miui/packageInstaller/model/CloudParams;

    if-eqz v2, :cond_3

    iget-object v2, v2, Lcom/miui/packageInstaller/model/CloudParams;->incrementPackageInfo:Lcom/miui/packageInstaller/model/HasIncrement;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lcom/miui/packageInstaller/model/HasIncrement;->getViewMonitorUrls()[Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v0, v2}, Lcom/miui/packageInstaller/model/AdData;->setViewMonitorUrls([Ljava/lang/String;)V

    :cond_3
    iget-object v2, p0, Lm5/z0;->M:Lo5/c;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Lo5/c;->p()Ljava/lang/String;

    move-result-object v1

    :cond_4
    invoke-static {p1, v0, v1}, Lk2/a;->c(Ljava/lang/String;Lcom/miui/packageInstaller/model/AdInterface;Ljava/lang/String;)V

    :cond_5
    return-void
.end method

.method public S1()V
    .locals 1

    iget-object v0, p0, Lm5/z0;->G:Lw5/a;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lw5/a;->e()V

    :cond_0
    return-void
.end method

.method public final T1(Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;)V
    .locals 4

    const-string v0, "<this>"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->getMFirstButton()Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v1, p0, Lm5/z0;->C:Lcom/miui/packageInstaller/model/CloudParams;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    iget-object v1, v1, Lcom/miui/packageInstaller/model/CloudParams;->positiveButtonTip:Lcom/miui/packageInstaller/model/PositiveButtonRules;

    if-eqz v1, :cond_0

    iget-object v1, v1, Lcom/miui/packageInstaller/model/PositiveButtonRules;->method:Ljava/lang/String;

    goto :goto_0

    :cond_0
    move-object v1, v2

    :goto_0
    const-string v3, "jump"

    invoke-static {v1, v3}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lm5/z0;->C:Lcom/miui/packageInstaller/model/CloudParams;

    if-eqz v1, :cond_1

    iget-object v1, v1, Lcom/miui/packageInstaller/model/CloudParams;->positiveButtonTip:Lcom/miui/packageInstaller/model/PositiveButtonRules;

    if-eqz v1, :cond_1

    iget-object v2, v1, Lcom/miui/packageInstaller/model/PositiveButtonRules;->text:Ljava/lang/String;

    :cond_1
    invoke-interface {v0, v2}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;->setButtonText(Ljava/lang/CharSequence;)V

    new-instance v1, Lm5/n0;

    invoke-direct {v1, p0}, Lm5/n0;-><init>(Lm5/z0;)V

    invoke-interface {v0, v1}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;->setClick(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    :cond_2
    invoke-interface {v0}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;->a()Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_3
    :goto_1
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->getMSecondButton()Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;

    move-result-object p1

    if-eqz p1, :cond_4

    const v0, 0x7f1100b5

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;->setButtonText(Ljava/lang/CharSequence;)V

    invoke-interface {p1}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;->a()Landroid/view/View;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    new-instance v0, Lm5/s0;

    invoke-direct {v0, p0}, Lm5/s0;-><init>(Lm5/z0;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_4
    return-void
.end method

.method public X1()Ljava/lang/String;
    .locals 4

    iget-object v0, p0, Lm5/z0;->C:Lcom/miui/packageInstaller/model/CloudParams;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    iget-object v2, v0, Lcom/miui/packageInstaller/model/CloudParams;->secureInstallTip:Lcom/miui/packageInstaller/model/Tips;

    if-eqz v2, :cond_1

    iget-object v2, v2, Lcom/miui/packageInstaller/model/Tips;->text:Ljava/lang/String;

    goto :goto_0

    :cond_1
    move-object v2, v1

    :goto_0
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    return-object v1

    :cond_2
    iget-boolean v3, v0, Lcom/miui/packageInstaller/model/CloudParams;->backgroundInstall:Z

    if-eqz v3, :cond_3

    iget-boolean v0, v0, Lcom/miui/packageInstaller/model/CloudParams;->bundleApp:Z

    if-nez v0, :cond_3

    return-object v2

    :cond_3
    return-object v1
.end method

.method public Y0(Landroid/content/Intent;)V
    .locals 3

    const-string v0, "intent"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lm5/z0;->Q:Lcom/miui/packageInstaller/model/ApkInfo;

    const-string v1, "apk_info"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-virtual {p0}, Lm5/z0;->c()Lm5/e;

    move-result-object v0

    const-string v1, "caller"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    iget-object v0, p0, Lm5/z0;->B:Lcom/miui/packageInstaller/model/Virus;

    const-string v1, "virus_data"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    iget-object v0, p0, Lm5/z0;->T:Ljava/lang/String;

    const-string v1, "installId"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Lq2/b;->q:Lo5/b;

    const-string v1, "fromPage"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    iget-object v0, p0, Lm5/z0;->M:Lo5/c;

    const-string v1, "static_params_package"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "android.intent.extra.RETURN_RESULT"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/high16 v0, 0x2000000

    invoke-virtual {p1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "android.intent.extra.INSTALLER_PACKAGE_NAME"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_1
    return-void
.end method

.method public Y1(Lcom/miui/packageInstaller/model/ApkInfo;I)V
    .locals 4

    if-eqz p1, :cond_2

    sget-object p2, Lt5/r;->a:Lt5/r;

    iget-object v0, p0, Lm5/z0;->y:Lm5/e;

    invoke-virtual {v0}, Lm5/e;->j()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mCallingPackage.callingPackage"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/model/ApkInfo;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    if-nez v1, :cond_0

    move-object v1, v2

    :cond_0
    invoke-virtual {p1}, Lcom/miui/packageInstaller/model/ApkInfo;->getLabel()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_1

    goto :goto_0

    :cond_1
    move-object v2, v3

    :goto_0
    invoke-virtual {p2, v0, v1, v2}, Lt5/r;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    iput-object p2, p0, Lm5/z0;->T:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/miui/packageInstaller/model/ApkInfo;->getLabel()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1}, Lcom/miui/packageInstaller/model/ApkInfo;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/miui/packageInstaller/model/ApkInfo;->getVersionCode()I

    move-result v1

    invoke-static {p2, v0, v1}, Lf6/p;->b(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/model/ApkInfo;->getPackageName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p2}, Lm5/z0;->Z0(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/model/ApkInfo;->getPackageName()Ljava/lang/String;

    move-result-object p2

    sput-object p2, Lcom/android/packageinstaller/InstallerApplication;->d:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/miui/packageInstaller/model/ApkInfo;->getApkMd5()Ljava/lang/String;

    move-result-object p2

    sput-object p2, Lcom/android/packageinstaller/InstallerApplication;->e:Ljava/lang/String;

    iget-object p2, p0, Lm5/z0;->M:Lo5/c;

    invoke-virtual {p1}, Lcom/miui/packageInstaller/model/ApkInfo;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "package_name"

    invoke-virtual {p2, v1, v0}, Lo5/c;->l(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {p0}, Lm5/z0;->t1()Lt5/x;

    move-result-object p2

    new-instance v0, Lm5/z0$d;

    invoke-direct {v0, p0, p1}, Lm5/z0$d;-><init>(Lm5/z0;Lcom/miui/packageInstaller/model/ApkInfo;)V

    invoke-virtual {p2, p1, v0}, Lt5/x;->m(Lcom/miui/packageInstaller/model/ApkInfo;Ll8/a;)V

    goto :goto_1

    :cond_2
    new-instance p1, Lt5/g;

    invoke-direct {p1, p0}, Lt5/g;-><init>(Lm5/z0;)V

    invoke-virtual {p1, p2}, Lt5/g;->d(I)V

    :goto_1
    return-void
.end method

.method public final Z0(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lq2/b;->q:Lo5/b;

    iget-object v1, p0, Lm5/z0;->Q:Lcom/miui/packageInstaller/model/ApkInfo;

    const-string v2, ""

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/ApkInfo;->getLabel()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    move-object v1, v2

    :cond_1
    invoke-virtual {v0, v1}, Lo5/b;->g(Ljava/lang/String;)V

    iget-object v0, p0, Lq2/b;->q:Lo5/b;

    if-nez p1, :cond_2

    move-object p1, v2

    :cond_2
    invoke-virtual {v0, p1}, Lo5/b;->h(Ljava/lang/String;)V

    iget-object p1, p0, Lq2/b;->q:Lo5/b;

    iget-object v0, p0, Lm5/z0;->Q:Lcom/miui/packageInstaller/model/ApkInfo;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/ApkInfo;->getVersionName()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    goto :goto_0

    :cond_3
    move-object v2, v0

    :cond_4
    :goto_0
    invoke-virtual {p1, v2}, Lo5/b;->w(Ljava/lang/String;)V

    return-void
.end method

.method public a1(Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;Z)Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;
    .locals 1

    const-string p3, "<this>"

    invoke-static {p1, p3}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p3, p0, Lm5/z0;->C:Lcom/miui/packageInstaller/model/CloudParams;

    if-eqz p3, :cond_4

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->getMFirstButton()Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;

    move-result-object p2

    iget-boolean v0, p3, Lcom/miui/packageInstaller/model/CloudParams;->storeListed:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lm5/z0;->r2(Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p1}, Lm5/z0;->T1(Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;)V

    :goto_0
    iget-boolean p3, p3, Lcom/miui/packageInstaller/model/CloudParams;->storeListed:Z

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->getTvTips()Landroid/widget/TextView;

    move-result-object p1

    if-eqz p3, :cond_2

    if-nez p1, :cond_1

    goto :goto_2

    :cond_1
    const/4 p3, 0x0

    goto :goto_1

    :cond_2
    if-nez p1, :cond_3

    goto :goto_2

    :cond_3
    const/16 p3, 0x8

    :goto_1
    invoke-virtual {p1, p3}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_4
    :goto_2
    return-object p2
.end method

.method public a2()V
    .locals 5

    new-instance v0, Lt5/a0;

    invoke-direct {v0, p0}, Lt5/a0;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lm5/z0;->C:Lcom/miui/packageInstaller/model/CloudParams;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    iget-boolean v4, v1, Lcom/miui/packageInstaller/model/CloudParams;->useSystemAppRules:Z

    if-ne v4, v2, :cond_0

    goto :goto_0

    :cond_0
    move v2, v3

    :goto_0
    if-nez v2, :cond_5

    iget-object v2, p0, Lm5/z0;->B:Lcom/miui/packageInstaller/model/Virus;

    if-eqz v2, :cond_1

    new-instance v1, Lt5/n0;

    iget-object v2, p0, Lm5/z0;->Q:Lcom/miui/packageInstaller/model/ApkInfo;

    invoke-static {v2}, Lm8/i;->c(Ljava/lang/Object;)V

    iget-object v3, p0, Lm5/z0;->B:Lcom/miui/packageInstaller/model/Virus;

    invoke-static {v3}, Lm8/i;->c(Ljava/lang/Object;)V

    iget-object v4, p0, Lm5/z0;->y:Lm5/e;

    invoke-direct {v1, p0, v2, v3, v4}, Lt5/n0;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/ApkInfo;Lcom/miui/packageInstaller/model/Virus;Lm5/e;)V

    :goto_1
    invoke-virtual {v0, v1}, Lt5/a0;->c(Lt5/g0;)V

    goto :goto_3

    :cond_1
    if-eqz v1, :cond_5

    invoke-static {p0}, Lm2/b;->g(Landroid/content/Context;)Lm2/b;

    move-result-object v1

    invoke-virtual {v1}, Lm2/b;->h()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lm5/z0;->C:Lcom/miui/packageInstaller/model/CloudParams;

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    iget-object v3, v1, Lcom/miui/packageInstaller/model/CloudParams;->rc:Lcom/miui/packageInstaller/model/RiskControlConfig;

    goto :goto_2

    :cond_2
    move-object v3, v2

    :goto_2
    if-eqz v3, :cond_4

    new-instance v3, Lt5/j0;

    if-eqz v1, :cond_3

    iget-object v2, v1, Lcom/miui/packageInstaller/model/CloudParams;->rc:Lcom/miui/packageInstaller/model/RiskControlConfig;

    :cond_3
    invoke-static {v2}, Lm8/i;->c(Ljava/lang/Object;)V

    iget-object v1, p0, Lm5/z0;->Q:Lcom/miui/packageInstaller/model/ApkInfo;

    iget-object v4, p0, Lm5/z0;->y:Lm5/e;

    invoke-direct {v3, p0, v2, v1, v4}, Lt5/j0;-><init>(Lq2/b;Lcom/miui/packageInstaller/model/RiskControlConfig;Lcom/miui/packageInstaller/model/ApkInfo;Lm5/e;)V

    invoke-virtual {v0, v3}, Lt5/a0;->c(Lt5/g0;)V

    goto :goto_3

    :cond_4
    new-instance v1, Lt5/y;

    invoke-virtual {p0}, Lm5/z0;->c()Lm5/e;

    move-result-object v2

    invoke-static {v2}, Lm8/i;->c(Ljava/lang/Object;)V

    iget-object v3, p0, Lm5/z0;->Q:Lcom/miui/packageInstaller/model/ApkInfo;

    invoke-static {v3}, Lm8/i;->c(Ljava/lang/Object;)V

    iget-object v4, p0, Lm5/z0;->C:Lcom/miui/packageInstaller/model/CloudParams;

    invoke-static {v4}, Lm8/i;->c(Ljava/lang/Object;)V

    invoke-direct {v1, p0, v2, v3, v4}, Lt5/y;-><init>(Landroid/content/Context;Lm5/e;Lcom/miui/packageInstaller/model/ApkInfo;Lcom/miui/packageInstaller/model/CloudParams;)V

    goto :goto_1

    :cond_5
    :goto_3
    iget-object v1, p0, Lq2/b;->q:Lo5/b;

    invoke-virtual {v1}, Lo5/b;->d()V

    new-instance v1, Lp5/g;

    const-string v2, "virus_cue_popup"

    const-string v3, "popup"

    invoke-direct {v1, v2, v3, p0}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v1}, Lp5/f;->c()Z

    new-instance v1, Lm5/z0$e;

    invoke-direct {v1, p0}, Lm5/z0$e;-><init>(Lm5/z0;)V

    invoke-virtual {v0, v1}, Lt5/a0;->a(Lt5/g0$a;)V

    return-void
.end method

.method public c()Lm5/e;
    .locals 1

    iget-object v0, p0, Lm5/z0;->y:Lm5/e;

    return-object v0
.end method

.method public final c2(Z)V
    .locals 0

    iput-boolean p1, p0, Lm5/z0;->L:Z

    return-void
.end method

.method public final d2(Z)V
    .locals 0

    iput-boolean p1, p0, Lm5/z0;->E:Z

    return-void
.end method

.method public e1()V
    .locals 0

    return-void
.end method

.method public final e2(Z)V
    .locals 0

    iput-boolean p1, p0, Lm5/z0;->D:Z

    return-void
.end method

.method public final f1()V
    .locals 5

    iget-object v0, p0, Lm5/z0;->C:Lcom/miui/packageInstaller/model/CloudParams;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    iget-boolean v3, v0, Lcom/miui/packageInstaller/model/CloudParams;->bundleApp:Z

    if-ne v3, v1, :cond_0

    goto :goto_0

    :cond_0
    move v1, v2

    :goto_0
    const/4 v2, 0x0

    if-eqz v1, :cond_1

    if-eqz v0, :cond_2

    iget-object v0, v0, Lcom/miui/packageInstaller/model/CloudParams;->appInfo:Lcom/miui/packageInstaller/model/MarketAppInfo;

    if-eqz v0, :cond_2

    iget-object v0, v0, Lcom/miui/packageInstaller/model/MarketAppInfo;->packageName:Ljava/lang/String;

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lm5/z0;->Q:Lcom/miui/packageInstaller/model/ApkInfo;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/ApkInfo;->getPackageName()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    move-object v0, v2

    :goto_1
    if-nez v0, :cond_3

    return-void

    :cond_3
    iget-object v1, p0, Lm5/z0;->C:Lcom/miui/packageInstaller/model/CloudParams;

    if-eqz v1, :cond_7

    iget-object v1, v1, Lcom/miui/packageInstaller/model/CloudParams;->positiveButtonTip:Lcom/miui/packageInstaller/model/PositiveButtonRules;

    if-eqz v1, :cond_7

    iget-object v1, v1, Lcom/miui/packageInstaller/model/PositiveButtonRules;->actionUrl:Ljava/lang/String;

    if-nez v1, :cond_4

    goto :goto_2

    :cond_4
    new-instance v1, Ls5/l$a;

    sget-object v3, Lcom/android/packageinstaller/InstallerApplication;->c:Lcom/android/packageinstaller/InstallerApplication;

    const-string v4, "sInstance"

    invoke-static {v3, v4}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v1, v3}, Ls5/l$a;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Ls5/l$a;->c()Ls5/n$a;

    move-result-object v1

    invoke-virtual {v1, v0}, Ls5/n$a;->c(Ljava/lang/String;)Ls5/n$a;

    move-result-object v0

    iget-object v1, p0, Lm5/z0;->y:Lm5/e;

    invoke-virtual {v1}, Lm5/e;->j()Ljava/lang/String;

    move-result-object v1

    const-string v3, "mCallingPackage.callingPackage"

    invoke-static {v1, v3}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ls5/n$a;->d(Ljava/lang/String;)Ls5/n$a;

    move-result-object v0

    iget-object v1, p0, Lm5/z0;->C:Lcom/miui/packageInstaller/model/CloudParams;

    if-eqz v1, :cond_5

    iget-object v1, v1, Lcom/miui/packageInstaller/model/CloudParams;->positiveButtonTip:Lcom/miui/packageInstaller/model/PositiveButtonRules;

    if-eqz v1, :cond_5

    iget-object v2, v1, Lcom/miui/packageInstaller/model/PositiveButtonRules;->actionUrl:Ljava/lang/String;

    :cond_5
    if-nez v2, :cond_6

    const-string v2, ""

    :cond_6
    invoke-virtual {v0, v2}, Ls5/n$a;->b(Ljava/lang/String;)Ls5/n$a;

    move-result-object v0

    invoke-virtual {v0}, Ls5/n$a;->a()Ls5/l;

    move-result-object v0

    new-instance v1, Lm5/z0$b;

    invoke-direct {v1, p0}, Lm5/z0$b;-><init>(Lm5/z0;)V

    invoke-virtual {v0, v1}, Ls5/l;->m(Ls5/l$c;)V

    invoke-virtual {v0}, Ls5/l;->n()V

    :cond_7
    :goto_2
    return-void
.end method

.method public final f2(Lcom/miui/packageInstaller/model/ApkInfo;)V
    .locals 0

    iput-object p1, p0, Lm5/z0;->Q:Lcom/miui/packageInstaller/model/ApkInfo;

    return-void
.end method

.method public final g1()V
    .locals 4

    iget v0, p0, Lm5/z0;->w:I

    const/4 v1, 0x0

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    iget-object v2, p0, Lm5/z0;->v:Landroid/content/pm/PackageInstaller;

    invoke-static {v2, v0, v1}, Lcom/android/packageinstaller/compat/PackageInstallerCompat;->setPermissionsResult(Landroid/content/pm/PackageInstaller;IZ)V

    :cond_0
    iget-object v0, p0, Lm5/z0;->y:Lm5/e;

    if-eqz v0, :cond_1

    iget-object v2, p0, Lm5/z0;->Q:Lcom/miui/packageInstaller/model/ApkInfo;

    if-eqz v2, :cond_1

    sget-object v3, Lf6/h;->a:Lf6/h;

    invoke-static {v2}, Lm8/i;->c(Ljava/lang/Object;)V

    invoke-virtual {v3, v0, v2, v1}, Lf6/h;->h(Lm5/e;Lcom/miui/packageInstaller/model/ApkInfo;Z)V

    :cond_1
    sget-object v0, Lt5/r;->a:Lt5/r;

    iget-object v1, p0, Lm5/z0;->T:Ljava/lang/String;

    const-string v2, "canceled"

    invoke-virtual {v0, v1, v2}, Lt5/r;->j(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lmiuix/appcompat/app/j;->finish()V

    return-void
.end method

.method public final g2(Lt5/x;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lm5/z0;->z:Lt5/x;

    return-void
.end method

.method public h(Lz5/a;)V
    .locals 1

    const-string v0, "listener"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lm5/z0;->P:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lm5/z0;->P:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public final h1()V
    .locals 4

    iget v0, p0, Lm5/z0;->w:I

    const/4 v1, 0x1

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    iget-object v2, p0, Lm5/z0;->v:Landroid/content/pm/PackageInstaller;

    invoke-static {v2, v0, v1}, Lcom/android/packageinstaller/compat/PackageInstallerCompat;->setPermissionsResult(Landroid/content/pm/PackageInstaller;IZ)V

    invoke-virtual {p0}, Lmiuix/appcompat/app/j;->finish()V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lm5/z0;->y1()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lm5/z0;->Y0(Landroid/content/Intent;)V

    const/4 v2, 0x0

    const-string v3, "installType"

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v2, p0, Lm5/z0;->M:Lo5/c;

    const-string v3, "static_params_package"

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lmiuix/appcompat/app/j;->finish()V

    iput-boolean v1, p0, Lm5/z0;->F:Z

    :goto_0
    return-void
.end method

.method public final h2(I)V
    .locals 0

    iput p1, p0, Lm5/z0;->K:I

    return-void
.end method

.method public i1()V
    .locals 4

    iget-object v0, p0, Lm5/z0;->C:Lcom/miui/packageInstaller/model/CloudParams;

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    iget-boolean v0, v0, Lcom/miui/packageInstaller/model/CloudParams;->backgroundInstall:Z

    if-ne v0, v2, :cond_0

    move v0, v2

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lm5/z0;->f1()V

    return-void

    :cond_1
    invoke-virtual {p0}, Lm5/z0;->y1()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "installType"

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lm5/z0;->Y0(Landroid/content/Intent;)V

    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    iget v0, p0, Lm5/z0;->w:I

    const/4 v3, -0x1

    if-eq v0, v3, :cond_2

    iget-object v3, p0, Lm5/z0;->v:Landroid/content/pm/PackageInstaller;

    invoke-static {v3, v0, v1}, Lcom/android/packageinstaller/compat/PackageInstallerCompat;->setPermissionsResult(Landroid/content/pm/PackageInstaller;IZ)V

    :cond_2
    iput-boolean v2, p0, Lm5/z0;->F:Z

    invoke-virtual {p0}, Lmiuix/appcompat/app/j;->finish()V

    return-void
.end method

.method public final i2(Z)V
    .locals 0

    iput-boolean p1, p0, Lm5/z0;->I:Z

    return-void
.end method

.method public final j1()V
    .locals 6

    iget-object v0, p0, Lm5/z0;->Q:Lcom/miui/packageInstaller/model/ApkInfo;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/ApkInfo;->getCloudParams()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    if-eqz v0, :cond_1c

    iget-object v0, p0, Lq2/b;->q:Lo5/b;

    iget-object v2, p0, Lm5/z0;->Q:Lcom/miui/packageInstaller/model/ApkInfo;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/miui/packageInstaller/model/ApkInfo;->getCloudParams()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, v2, Lcom/miui/packageInstaller/model/CloudParams;->expId:Ljava/lang/String;

    goto :goto_1

    :cond_1
    move-object v2, v1

    :goto_1
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lo5/b;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lm5/z0;->Q:Lcom/miui/packageInstaller/model/ApkInfo;

    invoke-static {v0}, Lm8/i;->c(Ljava/lang/Object;)V

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/ApkInfo;->getCloudParams()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v0

    invoke-static {v0}, Lm8/i;->c(Ljava/lang/Object;)V

    iget-object v0, v0, Lcom/miui/packageInstaller/model/CloudParams;->appInfo:Lcom/miui/packageInstaller/model/MarketAppInfo;

    if-eqz v0, :cond_2

    iget-object v0, v0, Lcom/miui/packageInstaller/model/MarketAppInfo;->publisherName:Ljava/lang/String;

    goto :goto_2

    :cond_2
    move-object v0, v1

    :goto_2
    if-eqz v0, :cond_4

    iget-object v0, p0, Lm5/z0;->M:Lo5/c;

    iget-object v2, p0, Lm5/z0;->Q:Lcom/miui/packageInstaller/model/ApkInfo;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lcom/miui/packageInstaller/model/ApkInfo;->getCloudParams()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v2

    if-eqz v2, :cond_3

    iget-object v2, v2, Lcom/miui/packageInstaller/model/CloudParams;->appInfo:Lcom/miui/packageInstaller/model/MarketAppInfo;

    if-eqz v2, :cond_3

    iget-object v2, v2, Lcom/miui/packageInstaller/model/MarketAppInfo;->publisherName:Ljava/lang/String;

    goto :goto_3

    :cond_3
    move-object v2, v1

    :goto_3
    const-string v3, "company"

    invoke-virtual {v0, v3, v2}, Lo5/c;->l(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_4
    iget-object v0, p0, Lm5/z0;->M:Lo5/c;

    iget-object v2, p0, Lm5/z0;->Q:Lcom/miui/packageInstaller/model/ApkInfo;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Lcom/miui/packageInstaller/model/ApkInfo;->getCloudParams()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v2

    if-eqz v2, :cond_5

    iget-object v2, v2, Lcom/miui/packageInstaller/model/CloudParams;->appInfo:Lcom/miui/packageInstaller/model/MarketAppInfo;

    if-eqz v2, :cond_5

    iget-object v2, v2, Lcom/miui/packageInstaller/model/MarketAppInfo;->level1Category:Ljava/lang/String;

    goto :goto_4

    :cond_5
    move-object v2, v1

    :goto_4
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "category"

    invoke-virtual {v0, v3, v2}, Lo5/c;->l(Ljava/lang/String;Ljava/lang/Object;)V

    iget-object v0, p0, Lm5/z0;->M:Lo5/c;

    iget-object v2, p0, Lm5/z0;->Q:Lcom/miui/packageInstaller/model/ApkInfo;

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Lcom/miui/packageInstaller/model/ApkInfo;->getCloudParams()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v2

    if-eqz v2, :cond_6

    iget-object v2, v2, Lcom/miui/packageInstaller/model/CloudParams;->appInfo:Lcom/miui/packageInstaller/model/MarketAppInfo;

    if-eqz v2, :cond_6

    iget-object v2, v2, Lcom/miui/packageInstaller/model/MarketAppInfo;->level2Category:Ljava/lang/String;

    goto :goto_5

    :cond_6
    move-object v2, v1

    :goto_5
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "subcategory"

    invoke-virtual {v0, v3, v2}, Lo5/c;->l(Ljava/lang/String;Ljava/lang/Object;)V

    iget-object v0, p0, Lm5/z0;->M:Lo5/c;

    iget-object v2, p0, Lm5/z0;->Q:Lcom/miui/packageInstaller/model/ApkInfo;

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Lcom/miui/packageInstaller/model/ApkInfo;->getCloudParams()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v2

    if-eqz v2, :cond_7

    iget-object v2, v2, Lcom/miui/packageInstaller/model/CloudParams;->channel:Ljava/lang/String;

    goto :goto_6

    :cond_7
    move-object v2, v1

    :goto_6
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "apk_channel"

    invoke-virtual {v0, v3, v2}, Lo5/c;->l(Ljava/lang/String;Ljava/lang/Object;)V

    iget-object v0, p0, Lm5/z0;->M:Lo5/c;

    iget-object v2, p0, Lm5/z0;->Q:Lcom/miui/packageInstaller/model/ApkInfo;

    if-eqz v2, :cond_8

    invoke-virtual {v2}, Lcom/miui/packageInstaller/model/ApkInfo;->getVersionCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto :goto_7

    :cond_8
    move-object v2, v1

    :goto_7
    const-string v3, "app_version"

    invoke-virtual {v0, v3, v2}, Lo5/c;->l(Ljava/lang/String;Ljava/lang/Object;)V

    iget-object v0, p0, Lm5/z0;->M:Lo5/c;

    iget-object v2, p0, Lm5/z0;->Q:Lcom/miui/packageInstaller/model/ApkInfo;

    if-eqz v2, :cond_9

    invoke-virtual {v2}, Lcom/miui/packageInstaller/model/ApkInfo;->getVersionName()Ljava/lang/String;

    move-result-object v2

    goto :goto_8

    :cond_9
    move-object v2, v1

    :goto_8
    const-string v3, "version_name"

    invoke-virtual {v0, v3, v2}, Lo5/c;->l(Ljava/lang/String;Ljava/lang/Object;)V

    iget-object v0, p0, Lm5/z0;->M:Lo5/c;

    iget-object v2, p0, Lm5/z0;->Q:Lcom/miui/packageInstaller/model/ApkInfo;

    if-eqz v2, :cond_a

    invoke-virtual {v2}, Lcom/miui/packageInstaller/model/ApkInfo;->getCloudParams()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v2

    if-eqz v2, :cond_a

    iget-object v2, v2, Lcom/miui/packageInstaller/model/CloudParams;->categoryAbbreviation:Ljava/lang/String;

    goto :goto_9

    :cond_a
    move-object v2, v1

    :goto_9
    const-string v3, "block_scene"

    invoke-virtual {v0, v3, v2}, Lo5/c;->l(Ljava/lang/String;Ljava/lang/Object;)V

    iget-object v0, p0, Lm5/z0;->Q:Lcom/miui/packageInstaller/model/ApkInfo;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v0, :cond_b

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/ApkInfo;->isOtherVersionInstalled()Z

    move-result v0

    if-ne v0, v2, :cond_b

    move v0, v2

    goto :goto_a

    :cond_b
    move v0, v3

    :goto_a
    if-eqz v0, :cond_c

    iget-object v0, p0, Lq2/b;->q:Lo5/b;

    const-string v4, "update"

    goto :goto_b

    :cond_c
    iget-object v0, p0, Lq2/b;->q:Lo5/b;

    const-string v4, "install"

    :goto_b
    invoke-virtual {v0, v4}, Lo5/b;->p(Ljava/lang/String;)V

    iget-object v0, p0, Lq2/b;->q:Lo5/b;

    iget-object v4, p0, Lm5/z0;->Q:Lcom/miui/packageInstaller/model/ApkInfo;

    if-eqz v4, :cond_d

    invoke-virtual {v4}, Lcom/miui/packageInstaller/model/ApkInfo;->getCloudParams()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v4

    if-eqz v4, :cond_d

    iget-object v4, v4, Lcom/miui/packageInstaller/model/CloudParams;->appType:Ljava/lang/String;

    goto :goto_c

    :cond_d
    move-object v4, v1

    :goto_c
    const-string v5, ""

    if-nez v4, :cond_e

    move-object v4, v5

    :cond_e
    invoke-virtual {v0, v4}, Lo5/b;->m(Ljava/lang/String;)V

    iget-object v0, p0, Lq2/b;->q:Lo5/b;

    iget-object v4, p0, Lm5/z0;->Q:Lcom/miui/packageInstaller/model/ApkInfo;

    if-eqz v4, :cond_f

    invoke-virtual {v4}, Lcom/miui/packageInstaller/model/ApkInfo;->getCloudParams()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v4

    if-eqz v4, :cond_f

    iget-object v4, v4, Lcom/miui/packageInstaller/model/CloudParams;->appInfo:Lcom/miui/packageInstaller/model/MarketAppInfo;

    if-eqz v4, :cond_f

    iget-object v4, v4, Lcom/miui/packageInstaller/model/MarketAppInfo;->appId:Ljava/lang/String;

    goto :goto_d

    :cond_f
    move-object v4, v1

    :goto_d
    if-nez v4, :cond_10

    goto :goto_e

    :cond_10
    move-object v5, v4

    :goto_e
    invoke-virtual {v0, v5}, Lo5/b;->e(Ljava/lang/String;)V

    iget-object v0, p0, Lq2/b;->q:Lo5/b;

    sget-object v4, Lcom/miui/packageInstaller/model/AppManageSceneMode;->Companion:Lcom/miui/packageInstaller/model/AppManageSceneMode$Companion;

    iget-object v5, p0, Lm5/z0;->Q:Lcom/miui/packageInstaller/model/ApkInfo;

    if-eqz v5, :cond_11

    invoke-virtual {v5}, Lcom/miui/packageInstaller/model/ApkInfo;->getCloudParams()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v5

    if-eqz v5, :cond_11

    iget-object v5, v5, Lcom/miui/packageInstaller/model/CloudParams;->appManageScene:Ljava/lang/String;

    goto :goto_f

    :cond_11
    move-object v5, v1

    :goto_f
    invoke-virtual {v4, v5}, Lcom/miui/packageInstaller/model/AppManageSceneMode$Companion;->getAppManagerScene(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lo5/b;->j(Ljava/lang/String;)V

    iget-object v0, p0, Lq2/b;->q:Lo5/b;

    iget-object v4, p0, Lm5/z0;->C:Lcom/miui/packageInstaller/model/CloudParams;

    if-eqz v4, :cond_12

    iget-boolean v4, v4, Lcom/miui/packageInstaller/model/CloudParams;->bundleApp:Z

    if-ne v4, v2, :cond_12

    goto :goto_10

    :cond_12
    move v2, v3

    :goto_10
    invoke-virtual {v0, v2}, Lo5/b;->o(Z)V

    iget-object v0, p0, Lm5/z0;->Q:Lcom/miui/packageInstaller/model/ApkInfo;

    if-eqz v0, :cond_13

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/ApkInfo;->getCloudParams()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v0

    if-eqz v0, :cond_13

    iget-object v0, v0, Lcom/miui/packageInstaller/model/CloudParams;->appType:Ljava/lang/String;

    goto :goto_11

    :cond_13
    move-object v0, v1

    :goto_11
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_15

    iget-object v0, p0, Lm5/z0;->M:Lo5/c;

    iget-object v2, p0, Lm5/z0;->Q:Lcom/miui/packageInstaller/model/ApkInfo;

    if-eqz v2, :cond_14

    invoke-virtual {v2}, Lcom/miui/packageInstaller/model/ApkInfo;->getLabel()Ljava/lang/String;

    move-result-object v2

    goto :goto_12

    :cond_14
    move-object v2, v1

    :goto_12
    const-string v3, "item_name"

    invoke-virtual {v0, v3, v2}, Lo5/c;->l(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_15
    iget-object v0, p0, Lm5/z0;->M:Lo5/c;

    invoke-virtual {p0}, Lq2/b;->E()Ljava/lang/String;

    move-result-object v2

    const-string v3, "item_type"

    invoke-virtual {v0, v3, v2}, Lo5/c;->l(Ljava/lang/String;Ljava/lang/Object;)V

    iget-object v0, p0, Lm5/z0;->Q:Lcom/miui/packageInstaller/model/ApkInfo;

    if-eqz v0, :cond_16

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/ApkInfo;->getCloudParams()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v0

    if-eqz v0, :cond_16

    iget-object v0, v0, Lcom/miui/packageInstaller/model/CloudParams;->appInfo:Lcom/miui/packageInstaller/model/MarketAppInfo;

    if-eqz v0, :cond_16

    iget-object v0, v0, Lcom/miui/packageInstaller/model/MarketAppInfo;->appId:Ljava/lang/String;

    goto :goto_13

    :cond_16
    move-object v0, v1

    :goto_13
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_18

    iget-object v0, p0, Lm5/z0;->M:Lo5/c;

    iget-object v2, p0, Lm5/z0;->Q:Lcom/miui/packageInstaller/model/ApkInfo;

    if-eqz v2, :cond_17

    invoke-virtual {v2}, Lcom/miui/packageInstaller/model/ApkInfo;->getCloudParams()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v2

    if-eqz v2, :cond_17

    iget-object v2, v2, Lcom/miui/packageInstaller/model/CloudParams;->appInfo:Lcom/miui/packageInstaller/model/MarketAppInfo;

    if-eqz v2, :cond_17

    iget-object v2, v2, Lcom/miui/packageInstaller/model/MarketAppInfo;->appId:Ljava/lang/String;

    goto :goto_14

    :cond_17
    move-object v2, v1

    :goto_14
    const-string v3, "item_id"

    invoke-virtual {v0, v3, v2}, Lo5/c;->l(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_18
    iget-object v0, p0, Lm5/z0;->C:Lcom/miui/packageInstaller/model/CloudParams;

    if-eqz v0, :cond_19

    iget-object v0, v0, Lcom/miui/packageInstaller/model/CloudParams;->incrementPackageInfo:Lcom/miui/packageInstaller/model/HasIncrement;

    if-eqz v0, :cond_19

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/HasIncrement;->getTagId()Ljava/lang/String;

    move-result-object v0

    goto :goto_15

    :cond_19
    move-object v0, v1

    :goto_15
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1c

    iget-object v0, p0, Lm5/z0;->C:Lcom/miui/packageInstaller/model/CloudParams;

    if-eqz v0, :cond_1a

    iget-object v0, v0, Lcom/miui/packageInstaller/model/CloudParams;->incrementPackageInfo:Lcom/miui/packageInstaller/model/HasIncrement;

    if-eqz v0, :cond_1a

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/HasIncrement;->getEx()Ljava/lang/String;

    move-result-object v0

    goto :goto_16

    :cond_1a
    move-object v0, v1

    :goto_16
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1c

    iget-object v0, p0, Lm5/z0;->M:Lo5/c;

    iget-object v2, p0, Lm5/z0;->C:Lcom/miui/packageInstaller/model/CloudParams;

    if-eqz v2, :cond_1b

    iget-object v2, v2, Lcom/miui/packageInstaller/model/CloudParams;->incrementPackageInfo:Lcom/miui/packageInstaller/model/HasIncrement;

    if-eqz v2, :cond_1b

    invoke-virtual {v2}, Lcom/miui/packageInstaller/model/HasIncrement;->getTagId()Ljava/lang/String;

    move-result-object v1

    :cond_1b
    const-string v2, "tag_id"

    invoke-virtual {v0, v2, v1}, Lo5/c;->l(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_1c
    return-void
.end method

.method public final j2(Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;Ljava/lang/CharSequence;)V
    .locals 4

    const-string v0, "button"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "cancelText"

    invoke-static {p2, v1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lm5/z0;->C:Lcom/miui/packageInstaller/model/CloudParams;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    iget-boolean v1, v1, Lcom/miui/packageInstaller/model/CloudParams;->storeListed:Z

    if-ne v1, v2, :cond_0

    goto :goto_0

    :cond_0
    move v2, v3

    :goto_0
    if-eqz v2, :cond_1

    invoke-direct {p0}, Lm5/z0;->n1()Ljava/lang/String;

    move-result-object p2

    :cond_1
    new-instance v1, Lp5/g;

    const-string v2, "cancel_install_btn"

    invoke-direct {v1, v2, v0, p0}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v1}, Lp5/f;->c()Z

    invoke-interface {p1, p2}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;->setButtonText(Ljava/lang/CharSequence;)V

    invoke-interface {p1}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;->a()Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, v3}, Landroid/view/View;->setVisibility(I)V

    new-instance p2, Lm5/u0;

    invoke-direct {p2, p0}, Lm5/u0;-><init>(Lm5/z0;)V

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final k1()Ll6/d;
    .locals 1

    iget-object v0, p0, Lm5/z0;->O:Ll6/d;

    return-object v0
.end method

.method public final l1()Z
    .locals 1

    iget-boolean v0, p0, Lm5/z0;->L:Z

    return v0
.end method

.method public m(Lcom/miui/packageInstaller/model/ApkInfo;)V
    .locals 3

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/miui/packageInstaller/model/ApkInfo;->getCloudParams()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object p1

    goto :goto_0

    :cond_0
    move-object p1, v0

    :goto_0
    iput-object p1, p0, Lm5/z0;->C:Lcom/miui/packageInstaller/model/CloudParams;

    iget-object v1, p0, Lq2/b;->q:Lo5/b;

    if-eqz p1, :cond_1

    iget-object v0, p1, Lcom/miui/packageInstaller/model/CloudParams;->miPackageName:Ljava/lang/String;

    :cond_1
    if-nez v0, :cond_2

    const-string v0, ""

    :cond_2
    invoke-virtual {v1, v0}, Lo5/b;->r(Ljava/lang/String;)V

    invoke-virtual {p0}, Lm5/z0;->j1()V

    iget-boolean p1, p0, Lm5/z0;->E:Z

    if-eqz p1, :cond_3

    invoke-virtual {p0}, Lm5/z0;->S1()V

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Lm5/z0;->t1()Lt5/x;

    move-result-object p1

    iget-object v0, p0, Lm5/z0;->Q:Lcom/miui/packageInstaller/model/ApkInfo;

    invoke-static {v0}, Lm8/i;->c(Ljava/lang/Object;)V

    new-instance v1, Lm5/z0$f;

    invoke-direct {v1, p0}, Lm5/z0$f;-><init>(Lm5/z0;)V

    invoke-virtual {p1, v0, v1}, Lt5/x;->k(Lcom/miui/packageInstaller/model/ApkInfo;Ll8/a;)V

    :goto_1
    iget-object p1, p0, Lm5/z0;->C:Lcom/miui/packageInstaller/model/CloudParams;

    if-eqz p1, :cond_4

    iget-object p1, p1, Lcom/miui/packageInstaller/model/CloudParams;->cInfo:Lcom/miui/packageInstaller/model/CInfo;

    if-eqz p1, :cond_4

    sget-object v0, Lf6/h;->a:Lf6/h;

    iget-object v1, p0, Lm5/z0;->Q:Lcom/miui/packageInstaller/model/ApkInfo;

    iget-object v2, p0, Lm5/z0;->y:Lm5/e;

    invoke-virtual {v0, p1, v1, v2}, Lf6/h;->k(Lcom/miui/packageInstaller/model/CInfo;Lcom/miui/packageInstaller/model/ApkInfo;Lm5/e;)V

    :cond_4
    return-void
.end method

.method public m1()Lcom/miui/packageInstaller/model/ApkInfo;
    .locals 1

    iget-object v0, p0, Lm5/z0;->Q:Lcom/miui/packageInstaller/model/ApkInfo;

    return-object v0
.end method

.method public final m2(Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;Lcom/miui/packageInstaller/model/PositiveButtonRules;)V
    .locals 4

    const-string v0, "button"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "rules"

    invoke-static {p2, v1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;->a()Landroid/view/View;

    move-result-object p2

    const/4 v1, 0x0

    invoke-virtual {p2, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object p2, p0, Lm5/z0;->C:Lcom/miui/packageInstaller/model/CloudParams;

    const/4 v2, 0x0

    if-eqz p2, :cond_0

    iget-object p2, p2, Lcom/miui/packageInstaller/model/CloudParams;->positiveButtonTip:Lcom/miui/packageInstaller/model/PositiveButtonRules;

    if-eqz p2, :cond_0

    iget-object p2, p2, Lcom/miui/packageInstaller/model/PositiveButtonRules;->text:Ljava/lang/String;

    goto :goto_0

    :cond_0
    move-object p2, v2

    :goto_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p2

    const/4 v3, 0x1

    if-nez p2, :cond_2

    iget-object p2, p0, Lm5/z0;->C:Lcom/miui/packageInstaller/model/CloudParams;

    if-eqz p2, :cond_1

    iget-object p2, p2, Lcom/miui/packageInstaller/model/CloudParams;->positiveButtonTip:Lcom/miui/packageInstaller/model/PositiveButtonRules;

    if-eqz p2, :cond_1

    iget-object p2, p2, Lcom/miui/packageInstaller/model/PositiveButtonRules;->text:Ljava/lang/String;

    goto :goto_3

    :cond_1
    move-object p2, v2

    goto :goto_3

    :cond_2
    iget-object p2, p0, Lm5/z0;->Q:Lcom/miui/packageInstaller/model/ApkInfo;

    if-eqz p2, :cond_3

    invoke-virtual {p2}, Lcom/miui/packageInstaller/model/ApkInfo;->isOtherVersionInstalled()Z

    move-result p2

    if-nez p2, :cond_3

    move p2, v3

    goto :goto_1

    :cond_3
    move p2, v1

    :goto_1
    if-eqz p2, :cond_4

    const p2, 0x7f110381

    goto :goto_2

    :cond_4
    const p2, 0x7f1103dc

    :goto_2
    invoke-virtual {p0, p2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object p2

    :goto_3
    invoke-interface {p1, p2}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;->setButtonText(Ljava/lang/CharSequence;)V

    iget-object p2, p0, Lm5/z0;->C:Lcom/miui/packageInstaller/model/CloudParams;

    if-eqz p2, :cond_5

    iget-object p2, p2, Lcom/miui/packageInstaller/model/CloudParams;->positiveButtonTip:Lcom/miui/packageInstaller/model/PositiveButtonRules;

    if-eqz p2, :cond_5

    iget-object v2, p2, Lcom/miui/packageInstaller/model/PositiveButtonRules;->method:Ljava/lang/String;

    :cond_5
    const-string p2, "install"

    invoke-static {p2, v2}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_8

    iget-object p2, p0, Lm5/z0;->C:Lcom/miui/packageInstaller/model/CloudParams;

    if-eqz p2, :cond_6

    iget-boolean p2, p2, Lcom/miui/packageInstaller/model/CloudParams;->backgroundInstall:Z

    if-ne p2, v3, :cond_6

    move v1, v3

    :cond_6
    if-eqz v1, :cond_7

    const-string p2, "background"

    goto :goto_4

    :cond_7
    const-string p2, "front_desk"

    :goto_4
    new-instance v1, Lp5/g;

    const-string v2, "xiaomi_market_install_btn"

    invoke-direct {v1, v2, v0, p0}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    const-string v0, "appstore_install_type"

    invoke-virtual {v1, v0, p2}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p2

    invoke-virtual {p2}, Lp5/f;->c()Z

    const-string p2, "VIEW"

    invoke-virtual {p0, p2}, Lm5/z0;->R1(Ljava/lang/String;)V

    goto :goto_5

    :cond_8
    new-instance p2, Lp5/g;

    const-string v1, "search_similar_app_btn"

    invoke-direct {p2, v1, v0, p0}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p2}, Lp5/f;->c()Z

    :goto_5
    new-instance p2, Lm5/t0;

    invoke-direct {p2, p0}, Lm5/t0;-><init>(Lm5/z0;)V

    invoke-interface {p1, p2}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;->setClick(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final o2(Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;Ljava/lang/String;Z)V
    .locals 5

    const-string p3, "button"

    invoke-static {p1, p3}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "installText"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lm5/z0;->C:Lcom/miui/packageInstaller/model/CloudParams;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    iget-boolean v0, v0, Lcom/miui/packageInstaller/model/CloudParams;->storeListed:Z

    if-ne v0, v1, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    invoke-direct {p0}, Lm5/z0;->o1()Ljava/lang/String;

    move-result-object p2

    :cond_1
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const v3, 0x7f1103dc

    invoke-virtual {p0, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p2, v3}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    const-string v4, "button_type"

    if-eqz v3, :cond_2

    const-string v3, "continue_update"

    :goto_1
    invoke-interface {v0, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_2
    const v3, 0x7f1103e5

    invoke-virtual {p0, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p2, v3}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v3, "verify_install"

    goto :goto_1

    :cond_3
    const-string v3, "continue_install"

    goto :goto_1

    :goto_2
    new-instance v3, Lp5/g;

    const-string v4, "install_btn"

    invoke-direct {v3, v4, p3, p0}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v3, v0}, Lp5/f;->g(Ljava/util/Map;)Lp5/f;

    move-result-object p3

    invoke-virtual {p3}, Lp5/f;->c()Z

    invoke-interface {p1, p2}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;->setButtonText(Ljava/lang/CharSequence;)V

    invoke-interface {p1}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;->a()Landroid/view/View;

    move-result-object p3

    iget-boolean v3, p0, Lm5/z0;->A:Z

    xor-int/2addr v1, v3

    invoke-interface {p1, v1}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;->setProgressVisibility(Z)V

    invoke-interface {p1, p2}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;->setButtonText(Ljava/lang/CharSequence;)V

    new-instance p1, Lm5/y0;

    invoke-direct {p1, p0, v0}, Lm5/y0;-><init>(Lm5/z0;Ljava/util/HashMap;)V

    invoke-virtual {p3, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p3, v2}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    invoke-super {p0, p1, p2, p3}, Landroidx/fragment/app/e;->onActivityResult(IILandroid/content/Intent;)V

    invoke-virtual {p0}, Lm5/z0;->t1()Lt5/x;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lt5/x;->s(IILandroid/content/Intent;)V

    return-void
.end method

.method public onBackPressed()V
    .locals 3

    iget-object v0, p0, Lm5/z0;->P:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lz5/a;

    invoke-interface {v1}, Lz5/a;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    return-void

    :cond_1
    new-instance v0, Lp5/b;

    const-string v1, "page_back_btn"

    const-string v2, "button"

    invoke-direct {v0, v1, v2, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    const-string v1, "back_type"

    const-string v2, "system"

    invoke-virtual {v0, v1, v2}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object v0

    invoke-virtual {v0}, Lp5/f;->c()Z

    invoke-virtual {p0}, Lm5/z0;->g1()V

    invoke-super {p0}, Lmiuix/appcompat/app/j;->onBackPressed()V

    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3

    const-string v0, "newConfig"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lq2/b;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "newConfig = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, " newConfig.colorMode = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p1, Landroid/content/res/Configuration;->uiMode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, " isNight = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p1, p1, Landroid/content/res/Configuration;->uiMode:I

    and-int/lit8 p1, p1, 0x30

    const/4 v1, 0x0

    const/16 v2, 0x20

    if-ne p1, v2, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    move p1, v1

    :goto_0
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "BaseDialog"

    invoke-static {v0, p1}, Lf6/o;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->u()Z

    move-result p1

    if-nez p1, :cond_1

    iget-boolean p1, p0, Lm5/z0;->D:Z

    if-eqz p1, :cond_1

    iput-boolean v1, p0, Lm5/z0;->D:Z

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object p1

    new-instance v0, Lm5/p0;

    invoke-direct {v0, p0}, Lm5/p0;-><init>(Lm5/z0;)V

    invoke-virtual {p1, v0}, Lf6/z;->g(Ljava/lang/Runnable;)V

    :cond_1
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8

    invoke-super {p0, p1}, Lq2/b;->onCreate(Landroid/os/Bundle;)V

    invoke-direct {p0}, Lm5/z0;->b2()V

    invoke-direct {p0}, Lm5/z0;->d1()V

    invoke-static {}, Lcom/android/packageinstaller/compat/PackageInstallerCompat;->getSessionAction()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    invoke-virtual {p0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/pm/PackageManager;->getPackageInstaller()Landroid/content/pm/PackageInstaller;

    move-result-object p1

    iput-object p1, p0, Lm5/z0;->v:Landroid/content/pm/PackageInstaller;

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const/4 v0, -0x1

    const-string v1, "android.content.pm.extra.SESSION_ID"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result p1

    iget-object v0, p0, Lm5/z0;->v:Landroid/content/pm/PackageInstaller;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Landroid/content/pm/PackageInstaller;->getSessionInfo(I)Landroid/content/pm/PackageInstaller$SessionInfo;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_2

    invoke-static {v0}, Lcom/android/packageinstaller/compat/SessionInfoCompat;->sealed(Landroid/content/pm/PackageInstaller$SessionInfo;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {v0}, Lcom/android/packageinstaller/compat/SessionInfoCompat;->resolvedBaseCodePath(Landroid/content/pm/PackageInstaller$SessionInfo;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    goto :goto_1

    :cond_1
    iput p1, p0, Lm5/z0;->w:I

    new-instance p1, Ljava/io/File;

    invoke-static {v0}, Lcom/android/packageinstaller/compat/SessionInfoCompat;->resolvedBaseCodePath(Landroid/content/pm/PackageInstaller$SessionInfo;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object p1

    goto :goto_2

    :cond_2
    :goto_1
    iget-object v0, p0, Lm5/z0;->u:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Session "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " in funky state; ignoring"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lf6/o;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    invoke-virtual {p0}, Lmiuix/appcompat/app/j;->finish()V

    return-void

    :cond_3
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object p1

    :goto_2
    iput-object p1, p0, Lm5/z0;->x:Landroid/net/Uri;

    invoke-direct {p0}, Lm5/z0;->C1()V

    invoke-direct {p0}, Lm5/z0;->W1()V

    invoke-virtual {p0}, Lm5/z0;->F1()V

    sget-object v0, Ll2/a;->a:Ll2/a;

    invoke-virtual {p0}, Lm5/z0;->D0()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    iget-object v5, p0, Lm5/z0;->y:Lm5/e;

    const/16 v6, 0xc

    const/4 v7, 0x0

    move-object v1, p0

    invoke-static/range {v0 .. v7}, Ll2/a;->c(Ll2/a;Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lm5/e;ILjava/lang/Object;)V

    return-void
.end method

.method protected onDestroy()V
    .locals 2

    invoke-super {p0}, Landroidx/fragment/app/e;->onDestroy()V

    instance-of v0, p0, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lm5/z0;->b1()V

    :cond_0
    iget-boolean v0, p0, Lq2/b;->s:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lm5/z0;->x:Landroid/net/Uri;

    if-eqz v0, :cond_2

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    const-string v1, "content"

    invoke-static {v1, v0}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lm5/z0;->x:Landroid/net/Uri;

    invoke-static {v0}, Lf6/c0;->b(Landroid/net/Uri;)V

    :cond_2
    invoke-static {}, Lj2/f;->a()V

    return-void
.end method

.method protected onResume()V
    .locals 1

    invoke-super {p0}, Landroidx/fragment/app/e;->onResume()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lm5/z0;->J:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lm5/z0;->F:Z

    return-void
.end method

.method protected onStop()V
    .locals 1

    invoke-super {p0}, Lq2/b;->onStop()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lm5/z0;->J:Z

    return-void
.end method

.method public p()Lm5/e;
    .locals 1

    iget-object v0, p0, Lm5/z0;->y:Lm5/e;

    return-object v0
.end method

.method public final p1()Lcom/miui/packageInstaller/model/InstallSourceTips;
    .locals 1

    iget-object v0, p0, Lm5/z0;->C:Lcom/miui/packageInstaller/model/CloudParams;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/miui/packageInstaller/model/CloudParams;->installSourceTips:Lcom/miui/packageInstaller/model/InstallSourceTips;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public final q1()Lcom/miui/packageInstaller/model/ApkInfo;
    .locals 1

    iget-object v0, p0, Lm5/z0;->Q:Lcom/miui/packageInstaller/model/ApkInfo;

    return-object v0
.end method

.method public r(Lcom/miui/packageInstaller/model/Virus;)V
    .locals 1

    iget-boolean v0, p0, Lm5/z0;->A:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lm5/z0;->A:Z

    iput-object p1, p0, Lm5/z0;->B:Lcom/miui/packageInstaller/model/Virus;

    if-eqz p1, :cond_0

    iget-object p1, p0, Lq2/b;->q:Lo5/b;

    const-string v0, "virus_engine"

    invoke-virtual {p1, v0}, Lo5/b;->f(Ljava/lang/String;)V

    :cond_0
    iget-boolean p1, p0, Lm5/z0;->D:Z

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lm5/z0;->e1()V

    :cond_1
    return-void
.end method

.method public final r1()Lm5/e;
    .locals 1

    iget-object v0, p0, Lm5/z0;->y:Lm5/e;

    return-object v0
.end method

.method public final r2(Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;)V
    .locals 2

    const-string v0, "<this>"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->getMFirstButton()Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;

    move-result-object v0

    if-eqz v0, :cond_0

    const v1, 0x7f110023

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;->setButtonText(Ljava/lang/CharSequence;)V

    new-instance v1, Lm5/w0;

    invoke-direct {v1, p0}, Lm5/w0;-><init>(Lm5/z0;)V

    invoke-interface {v0, v1}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;->setClick(Landroid/view/View$OnClickListener;)V

    :cond_0
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->getMSecondButton()Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;

    move-result-object p1

    if-eqz p1, :cond_1

    const v0, 0x7f1100b5

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;->setButtonText(Ljava/lang/CharSequence;)V

    invoke-interface {p1}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;->a()Landroid/view/View;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    new-instance v0, Lm5/q0;

    invoke-direct {v0, p0}, Lm5/q0;-><init>(Lm5/z0;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    return-void
.end method

.method public final s1()Lcom/miui/packageInstaller/model/CloudParams;
    .locals 1

    iget-object v0, p0, Lm5/z0;->C:Lcom/miui/packageInstaller/model/CloudParams;

    return-object v0
.end method

.method public final t1()Lt5/x;
    .locals 1

    iget-object v0, p0, Lm5/z0;->z:Lt5/x;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    const-string v0, "mInstallSourceManager"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v0, 0x0

    return-object v0
.end method

.method public final u1()Z
    .locals 1

    iget-boolean v0, p0, Lm5/z0;->J:Z

    return v0
.end method

.method public final u2(Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;)V
    .locals 8

    const-string v0, "<this>"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->getMFirstButton()Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;

    move-result-object v2

    const/4 v0, 0x1

    const/4 v7, 0x0

    if-eqz v2, :cond_2

    iget-object v1, p0, Lm5/z0;->Q:Lcom/miui/packageInstaller/model/ApkInfo;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/ApkInfo;->isOtherVersionInstalled()Z

    move-result v1

    if-nez v1, :cond_0

    move v1, v0

    goto :goto_0

    :cond_0
    move v1, v7

    :goto_0
    if-eqz v1, :cond_1

    const v1, 0x7f110381

    goto :goto_1

    :cond_1
    const v1, 0x7f1103dc

    :goto_1
    invoke-virtual {p0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object v3, v1

    const-string v1, "if (mApkInfo?.isOtherVer\u2026_start)\n                }"

    invoke-static {v3, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p0

    invoke-static/range {v1 .. v6}, Lm5/z0;->p2(Lm5/z0;Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;Ljava/lang/String;ZILjava/lang/Object;)V

    :cond_2
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->getMSecondButton()Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;

    move-result-object v1

    if-eqz v1, :cond_5

    iget-object v2, p0, Lm5/z0;->Q:Lcom/miui/packageInstaller/model/ApkInfo;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lcom/miui/packageInstaller/model/ApkInfo;->isOtherVersionInstalled()Z

    move-result v2

    if-nez v2, :cond_3

    goto :goto_2

    :cond_3
    move v0, v7

    :goto_2
    if-eqz v0, :cond_4

    const v0, 0x7f110055

    goto :goto_3

    :cond_4
    const v0, 0x7f110056

    :goto_3
    invoke-virtual {p0, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v2, "if (mApkInfo?.isOtherVer\u2026update)\n                }"

    invoke-static {v0, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v1, v0}, Lm5/z0;->j2(Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;Ljava/lang/CharSequence;)V

    :cond_5
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->getMThirdButton()Lcom/miui/packageInstaller/view/InstallerActionButton;

    move-result-object p1

    if-nez p1, :cond_6

    goto :goto_4

    :cond_6
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setVisibility(I)V

    :goto_4
    return-void
.end method

.method public final v1()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lm5/z0;->x:Landroid/net/Uri;

    return-object v0
.end method

.method public final w1()Lw5/a;
    .locals 1

    iget-object v0, p0, Lm5/z0;->G:Lw5/a;

    return-object v0
.end method

.method public final x1()Lcom/miui/packageInstaller/model/Virus;
    .locals 1

    iget-object v0, p0, Lm5/z0;->B:Lcom/miui/packageInstaller/model/Virus;

    return-object v0
.end method

.method public y(Ljava/util/List;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lm6/a<",
            "*>;>;",
            "Ljava/util/List<",
            "Lm6/a<",
            "*>;>;)V"
        }
    .end annotation

    return-void
.end method

.method public abstract y1()Landroid/content/Intent;
.end method

.method public final z1()I
    .locals 1

    iget v0, p0, Lm5/z0;->K:I

    return v0
.end method
