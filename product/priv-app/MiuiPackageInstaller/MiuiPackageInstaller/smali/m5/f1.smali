.class public Lm5/f1;
.super Ljava/lang/Object;


# instance fields
.field private a:Lo5/a;


# direct methods
.method public constructor <init>(Lo5/a;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lm5/f1;->a:Lo5/a;

    return-void
.end method


# virtual methods
.method public getVersionCode()J
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    sget-object v0, Lcom/android/packageinstaller/InstallerApplication;->c:Lcom/android/packageinstaller/InstallerApplication;

    invoke-virtual {v0}, Landroid/app/Application;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lj2/f;->g(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public getVersionName()Ljava/lang/String;
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    sget-object v0, Lcom/android/packageinstaller/InstallerApplication;->c:Lcom/android/packageinstaller/InstallerApplication;

    invoke-virtual {v0}, Landroid/app/Application;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lj2/f;->h(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public trackEvent(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    new-instance v0, Lp4/e;

    invoke-direct {v0}, Lp4/e;-><init>()V

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    :try_start_0
    new-instance v2, Lm5/f1$a;

    invoke-direct {v2, p0}, Lm5/f1$a;-><init>(Lm5/f1;)V

    invoke-virtual {v2}, Lw4/a;->e()Ljava/lang/reflect/Type;

    move-result-object v2

    invoke-virtual {v0, p2, v2}, Lp4/e;->i(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/util/Map;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, p2

    goto :goto_0

    :catch_0
    move-exception p2

    invoke-virtual {p2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object p2

    const-string v0, "PackageInstallerJSImpl"

    invoke-static {v0, p2}, Lf6/o;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    :goto_0
    invoke-static {v1}, Lk5/b;->b(Ljava/util/Map;)Z

    move-result p2

    if-nez p2, :cond_0

    sget-object p2, Lp5/f;->f:Lp5/f$a;

    iget-object v0, p0, Lm5/f1;->a:Lo5/a;

    const-string v2, ""

    invoke-virtual {p2, v0, p1, v2, v2}, Lp5/f$a;->a(Lo5/a;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p1

    invoke-virtual {p1, v1}, Lp5/f;->g(Ljava/util/Map;)Lp5/f;

    move-result-object p1

    invoke-virtual {p1}, Lp5/f;->c()Z

    :cond_0
    return-void
.end method

.method public trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    sget-object v0, Lp5/f;->f:Lp5/f$a;

    iget-object v1, p0, Lm5/f1;->a:Lo5/a;

    invoke-virtual {v0, v1, p1, p3, p2}, Lp5/f$a;->a(Lo5/a;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p1

    invoke-virtual {p1, p4}, Lp5/f;->g(Ljava/util/Map;)Lp5/f;

    move-result-object p1

    invoke-virtual {p1}, Lp5/f;->c()Z

    return-void
.end method
