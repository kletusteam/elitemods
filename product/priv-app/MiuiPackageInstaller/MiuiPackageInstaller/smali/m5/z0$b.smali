.class public final Lm5/z0$b;
.super Ljava/lang/Object;

# interfaces
.implements Ls5/l$c;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lm5/z0;->f1()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field private a:Z

.field private b:Z

.field final synthetic c:Lm5/z0;


# direct methods
.method constructor <init>(Lm5/z0;)V
    .locals 0

    iput-object p1, p0, Lm5/z0$b;->c:Lm5/z0;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public C(Ls5/l;II)V
    .locals 2

    iget-boolean p1, p0, Lm5/z0$b;->b:Z

    if-eqz p1, :cond_0

    return-void

    :cond_0
    const p1, -0xea66

    const/4 p2, 0x1

    const/4 v0, 0x0

    if-eq p3, p1, :cond_2

    const p1, -0xea63

    if-eq p3, p1, :cond_1

    const p1, -0xea61

    if-eq p3, p1, :cond_1

    goto :goto_1

    :cond_1
    sget-object p1, Lcom/android/packageinstaller/InstallerApplication;->c:Lcom/android/packageinstaller/InstallerApplication;

    iget-object p3, p0, Lm5/z0$b;->c:Lm5/z0;

    const v1, 0x7f11004a

    goto :goto_0

    :cond_2
    sget-object p1, Lcom/android/packageinstaller/InstallerApplication;->c:Lcom/android/packageinstaller/InstallerApplication;

    iget-object p3, p0, Lm5/z0$b;->c:Lm5/z0;

    const v1, 0x7f11004b

    :goto_0
    invoke-virtual {p3, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object p3

    invoke-static {p1, p3, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    iput-boolean p2, p0, Lm5/z0$b;->b:Z

    :goto_1
    return-void
.end method

.method public q(Ls5/l;II)V
    .locals 4

    const/4 p1, 0x4

    if-eq p2, p1, :cond_0

    const/16 p1, 0x9

    if-ne p2, p1, :cond_2

    iget-boolean p1, p0, Lm5/z0$b;->a:Z

    if-nez p1, :cond_2

    :cond_0
    const/4 p1, 0x1

    iput-boolean p1, p0, Lm5/z0$b;->a:Z

    sget-object p2, Lf6/j;->i:Lf6/j$a;

    sget-object p3, Lcom/android/packageinstaller/InstallerApplication;->c:Lcom/android/packageinstaller/InstallerApplication;

    const-string v0, "sInstance"

    invoke-static {p3, v0}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lm5/z0$b;->c:Lm5/z0;

    const v1, 0x7f110049

    new-array p1, p1, [Ljava/lang/Object;

    invoke-virtual {v0}, Lm5/z0;->q1()Lcom/miui/packageInstaller/model/ApkInfo;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/miui/packageInstaller/model/ApkInfo;->getCloudParams()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, v2, Lcom/miui/packageInstaller/model/CloudParams;->appInfo:Lcom/miui/packageInstaller/model/MarketAppInfo;

    if-eqz v2, :cond_1

    iget-object v2, v2, Lcom/miui/packageInstaller/model/MarketAppInfo;->displayName:Ljava/lang/String;

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_0
    const/4 v3, 0x0

    aput-object v2, p1, v3

    invoke-virtual {v0, v1, p1}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "getString(R.string.backg\u2026ms?.appInfo?.displayName)"

    invoke-static {p1, v0}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2, p3, p1, v3}, Lf6/j$a;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Lf6/j;

    move-result-object p1

    invoke-virtual {p1}, Lf6/j;->o()V

    iget-object p1, p0, Lm5/z0$b;->c:Lm5/z0;

    invoke-virtual {p1}, Lmiuix/appcompat/app/j;->finish()V

    :cond_2
    return-void
.end method

.method public u(Ls5/l;)V
    .locals 0

    return-void
.end method
