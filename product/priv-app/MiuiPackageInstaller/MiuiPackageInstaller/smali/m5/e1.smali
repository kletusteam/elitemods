.class public final Lm5/e1;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lm5/e1$a;
    }
.end annotation


# static fields
.field public static final a:Lm5/e1$a;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lm5/e1$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lm5/e1$a;-><init>(Lm8/g;)V

    sput-object v0, Lm5/e1;->a:Lm5/e1$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/net/Uri;ZZLjava/lang/String;)Lcom/miui/packageInstaller/model/ExtraGuardVirusInfoEntity;
    .locals 8

    const-string v0, "context"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "callingPackage"

    invoke-static {p5, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "uri is null"

    invoke-static {p2, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 p1, 0x0

    if-eqz p4, :cond_0

    :try_start_0
    const-string v0, "content://guard/install_scan"

    goto :goto_0

    :cond_0
    const-string v0, "content://guard/sync_scan"

    :goto_0
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    if-eqz v2, :cond_3

    new-instance v0, Lcom/miui/packageInstaller/model/ExtraGuardVirusInfoEntity;

    invoke-direct {v0}, Lcom/miui/packageInstaller/model/ExtraGuardVirusInfoEntity;-><init>()V

    new-instance v3, Landroid/util/ArrayMap;

    invoke-direct {v3}, Landroid/util/ArrayMap;-><init>()V

    const-string v4, "installPackage"

    invoke-interface {v3, v4, p5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 p5, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x2

    if-eqz p4, :cond_1

    const/4 p4, 0x3

    new-array p4, p4, [Ljava/lang/String;

    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p2

    aput-object p2, p4, v4

    invoke-static {p3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object p2

    aput-object p2, p4, p5

    new-instance p2, Lp4/e;

    invoke-direct {p2}, Lp4/e;-><init>()V

    invoke-virtual {p2, v3}, Lp4/e;->q(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    aput-object p2, p4, v5

    goto :goto_1

    :cond_1
    new-array p4, v5, [Ljava/lang/String;

    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p2

    aput-object p2, p4, v4

    invoke-static {p3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object p2

    aput-object p2, p4, p5

    :goto_1
    move-object v5, p4

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-eqz p2, :cond_2

    :try_start_1
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result p3

    if-eqz p3, :cond_2

    const-string p3, "Type"

    invoke-interface {p2, p3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result p3

    invoke-interface {p2, p3}, Landroid/database/Cursor;->getInt(I)I

    move-result p3

    invoke-virtual {v0, p3}, Lcom/miui/packageInstaller/model/ExtraGuardVirusInfoEntity;->setType(I)V

    const-string p3, "Description"

    invoke-interface {p2, p3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result p3

    invoke-interface {p2, p3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {v0, p3}, Lcom/miui/packageInstaller/model/ExtraGuardVirusInfoEntity;->setDescription(Ljava/lang/String;)V

    const-string p3, "VirusName"

    invoke-interface {p2, p3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result p3

    invoke-interface {p2, p3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {v0, p3}, Lcom/miui/packageInstaller/model/ExtraGuardVirusInfoEntity;->setVirusName(Ljava/lang/String;)V

    const-string p3, "EngineName"

    invoke-interface {p2, p3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result p3

    invoke-interface {p2, p3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {v0, p3}, Lcom/miui/packageInstaller/model/ExtraGuardVirusInfoEntity;->setEngineName(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception p1

    move-object v7, p2

    move-object p2, p1

    move-object p1, v7

    goto :goto_5

    :catch_0
    move-exception p3

    move-object v7, p3

    move-object p3, p2

    move-object p2, v7

    goto :goto_3

    :cond_2
    :goto_2
    invoke-static {p2}, Laa/a;->a(Ljava/io/Closeable;)V

    return-object v0

    :cond_3
    invoke-static {p1}, Laa/a;->a(Ljava/io/Closeable;)V

    goto :goto_4

    :catchall_1
    move-exception p2

    goto :goto_5

    :catch_1
    move-exception p2

    move-object p3, p1

    :goto_3
    :try_start_2
    invoke-virtual {p2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    invoke-static {p3}, Laa/a;->a(Ljava/io/Closeable;)V

    :goto_4
    return-object p1

    :catchall_2
    move-exception p2

    move-object p1, p3

    :goto_5
    invoke-static {p1}, Laa/a;->a(Ljava/io/Closeable;)V

    throw p2
.end method

.method public final b(Ljava/lang/String;Lcom/miui/packageInstaller/model/ApkInfo;Ld8/d;)Ljava/lang/Object;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/miui/packageInstaller/model/ApkInfo;",
            "Ld8/d<",
            "-",
            "Lcom/miui/packageInstaller/model/Virus;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    move-object/from16 v0, p3

    const-string v1, "Exception when check virus: "

    const-string v2, "NewVirusScanner"

    const-string v3, "com.miui.guardprovider"

    instance-of v4, v0, Lm5/e1$b;

    if-eqz v4, :cond_0

    move-object v4, v0

    check-cast v4, Lm5/e1$b;

    iget v5, v4, Lm5/e1$b;->g:I

    const/high16 v6, -0x80000000

    and-int v7, v5, v6

    if-eqz v7, :cond_0

    sub-int/2addr v5, v6

    iput v5, v4, Lm5/e1$b;->g:I

    move-object/from16 v11, p0

    goto :goto_0

    :cond_0
    new-instance v4, Lm5/e1$b;

    move-object/from16 v11, p0

    invoke-direct {v4, v11, v0}, Lm5/e1$b;-><init>(Lm5/e1;Ld8/d;)V

    :goto_0
    iget-object v0, v4, Lm5/e1$b;->e:Ljava/lang/Object;

    invoke-static {}, Le8/b;->c()Ljava/lang/Object;

    move-result-object v12

    iget v5, v4, Lm5/e1$b;->g:I

    const/4 v13, 0x1

    if-eqz v5, :cond_2

    if-ne v5, v13, :cond_1

    iget-object v1, v4, Lm5/e1$b;->d:Ljava/lang/Object;

    check-cast v1, Lcom/miui/packageInstaller/model/Virus;

    invoke-static {v0}, La8/n;->b(Ljava/lang/Object;)V

    goto/16 :goto_8

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    invoke-static {v0}, La8/n;->b(Ljava/lang/Object;)V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v14

    const/16 v16, 0x0

    :try_start_0
    invoke-static {}, Lcom/android/packageinstaller/InstallerApplication;->g()Lcom/android/packageinstaller/InstallerApplication;

    move-result-object v6
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    const/4 v5, 0x0

    :try_start_1
    invoke-virtual {v6}, Landroid/app/Application;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/16 v7, 0x80

    invoke-virtual {v0, v3, v7}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    const-string v7, "context.getPackageManage\u2026ageManager.GET_META_DATA)"

    invoke-static {v0, v7}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    if-eqz v0, :cond_3

    const-string v7, "guardprovider.scan_properties"

    invoke-virtual {v0, v7, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    const/4 v7, 0x2

    if-lt v0, v7, :cond_3

    move v0, v13

    goto :goto_1

    :catch_0
    move-exception v0

    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :cond_3
    move v0, v5

    :goto_1
    invoke-virtual/range {p2 .. p2}, Lcom/miui/packageInstaller/model/ApkInfo;->getFileUri()Landroid/net/Uri;

    move-result-object v7

    new-instance v8, Ljava/io/File;

    if-eqz v7, :cond_4

    invoke-virtual {v7}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v9

    goto :goto_2

    :cond_4
    move-object/from16 v9, v16

    :goto_2
    invoke-direct {v8, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Landroid/app/Application;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/pm/PackageManager;->getPackageInstaller()Landroid/content/pm/PackageInstaller;

    move-result-object v9

    invoke-static {v9}, Lcom/android/packageinstaller/utils/t;->c(Landroid/content/pm/PackageInstaller;)Z

    move-result v9

    if-eqz v9, :cond_5

    invoke-virtual {v6}, Landroid/app/Application;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/pm/PackageManager;->getPackageInstaller()Landroid/content/pm/PackageInstaller;

    move-result-object v9

    invoke-static {v9}, Lcom/android/packageinstaller/utils/t;->a(Landroid/content/pm/PackageInstaller;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_6

    :cond_5
    const-string v7, "com.miui.packageinstaller.provider"

    invoke-static {v6, v7, v8}, Landroidx/core/content/FileProvider;->getUriForFile(Landroid/content/Context;Ljava/lang/String;Ljava/io/File;)Landroid/net/Uri;

    move-result-object v7

    invoke-static {}, Lcom/android/packageinstaller/InstallerApplication;->g()Lcom/android/packageinstaller/InstallerApplication;

    move-result-object v8

    invoke-virtual {v8, v3, v7, v13}, Landroid/app/Application;->grantUriPermission(Ljava/lang/String;Landroid/net/Uri;I)V

    :cond_6
    const-string v3, "context"

    invoke-static {v6, v3}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v8, 0x1

    if-eqz v0, :cond_7

    move v9, v13

    goto :goto_3

    :cond_7
    move v9, v5

    :goto_3
    move-object/from16 v5, p0

    move-object/from16 v10, p1

    invoke-virtual/range {v5 .. v10}, Lm5/e1;->a(Landroid/content/Context;Landroid/net/Uri;ZZLjava/lang/String;)Lcom/miui/packageInstaller/model/ExtraGuardVirusInfoEntity;

    move-result-object v0

    if-eqz v0, :cond_8

    invoke-static {v0}, Lq2/f;->c(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    new-instance v3, Lcom/miui/packageInstaller/model/Virus;

    invoke-direct {v3}, Lcom/miui/packageInstaller/model/Virus;-><init>()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    :try_start_3
    invoke-static {v0}, Lq2/f;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/miui/packageInstaller/model/Virus;->setName(Ljava/lang/String;)V

    invoke-static {v0}, Lq2/f;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/miui/packageInstaller/model/Virus;->setVirusInfo(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    move-object v1, v3

    goto :goto_5

    :catch_1
    move-exception v0

    :try_start_4
    invoke-static {v2, v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_4

    :catch_2
    move-exception v0

    invoke-static {v2, v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_8
    :goto_4
    move-object/from16 v1, v16

    :goto_5
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, v14

    const-wide/16 v5, 0xbb8

    cmp-long v0, v2, v5

    if-gtz v0, :cond_a

    invoke-virtual/range {p2 .. p2}, Lcom/miui/packageInstaller/model/ApkInfo;->getSystemApp()Z

    move-result v0

    if-eqz v0, :cond_9

    goto :goto_6

    :cond_9
    const/16 v0, 0xbb8

    int-to-long v5, v0

    sub-long/2addr v5, v2

    goto :goto_7

    :cond_a
    :goto_6
    const-wide/16 v5, 0x0

    :goto_7
    iput-object v1, v4, Lm5/e1$b;->d:Ljava/lang/Object;

    iput v13, v4, Lm5/e1$b;->g:I

    invoke-static {v5, v6, v4}, Lv8/o0;->a(JLd8/d;)Ljava/lang/Object;

    move-result-object v0

    if-ne v0, v12, :cond_b

    return-object v12

    :cond_b
    :goto_8
    return-object v1
.end method
