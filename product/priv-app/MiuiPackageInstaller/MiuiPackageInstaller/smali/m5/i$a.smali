.class public final Lm5/i$a;
.super Ljava/lang/Object;

# interfaces
.implements Lgc/d;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lm5/i;->d()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lgc/d<",
        "Lcom/miui/packageInstaller/model/MiResponse<",
        "Lcom/miui/packageInstaller/model/CloudConfigModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Landroid/content/SharedPreferences;


# direct methods
.method constructor <init>(Landroid/content/SharedPreferences;)V
    .locals 0

    iput-object p1, p0, Lm5/i$a;->a:Landroid/content/SharedPreferences;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lgc/b;Lgc/t;)V
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lgc/b<",
            "Lcom/miui/packageInstaller/model/MiResponse<",
            "Lcom/miui/packageInstaller/model/CloudConfigModel;",
            ">;>;",
            "Lgc/t<",
            "Lcom/miui/packageInstaller/model/MiResponse<",
            "Lcom/miui/packageInstaller/model/CloudConfigModel;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "call"

    move-object/from16 v1, p1

    invoke-static {v1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "response"

    move-object/from16 v1, p2

    invoke-static {v1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p2 .. p2}, Lgc/t;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/packageInstaller/model/MiResponse;

    :try_start_0
    invoke-static {v0}, Lm8/i;->c(Ljava/lang/Object;)V

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/MiResponse;->isSuccess()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/MiResponse;->getData()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/packageInstaller/model/CloudConfigModel;

    if-eqz v0, :cond_3

    sget-object v1, Lm2/a;->b:Lm2/a$b;

    invoke-virtual {v1}, Lm2/a$b;->a()Lm2/a;

    move-result-object v2

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/CloudConfigModel;->getGuideOpenSafeModePopTips()Lcom/miui/packageInstaller/model/GuideOpenSafeModePopTips;

    move-result-object v3

    invoke-virtual {v2, v3}, Lm2/a;->f(Lcom/miui/packageInstaller/model/GuideOpenSafeModePopTips;)V

    invoke-virtual {v1}, Lm2/a$b;->a()Lm2/a;

    move-result-object v2

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/CloudConfigModel;->getInstallFinishedPopTips()Lcom/miui/packageInstaller/model/GuideOpenSafeModePopTips;

    move-result-object v3

    invoke-virtual {v2, v3}, Lm2/a;->e(Lcom/miui/packageInstaller/model/GuideOpenSafeModePopTips;)V

    invoke-virtual {v1}, Lm2/a$b;->a()Lm2/a;

    move-result-object v1

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/CloudConfigModel;->getThirdPartyInvokeInstallerPopTips()Lcom/miui/packageInstaller/model/ThirdPartyInvokeInstallerPopTips;

    move-result-object v2

    invoke-virtual {v1, v2}, Lm2/a;->g(Lcom/miui/packageInstaller/model/ThirdPartyInvokeInstallerPopTips;)V

    sget-object v3, Lr5/j;->b:Lr5/j$a;

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/CloudConfigModel;->getAsd()Z

    move-result v4

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/CloudConfigModel;->getAsdt()I

    move-result v1

    int-to-long v5, v1

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/CloudConfigModel;->getMfs()Z

    move-result v7

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/CloudConfigModel;->getMfst()I

    move-result v1

    int-to-long v8, v1

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/CloudConfigModel;->getBda()Z

    move-result v10

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/CloudConfigModel;->getBdat()I

    move-result v1

    int-to-long v11, v1

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/CloudConfigModel;->getMinorASGOSMPTips()Lcom/miui/packageInstaller/model/AppStoreGuideOpenSafeModePopTips;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v14, 0x1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/AppStoreGuideOpenSafeModePopTips;->getPopSwitch()Z

    move-result v1

    if-ne v1, v14, :cond_0

    move v13, v14

    goto :goto_0

    :cond_0
    move v13, v2

    :goto_0
    const-wide/16 v15, 0x0

    move v1, v14

    move-wide v14, v15

    invoke-virtual/range {v3 .. v15}, Lr5/j$a;->a(ZJZJZJZJ)V

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/CloudConfigModel;->getAlwaysAllowPopConfirm()I

    move-result v3

    if-ne v3, v1, :cond_1

    move v2, v1

    :cond_1
    invoke-static {v2}, Lt5/z;->c(Z)Z

    sget-object v1, Lf6/s;->a:Lf6/s$a;

    invoke-virtual {v1}, Lf6/s$a;->a()Lf6/s;

    move-result-object v2

    const-string v3, "safe_mode_is_open_cloud_config"

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/CloudConfigModel;->getSafeModeDefaultState()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lf6/s;->i(Ljava/lang/String;I)V

    invoke-virtual {v1}, Lf6/s$a;->a()Lf6/s;

    move-result-object v2

    const-string v3, "safe_mode_verify_type_cloud_config"

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/CloudConfigModel;->getSecureVerifyType()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lf6/s;->i(Ljava/lang/String;I)V

    invoke-virtual {v1}, Lf6/s$a;->a()Lf6/s;

    move-result-object v2

    const-string v3, "safe_mode_des_url_config"

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/CloudConfigModel;->getCndPureMode()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lf6/s;->k(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-object/from16 v2, p0

    :try_start_1
    iget-object v3, v2, Lm5/i$a;->a:Landroid/content/SharedPreferences;

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "last_sync_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-interface {v3, v4, v5, v6}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->apply()V

    invoke-virtual {v1}, Lf6/s$a;->a()Lf6/s;

    move-result-object v3

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/CloudConfigModel;->getBsl()Ljava/util/List;

    move-result-object v4

    invoke-virtual {v3, v4}, Lf6/s;->l(Ljava/util/List;)V

    invoke-virtual {v1}, Lf6/s$a;->a()Lf6/s;

    move-result-object v3

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/CloudConfigModel;->getCsl()Ljava/util/List;

    move-result-object v4

    invoke-virtual {v3, v4}, Lf6/s;->m(Ljava/util/List;)V

    const-string v3, "CloudConfig"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "update cloud config success result = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lf6/o;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/CloudConfigModel;->getSourceAuthorityResetInfo()Lcom/miui/packageInstaller/model/SourceAuthorityResetInfo;

    move-result-object v3

    if-eqz v3, :cond_2

    sget-object v3, Lm5/i;->a:Lm5/i;

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/CloudConfigModel;->getSourceAuthorityResetInfo()Lcom/miui/packageInstaller/model/SourceAuthorityResetInfo;

    move-result-object v4

    invoke-static {v3, v4}, Lm5/i;->b(Lm5/i;Lcom/miui/packageInstaller/model/SourceAuthorityResetInfo;)V

    :cond_2
    invoke-virtual {v1}, Lf6/s$a;->a()Lf6/s;

    move-result-object v3

    const-string v4, "minorLITips"

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/CloudConfigModel;->getMinorLITips()Lcom/miui/packageInstaller/model/MinorLaunchInstallerTips;

    move-result-object v5

    invoke-static {v5}, Lcom/android/packageinstaller/utils/j;->c(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "toJson(data.minorLITips)"

    invoke-static {v5, v6}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3, v4, v5}, Lf6/s;->k(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lf6/s$a;->a()Lf6/s;

    move-result-object v3

    const-string v4, "minorASGOSMPTips"

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/CloudConfigModel;->getMinorASGOSMPTips()Lcom/miui/packageInstaller/model/AppStoreGuideOpenSafeModePopTips;

    move-result-object v5

    invoke-static {v5}, Lcom/android/packageinstaller/utils/j;->c(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "toJson(data.minorASGOSMPTips)"

    invoke-static {v5, v6}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3, v4, v5}, Lf6/s;->k(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lf6/s$a;->a()Lf6/s;

    move-result-object v1

    const-string v3, "smofcTips"

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/CloudConfigModel;->getSmofcTips()Lcom/miui/packageInstaller/model/SafeModeOpenedFloatCardTips;

    move-result-object v0

    invoke-static {v0}, Lcom/android/packageinstaller/utils/j;->c(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v4, "toJson(data.smofcTips)"

    invoke-static {v0, v4}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, v3, v0}, Lf6/s;->k(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    :catch_0
    move-exception v0

    goto :goto_1

    :cond_3
    move-object/from16 v2, p0

    goto :goto_2

    :catch_1
    move-exception v0

    move-object/from16 v2, p0

    :goto_1
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :goto_2
    return-void
.end method

.method public b(Lgc/b;Ljava/lang/Throwable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lgc/b<",
            "Lcom/miui/packageInstaller/model/MiResponse<",
            "Lcom/miui/packageInstaller/model/CloudConfigModel;",
            ">;>;",
            "Ljava/lang/Throwable;",
            ")V"
        }
    .end annotation

    const-string v0, "call"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "t"

    invoke-static {p2, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/Throwable;->printStackTrace()V

    return-void
.end method
