.class public final Lm5/i;
.super Ljava/lang/Object;


# static fields
.field public static final a:Lm5/i;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lm5/i;

    invoke-direct {v0}, Lm5/i;-><init>()V

    sput-object v0, Lm5/i;->a:Lm5/i;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static synthetic a()V
    .locals 0

    invoke-static {}, Lm5/i;->d()V

    return-void
.end method

.method public static final synthetic b(Lm5/i;Lcom/miui/packageInstaller/model/SourceAuthorityResetInfo;)V
    .locals 0

    invoke-direct {p0, p1}, Lm5/i;->e(Lcom/miui/packageInstaller/model/SourceAuthorityResetInfo;)V

    return-void
.end method

.method public static final c()V
    .locals 2

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object v0

    sget-object v1, Lm5/h;->a:Lm5/h;

    invoke-virtual {v0, v1}, Lf6/z;->g(Ljava/lang/Runnable;)V

    return-void
.end method

.method private static final d()V
    .locals 15

    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    invoke-static {}, Lcom/android/packageinstaller/InstallerApplication;->g()Lcom/android/packageinstaller/InstallerApplication;

    move-result-object v0

    const-string v1, "cloudConfig"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/Application;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "last_sync_time"

    const-wide/16 v3, 0x0

    invoke-interface {v0, v1, v3, v4}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v5

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    sub-long/2addr v7, v5

    sget-object v1, Lcom/android/packageinstaller/InstallerApplication;->c:Lcom/android/packageinstaller/InstallerApplication;

    invoke-static {v1}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->getFirstBootTime(Landroid/content/Context;)J

    move-result-wide v5

    cmp-long v1, v5, v3

    if-lez v1, :cond_1

    sget-object v1, Lcom/android/packageinstaller/InstallerApplication;->c:Lcom/android/packageinstaller/InstallerApplication;

    invoke-static {v1}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->getFirstBootTime(Landroid/content/Context;)J

    move-result-wide v5

    goto :goto_0

    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    sget-object v1, Lcom/android/packageinstaller/InstallerApplication;->c:Lcom/android/packageinstaller/InstallerApplication;

    invoke-static {v1, v5, v6}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->setFirstBootTime(Landroid/content/Context;J)V

    :goto_0
    const-wide/32 v9, 0x36ee80

    long-to-double v9, v9

    new-instance v1, Ljava/util/Random;

    invoke-direct {v1}, Ljava/util/Random;-><init>()V

    invoke-virtual {v1}, Ljava/util/Random;->nextDouble()D

    move-result-wide v11

    const-wide v13, 0x3fb999999999999aL    # 0.1

    add-double/2addr v11, v13

    mul-double/2addr v9, v11

    const/16 v1, 0x18

    int-to-double v11, v1

    mul-double/2addr v9, v11

    double-to-long v9, v9

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "time pass : "

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v11, " randomTime = "

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v11, " new Random().nextInt() = "

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v11, Ljava/util/Random;

    invoke-direct {v11}, Ljava/util/Random;-><init>()V

    invoke-virtual {v11}, Ljava/util/Random;->nextDouble()D

    move-result-wide v11

    add-double/2addr v11, v13

    invoke-virtual {v1, v11, v12}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v11, "CloudConfig"

    invoke-static {v11, v1}, Lf6/o;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    cmp-long v1, v7, v3

    if-ltz v1, :cond_3

    cmp-long v1, v7, v9

    if-gtz v1, :cond_3

    const-wide/32 v3, 0x5265c00

    cmp-long v1, v7, v3

    if-lez v1, :cond_2

    goto :goto_1

    :cond_2
    const-string v0, "skip sync cloud config"

    invoke-static {v11, v0}, Lf6/o;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    goto :goto_2

    :cond_3
    :goto_1
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-static {}, Lcom/android/packageinstaller/InstallerApplication;->g()Lcom/android/packageinstaller/InstallerApplication;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Application;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "unknown_source_install_config_version"

    invoke-static {v3, v4, v2}, Lya/a;->b(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    const-string v4, "firstboot_time"

    invoke-interface {v1, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "source_auth_reset_v"

    invoke-interface {v1, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-class v2, Lu5/j;

    invoke-static {v2}, Lu5/k;->f(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lu5/j;

    invoke-interface {v2, v1}, Lu5/j;->a(Ljava/util/Map;)Lgc/b;

    move-result-object v1

    new-instance v2, Lm5/i$a;

    invoke-direct {v2, v0}, Lm5/i$a;-><init>(Landroid/content/SharedPreferences;)V

    invoke-interface {v1, v2}, Lgc/b;->M(Lgc/d;)V

    :goto_2
    return-void
.end method

.method private final e(Lcom/miui/packageInstaller/model/SourceAuthorityResetInfo;)V
    .locals 2

    invoke-static {}, Lcom/android/packageinstaller/InstallerApplication;->g()Lcom/android/packageinstaller/InstallerApplication;

    move-result-object v0

    const-string v1, "getInstance()"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lm2/b;->g(Landroid/content/Context;)Lm2/b;

    move-result-object v0

    invoke-static {p1}, Lcom/android/packageinstaller/utils/j;->c(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lm2/b;->O(Ljava/lang/String;)V

    return-void
.end method
