.class public final Lm5/d;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lm5/d$a;
    }
.end annotation


# static fields
.field public static final a:Lm5/d$a;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lm5/d$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lm5/d$a;-><init>(Lm8/g;)V

    sput-object v0, Lm5/d;->a:Lm5/d$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private final a()Lcom/miui/packageInstaller/model/CloudParams;
    .locals 2

    new-instance v0, Lcom/miui/packageInstaller/model/CloudParams;

    invoke-direct {v0}, Lcom/miui/packageInstaller/model/CloudParams;-><init>()V

    const-string v1, "500_error"

    iput-object v1, v0, Lcom/miui/packageInstaller/model/CloudParams;->categoryAbbreviation:Ljava/lang/String;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/miui/packageInstaller/model/CloudParams;->showAdsBefore:Z

    return-object v0
.end method

.method private final b()Ljava/lang/String;
    .locals 4

    new-instance v0, Lm2/c;

    invoke-static {}, Lcom/android/packageinstaller/InstallerApplication;->g()Lcom/android/packageinstaller/InstallerApplication;

    move-result-object v1

    const-string v2, "uuid"

    invoke-direct {v0, v1, v2}, Lm2/c;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    new-instance v1, Lm2/c;

    invoke-static {}, Lcom/android/packageinstaller/InstallerApplication;->g()Lcom/android/packageinstaller/InstallerApplication;

    move-result-object v3

    invoke-direct {v1, v3, v2}, Lm2/c;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    const-string v2, "device_uuid"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lm2/c;->d(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lm2/c;->m(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const-string v0, "deviceUUID"

    invoke-static {v1, v0}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v1
.end method


# virtual methods
.method public final c(Landroid/content/Context;Lm5/e;ILcom/miui/packageInstaller/model/ApkInfo;Ld8/d;)Ljava/lang/Object;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lm5/e;",
            "I",
            "Lcom/miui/packageInstaller/model/ApkInfo;",
            "Ld8/d<",
            "-",
            "Lcom/miui/packageInstaller/model/CloudParams;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v0, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p4

    move-object/from16 v4, p5

    const-class v5, Lu5/b;

    instance-of v6, v4, Lm5/d$b;

    if-eqz v6, :cond_0

    move-object v6, v4

    check-cast v6, Lm5/d$b;

    iget v7, v6, Lm5/d$b;->j:I

    const/high16 v8, -0x80000000

    and-int v9, v7, v8

    if-eqz v9, :cond_0

    sub-int/2addr v7, v8

    iput v7, v6, Lm5/d$b;->j:I

    goto :goto_0

    :cond_0
    new-instance v6, Lm5/d$b;

    invoke-direct {v6, v1, v4}, Lm5/d$b;-><init>(Lm5/d;Ld8/d;)V

    :goto_0
    iget-object v4, v6, Lm5/d$b;->h:Ljava/lang/Object;

    invoke-static {}, Le8/b;->c()Ljava/lang/Object;

    move-result-object v7

    iget v8, v6, Lm5/d$b;->j:I

    const-string v10, "callingPackage.callingPackage"

    const/4 v11, 0x2

    const-string v12, ""

    const/4 v13, 0x1

    if-eqz v8, :cond_3

    if-eq v8, v13, :cond_2

    if-ne v8, v11, :cond_1

    iget-object v0, v6, Lm5/d$b;->g:Ljava/lang/Object;

    check-cast v0, Lcom/miui/packageInstaller/model/ApkInfo;

    iget-object v2, v6, Lm5/d$b;->f:Ljava/lang/Object;

    check-cast v2, Lm5/e;

    iget-object v3, v6, Lm5/d$b;->e:Ljava/lang/Object;

    check-cast v3, Landroid/content/Context;

    iget-object v5, v6, Lm5/d$b;->d:Ljava/lang/Object;

    check-cast v5, Lm5/d;

    :try_start_0
    invoke-static {v4}, La8/n;->b(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object/from16 v16, v3

    move-object v3, v0

    move-object/from16 v0, v16

    goto/16 :goto_6

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget-object v0, v6, Lm5/d$b;->g:Ljava/lang/Object;

    check-cast v0, Lcom/miui/packageInstaller/model/ApkInfo;

    iget-object v2, v6, Lm5/d$b;->f:Ljava/lang/Object;

    check-cast v2, Lm5/e;

    iget-object v3, v6, Lm5/d$b;->e:Ljava/lang/Object;

    check-cast v3, Landroid/content/Context;

    iget-object v5, v6, Lm5/d$b;->d:Ljava/lang/Object;

    check-cast v5, Lm5/d;

    :try_start_1
    invoke-static {v4}, La8/n;->b(Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-object/from16 v16, v3

    move-object v3, v0

    move-object/from16 v0, v16

    goto/16 :goto_6

    :catch_0
    move-exception v0

    goto/16 :goto_9

    :cond_3
    invoke-static {v4}, La8/n;->b(Ljava/lang/Object;)V

    invoke-virtual/range {p4 .. p4}, Lcom/miui/packageInstaller/model/ApkInfo;->getSystemApp()Z

    move-result v4

    if-eqz v4, :cond_4

    new-instance v0, Lcom/miui/packageInstaller/model/CloudParams;

    invoke-direct {v0}, Lcom/miui/packageInstaller/model/CloudParams;-><init>()V

    iput-boolean v13, v0, Lcom/miui/packageInstaller/model/CloudParams;->useSystemAppRules:Z

    const-string v2, "miui"

    iput-object v2, v0, Lcom/miui/packageInstaller/model/CloudParams;->categoryAbbreviation:Ljava/lang/String;

    return-object v0

    :cond_4
    invoke-static/range {p1 .. p1}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->isInstallRiskEnabled(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_5

    invoke-static/range {p1 .. p1}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->isSafeModelEnable(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_5

    new-instance v0, Lcom/miui/packageInstaller/model/CloudParams;

    invoke-direct {v0}, Lcom/miui/packageInstaller/model/CloudParams;-><init>()V

    const-string v2, "no_block"

    iput-object v2, v0, Lcom/miui/packageInstaller/model/CloudParams;->categoryAbbreviation:Ljava/lang/String;

    return-object v0

    :cond_5
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    invoke-virtual/range {p2 .. p2}, Lm5/e;->j()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8, v10}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v14, "source"

    invoke-interface {v4, v14, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static/range {p3 .. p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    const-string v14, "rn"

    invoke-interface {v4, v14, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v8, v2, Lm5/e;->e:Ljava/lang/String;

    const-string v14, "callingPackage.name"

    invoke-static {v8, v14}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v14, "sourceAppName"

    invoke-interface {v4, v14, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual/range {p4 .. p4}, Lcom/miui/packageInstaller/model/ApkInfo;->getVersionCode()I

    move-result v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    const-string v14, "versionCode"

    invoke-interface {v4, v14, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual/range {p4 .. p4}, Lcom/miui/packageInstaller/model/ApkInfo;->getVersionName()Ljava/lang/String;

    move-result-object v8

    const-string v14, "versionName"

    invoke-interface {v4, v14, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual/range {p4 .. p4}, Lcom/miui/packageInstaller/model/ApkInfo;->getInstalledVersionCode()I

    move-result v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    const-string v14, "localVersionCode"

    invoke-interface {v4, v14, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual/range {p4 .. p4}, Lcom/miui/packageInstaller/model/ApkInfo;->isOtherVersionInstalled()Z

    move-result v8

    const-string v14, "1"

    if-eqz v8, :cond_6

    const-string v8, "2"

    goto :goto_1

    :cond_6
    move-object v8, v14

    :goto_1
    const-string v15, "installationMode"

    invoke-interface {v4, v15, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual/range {p4 .. p4}, Lcom/miui/packageInstaller/model/ApkInfo;->getPackageName()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    const-string v15, "packageName"

    invoke-interface {v4, v15, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual/range {p4 .. p4}, Lcom/miui/packageInstaller/model/ApkInfo;->getLabel()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    const-string v15, "appName"

    invoke-interface {v4, v15, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual/range {p4 .. p4}, Lcom/miui/packageInstaller/model/ApkInfo;->getApkMd5()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    const-string v15, "apkMd5"

    invoke-interface {v4, v15, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual/range {p4 .. p4}, Lcom/miui/packageInstaller/model/ApkInfo;->getApkSignature()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    const-string v15, "apkSignature"

    invoke-interface {v4, v15, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual/range {p4 .. p4}, Lcom/miui/packageInstaller/model/ApkInfo;->getApkSignature2()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    const-string v15, "apkSignature2"

    invoke-interface {v4, v15, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual/range {p4 .. p4}, Lcom/miui/packageInstaller/model/ApkInfo;->getApkSignatureSha1()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    const-string v15, "apkSignatureSha1"

    invoke-interface {v4, v15, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual/range {p4 .. p4}, Lcom/miui/packageInstaller/model/ApkInfo;->getApkSignatureSha256()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    const-string v15, "apkSignatureSha256"

    invoke-interface {v4, v15, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->o()Ljava/lang/String;

    move-result-object v8

    const-string v15, "getUA()"

    invoke-static {v8, v15}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v15, "ua"

    invoke-interface {v4, v15, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget v8, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v15, 0x1c

    if-gt v8, v15, :cond_7

    invoke-static {}, Lcom/android/packageinstaller/InstallerApplication;->g()Lcom/android/packageinstaller/InstallerApplication;

    move-result-object v15

    invoke-static {v15}, Lcom/android/packageinstaller/utils/g;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v15

    const-string v9, "getImeiMd5(InstallerApplication.getInstance())"

    invoke-static {v15, v9}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v9, "imeiMd5"

    invoke-interface {v4, v9, v15}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_7
    const-string v9, "adTagId"

    const-string v15, "1.19.k.1"

    invoke-interface {v4, v9, v15}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->b()Ljava/lang/String;

    move-result-object v9

    const-string v15, "getCpuArchitecture()"

    invoke-static {v9, v15}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v15, "cpuArchitecture"

    invoke-interface {v4, v15, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    const-string v9, "androidSdkVersion"

    invoke-interface {v4, v9, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Lcom/android/packageinstaller/InstallerApplication;->g()Lcom/android/packageinstaller/InstallerApplication;

    move-result-object v8

    invoke-static {v8}, Lm2/b;->g(Landroid/content/Context;)Lm2/b;

    move-result-object v8

    invoke-virtual {v8}, Lm2/b;->r()Z

    move-result v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v8

    const-string v9, "irec"

    invoke-interface {v4, v9, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v8, Lq2/a;->c:Ljava/lang/String;

    const-string v9, "sClientSessionId"

    invoke-static {v8, v9}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v9, "client_session_id"

    invoke-interface {v4, v9, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v8, "null cannot be cast to non-null type com.miui.packageInstaller.analytics.IPage"

    invoke-static {v0, v8}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v8, v0

    check-cast v8, Lo5/a;

    invoke-interface {v8}, Lo5/a;->z()Ljava/lang/String;

    move-result-object v8

    const-string v9, "context as IPage).launchRef"

    invoke-static {v8, v9}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v9, "launch_source_package"

    invoke-interface {v4, v9, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual/range {p4 .. p4}, Lcom/miui/packageInstaller/model/ApkInfo;->getRealFileName()Ljava/lang/String;

    move-result-object v8

    if-nez v8, :cond_8

    move-object v8, v12

    :cond_8
    const-string v9, "apkFileName"

    invoke-interface {v4, v9, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Lea/a;->k()I

    move-result v8

    const/4 v9, -0x1

    const-string v15, "unknown"

    if-eq v8, v9, :cond_c

    if-eqz v8, :cond_b

    if-eq v8, v13, :cond_a

    if-eq v8, v11, :cond_9

    goto :goto_2

    :cond_9
    const-string v15, "high"

    goto :goto_2

    :cond_a
    const-string v15, "middle"

    goto :goto_2

    :cond_b
    const-string v15, "low"

    :cond_c
    :goto_2
    const-string v8, "device_type"

    invoke-interface {v4, v8, v15}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct/range {p0 .. p0}, Lm5/d;->b()Ljava/lang/String;

    move-result-object v8

    const-string v9, "client_uuid"

    invoke-interface {v4, v9, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->isPersonalizedAdEnabled()Z

    move-result v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v8

    const-string v9, "isPersonalizedAdEnabled"

    invoke-interface {v4, v9, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-boolean v8, Lcom/android/packageinstaller/utils/g;->g:Z

    const-string v9, "0"

    if-eqz v8, :cond_d

    move-object v8, v14

    goto :goto_3

    :cond_d
    move-object v8, v9

    :goto_3
    const-string v15, "d_bit"

    invoke-interface {v4, v15, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual/range {p4 .. p4}, Lcom/miui/packageInstaller/model/ApkInfo;->getApkAbi()Ljava/lang/String;

    move-result-object v8

    const-string v15, "apk_bit"

    invoke-interface {v4, v15, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v8, Lcom/android/packageinstaller/InstallerApplication;->c:Lcom/android/packageinstaller/InstallerApplication;

    invoke-static {v8}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->isInstallRiskEnabled(Landroid/content/Context;)Z

    move-result v8

    if-eqz v8, :cond_e

    move-object v8, v14

    goto :goto_4

    :cond_e
    move-object v8, v9

    :goto_4
    const-string v15, "app_sec_check"

    invoke-interface {v4, v15, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->getSecureSettingLocation()Ljava/lang/String;

    move-result-object v8

    const-string v15, "getSecureSettingLocation()"

    invoke-static {v8, v15}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v15, "se_location"

    invoke-interface {v4, v15, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result v8

    if-eqz v8, :cond_f

    goto :goto_5

    :cond_f
    move-object v14, v9

    :goto_5
    const-string v8, "phoneDevice"

    invoke-interface {v4, v8, v14}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v8, Lf6/d0;->a:Lf6/d0;

    invoke-virtual/range {p4 .. p4}, Lcom/miui/packageInstaller/model/ApkInfo;->getChannelMessage()Ljava/lang/String;

    move-result-object v9

    if-nez v9, :cond_10

    move-object v9, v12

    :cond_10
    invoke-virtual {v8, v9}, Lf6/d0;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v9, "appBundleMessage"

    invoke-interface {v4, v9, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v8, Lq2/a;->a:Landroid/util/ArrayMap;

    invoke-virtual {v4, v8}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    :try_start_2
    invoke-static/range {p1 .. p1}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->isSafeModelEnable(Landroid/content/Context;)Z

    move-result v8

    if-eqz v8, :cond_12

    invoke-static {v5}, Lu5/k;->f(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lu5/b;

    iput-object v1, v6, Lm5/d$b;->d:Ljava/lang/Object;

    iput-object v0, v6, Lm5/d$b;->e:Ljava/lang/Object;

    iput-object v2, v6, Lm5/d$b;->f:Ljava/lang/Object;

    iput-object v3, v6, Lm5/d$b;->g:Ljava/lang/Object;

    iput v13, v6, Lm5/d$b;->j:I

    invoke-interface {v5, v4, v6}, Lu5/b;->g(Ljava/util/Map;Ld8/d;)Ljava/lang/Object;

    move-result-object v4

    if-ne v4, v7, :cond_11

    return-object v7

    :cond_11
    move-object v5, v1

    :goto_6
    check-cast v4, Lgc/t;

    goto :goto_7

    :cond_12
    invoke-static {v5}, Lu5/k;->f(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lu5/b;

    iput-object v1, v6, Lm5/d$b;->d:Ljava/lang/Object;

    iput-object v0, v6, Lm5/d$b;->e:Ljava/lang/Object;

    iput-object v2, v6, Lm5/d$b;->f:Ljava/lang/Object;

    iput-object v3, v6, Lm5/d$b;->g:Ljava/lang/Object;

    iput v11, v6, Lm5/d$b;->j:I

    invoke-interface {v5, v4, v6}, Lu5/b;->f(Ljava/util/Map;Ld8/d;)Ljava/lang/Object;

    move-result-object v4

    if-ne v4, v7, :cond_13

    return-object v7

    :cond_13
    move-object v5, v1

    goto :goto_6

    :goto_7
    invoke-virtual {v4}, Lgc/t;->d()Z

    move-result v6

    if-eqz v6, :cond_19

    invoke-virtual {v4}, Lgc/t;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lpb/c0;

    if-eqz v4, :cond_14

    new-instance v5, Lp4/e;

    invoke-direct {v5}, Lp4/e;-><init>()V

    invoke-virtual {v4}, Lpb/c0;->s()Ljava/lang/String;

    move-result-object v4

    const-class v6, Lcom/miui/packageInstaller/model/CloudParams;

    invoke-virtual {v5, v4, v6}, Lp4/e;->h(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/miui/packageInstaller/model/CloudParams;

    goto :goto_8

    :cond_14
    const/4 v4, 0x0

    :goto_8
    if-eqz v4, :cond_18

    sget-object v5, Lt5/f0;->a:Lt5/f0;

    invoke-virtual {v2}, Lm5/e;->j()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v10}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v5, v2, v4}, Lt5/f0;->j(Ljava/lang/String;Lcom/miui/packageInstaller/model/CloudParams;)V

    iget-object v2, v4, Lcom/miui/packageInstaller/model/CloudParams;->authFunction:Lcom/miui/packageInstaller/model/AuthFunction;

    if-eqz v2, :cond_15

    const-string v5, "authFunction"

    invoke-static {v2, v5}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lm2/b;->g(Landroid/content/Context;)Lm2/b;

    move-result-object v5

    invoke-virtual {v2}, Lcom/miui/packageInstaller/model/AuthFunction;->getInstallerOpenSafetyModel()Z

    move-result v6

    invoke-virtual {v5, v6}, Lm2/b;->E(Z)V

    invoke-static {v0}, Lm2/b;->g(Landroid/content/Context;)Lm2/b;

    move-result-object v5

    invoke-virtual {v2}, Lcom/miui/packageInstaller/model/AuthFunction;->getInstallerCloseSafetyModel()Z

    move-result v6

    invoke-virtual {v5, v6}, Lm2/b;->y(Z)V

    invoke-static {v0}, Lm2/b;->g(Landroid/content/Context;)Lm2/b;

    move-result-object v0

    invoke-virtual {v2}, Lcom/miui/packageInstaller/model/AuthFunction;->getInstallerSingleAuth()Z

    move-result v2

    invoke-virtual {v0, v2}, Lm2/b;->N(Z)V

    :cond_15
    iget-object v0, v4, Lcom/miui/packageInstaller/model/CloudParams;->uiConfig:Lcom/miui/packageInstaller/model/UiConfig;

    if-eqz v0, :cond_18

    iget v0, v0, Lcom/miui/packageInstaller/model/UiConfig;->l:I

    sget-object v2, Lcom/miui/packageInstaller/util/XiaoMiSafe;->a:Lcom/miui/packageInstaller/util/XiaoMiSafe$Companion;

    invoke-virtual {v3}, Lcom/miui/packageInstaller/model/ApkInfo;->getFileUri()Landroid/net/Uri;

    move-result-object v3

    if-eqz v3, :cond_16

    invoke-virtual {v3}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_17

    :cond_16
    move-object v3, v12

    :cond_17
    const-string v5, "apkInfo.fileUri?.path ?: \"\""

    invoke-static {v3, v5}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2, v3, v0}, Lcom/miui/packageInstaller/util/XiaoMiSafe$Companion;->checkPackageSafe(Ljava/lang/String;I)Ljava/lang/String;

    :cond_18
    return-object v4

    :cond_19
    invoke-virtual {v4}, Lgc/t;->b()I

    move-result v0

    const/16 v2, 0x23a

    if-ne v0, v2, :cond_1b

    invoke-direct {v5}, Lm5/d;->a()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    return-object v0

    :goto_9
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1a

    goto :goto_a

    :cond_1a
    move-object v12, v2

    :goto_a
    const-string v2, "AppMarket"

    invoke-static {v2, v12, v0}, Lf6/o;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :cond_1b
    const/4 v2, 0x0

    return-object v2
.end method
