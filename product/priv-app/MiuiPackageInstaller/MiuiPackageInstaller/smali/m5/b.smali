.class public Lm5/b;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lm5/b$b;
    }
.end annotation


# static fields
.field private static a:I

.field private static b:I

.field private static c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lm5/b$b;",
            ">;"
        }
    .end annotation
.end field

.field private static d:Z

.field private static e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private static f:Landroid/app/Application$ActivityLifecycleCallbacks;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lm5/b;->c:Ljava/util/List;

    const/4 v0, 0x0

    sput-boolean v0, Lm5/b;->d:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lm5/b;->e:Ljava/util/List;

    new-instance v0, Lm5/b$a;

    invoke-direct {v0}, Lm5/b$a;-><init>()V

    sput-object v0, Lm5/b;->f:Landroid/app/Application$ActivityLifecycleCallbacks;

    return-void
.end method

.method public static synthetic a(Lm5/b$b;)V
    .locals 0

    invoke-static {p0}, Lm5/b;->q(Lm5/b$b;)V

    return-void
.end method

.method static synthetic b()I
    .locals 1

    sget v0, Lm5/b;->b:I

    return v0
.end method

.method static synthetic c()I
    .locals 2

    sget v0, Lm5/b;->b:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lm5/b;->b:I

    return v0
.end method

.method static synthetic d()I
    .locals 2

    sget v0, Lm5/b;->b:I

    add-int/lit8 v1, v0, -0x1

    sput v1, Lm5/b;->b:I

    return v0
.end method

.method static synthetic e()Ljava/util/List;
    .locals 1

    sget-object v0, Lm5/b;->e:Ljava/util/List;

    return-object v0
.end method

.method static synthetic f()I
    .locals 1

    sget v0, Lm5/b;->a:I

    return v0
.end method

.method static synthetic g()I
    .locals 2

    sget v0, Lm5/b;->a:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lm5/b;->a:I

    return v0
.end method

.method static synthetic h()I
    .locals 2

    sget v0, Lm5/b;->a:I

    add-int/lit8 v1, v0, -0x1

    sput v1, Lm5/b;->a:I

    return v0
.end method

.method static synthetic i()Z
    .locals 1

    sget-boolean v0, Lm5/b;->d:Z

    return v0
.end method

.method static synthetic j(Z)Z
    .locals 0

    sput-boolean p0, Lm5/b;->d:Z

    return p0
.end method

.method static synthetic k()Ljava/util/List;
    .locals 1

    sget-object v0, Lm5/b;->c:Ljava/util/List;

    return-object v0
.end method

.method public static l()V
    .locals 3

    sget-object v0, Lm5/b;->e:Ljava/util/List;

    if-eqz v0, :cond_3

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_1

    :cond_0
    const/4 v0, 0x0

    sget-object v1, Lm5/b;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    :goto_0
    if-ge v0, v1, :cond_2

    sget-object v2, Lm5/b;->e:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_1

    sget-object v2, Lm5/b;->e:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->finish()V

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    sget-object v0, Lm5/b;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    :cond_3
    :goto_1
    return-void
.end method

.method public static m()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    sget-object v1, Lm5/b;->e:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public static n()Landroid/app/Activity;
    .locals 3

    sget-object v0, Lm5/b;->e:Ljava/util/List;

    invoke-static {v0}, Lk5/b;->a(Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lm5/b;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-lt v0, v1, :cond_0

    sget-object v0, Lm5/b;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    sub-int/2addr v2, v1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public static o(Landroid/app/Application;)V
    .locals 1

    sget-object v0, Lm5/b;->f:Landroid/app/Application$ActivityLifecycleCallbacks;

    invoke-virtual {p0, v0}, Landroid/app/Application;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    return-void
.end method

.method public static p(Landroid/content/Context;)Z
    .locals 2

    instance-of v0, p0, Landroid/app/Activity;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    check-cast p0, Landroid/app/Activity;

    invoke-virtual {p0}, Landroid/app/Activity;->isDestroyed()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Landroid/app/Activity;->isFinishing()Z

    move-result p0

    if-nez p0, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method private static synthetic q(Lm5/b$b;)V
    .locals 1

    sget-object v0, Lm5/b;->c:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lm5/b;->c:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public static r(Lm5/b$b;)V
    .locals 2

    if-eqz p0, :cond_0

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object v0

    new-instance v1, Lm5/a;

    invoke-direct {v1, p0}, Lm5/a;-><init>(Lm5/b$b;)V

    invoke-virtual {v0, v1}, Lf6/z;->e(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method
