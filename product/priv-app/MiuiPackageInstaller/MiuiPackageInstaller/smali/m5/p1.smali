.class public abstract Lm5/p1;
.super Lq2/b;


# instance fields
.field private u:Ljava/lang/String;

.field private v:Lmiuix/appcompat/app/a;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lq2/b;-><init>()V

    return-void
.end method


# virtual methods
.method public final J0(Ljava/lang/String;)V
    .locals 1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p1, ""

    :cond_0
    iput-object p1, p0, Lm5/p1;->u:Ljava/lang/String;

    invoke-virtual {p0, p1}, Lm5/p1;->setTitle(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final K0()Lmiuix/appcompat/app/a;
    .locals 1

    invoke-virtual {p0}, Lmiuix/appcompat/app/j;->r0()Lmiuix/appcompat/app/a;

    move-result-object v0

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lq2/b;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lm5/p1;->K0()Lmiuix/appcompat/app/a;

    move-result-object p1

    iput-object p1, p0, Lm5/p1;->v:Lmiuix/appcompat/app/a;

    if-eqz p1, :cond_0

    invoke-static {p0}, Lcom/android/packageinstaller/utils/g;->t(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lm5/p1;->v:Lmiuix/appcompat/app/a;

    invoke-static {p1}, Lm8/i;->c(Ljava/lang/Object;)V

    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    const/high16 v1, -0x1000000

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p1, v0}, Ld/a;->s(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1

    iget-object v0, p0, Lm5/p1;->v:Lmiuix/appcompat/app/a;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Ld/a;->v(Ljava/lang/CharSequence;)V

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    return-void
.end method
