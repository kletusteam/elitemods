.class public final Li3/c;
.super Ljava/lang/Object;

# interfaces
.implements Li3/e;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Li3/e<",
        "Landroid/graphics/drawable/Drawable;",
        "[B>;"
    }
.end annotation


# instance fields
.field private final a:Lx2/d;

.field private final b:Li3/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Li3/e<",
            "Landroid/graphics/Bitmap;",
            "[B>;"
        }
    .end annotation
.end field

.field private final c:Li3/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Li3/e<",
            "Lh3/c;",
            "[B>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lx2/d;Li3/e;Li3/e;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lx2/d;",
            "Li3/e<",
            "Landroid/graphics/Bitmap;",
            "[B>;",
            "Li3/e<",
            "Lh3/c;",
            "[B>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Li3/c;->a:Lx2/d;

    iput-object p2, p0, Li3/c;->b:Li3/e;

    iput-object p3, p0, Li3/c;->c:Li3/e;

    return-void
.end method

.method private static b(Lw2/v;)Lw2/v;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lw2/v<",
            "Landroid/graphics/drawable/Drawable;",
            ">;)",
            "Lw2/v<",
            "Lh3/c;",
            ">;"
        }
    .end annotation

    return-object p0
.end method


# virtual methods
.method public a(Lw2/v;Lu2/h;)Lw2/v;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lw2/v<",
            "Landroid/graphics/drawable/Drawable;",
            ">;",
            "Lu2/h;",
            ")",
            "Lw2/v<",
            "[B>;"
        }
    .end annotation

    invoke-interface {p1}, Lw2/v;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    instance-of v1, v0, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v1, :cond_0

    iget-object p1, p0, Li3/c;->b:Li3/e;

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v1, p0, Li3/c;->a:Lx2/d;

    invoke-static {v0, v1}, Ld3/e;->f(Landroid/graphics/Bitmap;Lx2/d;)Ld3/e;

    move-result-object v0

    invoke-interface {p1, v0, p2}, Li3/e;->a(Lw2/v;Lu2/h;)Lw2/v;

    move-result-object p1

    return-object p1

    :cond_0
    instance-of v0, v0, Lh3/c;

    if-eqz v0, :cond_1

    iget-object v0, p0, Li3/c;->c:Li3/e;

    invoke-static {p1}, Li3/c;->b(Lw2/v;)Lw2/v;

    move-result-object p1

    invoke-interface {v0, p1, p2}, Li3/e;->a(Lw2/v;Lu2/h;)Lw2/v;

    move-result-object p1

    return-object p1

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method
