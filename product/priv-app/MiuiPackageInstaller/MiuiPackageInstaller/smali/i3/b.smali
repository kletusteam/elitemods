.class public Li3/b;
.super Ljava/lang/Object;

# interfaces
.implements Li3/e;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Li3/e<",
        "Landroid/graphics/Bitmap;",
        "Landroid/graphics/drawable/BitmapDrawable;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lq3/j;->d(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/content/res/Resources;

    iput-object p1, p0, Li3/b;->a:Landroid/content/res/Resources;

    return-void
.end method


# virtual methods
.method public a(Lw2/v;Lu2/h;)Lw2/v;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lw2/v<",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Lu2/h;",
            ")",
            "Lw2/v<",
            "Landroid/graphics/drawable/BitmapDrawable;",
            ">;"
        }
    .end annotation

    iget-object p2, p0, Li3/b;->a:Landroid/content/res/Resources;

    invoke-static {p2, p1}, Ld3/v;->f(Landroid/content/res/Resources;Lw2/v;)Lw2/v;

    move-result-object p1

    return-object p1
.end method
