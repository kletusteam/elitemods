.class public final Lr4/h;
.super Ljava/util/AbstractMap;

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lr4/h$c;,
        Lr4/h$b;,
        Lr4/h$d;,
        Lr4/h$e;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/AbstractMap<",
        "TK;TV;>;",
        "Ljava/io/Serializable;"
    }
.end annotation


# static fields
.field private static final h:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator<",
            "Ljava/lang/Comparable;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field a:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator<",
            "-TK;>;"
        }
    .end annotation
.end field

.field b:Lr4/h$e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lr4/h$e<",
            "TK;TV;>;"
        }
    .end annotation
.end field

.field c:I

.field d:I

.field final e:Lr4/h$e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lr4/h$e<",
            "TK;TV;>;"
        }
    .end annotation
.end field

.field private f:Lr4/h$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lr4/h<",
            "TK;TV;>.b;"
        }
    .end annotation
.end field

.field private g:Lr4/h$c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lr4/h<",
            "TK;TV;>.c;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lr4/h;

    new-instance v0, Lr4/h$a;

    invoke-direct {v0}, Lr4/h$a;-><init>()V

    sput-object v0, Lr4/h;->h:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    sget-object v0, Lr4/h;->h:Ljava/util/Comparator;

    invoke-direct {p0, v0}, Lr4/h;-><init>(Ljava/util/Comparator;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/Comparator;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Comparator<",
            "-TK;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/util/AbstractMap;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lr4/h;->c:I

    iput v0, p0, Lr4/h;->d:I

    new-instance v0, Lr4/h$e;

    invoke-direct {v0}, Lr4/h$e;-><init>()V

    iput-object v0, p0, Lr4/h;->e:Lr4/h$e;

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    sget-object p1, Lr4/h;->h:Ljava/util/Comparator;

    :goto_0
    iput-object p1, p0, Lr4/h;->a:Ljava/util/Comparator;

    return-void
.end method

.method private a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 0

    if-eq p1, p2, :cond_1

    if-eqz p1, :cond_0

    invoke-virtual {p1, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method private e(Lr4/h$e;Z)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lr4/h$e<",
            "TK;TV;>;Z)V"
        }
    .end annotation

    :goto_0
    if-eqz p1, :cond_e

    iget-object v0, p1, Lr4/h$e;->b:Lr4/h$e;

    iget-object v1, p1, Lr4/h$e;->c:Lr4/h$e;

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    iget v3, v0, Lr4/h$e;->h:I

    goto :goto_1

    :cond_0
    move v3, v2

    :goto_1
    if-eqz v1, :cond_1

    iget v4, v1, Lr4/h$e;->h:I

    goto :goto_2

    :cond_1
    move v4, v2

    :goto_2
    sub-int v5, v3, v4

    const/4 v6, -0x2

    if-ne v5, v6, :cond_6

    iget-object v0, v1, Lr4/h$e;->b:Lr4/h$e;

    iget-object v3, v1, Lr4/h$e;->c:Lr4/h$e;

    if-eqz v3, :cond_2

    iget v3, v3, Lr4/h$e;->h:I

    goto :goto_3

    :cond_2
    move v3, v2

    :goto_3
    if-eqz v0, :cond_3

    iget v2, v0, Lr4/h$e;->h:I

    :cond_3
    sub-int/2addr v2, v3

    const/4 v0, -0x1

    if-eq v2, v0, :cond_5

    if-nez v2, :cond_4

    if-nez p2, :cond_4

    goto :goto_4

    :cond_4
    invoke-direct {p0, v1}, Lr4/h;->j(Lr4/h$e;)V

    :cond_5
    :goto_4
    invoke-direct {p0, p1}, Lr4/h;->i(Lr4/h$e;)V

    if-eqz p2, :cond_d

    goto :goto_7

    :cond_6
    const/4 v1, 0x2

    const/4 v6, 0x1

    if-ne v5, v1, :cond_b

    iget-object v1, v0, Lr4/h$e;->b:Lr4/h$e;

    iget-object v3, v0, Lr4/h$e;->c:Lr4/h$e;

    if-eqz v3, :cond_7

    iget v3, v3, Lr4/h$e;->h:I

    goto :goto_5

    :cond_7
    move v3, v2

    :goto_5
    if-eqz v1, :cond_8

    iget v2, v1, Lr4/h$e;->h:I

    :cond_8
    sub-int/2addr v2, v3

    if-eq v2, v6, :cond_a

    if-nez v2, :cond_9

    if-nez p2, :cond_9

    goto :goto_6

    :cond_9
    invoke-direct {p0, v0}, Lr4/h;->i(Lr4/h$e;)V

    :cond_a
    :goto_6
    invoke-direct {p0, p1}, Lr4/h;->j(Lr4/h$e;)V

    if-eqz p2, :cond_d

    goto :goto_7

    :cond_b
    if-nez v5, :cond_c

    add-int/lit8 v3, v3, 0x1

    iput v3, p1, Lr4/h$e;->h:I

    if-eqz p2, :cond_d

    goto :goto_7

    :cond_c
    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v0

    add-int/2addr v0, v6

    iput v0, p1, Lr4/h$e;->h:I

    if-nez p2, :cond_d

    goto :goto_7

    :cond_d
    iget-object p1, p1, Lr4/h$e;->a:Lr4/h$e;

    goto :goto_0

    :cond_e
    :goto_7
    return-void
.end method

.method private h(Lr4/h$e;Lr4/h$e;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lr4/h$e<",
            "TK;TV;>;",
            "Lr4/h$e<",
            "TK;TV;>;)V"
        }
    .end annotation

    iget-object v0, p1, Lr4/h$e;->a:Lr4/h$e;

    const/4 v1, 0x0

    iput-object v1, p1, Lr4/h$e;->a:Lr4/h$e;

    if-eqz p2, :cond_0

    iput-object v0, p2, Lr4/h$e;->a:Lr4/h$e;

    :cond_0
    if-eqz v0, :cond_2

    iget-object v1, v0, Lr4/h$e;->b:Lr4/h$e;

    if-ne v1, p1, :cond_1

    iput-object p2, v0, Lr4/h$e;->b:Lr4/h$e;

    goto :goto_0

    :cond_1
    iput-object p2, v0, Lr4/h$e;->c:Lr4/h$e;

    goto :goto_0

    :cond_2
    iput-object p2, p0, Lr4/h;->b:Lr4/h$e;

    :goto_0
    return-void
.end method

.method private i(Lr4/h$e;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lr4/h$e<",
            "TK;TV;>;)V"
        }
    .end annotation

    iget-object v0, p1, Lr4/h$e;->b:Lr4/h$e;

    iget-object v1, p1, Lr4/h$e;->c:Lr4/h$e;

    iget-object v2, v1, Lr4/h$e;->b:Lr4/h$e;

    iget-object v3, v1, Lr4/h$e;->c:Lr4/h$e;

    iput-object v2, p1, Lr4/h$e;->c:Lr4/h$e;

    if-eqz v2, :cond_0

    iput-object p1, v2, Lr4/h$e;->a:Lr4/h$e;

    :cond_0
    invoke-direct {p0, p1, v1}, Lr4/h;->h(Lr4/h$e;Lr4/h$e;)V

    iput-object p1, v1, Lr4/h$e;->b:Lr4/h$e;

    iput-object v1, p1, Lr4/h$e;->a:Lr4/h$e;

    const/4 v4, 0x0

    if-eqz v0, :cond_1

    iget v0, v0, Lr4/h$e;->h:I

    goto :goto_0

    :cond_1
    move v0, v4

    :goto_0
    if-eqz v2, :cond_2

    iget v2, v2, Lr4/h$e;->h:I

    goto :goto_1

    :cond_2
    move v2, v4

    :goto_1
    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iput v0, p1, Lr4/h$e;->h:I

    if-eqz v3, :cond_3

    iget v4, v3, Lr4/h$e;->h:I

    :cond_3
    invoke-static {v0, v4}, Ljava/lang/Math;->max(II)I

    move-result p1

    add-int/lit8 p1, p1, 0x1

    iput p1, v1, Lr4/h$e;->h:I

    return-void
.end method

.method private j(Lr4/h$e;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lr4/h$e<",
            "TK;TV;>;)V"
        }
    .end annotation

    iget-object v0, p1, Lr4/h$e;->b:Lr4/h$e;

    iget-object v1, p1, Lr4/h$e;->c:Lr4/h$e;

    iget-object v2, v0, Lr4/h$e;->b:Lr4/h$e;

    iget-object v3, v0, Lr4/h$e;->c:Lr4/h$e;

    iput-object v3, p1, Lr4/h$e;->b:Lr4/h$e;

    if-eqz v3, :cond_0

    iput-object p1, v3, Lr4/h$e;->a:Lr4/h$e;

    :cond_0
    invoke-direct {p0, p1, v0}, Lr4/h;->h(Lr4/h$e;Lr4/h$e;)V

    iput-object p1, v0, Lr4/h$e;->c:Lr4/h$e;

    iput-object v0, p1, Lr4/h$e;->a:Lr4/h$e;

    const/4 v4, 0x0

    if-eqz v1, :cond_1

    iget v1, v1, Lr4/h$e;->h:I

    goto :goto_0

    :cond_1
    move v1, v4

    :goto_0
    if-eqz v3, :cond_2

    iget v3, v3, Lr4/h$e;->h:I

    goto :goto_1

    :cond_2
    move v3, v4

    :goto_1
    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    iput v1, p1, Lr4/h$e;->h:I

    if-eqz v2, :cond_3

    iget v4, v2, Lr4/h$e;->h:I

    :cond_3
    invoke-static {v1, v4}, Ljava/lang/Math;->max(II)I

    move-result p1

    add-int/lit8 p1, p1, 0x1

    iput p1, v0, Lr4/h$e;->h:I

    return-void
.end method


# virtual methods
.method b(Ljava/lang/Object;Z)Lr4/h$e;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;Z)",
            "Lr4/h$e<",
            "TK;TV;>;"
        }
    .end annotation

    goto/32 :goto_2e

    nop

    :goto_0
    if-nez v0, :cond_0

    goto/32 :goto_23

    :cond_0
    goto/32 :goto_22

    nop

    :goto_1
    iget-object p2, p0, Lr4/h;->e:Lr4/h$e;

    goto/32 :goto_4c

    nop

    :goto_2
    iget-object v4, v1, Lr4/h$e;->f:Ljava/lang/Object;

    goto/32 :goto_39

    nop

    :goto_3
    goto/16 :goto_33

    :goto_4
    goto/32 :goto_32

    nop

    :goto_5
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_6

    nop

    :goto_6
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_38

    nop

    :goto_7
    move-object v3, v2

    :goto_8
    goto/32 :goto_2

    nop

    :goto_9
    invoke-direct {v0, v1, p1, p2, v3}, Lr4/h$e;-><init>(Lr4/h$e;Ljava/lang/Object;Lr4/h$e;Lr4/h$e;)V

    goto/32 :goto_e

    nop

    :goto_a
    throw p2

    :goto_b
    goto/32 :goto_4e

    nop

    :goto_c
    new-instance v0, Lr4/h$e;

    goto/32 :goto_14

    nop

    :goto_d
    iget-object v3, p2, Lr4/h$e;->e:Lr4/h$e;

    goto/32 :goto_3b

    nop

    :goto_e
    if-ltz v4, :cond_1

    goto/32 :goto_10

    :cond_1
    goto/32 :goto_43

    nop

    :goto_f
    goto/16 :goto_2d

    :goto_10
    goto/32 :goto_2c

    nop

    :goto_11
    if-eqz v5, :cond_2

    goto/32 :goto_17

    :cond_2
    goto/32 :goto_16

    nop

    :goto_12
    goto :goto_8

    :goto_13
    goto/32 :goto_40

    nop

    :goto_14
    iget-object v3, p2, Lr4/h$e;->e:Lr4/h$e;

    goto/32 :goto_9

    nop

    :goto_15
    move-object v3, p1

    goto/32 :goto_27

    nop

    :goto_16
    goto/16 :goto_41

    :goto_17
    goto/32 :goto_48

    nop

    :goto_18
    iget p1, p0, Lr4/h;->d:I

    goto/32 :goto_4a

    nop

    :goto_19
    goto :goto_8

    :goto_1a
    goto/32 :goto_7

    nop

    :goto_1b
    instance-of v0, p1, Ljava/lang/Comparable;

    goto/32 :goto_0

    nop

    :goto_1c
    new-instance p2, Ljava/lang/ClassCastException;

    goto/32 :goto_5

    nop

    :goto_1d
    return-object v2

    :goto_1e
    goto/32 :goto_1

    nop

    :goto_1f
    goto/16 :goto_3e

    :goto_20
    goto/32 :goto_3d

    nop

    :goto_21
    iput-object v0, p0, Lr4/h;->b:Lr4/h$e;

    goto/32 :goto_35

    nop

    :goto_22
    goto :goto_b

    :goto_23
    goto/32 :goto_1c

    nop

    :goto_24
    if-eq v0, v3, :cond_3

    goto/32 :goto_b

    :cond_3
    goto/32 :goto_1b

    nop

    :goto_25
    iget p1, p0, Lr4/h;->c:I

    goto/32 :goto_4d

    nop

    :goto_26
    sget-object v3, Lr4/h;->h:Ljava/util/Comparator;

    goto/32 :goto_24

    nop

    :goto_27
    check-cast v3, Ljava/lang/Comparable;

    goto/32 :goto_19

    nop

    :goto_28
    return-object v1

    :goto_29
    goto/32 :goto_49

    nop

    :goto_2a
    if-nez v1, :cond_4

    goto/32 :goto_13

    :cond_4
    goto/32 :goto_42

    nop

    :goto_2b
    if-eqz p2, :cond_5

    goto/32 :goto_1e

    :cond_5
    goto/32 :goto_1d

    nop

    :goto_2c
    iput-object v0, v1, Lr4/h$e;->c:Lr4/h$e;

    :goto_2d
    goto/32 :goto_2f

    nop

    :goto_2e
    iget-object v0, p0, Lr4/h;->a:Ljava/util/Comparator;

    goto/32 :goto_47

    nop

    :goto_2f
    invoke-direct {p0, v1, v2}, Lr4/h;->e(Lr4/h$e;Z)V

    :goto_30
    goto/32 :goto_25

    nop

    :goto_31
    const/4 v2, 0x0

    goto/32 :goto_2a

    nop

    :goto_32
    iget-object v5, v1, Lr4/h$e;->c:Lr4/h$e;

    :goto_33
    goto/32 :goto_11

    nop

    :goto_34
    if-eqz v4, :cond_6

    goto/32 :goto_29

    :cond_6
    goto/32 :goto_28

    nop

    :goto_35
    goto :goto_30

    :goto_36
    goto/32 :goto_c

    nop

    :goto_37
    if-eq v0, v3, :cond_7

    goto/32 :goto_1a

    :cond_7
    goto/32 :goto_15

    nop

    :goto_38
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    goto/32 :goto_4b

    nop

    :goto_39
    if-nez v3, :cond_8

    goto/32 :goto_20

    :cond_8
    goto/32 :goto_52

    nop

    :goto_3a
    iput p1, p0, Lr4/h;->d:I

    goto/32 :goto_4f

    nop

    :goto_3b
    invoke-direct {v0, v1, p1, p2, v3}, Lr4/h$e;-><init>(Lr4/h$e;Ljava/lang/Object;Lr4/h$e;Lr4/h$e;)V

    goto/32 :goto_21

    nop

    :goto_3c
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_51

    nop

    :goto_3d
    invoke-interface {v0, p1, v4}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v4

    :goto_3e
    goto/32 :goto_34

    nop

    :goto_3f
    if-eqz v1, :cond_9

    goto/32 :goto_36

    :cond_9
    goto/32 :goto_26

    nop

    :goto_40
    const/4 v4, 0x0

    :goto_41
    goto/32 :goto_2b

    nop

    :goto_42
    sget-object v3, Lr4/h;->h:Ljava/util/Comparator;

    goto/32 :goto_37

    nop

    :goto_43
    iput-object v0, v1, Lr4/h$e;->b:Lr4/h$e;

    goto/32 :goto_f

    nop

    :goto_44
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_53

    nop

    :goto_45
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_44

    nop

    :goto_46
    iput p1, p0, Lr4/h;->c:I

    goto/32 :goto_18

    nop

    :goto_47
    iget-object v1, p0, Lr4/h;->b:Lr4/h$e;

    goto/32 :goto_31

    nop

    :goto_48
    move-object v1, v5

    goto/32 :goto_12

    nop

    :goto_49
    if-ltz v4, :cond_a

    goto/32 :goto_4

    :cond_a
    goto/32 :goto_50

    nop

    :goto_4a
    add-int/2addr p1, v2

    goto/32 :goto_3a

    nop

    :goto_4b
    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_3c

    nop

    :goto_4c
    const/4 v2, 0x1

    goto/32 :goto_3f

    nop

    :goto_4d
    add-int/2addr p1, v2

    goto/32 :goto_46

    nop

    :goto_4e
    new-instance v0, Lr4/h$e;

    goto/32 :goto_d

    nop

    :goto_4f
    return-object v0

    :goto_50
    iget-object v5, v1, Lr4/h$e;->b:Lr4/h$e;

    goto/32 :goto_3

    nop

    :goto_51
    const-string p1, " is not Comparable"

    goto/32 :goto_45

    nop

    :goto_52
    invoke-interface {v3, v4}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v4

    goto/32 :goto_1f

    nop

    :goto_53
    invoke-direct {p2, p1}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_a

    nop
.end method

.method c(Ljava/util/Map$Entry;)Lr4/h$e;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map$Entry<",
            "**>;)",
            "Lr4/h$e<",
            "TK;TV;>;"
        }
    .end annotation

    goto/32 :goto_11

    nop

    :goto_0
    invoke-virtual {p0, v0}, Lr4/h;->d(Ljava/lang/Object;)Lr4/h$e;

    move-result-object v0

    goto/32 :goto_10

    nop

    :goto_1
    invoke-direct {p0, v1, p1}, Lr4/h;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    goto/32 :goto_4

    nop

    :goto_2
    goto :goto_b

    :goto_3
    goto/32 :goto_a

    nop

    :goto_4
    if-nez p1, :cond_0

    goto/32 :goto_d

    :cond_0
    goto/32 :goto_6

    nop

    :goto_5
    return-object v0

    :goto_6
    const/4 p1, 0x1

    goto/32 :goto_c

    nop

    :goto_7
    const/4 p1, 0x0

    :goto_8
    goto/32 :goto_f

    nop

    :goto_9
    iget-object v1, v0, Lr4/h$e;->g:Ljava/lang/Object;

    goto/32 :goto_e

    nop

    :goto_a
    const/4 v0, 0x0

    :goto_b
    goto/32 :goto_5

    nop

    :goto_c
    goto :goto_8

    :goto_d
    goto/32 :goto_7

    nop

    :goto_e
    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object p1

    goto/32 :goto_1

    nop

    :goto_f
    if-nez p1, :cond_1

    goto/32 :goto_3

    :cond_1
    goto/32 :goto_2

    nop

    :goto_10
    if-nez v0, :cond_2

    goto/32 :goto_d

    :cond_2
    goto/32 :goto_9

    nop

    :goto_11
    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_0

    nop
.end method

.method public clear()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lr4/h;->b:Lr4/h$e;

    const/4 v0, 0x0

    iput v0, p0, Lr4/h;->c:I

    iget v0, p0, Lr4/h;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lr4/h;->d:I

    iget-object v0, p0, Lr4/h;->e:Lr4/h$e;

    iput-object v0, v0, Lr4/h$e;->e:Lr4/h$e;

    iput-object v0, v0, Lr4/h$e;->d:Lr4/h$e;

    return-void
.end method

.method public containsKey(Ljava/lang/Object;)Z
    .locals 0

    invoke-virtual {p0, p1}, Lr4/h;->d(Ljava/lang/Object;)Lr4/h$e;

    move-result-object p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method d(Ljava/lang/Object;)Lr4/h$e;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Lr4/h$e<",
            "TK;TV;>;"
        }
    .end annotation

    goto/32 :goto_3

    nop

    :goto_0
    if-nez p1, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_1

    nop

    :goto_1
    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p0, p1, v1}, Lr4/h;->b(Ljava/lang/Object;Z)Lr4/h$e;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    nop

    :catch_0
    :goto_2
    goto/32 :goto_4

    nop

    :goto_3
    const/4 v0, 0x0

    goto/32 :goto_0

    nop

    :goto_4
    return-object v0
.end method

.method public entrySet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/util/Map$Entry<",
            "TK;TV;>;>;"
        }
    .end annotation

    iget-object v0, p0, Lr4/h;->f:Lr4/h$b;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Lr4/h$b;

    invoke-direct {v0, p0}, Lr4/h$b;-><init>(Lr4/h;)V

    iput-object v0, p0, Lr4/h;->f:Lr4/h$b;

    :goto_0
    return-object v0
.end method

.method f(Lr4/h$e;Z)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lr4/h$e<",
            "TK;TV;>;Z)V"
        }
    .end annotation

    goto/32 :goto_23

    nop

    :goto_0
    iget-object v0, p1, Lr4/h$e;->c:Lr4/h$e;

    goto/32 :goto_2a

    nop

    :goto_1
    iput-object v3, p1, Lr4/h$e;->c:Lr4/h$e;

    goto/32 :goto_15

    nop

    :goto_2
    goto/16 :goto_34

    :goto_3
    goto/32 :goto_33

    nop

    :goto_4
    const/4 v3, 0x0

    goto/32 :goto_1c

    nop

    :goto_5
    return-void

    :goto_6
    goto/32 :goto_9

    nop

    :goto_7
    iget v4, v0, Lr4/h$e;->h:I

    goto/32 :goto_2e

    nop

    :goto_8
    invoke-virtual {p0, p2, v2}, Lr4/h;->f(Lr4/h$e;Z)V

    goto/32 :goto_26

    nop

    :goto_9
    if-nez p2, :cond_0

    goto/32 :goto_29

    :cond_0
    goto/32 :goto_21

    nop

    :goto_a
    iget-object v0, p1, Lr4/h$e;->d:Lr4/h$e;

    goto/32 :goto_14

    nop

    :goto_b
    iget p1, p0, Lr4/h;->d:I

    goto/32 :goto_2f

    nop

    :goto_c
    const/4 v2, 0x0

    goto/32 :goto_4

    nop

    :goto_d
    invoke-direct {p0, p1, v3}, Lr4/h;->h(Lr4/h$e;Lr4/h$e;)V

    :goto_e
    goto/32 :goto_3a

    nop

    :goto_f
    if-nez v0, :cond_1

    goto/32 :goto_16

    :cond_1
    goto/32 :goto_13

    nop

    :goto_10
    iput-object v3, p1, Lr4/h$e;->b:Lr4/h$e;

    goto/32 :goto_28

    nop

    :goto_11
    if-nez v0, :cond_2

    goto/32 :goto_6

    :cond_2
    goto/32 :goto_37

    nop

    :goto_12
    invoke-direct {p0, p1, p2}, Lr4/h;->h(Lr4/h$e;Lr4/h$e;)V

    goto/32 :goto_5

    nop

    :goto_13
    invoke-direct {p0, p1, v0}, Lr4/h;->h(Lr4/h$e;Lr4/h$e;)V

    goto/32 :goto_1

    nop

    :goto_14
    iput-object v0, p2, Lr4/h$e;->d:Lr4/h$e;

    goto/32 :goto_3b

    nop

    :goto_15
    goto :goto_e

    :goto_16
    goto/32 :goto_d

    nop

    :goto_17
    iget-object p2, p1, Lr4/h$e;->b:Lr4/h$e;

    goto/32 :goto_25

    nop

    :goto_18
    iput-object p2, v0, Lr4/h$e;->e:Lr4/h$e;

    :goto_19
    goto/32 :goto_17

    nop

    :goto_1a
    add-int/lit8 p1, p1, -0x1

    goto/32 :goto_41

    nop

    :goto_1b
    iput-object v3, p1, Lr4/h$e;->b:Lr4/h$e;

    goto/32 :goto_2

    nop

    :goto_1c
    if-nez p2, :cond_3

    goto/32 :goto_6

    :cond_3
    goto/32 :goto_11

    nop

    :goto_1d
    iput v0, p2, Lr4/h$e;->h:I

    goto/32 :goto_12

    nop

    :goto_1e
    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto/32 :goto_38

    nop

    :goto_1f
    iput p1, p0, Lr4/h;->d:I

    goto/32 :goto_3c

    nop

    :goto_20
    iget v1, v0, Lr4/h$e;->h:I

    goto/32 :goto_24

    nop

    :goto_21
    invoke-direct {p0, p1, p2}, Lr4/h;->h(Lr4/h$e;Lr4/h$e;)V

    goto/32 :goto_10

    nop

    :goto_22
    invoke-virtual {p2}, Lr4/h$e;->b()Lr4/h$e;

    move-result-object p2

    goto/32 :goto_2b

    nop

    :goto_23
    if-nez p2, :cond_4

    goto/32 :goto_19

    :cond_4
    goto/32 :goto_2d

    nop

    :goto_24
    iput-object v0, p2, Lr4/h$e;->b:Lr4/h$e;

    goto/32 :goto_30

    nop

    :goto_25
    iget-object v0, p1, Lr4/h$e;->c:Lr4/h$e;

    goto/32 :goto_40

    nop

    :goto_26
    iget-object v0, p1, Lr4/h$e;->b:Lr4/h$e;

    goto/32 :goto_39

    nop

    :goto_27
    iput-object v0, p2, Lr4/h$e;->c:Lr4/h$e;

    goto/32 :goto_3d

    nop

    :goto_28
    goto/16 :goto_e

    :goto_29
    goto/32 :goto_f

    nop

    :goto_2a
    if-nez v0, :cond_5

    goto/32 :goto_3f

    :cond_5
    goto/32 :goto_31

    nop

    :goto_2b
    goto :goto_36

    :goto_2c
    goto/32 :goto_35

    nop

    :goto_2d
    iget-object p2, p1, Lr4/h$e;->e:Lr4/h$e;

    goto/32 :goto_a

    nop

    :goto_2e
    if-gt v1, v4, :cond_6

    goto/32 :goto_2c

    :cond_6
    goto/32 :goto_22

    nop

    :goto_2f
    add-int/lit8 p1, p1, 0x1

    goto/32 :goto_1f

    nop

    :goto_30
    iput-object p2, v0, Lr4/h$e;->a:Lr4/h$e;

    goto/32 :goto_1b

    nop

    :goto_31
    iget v2, v0, Lr4/h$e;->h:I

    goto/32 :goto_27

    nop

    :goto_32
    iget p1, p0, Lr4/h;->c:I

    goto/32 :goto_1a

    nop

    :goto_33
    move v1, v2

    :goto_34
    goto/32 :goto_0

    nop

    :goto_35
    invoke-virtual {v0}, Lr4/h$e;->a()Lr4/h$e;

    move-result-object p2

    :goto_36
    goto/32 :goto_8

    nop

    :goto_37
    iget v1, p2, Lr4/h$e;->h:I

    goto/32 :goto_7

    nop

    :goto_38
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_1d

    nop

    :goto_39
    if-nez v0, :cond_7

    goto/32 :goto_3

    :cond_7
    goto/32 :goto_20

    nop

    :goto_3a
    invoke-direct {p0, v1, v2}, Lr4/h;->e(Lr4/h$e;Z)V

    goto/32 :goto_32

    nop

    :goto_3b
    iget-object v0, p1, Lr4/h$e;->d:Lr4/h$e;

    goto/32 :goto_18

    nop

    :goto_3c
    return-void

    :goto_3d
    iput-object p2, v0, Lr4/h$e;->a:Lr4/h$e;

    goto/32 :goto_3e

    nop

    :goto_3e
    iput-object v3, p1, Lr4/h$e;->c:Lr4/h$e;

    :goto_3f
    goto/32 :goto_1e

    nop

    :goto_40
    iget-object v1, p1, Lr4/h$e;->a:Lr4/h$e;

    goto/32 :goto_c

    nop

    :goto_41
    iput p1, p0, Lr4/h;->c:I

    goto/32 :goto_b

    nop
.end method

.method g(Ljava/lang/Object;)Lr4/h$e;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Lr4/h$e<",
            "TK;TV;>;"
        }
    .end annotation

    goto/32 :goto_0

    nop

    :goto_0
    invoke-virtual {p0, p1}, Lr4/h;->d(Ljava/lang/Object;)Lr4/h$e;

    move-result-object p1

    goto/32 :goto_5

    nop

    :goto_1
    invoke-virtual {p0, p1, v0}, Lr4/h;->f(Lr4/h$e;Z)V

    :goto_2
    goto/32 :goto_3

    nop

    :goto_3
    return-object p1

    :goto_4
    const/4 v0, 0x1

    goto/32 :goto_1

    nop

    :goto_5
    if-nez p1, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_4

    nop
.end method

.method public get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    invoke-virtual {p0, p1}, Lr4/h;->d(Ljava/lang/Object;)Lr4/h$e;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object p1, p1, Lr4/h$e;->g:Ljava/lang/Object;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method public keySet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "TK;>;"
        }
    .end annotation

    iget-object v0, p0, Lr4/h;->g:Lr4/h$c;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Lr4/h$c;

    invoke-direct {v0, p0}, Lr4/h$c;-><init>(Lr4/h;)V

    iput-object v0, p0, Lr4/h;->g:Lr4/h$c;

    :goto_0
    return-object v0
.end method

.method public put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)TV;"
        }
    .end annotation

    const-string v0, "key == null"

    invoke-static {p1, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lr4/h;->b(Ljava/lang/Object;Z)Lr4/h$e;

    move-result-object p1

    iget-object v0, p1, Lr4/h$e;->g:Ljava/lang/Object;

    iput-object p2, p1, Lr4/h$e;->g:Ljava/lang/Object;

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    invoke-virtual {p0, p1}, Lr4/h;->g(Ljava/lang/Object;)Lr4/h$e;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object p1, p1, Lr4/h$e;->g:Ljava/lang/Object;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method public size()I
    .locals 1

    iget v0, p0, Lr4/h;->c:I

    return v0
.end method
