.class abstract Lr4/h$d;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lr4/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x402
    name = "d"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/Iterator<",
        "TT;>;"
    }
.end annotation


# instance fields
.field a:Lr4/h$e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lr4/h$e<",
            "TK;TV;>;"
        }
    .end annotation
.end field

.field b:Lr4/h$e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lr4/h$e<",
            "TK;TV;>;"
        }
    .end annotation
.end field

.field c:I

.field final synthetic d:Lr4/h;


# direct methods
.method constructor <init>(Lr4/h;)V
    .locals 1

    iput-object p1, p0, Lr4/h$d;->d:Lr4/h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget-object v0, p1, Lr4/h;->e:Lr4/h$e;

    iget-object v0, v0, Lr4/h$e;->d:Lr4/h$e;

    iput-object v0, p0, Lr4/h$d;->a:Lr4/h$e;

    const/4 v0, 0x0

    iput-object v0, p0, Lr4/h$d;->b:Lr4/h$e;

    iget p1, p1, Lr4/h;->d:I

    iput p1, p0, Lr4/h$d;->c:I

    return-void
.end method


# virtual methods
.method final a()Lr4/h$e;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lr4/h$e<",
            "TK;TV;>;"
        }
    .end annotation

    goto/32 :goto_4

    nop

    :goto_0
    iget-object v1, v0, Lr4/h$e;->d:Lr4/h$e;

    goto/32 :goto_e

    nop

    :goto_1
    return-object v0

    :goto_2
    goto/32 :goto_8

    nop

    :goto_3
    if-eq v1, v2, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_0

    nop

    :goto_4
    iget-object v0, p0, Lr4/h$d;->a:Lr4/h$e;

    goto/32 :goto_9

    nop

    :goto_5
    iput-object v0, p0, Lr4/h$d;->b:Lr4/h$e;

    goto/32 :goto_1

    nop

    :goto_6
    iget-object v2, v1, Lr4/h;->e:Lr4/h$e;

    goto/32 :goto_b

    nop

    :goto_7
    invoke-direct {v0}, Ljava/util/ConcurrentModificationException;-><init>()V

    goto/32 :goto_c

    nop

    :goto_8
    new-instance v0, Ljava/util/ConcurrentModificationException;

    goto/32 :goto_7

    nop

    :goto_9
    iget-object v1, p0, Lr4/h$d;->d:Lr4/h;

    goto/32 :goto_6

    nop

    :goto_a
    iget v1, v1, Lr4/h;->d:I

    goto/32 :goto_f

    nop

    :goto_b
    if-ne v0, v2, :cond_1

    goto/32 :goto_d

    :cond_1
    goto/32 :goto_a

    nop

    :goto_c
    throw v0

    :goto_d
    goto/32 :goto_10

    nop

    :goto_e
    iput-object v1, p0, Lr4/h$d;->a:Lr4/h$e;

    goto/32 :goto_5

    nop

    :goto_f
    iget v2, p0, Lr4/h$d;->c:I

    goto/32 :goto_3

    nop

    :goto_10
    new-instance v0, Ljava/util/NoSuchElementException;

    goto/32 :goto_11

    nop

    :goto_11
    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    goto/32 :goto_12

    nop

    :goto_12
    throw v0
.end method

.method public final hasNext()Z
    .locals 2

    iget-object v0, p0, Lr4/h$d;->a:Lr4/h$e;

    iget-object v1, p0, Lr4/h$d;->d:Lr4/h;

    iget-object v1, v1, Lr4/h;->e:Lr4/h$e;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final remove()V
    .locals 3

    iget-object v0, p0, Lr4/h$d;->b:Lr4/h$e;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lr4/h$d;->d:Lr4/h;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lr4/h;->f(Lr4/h$e;Z)V

    const/4 v0, 0x0

    iput-object v0, p0, Lr4/h$d;->b:Lr4/h$e;

    iget-object v0, p0, Lr4/h$d;->d:Lr4/h;

    iget v0, v0, Lr4/h;->d:I

    iput v0, p0, Lr4/h$d;->c:I

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method
