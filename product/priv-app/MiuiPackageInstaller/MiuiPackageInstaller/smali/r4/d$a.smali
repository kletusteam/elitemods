.class Lr4/d$a;
.super Lp4/v;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lr4/d;->a(Lp4/e;Lw4/a;)Lp4/v;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lp4/v<",
        "TT;>;"
    }
.end annotation


# instance fields
.field private a:Lp4/v;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lp4/v<",
            "TT;>;"
        }
    .end annotation
.end field

.field final synthetic b:Z

.field final synthetic c:Z

.field final synthetic d:Lp4/e;

.field final synthetic e:Lw4/a;

.field final synthetic f:Lr4/d;


# direct methods
.method constructor <init>(Lr4/d;ZZLp4/e;Lw4/a;)V
    .locals 0

    iput-object p1, p0, Lr4/d$a;->f:Lr4/d;

    iput-boolean p2, p0, Lr4/d$a;->b:Z

    iput-boolean p3, p0, Lr4/d$a;->c:Z

    iput-object p4, p0, Lr4/d$a;->d:Lp4/e;

    iput-object p5, p0, Lr4/d$a;->e:Lw4/a;

    invoke-direct {p0}, Lp4/v;-><init>()V

    return-void
.end method

.method private e()Lp4/v;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lp4/v<",
            "TT;>;"
        }
    .end annotation

    iget-object v0, p0, Lr4/d$a;->a:Lp4/v;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lr4/d$a;->d:Lp4/e;

    iget-object v1, p0, Lr4/d$a;->f:Lr4/d;

    iget-object v2, p0, Lr4/d$a;->e:Lw4/a;

    invoke-virtual {v0, v1, v2}, Lp4/e;->m(Lp4/w;Lw4/a;)Lp4/v;

    move-result-object v0

    iput-object v0, p0, Lr4/d$a;->a:Lp4/v;

    :goto_0
    return-object v0
.end method


# virtual methods
.method public b(Lx4/a;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lx4/a;",
            ")TT;"
        }
    .end annotation

    iget-boolean v0, p0, Lr4/d$a;->b:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lx4/a;->k0()V

    const/4 p1, 0x0

    return-object p1

    :cond_0
    invoke-direct {p0}, Lr4/d$a;->e()Lp4/v;

    move-result-object v0

    invoke-virtual {v0, p1}, Lp4/v;->b(Lx4/a;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public d(Lx4/c;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lx4/c;",
            "TT;)V"
        }
    .end annotation

    iget-boolean v0, p0, Lr4/d$a;->c:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lx4/c;->F()Lx4/c;

    return-void

    :cond_0
    invoke-direct {p0}, Lr4/d$a;->e()Lp4/v;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lp4/v;->d(Lx4/c;Ljava/lang/Object;)V

    return-void
.end method
