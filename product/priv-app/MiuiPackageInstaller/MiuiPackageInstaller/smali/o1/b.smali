.class public Lo1/b;
.super Ljava/lang/Object;


# instance fields
.field a:Ljava/lang/String;

.field b:[Ljava/lang/String;

.field c:[Ljava/lang/String;

.field d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field e:J

.field f:I

.field g:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lo1/b;->g:Z

    iput-object p1, p0, Lo1/b;->a:Ljava/lang/String;

    sget-object p1, Ly1/b;->a:[Ljava/lang/String;

    iput-object p1, p0, Lo1/b;->b:[Ljava/lang/String;

    iput-object p1, p0, Lo1/b;->c:[Ljava/lang/String;

    sget-object p1, Ly1/b;->b:Ljava/util/HashMap;

    iput-object p1, p0, Lo1/b;->d:Ljava/util/Map;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lo1/b;->e:J

    const/16 p1, 0x3c

    iput p1, p0, Lo1/b;->f:I

    iput-boolean v0, p0, Lo1/b;->g:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;Ljava/util/Map;ZZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;ZZ)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p5, 0x0

    iput-boolean p5, p0, Lo1/b;->g:Z

    iput-object p1, p0, Lo1/b;->a:Ljava/lang/String;

    iput-object p2, p0, Lo1/b;->b:[Ljava/lang/String;

    iput-object p3, p0, Lo1/b;->c:[Ljava/lang/String;

    iput-object p4, p0, Lo1/b;->d:Ljava/util/Map;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide p1

    iput-wide p1, p0, Lo1/b;->e:J

    const/16 p1, 0x3c

    iput p1, p0, Lo1/b;->f:I

    iput-boolean p6, p0, Lo1/b;->g:Z

    return-void
.end method


# virtual methods
.method public a()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lo1/b;->b:[Ljava/lang/String;

    return-object v0
.end method

.method public b()Z
    .locals 6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lo1/b;->e:J

    iget v4, p0, Lo1/b;->f:I

    mul-int/lit16 v4, v4, 0x3e8

    int-to-long v4, v4

    add-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public c()Z
    .locals 1

    iget-boolean v0, p0, Lo1/b;->g:Z

    return v0
.end method

.method public d(Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lq1/a;",
            ">;)V"
        }
    .end annotation

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 v2, 0x0

    const v3, 0x7fffffff

    const/4 v4, 0x0

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lq1/a;

    invoke-virtual {v5}, Lq1/a;->m()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lo1/b;->a:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v5}, Lq1/a;->p()I

    move-result v6

    sget-object v7, Lo1/g;->a:Lo1/g;

    invoke-virtual {v7}, Ljava/lang/Enum;->ordinal()I

    move-result v7

    if-ne v6, v7, :cond_1

    invoke-virtual {v5}, Lq1/a;->o()[Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lo1/b;->b:[Ljava/lang/String;

    goto :goto_1

    :cond_1
    invoke-virtual {v5}, Lq1/a;->p()I

    move-result v6

    sget-object v7, Lo1/g;->b:Lo1/g;

    invoke-virtual {v7}, Ljava/lang/Enum;->ordinal()I

    move-result v7

    if-ne v6, v7, :cond_2

    invoke-virtual {v5}, Lq1/a;->o()[Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lo1/b;->c:[Ljava/lang/String;

    :cond_2
    :goto_1
    invoke-virtual {v5}, Lq1/a;->l()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_3

    invoke-virtual {v5}, Lq1/a;->l()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_3

    invoke-virtual {v5}, Lq1/a;->l()Ljava/lang/String;

    move-result-object v2

    :cond_3
    invoke-virtual {v5}, Lq1/a;->b()J

    move-result-wide v6

    cmp-long v6, v0, v6

    if-lez v6, :cond_4

    invoke-virtual {v5}, Lq1/a;->b()J

    move-result-wide v0

    :cond_4
    invoke-virtual {v5}, Lq1/a;->a()I

    move-result v6

    if-le v3, v6, :cond_5

    invoke-virtual {v5}, Lq1/a;->a()I

    move-result v3

    :cond_5
    invoke-virtual {v5}, Lq1/a;->r()Z

    move-result v5

    or-int/2addr v4, v5

    goto :goto_0

    :cond_6
    invoke-static {v2}, Ly1/a;->c(Ljava/lang/String;)Ljava/util/Map;

    move-result-object p1

    iput-object p1, p0, Lo1/b;->d:Ljava/util/Map;

    iput-wide v0, p0, Lo1/b;->e:J

    iput v3, p0, Lo1/b;->f:I

    iput-boolean v4, p0, Lo1/b;->g:Z

    return-void
.end method

.method public e(Lq1/a;)V
    .locals 2

    invoke-virtual {p1}, Lq1/a;->m()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lo1/b;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lq1/a;->p()I

    move-result v0

    sget-object v1, Lo1/g;->a:Lo1/g;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lq1/a;->o()[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lo1/b;->b:[Ljava/lang/String;

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lq1/a;->p()I

    move-result v0

    sget-object v1, Lo1/g;->b:Lo1/g;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    if-ne v0, v1, :cond_1

    invoke-virtual {p1}, Lq1/a;->o()[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lo1/b;->c:[Ljava/lang/String;

    :cond_1
    :goto_0
    invoke-virtual {p1}, Lq1/a;->l()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ly1/a;->c(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lo1/b;->d:Ljava/util/Map;

    invoke-virtual {p1}, Lq1/a;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lo1/b;->e:J

    invoke-virtual {p1}, Lq1/a;->a()I

    move-result v0

    iput v0, p0, Lo1/b;->f:I

    invoke-virtual {p1}, Lq1/a;->r()Z

    move-result p1

    iput-boolean p1, p0, Lo1/b;->g:Z

    :cond_2
    return-void
.end method

.method public f([Ljava/lang/String;Lo1/g;)V
    .locals 1

    sget-object v0, Lo1/b$a;->a:[I

    invoke-virtual {p2}, Ljava/lang/Enum;->ordinal()I

    move-result p2

    aget p2, v0, p2

    const/4 v0, 0x1

    if-eq p2, v0, :cond_1

    const/4 v0, 0x2

    if-eq p2, v0, :cond_0

    goto :goto_0

    :cond_0
    iput-object p1, p0, Lo1/b;->c:[Ljava/lang/String;

    goto :goto_0

    :cond_1
    iput-object p1, p0, Lo1/b;->b:[Ljava/lang/String;

    :goto_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "host:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lo1/b;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", ips:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lo1/b;->b:[Ljava/lang/String;

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", ipv6s:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lo1/b;->c:[Ljava/lang/String;

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", extras:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lo1/b;->d:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", expired:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lo1/b;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", fromDB:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lo1/b;->g:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
