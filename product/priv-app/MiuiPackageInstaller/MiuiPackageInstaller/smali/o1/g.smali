.class public final enum Lo1/g;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lo1/g;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lo1/g;

.field public static final enum b:Lo1/g;

.field public static final enum c:Lo1/g;

.field private static final synthetic d:[Lo1/g;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    new-instance v0, Lo1/g;

    const-string v1, "v4"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lo1/g;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lo1/g;->a:Lo1/g;

    new-instance v1, Lo1/g;

    const-string v3, "v6"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lo1/g;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lo1/g;->b:Lo1/g;

    new-instance v3, Lo1/g;

    const-string v5, "both"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lo1/g;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lo1/g;->c:Lo1/g;

    const/4 v5, 0x3

    new-array v5, v5, [Lo1/g;

    aput-object v0, v5, v2

    aput-object v1, v5, v4

    aput-object v3, v5, v6

    sput-object v5, Lo1/g;->d:[Lo1/g;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lo1/g;
    .locals 1

    const-class v0, Lo1/g;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lo1/g;

    return-object p0
.end method

.method public static values()[Lo1/g;
    .locals 1

    sget-object v0, Lo1/g;->d:[Lo1/g;

    invoke-virtual {v0}, [Lo1/g;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lo1/g;

    return-object v0
.end method
