.class public final Lm2/a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lm2/a$b;
    }
.end annotation


# static fields
.field public static final b:Lm2/a$b;

.field private static final c:La8/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La8/f<",
            "Lm2/a;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Lm2/c;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lm2/a$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lm2/a$b;-><init>(Lm8/g;)V

    sput-object v0, Lm2/a;->b:Lm2/a$b;

    sget-object v0, La8/j;->a:La8/j;

    sget-object v1, Lm2/a$a;->b:Lm2/a$a;

    invoke-static {v0, v1}, La8/g;->a(La8/j;Ll8/a;)La8/f;

    move-result-object v0

    sput-object v0, Lm2/a;->c:La8/f;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lm2/c;

    invoke-static {}, Lcom/android/packageinstaller/InstallerApplication;->g()Lcom/android/packageinstaller/InstallerApplication;

    move-result-object v1

    const-string v2, "local_cache_data"

    invoke-direct {v0, v1, v2}, Lm2/c;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lm2/a;->a:Lm2/c;

    return-void
.end method

.method public static final synthetic a()La8/f;
    .locals 1

    sget-object v0, Lm2/a;->c:La8/f;

    return-object v0
.end method


# virtual methods
.method public final b()Lcom/miui/packageInstaller/model/GuideOpenSafeModePopTips;
    .locals 4

    :try_start_0
    invoke-static {}, Lcom/android/packageinstaller/utils/j;->b()Lp4/e;

    move-result-object v0

    iget-object v1, p0, Lm2/a;->a:Lm2/c;

    const-string v2, "safe_mode_tip_text_finish"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lm2/c;->d(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/miui/packageInstaller/model/GuideOpenSafeModePopTips;

    invoke-virtual {v0, v1, v2}, Lp4/e;->h(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/packageInstaller/model/GuideOpenSafeModePopTips;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    const-string v2, "CacheDataSp"

    invoke-static {v2, v1, v0}, Lf6/o;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()Lcom/miui/packageInstaller/model/GuideOpenSafeModePopTips;
    .locals 4

    :try_start_0
    invoke-static {}, Lcom/android/packageinstaller/utils/j;->b()Lp4/e;

    move-result-object v0

    iget-object v1, p0, Lm2/a;->a:Lm2/c;

    const-string v2, "safe_mode_tip_text"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lm2/c;->d(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/miui/packageInstaller/model/GuideOpenSafeModePopTips;

    invoke-virtual {v0, v1, v2}, Lp4/e;->h(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/packageInstaller/model/GuideOpenSafeModePopTips;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    const-string v2, "CacheDataSp"

    invoke-static {v2, v1, v0}, Lf6/o;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x0

    return-object v0
.end method

.method public final d()Lcom/miui/packageInstaller/model/ThirdPartyInvokeInstallerPopTips;
    .locals 4

    :try_start_0
    invoke-static {}, Lcom/android/packageinstaller/utils/j;->b()Lp4/e;

    move-result-object v0

    iget-object v1, p0, Lm2/a;->a:Lm2/c;

    const-string v2, "third_install_pop_tips"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lm2/c;->d(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/miui/packageInstaller/model/ThirdPartyInvokeInstallerPopTips;

    invoke-virtual {v0, v1, v2}, Lp4/e;->h(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/packageInstaller/model/ThirdPartyInvokeInstallerPopTips;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    const-string v2, "CacheDataSp"

    invoke-static {v2, v1, v0}, Lf6/o;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x0

    return-object v0
.end method

.method public final e(Lcom/miui/packageInstaller/model/GuideOpenSafeModePopTips;)V
    .locals 2

    if-nez p1, :cond_0

    return-void

    :cond_0
    :try_start_0
    invoke-static {}, Lcom/android/packageinstaller/utils/j;->b()Lp4/e;

    move-result-object v0

    invoke-virtual {v0, p1}, Lp4/e;->q(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lm2/a;->a:Lm2/c;

    const-string v1, "safe_mode_tip_text_finish"

    invoke-virtual {v0, v1, p1}, Lm2/c;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    const-string v1, "CacheDataSp"

    invoke-static {v1, v0, p1}, Lf6/o;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_0
    return-void
.end method

.method public final f(Lcom/miui/packageInstaller/model/GuideOpenSafeModePopTips;)V
    .locals 2

    if-nez p1, :cond_0

    return-void

    :cond_0
    :try_start_0
    invoke-static {}, Lcom/android/packageinstaller/utils/j;->b()Lp4/e;

    move-result-object v0

    invoke-virtual {v0, p1}, Lp4/e;->q(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lm2/a;->a:Lm2/c;

    const-string v1, "safe_mode_tip_text"

    invoke-virtual {v0, v1, p1}, Lm2/c;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    const-string v1, "CacheDataSp"

    invoke-static {v1, v0, p1}, Lf6/o;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_0
    return-void
.end method

.method public final g(Lcom/miui/packageInstaller/model/ThirdPartyInvokeInstallerPopTips;)V
    .locals 2

    if-nez p1, :cond_0

    return-void

    :cond_0
    :try_start_0
    invoke-static {}, Lcom/android/packageinstaller/utils/j;->b()Lp4/e;

    move-result-object v0

    invoke-virtual {v0, p1}, Lp4/e;->q(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lm2/a;->a:Lm2/c;

    const-string v1, "third_install_pop_tips"

    invoke-virtual {v0, v1, p1}, Lm2/c;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    const-string v1, "CacheDataSp"

    invoke-static {v1, v0, p1}, Lf6/o;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_0
    return-void
.end method
