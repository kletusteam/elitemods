.class public Lm2/b;
.super Ljava/lang/Object;


# static fields
.field private static c:Lm2/b;


# instance fields
.field private a:Lm2/c;

.field private b:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lm2/b;->b:Landroid/content/Context;

    new-instance v0, Lm2/c;

    const-string v1, "common"

    invoke-direct {v0, p1, v1}, Lm2/c;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lm2/b;->a:Lm2/c;

    return-void
.end method

.method public static declared-synchronized g(Landroid/content/Context;)Lm2/b;
    .locals 2

    const-class v0, Lm2/b;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lm2/b;->c:Lm2/b;

    if-nez v1, :cond_0

    new-instance v1, Lm2/b;

    invoke-direct {v1, p0}, Lm2/b;-><init>(Landroid/content/Context;)V

    sput-object v1, Lm2/b;->c:Lm2/b;

    :cond_0
    sget-object p0, Lm2/b;->c:Lm2/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object p0

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method


# virtual methods
.method public A(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lm2/b;->a:Lm2/c;

    const-string v1, "extTokenStr"

    invoke-virtual {v0, v1, p1}, Lm2/c;->m(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public B(J)V
    .locals 2

    iget-object v0, p0, Lm2/b;->a:Lm2/c;

    const-string v1, "initTime"

    invoke-virtual {v0, v1, p1, p2}, Lm2/c;->l(Ljava/lang/String;J)V

    return-void
.end method

.method public C(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lm2/b;->a:Lm2/c;

    const-string v1, "security_mode_install_once_date"

    invoke-virtual {v0, v1, p1}, Lm2/c;->m(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public D(Z)V
    .locals 0

    return-void
.end method

.method public E(Z)V
    .locals 2

    iget-object v0, p0, Lm2/b;->a:Lm2/c;

    const-string v1, "installerOpenSafetyModel"

    invoke-virtual {v0, v1, p1}, Lm2/c;->k(Ljava/lang/String;Z)V

    return-void
.end method

.method public F(Z)V
    .locals 1

    iget-object v0, p0, Lm2/b;->b:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->setInstallRiskEnabled(Landroid/content/Context;Z)V

    return-void
.end method

.method public G(Lx5/a;)V
    .locals 2

    iget-object v0, p0, Lm2/b;->a:Lm2/c;

    invoke-virtual {p1}, Lx5/a;->c()I

    move-result p1

    const-string v1, "risk_install_type"

    invoke-virtual {v0, v1, p1}, Lm2/c;->g(Ljava/lang/String;I)V

    return-void
.end method

.method public H(Z)V
    .locals 2

    iget-object v0, p0, Lm2/b;->a:Lm2/c;

    const-string v1, "secure_verify_cloud_once"

    invoke-virtual {v0, v1, p1}, Lm2/c;->k(Ljava/lang/String;Z)V

    return-void
.end method

.method public I(Z)V
    .locals 2

    iget-object v0, p0, Lm2/b;->a:Lm2/c;

    const-string v1, "secure_verify_enable"

    invoke-virtual {v0, v1, p1}, Lm2/c;->k(Ljava/lang/String;Z)V

    return-void
.end method

.method public J(Lx5/a;)V
    .locals 2

    iget-object v0, p0, Lm2/b;->a:Lm2/c;

    invoke-virtual {p1}, Lx5/a;->c()I

    move-result p1

    const-string v1, "secure_verify_type"

    invoke-virtual {v0, v1, p1}, Lm2/c;->g(Ljava/lang/String;I)V

    return-void
.end method

.method public K(Z)V
    .locals 2

    iget-object v0, p0, Lm2/b;->a:Lm2/c;

    const-string v1, "security_mode_app_error_install"

    invoke-virtual {v0, v1, p1}, Lm2/c;->k(Ljava/lang/String;Z)V

    return-void
.end method

.method public L(Z)V
    .locals 2

    iget-object v0, p0, Lm2/b;->a:Lm2/c;

    const-string v1, "setting_switch_clear_package"

    invoke-virtual {v0, v1, p1}, Lm2/c;->k(Ljava/lang/String;Z)V

    return-void
.end method

.method public M(Z)V
    .locals 2

    iget-object v0, p0, Lm2/b;->a:Lm2/c;

    const-string v1, "setting_switch_unknown_source"

    invoke-virtual {v0, v1, p1}, Lm2/c;->k(Ljava/lang/String;Z)V

    return-void
.end method

.method public N(Z)V
    .locals 2

    iget-object v0, p0, Lm2/b;->a:Lm2/c;

    const-string v1, "installerSingleAuth"

    invoke-virtual {v0, v1, p1}, Lm2/c;->k(Ljava/lang/String;Z)V

    return-void
.end method

.method public O(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lm2/b;->a:Lm2/c;

    const-string v1, "source_authority_info"

    invoke-virtual {v0, v1, p1}, Lm2/c;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public P(Ljava/util/Set;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lm2/b;->a:Lm2/c;

    const-string v1, "white_app_install_control"

    invoke-virtual {v0, v1, p1}, Lm2/c;->j(Ljava/lang/String;Ljava/util/Set;)V

    return-void
.end method

.method public Q(Ljava/util/Set;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lm2/b;->a:Lm2/c;

    const-string v1, "white_no_system_app_install_other"

    invoke-virtual {v0, v1, p1}, Lm2/c;->j(Ljava/lang/String;Ljava/util/Set;)V

    return-void
.end method

.method public R()Z
    .locals 3

    iget-object v0, p0, Lm2/b;->a:Lm2/c;

    const-string v1, "installerSingleAuth"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lm2/c;->f(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public a()Z
    .locals 3

    iget-object v0, p0, Lm2/b;->a:Lm2/c;

    const-string v1, "installerCloseSafetyModel"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lm2/c;->f(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public b()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lm2/b;->a:Lm2/c;

    const-string v1, "cUserId"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lm2/c;->d(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()J
    .locals 4

    iget-object v0, p0, Lm2/b;->a:Lm2/c;

    const-string v1, "cloud_data_fetch_time"

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lm2/c;->c(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public d()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lm2/b;->a:Lm2/c;

    const-string v1, "extTokenStr"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lm2/c;->d(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public e()J
    .locals 4

    iget-object v0, p0, Lm2/b;->a:Lm2/c;

    const-string v1, "initTime"

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lm2/c;->c(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public f()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lm2/b;->a:Lm2/c;

    const-string v1, "security_mode_install_once_date"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lm2/c;->d(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public h()Z
    .locals 1

    iget-object v0, p0, Lm2/b;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->isInstallRiskEnabled(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public i()Lx5/a;
    .locals 3

    iget-object v0, p0, Lm2/b;->a:Lm2/c;

    const-string v1, "risk_install_type"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lm2/c;->b(Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, Lx5/a;->b(I)Lx5/a;

    move-result-object v0

    return-object v0
.end method

.method public j()Z
    .locals 3

    iget-object v0, p0, Lm2/b;->a:Lm2/c;

    const-string v1, "secure_verify_cloud_once"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lm2/c;->f(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public k()Lx5/a;
    .locals 3

    iget-object v0, p0, Lm2/b;->a:Lm2/c;

    const-string v1, "secure_verify_type"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lm2/c;->b(Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, Lx5/a;->b(I)Lx5/a;

    move-result-object v0

    return-object v0
.end method

.method public l()Z
    .locals 3

    iget-object v0, p0, Lm2/b;->a:Lm2/c;

    const-string v1, "security_mode_app_error_install"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lm2/c;->f(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public m()Z
    .locals 3

    iget-object v0, p0, Lm2/b;->a:Lm2/c;

    const-string v1, "setting_switch_clear_package"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lm2/c;->f(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public n()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lm2/b;->a:Lm2/c;

    const-string v1, "source_authority_info"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lm2/c;->d(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public o()Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lm2/b;->a:Lm2/c;

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    const-string v2, "white_app_install_control"

    invoke-virtual {v0, v2, v1}, Lm2/c;->e(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public p()Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lm2/b;->a:Lm2/c;

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    const-string v2, "white_no_system_app_install_other"

    invoke-virtual {v0, v2, v1}, Lm2/c;->e(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public q()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public r()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public s()Z
    .locals 3

    iget-object v0, p0, Lm2/b;->a:Lm2/c;

    const-string v1, "secure_verify_enable"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lm2/c;->f(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public t()Z
    .locals 3

    iget-object v0, p0, Lm2/b;->a:Lm2/c;

    const-string v1, "installerOpenSafetyModel"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lm2/c;->f(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public u(Z)V
    .locals 0

    return-void
.end method

.method public v(Z)V
    .locals 0

    return-void
.end method

.method public w(Ljava/util/Set;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lm2/b;->a:Lm2/c;

    const-string v1, "black_app_install_control"

    invoke-virtual {v0, v1, p1}, Lm2/c;->j(Ljava/lang/String;Ljava/util/Set;)V

    return-void
.end method

.method public x(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lm2/b;->a:Lm2/c;

    const-string v1, "cUserId"

    invoke-virtual {v0, v1, p1}, Lm2/c;->m(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public y(Z)V
    .locals 2

    iget-object v0, p0, Lm2/b;->a:Lm2/c;

    const-string v1, "installerCloseSafetyModel"

    invoke-virtual {v0, v1, p1}, Lm2/c;->k(Ljava/lang/String;Z)V

    return-void
.end method

.method public z(J)V
    .locals 2

    iget-object v0, p0, Lm2/b;->a:Lm2/c;

    const-string v1, "cloud_data_fetch_time"

    invoke-virtual {v0, v1, p1, p2}, Lm2/c;->h(Ljava/lang/String;J)V

    return-void
.end method
