.class Lh7/c$b;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lh7/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lh7/c;


# direct methods
.method constructor <init>(Lh7/c;)V
    .locals 0

    iput-object p1, p0, Lh7/c$b;->a:Lh7/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    const/4 v0, 0x1

    :try_start_0
    invoke-static {}, Lh7/c;->u()Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v2, p0, Lh7/c$b;->a:Lh7/c;

    invoke-static {v2}, Lh7/c;->v(Lh7/c;)V

    const/4 v2, 0x0

    iget-object v3, p0, Lh7/c$b;->a:Lh7/c;

    invoke-static {v3}, Lh7/c;->w(Lh7/c;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {}, Lh7/c;->x()Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    iget-object v2, p0, Lh7/c$b;->a:Lh7/c;

    invoke-static {v2}, Lh7/c;->y(Lh7/c;)Lj7/a;

    iget-object v2, p0, Lh7/c$b;->a:Lh7/c;

    invoke-static {v2}, Lh7/c;->z(Lh7/c;)Lj7/c;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-interface {v2}, Lj7/a;->a()V

    :cond_1
    if-eqz v2, :cond_2

    const-string v3, "SdkManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "sys version = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v2}, Lj7/a;->getVersion()Lh7/e;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Li7/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    invoke-static {}, Lh7/c;->x()Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v3, "SdkManager"

    const-string v4, "use system analytics only, so don\'t load asset/local analytics.apk"

    invoke-static {v3, v4}, Li7/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lh7/c$b;->a:Lh7/c;

    invoke-static {v3, v2}, Lh7/c;->b(Lh7/c;Lj7/a;)Lj7/a;

    iget-object v2, p0, Lh7/c$b;->a:Lh7/c;

    invoke-static {v2}, Lh7/c;->a(Lh7/c;)Lj7/a;

    move-result-object v3

    invoke-static {v2, v3}, Lh7/c;->A(Lh7/c;Lj7/a;)V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v1, p0, Lh7/c$b;->a:Lh7/c;

    invoke-static {v1, v0}, Lh7/c;->k(Lh7/c;Z)Z

    return-void

    :cond_3
    :try_start_2
    iget-object v3, p0, Lh7/c$b;->a:Lh7/c;

    invoke-static {v3}, Lh7/c;->d(Lh7/c;)Lj7/a;

    move-result-object v3

    const-string v4, "SdkManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "assets analytics null "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v6, 0x0

    if-nez v3, :cond_4

    move v7, v0

    goto :goto_0

    :cond_4
    move v7, v6

    :goto_0
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Li7/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, p0, Lh7/c$b;->a:Lh7/c;

    invoke-static {v4}, Lh7/c;->e(Lh7/c;)Lj7/a;

    move-result-object v4

    const-string v5, "SdkManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "local analytics null "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-nez v4, :cond_5

    move v6, v0

    :cond_5
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Li7/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v3, :cond_7

    if-eqz v4, :cond_6

    invoke-interface {v4}, Lj7/a;->getVersion()Lh7/e;

    move-result-object v5

    invoke-interface {v3}, Lj7/a;->getVersion()Lh7/e;

    move-result-object v6

    invoke-virtual {v5, v6}, Lh7/e;->a(Lh7/e;)I

    move-result v5

    if-lez v5, :cond_6

    goto :goto_1

    :cond_6
    const-string v4, "SdkManager"

    const-string v5, "use assets analytics."

    invoke-static {v4, v5}, Li7/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_7
    :goto_1
    const-string v3, "SdkManager"

    const-string v5, "use local analytics."

    invoke-static {v3, v5}, Li7/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    move-object v3, v4

    :goto_2
    if-eqz v2, :cond_9

    if-eqz v3, :cond_8

    invoke-interface {v3}, Lj7/a;->getVersion()Lh7/e;

    move-result-object v4

    invoke-interface {v2}, Lj7/a;->getVersion()Lh7/e;

    move-result-object v5

    invoke-virtual {v4, v5}, Lh7/e;->a(Lh7/e;)I

    move-result v4

    if-lez v4, :cond_8

    goto :goto_3

    :cond_8
    const-string v4, "SdkManager"

    const-string v5, "use sys analytics."

    invoke-static {v4, v5}, Li7/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, p0, Lh7/c$b;->a:Lh7/c;

    invoke-static {v4, v3}, Lh7/c;->h(Lh7/c;Lj7/a;)Lj7/a;

    iget-object v3, p0, Lh7/c$b;->a:Lh7/c;

    invoke-static {v3}, Lh7/c;->i(Lh7/c;)V

    goto :goto_4

    :cond_9
    :goto_3
    const-string v2, "SdkManager"

    const-string v4, "use dex analytics."

    invoke-static {v2, v4}, Li7/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v3, :cond_a

    invoke-interface {v3}, Lj7/a;->a()V

    :cond_a
    iget-object v2, p0, Lh7/c$b;->a:Lh7/c;

    invoke-static {v2, v0}, Lh7/c;->f(Lh7/c;Z)V

    move-object v2, v3

    :goto_4
    if-eqz v2, :cond_b

    invoke-interface {v2}, Lj7/a;->getVersion()Lh7/e;

    move-result-object v3

    sget-object v4, Lh7/a;->b:Lh7/e;

    invoke-virtual {v3, v4}, Lh7/e;->a(Lh7/e;)I

    move-result v3

    if-ltz v3, :cond_b

    iget-object v3, p0, Lh7/c$b;->a:Lh7/c;

    invoke-static {v3, v2}, Lh7/c;->b(Lh7/c;Lj7/a;)Lj7/a;

    :cond_b
    iget-object v2, p0, Lh7/c$b;->a:Lh7/c;

    invoke-static {v2}, Lh7/c;->j(Lh7/c;)V

    iget-object v2, p0, Lh7/c$b;->a:Lh7/c;

    invoke-static {v2}, Lh7/c;->a(Lh7/c;)Lj7/a;

    move-result-object v3

    invoke-static {v2, v3}, Lh7/c;->A(Lh7/c;Lj7/a;)V

    monitor-exit v1

    goto :goto_5

    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v2
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception v1

    goto :goto_6

    :catch_0
    move-exception v1

    :try_start_4
    const-string v2, "SdkManager"

    invoke-static {v2}, Li7/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "heavy work exception"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :goto_5
    iget-object v1, p0, Lh7/c$b;->a:Lh7/c;

    invoke-static {v1, v0}, Lh7/c;->k(Lh7/c;Z)Z

    return-void

    :goto_6
    iget-object v2, p0, Lh7/c$b;->a:Lh7/c;

    invoke-static {v2, v0}, Lh7/c;->k(Lh7/c;Z)Z

    throw v1
.end method
