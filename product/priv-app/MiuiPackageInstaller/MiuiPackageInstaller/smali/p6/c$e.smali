.class Lp6/c$e;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lp6/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "e"
.end annotation


# instance fields
.field final synthetic a:Lp6/c;


# direct methods
.method constructor <init>(Lp6/c;)V
    .locals 0

    iput-object p1, p0, Lp6/c$e;->a:Lp6/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic a(Lp6/c$e;Ljava/lang/String;IZ)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lp6/c$e;->b(Ljava/lang/String;IZ)V

    return-void
.end method

.method private b(Ljava/lang/String;IZ)V
    .locals 5

    const-string v0, "app_security_risk_app.db"

    const/4 v1, 0x1

    const/4 v2, 0x0

    :try_start_0
    new-instance v3, Lz4/b;

    sget-object v4, Lo6/a;->a:Landroid/content/Context;

    invoke-direct {v3, v4, p1, p2}, Lz4/b;-><init>(Landroid/content/Context;Ljava/lang/String;I)V

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_0

    iget-object p2, p0, Lp6/c$e;->a:Lp6/c;

    invoke-static {p2}, Lp6/c;->f(Lp6/c;)Lb5/g$a;

    move-result-object p2

    goto :goto_0

    :cond_0
    move-object p2, v2

    :goto_0
    iput-object p2, v3, Lz4/b;->f:Lb5/g$a;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lp6/c$e;->a:Lp6/c;

    invoke-static {p1}, Lp6/c;->g(Lp6/c;)Lb5/g$b;

    move-result-object p1

    goto :goto_1

    :cond_1
    move-object p1, v2

    :goto_1
    iput-object p1, v3, Lz4/b;->e:Lb5/g$b;

    iput-boolean v1, v3, Lz4/b;->g:Z

    const/4 p1, 0x0

    iput-boolean p1, v3, Lz4/b;->b:Z

    if-eqz p3, :cond_2

    iget-object p1, p0, Lp6/c$e;->a:Lp6/c;

    invoke-static {v3}, Ly4/a;->B(Lz4/b;)Ly4/a;

    move-result-object p2

    :goto_2
    invoke-static {p1, p2}, Lp6/c;->c(Lp6/c;Ly4/a;)Ly4/a;

    goto :goto_3

    :cond_2
    iget-object p1, p0, Lp6/c$e;->a:Lp6/c;

    invoke-static {v3}, Ly4/a;->C(Lz4/b;)Ly4/a;

    move-result-object p2

    goto :goto_2

    :goto_3
    iget-object p1, p0, Lp6/c$e;->a:Lp6/c;

    invoke-static {p1}, Lp6/c;->b(Lp6/c;)Ly4/a;

    move-result-object p1

    invoke-virtual {p1}, Ly4/a;->D()V

    iget-object p1, p0, Lp6/c$e;->a:Lp6/c;

    invoke-static {p1}, Lp6/c;->h(Lp6/c;)Ljava/lang/Runnable;

    move-result-object p1

    if-eqz p1, :cond_3

    iget-object p1, p0, Lp6/c$e;->a:Lp6/c;

    invoke-static {p1}, Lp6/c;->h(Lp6/c;)Ljava/lang/Runnable;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    :cond_3
    iget-object p1, p0, Lp6/c$e;->a:Lp6/c;

    invoke-static {p1}, Lp6/c;->a(Lp6/c;)Ljava/lang/Runnable;

    move-result-object p1

    if-eqz p1, :cond_4

    iget-object p1, p0, Lp6/c$e;->a:Lp6/c;

    invoke-static {p1}, Lp6/c;->a(Lp6/c;)Ljava/lang/Runnable;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/Runnable;->run()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_4

    :catch_0
    move-exception p1

    const-string p2, "DbHelper"

    const-string p3, "DB instance create failed, DB lock down!"

    invoke-static {p2, p3, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    iget-object p1, p0, Lp6/c$e;->a:Lp6/c;

    invoke-static {p1, v2}, Lp6/c;->c(Lp6/c;Ly4/a;)Ly4/a;

    iget-object p1, p0, Lp6/c$e;->a:Lp6/c;

    invoke-static {p1, v1}, Lp6/c;->e(Lp6/c;Z)Z

    :cond_4
    :goto_4
    return-void
.end method
