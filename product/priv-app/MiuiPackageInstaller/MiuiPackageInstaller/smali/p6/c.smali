.class final enum Lp6/c;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lp6/c$e;,
        Lp6/c$f;,
        Lp6/c$h;,
        Lp6/c$g;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lp6/c;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum h:Lp6/c;

.field private static final synthetic i:[Lp6/c;


# instance fields
.field private a:Ly4/a;

.field private b:Lp6/c$f;

.field private volatile c:Z

.field private d:Lb5/g$b;

.field private e:Lb5/g$a;

.field private f:Ljava/lang/Runnable;

.field private g:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    new-instance v6, Lp6/c;

    const-string v1, "MAIN"

    const/4 v2, 0x0

    const-string v3, "app_security_risk_app.db"

    const/4 v4, 0x1

    const/4 v5, 0x1

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lp6/c;-><init>(Ljava/lang/String;ILjava/lang/String;IZ)V

    sput-object v6, Lp6/c;->h:Lp6/c;

    const/4 v0, 0x1

    new-array v0, v0, [Lp6/c;

    const/4 v1, 0x0

    aput-object v6, v0, v1

    sput-object v0, Lp6/c;->i:[Lp6/c;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;IZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "IZ)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    new-instance p1, Lp6/c$f;

    const/4 p2, 0x0

    invoke-direct {p1, p0, p2}, Lp6/c$f;-><init>(Lp6/c;Lp6/c$a;)V

    iput-object p1, p0, Lp6/c;->b:Lp6/c$f;

    const/4 p1, 0x0

    iput-boolean p1, p0, Lp6/c;->c:Z

    new-instance p1, Lp6/c$e;

    invoke-direct {p1, p0}, Lp6/c$e;-><init>(Lp6/c;)V

    invoke-static {p1, p3, p4, p5}, Lp6/c$e;->a(Lp6/c$e;Ljava/lang/String;IZ)V

    return-void
.end method

.method static synthetic a(Lp6/c;)Ljava/lang/Runnable;
    .locals 0

    iget-object p0, p0, Lp6/c;->f:Ljava/lang/Runnable;

    return-object p0
.end method

.method static synthetic b(Lp6/c;)Ly4/a;
    .locals 0

    iget-object p0, p0, Lp6/c;->a:Ly4/a;

    return-object p0
.end method

.method static synthetic c(Lp6/c;Ly4/a;)Ly4/a;
    .locals 0

    iput-object p1, p0, Lp6/c;->a:Ly4/a;

    return-object p1
.end method

.method static synthetic d(Lp6/c;)Z
    .locals 0

    iget-boolean p0, p0, Lp6/c;->c:Z

    return p0
.end method

.method static synthetic e(Lp6/c;Z)Z
    .locals 0

    iput-boolean p1, p0, Lp6/c;->c:Z

    return p1
.end method

.method static synthetic f(Lp6/c;)Lb5/g$a;
    .locals 0

    iget-object p0, p0, Lp6/c;->e:Lb5/g$a;

    return-object p0
.end method

.method static synthetic g(Lp6/c;)Lb5/g$b;
    .locals 0

    iget-object p0, p0, Lp6/c;->d:Lb5/g$b;

    return-object p0
.end method

.method static synthetic h(Lp6/c;)Ljava/lang/Runnable;
    .locals 0

    iget-object p0, p0, Lp6/c;->g:Ljava/lang/Runnable;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lp6/c;
    .locals 1

    const-class v0, Lp6/c;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lp6/c;

    return-object p0
.end method

.method public static values()[Lp6/c;
    .locals 1

    sget-object v0, Lp6/c;->i:[Lp6/c;

    invoke-virtual {v0}, [Lp6/c;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lp6/c;

    return-object v0
.end method


# virtual methods
.method i(Ljava/lang/Object;)Lq6/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Lq6/b<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    goto/32 :goto_5

    nop

    :goto_0
    iget-object p1, p0, Lp6/c;->b:Lp6/c$f;

    goto/32 :goto_4

    nop

    :goto_1
    return-object p1

    :goto_2
    invoke-direct {v0, p0, p1}, Lp6/c$d;-><init>(Lp6/c;Ljava/lang/Object;)V

    goto/32 :goto_0

    nop

    :goto_3
    iget-object p1, v0, Lp6/c$h;->a:Lq6/b;

    goto/32 :goto_1

    nop

    :goto_4
    invoke-static {p1, v0}, Lp6/c$f;->b(Lp6/c$f;Lp6/c$h;)V

    goto/32 :goto_3

    nop

    :goto_5
    new-instance v0, Lp6/c$d;

    goto/32 :goto_2

    nop
.end method

.method j(Ljava/lang/Class;)Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)",
            "Ljava/util/ArrayList<",
            "TT;>;"
        }
    .end annotation

    goto/32 :goto_1

    nop

    :goto_0
    invoke-direct {v1, p0, p1}, Lp6/c$a;-><init>(Lp6/c;Ljava/lang/Class;)V

    goto/32 :goto_4

    nop

    :goto_1
    iget-object v0, p0, Lp6/c;->b:Lp6/c$f;

    goto/32 :goto_3

    nop

    :goto_2
    if-eqz p1, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_8

    nop

    :goto_3
    new-instance v1, Lp6/c$a;

    goto/32 :goto_0

    nop

    :goto_4
    invoke-static {v0, v1}, Lp6/c$f;->a(Lp6/c$f;Lp6/c$g;)Ljava/lang/Object;

    move-result-object p1

    goto/32 :goto_5

    nop

    :goto_5
    check-cast p1, Ljava/util/ArrayList;

    goto/32 :goto_2

    nop

    :goto_6
    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    :goto_7
    goto/32 :goto_9

    nop

    :goto_8
    new-instance p1, Ljava/util/ArrayList;

    goto/32 :goto_6

    nop

    :goto_9
    return-object p1
.end method

.method k(Ljava/lang/Object;)Lq6/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Lq6/b<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    goto/32 :goto_1

    nop

    :goto_0
    invoke-static {p1, v0}, Lp6/c$f;->b(Lp6/c$f;Lp6/c$h;)V

    goto/32 :goto_4

    nop

    :goto_1
    new-instance v0, Lp6/c$b;

    goto/32 :goto_3

    nop

    :goto_2
    return-object p1

    :goto_3
    invoke-direct {v0, p0, p1}, Lp6/c$b;-><init>(Lp6/c;Ljava/lang/Object;)V

    goto/32 :goto_5

    nop

    :goto_4
    iget-object p1, v0, Lp6/c$h;->a:Lq6/b;

    goto/32 :goto_2

    nop

    :goto_5
    iget-object p1, p0, Lp6/c;->b:Lp6/c$f;

    goto/32 :goto_0

    nop
.end method

.method l(Ljava/lang/Object;)Lq6/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Lq6/b<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    goto/32 :goto_5

    nop

    :goto_0
    iget-object p1, v0, Lp6/c$h;->a:Lq6/b;

    goto/32 :goto_4

    nop

    :goto_1
    iget-object p1, p0, Lp6/c;->b:Lp6/c$f;

    goto/32 :goto_3

    nop

    :goto_2
    invoke-direct {v0, p0, p1}, Lp6/c$c;-><init>(Lp6/c;Ljava/lang/Object;)V

    goto/32 :goto_1

    nop

    :goto_3
    invoke-static {p1, v0}, Lp6/c$f;->b(Lp6/c$f;Lp6/c$h;)V

    goto/32 :goto_0

    nop

    :goto_4
    return-object p1

    :goto_5
    new-instance v0, Lp6/c$c;

    goto/32 :goto_2

    nop
.end method
