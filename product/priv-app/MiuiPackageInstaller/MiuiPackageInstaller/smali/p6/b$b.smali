.class abstract Lp6/b$b;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lp6/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "b"
.end annotation


# instance fields
.field public volatile a:Z

.field protected b:Ljava/lang/Class;

.field protected c:Le5/c;

.field protected d:Ljava/lang/reflect/Field;

.field protected e:Z


# direct methods
.method public constructor <init>(Ljava/lang/Class;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lp6/b$b;->a:Z

    iput-object p1, p0, Lp6/b$b;->b:Ljava/lang/Class;

    invoke-static {p1}, Lz4/c;->p(Ljava/lang/Class;)Le5/c;

    move-result-object v0

    iput-object v0, p0, Lp6/b$b;->c:Le5/c;

    invoke-static {p1}, Lp6/a;->d(Ljava/lang/Class;)Ljava/lang/reflect/Field;

    move-result-object v0

    iput-object v0, p0, Lp6/b$b;->d:Ljava/lang/reflect/Field;

    invoke-static {p1}, Lp6/b;->c(Ljava/lang/Class;)Z

    move-result p1

    iput-boolean p1, p0, Lp6/b$b;->e:Z

    return-void
.end method


# virtual methods
.method public abstract a(Ljava/lang/Object;)V
.end method

.method public b(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "*>;)V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Lp6/b$b;->a(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public abstract c()Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end method

.method public abstract d(Ljava/lang/Object;)Ljava/lang/Object;
.end method

.method protected e(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lp6/b$b;->d:Ljava/lang/reflect/Field;

    invoke-static {v0, p1}, Lp6/a;->c(Ljava/lang/reflect/Field;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public abstract f(Ljava/lang/Object;)V
.end method

.method public abstract g(Ljava/lang/Object;)V
.end method
