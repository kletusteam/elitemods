.class public Lmicloud/compat/independent/sync/CNPrivacyUtilsCompat_V36;
.super Lmicloud/compat/independent/sync/CNPrivacyUtilsCompat_Base;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lmicloud/compat/independent/sync/CNPrivacyUtilsCompat_Base;-><init>()V

    return-void
.end method


# virtual methods
.method public sendCNPrivacyDeniedBroadcast(Landroid/content/Context;)V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.xiaomi.action.CN_PRIVACY_DENIED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.miui.cloudservice"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method
