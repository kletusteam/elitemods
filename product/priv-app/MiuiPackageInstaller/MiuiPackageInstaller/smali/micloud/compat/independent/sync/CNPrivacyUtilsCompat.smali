.class public Lmicloud/compat/independent/sync/CNPrivacyUtilsCompat;
.super Ljava/lang/Object;


# static fields
.field private static final sCNPrivacyUtilsCompatImpl:Lmicloud/compat/independent/sync/ICNPrivacyUtilsCompat;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    sget v0, Lt7/d;->a:I

    const/16 v1, 0x24

    if-lt v0, v1, :cond_0

    new-instance v0, Lmicloud/compat/independent/sync/CNPrivacyUtilsCompat_V36;

    invoke-direct {v0}, Lmicloud/compat/independent/sync/CNPrivacyUtilsCompat_V36;-><init>()V

    goto :goto_0

    :cond_0
    new-instance v0, Lmicloud/compat/independent/sync/CNPrivacyUtilsCompat_Base;

    invoke-direct {v0}, Lmicloud/compat/independent/sync/CNPrivacyUtilsCompat_Base;-><init>()V

    :goto_0
    sput-object v0, Lmicloud/compat/independent/sync/CNPrivacyUtilsCompat;->sCNPrivacyUtilsCompatImpl:Lmicloud/compat/independent/sync/ICNPrivacyUtilsCompat;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static sendCNPrivacyDeniedBroadcast(Landroid/content/Context;)V
    .locals 1

    sget-object v0, Lmicloud/compat/independent/sync/CNPrivacyUtilsCompat;->sCNPrivacyUtilsCompatImpl:Lmicloud/compat/independent/sync/ICNPrivacyUtilsCompat;

    invoke-interface {v0, p0}, Lmicloud/compat/independent/sync/ICNPrivacyUtilsCompat;->sendCNPrivacyDeniedBroadcast(Landroid/content/Context;)V

    return-void
.end method
