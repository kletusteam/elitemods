.class Lmicloud/compat/independent/request/RequestEnvBuilderCompat_V18$1;
.super Ljava/lang/Object;

# interfaces
.implements Lmicloud/compat/independent/request/IRequestEnvBuilderCompat$RequestEnv;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lmicloud/compat/independent/request/RequestEnvBuilderCompat_V18;->build()Lmicloud/compat/independent/request/IRequestEnvBuilderCompat$RequestEnv;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lmicloud/compat/independent/request/RequestEnvBuilderCompat_V18;


# direct methods
.method constructor <init>(Lmicloud/compat/independent/request/RequestEnvBuilderCompat_V18;)V
    .locals 0

    iput-object p1, p0, Lmicloud/compat/independent/request/RequestEnvBuilderCompat_V18$1;->this$0:Lmicloud/compat/independent/request/RequestEnvBuilderCompat_V18;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAutoRetryInterval()J
    .locals 2

    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getEncryptedUserId(Landroid/content/Context;Landroid/os/IBinder;Landroid/accounts/Account;)Ljava/lang/String;
    .locals 0

    invoke-static {p2, p3}, Lcom/xiaomi/micloudsdk/utils/IXiaomiAccountServiceProxy;->getEncryptedUserId(Landroid/os/IBinder;Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getMaxRetryCount()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getSystemAccount(Landroid/content/Context;)Landroid/accounts/Account;
    .locals 1

    invoke-static {p1}, Lmiui/accounts/ExtraAccountManager;->getXiaomiAccount(Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object p1

    if-nez p1, :cond_0

    const-string p1, "RequestEvnCompat_V18"

    const-string v0, "no account in system"

    invoke-static {p1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 p1, 0x0

    :cond_0
    return-object p1
.end method

.method public declared-synchronized getUserAgent()Ljava/lang/String;
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lmicloud/compat/independent/request/RequestEnvBuilderCompat_V18$1;->this$0:Lmicloud/compat/independent/request/RequestEnvBuilderCompat_V18;

    invoke-static {v0}, Lmicloud/compat/independent/request/RequestEnvBuilderCompat_V18;->access$200(Lmicloud/compat/independent/request/RequestEnvBuilderCompat_V18;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "; MIUI/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Landroid/os/Build$VERSION;->INCREMENTAL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    const-string v1, "miui.os.Build"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const-string v2, "IS_ALPHA_BUILD"

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, "ALPHA"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/NoSuchFieldException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catch_0
    :try_start_2
    const-string v1, "RequestEvnCompat_V18"

    const-string v2, "Not in MIUI in getUserAgent"

    :goto_0
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :catch_1
    const-string v1, "RequestEvnCompat_V18"

    const-string v2, "Not in MIUI in getUserAgent"

    goto :goto_0

    :catch_2
    const-string v1, "RequestEvnCompat_V18"

    const-string v2, "Not in MIUI in getUserAgent"

    goto :goto_0

    :catch_3
    const-string v1, "RequestEvnCompat_V18"

    const-string v2, "Not in MIUI in getUserAgent"

    goto :goto_0

    :cond_0
    :goto_1
    iget-object v1, p0, Lmicloud/compat/independent/request/RequestEnvBuilderCompat_V18$1;->this$0:Lmicloud/compat/independent/request/RequestEnvBuilderCompat_V18;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lmicloud/compat/independent/request/RequestEnvBuilderCompat_V18;->access$202(Lmicloud/compat/independent/request/RequestEnvBuilderCompat_V18;Ljava/lang/String;)Ljava/lang/String;

    :cond_1
    iget-object v0, p0, Lmicloud/compat/independent/request/RequestEnvBuilderCompat_V18$1;->this$0:Lmicloud/compat/independent/request/RequestEnvBuilderCompat_V18;

    invoke-static {v0}, Lmicloud/compat/independent/request/RequestEnvBuilderCompat_V18;->access$200(Lmicloud/compat/independent/request/RequestEnvBuilderCompat_V18;)Ljava/lang/String;

    move-result-object v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public invalidateAuthToken(Landroid/content/Context;)V
    .locals 2

    iget-object v0, p0, Lmicloud/compat/independent/request/RequestEnvBuilderCompat_V18$1;->this$0:Lmicloud/compat/independent/request/RequestEnvBuilderCompat_V18;

    invoke-static {v0}, Lmicloud/compat/independent/request/RequestEnvBuilderCompat_V18;->access$000(Lmicloud/compat/independent/request/RequestEnvBuilderCompat_V18;)Ljava/lang/ThreadLocal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v0, "RequestEvnCompat_V18"

    const-string v1, "invalidateAutoToken"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object p1

    iget-object v0, p0, Lmicloud/compat/independent/request/RequestEnvBuilderCompat_V18$1;->this$0:Lmicloud/compat/independent/request/RequestEnvBuilderCompat_V18;

    invoke-static {v0}, Lmicloud/compat/independent/request/RequestEnvBuilderCompat_V18;->access$000(Lmicloud/compat/independent/request/RequestEnvBuilderCompat_V18;)Ljava/lang/ThreadLocal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "com.xiaomi"

    invoke-virtual {p1, v1, v0}, Landroid/accounts/AccountManager;->invalidateAuthToken(Ljava/lang/String;Ljava/lang/String;)V

    iget-object p1, p0, Lmicloud/compat/independent/request/RequestEnvBuilderCompat_V18$1;->this$0:Lmicloud/compat/independent/request/RequestEnvBuilderCompat_V18;

    invoke-static {p1}, Lmicloud/compat/independent/request/RequestEnvBuilderCompat_V18;->access$000(Lmicloud/compat/independent/request/RequestEnvBuilderCompat_V18;)Ljava/lang/ThreadLocal;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public queryAuthToken(Landroid/content/Context;)Ljava/lang/String;
    .locals 11

    const-string v0, "RequestEvnCompat_V18"

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    const/4 v3, 0x3

    const/4 v4, 0x0

    if-ge v1, v3, :cond_3

    :try_start_0
    invoke-virtual {p0, p1}, Lmicloud/compat/independent/request/RequestEnvBuilderCompat_V18$1;->getSystemAccount(Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v6

    if-nez v6, :cond_0

    return-object v4

    :cond_0
    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v5

    const-string v7, "micloud"

    const/4 v8, 0x1

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual/range {v5 .. v10}, Landroid/accounts/AccountManager;->getAuthToken(Landroid/accounts/Account;Ljava/lang/String;ZLandroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    move-result-object v3

    invoke-interface {v3}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/Bundle;

    const-string v5, "authtoken"

    invoke-virtual {v3, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_1

    return-object v4

    :cond_1
    iget-object v5, p0, Lmicloud/compat/independent/request/RequestEnvBuilderCompat_V18$1;->this$0:Lmicloud/compat/independent/request/RequestEnvBuilderCompat_V18;

    invoke-static {v5}, Lmicloud/compat/independent/request/RequestEnvBuilderCompat_V18;->access$000(Lmicloud/compat/independent/request/RequestEnvBuilderCompat_V18;)Ljava/lang/ThreadLocal;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    iget-object v3, p0, Lmicloud/compat/independent/request/RequestEnvBuilderCompat_V18$1;->this$0:Lmicloud/compat/independent/request/RequestEnvBuilderCompat_V18;

    invoke-static {v3}, Lmicloud/compat/independent/request/RequestEnvBuilderCompat_V18;->access$000(Lmicloud/compat/independent/request/RequestEnvBuilderCompat_V18;)Ljava/lang/ThreadLocal;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v3

    :catch_0
    move-exception v3

    const-string v5, "AuthenticatorException when getting service token"

    invoke-static {v0, v5, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    if-nez v2, :cond_3

    invoke-virtual {p0, p1}, Lmicloud/compat/independent/request/RequestEnvBuilderCompat_V18$1;->invalidateAuthToken(Landroid/content/Context;)V

    const/4 v2, 0x1

    goto :goto_1

    :catch_1
    move-exception p1

    const-string v1, "OperationCanceledException when getting service token"

    invoke-static {v0, v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    return-object v4

    :catch_2
    move-exception v3

    const-string v4, "IOException when getting service token"

    invoke-static {v0, v4, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v4, 0x2

    if-ge v1, v4, :cond_2

    :try_start_1
    invoke-static {}, Lmicloud/compat/independent/request/RequestEnvBuilderCompat_V18;->access$100()[I

    move-result-object v4

    aget v4, v4, v1

    int-to-long v4, v4

    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_3

    goto :goto_1

    :catch_3
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Thread;->interrupt()V

    const-string v4, "InterruptedException when sleep"

    invoke-static {v0, v4, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_2
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    return-object v4
.end method

.method public shouldUpdateHost()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
