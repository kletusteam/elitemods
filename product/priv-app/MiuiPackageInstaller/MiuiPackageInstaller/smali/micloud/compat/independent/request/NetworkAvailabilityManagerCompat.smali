.class public Lmicloud/compat/independent/request/NetworkAvailabilityManagerCompat;
.super Ljava/lang/Object;


# static fields
.field private static final sNetworkAvailabilityManagerCompatImpl:Lmicloud/compat/independent/request/INetworkAvailabilityManagerCompat;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    sget v0, Lt7/d;->a:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_0

    new-instance v0, Lmicloud/compat/independent/request/NetworkAvailabilityManagerCompat_V18;

    invoke-direct {v0}, Lmicloud/compat/independent/request/NetworkAvailabilityManagerCompat_V18;-><init>()V

    goto :goto_0

    :cond_0
    new-instance v0, Lmicloud/compat/independent/request/NetworkAvailabilityManagerCompat_Base;

    invoke-direct {v0}, Lmicloud/compat/independent/request/NetworkAvailabilityManagerCompat_Base;-><init>()V

    :goto_0
    sput-object v0, Lmicloud/compat/independent/request/NetworkAvailabilityManagerCompat;->sNetworkAvailabilityManagerCompatImpl:Lmicloud/compat/independent/request/INetworkAvailabilityManagerCompat;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getAvailability(Landroid/content/Context;)Z
    .locals 1

    sget-object v0, Lmicloud/compat/independent/request/NetworkAvailabilityManagerCompat;->sNetworkAvailabilityManagerCompatImpl:Lmicloud/compat/independent/request/INetworkAvailabilityManagerCompat;

    invoke-interface {v0, p0}, Lmicloud/compat/independent/request/INetworkAvailabilityManagerCompat;->getAvailability(Landroid/content/Context;)Z

    move-result p0

    return p0
.end method

.method public static setAvailability(Landroid/content/Context;Z)V
    .locals 1

    sget-object v0, Lmicloud/compat/independent/request/NetworkAvailabilityManagerCompat;->sNetworkAvailabilityManagerCompatImpl:Lmicloud/compat/independent/request/INetworkAvailabilityManagerCompat;

    invoke-interface {v0, p0, p1}, Lmicloud/compat/independent/request/INetworkAvailabilityManagerCompat;->setAvailability(Landroid/content/Context;Z)V

    return-void
.end method
