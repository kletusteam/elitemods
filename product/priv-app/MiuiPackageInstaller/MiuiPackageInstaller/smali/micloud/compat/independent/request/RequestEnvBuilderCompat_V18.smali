.class Lmicloud/compat/independent/request/RequestEnvBuilderCompat_V18;
.super Lmicloud/compat/independent/request/RequestEnvBuilderCompat_Base;


# static fields
.field private static final MAX_RETRY_TIME:I = 0x3

.field private static final RETRY_INTERVALS:[I

.field private static final SERVICE_MICLOUD:Ljava/lang/String; = "micloud"

.field private static final TAG:Ljava/lang/String; = "RequestEvnCompat_V18"


# instance fields
.field private mExtendedAuthToken:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mUserAgent:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lmicloud/compat/independent/request/RequestEnvBuilderCompat_V18;->RETRY_INTERVALS:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x1388
        0x2710
    .end array-data
.end method

.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lmicloud/compat/independent/request/RequestEnvBuilderCompat_Base;-><init>()V

    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    iput-object v0, p0, Lmicloud/compat/independent/request/RequestEnvBuilderCompat_V18;->mExtendedAuthToken:Ljava/lang/ThreadLocal;

    return-void
.end method

.method static synthetic access$000(Lmicloud/compat/independent/request/RequestEnvBuilderCompat_V18;)Ljava/lang/ThreadLocal;
    .locals 0

    iget-object p0, p0, Lmicloud/compat/independent/request/RequestEnvBuilderCompat_V18;->mExtendedAuthToken:Ljava/lang/ThreadLocal;

    return-object p0
.end method

.method static synthetic access$100()[I
    .locals 1

    sget-object v0, Lmicloud/compat/independent/request/RequestEnvBuilderCompat_V18;->RETRY_INTERVALS:[I

    return-object v0
.end method

.method static synthetic access$200(Lmicloud/compat/independent/request/RequestEnvBuilderCompat_V18;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lmicloud/compat/independent/request/RequestEnvBuilderCompat_V18;->mUserAgent:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$202(Lmicloud/compat/independent/request/RequestEnvBuilderCompat_V18;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lmicloud/compat/independent/request/RequestEnvBuilderCompat_V18;->mUserAgent:Ljava/lang/String;

    return-object p1
.end method


# virtual methods
.method public build()Lmicloud/compat/independent/request/IRequestEnvBuilderCompat$RequestEnv;
    .locals 1

    new-instance v0, Lmicloud/compat/independent/request/RequestEnvBuilderCompat_V18$1;

    invoke-direct {v0, p0}, Lmicloud/compat/independent/request/RequestEnvBuilderCompat_V18$1;-><init>(Lmicloud/compat/independent/request/RequestEnvBuilderCompat_V18;)V

    return-object v0
.end method
