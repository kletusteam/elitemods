.class public interface abstract Lmicloud/compat/v18/finddevice/IFindDeviceLocateManagerCompat;
.super Ljava/lang/Object;


# virtual methods
.method public abstract isLocalLocateSupported(Landroid/content/Context;)Z
.end method

.method public abstract isLowBatteryLocateEnabled(Landroid/content/Context;)Z
.end method

.method public abstract isPowerPressLocateEnabled(Landroid/content/Context;)Z
.end method

.method public abstract resetLowBatteryLocate(Landroid/content/Context;)V
.end method

.method public abstract resetPowerPressLocate(Landroid/content/Context;)V
.end method

.method public abstract setLowBatteryLocateEnabled(Landroid/content/Context;Z)V
.end method

.method public abstract setPowerPressLocateEnabled(Landroid/content/Context;Z)V
.end method
