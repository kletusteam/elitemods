.class interface abstract Lmicloud/compat/v18/stat/IMiCloudStatUtilCompat;
.super Ljava/lang/Object;


# virtual methods
.method public abstract performSyncErrorStat(Landroid/content/Context;JLandroid/os/Bundle;)V
.end method

.method public abstract performSyncPhoneStateStat(Landroid/content/Context;Landroid/os/Bundle;)V
.end method

.method public abstract performSyncSuccessStat(Landroid/content/Context;Ljava/lang/String;JLandroid/os/Bundle;)V
.end method

.method public abstract setMasterSyncAutomatically(Landroid/content/Context;Z)V
.end method

.method public abstract startMiCloudMainActivity(Landroid/content/Context;)V
.end method

.method public abstract wrapErrorBundle(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Z)V
.end method
