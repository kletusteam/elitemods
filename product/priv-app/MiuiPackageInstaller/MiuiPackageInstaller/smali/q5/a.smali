.class public final Lq5/a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lq5/a$b;
    }
.end annotation


# static fields
.field public static final b:Lq5/a$b;

.field private static final c:La8/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La8/f<",
            "Lq5/a;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Landroid/content/SharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lq5/a$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lq5/a$b;-><init>(Lm8/g;)V

    sput-object v0, Lq5/a;->b:Lq5/a$b;

    sget-object v0, La8/j;->a:La8/j;

    sget-object v1, Lq5/a$a;->b:Lq5/a$a;

    invoke-static {v0, v1}, La8/g;->a(La8/j;Ll8/a;)La8/f;

    move-result-object v0

    sput-object v0, Lq5/a;->c:La8/f;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/android/packageinstaller/InstallerApplication;->c:Lcom/android/packageinstaller/InstallerApplication;

    const-string v1, "pure_mode"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/Application;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "sInstance.getSharedPrefe\u2026e\", Context.MODE_PRIVATE)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lq5/a;->a:Landroid/content/SharedPreferences;

    return-void
.end method

.method public static final synthetic a()La8/f;
    .locals 1

    sget-object v0, Lq5/a;->c:La8/f;

    return-object v0
.end method


# virtual methods
.method public final b(Ljava/lang/String;)J
    .locals 3

    const-string v0, "key"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lq5/a;->a:Landroid/content/SharedPreferences;

    const-wide/16 v1, 0x0

    invoke-interface {v0, p1, v1, v2}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final c(Ljava/lang/String;J)V
    .locals 1

    const-string v0, "key"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lq5/a;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0, p1, p2, p3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    :cond_0
    if-eqz v0, :cond_1

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_1
    return-void
.end method
