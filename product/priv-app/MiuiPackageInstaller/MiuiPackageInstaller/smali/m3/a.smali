.class public abstract Lm3/a;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lm3/a<",
        "TT;>;>",
        "Ljava/lang/Object;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:F

.field private c:Lw2/j;

.field private d:Lcom/bumptech/glide/g;

.field private e:Landroid/graphics/drawable/Drawable;

.field private f:I

.field private g:Landroid/graphics/drawable/Drawable;

.field private h:I

.field private i:Z

.field private j:I

.field private k:I

.field private l:Lu2/f;

.field private m:Z

.field private n:Z

.field private o:Landroid/graphics/drawable/Drawable;

.field private p:I

.field private q:Lu2/h;

.field private r:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lu2/l<",
            "*>;>;"
        }
    .end annotation
.end field

.field private s:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation
.end field

.field private t:Z

.field private u:Landroid/content/res/Resources$Theme;

.field private v:Z

.field private w:Z

.field private x:Z

.field private y:Z

.field private z:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lm3/a;->b:F

    sget-object v0, Lw2/j;->e:Lw2/j;

    iput-object v0, p0, Lm3/a;->c:Lw2/j;

    sget-object v0, Lcom/bumptech/glide/g;->c:Lcom/bumptech/glide/g;

    iput-object v0, p0, Lm3/a;->d:Lcom/bumptech/glide/g;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lm3/a;->i:Z

    const/4 v1, -0x1

    iput v1, p0, Lm3/a;->j:I

    iput v1, p0, Lm3/a;->k:I

    invoke-static {}, Lp3/c;->c()Lp3/c;

    move-result-object v1

    iput-object v1, p0, Lm3/a;->l:Lu2/f;

    iput-boolean v0, p0, Lm3/a;->n:Z

    new-instance v1, Lu2/h;

    invoke-direct {v1}, Lu2/h;-><init>()V

    iput-object v1, p0, Lm3/a;->q:Lu2/h;

    new-instance v1, Lq3/b;

    invoke-direct {v1}, Lq3/b;-><init>()V

    iput-object v1, p0, Lm3/a;->r:Ljava/util/Map;

    const-class v1, Ljava/lang/Object;

    iput-object v1, p0, Lm3/a;->s:Ljava/lang/Class;

    iput-boolean v0, p0, Lm3/a;->y:Z

    return-void
.end method

.method private G(I)Z
    .locals 1

    iget v0, p0, Lm3/a;->a:I

    invoke-static {v0, p1}, Lm3/a;->H(II)Z

    move-result p1

    return p1
.end method

.method private static H(II)Z
    .locals 0

    and-int/2addr p0, p1

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private U(Ld3/m;Lu2/l;)Lm3/a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ld3/m;",
            "Lu2/l<",
            "Landroid/graphics/Bitmap;",
            ">;)TT;"
        }
    .end annotation

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lm3/a;->Z(Ld3/m;Lu2/l;Z)Lm3/a;

    move-result-object p1

    return-object p1
.end method

.method private Z(Ld3/m;Lu2/l;Z)Lm3/a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ld3/m;",
            "Lu2/l<",
            "Landroid/graphics/Bitmap;",
            ">;Z)TT;"
        }
    .end annotation

    if-eqz p3, :cond_0

    invoke-virtual {p0, p1, p2}, Lm3/a;->g0(Ld3/m;Lu2/l;)Lm3/a;

    move-result-object p1

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p1, p2}, Lm3/a;->V(Ld3/m;Lu2/l;)Lm3/a;

    move-result-object p1

    :goto_0
    const/4 p2, 0x1

    iput-boolean p2, p1, Lm3/a;->y:Z

    return-object p1
.end method

.method private a0()Lm3/a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    return-object p0
.end method


# virtual methods
.method public final A()Z
    .locals 1

    iget-boolean v0, p0, Lm3/a;->z:Z

    return v0
.end method

.method public final B()Z
    .locals 1

    iget-boolean v0, p0, Lm3/a;->w:Z

    return v0
.end method

.method protected final C()Z
    .locals 1

    iget-boolean v0, p0, Lm3/a;->v:Z

    return v0
.end method

.method public final D()Z
    .locals 1

    iget-boolean v0, p0, Lm3/a;->i:Z

    return v0
.end method

.method public final E()Z
    .locals 1

    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lm3/a;->G(I)Z

    move-result v0

    return v0
.end method

.method F()Z
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-boolean v0, p0, Lm3/a;->y:Z

    goto/32 :goto_1

    nop

    :goto_1
    return v0
.end method

.method public final I()Z
    .locals 1

    iget-boolean v0, p0, Lm3/a;->n:Z

    return v0
.end method

.method public final N()Z
    .locals 1

    iget-boolean v0, p0, Lm3/a;->m:Z

    return v0
.end method

.method public final O()Z
    .locals 1

    const/16 v0, 0x800

    invoke-direct {p0, v0}, Lm3/a;->G(I)Z

    move-result v0

    return v0
.end method

.method public final P()Z
    .locals 2

    iget v0, p0, Lm3/a;->k:I

    iget v1, p0, Lm3/a;->j:I

    invoke-static {v0, v1}, Lq3/k;->s(II)Z

    move-result v0

    return v0
.end method

.method public Q()Lm3/a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    const/4 v0, 0x1

    iput-boolean v0, p0, Lm3/a;->t:Z

    invoke-direct {p0}, Lm3/a;->a0()Lm3/a;

    move-result-object v0

    return-object v0
.end method

.method public R()Lm3/a;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    sget-object v0, Ld3/m;->e:Ld3/m;

    new-instance v1, Ld3/i;

    invoke-direct {v1}, Ld3/i;-><init>()V

    invoke-virtual {p0, v0, v1}, Lm3/a;->V(Ld3/m;Lu2/l;)Lm3/a;

    move-result-object v0

    return-object v0
.end method

.method public S()Lm3/a;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    sget-object v0, Ld3/m;->d:Ld3/m;

    new-instance v1, Ld3/j;

    invoke-direct {v1}, Ld3/j;-><init>()V

    invoke-direct {p0, v0, v1}, Lm3/a;->U(Ld3/m;Lu2/l;)Lm3/a;

    move-result-object v0

    return-object v0
.end method

.method public T()Lm3/a;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    sget-object v0, Ld3/m;->c:Ld3/m;

    new-instance v1, Ld3/r;

    invoke-direct {v1}, Ld3/r;-><init>()V

    invoke-direct {p0, v0, v1}, Lm3/a;->U(Ld3/m;Lu2/l;)Lm3/a;

    move-result-object v0

    return-object v0
.end method

.method final V(Ld3/m;Lu2/l;)Lm3/a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ld3/m;",
            "Lu2/l<",
            "Landroid/graphics/Bitmap;",
            ">;)TT;"
        }
    .end annotation

    goto/32 :goto_7

    nop

    :goto_0
    invoke-virtual {p0, p1}, Lm3/a;->i(Ld3/m;)Lm3/a;

    goto/32 :goto_6

    nop

    :goto_1
    invoke-virtual {p0, p2, p1}, Lm3/a;->j0(Lu2/l;Z)Lm3/a;

    move-result-object p1

    goto/32 :goto_3

    nop

    :goto_2
    if-nez v0, :cond_0

    goto/32 :goto_9

    :cond_0
    goto/32 :goto_5

    nop

    :goto_3
    return-object p1

    :goto_4
    invoke-virtual {v0, p1, p2}, Lm3/a;->V(Ld3/m;Lu2/l;)Lm3/a;

    move-result-object p1

    goto/32 :goto_8

    nop

    :goto_5
    invoke-virtual {p0}, Lm3/a;->f()Lm3/a;

    move-result-object v0

    goto/32 :goto_4

    nop

    :goto_6
    const/4 p1, 0x0

    goto/32 :goto_1

    nop

    :goto_7
    iget-boolean v0, p0, Lm3/a;->v:Z

    goto/32 :goto_2

    nop

    :goto_8
    return-object p1

    :goto_9
    goto/32 :goto_0

    nop
.end method

.method public W(I)Lm3/a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    invoke-virtual {p0, p1, p1}, Lm3/a;->X(II)Lm3/a;

    move-result-object p1

    return-object p1
.end method

.method public X(II)Lm3/a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)TT;"
        }
    .end annotation

    iget-boolean v0, p0, Lm3/a;->v:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lm3/a;->f()Lm3/a;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lm3/a;->X(II)Lm3/a;

    move-result-object p1

    return-object p1

    :cond_0
    iput p1, p0, Lm3/a;->k:I

    iput p2, p0, Lm3/a;->j:I

    iget p1, p0, Lm3/a;->a:I

    or-int/lit16 p1, p1, 0x200

    iput p1, p0, Lm3/a;->a:I

    invoke-virtual {p0}, Lm3/a;->b0()Lm3/a;

    move-result-object p1

    return-object p1
.end method

.method public Y(Lcom/bumptech/glide/g;)Lm3/a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/bumptech/glide/g;",
            ")TT;"
        }
    .end annotation

    iget-boolean v0, p0, Lm3/a;->v:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lm3/a;->f()Lm3/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lm3/a;->Y(Lcom/bumptech/glide/g;)Lm3/a;

    move-result-object p1

    return-object p1

    :cond_0
    invoke-static {p1}, Lq3/j;->d(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/bumptech/glide/g;

    iput-object p1, p0, Lm3/a;->d:Lcom/bumptech/glide/g;

    iget p1, p0, Lm3/a;->a:I

    or-int/lit8 p1, p1, 0x8

    iput p1, p0, Lm3/a;->a:I

    invoke-virtual {p0}, Lm3/a;->b0()Lm3/a;

    move-result-object p1

    return-object p1
.end method

.method public a(Lm3/a;)Lm3/a;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lm3/a<",
            "*>;)TT;"
        }
    .end annotation

    iget-boolean v0, p0, Lm3/a;->v:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lm3/a;->f()Lm3/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lm3/a;->a(Lm3/a;)Lm3/a;

    move-result-object p1

    return-object p1

    :cond_0
    iget v0, p1, Lm3/a;->a:I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lm3/a;->H(II)Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p1, Lm3/a;->b:F

    iput v0, p0, Lm3/a;->b:F

    :cond_1
    iget v0, p1, Lm3/a;->a:I

    const/high16 v1, 0x40000

    invoke-static {v0, v1}, Lm3/a;->H(II)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p1, Lm3/a;->w:Z

    iput-boolean v0, p0, Lm3/a;->w:Z

    :cond_2
    iget v0, p1, Lm3/a;->a:I

    const/high16 v1, 0x100000

    invoke-static {v0, v1}, Lm3/a;->H(II)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-boolean v0, p1, Lm3/a;->z:Z

    iput-boolean v0, p0, Lm3/a;->z:Z

    :cond_3
    iget v0, p1, Lm3/a;->a:I

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lm3/a;->H(II)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p1, Lm3/a;->c:Lw2/j;

    iput-object v0, p0, Lm3/a;->c:Lw2/j;

    :cond_4
    iget v0, p1, Lm3/a;->a:I

    const/16 v1, 0x8

    invoke-static {v0, v1}, Lm3/a;->H(II)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p1, Lm3/a;->d:Lcom/bumptech/glide/g;

    iput-object v0, p0, Lm3/a;->d:Lcom/bumptech/glide/g;

    :cond_5
    iget v0, p1, Lm3/a;->a:I

    const/16 v1, 0x10

    invoke-static {v0, v1}, Lm3/a;->H(II)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_6

    iget-object v0, p1, Lm3/a;->e:Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Lm3/a;->e:Landroid/graphics/drawable/Drawable;

    iput v1, p0, Lm3/a;->f:I

    iget v0, p0, Lm3/a;->a:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lm3/a;->a:I

    :cond_6
    iget v0, p1, Lm3/a;->a:I

    const/16 v2, 0x20

    invoke-static {v0, v2}, Lm3/a;->H(II)Z

    move-result v0

    const/4 v2, 0x0

    if-eqz v0, :cond_7

    iget v0, p1, Lm3/a;->f:I

    iput v0, p0, Lm3/a;->f:I

    iput-object v2, p0, Lm3/a;->e:Landroid/graphics/drawable/Drawable;

    iget v0, p0, Lm3/a;->a:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lm3/a;->a:I

    :cond_7
    iget v0, p1, Lm3/a;->a:I

    const/16 v3, 0x40

    invoke-static {v0, v3}, Lm3/a;->H(II)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p1, Lm3/a;->g:Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Lm3/a;->g:Landroid/graphics/drawable/Drawable;

    iput v1, p0, Lm3/a;->h:I

    iget v0, p0, Lm3/a;->a:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lm3/a;->a:I

    :cond_8
    iget v0, p1, Lm3/a;->a:I

    const/16 v3, 0x80

    invoke-static {v0, v3}, Lm3/a;->H(II)Z

    move-result v0

    if-eqz v0, :cond_9

    iget v0, p1, Lm3/a;->h:I

    iput v0, p0, Lm3/a;->h:I

    iput-object v2, p0, Lm3/a;->g:Landroid/graphics/drawable/Drawable;

    iget v0, p0, Lm3/a;->a:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lm3/a;->a:I

    :cond_9
    iget v0, p1, Lm3/a;->a:I

    const/16 v3, 0x100

    invoke-static {v0, v3}, Lm3/a;->H(II)Z

    move-result v0

    if-eqz v0, :cond_a

    iget-boolean v0, p1, Lm3/a;->i:Z

    iput-boolean v0, p0, Lm3/a;->i:Z

    :cond_a
    iget v0, p1, Lm3/a;->a:I

    const/16 v3, 0x200

    invoke-static {v0, v3}, Lm3/a;->H(II)Z

    move-result v0

    if-eqz v0, :cond_b

    iget v0, p1, Lm3/a;->k:I

    iput v0, p0, Lm3/a;->k:I

    iget v0, p1, Lm3/a;->j:I

    iput v0, p0, Lm3/a;->j:I

    :cond_b
    iget v0, p1, Lm3/a;->a:I

    const/16 v3, 0x400

    invoke-static {v0, v3}, Lm3/a;->H(II)Z

    move-result v0

    if-eqz v0, :cond_c

    iget-object v0, p1, Lm3/a;->l:Lu2/f;

    iput-object v0, p0, Lm3/a;->l:Lu2/f;

    :cond_c
    iget v0, p1, Lm3/a;->a:I

    const/16 v3, 0x1000

    invoke-static {v0, v3}, Lm3/a;->H(II)Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object v0, p1, Lm3/a;->s:Ljava/lang/Class;

    iput-object v0, p0, Lm3/a;->s:Ljava/lang/Class;

    :cond_d
    iget v0, p1, Lm3/a;->a:I

    const/16 v3, 0x2000

    invoke-static {v0, v3}, Lm3/a;->H(II)Z

    move-result v0

    if-eqz v0, :cond_e

    iget-object v0, p1, Lm3/a;->o:Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Lm3/a;->o:Landroid/graphics/drawable/Drawable;

    iput v1, p0, Lm3/a;->p:I

    iget v0, p0, Lm3/a;->a:I

    and-int/lit16 v0, v0, -0x4001

    iput v0, p0, Lm3/a;->a:I

    :cond_e
    iget v0, p1, Lm3/a;->a:I

    const/16 v3, 0x4000

    invoke-static {v0, v3}, Lm3/a;->H(II)Z

    move-result v0

    if-eqz v0, :cond_f

    iget v0, p1, Lm3/a;->p:I

    iput v0, p0, Lm3/a;->p:I

    iput-object v2, p0, Lm3/a;->o:Landroid/graphics/drawable/Drawable;

    iget v0, p0, Lm3/a;->a:I

    and-int/lit16 v0, v0, -0x2001

    iput v0, p0, Lm3/a;->a:I

    :cond_f
    iget v0, p1, Lm3/a;->a:I

    const v2, 0x8000

    invoke-static {v0, v2}, Lm3/a;->H(II)Z

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, p1, Lm3/a;->u:Landroid/content/res/Resources$Theme;

    iput-object v0, p0, Lm3/a;->u:Landroid/content/res/Resources$Theme;

    :cond_10
    iget v0, p1, Lm3/a;->a:I

    const/high16 v2, 0x10000

    invoke-static {v0, v2}, Lm3/a;->H(II)Z

    move-result v0

    if-eqz v0, :cond_11

    iget-boolean v0, p1, Lm3/a;->n:Z

    iput-boolean v0, p0, Lm3/a;->n:Z

    :cond_11
    iget v0, p1, Lm3/a;->a:I

    const/high16 v2, 0x20000

    invoke-static {v0, v2}, Lm3/a;->H(II)Z

    move-result v0

    if-eqz v0, :cond_12

    iget-boolean v0, p1, Lm3/a;->m:Z

    iput-boolean v0, p0, Lm3/a;->m:Z

    :cond_12
    iget v0, p1, Lm3/a;->a:I

    const/16 v2, 0x800

    invoke-static {v0, v2}, Lm3/a;->H(II)Z

    move-result v0

    if-eqz v0, :cond_13

    iget-object v0, p0, Lm3/a;->r:Ljava/util/Map;

    iget-object v2, p1, Lm3/a;->r:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    iget-boolean v0, p1, Lm3/a;->y:Z

    iput-boolean v0, p0, Lm3/a;->y:Z

    :cond_13
    iget v0, p1, Lm3/a;->a:I

    const/high16 v2, 0x80000

    invoke-static {v0, v2}, Lm3/a;->H(II)Z

    move-result v0

    if-eqz v0, :cond_14

    iget-boolean v0, p1, Lm3/a;->x:Z

    iput-boolean v0, p0, Lm3/a;->x:Z

    :cond_14
    iget-boolean v0, p0, Lm3/a;->n:Z

    if-nez v0, :cond_15

    iget-object v0, p0, Lm3/a;->r:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iget v0, p0, Lm3/a;->a:I

    and-int/lit16 v0, v0, -0x801

    iput-boolean v1, p0, Lm3/a;->m:Z

    const v1, -0x20001

    and-int/2addr v0, v1

    iput v0, p0, Lm3/a;->a:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lm3/a;->y:Z

    :cond_15
    iget v0, p0, Lm3/a;->a:I

    iget v1, p1, Lm3/a;->a:I

    or-int/2addr v0, v1

    iput v0, p0, Lm3/a;->a:I

    iget-object v0, p0, Lm3/a;->q:Lu2/h;

    iget-object p1, p1, Lm3/a;->q:Lu2/h;

    invoke-virtual {v0, p1}, Lu2/h;->d(Lu2/h;)V

    invoke-virtual {p0}, Lm3/a;->b0()Lm3/a;

    move-result-object p1

    return-object p1
.end method

.method protected final b0()Lm3/a;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    iget-boolean v0, p0, Lm3/a;->t:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lm3/a;->a0()Lm3/a;

    move-result-object v0

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "You cannot modify locked T, consider clone()"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public c()Lm3/a;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    iget-boolean v0, p0, Lm3/a;->t:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lm3/a;->v:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "You cannot auto lock an already locked options object, try clone() first"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lm3/a;->v:Z

    invoke-virtual {p0}, Lm3/a;->Q()Lm3/a;

    move-result-object v0

    return-object v0
.end method

.method public c0(Lu2/g;Ljava/lang/Object;)Lm3/a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Y:",
            "Ljava/lang/Object;",
            ">(",
            "Lu2/g<",
            "TY;>;TY;)TT;"
        }
    .end annotation

    iget-boolean v0, p0, Lm3/a;->v:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lm3/a;->f()Lm3/a;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lm3/a;->c0(Lu2/g;Ljava/lang/Object;)Lm3/a;

    move-result-object p1

    return-object p1

    :cond_0
    invoke-static {p1}, Lq3/j;->d(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lq3/j;->d(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lm3/a;->q:Lu2/h;

    invoke-virtual {v0, p1, p2}, Lu2/h;->e(Lu2/g;Ljava/lang/Object;)Lu2/h;

    invoke-virtual {p0}, Lm3/a;->b0()Lm3/a;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lm3/a;->f()Lm3/a;

    move-result-object v0

    return-object v0
.end method

.method public d()Lm3/a;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    sget-object v0, Ld3/m;->e:Ld3/m;

    new-instance v1, Ld3/i;

    invoke-direct {v1}, Ld3/i;-><init>()V

    invoke-virtual {p0, v0, v1}, Lm3/a;->g0(Ld3/m;Lu2/l;)Lm3/a;

    move-result-object v0

    return-object v0
.end method

.method public d0(Lu2/f;)Lm3/a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lu2/f;",
            ")TT;"
        }
    .end annotation

    iget-boolean v0, p0, Lm3/a;->v:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lm3/a;->f()Lm3/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lm3/a;->d0(Lu2/f;)Lm3/a;

    move-result-object p1

    return-object p1

    :cond_0
    invoke-static {p1}, Lq3/j;->d(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lu2/f;

    iput-object p1, p0, Lm3/a;->l:Lu2/f;

    iget p1, p0, Lm3/a;->a:I

    or-int/lit16 p1, p1, 0x400

    iput p1, p0, Lm3/a;->a:I

    invoke-virtual {p0}, Lm3/a;->b0()Lm3/a;

    move-result-object p1

    return-object p1
.end method

.method public e()Lm3/a;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    sget-object v0, Ld3/m;->d:Ld3/m;

    new-instance v1, Ld3/k;

    invoke-direct {v1}, Ld3/k;-><init>()V

    invoke-virtual {p0, v0, v1}, Lm3/a;->g0(Ld3/m;Lu2/l;)Lm3/a;

    move-result-object v0

    return-object v0
.end method

.method public e0(F)Lm3/a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(F)TT;"
        }
    .end annotation

    iget-boolean v0, p0, Lm3/a;->v:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lm3/a;->f()Lm3/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lm3/a;->e0(F)Lm3/a;

    move-result-object p1

    return-object p1

    :cond_0
    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-ltz v0, :cond_1

    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float v0, p1, v0

    if-gtz v0, :cond_1

    iput p1, p0, Lm3/a;->b:F

    iget p1, p0, Lm3/a;->a:I

    or-int/lit8 p1, p1, 0x2

    iput p1, p0, Lm3/a;->a:I

    invoke-virtual {p0}, Lm3/a;->b0()Lm3/a;

    move-result-object p1

    return-object p1

    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "sizeMultiplier must be between 0 and 1"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    instance-of v0, p1, Lm3/a;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    check-cast p1, Lm3/a;

    iget v0, p1, Lm3/a;->b:F

    iget v2, p0, Lm3/a;->b:F

    invoke-static {v0, v2}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lm3/a;->f:I

    iget v2, p1, Lm3/a;->f:I

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lm3/a;->e:Landroid/graphics/drawable/Drawable;

    iget-object v2, p1, Lm3/a;->e:Landroid/graphics/drawable/Drawable;

    invoke-static {v0, v2}, Lq3/k;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lm3/a;->h:I

    iget v2, p1, Lm3/a;->h:I

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lm3/a;->g:Landroid/graphics/drawable/Drawable;

    iget-object v2, p1, Lm3/a;->g:Landroid/graphics/drawable/Drawable;

    invoke-static {v0, v2}, Lq3/k;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lm3/a;->p:I

    iget v2, p1, Lm3/a;->p:I

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lm3/a;->o:Landroid/graphics/drawable/Drawable;

    iget-object v2, p1, Lm3/a;->o:Landroid/graphics/drawable/Drawable;

    invoke-static {v0, v2}, Lq3/k;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lm3/a;->i:Z

    iget-boolean v2, p1, Lm3/a;->i:Z

    if-ne v0, v2, :cond_0

    iget v0, p0, Lm3/a;->j:I

    iget v2, p1, Lm3/a;->j:I

    if-ne v0, v2, :cond_0

    iget v0, p0, Lm3/a;->k:I

    iget v2, p1, Lm3/a;->k:I

    if-ne v0, v2, :cond_0

    iget-boolean v0, p0, Lm3/a;->m:Z

    iget-boolean v2, p1, Lm3/a;->m:Z

    if-ne v0, v2, :cond_0

    iget-boolean v0, p0, Lm3/a;->n:Z

    iget-boolean v2, p1, Lm3/a;->n:Z

    if-ne v0, v2, :cond_0

    iget-boolean v0, p0, Lm3/a;->w:Z

    iget-boolean v2, p1, Lm3/a;->w:Z

    if-ne v0, v2, :cond_0

    iget-boolean v0, p0, Lm3/a;->x:Z

    iget-boolean v2, p1, Lm3/a;->x:Z

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lm3/a;->c:Lw2/j;

    iget-object v2, p1, Lm3/a;->c:Lw2/j;

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lm3/a;->d:Lcom/bumptech/glide/g;

    iget-object v2, p1, Lm3/a;->d:Lcom/bumptech/glide/g;

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lm3/a;->q:Lu2/h;

    iget-object v2, p1, Lm3/a;->q:Lu2/h;

    invoke-virtual {v0, v2}, Lu2/h;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lm3/a;->r:Ljava/util/Map;

    iget-object v2, p1, Lm3/a;->r:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lm3/a;->s:Ljava/lang/Class;

    iget-object v2, p1, Lm3/a;->s:Ljava/lang/Class;

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lm3/a;->l:Lu2/f;

    iget-object v2, p1, Lm3/a;->l:Lu2/f;

    invoke-static {v0, v2}, Lq3/k;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lm3/a;->u:Landroid/content/res/Resources$Theme;

    iget-object p1, p1, Lm3/a;->u:Landroid/content/res/Resources$Theme;

    invoke-static {v0, p1}, Lq3/k;->c(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method public f()Lm3/a;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lm3/a;

    new-instance v1, Lu2/h;

    invoke-direct {v1}, Lu2/h;-><init>()V

    iput-object v1, v0, Lm3/a;->q:Lu2/h;

    iget-object v2, p0, Lm3/a;->q:Lu2/h;

    invoke-virtual {v1, v2}, Lu2/h;->d(Lu2/h;)V

    new-instance v1, Lq3/b;

    invoke-direct {v1}, Lq3/b;-><init>()V

    iput-object v1, v0, Lm3/a;->r:Ljava/util/Map;

    iget-object v2, p0, Lm3/a;->r:Ljava/util/Map;

    invoke-interface {v1, v2}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    const/4 v1, 0x0

    iput-boolean v1, v0, Lm3/a;->t:Z

    iput-boolean v1, v0, Lm3/a;->v:Z
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public f0(Z)Lm3/a;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)TT;"
        }
    .end annotation

    iget-boolean v0, p0, Lm3/a;->v:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lm3/a;->f()Lm3/a;

    move-result-object p1

    invoke-virtual {p1, v1}, Lm3/a;->f0(Z)Lm3/a;

    move-result-object p1

    return-object p1

    :cond_0
    xor-int/2addr p1, v1

    iput-boolean p1, p0, Lm3/a;->i:Z

    iget p1, p0, Lm3/a;->a:I

    or-int/lit16 p1, p1, 0x100

    iput p1, p0, Lm3/a;->a:I

    invoke-virtual {p0}, Lm3/a;->b0()Lm3/a;

    move-result-object p1

    return-object p1
.end method

.method public g(Ljava/lang/Class;)Lm3/a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "*>;)TT;"
        }
    .end annotation

    iget-boolean v0, p0, Lm3/a;->v:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lm3/a;->f()Lm3/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lm3/a;->g(Ljava/lang/Class;)Lm3/a;

    move-result-object p1

    return-object p1

    :cond_0
    invoke-static {p1}, Lq3/j;->d(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Class;

    iput-object p1, p0, Lm3/a;->s:Ljava/lang/Class;

    iget p1, p0, Lm3/a;->a:I

    or-int/lit16 p1, p1, 0x1000

    iput p1, p0, Lm3/a;->a:I

    invoke-virtual {p0}, Lm3/a;->b0()Lm3/a;

    move-result-object p1

    return-object p1
.end method

.method final g0(Ld3/m;Lu2/l;)Lm3/a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ld3/m;",
            "Lu2/l<",
            "Landroid/graphics/Bitmap;",
            ">;)TT;"
        }
    .end annotation

    goto/32 :goto_6

    nop

    :goto_0
    invoke-virtual {p0, p1}, Lm3/a;->i(Ld3/m;)Lm3/a;

    goto/32 :goto_7

    nop

    :goto_1
    invoke-virtual {v0, p1, p2}, Lm3/a;->g0(Ld3/m;Lu2/l;)Lm3/a;

    move-result-object p1

    goto/32 :goto_4

    nop

    :goto_2
    return-object p1

    :goto_3
    if-nez v0, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_8

    nop

    :goto_4
    return-object p1

    :goto_5
    goto/32 :goto_0

    nop

    :goto_6
    iget-boolean v0, p0, Lm3/a;->v:Z

    goto/32 :goto_3

    nop

    :goto_7
    invoke-virtual {p0, p2}, Lm3/a;->i0(Lu2/l;)Lm3/a;

    move-result-object p1

    goto/32 :goto_2

    nop

    :goto_8
    invoke-virtual {p0}, Lm3/a;->f()Lm3/a;

    move-result-object v0

    goto/32 :goto_1

    nop
.end method

.method public h(Lw2/j;)Lm3/a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lw2/j;",
            ")TT;"
        }
    .end annotation

    iget-boolean v0, p0, Lm3/a;->v:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lm3/a;->f()Lm3/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lm3/a;->h(Lw2/j;)Lm3/a;

    move-result-object p1

    return-object p1

    :cond_0
    invoke-static {p1}, Lq3/j;->d(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lw2/j;

    iput-object p1, p0, Lm3/a;->c:Lw2/j;

    iget p1, p0, Lm3/a;->a:I

    or-int/lit8 p1, p1, 0x4

    iput p1, p0, Lm3/a;->a:I

    invoke-virtual {p0}, Lm3/a;->b0()Lm3/a;

    move-result-object p1

    return-object p1
.end method

.method h0(Ljava/lang/Class;Lu2/l;Z)Lm3/a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Y:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class<",
            "TY;>;",
            "Lu2/l<",
            "TY;>;Z)TT;"
        }
    .end annotation

    goto/32 :goto_1

    nop

    :goto_0
    iget-object v0, p0, Lm3/a;->r:Ljava/util/Map;

    goto/32 :goto_15

    nop

    :goto_1
    iget-boolean v0, p0, Lm3/a;->v:Z

    goto/32 :goto_10

    nop

    :goto_2
    invoke-virtual {p0}, Lm3/a;->f()Lm3/a;

    move-result-object v0

    goto/32 :goto_1a

    nop

    :goto_3
    iput-boolean p2, p0, Lm3/a;->m:Z

    :goto_4
    goto/32 :goto_b

    nop

    :goto_5
    or-int/2addr p1, v0

    goto/32 :goto_11

    nop

    :goto_6
    invoke-static {p1}, Lq3/j;->d(Ljava/lang/Object;)Ljava/lang/Object;

    goto/32 :goto_d

    nop

    :goto_7
    iget p1, p0, Lm3/a;->a:I

    goto/32 :goto_13

    nop

    :goto_8
    return-object p1

    :goto_9
    goto/32 :goto_6

    nop

    :goto_a
    const/4 p2, 0x1

    goto/32 :goto_e

    nop

    :goto_b
    invoke-virtual {p0}, Lm3/a;->b0()Lm3/a;

    move-result-object p1

    goto/32 :goto_f

    nop

    :goto_c
    iput p1, p0, Lm3/a;->a:I

    goto/32 :goto_3

    nop

    :goto_d
    invoke-static {p2}, Lq3/j;->d(Ljava/lang/Object;)Ljava/lang/Object;

    goto/32 :goto_0

    nop

    :goto_e
    iput-boolean p2, p0, Lm3/a;->n:Z

    goto/32 :goto_14

    nop

    :goto_f
    return-object p1

    :goto_10
    if-nez v0, :cond_0

    goto/32 :goto_9

    :cond_0
    goto/32 :goto_2

    nop

    :goto_11
    iput p1, p0, Lm3/a;->a:I

    goto/32 :goto_19

    nop

    :goto_12
    const/high16 p3, 0x20000

    goto/32 :goto_18

    nop

    :goto_13
    or-int/lit16 p1, p1, 0x800

    goto/32 :goto_a

    nop

    :goto_14
    const/high16 v0, 0x10000

    goto/32 :goto_5

    nop

    :goto_15
    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/32 :goto_7

    nop

    :goto_16
    if-nez p3, :cond_1

    goto/32 :goto_4

    :cond_1
    goto/32 :goto_12

    nop

    :goto_17
    iput-boolean v0, p0, Lm3/a;->y:Z

    goto/32 :goto_16

    nop

    :goto_18
    or-int/2addr p1, p3

    goto/32 :goto_c

    nop

    :goto_19
    const/4 v0, 0x0

    goto/32 :goto_17

    nop

    :goto_1a
    invoke-virtual {v0, p1, p2, p3}, Lm3/a;->h0(Ljava/lang/Class;Lu2/l;Z)Lm3/a;

    move-result-object p1

    goto/32 :goto_8

    nop
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lm3/a;->b:F

    invoke-static {v0}, Lq3/k;->k(F)I

    move-result v0

    iget v1, p0, Lm3/a;->f:I

    invoke-static {v1, v0}, Lq3/k;->m(II)I

    move-result v0

    iget-object v1, p0, Lm3/a;->e:Landroid/graphics/drawable/Drawable;

    invoke-static {v1, v0}, Lq3/k;->n(Ljava/lang/Object;I)I

    move-result v0

    iget v1, p0, Lm3/a;->h:I

    invoke-static {v1, v0}, Lq3/k;->m(II)I

    move-result v0

    iget-object v1, p0, Lm3/a;->g:Landroid/graphics/drawable/Drawable;

    invoke-static {v1, v0}, Lq3/k;->n(Ljava/lang/Object;I)I

    move-result v0

    iget v1, p0, Lm3/a;->p:I

    invoke-static {v1, v0}, Lq3/k;->m(II)I

    move-result v0

    iget-object v1, p0, Lm3/a;->o:Landroid/graphics/drawable/Drawable;

    invoke-static {v1, v0}, Lq3/k;->n(Ljava/lang/Object;I)I

    move-result v0

    iget-boolean v1, p0, Lm3/a;->i:Z

    invoke-static {v1, v0}, Lq3/k;->o(ZI)I

    move-result v0

    iget v1, p0, Lm3/a;->j:I

    invoke-static {v1, v0}, Lq3/k;->m(II)I

    move-result v0

    iget v1, p0, Lm3/a;->k:I

    invoke-static {v1, v0}, Lq3/k;->m(II)I

    move-result v0

    iget-boolean v1, p0, Lm3/a;->m:Z

    invoke-static {v1, v0}, Lq3/k;->o(ZI)I

    move-result v0

    iget-boolean v1, p0, Lm3/a;->n:Z

    invoke-static {v1, v0}, Lq3/k;->o(ZI)I

    move-result v0

    iget-boolean v1, p0, Lm3/a;->w:Z

    invoke-static {v1, v0}, Lq3/k;->o(ZI)I

    move-result v0

    iget-boolean v1, p0, Lm3/a;->x:Z

    invoke-static {v1, v0}, Lq3/k;->o(ZI)I

    move-result v0

    iget-object v1, p0, Lm3/a;->c:Lw2/j;

    invoke-static {v1, v0}, Lq3/k;->n(Ljava/lang/Object;I)I

    move-result v0

    iget-object v1, p0, Lm3/a;->d:Lcom/bumptech/glide/g;

    invoke-static {v1, v0}, Lq3/k;->n(Ljava/lang/Object;I)I

    move-result v0

    iget-object v1, p0, Lm3/a;->q:Lu2/h;

    invoke-static {v1, v0}, Lq3/k;->n(Ljava/lang/Object;I)I

    move-result v0

    iget-object v1, p0, Lm3/a;->r:Ljava/util/Map;

    invoke-static {v1, v0}, Lq3/k;->n(Ljava/lang/Object;I)I

    move-result v0

    iget-object v1, p0, Lm3/a;->s:Ljava/lang/Class;

    invoke-static {v1, v0}, Lq3/k;->n(Ljava/lang/Object;I)I

    move-result v0

    iget-object v1, p0, Lm3/a;->l:Lu2/f;

    invoke-static {v1, v0}, Lq3/k;->n(Ljava/lang/Object;I)I

    move-result v0

    iget-object v1, p0, Lm3/a;->u:Landroid/content/res/Resources$Theme;

    invoke-static {v1, v0}, Lq3/k;->n(Ljava/lang/Object;I)I

    move-result v0

    return v0
.end method

.method public i(Ld3/m;)Lm3/a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ld3/m;",
            ")TT;"
        }
    .end annotation

    sget-object v0, Ld3/m;->h:Lu2/g;

    invoke-static {p1}, Lq3/j;->d(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {p0, v0, p1}, Lm3/a;->c0(Lu2/g;Ljava/lang/Object;)Lm3/a;

    move-result-object p1

    return-object p1
.end method

.method public i0(Lu2/l;)Lm3/a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lu2/l<",
            "Landroid/graphics/Bitmap;",
            ">;)TT;"
        }
    .end annotation

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lm3/a;->j0(Lu2/l;Z)Lm3/a;

    move-result-object p1

    return-object p1
.end method

.method public final j()Lw2/j;
    .locals 1

    iget-object v0, p0, Lm3/a;->c:Lw2/j;

    return-object v0
.end method

.method j0(Lu2/l;Z)Lm3/a;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lu2/l<",
            "Landroid/graphics/Bitmap;",
            ">;Z)TT;"
        }
    .end annotation

    goto/32 :goto_b

    nop

    :goto_0
    return-object p1

    :goto_1
    const-class v1, Landroid/graphics/drawable/Drawable;

    goto/32 :goto_d

    nop

    :goto_2
    const-class v1, Landroid/graphics/drawable/BitmapDrawable;

    goto/32 :goto_8

    nop

    :goto_3
    const-class v1, Landroid/graphics/Bitmap;

    goto/32 :goto_5

    nop

    :goto_4
    invoke-direct {v0, p1, p2}, Ld3/p;-><init>(Lu2/l;Z)V

    goto/32 :goto_3

    nop

    :goto_5
    invoke-virtual {p0, v1, p1, p2}, Lm3/a;->h0(Ljava/lang/Class;Lu2/l;Z)Lm3/a;

    goto/32 :goto_1

    nop

    :goto_6
    const-class v0, Lh3/c;

    goto/32 :goto_f

    nop

    :goto_7
    invoke-virtual {p0, v1, v0, p2}, Lm3/a;->h0(Ljava/lang/Class;Lu2/l;Z)Lm3/a;

    goto/32 :goto_6

    nop

    :goto_8
    invoke-virtual {v0}, Ld3/p;->c()Lu2/l;

    move-result-object v0

    goto/32 :goto_7

    nop

    :goto_9
    invoke-virtual {p0}, Lm3/a;->b0()Lm3/a;

    move-result-object p1

    goto/32 :goto_0

    nop

    :goto_a
    new-instance v0, Ld3/p;

    goto/32 :goto_4

    nop

    :goto_b
    iget-boolean v0, p0, Lm3/a;->v:Z

    goto/32 :goto_c

    nop

    :goto_c
    if-nez v0, :cond_0

    goto/32 :goto_14

    :cond_0
    goto/32 :goto_11

    nop

    :goto_d
    invoke-virtual {p0, v1, v0, p2}, Lm3/a;->h0(Ljava/lang/Class;Lu2/l;Z)Lm3/a;

    goto/32 :goto_2

    nop

    :goto_e
    invoke-virtual {v0, p1, p2}, Lm3/a;->j0(Lu2/l;Z)Lm3/a;

    move-result-object p1

    goto/32 :goto_13

    nop

    :goto_f
    new-instance v1, Lh3/f;

    goto/32 :goto_12

    nop

    :goto_10
    invoke-virtual {p0, v0, v1, p2}, Lm3/a;->h0(Ljava/lang/Class;Lu2/l;Z)Lm3/a;

    goto/32 :goto_9

    nop

    :goto_11
    invoke-virtual {p0}, Lm3/a;->f()Lm3/a;

    move-result-object v0

    goto/32 :goto_e

    nop

    :goto_12
    invoke-direct {v1, p1}, Lh3/f;-><init>(Lu2/l;)V

    goto/32 :goto_10

    nop

    :goto_13
    return-object p1

    :goto_14
    goto/32 :goto_a

    nop
.end method

.method public final k()I
    .locals 1

    iget v0, p0, Lm3/a;->f:I

    return v0
.end method

.method public k0(Z)Lm3/a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)TT;"
        }
    .end annotation

    iget-boolean v0, p0, Lm3/a;->v:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lm3/a;->f()Lm3/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lm3/a;->k0(Z)Lm3/a;

    move-result-object p1

    return-object p1

    :cond_0
    iput-boolean p1, p0, Lm3/a;->z:Z

    iget p1, p0, Lm3/a;->a:I

    const/high16 v0, 0x100000

    or-int/2addr p1, v0

    iput p1, p0, Lm3/a;->a:I

    invoke-virtual {p0}, Lm3/a;->b0()Lm3/a;

    move-result-object p1

    return-object p1
.end method

.method public final l()Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Lm3/a;->e:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public final m()Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Lm3/a;->o:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public final n()I
    .locals 1

    iget v0, p0, Lm3/a;->p:I

    return v0
.end method

.method public final o()Z
    .locals 1

    iget-boolean v0, p0, Lm3/a;->x:Z

    return v0
.end method

.method public final p()Lu2/h;
    .locals 1

    iget-object v0, p0, Lm3/a;->q:Lu2/h;

    return-object v0
.end method

.method public final q()I
    .locals 1

    iget v0, p0, Lm3/a;->j:I

    return v0
.end method

.method public final r()I
    .locals 1

    iget v0, p0, Lm3/a;->k:I

    return v0
.end method

.method public final s()Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Lm3/a;->g:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public final t()I
    .locals 1

    iget v0, p0, Lm3/a;->h:I

    return v0
.end method

.method public final u()Lcom/bumptech/glide/g;
    .locals 1

    iget-object v0, p0, Lm3/a;->d:Lcom/bumptech/glide/g;

    return-object v0
.end method

.method public final v()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    iget-object v0, p0, Lm3/a;->s:Ljava/lang/Class;

    return-object v0
.end method

.method public final w()Lu2/f;
    .locals 1

    iget-object v0, p0, Lm3/a;->l:Lu2/f;

    return-object v0
.end method

.method public final x()F
    .locals 1

    iget v0, p0, Lm3/a;->b:F

    return v0
.end method

.method public final y()Landroid/content/res/Resources$Theme;
    .locals 1

    iget-object v0, p0, Lm3/a;->u:Landroid/content/res/Resources$Theme;

    return-object v0
.end method

.method public final z()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lu2/l<",
            "*>;>;"
        }
    .end annotation

    iget-object v0, p0, Lm3/a;->r:Ljava/util/Map;

    return-object v0
.end method
