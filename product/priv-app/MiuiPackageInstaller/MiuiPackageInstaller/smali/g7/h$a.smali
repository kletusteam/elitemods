.class public Lg7/h$a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lg7/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lg7/h$b;)Lg7/h;
    .locals 3

    invoke-static {}, Lg7/p;->a()Lg7/p$a;

    move-result-object v0

    invoke-static {}, Lg7/j;->a()Lg7/j$c;

    move-result-object v1

    new-instance v2, Lg7/h$a;

    invoke-direct {v2}, Lg7/h$a;-><init>()V

    invoke-virtual {v2, p1, v0, v1}, Lg7/h$a;->b(Lg7/h$b;Lg7/p$a;Lg7/j$c;)Lg7/h;

    move-result-object p1

    return-object p1
.end method

.method b(Lg7/h$b;Lg7/p$a;Lg7/j$c;)Lg7/h;
    .locals 5

    goto/32 :goto_1b

    nop

    :goto_0
    invoke-static {p1, v3}, Landroid/util/Base64;->encode([BI)[B

    move-result-object p1

    :try_start_0
    new-instance p3, Ljava/lang/String;

    invoke-direct {p3, p1, v1}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_2

    goto/32 :goto_24

    nop

    :goto_1
    goto/16 :goto_2a

    :goto_2
    goto/32 :goto_29

    nop

    :goto_3
    invoke-interface {p3}, Lg7/j$c;->b()Z

    move-result v3

    goto/32 :goto_11

    nop

    :goto_4
    invoke-direct {p1, p2, p3}, Lg7/h;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto/32 :goto_1f

    nop

    :goto_5
    new-instance p1, Ljava/lang/IllegalArgumentException;

    goto/32 :goto_28

    nop

    :goto_6
    if-eqz p2, :cond_0

    goto/32 :goto_1e

    :cond_0
    goto/32 :goto_1d

    nop

    :goto_7
    throw p1

    :goto_8
    invoke-static {v0, p1}, Lg7/b;->k(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/32 :goto_17

    nop

    :goto_9
    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_7

    nop

    :goto_a
    invoke-static {v0, p1}, Lg7/b;->k(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/32 :goto_16

    nop

    :goto_b
    const-string p1, "n"

    goto/32 :goto_1

    nop

    :goto_c
    if-nez p1, :cond_1

    goto/32 :goto_19

    :cond_1
    goto/32 :goto_21

    nop

    :goto_d
    return-object v2

    :goto_e
    goto/32 :goto_0

    nop

    :goto_f
    return-object v2

    :goto_10
    goto/32 :goto_1a

    nop

    :goto_11
    if-eqz v3, :cond_2

    goto/32 :goto_10

    :cond_2
    goto/32 :goto_f

    nop

    :goto_12
    return-object v2

    :goto_13
    goto/32 :goto_3

    nop

    :goto_14
    if-eqz p3, :cond_3

    goto/32 :goto_13

    :cond_3
    goto/32 :goto_12

    nop

    :goto_15
    if-eq p1, v3, :cond_4

    goto/32 :goto_2

    :cond_4
    goto/32 :goto_b

    nop

    :goto_16
    return-object v2

    :catch_0
    move-exception p1

    goto/32 :goto_8

    nop

    :goto_17
    return-object v2

    :catch_1
    move-exception p1

    goto/32 :goto_26

    nop

    :goto_18
    return-object v2

    :goto_19
    goto/32 :goto_5

    nop

    :goto_1a
    sget-object v3, Lg7/h$b;->a:Lg7/h$b;

    goto/32 :goto_15

    nop

    :goto_1b
    const-string v0, "FidNonce"

    goto/32 :goto_25

    nop

    :goto_1c
    if-eqz p1, :cond_5

    goto/32 :goto_e

    :cond_5
    goto/32 :goto_d

    nop

    :goto_1d
    return-object v2

    :goto_1e
    goto/32 :goto_14

    nop

    :goto_1f
    return-object p1

    :catch_2
    move-exception p1

    goto/32 :goto_a

    nop

    :goto_20
    invoke-virtual {p0}, Lg7/h$a;->e()Ljava/lang/String;

    move-result-object v3

    goto/32 :goto_23

    nop

    :goto_21
    const/4 v2, 0x0

    goto/32 :goto_6

    nop

    :goto_22
    invoke-virtual {p0, v3, v4}, Lg7/h$a;->d(J)Ljava/lang/String;

    move-result-object p2

    goto/32 :goto_20

    nop

    :goto_23
    invoke-virtual {p0, p1, p2, v3}, Lg7/h$a;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    :try_start_1
    invoke-virtual {p1, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object p2

    const/16 v3, 0xa

    invoke-static {p2, v3}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object p2
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1

    :try_start_2
    invoke-virtual {p1, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object p1

    invoke-interface {p3, p1}, Lg7/j$c;->a([B)[B

    move-result-object p1
    :try_end_2
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2 .. :try_end_2} :catch_0

    goto/32 :goto_1c

    nop

    :goto_24
    new-instance p1, Lg7/h;

    goto/32 :goto_4

    nop

    :goto_25
    const-string v1, "UTF-8"

    goto/32 :goto_c

    nop

    :goto_26
    invoke-static {v0, p1}, Lg7/b;->k(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/32 :goto_18

    nop

    :goto_27
    invoke-interface {p2}, Lg7/p$a;->a()J

    move-result-wide v3

    goto/32 :goto_22

    nop

    :goto_28
    const-string p2, "type == null"

    goto/32 :goto_9

    nop

    :goto_29
    const-string p1, "wb"

    :goto_2a
    goto/32 :goto_27

    nop
.end method

.method c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v1, "tp"

    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string p1, "nonce"

    invoke-virtual {v0, p1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string p1, "v"

    invoke-virtual {v0, p1, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_1

    nop

    :goto_0
    new-instance p1, Ljava/lang/IllegalStateException;

    goto/32 :goto_4

    nop

    :goto_1
    return-object p1

    :catch_0
    goto/32 :goto_0

    nop

    :goto_2
    throw p1

    :goto_3
    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_2

    nop

    :goto_4
    const-string p2, "should not happen"

    goto/32 :goto_3

    nop
.end method

.method d(J)Ljava/lang/String;
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-object p1

    :goto_1
    invoke-static {p1, p2}, Lg7/m;->a(J)Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_0

    nop
.end method

.method e()Ljava/lang/String;
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return-object v0

    :goto_1
    invoke-static {}, Lg7/r;->a()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_0

    nop
.end method
