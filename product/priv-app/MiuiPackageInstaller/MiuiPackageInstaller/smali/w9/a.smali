.class public Lw9/a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lw9/a$a;
    }
.end annotation


# static fields
.field private static a:Lx9/b;


# direct methods
.method public static a()V
    .locals 1

    sget-object v0, Lw9/a;->a:Lx9/b;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lx9/b;->a()V

    :cond_0
    return-void
.end method

.method public static b(Landroid/view/View;Landroid/view/View;Lw9/a$a;)V
    .locals 1

    sget-object v0, Lw9/a;->a:Lx9/b;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lia/e;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lia/e;->b()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lx9/c;

    invoke-direct {v0}, Lx9/c;-><init>()V

    goto :goto_0

    :cond_0
    new-instance v0, Lx9/d;

    invoke-direct {v0}, Lx9/d;-><init>()V

    :goto_0
    sput-object v0, Lw9/a;->a:Lx9/b;

    :cond_1
    sget-object v0, Lw9/a;->a:Lx9/b;

    invoke-interface {v0, p0, p1, p2}, Lx9/b;->b(Landroid/view/View;Landroid/view/View;Lw9/a$a;)V

    const/4 p0, 0x0

    sput-object p0, Lw9/a;->a:Lx9/b;

    return-void
.end method

.method public static c(Landroid/view/View;Landroid/view/View;ZLmiuix/appcompat/app/i$d;)V
    .locals 1

    sget-object v0, Lw9/a;->a:Lx9/b;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lia/e;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lia/e;->b()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lx9/c;

    invoke-direct {v0}, Lx9/c;-><init>()V

    goto :goto_0

    :cond_0
    new-instance v0, Lx9/d;

    invoke-direct {v0}, Lx9/d;-><init>()V

    :goto_0
    sput-object v0, Lw9/a;->a:Lx9/b;

    :cond_1
    sget-object v0, Lw9/a;->a:Lx9/b;

    invoke-interface {v0, p0, p1, p2, p3}, Lx9/b;->c(Landroid/view/View;Landroid/view/View;ZLmiuix/appcompat/app/i$d;)V

    return-void
.end method
