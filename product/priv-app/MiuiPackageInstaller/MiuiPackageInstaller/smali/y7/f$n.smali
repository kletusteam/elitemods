.class public Ly7/f$n;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ly7/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "n"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ly7/f$n$a;
    }
.end annotation


# instance fields
.field public final a:I

.field public final b:Landroid/graphics/drawable/Drawable;

.field public final c:I

.field public final d:I

.field public final e:I

.field public final f:Landroid/graphics/Rect;

.field public final g:Landroid/graphics/Rect;


# direct methods
.method private constructor <init>(ILandroid/graphics/drawable/Drawable;IIILandroid/graphics/Rect;Landroid/graphics/Rect;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Ly7/f$n;->a:I

    iput-object p2, p0, Ly7/f$n;->b:Landroid/graphics/drawable/Drawable;

    iput p3, p0, Ly7/f$n;->c:I

    iput p4, p0, Ly7/f$n;->d:I

    iput p5, p0, Ly7/f$n;->e:I

    iput-object p6, p0, Ly7/f$n;->f:Landroid/graphics/Rect;

    iput-object p7, p0, Ly7/f$n;->g:Landroid/graphics/Rect;

    return-void
.end method

.method synthetic constructor <init>(ILandroid/graphics/drawable/Drawable;IIILandroid/graphics/Rect;Landroid/graphics/Rect;Ly7/f$a;)V
    .locals 0

    invoke-direct/range {p0 .. p7}, Ly7/f$n;-><init>(ILandroid/graphics/drawable/Drawable;IIILandroid/graphics/Rect;Landroid/graphics/Rect;)V

    return-void
.end method


# virtual methods
.method public a()Z
    .locals 1

    iget v0, p0, Ly7/f$n;->d:I

    if-gtz v0, :cond_1

    iget v0, p0, Ly7/f$n;->e:I

    if-lez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method
