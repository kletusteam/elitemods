.class Ly7/f$b;
.super Lw7/a$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Ly7/f;->g0(Ljava/lang/String;)Lw7/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lw7/a$b<",
        "Lz7/d;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Ly7/f;


# direct methods
.method constructor <init>(Ly7/f;)V
    .locals 0

    iput-object p1, p0, Ly7/f$b;->a:Ly7/f;

    invoke-direct {p0}, Lw7/a$b;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lw7/a;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lw7/a<",
            "Lz7/d;",
            ">;)V"
        }
    .end annotation

    :try_start_0
    invoke-virtual {p1}, Ljava/util/concurrent/FutureTask;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lz7/d;

    if-eqz p1, :cond_0

    iget-object v0, p0, Ly7/f$b;->a:Ly7/f;

    invoke-virtual {p1}, Lz7/d;->a()I

    move-result v1

    invoke-static {v0, v1}, Ly7/f;->M(Ly7/f;I)I

    iget-object v0, p0, Ly7/f$b;->a:Ly7/f;

    invoke-virtual {p1}, Lz7/d;->b()I

    move-result p1

    invoke-static {v0, p1}, Ly7/f;->O(Ly7/f;I)I

    iget-object p1, p0, Ly7/f$b;->a:Ly7/f;

    invoke-static {p1}, Ly7/f;->Q(Ly7/f;)Lz7/k;

    move-result-object p1

    const-string v0, "lastDownloadTime"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lz7/k;->d(Ljava/lang/String;J)V

    iget-object p1, p0, Ly7/f$b;->a:Ly7/f;

    invoke-static {p1}, Ly7/f;->Q(Ly7/f;)Lz7/k;

    move-result-object p1

    const-string v0, "frequency"

    iget-object v1, p0, Ly7/f$b;->a:Ly7/f;

    invoke-static {v1}, Ly7/f;->L(Ly7/f;)I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lz7/k;->c(Ljava/lang/String;I)V

    iget-object p1, p0, Ly7/f$b;->a:Ly7/f;

    invoke-static {p1}, Ly7/f;->Q(Ly7/f;)Lz7/k;

    move-result-object p1

    const-string v0, "maxduration"

    iget-object v1, p0, Ly7/f$b;->a:Ly7/f;

    invoke-static {v1}, Ly7/f;->N(Ly7/f;)I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lz7/k;->c(Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/util/concurrent/ExecutionException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/InterruptedException;->printStackTrace()V

    :cond_0
    :goto_0
    return-void
.end method
