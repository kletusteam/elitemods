.class Ly7/f$e;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Ly7/f;->t0(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Ly7/f;


# direct methods
.method constructor <init>(Ly7/f;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Ly7/f$e;->b:Ly7/f;

    iput-object p2, p0, Ly7/f$e;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    iget-object v0, p0, Ly7/f$e;->b:Ly7/f;

    invoke-static {v0}, Ly7/f;->S(Ly7/f;)Landroid/app/Activity;

    move-result-object v2

    if-nez v2, :cond_0

    return-void

    :cond_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "_modifyStyle"

    const-string v3, "true"

    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Ly7/f$e;->a:Ljava/lang/String;

    invoke-static {v1, v0}, Ly7/f;->T(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x2

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-ne v1, v3, :cond_1

    move v6, v4

    goto :goto_0

    :cond_1
    move v6, v5

    :goto_0
    iget-object v1, p0, Ly7/f$e;->b:Ly7/f;

    if-eqz v6, :cond_2

    invoke-static {v1}, Ly7/f;->U(Ly7/f;)Ly7/f$n;

    move-result-object v1

    goto :goto_1

    :cond_2
    invoke-static {v1}, Ly7/f;->V(Ly7/f;)Ly7/f$n;

    move-result-object v1

    :goto_1
    move-object v7, v1

    iget-object v1, p0, Ly7/f$e;->b:Ly7/f;

    invoke-static {v1}, Ly7/f;->W(Ly7/f;)Landroid/view/View;

    move-result-object v1

    const/4 v8, 0x0

    if-nez v1, :cond_3

    iget-object v1, p0, Ly7/f$e;->b:Ly7/f;

    invoke-virtual {v2}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v9

    sget v10, Ly7/d;->a:I

    invoke-virtual {v9, v10, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v9

    invoke-static {v1, v9}, Ly7/f;->X(Ly7/f;Landroid/view/View;)Landroid/view/View;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v9, -0x1

    const/4 v10, -0x2

    invoke-direct {v1, v9, v10}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    const/16 v9, 0x11

    iput v9, v1, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    iget-object v9, p0, Ly7/f$e;->b:Ly7/f;

    invoke-static {v9}, Ly7/f;->W(Ly7/f;)Landroid/view/View;

    move-result-object v9

    invoke-virtual {v9, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_3
    iget-object v1, p0, Ly7/f$e;->b:Ly7/f;

    invoke-static {v1}, Ly7/f;->c(Ly7/f;)Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_4

    iget-object v1, p0, Ly7/f$e;->b:Ly7/f;

    invoke-static {v1}, Ly7/f;->W(Ly7/f;)Landroid/view/View;

    move-result-object v9

    sget v10, Ly7/c;->d:I

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    invoke-static {v1, v9}, Ly7/f;->d(Ly7/f;Landroid/view/View;)Landroid/view/View;

    :cond_4
    iget-object v1, p0, Ly7/f$e;->b:Ly7/f;

    invoke-static {v1}, Ly7/f;->c(Ly7/f;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v7}, Ly7/f$n;->a()Z

    move-result v9

    if-eqz v9, :cond_5

    move v9, v5

    goto :goto_2

    :cond_5
    const/16 v9, 0x8

    :goto_2
    invoke-virtual {v1, v9}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Ly7/f$e;->b:Ly7/f;

    invoke-static {v1}, Ly7/f;->e(Ly7/f;)Landroid/webkit/WebView;

    move-result-object v1

    if-nez v1, :cond_6

    iget-object v1, p0, Ly7/f$e;->b:Ly7/f;

    invoke-static {v1}, Ly7/f;->W(Ly7/f;)Landroid/view/View;

    move-result-object v9

    sget v10, Ly7/c;->c:I

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/webkit/WebView;

    invoke-static {v1, v9}, Ly7/f;->f(Ly7/f;Landroid/webkit/WebView;)Landroid/webkit/WebView;

    :cond_6
    iget-object v1, p0, Ly7/f$e;->b:Ly7/f;

    invoke-static {v1}, Ly7/f;->g(Ly7/f;)Landroid/widget/LinearLayout;

    move-result-object v1

    if-nez v1, :cond_7

    iget-object v1, p0, Ly7/f$e;->b:Ly7/f;

    invoke-static {v1}, Ly7/f;->W(Ly7/f;)Landroid/view/View;

    move-result-object v9

    sget v10, Ly7/c;->b:I

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/LinearLayout;

    invoke-static {v1, v9}, Ly7/f;->h(Ly7/f;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;

    :cond_7
    iget-object v1, p0, Ly7/f$e;->b:Ly7/f;

    invoke-static {v1}, Ly7/f;->i(Ly7/f;)Landroid/app/AlertDialog;

    move-result-object v1

    if-eqz v1, :cond_8

    iget-object v1, p0, Ly7/f$e;->b:Ly7/f;

    invoke-static {v1}, Ly7/f;->i(Ly7/f;)Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V

    iget-object v1, p0, Ly7/f$e;->b:Ly7/f;

    invoke-static {v1, v8}, Ly7/f;->j(Ly7/f;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    :cond_8
    new-instance v1, Landroid/app/AlertDialog$Builder;

    const v8, 0x103023a

    invoke-direct {v1, v2, v8}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v8

    iget v8, v8, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/2addr v3, v8

    if-eqz v3, :cond_9

    invoke-static {v4}, Landroid/webkit/WebView;->setWebContentsDebuggingEnabled(Z)V

    :cond_9
    iget-object v3, p0, Ly7/f$e;->b:Ly7/f;

    invoke-static {v3}, Ly7/f;->e(Ly7/f;)Landroid/webkit/WebView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/webkit/WebSettings;->setSupportMultipleWindows(Z)V

    invoke-virtual {v3, v4}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    invoke-virtual {v3, v4}, Landroid/webkit/WebSettings;->setUseWideViewPort(Z)V

    iget-object v4, p0, Ly7/f$e;->b:Ly7/f;

    invoke-static {v4, v2}, Ly7/f;->k(Ly7/f;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/webkit/WebSettings;->setUserAgentString(Ljava/lang/String;)V

    iget-object v3, p0, Ly7/f$e;->b:Ly7/f;

    invoke-static {v3}, Ly7/f;->e(Ly7/f;)Landroid/webkit/WebView;

    move-result-object v3

    new-instance v4, Ly7/f$e$a;

    invoke-direct {v4, p0}, Ly7/f$e$a;-><init>(Ly7/f$e;)V

    invoke-virtual {v3, v4}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    iget-object v3, p0, Ly7/f$e;->b:Ly7/f;

    invoke-static {v3}, Ly7/f;->e(Ly7/f;)Landroid/webkit/WebView;

    move-result-object v3

    new-instance v4, Ly7/f$e$b;

    invoke-direct {v4, p0}, Ly7/f$e$b;-><init>(Ly7/f$e;)V

    invoke-virtual {v3, v4}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    iget-object v3, p0, Ly7/f$e;->b:Ly7/f;

    invoke-static {v3}, Ly7/f;->W(Ly7/f;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    if-eqz v3, :cond_a

    invoke-virtual {v3}, Landroid/view/ViewGroup;->removeAllViews()V

    :cond_a
    iget-object v3, p0, Ly7/f$e;->b:Ly7/f;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-static {v3, v1}, Ly7/f;->j(Ly7/f;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    iget-object v1, p0, Ly7/f$e;->b:Ly7/f;

    invoke-static {v1}, Ly7/f;->i(Ly7/f;)Landroid/app/AlertDialog;

    move-result-object v1

    iget-object v3, p0, Ly7/f$e;->b:Ly7/f;

    invoke-static {v3}, Ly7/f;->W(Ly7/f;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog;->setView(Landroid/view/View;)V

    iget-object v1, p0, Ly7/f$e;->b:Ly7/f;

    invoke-static {v1}, Ly7/f;->i(Ly7/f;)Landroid/app/AlertDialog;

    move-result-object v1

    iget-object v3, p0, Ly7/f$e;->b:Ly7/f;

    invoke-static {v3}, Ly7/f;->t(Ly7/f;)Landroid/content/DialogInterface$OnKeyListener;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    iget-object v1, p0, Ly7/f$e;->b:Ly7/f;

    invoke-static {v1}, Ly7/f;->i(Ly7/f;)Landroid/app/AlertDialog;

    move-result-object v1

    iget-object v3, p0, Ly7/f$e;->b:Ly7/f;

    invoke-static {v3}, Ly7/f;->u(Ly7/f;)Landroid/content/DialogInterface$OnDismissListener;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    iget-object v1, p0, Ly7/f$e;->b:Ly7/f;

    invoke-static {v1}, Ly7/f;->i(Ly7/f;)Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    iget-object v1, p0, Ly7/f$e;->b:Ly7/f;

    invoke-static {v1}, Ly7/f;->e(Ly7/f;)Landroid/webkit/WebView;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/webkit/WebView;->setBackgroundColor(I)V

    iget-object v1, p0, Ly7/f$e;->b:Ly7/f;

    invoke-static {v1}, Ly7/f;->e(Ly7/f;)Landroid/webkit/WebView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/webkit/WebView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    iget-object v3, v7, Ly7/f$n;->g:Landroid/graphics/Rect;

    if-eqz v3, :cond_b

    iget v4, v3, Landroid/graphics/Rect;->left:I

    iget v5, v3, Landroid/graphics/Rect;->top:I

    iget v8, v3, Landroid/graphics/Rect;->right:I

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v1, v4, v5, v8, v3}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    goto :goto_3

    :cond_b
    invoke-virtual {v1, v5, v5, v5, v5}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    :goto_3
    iget-object v3, p0, Ly7/f$e;->b:Ly7/f;

    invoke-static {v3}, Ly7/f;->e(Ly7/f;)Landroid/webkit/WebView;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/webkit/WebView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Ly7/f$e;->b:Ly7/f;

    invoke-static {v1}, Ly7/f;->e(Ly7/f;)Landroid/webkit/WebView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    iget-object v1, p0, Ly7/f$e;->b:Ly7/f;

    invoke-static {v1}, Ly7/f;->W(Ly7/f;)Landroid/view/View;

    move-result-object v0

    sget v3, Ly7/c;->a:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iget-object v0, p0, Ly7/f$e;->b:Ly7/f;

    invoke-static {v0}, Ly7/f;->i(Ly7/f;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v4

    move-object v5, v7

    invoke-static/range {v1 .. v6}, Ly7/f;->v(Ly7/f;Landroid/app/Activity;Landroid/view/View;Landroid/view/Window;Ly7/f$n;Z)V

    return-void
.end method
