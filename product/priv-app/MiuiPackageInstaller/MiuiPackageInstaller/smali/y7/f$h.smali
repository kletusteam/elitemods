.class Ly7/f$h;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Ly7/f;->b0()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ly7/f;


# direct methods
.method constructor <init>(Ly7/f;)V
    .locals 0

    iput-object p1, p0, Ly7/f$h;->a:Ly7/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    const-string v0, "env"

    iget-object v1, p0, Ly7/f$h;->a:Ly7/f;

    invoke-static {v1}, Ly7/f;->x(Ly7/f;)Lz7/j;

    move-result-object v1

    invoke-virtual {v1}, Lz7/j;->p()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Ly7/f$h;->a:Ly7/f;

    invoke-static {v1}, Ly7/f;->x(Ly7/f;)Lz7/j;

    move-result-object v1

    invoke-virtual {v1}, Lz7/j;->g()V

    :cond_0
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    iget-object v2, p0, Ly7/f$h;->a:Ly7/f;

    invoke-static {v2}, Ly7/f;->x(Ly7/f;)Lz7/j;

    move-result-object v2

    invoke-virtual {v2}, Lz7/j;->p()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    const-string v3, "voiceover"

    iget-object v4, p0, Ly7/f$h;->a:Ly7/f;

    invoke-static {v4}, Ly7/f;->y(Ly7/f;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x1

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    :goto_0
    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "force"

    iget-object v2, p0, Ly7/f$h;->a:Ly7/f;

    invoke-static {v2}, Ly7/f;->z(Ly7/f;)Z

    move-result v2

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    iget-object v0, p0, Ly7/f$h;->a:Ly7/f;

    invoke-static {v0}, Ly7/f;->A(Ly7/f;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    if-eqz v0, :cond_2

    const-string v2, "talkBack"

    invoke-static {v0}, Lz7/l;->a(Landroid/content/Context;)Z

    move-result v0

    invoke-virtual {v1, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    :cond_2
    const-string v0, "uid"

    iget-object v2, p0, Ly7/f$h;->a:Ly7/f;

    invoke-static {v2}, Ly7/f;->B(Ly7/f;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "version"

    const-string v2, "2.0"

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "scene"

    iget-object v2, p0, Ly7/f$h;->a:Ly7/f;

    invoke-static {v2}, Ly7/f;->D(Ly7/f;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    new-instance v2, Ljava/security/SecureRandom;

    invoke-direct {v2}, Ljava/security/SecureRandom;-><init>()V

    invoke-virtual {v2}, Ljava/security/SecureRandom;->nextLong()J

    move-result-wide v2

    const-string v4, "r"

    invoke-virtual {v0, v4, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v2, "t"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    const-wide/16 v5, 0x3e8

    div-long/2addr v3, v5

    invoke-virtual {v0, v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v2, "nonce"

    invoke-virtual {v1, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    iget-object v0, p0, Ly7/f$h;->a:Ly7/f;

    invoke-static {v0}, Ly7/f;->x(Ly7/f;)Lz7/j;

    move-result-object v0

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lz7/j;->I(Ljava/lang/String;)V

    iget-object v0, p0, Ly7/f$h;->a:Ly7/f;

    invoke-static {v0}, Ly7/f;->x(Ly7/f;)Lz7/j;

    move-result-object v1

    iget-object v0, p0, Ly7/f$h;->a:Ly7/f;

    invoke-static {v0}, Ly7/f;->x(Ly7/f;)Lz7/j;

    move-result-object v0

    invoke-virtual {v0}, Lz7/j;->p()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Ly7/f$h;->a:Ly7/f;

    invoke-static {v0}, Ly7/f;->E(Ly7/f;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Ly7/f$h;->a:Ly7/f;

    invoke-static {v0}, Ly7/f;->D(Ly7/f;)Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Ly7/f$h;->a:Ly7/f;

    invoke-static {v0}, Ly7/f;->p(Ly7/f;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    iget-object v0, p0, Ly7/f$h;->a:Ly7/f;

    invoke-static {v0}, Ly7/f;->F(Ly7/f;)Ljava/lang/String;

    move-result-object v6

    iget-object v0, p0, Ly7/f$h;->a:Ly7/f;

    invoke-static {v0}, Ly7/f;->G(Ly7/f;)Ljava/lang/String;

    move-result-object v7

    iget-object v0, p0, Ly7/f$h;->a:Ly7/f;

    invoke-static {v0}, Ly7/f;->H(Ly7/f;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    new-instance v9, Ly7/f$h$a;

    invoke-direct {v9, p0}, Ly7/f$h$a;-><init>(Ly7/f$h;)V

    invoke-virtual/range {v1 .. v9}, Lz7/j;->M(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ly7/f$m;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    iget-object v1, p0, Ly7/f$h;->a:Ly7/f;

    sget-object v2, Lz7/g$a;->c:Lz7/g$a;

    invoke-virtual {v2}, Lz7/g$a;->a()I

    move-result v3

    invoke-static {v2}, Lz7/g;->a(Lz7/g$a;)I

    move-result v4

    invoke-static {v1, v3, v4}, Ly7/f;->I(Ly7/f;II)V

    invoke-virtual {v2}, Lz7/g$a;->a()I

    move-result v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "registere:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lorg/json/JSONException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Ly7/f;->h0(ILjava/lang/String;)Lz7/n;

    move-result-object v0

    iget-object v1, p0, Ly7/f$h;->a:Ly7/f;

    invoke-static {v1}, Ly7/f;->C(Ly7/f;)Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Ly7/f$h$b;

    invoke-direct {v2, p0, v0}, Ly7/f$h$b;-><init>(Ly7/f$h;Lz7/n;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :goto_1
    return-void
.end method
