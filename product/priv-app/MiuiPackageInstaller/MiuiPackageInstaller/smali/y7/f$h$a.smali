.class Ly7/f$h$a;
.super Ljava/lang/Object;

# interfaces
.implements Ly7/f$m;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Ly7/f$h;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ly7/f$h;


# direct methods
.method constructor <init>(Ly7/f$h;)V
    .locals 0

    iput-object p1, p0, Ly7/f$h$a;->a:Ly7/f$h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lz7/p;)V
    .locals 2

    iget-object v0, p0, Ly7/f$h$a;->a:Ly7/f$h;

    iget-object v0, v0, Ly7/f$h;->a:Ly7/f;

    invoke-static {v0}, Ly7/f;->m(Ly7/f;)V

    iget-object v0, p0, Ly7/f$h$a;->a:Ly7/f$h;

    iget-object v0, v0, Ly7/f$h;->a:Ly7/f;

    invoke-static {v0}, Ly7/f;->C(Ly7/f;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Ly7/f$h$a$a;

    invoke-direct {v1, p0, p1}, Ly7/f$h$a$a;-><init>(Ly7/f$h$a;Lz7/p;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public b(Lz7/n;)V
    .locals 3

    iget-object v0, p0, Ly7/f$h$a;->a:Ly7/f$h;

    iget-object v0, v0, Ly7/f$h;->a:Ly7/f;

    invoke-virtual {p1}, Lz7/n;->a()I

    move-result v1

    invoke-virtual {p1}, Lz7/n;->b()I

    move-result v2

    invoke-static {v0, v1, v2}, Ly7/f;->I(Ly7/f;II)V

    iget-object v0, p0, Ly7/f$h$a;->a:Ly7/f$h;

    iget-object v0, v0, Ly7/f$h;->a:Ly7/f;

    invoke-static {v0}, Ly7/f;->C(Ly7/f;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Ly7/f$h$a$b;

    invoke-direct {v1, p0, p1}, Ly7/f$h$a$b;-><init>(Ly7/f$h$a;Lz7/n;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Ly7/f$h$a;->a:Ly7/f$h;

    iget-object v0, v0, Ly7/f$h;->a:Ly7/f;

    invoke-static {v0}, Ly7/f;->J(Ly7/f;)Ly7/f$l;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ly7/f$h$a;->a:Ly7/f$h;

    iget-object v0, v0, Ly7/f$h;->a:Ly7/f;

    invoke-static {v0}, Ly7/f;->J(Ly7/f;)Ly7/f$l;

    move-result-object v0

    invoke-interface {v0}, Ly7/f$l;->a()V

    :cond_0
    iget-object v0, p0, Ly7/f$h$a;->a:Ly7/f$h;

    iget-object v0, v0, Ly7/f$h;->a:Ly7/f;

    invoke-static {v0, p1}, Ly7/f;->o(Ly7/f;Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Ly7/f$h$a;->a:Ly7/f$h;

    iget-object v0, v0, Ly7/f$h;->a:Ly7/f;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ly7/f;->q(Ly7/f;Z)Z

    iget-object v0, p0, Ly7/f$h$a;->a:Ly7/f$h;

    iget-object v0, v0, Ly7/f$h;->a:Ly7/f;

    invoke-static {v0}, Ly7/f;->C(Ly7/f;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Ly7/f$h$a$c;

    invoke-direct {v1, p0, p1}, Ly7/f$h$a$c;-><init>(Ly7/f$h$a;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
