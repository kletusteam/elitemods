.class public Ly7/f;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ly7/f$n;,
        Ly7/f$l;,
        Ly7/f$o;,
        Ly7/f$m;
    }
.end annotation


# static fields
.field private static final F:Ljava/util/concurrent/ExecutorService;


# instance fields
.field private A:Z

.field private B:Landroid/view/View;

.field private final C:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private D:Landroid/content/DialogInterface$OnKeyListener;

.field private E:Landroid/content/DialogInterface$OnDismissListener;

.field private a:Lw7/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lw7/a<",
            "Lz7/d;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lz7/j;

.field private c:Ly7/f$o;

.field private d:Ly7/f$l;

.field private e:Landroid/view/View;

.field private f:Landroid/webkit/WebView;

.field private g:Landroid/widget/LinearLayout;

.field private h:Landroid/app/AlertDialog;

.field private i:Landroid/os/Handler;

.field private j:Ljava/lang/String;

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field private r:Ljava/lang/String;

.field private s:Ljava/lang/String;

.field private t:Z

.field private u:Ly7/f$n;

.field private v:Ly7/f$n;

.field private w:I

.field private x:I

.field private y:Lz7/k;

.field private z:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    sput-object v0, Ly7/f;->F:Ljava/util/concurrent/ExecutorService;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ly7/f$n$a;

    invoke-direct {v0}, Ly7/f$n$a;-><init>()V

    invoke-virtual {v0}, Ly7/f$n$a;->a()Ly7/f$n;

    move-result-object v0

    iput-object v0, p0, Ly7/f;->u:Ly7/f$n;

    new-instance v0, Ly7/f$n$a;

    invoke-direct {v0}, Ly7/f$n$a;-><init>()V

    invoke-virtual {v0}, Ly7/f$n$a;->a()Ly7/f$n;

    move-result-object v0

    iput-object v0, p0, Ly7/f;->v:Ly7/f$n;

    const/4 v0, 0x1

    iput-boolean v0, p0, Ly7/f;->A:Z

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Ly7/f;->C:Ljava/util/concurrent/atomic/AtomicBoolean;

    new-instance v0, Ly7/f$a;

    invoke-direct {v0, p0}, Ly7/f$a;-><init>(Ly7/f;)V

    iput-object v0, p0, Ly7/f;->D:Landroid/content/DialogInterface$OnKeyListener;

    new-instance v0, Ly7/f$d;

    invoke-direct {v0, p0}, Ly7/f$d;-><init>(Ly7/f;)V

    iput-object v0, p0, Ly7/f;->E:Landroid/content/DialogInterface$OnDismissListener;

    if-eqz p1, :cond_0

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Ly7/f;->i:Landroid/os/Handler;

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Ly7/f;->z:Ljava/lang/ref/WeakReference;

    new-instance v0, Lz7/j;

    invoke-virtual {p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lz7/j;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Ly7/f;->b:Lz7/j;

    new-instance v0, Lz7/k;

    const-string v1, "VerificationConfig"

    invoke-direct {v0, p1, v1}, Lz7/k;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Ly7/f;->y:Lz7/k;

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "activity  should not be null"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method static synthetic A(Ly7/f;)Ljava/lang/ref/WeakReference;
    .locals 0

    iget-object p0, p0, Ly7/f;->z:Ljava/lang/ref/WeakReference;

    return-object p0
.end method

.method static synthetic B(Ly7/f;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Ly7/f;->q:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic C(Ly7/f;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Ly7/f;->i:Landroid/os/Handler;

    return-object p0
.end method

.method static synthetic D(Ly7/f;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Ly7/f;->p:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic E(Ly7/f;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Ly7/f;->o:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic F(Ly7/f;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Ly7/f;->s:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic G(Ly7/f;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Ly7/f;->r:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic H(Ly7/f;)Z
    .locals 0

    iget-boolean p0, p0, Ly7/f;->t:Z

    return p0
.end method

.method static synthetic I(Ly7/f;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ly7/f;->s0(II)V

    return-void
.end method

.method static synthetic J(Ly7/f;)Ly7/f$l;
    .locals 0

    iget-object p0, p0, Ly7/f;->d:Ly7/f$l;

    return-object p0
.end method

.method static synthetic K(Ly7/f;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Ly7/f;->v0(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic L(Ly7/f;)I
    .locals 0

    iget p0, p0, Ly7/f;->x:I

    return p0
.end method

.method static synthetic M(Ly7/f;I)I
    .locals 0

    iput p1, p0, Ly7/f;->x:I

    return p1
.end method

.method static synthetic N(Ly7/f;)I
    .locals 0

    iget p0, p0, Ly7/f;->w:I

    return p0
.end method

.method static synthetic O(Ly7/f;I)I
    .locals 0

    iput p1, p0, Ly7/f;->w:I

    return p1
.end method

.method static synthetic P(Ly7/f;)Z
    .locals 0

    iget-boolean p0, p0, Ly7/f;->A:Z

    return p0
.end method

.method static synthetic Q(Ly7/f;)Lz7/k;
    .locals 0

    iget-object p0, p0, Ly7/f;->y:Lz7/k;

    return-object p0
.end method

.method static synthetic R(Ly7/f;Z)Z
    .locals 0

    iput-boolean p1, p0, Ly7/f;->A:Z

    return p1
.end method

.method static synthetic S(Ly7/f;)Landroid/app/Activity;
    .locals 0

    invoke-direct {p0}, Ly7/f;->f0()Landroid/app/Activity;

    move-result-object p0

    return-object p0
.end method

.method static synthetic T(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;
    .locals 0

    invoke-static {p0, p1}, Ly7/f;->Y(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static synthetic U(Ly7/f;)Ly7/f$n;
    .locals 0

    iget-object p0, p0, Ly7/f;->u:Ly7/f$n;

    return-object p0
.end method

.method static synthetic V(Ly7/f;)Ly7/f$n;
    .locals 0

    iget-object p0, p0, Ly7/f;->v:Ly7/f$n;

    return-object p0
.end method

.method static synthetic W(Ly7/f;)Landroid/view/View;
    .locals 0

    iget-object p0, p0, Ly7/f;->B:Landroid/view/View;

    return-object p0
.end method

.method static synthetic X(Ly7/f;Landroid/view/View;)Landroid/view/View;
    .locals 0

    iput-object p1, p0, Ly7/f;->B:Landroid/view/View;

    return-object p1
.end method

.method private static Y(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    const-string v0, "origin is not allowed null"

    invoke-static {p0, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p0

    invoke-virtual {p0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object p0

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, v1, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object p0

    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private Z(Landroid/view/Window;Landroid/view/WindowManager;)V
    .locals 1

    const/high16 v0, 0x20000

    invoke-virtual {p1, v0}, Landroid/view/Window;->clearFlags(I)V

    invoke-interface {p2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object p2

    invoke-virtual {p1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    invoke-virtual {p2}, Landroid/view/Display;->getWidth()I

    move-result p2

    iput p2, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    const/16 p2, 0x51

    iput p2, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    invoke-virtual {p1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    return-void
.end method

.method static synthetic a(Ly7/f;)V
    .locals 0

    invoke-direct {p0}, Ly7/f;->j0()V

    return-void
.end method

.method private a0(Landroid/app/Activity;Landroid/view/View;Landroid/view/Window;Ly7/f$n;Z)V
    .locals 6

    const/high16 v0, 0x20000

    invoke-virtual {p3, v0}, Landroid/view/Window;->clearFlags(I)V

    iget v0, p4, Ly7/f$n;->a:I

    if-lez v0, :cond_0

    invoke-virtual {p2, v0}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_0

    :cond_0
    iget-object v0, p4, Ly7/f$n;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    invoke-virtual {p2, v0}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Ly7/a;->a:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/view/View;->setBackgroundColor(I)V

    :goto_0
    invoke-virtual {p3}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object p2

    invoke-virtual {p3}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    iget-object v2, p4, Ly7/f$n;->f:Landroid/graphics/Rect;

    if-eqz v2, :cond_3

    iget v3, v2, Landroid/graphics/Rect;->left:I

    iget v4, v2, Landroid/graphics/Rect;->top:I

    iget v5, v2, Landroid/graphics/Rect;->right:I

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v0, v3, v4, v5, v2}, Landroid/view/View;->setPadding(IIII)V

    iget v0, p4, Ly7/f$n;->c:I

    and-int/lit8 v2, v0, 0x50

    if-eqz v2, :cond_2

    iget-object v0, p4, Ly7/f$n;->f:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->bottom:I

    goto :goto_1

    :cond_2
    and-int/lit8 v0, v0, 0x30

    if-eqz v0, :cond_4

    iget-object v0, p4, Ly7/f$n;->f:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->top:I

    goto :goto_1

    :cond_3
    invoke-virtual {p4}, Ly7/f$n;->a()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/view/View;->setPadding(IIII)V

    :cond_4
    :goto_1
    invoke-virtual {p4}, Ly7/f$n;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    iget p1, p4, Ly7/f$n;->d:I

    iput p1, p2, Landroid/view/WindowManager$LayoutParams;->width:I

    iget p1, p4, Ly7/f$n;->e:I

    add-int/2addr p1, v1

    iput p1, p2, Landroid/view/WindowManager$LayoutParams;->height:I

    goto :goto_3

    :cond_5
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    invoke-virtual {p1}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object p1

    invoke-interface {p1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object p1

    invoke-virtual {p1, v0}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    if-eqz p5, :cond_6

    iget p1, v0, Landroid/graphics/Point;->y:I

    goto :goto_2

    :cond_6
    iget p1, v0, Landroid/graphics/Point;->x:I

    :goto_2
    iput p1, p2, Landroid/view/WindowManager$LayoutParams;->width:I

    :goto_3
    iget p1, p4, Ly7/f$n;->c:I

    iput p1, p2, Landroid/view/WindowManager$LayoutParams;->gravity:I

    invoke-virtual {p3, p2}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    return-void
.end method

.method static synthetic b(Ly7/f;)Ly7/f$o;
    .locals 0

    iget-object p0, p0, Ly7/f;->c:Ly7/f$o;

    return-object p0
.end method

.method private b0()V
    .locals 2

    sget-object v0, Ly7/f;->F:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Ly7/f$h;

    invoke-direct {v1, p0}, Ly7/f$h;-><init>(Ly7/f;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic c(Ly7/f;)Landroid/view/View;
    .locals 0

    iget-object p0, p0, Ly7/f;->e:Landroid/view/View;

    return-object p0
.end method

.method static c0(Ljava/util/concurrent/atomic/AtomicBoolean;)Z
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result p0

    return p0
.end method

.method static synthetic d(Ly7/f;Landroid/view/View;)Landroid/view/View;
    .locals 0

    iput-object p1, p0, Ly7/f;->e:Landroid/view/View;

    return-object p1
.end method

.method private d0()V
    .locals 1

    iget-object v0, p0, Ly7/f;->b:Lz7/j;

    invoke-virtual {v0}, Lz7/j;->h()V

    invoke-direct {p0}, Ly7/f;->w0()V

    return-void
.end method

.method static synthetic e(Ly7/f;)Landroid/webkit/WebView;
    .locals 0

    iget-object p0, p0, Ly7/f;->f:Landroid/webkit/WebView;

    return-object p0
.end method

.method private e0()V
    .locals 1

    iget-object v0, p0, Ly7/f;->h:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Ly7/f;->h:Landroid/app/AlertDialog;

    :cond_0
    return-void
.end method

.method static synthetic f(Ly7/f;Landroid/webkit/WebView;)Landroid/webkit/WebView;
    .locals 0

    iput-object p1, p0, Ly7/f;->f:Landroid/webkit/WebView;

    return-object p1
.end method

.method private f0()Landroid/app/Activity;
    .locals 3

    iget-object v0, p0, Ly7/f;->z:Ljava/lang/ref/WeakReference;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_0

    :cond_1
    return-object v0

    :cond_2
    :goto_0
    const-string v0, "VerificationManager"

    const-string v2, "Activity is destroy"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-object v1
.end method

.method static synthetic g(Ly7/f;)Landroid/widget/LinearLayout;
    .locals 0

    iget-object p0, p0, Ly7/f;->g:Landroid/widget/LinearLayout;

    return-object p0
.end method

.method private g0(Ljava/lang/String;)Lw7/a;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lw7/a<",
            "Lz7/d;",
            ">;"
        }
    .end annotation

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ly7/f$b;

    invoke-direct {v0, p0}, Ly7/f$b;-><init>(Ly7/f;)V

    new-instance v1, Lw7/a;

    new-instance v2, Ly7/f$c;

    invoke-direct {v2, p0, p1}, Ly7/f$c;-><init>(Ly7/f;Ljava/lang/String;)V

    invoke-direct {v1, v2, v0}, Lw7/a;-><init>(Ljava/util/concurrent/Callable;Lw7/a$b;)V

    iput-object v1, p0, Ly7/f;->a:Lw7/a;

    sget-object p1, Ly7/f;->F:Ljava/util/concurrent/ExecutorService;

    invoke-interface {p1, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    iget-object p1, p0, Ly7/f;->a:Lw7/a;

    return-object p1

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "getConfig: url is null"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method static synthetic h(Ly7/f;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;
    .locals 0

    iput-object p1, p0, Ly7/f;->g:Landroid/widget/LinearLayout;

    return-object p1
.end method

.method public static h0(ILjava/lang/String;)Lz7/n;
    .locals 1

    new-instance v0, Lz7/n$a;

    invoke-direct {v0}, Lz7/n$a;-><init>()V

    invoke-virtual {v0, p0}, Lz7/n$a;->e(I)Lz7/n$a;

    move-result-object p0

    invoke-virtual {p0, p1}, Lz7/n$a;->g(Ljava/lang/String;)Lz7/n$a;

    move-result-object p0

    invoke-virtual {p0}, Lz7/n$a;->d()Lz7/n;

    move-result-object p0

    return-object p0
.end method

.method static synthetic i(Ly7/f;)Landroid/app/AlertDialog;
    .locals 0

    iget-object p0, p0, Ly7/f;->h:Landroid/app/AlertDialog;

    return-object p0
.end method

.method private i0(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    invoke-static {p1}, Landroid/webkit/WebSettings;->getDefaultUserAgent(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " androidVerifySDK/"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "3.8.1"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " androidVerifySDK/VersionCode/"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const v0, 0x94d4

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, " AppPackageName/"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method static synthetic j(Ly7/f;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0

    iput-object p1, p0, Ly7/f;->h:Landroid/app/AlertDialog;

    return-object p1
.end method

.method private j0()V
    .locals 1

    iget-object v0, p0, Ly7/f;->h:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->hide()V

    :cond_0
    return-void
.end method

.method static synthetic k(Ly7/f;Landroid/content/Context;)Ljava/lang/String;
    .locals 0

    invoke-direct {p0, p1}, Ly7/f;->i0(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static synthetic l(Ly7/f;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Ly7/f;->m0(Ljava/lang/String;)V

    return-void
.end method

.method private l0()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method static synthetic m(Ly7/f;)V
    .locals 0

    invoke-direct {p0}, Ly7/f;->d0()V

    return-void
.end method

.method private m0(Ljava/lang/String;)V
    .locals 2

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-direct {p0}, Ly7/f;->f0()Landroid/app/Activity;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method

.method static synthetic n(Ly7/f;)V
    .locals 0

    invoke-direct {p0}, Ly7/f;->e0()V

    return-void
.end method

.method static n0(Ljava/util/concurrent/atomic/AtomicBoolean;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    return-void
.end method

.method static synthetic o(Ly7/f;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Ly7/f;->j:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic p(Ly7/f;)Z
    .locals 0

    iget-boolean p0, p0, Ly7/f;->k:Z

    return p0
.end method

.method static synthetic q(Ly7/f;Z)Z
    .locals 0

    iput-boolean p1, p0, Ly7/f;->k:Z

    return p1
.end method

.method static synthetic r(Ly7/f;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 0

    iget-object p0, p0, Ly7/f;->C:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object p0
.end method

.method static synthetic s(Ly7/f;Z)Z
    .locals 0

    iput-boolean p1, p0, Ly7/f;->l:Z

    return p1
.end method

.method private s0(II)V
    .locals 2

    invoke-direct {p0}, Ly7/f;->f0()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Ly7/f;->i:Landroid/os/Handler;

    new-instance v1, Ly7/f$f;

    invoke-direct {v1, p0, p2, p1}, Ly7/f$f;-><init>(Ly7/f;II)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    iget-object p1, p0, Ly7/f;->i:Landroid/os/Handler;

    new-instance p2, Ly7/f$g;

    invoke-direct {p2, p0}, Ly7/f$g;-><init>(Ly7/f;)V

    const-wide/16 v0, 0x7d0

    invoke-virtual {p1, p2, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method static synthetic t(Ly7/f;)Landroid/content/DialogInterface$OnKeyListener;
    .locals 0

    iget-object p0, p0, Ly7/f;->D:Landroid/content/DialogInterface$OnKeyListener;

    return-object p0
.end method

.method private t0(Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Ly7/f;->f0()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Ly7/f;->i:Landroid/os/Handler;

    new-instance v1, Ly7/f$e;

    invoke-direct {v1, p0, p1}, Ly7/f$e;-><init>(Ly7/f;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void

    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "showDialog:url should not be null"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method static synthetic u(Ly7/f;)Landroid/content/DialogInterface$OnDismissListener;
    .locals 0

    iget-object p0, p0, Ly7/f;->E:Landroid/content/DialogInterface$OnDismissListener;

    return-object p0
.end method

.method private u0()V
    .locals 3

    invoke-direct {p0}, Ly7/f;->f0()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-static {v0}, Lz7/h;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Ly7/f;->h:Landroid/app/AlertDialog;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ly7/f;->i:Landroid/os/Handler;

    new-instance v1, Ly7/f$j;

    invoke-direct {v1, p0}, Ly7/f$j;-><init>(Ly7/f;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :cond_1
    sget-object v0, Lz7/g$a;->k:Lz7/g$a;

    invoke-virtual {v0}, Lz7/g$a;->a()I

    move-result v1

    invoke-static {v0}, Lz7/g;->a(Lz7/g$a;)I

    move-result v2

    invoke-direct {p0, v1, v2}, Ly7/f;->s0(II)V

    invoke-virtual {v0}, Lz7/g$a;->a()I

    move-result v0

    const-string v1, "network disconnected"

    invoke-static {v0, v1}, Ly7/f;->h0(ILjava/lang/String;)Lz7/n;

    move-result-object v0

    iget-object v1, p0, Ly7/f;->i:Landroid/os/Handler;

    new-instance v2, Ly7/f$k;

    invoke-direct {v2, p0, v0}, Ly7/f$k;-><init>(Ly7/f;Lz7/n;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_2
    :goto_0
    return-void
.end method

.method static synthetic v(Ly7/f;Landroid/app/Activity;Landroid/view/View;Landroid/view/Window;Ly7/f$n;Z)V
    .locals 0

    invoke-direct/range {p0 .. p5}, Ly7/f;->a0(Landroid/app/Activity;Landroid/view/View;Landroid/view/Window;Ly7/f$n;Z)V

    return-void
.end method

.method private v0(Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Ly7/f;->f0()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-static {v0}, Lz7/h;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, p1}, Ly7/f;->t0(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    sget-object p1, Lz7/g$a;->k:Lz7/g$a;

    invoke-virtual {p1}, Lz7/g$a;->a()I

    move-result v0

    invoke-static {p1}, Lz7/g;->a(Lz7/g$a;)I

    move-result v1

    invoke-direct {p0, v0, v1}, Ly7/f;->s0(II)V

    invoke-virtual {p1}, Lz7/g$a;->a()I

    move-result p1

    const-string v0, "network disconnected"

    invoke-static {p1, v0}, Ly7/f;->h0(ILjava/lang/String;)Lz7/n;

    move-result-object p1

    iget-object v0, p0, Ly7/f;->i:Landroid/os/Handler;

    new-instance v1, Ly7/f$i;

    invoke-direct {v1, p0, p1}, Ly7/f$i;-><init>(Ly7/f;Lz7/n;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :goto_0
    return-void
.end method

.method static synthetic w(Ly7/f;Landroid/view/Window;Landroid/view/WindowManager;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ly7/f;->Z(Landroid/view/Window;Landroid/view/WindowManager;)V

    return-void
.end method

.method private w0()V
    .locals 6

    iget-object v0, p0, Ly7/f;->y:Lz7/k;

    const-string v1, "maxduration"

    const/16 v2, 0x1388

    invoke-virtual {v0, v1, v2}, Lz7/k;->a(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Ly7/f;->w:I

    iget-object v0, p0, Ly7/f;->y:Lz7/k;

    const-string v1, "frequency"

    const/16 v2, 0x32

    invoke-virtual {v0, v1, v2}, Lz7/k;->a(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Ly7/f;->x:I

    iget-object v1, p0, Ly7/f;->b:Lz7/j;

    iget v2, p0, Ly7/f;->w:I

    invoke-virtual {v1, v0, v2}, Lz7/j;->i(II)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-object v2, p0, Ly7/f;->y:Lz7/k;

    const-string v3, "lastDownloadTime"

    const-wide/16 v4, 0x0

    invoke-virtual {v2, v3, v4, v5}, Lz7/k;->b(Ljava/lang/String;J)J

    move-result-wide v2

    sub-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    const-wide/32 v2, 0x5265c00

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const-string v0, "VerificationManager"

    const-string v1, "get config from server"

    invoke-static {v0, v1}, Lg7/b;->f(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Ly7/f;->r:Ljava/lang/String;

    const-string v1, "/captcha/v2/config"

    invoke-static {v0, v1}, Lz7/e;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ly7/f;->g0(Ljava/lang/String;)Lw7/a;

    :cond_0
    return-void
.end method

.method static synthetic x(Ly7/f;)Lz7/j;
    .locals 0

    iget-object p0, p0, Ly7/f;->b:Lz7/j;

    return-object p0
.end method

.method static synthetic y(Ly7/f;)Z
    .locals 0

    iget-boolean p0, p0, Ly7/f;->m:Z

    return p0
.end method

.method static synthetic z(Ly7/f;)Z
    .locals 0

    iget-boolean p0, p0, Ly7/f;->n:Z

    return p0
.end method


# virtual methods
.method public k0()V
    .locals 0

    invoke-direct {p0}, Ly7/f;->w0()V

    return-void
.end method

.method public o0(Ljava/lang/String;)Ly7/f;
    .locals 0

    iput-object p1, p0, Ly7/f;->p:Ljava/lang/String;

    return-object p0
.end method

.method public p0(Z)Ly7/f;
    .locals 0

    iput-boolean p1, p0, Ly7/f;->n:Z

    return-object p0
.end method

.method public q0(Ljava/lang/String;)Ly7/f;
    .locals 0

    iput-object p1, p0, Ly7/f;->o:Ljava/lang/String;

    return-object p0
.end method

.method public r0(Ly7/f$o;)Ly7/f;
    .locals 0

    iput-object p1, p0, Ly7/f;->c:Ly7/f$o;

    return-object p0
.end method

.method public x0()V
    .locals 2

    iget-object v0, p0, Ly7/f;->C:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-static {v0}, Ly7/f;->c0(Ljava/util/concurrent/atomic/AtomicBoolean;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Ly7/f;->o:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Ly7/f;->p:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    iput-boolean v0, p0, Ly7/f;->A:Z

    iget-object v0, p0, Ly7/f;->c:Ly7/f$o;

    if-eqz v0, :cond_2

    invoke-direct {p0}, Ly7/f;->l0()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    iput-boolean v0, p0, Ly7/f;->l:Z

    invoke-direct {p0}, Ly7/f;->b0()V

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Ly7/f;->u0()V

    :goto_0
    return-void

    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "startVerify: mVerifyResultCallback should not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "action is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "key is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
