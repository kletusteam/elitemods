.class public abstract Lp5/f;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lp5/f$a;,
        Lp5/f$b;
    }
.end annotation


# static fields
.field public static final f:Lp5/f$a;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Lo5/a;

.field private final d:Ljava/lang/String;

.field private e:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lp5/f$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lp5/f$a;-><init>(Lm8/g;)V

    sput-object v0, Lp5/f;->f:Lp5/f$a;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V
    .locals 1

    const-string v0, "itemName"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "itemType"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "page"

    invoke-static {p3, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lp5/f;->a:Ljava/lang/String;

    iput-object p2, p0, Lp5/f;->b:Ljava/lang/String;

    iput-object p3, p0, Lp5/f;->c:Lo5/a;

    const-string p1, "newEvent"

    iput-object p1, p0, Lp5/f;->d:Ljava/lang/String;

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lp5/f;->e:Ljava/util/HashMap;

    return-void
.end method

.method public static synthetic a(Lp5/f;)V
    .locals 0

    invoke-static {p0}, Lp5/f;->d(Lp5/f;)V

    return-void
.end method

.method private static final d(Lp5/f;)V
    .locals 5

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lp5/f;->e()V

    iget-object v0, p0, Lp5/f;->e:Ljava/util/HashMap;

    if-eqz v0, :cond_4

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_4

    iget-object v0, p0, Lp5/f;->e:Ljava/util/HashMap;

    invoke-static {v0}, Lm8/i;->c(Ljava/lang/Object;)V

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const-string v1, "{"

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x22

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\": \""

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\", \n"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    :cond_2
    invoke-virtual {p0}, Lp5/f;->b()Ljava/lang/String;

    move-result-object v0

    const-string v2, "expose"

    invoke-static {v0, v2}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lp5/f;->b()Ljava/lang/String;

    move-result-object v0

    const-string v2, "view"

    invoke-static {v0, v2}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lp5/f;->b()Ljava/lang/String;

    move-result-object v0

    const-string v2, "active"

    invoke-static {v0, v2}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lp5/f;->b()Ljava/lang/String;

    move-result-object v0

    const-string v2, "click"

    invoke-static {v0, v2}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lp5/f;->b()Ljava/lang/String;

    move-result-object v0

    const-string v2, "download"

    invoke-static {v0, v2}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lp5/f;->b()Ljava/lang/String;

    move-result-object v0

    const-string v2, "install"

    invoke-static {v0, v2}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lp5/f;->b()Ljava/lang/String;

    move-result-object v0

    const-string v2, "error"

    invoke-static {v0, v2}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    iget-object v0, p0, Lp5/f;->d:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "recordCountEvent :\n key :$"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lp5/f;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " \n params : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lf6/o;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    :cond_4
    invoke-static {}, Ll2/b;->a()Ll2/b;

    move-result-object v0

    invoke-virtual {p0}, Lp5/f;->b()Ljava/lang/String;

    move-result-object v1

    iget-object p0, p0, Lp5/f;->e:Ljava/util/HashMap;

    invoke-virtual {v0, v1, p0}, Ll2/b;->f(Ljava/lang/String;Ljava/util/Map;)V

    return-void
.end method


# virtual methods
.method public abstract b()Ljava/lang/String;
.end method

.method public c()Z
    .locals 1

    new-instance v0, Lp5/e;

    invoke-direct {v0, p0}, Lp5/e;-><init>(Lp5/f;)V

    invoke-static {v0}, Lf6/e;->a(Ljava/lang/Runnable;)V

    const/4 v0, 0x1

    return v0
.end method

.method public final e()V
    .locals 5

    iget-object v0, p0, Lp5/f;->e:Ljava/util/HashMap;

    if-eqz v0, :cond_e

    sget-object v1, Lq2/a;->a:Landroid/util/ArrayMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    iget-object v1, p0, Lp5/f;->c:Lo5/a;

    invoke-interface {v1}, Lo5/a;->z()Ljava/lang/String;

    move-result-object v1

    const-string v2, "launch_ref"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lp5/f;->c:Lo5/a;

    invoke-interface {v1}, Lo5/a;->z()Ljava/lang/String;

    move-result-object v1

    const-string v2, "launch_source_package"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "ref"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lp5/f;->c:Lo5/a;

    invoke-interface {v2}, Lo5/a;->getRef()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iget-object v1, p0, Lp5/f;->c:Lo5/a;

    invoke-interface {v1}, Lo5/a;->n()Ljava/lang/String;

    move-result-object v1

    const-string v2, "refs"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lp5/f;->c:Lo5/a;

    invoke-interface {v1}, Lo5/a;->k()Ljava/lang/String;

    move-result-object v1

    const-string v2, "from_ref"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lt5/n;->b:Lt5/n$a;

    invoke-virtual {v1}, Lt5/n$a;->b()Lt5/n;

    move-result-object v1

    invoke-virtual {v1}, Lt5/n;->c()Ljava/lang/String;

    move-result-object v1

    const-string v2, "exp_id"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "item_type"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lp5/f;->b:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    const-string v1, "item_name"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lp5/f;->a:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    sget-object v1, Lcom/android/packageinstaller/InstallerApplication;->c:Lcom/android/packageinstaller/InstallerApplication;

    invoke-static {v1}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->isSafeModelEnable(Landroid/content/Context;)Z

    move-result v1

    const-string v2, "installer_mode"

    const-string v3, "normal"

    if-eqz v1, :cond_4

    const-string v1, "safe_mode"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/android/packageinstaller/InstallerApplication;->e:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    sget-object v1, Lcom/android/packageinstaller/InstallerApplication;->e:Ljava/lang/String;

    sget-object v2, Ld6/l0;->c:Ld6/l0$a;

    invoke-virtual {v2}, Ld6/l0$a;->c()Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;->getPackageMd5()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_3
    const/4 v2, 0x0

    :goto_0
    invoke-static {v1, v2}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, "ext"

    const-string v2, "single_authorize"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_4
    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_5
    :goto_1
    iget-object v1, p0, Lp5/f;->c:Lo5/a;

    invoke-interface {v1}, Lo5/a;->H()Ljava/lang/String;

    move-result-object v1

    const-string v2, "install_process_type"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lp5/f;->c:Lo5/a;

    invoke-interface {v1}, Lo5/a;->v()Ljava/lang/String;

    move-result-object v1

    const-string v2, "install_process"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lp5/f;->c:Lo5/a;

    invoke-interface {v1}, Lo5/a;->E()Ljava/lang/String;

    move-result-object v1

    const-string v2, "main_app_type"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lp5/f;->c:Lo5/a;

    invoke-interface {v1}, Lo5/a;->Q()Ljava/lang/String;

    move-result-object v1

    const-string v2, "main_app_id"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lp5/f;->c:Lo5/a;

    invoke-interface {v1}, Lo5/a;->b()Ljava/lang/String;

    move-result-object v1

    const-string v2, "main_app_name"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lp5/f;->c:Lo5/a;

    invoke-interface {v1}, Lo5/a;->i()Ljava/lang/String;

    move-result-object v1

    const-string v2, "app_manage_scene"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lp5/f;->c:Lo5/a;

    invoke-interface {v1}, Lo5/a;->A()Ljava/lang/String;

    move-result-object v1

    const-string v2, "app_local_scene"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lp5/f;->c:Lo5/a;

    invoke-interface {v1}, Lo5/a;->x()Ljava/lang/String;

    move-result-object v1

    const-string v2, "package_name"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lp5/f;->c:Lo5/a;

    invoke-interface {v1}, Lo5/a;->getVersionName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "version_name"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lp5/f;->c:Lo5/a;

    invoke-interface {v1}, Lo5/a;->t()Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "is_bundle_download"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    const-string v2, "visit_time"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lp5/f;->c:Lo5/a;

    invoke-interface {v1}, Lo5/a;->l()Ljava/lang/String;

    move-result-object v1

    const-string v2, "mi_package_name"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/android/packageinstaller/InstallerApplication;->c:Lcom/android/packageinstaller/InstallerApplication;

    invoke-virtual {v1}, Landroid/app/Application;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lj2/f;->g(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "install_version_code"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/android/packageinstaller/InstallerApplication;->c:Lcom/android/packageinstaller/InstallerApplication;

    invoke-virtual {v1}, Landroid/app/Application;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lj2/f;->h(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "install_version_name"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/android/packageinstaller/InstallerApplication;->c:Lcom/android/packageinstaller/InstallerApplication;

    invoke-static {v1}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->isSafeModelEnable(Landroid/content/Context;)Z

    move-result v1

    const-string v2, ""

    const-string v4, "safe_mode_type"

    if-eqz v1, :cond_7

    sget-object v1, Lcom/android/packageinstaller/InstallerApplication;->c:Lcom/android/packageinstaller/InstallerApplication;

    invoke-static {v1}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->getSecurityModeStyle(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v3}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    const-string v1, "protect"

    invoke-virtual {v0, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_6
    invoke-virtual {v0, v4, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_7
    invoke-virtual {v0, v4, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_2
    invoke-static {}, Lcom/android/packageinstaller/utils/g;->c()Ljava/lang/String;

    move-result-object v1

    const-string v3, "device"

    invoke-interface {v0, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/android/packageinstaller/InstallerApplication;->c:Lcom/android/packageinstaller/InstallerApplication;

    invoke-static {v1}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->isSafeModelEnable(Landroid/content/Context;)Z

    move-result v1

    const-string v3, "verify_method_setting"

    if-eqz v1, :cond_9

    sget-object v1, Lcom/android/packageinstaller/InstallerApplication;->c:Lcom/android/packageinstaller/InstallerApplication;

    invoke-static {v1}, Lm2/b;->g(Landroid/content/Context;)Lm2/b;

    move-result-object v1

    invoke-virtual {v1}, Lm2/b;->s()Z

    move-result v1

    if-eqz v1, :cond_9

    sget-object v1, Lcom/android/packageinstaller/InstallerApplication;->c:Lcom/android/packageinstaller/InstallerApplication;

    invoke-static {v1}, Lm2/b;->g(Landroid/content/Context;)Lm2/b;

    move-result-object v1

    invoke-virtual {v1}, Lm2/b;->k()Lx5/a;

    move-result-object v1

    if-nez v1, :cond_8

    const/4 v1, -0x1

    goto :goto_3

    :cond_8
    sget-object v4, Lp5/f$b;->a:[I

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v1, v4, v1

    :goto_3
    const/4 v4, 0x1

    if-eq v1, v4, :cond_d

    const/4 v4, 0x2

    if-eq v1, v4, :cond_c

    const/4 v4, 0x3

    if-eq v1, v4, :cond_b

    const/4 v4, 0x4

    if-eq v1, v4, :cond_a

    const/4 v4, 0x5

    if-eq v1, v4, :cond_9

    goto :goto_5

    :cond_9
    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_5

    :cond_a
    const-string v1, "face_password"

    goto :goto_4

    :cond_b
    const-string v1, "fingerprint_password"

    goto :goto_4

    :cond_c
    const-string v1, "screen_password"

    goto :goto_4

    :cond_d
    const-string v1, "mi_account"

    :goto_4
    invoke-interface {v0, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_e
    :goto_5
    return-void
.end method

.method public f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;
    .locals 1

    const-string v0, "key"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lp5/f;->e:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object p0
.end method

.method public g(Ljava/util/Map;)Lp5/f;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lp5/f;"
        }
    .end annotation

    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_2

    return-object p0

    :cond_2
    iget-object v0, p0, Lp5/f;->e:Ljava/util/HashMap;

    if-eqz v0, :cond_3

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    :cond_3
    return-object p0
.end method
