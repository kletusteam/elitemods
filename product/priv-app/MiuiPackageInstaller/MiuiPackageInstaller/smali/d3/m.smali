.class public abstract Ld3/m;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ld3/m$g;,
        Ld3/m$c;,
        Ld3/m$f;,
        Ld3/m$b;,
        Ld3/m$a;,
        Ld3/m$d;,
        Ld3/m$e;
    }
.end annotation


# static fields
.field public static final a:Ld3/m;

.field public static final b:Ld3/m;

.field public static final c:Ld3/m;

.field public static final d:Ld3/m;

.field public static final e:Ld3/m;

.field public static final f:Ld3/m;

.field public static final g:Ld3/m;

.field public static final h:Lu2/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lu2/g<",
            "Ld3/m;",
            ">;"
        }
    .end annotation
.end field

.field static final i:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ld3/m$a;

    invoke-direct {v0}, Ld3/m$a;-><init>()V

    sput-object v0, Ld3/m;->a:Ld3/m;

    new-instance v0, Ld3/m$b;

    invoke-direct {v0}, Ld3/m$b;-><init>()V

    sput-object v0, Ld3/m;->b:Ld3/m;

    new-instance v0, Ld3/m$e;

    invoke-direct {v0}, Ld3/m$e;-><init>()V

    sput-object v0, Ld3/m;->c:Ld3/m;

    new-instance v0, Ld3/m$c;

    invoke-direct {v0}, Ld3/m$c;-><init>()V

    sput-object v0, Ld3/m;->d:Ld3/m;

    new-instance v0, Ld3/m$d;

    invoke-direct {v0}, Ld3/m$d;-><init>()V

    sput-object v0, Ld3/m;->e:Ld3/m;

    new-instance v1, Ld3/m$f;

    invoke-direct {v1}, Ld3/m$f;-><init>()V

    sput-object v1, Ld3/m;->f:Ld3/m;

    sput-object v0, Ld3/m;->g:Ld3/m;

    const-string v1, "com.bumptech.glide.load.resource.bitmap.Downsampler.DownsampleStrategy"

    invoke-static {v1, v0}, Lu2/g;->f(Ljava/lang/String;Ljava/lang/Object;)Lu2/g;

    move-result-object v0

    sput-object v0, Ld3/m;->h:Lu2/g;

    const/4 v0, 0x1

    sput-boolean v0, Ld3/m;->i:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a(IIII)Ld3/m$g;
.end method

.method public abstract b(IIII)F
.end method
