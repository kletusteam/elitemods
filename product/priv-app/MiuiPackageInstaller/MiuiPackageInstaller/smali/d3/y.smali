.class public Ld3/y;
.super Ljava/lang/Object;

# interfaces
.implements Lu2/j;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lu2/j<",
        "Landroid/net/Uri;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lf3/d;

.field private final b:Lx2/d;


# direct methods
.method public constructor <init>(Lf3/d;Lx2/d;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Ld3/y;->a:Lf3/d;

    iput-object p2, p0, Ld3/y;->b:Lx2/d;

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;Lu2/h;)Z
    .locals 0

    check-cast p1, Landroid/net/Uri;

    invoke-virtual {p0, p1, p2}, Ld3/y;->d(Landroid/net/Uri;Lu2/h;)Z

    move-result p1

    return p1
.end method

.method public bridge synthetic b(Ljava/lang/Object;IILu2/h;)Lw2/v;
    .locals 0

    check-cast p1, Landroid/net/Uri;

    invoke-virtual {p0, p1, p2, p3, p4}, Ld3/y;->c(Landroid/net/Uri;IILu2/h;)Lw2/v;

    move-result-object p1

    return-object p1
.end method

.method public c(Landroid/net/Uri;IILu2/h;)Lw2/v;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "II",
            "Lu2/h;",
            ")",
            "Lw2/v<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Ld3/y;->a:Lf3/d;

    invoke-virtual {v0, p1, p2, p3, p4}, Lf3/d;->c(Landroid/net/Uri;IILu2/h;)Lw2/v;

    move-result-object p1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    invoke-interface {p1}, Lw2/v;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/graphics/drawable/Drawable;

    iget-object p4, p0, Ld3/y;->b:Lx2/d;

    invoke-static {p4, p1, p2, p3}, Ld3/o;->a(Lx2/d;Landroid/graphics/drawable/Drawable;II)Lw2/v;

    move-result-object p1

    return-object p1
.end method

.method public d(Landroid/net/Uri;Lu2/h;)Z
    .locals 0

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object p1

    const-string p2, "android.resource"

    invoke-virtual {p2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method
