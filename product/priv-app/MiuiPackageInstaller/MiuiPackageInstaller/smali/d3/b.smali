.class public Ld3/b;
.super Ljava/lang/Object;

# interfaces
.implements Lu2/k;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lu2/k<",
        "Landroid/graphics/drawable/BitmapDrawable;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lx2/d;

.field private final b:Lu2/k;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lu2/k<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lx2/d;Lu2/k;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lx2/d;",
            "Lu2/k<",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Ld3/b;->a:Lx2/d;

    iput-object p2, p0, Ld3/b;->b:Lu2/k;

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;Ljava/io/File;Lu2/h;)Z
    .locals 0

    check-cast p1, Lw2/v;

    invoke-virtual {p0, p1, p2, p3}, Ld3/b;->c(Lw2/v;Ljava/io/File;Lu2/h;)Z

    move-result p1

    return p1
.end method

.method public b(Lu2/h;)Lu2/c;
    .locals 1

    iget-object v0, p0, Ld3/b;->b:Lu2/k;

    invoke-interface {v0, p1}, Lu2/k;->b(Lu2/h;)Lu2/c;

    move-result-object p1

    return-object p1
.end method

.method public c(Lw2/v;Ljava/io/File;Lu2/h;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lw2/v<",
            "Landroid/graphics/drawable/BitmapDrawable;",
            ">;",
            "Ljava/io/File;",
            "Lu2/h;",
            ")Z"
        }
    .end annotation

    iget-object v0, p0, Ld3/b;->b:Lu2/k;

    new-instance v1, Ld3/e;

    invoke-interface {p1}, Lw2/v;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object p1

    iget-object v2, p0, Ld3/b;->a:Lx2/d;

    invoke-direct {v1, p1, v2}, Ld3/e;-><init>(Landroid/graphics/Bitmap;Lx2/d;)V

    invoke-interface {v0, v1, p2, p3}, Lu2/d;->a(Ljava/lang/Object;Ljava/io/File;Lu2/h;)Z

    move-result p1

    return p1
.end method
