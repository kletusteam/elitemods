.class public Ld3/g;
.super Ljava/lang/Object;

# interfaces
.implements Lu2/j;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lu2/j<",
        "Ljava/nio/ByteBuffer;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ld3/n;


# direct methods
.method public constructor <init>(Ld3/n;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Ld3/g;->a:Ld3/n;

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;Lu2/h;)Z
    .locals 0

    check-cast p1, Ljava/nio/ByteBuffer;

    invoke-virtual {p0, p1, p2}, Ld3/g;->d(Ljava/nio/ByteBuffer;Lu2/h;)Z

    move-result p1

    return p1
.end method

.method public bridge synthetic b(Ljava/lang/Object;IILu2/h;)Lw2/v;
    .locals 0

    check-cast p1, Ljava/nio/ByteBuffer;

    invoke-virtual {p0, p1, p2, p3, p4}, Ld3/g;->c(Ljava/nio/ByteBuffer;IILu2/h;)Lw2/v;

    move-result-object p1

    return-object p1
.end method

.method public c(Ljava/nio/ByteBuffer;IILu2/h;)Lw2/v;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/nio/ByteBuffer;",
            "II",
            "Lu2/h;",
            ")",
            "Lw2/v<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    invoke-static {p1}, Lq3/a;->f(Ljava/nio/ByteBuffer;)Ljava/io/InputStream;

    move-result-object p1

    iget-object v0, p0, Ld3/g;->a:Ld3/n;

    invoke-virtual {v0, p1, p2, p3, p4}, Ld3/n;->f(Ljava/io/InputStream;IILu2/h;)Lw2/v;

    move-result-object p1

    return-object p1
.end method

.method public d(Ljava/nio/ByteBuffer;Lu2/h;)Z
    .locals 0

    iget-object p2, p0, Ld3/g;->a:Ld3/n;

    invoke-virtual {p2, p1}, Ld3/n;->q(Ljava/nio/ByteBuffer;)Z

    move-result p1

    return p1
.end method
