.class public final Ld3/u;
.super Ljava/lang/Object;

# interfaces
.implements Lu2/j;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lu2/j<",
        "Ljava/io/InputStream;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ld3/d;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ld3/d;

    invoke-direct {v0}, Ld3/d;-><init>()V

    iput-object v0, p0, Ld3/u;->a:Ld3/d;

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;Lu2/h;)Z
    .locals 0

    check-cast p1, Ljava/io/InputStream;

    invoke-virtual {p0, p1, p2}, Ld3/u;->d(Ljava/io/InputStream;Lu2/h;)Z

    move-result p1

    return p1
.end method

.method public bridge synthetic b(Ljava/lang/Object;IILu2/h;)Lw2/v;
    .locals 0

    check-cast p1, Ljava/io/InputStream;

    invoke-virtual {p0, p1, p2, p3, p4}, Ld3/u;->c(Ljava/io/InputStream;IILu2/h;)Lw2/v;

    move-result-object p1

    return-object p1
.end method

.method public c(Ljava/io/InputStream;IILu2/h;)Lw2/v;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/InputStream;",
            "II",
            "Lu2/h;",
            ")",
            "Lw2/v<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    invoke-static {p1}, Lq3/a;->b(Ljava/io/InputStream;)Ljava/nio/ByteBuffer;

    move-result-object p1

    invoke-static {p1}, Landroid/graphics/ImageDecoder;->createSource(Ljava/nio/ByteBuffer;)Landroid/graphics/ImageDecoder$Source;

    move-result-object p1

    iget-object v0, p0, Ld3/u;->a:Ld3/d;

    invoke-virtual {v0, p1, p2, p3, p4}, Lc3/a;->d(Landroid/graphics/ImageDecoder$Source;IILu2/h;)Lw2/v;

    move-result-object p1

    return-object p1
.end method

.method public d(Ljava/io/InputStream;Lu2/h;)Z
    .locals 0

    const/4 p1, 0x1

    return p1
.end method
