.class public final Ld3/w;
.super Ljava/lang/Object;

# interfaces
.implements Lu2/j;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lu2/j<",
        "Landroid/os/ParcelFileDescriptor;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ld3/n;


# direct methods
.method public constructor <init>(Ld3/n;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Ld3/w;->a:Ld3/n;

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;Lu2/h;)Z
    .locals 0

    check-cast p1, Landroid/os/ParcelFileDescriptor;

    invoke-virtual {p0, p1, p2}, Ld3/w;->d(Landroid/os/ParcelFileDescriptor;Lu2/h;)Z

    move-result p1

    return p1
.end method

.method public bridge synthetic b(Ljava/lang/Object;IILu2/h;)Lw2/v;
    .locals 0

    check-cast p1, Landroid/os/ParcelFileDescriptor;

    invoke-virtual {p0, p1, p2, p3, p4}, Ld3/w;->c(Landroid/os/ParcelFileDescriptor;IILu2/h;)Lw2/v;

    move-result-object p1

    return-object p1
.end method

.method public c(Landroid/os/ParcelFileDescriptor;IILu2/h;)Lw2/v;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/ParcelFileDescriptor;",
            "II",
            "Lu2/h;",
            ")",
            "Lw2/v<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Ld3/w;->a:Ld3/n;

    invoke-virtual {v0, p1, p2, p3, p4}, Ld3/n;->d(Landroid/os/ParcelFileDescriptor;IILu2/h;)Lw2/v;

    move-result-object p1

    return-object p1
.end method

.method public d(Landroid/os/ParcelFileDescriptor;Lu2/h;)Z
    .locals 0

    iget-object p2, p0, Ld3/w;->a:Ld3/n;

    invoke-virtual {p2, p1}, Ld3/n;->o(Landroid/os/ParcelFileDescriptor;)Z

    move-result p1

    return p1
.end method
