.class public Ld3/z;
.super Ljava/lang/Object;

# interfaces
.implements Lu2/j;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ld3/z$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lu2/j<",
        "Ljava/io/InputStream;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ld3/n;

.field private final b:Lx2/b;


# direct methods
.method public constructor <init>(Ld3/n;Lx2/b;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Ld3/z;->a:Ld3/n;

    iput-object p2, p0, Ld3/z;->b:Lx2/b;

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;Lu2/h;)Z
    .locals 0

    check-cast p1, Ljava/io/InputStream;

    invoke-virtual {p0, p1, p2}, Ld3/z;->d(Ljava/io/InputStream;Lu2/h;)Z

    move-result p1

    return p1
.end method

.method public bridge synthetic b(Ljava/lang/Object;IILu2/h;)Lw2/v;
    .locals 0

    check-cast p1, Ljava/io/InputStream;

    invoke-virtual {p0, p1, p2, p3, p4}, Ld3/z;->c(Ljava/io/InputStream;IILu2/h;)Lw2/v;

    move-result-object p1

    return-object p1
.end method

.method public c(Ljava/io/InputStream;IILu2/h;)Lw2/v;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/InputStream;",
            "II",
            "Lu2/h;",
            ")",
            "Lw2/v<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    instance-of v0, p1, Ld3/x;

    if-eqz v0, :cond_0

    check-cast p1, Ld3/x;

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    new-instance v0, Ld3/x;

    iget-object v1, p0, Ld3/z;->b:Lx2/b;

    invoke-direct {v0, p1, v1}, Ld3/x;-><init>(Ljava/io/InputStream;Lx2/b;)V

    const/4 p1, 0x1

    move-object v8, v0

    move v0, p1

    move-object p1, v8

    :goto_0
    invoke-static {p1}, Lq3/d;->g(Ljava/io/InputStream;)Lq3/d;

    move-result-object v1

    new-instance v3, Lq3/h;

    invoke-direct {v3, v1}, Lq3/h;-><init>(Ljava/io/InputStream;)V

    new-instance v7, Ld3/z$a;

    invoke-direct {v7, p1, v1}, Ld3/z$a;-><init>(Ld3/x;Lq3/d;)V

    :try_start_0
    iget-object v2, p0, Ld3/z;->a:Ld3/n;

    move v4, p2

    move v5, p3

    move-object v6, p4

    invoke-virtual/range {v2 .. v7}, Ld3/n;->g(Ljava/io/InputStream;IILu2/h;Ld3/n$b;)Lw2/v;

    move-result-object p2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v1}, Lq3/d;->m()V

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Ld3/x;->m()V

    :cond_1
    return-object p2

    :catchall_0
    move-exception p2

    invoke-virtual {v1}, Lq3/d;->m()V

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Ld3/x;->m()V

    :cond_2
    throw p2
.end method

.method public d(Ljava/io/InputStream;Lu2/h;)Z
    .locals 0

    iget-object p2, p0, Ld3/z;->a:Ld3/n;

    invoke-virtual {p2, p1}, Ld3/n;->p(Ljava/io/InputStream;)Z

    move-result p1

    return p1
.end method
