.class final Ld3/l$b;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ld3/l;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "b"
.end annotation


# instance fields
.field private final a:Ljava/nio/ByteBuffer;


# direct methods
.method constructor <init>([BI)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object p1

    sget-object v0, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object p1

    invoke-virtual {p1, p2}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    move-result-object p1

    check-cast p1, Ljava/nio/ByteBuffer;

    iput-object p1, p0, Ld3/l$b;->a:Ljava/nio/ByteBuffer;

    return-void
.end method

.method private c(II)Z
    .locals 1

    iget-object v0, p0, Ld3/l$b;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    sub-int/2addr v0, p1

    if-lt v0, p2, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method


# virtual methods
.method a(I)S
    .locals 1

    goto/32 :goto_4

    nop

    :goto_0
    return p1

    :goto_1
    invoke-direct {p0, p1, v0}, Ld3/l$b;->c(II)Z

    move-result v0

    goto/32 :goto_7

    nop

    :goto_2
    const/4 p1, -0x1

    :goto_3
    goto/32 :goto_0

    nop

    :goto_4
    const/4 v0, 0x2

    goto/32 :goto_1

    nop

    :goto_5
    goto :goto_3

    :goto_6
    goto/32 :goto_2

    nop

    :goto_7
    if-nez v0, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_8

    nop

    :goto_8
    iget-object v0, p0, Ld3/l$b;->a:Ljava/nio/ByteBuffer;

    goto/32 :goto_9

    nop

    :goto_9
    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->getShort(I)S

    move-result p1

    goto/32 :goto_5

    nop
.end method

.method b(I)I
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    iget-object v0, p0, Ld3/l$b;->a:Ljava/nio/ByteBuffer;

    goto/32 :goto_1

    nop

    :goto_1
    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result p1

    goto/32 :goto_3

    nop

    :goto_2
    const/4 v0, 0x4

    goto/32 :goto_9

    nop

    :goto_3
    goto :goto_6

    :goto_4
    goto/32 :goto_5

    nop

    :goto_5
    const/4 p1, -0x1

    :goto_6
    goto/32 :goto_8

    nop

    :goto_7
    if-nez v0, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_0

    nop

    :goto_8
    return p1

    :goto_9
    invoke-direct {p0, p1, v0}, Ld3/l$b;->c(II)Z

    move-result v0

    goto/32 :goto_7

    nop
.end method

.method d()I
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Ld3/l$b;->a:Ljava/nio/ByteBuffer;

    goto/32 :goto_2

    nop

    :goto_1
    return v0

    :goto_2
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    goto/32 :goto_1

    nop
.end method

.method e(Ljava/nio/ByteOrder;)V
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    iget-object v0, p0, Ld3/l$b;->a:Ljava/nio/ByteBuffer;

    goto/32 :goto_2

    nop

    :goto_2
    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    goto/32 :goto_0

    nop
.end method
