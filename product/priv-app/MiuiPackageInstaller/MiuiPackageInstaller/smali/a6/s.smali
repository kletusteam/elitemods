.class public final La6/s;
.super La6/e;


# instance fields
.field private final k:Ljava/lang/String;

.field private final l:Lcom/miui/packageInstaller/model/RiskAppProtectionGuardTip;

.field private m:Ll8/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ll8/a<",
            "La8/v;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/miui/packageInstaller/model/RiskAppProtectionGuardTip;Landroid/content/Context;)V
    .locals 1

    const-string v0, "riskGuardTip"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p3, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const v0, 0x7f1205c5

    invoke-direct {p0, p3, v0}, La6/e;-><init>(Landroid/content/Context;I)V

    iput-object p1, p0, La6/s;->k:Ljava/lang/String;

    iput-object p2, p0, La6/s;->l:Lcom/miui/packageInstaller/model/RiskAppProtectionGuardTip;

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->setCancelable(Z)V

    return-void
.end method

.method public static synthetic g(La6/s;Lm8/t;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1, p2}, La6/s;->m(La6/s;Lm8/t;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic h(La6/s;Lm8/t;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1, p2}, La6/s;->l(La6/s;Lm8/t;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic i(La6/s;Lm8/t;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1, p2}, La6/s;->k(La6/s;Lm8/t;Landroid/view/View;)V

    return-void
.end method

.method private final j(Lo5/a;)V
    .locals 3

    new-instance v0, Lp5/g;

    const-string v1, "risk_app_safe_mode_guidance_popup"

    const-string v2, "popup"

    invoke-direct {v0, v1, v2, p1}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    new-instance v0, Lp5/g;

    const-string v1, "risk_app_safe_mode_guidance_popup_open_btn"

    const-string v2, "button"

    invoke-direct {v0, v1, v2, p1}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    new-instance v0, Lp5/g;

    const-string v1, "risk_app_safe_mode_guidance_popup_cancel_btn"

    invoke-direct {v0, v1, v2, p1}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    new-instance v0, Lp5/g;

    const-string v1, "risk_app_safe_mode_guidance_popup_know_btn"

    invoke-direct {v0, v1, v2, p1}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    return-void
.end method

.method private static final k(La6/s;Lm8/t;Landroid/view/View;)V
    .locals 2

    const-string p2, "this$0"

    invoke-static {p0, p2}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "$iPage"

    invoke-static {p1, p2}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p2, Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lcom/miui/packageInstaller/ui/SecureModeAdvantageActivity;

    invoke-direct {p2, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v0, p1, Lm8/t;->a:Ljava/lang/Object;

    check-cast v0, Ljava/io/Serializable;

    const-string v1, "fromPage"

    invoke-virtual {p2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Landroid/app/Dialog;->dismiss()V

    new-instance p0, Lp5/b;

    iget-object p1, p1, Lm8/t;->a:Ljava/lang/Object;

    check-cast p1, Lo5/a;

    const-string p2, "risk_app_safe_mode_guidance_popup_know_btn"

    const-string v0, "button"

    invoke-direct {p0, p2, v0, p1}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p0}, Lp5/f;->c()Z

    return-void
.end method

.method private static final l(La6/s;Lm8/t;Landroid/view/View;)V
    .locals 1

    const-string p2, "this$0"

    invoke-static {p0, p2}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "$iPage"

    invoke-static {p1, p2}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object p2

    const/4 v0, 0x1

    invoke-static {p2, v0}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->setSafeModelEnabled(Landroid/content/Context;Z)V

    iget-object p2, p0, La6/s;->m:Ll8/a;

    if-eqz p2, :cond_0

    invoke-interface {p2}, Ll8/a;->a()Ljava/lang/Object;

    :cond_0
    invoke-virtual {p0}, Landroid/app/Dialog;->dismiss()V

    new-instance p0, Lp5/b;

    iget-object p1, p1, Lm8/t;->a:Ljava/lang/Object;

    check-cast p1, Lo5/a;

    const-string p2, "risk_app_safe_mode_guidance_popup_open_btn"

    const-string v0, "button"

    invoke-direct {p0, p2, v0, p1}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p0}, Lp5/f;->c()Z

    return-void
.end method

.method private static final m(La6/s;Lm8/t;Landroid/view/View;)V
    .locals 2

    const-string p2, "this$0"

    invoke-static {p0, p2}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "$iPage"

    invoke-static {p1, p2}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/app/Dialog;->dismiss()V

    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object p2

    instance-of p2, p2, Landroid/content/ContextWrapper;

    const-string v0, "null cannot be cast to non-null type android.app.Activity"

    if-eqz p2, :cond_0

    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object p2

    const-string v1, "null cannot be cast to non-null type android.content.ContextWrapper"

    invoke-static {p2, v1}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Landroid/content/ContextWrapper;

    invoke-virtual {p2}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object p2

    instance-of p2, p2, Landroid/app/Activity;

    if-eqz p2, :cond_1

    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-static {p0, v1}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p0, Landroid/content/ContextWrapper;

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object p0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object p2

    instance-of p2, p2, Landroid/app/Activity;

    if-eqz p2, :cond_1

    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object p0

    :goto_0
    invoke-static {p0, v0}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p0, Landroid/app/Activity;

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :cond_1
    new-instance p0, Lp5/b;

    iget-object p1, p1, Lm8/t;->a:Ljava/lang/Object;

    check-cast p1, Lo5/a;

    const-string p2, "risk_app_safe_mode_guidance_popup_cancel_btn"

    const-string v0, "button"

    invoke-direct {p0, p2, v0, p1}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p0}, Lp5/f;->c()Z

    return-void
.end method


# virtual methods
.method public final n(Ll8/a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ll8/a<",
            "La8/v;",
            ">;)V"
        }
    .end annotation

    const-string v0, "callBack"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, La6/s;->m:Ll8/a;

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 13

    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0d013f

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->setContentView(I)V

    const p1, 0x7f0a02d3

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroidx/constraintlayout/widget/ConstraintLayout;

    const v0, 0x7f0a0383

    invoke-virtual {p0, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/AppCompatTextView;

    const v1, 0x7f0a0380

    invoke-virtual {p0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroidx/appcompat/widget/AppCompatTextView;

    const v2, 0x7f0a02bb

    invoke-virtual {p0, v2}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroidx/appcompat/widget/AppCompatTextView;

    const v3, 0x7f0a009f

    invoke-virtual {p0, v3}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    const v4, 0x7f0a00a0

    invoke-virtual {p0, v4}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Laa/b;->d(Landroid/content/Context;)I

    move-result v5

    const/4 v6, 0x0

    invoke-virtual {p1, v6, v6, v6, v5}, Landroid/view/ViewGroup;->setPadding(IIII)V

    iget-object p1, p0, La6/s;->l:Lcom/miui/packageInstaller/model/RiskAppProtectionGuardTip;

    invoke-virtual {p1}, Lcom/miui/packageInstaller/model/RiskAppProtectionGuardTip;->getTitle()Ljava/lang/String;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result p1

    const/4 v5, 0x1

    if-lez p1, :cond_0

    move p1, v5

    goto :goto_0

    :cond_0
    move p1, v6

    :goto_0
    if-eqz p1, :cond_1

    iget-object p1, p0, La6/s;->l:Lcom/miui/packageInstaller/model/RiskAppProtectionGuardTip;

    invoke-virtual {p1}, Lcom/miui/packageInstaller/model/RiskAppProtectionGuardTip;->getTitle()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    iget-object v9, p0, La6/s;->k:Ljava/lang/String;

    if-eqz v9, :cond_4

    iget-object p1, p0, La6/s;->l:Lcom/miui/packageInstaller/model/RiskAppProtectionGuardTip;

    invoke-virtual {p1}, Lcom/miui/packageInstaller/model/RiskAppProtectionGuardTip;->getContent()Ljava/lang/String;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result p1

    if-lez p1, :cond_2

    move p1, v5

    goto :goto_1

    :cond_2
    move p1, v6

    :goto_1
    if-eqz p1, :cond_3

    iget-object p1, p0, La6/s;->l:Lcom/miui/packageInstaller/model/RiskAppProtectionGuardTip;

    invoke-virtual {p1}, Lcom/miui/packageInstaller/model/RiskAppProtectionGuardTip;->getContent()Ljava/lang/String;

    move-result-object v7

    const/4 v10, 0x0

    const/4 v11, 0x4

    const/4 v12, 0x0

    const-string v8, "*"

    invoke-static/range {v7 .. v12}, Lu8/g;->r(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    goto :goto_2

    :cond_3
    sget-object p1, Lm8/w;->a:Lm8/w;

    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object p1

    const v1, 0x7f1102dc

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string v1, "context.getString(R.stri\u2026en_pure_mode_description)"

    invoke-static {p1, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    new-array v1, v5, [Ljava/lang/Object;

    aput-object v9, v1, v6

    invoke-static {v1, v5}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v1

    invoke-static {p1, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    const-string v1, "format(format, *args)"

    invoke-static {p1, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_2
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_4
    iget-object p1, p0, La6/s;->l:Lcom/miui/packageInstaller/model/RiskAppProtectionGuardTip;

    invoke-virtual {p1}, Lcom/miui/packageInstaller/model/RiskAppProtectionGuardTip;->getLinkName()Ljava/lang/String;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result p1

    if-lez p1, :cond_5

    move p1, v5

    goto :goto_3

    :cond_5
    move p1, v6

    :goto_3
    if-eqz p1, :cond_6

    iget-object p1, p0, La6/s;->l:Lcom/miui/packageInstaller/model/RiskAppProtectionGuardTip;

    invoke-virtual {p1}, Lcom/miui/packageInstaller/model/RiskAppProtectionGuardTip;->getLinkName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_6
    iget-object p1, p0, La6/s;->l:Lcom/miui/packageInstaller/model/RiskAppProtectionGuardTip;

    invoke-virtual {p1}, Lcom/miui/packageInstaller/model/RiskAppProtectionGuardTip;->getButtonTop()Ljava/lang/String;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result p1

    if-lez p1, :cond_7

    move p1, v5

    goto :goto_4

    :cond_7
    move p1, v6

    :goto_4
    if-eqz p1, :cond_8

    iget-object p1, p0, La6/s;->l:Lcom/miui/packageInstaller/model/RiskAppProtectionGuardTip;

    invoke-virtual {p1}, Lcom/miui/packageInstaller/model/RiskAppProtectionGuardTip;->getButtonTop()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :cond_8
    iget-object p1, p0, La6/s;->l:Lcom/miui/packageInstaller/model/RiskAppProtectionGuardTip;

    invoke-virtual {p1}, Lcom/miui/packageInstaller/model/RiskAppProtectionGuardTip;->getButtonBottom()Ljava/lang/String;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result p1

    if-lez p1, :cond_9

    move v6, v5

    :cond_9
    if-eqz v6, :cond_a

    iget-object p1, p0, La6/s;->l:Lcom/miui/packageInstaller/model/RiskAppProtectionGuardTip;

    invoke-virtual {p1}, Lcom/miui/packageInstaller/model/RiskAppProtectionGuardTip;->getButtonBottom()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v4, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :cond_a
    new-instance p1, Lm8/t;

    invoke-direct {p1}, Lm8/t;-><init>()V

    new-instance v0, Lo5/b;

    const-string v1, ""

    invoke-direct {v0, v1, v1}, Lo5/b;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p1, Lm8/t;->a:Ljava/lang/Object;

    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v0

    instance-of v0, v0, Landroid/content/ContextWrapper;

    const-string v1, "null cannot be cast to non-null type com.android.packageinstaller.miui.BaseActivity"

    if-eqz v0, :cond_b

    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "null cannot be cast to non-null type android.content.ContextWrapper"

    invoke-static {v0, v2}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/content/ContextWrapper;

    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    instance-of v0, v0, Lq2/b;

    if-eqz v0, :cond_c

    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v2}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/content/ContextWrapper;

    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v1}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lq2/b;

    invoke-virtual {v0}, Lq2/b;->F0()Lo5/b;

    move-result-object v0

    const-string v1, "((context as ContextWrap\u2026t) as BaseActivity).mPage"

    goto :goto_5

    :cond_b
    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v0

    instance-of v0, v0, Lq2/b;

    if-eqz v0, :cond_c

    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v1}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lq2/b;

    invoke-virtual {v0}, Lq2/b;->F0()Lo5/b;

    move-result-object v0

    const-string v1, "context as BaseActivity).mPage"

    :goto_5
    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p1, Lm8/t;->a:Ljava/lang/Object;

    :cond_c
    iget-object v0, p1, Lm8/t;->a:Ljava/lang/Object;

    check-cast v0, Lo5/a;

    invoke-direct {p0, v0}, La6/s;->j(Lo5/a;)V

    const v0, 0x7f0a01e1

    invoke-virtual {p0, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/constraintlayout/widget/ConstraintLayout;

    if-eqz v0, :cond_d

    new-instance v1, La6/r;

    invoke-direct {v1, p0, p1}, La6/r;-><init>(La6/s;Lm8/t;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_d
    new-instance v0, La6/q;

    invoke-direct {v0, p0, p1}, La6/q;-><init>(La6/s;Lm8/t;)V

    invoke-virtual {v3, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, La6/p;

    invoke-direct {v0, p0, p1}, La6/p;-><init>(La6/s;Lm8/t;)V

    invoke-virtual {v4, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
