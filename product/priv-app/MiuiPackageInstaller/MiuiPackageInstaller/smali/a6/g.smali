.class public La6/g;
.super La6/e;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        La6/g$a;
    }
.end annotation


# instance fields
.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;

.field private p:La6/g$a;

.field private q:La6/g$a;

.field private r:La6/g$a;

.field private final s:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, La6/e;-><init>(Landroid/content/Context;)V

    new-instance p1, La6/f;

    invoke-direct {p1, p0}, La6/f;-><init>(La6/g;)V

    iput-object p1, p0, La6/g;->s:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public static synthetic g(La6/g;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, La6/g;->h(La6/g;Landroid/view/View;)V

    return-void
.end method

.method private static final h(La6/g;Landroid/view/View;)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result p1

    packed-switch p1, :pswitch_data_0

    goto :goto_1

    :pswitch_0
    iget-object p1, p0, La6/g;->q:La6/g$a;

    if-eqz p1, :cond_0

    goto :goto_0

    :pswitch_1
    iget-object p1, p0, La6/g;->r:La6/g$a;

    if-eqz p1, :cond_0

    goto :goto_0

    :pswitch_2
    iget-object p1, p0, La6/g;->p:La6/g$a;

    if-eqz p1, :cond_0

    :goto_0
    invoke-interface {p1}, La6/g$a;->a()V

    :cond_0
    :goto_1
    invoke-virtual {p0}, Landroid/app/Dialog;->dismiss()V

    return-void

    :pswitch_data_0
    .packed-switch 0x7f0a009c
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final i(Ljava/lang/String;)La6/g;
    .locals 0

    iput-object p1, p0, La6/g;->l:Ljava/lang/String;

    return-object p0
.end method

.method public final j(Ljava/lang/String;La6/g$a;)La6/g;
    .locals 1

    const-string v0, "oneBtnText"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "btnClickListener"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, La6/g;->m:Ljava/lang/String;

    iput-object p2, p0, La6/g;->p:La6/g$a;

    return-object p0
.end method

.method public final k(Ljava/lang/String;La6/g$a;)La6/g;
    .locals 1

    const-string v0, "threeBtnText"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "btnClickListener"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, La6/g;->o:Ljava/lang/String;

    iput-object p2, p0, La6/g;->r:La6/g$a;

    return-object p0
.end method

.method public final l(Ljava/lang/String;)La6/g;
    .locals 0

    iput-object p1, p0, La6/g;->k:Ljava/lang/String;

    return-object p0
.end method

.method public final m(Ljava/lang/String;La6/g$a;)La6/g;
    .locals 1

    const-string v0, "twoBtnText"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "btnClickListener"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, La6/g;->n:Ljava/lang/String;

    iput-object p2, p0, La6/g;->q:La6/g$a;

    return-object p0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7

    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object p1

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f06050f

    invoke-virtual {v0, v1}, Landroid/content/Context;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/Window;->setNavigationBarColor(I)V

    :goto_0
    const p1, 0x7f0d0038

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->setContentView(I)V

    const p1, 0x7f0a02d1

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->u()Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    const v0, 0x7f080139

    goto :goto_2

    :cond_2
    :goto_1
    const v0, 0x7f080143

    :goto_2
    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    const p1, 0x7f0a03a8

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    const v0, 0x7f0a03a6

    invoke-virtual {p0, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0a009c

    invoke-virtual {p0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    const v2, 0x7f0a009e

    invoke-virtual {p0, v2}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    const v3, 0x7f0a009d

    invoke-virtual {p0, v3}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iget-object v4, p0, La6/g;->s:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v4, p0, La6/g;->s:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v4, p0, La6/g;->s:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v4, p0, La6/g;->k:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    const/4 v5, 0x0

    if-nez v4, :cond_3

    invoke-virtual {p1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v4, p0, La6/g;->k:Ljava/lang/String;

    invoke-virtual {p1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    iget-object p1, p0, La6/g;->l:Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_4

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object p1, p0, La6/g;->l:Ljava/lang/String;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_4
    iget-object p1, p0, La6/g;->m:Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_5

    invoke-virtual {v1, v5}, Landroid/widget/Button;->setVisibility(I)V

    iget-object p1, p0, La6/g;->m:Ljava/lang/String;

    invoke-virtual {v1, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :cond_5
    iget-object p1, p0, La6/g;->n:Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_6

    invoke-virtual {v2, v5}, Landroid/widget/Button;->setVisibility(I)V

    iget-object p1, p0, La6/g;->n:Ljava/lang/String;

    invoke-virtual {v2, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :cond_6
    iget-object p1, p0, La6/g;->o:Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_7

    invoke-virtual {v3, v5}, Landroid/widget/Button;->setVisibility(I)V

    iget-object p1, p0, La6/g;->o:Ljava/lang/String;

    invoke-virtual {v3, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :cond_7
    const/4 p1, 0x1

    new-array v0, p1, [Landroid/view/View;

    aput-object v1, v0, v5

    invoke-static {v0}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object v0

    invoke-interface {v0}, Lmiuix/animation/e;->c()Lmiuix/animation/j;

    move-result-object v0

    new-array v4, v5, [Lmiuix/animation/j$b;

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-interface {v0, v6, v4}, Lmiuix/animation/j;->G(F[Lmiuix/animation/j$b;)Lmiuix/animation/j;

    move-result-object v0

    new-array v4, v5, [Lc9/a;

    invoke-interface {v0, v1, v4}, Lmiuix/animation/j;->z(Landroid/view/View;[Lc9/a;)V

    new-array v0, p1, [Landroid/view/View;

    aput-object v2, v0, v5

    invoke-static {v0}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object v0

    invoke-interface {v0}, Lmiuix/animation/e;->c()Lmiuix/animation/j;

    move-result-object v0

    new-array v1, v5, [Lmiuix/animation/j$b;

    invoke-interface {v0, v6, v1}, Lmiuix/animation/j;->G(F[Lmiuix/animation/j$b;)Lmiuix/animation/j;

    move-result-object v0

    new-array v1, v5, [Lc9/a;

    invoke-interface {v0, v2, v1}, Lmiuix/animation/j;->z(Landroid/view/View;[Lc9/a;)V

    new-array p1, p1, [Landroid/view/View;

    aput-object v3, p1, v5

    invoke-static {p1}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object p1

    invoke-interface {p1}, Lmiuix/animation/e;->c()Lmiuix/animation/j;

    move-result-object p1

    new-array v0, v5, [Lmiuix/animation/j$b;

    invoke-interface {p1, v6, v0}, Lmiuix/animation/j;->G(F[Lmiuix/animation/j$b;)Lmiuix/animation/j;

    move-result-object p1

    new-array v0, v5, [Lc9/a;

    invoke-interface {p1, v3, v0}, Lmiuix/animation/j;->z(Landroid/view/View;[Lc9/a;)V

    return-void
.end method
