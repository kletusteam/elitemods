.class public La6/e;
.super Landroid/app/Dialog;

# interfaces
.implements Landroid/content/ComponentCallbacks2;


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:Z

.field private h:I

.field private i:Landroid/content/res/Configuration;

.field private j:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const v0, 0x7f12011f

    invoke-direct {p0, p1, v0}, La6/e;-><init>(Landroid/content/Context;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 2

    invoke-direct {p0, p1, p2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    const/16 p2, 0x11

    iput p2, p0, La6/e;->a:I

    const/4 p2, -0x1

    iput p2, p0, La6/e;->b:I

    const/4 p2, -0x2

    iput p2, p0, La6/e;->c:I

    const/4 p2, 0x1

    iput-boolean p2, p0, La6/e;->g:Z

    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f07010b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, La6/e;->h:I

    invoke-virtual {p0, p2}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object p1

    iput-object p1, p0, La6/e;->i:Landroid/content/res/Configuration;

    invoke-direct {p0}, La6/e;->b()V

    return-void
.end method

.method private a()V
    .locals 9

    invoke-direct {p0}, La6/e;->b()V

    invoke-virtual {p0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v3, p0, La6/e;->b:I

    iget v4, p0, La6/e;->c:I

    const/4 v5, -0x2

    const/4 v6, -0x1

    if-ne v3, v6, :cond_1

    iget v3, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v7, p0, La6/e;->d:I

    shl-int/lit8 v7, v7, 0x1

    sub-int/2addr v3, v7

    goto :goto_0

    :cond_1
    if-ne v3, v5, :cond_2

    iget v7, p0, La6/e;->d:I

    iget v8, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    div-int/2addr v7, v8

    int-to-float v7, v7

    iput v7, v1, Landroid/view/WindowManager$LayoutParams;->horizontalMargin:F

    :cond_2
    :goto_0
    if-ne v4, v6, :cond_3

    iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v4, p0, La6/e;->e:I

    sub-int/2addr v2, v4

    shl-int/lit8 v4, v2, 0x1

    goto :goto_1

    :cond_3
    if-ne v4, v5, :cond_4

    iget v5, p0, La6/e;->e:I

    iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    div-int/2addr v5, v2

    int-to-float v2, v5

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->verticalMargin:F

    :cond_4
    :goto_1
    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result v2

    if-eqz v2, :cond_5

    iget v2, p0, La6/e;->h:I

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    goto :goto_2

    :cond_5
    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    :goto_2
    iput v4, v1, Landroid/view/WindowManager$LayoutParams;->height:I

    iget v1, p0, La6/e;->a:I

    invoke-virtual {v0, v1}, Landroid/view/Window;->setGravity(I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, La6/e;->g:Z

    return-void
.end method

.method private b()V
    .locals 5

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->c()Ljava/lang/String;

    move-result-object v0

    const-string v1, "cetus"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const v1, 0x7f07011d

    const/4 v2, -0x2

    const/16 v3, 0x11

    if-eqz v0, :cond_2

    iget-object v0, p0, La6/e;->i:Landroid/content/res/Configuration;

    iget v0, v0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v0, v0, 0xf

    iput v0, p0, La6/e;->j:I

    const/4 v4, 0x3

    if-ne v0, v4, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    const/high16 v0, 0x43980000    # 304.0f

    invoke-static {v0}, Lf6/d;->a(F)I

    move-result v0

    invoke-virtual {p0, v0, v2}, La6/e;->f(II)V

    invoke-virtual {p0, v3}, La6/e;->d(I)V

    goto :goto_2

    :cond_2
    invoke-static {}, Lcom/android/packageinstaller/utils/g;->c()Ljava/lang/String;

    move-result-object v0

    const-string v4, "zizhan"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    :goto_1
    invoke-virtual {p0, v3}, La6/e;->d(I)V

    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0, v0, v2}, La6/e;->f(II)V

    goto :goto_2

    :cond_3
    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070146

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    invoke-virtual {p0, v3, v0, v1}, La6/e;->e(III)V

    goto :goto_2

    :cond_4
    const/16 v0, 0x50

    invoke-virtual {p0, v0}, La6/e;->d(I)V

    :goto_2
    return-void
.end method

.method private c()V
    .locals 9

    iget-boolean v0, p0, La6/e;->g:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, La6/e;->g:Z

    invoke-virtual {p0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    if-nez v0, :cond_1

    return-void

    :cond_1
    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    iget v2, p0, La6/e;->a:I

    and-int/lit8 v3, v2, 0x30

    const/16 v4, 0x30

    if-ne v3, v4, :cond_3

    iget v2, p0, La6/e;->f:I

    if-nez v2, :cond_2

    const v2, 0x7f120132

    :cond_2
    :goto_0
    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    goto :goto_1

    :cond_3
    const/16 v3, 0x50

    and-int/2addr v2, v3

    if-ne v2, v3, :cond_4

    iget v2, p0, La6/e;->f:I

    if-nez v2, :cond_2

    const v2, 0x7f12012d

    goto :goto_0

    :cond_4
    iget v2, p0, La6/e;->f:I

    if-nez v2, :cond_2

    const v2, 0x7f120130

    goto :goto_0

    :goto_1
    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v3, p0, La6/e;->b:I

    iget v4, p0, La6/e;->c:I

    const/4 v5, -0x2

    const/4 v6, -0x1

    if-ne v3, v6, :cond_5

    iget v3, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v7, p0, La6/e;->d:I

    shl-int/lit8 v7, v7, 0x1

    sub-int/2addr v3, v7

    goto :goto_2

    :cond_5
    if-ne v3, v5, :cond_6

    iget v7, p0, La6/e;->d:I

    iget v8, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    div-int/2addr v7, v8

    int-to-float v7, v7

    iput v7, v1, Landroid/view/WindowManager$LayoutParams;->horizontalMargin:F

    :cond_6
    :goto_2
    if-ne v4, v6, :cond_7

    iget v2, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v4, p0, La6/e;->e:I

    sub-int/2addr v2, v4

    shl-int/lit8 v4, v2, 0x1

    goto :goto_3

    :cond_7
    if-ne v4, v5, :cond_8

    iget v5, p0, La6/e;->e:I

    iget v2, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    div-int/2addr v5, v2

    int-to-float v2, v5

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->verticalMargin:F

    :cond_8
    :goto_3
    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result v2

    if-eqz v2, :cond_9

    iget v2, p0, La6/e;->b:I

    if-ne v2, v6, :cond_9

    iget v2, p0, La6/e;->h:I

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    goto :goto_4

    :cond_9
    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    :goto_4
    iput v4, v1, Landroid/view/WindowManager$LayoutParams;->height:I

    iget v1, p0, La6/e;->a:I

    invoke-virtual {v0, v1}, Landroid/view/Window;->setGravity(I)V

    return-void
.end method


# virtual methods
.method public d(I)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v0}, La6/e;->e(III)V

    return-void
.end method

.method public e(III)V
    .locals 2

    iget v0, p0, La6/e;->a:I

    const/4 v1, 0x1

    if-eq v0, p1, :cond_0

    iput-boolean v1, p0, La6/e;->g:Z

    :cond_0
    iput p1, p0, La6/e;->a:I

    iget p1, p0, La6/e;->d:I

    if-eq p1, p2, :cond_1

    iput-boolean v1, p0, La6/e;->g:Z

    :cond_1
    iput p2, p0, La6/e;->d:I

    iget p1, p0, La6/e;->e:I

    if-eq p1, p3, :cond_2

    iput-boolean v1, p0, La6/e;->g:Z

    :cond_2
    iput p3, p0, La6/e;->e:I

    return-void
.end method

.method public f(II)V
    .locals 1

    iget v0, p0, La6/e;->b:I

    if-ne v0, p1, :cond_0

    iget v0, p0, La6/e;->c:I

    if-eq v0, p2, :cond_1

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, La6/e;->g:Z

    :cond_1
    iput p1, p0, La6/e;->b:I

    iput p2, p0, La6/e;->c:I

    return-void
.end method

.method public onAttachedToWindow()V
    .locals 1

    invoke-super {p0}, Landroid/app/Dialog;->onAttachedToWindow()V

    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/Context;->registerComponentCallbacks(Landroid/content/ComponentCallbacks;)V

    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4

    iget-object v0, p0, La6/e;->i:Landroid/content/res/Configuration;

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x1

    if-eq v0, v1, :cond_0

    move v0, v2

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {}, Lcom/android/packageinstaller/utils/g;->c()Ljava/lang/String;

    move-result-object v1

    const-string v3, "cetus"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget v1, p1, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v1, v1, 0xf

    iget v3, p0, La6/e;->j:I

    if-eq v1, v3, :cond_1

    goto :goto_1

    :cond_1
    move v2, v0

    :goto_1
    if-eqz v2, :cond_2

    iput-object p1, p0, La6/e;->i:Landroid/content/res/Configuration;

    invoke-direct {p0}, La6/e;->a()V

    :cond_2
    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 1

    invoke-super {p0}, Landroid/app/Dialog;->onDetachedFromWindow()V

    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/Context;->unregisterComponentCallbacks(Landroid/content/ComponentCallbacks;)V

    return-void
.end method

.method public onLowMemory()V
    .locals 0

    return-void
.end method

.method protected onStart()V
    .locals 0

    invoke-super {p0}, Landroid/app/Dialog;->onStart()V

    invoke-direct {p0}, La6/e;->c()V

    return-void
.end method

.method public onTrimMemory(I)V
    .locals 0

    return-void
.end method
