.class public final La6/l;
.super La6/e;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        La6/l$a;
    }
.end annotation


# static fields
.field public static final n:La6/l$a;


# instance fields
.field private k:Landroid/content/Context;

.field private final l:Z

.field private m:Ll8/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ll8/a<",
            "La8/v;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, La6/l$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, La6/l$a;-><init>(Lm8/g;)V

    sput-object v0, La6/l;->n:La6/l$a;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 1

    const-string v0, "mContext"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const v0, 0x7f1205c5

    invoke-direct {p0, p1, v0}, La6/e;-><init>(Landroid/content/Context;I)V

    iput-object p1, p0, La6/l;->k:Landroid/content/Context;

    iput-boolean p2, p0, La6/l;->l:Z

    return-void
.end method

.method public static synthetic g(La6/l;Landroid/widget/CheckBox;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1, p2}, La6/l;->j(La6/l;Landroid/widget/CheckBox;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic h(La6/l;Landroid/widget/CheckBox;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1, p2}, La6/l;->i(La6/l;Landroid/widget/CheckBox;Landroid/view/View;)V

    return-void
.end method

.method private static final i(La6/l;Landroid/widget/CheckBox;Landroid/view/View;)V
    .locals 3

    const-string p2, "this$0"

    invoke-static {p0, p2}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p2, Lp5/b;

    iget-object v0, p0, La6/l;->k:Landroid/content/Context;

    const-string v1, "null cannot be cast to non-null type com.miui.packageInstaller.analytics.IPage"

    invoke-static {v0, v1}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lo5/a;

    const-string v1, "safe_mode_guidance_popup_cancel_btn"

    const-string v2, "button"

    invoke-direct {p2, v1, v2, v0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "true"

    goto :goto_0

    :cond_0
    const-string v0, "false"

    :goto_0
    const-string v1, "is_remember"

    invoke-virtual {p2, v1, v0}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p2

    invoke-virtual {p2}, Lp5/f;->c()Z

    invoke-virtual {p1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result p1

    if-eqz p1, :cond_2

    sget-object p1, Lq5/a;->b:Lq5/a$b;

    invoke-virtual {p1}, Lq5/a$b;->a()Lq5/a;

    move-result-object p1

    iget-boolean p2, p0, La6/l;->l:Z

    if-eqz p2, :cond_1

    const-string p2, "pure_mode_guide_dialog_day_finish"

    goto :goto_1

    :cond_1
    const-string p2, "pure_mode_guide_dialog_day_start"

    :goto_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {p1, p2, v0, v1}, Lq5/a;->c(Ljava/lang/String;J)V

    :cond_2
    iget-object p1, p0, La6/l;->m:Ll8/a;

    if-eqz p1, :cond_3

    invoke-interface {p1}, Ll8/a;->a()Ljava/lang/Object;

    :cond_3
    invoke-virtual {p0}, Landroid/app/Dialog;->dismiss()V

    return-void
.end method

.method private static final j(La6/l;Landroid/widget/CheckBox;Landroid/view/View;)V
    .locals 3

    const-string p2, "this$0"

    invoke-static {p0, p2}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p2, Lp5/b;

    iget-object v0, p0, La6/l;->k:Landroid/content/Context;

    const-string v1, "null cannot be cast to non-null type com.miui.packageInstaller.analytics.IPage"

    invoke-static {v0, v1}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lo5/a;

    const-string v1, "safe_mode_guidance_popup_open_btn"

    const-string v2, "button"

    invoke-direct {p2, v1, v2, v0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "true"

    goto :goto_0

    :cond_0
    const-string v0, "false"

    :goto_0
    const-string v1, "is_remember"

    invoke-virtual {p2, v1, v0}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p2

    invoke-virtual {p2}, Lp5/f;->c()Z

    invoke-virtual {p1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result p1

    if-eqz p1, :cond_2

    sget-object p1, Lq5/a;->b:Lq5/a$b;

    invoke-virtual {p1}, Lq5/a$b;->a()Lq5/a;

    move-result-object p1

    iget-boolean p2, p0, La6/l;->l:Z

    if-eqz p2, :cond_1

    const-string p2, "pure_mode_guide_dialog_day_finish"

    goto :goto_1

    :cond_1
    const-string p2, "pure_mode_guide_dialog_day_start"

    :goto_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {p1, p2, v0, v1}, Lq5/a;->c(Ljava/lang/String;J)V

    :cond_2
    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object p1

    const/4 p2, 0x1

    invoke-static {p1, p2}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->setSafeModelEnabled(Landroid/content/Context;Z)V

    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object p1

    const p2, 0x7f1102c3

    const/4 v0, 0x0

    invoke-static {p1, p2, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    iget-object p1, p0, La6/l;->m:Ll8/a;

    if-eqz p1, :cond_3

    invoke-interface {p1}, Ll8/a;->a()Ljava/lang/Object;

    :cond_3
    invoke-virtual {p0}, Landroid/app/Dialog;->dismiss()V

    return-void
.end method


# virtual methods
.method public final k(Ll8/a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ll8/a<",
            "La8/v;",
            ">;)V"
        }
    .end annotation

    const-string v0, "click"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, La6/l;->m:Ll8/a;

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 12

    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->setCancelable(Z)V

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    invoke-virtual {p0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f06050f

    invoke-virtual {v1, v2}, Landroid/content/Context;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/Window;->setNavigationBarColor(I)V

    :goto_0
    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->u()Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    const v0, 0x7f0d0169

    goto :goto_2

    :cond_2
    :goto_1
    const v0, 0x7f0d016a

    :goto_2
    invoke-virtual {p0, v0}, Landroid/app/Dialog;->setContentView(I)V

    const v0, 0x7f0a00bb

    invoke-virtual {p0, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    const v1, 0x7f0a00a0

    invoke-virtual {p0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    const v2, 0x7f0a00a1

    invoke-virtual {p0, v2}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    const v3, 0x7f0a0380

    invoke-virtual {p0, v3}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f0a03af

    invoke-virtual {p0, v4}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iget-boolean v5, p0, La6/l;->l:Z

    if-eqz v5, :cond_3

    sget-object v5, Lm2/a;->b:Lm2/a$b;

    invoke-virtual {v5}, Lm2/a$b;->a()Lm2/a;

    move-result-object v5

    invoke-virtual {v5}, Lm2/a;->b()Lcom/miui/packageInstaller/model/GuideOpenSafeModePopTips;

    move-result-object v5

    goto :goto_3

    :cond_3
    sget-object v5, Lm2/a;->b:Lm2/a$b;

    invoke-virtual {v5}, Lm2/a$b;->a()Lm2/a;

    move-result-object v5

    invoke-virtual {v5}, Lm2/a;->c()Lcom/miui/packageInstaller/model/GuideOpenSafeModePopTips;

    move-result-object v5

    :goto_3
    const v6, 0x7f0a02d3

    invoke-virtual {p0, v6}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroidx/appcompat/widget/LinearLayoutCompat;

    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Laa/b;->d(Landroid/content/Context;)I

    move-result v7

    invoke-virtual {v6, p1, p1, p1, v7}, Landroid/view/ViewGroup;->setPadding(IIII)V

    sget-object p1, Lq5/a;->b:Lq5/a$b;

    invoke-virtual {p1}, Lq5/a$b;->a()Lq5/a;

    move-result-object v6

    iget-boolean v7, p0, La6/l;->l:Z

    const-string v8, "pure_mode_guide_dialog_show_num_finish"

    const-string v9, "pure_mode_guide_dialog_show_num_start"

    if-eqz v7, :cond_4

    move-object v7, v8

    goto :goto_4

    :cond_4
    move-object v7, v9

    :goto_4
    invoke-virtual {p1}, Lq5/a$b;->a()Lq5/a;

    move-result-object v10

    iget-boolean v11, p0, La6/l;->l:Z

    if-eqz v11, :cond_5

    goto :goto_5

    :cond_5
    move-object v8, v9

    :goto_5
    invoke-virtual {v10, v8}, Lq5/a;->b(Ljava/lang/String;)J

    move-result-wide v8

    const-wide/16 v10, 0x1

    add-long/2addr v8, v10

    invoke-virtual {v6, v7, v8, v9}, Lq5/a;->c(Ljava/lang/String;J)V

    invoke-virtual {p1}, Lq5/a$b;->a()Lq5/a;

    move-result-object p1

    iget-boolean v6, p0, La6/l;->l:Z

    if-eqz v6, :cond_6

    const-string v6, "pure_mode_guide_dialog_show_time_finish"

    goto :goto_6

    :cond_6
    const-string v6, "pure_mode_guide_dialog_show_time_start"

    :goto_6
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    invoke-virtual {p1, v6, v7, v8}, Lq5/a;->c(Ljava/lang/String;J)V

    if-eqz v5, :cond_7

    invoke-virtual {v5}, Lcom/miui/packageInstaller/model/GuideOpenSafeModePopTips;->getTitle()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v5}, Lcom/miui/packageInstaller/model/GuideOpenSafeModePopTips;->getText()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v4, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v5}, Lcom/miui/packageInstaller/model/GuideOpenSafeModePopTips;->getBl()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v5}, Lcom/miui/packageInstaller/model/GuideOpenSafeModePopTips;->getBr()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_7
    new-instance p1, La6/k;

    invoke-direct {p1, p0, v0}, La6/k;-><init>(La6/l;Landroid/widget/CheckBox;)V

    invoke-virtual {v1, p1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance p1, La6/j;

    invoke-direct {p1, p0, v0}, La6/j;-><init>(La6/l;Landroid/widget/CheckBox;)V

    invoke-virtual {v2, p1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public show()V
    .locals 5

    invoke-super {p0}, Landroid/app/Dialog;->show()V

    new-instance v0, Lp5/g;

    iget-object v1, p0, La6/l;->k:Landroid/content/Context;

    const-string v2, "null cannot be cast to non-null type com.miui.packageInstaller.analytics.IPage"

    invoke-static {v1, v2}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lo5/a;

    const-string v3, "safe_mode_guidance_popup"

    const-string v4, "popup"

    invoke-direct {v0, v3, v4, v1}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    new-instance v0, Lp5/g;

    iget-object v1, p0, La6/l;->k:Landroid/content/Context;

    invoke-static {v1, v2}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lo5/a;

    const-string v3, "safe_mode_guidance_popup_open_btn"

    const-string v4, "button"

    invoke-direct {v0, v3, v4, v1}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    new-instance v0, Lp5/g;

    iget-object v1, p0, La6/l;->k:Landroid/content/Context;

    invoke-static {v1, v2}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lo5/a;

    const-string v2, "safe_mode_guidance_popup_cancel_btn"

    invoke-direct {v0, v2, v4, v1}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    return-void
.end method
