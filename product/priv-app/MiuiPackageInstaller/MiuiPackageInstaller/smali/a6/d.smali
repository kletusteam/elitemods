.class public final La6/d;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        La6/d$a;
    }
.end annotation


# static fields
.field public static final a:La6/d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, La6/d;

    invoke-direct {v0}, La6/d;-><init>()V

    sput-object v0, La6/d;->a:La6/d;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static synthetic a(Lmiuix/appcompat/app/i;Landroid/view/View$OnClickListener;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1, p2}, La6/d;->h(Lmiuix/appcompat/app/i;Landroid/view/View$OnClickListener;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic b(La6/d$a;Landroid/content/DialogInterface;I)V
    .locals 0

    invoke-static {p0, p1, p2}, La6/d;->f(La6/d$a;Landroid/content/DialogInterface;I)V

    return-void
.end method

.method public static synthetic c(La6/d$a;Landroid/content/DialogInterface;I)V
    .locals 0

    invoke-static {p0, p1, p2}, La6/d;->e(La6/d$a;Landroid/content/DialogInterface;I)V

    return-void
.end method

.method private static final e(La6/d$a;Landroid/content/DialogInterface;I)V
    .locals 0

    const-string p1, "$mSafeModeDialogInterface"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p0}, La6/d$a;->cancel()V

    return-void
.end method

.method private static final f(La6/d$a;Landroid/content/DialogInterface;I)V
    .locals 0

    const-string p1, "$mSafeModeDialogInterface"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p0}, La6/d$a;->a()V

    return-void
.end method

.method private static final h(Lmiuix/appcompat/app/i;Landroid/view/View$OnClickListener;Landroid/view/View;)V
    .locals 1

    const-string v0, "$dialogVirus"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$onClickListener"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lmiuix/appcompat/app/i;->dismiss()V

    invoke-interface {p1, p2}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public final d(Landroid/content/Context;La6/d$a;)Lmiuix/appcompat/app/i;
    .locals 2

    const-string v0, "context"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mSafeModeDialogInterface"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lmiuix/appcompat/app/i$b;

    invoke-direct {v0, p1}, Lmiuix/appcompat/app/i$b;-><init>(Landroid/content/Context;)V

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->v()Z

    move-result p1

    if-eqz p1, :cond_0

    const p1, 0x7f0d018a

    goto :goto_0

    :cond_0
    const p1, 0x7f0d0188

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/android/packageinstaller/utils/g;->v()Z

    move-result p1

    if-eqz p1, :cond_2

    const p1, 0x7f0d0189

    goto :goto_0

    :cond_2
    const p1, 0x7f0d0187

    :goto_0
    invoke-virtual {v0, p1}, Lmiuix/appcompat/app/i$b;->u(I)Lmiuix/appcompat/app/i$b;

    const p1, 0x7f1100b5

    new-instance v1, La6/b;

    invoke-direct {v1, p2}, La6/b;-><init>(La6/d$a;)V

    invoke-virtual {v0, p1, v1}, Lmiuix/appcompat/app/i$b;->o(ILandroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/i$b;

    const p1, 0x7f1103e7

    new-instance v1, La6/a;

    invoke-direct {v1, p2}, La6/a;-><init>(La6/d$a;)V

    invoke-virtual {v0, p1, v1}, Lmiuix/appcompat/app/i$b;->h(ILandroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/i$b;

    invoke-virtual {v0}, Lmiuix/appcompat/app/i$b;->a()Lmiuix/appcompat/app/i;

    move-result-object p1

    const-string p2, "alertDialog.create()"

    invoke-static {p1, p2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/app/Dialog;->show()V

    return-object p1
.end method

.method public final g(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)Lmiuix/appcompat/app/i;
    .locals 2

    const-string v0, "context"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "title"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "des"

    invoke-static {p3, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onClickListener"

    invoke-static {p4, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lmiuix/appcompat/app/i$b;

    invoke-direct {v0, p1}, Lmiuix/appcompat/app/i$b;-><init>(Landroid/content/Context;)V

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result p1

    if-eqz p1, :cond_0

    const p1, 0x7f0d01d0

    goto :goto_0

    :cond_0
    const p1, 0x7f0d01cf

    :goto_0
    invoke-virtual {v0, p1}, Lmiuix/appcompat/app/i$b;->u(I)Lmiuix/appcompat/app/i$b;

    invoke-virtual {v0}, Lmiuix/appcompat/app/i$b;->a()Lmiuix/appcompat/app/i;

    move-result-object p1

    const-string v0, "alertDialog.create()"

    invoke-static {p1, v0}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/app/Dialog;->show()V

    const v0, 0x7f0a03d3

    invoke-virtual {p1, v0}, Ld/f;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0a03af

    invoke-virtual {p1, v1}, Ld/f;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    if-nez v0, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    if-nez v1, :cond_2

    goto :goto_2

    :cond_2
    invoke-virtual {v1, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_2
    const p2, 0x7f0a00a7

    invoke-virtual {p1, p2}, Ld/f;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/Button;

    if-eqz p2, :cond_3

    new-instance p3, La6/c;

    invoke-direct {p3, p1, p4}, La6/c;-><init>(Lmiuix/appcompat/app/i;Landroid/view/View$OnClickListener;)V

    invoke-virtual {p2, p3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_3
    return-object p1
.end method
