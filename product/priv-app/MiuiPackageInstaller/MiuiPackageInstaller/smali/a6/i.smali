.class public final La6/i;
.super La6/e;


# instance fields
.field private k:Landroid/widget/ImageView;

.field private l:Landroid/widget/TextView;

.field private m:I

.field private n:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, La6/e;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public final g(I)V
    .locals 1

    iput p1, p0, La6/i;->n:I

    iget-object v0, p0, La6/i;->l:Landroid/widget/TextView;

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method public final h(I)V
    .locals 1

    iput p1, p0, La6/i;->m:I

    iget-object v0, p0, La6/i;->k:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object p1

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f06050f

    invoke-virtual {v0, v1}, Landroid/content/Context;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/Window;->setNavigationBarColor(I)V

    :goto_0
    const p1, 0x7f0d005f

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->setContentView(I)V

    const p1, 0x7f0a02d1

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->u()Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    const v0, 0x7f080139

    goto :goto_2

    :cond_2
    :goto_1
    const v0, 0x7f080143

    :goto_2
    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    const p1, 0x7f0a0168

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, La6/i;->k:Landroid/widget/ImageView;

    const p1, 0x7f0a03a7

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, La6/i;->l:Landroid/widget/TextView;

    iget-object p1, p0, La6/i;->k:Landroid/widget/ImageView;

    if-nez p1, :cond_3

    goto :goto_3

    :cond_3
    iget v0, p0, La6/i;->m:I

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_3
    iget-object p1, p0, La6/i;->l:Landroid/widget/TextView;

    if-nez p1, :cond_4

    goto :goto_4

    :cond_4
    iget v0, p0, La6/i;->n:I

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_4
    return-void
.end method
