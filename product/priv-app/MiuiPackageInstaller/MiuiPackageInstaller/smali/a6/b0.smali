.class public La6/b0;
.super La6/c0;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lm5/e;)V
    .locals 6

    invoke-direct {p0, p1}, La6/c0;-><init>(Landroid/content/Context;)V

    new-instance v0, Lp5/g;

    move-object v1, p1

    check-cast v1, Lo5/a;

    const-string v2, "file_manager_authorize_popup"

    const-string v3, "popup"

    invoke-direct {v0, v2, v3, v1}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    new-instance v0, Lp5/g;

    const-string v2, "file_manager_authorize_popup_permit_btn"

    const-string v3, "button"

    invoke-direct {v0, v2, v3, v1}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    new-instance v0, Lp5/g;

    const-string v2, "file_manager_authorize_popup_forbid_btn"

    invoke-direct {v0, v2, v3, v1}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0d01cb

    goto :goto_0

    :cond_0
    const v0, 0x7f0d01ca

    :goto_0
    invoke-virtual {p0, v0}, Landroid/app/Dialog;->setContentView(I)V

    const v0, 0x7f0a02d1

    invoke-virtual {p0, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->u()Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_1

    :cond_1
    const v1, 0x7f080139

    goto :goto_2

    :cond_2
    :goto_1
    const v1, 0x7f080143

    :goto_2
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    invoke-static {p1}, Laa/b;->d(Landroid/content/Context;)I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v2, v2, v1}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    const v1, 0x7f0a02e2

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/miui/packageInstaller/view/SupportMaxHeightScrollView;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f070101

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/miui/packageInstaller/view/SupportMaxHeightScrollView;->setMaxHeight(I)V

    const v0, 0x7f0a03a8

    invoke-virtual {p0, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f1103da

    const/4 v3, 0x1

    new-array v4, v3, [Ljava/lang/Object;

    iget-object p2, p2, Lm5/e;->e:Ljava/lang/String;

    aput-object p2, v4, v2

    invoke-virtual {p1, v1, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const p2, 0x7f0a009c

    invoke-virtual {p0, p2}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/Button;

    const v0, 0x7f0a009e

    invoke-virtual {p0, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const v1, 0x7f0a00bb

    invoke-virtual {p0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    new-instance v4, La6/z;

    invoke-direct {v4, p0, v1, p1}, La6/z;-><init>(La6/b0;Landroid/widget/CheckBox;Landroid/content/Context;)V

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v4, 0x7f1103d4

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setText(I)V

    new-instance v4, La6/a0;

    invoke-direct {v4, p0, v1, p1}, La6/a0;-><init>(La6/b0;Landroid/widget/CheckBox;Landroid/content/Context;)V

    invoke-virtual {p2, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p1, 0x7f1103d5

    invoke-virtual {p2, p1}, Landroid/widget/Button;->setText(I)V

    new-array p1, v3, [Landroid/view/View;

    aput-object p2, p1, v2

    invoke-static {p1}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object p1

    invoke-interface {p1}, Lmiuix/animation/e;->c()Lmiuix/animation/j;

    move-result-object p1

    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v1

    const v4, 0x7f06002d

    invoke-virtual {v1, v4}, Landroid/content/Context;->getColor(I)I

    move-result v1

    invoke-interface {p1, v1}, Lmiuix/animation/j;->m(I)Lmiuix/animation/j;

    move-result-object p1

    new-array v1, v2, [Lmiuix/animation/j$b;

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-interface {p1, v5, v1}, Lmiuix/animation/j;->G(F[Lmiuix/animation/j$b;)Lmiuix/animation/j;

    move-result-object p1

    new-array v1, v2, [Lc9/a;

    invoke-interface {p1, p2, v1}, Lmiuix/animation/j;->z(Landroid/view/View;[Lc9/a;)V

    new-array p1, v3, [Landroid/view/View;

    aput-object v0, p1, v2

    invoke-static {p1}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object p1

    invoke-interface {p1}, Lmiuix/animation/e;->c()Lmiuix/animation/j;

    move-result-object p1

    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-virtual {p2, v4}, Landroid/content/Context;->getColor(I)I

    move-result p2

    invoke-interface {p1, p2}, Lmiuix/animation/j;->m(I)Lmiuix/animation/j;

    move-result-object p1

    new-array p2, v2, [Lmiuix/animation/j$b;

    invoke-interface {p1, v5, p2}, Lmiuix/animation/j;->G(F[Lmiuix/animation/j$b;)Lmiuix/animation/j;

    move-result-object p1

    new-array p2, v2, [Lc9/a;

    invoke-interface {p1, v0, p2}, Lmiuix/animation/j;->z(Landroid/view/View;[Lc9/a;)V

    return-void
.end method

.method public static synthetic i(La6/b0;Landroid/widget/CheckBox;Landroid/content/Context;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, La6/b0;->k(Landroid/widget/CheckBox;Landroid/content/Context;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic j(La6/b0;Landroid/widget/CheckBox;Landroid/content/Context;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, La6/b0;->l(Landroid/widget/CheckBox;Landroid/content/Context;Landroid/view/View;)V

    return-void
.end method

.method private synthetic k(Landroid/widget/CheckBox;Landroid/content/Context;Landroid/view/View;)V
    .locals 2

    invoke-virtual {p0}, La6/c0;->g()La6/c0$a;

    move-result-object p3

    if-eqz p3, :cond_1

    invoke-virtual {p0}, La6/c0;->g()La6/c0$a;

    move-result-object p3

    const/4 v0, 0x1

    invoke-virtual {p1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    invoke-interface {p3, v0, v1}, La6/c0$a;->a(ZZ)V

    invoke-virtual {p1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result p1

    if-eqz p1, :cond_0

    const-string p1, "true"

    goto :goto_0

    :cond_0
    const-string p1, "false"

    :goto_0
    new-instance p3, Lp5/b;

    check-cast p2, Lo5/a;

    const-string v0, "file_manager_authorize_popup_permit_btn"

    const-string v1, "button"

    invoke-direct {p3, v0, v1, p2}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    const-string p2, "is_remember"

    invoke-virtual {p3, p2, p1}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p1

    invoke-virtual {p1}, Lp5/f;->c()Z

    :cond_1
    invoke-virtual {p0}, Landroid/app/Dialog;->dismiss()V

    return-void
.end method

.method private synthetic l(Landroid/widget/CheckBox;Landroid/content/Context;Landroid/view/View;)V
    .locals 2

    invoke-virtual {p0}, La6/c0;->g()La6/c0$a;

    move-result-object p3

    if-eqz p3, :cond_1

    invoke-virtual {p0}, La6/c0;->g()La6/c0$a;

    move-result-object p3

    const/4 v0, 0x0

    invoke-virtual {p1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    invoke-interface {p3, v0, v1}, La6/c0$a;->a(ZZ)V

    invoke-virtual {p1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result p1

    if-eqz p1, :cond_0

    const-string p1, "true"

    goto :goto_0

    :cond_0
    const-string p1, "false"

    :goto_0
    new-instance p3, Lp5/b;

    check-cast p2, Lo5/a;

    const-string v0, "file_manager_authorize_popup_forbid_btn"

    const-string v1, "button"

    invoke-direct {p3, v0, v1, p2}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    const-string p2, "is_remember"

    invoke-virtual {p3, p2, p1}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p1

    invoke-virtual {p1}, Lp5/f;->c()Z

    :cond_1
    invoke-virtual {p0}, Landroid/app/Dialog;->dismiss()V

    return-void
.end method
