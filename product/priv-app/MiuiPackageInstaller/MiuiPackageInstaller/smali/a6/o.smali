.class public final La6/o;
.super La6/c0;


# instance fields
.field private final l:Landroid/content/Context;

.field private final m:Lcom/miui/packageInstaller/model/InstallSourceTips;

.field private final n:Lm5/e;

.field private final o:Lcom/miui/packageInstaller/model/ApkInfo;

.field private p:Landroid/widget/CheckBox;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/InstallSourceTips;Lm5/e;Lcom/miui/packageInstaller/model/ApkInfo;)V
    .locals 1

    const-string v0, "mContent"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "installTip"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mCallingPackage"

    invoke-static {p3, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, La6/c0;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, La6/o;->l:Landroid/content/Context;

    iput-object p2, p0, La6/o;->m:Lcom/miui/packageInstaller/model/InstallSourceTips;

    iput-object p3, p0, La6/o;->n:Lm5/e;

    iput-object p4, p0, La6/o;->o:Lcom/miui/packageInstaller/model/ApkInfo;

    return-void
.end method

.method public static synthetic i(La6/o;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, La6/o;->m(La6/o;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic j(La6/o;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, La6/o;->l(La6/o;Landroid/view/View;)V

    return-void
.end method

.method private static final l(La6/o;Landroid/view/View;)V
    .locals 5

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p0, La6/o;->m:Lcom/miui/packageInstaller/model/InstallSourceTips;

    invoke-virtual {p1}, Lcom/miui/packageInstaller/model/InstallSourceTips;->getType()I

    move-result p1

    const-string v0, "null cannot be cast to non-null type com.miui.packageInstaller.analytics.IPage"

    const-string v1, "button"

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eq p1, v3, :cond_4

    iget-object p1, p0, La6/o;->m:Lcom/miui/packageInstaller/model/InstallSourceTips;

    invoke-virtual {p1}, Lcom/miui/packageInstaller/model/InstallSourceTips;->getType()I

    move-result p1

    const/4 v4, 0x2

    if-ne p1, v4, :cond_0

    goto :goto_2

    :cond_0
    new-instance p1, Lp5/b;

    iget-object v4, p0, La6/o;->l:Landroid/content/Context;

    invoke-static {v4, v0}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v4, Lo5/a;

    const-string v0, "safe_mode_install_warning_popup_forbid_btn"

    invoke-direct {p1, v0, v1, v4}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    iget-object v0, p0, La6/o;->p:Landroid/widget/CheckBox;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-ne v0, v3, :cond_1

    move v0, v3

    goto :goto_0

    :cond_1
    move v0, v2

    :goto_0
    if-eqz v0, :cond_2

    const-string v0, "true"

    goto :goto_1

    :cond_2
    const-string v0, "false"

    :goto_1
    const-string v1, "is_remember"

    invoke-virtual {p1, v1, v0}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p1

    invoke-virtual {p1}, Lp5/f;->c()Z

    invoke-virtual {p0}, La6/c0;->g()La6/c0$a;

    move-result-object p1

    if-eqz p1, :cond_5

    iget-object v0, p0, La6/o;->p:Landroid/widget/CheckBox;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-ne v0, v3, :cond_3

    goto :goto_3

    :cond_3
    move v3, v2

    goto :goto_3

    :cond_4
    :goto_2
    new-instance p1, Lp5/b;

    iget-object v4, p0, La6/o;->l:Landroid/content/Context;

    invoke-static {v4, v0}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v4, Lo5/a;

    const-string v0, "safe_mode_install_warning_popup_forbid_install_btn"

    invoke-direct {p1, v0, v1, v4}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p1}, Lp5/f;->c()Z

    invoke-virtual {p0}, La6/c0;->g()La6/c0$a;

    move-result-object p1

    if-eqz p1, :cond_5

    :goto_3
    invoke-interface {p1, v2, v3}, La6/c0$a;->a(ZZ)V

    :cond_5
    invoke-virtual {p0}, Landroid/app/Dialog;->dismiss()V

    return-void
.end method

.method private static final m(La6/o;Landroid/view/View;)V
    .locals 5

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/app/Dialog;->dismiss()V

    iget-object p1, p0, La6/o;->m:Lcom/miui/packageInstaller/model/InstallSourceTips;

    invoke-virtual {p1}, Lcom/miui/packageInstaller/model/InstallSourceTips;->getType()I

    move-result p1

    const-string v0, "null cannot be cast to non-null type com.miui.packageInstaller.analytics.IPage"

    const-string v1, "button"

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eq p1, v3, :cond_5

    iget-object p1, p0, La6/o;->m:Lcom/miui/packageInstaller/model/InstallSourceTips;

    invoke-virtual {p1}, Lcom/miui/packageInstaller/model/InstallSourceTips;->getType()I

    move-result p1

    const/4 v4, 0x2

    if-ne p1, v4, :cond_0

    goto :goto_2

    :cond_0
    invoke-virtual {p0}, La6/c0;->g()La6/c0$a;

    move-result-object p1

    if-eqz p1, :cond_2

    iget-object v4, p0, La6/o;->p:Landroid/widget/CheckBox;

    if-eqz v4, :cond_1

    invoke-virtual {v4}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v4

    if-ne v4, v3, :cond_1

    move v4, v3

    goto :goto_0

    :cond_1
    move v4, v2

    :goto_0
    invoke-interface {p1, v3, v4}, La6/c0$a;->a(ZZ)V

    :cond_2
    new-instance p1, Lp5/b;

    iget-object v4, p0, La6/o;->l:Landroid/content/Context;

    invoke-static {v4, v0}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v4, Lo5/a;

    const-string v0, "safe_mode_install_warning_popup_permit_btn"

    invoke-direct {p1, v0, v1, v4}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    iget-object p0, p0, La6/o;->p:Landroid/widget/CheckBox;

    if-eqz p0, :cond_3

    invoke-virtual {p0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result p0

    if-ne p0, v3, :cond_3

    move v2, v3

    :cond_3
    if-eqz v2, :cond_4

    const-string p0, "true"

    goto :goto_1

    :cond_4
    const-string p0, "false"

    :goto_1
    const-string v0, "is_remember"

    invoke-virtual {p1, v0, p0}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p0

    invoke-virtual {p0}, Lp5/f;->c()Z

    goto :goto_3

    :cond_5
    :goto_2
    new-instance p1, Lp5/b;

    iget-object v3, p0, La6/o;->l:Landroid/content/Context;

    invoke-static {v3, v0}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Lo5/a;

    const-string v0, "safe_mode_install_warning_popup_cancel_install_btn"

    invoke-direct {p1, v0, v1, v3}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p1}, Lp5/f;->c()Z

    invoke-virtual {p0}, La6/c0;->g()La6/c0$a;

    move-result-object p0

    if-eqz p0, :cond_6

    invoke-interface {p0, v2, v2}, La6/c0$a;->a(ZZ)V

    :cond_6
    :goto_3
    return-void
.end method

.method private final n(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    if-eqz p1, :cond_2

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object p2

    const v0, 0x7f1103be

    invoke-virtual {p2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p2

    :cond_0
    move-object v2, p2

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p2

    if-eqz p2, :cond_1

    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object p2

    const p3, 0x7f110048

    invoke-virtual {p2, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p3

    :cond_1
    move-object v3, p3

    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object p2

    const p3, 0x7f060030

    invoke-virtual {p2, p3}, Landroid/content/Context;->getColor(I)I

    move-result v4

    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-virtual {p2, p3}, Landroid/content/Context;->getColor(I)I

    move-result v5

    sget-object v0, Lf6/b0;->a:Lf6/b0$a;

    invoke-static {v2}, Lm8/i;->c(Ljava/lang/Object;)V

    invoke-static {v3}, Lm8/i;->c(Ljava/lang/Object;)V

    new-instance v6, La6/o$a;

    invoke-direct {v6, p0}, La6/o$a;-><init>(La6/o;)V

    move-object v1, p1

    invoke-virtual/range {v0 .. v6}, Lf6/b0$a;->a(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;IILf6/b0$a$a;)V

    :cond_2
    return-void
.end method

.method private final o(Landroid/widget/FrameLayout;)V
    .locals 9

    iget-object v0, p0, La6/o;->m:Lcom/miui/packageInstaller/model/InstallSourceTips;

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/InstallSourceTips;->getType()I

    move-result v0

    const v1, 0x7f0a007b

    const v2, 0x7f0a0073

    const/4 v3, 0x2

    const/4 v4, 0x1

    const-string v5, "null cannot be cast to non-null type com.miui.packageInstaller.analytics.IPage"

    const-string v6, "button"

    if-eq v0, v4, :cond_2

    if-eq v0, v3, :cond_2

    const/4 v3, 0x3

    if-eq v0, v3, :cond_0

    goto/16 :goto_5

    :cond_0
    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result v3

    if-eqz v3, :cond_1

    const v3, 0x7f0d0067

    goto :goto_0

    :cond_1
    const v3, 0x7f0d0066

    :goto_0
    invoke-virtual {v0, v3, p1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/AppCompatImageView;

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroidx/appcompat/widget/AppCompatTextView;

    const v2, 0x7f0a00bb

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/CheckBox;

    iput-object p1, p0, La6/o;->p:Landroid/widget/CheckBox;

    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object p1

    iget-object v2, p0, La6/o;->n:Lm5/e;

    invoke-virtual {v2}, Lm5/e;->j()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v2}, Lj2/f;->b(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroidx/appcompat/widget/AppCompatImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object p1, p0, La6/o;->n:Lm5/e;

    iget-object p1, p1, Lm5/e;->e:Ljava/lang/String;

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance p1, Lp5/g;

    iget-object v0, p0, La6/o;->l:Landroid/content/Context;

    invoke-static {v0, v5}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lo5/a;

    const-string v1, "safe_mode_install_warning_popup_forbid_btn"

    invoke-direct {p1, v1, v6, v0}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p1}, Lp5/f;->c()Z

    new-instance p1, Lp5/g;

    iget-object v0, p0, La6/o;->l:Landroid/content/Context;

    invoke-static {v0, v5}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lo5/a;

    const-string v1, "safe_mode_install_warning_popup_permit_btn"

    invoke-direct {p1, v1, v6, v0}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    :goto_1
    invoke-virtual {p1}, Lp5/f;->c()Z

    goto/16 :goto_5

    :cond_2
    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result v7

    if-eqz v7, :cond_3

    const v7, 0x7f0d0180

    goto :goto_2

    :cond_3
    const v7, 0x7f0d017f

    :goto_2
    invoke-virtual {v0, v7, p1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    const v0, 0x7f0a03c2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/AppCompatTextView;

    const v7, 0x7f0a02cf

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroidx/appcompat/widget/AppCompatTextView;

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroidx/appcompat/widget/AppCompatImageView;

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroidx/appcompat/widget/AppCompatTextView;

    iget-object v8, p0, La6/o;->n:Lm5/e;

    iget-object v8, v8, Lm5/e;->e:Ljava/lang/String;

    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v8, p0, La6/o;->n:Lm5/e;

    invoke-virtual {v8}, Lm5/e;->j()Ljava/lang/String;

    move-result-object v8

    invoke-static {v1, v8}, Lj2/f;->b(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroidx/appcompat/widget/AppCompatImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v1, p0, La6/o;->m:Lcom/miui/packageInstaller/model/InstallSourceTips;

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/InstallSourceTips;->getWarningText()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, La6/o;->m:Lcom/miui/packageInstaller/model/InstallSourceTips;

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/InstallSourceTips;->getWarningText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_4
    iget-object v1, p0, La6/o;->m:Lcom/miui/packageInstaller/model/InstallSourceTips;

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/InstallSourceTips;->getType()I

    move-result v1

    if-ne v1, v4, :cond_5

    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f060069

    invoke-virtual {v1, v2}, Landroid/content/Context;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    const v0, 0x7f08056b

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    const/4 p1, 0x0

    :goto_3
    invoke-virtual {v7, p1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_4

    :cond_5
    iget-object v1, p0, La6/o;->m:Lcom/miui/packageInstaller/model/InstallSourceTips;

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/InstallSourceTips;->getType()I

    move-result v1

    if-ne v1, v3, :cond_6

    const v1, 0x7f080543

    invoke-virtual {p1, v1}, Landroid/view/View;->setBackgroundResource(I)V

    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object p1

    const v1, 0x7f060070

    invoke-virtual {p1, v1}, Landroid/content/Context;->getColor(I)I

    move-result p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    const/16 p1, 0x8

    goto :goto_3

    :cond_6
    :goto_4
    new-instance p1, Lp5/g;

    iget-object v0, p0, La6/o;->l:Landroid/content/Context;

    invoke-static {v0, v5}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lo5/a;

    const-string v1, "safe_mode_install_warning_popup_forbid_install_btn"

    invoke-direct {p1, v1, v6, v0}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p1}, Lp5/f;->c()Z

    new-instance p1, Lp5/g;

    iget-object v0, p0, La6/o;->l:Landroid/content/Context;

    invoke-static {v0, v5}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lo5/a;

    const-string v1, "safe_mode_install_warning_popup_cancel_install_btn"

    invoke-direct {p1, v1, v6, v0}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p1}, Lp5/f;->c()Z

    new-instance p1, Lp5/g;

    iget-object v0, p0, La6/o;->l:Landroid/content/Context;

    invoke-static {v0, v5}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lo5/a;

    const-string v1, "safe_mode_install_warning_popup_authorize_install_btn"

    invoke-direct {p1, v1, v6, v0}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    goto/16 :goto_1

    :goto_5
    return-void
.end method

.method private final p(Landroidx/appcompat/widget/AppCompatTextView;)V
    .locals 10

    iget-object v0, p0, La6/o;->m:Lcom/miui/packageInstaller/model/InstallSourceTips;

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/InstallSourceTips;->getInstallSourceAuthText()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f1103d7

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f110047

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, La6/o;->n(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6

    :cond_0
    iget-object v0, p0, La6/o;->m:Lcom/miui/packageInstaller/model/InstallSourceTips;

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/InstallSourceTips;->getInstallSourceAuthText()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, La6/o;->m:Lcom/miui/packageInstaller/model/InstallSourceTips;

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/InstallSourceTips;->getInstallSourceAuthText()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    const/4 v3, 0x0

    move v4, v3

    :goto_0
    const/16 v5, 0x23

    const/4 v6, 0x1

    const/4 v7, -0x1

    if-ge v4, v2, :cond_3

    invoke-interface {v1, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v8

    if-ne v8, v5, :cond_1

    move v8, v6

    goto :goto_1

    :cond_1
    move v8, v3

    :goto_1
    if-eqz v8, :cond_2

    goto :goto_2

    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_3
    move v4, v7

    :goto_2
    add-int/2addr v4, v6

    iget-object v1, p0, La6/o;->m:Lcom/miui/packageInstaller/model/InstallSourceTips;

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/InstallSourceTips;->getInstallSourceAuthText()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    add-int/2addr v2, v7

    if-ltz v2, :cond_7

    :goto_3
    add-int/lit8 v8, v2, -0x1

    invoke-interface {v1, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v9

    if-ne v9, v5, :cond_4

    move v9, v6

    goto :goto_4

    :cond_4
    move v9, v3

    :goto_4
    if-eqz v9, :cond_5

    move v7, v2

    goto :goto_5

    :cond_5
    if-gez v8, :cond_6

    goto :goto_5

    :cond_6
    move v2, v8

    goto :goto_3

    :cond_7
    :goto_5
    invoke-virtual {v0, v4, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    const-string v1, "this as java.lang.String\u2026ing(startIndex, endIndex)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, La6/o;->m:Lcom/miui/packageInstaller/model/InstallSourceTips;

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/InstallSourceTips;->getInstallSourceAuthText()Ljava/lang/String;

    move-result-object v2

    const/4 v5, 0x0

    const/4 v6, 0x4

    const/4 v7, 0x0

    const-string v3, "#"

    const-string v4, ""

    invoke-static/range {v2 .. v7}, Lu8/g;->r(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1, v1, v0}, La6/o;->n(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;)V

    :goto_6
    return-void
.end method


# virtual methods
.method public final k()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, La6/o;->l:Landroid/content/Context;

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 12

    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result p1

    if-nez p1, :cond_1

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->u()Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const p1, 0x7f0d017a

    goto :goto_1

    :cond_1
    :goto_0
    const p1, 0x7f0d017b

    :goto_1
    invoke-virtual {p0, p1}, Landroid/app/Dialog;->setContentView(I)V

    const p1, 0x7f0a0380

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroidx/appcompat/widget/AppCompatTextView;

    const v0, 0x7f0a0383

    invoke-virtual {p0, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/AppCompatTextView;

    const v1, 0x7f0a0174

    invoke-virtual {p0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    const v2, 0x7f0a012d

    invoke-virtual {p0, v2}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroidx/appcompat/widget/AppCompatImageView;

    const v3, 0x7f0a0388

    invoke-virtual {p0, v3}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const v4, 0x7f0a00a0

    invoke-virtual {p0, v4}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    const v5, 0x7f0a009f

    invoke-virtual {p0, v5}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    const v6, 0x7f0a02d3

    invoke-virtual {p0, v6}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroidx/constraintlayout/widget/ConstraintLayout;

    const-string v7, "contentLayout"

    invoke-static {v1, v7}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v1}, La6/o;->o(Landroid/widget/FrameLayout;)V

    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Laa/b;->d(Landroid/content/Context;)I

    move-result v1

    const/4 v7, 0x0

    invoke-virtual {v6, v7, v7, v7, v1}, Landroid/view/ViewGroup;->setPadding(IIII)V

    iget-object v1, p0, La6/o;->m:Lcom/miui/packageInstaller/model/InstallSourceTips;

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/InstallSourceTips;->getButton()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v1

    const v6, 0x7f1102ae

    invoke-virtual {v1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    :cond_2
    iget-object v1, p0, La6/o;->m:Lcom/miui/packageInstaller/model/InstallSourceTips;

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/InstallSourceTips;->getButton()Ljava/lang/String;

    move-result-object v1

    :goto_2
    invoke-virtual {v4, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, La6/o;->m:Lcom/miui/packageInstaller/model/InstallSourceTips;

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/InstallSourceTips;->getType()I

    move-result v1

    const/4 v6, 0x1

    if-eqz v1, :cond_5

    const v8, 0x7f060513

    const v9, 0x7f080556

    const-string v10, "titleDes"

    if-eq v1, v6, :cond_4

    const/4 v11, 0x2

    if-eq v1, v11, :cond_3

    const/4 v8, 0x3

    if-eq v1, v8, :cond_5

    goto/16 :goto_6

    :cond_3
    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v1

    const v6, 0x7f11017b

    invoke-virtual {v1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {v0, v10}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0}, La6/o;->p(Landroidx/appcompat/widget/AppCompatTextView;)V

    const p1, 0x7f080566

    invoke-virtual {v2, p1}, Landroidx/appcompat/widget/AppCompatImageView;->setImageResource(I)V

    const p1, 0x7f080568

    goto :goto_3

    :cond_4
    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v1

    const v6, 0x7f1103d6

    invoke-virtual {v1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {v0, v10}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0}, La6/o;->p(Landroidx/appcompat/widget/AppCompatTextView;)V

    const p1, 0x7f080564

    invoke-virtual {v2, p1}, Landroidx/appcompat/widget/AppCompatImageView;->setImageResource(I)V

    const p1, 0x7f080562

    :goto_3
    invoke-virtual {v3, p1}, Landroid/view/View;->setBackgroundResource(I)V

    invoke-virtual {v4, v9}, Landroid/widget/Button;->setBackgroundResource(I)V

    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1, v8}, Landroid/content/Context;->getColor(I)I

    move-result p1

    :goto_4
    invoke-virtual {v4, p1}, Landroid/widget/Button;->setTextColor(I)V

    goto/16 :goto_6

    :cond_5
    sget-object v1, Lm8/w;->a:Lm8/w;

    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v1

    const v8, 0x7f110178

    invoke-virtual {v1, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v8, "context.getString(R.stri\u2026source_dialog_safe_title)"

    invoke-static {v1, v8}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    new-array v8, v6, [Ljava/lang/Object;

    iget-object v9, p0, La6/o;->n:Lm5/e;

    iget-object v9, v9, Lm5/e;->e:Ljava/lang/String;

    aput-object v9, v8, v7

    invoke-static {v8, v6}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v6

    invoke-static {v1, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v6, "format(format, *args)"

    invoke-static {v1, v6}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object p1, p0, La6/o;->m:Lcom/miui/packageInstaller/model/InstallSourceTips;

    invoke-virtual {p1}, Lcom/miui/packageInstaller/model/InstallSourceTips;->getWarningText()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_6

    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object p1

    const v1, 0x7f110179

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_5

    :cond_6
    iget-object p1, p0, La6/o;->m:Lcom/miui/packageInstaller/model/InstallSourceTips;

    invoke-virtual {p1}, Lcom/miui/packageInstaller/model/InstallSourceTips;->getWarningText()Ljava/lang/String;

    move-result-object p1

    :goto_5
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const p1, 0x7f080565

    invoke-virtual {v2, p1}, Landroidx/appcompat/widget/AppCompatImageView;->setImageResource(I)V

    const p1, 0x7f08012c

    invoke-virtual {v3, p1}, Landroid/view/View;->setBackgroundResource(I)V

    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object p1

    const v0, 0x7f1103d4

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v5, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object p1, p0, La6/o;->m:Lcom/miui/packageInstaller/model/InstallSourceTips;

    invoke-virtual {p1}, Lcom/miui/packageInstaller/model/InstallSourceTips;->getButton()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_7

    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object p1

    const v0, 0x7f1103d5

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v4, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :cond_7
    const p1, 0x7f080555

    invoke-virtual {v4, p1}, Landroid/widget/Button;->setBackgroundResource(I)V

    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object p1

    const v0, 0x7f060032

    invoke-virtual {p1, v0}, Landroid/content/Context;->getColor(I)I

    move-result p1

    goto/16 :goto_4

    :goto_6
    new-instance p1, La6/n;

    invoke-direct {p1, p0}, La6/n;-><init>(La6/o;)V

    invoke-virtual {v4, p1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance p1, La6/m;

    invoke-direct {p1, p0}, La6/m;-><init>(La6/o;)V

    invoke-virtual {v5, p1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public show()V
    .locals 4

    invoke-super {p0}, Landroid/app/Dialog;->show()V

    new-instance v0, Lp5/g;

    iget-object v1, p0, La6/o;->l:Landroid/content/Context;

    const-string v2, "null cannot be cast to non-null type com.miui.packageInstaller.analytics.IPage"

    invoke-static {v1, v2}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lo5/a;

    const-string v2, "safe_mode_install_warning_popup"

    const-string v3, "popup"

    invoke-direct {v0, v2, v3, v1}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    return-void
.end method
