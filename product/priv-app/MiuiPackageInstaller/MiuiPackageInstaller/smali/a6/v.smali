.class public La6/v;
.super La6/e;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        La6/v$a;
    }
.end annotation


# instance fields
.field private k:La6/v$a;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, La6/e;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public static synthetic g(La6/v;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, La6/v;->i(La6/v;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic h(La6/v;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, La6/v;->j(La6/v;Landroid/view/View;)V

    return-void
.end method

.method private static final i(La6/v;Landroid/view/View;)V
    .locals 0

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/app/Dialog;->dismiss()V

    return-void
.end method

.method private static final j(La6/v;Landroid/view/View;)V
    .locals 0

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p0, p0, La6/v;->k:La6/v$a;

    if-eqz p0, :cond_0

    invoke-interface {p0}, La6/v$a;->a()V

    :cond_0
    return-void
.end method


# virtual methods
.method public final k(La6/v$a;)V
    .locals 1

    const-string v0, "safeModeListener"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, La6/v;->k:La6/v$a;

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result p1

    if-eqz p1, :cond_0

    const p1, 0x7f0d017e

    goto :goto_0

    :cond_0
    const p1, 0x7f0d017d

    :goto_0
    invoke-virtual {p0, p1}, Landroid/app/Dialog;->setContentView(I)V

    const p1, 0x7f0a02d1

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->u()Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    const v0, 0x7f080139

    goto :goto_2

    :cond_2
    :goto_1
    const v0, 0x7f080143

    :goto_2
    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    const p1, 0x7f0a00a0

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    new-instance v0, La6/t;

    invoke-direct {v0, p0}, La6/t;-><init>(La6/v;)V

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p1, 0x7f0a00a1

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    new-instance v0, La6/u;

    invoke-direct {v0, p0}, La6/u;-><init>(La6/v;)V

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
