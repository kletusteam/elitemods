.class public final Lf6/b0$a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf6/b0;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf6/b0$a$a;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lm8/g;)V
    .locals 0

    invoke-direct {p0}, Lf6/b0$a;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;IILf6/b0$a$a;)V
    .locals 7

    const-string v0, "textView"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "allString"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clickString"

    invoke-static {p3, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "callBack"

    invoke-static {p6, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lf6/b0$a$b;

    invoke-direct {v0, p4, p5, p6}, Lf6/b0$a$b;-><init>(IILf6/b0$a$a;)V

    const/4 p4, 0x0

    invoke-virtual {p1, p4}, Landroid/widget/TextView;->setVisibility(I)V

    new-instance p5, Landroid/text/SpannableStringBuilder;

    invoke-direct {p5, p2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x6

    const/4 v6, 0x0

    move-object v1, p5

    move-object v2, p3

    invoke-static/range {v1 .. v6}, Lu8/g;->L(Ljava/lang/CharSequence;Ljava/lang/String;IZILjava/lang/Object;)I

    move-result p2

    if-ltz p2, :cond_1

    invoke-virtual {p5}, Landroid/text/SpannableStringBuilder;->length()I

    move-result p6

    if-le p2, p6, :cond_0

    goto :goto_0

    :cond_0
    move p4, p2

    :cond_1
    :goto_0
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result p2

    add-int/2addr p2, p4

    if-ltz p2, :cond_2

    invoke-virtual {p5}, Landroid/text/SpannableStringBuilder;->length()I

    move-result p3

    if-le p2, p3, :cond_3

    :cond_2
    invoke-virtual {p5}, Landroid/text/SpannableStringBuilder;->length()I

    move-result p2

    :cond_3
    const/16 p3, 0x21

    invoke-virtual {p5, v0, p4, p2, p3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    new-instance p6, Landroid/text/style/UnderlineSpan;

    invoke-direct {p6}, Landroid/text/style/UnderlineSpan;-><init>()V

    invoke-virtual {p5, p6, p4, p2, p3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    invoke-static {}, Ln2/a;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    invoke-virtual {p1, p5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
