.class public Lf6/j;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf6/j$a;,
        Lf6/j$b;
    }
.end annotation


# static fields
.field public static final i:Lf6/j$a;

.field private static j:Lo5/b;


# instance fields
.field private final a:Landroid/view/View;

.field private final b:Landroid/widget/TextView;

.field private final c:Landroid/view/View;

.field private d:Ljava/lang/CharSequence;

.field private e:I

.field private f:Ljava/lang/CharSequence;

.field private g:Landroid/view/View$OnClickListener;

.field private final h:Lf6/j$b;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lf6/j$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lf6/j$a;-><init>(Lm8/g;)V

    sput-object v0, Lf6/j;->i:Lf6/j$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lf6/j$b;

    invoke-direct {v0, p0}, Lf6/j$b;-><init>(Lf6/j;)V

    iput-object v0, p0, Lf6/j;->h:Lf6/j$b;

    invoke-static {}, Lcom/android/packageinstaller/InstallerApplication;->g()Lcom/android/packageinstaller/InstallerApplication;

    move-result-object v0

    const-string v1, "getInstance()"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0d006c

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    const-string v1, "from(context).inflate(R.\u2026ayout_custom_toast, null)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lf6/j;->a:Landroid/view/View;

    const v1, 0x7f0a038a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const-string v2, "mView.findViewById(R.id.toast_msg)"

    invoke-static {v1, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lf6/j;->b:Landroid/widget/TextView;

    const v1, 0x7f0a0198

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "mView.findViewById(R.id.icon)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lf6/j;->c:Landroid/view/View;

    return-void
.end method

.method public static synthetic a(Lf6/j;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lf6/j;->j(Lf6/j;Landroid/view/View;)V

    return-void
.end method

.method public static final synthetic b()Lo5/b;
    .locals 1

    sget-object v0, Lf6/j;->j:Lo5/b;

    return-object v0
.end method

.method public static final synthetic c(Lf6/j;)Landroid/view/View;
    .locals 0

    invoke-direct {p0}, Lf6/j;->g()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic d(Lf6/j;)V
    .locals 0

    invoke-direct {p0}, Lf6/j;->i()V

    return-void
.end method

.method public static final synthetic e(Lo5/b;)V
    .locals 0

    sput-object p0, Lf6/j;->j:Lo5/b;

    return-void
.end method

.method private final f(Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)Landroid/text/Spannable;
    .locals 8

    iget-object v0, p0, Lf6/j;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1, p1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x6

    const/4 v7, 0x0

    move-object v2, p1

    move-object v3, p2

    invoke-static/range {v2 .. v7}, Lu8/g;->Q(Ljava/lang/CharSequence;Ljava/lang/String;IZILjava/lang/Object;)I

    move-result p1

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result p2

    add-int/2addr p2, p1

    const/4 v2, -0x1

    if-eq p1, v2, :cond_0

    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    if-gt p2, v2, :cond_0

    const v2, 0x7f06004a

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    const v3, 0x7f06004d

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    new-instance v3, Lf6/j$c;

    invoke-direct {v3, p3, v2, v0}, Lf6/j$c;-><init>(Landroid/view/View$OnClickListener;II)V

    const/16 p3, 0x21

    invoke-virtual {v1, v3, p1, p2, p3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :cond_0
    invoke-static {v1}, Landroid/text/SpannableString;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object p1

    const-string p2, "valueOf(this)"

    invoke-static {p1, p2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final g()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lf6/j;->a:Landroid/view/View;

    return-object v0
.end method

.method public static final h(Landroid/content/Context;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lm5/e;Landroid/view/View$OnClickListener;I)Lf6/j;
    .locals 8

    sget-object v0, Lf6/j;->i:Lf6/j$a;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move v7, p6

    invoke-virtual/range {v0 .. v7}, Lf6/j$a;->b(Landroid/content/Context;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lm5/e;Landroid/view/View$OnClickListener;I)Lf6/j;

    move-result-object p0

    return-object p0
.end method

.method private final i()V
    .locals 6

    iget v0, p0, Lf6/j;->e:I

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v2, p0, Lf6/j;->c:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setBackgroundResource(I)V

    iget-object v0, p0, Lf6/j;->c:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lf6/j;->c:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    iget-object v0, p0, Lf6/j;->d:Ljava/lang/CharSequence;

    iget-object v2, p0, Lf6/j;->f:Ljava/lang/CharSequence;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lf6/j;->a:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v3, p0, Lf6/j;->f:Ljava/lang/CharSequence;

    if-eqz v3, :cond_1

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    const v0, 0x7f1100b6

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lf6/j;->d:Ljava/lang/CharSequence;

    aput-object v5, v4, v1

    const/4 v1, 0x1

    aput-object v3, v4, v1

    invoke-virtual {v2, v0, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lf6/i;

    invoke-direct {v1, p0}, Lf6/i;-><init>(Lf6/j;)V

    invoke-direct {p0, v0, v3, v1}, Lf6/j;->f(Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)Landroid/text/Spannable;

    move-result-object v0

    :cond_1
    iget-object v1, p0, Lf6/j;->b:Landroid/widget/TextView;

    invoke-static {}, Ln2/a;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    :cond_2
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lf6/j;->b:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    return-void
.end method

.method private static final j(Lf6/j;Landroid/view/View;)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lf6/j;->g:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    :cond_0
    iget-object p0, p0, Lf6/j;->h:Lf6/j$b;

    invoke-virtual {p0}, Lf6/j$b;->d()V

    return-void
.end method


# virtual methods
.method public final k(Ljava/lang/CharSequence;)V
    .locals 1

    const-string v0, "button"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lf6/j;->f:Ljava/lang/CharSequence;

    return-void
.end method

.method public final l(Landroid/view/View$OnClickListener;)V
    .locals 1

    const-string v0, "onClickListener"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lf6/j;->g:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public final m(I)V
    .locals 0

    iput p1, p0, Lf6/j;->e:I

    return-void
.end method

.method public final n(Ljava/lang/CharSequence;)V
    .locals 1

    const-string v0, "text"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lf6/j;->d:Ljava/lang/CharSequence;

    return-void
.end method

.method public final o()V
    .locals 1

    iget-object v0, p0, Lf6/j;->h:Lf6/j$b;

    invoke-virtual {v0}, Lf6/j$b;->f()V

    return-void
.end method
