.class public final Lf6/j$a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf6/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lm8/g;)V
    .locals 0

    invoke-direct {p0}, Lf6/j$a;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/lang/CharSequence;I)Lf6/j;
    .locals 9

    const-string v0, "context"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "text"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v8, p3

    invoke-virtual/range {v1 .. v8}, Lf6/j$a;->b(Landroid/content/Context;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lm5/e;Landroid/view/View$OnClickListener;I)Lf6/j;

    move-result-object p1

    return-object p1
.end method

.method public final b(Landroid/content/Context;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lm5/e;Landroid/view/View$OnClickListener;I)Lf6/j;
    .locals 1

    const-string p7, "context"

    invoke-static {p1, p7}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p7, "text"

    invoke-static {p2, p7}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p7, Lf6/j;

    invoke-direct {p7}, Lf6/j;-><init>()V

    if-eqz p3, :cond_0

    invoke-virtual {p7, p3}, Lf6/j;->m(I)V

    :cond_0
    instance-of p1, p1, Lq2/b;

    if-nez p1, :cond_3

    new-instance p1, Lo5/b;

    const-string p3, "other_app_launch"

    const-string v0, ""

    invoke-direct {p1, p3, v0}, Lo5/b;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p1}, Lf6/j;->e(Lo5/b;)V

    invoke-static {}, Lf6/j;->b()Lo5/b;

    move-result-object p1

    if-nez p1, :cond_1

    goto :goto_1

    :cond_1
    if-eqz p5, :cond_2

    invoke-virtual {p5}, Lm5/e;->j()Ljava/lang/String;

    move-result-object p3

    goto :goto_0

    :cond_2
    const/4 p3, 0x0

    :goto_0
    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p1, p3}, Lo5/b;->q(Ljava/lang/String;)V

    :cond_3
    :goto_1
    invoke-virtual {p7, p2}, Lf6/j;->n(Ljava/lang/CharSequence;)V

    if-eqz p4, :cond_4

    invoke-virtual {p7, p4}, Lf6/j;->k(Ljava/lang/CharSequence;)V

    :cond_4
    if-eqz p6, :cond_5

    invoke-virtual {p7, p6}, Lf6/j;->l(Landroid/view/View$OnClickListener;)V

    :cond_5
    return-object p7
.end method
