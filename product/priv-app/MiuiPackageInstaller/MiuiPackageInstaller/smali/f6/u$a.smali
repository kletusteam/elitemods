.class public final Lf6/u$a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf6/u;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lm8/g;)V
    .locals 0

    invoke-direct {p0}, Lf6/u$a;-><init>()V

    return-void
.end method

.method private final b(Lr6/d;)V
    .locals 2

    :try_start_0
    sget-object v0, Lp6/a;->c:Lp6/a;

    invoke-virtual {v0, p1}, Lp6/a;->a(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "deleteSourceAuthority failed, because "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "SourceAuthorityHelper"

    invoke-static {v0, p1}, Lf6/o;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    return-void
.end method

.method private final c(Ljava/lang/String;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lr6/d;",
            ">;"
        }
    .end annotation

    :try_start_0
    sget-object v0, Lp6/a;->c:Lp6/a;

    const-class v1, Lr6/d;

    const-string v2, "package_name"

    invoke-virtual {v0, v1, v2, p1}, Lp6/a;->g(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "query getSourceAuthorityInfo failed, because "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "SourceAuthorityHelper"

    invoke-static {v0, p1}, Lf6/o;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    const/4 p1, 0x0

    return-object p1
.end method

.method private final d(Lr6/d;)V
    .locals 2

    :try_start_0
    sget-object v0, Lp6/a;->c:Lp6/a;

    invoke-virtual {v0, p1}, Lp6/a;->i(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "insertSourceAuthority failed, because "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "SourceAuthorityHelper"

    invoke-static {v0, p1}, Lf6/o;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    return-void
.end method


# virtual methods
.method public final a(Lm5/e;)V
    .locals 3

    const-string v0, "callingPackage"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lm5/e;->j()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lf6/u$a;->c(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_2

    new-instance v0, Lr6/d;

    invoke-direct {v0}, Lr6/d;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lr6/d;->b(J)V

    invoke-virtual {p1}, Lm5/e;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lr6/d;->d(Ljava/lang/String;)V

    iget-object v1, p1, Lm5/e;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lr6/d;->c(Ljava/lang/String;)V

    invoke-virtual {p1}, Lm5/e;->l()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Lr6/d;->e(Ljava/lang/Integer;)V

    invoke-direct {p0, v0}, Lf6/u$a;->d(Lr6/d;)V

    :cond_2
    return-void
.end method

.method public final e(Lm5/e;)V
    .locals 10

    const-string v0, "callingPackage"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Lcom/android/packageinstaller/InstallerApplication;->g()Lcom/android/packageinstaller/InstallerApplication;

    move-result-object v0

    const-string v1, "getInstance()"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lm2/b;->g(Landroid/content/Context;)Lm2/b;

    move-result-object v1

    invoke-virtual {v1}, Lm2/b;->n()Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/miui/packageInstaller/model/SourceAuthorityResetInfo;

    invoke-static {v1, v2}, Lcom/android/packageinstaller/utils/j;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/packageInstaller/model/SourceAuthorityResetInfo;

    if-nez v1, :cond_0

    return-void

    :cond_0
    invoke-virtual {p1}, Lm5/e;->j()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lf6/u$a;->c(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    const-string v3, "appops"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    const-string v3, "null cannot be cast to non-null type android.app.AppOpsManager"

    invoke-static {v0, v3}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/app/AppOpsManager;

    invoke-virtual {p1}, Lm5/e;->l()I

    move-result v3

    invoke-virtual {p1}, Lm5/e;->j()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x42

    invoke-static {v0, v5, v3, v4}, Lcom/android/packageinstaller/compat/AppOpsManagerCompat;->checkOpNoThrow(Landroid/app/AppOpsManager;IILjava/lang/String;)I

    move-result v3

    if-eqz v2, :cond_2

    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 v4, 0x1

    :goto_1
    if-eqz v4, :cond_3

    if-nez v3, :cond_5

    new-instance v0, Lr6/d;

    invoke-direct {v0}, Lr6/d;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lr6/d;->b(J)V

    invoke-virtual {p1}, Lm5/e;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lr6/d;->d(Ljava/lang/String;)V

    iget-object v1, p1, Lm5/e;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lr6/d;->c(Ljava/lang/String;)V

    invoke-virtual {p1}, Lm5/e;->l()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Lr6/d;->e(Ljava/lang/Integer;)V

    invoke-direct {p0, v0}, Lf6/u$a;->d(Lr6/d;)V

    goto :goto_3

    :cond_3
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lr6/d;

    :try_start_0
    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/SourceAuthorityResetInfo;->getEnable()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-virtual {v4}, Lr6/d;->a()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-eqz v6, :cond_4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {v4}, Lr6/d;->a()J

    move-result-wide v8

    sub-long/2addr v6, v8

    const-wide/32 v8, 0x5265c00

    div-long/2addr v6, v8

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/SourceAuthorityResetInfo;->getGap()Ljava/lang/Integer;

    move-result-object v8

    if-eqz v8, :cond_4

    invoke-virtual {v8}, Ljava/lang/Number;->intValue()I

    move-result v8

    int-to-long v8, v8

    cmp-long v6, v6, v8

    if-ltz v6, :cond_4

    if-nez v3, :cond_4

    invoke-virtual {p1}, Lm5/e;->l()I

    move-result v6

    invoke-virtual {p1}, Lm5/e;->j()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x2

    invoke-static {v0, v5, v6, v7, v8}, Lcom/android/packageinstaller/compat/AppOpsManagerCompat;->setMode(Landroid/app/AppOpsManager;IILjava/lang/String;I)V

    sget-object v6, Lf6/u;->a:Lf6/u$a;

    invoke-direct {v6, v4}, Lf6/u$a;->b(Lr6/d;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const-string v6, "SourceAuthorityHelper"

    invoke-static {v6, v4}, Lf6/o;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    goto :goto_2

    :cond_5
    :goto_3
    return-void
.end method
