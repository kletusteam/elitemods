.class final Lf6/a$a$a$a;
.super Lf8/k;

# interfaces
.implements Ll8/p;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf6/a$a$a;->n(Ljava/lang/Object;)Ljava/lang/Object;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf8/k;",
        "Ll8/p<",
        "Lv8/e0;",
        "Ld8/d<",
        "-",
        "La8/v;",
        ">;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation runtime Lf8/f;
    c = "com.miui.packageInstaller.util.CallingPackageUtil$Companion$doApkParser$2$parserJob$1"
    f = "CallingPackageUtil.kt"
    l = {}
    m = "invokeSuspend"
.end annotation


# instance fields
.field e:I

.field final synthetic f:Lm5/e;

.field final synthetic g:Lcom/miui/packageInstaller/model/ApkInfo;


# direct methods
.method constructor <init>(Lm5/e;Lcom/miui/packageInstaller/model/ApkInfo;Ld8/d;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lm5/e;",
            "Lcom/miui/packageInstaller/model/ApkInfo;",
            "Ld8/d<",
            "-",
            "Lf6/a$a$a$a;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lf6/a$a$a$a;->f:Lm5/e;

    iput-object p2, p0, Lf6/a$a$a$a;->g:Lcom/miui/packageInstaller/model/ApkInfo;

    const/4 p1, 0x2

    invoke-direct {p0, p1, p3}, Lf8/k;-><init>(ILd8/d;)V

    return-void
.end method


# virtual methods
.method public final b(Ljava/lang/Object;Ld8/d;)Ld8/d;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ld8/d<",
            "*>;)",
            "Ld8/d<",
            "La8/v;",
            ">;"
        }
    .end annotation

    new-instance p1, Lf6/a$a$a$a;

    iget-object v0, p0, Lf6/a$a$a$a;->f:Lm5/e;

    iget-object v1, p0, Lf6/a$a$a$a;->g:Lcom/miui/packageInstaller/model/ApkInfo;

    invoke-direct {p1, v0, v1, p2}, Lf6/a$a$a$a;-><init>(Lm5/e;Lcom/miui/packageInstaller/model/ApkInfo;Ld8/d;)V

    return-object p1
.end method

.method public bridge synthetic g(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lv8/e0;

    check-cast p2, Ld8/d;

    invoke-virtual {p0, p1, p2}, Lf6/a$a$a$a;->q(Lv8/e0;Ld8/d;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final n(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    invoke-static {}, Le8/b;->c()Ljava/lang/Object;

    iget v0, p0, Lf6/a$a$a$a;->e:I

    if-nez v0, :cond_2

    invoke-static {p1}, La8/n;->b(Ljava/lang/Object;)V

    new-instance p1, Lv5/b;

    iget-object v0, p0, Lf6/a$a$a$a;->f:Lm5/e;

    iget-object v1, p0, Lf6/a$a$a$a;->g:Lcom/miui/packageInstaller/model/ApkInfo;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/ApkInfo;->getOriginalUri()Landroid/net/Uri;

    move-result-object v1

    goto :goto_0

    :cond_0
    move-object v1, v2

    :goto_0
    invoke-static {v1}, Lm8/i;->c(Ljava/lang/Object;)V

    invoke-direct {p1, v0, v1}, Lv5/b;-><init>(Lm5/e;Landroid/net/Uri;)V

    iget-object v0, p0, Lf6/a$a$a$a;->g:Lcom/miui/packageInstaller/model/ApkInfo;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/ApkInfo;->getOriginalFilePath()Ljava/lang/String;

    move-result-object v2

    :cond_1
    iget-object v0, p0, Lf6/a$a$a$a;->g:Lcom/miui/packageInstaller/model/ApkInfo;

    invoke-static {v0}, Lm8/i;->c(Ljava/lang/Object;)V

    invoke-virtual {p1, v2, v0}, Lv5/b;->e(Ljava/lang/String;Lcom/miui/packageInstaller/model/ApkInfo;)V

    sget-object p1, La8/v;->a:La8/v;

    return-object p1

    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final q(Lv8/e0;Ld8/d;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lv8/e0;",
            "Ld8/d<",
            "-",
            "La8/v;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lf6/a$a$a$a;->b(Ljava/lang/Object;Ld8/d;)Ld8/d;

    move-result-object p1

    check-cast p1, Lf6/a$a$a$a;

    sget-object p2, La8/v;->a:La8/v;

    invoke-virtual {p1, p2}, Lf6/a$a$a$a;->n(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method
