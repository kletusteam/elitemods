.class final Lf6/j$b;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf6/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf6/j$b$b;,
        Lf6/j$b$a;
    }
.end annotation


# static fields
.field public static final f:Lf6/j$b$b;

.field private static g:Lf6/j$b;


# instance fields
.field private final a:Lf6/j;

.field private b:Z

.field private c:Landroid/view/WindowManager;

.field private d:Landroid/view/WindowManager$LayoutParams;

.field private e:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lf6/j$b$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lf6/j$b$b;-><init>(Lm8/g;)V

    sput-object v0, Lf6/j$b;->f:Lf6/j$b$b;

    return-void
.end method

.method public constructor <init>(Lf6/j;)V
    .locals 2

    const-string v0, "mToast"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf6/j$b;->a:Lf6/j;

    invoke-static {}, Lcom/android/packageinstaller/InstallerApplication;->g()Lcom/android/packageinstaller/InstallerApplication;

    move-result-object p1

    const-string v0, "window"

    invoke-virtual {p1, v0}, Landroid/app/Application;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    const-string v0, "null cannot be cast to non-null type android.view.WindowManager"

    invoke-static {p1, v0}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/view/WindowManager;

    iput-object p1, p0, Lf6/j$b;->c:Landroid/view/WindowManager;

    new-instance p1, Landroid/view/WindowManager$LayoutParams;

    const/16 v0, 0x7d8

    invoke-direct {p1, v0}, Landroid/view/WindowManager$LayoutParams;-><init>(I)V

    iput-object p1, p0, Lf6/j$b;->d:Landroid/view/WindowManager$LayoutParams;

    new-instance p1, Lf6/j$b$a;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    const-string v1, "getMainLooper()"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p1, p0, v0}, Lf6/j$b$a;-><init>(Lf6/j$b;Landroid/os/Looper;)V

    iput-object p1, p0, Lf6/j$b;->e:Landroid/os/Handler;

    iget-object p1, p0, Lf6/j$b;->d:Landroid/view/WindowManager$LayoutParams;

    const/4 v0, -0x3

    iput v0, p1, Landroid/view/WindowManager$LayoutParams;->format:I

    const/4 v0, -0x2

    iput v0, p1, Landroid/view/WindowManager$LayoutParams;->width:I

    iput v0, p1, Landroid/view/WindowManager$LayoutParams;->height:I

    const/16 v0, 0x8

    iput v0, p1, Landroid/view/WindowManager$LayoutParams;->flags:I

    const/16 v0, 0x31

    iput v0, p1, Landroid/view/WindowManager$LayoutParams;->gravity:I

    const/4 v0, 0x0

    iput v0, p1, Landroid/view/WindowManager$LayoutParams;->x:I

    const/16 v0, 0xb4

    iput v0, p1, Landroid/view/WindowManager$LayoutParams;->y:I

    const v0, 0x1030004

    iput v0, p1, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    return-void
.end method

.method public static final synthetic a(Lf6/j$b;)V
    .locals 0

    invoke-direct {p0}, Lf6/j$b;->c()V

    return-void
.end method

.method public static final synthetic b(Lf6/j$b;)V
    .locals 0

    invoke-direct {p0}, Lf6/j$b;->e()V

    return-void
.end method

.method private final c()V
    .locals 4

    iget-boolean v0, p0, Lf6/j$b;->b:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    invoke-static {}, Lf6/j;->b()Lo5/b;

    move-result-object v0

    if-eqz v0, :cond_1

    new-instance v1, Lp5/g;

    const-string v2, "forbid_install_toast"

    const-string v3, "toast"

    invoke-direct {v1, v2, v3, v0}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v1}, Lp5/f;->c()Z

    new-instance v1, Lp5/g;

    const-string v2, "forbid_install_toast_install_btn"

    const-string v3, "button"

    invoke-direct {v1, v2, v3, v0}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v1}, Lp5/f;->c()Z

    :cond_1
    sget-object v0, Lf6/j$b;->g:Lf6/j$b;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lf6/j$b;->d()V

    :cond_2
    iget-object v0, p0, Lf6/j$b;->a:Lf6/j;

    invoke-static {v0}, Lf6/j;->d(Lf6/j;)V

    iget-object v0, p0, Lf6/j$b;->c:Landroid/view/WindowManager;

    iget-object v1, p0, Lf6/j$b;->a:Lf6/j;

    invoke-static {v1}, Lf6/j;->c(Lf6/j;)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lf6/j$b;->d:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lf6/j$b;->b:Z

    sput-object p0, Lf6/j$b;->g:Lf6/j$b;

    iget-object v0, p0, Lf6/j$b;->e:Landroid/os/Handler;

    const/4 v1, 0x0

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    return-void
.end method

.method private final e()V
    .locals 2

    iget-boolean v0, p0, Lf6/j$b;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf6/j$b;->c:Landroid/view/WindowManager;

    iget-object v1, p0, Lf6/j$b;->a:Lf6/j;

    invoke-static {v1}, Lf6/j;->c(Lf6/j;)Landroid/view/View;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    :cond_0
    sget-object v0, Lf6/j$b;->g:Lf6/j$b;

    invoke-static {v0, p0}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    sput-object v0, Lf6/j$b;->g:Lf6/j$b;

    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lf6/j$b;->b:Z

    return-void
.end method


# virtual methods
.method public final d()V
    .locals 2

    iget-object v0, p0, Lf6/j$b;->e:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method public final f()V
    .locals 2

    iget-object v0, p0, Lf6/j$b;->e:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method
