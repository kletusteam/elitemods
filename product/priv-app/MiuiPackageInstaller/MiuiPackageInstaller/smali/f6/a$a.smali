.class public final Lf6/a$a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf6/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lm8/g;)V
    .locals 0

    invoke-direct {p0}, Lf6/a$a;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lm5/e;Lcom/miui/packageInstaller/model/ApkInfo;Ld8/d;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lm5/e;",
            "Lcom/miui/packageInstaller/model/ApkInfo;",
            "Ld8/d<",
            "-",
            "La8/v;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    invoke-static {}, Lv8/t0;->c()Lv8/v1;

    move-result-object v0

    new-instance v1, Lf6/a$a$a;

    const/4 v2, 0x0

    invoke-direct {v1, p1, p2, v2}, Lf6/a$a$a;-><init>(Lm5/e;Lcom/miui/packageInstaller/model/ApkInfo;Ld8/d;)V

    invoke-static {v0, v1, p3}, Lv8/f;->e(Ld8/g;Ll8/p;Ld8/d;)Ljava/lang/Object;

    move-result-object p1

    invoke-static {}, Le8/b;->c()Ljava/lang/Object;

    move-result-object p2

    if-ne p1, p2, :cond_0

    return-object p1

    :cond_0
    sget-object p1, La8/v;->a:La8/v;

    return-object p1
.end method

.method public final b(Landroid/app/Activity;)Ljava/lang/String;
    .locals 3

    const-string v0, "activity"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/app/Activity;->getCallingPackage()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    :try_start_0
    const-string v1, "android.app.Activity"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const-string v2, "mReferrer"

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    invoke-virtual {v1, p1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    const-string v1, "null cannot be cast to non-null type kotlin.String"

    invoke-static {p1, v1}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    invoke-virtual {p0}, Lf6/a$a;->e()Ljava/lang/String;

    move-result-object v1

    const-string v2, "mReferrer invoke failed"

    invoke-static {v1, v2, p1}, Lf6/o;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    return-object v0
.end method

.method public final c(Landroid/app/Activity;Landroid/content/pm/PackageManager;Landroid/content/Intent;Ljava/lang/String;)I
    .locals 2

    const-string v0, "activity"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mPm"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "intent"

    invoke-static {p3, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "android.intent.extra.ORIGINATING_UID"

    const/4 v1, -0x1

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result p3

    invoke-virtual {p0, p1, p4}, Lf6/a$a;->d(Landroid/app/Activity;Ljava/lang/String;)Landroid/content/pm/ApplicationInfo;

    move-result-object p4

    if-eqz p4, :cond_1

    if-eq p3, v1, :cond_0

    invoke-static {p4}, Lcom/android/packageinstaller/compat/ApplicationInfoCompat;->privateFlags(Landroid/content/pm/ApplicationInfo;)I

    move-result p1

    and-int/lit8 p1, p1, 0x8

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    iget p3, p4, Landroid/content/pm/ApplicationInfo;->uid:I

    :goto_0
    return p3

    :cond_1
    :try_start_0
    invoke-static {p1}, Lcom/android/packageinstaller/compat/ActivityCompat;->getActivityToken(Landroid/app/Activity;)Landroid/os/IBinder;

    move-result-object p1

    invoke-static {p1}, Lcom/android/packageinstaller/compat/ActivityManagerCompat;->getLaunchedFromUid(Landroid/os/IBinder;)I

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    if-eq p3, v1, :cond_3

    invoke-virtual {p2, p1}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object p4

    if-eqz p4, :cond_3

    invoke-static {p4}, Lm8/b;->a([Ljava/lang/Object;)Ljava/util/Iterator;

    move-result-object p4

    :catch_0
    :cond_2
    invoke-interface {p4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v1, 0x0

    :try_start_1
    invoke-virtual {p2, v0, v1}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    const-string v1, "mPm.getApplicationInfo(packageName, 0)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/android/packageinstaller/compat/ApplicationInfoCompat;->privateFlags(Landroid/content/pm/ApplicationInfo;)I

    move-result v0
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_2

    return p3

    :cond_3
    return p1

    :catch_1
    invoke-virtual {p0}, Lf6/a$a;->e()Ljava/lang/String;

    move-result-object p1

    const-string p2, "Could not determine the launching uid."

    invoke-static {p1, p2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return v1
.end method

.method public final d(Landroid/app/Activity;Ljava/lang/String;)Landroid/content/pm/ApplicationInfo;
    .locals 1

    const-string v0, "activity"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_0

    :try_start_0
    invoke-virtual {p1}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object p1
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    invoke-static {}, Lf6/a;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
