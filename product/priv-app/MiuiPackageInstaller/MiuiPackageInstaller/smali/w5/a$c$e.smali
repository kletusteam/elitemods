.class final Lw5/a$c$e;
.super Lf8/k;

# interfaces
.implements Ll8/p;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lw5/a$c;->n(Ljava/lang/Object;)Ljava/lang/Object;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf8/k;",
        "Ll8/p<",
        "Lv8/e0;",
        "Ld8/d<",
        "-",
        "La8/v;",
        ">;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation runtime Lf8/f;
    c = "com.miui.packageInstaller.presenter.PackageInstallerPresenter$launchLoadJob$1$experimentJob$1"
    f = "PackageInstallerPresenter.kt"
    l = {
        0x32
    }
    m = "invokeSuspend"
.end annotation


# instance fields
.field e:I


# direct methods
.method constructor <init>(Ld8/d;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ld8/d<",
            "-",
            "Lw5/a$c$e;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x2

    invoke-direct {p0, v0, p1}, Lf8/k;-><init>(ILd8/d;)V

    return-void
.end method


# virtual methods
.method public final b(Ljava/lang/Object;Ld8/d;)Ld8/d;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ld8/d<",
            "*>;)",
            "Ld8/d<",
            "La8/v;",
            ">;"
        }
    .end annotation

    new-instance p1, Lw5/a$c$e;

    invoke-direct {p1, p2}, Lw5/a$c$e;-><init>(Ld8/d;)V

    return-object p1
.end method

.method public bridge synthetic g(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lv8/e0;

    check-cast p2, Ld8/d;

    invoke-virtual {p0, p1, p2}, Lw5/a$c$e;->q(Lv8/e0;Ld8/d;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final n(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    invoke-static {}, Le8/b;->c()Ljava/lang/Object;

    move-result-object v0

    iget v1, p0, Lw5/a$c$e;->e:I

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    if-ne v1, v2, :cond_0

    invoke-static {p1}, La8/n;->b(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    invoke-static {p1}, La8/n;->b(Ljava/lang/Object;)V

    sget-object p1, Lt5/n;->b:Lt5/n$a;

    invoke-virtual {p1}, Lt5/n$a;->b()Lt5/n;

    move-result-object p1

    const-string v1, "installPrepare"

    filled-new-array {v1}, [Ljava/lang/String;

    move-result-object v1

    iput v2, p0, Lw5/a$c$e;->e:I

    invoke-virtual {p1, v1, p0}, Lt5/n;->j([Ljava/lang/String;Ld8/d;)Ljava/lang/Object;

    move-result-object p1

    if-ne p1, v0, :cond_2

    return-object v0

    :cond_2
    :goto_0
    sget-object p1, La8/v;->a:La8/v;

    return-object p1
.end method

.method public final q(Lv8/e0;Ld8/d;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lv8/e0;",
            "Ld8/d<",
            "-",
            "La8/v;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lw5/a$c$e;->b(Ljava/lang/Object;Ld8/d;)Ld8/d;

    move-result-object p1

    check-cast p1, Lw5/a$c$e;

    sget-object p2, La8/v;->a:La8/v;

    invoke-virtual {p1, p2}, Lw5/a$c$e;->n(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method
