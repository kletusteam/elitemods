.class final Lw5/a$c;
.super Lf8/k;

# interfaces
.implements Ll8/p;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lw5/a;->j(Lcom/miui/packageInstaller/model/ApkInfo;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf8/k;",
        "Ll8/p<",
        "Lv8/e0;",
        "Ld8/d<",
        "-",
        "La8/v;",
        ">;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation runtime Lf8/f;
    c = "com.miui.packageInstaller.presenter.PackageInstallerPresenter$launchLoadJob$1"
    f = "PackageInstallerPresenter.kt"
    l = {
        0x3f,
        0x5b,
        0x5e
    }
    m = "invokeSuspend"
.end annotation


# instance fields
.field e:Ljava/lang/Object;

.field f:Ljava/lang/Object;

.field g:I

.field private synthetic h:Ljava/lang/Object;

.field final synthetic i:Lw5/a;

.field final synthetic j:I

.field final synthetic k:Lcom/miui/packageInstaller/model/ApkInfo;


# direct methods
.method constructor <init>(Lw5/a;ILcom/miui/packageInstaller/model/ApkInfo;Ld8/d;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lw5/a;",
            "I",
            "Lcom/miui/packageInstaller/model/ApkInfo;",
            "Ld8/d<",
            "-",
            "Lw5/a$c;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lw5/a$c;->i:Lw5/a;

    iput p2, p0, Lw5/a$c;->j:I

    iput-object p3, p0, Lw5/a$c;->k:Lcom/miui/packageInstaller/model/ApkInfo;

    const/4 p1, 0x2

    invoke-direct {p0, p1, p4}, Lf8/k;-><init>(ILd8/d;)V

    return-void
.end method

.method public static synthetic q(Lm8/t;Lcom/miui/packageInstaller/model/ApkInfo;)V
    .locals 0

    invoke-static {p0, p1}, Lw5/a$c;->s(Lm8/t;Lcom/miui/packageInstaller/model/ApkInfo;)V

    return-void
.end method

.method private static final s(Lm8/t;Lcom/miui/packageInstaller/model/ApkInfo;)V
    .locals 8

    iget-object v0, p0, Lm8/t;->a:Ljava/lang/Object;

    check-cast v0, Lcom/miui/packageInstaller/model/AdModel;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/AdModel;->getData()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    iget-object p0, p0, Lm8/t;->a:Ljava/lang/Object;

    check-cast p0, Lcom/miui/packageInstaller/model/AdModel;

    if-eqz p0, :cond_1

    invoke-virtual {p0}, Lcom/miui/packageInstaller/model/AdModel;->getData()Ljava/util/List;

    move-result-object p0

    if-eqz p0, :cond_1

    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/miui/packageInstaller/model/AdModel$DesData;

    if-eqz p0, :cond_1

    invoke-virtual {p0}, Lcom/miui/packageInstaller/model/AdModel$DesData;->getData()Lcom/miui/packageInstaller/model/AdData;

    move-result-object p0

    if-eqz p0, :cond_1

    invoke-virtual {p0}, Lcom/miui/packageInstaller/model/AdData;->getAppId()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p0

    goto :goto_1

    :cond_1
    const/4 p0, 0x0

    :goto_1
    move-object v6, p0

    sget-object v0, Ln5/e;->a:Ln5/e;

    invoke-virtual {p1}, Lcom/miui/packageInstaller/model/ApkInfo;->getLabel()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/miui/packageInstaller/model/ApkInfo;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/miui/packageInstaller/model/ApkInfo;->getLabel()Ljava/lang/String;

    move-result-object p0

    if-nez p0, :cond_2

    sget-object p0, Lcom/android/packageinstaller/InstallerApplication;->c:Lcom/android/packageinstaller/InstallerApplication;

    const v3, 0x7f1101dc

    invoke-virtual {p0, v3}, Landroid/app/Application;->getString(I)Ljava/lang/String;

    move-result-object p0

    const-string v3, "sInstance.getString(R.st\u2026ui_install_source_unkown)"

    invoke-static {p0, v3}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_2
    move-object v3, p0

    invoke-virtual {p1}, Lcom/miui/packageInstaller/model/ApkInfo;->getNewInstall()I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v7, 0x0

    const-string v5, "08-0"

    invoke-virtual/range {v0 .. v7}, Ln5/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ln5/a;)V

    return-void
.end method


# virtual methods
.method public final b(Ljava/lang/Object;Ld8/d;)Ld8/d;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ld8/d<",
            "*>;)",
            "Ld8/d<",
            "La8/v;",
            ">;"
        }
    .end annotation

    new-instance v0, Lw5/a$c;

    iget-object v1, p0, Lw5/a$c;->i:Lw5/a;

    iget v2, p0, Lw5/a$c;->j:I

    iget-object v3, p0, Lw5/a$c;->k:Lcom/miui/packageInstaller/model/ApkInfo;

    invoke-direct {v0, v1, v2, v3, p2}, Lw5/a$c;-><init>(Lw5/a;ILcom/miui/packageInstaller/model/ApkInfo;Ld8/d;)V

    iput-object p1, v0, Lw5/a$c;->h:Ljava/lang/Object;

    return-object v0
.end method

.method public bridge synthetic g(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lv8/e0;

    check-cast p2, Ld8/d;

    invoke-virtual {p0, p1, p2}, Lw5/a$c;->r(Lv8/e0;Ld8/d;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final n(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 18

    move-object/from16 v1, p0

    invoke-static {}, Le8/b;->c()Ljava/lang/Object;

    move-result-object v2

    iget v0, v1, Lw5/a$c;->g:I

    const/4 v3, 0x3

    const/4 v4, 0x2

    const/4 v5, 0x1

    const/4 v6, 0x0

    if-eqz v0, :cond_3

    if-eq v0, v5, :cond_2

    if-eq v0, v4, :cond_1

    if-ne v0, v3, :cond_0

    iget-object v0, v1, Lw5/a$c;->h:Ljava/lang/Object;

    check-cast v0, Lcom/miui/packageInstaller/model/Virus;

    invoke-static/range {p1 .. p1}, La8/n;->b(Ljava/lang/Object;)V

    move-object v3, v0

    move-object/from16 v0, p1

    goto/16 :goto_8

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, v1, Lw5/a$c;->h:Ljava/lang/Object;

    check-cast v0, Lm8/t;

    invoke-static/range {p1 .. p1}, La8/n;->b(Ljava/lang/Object;)V

    move-object/from16 v3, p1

    goto/16 :goto_6

    :cond_2
    iget-object v0, v1, Lw5/a$c;->f:Ljava/lang/Object;

    move-object v5, v0

    check-cast v5, Lm8/t;

    iget-object v0, v1, Lw5/a$c;->e:Ljava/lang/Object;

    move-object v7, v0

    check-cast v7, Lm8/t;

    iget-object v0, v1, Lw5/a$c;->h:Ljava/lang/Object;

    move-object v8, v0

    check-cast v8, Lv8/l0;

    :try_start_0
    invoke-static/range {p1 .. p1}, La8/n;->b(Ljava/lang/Object;)V
    :try_end_0
    .catch Lv8/g2; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_4

    :catch_0
    move-exception v0

    goto/16 :goto_3

    :cond_3
    invoke-static/range {p1 .. p1}, La8/n;->b(Ljava/lang/Object;)V

    iget-object v0, v1, Lw5/a$c;->h:Ljava/lang/Object;

    check-cast v0, Lv8/e0;

    new-instance v13, Lm8/t;

    invoke-direct {v13}, Lm8/t;-><init>()V

    invoke-static {}, Lv8/t0;->b()Lv8/a0;

    move-result-object v8

    const/4 v9, 0x0

    new-instance v10, Lw5/a$c$f;

    iget-object v7, v1, Lw5/a$c;->i:Lw5/a;

    iget v11, v1, Lw5/a$c;->j:I

    iget-object v12, v1, Lw5/a$c;->k:Lcom/miui/packageInstaller/model/ApkInfo;

    invoke-direct {v10, v7, v11, v12, v6}, Lw5/a$c$f;-><init>(Lw5/a;ILcom/miui/packageInstaller/model/ApkInfo;Ld8/d;)V

    const/4 v11, 0x2

    const/4 v12, 0x0

    move-object v7, v0

    invoke-static/range {v7 .. v12}, Lv8/f;->b(Lv8/e0;Ld8/g;Lv8/g0;Ll8/p;ILjava/lang/Object;)Lv8/l0;

    move-result-object v7

    iput-object v7, v13, Lm8/t;->a:Ljava/lang/Object;

    iget-object v7, v1, Lw5/a$c;->i:Lw5/a;

    invoke-virtual {v7}, Lw5/a;->g()Lw5/a$a;

    move-result-object v7

    invoke-interface {v7}, Lw5/a$a;->c()Lm5/e;

    move-result-object v7

    if-eqz v7, :cond_4

    invoke-virtual {v7}, Lm5/e;->j()Ljava/lang/String;

    move-result-object v7

    goto :goto_0

    :cond_4
    move-object v7, v6

    :goto_0
    if-nez v7, :cond_5

    const-string v7, ""

    :cond_5
    move-object v14, v7

    new-instance v15, Lm8/t;

    invoke-direct {v15}, Lm8/t;-><init>()V

    invoke-static {}, Lv8/t0;->b()Lv8/a0;

    move-result-object v8

    const/4 v9, 0x0

    new-instance v10, Lw5/a$c$d;

    iget-object v7, v1, Lw5/a$c;->k:Lcom/miui/packageInstaller/model/ApkInfo;

    invoke-direct {v10, v7, v14, v6}, Lw5/a$c$d;-><init>(Lcom/miui/packageInstaller/model/ApkInfo;Ljava/lang/String;Ld8/d;)V

    const/4 v11, 0x2

    const/4 v12, 0x0

    move-object v7, v0

    invoke-static/range {v7 .. v12}, Lv8/f;->b(Lv8/e0;Ld8/g;Lv8/g0;Ll8/p;ILjava/lang/Object;)Lv8/l0;

    move-result-object v7

    iput-object v7, v15, Lm8/t;->a:Ljava/lang/Object;

    new-instance v12, Lm8/t;

    invoke-direct {v12}, Lm8/t;-><init>()V

    invoke-static {}, Lv8/t0;->b()Lv8/a0;

    move-result-object v8

    new-instance v10, Lw5/a$c$e;

    invoke-direct {v10, v6}, Lw5/a$c$e;-><init>(Ld8/d;)V

    const/16 v16, 0x0

    move-object v7, v0

    move-object v3, v12

    move-object/from16 v12, v16

    invoke-static/range {v7 .. v12}, Lv8/f;->b(Lv8/e0;Ld8/g;Lv8/g0;Ll8/p;ILjava/lang/Object;)Lv8/l0;

    move-result-object v7

    iput-object v7, v3, Lm8/t;->a:Ljava/lang/Object;

    iget-object v7, v1, Lw5/a$c;->i:Lw5/a;

    invoke-virtual {v7}, Lw5/a;->g()Lw5/a$a;

    move-result-object v7

    invoke-interface {v7}, Lw5/a$a;->O()V

    iget-object v7, v1, Lw5/a$c;->i:Lw5/a;

    invoke-static {v7}, Lw5/a;->b(Lw5/a;)Z

    move-result v7

    if-eqz v7, :cond_6

    invoke-static {}, Lv8/t0;->b()Lv8/a0;

    move-result-object v8

    const/4 v9, 0x0

    new-instance v10, Lw5/a$c$a;

    iget-object v7, v1, Lw5/a$c;->k:Lcom/miui/packageInstaller/model/ApkInfo;

    invoke-direct {v10, v14, v7, v6}, Lw5/a$c$a;-><init>(Ljava/lang/String;Lcom/miui/packageInstaller/model/ApkInfo;Ld8/d;)V

    const/4 v11, 0x2

    const/4 v12, 0x0

    move-object v7, v0

    invoke-static/range {v7 .. v12}, Lv8/f;->b(Lv8/e0;Ld8/g;Lv8/g0;Ll8/p;ILjava/lang/Object;)Lv8/l0;

    move-result-object v0

    move-object v14, v0

    goto :goto_1

    :cond_6
    move-object v14, v6

    :goto_1
    new-instance v12, Lm8/t;

    invoke-direct {v12}, Lm8/t;-><init>()V

    new-instance v11, Lm8/t;

    invoke-direct {v11}, Lm8/t;-><init>()V

    const-wide/16 v9, 0x4e20

    :try_start_1
    new-instance v0, Lw5/a$c$b;

    iget-object v8, v1, Lw5/a$c;->i:Lw5/a;
    :try_end_1
    .catch Lv8/g2; {:try_start_1 .. :try_end_1} :catch_2

    const/16 v16, 0x0

    move-object v7, v0

    move-object/from16 v17, v8

    move-object v8, v12

    move-object v9, v13

    move-object v10, v11

    move-object v13, v11

    move-object v11, v15

    move-object v15, v12

    move-object v12, v3

    move-object v3, v13

    move-object/from16 v13, v17

    move-object v4, v14

    move-object/from16 v14, v16

    :try_start_2
    invoke-direct/range {v7 .. v14}, Lw5/a$c$b;-><init>(Lm8/t;Lm8/t;Lm8/t;Lm8/t;Lm8/t;Lw5/a;Ld8/d;)V

    iput-object v4, v1, Lw5/a$c;->h:Ljava/lang/Object;

    iput-object v15, v1, Lw5/a$c;->e:Ljava/lang/Object;

    iput-object v3, v1, Lw5/a$c;->f:Ljava/lang/Object;

    iput v5, v1, Lw5/a$c;->g:I

    const-wide/16 v7, 0x4e20

    invoke-static {v7, v8, v0, v1}, Lv8/i2;->c(JLl8/p;Ld8/d;)Ljava/lang/Object;

    move-result-object v0
    :try_end_2
    .catch Lv8/g2; {:try_start_2 .. :try_end_2} :catch_1

    if-ne v0, v2, :cond_7

    return-object v2

    :cond_7
    move-object v5, v3

    move-object v8, v4

    move-object v7, v15

    goto :goto_4

    :catch_1
    move-exception v0

    goto :goto_2

    :catch_2
    move-exception v0

    move-object v3, v11

    move-object v15, v12

    move-object v4, v14

    :goto_2
    move-object v5, v3

    move-object v8, v4

    move-object v7, v15

    :goto_3
    const-string v3, "Presenter"

    const-string v4, "request as time out"

    invoke-static {v3, v4, v0}, Lf6/o;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    iget-object v0, v1, Lw5/a$c;->i:Lw5/a;

    iget-object v3, v7, Lm8/t;->a:Ljava/lang/Object;

    check-cast v3, Lcom/miui/packageInstaller/model/CloudParams;

    invoke-static {v0, v3}, Lw5/a;->d(Lw5/a;Lcom/miui/packageInstaller/model/CloudParams;)V

    :goto_4
    move-object v0, v7

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object v3

    iget-object v4, v1, Lw5/a$c;->k:Lcom/miui/packageInstaller/model/ApkInfo;

    new-instance v7, Lw5/b;

    invoke-direct {v7, v5, v4}, Lw5/b;-><init>(Lm8/t;Lcom/miui/packageInstaller/model/ApkInfo;)V

    invoke-virtual {v3, v7}, Lf6/z;->g(Ljava/lang/Runnable;)V

    iget-object v3, v0, Lm8/t;->a:Ljava/lang/Object;

    check-cast v3, Lcom/miui/packageInstaller/model/CloudParams;

    if-eqz v3, :cond_8

    iget-object v3, v3, Lcom/miui/packageInstaller/model/CloudParams;->categoryAbbreviation:Ljava/lang/String;

    goto :goto_5

    :cond_8
    move-object v3, v6

    :goto_5
    const-string v4, "500_error"

    invoke-static {v3, v4}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_a

    if-eqz v8, :cond_a

    iput-object v0, v1, Lw5/a$c;->h:Ljava/lang/Object;

    iput-object v6, v1, Lw5/a$c;->e:Ljava/lang/Object;

    iput-object v6, v1, Lw5/a$c;->f:Ljava/lang/Object;

    const/4 v3, 0x2

    iput v3, v1, Lw5/a$c;->g:I

    invoke-interface {v8, v1}, Lv8/l0;->z(Ld8/d;)Ljava/lang/Object;

    move-result-object v3

    if-ne v3, v2, :cond_9

    return-object v2

    :cond_9
    :goto_6
    check-cast v3, Lcom/miui/packageInstaller/model/Virus;

    goto :goto_7

    :cond_a
    move-object v3, v6

    :goto_7
    iget-object v0, v0, Lm8/t;->a:Ljava/lang/Object;

    check-cast v0, Lcom/miui/packageInstaller/model/CloudParams;

    if-eqz v0, :cond_c

    iget-object v0, v0, Lcom/miui/packageInstaller/model/CloudParams;->uiConfig:Lcom/miui/packageInstaller/model/UiConfig;

    if-eqz v0, :cond_c

    iget v0, v0, Lcom/miui/packageInstaller/model/UiConfig;->t:I

    iget-object v4, v1, Lw5/a$c;->k:Lcom/miui/packageInstaller/model/ApkInfo;

    invoke-static {}, Lv8/t0;->b()Lv8/a0;

    move-result-object v5

    new-instance v7, Lw5/a$c$c;

    invoke-direct {v7, v4, v0, v6}, Lw5/a$c$c;-><init>(Lcom/miui/packageInstaller/model/ApkInfo;ILd8/d;)V

    iput-object v3, v1, Lw5/a$c;->h:Ljava/lang/Object;

    iput-object v6, v1, Lw5/a$c;->e:Ljava/lang/Object;

    iput-object v6, v1, Lw5/a$c;->f:Ljava/lang/Object;

    const/4 v4, 0x3

    iput v4, v1, Lw5/a$c;->g:I

    invoke-static {v5, v7, v1}, Lv8/f;->e(Ld8/g;Ll8/p;Ld8/d;)Ljava/lang/Object;

    move-result-object v0

    if-ne v0, v2, :cond_b

    return-object v2

    :cond_b
    :goto_8
    check-cast v0, Ljava/lang/String;

    :cond_c
    iget-object v0, v1, Lw5/a$c;->i:Lw5/a;

    invoke-static {v0, v3}, Lw5/a;->c(Lw5/a;Lcom/miui/packageInstaller/model/Virus;)V

    sget-object v0, La8/v;->a:La8/v;

    return-object v0
.end method

.method public final r(Lv8/e0;Ld8/d;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lv8/e0;",
            "Ld8/d<",
            "-",
            "La8/v;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lw5/a$c;->b(Ljava/lang/Object;Ld8/d;)Ld8/d;

    move-result-object p1

    check-cast p1, Lw5/a$c;

    sget-object p2, La8/v;->a:La8/v;

    invoke-virtual {p1, p2}, Lw5/a$c;->n(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method
