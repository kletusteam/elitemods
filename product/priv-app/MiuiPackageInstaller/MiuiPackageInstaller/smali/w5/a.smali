.class public final Lw5/a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lw5/a$a;
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ll6/d;

.field private c:Lw5/a$a;

.field private d:Lcom/miui/packageInstaller/model/ApkInfo;

.field private final e:Lb6/r;

.field private f:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ll6/d;Lw5/a$a;)V
    .locals 1

    const-string v0, "mContext"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mActionDelegateProvider"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mView"

    invoke-static {p3, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lw5/a;->a:Landroid/content/Context;

    iput-object p2, p0, Lw5/a;->b:Ll6/d;

    iput-object p3, p0, Lw5/a;->c:Lw5/a$a;

    invoke-direct {p0}, Lw5/a;->f()Lb6/r;

    move-result-object p1

    iput-object p1, p0, Lw5/a;->e:Lb6/r;

    return-void
.end method

.method public static final synthetic a(Lw5/a;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lw5/a;->a:Landroid/content/Context;

    return-object p0
.end method

.method public static final synthetic b(Lw5/a;)Z
    .locals 0

    invoke-direct {p0}, Lw5/a;->i()Z

    move-result p0

    return p0
.end method

.method public static final synthetic c(Lw5/a;Lcom/miui/packageInstaller/model/Virus;)V
    .locals 0

    invoke-direct {p0, p1}, Lw5/a;->k(Lcom/miui/packageInstaller/model/Virus;)V

    return-void
.end method

.method public static final synthetic d(Lw5/a;Lcom/miui/packageInstaller/model/CloudParams;)V
    .locals 0

    invoke-direct {p0, p1}, Lw5/a;->m(Lcom/miui/packageInstaller/model/CloudParams;)V

    return-void
.end method

.method private final f()Lb6/r;
    .locals 3

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result v0

    iget-object v1, p0, Lw5/a;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->isSafeModelEnable(Landroid/content/Context;)Z

    move-result v1

    if-eqz v0, :cond_1

    if-eqz v1, :cond_0

    new-instance v0, Lb6/b;

    iget-object v1, p0, Lw5/a;->a:Landroid/content/Context;

    iget-object v2, p0, Lw5/a;->b:Ll6/d;

    invoke-direct {v0, v1, v2}, Lb6/b;-><init>(Landroid/content/Context;Ll6/c;)V

    goto :goto_0

    :cond_0
    new-instance v0, Lb6/e;

    iget-object v1, p0, Lw5/a;->a:Landroid/content/Context;

    iget-object v2, p0, Lw5/a;->b:Ll6/d;

    invoke-direct {v0, v1, v2}, Lb6/e;-><init>(Landroid/content/Context;Ll6/c;)V

    :goto_0
    return-object v0

    :cond_1
    if-eqz v1, :cond_2

    new-instance v0, Lb6/l;

    iget-object v1, p0, Lw5/a;->a:Landroid/content/Context;

    iget-object v2, p0, Lw5/a;->b:Ll6/d;

    invoke-direct {v0, v1, v2}, Lb6/l;-><init>(Landroid/content/Context;Ll6/c;)V

    goto :goto_1

    :cond_2
    new-instance v0, Lb6/r;

    iget-object v1, p0, Lw5/a;->a:Landroid/content/Context;

    iget-object v2, p0, Lw5/a;->b:Ll6/d;

    invoke-direct {v0, v1, v2}, Lb6/r;-><init>(Landroid/content/Context;Ll6/c;)V

    :goto_1
    return-object v0
.end method

.method private final h()Z
    .locals 2

    iget-object v0, p0, Lw5/a;->a:Landroid/content/Context;

    const-string v1, "null cannot be cast to non-null type android.app.Activity"

    invoke-static {v0, v1}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isDestroyed()Z

    move-result v0

    return v0
.end method

.method private final i()Z
    .locals 1

    iget-object v0, p0, Lw5/a;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->isInstallRiskEnabled(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lw5/a;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->isSafeModelEnable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method private final k(Lcom/miui/packageInstaller/model/Virus;)V
    .locals 1

    invoke-direct {p0}, Lw5/a;->h()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lw5/a;->c:Lw5/a$a;

    invoke-interface {v0, p1}, Lw5/a$a;->r(Lcom/miui/packageInstaller/model/Virus;)V

    :cond_0
    return-void
.end method

.method private final m(Lcom/miui/packageInstaller/model/CloudParams;)V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lw5/a;->f:Z

    iget-object v0, p0, Lw5/a;->d:Lcom/miui/packageInstaller/model/ApkInfo;

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {v0, p1}, Lcom/miui/packageInstaller/model/ApkInfo;->setCloudParams(Lcom/miui/packageInstaller/model/CloudParams;)V

    :goto_0
    iget-object p1, p0, Lw5/a;->c:Lw5/a$a;

    iget-object v0, p0, Lw5/a;->d:Lcom/miui/packageInstaller/model/ApkInfo;

    invoke-interface {p1, v0}, Lw5/a$a;->m(Lcom/miui/packageInstaller/model/ApkInfo;)V

    return-void
.end method


# virtual methods
.method public final e()V
    .locals 4

    invoke-direct {p0}, Lw5/a;->h()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lw5/a;->d:Lcom/miui/packageInstaller/model/ApkInfo;

    if-nez v0, :cond_0

    goto :goto_1

    :cond_0
    iget-object v1, p0, Lw5/a;->e:Lb6/r;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/ApkInfo;->getCloudParams()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lw5/a;->d:Lcom/miui/packageInstaller/model/ApkInfo;

    invoke-static {v2}, Lm8/i;->c(Ljava/lang/Object;)V

    new-instance v3, Lw5/a$b;

    invoke-direct {v3, p0}, Lw5/a$b;-><init>(Lw5/a;)V

    invoke-virtual {v1, v0, v2, v3}, Lb6/r;->n(Lcom/miui/packageInstaller/model/CloudParams;Lcom/miui/packageInstaller/model/ApkInfo;Lb6/r$a;)V

    :cond_2
    :goto_1
    return-void
.end method

.method public final g()Lw5/a$a;
    .locals 1

    iget-object v0, p0, Lw5/a;->c:Lw5/a$a;

    return-object v0
.end method

.method public final j(Lcom/miui/packageInstaller/model/ApkInfo;I)V
    .locals 7

    const-string v0, "apk"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lw5/a;->d:Lcom/miui/packageInstaller/model/ApkInfo;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lw5/a;->f:Z

    iget-object v0, p0, Lw5/a;->a:Landroid/content/Context;

    const-string v1, "null cannot be cast to non-null type com.miui.packageInstaller.InstallerPrepareActivity"

    invoke-static {v0, v1}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lm5/z0;

    invoke-static {v0}, Landroidx/lifecycle/k;->a(Landroidx/lifecycle/j;)Landroidx/lifecycle/e;

    move-result-object v1

    invoke-static {}, Lv8/t0;->c()Lv8/v1;

    move-result-object v2

    new-instance v4, Lw5/a$c;

    const/4 v0, 0x0

    invoke-direct {v4, p0, p2, p1, v0}, Lw5/a$c;-><init>(Lw5/a;ILcom/miui/packageInstaller/model/ApkInfo;Ld8/d;)V

    const/4 v3, 0x0

    const/4 v5, 0x2

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lv8/f;->d(Lv8/e0;Ld8/g;Lv8/g0;Ll8/p;ILjava/lang/Object;)Lv8/k1;

    return-void
.end method

.method public final l(Lcom/miui/packageInstaller/model/ApkInfo;I)V
    .locals 1

    const-string v0, "apk"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-boolean v0, p0, Lw5/a;->f:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0, p1, p2}, Lw5/a;->j(Lcom/miui/packageInstaller/model/ApkInfo;I)V

    return-void
.end method
