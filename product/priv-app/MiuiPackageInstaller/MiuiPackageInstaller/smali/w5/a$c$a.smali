.class final Lw5/a$c$a;
.super Lf8/k;

# interfaces
.implements Ll8/p;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lw5/a$c;->n(Ljava/lang/Object;)Ljava/lang/Object;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf8/k;",
        "Ll8/p<",
        "Lv8/e0;",
        "Ld8/d<",
        "-",
        "Lcom/miui/packageInstaller/model/Virus;",
        ">;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation runtime Lf8/f;
    c = "com.miui.packageInstaller.presenter.PackageInstallerPresenter$launchLoadJob$1$1"
    f = "PackageInstallerPresenter.kt"
    l = {
        0x38
    }
    m = "invokeSuspend"
.end annotation


# instance fields
.field e:I

.field final synthetic f:Ljava/lang/String;

.field final synthetic g:Lcom/miui/packageInstaller/model/ApkInfo;


# direct methods
.method constructor <init>(Ljava/lang/String;Lcom/miui/packageInstaller/model/ApkInfo;Ld8/d;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/miui/packageInstaller/model/ApkInfo;",
            "Ld8/d<",
            "-",
            "Lw5/a$c$a;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lw5/a$c$a;->f:Ljava/lang/String;

    iput-object p2, p0, Lw5/a$c$a;->g:Lcom/miui/packageInstaller/model/ApkInfo;

    const/4 p1, 0x2

    invoke-direct {p0, p1, p3}, Lf8/k;-><init>(ILd8/d;)V

    return-void
.end method


# virtual methods
.method public final b(Ljava/lang/Object;Ld8/d;)Ld8/d;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ld8/d<",
            "*>;)",
            "Ld8/d<",
            "La8/v;",
            ">;"
        }
    .end annotation

    new-instance p1, Lw5/a$c$a;

    iget-object v0, p0, Lw5/a$c$a;->f:Ljava/lang/String;

    iget-object v1, p0, Lw5/a$c$a;->g:Lcom/miui/packageInstaller/model/ApkInfo;

    invoke-direct {p1, v0, v1, p2}, Lw5/a$c$a;-><init>(Ljava/lang/String;Lcom/miui/packageInstaller/model/ApkInfo;Ld8/d;)V

    return-object p1
.end method

.method public bridge synthetic g(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lv8/e0;

    check-cast p2, Ld8/d;

    invoke-virtual {p0, p1, p2}, Lw5/a$c$a;->q(Lv8/e0;Ld8/d;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final n(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    invoke-static {}, Le8/b;->c()Ljava/lang/Object;

    move-result-object v0

    iget v1, p0, Lw5/a$c$a;->e:I

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    if-ne v1, v2, :cond_0

    invoke-static {p1}, La8/n;->b(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    invoke-static {p1}, La8/n;->b(Ljava/lang/Object;)V

    new-instance p1, Lm5/e1;

    invoke-direct {p1}, Lm5/e1;-><init>()V

    iget-object v1, p0, Lw5/a$c$a;->f:Ljava/lang/String;

    iget-object v3, p0, Lw5/a$c$a;->g:Lcom/miui/packageInstaller/model/ApkInfo;

    iput v2, p0, Lw5/a$c$a;->e:I

    invoke-virtual {p1, v1, v3, p0}, Lm5/e1;->b(Ljava/lang/String;Lcom/miui/packageInstaller/model/ApkInfo;Ld8/d;)Ljava/lang/Object;

    move-result-object p1

    if-ne p1, v0, :cond_2

    return-object v0

    :cond_2
    :goto_0
    return-object p1
.end method

.method public final q(Lv8/e0;Ld8/d;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lv8/e0;",
            "Ld8/d<",
            "-",
            "Lcom/miui/packageInstaller/model/Virus;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lw5/a$c$a;->b(Ljava/lang/Object;Ld8/d;)Ld8/d;

    move-result-object p1

    check-cast p1, Lw5/a$c$a;

    sget-object p2, La8/v;->a:La8/v;

    invoke-virtual {p1, p2}, Lw5/a$c$a;->n(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method
