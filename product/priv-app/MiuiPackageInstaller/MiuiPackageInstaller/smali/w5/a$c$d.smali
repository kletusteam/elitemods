.class final Lw5/a$c$d;
.super Lf8/k;

# interfaces
.implements Ll8/p;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lw5/a$c;->n(Ljava/lang/Object;)Ljava/lang/Object;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf8/k;",
        "Ll8/p<",
        "Lv8/e0;",
        "Ld8/d<",
        "-",
        "Lcom/miui/packageInstaller/model/AdModel;",
        ">;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation runtime Lf8/f;
    c = "com.miui.packageInstaller.presenter.PackageInstallerPresenter$launchLoadJob$1$adJob$1"
    f = "PackageInstallerPresenter.kt"
    l = {
        0x29
    }
    m = "invokeSuspend"
.end annotation


# instance fields
.field e:I

.field final synthetic f:Lcom/miui/packageInstaller/model/ApkInfo;

.field final synthetic g:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/miui/packageInstaller/model/ApkInfo;Ljava/lang/String;Ld8/d;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/miui/packageInstaller/model/ApkInfo;",
            "Ljava/lang/String;",
            "Ld8/d<",
            "-",
            "Lw5/a$c$d;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lw5/a$c$d;->f:Lcom/miui/packageInstaller/model/ApkInfo;

    iput-object p2, p0, Lw5/a$c$d;->g:Ljava/lang/String;

    const/4 p1, 0x2

    invoke-direct {p0, p1, p3}, Lf8/k;-><init>(ILd8/d;)V

    return-void
.end method


# virtual methods
.method public final b(Ljava/lang/Object;Ld8/d;)Ld8/d;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ld8/d<",
            "*>;)",
            "Ld8/d<",
            "La8/v;",
            ">;"
        }
    .end annotation

    new-instance p1, Lw5/a$c$d;

    iget-object v0, p0, Lw5/a$c$d;->f:Lcom/miui/packageInstaller/model/ApkInfo;

    iget-object v1, p0, Lw5/a$c$d;->g:Ljava/lang/String;

    invoke-direct {p1, v0, v1, p2}, Lw5/a$c$d;-><init>(Lcom/miui/packageInstaller/model/ApkInfo;Ljava/lang/String;Ld8/d;)V

    return-object p1
.end method

.method public bridge synthetic g(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lv8/e0;

    check-cast p2, Ld8/d;

    invoke-virtual {p0, p1, p2}, Lw5/a$c$d;->q(Lv8/e0;Ld8/d;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final n(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 11

    invoke-static {}, Le8/b;->c()Ljava/lang/Object;

    move-result-object v0

    iget v1, p0, Lw5/a$c$d;->e:I

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    if-ne v1, v2, :cond_0

    invoke-static {p1}, La8/n;->b(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    invoke-static {p1}, La8/n;->b(Ljava/lang/Object;)V

    sget-object v1, Ln5/e;->a:Ln5/e;

    iget-object p1, p0, Lw5/a$c$d;->f:Lcom/miui/packageInstaller/model/ApkInfo;

    invoke-virtual {p1}, Lcom/miui/packageInstaller/model/ApkInfo;->getLabel()Ljava/lang/String;

    move-result-object p1

    iget-object v3, p0, Lw5/a$c$d;->f:Lcom/miui/packageInstaller/model/ApkInfo;

    invoke-virtual {v3}, Lcom/miui/packageInstaller/model/ApkInfo;->getPackageName()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lw5/a$c$d;->g:Ljava/lang/String;

    iget-object v5, p0, Lw5/a$c$d;->f:Lcom/miui/packageInstaller/model/ApkInfo;

    invoke-virtual {v5}, Lcom/miui/packageInstaller/model/ApkInfo;->getNewInstall()I

    move-result v5

    invoke-static {v5}, Lf8/b;->b(I)Ljava/lang/Integer;

    move-result-object v5

    const/4 v7, 0x0

    const/16 v9, 0x20

    const/4 v10, 0x0

    iput v2, p0, Lw5/a$c$d;->e:I

    const-string v6, "08-1"

    move-object v2, p1

    move-object v8, p0

    invoke-static/range {v1 .. v10}, Ln5/e;->h(Ln5/e;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ld8/d;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    if-ne p1, v0, :cond_2

    return-object v0

    :cond_2
    :goto_0
    return-object p1
.end method

.method public final q(Lv8/e0;Ld8/d;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lv8/e0;",
            "Ld8/d<",
            "-",
            "Lcom/miui/packageInstaller/model/AdModel;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lw5/a$c$d;->b(Ljava/lang/Object;Ld8/d;)Ld8/d;

    move-result-object p1

    check-cast p1, Lw5/a$c$d;

    sget-object p2, La8/v;->a:La8/v;

    invoke-virtual {p1, p2}, Lw5/a$c$d;->n(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method
