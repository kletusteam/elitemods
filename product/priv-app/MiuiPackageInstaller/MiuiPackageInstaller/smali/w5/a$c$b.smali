.class final Lw5/a$c$b;
.super Lf8/k;

# interfaces
.implements Ll8/p;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lw5/a$c;->n(Ljava/lang/Object;)Ljava/lang/Object;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf8/k;",
        "Ll8/p<",
        "Lv8/e0;",
        "Ld8/d<",
        "-",
        "La8/v;",
        ">;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation runtime Lf8/f;
    c = "com.miui.packageInstaller.presenter.PackageInstallerPresenter$launchLoadJob$1$2"
    f = "PackageInstallerPresenter.kt"
    l = {
        0x40,
        0x41,
        0x42
    }
    m = "invokeSuspend"
.end annotation


# instance fields
.field e:Ljava/lang/Object;

.field f:I

.field final synthetic g:Lm8/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lm8/t<",
            "Lcom/miui/packageInstaller/model/CloudParams;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic h:Lm8/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lm8/t<",
            "Lv8/l0<",
            "Lcom/miui/packageInstaller/model/CloudParams;",
            ">;>;"
        }
    .end annotation
.end field

.field final synthetic i:Lm8/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lm8/t<",
            "Lcom/miui/packageInstaller/model/AdModel;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic j:Lm8/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lm8/t<",
            "Lv8/l0<",
            "Lcom/miui/packageInstaller/model/AdModel;",
            ">;>;"
        }
    .end annotation
.end field

.field final synthetic k:Lm8/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lm8/t<",
            "Lv8/l0<",
            "La8/v;",
            ">;>;"
        }
    .end annotation
.end field

.field final synthetic l:Lw5/a;


# direct methods
.method constructor <init>(Lm8/t;Lm8/t;Lm8/t;Lm8/t;Lm8/t;Lw5/a;Ld8/d;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lm8/t<",
            "Lcom/miui/packageInstaller/model/CloudParams;",
            ">;",
            "Lm8/t<",
            "Lv8/l0<",
            "Lcom/miui/packageInstaller/model/CloudParams;",
            ">;>;",
            "Lm8/t<",
            "Lcom/miui/packageInstaller/model/AdModel;",
            ">;",
            "Lm8/t<",
            "Lv8/l0<",
            "Lcom/miui/packageInstaller/model/AdModel;",
            ">;>;",
            "Lm8/t<",
            "Lv8/l0<",
            "La8/v;",
            ">;>;",
            "Lw5/a;",
            "Ld8/d<",
            "-",
            "Lw5/a$c$b;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lw5/a$c$b;->g:Lm8/t;

    iput-object p2, p0, Lw5/a$c$b;->h:Lm8/t;

    iput-object p3, p0, Lw5/a$c$b;->i:Lm8/t;

    iput-object p4, p0, Lw5/a$c$b;->j:Lm8/t;

    iput-object p5, p0, Lw5/a$c$b;->k:Lm8/t;

    iput-object p6, p0, Lw5/a$c$b;->l:Lw5/a;

    const/4 p1, 0x2

    invoke-direct {p0, p1, p7}, Lf8/k;-><init>(ILd8/d;)V

    return-void
.end method


# virtual methods
.method public final b(Ljava/lang/Object;Ld8/d;)Ld8/d;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ld8/d<",
            "*>;)",
            "Ld8/d<",
            "La8/v;",
            ">;"
        }
    .end annotation

    new-instance p1, Lw5/a$c$b;

    iget-object v1, p0, Lw5/a$c$b;->g:Lm8/t;

    iget-object v2, p0, Lw5/a$c$b;->h:Lm8/t;

    iget-object v3, p0, Lw5/a$c$b;->i:Lm8/t;

    iget-object v4, p0, Lw5/a$c$b;->j:Lm8/t;

    iget-object v5, p0, Lw5/a$c$b;->k:Lm8/t;

    iget-object v6, p0, Lw5/a$c$b;->l:Lw5/a;

    move-object v0, p1

    move-object v7, p2

    invoke-direct/range {v0 .. v7}, Lw5/a$c$b;-><init>(Lm8/t;Lm8/t;Lm8/t;Lm8/t;Lm8/t;Lw5/a;Ld8/d;)V

    return-object p1
.end method

.method public bridge synthetic g(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lv8/e0;

    check-cast p2, Ld8/d;

    invoke-virtual {p0, p1, p2}, Lw5/a$c$b;->q(Lv8/e0;Ld8/d;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final n(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    invoke-static {}, Le8/b;->c()Ljava/lang/Object;

    move-result-object v0

    iget v1, p0, Lw5/a$c$b;->f:I

    const/4 v2, 0x3

    const/4 v3, 0x2

    const/4 v4, 0x1

    if-eqz v1, :cond_3

    if-eq v1, v4, :cond_2

    if-eq v1, v3, :cond_1

    if-ne v1, v2, :cond_0

    invoke-static {p1}, La8/n;->b(Ljava/lang/Object;)V

    goto :goto_2

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    iget-object v1, p0, Lw5/a$c$b;->e:Ljava/lang/Object;

    check-cast v1, Lm8/t;

    invoke-static {p1}, La8/n;->b(Ljava/lang/Object;)V

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lw5/a$c$b;->e:Ljava/lang/Object;

    check-cast v1, Lm8/t;

    invoke-static {p1}, La8/n;->b(Ljava/lang/Object;)V

    goto :goto_0

    :cond_3
    invoke-static {p1}, La8/n;->b(Ljava/lang/Object;)V

    iget-object v1, p0, Lw5/a$c$b;->g:Lm8/t;

    iget-object p1, p0, Lw5/a$c$b;->h:Lm8/t;

    iget-object p1, p1, Lm8/t;->a:Ljava/lang/Object;

    check-cast p1, Lv8/l0;

    iput-object v1, p0, Lw5/a$c$b;->e:Ljava/lang/Object;

    iput v4, p0, Lw5/a$c$b;->f:I

    invoke-interface {p1, p0}, Lv8/l0;->z(Ld8/d;)Ljava/lang/Object;

    move-result-object p1

    if-ne p1, v0, :cond_4

    return-object v0

    :cond_4
    :goto_0
    iput-object p1, v1, Lm8/t;->a:Ljava/lang/Object;

    iget-object v1, p0, Lw5/a$c$b;->i:Lm8/t;

    iget-object p1, p0, Lw5/a$c$b;->j:Lm8/t;

    iget-object p1, p1, Lm8/t;->a:Ljava/lang/Object;

    check-cast p1, Lv8/l0;

    iput-object v1, p0, Lw5/a$c$b;->e:Ljava/lang/Object;

    iput v3, p0, Lw5/a$c$b;->f:I

    invoke-interface {p1, p0}, Lv8/l0;->z(Ld8/d;)Ljava/lang/Object;

    move-result-object p1

    if-ne p1, v0, :cond_5

    return-object v0

    :cond_5
    :goto_1
    iput-object p1, v1, Lm8/t;->a:Ljava/lang/Object;

    iget-object p1, p0, Lw5/a$c$b;->k:Lm8/t;

    iget-object p1, p1, Lm8/t;->a:Ljava/lang/Object;

    check-cast p1, Lv8/l0;

    const/4 v1, 0x0

    iput-object v1, p0, Lw5/a$c$b;->e:Ljava/lang/Object;

    iput v2, p0, Lw5/a$c$b;->f:I

    invoke-interface {p1, p0}, Lv8/l0;->z(Ld8/d;)Ljava/lang/Object;

    move-result-object p1

    if-ne p1, v0, :cond_6

    return-object v0

    :cond_6
    :goto_2
    iget-object p1, p0, Lw5/a$c$b;->l:Lw5/a;

    iget-object v0, p0, Lw5/a$c$b;->g:Lm8/t;

    iget-object v0, v0, Lm8/t;->a:Ljava/lang/Object;

    check-cast v0, Lcom/miui/packageInstaller/model/CloudParams;

    invoke-static {p1, v0}, Lw5/a;->d(Lw5/a;Lcom/miui/packageInstaller/model/CloudParams;)V

    sget-object p1, La8/v;->a:La8/v;

    return-object p1
.end method

.method public final q(Lv8/e0;Ld8/d;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lv8/e0;",
            "Ld8/d<",
            "-",
            "La8/v;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lw5/a$c$b;->b(Ljava/lang/Object;Ld8/d;)Ld8/d;

    move-result-object p1

    check-cast p1, Lw5/a$c$b;

    sget-object p2, La8/v;->a:La8/v;

    invoke-virtual {p1, p2}, Lw5/a$c$b;->n(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method
