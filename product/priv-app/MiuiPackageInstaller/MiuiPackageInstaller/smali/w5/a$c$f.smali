.class final Lw5/a$c$f;
.super Lf8/k;

# interfaces
.implements Ll8/p;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lw5/a$c;->n(Ljava/lang/Object;)Ljava/lang/Object;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf8/k;",
        "Ll8/p<",
        "Lv8/e0;",
        "Ld8/d<",
        "-",
        "Lcom/miui/packageInstaller/model/CloudParams;",
        ">;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation runtime Lf8/f;
    c = "com.miui.packageInstaller.presenter.PackageInstallerPresenter$launchLoadJob$1$marketJob$1"
    f = "PackageInstallerPresenter.kt"
    l = {
        0x25
    }
    m = "invokeSuspend"
.end annotation


# instance fields
.field e:I

.field final synthetic f:Lw5/a;

.field final synthetic g:I

.field final synthetic h:Lcom/miui/packageInstaller/model/ApkInfo;


# direct methods
.method constructor <init>(Lw5/a;ILcom/miui/packageInstaller/model/ApkInfo;Ld8/d;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lw5/a;",
            "I",
            "Lcom/miui/packageInstaller/model/ApkInfo;",
            "Ld8/d<",
            "-",
            "Lw5/a$c$f;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lw5/a$c$f;->f:Lw5/a;

    iput p2, p0, Lw5/a$c$f;->g:I

    iput-object p3, p0, Lw5/a$c$f;->h:Lcom/miui/packageInstaller/model/ApkInfo;

    const/4 p1, 0x2

    invoke-direct {p0, p1, p4}, Lf8/k;-><init>(ILd8/d;)V

    return-void
.end method


# virtual methods
.method public final b(Ljava/lang/Object;Ld8/d;)Ld8/d;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ld8/d<",
            "*>;)",
            "Ld8/d<",
            "La8/v;",
            ">;"
        }
    .end annotation

    new-instance p1, Lw5/a$c$f;

    iget-object v0, p0, Lw5/a$c$f;->f:Lw5/a;

    iget v1, p0, Lw5/a$c$f;->g:I

    iget-object v2, p0, Lw5/a$c$f;->h:Lcom/miui/packageInstaller/model/ApkInfo;

    invoke-direct {p1, v0, v1, v2, p2}, Lw5/a$c$f;-><init>(Lw5/a;ILcom/miui/packageInstaller/model/ApkInfo;Ld8/d;)V

    return-object p1
.end method

.method public bridge synthetic g(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lv8/e0;

    check-cast p2, Ld8/d;

    invoke-virtual {p0, p1, p2}, Lw5/a$c$f;->q(Lv8/e0;Ld8/d;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final n(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7

    invoke-static {}, Le8/b;->c()Ljava/lang/Object;

    move-result-object v0

    iget v1, p0, Lw5/a$c$f;->e:I

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    if-ne v1, v2, :cond_0

    invoke-static {p1}, La8/n;->b(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    invoke-static {p1}, La8/n;->b(Ljava/lang/Object;)V

    new-instance v1, Lm5/d;

    invoke-direct {v1}, Lm5/d;-><init>()V

    iget-object p1, p0, Lw5/a$c$f;->f:Lw5/a;

    invoke-static {p1}, Lw5/a;->a(Lw5/a;)Landroid/content/Context;

    move-result-object p1

    iget-object v3, p0, Lw5/a$c$f;->f:Lw5/a;

    invoke-virtual {v3}, Lw5/a;->g()Lw5/a$a;

    move-result-object v3

    invoke-interface {v3}, Lw5/a$a;->c()Lm5/e;

    move-result-object v3

    invoke-static {v3}, Lm8/i;->c(Ljava/lang/Object;)V

    iget v4, p0, Lw5/a$c$f;->g:I

    iget-object v5, p0, Lw5/a$c$f;->h:Lcom/miui/packageInstaller/model/ApkInfo;

    iput v2, p0, Lw5/a$c$f;->e:I

    move-object v2, p1

    move-object v6, p0

    invoke-virtual/range {v1 .. v6}, Lm5/d;->c(Landroid/content/Context;Lm5/e;ILcom/miui/packageInstaller/model/ApkInfo;Ld8/d;)Ljava/lang/Object;

    move-result-object p1

    if-ne p1, v0, :cond_2

    return-object v0

    :cond_2
    :goto_0
    return-object p1
.end method

.method public final q(Lv8/e0;Ld8/d;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lv8/e0;",
            "Ld8/d<",
            "-",
            "Lcom/miui/packageInstaller/model/CloudParams;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lw5/a$c$f;->b(Ljava/lang/Object;Ld8/d;)Ld8/d;

    move-result-object p1

    check-cast p1, Lw5/a$c$f;

    sget-object p2, La8/v;->a:La8/v;

    invoke-virtual {p1, p2}, Lw5/a$c$f;->n(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method
