.class public Lf1/a;
.super Ljava/lang/Object;


# static fields
.field private static a:Lg1/c$a;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const-string v0, "k"

    const-string v1, "x"

    const-string v2, "y"

    filled-new-array {v0, v1, v2}, [Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lg1/c$a;->a([Ljava/lang/String;)Lg1/c$a;

    move-result-object v0

    sput-object v0, Lf1/a;->a:Lg1/c$a;

    return-void
.end method

.method public static a(Lg1/c;Lv0/d;)Lb1/e;
    .locals 3

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lg1/c;->J()Lg1/c$b;

    move-result-object v1

    sget-object v2, Lg1/c$b;->a:Lg1/c$b;

    if-ne v1, v2, :cond_1

    invoke-virtual {p0}, Lg1/c;->g()V

    :goto_0
    invoke-virtual {p0}, Lg1/c;->s()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0, p1}, Lf1/w;->a(Lg1/c;Lv0/d;)Ly0/h;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lg1/c;->n()V

    invoke-static {v0}, Lf1/r;->b(Ljava/util/List;)V

    goto :goto_1

    :cond_1
    new-instance p1, Li1/a;

    invoke-static {}, Lh1/h;->e()F

    move-result v1

    invoke-static {p0, v1}, Lf1/p;->e(Lg1/c;F)Landroid/graphics/PointF;

    move-result-object p0

    invoke-direct {p1, p0}, Li1/a;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_1
    new-instance p0, Lb1/e;

    invoke-direct {p0, v0}, Lb1/e;-><init>(Ljava/util/List;)V

    return-object p0
.end method

.method static b(Lg1/c;Lv0/d;)Lb1/m;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lg1/c;",
            "Lv0/d;",
            ")",
            "Lb1/m<",
            "Landroid/graphics/PointF;",
            "Landroid/graphics/PointF;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Lg1/c;->m()V

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    move-object v3, v1

    move v4, v2

    move-object v2, v3

    :goto_0
    invoke-virtual {p0}, Lg1/c;->J()Lg1/c$b;

    move-result-object v5

    sget-object v6, Lg1/c$b;->d:Lg1/c$b;

    if-eq v5, v6, :cond_5

    sget-object v5, Lf1/a;->a:Lg1/c$a;

    invoke-virtual {p0, v5}, Lg1/c;->L(Lg1/c$a;)I

    move-result v5

    if-eqz v5, :cond_4

    if-eq v5, v0, :cond_2

    const/4 v6, 0x2

    if-eq v5, v6, :cond_0

    invoke-virtual {p0}, Lg1/c;->M()V

    invoke-virtual {p0}, Lg1/c;->U()V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lg1/c;->J()Lg1/c$b;

    move-result-object v5

    sget-object v6, Lg1/c$b;->f:Lg1/c$b;

    if-ne v5, v6, :cond_1

    goto :goto_1

    :cond_1
    invoke-static {p0, p1}, Lf1/d;->e(Lg1/c;Lv0/d;)Lb1/b;

    move-result-object v3

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lg1/c;->J()Lg1/c$b;

    move-result-object v5

    sget-object v6, Lg1/c$b;->f:Lg1/c$b;

    if-ne v5, v6, :cond_3

    :goto_1
    invoke-virtual {p0}, Lg1/c;->U()V

    move v4, v0

    goto :goto_0

    :cond_3
    invoke-static {p0, p1}, Lf1/d;->e(Lg1/c;Lv0/d;)Lb1/b;

    move-result-object v2

    goto :goto_0

    :cond_4
    invoke-static {p0, p1}, Lf1/a;->a(Lg1/c;Lv0/d;)Lb1/e;

    move-result-object v1

    goto :goto_0

    :cond_5
    invoke-virtual {p0}, Lg1/c;->p()V

    if-eqz v4, :cond_6

    const-string p0, "Lottie doesn\'t support expressions."

    invoke-virtual {p1, p0}, Lv0/d;->a(Ljava/lang/String;)V

    :cond_6
    if-eqz v1, :cond_7

    return-object v1

    :cond_7
    new-instance p0, Lb1/i;

    invoke-direct {p0, v2, v3}, Lb1/i;-><init>(Lb1/b;Lb1/b;)V

    return-object p0
.end method
