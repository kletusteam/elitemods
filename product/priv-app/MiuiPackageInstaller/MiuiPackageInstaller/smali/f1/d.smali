.class public Lf1/d;
.super Ljava/lang/Object;


# direct methods
.method private static a(Lg1/c;FLv0/d;Lf1/j0;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lg1/c;",
            "F",
            "Lv0/d;",
            "Lf1/j0<",
            "TT;>;)",
            "Ljava/util/List<",
            "Li1/a<",
            "TT;>;>;"
        }
    .end annotation

    invoke-static {p0, p2, p1, p3}, Lf1/r;->a(Lg1/c;Lv0/d;FLf1/j0;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method private static b(Lg1/c;Lv0/d;Lf1/j0;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lg1/c;",
            "Lv0/d;",
            "Lf1/j0<",
            "TT;>;)",
            "Ljava/util/List<",
            "Li1/a<",
            "TT;>;>;"
        }
    .end annotation

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-static {p0, p1, v0, p2}, Lf1/r;->a(Lg1/c;Lv0/d;FLf1/j0;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method static c(Lg1/c;Lv0/d;)Lb1/a;
    .locals 2

    new-instance v0, Lb1/a;

    sget-object v1, Lf1/f;->a:Lf1/f;

    invoke-static {p0, p1, v1}, Lf1/d;->b(Lg1/c;Lv0/d;Lf1/j0;)Ljava/util/List;

    move-result-object p0

    invoke-direct {v0, p0}, Lb1/a;-><init>(Ljava/util/List;)V

    return-object v0
.end method

.method static d(Lg1/c;Lv0/d;)Lb1/j;
    .locals 2

    new-instance v0, Lb1/j;

    sget-object v1, Lf1/h;->a:Lf1/h;

    invoke-static {p0, p1, v1}, Lf1/d;->b(Lg1/c;Lv0/d;Lf1/j0;)Ljava/util/List;

    move-result-object p0

    invoke-direct {v0, p0}, Lb1/j;-><init>(Ljava/util/List;)V

    return-object v0
.end method

.method public static e(Lg1/c;Lv0/d;)Lb1/b;
    .locals 1

    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Lf1/d;->f(Lg1/c;Lv0/d;Z)Lb1/b;

    move-result-object p0

    return-object p0
.end method

.method public static f(Lg1/c;Lv0/d;Z)Lb1/b;
    .locals 2

    new-instance v0, Lb1/b;

    if-eqz p2, :cond_0

    invoke-static {}, Lh1/h;->e()F

    move-result p2

    goto :goto_0

    :cond_0
    const/high16 p2, 0x3f800000    # 1.0f

    :goto_0
    sget-object v1, Lf1/i;->a:Lf1/i;

    invoke-static {p0, p2, p1, v1}, Lf1/d;->a(Lg1/c;FLv0/d;Lf1/j0;)Ljava/util/List;

    move-result-object p0

    invoke-direct {v0, p0}, Lb1/b;-><init>(Ljava/util/List;)V

    return-object v0
.end method

.method static g(Lg1/c;Lv0/d;I)Lb1/c;
    .locals 2

    new-instance v0, Lb1/c;

    new-instance v1, Lf1/l;

    invoke-direct {v1, p2}, Lf1/l;-><init>(I)V

    invoke-static {p0, p1, v1}, Lf1/d;->b(Lg1/c;Lv0/d;Lf1/j0;)Ljava/util/List;

    move-result-object p0

    invoke-direct {v0, p0}, Lb1/c;-><init>(Ljava/util/List;)V

    return-object v0
.end method

.method static h(Lg1/c;Lv0/d;)Lb1/d;
    .locals 2

    new-instance v0, Lb1/d;

    sget-object v1, Lf1/o;->a:Lf1/o;

    invoke-static {p0, p1, v1}, Lf1/d;->b(Lg1/c;Lv0/d;Lf1/j0;)Ljava/util/List;

    move-result-object p0

    invoke-direct {v0, p0}, Lb1/d;-><init>(Ljava/util/List;)V

    return-object v0
.end method

.method static i(Lg1/c;Lv0/d;)Lb1/f;
    .locals 3

    new-instance v0, Lb1/f;

    invoke-static {}, Lh1/h;->e()F

    move-result v1

    sget-object v2, Lf1/y;->a:Lf1/y;

    invoke-static {p0, v1, p1, v2}, Lf1/d;->a(Lg1/c;FLv0/d;Lf1/j0;)Ljava/util/List;

    move-result-object p0

    invoke-direct {v0, p0}, Lb1/f;-><init>(Ljava/util/List;)V

    return-object v0
.end method

.method static j(Lg1/c;Lv0/d;)Lb1/g;
    .locals 2

    new-instance v0, Lb1/g;

    sget-object v1, Lf1/c0;->a:Lf1/c0;

    invoke-static {p0, p1, v1}, Lf1/d;->b(Lg1/c;Lv0/d;Lf1/j0;)Ljava/util/List;

    move-result-object p0

    invoke-direct {v0, p0}, Lb1/g;-><init>(Ljava/util/List;)V

    return-object v0
.end method

.method static k(Lg1/c;Lv0/d;)Lb1/h;
    .locals 3

    new-instance v0, Lb1/h;

    invoke-static {}, Lh1/h;->e()F

    move-result v1

    sget-object v2, Lf1/d0;->a:Lf1/d0;

    invoke-static {p0, v1, p1, v2}, Lf1/d;->a(Lg1/c;FLv0/d;Lf1/j0;)Ljava/util/List;

    move-result-object p0

    invoke-direct {v0, p0}, Lb1/h;-><init>(Ljava/util/List;)V

    return-object v0
.end method
