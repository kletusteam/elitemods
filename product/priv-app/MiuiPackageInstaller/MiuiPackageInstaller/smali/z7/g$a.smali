.class public final enum Lz7/g$a;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lz7/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lz7/g$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum b:Lz7/g$a;

.field public static final enum c:Lz7/g$a;

.field public static final enum d:Lz7/g$a;

.field public static final enum e:Lz7/g$a;

.field public static final enum f:Lz7/g$a;

.field public static final enum g:Lz7/g$a;

.field public static final enum h:Lz7/g$a;

.field public static final enum i:Lz7/g$a;

.field public static final enum j:Lz7/g$a;

.field public static final enum k:Lz7/g$a;

.field public static final enum l:Lz7/g$a;

.field public static final enum m:Lz7/g$a;

.field public static final enum n:Lz7/g$a;

.field public static final enum o:Lz7/g$a;

.field public static final enum p:Lz7/g$a;

.field private static final synthetic q:[Lz7/g$a;


# instance fields
.field private a:I


# direct methods
.method static constructor <clinit>()V
    .locals 18

    new-instance v0, Lz7/g$a;

    const-string v1, "ERROR_SERVER"

    const/4 v2, 0x0

    const/4 v3, -0x1

    invoke-direct {v0, v1, v2, v3}, Lz7/g$a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lz7/g$a;->b:Lz7/g$a;

    new-instance v1, Lz7/g$a;

    const-string v3, "ERROR_JSON_EXCEPTION"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4, v4}, Lz7/g$a;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lz7/g$a;->c:Lz7/g$a;

    new-instance v3, Lz7/g$a;

    const-string v5, "ERROR_ACCESSDENIED_EXCEPTION"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6, v6}, Lz7/g$a;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lz7/g$a;->d:Lz7/g$a;

    new-instance v5, Lz7/g$a;

    const-string v7, "ERROR_AUTHENTICATIONFAILURE_EXCEPTION"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8, v8}, Lz7/g$a;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lz7/g$a;->e:Lz7/g$a;

    new-instance v7, Lz7/g$a;

    const-string v9, "ERROR_NETWORK_EXCEPTION"

    const/4 v10, 0x4

    invoke-direct {v7, v9, v10, v10}, Lz7/g$a;-><init>(Ljava/lang/String;II)V

    sput-object v7, Lz7/g$a;->f:Lz7/g$a;

    new-instance v9, Lz7/g$a;

    const-string v11, "ERROR_ENCRYPT_EXCEPTION"

    const/4 v12, 0x5

    invoke-direct {v9, v11, v12, v12}, Lz7/g$a;-><init>(Ljava/lang/String;II)V

    sput-object v9, Lz7/g$a;->g:Lz7/g$a;

    new-instance v11, Lz7/g$a;

    const-string v13, "ERROR_INTERRUPTED_EXCEPTION"

    const/4 v14, 0x6

    invoke-direct {v11, v13, v14, v14}, Lz7/g$a;-><init>(Ljava/lang/String;II)V

    sput-object v11, Lz7/g$a;->h:Lz7/g$a;

    new-instance v13, Lz7/g$a;

    const-string v15, "ERROR_HUMANCOMPUTER_VERIFICATION_FAILED"

    const/4 v14, 0x7

    invoke-direct {v13, v15, v14, v14}, Lz7/g$a;-><init>(Ljava/lang/String;II)V

    sput-object v13, Lz7/g$a;->i:Lz7/g$a;

    new-instance v15, Lz7/g$a;

    const-string v14, "ERROR_EVENTID_EXPIRED"

    const/16 v12, 0x8

    invoke-direct {v15, v14, v12, v12}, Lz7/g$a;-><init>(Ljava/lang/String;II)V

    sput-object v15, Lz7/g$a;->j:Lz7/g$a;

    new-instance v14, Lz7/g$a;

    const-string v12, "ERROR_CONNECT_UNREACHABLE_EXCEPTION"

    const/16 v10, 0x9

    invoke-direct {v14, v12, v10, v10}, Lz7/g$a;-><init>(Ljava/lang/String;II)V

    sput-object v14, Lz7/g$a;->k:Lz7/g$a;

    new-instance v12, Lz7/g$a;

    const-string v10, "ERROR_SOCKET_TIMEOUT_EXCEPTION"

    const/16 v8, 0xa

    invoke-direct {v12, v10, v8, v8}, Lz7/g$a;-><init>(Ljava/lang/String;II)V

    sput-object v12, Lz7/g$a;->l:Lz7/g$a;

    new-instance v10, Lz7/g$a;

    const-string v8, "ERROR_CONNECT_TIMEOUT_EXCEPTION"

    const/16 v6, 0xb

    invoke-direct {v10, v8, v6, v6}, Lz7/g$a;-><init>(Ljava/lang/String;II)V

    sput-object v10, Lz7/g$a;->m:Lz7/g$a;

    new-instance v8, Lz7/g$a;

    const-string v6, "ERROR_IO_EXCEPTION"

    const/16 v4, 0xc

    invoke-direct {v8, v6, v4, v4}, Lz7/g$a;-><init>(Ljava/lang/String;II)V

    sput-object v8, Lz7/g$a;->n:Lz7/g$a;

    new-instance v6, Lz7/g$a;

    const-string v4, "ERROR_REGISTRATION_SESSION_EXCEPTION"

    const/16 v2, 0xd

    invoke-direct {v6, v4, v2, v2}, Lz7/g$a;-><init>(Ljava/lang/String;II)V

    sput-object v6, Lz7/g$a;->o:Lz7/g$a;

    new-instance v4, Lz7/g$a;

    const-string v2, "ERROR_VERIFY_SERVER"

    move-object/from16 v17, v6

    const/16 v6, 0xe

    invoke-direct {v4, v2, v6, v6}, Lz7/g$a;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lz7/g$a;->p:Lz7/g$a;

    const/16 v2, 0xf

    new-array v2, v2, [Lz7/g$a;

    const/16 v16, 0x0

    aput-object v0, v2, v16

    const/4 v0, 0x1

    aput-object v1, v2, v0

    const/4 v0, 0x2

    aput-object v3, v2, v0

    const/4 v0, 0x3

    aput-object v5, v2, v0

    const/4 v0, 0x4

    aput-object v7, v2, v0

    const/4 v0, 0x5

    aput-object v9, v2, v0

    const/4 v0, 0x6

    aput-object v11, v2, v0

    const/4 v0, 0x7

    aput-object v13, v2, v0

    const/16 v0, 0x8

    aput-object v15, v2, v0

    const/16 v0, 0x9

    aput-object v14, v2, v0

    const/16 v0, 0xa

    aput-object v12, v2, v0

    const/16 v0, 0xb

    aput-object v10, v2, v0

    const/16 v0, 0xc

    aput-object v8, v2, v0

    const/16 v0, 0xd

    aput-object v17, v2, v0

    aput-object v4, v2, v6

    sput-object v2, Lz7/g$a;->q:[Lz7/g$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lz7/g$a;->a:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lz7/g$a;
    .locals 1

    const-class v0, Lz7/g$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lz7/g$a;

    return-object p0
.end method

.method public static values()[Lz7/g$a;
    .locals 1

    sget-object v0, Lz7/g$a;->q:[Lz7/g$a;

    invoke-virtual {v0}, [Lz7/g$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lz7/g$a;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lz7/g$a;->a:I

    return v0
.end method
