.class Lz7/j$b;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lz7/j;->M(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ly7/f$m;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ly7/f$m;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Ljava/lang/String;

.field final synthetic e:Ljava/lang/Boolean;

.field final synthetic f:Ljava/lang/String;

.field final synthetic g:Ljava/lang/String;

.field final synthetic h:Ljava/lang/Boolean;

.field final synthetic i:Lz7/j;


# direct methods
.method constructor <init>(Lz7/j;Ly7/f$m;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V
    .locals 0

    iput-object p1, p0, Lz7/j$b;->i:Lz7/j;

    iput-object p2, p0, Lz7/j$b;->a:Ly7/f$m;

    iput-object p3, p0, Lz7/j$b;->b:Ljava/lang/String;

    iput-object p4, p0, Lz7/j$b;->c:Ljava/lang/String;

    iput-object p5, p0, Lz7/j$b;->d:Ljava/lang/String;

    iput-object p6, p0, Lz7/j$b;->e:Ljava/lang/Boolean;

    iput-object p7, p0, Lz7/j$b;->f:Ljava/lang/String;

    iput-object p8, p0, Lz7/j$b;->g:Ljava/lang/String;

    iput-object p9, p0, Lz7/j$b;->h:Ljava/lang/Boolean;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    const-string v0, "uploadData:"

    const-string v1, ""

    const-string v2, "SensorHelper"

    :try_start_0
    iget-object v3, p0, Lz7/j$b;->a:Ly7/f$m;

    if-eqz v3, :cond_c

    iget-object v3, p0, Lz7/j$b;->b:Ljava/lang/String;

    invoke-static {v3}, Lz7/f;->c(Ljava/lang/String;)Lz7/f$b;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lz7/j$b;->c:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, p0, Lz7/j$b;->d:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, v3, Lz7/f$b;->b:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, v3, Lz7/f$b;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lz7/j$b;->i:Lz7/j;

    invoke-static {v5}, Lz7/j;->b(Lz7/j;)Landroid/content/Context;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {v5, v4, v6}, Lv7/c;->e(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Ljava/util/concurrent/FutureTask;

    move-result-object v4
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_7
    .catch Lz7/f$a; {:try_start_0 .. :try_end_0} :catch_6
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Lf7/a; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lf7/b; {:try_start_0 .. :try_end_0} :catch_3

    const-wide/16 v5, 0xbb8

    :try_start_1
    sget-object v7, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, v5, v6, v7}, Ljava/util/concurrent/FutureTask;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/os/Bundle;

    if-eqz v4, :cond_1

    const-string v5, "booleanResult"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v5, "userData"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    :cond_0
    const-string v5, "errorCode"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    const-string v6, "errorMessage"

    invoke-virtual {v4, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "error code: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Lg7/b;->f(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "error msg: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lg7/b;->f(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Lz7/f$a; {:try_start_1 .. :try_end_1} :catch_6
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Lf7/a; {:try_start_1 .. :try_end_1} :catch_4
    .catch Lf7/b; {:try_start_1 .. :try_end_1} :catch_3

    goto :goto_0

    :catch_0
    move-exception v4

    :try_start_2
    invoke-virtual {v4}, Ljava/util/concurrent/TimeoutException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v4

    invoke-virtual {v4}, Ljava/util/concurrent/ExecutionException;->printStackTrace()V

    goto :goto_0

    :catch_2
    move-exception v4

    invoke-virtual {v4}, Ljava/lang/InterruptedException;->printStackTrace()V

    :cond_1
    :goto_0
    move-object v4, v1

    :goto_1
    new-instance v6, Lg7/g;

    invoke-direct {v6}, Lg7/g;-><init>()V

    iget-object v5, p0, Lz7/j$b;->e:Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_7
    .catch Lz7/f$a; {:try_start_2 .. :try_end_2} :catch_6
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Lf7/a; {:try_start_2 .. :try_end_2} :catch_4
    .catch Lf7/b; {:try_start_2 .. :try_end_2} :catch_3

    const-string v7, "d"

    const-string v8, "s"

    if-eqz v5, :cond_2

    :try_start_3
    invoke-virtual {v6, v8, v1}, Lg7/g;->a(Ljava/lang/Object;Ljava/lang/Object;)Lg7/g;

    invoke-virtual {v6, v7, v1}, Lg7/g;->a(Ljava/lang/Object;Ljava/lang/Object;)Lg7/g;

    goto :goto_2

    :cond_2
    iget-object v5, v3, Lz7/f$b;->b:Ljava/lang/String;

    invoke-virtual {v6, v8, v5}, Lg7/g;->a(Ljava/lang/Object;Ljava/lang/Object;)Lg7/g;

    iget-object v3, v3, Lz7/f$b;->a:Ljava/lang/String;

    invoke-virtual {v6, v7, v3}, Lg7/g;->a(Ljava/lang/Object;Ljava/lang/Object;)Lg7/g;

    :goto_2
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "t"

    invoke-virtual {v6, v3, v4}, Lg7/g;->a(Ljava/lang/Object;Ljava/lang/Object;)Lg7/g;

    :cond_3
    iget-object v3, p0, Lz7/j$b;->d:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    const-string v3, "a"

    iget-object v4, p0, Lz7/j$b;->d:Ljava/lang/String;

    invoke-virtual {v6, v3, v4}, Lg7/g;->a(Ljava/lang/Object;Ljava/lang/Object;)Lg7/g;

    :cond_4
    const-string v3, "http.agent"

    invoke-static {v3}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lg7/g;

    invoke-direct {v4}, Lg7/g;-><init>()V

    const-string v5, "User-Agent"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " AndroidVerifySDK/"

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "3.8.1"

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v5, v3}, Lg7/g;->a(Ljava/lang/Object;Ljava/lang/Object;)Lg7/g;

    move-result-object v8

    iget-object v3, p0, Lz7/j$b;->f:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-static {v3}, Lz7/c;->a(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    :cond_5
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lz7/j$b;->g:Ljava/lang/String;

    const-string v7, "/captcha/v2/data?"

    invoke-static {v5, v7}, Lz7/e;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "k="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, p0, Lz7/j$b;->c:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "&locale="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "&errorAction="

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lz7/j$b;->h:Ljava/lang/Boolean;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x0

    const/4 v9, 0x1

    const/16 v3, 0xbb8

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-static/range {v5 .. v10}, Lf7/e;->k(Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;ZLjava/lang/Integer;)Lf7/e$f;

    move-result-object v3

    if-nez v3, :cond_6

    sget-object v3, Lz7/g$a;->f:Lz7/g$a;

    invoke-virtual {v3}, Lz7/g$a;->a()I

    move-result v4

    const-string v5, "uploadData:network exception"

    invoke-static {v3}, Lz7/g;->a(Lz7/g$a;)I

    move-result v3

    invoke-static {v4, v5, v3}, Lz7/j;->v(ILjava/lang/String;I)Lz7/n;

    move-result-object v3

    iget-object v4, p0, Lz7/j$b;->a:Ly7/f$m;

    invoke-interface {v4, v3}, Ly7/f$m;->b(Lz7/n;)V

    return-void

    :cond_6
    new-instance v4, Lorg/json/JSONObject;

    invoke-virtual {v3}, Lf7/e$f;->d()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v3, "code"

    invoke-virtual {v4, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v3

    const-string v5, "msg"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-nez v3, :cond_a

    const-string v3, "data"

    invoke-virtual {v4, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    const-string v4, "result"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v4

    const-string v5, "token"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "url"

    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "extra"

    invoke-virtual {v3, v7}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    if-eqz v3, :cond_7

    invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lz7/i;->c(Ljava/lang/String;)V

    :cond_7
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_8

    iget-object v3, p0, Lz7/j$b;->a:Ly7/f$m;

    invoke-interface {v3, v6}, Ly7/f$m;->c(Ljava/lang/String;)V

    goto/16 :goto_8

    :cond_8
    if-eqz v4, :cond_9

    new-instance v3, Lz7/p$b;

    invoke-direct {v3}, Lz7/p$b;-><init>()V

    invoke-virtual {v3, v5}, Lz7/p$b;->e(Ljava/lang/String;)Lz7/p$b;

    move-result-object v3

    invoke-static {}, Lz7/i;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lz7/p$b;->d(Ljava/lang/String;)Lz7/p$b;

    move-result-object v3

    invoke-virtual {v3}, Lz7/p$b;->c()Lz7/p;

    move-result-object v3

    iget-object v4, p0, Lz7/j$b;->a:Ly7/f$m;

    :goto_3
    invoke-interface {v4, v3}, Ly7/f$m;->a(Lz7/p;)V

    goto/16 :goto_8

    :cond_9
    sget-object v3, Lz7/g$a;->i:Lz7/g$a;

    invoke-virtual {v3}, Lz7/g$a;->a()I

    move-result v4

    const-string v5, "uploadData:human computer verification failed"

    invoke-static {v3}, Lz7/g;->a(Lz7/g$a;)I

    move-result v3

    invoke-static {v4, v5, v3}, Lz7/j;->v(ILjava/lang/String;I)Lz7/n;

    move-result-object v3

    iget-object v4, p0, Lz7/j$b;->a:Ly7/f$m;

    :goto_4
    invoke-interface {v4, v3}, Ly7/f$m;->b(Lz7/n;)V

    goto/16 :goto_8

    :cond_a
    const/16 v4, 0x1f4

    if-ne v3, v4, :cond_b

    new-instance v3, Lz7/p$b;

    invoke-direct {v3}, Lz7/p$b;-><init>()V

    invoke-static {}, Lz7/f;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lz7/p$b;->e(Ljava/lang/String;)Lz7/p$b;

    move-result-object v3

    invoke-virtual {v3}, Lz7/p$b;->c()Lz7/p;

    move-result-object v3

    iget-object v4, p0, Lz7/j$b;->a:Ly7/f$m;

    goto :goto_3

    :cond_b
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lz7/g$a;->b:Lz7/g$a;

    invoke-static {v5}, Lz7/g;->a(Lz7/g$a;)I

    move-result v5

    invoke-static {v3, v4, v5}, Lz7/j;->v(ILjava/lang/String;I)Lz7/n;

    move-result-object v3

    iget-object v4, p0, Lz7/j$b;->a:Ly7/f$m;

    goto :goto_4

    :cond_c
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "uploadData :verifyCallback not be null"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_7
    .catch Lz7/f$a; {:try_start_3 .. :try_end_3} :catch_6
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_5
    .catch Lf7/a; {:try_start_3 .. :try_end_3} :catch_4
    .catch Lf7/b; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    move-exception v3

    invoke-static {v2, v1, v3}, Lg7/b;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    sget-object v1, Lz7/g$a;->e:Lz7/g$a;

    invoke-virtual {v1}, Lz7/g$a;->a()I

    move-result v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    goto :goto_5

    :catch_4
    move-exception v3

    invoke-static {v2, v1, v3}, Lg7/b;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    sget-object v1, Lz7/g$a;->d:Lz7/g$a;

    invoke-virtual {v1}, Lz7/g$a;->a()I

    move-result v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    :goto_5
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ld7/b;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_6

    :catch_5
    move-exception v0

    iget-object v1, p0, Lz7/j$b;->i:Lz7/j;

    iget-object v2, p0, Lz7/j$b;->a:Ly7/f$m;

    invoke-static {v1, v2, v0}, Lz7/j;->c(Lz7/j;Ly7/f$m;Ljava/io/IOException;)V

    goto :goto_8

    :catch_6
    move-exception v3

    invoke-static {v2, v1, v3}, Lg7/b;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    sget-object v1, Lz7/g$a;->g:Lz7/g$a;

    invoke-virtual {v1}, Lz7/g$a;->a()I

    move-result v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_6
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1}, Lz7/g;->a(Lz7/g$a;)I

    move-result v1

    invoke-static {v2, v0, v1}, Lz7/j;->v(ILjava/lang/String;I)Lz7/n;

    move-result-object v0

    goto :goto_7

    :catch_7
    move-exception v1

    const-string v3, "fail to parse JSONObject"

    invoke-static {v2, v3, v1}, Lg7/b;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    sget-object v2, Lz7/g$a;->c:Lz7/g$a;

    invoke-virtual {v2}, Lz7/g$a;->a()I

    move-result v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lorg/json/JSONException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2}, Lz7/g;->a(Lz7/g$a;)I

    move-result v1

    invoke-static {v3, v0, v1}, Lz7/j;->v(ILjava/lang/String;I)Lz7/n;

    move-result-object v0

    :goto_7
    iget-object v1, p0, Lz7/j$b;->a:Ly7/f$m;

    invoke-interface {v1, v0}, Ly7/f$m;->b(Lz7/n;)V

    :goto_8
    return-void
.end method
