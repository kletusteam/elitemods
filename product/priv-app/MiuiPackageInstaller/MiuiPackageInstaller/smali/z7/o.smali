.class public Lz7/o;
.super Ljava/lang/Object;


# direct methods
.method public static a(Ljava/lang/String;)Lz7/d;
    .locals 4

    const/4 v0, 0x0

    :try_start_0
    new-instance v1, Lg7/g;

    invoke-direct {v1}, Lg7/g;-><init>()V

    const-string v2, "type"

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lg7/g;->a(Ljava/lang/Object;Ljava/lang/Object;)Lg7/g;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {p0, v1, v0, v0, v2}, Lf7/f;->e(Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Z)Lf7/e$f;

    move-result-object p0

    if-nez p0, :cond_0

    return-object v0

    :cond_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-virtual {p0}, Lf7/e$f;->d()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v1, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string p0, "maxDuration"

    invoke-virtual {v1, p0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result p0

    const-string v2, "frequency"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v1

    new-instance v2, Lz7/d;

    invoke-direct {v2}, Lz7/d;-><init>()V

    invoke-virtual {v2, p0}, Lz7/d;->d(I)V

    invoke-virtual {v2, v1}, Lz7/d;->c(I)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lf7/a; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lf7/b; {:try_start_0 .. :try_end_0} :catch_0

    return-object v2

    :catch_0
    move-exception p0

    goto :goto_0

    :catch_1
    move-exception p0

    :goto_0
    invoke-virtual {p0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    :catch_2
    move-exception p0

    invoke-virtual {p0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    :catch_3
    move-exception p0

    const-string v1, "VerifyRequest"

    const-string v2, "fail to parse JSONObject"

    invoke-static {v1, v2, p0}, Lg7/b;->j(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_1
    return-object v0
.end method
