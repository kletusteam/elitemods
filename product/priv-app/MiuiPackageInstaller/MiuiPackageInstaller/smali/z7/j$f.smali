.class Lz7/j$f;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lz7/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "f"
.end annotation


# instance fields
.field private final a:[F

.field private final b:I

.field final synthetic c:Lz7/j;


# direct methods
.method constructor <init>(Lz7/j;Landroid/hardware/SensorEvent;)V
    .locals 0

    iput-object p1, p0, Lz7/j$f;->c:Lz7/j;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget-object p1, p2, Landroid/hardware/SensorEvent;->values:[F

    iput-object p1, p0, Lz7/j$f;->a:[F

    iget-object p1, p2, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {p1}, Landroid/hardware/Sensor;->getType()I

    move-result p1

    iput p1, p0, Lz7/j$f;->b:I

    return-void
.end method

.method static synthetic a(Lz7/j$f;)I
    .locals 0

    iget p0, p0, Lz7/j$f;->b:I

    return p0
.end method

.method private d()I
    .locals 3

    iget v0, p0, Lz7/j$f;->b:I

    const/4 v1, 0x2

    const/4 v2, 0x1

    if-eq v0, v2, :cond_4

    if-eq v0, v1, :cond_3

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v2, 0x5

    if-eq v0, v2, :cond_1

    const/4 v1, 0x6

    if-eq v0, v1, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    return v2

    :cond_1
    return v1

    :cond_2
    return v2

    :cond_3
    const/4 v0, 0x3

    return v0

    :cond_4
    return v1
.end method


# virtual methods
.method b()Ljava/lang/String;
    .locals 6

    goto/32 :goto_14

    nop

    :goto_0
    goto :goto_a

    :goto_1
    goto/32 :goto_e

    nop

    :goto_2
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_b

    nop

    :goto_3
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_7

    nop

    :goto_4
    return-object v0

    :goto_5
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_8

    nop

    :goto_6
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    goto/32 :goto_2

    nop

    :goto_7
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    goto/32 :goto_f

    nop

    :goto_8
    iget-object v1, p0, Lz7/j$f;->a:[F

    goto/32 :goto_c

    nop

    :goto_9
    const/4 v3, 0x0

    :goto_a
    goto/32 :goto_11

    nop

    :goto_b
    add-int/lit8 v3, v3, 0x1

    goto/32 :goto_0

    nop

    :goto_c
    array-length v2, v1

    goto/32 :goto_9

    nop

    :goto_d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/32 :goto_3

    nop

    :goto_e
    invoke-direct {p0}, Lz7/j$f;->d()I

    move-result v1

    goto/32 :goto_d

    nop

    :goto_f
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    goto/32 :goto_10

    nop

    :goto_10
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_4

    nop

    :goto_11
    const-string v4, ","

    goto/32 :goto_12

    nop

    :goto_12
    if-lt v3, v2, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_13

    nop

    :goto_13
    aget v5, v1, v3

    goto/32 :goto_6

    nop

    :goto_14
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_5

    nop
.end method

.method c()Lorg/json/JSONArray;
    .locals 10

    goto/32 :goto_3

    nop

    :goto_0
    invoke-static {v3}, Lz7/j;->a(Lz7/j;)J

    move-result-wide v3

    goto/32 :goto_6

    nop

    :goto_1
    return-object v0

    :goto_2
    goto/16 :goto_23

    :catch_0
    move-exception v6

    goto/32 :goto_d

    nop

    :goto_3
    new-instance v0, Lorg/json/JSONArray;

    goto/32 :goto_7

    nop

    :goto_4
    goto :goto_18

    :goto_5
    goto/32 :goto_15

    nop

    :goto_6
    sub-long/2addr v1, v3

    goto/32 :goto_12

    nop

    :goto_7
    invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V

    goto/32 :goto_11

    nop

    :goto_8
    array-length v2, v1

    :goto_9
    goto/32 :goto_10

    nop

    :goto_a
    iget v2, p0, Lz7/j$f;->b:I

    goto/32 :goto_16

    nop

    :goto_b
    new-instance v7, Ljava/text/DecimalFormat;

    goto/32 :goto_21

    nop

    :goto_c
    new-instance v1, Ljava/text/DecimalFormat;

    goto/32 :goto_14

    nop

    :goto_d
    invoke-virtual {v6}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    goto/32 :goto_1f

    nop

    :goto_e
    add-int/lit8 v5, v5, 0x1

    goto/32 :goto_17

    nop

    :goto_f
    iget-object v3, p0, Lz7/j$f;->c:Lz7/j;

    goto/32 :goto_0

    nop

    :goto_10
    if-lt v5, v2, :cond_0

    goto/32 :goto_18

    :cond_0
    goto/32 :goto_24

    nop

    :goto_11
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    goto/32 :goto_f

    nop

    :goto_12
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONArray;->put(J)Lorg/json/JSONArray;

    goto/32 :goto_c

    nop

    :goto_13
    invoke-virtual {v0, v6, v7}, Lorg/json/JSONArray;->put(D)Lorg/json/JSONArray;

    goto/32 :goto_e

    nop

    :goto_14
    const-string v2, "##0.0"

    goto/32 :goto_1a

    nop

    :goto_15
    iget-object v1, p0, Lz7/j$f;->a:[F

    goto/32 :goto_8

    nop

    :goto_16
    const-wide/16 v3, 0x0

    goto/32 :goto_1c

    nop

    :goto_17
    goto :goto_9

    :goto_18
    goto/32 :goto_1

    nop

    :goto_19
    float-to-double v8, v6

    :try_start_0
    invoke-virtual {v7, v8, v9}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v6
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_2

    nop

    :goto_1a
    invoke-direct {v1, v2}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    goto/32 :goto_a

    nop

    :goto_1b
    invoke-virtual {v0, v3, v4}, Lorg/json/JSONArray;->put(D)Lorg/json/JSONArray;

    goto/32 :goto_4

    nop

    :goto_1c
    const/4 v5, 0x0

    goto/32 :goto_25

    nop

    :goto_1d
    invoke-direct {v7, v8}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    goto/32 :goto_19

    nop

    :goto_1e
    if-eq v2, v6, :cond_1

    goto/32 :goto_5

    :cond_1
    :try_start_1
    iget-object v2, p0, Lz7/j$f;->a:[F

    aget v2, v2, v5

    float-to-double v5, v2

    invoke-virtual {v1, v5, v6}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v3
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    goto/32 :goto_1b

    nop

    :goto_1f
    const-string v7, "SensorHelper"

    goto/32 :goto_20

    nop

    :goto_20
    invoke-static {v7, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_22

    nop

    :goto_21
    const-string v8, "##0.0000"

    goto/32 :goto_1d

    nop

    :goto_22
    move-wide v6, v3

    :goto_23
    goto/32 :goto_13

    nop

    :goto_24
    aget v6, v1, v5

    goto/32 :goto_b

    nop

    :goto_25
    const/4 v6, 0x5

    goto/32 :goto_1e

    nop
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lz7/j$f;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
