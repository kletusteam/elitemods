.class public final enum Lz7/g;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lz7/g$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lz7/g;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum b:Lz7/g;

.field public static final enum c:Lz7/g;

.field public static final enum d:Lz7/g;

.field public static final enum e:Lz7/g;

.field public static final enum f:Lz7/g;

.field private static final synthetic g:[Lz7/g;


# instance fields
.field public a:I


# direct methods
.method static constructor <clinit>()V
    .locals 12

    new-instance v0, Lz7/g;

    sget v1, Ly7/e;->b:I

    const-string v2, "NETWORK_ERROR_INFO"

    const/4 v3, 0x0

    invoke-direct {v0, v2, v3, v1}, Lz7/g;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lz7/g;->b:Lz7/g;

    new-instance v1, Lz7/g;

    sget v2, Ly7/e;->c:I

    const-string v4, "NETWORK_TIMEOUT_INFO"

    const/4 v5, 0x1

    invoke-direct {v1, v4, v5, v2}, Lz7/g;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lz7/g;->c:Lz7/g;

    new-instance v2, Lz7/g;

    sget v4, Ly7/e;->d:I

    const-string v6, "SYSTEM_ERROR_INFO"

    const/4 v7, 0x2

    invoke-direct {v2, v6, v7, v4}, Lz7/g;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lz7/g;->d:Lz7/g;

    new-instance v4, Lz7/g;

    sget v6, Ly7/e;->a:I

    const-string v8, "ACCESS_DENIED_INFO"

    const/4 v9, 0x3

    invoke-direct {v4, v8, v9, v6}, Lz7/g;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lz7/g;->e:Lz7/g;

    new-instance v6, Lz7/g;

    sget v8, Ly7/e;->e:I

    const-string v10, "UNKNOWN_ERROR_INFO"

    const/4 v11, 0x4

    invoke-direct {v6, v10, v11, v8}, Lz7/g;-><init>(Ljava/lang/String;II)V

    sput-object v6, Lz7/g;->f:Lz7/g;

    const/4 v8, 0x5

    new-array v8, v8, [Lz7/g;

    aput-object v0, v8, v3

    aput-object v1, v8, v5

    aput-object v2, v8, v7

    aput-object v4, v8, v9

    aput-object v6, v8, v11

    sput-object v8, Lz7/g;->g:[Lz7/g;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lz7/g;->a:I

    return-void
.end method

.method public static a(Lz7/g$a;)I
    .locals 1

    sget-object v0, Lz7/g$a;->c:Lz7/g$a;

    if-ne p0, v0, :cond_0

    :goto_0
    sget-object p0, Lz7/g;->d:Lz7/g;

    :goto_1
    iget p0, p0, Lz7/g;->a:I

    goto :goto_4

    :cond_0
    sget-object v0, Lz7/g$a;->d:Lz7/g$a;

    if-ne p0, v0, :cond_1

    sget-object p0, Lz7/g;->e:Lz7/g;

    goto :goto_1

    :cond_1
    sget-object v0, Lz7/g$a;->e:Lz7/g$a;

    if-ne p0, v0, :cond_2

    goto :goto_0

    :cond_2
    sget-object v0, Lz7/g$a;->f:Lz7/g$a;

    if-ne p0, v0, :cond_3

    goto :goto_0

    :cond_3
    sget-object v0, Lz7/g$a;->g:Lz7/g$a;

    if-ne p0, v0, :cond_4

    goto :goto_0

    :cond_4
    sget-object v0, Lz7/g$a;->i:Lz7/g$a;

    if-ne p0, v0, :cond_5

    goto :goto_0

    :cond_5
    sget-object v0, Lz7/g$a;->k:Lz7/g$a;

    if-ne p0, v0, :cond_6

    :goto_2
    sget-object p0, Lz7/g;->b:Lz7/g;

    goto :goto_1

    :cond_6
    sget-object v0, Lz7/g$a;->l:Lz7/g$a;

    if-ne p0, v0, :cond_7

    :goto_3
    sget-object p0, Lz7/g;->c:Lz7/g;

    goto :goto_1

    :cond_7
    sget-object v0, Lz7/g$a;->m:Lz7/g$a;

    if-ne p0, v0, :cond_8

    goto :goto_3

    :cond_8
    sget-object v0, Lz7/g$a;->b:Lz7/g$a;

    if-ne p0, v0, :cond_9

    goto :goto_0

    :cond_9
    sget-object v0, Lz7/g$a;->h:Lz7/g$a;

    if-ne p0, v0, :cond_a

    goto :goto_0

    :cond_a
    sget-object v0, Lz7/g$a;->j:Lz7/g$a;

    if-ne p0, v0, :cond_b

    goto :goto_0

    :cond_b
    sget-object v0, Lz7/g$a;->n:Lz7/g$a;

    if-ne p0, v0, :cond_c

    goto :goto_2

    :cond_c
    sget-object p0, Lz7/g;->f:Lz7/g;

    goto :goto_1

    :goto_4
    return p0
.end method

.method public static valueOf(Ljava/lang/String;)Lz7/g;
    .locals 1

    const-class v0, Lz7/g;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lz7/g;

    return-object p0
.end method

.method public static values()[Lz7/g;
    .locals 1

    sget-object v0, Lz7/g;->g:[Lz7/g;

    invoke-virtual {v0}, [Lz7/g;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lz7/g;

    return-object v0
.end method
