.class public final Lg9/g;
.super Lg9/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lg9/c<",
        "Lg9/g;",
        ">;"
    }
.end annotation


# instance fields
.field private n:Lg9/i;

.field private o:F

.field private p:Z


# direct methods
.method public constructor <init>(Ljava/lang/Object;Lh9/b;F)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            ">(TK;",
            "Lh9/b<",
            "TK;>;F)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lg9/c;-><init>(Ljava/lang/Object;Lh9/b;)V

    const/4 p1, 0x0

    iput-object p1, p0, Lg9/g;->n:Lg9/i;

    const p1, 0x7f7fffff    # Float.MAX_VALUE

    iput p1, p0, Lg9/g;->o:F

    const/4 p1, 0x0

    iput-boolean p1, p0, Lg9/g;->p:Z

    new-instance p1, Lg9/i;

    invoke-direct {p1, p3}, Lg9/i;-><init>(F)V

    iput-object p1, p0, Lg9/g;->n:Lg9/i;

    return-void
.end method

.method private w()V
    .locals 4

    iget-object v0, p0, Lg9/g;->n:Lg9/i;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lg9/i;->a()F

    move-result v0

    float-to-double v0, v0

    iget v2, p0, Lg9/c;->g:F

    float-to-double v2, v2

    cmpl-double v2, v0, v2

    if-gtz v2, :cond_1

    iget v2, p0, Lg9/c;->h:F

    float-to-double v2, v2

    cmpg-double v0, v0, v2

    if-ltz v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Final position of the spring cannot be less than the min value."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Final position of the spring cannot be greater than the max value."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Incomplete SpringAnimation: Either final position or a spring force needs to be set."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method p(F)V
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    return-void
.end method

.method public q()V
    .locals 3

    invoke-direct {p0}, Lg9/g;->w()V

    iget-object v0, p0, Lg9/g;->n:Lg9/i;

    invoke-virtual {p0}, Lg9/c;->h()F

    move-result v1

    float-to-double v1, v1

    invoke-virtual {v0, v1, v2}, Lg9/i;->g(D)V

    invoke-super {p0}, Lg9/c;->q()V

    return-void
.end method

.method s(J)Z
    .locals 20

    goto/32 :goto_26

    nop

    :goto_0
    iget-object v1, v0, Lg9/g;->n:Lg9/i;

    goto/32 :goto_c

    nop

    :goto_1
    div-long v18, p1, v11

    goto/32 :goto_20

    nop

    :goto_2
    move-wide/from16 v16, v5

    goto/32 :goto_24

    nop

    :goto_3
    if-nez v1, :cond_0

    goto/32 :goto_16

    :cond_0
    goto/32 :goto_7

    nop

    :goto_4
    iget v7, v0, Lg9/g;->o:F

    goto/32 :goto_5

    nop

    :goto_5
    invoke-virtual {v6, v7}, Lg9/i;->e(F)Lg9/i;

    goto/32 :goto_39

    nop

    :goto_6
    iput v1, v0, Lg9/c;->b:F

    goto/32 :goto_32

    nop

    :goto_7
    iget v1, v0, Lg9/g;->o:F

    goto/32 :goto_2f

    nop

    :goto_8
    float-to-double v7, v1

    goto/32 :goto_47

    nop

    :goto_9
    iget v1, v1, Lg9/c$a;->b:F

    goto/32 :goto_22

    nop

    :goto_a
    iget-object v1, v0, Lg9/g;->n:Lg9/i;

    goto/32 :goto_4a

    nop

    :goto_b
    invoke-virtual/range {v6 .. v12}, Lg9/i;->h(DDJ)Lg9/c$a;

    move-result-object v1

    goto/32 :goto_17

    nop

    :goto_c
    invoke-virtual {v1}, Lg9/i;->a()F

    move-result v1

    goto/32 :goto_27

    nop

    :goto_d
    invoke-virtual {v0, v1, v5}, Lg9/g;->v(FF)Z

    move-result v1

    goto/32 :goto_45

    nop

    :goto_e
    float-to-double v14, v5

    goto/32 :goto_18

    nop

    :goto_f
    const/4 v2, 0x1

    goto/32 :goto_13

    nop

    :goto_10
    iget v5, v0, Lg9/c;->h:F

    goto/32 :goto_1b

    nop

    :goto_11
    const-wide/16 v11, 0x2

    goto/32 :goto_1

    nop

    :goto_12
    float-to-double v9, v1

    goto/32 :goto_11

    nop

    :goto_13
    const/4 v3, 0x0

    goto/32 :goto_4c

    nop

    :goto_14
    iget-object v13, v0, Lg9/g;->n:Lg9/i;

    goto/32 :goto_44

    nop

    :goto_15
    return v2

    :goto_16
    goto/32 :goto_1c

    nop

    :goto_17
    iget-object v6, v0, Lg9/g;->n:Lg9/i;

    goto/32 :goto_4

    nop

    :goto_18
    iget v1, v1, Lg9/c$a;->b:F

    goto/32 :goto_43

    nop

    :goto_19
    invoke-virtual {v6, v1}, Lg9/i;->e(F)Lg9/i;

    goto/32 :goto_2b

    nop

    :goto_1a
    float-to-double v5, v1

    goto/32 :goto_2

    nop

    :goto_1b
    invoke-static {v1, v5}, Ljava/lang/Math;->max(FF)F

    move-result v1

    goto/32 :goto_3e

    nop

    :goto_1c
    iget v1, v0, Lg9/g;->o:F

    goto/32 :goto_4b

    nop

    :goto_1d
    iget v1, v0, Lg9/c;->b:F

    goto/32 :goto_8

    nop

    :goto_1e
    iget-object v6, v0, Lg9/g;->n:Lg9/i;

    goto/32 :goto_19

    nop

    :goto_1f
    iget v5, v0, Lg9/c;->g:F

    goto/32 :goto_3d

    nop

    :goto_20
    move-wide/from16 v11, v18

    goto/32 :goto_b

    nop

    :goto_21
    iput-boolean v3, v0, Lg9/g;->p:Z

    goto/32 :goto_15

    nop

    :goto_22
    iput v1, v0, Lg9/c;->a:F

    goto/32 :goto_3b

    nop

    :goto_23
    iget-object v6, v0, Lg9/g;->n:Lg9/i;

    goto/32 :goto_1d

    nop

    :goto_24
    move-wide/from16 v18, p1

    :goto_25
    goto/32 :goto_3a

    nop

    :goto_26
    move-object/from16 v0, p0

    goto/32 :goto_41

    nop

    :goto_27
    iput v1, v0, Lg9/c;->b:F

    goto/32 :goto_36

    nop

    :goto_28
    invoke-virtual {v1}, Lg9/i;->a()F

    goto/32 :goto_23

    nop

    :goto_29
    iput v4, v0, Lg9/c;->a:F

    goto/32 :goto_2d

    nop

    :goto_2a
    if-nez v1, :cond_1

    goto/32 :goto_35

    :cond_1
    goto/32 :goto_30

    nop

    :goto_2b
    iput v5, v0, Lg9/g;->o:F

    :goto_2c
    goto/32 :goto_0

    nop

    :goto_2d
    return v2

    :goto_2e
    goto/32 :goto_40

    nop

    :goto_2f
    cmpl-float v6, v1, v5

    goto/32 :goto_3f

    nop

    :goto_30
    iget-object v1, v0, Lg9/g;->n:Lg9/i;

    goto/32 :goto_28

    nop

    :goto_31
    iget v1, v0, Lg9/c;->a:F

    goto/32 :goto_1a

    nop

    :goto_32
    iget v5, v0, Lg9/c;->a:F

    goto/32 :goto_d

    nop

    :goto_33
    iput v1, v0, Lg9/c;->b:F

    goto/32 :goto_29

    nop

    :goto_34
    goto :goto_25

    :goto_35
    goto/32 :goto_14

    nop

    :goto_36
    iput v4, v0, Lg9/c;->a:F

    goto/32 :goto_21

    nop

    :goto_37
    const v5, 0x7f7fffff    # Float.MAX_VALUE

    goto/32 :goto_3

    nop

    :goto_38
    iput v5, v0, Lg9/c;->b:F

    goto/32 :goto_9

    nop

    :goto_39
    iput v5, v0, Lg9/g;->o:F

    goto/32 :goto_49

    nop

    :goto_3a
    invoke-virtual/range {v13 .. v19}, Lg9/i;->h(DDJ)Lg9/c$a;

    move-result-object v1

    goto/32 :goto_48

    nop

    :goto_3b
    iget v1, v0, Lg9/c;->b:F

    goto/32 :goto_10

    nop

    :goto_3c
    iget v5, v1, Lg9/c$a;->a:F

    goto/32 :goto_e

    nop

    :goto_3d
    invoke-static {v1, v5}, Ljava/lang/Math;->min(FF)F

    move-result v1

    goto/32 :goto_6

    nop

    :goto_3e
    iput v1, v0, Lg9/c;->b:F

    goto/32 :goto_1f

    nop

    :goto_3f
    if-nez v6, :cond_2

    goto/32 :goto_2c

    :cond_2
    goto/32 :goto_1e

    nop

    :goto_40
    return v3

    :goto_41
    iget-boolean v1, v0, Lg9/g;->p:Z

    goto/32 :goto_f

    nop

    :goto_42
    move-wide/from16 v16, v5

    goto/32 :goto_34

    nop

    :goto_43
    float-to-double v5, v1

    goto/32 :goto_42

    nop

    :goto_44
    iget v1, v0, Lg9/c;->b:F

    goto/32 :goto_46

    nop

    :goto_45
    if-nez v1, :cond_3

    goto/32 :goto_2e

    :cond_3
    goto/32 :goto_a

    nop

    :goto_46
    float-to-double v14, v1

    goto/32 :goto_31

    nop

    :goto_47
    iget v1, v0, Lg9/c;->a:F

    goto/32 :goto_12

    nop

    :goto_48
    iget v5, v1, Lg9/c$a;->a:F

    goto/32 :goto_38

    nop

    :goto_49
    iget-object v13, v0, Lg9/g;->n:Lg9/i;

    goto/32 :goto_3c

    nop

    :goto_4a
    invoke-virtual {v1}, Lg9/i;->a()F

    move-result v1

    goto/32 :goto_33

    nop

    :goto_4b
    cmpl-float v1, v1, v5

    goto/32 :goto_2a

    nop

    :goto_4c
    const/4 v4, 0x0

    goto/32 :goto_37

    nop
.end method

.method public t()Z
    .locals 4

    iget-object v0, p0, Lg9/g;->n:Lg9/i;

    iget-wide v0, v0, Lg9/i;->b:D

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public u()Lg9/i;
    .locals 1

    iget-object v0, p0, Lg9/g;->n:Lg9/i;

    return-object v0
.end method

.method v(FF)Z
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    return p1

    :goto_1
    invoke-virtual {v0, p1, p2}, Lg9/i;->c(FF)Z

    move-result p1

    goto/32 :goto_0

    nop

    :goto_2
    iget-object v0, p0, Lg9/g;->n:Lg9/i;

    goto/32 :goto_1

    nop
.end method

.method public x()V
    .locals 2

    invoke-virtual {p0}, Lg9/g;->t()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lg9/c;->f()Lg9/b;

    move-result-object v0

    invoke-virtual {v0}, Lg9/b;->l()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lg9/c;->f:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lg9/g;->p:Z

    :cond_0
    return-void

    :cond_1
    new-instance v0, Landroid/util/AndroidRuntimeException;

    const-string v1, "Animations may only be started on the same thread as the animation handler"

    invoke-direct {v0, v1}, Landroid/util/AndroidRuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Spring animations can only come to an end when there is damping"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
