.class public final Lg9/i;
.super Ljava/lang/Object;


# instance fields
.field a:D

.field b:D

.field private c:Z

.field private d:D

.field private e:D

.field private f:D

.field private g:D

.field private h:D

.field private i:D

.field private final j:Lg9/c$a;


# direct methods
.method public constructor <init>(F)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide v0, 0x4097700000000000L    # 1500.0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    iput-wide v0, p0, Lg9/i;->a:D

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    iput-wide v0, p0, Lg9/i;->b:D

    const/4 v0, 0x0

    iput-boolean v0, p0, Lg9/i;->c:Z

    const-wide v0, 0x7fefffffffffffffL    # Double.MAX_VALUE

    iput-wide v0, p0, Lg9/i;->i:D

    new-instance v0, Lg9/c$a;

    invoke-direct {v0}, Lg9/c$a;-><init>()V

    iput-object v0, p0, Lg9/i;->j:Lg9/c$a;

    float-to-double v0, p1

    iput-wide v0, p0, Lg9/i;->i:D

    return-void
.end method

.method private b()V
    .locals 8

    iget-boolean v0, p0, Lg9/i;->c:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-wide v0, p0, Lg9/i;->i:D

    const-wide v2, 0x7fefffffffffffffL    # Double.MAX_VALUE

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_3

    iget-wide v0, p0, Lg9/i;->b:D

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    cmpl-double v4, v0, v2

    if-lez v4, :cond_1

    neg-double v4, v0

    iget-wide v6, p0, Lg9/i;->a:D

    mul-double/2addr v4, v6

    mul-double/2addr v0, v0

    sub-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    mul-double/2addr v6, v0

    add-double/2addr v4, v6

    iput-wide v4, p0, Lg9/i;->f:D

    iget-wide v0, p0, Lg9/i;->b:D

    neg-double v4, v0

    iget-wide v6, p0, Lg9/i;->a:D

    mul-double/2addr v4, v6

    mul-double/2addr v0, v0

    sub-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    mul-double/2addr v6, v0

    sub-double/2addr v4, v6

    iput-wide v4, p0, Lg9/i;->g:D

    goto :goto_0

    :cond_1
    const-wide/16 v4, 0x0

    cmpl-double v4, v0, v4

    if-ltz v4, :cond_2

    cmpg-double v4, v0, v2

    if-gez v4, :cond_2

    iget-wide v4, p0, Lg9/i;->a:D

    mul-double/2addr v0, v0

    sub-double/2addr v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    mul-double/2addr v4, v0

    iput-wide v4, p0, Lg9/i;->h:D

    :cond_2
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lg9/i;->c:Z

    return-void

    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Error: Final position of the spring must be set before the miuix.animation starts"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public a()F
    .locals 2

    iget-wide v0, p0, Lg9/i;->i:D

    double-to-float v0, v0

    return v0
.end method

.method public c(FF)Z
    .locals 4

    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result p2

    float-to-double v0, p2

    iget-wide v2, p0, Lg9/i;->e:D

    cmpg-double p2, v0, v2

    if-gez p2, :cond_0

    invoke-virtual {p0}, Lg9/i;->a()F

    move-result p2

    sub-float/2addr p1, p2

    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result p1

    float-to-double p1, p1

    iget-wide v0, p0, Lg9/i;->d:D

    cmpg-double p1, p1, v0

    if-gez p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public d(F)Lg9/i;
    .locals 2

    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-ltz v0, :cond_0

    float-to-double v0, p1

    iput-wide v0, p0, Lg9/i;->b:D

    const/4 p1, 0x0

    iput-boolean p1, p0, Lg9/i;->c:Z

    return-object p0

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Damping ratio must be non-negative"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public e(F)Lg9/i;
    .locals 2

    float-to-double v0, p1

    iput-wide v0, p0, Lg9/i;->i:D

    return-object p0
.end method

.method public f(F)Lg9/i;
    .locals 2

    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-lez v0, :cond_0

    float-to-double v0, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    iput-wide v0, p0, Lg9/i;->a:D

    const/4 p1, 0x0

    iput-boolean p1, p0, Lg9/i;->c:Z

    return-object p0

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Spring stiffness constant must be positive."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method g(D)V
    .locals 2

    goto/32 :goto_5

    nop

    :goto_0
    iput-wide p1, p0, Lg9/i;->e:D

    goto/32 :goto_2

    nop

    :goto_1
    mul-double/2addr p1, v0

    goto/32 :goto_0

    nop

    :goto_2
    return-void

    :goto_3
    const-wide v0, 0x404f400000000000L    # 62.5

    goto/32 :goto_1

    nop

    :goto_4
    iput-wide p1, p0, Lg9/i;->d:D

    goto/32 :goto_3

    nop

    :goto_5
    invoke-static {p1, p2}, Ljava/lang/Math;->abs(D)D

    move-result-wide p1

    goto/32 :goto_4

    nop
.end method

.method h(DDJ)Lg9/c$a;
    .locals 16

    goto/32 :goto_43

    nop

    :goto_0
    mul-double/2addr v12, v1

    goto/32 :goto_26

    nop

    :goto_1
    const-wide/high16 v7, 0x3ff0000000000000L    # 1.0

    goto/32 :goto_5

    nop

    :goto_2
    mul-double/2addr v5, v3

    goto/32 :goto_3

    nop

    :goto_3
    iget-wide v12, v0, Lg9/i;->a:D

    goto/32 :goto_1d

    nop

    :goto_4
    neg-double v14, v12

    goto/32 :goto_25

    nop

    :goto_5
    cmpl-double v9, v5, v7

    goto/32 :goto_29

    nop

    :goto_6
    iget-wide v5, v0, Lg9/i;->g:D

    goto/32 :goto_16

    nop

    :goto_7
    invoke-static {v11, v12, v9, v10}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v9

    goto/32 :goto_2e

    nop

    :goto_8
    sub-double v14, v5, v12

    goto/32 :goto_7a

    nop

    :goto_9
    mul-double/2addr v5, v7

    goto/32 :goto_6e

    nop

    :goto_a
    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v12

    goto/32 :goto_d

    nop

    :goto_b
    mul-double/2addr v3, v5

    goto/32 :goto_70

    nop

    :goto_c
    return-object v1

    :goto_d
    mul-double/2addr v3, v12

    goto/32 :goto_4a

    nop

    :goto_e
    iget-wide v12, v0, Lg9/i;->a:D

    goto/32 :goto_3f

    nop

    :goto_f
    invoke-static {v10, v11, v5, v6}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v5

    goto/32 :goto_2

    nop

    :goto_10
    mul-double/2addr v3, v1

    goto/32 :goto_1f

    nop

    :goto_11
    sub-double v7, v7, p3

    goto/32 :goto_7c

    nop

    :goto_12
    neg-double v14, v12

    goto/32 :goto_53

    nop

    :goto_13
    invoke-static {v12, v13}, Ljava/lang/Math;->cos(D)D

    move-result-wide v12

    goto/32 :goto_49

    nop

    :goto_14
    invoke-static {v11, v12}, Ljava/lang/Math;->sin(D)D

    move-result-wide v3

    goto/32 :goto_7d

    nop

    :goto_15
    const-wide v11, 0x4005bf0a8b145769L    # Math.E

    goto/32 :goto_7

    nop

    :goto_16
    mul-double v7, v5, v3

    goto/32 :goto_11

    nop

    :goto_17
    cmpl-double v9, v5, v7

    goto/32 :goto_61

    nop

    :goto_18
    invoke-static {v10, v11, v5, v6}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v5

    goto/32 :goto_9

    nop

    :goto_19
    mul-double v7, v5, v3

    goto/32 :goto_3c

    nop

    :goto_1a
    iget-wide v14, v0, Lg9/i;->h:D

    goto/32 :goto_6b

    nop

    :goto_1b
    neg-double v12, v12

    goto/32 :goto_5d

    nop

    :goto_1c
    iget-wide v10, v0, Lg9/i;->b:D

    goto/32 :goto_3d

    nop

    :goto_1d
    neg-double v12, v12

    goto/32 :goto_66

    nop

    :goto_1e
    sub-double v3, p1, v3

    goto/32 :goto_73

    nop

    :goto_1f
    invoke-static {v3, v4}, Ljava/lang/Math;->cos(D)D

    move-result-wide v1

    goto/32 :goto_22

    nop

    :goto_20
    mul-double/2addr v12, v1

    goto/32 :goto_13

    nop

    :goto_21
    move-wide/from16 p1, v5

    goto/32 :goto_38

    nop

    :goto_22
    mul-double/2addr v7, v1

    goto/32 :goto_2a

    nop

    :goto_23
    mul-double/2addr v5, v1

    goto/32 :goto_f

    nop

    :goto_24
    if-gtz v9, :cond_0

    goto/32 :goto_33

    :cond_0
    goto/32 :goto_6

    nop

    :goto_25
    mul-double/2addr v3, v14

    goto/32 :goto_1b

    nop

    :goto_26
    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v12

    goto/32 :goto_4e

    nop

    :goto_27
    add-double/2addr v5, v12

    goto/32 :goto_67

    nop

    :goto_28
    invoke-static {v14, v15}, Ljava/lang/Math;->sin(D)D

    move-result-wide v14

    goto/32 :goto_76

    nop

    :goto_29
    const-wide v10, 0x4005bf0a8b145769L    # Math.E

    goto/32 :goto_24

    nop

    :goto_2a
    add-double/2addr v5, v7

    goto/32 :goto_30

    nop

    :goto_2b
    iput v2, v1, Lg9/c$a;->a:F

    goto/32 :goto_7e

    nop

    :goto_2c
    iget-wide v5, v0, Lg9/i;->a:D

    goto/32 :goto_19

    nop

    :goto_2d
    mul-double/2addr v9, v12

    goto/32 :goto_39

    nop

    :goto_2e
    iget-wide v11, v0, Lg9/i;->h:D

    goto/32 :goto_21

    nop

    :goto_2f
    mul-double/2addr v5, v1

    goto/32 :goto_65

    nop

    :goto_30
    mul-double/2addr v9, v5

    goto/32 :goto_58

    nop

    :goto_31
    mul-double/2addr v5, v12

    goto/32 :goto_5b

    nop

    :goto_32
    goto/16 :goto_4c

    :goto_33
    goto/32 :goto_17

    nop

    :goto_34
    sub-double v12, v5, v12

    goto/32 :goto_68

    nop

    :goto_35
    long-to-double v1, v1

    goto/32 :goto_41

    nop

    :goto_36
    sub-double v7, v3, v7

    goto/32 :goto_b

    nop

    :goto_37
    iget-wide v2, v0, Lg9/i;->i:D

    goto/32 :goto_6c

    nop

    :goto_38
    neg-double v5, v11

    goto/32 :goto_54

    nop

    :goto_39
    mul-double/2addr v9, v1

    goto/32 :goto_15

    nop

    :goto_3a
    mul-double/2addr v7, v14

    goto/32 :goto_56

    nop

    :goto_3b
    move-wide/from16 v1, p5

    goto/32 :goto_35

    nop

    :goto_3c
    add-double v7, p3, v7

    goto/32 :goto_52

    nop

    :goto_3d
    mul-double/2addr v14, v10

    goto/32 :goto_5f

    nop

    :goto_3e
    mul-double/2addr v5, v1

    goto/32 :goto_18

    nop

    :goto_3f
    mul-double v14, v5, v12

    goto/32 :goto_51

    nop

    :goto_40
    double-to-float v2, v5

    goto/32 :goto_2b

    nop

    :goto_41
    const-wide v3, 0x408f400000000000L    # 1000.0

    goto/32 :goto_5e

    nop

    :goto_42
    add-double/2addr v3, v12

    goto/32 :goto_64

    nop

    :goto_43
    move-object/from16 v0, p0

    goto/32 :goto_72

    nop

    :goto_44
    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v12

    goto/32 :goto_46

    nop

    :goto_45
    mul-double/2addr v3, v12

    goto/32 :goto_4f

    nop

    :goto_46
    mul-double/2addr v7, v12

    goto/32 :goto_60

    nop

    :goto_47
    iget-object v1, v0, Lg9/i;->j:Lg9/c$a;

    goto/32 :goto_37

    nop

    :goto_48
    add-double/2addr v7, v3

    goto/32 :goto_77

    nop

    :goto_49
    mul-double/2addr v12, v3

    goto/32 :goto_1a

    nop

    :goto_4a
    iget-wide v12, v0, Lg9/i;->a:D

    goto/32 :goto_4

    nop

    :goto_4b
    move-wide/from16 v5, p1

    :goto_4c
    goto/32 :goto_47

    nop

    :goto_4d
    add-double v14, v14, p3

    goto/32 :goto_3a

    nop

    :goto_4e
    mul-double/2addr v12, v3

    goto/32 :goto_27

    nop

    :goto_4f
    mul-double/2addr v12, v1

    goto/32 :goto_63

    nop

    :goto_50
    iget-wide v3, v0, Lg9/i;->i:D

    goto/32 :goto_1e

    nop

    :goto_51
    mul-double/2addr v14, v3

    goto/32 :goto_4d

    nop

    :goto_52
    mul-double v12, v7, v1

    goto/32 :goto_42

    nop

    :goto_53
    mul-double/2addr v14, v5

    goto/32 :goto_1c

    nop

    :goto_54
    mul-double/2addr v5, v3

    goto/32 :goto_5c

    nop

    :goto_55
    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v1

    goto/32 :goto_74

    nop

    :goto_56
    neg-double v5, v5

    goto/32 :goto_7b

    nop

    :goto_57
    add-double/2addr v7, v3

    goto/32 :goto_32

    nop

    :goto_58
    add-double v7, v14, v9

    goto/32 :goto_4b

    nop

    :goto_59
    iget-wide v12, v0, Lg9/i;->h:D

    goto/32 :goto_6d

    nop

    :goto_5a
    mul-double/2addr v7, v12

    goto/32 :goto_71

    nop

    :goto_5b
    iget-wide v12, v0, Lg9/i;->a:D

    goto/32 :goto_12

    nop

    :goto_5c
    mul-double/2addr v11, v1

    goto/32 :goto_14

    nop

    :goto_5d
    mul-double/2addr v12, v1

    goto/32 :goto_55

    nop

    :goto_5e
    div-double/2addr v1, v3

    goto/32 :goto_50

    nop

    :goto_5f
    neg-double v9, v10

    goto/32 :goto_2d

    nop

    :goto_60
    iget-wide v12, v0, Lg9/i;->f:D

    goto/32 :goto_45

    nop

    :goto_61
    if-eqz v9, :cond_1

    goto/32 :goto_78

    :cond_1
    goto/32 :goto_2c

    nop

    :goto_62
    add-double/2addr v12, v14

    goto/32 :goto_31

    nop

    :goto_63
    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v1

    goto/32 :goto_6a

    nop

    :goto_64
    neg-double v5, v5

    goto/32 :goto_23

    nop

    :goto_65
    invoke-static {v10, v11, v5, v6}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v5

    goto/32 :goto_79

    nop

    :goto_66
    mul-double/2addr v12, v1

    goto/32 :goto_a

    nop

    :goto_67
    iget-wide v12, v0, Lg9/i;->g:D

    goto/32 :goto_5a

    nop

    :goto_68
    div-double/2addr v3, v12

    goto/32 :goto_3e

    nop

    :goto_69
    iput v2, v1, Lg9/c$a;->b:F

    goto/32 :goto_c

    nop

    :goto_6a
    mul-double/2addr v3, v1

    goto/32 :goto_57

    nop

    :goto_6b
    mul-double/2addr v14, v1

    goto/32 :goto_28

    nop

    :goto_6c
    add-double/2addr v5, v2

    goto/32 :goto_40

    nop

    :goto_6d
    div-double/2addr v7, v12

    goto/32 :goto_e

    nop

    :goto_6e
    iget-wide v12, v0, Lg9/i;->f:D

    goto/32 :goto_0

    nop

    :goto_6f
    mul-double/2addr v7, v3

    goto/32 :goto_10

    nop

    :goto_70
    sub-double v3, v3, p3

    goto/32 :goto_34

    nop

    :goto_71
    mul-double/2addr v12, v1

    goto/32 :goto_44

    nop

    :goto_72
    invoke-direct/range {p0 .. p0}, Lg9/i;->b()V

    goto/32 :goto_3b

    nop

    :goto_73
    iget-wide v5, v0, Lg9/i;->b:D

    goto/32 :goto_1

    nop

    :goto_74
    mul-double/2addr v7, v1

    goto/32 :goto_48

    nop

    :goto_75
    iget-wide v3, v0, Lg9/i;->h:D

    goto/32 :goto_6f

    nop

    :goto_76
    mul-double/2addr v14, v7

    goto/32 :goto_62

    nop

    :goto_77
    goto/16 :goto_4c

    :goto_78
    goto/32 :goto_59

    nop

    :goto_79
    iget-wide v12, v0, Lg9/i;->h:D

    goto/32 :goto_20

    nop

    :goto_7a
    div-double/2addr v7, v14

    goto/32 :goto_36

    nop

    :goto_7b
    mul-double/2addr v5, v12

    goto/32 :goto_2f

    nop

    :goto_7c
    iget-wide v12, v0, Lg9/i;->f:D

    goto/32 :goto_8

    nop

    :goto_7d
    mul-double/2addr v5, v3

    goto/32 :goto_75

    nop

    :goto_7e
    double-to-float v2, v7

    goto/32 :goto_69

    nop
.end method
