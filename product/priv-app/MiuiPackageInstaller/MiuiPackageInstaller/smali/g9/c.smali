.class public abstract Lg9/c;
.super Ljava/lang/Object;

# interfaces
.implements Lg9/b$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lg9/c$c;,
        Lg9/c$b;,
        Lg9/c$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lg9/c<",
        "TT;>;>",
        "Ljava/lang/Object;",
        "Lg9/b$b;"
    }
.end annotation


# instance fields
.field a:F

.field b:F

.field c:Z

.field final d:Ljava/lang/Object;

.field final e:Lh9/b;

.field f:Z

.field g:F

.field h:F

.field private i:J

.field private j:F

.field private k:J

.field private final l:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lg9/c$b;",
            ">;"
        }
    .end annotation
.end field

.field private final m:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lg9/c$c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/Object;Lh9/b;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            ">(TK;",
            "Lh9/b<",
            "TK;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lg9/c;->a:F

    const v0, 0x7f7fffff    # Float.MAX_VALUE

    iput v0, p0, Lg9/c;->b:F

    const/4 v1, 0x0

    iput-boolean v1, p0, Lg9/c;->c:Z

    iput-boolean v1, p0, Lg9/c;->f:Z

    iput v0, p0, Lg9/c;->g:F

    neg-float v0, v0

    iput v0, p0, Lg9/c;->h:F

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lg9/c;->i:J

    iput-wide v0, p0, Lg9/c;->k:J

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lg9/c;->l:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lg9/c;->m:Ljava/util/ArrayList;

    iput-object p1, p0, Lg9/c;->d:Ljava/lang/Object;

    iput-object p2, p0, Lg9/c;->e:Lh9/b;

    sget-object p1, Lh9/h;->g:Lh9/h;

    if-eq p2, p1, :cond_4

    sget-object p1, Lh9/h;->h:Lh9/h;

    if-eq p2, p1, :cond_4

    sget-object p1, Lh9/h;->i:Lh9/h;

    if-ne p2, p1, :cond_0

    goto :goto_1

    :cond_0
    sget-object p1, Lh9/h;->o:Lh9/h;

    if-ne p2, p1, :cond_1

    const/high16 p1, 0x3b800000    # 0.00390625f

    goto :goto_2

    :cond_1
    sget-object p1, Lh9/h;->e:Lh9/h;

    if-eq p2, p1, :cond_3

    sget-object p1, Lh9/h;->f:Lh9/h;

    if-ne p2, p1, :cond_2

    goto :goto_0

    :cond_2
    const/high16 p1, 0x3f800000    # 1.0f

    goto :goto_2

    :cond_3
    :goto_0
    const p1, 0x3b03126f    # 0.002f

    goto :goto_2

    :cond_4
    :goto_1
    const p1, 0x3dcccccd    # 0.1f

    :goto_2
    iput p1, p0, Lg9/c;->j:F

    return-void
.end method

.method private e(Z)V
    .locals 4

    const/4 v0, 0x0

    iput-boolean v0, p0, Lg9/c;->f:Z

    invoke-static {}, Lg9/b;->i()Lg9/b;

    move-result-object v1

    invoke-virtual {v1, p0}, Lg9/b;->m(Lg9/b$b;)V

    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lg9/c;->i:J

    iput-boolean v0, p0, Lg9/c;->c:Z

    :goto_0
    iget-object v1, p0, Lg9/c;->l:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lg9/c;->l:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lg9/c;->l:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lg9/c$b;

    iget v2, p0, Lg9/c;->b:F

    iget v3, p0, Lg9/c;->a:F

    invoke-interface {v1, p0, p1, v2, v3}, Lg9/c$b;->a(Lg9/c;ZFF)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lg9/c;->l:Ljava/util/ArrayList;

    invoke-static {p1}, Lg9/c;->j(Ljava/util/ArrayList;)V

    return-void
.end method

.method private g()F
    .locals 2

    iget-object v0, p0, Lg9/c;->e:Lh9/b;

    iget-object v1, p0, Lg9/c;->d:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lh9/b;->d(Ljava/lang/Object;)F

    move-result v0

    return v0
.end method

.method private static j(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/ArrayList<",
            "TT;>;)V"
        }
    .end annotation

    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private r()V
    .locals 3

    iget-boolean v0, p0, Lg9/c;->f:Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lg9/c;->f:Z

    iget-boolean v0, p0, Lg9/c;->c:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lg9/c;->g()F

    move-result v0

    iput v0, p0, Lg9/c;->b:F

    :cond_0
    iget v0, p0, Lg9/c;->b:F

    iget v1, p0, Lg9/c;->g:F

    cmpl-float v1, v0, v1

    if-gtz v1, :cond_1

    iget v1, p0, Lg9/c;->h:F

    cmpg-float v0, v0, v1

    if-ltz v0, :cond_1

    invoke-static {}, Lg9/b;->i()Lg9/b;

    move-result-object v0

    iget-wide v1, p0, Lg9/c;->k:J

    invoke-virtual {v0, p0, v1, v2}, Lg9/b;->f(Lg9/b$b;J)V

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Starting value need to be in between min value and max value"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    :goto_0
    return-void
.end method


# virtual methods
.method public a(J)Z
    .locals 4

    iget-wide v0, p0, Lg9/c;->i:J

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    const/4 v3, 0x0

    if-nez v2, :cond_0

    iput-wide p1, p0, Lg9/c;->i:J

    iget p1, p0, Lg9/c;->b:F

    invoke-virtual {p0, p1}, Lg9/c;->l(F)V

    return v3

    :cond_0
    sub-long v0, p1, v0

    iput-wide p1, p0, Lg9/c;->i:J

    invoke-virtual {p0, v0, v1}, Lg9/c;->s(J)Z

    move-result p1

    iget p2, p0, Lg9/c;->b:F

    iget v0, p0, Lg9/c;->g:F

    invoke-static {p2, v0}, Ljava/lang/Math;->min(FF)F

    move-result p2

    iput p2, p0, Lg9/c;->b:F

    iget v0, p0, Lg9/c;->h:F

    invoke-static {p2, v0}, Ljava/lang/Math;->max(FF)F

    move-result p2

    iput p2, p0, Lg9/c;->b:F

    invoke-virtual {p0, p2}, Lg9/c;->l(F)V

    if-eqz p1, :cond_1

    invoke-direct {p0, v3}, Lg9/c;->e(Z)V

    :cond_1
    return p1
.end method

.method public b(Lg9/c$b;)Lg9/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lg9/c$b;",
            ")TT;"
        }
    .end annotation

    iget-object v0, p0, Lg9/c;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lg9/c;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-object p0
.end method

.method public c(Lg9/c$c;)Lg9/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lg9/c$c;",
            ")TT;"
        }
    .end annotation

    invoke-virtual {p0}, Lg9/c;->i()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lg9/c;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lg9/c;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-object p0

    :cond_1
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "Error: Update listeners must be added beforethe miuix.animation."

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public d()V
    .locals 2

    invoke-virtual {p0}, Lg9/c;->f()Lg9/b;

    move-result-object v0

    invoke-virtual {v0}, Lg9/b;->l()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lg9/c;->f:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lg9/c;->e(Z)V

    :cond_0
    return-void

    :cond_1
    new-instance v0, Landroid/util/AndroidRuntimeException;

    const-string v1, "Animations may only be canceled from the same thread as the animation handler"

    invoke-direct {v0, v1}, Landroid/util/AndroidRuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public f()Lg9/b;
    .locals 1

    invoke-static {}, Lg9/b;->i()Lg9/b;

    move-result-object v0

    return-object v0
.end method

.method h()F
    .locals 2

    goto/32 :goto_2

    nop

    :goto_0
    mul-float/2addr v0, v1

    goto/32 :goto_3

    nop

    :goto_1
    const/high16 v1, 0x3f400000    # 0.75f

    goto/32 :goto_0

    nop

    :goto_2
    iget v0, p0, Lg9/c;->j:F

    goto/32 :goto_1

    nop

    :goto_3
    return v0
.end method

.method public i()Z
    .locals 1

    iget-boolean v0, p0, Lg9/c;->f:Z

    return v0
.end method

.method public k(F)Lg9/c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(F)TT;"
        }
    .end annotation

    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-lez v0, :cond_0

    iput p1, p0, Lg9/c;->j:F

    const/high16 v0, 0x3f400000    # 0.75f

    mul-float/2addr p1, v0

    invoke-virtual {p0, p1}, Lg9/c;->p(F)V

    return-object p0

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Minimum visible change must be positive."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method l(F)V
    .locals 3

    goto/32 :goto_5

    nop

    :goto_0
    goto :goto_13

    :goto_1
    goto/32 :goto_9

    nop

    :goto_2
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_c

    nop

    :goto_3
    iget-object v0, p0, Lg9/c;->m:Ljava/util/ArrayList;

    goto/32 :goto_2

    nop

    :goto_4
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_16

    nop

    :goto_5
    iget-object v0, p0, Lg9/c;->e:Lh9/b;

    goto/32 :goto_b

    nop

    :goto_6
    invoke-interface {v0, p0, v1, v2}, Lg9/c$c;->a(Lg9/c;FF)V

    :goto_7
    goto/32 :goto_14

    nop

    :goto_8
    invoke-virtual {v0, v1, p1}, Lh9/b;->f(Ljava/lang/Object;F)V

    goto/32 :goto_12

    nop

    :goto_9
    iget-object p1, p0, Lg9/c;->m:Ljava/util/ArrayList;

    goto/32 :goto_11

    nop

    :goto_a
    iget v2, p0, Lg9/c;->a:F

    goto/32 :goto_6

    nop

    :goto_b
    iget-object v1, p0, Lg9/c;->d:Ljava/lang/Object;

    goto/32 :goto_8

    nop

    :goto_c
    check-cast v0, Lg9/c$c;

    goto/32 :goto_f

    nop

    :goto_d
    if-lt p1, v0, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_15

    nop

    :goto_e
    return-void

    :goto_f
    iget v1, p0, Lg9/c;->b:F

    goto/32 :goto_a

    nop

    :goto_10
    iget-object v0, p0, Lg9/c;->m:Ljava/util/ArrayList;

    goto/32 :goto_17

    nop

    :goto_11
    invoke-static {p1}, Lg9/c;->j(Ljava/util/ArrayList;)V

    goto/32 :goto_e

    nop

    :goto_12
    const/4 p1, 0x0

    :goto_13
    goto/32 :goto_10

    nop

    :goto_14
    add-int/lit8 p1, p1, 0x1

    goto/32 :goto_0

    nop

    :goto_15
    iget-object v0, p0, Lg9/c;->m:Ljava/util/ArrayList;

    goto/32 :goto_4

    nop

    :goto_16
    if-nez v0, :cond_1

    goto/32 :goto_7

    :cond_1
    goto/32 :goto_3

    nop

    :goto_17
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto/32 :goto_d

    nop
.end method

.method public m(J)V
    .locals 3

    const-wide/16 v0, 0x0

    cmp-long v2, p1, v0

    if-gez v2, :cond_0

    move-wide p1, v0

    :cond_0
    iput-wide p1, p0, Lg9/c;->k:J

    return-void
.end method

.method public n(F)Lg9/c;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(F)TT;"
        }
    .end annotation

    iput p1, p0, Lg9/c;->b:F

    const/4 p1, 0x1

    iput-boolean p1, p0, Lg9/c;->c:Z

    return-object p0
.end method

.method public o(F)Lg9/c;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(F)TT;"
        }
    .end annotation

    iput p1, p0, Lg9/c;->a:F

    return-object p0
.end method

.method abstract p(F)V
.end method

.method public q()V
    .locals 2

    invoke-virtual {p0}, Lg9/c;->f()Lg9/b;

    move-result-object v0

    invoke-virtual {v0}, Lg9/b;->l()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lg9/c;->f:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lg9/c;->r()V

    :cond_0
    return-void

    :cond_1
    new-instance v0, Landroid/util/AndroidRuntimeException;

    const-string v1, "Animations may only be started on the same thread as the animation handler"

    invoke-direct {v0, v1}, Landroid/util/AndroidRuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method abstract s(J)Z
.end method
