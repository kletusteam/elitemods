.class Lg9/b$d;
.super Lg9/b$c;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lg9/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "d"
.end annotation


# instance fields
.field private final b:Landroid/view/Choreographer;

.field private final c:Landroid/os/Looper;

.field private final d:Landroid/view/Choreographer$FrameCallback;


# direct methods
.method constructor <init>(Lg9/b$a;)V
    .locals 0

    invoke-direct {p0, p1}, Lg9/b$c;-><init>(Lg9/b$a;)V

    invoke-static {}, Landroid/view/Choreographer;->getInstance()Landroid/view/Choreographer;

    move-result-object p1

    iput-object p1, p0, Lg9/b$d;->b:Landroid/view/Choreographer;

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object p1

    iput-object p1, p0, Lg9/b$d;->c:Landroid/os/Looper;

    new-instance p1, Lg9/b$d$a;

    invoke-direct {p1, p0}, Lg9/b$d$a;-><init>(Lg9/b$d;)V

    iput-object p1, p0, Lg9/b$d;->d:Landroid/view/Choreographer$FrameCallback;

    return-void
.end method


# virtual methods
.method a()Z
    .locals 2

    goto/32 :goto_1

    nop

    :goto_0
    iget-object v1, p0, Lg9/b$d;->c:Landroid/os/Looper;

    goto/32 :goto_4

    nop

    :goto_1
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    goto/32 :goto_0

    nop

    :goto_2
    return v0

    :goto_3
    const/4 v0, 0x1

    goto/32 :goto_5

    nop

    :goto_4
    invoke-virtual {v1}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v1

    goto/32 :goto_7

    nop

    :goto_5
    goto :goto_9

    :goto_6
    goto/32 :goto_8

    nop

    :goto_7
    if-eq v0, v1, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_3

    nop

    :goto_8
    const/4 v0, 0x0

    :goto_9
    goto/32 :goto_2

    nop
.end method

.method b()V
    .locals 2

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    iget-object v0, p0, Lg9/b$d;->b:Landroid/view/Choreographer;

    goto/32 :goto_2

    nop

    :goto_2
    iget-object v1, p0, Lg9/b$d;->d:Landroid/view/Choreographer$FrameCallback;

    goto/32 :goto_3

    nop

    :goto_3
    invoke-virtual {v0, v1}, Landroid/view/Choreographer;->postFrameCallback(Landroid/view/Choreographer$FrameCallback;)V

    goto/32 :goto_0

    nop
.end method
