.class public Lg9/b;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lg9/b$c;,
        Lg9/b$d;,
        Lg9/b$a;,
        Lg9/b$b;
    }
.end annotation


# static fields
.field public static final g:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal<",
            "Lg9/b;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Landroid/util/ArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArrayMap<",
            "Lg9/b$b;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lg9/b$b;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lg9/b$a;

.field private d:Lg9/b$c;

.field private e:J

.field private f:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    sput-object v0, Lg9/b;->g:Ljava/lang/ThreadLocal;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lg9/b;->a:Landroid/util/ArrayMap;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lg9/b;->b:Ljava/util/ArrayList;

    new-instance v0, Lg9/b$a;

    invoke-direct {v0, p0}, Lg9/b$a;-><init>(Lg9/b;)V

    iput-object v0, p0, Lg9/b;->c:Lg9/b$a;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lg9/b;->e:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Lg9/b;->f:Z

    return-void
.end method

.method static synthetic a(Lg9/b;)J
    .locals 2

    iget-wide v0, p0, Lg9/b;->e:J

    return-wide v0
.end method

.method static synthetic b(Lg9/b;J)J
    .locals 0

    iput-wide p1, p0, Lg9/b;->e:J

    return-wide p1
.end method

.method static synthetic c(Lg9/b;J)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lg9/b;->h(J)V

    return-void
.end method

.method static synthetic d(Lg9/b;)Ljava/util/ArrayList;
    .locals 0

    iget-object p0, p0, Lg9/b;->b:Ljava/util/ArrayList;

    return-object p0
.end method

.method static synthetic e(Lg9/b;)Lg9/b$c;
    .locals 0

    invoke-direct {p0}, Lg9/b;->j()Lg9/b$c;

    move-result-object p0

    return-object p0
.end method

.method private g()V
    .locals 2

    iget-boolean v0, p0, Lg9/b;->f:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lg9/b;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    iget-object v1, p0, Lg9/b;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lg9/b;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lg9/b;->f:Z

    :cond_2
    return-void
.end method

.method private h(J)V
    .locals 5

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    const/4 v2, 0x0

    :goto_0
    iget-object v3, p0, Lg9/b;->b:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v2, v3, :cond_2

    iget-object v3, p0, Lg9/b;->b:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lg9/b$b;

    if-nez v3, :cond_0

    goto :goto_1

    :cond_0
    invoke-direct {p0, v3, v0, v1}, Lg9/b;->k(Lg9/b$b;J)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3, p1, p2}, Lg9/b$b;->a(J)Z

    :cond_1
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lg9/b;->g()V

    return-void
.end method

.method public static i()Lg9/b;
    .locals 2

    sget-object v0, Lg9/b;->g:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    new-instance v1, Lg9/b;

    invoke-direct {v1}, Lg9/b;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    :cond_0
    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lg9/b;

    return-object v0
.end method

.method private j()Lg9/b$c;
    .locals 2

    iget-object v0, p0, Lg9/b;->d:Lg9/b$c;

    if-nez v0, :cond_0

    new-instance v0, Lg9/b$d;

    iget-object v1, p0, Lg9/b;->c:Lg9/b$a;

    invoke-direct {v0, v1}, Lg9/b$d;-><init>(Lg9/b$a;)V

    iput-object v0, p0, Lg9/b;->d:Lg9/b$c;

    :cond_0
    iget-object v0, p0, Lg9/b;->d:Lg9/b$c;

    return-object v0
.end method

.method private k(Lg9/b$b;J)Z
    .locals 4

    iget-object v0, p0, Lg9/b;->a:Landroid/util/ArrayMap;

    invoke-virtual {v0, p1}, Landroid/util/ArrayMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    const/4 v1, 0x1

    if-nez v0, :cond_0

    return v1

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long p2, v2, p2

    if-gez p2, :cond_1

    iget-object p2, p0, Lg9/b;->a:Landroid/util/ArrayMap;

    invoke-virtual {p2, p1}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return v1

    :cond_1
    const/4 p1, 0x0

    return p1
.end method


# virtual methods
.method public f(Lg9/b$b;J)V
    .locals 3

    iget-object v0, p0, Lg9/b;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lg9/b;->j()Lg9/b$c;

    move-result-object v0

    invoke-virtual {v0}, Lg9/b$c;->b()V

    :cond_0
    iget-object v0, p0, Lg9/b;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lg9/b;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-lez v0, :cond_2

    iget-object v0, p0, Lg9/b;->a:Landroid/util/ArrayMap;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    add-long/2addr v1, p2

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-virtual {v0, p1, p2}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    return-void
.end method

.method l()Z
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    return v0

    :goto_1
    invoke-virtual {v0}, Lg9/b$c;->a()Z

    move-result v0

    goto/32 :goto_0

    nop

    :goto_2
    invoke-direct {p0}, Lg9/b;->j()Lg9/b$c;

    move-result-object v0

    goto/32 :goto_1

    nop
.end method

.method public m(Lg9/b$b;)V
    .locals 2

    iget-object v0, p0, Lg9/b;->a:Landroid/util/ArrayMap;

    invoke-virtual {v0, p1}, Landroid/util/ArrayMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lg9/b;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result p1

    if-ltz p1, :cond_0

    iget-object v0, p0, Lg9/b;->b:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    const/4 p1, 0x1

    iput-boolean p1, p0, Lg9/b;->f:Z

    :cond_0
    return-void
.end method
