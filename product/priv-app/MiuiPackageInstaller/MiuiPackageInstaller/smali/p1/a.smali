.class public Lp1/a;
.super Ljava/lang/Object;


# direct methods
.method public static a(Landroid/content/Context;Ljava/lang/String;Ls1/d;)V
    .locals 3

    if-eqz p0, :cond_1

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    if-nez p2, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "sdkId"

    const-string v2, "httpdns"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "sdkVer"

    const-string v2, "2.1.0"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "accountId"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lj1/a$c;

    invoke-direct {v1}, Lj1/a$c;-><init>()V

    invoke-virtual {v1}, Lj1/a$c;->b()Lj1/a$c;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lj1/a$c;->d(Z)Lj1/a$c;

    move-result-object v1

    invoke-virtual {v1, v0}, Lj1/a$c;->c(Ljava/util/Map;)Lj1/a$c;

    move-result-object v0

    invoke-virtual {v0}, Lj1/a$c;->a()Lj1/a;

    move-result-object v0

    new-instance v1, Lp1/a$a;

    invoke-direct {v1, p2, p1}, Lp1/a$a;-><init>(Ls1/d;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lj1/a;->l(Lj1/a$g;)V

    new-instance p1, Lp1/a$b;

    invoke-direct {p1}, Lp1/a$b;-><init>()V

    invoke-virtual {v0, p1}, Lj1/a;->k(Lj1/a$f;)V

    invoke-virtual {v0, p0}, Lj1/a;->x(Landroid/content/Context;)V

    return-void

    :cond_1
    :goto_0
    const-string p0, "params is empty"

    invoke-static {p0}, Lz1/a;->i(Ljava/lang/String;)V

    return-void
.end method
