.class final Lp1/a$a;
.super Ljava/lang/Object;

# interfaces
.implements Lj1/a$g;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lp1/a;->a(Landroid/content/Context;Ljava/lang/String;Ls1/d;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic a:Ls1/d;

.field final synthetic b:Ljava/lang/String;


# direct methods
.method constructor <init>(Ls1/d;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lp1/a$a;->a:Ls1/d;

    iput-object p2, p0, Lp1/a$a;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lj1/a$d;",
            ">;)V"
        }
    .end annotation

    const-string v0, "disabled"

    const-string v1, "normal"

    if-eqz p1, :cond_4

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_4

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lj1/a$d;

    iget-object v3, v2, Lj1/a$d;->a:Ljava/lang/String;

    const-string v4, "___httpdns_service___"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    :try_start_0
    new-instance v3, Lorg/json/JSONObject;

    iget-object v2, v2, Lj1/a$d;->b:Ljava/lang/String;

    invoke-direct {v3, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v2, "status"

    invoke-virtual {v3, v2, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-eqz v2, :cond_1

    iget-object v2, p0, Lp1/a$a;->a:Ls1/d;

    invoke-virtual {v2, v4}, Ls1/d;->r(Z)V

    const-string v2, "beacon disabled"

    invoke-static {v2}, Lz1/a;->i(Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    iget-object v2, p0, Lp1/a$a;->a:Ls1/d;

    invoke-virtual {v2, v5}, Ls1/d;->r(Z)V

    const-string v2, "beacon normal"

    invoke-static {v2}, Lz1/a;->b(Ljava/lang/String;)V

    :goto_1
    const-string v2, "ut"

    invoke-virtual {v3, v2, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lp1/a$a;->b:Ljava/lang/String;

    invoke-static {v2, v5}, Lu1/b;->f(Ljava/lang/String;Z)V

    goto :goto_2

    :cond_2
    iget-object v2, p0, Lp1/a$a;->b:Ljava/lang/String;

    invoke-static {v2, v4}, Lu1/b;->f(Ljava/lang/String;Z)V

    :goto_2
    const-string v2, "ip-ranking"

    invoke-virtual {v3, v2, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lp1/a$a;->a:Ls1/d;

    invoke-virtual {v2, v4}, Ls1/d;->u(Z)V

    const-string v2, "beacon probe disabled"

    invoke-static {v2}, Lz1/a;->i(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lp1/a$a;->a:Ls1/d;

    invoke-virtual {v2, v5}, Ls1/d;->u(Z)V

    const-string v2, "beacon probe normal"

    invoke-static {v2}, Lz1/a;->b(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    :cond_4
    return-void
.end method
