.class public Lx0/d;
.super Ljava/lang/Object;

# interfaces
.implements Lx0/e;
.implements Lx0/m;
.implements Ly0/a$b;
.implements La1/f;


# instance fields
.field private a:Landroid/graphics/Paint;

.field private b:Landroid/graphics/RectF;

.field private final c:Landroid/graphics/Matrix;

.field private final d:Landroid/graphics/Path;

.field private final e:Landroid/graphics/RectF;

.field private final f:Ljava/lang/String;

.field private final g:Z

.field private final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lx0/c;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Lcom/airbnb/lottie/a;

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lx0/m;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ly0/o;


# direct methods
.method public constructor <init>(Lcom/airbnb/lottie/a;Ld1/a;Lc1/n;)V
    .locals 7

    invoke-virtual {p3}, Lc1/n;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p3}, Lc1/n;->d()Z

    move-result v4

    invoke-virtual {p3}, Lc1/n;->b()Ljava/util/List;

    move-result-object v0

    invoke-static {p1, p2, v0}, Lx0/d;->d(Lcom/airbnb/lottie/a;Ld1/a;Ljava/util/List;)Ljava/util/List;

    move-result-object v5

    invoke-virtual {p3}, Lc1/n;->b()Ljava/util/List;

    move-result-object p3

    invoke-static {p3}, Lx0/d;->i(Ljava/util/List;)Lb1/l;

    move-result-object v6

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v6}, Lx0/d;-><init>(Lcom/airbnb/lottie/a;Ld1/a;Ljava/lang/String;ZLjava/util/List;Lb1/l;)V

    return-void
.end method

.method constructor <init>(Lcom/airbnb/lottie/a;Ld1/a;Ljava/lang/String;ZLjava/util/List;Lb1/l;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/airbnb/lottie/a;",
            "Ld1/a;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/List<",
            "Lx0/c;",
            ">;",
            "Lb1/l;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lw0/a;

    invoke-direct {v0}, Lw0/a;-><init>()V

    iput-object v0, p0, Lx0/d;->a:Landroid/graphics/Paint;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lx0/d;->b:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lx0/d;->c:Landroid/graphics/Matrix;

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lx0/d;->d:Landroid/graphics/Path;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lx0/d;->e:Landroid/graphics/RectF;

    iput-object p3, p0, Lx0/d;->f:Ljava/lang/String;

    iput-object p1, p0, Lx0/d;->i:Lcom/airbnb/lottie/a;

    iput-boolean p4, p0, Lx0/d;->g:Z

    iput-object p5, p0, Lx0/d;->h:Ljava/util/List;

    if-eqz p6, :cond_0

    invoke-virtual {p6}, Lb1/l;->b()Ly0/o;

    move-result-object p1

    iput-object p1, p0, Lx0/d;->k:Ly0/o;

    invoke-virtual {p1, p2}, Ly0/o;->a(Ld1/a;)V

    iget-object p1, p0, Lx0/d;->k:Ly0/o;

    invoke-virtual {p1, p0}, Ly0/o;->b(Ly0/a$b;)V

    :cond_0
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p5}, Ljava/util/List;->size()I

    move-result p2

    add-int/lit8 p2, p2, -0x1

    :goto_0
    if-ltz p2, :cond_2

    invoke-interface {p5, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lx0/c;

    instance-of p4, p3, Lx0/j;

    if-eqz p4, :cond_1

    check-cast p3, Lx0/j;

    invoke-interface {p1, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 p2, p2, -0x1

    goto :goto_0

    :cond_2
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p2

    add-int/lit8 p2, p2, -0x1

    :goto_1
    if-ltz p2, :cond_3

    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lx0/j;

    invoke-interface {p5}, Ljava/util/List;->size()I

    move-result p4

    invoke-interface {p5, p4}, Ljava/util/List;->listIterator(I)Ljava/util/ListIterator;

    move-result-object p4

    invoke-interface {p3, p4}, Lx0/j;->d(Ljava/util/ListIterator;)V

    add-int/lit8 p2, p2, -0x1

    goto :goto_1

    :cond_3
    return-void
.end method

.method private static d(Lcom/airbnb/lottie/a;Ld1/a;Ljava/util/List;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/airbnb/lottie/a;",
            "Ld1/a;",
            "Ljava/util/List<",
            "Lc1/b;",
            ">;)",
            "Ljava/util/List<",
            "Lx0/c;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v1, 0x0

    :goto_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lc1/b;

    invoke-interface {v2, p0, p1}, Lc1/b;->a(Lcom/airbnb/lottie/a;Ld1/a;)Lx0/c;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method static i(Ljava/util/List;)Lb1/l;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lc1/b;",
            ">;)",
            "Lb1/l;"
        }
    .end annotation

    const/4 v0, 0x0

    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lc1/b;

    instance-of v2, v1, Lb1/l;

    if-eqz v2, :cond_0

    check-cast v1, Lb1/l;

    return-object v1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    return-object p0
.end method

.method private l()Z
    .locals 4

    const/4 v0, 0x0

    move v1, v0

    move v2, v1

    :goto_0
    iget-object v3, p0, Lx0/d;->h:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_1

    iget-object v3, p0, Lx0/d;->h:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    instance-of v3, v3, Lx0/e;

    if-eqz v3, :cond_0

    add-int/lit8 v2, v2, 0x1

    const/4 v3, 0x2

    if-lt v2, v3, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return v0
.end method


# virtual methods
.method public a(Landroid/graphics/RectF;Landroid/graphics/Matrix;Z)V
    .locals 3

    iget-object v0, p0, Lx0/d;->c:Landroid/graphics/Matrix;

    invoke-virtual {v0, p2}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    iget-object p2, p0, Lx0/d;->k:Ly0/o;

    if-eqz p2, :cond_0

    iget-object v0, p0, Lx0/d;->c:Landroid/graphics/Matrix;

    invoke-virtual {p2}, Ly0/o;->f()Landroid/graphics/Matrix;

    move-result-object p2

    invoke-virtual {v0, p2}, Landroid/graphics/Matrix;->preConcat(Landroid/graphics/Matrix;)Z

    :cond_0
    iget-object p2, p0, Lx0/d;->e:Landroid/graphics/RectF;

    const/4 v0, 0x0

    invoke-virtual {p2, v0, v0, v0, v0}, Landroid/graphics/RectF;->set(FFFF)V

    iget-object p2, p0, Lx0/d;->h:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    add-int/lit8 p2, p2, -0x1

    :goto_0
    if-ltz p2, :cond_2

    iget-object v0, p0, Lx0/d;->h:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lx0/c;

    instance-of v1, v0, Lx0/e;

    if-eqz v1, :cond_1

    check-cast v0, Lx0/e;

    iget-object v1, p0, Lx0/d;->e:Landroid/graphics/RectF;

    iget-object v2, p0, Lx0/d;->c:Landroid/graphics/Matrix;

    invoke-interface {v0, v1, v2, p3}, Lx0/e;->a(Landroid/graphics/RectF;Landroid/graphics/Matrix;Z)V

    iget-object v0, p0, Lx0/d;->e:Landroid/graphics/RectF;

    invoke-virtual {p1, v0}, Landroid/graphics/RectF;->union(Landroid/graphics/RectF;)V

    :cond_1
    add-int/lit8 p2, p2, -0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lx0/d;->i:Lcom/airbnb/lottie/a;

    invoke-virtual {v0}, Lcom/airbnb/lottie/a;->invalidateSelf()V

    return-void
.end method

.method public c(Ljava/util/List;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lx0/c;",
            ">;",
            "Ljava/util/List<",
            "Lx0/c;",
            ">;)V"
        }
    .end annotation

    new-instance p2, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lx0/d;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    invoke-direct {p2, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {p2, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget-object p1, p0, Lx0/d;->h:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    add-int/lit8 p1, p1, -0x1

    :goto_0
    if-ltz p1, :cond_0

    iget-object v0, p0, Lx0/d;->h:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lx0/c;

    iget-object v1, p0, Lx0/d;->h:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2, p1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, p2, v1}, Lx0/c;->c(Ljava/util/List;Ljava/util/List;)V

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 p1, p1, -0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public e(Landroid/graphics/Canvas;Landroid/graphics/Matrix;I)V
    .locals 4

    iget-boolean v0, p0, Lx0/d;->g:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lx0/d;->c:Landroid/graphics/Matrix;

    invoke-virtual {v0, p2}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    iget-object p2, p0, Lx0/d;->k:Ly0/o;

    if-eqz p2, :cond_2

    iget-object v0, p0, Lx0/d;->c:Landroid/graphics/Matrix;

    invoke-virtual {p2}, Ly0/o;->f()Landroid/graphics/Matrix;

    move-result-object p2

    invoke-virtual {v0, p2}, Landroid/graphics/Matrix;->preConcat(Landroid/graphics/Matrix;)Z

    iget-object p2, p0, Lx0/d;->k:Ly0/o;

    invoke-virtual {p2}, Ly0/o;->h()Ly0/a;

    move-result-object p2

    if-nez p2, :cond_1

    const/16 p2, 0x64

    goto :goto_0

    :cond_1
    iget-object p2, p0, Lx0/d;->k:Ly0/o;

    invoke-virtual {p2}, Ly0/o;->h()Ly0/a;

    move-result-object p2

    invoke-virtual {p2}, Ly0/a;->h()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p2

    :goto_0
    int-to-float p2, p2

    const/high16 v0, 0x42c80000    # 100.0f

    div-float/2addr p2, v0

    int-to-float p3, p3

    mul-float/2addr p2, p3

    const/high16 p3, 0x437f0000    # 255.0f

    div-float/2addr p2, p3

    mul-float/2addr p2, p3

    float-to-int p3, p2

    :cond_2
    iget-object p2, p0, Lx0/d;->i:Lcom/airbnb/lottie/a;

    invoke-virtual {p2}, Lcom/airbnb/lottie/a;->G()Z

    move-result p2

    const/16 v0, 0xff

    const/4 v1, 0x1

    if-eqz p2, :cond_3

    invoke-direct {p0}, Lx0/d;->l()Z

    move-result p2

    if-eqz p2, :cond_3

    if-eq p3, v0, :cond_3

    move p2, v1

    goto :goto_1

    :cond_3
    const/4 p2, 0x0

    :goto_1
    if-eqz p2, :cond_4

    iget-object v2, p0, Lx0/d;->b:Landroid/graphics/RectF;

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v3, v3, v3}, Landroid/graphics/RectF;->set(FFFF)V

    iget-object v2, p0, Lx0/d;->b:Landroid/graphics/RectF;

    iget-object v3, p0, Lx0/d;->c:Landroid/graphics/Matrix;

    invoke-virtual {p0, v2, v3, v1}, Lx0/d;->a(Landroid/graphics/RectF;Landroid/graphics/Matrix;Z)V

    iget-object v2, p0, Lx0/d;->a:Landroid/graphics/Paint;

    invoke-virtual {v2, p3}, Landroid/graphics/Paint;->setAlpha(I)V

    iget-object v2, p0, Lx0/d;->b:Landroid/graphics/RectF;

    iget-object v3, p0, Lx0/d;->a:Landroid/graphics/Paint;

    invoke-static {p1, v2, v3}, Lh1/h;->m(Landroid/graphics/Canvas;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    :cond_4
    if-eqz p2, :cond_5

    move p3, v0

    :cond_5
    iget-object v0, p0, Lx0/d;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int/2addr v0, v1

    :goto_2
    if-ltz v0, :cond_7

    iget-object v1, p0, Lx0/d;->h:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    instance-of v2, v1, Lx0/e;

    if-eqz v2, :cond_6

    check-cast v1, Lx0/e;

    iget-object v2, p0, Lx0/d;->c:Landroid/graphics/Matrix;

    invoke-interface {v1, p1, v2, p3}, Lx0/e;->e(Landroid/graphics/Canvas;Landroid/graphics/Matrix;I)V

    :cond_6
    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    :cond_7
    if-eqz p2, :cond_8

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    :cond_8
    return-void
.end method

.method public f(La1/e;ILjava/util/List;La1/e;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "La1/e;",
            "I",
            "Ljava/util/List<",
            "La1/e;",
            ">;",
            "La1/e;",
            ")V"
        }
    .end annotation

    invoke-virtual {p0}, Lx0/d;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, La1/e;->g(Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lx0/d;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "__container"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lx0/d;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p4, v0}, La1/e;->a(Ljava/lang/String;)La1/e;

    move-result-object p4

    invoke-virtual {p0}, Lx0/d;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, La1/e;->c(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p4, p0}, La1/e;->i(La1/f;)La1/e;

    move-result-object v0

    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-virtual {p0}, Lx0/d;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, La1/e;->h(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lx0/d;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, La1/e;->e(Ljava/lang/String;I)I

    move-result v0

    add-int/2addr p2, v0

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lx0/d;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_3

    iget-object v1, p0, Lx0/d;->h:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lx0/c;

    instance-of v2, v1, La1/f;

    if-eqz v2, :cond_2

    check-cast v1, La1/f;

    invoke-interface {v1, p1, p2, p3, p4}, La1/f;->f(La1/e;ILjava/util/List;La1/e;)V

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    return-void
.end method

.method public g()Landroid/graphics/Path;
    .locals 4

    iget-object v0, p0, Lx0/d;->c:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    iget-object v0, p0, Lx0/d;->k:Ly0/o;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lx0/d;->c:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Ly0/o;->f()Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    :cond_0
    iget-object v0, p0, Lx0/d;->d:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    iget-boolean v0, p0, Lx0/d;->g:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lx0/d;->d:Landroid/graphics/Path;

    return-object v0

    :cond_1
    iget-object v0, p0, Lx0/d;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_3

    iget-object v1, p0, Lx0/d;->h:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lx0/c;

    instance-of v2, v1, Lx0/m;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lx0/d;->d:Landroid/graphics/Path;

    check-cast v1, Lx0/m;

    invoke-interface {v1}, Lx0/m;->g()Landroid/graphics/Path;

    move-result-object v1

    iget-object v3, p0, Lx0/d;->c:Landroid/graphics/Matrix;

    invoke-virtual {v2, v1, v3}, Landroid/graphics/Path;->addPath(Landroid/graphics/Path;Landroid/graphics/Matrix;)V

    :cond_2
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lx0/d;->d:Landroid/graphics/Path;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lx0/d;->f:Ljava/lang/String;

    return-object v0
.end method

.method public h(Ljava/lang/Object;Li1/c;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;",
            "Li1/c<",
            "TT;>;)V"
        }
    .end annotation

    iget-object v0, p0, Lx0/d;->k:Ly0/o;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1, p2}, Ly0/o;->c(Ljava/lang/Object;Li1/c;)Z

    :cond_0
    return-void
.end method

.method j()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lx0/m;",
            ">;"
        }
    .end annotation

    goto/32 :goto_e

    nop

    :goto_0
    new-instance v0, Ljava/util/ArrayList;

    goto/32 :goto_2

    nop

    :goto_1
    iput-object v0, p0, Lx0/d;->j:Ljava/util/List;

    goto/32 :goto_8

    nop

    :goto_2
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto/32 :goto_1

    nop

    :goto_3
    iget-object v0, p0, Lx0/d;->j:Ljava/util/List;

    goto/32 :goto_17

    nop

    :goto_4
    check-cast v1, Lx0/m;

    goto/32 :goto_5

    nop

    :goto_5
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_6
    goto/32 :goto_12

    nop

    :goto_7
    iget-object v1, p0, Lx0/d;->h:Ljava/util/List;

    goto/32 :goto_b

    nop

    :goto_8
    const/4 v0, 0x0

    :goto_9
    goto/32 :goto_16

    nop

    :goto_a
    if-lt v0, v1, :cond_0

    goto/32 :goto_15

    :cond_0
    goto/32 :goto_7

    nop

    :goto_b
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_13

    nop

    :goto_c
    iget-object v2, p0, Lx0/d;->j:Ljava/util/List;

    goto/32 :goto_4

    nop

    :goto_d
    if-nez v2, :cond_1

    goto/32 :goto_6

    :cond_1
    goto/32 :goto_c

    nop

    :goto_e
    iget-object v0, p0, Lx0/d;->j:Ljava/util/List;

    goto/32 :goto_11

    nop

    :goto_f
    instance-of v2, v1, Lx0/m;

    goto/32 :goto_d

    nop

    :goto_10
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    goto/32 :goto_a

    nop

    :goto_11
    if-eqz v0, :cond_2

    goto/32 :goto_15

    :cond_2
    goto/32 :goto_0

    nop

    :goto_12
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_14

    nop

    :goto_13
    check-cast v1, Lx0/c;

    goto/32 :goto_f

    nop

    :goto_14
    goto :goto_9

    :goto_15
    goto/32 :goto_3

    nop

    :goto_16
    iget-object v1, p0, Lx0/d;->h:Ljava/util/List;

    goto/32 :goto_10

    nop

    :goto_17
    return-object v0
.end method

.method k()Landroid/graphics/Matrix;
    .locals 1

    goto/32 :goto_6

    nop

    :goto_0
    return-object v0

    :goto_1
    goto/32 :goto_8

    nop

    :goto_2
    return-object v0

    :goto_3
    iget-object v0, p0, Lx0/d;->c:Landroid/graphics/Matrix;

    goto/32 :goto_2

    nop

    :goto_4
    if-nez v0, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_7

    nop

    :goto_5
    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    goto/32 :goto_3

    nop

    :goto_6
    iget-object v0, p0, Lx0/d;->k:Ly0/o;

    goto/32 :goto_4

    nop

    :goto_7
    invoke-virtual {v0}, Ly0/o;->f()Landroid/graphics/Matrix;

    move-result-object v0

    goto/32 :goto_0

    nop

    :goto_8
    iget-object v0, p0, Lx0/d;->c:Landroid/graphics/Matrix;

    goto/32 :goto_5

    nop
.end method
