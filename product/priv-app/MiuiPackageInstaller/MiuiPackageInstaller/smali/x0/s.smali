.class public Lx0/s;
.super Ljava/lang/Object;

# interfaces
.implements Lx0/c;
.implements Ly0/a$b;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Z

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ly0/a$b;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lc1/q$a;

.field private final e:Ly0/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ly0/a<",
            "*",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ly0/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ly0/a<",
            "*",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ly0/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ly0/a<",
            "*",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ld1/a;Lc1/q;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lx0/s;->c:Ljava/util/List;

    invoke-virtual {p2}, Lc1/q;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lx0/s;->a:Ljava/lang/String;

    invoke-virtual {p2}, Lc1/q;->g()Z

    move-result v0

    iput-boolean v0, p0, Lx0/s;->b:Z

    invoke-virtual {p2}, Lc1/q;->f()Lc1/q$a;

    move-result-object v0

    iput-object v0, p0, Lx0/s;->d:Lc1/q$a;

    invoke-virtual {p2}, Lc1/q;->e()Lb1/b;

    move-result-object v0

    invoke-virtual {v0}, Lb1/b;->a()Ly0/a;

    move-result-object v0

    iput-object v0, p0, Lx0/s;->e:Ly0/a;

    invoke-virtual {p2}, Lc1/q;->b()Lb1/b;

    move-result-object v1

    invoke-virtual {v1}, Lb1/b;->a()Ly0/a;

    move-result-object v1

    iput-object v1, p0, Lx0/s;->f:Ly0/a;

    invoke-virtual {p2}, Lc1/q;->d()Lb1/b;

    move-result-object p2

    invoke-virtual {p2}, Lb1/b;->a()Ly0/a;

    move-result-object p2

    iput-object p2, p0, Lx0/s;->g:Ly0/a;

    invoke-virtual {p1, v0}, Ld1/a;->j(Ly0/a;)V

    invoke-virtual {p1, v1}, Ld1/a;->j(Ly0/a;)V

    invoke-virtual {p1, p2}, Ld1/a;->j(Ly0/a;)V

    invoke-virtual {v0, p0}, Ly0/a;->a(Ly0/a$b;)V

    invoke-virtual {v1, p0}, Ly0/a;->a(Ly0/a$b;)V

    invoke-virtual {p2, p0}, Ly0/a;->a(Ly0/a$b;)V

    return-void
.end method


# virtual methods
.method public b()V
    .locals 2

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lx0/s;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lx0/s;->c:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ly0/a$b;

    invoke-interface {v1}, Ly0/a$b;->b()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public c(Ljava/util/List;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lx0/c;",
            ">;",
            "Ljava/util/List<",
            "Lx0/c;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method d(Ly0/a$b;)V
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    iget-object v0, p0, Lx0/s;->c:Ljava/util/List;

    goto/32 :goto_2

    nop

    :goto_2
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/32 :goto_0

    nop
.end method

.method public f()Ly0/a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ly0/a<",
            "*",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lx0/s;->f:Ly0/a;

    return-object v0
.end method

.method public h()Ly0/a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ly0/a<",
            "*",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lx0/s;->g:Ly0/a;

    return-object v0
.end method

.method public i()Ly0/a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ly0/a<",
            "*",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lx0/s;->e:Ly0/a;

    return-object v0
.end method

.method j()Lc1/q$a;
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Lx0/s;->d:Lc1/q$a;

    goto/32 :goto_1

    nop

    :goto_1
    return-object v0
.end method

.method public k()Z
    .locals 1

    iget-boolean v0, p0, Lx0/s;->b:Z

    return v0
.end method
