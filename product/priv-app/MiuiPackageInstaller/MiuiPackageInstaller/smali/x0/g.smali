.class public Lx0/g;
.super Ljava/lang/Object;

# interfaces
.implements Lx0/e;
.implements Ly0/a$b;
.implements Lx0/k;


# instance fields
.field private final a:Landroid/graphics/Path;

.field private final b:Landroid/graphics/Paint;

.field private final c:Ld1/a;

.field private final d:Ljava/lang/String;

.field private final e:Z

.field private final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lx0/m;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ly0/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ly0/a<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Ly0/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ly0/a<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ly0/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ly0/a<",
            "Landroid/graphics/ColorFilter;",
            "Landroid/graphics/ColorFilter;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Lcom/airbnb/lottie/a;


# direct methods
.method public constructor <init>(Lcom/airbnb/lottie/a;Ld1/a;Lc1/m;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lx0/g;->a:Landroid/graphics/Path;

    new-instance v1, Lw0/a;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Lw0/a;-><init>(I)V

    iput-object v1, p0, Lx0/g;->b:Landroid/graphics/Paint;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lx0/g;->f:Ljava/util/List;

    iput-object p2, p0, Lx0/g;->c:Ld1/a;

    invoke-virtual {p3}, Lc1/m;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lx0/g;->d:Ljava/lang/String;

    invoke-virtual {p3}, Lc1/m;->f()Z

    move-result v1

    iput-boolean v1, p0, Lx0/g;->e:Z

    iput-object p1, p0, Lx0/g;->j:Lcom/airbnb/lottie/a;

    invoke-virtual {p3}, Lc1/m;->b()Lb1/a;

    move-result-object p1

    if-eqz p1, :cond_1

    invoke-virtual {p3}, Lc1/m;->e()Lb1/d;

    move-result-object p1

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p3}, Lc1/m;->c()Landroid/graphics/Path$FillType;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    invoke-virtual {p3}, Lc1/m;->b()Lb1/a;

    move-result-object p1

    invoke-virtual {p1}, Lb1/a;->a()Ly0/a;

    move-result-object p1

    iput-object p1, p0, Lx0/g;->g:Ly0/a;

    invoke-virtual {p1, p0}, Ly0/a;->a(Ly0/a$b;)V

    invoke-virtual {p2, p1}, Ld1/a;->j(Ly0/a;)V

    invoke-virtual {p3}, Lc1/m;->e()Lb1/d;

    move-result-object p1

    invoke-virtual {p1}, Lb1/d;->a()Ly0/a;

    move-result-object p1

    iput-object p1, p0, Lx0/g;->h:Ly0/a;

    invoke-virtual {p1, p0}, Ly0/a;->a(Ly0/a$b;)V

    invoke-virtual {p2, p1}, Ld1/a;->j(Ly0/a;)V

    return-void

    :cond_1
    :goto_0
    const/4 p1, 0x0

    iput-object p1, p0, Lx0/g;->g:Ly0/a;

    iput-object p1, p0, Lx0/g;->h:Ly0/a;

    return-void
.end method


# virtual methods
.method public a(Landroid/graphics/RectF;Landroid/graphics/Matrix;Z)V
    .locals 3

    iget-object p3, p0, Lx0/g;->a:Landroid/graphics/Path;

    invoke-virtual {p3}, Landroid/graphics/Path;->reset()V

    const/4 p3, 0x0

    move v0, p3

    :goto_0
    iget-object v1, p0, Lx0/g;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lx0/g;->a:Landroid/graphics/Path;

    iget-object v2, p0, Lx0/g;->f:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lx0/m;

    invoke-interface {v2}, Lx0/m;->g()Landroid/graphics/Path;

    move-result-object v2

    invoke-virtual {v1, v2, p2}, Landroid/graphics/Path;->addPath(Landroid/graphics/Path;Landroid/graphics/Matrix;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object p2, p0, Lx0/g;->a:Landroid/graphics/Path;

    invoke-virtual {p2, p1, p3}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    iget p2, p1, Landroid/graphics/RectF;->left:F

    const/high16 p3, 0x3f800000    # 1.0f

    sub-float/2addr p2, p3

    iget v0, p1, Landroid/graphics/RectF;->top:F

    sub-float/2addr v0, p3

    iget v1, p1, Landroid/graphics/RectF;->right:F

    add-float/2addr v1, p3

    iget v2, p1, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v2, p3

    invoke-virtual {p1, p2, v0, v1, v2}, Landroid/graphics/RectF;->set(FFFF)V

    return-void
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lx0/g;->j:Lcom/airbnb/lottie/a;

    invoke-virtual {v0}, Lcom/airbnb/lottie/a;->invalidateSelf()V

    return-void
.end method

.method public c(Ljava/util/List;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lx0/c;",
            ">;",
            "Ljava/util/List<",
            "Lx0/c;",
            ">;)V"
        }
    .end annotation

    const/4 p1, 0x0

    :goto_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_1

    invoke-interface {p2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lx0/c;

    instance-of v1, v0, Lx0/m;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lx0/g;->f:Ljava/util/List;

    check-cast v0, Lx0/m;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public e(Landroid/graphics/Canvas;Landroid/graphics/Matrix;I)V
    .locals 4

    iget-boolean v0, p0, Lx0/g;->e:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const-string v0, "FillContent#draw"

    invoke-static {v0}, Lv0/c;->a(Ljava/lang/String;)V

    iget-object v1, p0, Lx0/g;->b:Landroid/graphics/Paint;

    iget-object v2, p0, Lx0/g;->g:Ly0/a;

    check-cast v2, Ly0/b;

    invoke-virtual {v2}, Ly0/b;->o()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    int-to-float p3, p3

    const/high16 v1, 0x437f0000    # 255.0f

    div-float/2addr p3, v1

    iget-object v2, p0, Lx0/g;->h:Ly0/a;

    invoke-virtual {v2}, Ly0/a;->h()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr p3, v2

    const/high16 v2, 0x42c80000    # 100.0f

    div-float/2addr p3, v2

    mul-float/2addr p3, v1

    float-to-int p3, p3

    iget-object v1, p0, Lx0/g;->b:Landroid/graphics/Paint;

    const/16 v2, 0xff

    const/4 v3, 0x0

    invoke-static {p3, v3, v2}, Lh1/g;->c(III)I

    move-result p3

    invoke-virtual {v1, p3}, Landroid/graphics/Paint;->setAlpha(I)V

    iget-object p3, p0, Lx0/g;->i:Ly0/a;

    if-eqz p3, :cond_1

    iget-object v1, p0, Lx0/g;->b:Landroid/graphics/Paint;

    invoke-virtual {p3}, Ly0/a;->h()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Landroid/graphics/ColorFilter;

    invoke-virtual {v1, p3}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    :cond_1
    iget-object p3, p0, Lx0/g;->a:Landroid/graphics/Path;

    invoke-virtual {p3}, Landroid/graphics/Path;->reset()V

    :goto_0
    iget-object p3, p0, Lx0/g;->f:Ljava/util/List;

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result p3

    if-ge v3, p3, :cond_2

    iget-object p3, p0, Lx0/g;->a:Landroid/graphics/Path;

    iget-object v1, p0, Lx0/g;->f:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lx0/m;

    invoke-interface {v1}, Lx0/m;->g()Landroid/graphics/Path;

    move-result-object v1

    invoke-virtual {p3, v1, p2}, Landroid/graphics/Path;->addPath(Landroid/graphics/Path;Landroid/graphics/Matrix;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    iget-object p2, p0, Lx0/g;->a:Landroid/graphics/Path;

    iget-object p3, p0, Lx0/g;->b:Landroid/graphics/Paint;

    invoke-virtual {p1, p2, p3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    invoke-static {v0}, Lv0/c;->b(Ljava/lang/String;)F

    return-void
.end method

.method public f(La1/e;ILjava/util/List;La1/e;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "La1/e;",
            "I",
            "Ljava/util/List<",
            "La1/e;",
            ">;",
            "La1/e;",
            ")V"
        }
    .end annotation

    invoke-static {p1, p2, p3, p4, p0}, Lh1/g;->l(La1/e;ILjava/util/List;La1/e;Lx0/k;)V

    return-void
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lx0/g;->d:Ljava/lang/String;

    return-object v0
.end method

.method public h(Ljava/lang/Object;Li1/c;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;",
            "Li1/c<",
            "TT;>;)V"
        }
    .end annotation

    sget-object v0, Lv0/j;->a:Ljava/lang/Integer;

    if-ne p1, v0, :cond_0

    iget-object p1, p0, Lx0/g;->g:Ly0/a;

    :goto_0
    invoke-virtual {p1, p2}, Ly0/a;->m(Li1/c;)V

    goto :goto_1

    :cond_0
    sget-object v0, Lv0/j;->d:Ljava/lang/Integer;

    if-ne p1, v0, :cond_1

    iget-object p1, p0, Lx0/g;->h:Ly0/a;

    goto :goto_0

    :cond_1
    sget-object v0, Lv0/j;->C:Landroid/graphics/ColorFilter;

    if-ne p1, v0, :cond_4

    iget-object p1, p0, Lx0/g;->i:Ly0/a;

    if-eqz p1, :cond_2

    iget-object v0, p0, Lx0/g;->c:Ld1/a;

    invoke-virtual {v0, p1}, Ld1/a;->D(Ly0/a;)V

    :cond_2
    if-nez p2, :cond_3

    const/4 p1, 0x0

    iput-object p1, p0, Lx0/g;->i:Ly0/a;

    goto :goto_1

    :cond_3
    new-instance p1, Ly0/p;

    invoke-direct {p1, p2}, Ly0/p;-><init>(Li1/c;)V

    iput-object p1, p0, Lx0/g;->i:Ly0/a;

    invoke-virtual {p1, p0}, Ly0/a;->a(Ly0/a$b;)V

    iget-object p1, p0, Lx0/g;->c:Ld1/a;

    iget-object p2, p0, Lx0/g;->i:Ly0/a;

    invoke-virtual {p1, p2}, Ld1/a;->j(Ly0/a;)V

    :cond_4
    :goto_1
    return-void
.end method
