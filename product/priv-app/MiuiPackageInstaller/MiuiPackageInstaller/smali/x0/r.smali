.class public Lx0/r;
.super Lx0/a;


# instance fields
.field private final o:Ld1/a;

.field private final p:Ljava/lang/String;

.field private final q:Z

.field private final r:Ly0/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ly0/a<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private s:Ly0/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ly0/a<",
            "Landroid/graphics/ColorFilter;",
            "Landroid/graphics/ColorFilter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/airbnb/lottie/a;Ld1/a;Lc1/p;)V
    .locals 11

    invoke-virtual {p3}, Lc1/p;->b()Lc1/p$b;

    move-result-object v0

    invoke-virtual {v0}, Lc1/p$b;->a()Landroid/graphics/Paint$Cap;

    move-result-object v4

    invoke-virtual {p3}, Lc1/p;->e()Lc1/p$c;

    move-result-object v0

    invoke-virtual {v0}, Lc1/p$c;->a()Landroid/graphics/Paint$Join;

    move-result-object v5

    invoke-virtual {p3}, Lc1/p;->g()F

    move-result v6

    invoke-virtual {p3}, Lc1/p;->i()Lb1/d;

    move-result-object v7

    invoke-virtual {p3}, Lc1/p;->j()Lb1/b;

    move-result-object v8

    invoke-virtual {p3}, Lc1/p;->f()Ljava/util/List;

    move-result-object v9

    invoke-virtual {p3}, Lc1/p;->d()Lb1/b;

    move-result-object v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v1 .. v10}, Lx0/a;-><init>(Lcom/airbnb/lottie/a;Ld1/a;Landroid/graphics/Paint$Cap;Landroid/graphics/Paint$Join;FLb1/d;Lb1/b;Ljava/util/List;Lb1/b;)V

    iput-object p2, p0, Lx0/r;->o:Ld1/a;

    invoke-virtual {p3}, Lc1/p;->h()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lx0/r;->p:Ljava/lang/String;

    invoke-virtual {p3}, Lc1/p;->k()Z

    move-result p1

    iput-boolean p1, p0, Lx0/r;->q:Z

    invoke-virtual {p3}, Lc1/p;->c()Lb1/a;

    move-result-object p1

    invoke-virtual {p1}, Lb1/a;->a()Ly0/a;

    move-result-object p1

    iput-object p1, p0, Lx0/r;->r:Ly0/a;

    invoke-virtual {p1, p0}, Ly0/a;->a(Ly0/a$b;)V

    invoke-virtual {p2, p1}, Ld1/a;->j(Ly0/a;)V

    return-void
.end method


# virtual methods
.method public e(Landroid/graphics/Canvas;Landroid/graphics/Matrix;I)V
    .locals 2

    iget-boolean v0, p0, Lx0/r;->q:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lx0/a;->i:Landroid/graphics/Paint;

    iget-object v1, p0, Lx0/r;->r:Ly0/a;

    check-cast v1, Ly0/b;

    invoke-virtual {v1}, Ly0/b;->o()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lx0/r;->s:Ly0/a;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lx0/a;->i:Landroid/graphics/Paint;

    invoke-virtual {v0}, Ly0/a;->h()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/ColorFilter;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    :cond_1
    invoke-super {p0, p1, p2, p3}, Lx0/a;->e(Landroid/graphics/Canvas;Landroid/graphics/Matrix;I)V

    return-void
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lx0/r;->p:Ljava/lang/String;

    return-object v0
.end method

.method public h(Ljava/lang/Object;Li1/c;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;",
            "Li1/c<",
            "TT;>;)V"
        }
    .end annotation

    invoke-super {p0, p1, p2}, Lx0/a;->h(Ljava/lang/Object;Li1/c;)V

    sget-object v0, Lv0/j;->b:Ljava/lang/Integer;

    if-ne p1, v0, :cond_0

    iget-object p1, p0, Lx0/r;->r:Ly0/a;

    invoke-virtual {p1, p2}, Ly0/a;->m(Li1/c;)V

    goto :goto_0

    :cond_0
    sget-object v0, Lv0/j;->C:Landroid/graphics/ColorFilter;

    if-ne p1, v0, :cond_3

    iget-object p1, p0, Lx0/r;->s:Ly0/a;

    if-eqz p1, :cond_1

    iget-object v0, p0, Lx0/r;->o:Ld1/a;

    invoke-virtual {v0, p1}, Ld1/a;->D(Ly0/a;)V

    :cond_1
    if-nez p2, :cond_2

    const/4 p1, 0x0

    iput-object p1, p0, Lx0/r;->s:Ly0/a;

    goto :goto_0

    :cond_2
    new-instance p1, Ly0/p;

    invoke-direct {p1, p2}, Ly0/p;-><init>(Li1/c;)V

    iput-object p1, p0, Lx0/r;->s:Ly0/a;

    invoke-virtual {p1, p0}, Ly0/a;->a(Ly0/a$b;)V

    iget-object p1, p0, Lx0/r;->o:Ld1/a;

    iget-object p2, p0, Lx0/r;->r:Ly0/a;

    invoke-virtual {p1, p2}, Ld1/a;->j(Ly0/a;)V

    :cond_3
    :goto_0
    return-void
.end method
