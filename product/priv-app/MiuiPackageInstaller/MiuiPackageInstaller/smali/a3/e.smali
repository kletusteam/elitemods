.class public final La3/e;
.super Ljava/lang/Object;

# interfaces
.implements La3/n;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        La3/e$c;,
        La3/e$b;,
        La3/e$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Model:",
        "Ljava/lang/Object;",
        "Data:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "La3/n<",
        "TModel;TData;>;"
    }
.end annotation


# instance fields
.field private final a:La3/e$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La3/e$a<",
            "TData;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(La3/e$a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "La3/e$a<",
            "TData;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, La3/e;->a:La3/e$a;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;IILu2/h;)La3/n$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TModel;II",
            "Lu2/h;",
            ")",
            "La3/n$a<",
            "TData;>;"
        }
    .end annotation

    new-instance p2, La3/n$a;

    new-instance p3, Lp3/d;

    invoke-direct {p3, p1}, Lp3/d;-><init>(Ljava/lang/Object;)V

    new-instance p4, La3/e$b;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, La3/e;->a:La3/e$a;

    invoke-direct {p4, p1, v0}, La3/e$b;-><init>(Ljava/lang/String;La3/e$a;)V

    invoke-direct {p2, p3, p4}, La3/n$a;-><init>(Lu2/f;Lcom/bumptech/glide/load/data/d;)V

    return-object p2
.end method

.method public b(Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TModel;)Z"
        }
    .end annotation

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "data:image"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p1

    return p1
.end method
