.class public La3/b;
.super Ljava/lang/Object;

# interfaces
.implements La3/n;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        La3/b$d;,
        La3/b$a;,
        La3/b$c;,
        La3/b$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Data:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "La3/n<",
        "[BTData;>;"
    }
.end annotation


# instance fields
.field private final a:La3/b$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La3/b$b<",
            "TData;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(La3/b$b;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "La3/b$b<",
            "TData;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, La3/b;->a:La3/b$b;

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;IILu2/h;)La3/n$a;
    .locals 0

    check-cast p1, [B

    invoke-virtual {p0, p1, p2, p3, p4}, La3/b;->c([BIILu2/h;)La3/n$a;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic b(Ljava/lang/Object;)Z
    .locals 0

    check-cast p1, [B

    invoke-virtual {p0, p1}, La3/b;->d([B)Z

    move-result p1

    return p1
.end method

.method public c([BIILu2/h;)La3/n$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([BII",
            "Lu2/h;",
            ")",
            "La3/n$a<",
            "TData;>;"
        }
    .end annotation

    new-instance p2, La3/n$a;

    new-instance p3, Lp3/d;

    invoke-direct {p3, p1}, Lp3/d;-><init>(Ljava/lang/Object;)V

    new-instance p4, La3/b$c;

    iget-object v0, p0, La3/b;->a:La3/b$b;

    invoke-direct {p4, p1, v0}, La3/b$c;-><init>([BLa3/b$b;)V

    invoke-direct {p2, p3, p4}, La3/n$a;-><init>(Lu2/f;Lcom/bumptech/glide/load/data/d;)V

    return-object p2
.end method

.method public d([B)Z
    .locals 0

    const/4 p1, 0x1

    return p1
.end method
