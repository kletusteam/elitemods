.class public Lm9/a;
.super Ljava/lang/Object;


# instance fields
.field private a:Landroid/content/Context;

.field private b:Z

.field private c:Z

.field private d:Z

.field private e:Landroid/util/TypedValue;

.field private f:Landroid/util/TypedValue;

.field private g:Landroid/util/TypedValue;

.field private h:Landroid/util/TypedValue;

.field private i:Landroid/util/TypedValue;

.field private j:Landroid/util/TypedValue;

.field private k:Landroid/util/TypedValue;

.field private l:Landroid/util/TypedValue;

.field private m:Landroid/util/DisplayMetrics;

.field private n:Landroid/graphics/Point;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lm9/a;->b:Z

    iput-boolean v0, p0, Lm9/a;->c:Z

    iput-object p1, p0, Lm9/a;->a:Landroid/content/Context;

    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lm9/a;->n:Landroid/graphics/Point;

    invoke-virtual {p0, p1}, Lm9/a;->u(Landroid/content/Context;)V

    invoke-direct {p0, p1, p2}, Lm9/a;->r(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method private a()Landroid/util/TypedValue;
    .locals 1

    iget-boolean v0, p0, Lm9/a;->b:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lm9/a;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lm9/a;->f:Landroid/util/TypedValue;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method private b()Landroid/util/TypedValue;
    .locals 1

    iget-boolean v0, p0, Lm9/a;->b:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lm9/a;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lm9/a;->h:Landroid/util/TypedValue;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method private c()Landroid/util/TypedValue;
    .locals 1

    iget-boolean v0, p0, Lm9/a;->b:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lm9/a;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lm9/a;->g:Landroid/util/TypedValue;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method private d()Landroid/util/TypedValue;
    .locals 1

    iget-boolean v0, p0, Lm9/a;->b:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lm9/a;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lm9/a;->e:Landroid/util/TypedValue;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method private g()Landroid/util/TypedValue;
    .locals 1

    iget-boolean v0, p0, Lm9/a;->b:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lm9/a;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lm9/a;->l:Landroid/util/TypedValue;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method private h()Landroid/util/TypedValue;
    .locals 1

    iget-boolean v0, p0, Lm9/a;->b:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lm9/a;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lm9/a;->k:Landroid/util/TypedValue;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method private i()Landroid/util/TypedValue;
    .locals 1

    iget-boolean v0, p0, Lm9/a;->b:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lm9/a;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lm9/a;->j:Landroid/util/TypedValue;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method private j()Landroid/util/TypedValue;
    .locals 1

    iget-boolean v0, p0, Lm9/a;->b:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lm9/a;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lm9/a;->i:Landroid/util/TypedValue;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method private k(IZLandroid/util/TypedValue;Landroid/util/TypedValue;Landroid/util/TypedValue;Landroid/util/TypedValue;)I
    .locals 2

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    const/high16 v1, -0x80000000

    if-ne v0, v1, :cond_3

    invoke-direct {p0}, Lm9/a;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    move-object p3, p4

    :goto_0
    invoke-direct {p0, p3, p2}, Lm9/a;->s(Landroid/util/TypedValue;Z)I

    move-result p3

    if-lez p3, :cond_1

    const/high16 p1, 0x40000000    # 2.0f

    invoke-static {p3, p1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    goto :goto_2

    :cond_1
    if-eqz v0, :cond_2

    goto :goto_1

    :cond_2
    move-object p5, p6

    :goto_1
    invoke-direct {p0, p5, p2}, Lm9/a;->s(Landroid/util/TypedValue;Z)I

    move-result p2

    if-lez p2, :cond_3

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p1

    invoke-static {p2, p1}, Ljava/lang/Math;->min(II)I

    move-result p1

    invoke-static {p1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    :cond_3
    :goto_2
    return p1
.end method

.method private l(Landroid/view/ContextThemeWrapper;)I
    .locals 3

    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "getThemeResId"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcb/a;->d(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-static {p1, v0, v2}, Lcb/a;->e(Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    const-string v0, "FloatingABOLayoutSpec"

    const-string v1, "catch theme resource get exception"

    invoke-static {v0, v1, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private o()Z
    .locals 2

    iget-object v0, p0, Lm9/a;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method private r(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    if-nez p2, :cond_0

    return-void

    :cond_0
    sget-object v0, Lk9/l;->v2:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p2

    sget v0, Lk9/l;->I2:I

    invoke-virtual {p2, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    iput-object v1, p0, Lm9/a;->e:Landroid/util/TypedValue;

    invoke-virtual {p2, v0, v1}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    :cond_1
    sget v0, Lk9/l;->F2:I

    invoke-virtual {p2, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    iput-object v1, p0, Lm9/a;->f:Landroid/util/TypedValue;

    invoke-virtual {p2, v0, v1}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    :cond_2
    sget v0, Lk9/l;->H2:I

    invoke-virtual {p2, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_3

    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    iput-object v1, p0, Lm9/a;->g:Landroid/util/TypedValue;

    invoke-virtual {p2, v0, v1}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    :cond_3
    sget v0, Lk9/l;->G2:I

    invoke-virtual {p2, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_4

    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    iput-object v1, p0, Lm9/a;->h:Landroid/util/TypedValue;

    invoke-virtual {p2, v0, v1}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    :cond_4
    sget v0, Lk9/l;->O2:I

    invoke-virtual {p2, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_5

    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    iput-object v1, p0, Lm9/a;->i:Landroid/util/TypedValue;

    invoke-virtual {p2, v0, v1}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    :cond_5
    sget v0, Lk9/l;->N2:I

    invoke-virtual {p2, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_6

    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    iput-object v1, p0, Lm9/a;->j:Landroid/util/TypedValue;

    invoke-virtual {p2, v0, v1}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    :cond_6
    sget v0, Lk9/l;->L2:I

    invoke-virtual {p2, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_7

    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    iput-object v1, p0, Lm9/a;->l:Landroid/util/TypedValue;

    invoke-virtual {p2, v0, v1}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    :cond_7
    sget v0, Lk9/l;->M2:I

    invoke-virtual {p2, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_8

    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    iput-object v1, p0, Lm9/a;->k:Landroid/util/TypedValue;

    invoke-virtual {p2, v0, v1}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    :cond_8
    sget v0, Lk9/l;->A2:I

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lm9/a;->b:Z

    invoke-static {p1}, Ln9/a;->h(Landroid/content/Context;)Z

    move-result p1

    iput-boolean p1, p0, Lm9/a;->c:Z

    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method private s(Landroid/util/TypedValue;Z)I
    .locals 2

    if-eqz p1, :cond_2

    iget v0, p1, Landroid/util/TypedValue;->type:I

    if-eqz v0, :cond_2

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    iget-object p2, p0, Lm9/a;->m:Landroid/util/DisplayMetrics;

    invoke-virtual {p1, p2}, Landroid/util/TypedValue;->getDimension(Landroid/util/DisplayMetrics;)F

    move-result p1

    :goto_0
    float-to-int p1, p1

    goto :goto_2

    :cond_0
    const/4 v1, 0x6

    if-ne v0, v1, :cond_2

    if-eqz p2, :cond_1

    iget-object p2, p0, Lm9/a;->n:Landroid/graphics/Point;

    iget p2, p2, Landroid/graphics/Point;->x:I

    goto :goto_1

    :cond_1
    iget-object p2, p0, Lm9/a;->n:Landroid/graphics/Point;

    iget p2, p2, Landroid/graphics/Point;->y:I

    :goto_1
    int-to-float p2, p2

    invoke-virtual {p1, p2, p2}, Landroid/util/TypedValue;->getFraction(FF)F

    move-result p1

    goto :goto_0

    :cond_2
    const/4 p1, 0x0

    :goto_2
    return p1
.end method


# virtual methods
.method public e(I)I
    .locals 7

    invoke-direct {p0}, Lm9/a;->b()Landroid/util/TypedValue;

    move-result-object v3

    invoke-direct {p0}, Lm9/a;->a()Landroid/util/TypedValue;

    move-result-object v4

    invoke-direct {p0}, Lm9/a;->h()Landroid/util/TypedValue;

    move-result-object v5

    invoke-direct {p0}, Lm9/a;->g()Landroid/util/TypedValue;

    move-result-object v6

    const/4 v2, 0x0

    move-object v0, p0

    move v1, p1

    invoke-direct/range {v0 .. v6}, Lm9/a;->k(IZLandroid/util/TypedValue;Landroid/util/TypedValue;Landroid/util/TypedValue;Landroid/util/TypedValue;)I

    move-result p1

    return p1
.end method

.method public f(I)I
    .locals 7

    iget-object v3, p0, Lm9/a;->h:Landroid/util/TypedValue;

    iget-object v4, p0, Lm9/a;->f:Landroid/util/TypedValue;

    iget-object v5, p0, Lm9/a;->k:Landroid/util/TypedValue;

    iget-object v6, p0, Lm9/a;->l:Landroid/util/TypedValue;

    const/4 v2, 0x0

    move-object v0, p0

    move v1, p1

    invoke-direct/range {v0 .. v6}, Lm9/a;->k(IZLandroid/util/TypedValue;Landroid/util/TypedValue;Landroid/util/TypedValue;Landroid/util/TypedValue;)I

    move-result p1

    return p1
.end method

.method public m(I)I
    .locals 7

    invoke-direct {p0}, Lm9/a;->d()Landroid/util/TypedValue;

    move-result-object v3

    invoke-direct {p0}, Lm9/a;->c()Landroid/util/TypedValue;

    move-result-object v4

    invoke-direct {p0}, Lm9/a;->j()Landroid/util/TypedValue;

    move-result-object v5

    invoke-direct {p0}, Lm9/a;->i()Landroid/util/TypedValue;

    move-result-object v6

    const/4 v2, 0x1

    move-object v0, p0

    move v1, p1

    invoke-direct/range {v0 .. v6}, Lm9/a;->k(IZLandroid/util/TypedValue;Landroid/util/TypedValue;Landroid/util/TypedValue;Landroid/util/TypedValue;)I

    move-result p1

    return p1
.end method

.method public n(I)I
    .locals 7

    iget-object v3, p0, Lm9/a;->e:Landroid/util/TypedValue;

    iget-object v4, p0, Lm9/a;->g:Landroid/util/TypedValue;

    iget-object v5, p0, Lm9/a;->i:Landroid/util/TypedValue;

    iget-object v6, p0, Lm9/a;->j:Landroid/util/TypedValue;

    const/4 v2, 0x1

    move-object v0, p0

    move v1, p1

    invoke-direct/range {v0 .. v6}, Lm9/a;->k(IZLandroid/util/TypedValue;Landroid/util/TypedValue;Landroid/util/TypedValue;Landroid/util/TypedValue;)I

    move-result p1

    return p1
.end method

.method public p()V
    .locals 3

    iget-object v0, p0, Lm9/a;->a:Landroid/content/Context;

    iget-boolean v1, p0, Lm9/a;->d:Z

    if-eqz v1, :cond_0

    instance-of v1, v0, Landroid/view/ContextThemeWrapper;

    if-eqz v1, :cond_0

    move-object v1, v0

    check-cast v1, Landroid/view/ContextThemeWrapper;

    invoke-direct {p0, v1}, Lm9/a;->l(Landroid/view/ContextThemeWrapper;)I

    move-result v1

    if-lez v1, :cond_0

    new-instance v0, Landroid/view/ContextThemeWrapper;

    iget-object v2, p0, Lm9/a;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    :cond_0
    sget v1, Lk9/b;->P:I

    invoke-static {v0, v1}, Lia/d;->k(Landroid/content/Context;I)Landroid/util/TypedValue;

    move-result-object v1

    iput-object v1, p0, Lm9/a;->e:Landroid/util/TypedValue;

    sget v1, Lk9/b;->M:I

    invoke-static {v0, v1}, Lia/d;->k(Landroid/content/Context;I)Landroid/util/TypedValue;

    move-result-object v1

    iput-object v1, p0, Lm9/a;->f:Landroid/util/TypedValue;

    sget v1, Lk9/b;->O:I

    invoke-static {v0, v1}, Lia/d;->k(Landroid/content/Context;I)Landroid/util/TypedValue;

    move-result-object v1

    iput-object v1, p0, Lm9/a;->g:Landroid/util/TypedValue;

    sget v1, Lk9/b;->N:I

    invoke-static {v0, v1}, Lia/d;->k(Landroid/content/Context;I)Landroid/util/TypedValue;

    move-result-object v1

    iput-object v1, p0, Lm9/a;->h:Landroid/util/TypedValue;

    sget v1, Lk9/b;->T:I

    invoke-static {v0, v1}, Lia/d;->k(Landroid/content/Context;I)Landroid/util/TypedValue;

    move-result-object v1

    iput-object v1, p0, Lm9/a;->i:Landroid/util/TypedValue;

    sget v1, Lk9/b;->S:I

    invoke-static {v0, v1}, Lia/d;->k(Landroid/content/Context;I)Landroid/util/TypedValue;

    move-result-object v1

    iput-object v1, p0, Lm9/a;->j:Landroid/util/TypedValue;

    sget v1, Lk9/b;->R:I

    invoke-static {v0, v1}, Lia/d;->k(Landroid/content/Context;I)Landroid/util/TypedValue;

    move-result-object v1

    iput-object v1, p0, Lm9/a;->k:Landroid/util/TypedValue;

    sget v1, Lk9/b;->Q:I

    invoke-static {v0, v1}, Lia/d;->k(Landroid/content/Context;I)Landroid/util/TypedValue;

    move-result-object v1

    iput-object v1, p0, Lm9/a;->l:Landroid/util/TypedValue;

    invoke-virtual {p0, v0}, Lm9/a;->u(Landroid/content/Context;)V

    return-void
.end method

.method public q(Z)V
    .locals 1

    iget-boolean v0, p0, Lm9/a;->b:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    iput-boolean p1, p0, Lm9/a;->c:Z

    return-void
.end method

.method public t(Z)V
    .locals 0

    iput-boolean p1, p0, Lm9/a;->d:Z

    return-void
.end method

.method public u(Landroid/content/Context;)V
    .locals 1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iput-object v0, p0, Lm9/a;->m:Landroid/util/DisplayMetrics;

    invoke-static {p1}, Laa/e;->f(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object p1

    iput-object p1, p0, Lm9/a;->n:Landroid/graphics/Point;

    return-void
.end method
