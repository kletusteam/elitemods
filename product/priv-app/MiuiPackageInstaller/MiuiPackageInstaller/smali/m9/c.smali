.class public Lm9/c;
.super Ljava/lang/Object;


# direct methods
.method static synthetic a(Landroid/view/View;Lc9/a;)V
    .locals 0

    invoke-static {p0, p1}, Lm9/c;->i(Landroid/view/View;Lc9/a;)V

    return-void
.end method

.method public static b(Landroid/view/View;)V
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, v0}, Lm9/c;->c(Landroid/view/View;Lc9/a;)V

    return-void
.end method

.method public static c(Landroid/view/View;Lc9/a;)V
    .locals 5

    new-instance v0, Ld9/a;

    invoke-direct {v0}, Ld9/a;-><init>()V

    sget-object v1, Lh9/h;->b:Lh9/h;

    const/4 v2, 0x0

    int-to-double v3, v2

    invoke-virtual {v0, v1, v3, v4}, Ld9/a;->a(Ljava/lang/Object;D)Ld9/a;

    move-result-object v0

    const/4 v3, 0x1

    new-array v4, v3, [Landroid/view/View;

    aput-object p0, v4, v2

    invoke-static {v4}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object p0

    invoke-interface {p0}, Lmiuix/animation/e;->b()Lmiuix/animation/h;

    move-result-object p0

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v1, v4, v2

    const/16 v1, -0xc8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v3

    invoke-interface {p0, v4}, Lmiuix/animation/h;->D([Ljava/lang/Object;)Lmiuix/animation/h;

    move-result-object p0

    new-array v1, v3, [Lc9/a;

    if-nez p1, :cond_0

    const/4 p1, 0x0

    invoke-static {v2, p1}, Lm9/c;->l(ILjava/lang/Runnable;)Lc9/a;

    move-result-object p1

    :cond_0
    aput-object p1, v1, v2

    invoke-interface {p0, v0, v1}, Lmiuix/animation/h;->s(Ljava/lang/Object;[Lc9/a;)Lmiuix/animation/h;

    return-void
.end method

.method public static d(Landroid/view/View;Lc9/a;)V
    .locals 5

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v0

    new-instance v1, Ld9/a;

    invoke-direct {v1}, Ld9/a;-><init>()V

    sget-object v2, Lh9/h;->b:Lh9/h;

    int-to-double v3, v0

    invoke-virtual {v1, v2, v3, v4}, Ld9/a;->a(Ljava/lang/Object;D)Ld9/a;

    move-result-object v0

    const/4 v1, 0x1

    new-array v3, v1, [Landroid/view/View;

    const/4 v4, 0x0

    aput-object p0, v3, v4

    invoke-static {v3}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object p0

    invoke-interface {p0}, Lmiuix/animation/e;->b()Lmiuix/animation/h;

    move-result-object p0

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v2, v3, v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v3, v1

    invoke-interface {p0, v3}, Lmiuix/animation/h;->D([Ljava/lang/Object;)Lmiuix/animation/h;

    move-result-object p0

    new-array v1, v1, [Lc9/a;

    if-nez p1, :cond_0

    invoke-static {}, Lm9/c;->j()Lc9/a;

    move-result-object p1

    :cond_0
    aput-object p1, v1, v4

    invoke-interface {p0, v0, v1}, Lmiuix/animation/h;->s(Ljava/lang/Object;[Lc9/a;)Lmiuix/animation/h;

    return-void
.end method

.method public static e(Landroid/view/View;)V
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, v0}, Lm9/c;->f(Landroid/view/View;Lc9/a;)V

    return-void
.end method

.method public static f(Landroid/view/View;Lc9/a;)V
    .locals 1

    invoke-virtual {p0}, Landroid/view/View;->isAttachedToWindow()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0, p1}, Lm9/c;->i(Landroid/view/View;Lc9/a;)V

    goto :goto_0

    :cond_0
    new-instance v0, Lm9/c$b;

    invoke-direct {v0, p0, p1}, Lm9/c$b;-><init>(Landroid/view/View;Lc9/a;)V

    invoke-virtual {p0, v0}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    :goto_0
    return-void
.end method

.method public static g(Landroid/view/View;)V
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, v0}, Lm9/c;->h(Landroid/view/View;Lc9/a;)V

    return-void
.end method

.method public static h(Landroid/view/View;Lc9/a;)V
    .locals 4

    new-instance v0, Ld9/a;

    invoke-direct {v0}, Ld9/a;-><init>()V

    sget-object v1, Lh9/h;->b:Lh9/h;

    const-wide/high16 v2, -0x3f97000000000000L    # -200.0

    invoke-virtual {v0, v1, v2, v3}, Ld9/a;->a(Ljava/lang/Object;D)Ld9/a;

    move-result-object v0

    const/4 v1, 0x1

    new-array v2, v1, [Landroid/view/View;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-static {v2}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object p0

    invoke-interface {p0}, Lmiuix/animation/e;->b()Lmiuix/animation/h;

    move-result-object p0

    new-array v1, v1, [Lc9/a;

    if-nez p1, :cond_0

    const/4 p1, 0x0

    invoke-static {v3, p1}, Lm9/c;->l(ILjava/lang/Runnable;)Lc9/a;

    move-result-object p1

    :cond_0
    aput-object p1, v1, v3

    invoke-interface {p0, v0, v1}, Lmiuix/animation/h;->s(Ljava/lang/Object;[Lc9/a;)Lmiuix/animation/h;

    return-void
.end method

.method private static i(Landroid/view/View;Lc9/a;)V
    .locals 6

    new-instance v0, Ld9/a;

    invoke-direct {v0}, Ld9/a;-><init>()V

    sget-object v1, Lh9/h;->b:Lh9/h;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Ld9/a;->a(Ljava/lang/Object;D)Ld9/a;

    move-result-object v0

    const/4 v2, 0x1

    new-array v3, v2, [Landroid/view/View;

    const/4 v4, 0x0

    aput-object p0, v3, v4

    invoke-static {v3}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object v3

    invoke-interface {v3}, Lmiuix/animation/e;->b()Lmiuix/animation/h;

    move-result-object v3

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v1, v5, v4

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    aput-object p0, v5, v2

    invoke-interface {v3, v5}, Lmiuix/animation/h;->D([Ljava/lang/Object;)Lmiuix/animation/h;

    move-result-object p0

    new-array v1, v2, [Lc9/a;

    if-nez p1, :cond_0

    invoke-static {}, Lm9/c;->j()Lc9/a;

    move-result-object p1

    :cond_0
    aput-object p1, v1, v4

    invoke-interface {p0, v0, v1}, Lmiuix/animation/h;->s(Ljava/lang/Object;[Lc9/a;)Lmiuix/animation/h;

    return-void
.end method

.method public static j()Lc9/a;
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lm9/c;->l(ILjava/lang/Runnable;)Lc9/a;

    move-result-object v0

    return-object v0
.end method

.method private static k(I)Lc9/a;
    .locals 4

    new-instance v0, Lc9/a;

    invoke-direct {v0}, Lc9/a;-><init>()V

    const/4 v1, 0x2

    const/4 v2, -0x2

    if-eqz p0, :cond_1

    const/4 v3, 0x1

    if-eq p0, v3, :cond_0

    const/4 p0, 0x0

    invoke-static {p0}, Lm9/c;->k(I)Lc9/a;

    move-result-object v0

    goto :goto_1

    :cond_0
    new-array p0, v1, [F

    fill-array-data p0, :array_0

    invoke-static {v2, p0}, Lj9/c;->e(I[F)Lj9/c$a;

    move-result-object p0

    goto :goto_0

    :cond_1
    new-array p0, v1, [F

    fill-array-data p0, :array_1

    invoke-static {v2, p0}, Lj9/c;->e(I[F)Lj9/c$a;

    move-result-object p0

    :goto_0
    invoke-virtual {v0, p0}, Lc9/a;->l(Lj9/c$a;)Lc9/a;

    :goto_1
    return-object v0

    nop

    :array_0
    .array-data 4
        0x3f59999a    # 0.85f
        0x3e99999a    # 0.3f
    .end array-data

    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x3eeb851f    # 0.46f
    .end array-data
.end method

.method public static l(ILjava/lang/Runnable;)Lc9/a;
    .locals 3

    invoke-static {p0}, Lm9/c;->k(I)Lc9/a;

    move-result-object p0

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    new-array v0, v0, [Lf9/b;

    const/4 v1, 0x0

    new-instance v2, Lm9/c$a;

    invoke-direct {v2, p1, p0}, Lm9/c$a;-><init>(Ljava/lang/Runnable;Lc9/a;)V

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lc9/a;->a([Lf9/b;)Lc9/a;

    :cond_0
    return-object p0
.end method
