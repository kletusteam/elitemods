.class public Lm9/b;
.super Ljava/lang/Object;


# static fields
.field private static a:Z = false

.field private static b:Z = true


# direct methods
.method static constructor <clinit>()V
    .locals 3

    :try_start_0
    const-string v0, "android.view.animation.TranslateWithClipAnimation"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    const/4 v0, 0x1

    sput-boolean v0, Lm9/b;->a:Z
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "FloatingAnimHelper"

    const-string v2, "Failed to get isSupportTransWithClipAnim attributes"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_0
    :try_start_1
    const-string v0, "miuix.autodensity.AutoDensityConfig"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    const/4 v0, 0x0

    sput-boolean v0, Lm9/b;->b:Z

    :goto_1
    return-void
.end method

.method public static a(Lmiuix/appcompat/app/j;)V
    .locals 0

    return-void
.end method

.method public static b(Lmiuix/appcompat/app/j;)V
    .locals 2

    sget v0, Lk9/a;->g:I

    sget v1, Lk9/a;->h:I

    invoke-virtual {p0, v0, v1}, Landroid/app/Activity;->overridePendingTransition(II)V

    return-void
.end method

.method public static c(Lmiuix/appcompat/app/j;)V
    .locals 2

    sget v0, Lk9/a;->m:I

    sget v1, Lk9/a;->n:I

    invoke-virtual {p0, v0, v1}, Landroid/app/Activity;->overridePendingTransition(II)V

    return-void
.end method

.method public static d(Lmiuix/appcompat/app/j;)Z
    .locals 3

    sget-boolean v0, Lm9/b;->b:Z

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    const/4 v0, 0x1

    :try_start_0
    instance-of v2, p0, Lmiuix/autodensity/IDensity;

    if-eqz v2, :cond_1

    check-cast p0, Lmiuix/autodensity/IDensity;

    :goto_0
    invoke-interface {p0}, Lmiuix/autodensity/IDensity;->shouldAdaptAutoDensity()Z

    move-result p0

    move v1, p0

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v2

    instance-of v2, v2, Lmiuix/autodensity/IDensity;

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object p0

    check-cast p0, Lmiuix/autodensity/IDensity;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :cond_2
    move v1, v0

    :catch_0
    :goto_1
    return v1
.end method

.method private static e(Landroid/content/Context;)Z
    .locals 1

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    invoke-virtual {p0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object p0

    iget p0, p0, Landroid/content/res/Configuration;->orientation:I

    const/4 v0, 0x1

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public static f()Z
    .locals 1

    sget-boolean v0, Lm9/b;->a:Z

    return v0
.end method

.method public static g(Lmiuix/appcompat/app/j;I)V
    .locals 1

    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object p0

    invoke-virtual {p0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object p0

    sget v0, Lk9/g;->I:I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p0, v0, p1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    return-void
.end method

.method public static h(Lmiuix/appcompat/app/j;)I
    .locals 1

    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object p0

    invoke-virtual {p0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object p0

    sget v0, Lk9/g;->I:I

    invoke-virtual {p0, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object p0

    instance-of v0, p0, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    check-cast p0, Ljava/lang/Integer;

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    goto :goto_0

    :cond_0
    const/4 p0, -0x1

    :goto_0
    return p0
.end method

.method public static i(Lmiuix/appcompat/app/j;Z)V
    .locals 1

    sget-boolean v0, Lm9/b;->a:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_4

    invoke-static {p0}, Lm9/b;->d(Lmiuix/appcompat/app/j;)Z

    move-result p1

    if-eqz p1, :cond_2

    invoke-static {p0}, Lm9/b;->e(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_1

    sget p1, Lk9/a;->d:I

    sget v0, Lk9/a;->j:I

    goto :goto_0

    :cond_1
    sget p1, Lk9/a;->e:I

    sget v0, Lk9/a;->k:I

    goto :goto_0

    :cond_2
    invoke-static {p0}, Lm9/b;->e(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_3

    sget p1, Lk9/a;->c:I

    sget v0, Lk9/a;->i:I

    goto :goto_0

    :cond_3
    sget p1, Lk9/a;->f:I

    sget v0, Lk9/a;->l:I

    goto :goto_0

    :cond_4
    sget p1, Lk9/a;->a:I

    sget v0, Lk9/a;->b:I

    :goto_0
    invoke-virtual {p0, p1, v0}, Landroid/app/Activity;->overridePendingTransition(II)V

    return-void
.end method

.method public static j(Lmiuix/appcompat/app/j;)V
    .locals 1

    sget-boolean v0, Lm9/b;->a:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lmiuix/appcompat/app/j;->K()Z

    move-result v0

    invoke-static {p0, v0}, Lm9/b;->i(Lmiuix/appcompat/app/j;Z)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lmiuix/appcompat/app/j;->P()V

    :goto_0
    return-void
.end method

.method public static k(Lmiuix/appcompat/app/j;)V
    .locals 2

    sget-boolean v0, Lm9/b;->a:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lmiuix/appcompat/app/j;->K()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {p0}, Lm9/b;->d(Lmiuix/appcompat/app/j;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p0}, Lm9/b;->e(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget v0, Lk9/a;->d:I

    sget v1, Lk9/a;->j:I

    goto :goto_0

    :cond_1
    sget v0, Lk9/a;->e:I

    sget v1, Lk9/a;->k:I

    goto :goto_0

    :cond_2
    invoke-static {p0}, Lm9/b;->e(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget v0, Lk9/a;->c:I

    sget v1, Lk9/a;->i:I

    goto :goto_0

    :cond_3
    sget v0, Lk9/a;->f:I

    sget v1, Lk9/a;->l:I

    goto :goto_0

    :cond_4
    sget v0, Lk9/a;->a:I

    sget v1, Lk9/a;->b:I

    :goto_0
    invoke-virtual {p0, v0, v1}, Landroid/app/Activity;->overridePendingTransition(II)V

    return-void
.end method
