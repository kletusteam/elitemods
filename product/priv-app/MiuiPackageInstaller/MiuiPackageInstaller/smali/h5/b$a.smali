.class Lh5/b$a;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lh5/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lh5/b;


# direct methods
.method private constructor <init>(Lh5/b;)V
    .locals 0

    iput-object p1, p0, Lh5/b$a;->a:Lh5/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lh5/b;Lh5/a;)V
    .locals 0

    invoke-direct {p0, p1}, Lh5/b$a;-><init>(Lh5/b;)V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 1

    iget-object p1, p0, Lh5/b$a;->a:Lh5/b;

    invoke-virtual {p1, p2}, Lh5/b;->s(Landroid/os/IBinder;)V

    new-instance p1, Lh5/b$a$a;

    invoke-direct {p1, p0}, Lh5/b$a$a;-><init>(Lh5/b$a;)V

    iget-object p2, p0, Lh5/b$a;->a:Lh5/b;

    invoke-static {p2}, Lh5/b;->n(Lh5/b;)Ljava/util/concurrent/Executor;

    move-result-object p2

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {p1, p2, v0}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 0

    iget-object p1, p0, Lh5/b$a;->a:Lh5/b;

    invoke-virtual {p1}, Lh5/b;->t()V

    return-void
.end method
