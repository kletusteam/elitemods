.class public final Lr3/a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lr3/a$e;,
        Lr3/a$f;,
        Lr3/a$g;,
        Lr3/a$d;
    }
.end annotation


# static fields
.field private static final a:Lr3/a$g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lr3/a$g<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lr3/a$a;

    invoke-direct {v0}, Lr3/a$a;-><init>()V

    sput-object v0, Lr3/a;->a:Lr3/a$g;

    return-void
.end method

.method private static a(Le0/e;Lr3/a$d;)Le0/e;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lr3/a$f;",
            ">(",
            "Le0/e<",
            "TT;>;",
            "Lr3/a$d<",
            "TT;>;)",
            "Le0/e<",
            "TT;>;"
        }
    .end annotation

    invoke-static {}, Lr3/a;->c()Lr3/a$g;

    move-result-object v0

    invoke-static {p0, p1, v0}, Lr3/a;->b(Le0/e;Lr3/a$d;Lr3/a$g;)Le0/e;

    move-result-object p0

    return-object p0
.end method

.method private static b(Le0/e;Lr3/a$d;Lr3/a$g;)Le0/e;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Le0/e<",
            "TT;>;",
            "Lr3/a$d<",
            "TT;>;",
            "Lr3/a$g<",
            "TT;>;)",
            "Le0/e<",
            "TT;>;"
        }
    .end annotation

    new-instance v0, Lr3/a$e;

    invoke-direct {v0, p0, p1, p2}, Lr3/a$e;-><init>(Le0/e;Lr3/a$d;Lr3/a$g;)V

    return-object v0
.end method

.method private static c()Lr3/a$g;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Lr3/a$g<",
            "TT;>;"
        }
    .end annotation

    sget-object v0, Lr3/a;->a:Lr3/a$g;

    return-object v0
.end method

.method public static d(ILr3/a$d;)Le0/e;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lr3/a$f;",
            ">(I",
            "Lr3/a$d<",
            "TT;>;)",
            "Le0/e<",
            "TT;>;"
        }
    .end annotation

    new-instance v0, Le0/g;

    invoke-direct {v0, p0}, Le0/g;-><init>(I)V

    invoke-static {v0, p1}, Lr3/a;->a(Le0/e;Lr3/a$d;)Le0/e;

    move-result-object p0

    return-object p0
.end method

.method public static e()Le0/e;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Le0/e<",
            "Ljava/util/List<",
            "TT;>;>;"
        }
    .end annotation

    const/16 v0, 0x14

    invoke-static {v0}, Lr3/a;->f(I)Le0/e;

    move-result-object v0

    return-object v0
.end method

.method public static f(I)Le0/e;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(I)",
            "Le0/e<",
            "Ljava/util/List<",
            "TT;>;>;"
        }
    .end annotation

    new-instance v0, Le0/g;

    invoke-direct {v0, p0}, Le0/g;-><init>(I)V

    new-instance p0, Lr3/a$b;

    invoke-direct {p0}, Lr3/a$b;-><init>()V

    new-instance v1, Lr3/a$c;

    invoke-direct {v1}, Lr3/a$c;-><init>()V

    invoke-static {v0, p0, v1}, Lr3/a;->b(Le0/e;Lr3/a$d;Lr3/a$g;)Le0/e;

    move-result-object p0

    return-object p0
.end method
