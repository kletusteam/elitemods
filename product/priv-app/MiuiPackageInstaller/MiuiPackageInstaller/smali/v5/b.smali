.class public final Lv5/b;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lv5/b$a;
    }
.end annotation


# static fields
.field public static final c:Lv5/b$a;


# instance fields
.field private final a:Lm5/e;

.field private final b:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lv5/b$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lv5/b$a;-><init>(Lm8/g;)V

    sput-object v0, Lv5/b;->c:Lv5/b$a;

    return-void
.end method

.method public constructor <init>(Lm5/e;Landroid/net/Uri;)V
    .locals 1

    const-string v0, "mCallingPackage"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mPackageUri"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lv5/b;->a:Lm5/e;

    iput-object p2, p0, Lv5/b;->b:Landroid/net/Uri;

    return-void
.end method

.method private final f(Ljava/io/File;Lcom/miui/packageInstaller/model/ApkInfo;)V
    .locals 5

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1}, Lr2/c;->b(Ljava/io/File;)Ljava/util/Map;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    const v4, 0x7109871a

    if-nez v3, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-eq v3, v4, :cond_0

    :goto_1
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    const v4, 0x71777777

    if-nez v3, :cond_2

    goto :goto_2

    :cond_2
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-eq v3, v4, :cond_0

    :goto_2
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    const v4, 0x42726577

    if-nez v3, :cond_3

    goto :goto_3

    :cond_3
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-eq v3, v4, :cond_0

    :goto_3
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    const-string v3, "it.key"

    invoke-static {v2, v3}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Ljava/lang/Number;

    invoke-virtual {v2}, Ljava/lang/Number;->intValue()I

    move-result v2

    invoke-static {p1, v2}, Lr2/c;->d(Ljava/io/File;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "@"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_4
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/miui/packageInstaller/model/ApkInfo;->setChannelMessage(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_4

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p2

    const-string v0, "ApkParser"

    invoke-static {v0, p2, p1}, Lf6/o;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_4
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/pm/PackageInfo;Lcom/miui/packageInstaller/model/ApkInfo;)V
    .locals 4

    const-string v0, "packageInfo"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "apkInfo"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    :try_start_0
    iget-object v0, p1, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const-string v1, ""

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    :try_start_1
    array-length v3, v0

    if-lez v3, :cond_0

    aget-object v0, v0, v2

    invoke-virtual {v0}, Landroid/content/pm/Signature;->toChars()[C

    move-result-object v0

    const-string v3, "packageInfo!!.signatures[0].toChars()"

    invoke-static {v0, v3}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v0}, Ljava/lang/String;-><init>([C)V

    invoke-static {v3}, Lcom/android/packageinstaller/utils/d;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    invoke-virtual {p2, v0}, Lcom/miui/packageInstaller/model/ApkInfo;->setApkSignature(Ljava/lang/String;)V

    iget-object v0, p1, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    if-eqz v0, :cond_1

    array-length v3, v0

    if-lez v3, :cond_1

    aget-object v0, v0, v2

    invoke-virtual {v0}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v0

    invoke-static {v0}, Lcom/android/packageinstaller/utils/d;->d([B)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_1
    move-object v0, v1

    :goto_1
    invoke-virtual {p2, v0}, Lcom/miui/packageInstaller/model/ApkInfo;->setApkSignature2(Ljava/lang/String;)V

    iget-object v0, p1, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    if-eqz v0, :cond_2

    array-length v3, v0

    if-lez v3, :cond_2

    aget-object v0, v0, v2

    invoke-virtual {v0}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v0

    invoke-static {v0}, Lcom/android/packageinstaller/utils/d;->e([B)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_2
    move-object v0, v1

    :goto_2
    invoke-virtual {p2, v0}, Lcom/miui/packageInstaller/model/ApkInfo;->setApkSignatureSha1(Ljava/lang/String;)V

    iget-object v0, p1, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    if-eqz v0, :cond_4

    const-string v3, "packageInfo!!.signatures"

    invoke-static {v0, v3}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    array-length v0, v0

    const/4 v3, 0x1

    if-nez v0, :cond_3

    move v0, v3

    goto :goto_3

    :cond_3
    move v0, v2

    :goto_3
    xor-int/2addr v0, v3

    if-eqz v0, :cond_4

    iget-object p1, p1, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    aget-object p1, p1, v2

    invoke-virtual {p1}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object p1

    invoke-static {p1}, Lcom/android/packageinstaller/utils/d;->f([B)Ljava/lang/String;

    move-result-object v1

    :cond_4
    invoke-virtual {p2, v1}, Lcom/miui/packageInstaller/model/ApkInfo;->setApkSignatureSha256(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_4

    :catch_0
    const-string p1, "ApkParser"

    const-string p2, "obtain apkSignature failed"

    invoke-static {p1, p2}, Lf6/o;->g(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    :goto_4
    return-void
.end method

.method public final b(Lcom/miui/packageInstaller/model/ApkInfo;Ljava/io/File;)V
    .locals 12

    const-string v0, "ApkParser"

    const-string v1, "apkInfo"

    invoke-static {p1, v1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "file"

    invoke-static {p2, v1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    :try_start_0
    const-string v1, "android.content.pm.parsing.result.ParseTypeImpl"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const-string v2, "forDefaultParsing"

    const/4 v3, 0x0

    new-array v4, v3, [Ljava/lang/Class;

    new-array v5, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v4, v5}, Lf6/q;->d(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    const-string v2, "android.content.pm.parsing.ApkLiteParseUtils"

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    const-string v4, "parseApkLite"

    const/4 v5, 0x3

    new-array v6, v5, [Ljava/lang/Class;

    const-string v7, "android.content.pm.parsing.result.ParseInput"

    invoke-static {v7}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v7

    aput-object v7, v6, v3

    const-class v7, Ljava/io/File;

    const/4 v8, 0x1

    aput-object v7, v6, v8

    sget-object v7, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v9, 0x2

    aput-object v7, v6, v9

    new-array v5, v5, [Ljava/lang/Object;

    const-string v7, "reset"

    new-array v10, v3, [Ljava/lang/Class;

    new-array v11, v3, [Ljava/lang/Object;

    invoke-static {v1, v7, v10, v11}, Lf6/q;->b(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    aput-object v1, v5, v3

    aput-object p2, v5, v8

    const/16 v1, 0x20

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v5, v9

    invoke-static {v2, v4, v6, v5}, Lf6/q;->d(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    const-string v2, "isError"

    new-array v4, v3, [Ljava/lang/Class;

    new-array v5, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v4, v5}, Lf6/q;->b(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    sget-object v4, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-static {v2, v4}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Cannot parse package "

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ". Assuming defaults."

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lf6/o;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Cannot calculate installed size "

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p2, ". Try only apk size."

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lf6/o;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    goto :goto_0

    :cond_0
    const-string p2, "getResult"

    new-array v2, v3, [Ljava/lang/Class;

    new-array v4, v3, [Ljava/lang/Object;

    invoke-static {v1, p2, v2, v4}, Lf6/q;->b(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    const-string v1, "mSigningDetails"

    invoke-static {p2, v1}, Lf6/q;->e(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p2

    const-string v1, "mSignatures"

    invoke-static {p2, v1}, Lf6/q;->e(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p2

    const-string v1, "null cannot be cast to non-null type kotlin.Array<android.content.pm.Signature>"

    invoke-static {p2, v1}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, [Landroid/content/pm/Signature;

    aget-object v1, p2, v3

    invoke-virtual {v1}, Landroid/content/pm/Signature;->toChars()[C

    move-result-object v1

    const-string v2, "signatures[0].toChars()"

    invoke-static {v1, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v1}, Ljava/lang/String;-><init>([C)V

    invoke-static {v2}, Lcom/android/packageinstaller/utils/d;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/miui/packageInstaller/model/ApkInfo;->setApkSignature(Ljava/lang/String;)V

    aget-object v1, p2, v3

    invoke-virtual {v1}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v1

    invoke-static {v1}, Lcom/android/packageinstaller/utils/d;->d([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/miui/packageInstaller/model/ApkInfo;->setApkSignature2(Ljava/lang/String;)V

    aget-object v1, p2, v3

    invoke-virtual {v1}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v1

    invoke-static {v1}, Lcom/android/packageinstaller/utils/d;->e([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/miui/packageInstaller/model/ApkInfo;->setApkSignatureSha1(Ljava/lang/String;)V

    aget-object p2, p2, v3

    invoke-virtual {p2}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object p2

    invoke-static {p2}, Lcom/android/packageinstaller/utils/d;->f([B)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/miui/packageInstaller/model/ApkInfo;->setApkSignatureSha256(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    const-string p2, "Exception in buildApkSignatureV33"

    invoke-static {v0, p2, p1}, Lf6/o;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_0
    return-void
.end method

.method public final c(Ljava/io/File;Lcom/miui/packageInstaller/model/ApkInfo;)Landroid/content/pm/PackageInfo;
    .locals 9

    const-string v0, "file"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "apkInfo"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2}, Lv5/b;->f(Ljava/io/File;Lcom/miui/packageInstaller/model/ApkInfo;)V

    const/16 p2, 0x40

    invoke-static {p1, p2}, Lj2/f;->e(Ljava/io/File;I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    const/16 v2, 0x10c0

    const-wide/16 v3, 0x0

    const-wide/16 v5, 0x0

    const/4 v7, 0x0

    invoke-static {}, Lcom/android/packageinstaller/compat/PackageParserCompat;->createPackageUserState()Ljava/lang/Object;

    move-result-object v8

    invoke-static/range {v0 .. v8}, Lcom/android/packageinstaller/compat/PackageParserCompat;->generatePackageInfo(Ljava/lang/Object;[IIJJLjava/util/Set;Ljava/lang/Object;)Landroid/content/pm/PackageInfo;

    move-result-object p1

    const-string p2, "generatePackageInfo(\n   \u2026kageUserState()\n        )"

    invoke-static {p1, p2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    :cond_0
    new-instance p1, Lv5/a$a;

    const/16 p2, 0x21

    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-direct {p1, p2, v1, v0, v1}, Lv5/a$a;-><init>(ILjava/lang/String;ILm8/g;)V

    throw p1
.end method

.method public final d(Ljava/io/File;Lcom/miui/packageInstaller/model/ApkInfo;)Landroid/content/pm/PackageInfo;
    .locals 1

    const-string v0, "file"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "apkInfo"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2}, Lv5/b;->f(Ljava/io/File;Lcom/miui/packageInstaller/model/ApkInfo;)V

    invoke-static {}, Lcom/android/packageinstaller/InstallerApplication;->g()Lcom/android/packageinstaller/InstallerApplication;

    move-result-object p2

    const/16 v0, 0x1080

    invoke-static {p2, p1, v0}, Lj2/f;->d(Landroid/content/Context;Ljava/io/File;I)Landroid/content/pm/PackageInfo;

    move-result-object p1

    const-string p2, "getPackageInfo(Installer\u2026ageManager.GET_META_DATA)"

    invoke-static {p1, p2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final e(Ljava/lang/String;Lcom/miui/packageInstaller/model/ApkInfo;)V
    .locals 19

    move-object/from16 v1, p0

    move-object/from16 v0, p1

    move-object/from16 v2, p2

    const-string v3, "apkInfo"

    invoke-static {v2, v3}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    const-string v5, "ApkParser"

    const-string v6, "parse apk start"

    invoke-static {v5, v6}, Lf6/o;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    const/16 v6, 0x21

    if-eqz v0, :cond_f

    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/io/File;->length()J

    move-result-wide v8

    invoke-virtual {v2, v8, v9}, Lcom/miui/packageInstaller/model/ApkInfo;->setFileSize(J)V

    invoke-virtual/range {p2 .. p2}, Lcom/miui/packageInstaller/model/ApkInfo;->getFileSize()J

    move-result-wide v8

    invoke-static {v8, v9}, Lcom/android/packageinstaller/utils/h;->e(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/miui/packageInstaller/model/ApkInfo;->setFileSizeString(Ljava/lang/String;)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v6, :cond_0

    invoke-virtual {v1, v7, v2}, Lv5/b;->d(Ljava/io/File;Lcom/miui/packageInstaller/model/ApkInfo;)Landroid/content/pm/PackageInfo;

    move-result-object v0

    goto :goto_0

    :cond_0
    invoke-virtual {v1, v7, v2}, Lv5/b;->c(Ljava/io/File;Lcom/miui/packageInstaller/model/ApkInfo;)Landroid/content/pm/PackageInfo;

    move-result-object v0

    :goto_0
    move-object v8, v0

    const/4 v0, 0x2

    const/4 v9, 0x0

    if-eqz v8, :cond_e

    invoke-virtual {v2, v8}, Lcom/miui/packageInstaller/model/ApkInfo;->setPackageInfo(Landroid/content/pm/PackageInfo;)V

    iget-object v10, v8, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-virtual {v2, v10}, Lcom/miui/packageInstaller/model/ApkInfo;->setCurrentInstallVersionName(Ljava/lang/String;)V

    iget-object v10, v8, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v10, v10, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    if-eqz v10, :cond_2

    const-string v11, "com.miui.sdk.module"

    invoke-virtual {v10, v11}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_1

    goto :goto_1

    :cond_1
    const-string v2, "rejectnewInstall miui sdk"

    invoke-static {v5, v2}, Lf6/o;->g(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    new-instance v2, Lv5/a$a;

    invoke-direct {v2, v6, v9, v0, v9}, Lv5/a$a;-><init>(ILjava/lang/String;ILm8/g;)V

    throw v2

    :cond_2
    :goto_1
    invoke-static {}, Lcom/android/packageinstaller/InstallerApplication;->g()Lcom/android/packageinstaller/InstallerApplication;

    move-result-object v10

    const-string v11, "getInstance()"

    invoke-static {v10, v11}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v11, Lv5/a;->k:Lv5/a$b;

    invoke-virtual {v11}, Lv5/a$b;->a()Ljava/util/List;

    move-result-object v11

    iget-object v12, v8, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-interface {v11, v12}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v11

    const/4 v12, 0x1

    if-nez v11, :cond_4

    iget-object v11, v8, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-static {v10, v11}, Lj2/f;->p(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_4

    invoke-virtual {v2, v12}, Lcom/miui/packageInstaller/model/ApkInfo;->setSystemApp(Z)V

    iget-object v11, v1, Lv5/b;->a:Lm5/e;

    invoke-virtual {v11}, Lm5/e;->m()Ljava/lang/Boolean;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v11

    if-nez v11, :cond_4

    const-string v11, "persist.sys.allow_sys_app_update"

    const/4 v13, 0x0

    invoke-static {v11, v13}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v11

    if-eqz v11, :cond_3

    goto :goto_2

    :cond_3
    new-instance v2, Lv5/a$a;

    const/16 v3, 0x2c

    invoke-direct {v2, v3, v9, v0, v9}, Lv5/a$a;-><init>(ILjava/lang/String;ILm8/g;)V

    throw v2

    :cond_4
    :goto_2
    const/4 v11, 0x0

    :try_start_0
    invoke-static {}, Lcom/android/packageinstaller/InstallerApplication;->g()Lcom/android/packageinstaller/InstallerApplication;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Application;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v13, v8, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    const/16 v14, 0x2000

    invoke-virtual {v0, v13, v14}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v13
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v13, :cond_5

    :try_start_1
    iget v14, v13, Landroid/content/pm/ApplicationInfo;->flags:I

    const/high16 v15, 0x800000

    and-int/2addr v14, v15

    if-nez v14, :cond_5

    move-object v13, v9

    goto :goto_3

    :catch_0
    move-exception v0

    goto :goto_5

    :cond_5
    :goto_3
    if-eqz v13, :cond_6

    iget-object v14, v13, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v14, v11}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    goto :goto_4

    :cond_6
    move-object v0, v9

    :goto_4
    if-eqz v0, :cond_7

    iget v14, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-virtual {v2, v14}, Lcom/miui/packageInstaller/model/ApkInfo;->setInstalledVersionCode(I)V

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/miui/packageInstaller/model/ApkInfo;->setInstalledVersionName(Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_6

    :catch_1
    move-exception v0

    move-object v13, v9

    :goto_5
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    :cond_7
    :goto_6
    invoke-virtual {v2, v13}, Lcom/miui/packageInstaller/model/ApkInfo;->setInstalledPackageInfo(Landroid/content/pm/ApplicationInfo;)V

    if-nez v13, :cond_8

    move v0, v12

    goto :goto_7

    :cond_8
    move v0, v11

    :goto_7
    invoke-virtual {v2, v0}, Lcom/miui/packageInstaller/model/ApkInfo;->setNewInstall(I)V

    iget-object v0, v8, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-static {v10, v0, v7}, Lj2/f;->c(Landroid/content/Context;Landroid/content/pm/ApplicationInfo;Ljava/io/File;)Lj2/f$a;

    move-result-object v0

    if-eqz v0, :cond_9

    iget-object v10, v0, Lj2/f$a;->b:Landroid/graphics/drawable/Drawable;

    goto :goto_8

    :cond_9
    move-object v10, v9

    :goto_8
    invoke-virtual {v2, v10}, Lcom/miui/packageInstaller/model/ApkInfo;->setIcon(Landroid/graphics/drawable/Drawable;)V

    if-eqz v0, :cond_a

    iget-object v9, v0, Lj2/f$a;->a:Ljava/lang/CharSequence;

    :cond_a
    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/miui/packageInstaller/model/ApkInfo;->setLabel(Ljava/lang/String;)V

    invoke-static {}, Lcom/android/packageinstaller/InstallerApplication;->g()Lcom/android/packageinstaller/InstallerApplication;

    move-result-object v0

    iget-object v9, v1, Lv5/b;->b:Landroid/net/Uri;

    iget-object v10, v1, Lv5/b;->a:Lm5/e;

    invoke-virtual {v10}, Lm5/e;->j()Ljava/lang/String;

    move-result-object v10

    if-nez v13, :cond_b

    move v11, v12

    :cond_b
    invoke-static {v0, v9, v10, v11}, Lcom/android/packageinstaller/utils/h;->h(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_c

    const/16 v14, 0x2f

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x6

    const/16 v18, 0x0

    move-object v13, v0

    invoke-static/range {v13 .. v18}, Lu8/g;->P(Ljava/lang/CharSequence;CIZILjava/lang/Object;)I

    move-result v9

    const/16 v14, 0x2e

    invoke-static/range {v13 .. v18}, Lu8/g;->P(Ljava/lang/CharSequence;CIZILjava/lang/Object;)I

    move-result v10

    const/4 v11, -0x1

    if-eq v9, v11, :cond_c

    if-eq v10, v11, :cond_c

    add-int/2addr v9, v12

    if-ge v9, v10, :cond_c

    invoke-virtual {v0, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    const-string v10, "this as java.lang.String\u2026ing(startIndex, endIndex)"

    invoke-static {v9, v10}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2, v9}, Lcom/miui/packageInstaller/model/ApkInfo;->setRealFileName(Ljava/lang/String;)V

    :cond_c
    invoke-virtual {v2, v0}, Lcom/miui/packageInstaller/model/ApkInfo;->setOriginalFilePath(Ljava/lang/String;)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v6, :cond_d

    invoke-virtual {v1, v2, v7}, Lv5/b;->b(Lcom/miui/packageInstaller/model/ApkInfo;Ljava/io/File;)V

    goto :goto_9

    :cond_d
    invoke-virtual {v1, v8, v2}, Lv5/b;->a(Landroid/content/pm/PackageInfo;Lcom/miui/packageInstaller/model/ApkInfo;)V

    :goto_9
    const-string v0, "parse apk finished"

    invoke-static {v5, v0}, Lf6/o;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "parse apk time = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long/2addr v6, v3

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Lf6/o;->e(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    return-void

    :cond_e
    const-string v2, "packageInfo is null, can not continue"

    invoke-static {v5, v2}, Lf6/o;->g(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    new-instance v2, Lv5/a$a;

    invoke-direct {v2, v6, v9, v0, v9}, Lv5/a$a;-><init>(ILjava/lang/String;ILm8/g;)V

    throw v2

    :cond_f
    new-instance v0, Lv5/a$a;

    const-string v2, "empty path"

    invoke-direct {v0, v6, v2}, Lv5/a$a;-><init>(ILjava/lang/String;)V

    throw v0
.end method
