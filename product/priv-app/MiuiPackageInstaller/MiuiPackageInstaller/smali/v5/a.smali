.class public final Lv5/a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lv5/a$b;,
        Lv5/a$a;
    }
.end annotation


# static fields
.field public static final k:Lv5/a$b;

.field private static final l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Lm5/e;

.field private final b:Landroid/net/Uri;

.field private c:Ljava/lang/String;

.field private d:Landroid/net/Uri;

.field private e:Landroid/content/pm/PackageInfo;

.field private f:Lj2/f$a;

.field private g:Landroid/content/pm/ApplicationInfo;

.field private h:I

.field private i:Ljava/lang/String;

.field private j:Lcom/miui/packageInstaller/model/ApkInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lv5/a$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lv5/a$b;-><init>(Lm8/g;)V

    sput-object v0, Lv5/a;->k:Lv5/a$b;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lv5/a;->l:Ljava/util/List;

    const-string v1, "com.android.vending"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v1, "com.google.android.gms"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v1, "com.google.android.gsf"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v1, "com.google.android.syncadapters.contacts"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v1, "com.google.android.backuptransport"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v1, "com.google.android.onetimeinitializer"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v1, "com.google.android.partnersetup"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v1, "com.google.android.configupdater"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v1, "com.google.android.ext.services"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v1, "com.google.android.ext.shared"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v1, "com.google.android.printservice.recommendation"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v1, "com.google.android.googlequicksearchbox"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v1, "com.google.android.ims"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v1, "com.google.android.inputmethod.latin"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v1, "com.google.android.modulemetadata"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v1, "com.google.android.projection.gearhead"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v1, "com.google.android.soundpicker"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v1, "com.google.android.tts"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v1, "com.google.android.webview"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v1, "com.google.ar.core"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v1, "com.google.ar.lens"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public constructor <init>(Lm5/e;Landroid/net/Uri;)V
    .locals 1

    const-string v0, "mCallingPackage"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mPackageUri"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lv5/a;->a:Lm5/e;

    iput-object p2, p0, Lv5/a;->b:Landroid/net/Uri;

    const/4 p1, 0x1

    iput p1, p0, Lv5/a;->h:I

    const-string p1, ""

    iput-object p1, p0, Lv5/a;->i:Ljava/lang/String;

    new-instance p1, Lcom/miui/packageInstaller/model/ApkInfo;

    invoke-direct {p1}, Lcom/miui/packageInstaller/model/ApkInfo;-><init>()V

    iput-object p1, p0, Lv5/a;->j:Lcom/miui/packageInstaller/model/ApkInfo;

    return-void
.end method

.method public static final synthetic a(Lv5/a;)Lcom/miui/packageInstaller/model/ApkInfo;
    .locals 0

    invoke-direct {p0}, Lv5/a;->k()Lcom/miui/packageInstaller/model/ApkInfo;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic b(Lv5/a;Ld8/d;)Ljava/lang/Object;
    .locals 0

    invoke-direct {p0, p1}, Lv5/a;->l(Ld8/d;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic c(Lv5/a;)Lcom/miui/packageInstaller/model/ApkInfo;
    .locals 0

    iget-object p0, p0, Lv5/a;->j:Lcom/miui/packageInstaller/model/ApkInfo;

    return-object p0
.end method

.method public static final synthetic d(Lv5/a;)Lm5/e;
    .locals 0

    iget-object p0, p0, Lv5/a;->a:Lm5/e;

    return-object p0
.end method

.method public static final synthetic e(Lv5/a;)Landroid/net/Uri;
    .locals 0

    iget-object p0, p0, Lv5/a;->b:Landroid/net/Uri;

    return-object p0
.end method

.method public static final synthetic f()Ljava/util/List;
    .locals 1

    sget-object v0, Lv5/a;->l:Ljava/util/List;

    return-object v0
.end method

.method public static final synthetic g(Lv5/a;)Ljava/lang/String;
    .locals 0

    invoke-direct {p0}, Lv5/a;->m()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic h(Lv5/a;)V
    .locals 0

    invoke-direct {p0}, Lv5/a;->o()V

    return-void
.end method

.method public static final synthetic i(Lv5/a;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lv5/a;->p(Ljava/lang/String;)V

    return-void
.end method

.method public static final synthetic j(Lv5/a;Landroid/net/Uri;)V
    .locals 0

    iput-object p1, p0, Lv5/a;->d:Landroid/net/Uri;

    return-void
.end method

.method private final k()Lcom/miui/packageInstaller/model/ApkInfo;
    .locals 2

    iget-object v0, p0, Lv5/a;->j:Lcom/miui/packageInstaller/model/ApkInfo;

    iget-object v1, p0, Lv5/a;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/miui/packageInstaller/model/ApkInfo;->setApkMd5(Ljava/lang/String;)V

    iget-object v0, p0, Lv5/a;->j:Lcom/miui/packageInstaller/model/ApkInfo;

    iget-object v1, p0, Lv5/a;->d:Landroid/net/Uri;

    if-nez v1, :cond_0

    iget-object v1, p0, Lv5/a;->b:Landroid/net/Uri;

    :cond_0
    invoke-virtual {v0, v1}, Lcom/miui/packageInstaller/model/ApkInfo;->setFileUri(Landroid/net/Uri;)V

    iget-object v0, p0, Lv5/a;->j:Lcom/miui/packageInstaller/model/ApkInfo;

    iget-object v1, p0, Lv5/a;->b:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/miui/packageInstaller/model/ApkInfo;->setOriginalUri(Landroid/net/Uri;)V

    iget-object v0, p0, Lv5/a;->j:Lcom/miui/packageInstaller/model/ApkInfo;

    iget-object v1, p0, Lv5/a;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/miui/packageInstaller/model/ApkInfo;->setApkAbi(Ljava/lang/String;)V

    iget-object v0, p0, Lv5/a;->j:Lcom/miui/packageInstaller/model/ApkInfo;

    return-object v0
.end method

.method private final l(Ld8/d;)Ljava/lang/Object;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ld8/d<",
            "-",
            "Ljava/io/File;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    const-string p1, "ApkParser"

    const-string v0, "copy content start"

    invoke-static {p1, v0}, Lf6/o;->e(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {}, Lcom/android/packageinstaller/InstallerApplication;->g()Lcom/android/packageinstaller/InstallerApplication;

    move-result-object v2

    const-string v3, "getInstance()"

    invoke-static {v2, v3}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v3, 0xb

    const/4 v4, 0x0

    const/4 v5, 0x2

    const/4 v6, 0x0

    :try_start_0
    const-string v7, "package"

    const-string v8, ".apk"

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/pm/PackageManager;->getPackageInstaller()Landroid/content/pm/PackageInstaller;

    move-result-object v9

    invoke-static {v2, v9}, Lcom/android/packageinstaller/utils/t;->b(Landroid/content/Context;Landroid/content/pm/PackageInstaller;)Ljava/io/File;

    move-result-object v9

    invoke-static {v7, v8, v9}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v7
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lm5/g;->e(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    const/16 v9, 0x1a4

    invoke-static {v8, v9}, Landroid/system/Os;->chmod(Ljava/lang/String;I)V

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v8, p0, Lv5/a;->b:Landroid/net/Uri;

    invoke-virtual {v2, v8}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v2
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    new-instance v8, Ljava/io/FileOutputStream;

    invoke-direct {v8, v7}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v2, :cond_1

    const/high16 v9, 0x100000

    :try_start_3
    new-array v9, v9, [B

    :goto_0
    invoke-virtual {v2, v9}, Ljava/io/InputStream;->read([B)I

    move-result v10

    if-ltz v10, :cond_0

    invoke-static {}, Ljava/lang/Thread;->yield()V

    invoke-virtual {v8, v9, v4, v10}, Ljava/io/FileOutputStream;->write([BII)V

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "copy content byte read: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {p1, v10}, Lf6/o;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    goto :goto_0

    :cond_0
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "copy content cost: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    sub-long/2addr v10, v0

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lf6/o;->e(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    invoke-static {v2}, Lcom/android/packageinstaller/utils/k;->a(Ljava/lang/AutoCloseable;)V

    invoke-static {v8}, Lcom/android/packageinstaller/utils/k;->a(Ljava/lang/AutoCloseable;)V

    return-object v7

    :catch_0
    move-exception v0

    goto :goto_1

    :cond_1
    :try_start_4
    new-instance v0, Lv5/a$a;

    invoke-direct {v0, v3, v6, v5, v6}, Lv5/a$a;-><init>(ILjava/lang/String;ILm8/g;)V

    throw v0
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    :catchall_0
    move-exception p1

    move-object v8, v6

    goto :goto_2

    :catch_1
    move-exception v0

    move-object v8, v6

    goto :goto_1

    :catch_2
    move-exception v0

    move-object v2, v6

    move-object v8, v2

    goto :goto_1

    :catchall_1
    move-exception p1

    move-object v8, v6

    goto :goto_3

    :catch_3
    move-exception v0

    move-object v2, v6

    move-object v7, v2

    move-object v8, v7

    :goto_1
    :try_start_5
    const-string v1, "Error staging apk from content URI"

    invoke-static {p1, v1, v0}, Lf6/o;->f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    if-eqz v7, :cond_2

    invoke-virtual {v7}, Ljava/io/File;->delete()Z

    move-result p1

    invoke-static {p1}, Lf8/b;->a(Z)Ljava/lang/Boolean;

    :cond_2
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lm8/i;->c(Ljava/lang/Object;)V

    const-string v0, "No space left on device"

    invoke-static {p1, v0, v4, v5, v6}, Lu8/g;->A(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZILjava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    new-instance p1, Lv5/a$a;

    const/16 v0, 0x16

    invoke-direct {p1, v0, v6, v5, v6}, Lv5/a$a;-><init>(ILjava/lang/String;ILm8/g;)V

    throw p1

    :cond_3
    new-instance p1, Lv5/a$a;

    invoke-direct {p1, v3, v6, v5, v6}, Lv5/a$a;-><init>(ILjava/lang/String;ILm8/g;)V

    throw p1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :catchall_2
    move-exception p1

    :goto_2
    move-object v6, v2

    :goto_3
    invoke-static {v6}, Lcom/android/packageinstaller/utils/k;->a(Ljava/lang/AutoCloseable;)V

    invoke-static {v8}, Lcom/android/packageinstaller/utils/k;->a(Ljava/lang/AutoCloseable;)V

    throw p1
.end method

.method private final m()Ljava/lang/String;
    .locals 8

    const-string v0, ", cost: "

    const-string v1, "check md5: "

    const-string v2, "ApkParser"

    const-string v3, "obtain apk Md5 start"

    invoke-static {v2, v3}, Lf6/o;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    :try_start_0
    invoke-static {}, Lcom/android/packageinstaller/InstallerApplication;->g()Lcom/android/packageinstaller/InstallerApplication;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Application;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    iget-object v6, p0, Lv5/a;->b:Landroid/net/Uri;

    invoke-virtual {v6}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    const-string v7, "file"

    invoke-static {v7, v6}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    iget-object v5, p0, Lv5/a;->b:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/android/packageinstaller/utils/h;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lv5/a;->c:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    :goto_0
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lv5/a;->c:Ljava/lang/String;

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sub-long/2addr v0, v3

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lf6/o;->e(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    return-object v5

    :cond_0
    :try_start_1
    const-string v7, "content"

    invoke-static {v7, v6}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    iget-object v6, p0, Lv5/a;->b:Landroid/net/Uri;

    invoke-virtual {v5, v6}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v5

    invoke-static {v5}, Lcom/android/packageinstaller/utils/h;->j(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lv5/a;->c:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    goto :goto_0

    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    goto :goto_1

    :catchall_0
    move-exception v5

    goto :goto_2

    :catch_0
    move-exception v5

    :try_start_2
    const-string v6, "obtain apk Md5 failed :"

    invoke-static {v2, v6, v5}, Lf6/o;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    :goto_1
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lv5/a;->c:Ljava/lang/String;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sub-long/2addr v0, v3

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lf6/o;->e(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    const-string v0, ""

    return-object v0

    :goto_2
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lv5/a;->c:Ljava/lang/String;

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sub-long/2addr v0, v3

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lf6/o;->e(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    throw v5
.end method

.method private final o()V
    .locals 4

    invoke-static {}, Lcom/android/packageinstaller/InstallerApplication;->g()Lcom/android/packageinstaller/InstallerApplication;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Application;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lv5/a;->b:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x3000

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    iput-object v1, p0, Lv5/a;->e:Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    iget-object v1, p0, Lv5/a;->e:Landroid/content/pm/PackageInfo;

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    :try_start_1
    invoke-static {v1}, Lm8/i;->c(Ljava/lang/Object;)V

    iget-object v1, v1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    const/16 v3, 0x2000

    invoke-virtual {v0, v1, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    iput-object v1, p0, Lv5/a;->g:Landroid/content/pm/ApplicationInfo;

    if-eqz v1, :cond_0

    iget v1, v1, Landroid/content/pm/ApplicationInfo;->flags:I

    const/high16 v3, 0x800000

    and-int/2addr v1, v3

    if-nez v1, :cond_0

    iput-object v2, p0, Lv5/a;->g:Landroid/content/pm/ApplicationInfo;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    :cond_0
    iget-object v1, p0, Lv5/a;->g:Landroid/content/pm/ApplicationInfo;

    if-nez v1, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    iput v1, p0, Lv5/a;->h:I

    new-instance v1, Lj2/f$a;

    iget-object v2, p0, Lv5/a;->e:Landroid/content/pm/PackageInfo;

    invoke-static {v2}, Lm8/i;->c(Ljava/lang/Object;)V

    iget-object v2, v2, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v0, v2}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v2

    iget-object v3, p0, Lv5/a;->e:Landroid/content/pm/PackageInfo;

    invoke-static {v3}, Lm8/i;->c(Ljava/lang/Object;)V

    iget-object v3, v3, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v0, v3}, Landroid/content/pm/PackageManager;->getApplicationIcon(Landroid/content/pm/ApplicationInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lj2/f$a;-><init>(Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;)V

    iput-object v1, p0, Lv5/a;->f:Lj2/f$a;

    iget-object v0, p0, Lv5/a;->j:Lcom/miui/packageInstaller/model/ApkInfo;

    iget-object v1, p0, Lv5/a;->g:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v0, v1}, Lcom/miui/packageInstaller/model/ApkInfo;->setInstalledPackageInfo(Landroid/content/pm/ApplicationInfo;)V

    return-void

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Requested package "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lv5/a;->b:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " not available. Discontinuing installation"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ApkParser"

    invoke-static {v1, v0}, Lf6/o;->g(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    new-instance v0, Lv5/a$a;

    const/16 v1, 0x21

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3, v2}, Lv5/a$a;-><init>(ILjava/lang/String;ILm8/g;)V

    throw v0
.end method

.method private final p(Ljava/lang/String;)V
    .locals 21

    move-object/from16 v1, p0

    const-string v0, "zipEntry.name"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    const-string v5, ""

    iput-object v5, v1, Lv5/a;->i:Ljava/lang/String;

    const/4 v5, 0x0

    :try_start_0
    new-instance v6, Ljava/util/zip/ZipFile;

    move-object/from16 v7, p1

    invoke-direct {v6, v7}, Ljava/util/zip/ZipFile;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    invoke-virtual {v6}, Ljava/util/zip/ZipFile;->entries()Ljava/util/Enumeration;

    move-result-object v7

    const-string v8, "entries"

    invoke-static {v7, v8}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v7}, Lb8/j;->o(Ljava/util/Enumeration;)Ljava/util/Iterator;

    move-result-object v7

    const/4 v8, 0x0

    move v9, v8

    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v10
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const-string v11, "APkPareser_dkg"

    if-eqz v10, :cond_2

    :try_start_2
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/util/zip/ZipEntry;

    invoke-virtual {v10}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12, v0}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v13, "lib"

    const/4 v14, 0x2

    invoke-static {v12, v13, v8, v14, v5}, Lu8/g;->v(Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_0

    invoke-virtual {v10}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v15

    invoke-static {v15, v0}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v10, "/"

    filled-new-array {v10}, [Ljava/lang/String;

    move-result-object v16

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x6

    const/16 v20, 0x0

    invoke-static/range {v15 .. v20}, Lu8/g;->g0(Ljava/lang/CharSequence;[Ljava/lang/String;ZIILjava/lang/Object;)Ljava/util/List;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v12

    const/4 v13, 0x1

    if-le v12, v13, :cond_0

    invoke-interface {v10, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/CharSequence;

    invoke-static {v2, v12, v8, v14, v5}, Lu8/g;->A(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZILjava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_0

    if-eqz v9, :cond_1

    const-string v12, ","

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    invoke-interface {v10, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v9, v9, 0x1

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "i = "

    invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v12, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v14, "  name = "

    invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v10, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v11, v10}, Lf6/o;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    goto :goto_0

    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "stringBuilder.toString()"

    invoke-static {v0, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, v1, Lv5/a;->i:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "apkAbi = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, v1, Lv5/a;->i:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " time total = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    sub-long/2addr v7, v3

    invoke-virtual {v0, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v11, v0}, Lf6/o;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    invoke-virtual {v6}, Ljava/util/zip/ZipFile;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_2

    :catch_0
    move-exception v0

    move-object v2, v0

    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    :catchall_0
    move-exception v0

    move-object v2, v0

    move-object v5, v6

    goto :goto_3

    :catch_1
    move-exception v0

    move-object v5, v6

    goto :goto_1

    :catchall_1
    move-exception v0

    move-object v2, v0

    goto :goto_3

    :catch_2
    move-exception v0

    :goto_1
    :try_start_4
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    if-eqz v5, :cond_3

    :try_start_5
    invoke-virtual {v5}, Ljava/util/zip/ZipFile;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0

    :cond_3
    :goto_2
    return-void

    :goto_3
    if-eqz v5, :cond_4

    :try_start_6
    invoke-virtual {v5}, Ljava/util/zip/ZipFile;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    goto :goto_4

    :catch_3
    move-exception v0

    move-object v3, v0

    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    :cond_4
    :goto_4
    throw v2
.end method


# virtual methods
.method public final n(Ld8/d;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ld8/d<",
            "-",
            "Lcom/miui/packageInstaller/model/ApkInfo;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    iget-object v0, p0, Lv5/a;->b:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v2

    const v3, -0x301acbba

    if-eq v2, v3, :cond_3

    const v3, 0x2ff57c

    if-eq v2, v3, :cond_1

    const v3, 0x38b73479

    if-eq v2, v3, :cond_0

    goto :goto_0

    :cond_0
    const-string v2, "content"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-static {}, Lv8/t0;->c()Lv8/v1;

    move-result-object v0

    new-instance v2, Lv5/a$c;

    invoke-direct {v2, p0, v1}, Lv5/a$c;-><init>(Lv5/a;Ld8/d;)V

    invoke-static {v0, v2, p1}, Lv8/f;->e(Ld8/g;Ll8/p;Ld8/d;)Ljava/lang/Object;

    move-result-object p1

    return-object p1

    :cond_1
    const-string v2, "file"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    goto :goto_0

    :cond_2
    invoke-static {}, Lv8/t0;->c()Lv8/v1;

    move-result-object v0

    new-instance v2, Lv5/a$d;

    invoke-direct {v2, p0, v1}, Lv5/a$d;-><init>(Lv5/a;Ld8/d;)V

    invoke-static {v0, v2, p1}, Lv8/f;->e(Ld8/g;Ll8/p;Ld8/d;)Ljava/lang/Object;

    move-result-object p1

    invoke-static {}, Le8/b;->c()Ljava/lang/Object;

    move-result-object v0

    if-ne p1, v0, :cond_5

    return-object p1

    :cond_3
    const-string v2, "package"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    goto :goto_0

    :cond_4
    invoke-static {}, Lv8/t0;->c()Lv8/v1;

    move-result-object v0

    new-instance v2, Lv5/a$e;

    invoke-direct {v2, p0, v1}, Lv5/a$e;-><init>(Lv5/a;Ld8/d;)V

    invoke-static {v0, v2, p1}, Lv8/f;->e(Ld8/g;Ll8/p;Ld8/d;)Ljava/lang/Object;

    move-result-object p1

    invoke-static {}, Le8/b;->c()Ljava/lang/Object;

    move-result-object v0

    if-ne p1, v0, :cond_5

    return-object p1

    :cond_5
    move-object v1, p1

    check-cast v1, Lcom/miui/packageInstaller/model/ApkInfo;

    :cond_6
    :goto_0
    return-object v1
.end method
