.class final Lv5/a$c;
.super Lf8/k;

# interfaces
.implements Ll8/p;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lv5/a;->n(Ld8/d;)Ljava/lang/Object;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf8/k;",
        "Ll8/p<",
        "Lv8/e0;",
        "Ld8/d<",
        "-",
        "Lcom/miui/packageInstaller/model/ApkInfo;",
        ">;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation runtime Lf8/f;
    c = "com.miui.packageInstaller.parse.ApkInfoData$parse$2"
    f = "ApkInfoData.kt"
    l = {
        0x50,
        0x58,
        0x59,
        0x5a
    }
    m = "invokeSuspend"
.end annotation


# instance fields
.field e:Ljava/lang/Object;

.field f:I

.field private synthetic g:Ljava/lang/Object;

.field final synthetic h:Lv5/a;


# direct methods
.method constructor <init>(Lv5/a;Ld8/d;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lv5/a;",
            "Ld8/d<",
            "-",
            "Lv5/a$c;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lv5/a$c;->h:Lv5/a;

    const/4 p1, 0x2

    invoke-direct {p0, p1, p2}, Lf8/k;-><init>(ILd8/d;)V

    return-void
.end method


# virtual methods
.method public final b(Ljava/lang/Object;Ld8/d;)Ld8/d;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ld8/d<",
            "*>;)",
            "Ld8/d<",
            "La8/v;",
            ">;"
        }
    .end annotation

    new-instance v0, Lv5/a$c;

    iget-object v1, p0, Lv5/a$c;->h:Lv5/a;

    invoke-direct {v0, v1, p2}, Lv5/a$c;-><init>(Lv5/a;Ld8/d;)V

    iput-object p1, v0, Lv5/a$c;->g:Ljava/lang/Object;

    return-object v0
.end method

.method public bridge synthetic g(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lv8/e0;

    check-cast p2, Ld8/d;

    invoke-virtual {p0, p1, p2}, Lv5/a$c;->q(Lv8/e0;Ld8/d;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final n(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 17

    move-object/from16 v0, p0

    invoke-static {}, Le8/b;->c()Ljava/lang/Object;

    move-result-object v1

    iget v2, v0, Lv5/a$c;->f:I

    const/4 v3, 0x4

    const/4 v4, 0x3

    const/4 v5, 0x2

    const/4 v6, 0x1

    const/4 v7, 0x0

    if-eqz v2, :cond_4

    if-eq v2, v6, :cond_3

    if-eq v2, v5, :cond_2

    if-eq v2, v4, :cond_1

    if-ne v2, v3, :cond_0

    invoke-static/range {p1 .. p1}, La8/n;->b(Ljava/lang/Object;)V

    move-object/from16 v2, p1

    goto/16 :goto_3

    :cond_0
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    iget-object v2, v0, Lv5/a$c;->g:Ljava/lang/Object;

    check-cast v2, Lm8/t;

    invoke-static/range {p1 .. p1}, La8/n;->b(Ljava/lang/Object;)V

    goto/16 :goto_2

    :cond_2
    iget-object v2, v0, Lv5/a$c;->e:Ljava/lang/Object;

    check-cast v2, Lv8/l0;

    iget-object v5, v0, Lv5/a$c;->g:Ljava/lang/Object;

    check-cast v5, Lm8/t;

    invoke-static/range {p1 .. p1}, La8/n;->b(Ljava/lang/Object;)V

    goto/16 :goto_1

    :cond_3
    iget-object v2, v0, Lv5/a$c;->e:Ljava/lang/Object;

    check-cast v2, Lm8/t;

    iget-object v6, v0, Lv5/a$c;->g:Ljava/lang/Object;

    check-cast v6, Lv8/e0;

    invoke-static/range {p1 .. p1}, La8/n;->b(Ljava/lang/Object;)V

    move-object v14, v6

    move-object/from16 v6, p1

    goto :goto_0

    :cond_4
    invoke-static/range {p1 .. p1}, La8/n;->b(Ljava/lang/Object;)V

    iget-object v2, v0, Lv5/a$c;->g:Ljava/lang/Object;

    check-cast v2, Lv8/e0;

    new-instance v14, Lm8/t;

    invoke-direct {v14}, Lm8/t;-><init>()V

    invoke-static {}, Lv8/t0;->b()Lv8/a0;

    move-result-object v9

    const/4 v10, 0x0

    new-instance v11, Lv5/a$c$d;

    iget-object v8, v0, Lv5/a$c;->h:Lv5/a;

    invoke-direct {v11, v8, v7}, Lv5/a$c$d;-><init>(Lv5/a;Ld8/d;)V

    const/4 v12, 0x2

    const/4 v13, 0x0

    move-object v8, v2

    invoke-static/range {v8 .. v13}, Lv8/f;->b(Lv8/e0;Ld8/g;Lv8/g0;Ll8/p;ILjava/lang/Object;)Lv8/l0;

    move-result-object v8

    iput-object v8, v14, Lm8/t;->a:Ljava/lang/Object;

    invoke-static {}, Lv8/t0;->b()Lv8/a0;

    move-result-object v9

    new-instance v11, Lv5/a$c$c;

    iget-object v8, v0, Lv5/a$c;->h:Lv5/a;

    invoke-direct {v11, v8, v7}, Lv5/a$c$c;-><init>(Lv5/a;Ld8/d;)V

    move-object v8, v2

    invoke-static/range {v8 .. v13}, Lv8/f;->b(Lv8/e0;Ld8/g;Lv8/g0;Ll8/p;ILjava/lang/Object;)Lv8/l0;

    move-result-object v8

    iput-object v2, v0, Lv5/a$c;->g:Ljava/lang/Object;

    iput-object v14, v0, Lv5/a$c;->e:Ljava/lang/Object;

    iput v6, v0, Lv5/a$c;->f:I

    invoke-interface {v8, v0}, Lv8/l0;->z(Ld8/d;)Ljava/lang/Object;

    move-result-object v6

    if-ne v6, v1, :cond_5

    return-object v1

    :cond_5
    move-object/from16 v16, v14

    move-object v14, v2

    move-object/from16 v2, v16

    :goto_0
    check-cast v6, Ljava/io/File;

    if-eqz v6, :cond_9

    iget-object v15, v0, Lv5/a$c;->h:Lv5/a;

    invoke-static {v6}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v8

    invoke-static {v15, v8}, Lv5/a;->j(Lv5/a;Landroid/net/Uri;)V

    invoke-static {}, Lv8/t0;->b()Lv8/a0;

    move-result-object v9

    const/4 v10, 0x0

    new-instance v11, Lv5/a$c$b;

    invoke-direct {v11, v15, v6, v7}, Lv5/a$c$b;-><init>(Lv5/a;Ljava/io/File;Ld8/d;)V

    const/4 v12, 0x2

    const/4 v13, 0x0

    move-object v8, v14

    invoke-static/range {v8 .. v13}, Lv8/f;->b(Lv8/e0;Ld8/g;Lv8/g0;Ll8/p;ILjava/lang/Object;)Lv8/l0;

    move-result-object v13

    invoke-static {}, Lv8/t0;->b()Lv8/a0;

    move-result-object v9

    new-instance v11, Lv5/a$c$a;

    invoke-direct {v11, v15, v6, v7}, Lv5/a$c$a;-><init>(Lv5/a;Ljava/io/File;Ld8/d;)V

    const/4 v6, 0x0

    move-object v14, v13

    move-object v13, v6

    invoke-static/range {v8 .. v13}, Lv8/f;->b(Lv8/e0;Ld8/g;Lv8/g0;Ll8/p;ILjava/lang/Object;)Lv8/l0;

    move-result-object v6

    iput-object v2, v0, Lv5/a$c;->g:Ljava/lang/Object;

    iput-object v6, v0, Lv5/a$c;->e:Ljava/lang/Object;

    iput v5, v0, Lv5/a$c;->f:I

    invoke-interface {v14, v0}, Lv8/l0;->z(Ld8/d;)Ljava/lang/Object;

    move-result-object v5

    if-ne v5, v1, :cond_6

    return-object v1

    :cond_6
    move-object v5, v2

    move-object v2, v6

    :goto_1
    iput-object v5, v0, Lv5/a$c;->g:Ljava/lang/Object;

    iput-object v7, v0, Lv5/a$c;->e:Ljava/lang/Object;

    iput v4, v0, Lv5/a$c;->f:I

    invoke-interface {v2, v0}, Lv8/l0;->z(Ld8/d;)Ljava/lang/Object;

    move-result-object v2

    if-ne v2, v1, :cond_7

    return-object v1

    :cond_7
    move-object v2, v5

    :goto_2
    iget-object v2, v2, Lm8/t;->a:Ljava/lang/Object;

    check-cast v2, Lv8/l0;

    iput-object v7, v0, Lv5/a$c;->g:Ljava/lang/Object;

    iput v3, v0, Lv5/a$c;->f:I

    invoke-interface {v2, v0}, Lv8/l0;->z(Ld8/d;)Ljava/lang/Object;

    move-result-object v2

    if-ne v2, v1, :cond_8

    return-object v1

    :cond_8
    :goto_3
    check-cast v2, Ljava/lang/String;

    :cond_9
    iget-object v1, v0, Lv5/a$c;->h:Lv5/a;

    invoke-static {v1}, Lv5/a;->a(Lv5/a;)Lcom/miui/packageInstaller/model/ApkInfo;

    move-result-object v1

    return-object v1
.end method

.method public final q(Lv8/e0;Ld8/d;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lv8/e0;",
            "Ld8/d<",
            "-",
            "Lcom/miui/packageInstaller/model/ApkInfo;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lv5/a$c;->b(Ljava/lang/Object;Ld8/d;)Ld8/d;

    move-result-object p1

    check-cast p1, Lv5/a$c;

    sget-object p2, La8/v;->a:La8/v;

    invoke-virtual {p1, p2}, Lv5/a$c;->n(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method
