.class final Lv5/a$d;
.super Lf8/k;

# interfaces
.implements Ll8/p;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lv5/a;->n(Ld8/d;)Ljava/lang/Object;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf8/k;",
        "Ll8/p<",
        "Lv8/e0;",
        "Ld8/d<",
        "-",
        "Lcom/miui/packageInstaller/model/ApkInfo;",
        ">;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation runtime Lf8/f;
    c = "com.miui.packageInstaller.parse.ApkInfoData$parse$3"
    f = "ApkInfoData.kt"
    l = {
        0x6a,
        0x6b,
        0x6c
    }
    m = "invokeSuspend"
.end annotation


# instance fields
.field e:Ljava/lang/Object;

.field f:I

.field private synthetic g:Ljava/lang/Object;

.field final synthetic h:Lv5/a;


# direct methods
.method constructor <init>(Lv5/a;Ld8/d;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lv5/a;",
            "Ld8/d<",
            "-",
            "Lv5/a$d;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lv5/a$d;->h:Lv5/a;

    const/4 p1, 0x2

    invoke-direct {p0, p1, p2}, Lf8/k;-><init>(ILd8/d;)V

    return-void
.end method


# virtual methods
.method public final b(Ljava/lang/Object;Ld8/d;)Ld8/d;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ld8/d<",
            "*>;)",
            "Ld8/d<",
            "La8/v;",
            ">;"
        }
    .end annotation

    new-instance v0, Lv5/a$d;

    iget-object v1, p0, Lv5/a$d;->h:Lv5/a;

    invoke-direct {v0, v1, p2}, Lv5/a$d;-><init>(Lv5/a;Ld8/d;)V

    iput-object p1, v0, Lv5/a$d;->g:Ljava/lang/Object;

    return-object v0
.end method

.method public bridge synthetic g(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lv8/e0;

    check-cast p2, Ld8/d;

    invoke-virtual {p0, p1, p2}, Lv5/a$d;->q(Lv8/e0;Ld8/d;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final n(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 13

    invoke-static {}, Le8/b;->c()Ljava/lang/Object;

    move-result-object v0

    iget v1, p0, Lv5/a$d;->f:I

    const/4 v2, 0x3

    const/4 v3, 0x2

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-eqz v1, :cond_3

    if-eq v1, v4, :cond_2

    if-eq v1, v3, :cond_1

    if-ne v1, v2, :cond_0

    invoke-static {p1}, La8/n;->b(Ljava/lang/Object;)V

    goto/16 :goto_2

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    iget-object v1, p0, Lv5/a$d;->g:Ljava/lang/Object;

    check-cast v1, Lv8/l0;

    invoke-static {p1}, La8/n;->b(Ljava/lang/Object;)V

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lv5/a$d;->e:Ljava/lang/Object;

    check-cast v1, Lv8/l0;

    iget-object v4, p0, Lv5/a$d;->g:Ljava/lang/Object;

    check-cast v4, Lv8/l0;

    invoke-static {p1}, La8/n;->b(Ljava/lang/Object;)V

    goto :goto_0

    :cond_3
    invoke-static {p1}, La8/n;->b(Ljava/lang/Object;)V

    iget-object p1, p0, Lv5/a$d;->g:Ljava/lang/Object;

    check-cast p1, Lv8/e0;

    invoke-static {}, Lv8/t0;->b()Lv8/a0;

    move-result-object v7

    const/4 v8, 0x0

    new-instance v9, Lv5/a$d$b;

    iget-object v1, p0, Lv5/a$d;->h:Lv5/a;

    invoke-direct {v9, v1, v5}, Lv5/a$d$b;-><init>(Lv5/a;Ld8/d;)V

    const/4 v10, 0x2

    const/4 v11, 0x0

    move-object v6, p1

    invoke-static/range {v6 .. v11}, Lv8/f;->b(Lv8/e0;Ld8/g;Lv8/g0;Ll8/p;ILjava/lang/Object;)Lv8/l0;

    move-result-object v1

    invoke-static {}, Lv8/t0;->b()Lv8/a0;

    move-result-object v7

    new-instance v9, Lv5/a$d$c;

    iget-object v6, p0, Lv5/a$d;->h:Lv5/a;

    invoke-direct {v9, v6, v5}, Lv5/a$d$c;-><init>(Lv5/a;Ld8/d;)V

    move-object v6, p1

    invoke-static/range {v6 .. v11}, Lv8/f;->b(Lv8/e0;Ld8/g;Lv8/g0;Ll8/p;ILjava/lang/Object;)Lv8/l0;

    move-result-object v12

    invoke-static {}, Lv8/t0;->b()Lv8/a0;

    move-result-object v7

    new-instance v9, Lv5/a$d$a;

    iget-object v6, p0, Lv5/a$d;->h:Lv5/a;

    invoke-direct {v9, v6, v5}, Lv5/a$d$a;-><init>(Lv5/a;Ld8/d;)V

    move-object v6, p1

    invoke-static/range {v6 .. v11}, Lv8/f;->b(Lv8/e0;Ld8/g;Lv8/g0;Ll8/p;ILjava/lang/Object;)Lv8/l0;

    move-result-object p1

    iput-object v1, p0, Lv5/a$d;->g:Ljava/lang/Object;

    iput-object p1, p0, Lv5/a$d;->e:Ljava/lang/Object;

    iput v4, p0, Lv5/a$d;->f:I

    invoke-interface {v12, p0}, Lv8/l0;->z(Ld8/d;)Ljava/lang/Object;

    move-result-object v4

    if-ne v4, v0, :cond_4

    return-object v0

    :cond_4
    move-object v4, v1

    move-object v1, p1

    :goto_0
    iput-object v4, p0, Lv5/a$d;->g:Ljava/lang/Object;

    iput-object v5, p0, Lv5/a$d;->e:Ljava/lang/Object;

    iput v3, p0, Lv5/a$d;->f:I

    invoke-interface {v1, p0}, Lv8/l0;->z(Ld8/d;)Ljava/lang/Object;

    move-result-object p1

    if-ne p1, v0, :cond_5

    return-object v0

    :cond_5
    move-object v1, v4

    :goto_1
    iput-object v5, p0, Lv5/a$d;->g:Ljava/lang/Object;

    iput v2, p0, Lv5/a$d;->f:I

    invoke-interface {v1, p0}, Lv8/l0;->z(Ld8/d;)Ljava/lang/Object;

    move-result-object p1

    if-ne p1, v0, :cond_6

    return-object v0

    :cond_6
    :goto_2
    iget-object p1, p0, Lv5/a$d;->h:Lv5/a;

    invoke-static {p1}, Lv5/a;->a(Lv5/a;)Lcom/miui/packageInstaller/model/ApkInfo;

    move-result-object p1

    return-object p1
.end method

.method public final q(Lv8/e0;Ld8/d;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lv8/e0;",
            "Ld8/d<",
            "-",
            "Lcom/miui/packageInstaller/model/ApkInfo;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lv5/a$d;->b(Ljava/lang/Object;Ld8/d;)Ld8/d;

    move-result-object p1

    check-cast p1, Lv5/a$d;

    sget-object p2, La8/v;->a:La8/v;

    invoke-virtual {p1, p2}, Lv5/a$d;->n(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method
