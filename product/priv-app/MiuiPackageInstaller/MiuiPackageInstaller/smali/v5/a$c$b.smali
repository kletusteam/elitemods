.class final Lv5/a$c$b;
.super Lf8/k;

# interfaces
.implements Ll8/p;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lv5/a$c;->n(Ljava/lang/Object;)Ljava/lang/Object;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf8/k;",
        "Ll8/p<",
        "Lv8/e0;",
        "Ld8/d<",
        "-",
        "La8/v;",
        ">;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation runtime Lf8/f;
    c = "com.miui.packageInstaller.parse.ApkInfoData$parse$2$1$parserJob$1"
    f = "ApkInfoData.kt"
    l = {}
    m = "invokeSuspend"
.end annotation


# instance fields
.field e:I

.field final synthetic f:Lv5/a;

.field final synthetic g:Ljava/io/File;


# direct methods
.method constructor <init>(Lv5/a;Ljava/io/File;Ld8/d;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lv5/a;",
            "Ljava/io/File;",
            "Ld8/d<",
            "-",
            "Lv5/a$c$b;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lv5/a$c$b;->f:Lv5/a;

    iput-object p2, p0, Lv5/a$c$b;->g:Ljava/io/File;

    const/4 p1, 0x2

    invoke-direct {p0, p1, p3}, Lf8/k;-><init>(ILd8/d;)V

    return-void
.end method


# virtual methods
.method public final b(Ljava/lang/Object;Ld8/d;)Ld8/d;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ld8/d<",
            "*>;)",
            "Ld8/d<",
            "La8/v;",
            ">;"
        }
    .end annotation

    new-instance p1, Lv5/a$c$b;

    iget-object v0, p0, Lv5/a$c$b;->f:Lv5/a;

    iget-object v1, p0, Lv5/a$c$b;->g:Ljava/io/File;

    invoke-direct {p1, v0, v1, p2}, Lv5/a$c$b;-><init>(Lv5/a;Ljava/io/File;Ld8/d;)V

    return-object p1
.end method

.method public bridge synthetic g(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lv8/e0;

    check-cast p2, Ld8/d;

    invoke-virtual {p0, p1, p2}, Lv5/a$c$b;->q(Lv8/e0;Ld8/d;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final n(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    invoke-static {}, Le8/b;->c()Ljava/lang/Object;

    iget v0, p0, Lv5/a$c$b;->e:I

    if-nez v0, :cond_0

    invoke-static {p1}, La8/n;->b(Ljava/lang/Object;)V

    new-instance p1, Lv5/b;

    iget-object v0, p0, Lv5/a$c$b;->f:Lv5/a;

    invoke-static {v0}, Lv5/a;->d(Lv5/a;)Lm5/e;

    move-result-object v0

    iget-object v1, p0, Lv5/a$c$b;->f:Lv5/a;

    invoke-static {v1}, Lv5/a;->e(Lv5/a;)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {p1, v0, v1}, Lv5/b;-><init>(Lm5/e;Landroid/net/Uri;)V

    iget-object v0, p0, Lv5/a$c$b;->g:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lv5/a$c$b;->f:Lv5/a;

    invoke-static {v1}, Lv5/a;->c(Lv5/a;)Lcom/miui/packageInstaller/model/ApkInfo;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lv5/b;->e(Ljava/lang/String;Lcom/miui/packageInstaller/model/ApkInfo;)V

    sget-object p1, La8/v;->a:La8/v;

    return-object p1

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final q(Lv8/e0;Ld8/d;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lv8/e0;",
            "Ld8/d<",
            "-",
            "La8/v;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lv5/a$c$b;->b(Ljava/lang/Object;Ld8/d;)Ld8/d;

    move-result-object p1

    check-cast p1, Lv5/a$c$b;

    sget-object p2, La8/v;->a:La8/v;

    invoke-virtual {p1, p2}, Lv5/a$c$b;->n(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method
