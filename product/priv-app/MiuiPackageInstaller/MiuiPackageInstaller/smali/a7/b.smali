.class public La7/b;
.super Ljava/lang/Object;


# static fields
.field private static final b:La7/b;

.field private static c:J


# instance fields
.field private a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, La7/b;

    invoke-direct {v0}, La7/b;-><init>()V

    sput-object v0, La7/b;->b:La7/b;

    const-wide/16 v0, 0xbb8

    sput-wide v0, La7/b;->c:J

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    iput-object v0, p0, La7/b;->a:Ljava/lang/String;

    return-void
.end method

.method public static a()La7/b;
    .locals 1

    sget-object v0, La7/b;->b:La7/b;

    return-object v0
.end method

.method static synthetic b()J
    .locals 2

    sget-wide v0, La7/b;->c:J

    return-wide v0
.end method

.method private d()V
    .locals 3

    invoke-static {}, Lx6/h;->i()V

    iget-object v0, p0, La7/b;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    :try_start_0
    invoke-static {}, Ls6/a;->c()Ls6/a;

    move-result-object v0

    invoke-virtual {v0}, Ls6/a;->f()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lx6/a;->e(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    return-void

    :cond_1
    new-instance v1, La7/b$d;

    invoke-direct {v1, p0, v0}, La7/b$d;-><init>(La7/b;Landroid/content/Context;)V

    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    const-string v0, ""

    invoke-static {v0, v1}, Lx6/h;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method private e()Ljava/lang/String;
    .locals 6

    invoke-static {}, Ls6/a;->c()Ls6/a;

    move-result-object v0

    invoke-virtual {v0}, Ls6/a;->f()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, ""

    return-object v0

    :cond_0
    invoke-static {}, Lw6/e;->j()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, La7/d;->f(Ljava/lang/String;)Z

    move-result v2

    const/4 v3, 0x0

    const/4 v4, 0x1

    const-string v5, "AppUtdid"

    if-eqz v2, :cond_1

    new-array v2, v4, [Ljava/lang/Object;

    const-string v4, "read utdid from V5AppFile"

    aput-object v4, v2, v3

    invoke-static {v5, v2}, Lx6/h;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v2, 0x7

    invoke-static {v2}, La7/d;->m(I)V

    new-instance v2, La7/b$a;

    invoke-direct {v2, p0, v1, v0}, La7/b$a;-><init>(La7/b;Ljava/lang/String;Landroid/content/Context;)V

    invoke-static {v2}, La7/d;->c(La7/d$f;)V

    return-object v1

    :cond_1
    invoke-static {v0}, Lw6/e;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, La7/d;->f(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    new-array v0, v4, [Ljava/lang/Object;

    const-string v2, "read utdid from V5Settings"

    aput-object v2, v0, v3

    invoke-static {v5, v0}, Lx6/h;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    const/16 v0, 0x8

    invoke-static {v0}, La7/d;->m(I)V

    new-instance v0, La7/b$b;

    invoke-direct {v0, p0, v1}, La7/b$b;-><init>(La7/b;Ljava/lang/String;)V

    invoke-static {v0}, La7/d;->c(La7/d$f;)V

    return-object v1

    :cond_2
    invoke-static {}, Lw6/e;->l()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, La7/d;->f(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    new-array v2, v4, [Ljava/lang/Object;

    const-string v4, "read utdid from V5Sdcard"

    aput-object v4, v2, v3

    invoke-static {v5, v2}, Lx6/h;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    const/16 v2, 0x9

    invoke-static {v2}, La7/d;->m(I)V

    new-instance v2, La7/b$c;

    invoke-direct {v2, p0, v1, v0}, La7/b$c;-><init>(La7/b;Ljava/lang/String;Landroid/content/Context;)V

    invoke-static {v2}, La7/d;->c(La7/d$f;)V

    return-object v1

    :cond_3
    const/4 v0, 0x0

    return-object v0
.end method


# virtual methods
.method declared-synchronized c(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    goto/32 :goto_8

    nop

    :goto_0
    monitor-exit p0

    goto/32 :goto_a

    nop

    :goto_1
    monitor-exit p0

    goto/32 :goto_5

    nop

    :goto_2
    monitor-exit p0

    goto/32 :goto_4

    nop

    :goto_3
    return-object p1

    :cond_0
    :try_start_0
    const-string p1, "ffffffffffffffffffffffff"
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    :try_start_1
    invoke-static {}, Lx6/e;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto/32 :goto_0

    nop

    :goto_4
    return-object p1

    :cond_1
    :try_start_2
    invoke-static {}, Lx6/e;->b()V

    invoke-direct {p0}, La7/b;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {p1}, La7/d;->a(Landroid/content/Context;)La7/d;

    move-result-object p1

    invoke-virtual {p1}, La7/d;->j()Ljava/lang/String;

    move-result-object v0

    :cond_2
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_0

    iput-object v0, p0, La7/b;->a:Ljava/lang/String;

    invoke-direct {p0}, La7/b;->d()V

    iget-object p1, p0, La7/b;->a:Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    :try_start_3
    invoke-static {}, Lx6/e;->c()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto/32 :goto_6

    nop

    :goto_5
    return-object p1

    :catchall_0
    move-exception p1

    :try_start_4
    invoke-static {}, Lx6/e;->c()V

    throw p1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catchall_1
    move-exception p1

    goto/32 :goto_9

    nop

    :goto_6
    monitor-exit p0

    goto/32 :goto_3

    nop

    :goto_7
    throw p1

    :goto_8
    monitor-enter p0

    :try_start_5
    iget-object v0, p0, La7/b;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object p1, p0, La7/b;->a:Ljava/lang/String;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto/32 :goto_2

    nop

    :goto_9
    monitor-exit p0

    goto/32 :goto_7

    nop

    :goto_a
    return-object p1

    :catchall_2
    move-exception p1

    :try_start_6
    const-string v0, "AppUtdid"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, p1, v1}, Lx6/h;->d(Ljava/lang/String;Ljava/lang/Throwable;[Ljava/lang/Object;)V

    const-string p1, "ffffffffffffffffffffffff"
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :try_start_7
    invoke-static {}, Lx6/e;->c()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto/32 :goto_1

    nop
.end method

.method public declared-synchronized f()Ljava/lang/String;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, La7/b;->a:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
