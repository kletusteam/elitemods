.class public La7/d;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        La7/d$f;
    }
.end annotation


# static fields
.field private static d:La7/d;

.field private static e:Ljava/util/regex/Pattern;

.field private static final f:Ljava/lang/Object;

.field private static final g:Ljava/lang/String;

.field private static h:I

.field private static i:Ljava/lang/String;


# instance fields
.field private a:Landroid/content/Context;

.field private b:Ljava/lang/String;

.field private c:Lz6/c;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-string v0, "[^0-9a-zA-Z=/+]+"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, La7/d;->e:Ljava/util/regex/Pattern;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, La7/d;->f:Ljava/lang/Object;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ".UTSystemConfig"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "Global"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, La7/d;->g:Ljava/lang/String;

    const/4 v0, 0x0

    sput v0, La7/d;->h:I

    const-string v0, ""

    sput-object v0, La7/d;->i:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, La7/d;->b:Ljava/lang/String;

    iput-object v0, p0, La7/d;->c:Lz6/c;

    iput-object p1, p0, La7/d;->a:Landroid/content/Context;

    invoke-static {}, Ls6/a;->c()Ls6/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Ls6/a;->e(Landroid/content/Context;)V

    new-instance v0, Lz6/c;

    sget-object v1, La7/d;->g:Ljava/lang/String;

    const-string v2, "Alvin2"

    invoke-direct {v0, p1, v1, v2}, Lz6/c;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, La7/d;->c:Lz6/c;

    return-void
.end method

.method public static a(Landroid/content/Context;)La7/d;
    .locals 2

    if-eqz p0, :cond_1

    sget-object v0, La7/d;->d:La7/d;

    if-nez v0, :cond_1

    sget-object v0, La7/d;->f:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    sget-object v1, La7/d;->d:La7/d;

    if-nez v1, :cond_0

    new-instance v1, La7/d;

    invoke-direct {v1, p0}, La7/d;-><init>(Landroid/content/Context;)V

    sput-object v1, La7/d;->d:La7/d;

    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0

    :cond_1
    :goto_0
    sget-object p0, La7/d;->d:La7/d;

    return-object p0
.end method

.method static synthetic b(La7/d;)Lz6/c;
    .locals 0

    iget-object p0, p0, La7/d;->c:Lz6/c;

    return-object p0
.end method

.method static c(La7/d$f;)V
    .locals 2

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, La7/d$e;

    invoke-direct {v1, p0}, La7/d$e;-><init>(La7/d$f;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method static synthetic d(La7/d;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, La7/d;->k(Ljava/lang/String;)V

    return-void
.end method

.method private static e([B)Ljava/lang/String;
    .locals 4

    const/16 v0, 0x2c

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    const-string v1, "HmacSHA1"

    invoke-static {v1}, Ljavax/crypto/Mac;->getInstance(Ljava/lang/String;)Ljavax/crypto/Mac;

    move-result-object v1

    new-instance v2, Ljavax/crypto/spec/SecretKeySpec;

    invoke-static {v0}, Lx6/g;->c([B)[B

    move-result-object v0

    invoke-virtual {v1}, Ljavax/crypto/Mac;->getAlgorithm()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    invoke-virtual {v1, v2}, Ljavax/crypto/Mac;->init(Ljava/security/Key;)V

    invoke-virtual {v1, p0}, Ljavax/crypto/Mac;->doFinal([B)[B

    move-result-object p0

    const/4 v0, 0x2

    invoke-static {p0, v0}, Ly6/b;->f([BI)Ljava/lang/String;

    move-result-object p0

    return-object p0

    nop

    :array_0
    .array-data 1
        0x45t
        0x72t
        0x74t
        -0x21t
        0x7dt
        -0x36t
        -0x1ft
        0x56t
        -0xbt
        0xbt
        -0x4et
        -0x60t
        -0x11t
        -0x63t
        0x40t
        0x17t
        -0x5ft
        -0x7et
        -0x52t
        -0x40t
        0x71t
        0x74t
        -0x10t
        -0x67t
        0x31t
        -0x1et
        0x9t
        -0x27t
        0x21t
        -0x50t
        -0x44t
        -0x4et
        -0x75t
        0x35t
        0x1et
        -0x7at
        0x40t
        -0x68t
        0x4at
        -0x31t
        0x6at
        0x55t
        -0x26t
        -0x5dt
    .end array-data
.end method

.method static f(Ljava/lang/String;)Z
    .locals 3

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    :cond_0
    const-string v0, "\n"

    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    :cond_1
    const/16 v0, 0x18

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-ne v0, v2, :cond_2

    sget-object v0, La7/d;->e:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object p0

    invoke-virtual {p0}, Ljava/util/regex/Matcher;->find()Z

    move-result p0

    xor-int/lit8 p0, p0, 0x1

    return p0

    :cond_2
    return v1
.end method

.method private g()[B
    .locals 6

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "generateUtdid"

    aput-object v2, v0, v1

    const-string v3, "UTUtdid"

    invoke-static {v3, v0}, Lx6/h;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    long-to-int v2, v2

    new-instance v3, Ljava/util/Random;

    invoke-direct {v3}, Ljava/util/Random;-><init>()V

    invoke-virtual {v3}, Ljava/util/Random;->nextInt()I

    move-result v3

    invoke-static {v2}, Ly6/d;->a(I)[B

    move-result-object v2

    invoke-static {v3}, Ly6/d;->a(I)[B

    move-result-object v3

    const/4 v4, 0x4

    invoke-virtual {v0, v2, v1, v4}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    invoke-virtual {v0, v3, v1, v4}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Ljava/io/ByteArrayOutputStream;->write(I)V

    invoke-virtual {v0, v1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, La7/d;->i:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, La7/d;->a:Landroid/content/Context;

    invoke-static {v3}, Ly6/e;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, La7/d;->i:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v3, Ljava/util/Random;

    invoke-direct {v3}, Ljava/util/Random;-><init>()V

    invoke-virtual {v3}, Ljava/util/Random;->nextInt()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_0
    invoke-static {v2}, Ly6/f;->a(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ly6/d;->a(I)[B

    move-result-object v2

    invoke-virtual {v0, v2, v1, v4}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    invoke-static {v1}, La7/d;->e([B)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ly6/f;->a(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ly6/d;->a(I)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/ByteArrayOutputStream;->write([B)V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method

.method private h(Ljava/lang/String;)V
    .locals 4

    invoke-static {p1}, La7/d;->f(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x6

    sput v0, La7/d;->h:I

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "utdid type:"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v1, v2

    const-string v0, "UTUtdid"

    invoke-static {v0, v1}, Lx6/h;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, La7/d;->c:Lz6/c;

    sget v1, La7/d;->h:I

    invoke-virtual {v0, p1, v1}, Lz6/c;->d(Ljava/lang/String;I)V

    new-instance v0, La7/d$a;

    invoke-direct {v0, p0, p1}, La7/d$a;-><init>(La7/d;Ljava/lang/String;)V

    invoke-static {v0}, La7/d;->c(La7/d$f;)V

    :cond_0
    return-void
.end method

.method private i(Ljava/lang/String;)V
    .locals 1

    invoke-static {p1}, La7/d;->f(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    new-instance v0, La7/d$b;

    invoke-direct {v0, p0, p1}, La7/d$b;-><init>(La7/d;Ljava/lang/String;)V

    invoke-static {v0}, La7/d;->c(La7/d$f;)V

    return-void
.end method

.method private k(Ljava/lang/String;)V
    .locals 2

    invoke-static {p1}, La7/d;->f(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, La7/d;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "mqBRboGZkQPcAkyk"

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_0
    return-void
.end method

.method private l()Ljava/lang/String;
    .locals 3

    invoke-direct {p0}, La7/d;->n()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, La7/d;->f(Ljava/lang/String;)Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    :cond_0
    iput-object v0, p0, La7/d;->b:Ljava/lang/String;

    iget-object v0, p0, La7/d;->b:Ljava/lang/String;

    return-object v0

    :cond_1
    :try_start_0
    invoke-direct {p0}, La7/d;->g()[B

    move-result-object v0

    if-eqz v0, :cond_2

    const/4 v1, 0x2

    invoke-static {v0, v1}, Ly6/b;->f([BI)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, La7/d;->b:Ljava/lang/String;

    const/4 v1, 0x6

    sput v1, La7/d;->h:I

    invoke-direct {p0, v0}, La7/d;->h(Ljava/lang/String;)V

    iget-object v0, p0, La7/d;->b:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-array v1, v2, [Ljava/lang/Object;

    const-string v2, ""

    invoke-static {v2, v0, v1}, Lx6/h;->d(Ljava/lang/String;Ljava/lang/Throwable;[Ljava/lang/Object;)V

    :cond_2
    const/4 v0, 0x0

    return-object v0
.end method

.method static m(I)V
    .locals 0

    sput p0, La7/d;->h:I

    return-void
.end method

.method private n()Ljava/lang/String;
    .locals 8

    invoke-direct {p0}, La7/d;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, La7/d;->f(Ljava/lang/String;)Z

    move-result v1

    const-string v2, "utdid type"

    const/4 v3, 0x0

    const/4 v4, 0x1

    const-string v5, "UTUtdid"

    const/4 v6, 0x2

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    if-eqz v1, :cond_0

    sput v6, La7/d;->h:I

    new-array v1, v6, [Ljava/lang/Object;

    aput-object v2, v1, v3

    aput-object v7, v1, v4

    invoke-static {v5, v1}, Lx6/h;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-direct {p0, v0}, La7/d;->i(Ljava/lang/String;)V

    return-object v0

    :cond_0
    invoke-direct {p0}, La7/d;->p()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, La7/d;->f(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    sput v6, La7/d;->h:I

    new-array v1, v6, [Ljava/lang/Object;

    aput-object v2, v1, v3

    aput-object v7, v1, v4

    invoke-static {v5, v1}, Lx6/h;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-direct {p0, v0}, La7/d;->i(Ljava/lang/String;)V

    return-object v0

    :cond_1
    iget-object v0, p0, La7/d;->c:Lz6/c;

    invoke-virtual {v0}, Lz6/c;->h()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, La7/d;->f(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, La7/d;->c:Lz6/c;

    invoke-virtual {v1}, Lz6/c;->a()I

    move-result v1

    if-nez v1, :cond_2

    sput v4, La7/d;->h:I

    goto :goto_0

    :cond_2
    sput v1, La7/d;->h:I

    :goto_0
    new-array v1, v6, [Ljava/lang/Object;

    const-string v2, "get utdid from sp. type"

    aput-object v2, v1, v3

    sget v2, La7/d;->h:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v5, v1}, Lx6/h;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    new-instance v1, La7/d$c;

    invoke-direct {v1, p0, v0}, La7/d$c;-><init>(La7/d;Ljava/lang/String;)V

    invoke-static {v1}, La7/d;->c(La7/d$f;)V

    return-object v0

    :cond_3
    iget-object v0, p0, La7/d;->c:Lz6/c;

    invoke-virtual {v0}, Lz6/c;->i()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, La7/d;->f(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x3

    sput v1, La7/d;->h:I

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v2, v6, v3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v6, v4

    invoke-static {v5, v6}, Lx6/h;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v1, p0, La7/d;->c:Lz6/c;

    sget v2, La7/d;->h:I

    invoke-virtual {v1, v2}, Lz6/c;->e(I)Z

    new-instance v1, La7/d$d;

    invoke-direct {v1, p0, v0}, La7/d$d;-><init>(La7/d;Ljava/lang/String;)V

    invoke-static {v1}, La7/d;->c(La7/d$f;)V

    return-object v0

    :cond_4
    new-array v0, v4, [Ljava/lang/Object;

    const-string v1, "read utdid is null"

    aput-object v1, v0, v3

    invoke-static {v5, v0}, Lx6/h;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-static {v5, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return-object v0
.end method

.method private o()Ljava/lang/String;
    .locals 2

    :try_start_0
    iget-object v0, p0, La7/d;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "mqBRboGZkQPcAkyk"

    invoke-static {v0, v1}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const-string v0, ""

    :goto_0
    return-object v0
.end method

.method private p()Ljava/lang/String;
    .locals 8

    const-string v0, "UTUtdid"

    :try_start_0
    iget-object v1, p0, La7/d;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "dxCRMxhQkdGePGnp"

    invoke-static {v1, v2}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const/4 v1, 0x0

    :goto_0
    const/4 v2, 0x0

    :try_start_1
    invoke-static {v1}, Ly6/f;->c(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    new-instance v3, La7/f;

    invoke-direct {v3}, La7/f;-><init>()V

    invoke-virtual {v3, v1}, La7/f;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, La7/d;->f(Ljava/lang/String;)Z

    move-result v5

    const/4 v6, 0x1

    const/4 v7, 0x2

    if-eqz v5, :cond_0

    new-array v1, v7, [Ljava/lang/Object;

    const-string v3, "OldSettings_1"

    aput-object v3, v1, v2

    aput-object v4, v1, v6

    invoke-static {v0, v1}, Lx6/h;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-direct {p0, v4}, La7/d;->k(Ljava/lang/String;)V

    return-object v4

    :cond_0
    invoke-virtual {v3, v1}, La7/f;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, La7/d;->f(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    new-array v1, v7, [Ljava/lang/Object;

    const-string v4, "OldSettings_2"

    aput-object v4, v1, v2

    aput-object v3, v1, v6

    invoke-static {v0, v1}, Lx6/h;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-direct {p0, v3}, La7/d;->k(Ljava/lang/String;)V

    return-object v3

    :cond_1
    new-instance v3, La7/e;

    invoke-direct {v3}, La7/e;-><init>()V

    invoke-virtual {v3, v1}, La7/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, La7/d;->f(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    new-array v3, v7, [Ljava/lang/Object;

    const-string v4, "OldSettings_3"

    aput-object v4, v3, v2

    aput-object v1, v3, v6

    invoke-static {v0, v3}, Lx6/h;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-direct {p0, v1}, La7/d;->k(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v1

    :catchall_0
    move-exception v1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lx6/h;->f(Ljava/lang/String;Ljava/lang/Throwable;[Ljava/lang/Object;)V

    :cond_2
    const-string v0, ""

    return-object v0
.end method


# virtual methods
.method public declared-synchronized j()Ljava/lang/String;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, La7/d;->b:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    invoke-direct {p0}, La7/d;->l()Ljava/lang/String;

    move-result-object v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
