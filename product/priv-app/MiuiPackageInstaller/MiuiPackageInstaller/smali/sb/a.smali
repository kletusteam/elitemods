.class public final Lsb/a;
.super Ljava/lang/Object;

# interfaces
.implements Lpb/u;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsb/a$a;
    }
.end annotation


# static fields
.field public static final a:Lsb/a$a;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lsb/a$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lsb/a$a;-><init>(Lm8/g;)V

    sput-object v0, Lsb/a;->a:Lsb/a$a;

    return-void
.end method

.method public constructor <init>(Lpb/c;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lpb/u$a;)Lpb/b0;
    .locals 7

    const-string v0, "chain"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    new-instance v2, Lsb/b$b;

    invoke-interface {p1}, Lpb/u$a;->J()Lpb/z;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v2, v0, v1, v3, v4}, Lsb/b$b;-><init>(JLpb/z;Lpb/b0;)V

    invoke-virtual {v2}, Lsb/b$b;->b()Lsb/b;

    move-result-object v0

    invoke-virtual {v0}, Lsb/b;->b()Lpb/z;

    move-result-object v1

    invoke-virtual {v0}, Lsb/b;->a()Lpb/b0;

    move-result-object v0

    if-nez v1, :cond_0

    if-nez v0, :cond_0

    new-instance v0, Lpb/b0$a;

    invoke-direct {v0}, Lpb/b0$a;-><init>()V

    invoke-interface {p1}, Lpb/u$a;->J()Lpb/z;

    move-result-object p1

    invoke-virtual {v0, p1}, Lpb/b0$a;->s(Lpb/z;)Lpb/b0$a;

    move-result-object p1

    sget-object v0, Lpb/y;->c:Lpb/y;

    invoke-virtual {p1, v0}, Lpb/b0$a;->p(Lpb/y;)Lpb/b0$a;

    move-result-object p1

    const/16 v0, 0x1f8

    invoke-virtual {p1, v0}, Lpb/b0$a;->g(I)Lpb/b0$a;

    move-result-object p1

    const-string v0, "Unsatisfiable Request (only-if-cached)"

    invoke-virtual {p1, v0}, Lpb/b0$a;->m(Ljava/lang/String;)Lpb/b0$a;

    move-result-object p1

    sget-object v0, Lrb/b;->c:Lpb/c0;

    invoke-virtual {p1, v0}, Lpb/b0$a;->b(Lpb/c0;)Lpb/b0$a;

    move-result-object p1

    const-wide/16 v0, -0x1

    invoke-virtual {p1, v0, v1}, Lpb/b0$a;->t(J)Lpb/b0$a;

    move-result-object p1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lpb/b0$a;->q(J)Lpb/b0$a;

    move-result-object p1

    :goto_0
    invoke-virtual {p1}, Lpb/b0$a;->c()Lpb/b0;

    move-result-object p1

    return-object p1

    :cond_0
    if-nez v1, :cond_2

    if-nez v0, :cond_1

    invoke-static {}, Lm8/i;->o()V

    :cond_1
    invoke-virtual {v0}, Lpb/b0;->J()Lpb/b0$a;

    move-result-object p1

    sget-object v1, Lsb/a;->a:Lsb/a$a;

    invoke-static {v1, v0}, Lsb/a$a;->b(Lsb/a$a;Lpb/b0;)Lpb/b0;

    move-result-object v0

    invoke-virtual {p1, v0}, Lpb/b0$a;->d(Lpb/b0;)Lpb/b0$a;

    move-result-object p1

    goto :goto_0

    :cond_2
    :try_start_0
    invoke-interface {p1, v1}, Lpb/u$a;->e(Lpb/z;)Lpb/b0;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_5

    if-eqz p1, :cond_4

    invoke-virtual {p1}, Lpb/b0;->n()I

    move-result v1

    const/16 v2, 0x130

    if-ne v1, v2, :cond_4

    invoke-virtual {v0}, Lpb/b0;->J()Lpb/b0$a;

    move-result-object v1

    sget-object v2, Lsb/a;->a:Lsb/a$a;

    invoke-virtual {v0}, Lpb/b0;->z()Lpb/s;

    move-result-object v3

    invoke-virtual {p1}, Lpb/b0;->z()Lpb/s;

    move-result-object v5

    invoke-static {v2, v3, v5}, Lsb/a$a;->a(Lsb/a$a;Lpb/s;Lpb/s;)Lpb/s;

    move-result-object v3

    invoke-virtual {v1, v3}, Lpb/b0$a;->k(Lpb/s;)Lpb/b0$a;

    move-result-object v1

    invoke-virtual {p1}, Lpb/b0;->V()J

    move-result-wide v5

    invoke-virtual {v1, v5, v6}, Lpb/b0$a;->t(J)Lpb/b0$a;

    move-result-object v1

    invoke-virtual {p1}, Lpb/b0;->M()J

    move-result-wide v5

    invoke-virtual {v1, v5, v6}, Lpb/b0$a;->q(J)Lpb/b0$a;

    move-result-object v1

    invoke-static {v2, v0}, Lsb/a$a;->b(Lsb/a$a;Lpb/b0;)Lpb/b0;

    move-result-object v0

    invoke-virtual {v1, v0}, Lpb/b0$a;->d(Lpb/b0;)Lpb/b0$a;

    move-result-object v0

    invoke-static {v2, p1}, Lsb/a$a;->b(Lsb/a$a;Lpb/b0;)Lpb/b0;

    move-result-object v1

    invoke-virtual {v0, v1}, Lpb/b0$a;->n(Lpb/b0;)Lpb/b0$a;

    move-result-object v0

    invoke-virtual {v0}, Lpb/b0$a;->c()Lpb/b0;

    invoke-virtual {p1}, Lpb/b0;->b()Lpb/c0;

    move-result-object p1

    if-nez p1, :cond_3

    invoke-static {}, Lm8/i;->o()V

    :cond_3
    invoke-virtual {p1}, Lpb/c0;->close()V

    invoke-static {}, Lm8/i;->o()V

    throw v4

    :cond_4
    invoke-virtual {v0}, Lpb/b0;->b()Lpb/c0;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-static {v1}, Lrb/b;->j(Ljava/io/Closeable;)V

    :cond_5
    if-nez p1, :cond_6

    invoke-static {}, Lm8/i;->o()V

    :cond_6
    invoke-virtual {p1}, Lpb/b0;->J()Lpb/b0$a;

    move-result-object v1

    sget-object v2, Lsb/a;->a:Lsb/a$a;

    invoke-static {v2, v0}, Lsb/a$a;->b(Lsb/a$a;Lpb/b0;)Lpb/b0;

    move-result-object v0

    invoke-virtual {v1, v0}, Lpb/b0$a;->d(Lpb/b0;)Lpb/b0$a;

    move-result-object v0

    invoke-static {v2, p1}, Lsb/a$a;->b(Lsb/a$a;Lpb/b0;)Lpb/b0;

    move-result-object p1

    invoke-virtual {v0, p1}, Lpb/b0$a;->n(Lpb/b0;)Lpb/b0$a;

    move-result-object p1

    invoke-virtual {p1}, Lpb/b0$a;->c()Lpb/b0;

    move-result-object p1

    return-object p1

    :catchall_0
    move-exception p1

    throw p1
.end method
