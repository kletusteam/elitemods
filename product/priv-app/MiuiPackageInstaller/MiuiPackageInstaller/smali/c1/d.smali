.class public Lc1/d;
.super Ljava/lang/Object;

# interfaces
.implements Lc1/b;


# instance fields
.field private final a:Lc1/f;

.field private final b:Landroid/graphics/Path$FillType;

.field private final c:Lb1/c;

.field private final d:Lb1/d;

.field private final e:Lb1/f;

.field private final f:Lb1/f;

.field private final g:Ljava/lang/String;

.field private final h:Lb1/b;

.field private final i:Lb1/b;

.field private final j:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Lc1/f;Landroid/graphics/Path$FillType;Lb1/c;Lb1/d;Lb1/f;Lb1/f;Lb1/b;Lb1/b;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lc1/d;->a:Lc1/f;

    iput-object p3, p0, Lc1/d;->b:Landroid/graphics/Path$FillType;

    iput-object p4, p0, Lc1/d;->c:Lb1/c;

    iput-object p5, p0, Lc1/d;->d:Lb1/d;

    iput-object p6, p0, Lc1/d;->e:Lb1/f;

    iput-object p7, p0, Lc1/d;->f:Lb1/f;

    iput-object p1, p0, Lc1/d;->g:Ljava/lang/String;

    iput-object p8, p0, Lc1/d;->h:Lb1/b;

    iput-object p9, p0, Lc1/d;->i:Lb1/b;

    iput-boolean p10, p0, Lc1/d;->j:Z

    return-void
.end method


# virtual methods
.method public a(Lcom/airbnb/lottie/a;Ld1/a;)Lx0/c;
    .locals 1

    new-instance v0, Lx0/h;

    invoke-direct {v0, p1, p2, p0}, Lx0/h;-><init>(Lcom/airbnb/lottie/a;Ld1/a;Lc1/d;)V

    return-object v0
.end method

.method public b()Lb1/f;
    .locals 1

    iget-object v0, p0, Lc1/d;->f:Lb1/f;

    return-object v0
.end method

.method public c()Landroid/graphics/Path$FillType;
    .locals 1

    iget-object v0, p0, Lc1/d;->b:Landroid/graphics/Path$FillType;

    return-object v0
.end method

.method public d()Lb1/c;
    .locals 1

    iget-object v0, p0, Lc1/d;->c:Lb1/c;

    return-object v0
.end method

.method public e()Lc1/f;
    .locals 1

    iget-object v0, p0, Lc1/d;->a:Lc1/f;

    return-object v0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lc1/d;->g:Ljava/lang/String;

    return-object v0
.end method

.method public g()Lb1/d;
    .locals 1

    iget-object v0, p0, Lc1/d;->d:Lb1/d;

    return-object v0
.end method

.method public h()Lb1/f;
    .locals 1

    iget-object v0, p0, Lc1/d;->e:Lb1/f;

    return-object v0
.end method

.method public i()Z
    .locals 1

    iget-boolean v0, p0, Lc1/d;->j:Z

    return v0
.end method
