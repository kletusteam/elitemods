.class public Lc1/p;
.super Ljava/lang/Object;

# interfaces
.implements Lc1/b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lc1/p$c;,
        Lc1/p$b;
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lb1/b;

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lb1/b;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lb1/a;

.field private final e:Lb1/d;

.field private final f:Lb1/b;

.field private final g:Lc1/p$b;

.field private final h:Lc1/p$c;

.field private final i:F

.field private final j:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Lb1/b;Ljava/util/List;Lb1/a;Lb1/d;Lb1/b;Lc1/p$b;Lc1/p$c;FZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lb1/b;",
            "Ljava/util/List<",
            "Lb1/b;",
            ">;",
            "Lb1/a;",
            "Lb1/d;",
            "Lb1/b;",
            "Lc1/p$b;",
            "Lc1/p$c;",
            "FZ)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lc1/p;->a:Ljava/lang/String;

    iput-object p2, p0, Lc1/p;->b:Lb1/b;

    iput-object p3, p0, Lc1/p;->c:Ljava/util/List;

    iput-object p4, p0, Lc1/p;->d:Lb1/a;

    iput-object p5, p0, Lc1/p;->e:Lb1/d;

    iput-object p6, p0, Lc1/p;->f:Lb1/b;

    iput-object p7, p0, Lc1/p;->g:Lc1/p$b;

    iput-object p8, p0, Lc1/p;->h:Lc1/p$c;

    iput p9, p0, Lc1/p;->i:F

    iput-boolean p10, p0, Lc1/p;->j:Z

    return-void
.end method


# virtual methods
.method public a(Lcom/airbnb/lottie/a;Ld1/a;)Lx0/c;
    .locals 1

    new-instance v0, Lx0/r;

    invoke-direct {v0, p1, p2, p0}, Lx0/r;-><init>(Lcom/airbnb/lottie/a;Ld1/a;Lc1/p;)V

    return-object v0
.end method

.method public b()Lc1/p$b;
    .locals 1

    iget-object v0, p0, Lc1/p;->g:Lc1/p$b;

    return-object v0
.end method

.method public c()Lb1/a;
    .locals 1

    iget-object v0, p0, Lc1/p;->d:Lb1/a;

    return-object v0
.end method

.method public d()Lb1/b;
    .locals 1

    iget-object v0, p0, Lc1/p;->b:Lb1/b;

    return-object v0
.end method

.method public e()Lc1/p$c;
    .locals 1

    iget-object v0, p0, Lc1/p;->h:Lc1/p$c;

    return-object v0
.end method

.method public f()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lb1/b;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lc1/p;->c:Ljava/util/List;

    return-object v0
.end method

.method public g()F
    .locals 1

    iget v0, p0, Lc1/p;->i:F

    return v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lc1/p;->a:Ljava/lang/String;

    return-object v0
.end method

.method public i()Lb1/d;
    .locals 1

    iget-object v0, p0, Lc1/p;->e:Lb1/d;

    return-object v0
.end method

.method public j()Lb1/b;
    .locals 1

    iget-object v0, p0, Lc1/p;->f:Lb1/b;

    return-object v0
.end method

.method public k()Z
    .locals 1

    iget-boolean v0, p0, Lc1/p;->j:Z

    return v0
.end method
