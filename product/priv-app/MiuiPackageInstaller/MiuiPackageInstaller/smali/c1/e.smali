.class public Lc1/e;
.super Ljava/lang/Object;

# interfaces
.implements Lc1/b;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lc1/f;

.field private final c:Lb1/c;

.field private final d:Lb1/d;

.field private final e:Lb1/f;

.field private final f:Lb1/f;

.field private final g:Lb1/b;

.field private final h:Lc1/p$b;

.field private final i:Lc1/p$c;

.field private final j:F

.field private final k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lb1/b;",
            ">;"
        }
    .end annotation
.end field

.field private final l:Lb1/b;

.field private final m:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Lc1/f;Lb1/c;Lb1/d;Lb1/f;Lb1/f;Lb1/b;Lc1/p$b;Lc1/p$c;FLjava/util/List;Lb1/b;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lc1/f;",
            "Lb1/c;",
            "Lb1/d;",
            "Lb1/f;",
            "Lb1/f;",
            "Lb1/b;",
            "Lc1/p$b;",
            "Lc1/p$c;",
            "F",
            "Ljava/util/List<",
            "Lb1/b;",
            ">;",
            "Lb1/b;",
            "Z)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lc1/e;->a:Ljava/lang/String;

    iput-object p2, p0, Lc1/e;->b:Lc1/f;

    iput-object p3, p0, Lc1/e;->c:Lb1/c;

    iput-object p4, p0, Lc1/e;->d:Lb1/d;

    iput-object p5, p0, Lc1/e;->e:Lb1/f;

    iput-object p6, p0, Lc1/e;->f:Lb1/f;

    iput-object p7, p0, Lc1/e;->g:Lb1/b;

    iput-object p8, p0, Lc1/e;->h:Lc1/p$b;

    iput-object p9, p0, Lc1/e;->i:Lc1/p$c;

    iput p10, p0, Lc1/e;->j:F

    iput-object p11, p0, Lc1/e;->k:Ljava/util/List;

    iput-object p12, p0, Lc1/e;->l:Lb1/b;

    iput-boolean p13, p0, Lc1/e;->m:Z

    return-void
.end method


# virtual methods
.method public a(Lcom/airbnb/lottie/a;Ld1/a;)Lx0/c;
    .locals 1

    new-instance v0, Lx0/i;

    invoke-direct {v0, p1, p2, p0}, Lx0/i;-><init>(Lcom/airbnb/lottie/a;Ld1/a;Lc1/e;)V

    return-object v0
.end method

.method public b()Lc1/p$b;
    .locals 1

    iget-object v0, p0, Lc1/e;->h:Lc1/p$b;

    return-object v0
.end method

.method public c()Lb1/b;
    .locals 1

    iget-object v0, p0, Lc1/e;->l:Lb1/b;

    return-object v0
.end method

.method public d()Lb1/f;
    .locals 1

    iget-object v0, p0, Lc1/e;->f:Lb1/f;

    return-object v0
.end method

.method public e()Lb1/c;
    .locals 1

    iget-object v0, p0, Lc1/e;->c:Lb1/c;

    return-object v0
.end method

.method public f()Lc1/f;
    .locals 1

    iget-object v0, p0, Lc1/e;->b:Lc1/f;

    return-object v0
.end method

.method public g()Lc1/p$c;
    .locals 1

    iget-object v0, p0, Lc1/e;->i:Lc1/p$c;

    return-object v0
.end method

.method public h()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lb1/b;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lc1/e;->k:Ljava/util/List;

    return-object v0
.end method

.method public i()F
    .locals 1

    iget v0, p0, Lc1/e;->j:F

    return v0
.end method

.method public j()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lc1/e;->a:Ljava/lang/String;

    return-object v0
.end method

.method public k()Lb1/d;
    .locals 1

    iget-object v0, p0, Lc1/e;->d:Lb1/d;

    return-object v0
.end method

.method public l()Lb1/f;
    .locals 1

    iget-object v0, p0, Lc1/e;->e:Lb1/f;

    return-object v0
.end method

.method public m()Lb1/b;
    .locals 1

    iget-object v0, p0, Lc1/e;->g:Lb1/b;

    return-object v0
.end method

.method public n()Z
    .locals 1

    iget-boolean v0, p0, Lc1/e;->m:Z

    return v0
.end method
