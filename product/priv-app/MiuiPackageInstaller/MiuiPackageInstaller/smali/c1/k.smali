.class public Lc1/k;
.super Ljava/lang/Object;

# interfaces
.implements Lc1/b;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lb1/b;

.field private final c:Lb1/b;

.field private final d:Lb1/l;

.field private final e:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Lb1/b;Lb1/b;Lb1/l;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lc1/k;->a:Ljava/lang/String;

    iput-object p2, p0, Lc1/k;->b:Lb1/b;

    iput-object p3, p0, Lc1/k;->c:Lb1/b;

    iput-object p4, p0, Lc1/k;->d:Lb1/l;

    iput-boolean p5, p0, Lc1/k;->e:Z

    return-void
.end method


# virtual methods
.method public a(Lcom/airbnb/lottie/a;Ld1/a;)Lx0/c;
    .locals 1

    new-instance v0, Lx0/p;

    invoke-direct {v0, p1, p2, p0}, Lx0/p;-><init>(Lcom/airbnb/lottie/a;Ld1/a;Lc1/k;)V

    return-object v0
.end method

.method public b()Lb1/b;
    .locals 1

    iget-object v0, p0, Lc1/k;->b:Lb1/b;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lc1/k;->a:Ljava/lang/String;

    return-object v0
.end method

.method public d()Lb1/b;
    .locals 1

    iget-object v0, p0, Lc1/k;->c:Lb1/b;

    return-object v0
.end method

.method public e()Lb1/l;
    .locals 1

    iget-object v0, p0, Lc1/k;->d:Lb1/l;

    return-object v0
.end method

.method public f()Z
    .locals 1

    iget-boolean v0, p0, Lc1/k;->e:Z

    return v0
.end method
