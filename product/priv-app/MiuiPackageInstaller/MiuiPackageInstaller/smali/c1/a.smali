.class public Lc1/a;
.super Ljava/lang/Object;

# interfaces
.implements Lc1/b;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lb1/m;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb1/m<",
            "Landroid/graphics/PointF;",
            "Landroid/graphics/PointF;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lb1/f;

.field private final d:Z

.field private final e:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Lb1/m;Lb1/f;ZZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lb1/m<",
            "Landroid/graphics/PointF;",
            "Landroid/graphics/PointF;",
            ">;",
            "Lb1/f;",
            "ZZ)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lc1/a;->a:Ljava/lang/String;

    iput-object p2, p0, Lc1/a;->b:Lb1/m;

    iput-object p3, p0, Lc1/a;->c:Lb1/f;

    iput-boolean p4, p0, Lc1/a;->d:Z

    iput-boolean p5, p0, Lc1/a;->e:Z

    return-void
.end method


# virtual methods
.method public a(Lcom/airbnb/lottie/a;Ld1/a;)Lx0/c;
    .locals 1

    new-instance v0, Lx0/f;

    invoke-direct {v0, p1, p2, p0}, Lx0/f;-><init>(Lcom/airbnb/lottie/a;Ld1/a;Lc1/a;)V

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lc1/a;->a:Ljava/lang/String;

    return-object v0
.end method

.method public c()Lb1/m;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lb1/m<",
            "Landroid/graphics/PointF;",
            "Landroid/graphics/PointF;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lc1/a;->b:Lb1/m;

    return-object v0
.end method

.method public d()Lb1/f;
    .locals 1

    iget-object v0, p0, Lc1/a;->c:Lb1/f;

    return-object v0
.end method

.method public e()Z
    .locals 1

    iget-boolean v0, p0, Lc1/a;->e:Z

    return v0
.end method

.method public f()Z
    .locals 1

    iget-boolean v0, p0, Lc1/a;->d:Z

    return v0
.end method
