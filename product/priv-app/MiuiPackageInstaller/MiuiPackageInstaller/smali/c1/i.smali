.class public Lc1/i;
.super Ljava/lang/Object;

# interfaces
.implements Lc1/b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lc1/i$a;
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lc1/i$a;

.field private final c:Lb1/b;

.field private final d:Lb1/m;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb1/m<",
            "Landroid/graphics/PointF;",
            "Landroid/graphics/PointF;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lb1/b;

.field private final f:Lb1/b;

.field private final g:Lb1/b;

.field private final h:Lb1/b;

.field private final i:Lb1/b;

.field private final j:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Lc1/i$a;Lb1/b;Lb1/m;Lb1/b;Lb1/b;Lb1/b;Lb1/b;Lb1/b;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lc1/i$a;",
            "Lb1/b;",
            "Lb1/m<",
            "Landroid/graphics/PointF;",
            "Landroid/graphics/PointF;",
            ">;",
            "Lb1/b;",
            "Lb1/b;",
            "Lb1/b;",
            "Lb1/b;",
            "Lb1/b;",
            "Z)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lc1/i;->a:Ljava/lang/String;

    iput-object p2, p0, Lc1/i;->b:Lc1/i$a;

    iput-object p3, p0, Lc1/i;->c:Lb1/b;

    iput-object p4, p0, Lc1/i;->d:Lb1/m;

    iput-object p5, p0, Lc1/i;->e:Lb1/b;

    iput-object p6, p0, Lc1/i;->f:Lb1/b;

    iput-object p7, p0, Lc1/i;->g:Lb1/b;

    iput-object p8, p0, Lc1/i;->h:Lb1/b;

    iput-object p9, p0, Lc1/i;->i:Lb1/b;

    iput-boolean p10, p0, Lc1/i;->j:Z

    return-void
.end method


# virtual methods
.method public a(Lcom/airbnb/lottie/a;Ld1/a;)Lx0/c;
    .locals 1

    new-instance v0, Lx0/n;

    invoke-direct {v0, p1, p2, p0}, Lx0/n;-><init>(Lcom/airbnb/lottie/a;Ld1/a;Lc1/i;)V

    return-object v0
.end method

.method public b()Lb1/b;
    .locals 1

    iget-object v0, p0, Lc1/i;->f:Lb1/b;

    return-object v0
.end method

.method public c()Lb1/b;
    .locals 1

    iget-object v0, p0, Lc1/i;->h:Lb1/b;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lc1/i;->a:Ljava/lang/String;

    return-object v0
.end method

.method public e()Lb1/b;
    .locals 1

    iget-object v0, p0, Lc1/i;->g:Lb1/b;

    return-object v0
.end method

.method public f()Lb1/b;
    .locals 1

    iget-object v0, p0, Lc1/i;->i:Lb1/b;

    return-object v0
.end method

.method public g()Lb1/b;
    .locals 1

    iget-object v0, p0, Lc1/i;->c:Lb1/b;

    return-object v0
.end method

.method public h()Lb1/m;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lb1/m<",
            "Landroid/graphics/PointF;",
            "Landroid/graphics/PointF;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lc1/i;->d:Lb1/m;

    return-object v0
.end method

.method public i()Lb1/b;
    .locals 1

    iget-object v0, p0, Lc1/i;->e:Lb1/b;

    return-object v0
.end method

.method public j()Lc1/i$a;
    .locals 1

    iget-object v0, p0, Lc1/i;->b:Lc1/i$a;

    return-object v0
.end method

.method public k()Z
    .locals 1

    iget-boolean v0, p0, Lc1/i;->j:Z

    return v0
.end method
