.class public Lj6/b;
.super Landroidx/recyclerview/widget/RecyclerView$g;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lj6/b$e;,
        Lj6/b$d;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$g<",
        "Landroidx/recyclerview/widget/RecyclerView$d0;",
        ">;"
    }
.end annotation


# instance fields
.field private c:Landroidx/recyclerview/widget/RecyclerView;

.field private d:Lj6/a;

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lm6/a;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lm6/a;",
            ">;"
        }
    .end annotation
.end field

.field private g:I

.field private h:I

.field private i:I

.field private j:I

.field private k:I

.field private l:I

.field private m:I

.field private n:I

.field private o:Landroid/view/View$OnAttachStateChangeListener;

.field private p:Landroidx/recyclerview/widget/RecyclerView$t;


# direct methods
.method public constructor <init>()V
    .locals 2

    new-instance v0, Lj6/a;

    invoke-direct {v0}, Lj6/a;-><init>()V

    const/4 v1, 0x0

    invoke-direct {p0, v1, v0}, Lj6/b;-><init>(Landroidx/recyclerview/widget/RecyclerView;Lj6/a;)V

    return-void
.end method

.method public constructor <init>(Landroidx/recyclerview/widget/RecyclerView;)V
    .locals 1

    new-instance v0, Lj6/a;

    invoke-direct {v0}, Lj6/a;-><init>()V

    invoke-direct {p0, p1, v0}, Lj6/b;-><init>(Landroidx/recyclerview/widget/RecyclerView;Lj6/a;)V

    return-void
.end method

.method public constructor <init>(Landroidx/recyclerview/widget/RecyclerView;Lj6/a;)V
    .locals 1

    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$g;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lj6/b;->e:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lj6/b;->f:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lj6/b;->g:I

    iput v0, p0, Lj6/b;->h:I

    iput v0, p0, Lj6/b;->i:I

    iput v0, p0, Lj6/b;->j:I

    iput v0, p0, Lj6/b;->k:I

    iput v0, p0, Lj6/b;->l:I

    iput v0, p0, Lj6/b;->m:I

    iput v0, p0, Lj6/b;->n:I

    new-instance v0, Lj6/b$a;

    invoke-direct {v0, p0}, Lj6/b$a;-><init>(Lj6/b;)V

    iput-object v0, p0, Lj6/b;->o:Landroid/view/View$OnAttachStateChangeListener;

    new-instance v0, Lj6/b$b;

    invoke-direct {v0, p0}, Lj6/b$b;-><init>(Lj6/b;)V

    iput-object v0, p0, Lj6/b;->p:Landroidx/recyclerview/widget/RecyclerView$t;

    iput-object p2, p0, Lj6/b;->d:Lj6/a;

    const/4 p2, 0x1

    invoke-virtual {p0, p2}, Landroidx/recyclerview/widget/RecyclerView$g;->A(Z)V

    invoke-virtual {p0, p1}, Lj6/b;->l0(Landroidx/recyclerview/widget/RecyclerView;)V

    return-void
.end method

.method static synthetic C(Lj6/b;Lm6/a$b;)V
    .locals 0

    invoke-direct {p0, p1}, Lj6/b;->O(Lm6/a$b;)V

    return-void
.end method

.method static synthetic D(Lj6/b;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lj6/b;->e:Ljava/util/List;

    return-object p0
.end method

.method static synthetic E(Lj6/b;Landroidx/recyclerview/widget/RecyclerView;)V
    .locals 0

    invoke-direct {p0, p1}, Lj6/b;->a0(Landroidx/recyclerview/widget/RecyclerView;)V

    return-void
.end method

.method static synthetic F(Lj6/b;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lj6/b;->f:Ljava/util/List;

    return-object p0
.end method

.method private I(ILm6/a;ZZ)I
    .locals 0

    invoke-virtual {p2, p0}, Lm6/a;->v(Lj6/b;)V

    iget-object p4, p0, Lj6/b;->d:Lj6/a;

    invoke-virtual {p4, p2}, Lj6/a;->i(Lm6/a;)Z

    move-result p4

    if-eqz p4, :cond_0

    iget-object p4, p0, Lj6/b;->f:Ljava/util/List;

    invoke-interface {p4, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p4

    if-nez p4, :cond_0

    iget-object p4, p0, Lj6/b;->f:Ljava/util/List;

    invoke-interface {p4, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    if-eqz p3, :cond_1

    invoke-virtual {p0, p1, p2}, Lj6/b;->X(II)V

    :cond_1
    return p2
.end method

.method private N(ILm6/a$b;)V
    .locals 0

    invoke-virtual {p0, p1}, Lj6/b;->T(I)Lm6/a;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p1, p2}, Lm6/a;->d(Lm6/a$b;)V

    :cond_0
    return-void
.end method

.method private O(Lm6/a$b;)V
    .locals 2

    iget-object v0, p0, Lj6/b;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_0

    :try_start_0
    iget-object v1, p0, Lj6/b;->e:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lm6/a;

    invoke-virtual {v1, p1}, Lm6/a;->d(Lm6/a$b;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    :goto_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private a0(Landroidx/recyclerview/widget/RecyclerView;)V
    .locals 2

    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$o;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {p1}, Landroidx/recyclerview/widget/LinearLayoutManager;->a2()I

    move-result v0

    iput v0, p0, Lj6/b;->i:I

    invoke-virtual {p1}, Landroidx/recyclerview/widget/LinearLayoutManager;->e2()I

    move-result v0

    iput v0, p0, Lj6/b;->j:I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "raiseViewObjectScrollNotify mFirstVisibleItem = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lj6/b;->i:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", mLastVisibleItem = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lj6/b;->j:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "CommonAdapter"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Landroidx/recyclerview/widget/LinearLayoutManager;->V1()I

    move-result v0

    iput v0, p0, Lj6/b;->m:I

    invoke-virtual {p1}, Landroidx/recyclerview/widget/LinearLayoutManager;->b2()I

    move-result p1

    iput p1, p0, Lj6/b;->n:I

    iget p1, p0, Lj6/b;->i:I

    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    iget v1, p0, Lj6/b;->j:I

    if-ne v1, v0, :cond_0

    return-void

    :cond_0
    iget v1, p0, Lj6/b;->g:I

    if-ge p1, v1, :cond_1

    :goto_0
    iget v1, p0, Lj6/b;->g:I

    if-ge p1, v1, :cond_1

    sget-object v1, Lm6/a$b;->i:Lm6/a$b;

    invoke-direct {p0, p1, v1}, Lj6/b;->N(ILm6/a$b;)V

    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_1
    iget p1, p0, Lj6/b;->m:I

    if-eq p1, v0, :cond_2

    iget v1, p0, Lj6/b;->k:I

    if-ge p1, v1, :cond_2

    :goto_1
    iget v1, p0, Lj6/b;->k:I

    if-ge p1, v1, :cond_2

    sget-object v1, Lm6/a$b;->k:Lm6/a$b;

    invoke-direct {p0, p1, v1}, Lj6/b;->N(ILm6/a$b;)V

    add-int/lit8 p1, p1, 0x1

    goto :goto_1

    :cond_2
    iget p1, p0, Lj6/b;->n:I

    if-eq p1, v0, :cond_3

    iget v1, p0, Lj6/b;->l:I

    if-ge p1, v1, :cond_3

    :goto_2
    iget p1, p0, Lj6/b;->n:I

    if-le v1, p1, :cond_3

    sget-object p1, Lm6/a$b;->n:Lm6/a$b;

    invoke-direct {p0, v1, p1}, Lj6/b;->N(ILm6/a$b;)V

    add-int/lit8 v1, v1, -0x1

    goto :goto_2

    :cond_3
    iget p1, p0, Lj6/b;->j:I

    iget v1, p0, Lj6/b;->h:I

    if-ge p1, v1, :cond_4

    :goto_3
    iget p1, p0, Lj6/b;->j:I

    if-le v1, p1, :cond_4

    sget-object p1, Lm6/a$b;->p:Lm6/a$b;

    invoke-direct {p0, v1, p1}, Lj6/b;->N(ILm6/a$b;)V

    add-int/lit8 v1, v1, -0x1

    goto :goto_3

    :cond_4
    iget p1, p0, Lj6/b;->j:I

    iget v1, p0, Lj6/b;->h:I

    if-le p1, v1, :cond_5

    :goto_4
    add-int/lit8 v1, v1, 0x1

    iget p1, p0, Lj6/b;->j:I

    if-gt v1, p1, :cond_5

    sget-object p1, Lm6/a$b;->j:Lm6/a$b;

    invoke-direct {p0, v1, p1}, Lj6/b;->N(ILm6/a$b;)V

    goto :goto_4

    :cond_5
    iget p1, p0, Lj6/b;->n:I

    if-eq p1, v0, :cond_6

    iget v1, p0, Lj6/b;->l:I

    if-le p1, v1, :cond_6

    :goto_5
    add-int/lit8 v1, v1, 0x1

    iget p1, p0, Lj6/b;->n:I

    if-gt v1, p1, :cond_6

    sget-object p1, Lm6/a$b;->l:Lm6/a$b;

    invoke-direct {p0, v1, p1}, Lj6/b;->N(ILm6/a$b;)V

    goto :goto_5

    :cond_6
    iget p1, p0, Lj6/b;->m:I

    if-eq p1, v0, :cond_7

    iget v1, p0, Lj6/b;->k:I

    if-le p1, v1, :cond_7

    :goto_6
    iget p1, p0, Lj6/b;->m:I

    if-ge v1, p1, :cond_7

    sget-object p1, Lm6/a$b;->m:Lm6/a$b;

    invoke-direct {p0, v1, p1}, Lj6/b;->N(ILm6/a$b;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    :cond_7
    iget p1, p0, Lj6/b;->i:I

    iget v1, p0, Lj6/b;->g:I

    if-le p1, v1, :cond_8

    :goto_7
    iget p1, p0, Lj6/b;->i:I

    if-ge v1, p1, :cond_8

    sget-object p1, Lm6/a$b;->o:Lm6/a$b;

    invoke-direct {p0, v1, p1}, Lj6/b;->N(ILm6/a$b;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    :cond_8
    iget p1, p0, Lj6/b;->i:I

    iput p1, p0, Lj6/b;->g:I

    iget p1, p0, Lj6/b;->j:I

    iput p1, p0, Lj6/b;->h:I

    iget p1, p0, Lj6/b;->m:I

    if-eq p1, v0, :cond_9

    iput p1, p0, Lj6/b;->k:I

    :cond_9
    iget p1, p0, Lj6/b;->n:I

    if-eq p1, v0, :cond_a

    iput p1, p0, Lj6/b;->l:I

    :cond_a
    return-void
.end method

.method private e0(Lm6/a;ZZ)I
    .locals 1

    iget-object p3, p0, Lj6/b;->f:Ljava/util/List;

    invoke-interface {p3, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result p3

    const/4 v0, -0x1

    if-ne p3, v0, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lm6/a;->v(Lj6/b;)V

    iget-object v0, p0, Lj6/b;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    const/4 p1, 0x1

    if-eqz p2, :cond_1

    invoke-virtual {p0, p3, p1}, Lj6/b;->Z(II)V

    :cond_1
    return p1
.end method


# virtual methods
.method public G(ILm6/a;)I
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0}, Lj6/b;->H(ILm6/a;Z)I

    move-result p1

    return p1
.end method

.method public H(ILm6/a;Z)I
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, Lj6/b;->I(ILm6/a;ZZ)I

    move-result p1

    return p1
.end method

.method public J(Lm6/a;)I
    .locals 1

    iget-object v0, p0, Lj6/b;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {p0, v0, p1}, Lj6/b;->G(ILm6/a;)I

    move-result p1

    return p1
.end method

.method public K(ILjava/util/List;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "Lm6/a;",
            ">;)I"
        }
    .end annotation

    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0}, Lj6/b;->L(ILjava/util/List;Z)I

    move-result p1

    return p1
.end method

.method public L(ILjava/util/List;Z)I
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "Lm6/a;",
            ">;Z)I"
        }
    .end annotation

    const/4 v0, 0x0

    if-eqz p2, :cond_3

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_0

    goto :goto_1

    :cond_0
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    move v1, p1

    move v2, v0

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lm6/a;

    const/4 v4, 0x1

    invoke-direct {p0, v1, v3, v0, v4}, Lj6/b;->I(ILm6/a;ZZ)I

    move-result v3

    add-int/2addr v1, v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_1
    if-eqz p3, :cond_2

    invoke-virtual {p0, p1, v2}, Lj6/b;->X(II)V

    :cond_2
    return v2

    :cond_3
    :goto_1
    return v0
.end method

.method public M(Ljava/util/List;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lm6/a;",
            ">;)I"
        }
    .end annotation

    iget-object v0, p0, Lj6/b;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {p0, v0, p1}, Lj6/b;->K(ILjava/util/List;)I

    move-result p1

    return p1
.end method

.method public P()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lj6/b;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lm6/a;

    invoke-virtual {v2}, Lm6/a;->i()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public Q()I
    .locals 1

    iget-object v0, p0, Lj6/b;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public R()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lm6/a;",
            ">;"
        }
    .end annotation

    new-instance v0, Lj6/c;

    iget-object v1, p0, Lj6/b;->f:Ljava/util/List;

    invoke-direct {v0, v1}, Lj6/c;-><init>(Ljava/util/List;)V

    return-object v0
.end method

.method public S()Landroidx/recyclerview/widget/RecyclerView;
    .locals 1

    iget-object v0, p0, Lj6/b;->c:Landroidx/recyclerview/widget/RecyclerView;

    return-object v0
.end method

.method public T(I)Lm6/a;
    .locals 1

    invoke-virtual {p0, p1}, Lj6/b;->n0(I)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    iget-object v0, p0, Lj6/b;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lm6/a;

    return-object p1
.end method

.method public U(Lm6/a;)I
    .locals 1

    if-nez p1, :cond_0

    const/4 p1, -0x1

    return p1

    :cond_0
    iget-object v0, p0, Lj6/b;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result p1

    return p1
.end method

.method public V(Lm6/a;Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lj6/b;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result p1

    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, p2}, Lj6/b;->W(IILjava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public W(IILjava/lang/Object;)V
    .locals 0

    invoke-virtual {p0, p1, p2, p3}, Landroidx/recyclerview/widget/RecyclerView$g;->n(IILjava/lang/Object;)V

    return-void
.end method

.method public X(II)V
    .locals 0

    invoke-virtual {p0, p1, p2}, Landroidx/recyclerview/widget/RecyclerView$g;->o(II)V

    return-void
.end method

.method public Y(II)V
    .locals 0

    invoke-virtual {p0, p1, p2}, Landroidx/recyclerview/widget/RecyclerView$g;->m(II)V

    return-void
.end method

.method public Z(II)V
    .locals 0

    invoke-virtual {p0, p1, p2}, Landroidx/recyclerview/widget/RecyclerView$g;->p(II)V

    return-void
.end method

.method public b0(Lm6/a;)V
    .locals 1

    iget-object v0, p0, Lj6/b;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lj6/b;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public c0(Lm6/a;)I
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lj6/b;->d0(Lm6/a;Z)I

    move-result p1

    return p1
.end method

.method public d0(Lm6/a;Z)I
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lj6/b;->e0(Lm6/a;ZZ)I

    move-result p1

    return p1
.end method

.method public f()I
    .locals 1

    iget-object v0, p0, Lj6/b;->f:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_0
    return v0
.end method

.method public f0(Z)I
    .locals 4

    iget-object v0, p0, Lj6/b;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lj6/b;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lm6/a;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lm6/a;->v(Lj6/b;)V

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lj6/b;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$g;->k()V

    :cond_1
    return v0
.end method

.method public g(I)J
    .locals 2

    invoke-virtual {p0, p1}, Lj6/b;->T(I)Lm6/a;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Lm6/a;->j()I

    move-result p1

    :goto_0
    int-to-long v0, p1

    return-wide v0
.end method

.method public g0(ILm6/a;Z)V
    .locals 4

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lj6/b;->f:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iget-object v1, p0, Lj6/b;->f:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lm6/a;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-direct {p0, v1, v2, v3}, Lj6/b;->e0(Lm6/a;ZZ)I

    move-result v1

    invoke-direct {p0, p1, p2, v2, v3}, Lj6/b;->I(ILm6/a;ZZ)I

    move-result p2

    if-eqz p3, :cond_1

    const/4 p3, 0x0

    if-eq v1, p2, :cond_0

    new-instance p1, Lj6/b$c;

    invoke-direct {p1, p0, v0}, Lj6/b$c;-><init>(Lj6/b;Ljava/util/List;)V

    invoke-static {p1, v2}, Landroidx/recyclerview/widget/e;->a(Landroidx/recyclerview/widget/e$b;Z)Landroidx/recyclerview/widget/e$c;

    move-result-object p1

    new-instance p2, Lj6/b$d;

    invoke-direct {p2, p0, p3}, Lj6/b$d;-><init>(Lj6/b;Lj6/b$a;)V

    invoke-virtual {p1, p2}, Landroidx/recyclerview/widget/e$c;->d(Landroidx/recyclerview/widget/j;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lj6/b;->W(IILjava/lang/Object;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public h(I)I
    .locals 2

    iget-object v0, p0, Lj6/b;->d:Lj6/a;

    iget-object v1, p0, Lj6/b;->f:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lm6/a;

    invoke-virtual {v0, p1}, Lj6/a;->c(Lm6/a;)I

    move-result p1

    return p1
.end method

.method public h0(Lm6/a;Lm6/a;)V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0}, Lj6/b;->i0(Lm6/a;Lm6/a;Z)V

    return-void
.end method

.method public i0(Lm6/a;Lm6/a;Z)V
    .locals 1

    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    iget-object v0, p0, Lj6/b;->f:Ljava/util/List;

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result p1

    invoke-virtual {p0, p1}, Lj6/b;->n0(I)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0, p1, p2, p3}, Lj6/b;->g0(ILm6/a;Z)V

    :cond_1
    :goto_0
    return-void
.end method

.method public j0(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lm6/a;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lj6/b;->k0(Ljava/util/List;Z)V

    return-void
.end method

.method public k0(Ljava/util/List;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lm6/a;",
            ">;Z)V"
        }
    .end annotation

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lj6/b;->f0(Z)I

    invoke-virtual {p0, v0, p1, v0}, Lj6/b;->L(ILjava/util/List;Z)I

    if-eqz p2, :cond_0

    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$g;->k()V

    :cond_0
    return-void
.end method

.method public l0(Landroidx/recyclerview/widget/RecyclerView;)V
    .locals 2

    if-nez p1, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lj6/b;->c:Landroidx/recyclerview/widget/RecyclerView;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lj6/b;->o:Landroid/view/View$OnAttachStateChangeListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    iget-object v0, p0, Lj6/b;->c:Landroidx/recyclerview/widget/RecyclerView;

    iget-object v1, p0, Lj6/b;->p:Landroidx/recyclerview/widget/RecyclerView$t;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->Z0(Landroidx/recyclerview/widget/RecyclerView$t;)V

    :cond_1
    iput-object p1, p0, Lj6/b;->c:Landroidx/recyclerview/widget/RecyclerView;

    iget-object v0, p0, Lj6/b;->o:Landroid/view/View$OnAttachStateChangeListener;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    iget-object p1, p0, Lj6/b;->c:Landroidx/recyclerview/widget/RecyclerView;

    iget-object v0, p0, Lj6/b;->p:Landroidx/recyclerview/widget/RecyclerView$t;

    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->k(Landroidx/recyclerview/widget/RecyclerView$t;)V

    iget-object p1, p0, Lj6/b;->c:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {p1, p0}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$g;)V

    return-void
.end method

.method public m0(Lm6/a;)V
    .locals 1

    iget-object v0, p0, Lj6/b;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lj6/b;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public n0(I)Z
    .locals 1

    if-ltz p1, :cond_0

    iget-object v0, p0, Lj6/b;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public r(Landroidx/recyclerview/widget/RecyclerView$d0;I)V
    .locals 4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {p0, p2}, Lj6/b;->h(I)I

    iget-object v2, p0, Lj6/b;->d:Lj6/a;

    iget-object v3, p0, Lj6/b;->f:Ljava/util/List;

    invoke-virtual {v2, v3, p2, p1}, Lj6/a;->f(Ljava/util/List;ILandroidx/recyclerview/widget/RecyclerView$d0;)V

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " bind view using :"

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, v0

    invoke-virtual {p2, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string p1, " ms"

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "CommonAdapter"

    invoke-static {p2, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public s(Landroidx/recyclerview/widget/RecyclerView$d0;ILjava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/recyclerview/widget/RecyclerView$d0;",
            "I",
            "Ljava/util/List<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-interface {p3}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object p3, p0, Lj6/b;->d:Lj6/a;

    iget-object v2, p0, Lj6/b;->f:Ljava/util/List;

    invoke-virtual {p3, v2, p2, p1}, Lj6/a;->f(Ljava/util/List;ILandroidx/recyclerview/widget/RecyclerView$d0;)V

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lj6/b;->d:Lj6/a;

    iget-object v3, p0, Lj6/b;->f:Ljava/util/List;

    invoke-virtual {v2, v3, p2, p1, p3}, Lj6/a;->g(Ljava/util/List;ILandroidx/recyclerview/widget/RecyclerView$d0;Ljava/util/List;)V

    :goto_0
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " bind view using :"

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, v0

    invoke-virtual {p2, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string p1, " ms"

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "CommonAdapter"

    invoke-static {p2, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public t(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$d0;
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "WrongConstant"
        }
    .end annotation

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-object v2, p0, Lj6/b;->d:Lj6/a;

    invoke-virtual {v2, p1, p2}, Lj6/a;->h(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$d0;

    move-result-object p1

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " on create view holder using :"

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, v0

    invoke-virtual {p2, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v0, " ms"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    const-string v0, "CommonAdapter"

    invoke-static {v0, p2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-object p1
.end method

.method public y(Landroidx/recyclerview/widget/RecyclerView$d0;)V
    .locals 1

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView$d0;->getAdapterPosition()I

    move-result v0

    invoke-virtual {p0, v0}, Lj6/b;->T(I)Lm6/a;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lm6/a;->q(Landroidx/recyclerview/widget/RecyclerView$d0;)V

    :cond_0
    instance-of v0, p1, Lj6/b$e;

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, Lj6/b$e;

    invoke-interface {v0}, Lj6/b$e;->a()V

    :cond_1
    invoke-super {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$g;->y(Landroidx/recyclerview/widget/RecyclerView$d0;)V

    return-void
.end method
