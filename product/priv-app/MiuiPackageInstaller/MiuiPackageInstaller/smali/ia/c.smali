.class public Lia/c;
.super Ljava/lang/Object;


# static fields
.field private static volatile a:Z


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public static a(Landroid/view/View;)V
    .locals 1

    sget-object v0, Lmiuix/animation/f$a;->a:Lmiuix/animation/f$a;

    invoke-static {p0, v0}, Lia/c;->b(Landroid/view/View;Lmiuix/animation/f$a;)V

    return-void
.end method

.method public static b(Landroid/view/View;Lmiuix/animation/f$a;)V
    .locals 5

    const/4 v0, 0x1

    new-array v1, v0, [Landroid/view/View;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    invoke-static {v1}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object v1

    invoke-interface {v1}, Lmiuix/animation/e;->c()Lmiuix/animation/j;

    move-result-object v1

    new-array v3, v2, [Lmiuix/animation/j$b;

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-interface {v1, v4, v3}, Lmiuix/animation/j;->G(F[Lmiuix/animation/j$b;)Lmiuix/animation/j;

    move-result-object v1

    new-array v3, v2, [Lc9/a;

    invoke-interface {v1, p0, v3}, Lmiuix/animation/j;->z(Landroid/view/View;[Lc9/a;)V

    new-array v0, v0, [Landroid/view/View;

    aput-object p0, v0, v2

    invoke-static {v0}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object v0

    invoke-interface {v0}, Lmiuix/animation/e;->a()Lmiuix/animation/f;

    move-result-object v0

    invoke-interface {v0, p1}, Lmiuix/animation/f;->e(Lmiuix/animation/f$a;)Lmiuix/animation/f;

    move-result-object p1

    new-array v0, v2, [Lc9/a;

    invoke-interface {p1, p0, v0}, Lmiuix/animation/f;->x(Landroid/view/View;[Lc9/a;)V

    return-void
.end method

.method public static c(Landroid/view/View;)V
    .locals 6

    const/4 v0, 0x1

    new-array v1, v0, [Landroid/view/View;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    invoke-static {v1}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object v1

    invoke-interface {v1}, Lmiuix/animation/e;->c()Lmiuix/animation/j;

    move-result-object v1

    const/4 v3, 0x0

    invoke-interface {v1, v3, v3, v3, v3}, Lmiuix/animation/j;->a(FFFF)Lmiuix/animation/j;

    move-result-object v1

    new-array v4, v2, [Lmiuix/animation/j$b;

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-interface {v1, v5, v4}, Lmiuix/animation/j;->G(F[Lmiuix/animation/j$b;)Lmiuix/animation/j;

    move-result-object v1

    new-array v0, v0, [Landroid/view/View;

    aput-object p0, v0, v2

    invoke-static {v0}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object v0

    invoke-interface {v0}, Lmiuix/animation/e;->a()Lmiuix/animation/f;

    move-result-object v0

    invoke-interface {v0, v3, v3, v3, v3}, Lmiuix/animation/f;->a(FFFF)Lmiuix/animation/f;

    move-result-object v0

    sget-object v4, Lmiuix/animation/f$a;->a:Lmiuix/animation/f$a;

    invoke-interface {v0, v4}, Lmiuix/animation/f;->e(Lmiuix/animation/f$a;)Lmiuix/animation/f;

    move-result-object v0

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lia/i;->b(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_0

    const v3, 0x3e19999a    # 0.15f

    invoke-interface {v1, v3, v5, v5, v5}, Lmiuix/animation/j;->b(FFFF)Lmiuix/animation/j;

    invoke-interface {v0, v3, v5, v5, v5}, Lmiuix/animation/f;->b(FFFF)Lmiuix/animation/f;

    goto :goto_0

    :cond_0
    const v4, 0x3da3d70a    # 0.08f

    invoke-interface {v1, v4, v3, v3, v3}, Lmiuix/animation/j;->b(FFFF)Lmiuix/animation/j;

    invoke-interface {v0, v4, v3, v3, v3}, Lmiuix/animation/f;->b(FFFF)Lmiuix/animation/f;

    :goto_0
    new-array v3, v2, [Lc9/a;

    invoke-interface {v1, p0, v3}, Lmiuix/animation/j;->z(Landroid/view/View;[Lc9/a;)V

    new-array v1, v2, [Lc9/a;

    invoke-interface {v0, p0, v1}, Lmiuix/animation/f;->x(Landroid/view/View;[Lc9/a;)V

    return-void
.end method

.method public static d()Z
    .locals 1

    sget-boolean v0, Lia/c;->a:Z

    return v0
.end method
