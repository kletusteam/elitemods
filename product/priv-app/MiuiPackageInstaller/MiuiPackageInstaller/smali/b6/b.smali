.class public final Lb6/b;
.super Lb6/l;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ll6/c;)V
    .locals 1

    const-string v0, "mContext"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mActionDelegateProvider"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2}, Lb6/l;-><init>(Landroid/content/Context;Ll6/c;)V

    return-void
.end method

.method public static synthetic I(Lb6/r$a;Ljava/util/List;Ljava/util/List;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lb6/b;->K(Lb6/r$a;Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method private final J(Lcom/miui/packageInstaller/model/CloudParams;Lcom/miui/packageInstaller/model/ApkInfo;Lb6/r$a;)V
    .locals 18

    move-object/from16 v8, p1

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    new-instance v11, Lcom/miui/packageInstaller/ui/listcomponets/j;

    invoke-virtual/range {p0 .. p0}, Lb6/r;->u()Landroid/content/Context;

    move-result-object v1

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x18

    const/4 v7, 0x0

    move-object v0, v11

    move-object/from16 v2, p2

    move-object/from16 v3, p1

    invoke-direct/range {v0 .. v7}, Lcom/miui/packageInstaller/ui/listcomponets/j;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/ApkInfo;Lcom/miui/packageInstaller/model/CloudParams;Ll6/c;Lm6/b;ILm8/g;)V

    invoke-interface {v9, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, v8, Lcom/miui/packageInstaller/model/CloudParams;->bit64Tip:Lcom/miui/packageInstaller/model/BitTip;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/BitTip;->getText()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    move v0, v2

    :goto_0
    if-ne v0, v1, :cond_1

    goto :goto_1

    :cond_1
    move v1, v2

    :goto_1
    if-eqz v1, :cond_2

    new-instance v0, Lcom/miui/packageInstaller/ui/listcomponets/CpuCompatibleViewObject;

    invoke-virtual/range {p0 .. p0}, Lb6/r;->u()Landroid/content/Context;

    move-result-object v12

    iget-object v13, v8, Lcom/miui/packageInstaller/model/CloudParams;->bit64Tip:Lcom/miui/packageInstaller/model/BitTip;

    const-string v1, "rules.bit64Tip"

    invoke-static {v13, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0xc

    const/16 v17, 0x0

    move-object v11, v0

    invoke-direct/range {v11 .. v17}, Lcom/miui/packageInstaller/ui/listcomponets/CpuCompatibleViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/BitTip;Ll6/c;Lm6/b;ILm8/g;)V

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    iget-object v0, v8, Lcom/miui/packageInstaller/model/CloudParams;->secureWarningTip:Lcom/miui/packageInstaller/model/WarningCardInfo;

    const/4 v1, 0x0

    if-eqz v0, :cond_3

    iget-object v0, v0, Lcom/miui/packageInstaller/model/WarningCardInfo;->title:Ljava/lang/String;

    goto :goto_2

    :cond_3
    move-object v0, v1

    :goto_2
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, v8, Lcom/miui/packageInstaller/model/CloudParams;->secureWarningTip:Lcom/miui/packageInstaller/model/WarningCardInfo;

    if-eqz v0, :cond_4

    iget-object v1, v0, Lcom/miui/packageInstaller/model/WarningCardInfo;->text:Ljava/lang/String;

    :cond_4
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    new-instance v7, Lcom/miui/packageInstaller/ui/listcomponets/WarningInfoViewObject;

    invoke-virtual/range {p0 .. p0}, Lb6/r;->u()Landroid/content/Context;

    move-result-object v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xc

    const/4 v6, 0x0

    move-object v0, v7

    move-object/from16 v2, p1

    invoke-direct/range {v0 .. v6}, Lcom/miui/packageInstaller/ui/listcomponets/WarningInfoViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/CloudParams;Ll6/c;Lm6/b;ILm8/g;)V

    invoke-interface {v9, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_5
    new-instance v0, Lcom/miui/packageInstaller/ui/listcomponets/VirusInfoViewObject;

    invoke-virtual/range {p0 .. p0}, Lb6/r;->u()Landroid/content/Context;

    move-result-object v12

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0xe

    const/16 v17, 0x0

    move-object v11, v0

    invoke-direct/range {v11 .. v17}, Lcom/miui/packageInstaller/ui/listcomponets/VirusInfoViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/Virus;Ll6/c;Lm6/b;ILm8/g;)V

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeInDetailViewObject;

    invoke-virtual/range {p0 .. p0}, Lb6/r;->u()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x6

    const/4 v6, 0x0

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeInDetailViewObject;-><init>(Landroid/content/Context;Ll6/c;Lm6/b;ILm8/g;)V

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/miui/packageInstaller/ui/listcomponets/FootViewObject;

    invoke-virtual/range {p0 .. p0}, Lb6/r;->u()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/miui/packageInstaller/ui/listcomponets/FootViewObject;-><init>(Landroid/content/Context;)V

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object v0

    new-instance v1, Lb6/a;

    move-object/from16 v2, p3

    invoke-direct {v1, v2, v9, v10}, Lb6/a;-><init>(Lb6/r$a;Ljava/util/List;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Lf6/z;->e(Ljava/lang/Runnable;)V

    return-void
.end method

.method private static final K(Lb6/r$a;Ljava/util/List;Ljava/util/List;)V
    .locals 1

    const-string v0, "$callBack"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$layout"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$bottomLayout"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p0, p1, p2}, Lb6/r$a;->a(Ljava/util/List;Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method public o(Lcom/miui/packageInstaller/model/CloudParams;Lcom/miui/packageInstaller/model/ApkInfo;Lb6/r$a;)V
    .locals 2

    const-string v0, "rules"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "apkInfo"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "callBack"

    invoke-static {p3, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lb6/r;->u()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    invoke-direct {p0, p1, p2, p3}, Lb6/b;->J(Lcom/miui/packageInstaller/model/CloudParams;Lcom/miui/packageInstaller/model/ApkInfo;Lb6/r$a;)V

    goto :goto_0

    :cond_0
    invoke-super {p0, p1, p2, p3}, Lb6/l;->o(Lcom/miui/packageInstaller/model/CloudParams;Lcom/miui/packageInstaller/model/ApkInfo;Lb6/r$a;)V

    :goto_0
    return-void
.end method
