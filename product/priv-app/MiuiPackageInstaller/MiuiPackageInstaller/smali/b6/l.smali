.class public Lb6/l;
.super Lb6/r;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ll6/c;)V
    .locals 1

    const-string v0, "mContext"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mActionDelegateProvider"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2}, Lb6/r;-><init>(Landroid/content/Context;Ll6/c;)V

    return-void
.end method

.method public static synthetic A(Lb6/r$a;Ljava/util/List;Ljava/util/List;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lb6/l;->C(Lb6/r$a;Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method public static synthetic B(Lb6/r$a;Ljava/util/List;Ljava/util/List;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lb6/l;->E(Lb6/r$a;Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method private static final C(Lb6/r$a;Ljava/util/List;Ljava/util/List;)V
    .locals 1

    const-string v0, "$callBack"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$layout"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$bottomLayout"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p0, p1, p2}, Lb6/r$a;->a(Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method private static final D(Lb6/r$a;Ljava/util/List;Ljava/util/List;)V
    .locals 1

    const-string v0, "$callBack"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$layout"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$bottomLayout"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p0, p1, p2}, Lb6/r$a;->a(Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method private static final E(Lb6/r$a;Ljava/util/List;Ljava/util/List;)V
    .locals 1

    const-string v0, "$callBack"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$layout"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$bottomLayout"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p0, p1, p2}, Lb6/r$a;->a(Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method private static final F(Lb6/r$a;Ljava/util/List;Ljava/util/List;)V
    .locals 1

    const-string v0, "$callBack"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$layout"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$bottomLayout"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p0, p1, p2}, Lb6/r$a;->a(Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method private static final G(Lb6/r$a;Ljava/util/List;Ljava/util/ArrayList;)V
    .locals 1

    const-string v0, "$callBack"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$layout"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$bottomLayout"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p0, p1, p2}, Lb6/r$a;->a(Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method private static final H(Lb6/r$a;Ljava/util/List;Ljava/util/List;)V
    .locals 1

    const-string v0, "$callBack"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$layout"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$bottomLayout"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p0, p1, p2}, Lb6/r$a;->a(Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method public static synthetic w(Lb6/r$a;Ljava/util/List;Ljava/util/List;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lb6/l;->H(Lb6/r$a;Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method public static synthetic x(Lb6/r$a;Ljava/util/List;Ljava/util/List;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lb6/l;->F(Lb6/r$a;Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method public static synthetic y(Lb6/r$a;Ljava/util/List;Ljava/util/ArrayList;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lb6/l;->G(Lb6/r$a;Ljava/util/List;Ljava/util/ArrayList;)V

    return-void
.end method

.method public static synthetic z(Lb6/r$a;Ljava/util/List;Ljava/util/List;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lb6/l;->D(Lb6/r$a;Ljava/util/List;Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method public h(Lcom/miui/packageInstaller/model/CloudParams;Lcom/miui/packageInstaller/model/ApkInfo;Lb6/r$a;)V
    .locals 12

    const-string v0, "rules"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "apk"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "callBack"

    invoke-static {p3, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v2, p1, Lcom/miui/packageInstaller/model/CloudParams;->secureWarningTip:Lcom/miui/packageInstaller/model/WarningCardInfo;

    const/4 v3, 0x0

    if-eqz v2, :cond_0

    iget-object v2, v2, Lcom/miui/packageInstaller/model/WarningCardInfo;->title:Ljava/lang/String;

    goto :goto_0

    :cond_0
    move-object v2, v3

    :goto_0
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    iget-object v2, p1, Lcom/miui/packageInstaller/model/CloudParams;->secureWarningTip:Lcom/miui/packageInstaller/model/WarningCardInfo;

    if-eqz v2, :cond_1

    iget-object v2, v2, Lcom/miui/packageInstaller/model/WarningCardInfo;->text:Ljava/lang/String;

    goto :goto_1

    :cond_1
    move-object v2, v3

    :goto_1
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    new-instance v6, Lcom/miui/packageInstaller/model/PureModeTip;

    invoke-direct {v6}, Lcom/miui/packageInstaller/model/PureModeTip;-><init>()V

    iget-object v2, p1, Lcom/miui/packageInstaller/model/CloudParams;->secureWarningTip:Lcom/miui/packageInstaller/model/WarningCardInfo;

    if-eqz v2, :cond_2

    iget-object v2, v2, Lcom/miui/packageInstaller/model/WarningCardInfo;->title:Ljava/lang/String;

    goto :goto_2

    :cond_2
    move-object v2, v3

    :goto_2
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, Lcom/miui/packageInstaller/model/PureModeTip;->setTitle(Ljava/lang/String;)V

    iget-object v2, p1, Lcom/miui/packageInstaller/model/CloudParams;->secureWarningTip:Lcom/miui/packageInstaller/model/WarningCardInfo;

    if-eqz v2, :cond_3

    iget-object v3, v2, Lcom/miui/packageInstaller/model/WarningCardInfo;->text:Ljava/lang/String;

    :cond_3
    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, Lcom/miui/packageInstaller/model/PureModeTip;->setMessage(Ljava/lang/String;)V

    new-instance v3, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject;

    invoke-virtual {p0}, Lb6/r;->u()Landroid/content/Context;

    move-result-object v5

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x18

    const/4 v11, 0x0

    move-object v4, v3

    move-object v7, p2

    invoke-direct/range {v4 .. v11}, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/PureModeTip;Lcom/miui/packageInstaller/model/ApkInfo;Ll6/c;Lm6/b;ILm8/g;)V

    :cond_4
    new-instance v2, Lcom/miui/packageInstaller/ui/listcomponets/k;

    invoke-virtual {p0}, Lb6/r;->u()Landroid/content/Context;

    move-result-object v5

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0xc

    const/4 v10, 0x0

    move-object v4, v2

    move-object v6, p2

    invoke-direct/range {v4 .. v10}, Lcom/miui/packageInstaller/ui/listcomponets/k;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/ApkInfo;Ll6/c;Lm6/b;ILm8/g;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object p2, p1, Lcom/miui/packageInstaller/model/CloudParams;->bit64Tip:Lcom/miui/packageInstaller/model/BitTip;

    const/4 v2, 0x1

    const/4 v4, 0x0

    if-eqz p2, :cond_6

    invoke-virtual {p2}, Lcom/miui/packageInstaller/model/BitTip;->getText()Ljava/lang/String;

    move-result-object p2

    if-eqz p2, :cond_6

    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result p2

    if-lez p2, :cond_5

    move p2, v2

    goto :goto_3

    :cond_5
    move p2, v4

    :goto_3
    if-ne p2, v2, :cond_6

    goto :goto_4

    :cond_6
    move v2, v4

    :goto_4
    if-eqz v2, :cond_7

    new-instance p2, Lcom/miui/packageInstaller/ui/listcomponets/CpuCompatibleViewObject;

    invoke-virtual {p0}, Lb6/r;->u()Landroid/content/Context;

    move-result-object v5

    iget-object v6, p1, Lcom/miui/packageInstaller/model/CloudParams;->bit64Tip:Lcom/miui/packageInstaller/model/BitTip;

    const-string p1, "rules.bit64Tip"

    invoke-static {v6, p1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0xc

    const/4 v10, 0x0

    move-object v4, p2

    invoke-direct/range {v4 .. v10}, Lcom/miui/packageInstaller/ui/listcomponets/CpuCompatibleViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/BitTip;Ll6/c;Lm6/b;ILm8/g;)V

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_7
    if-eqz v3, :cond_8

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_8
    new-instance p1, Lcom/miui/packageInstaller/ui/listcomponets/FootViewObject;

    invoke-virtual {p0}, Lb6/r;->u()Landroid/content/Context;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/miui/packageInstaller/ui/listcomponets/FootViewObject;-><init>(Landroid/content/Context;)V

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object p1

    new-instance p2, Lb6/j;

    invoke-direct {p2, p3, v0, v1}, Lb6/j;-><init>(Lb6/r$a;Ljava/util/List;Ljava/util/List;)V

    invoke-virtual {p1, p2}, Lf6/z;->e(Ljava/lang/Runnable;)V

    return-void
.end method

.method public j(Lcom/miui/packageInstaller/model/CloudParams;Lcom/miui/packageInstaller/model/ApkInfo;Lb6/r$a;)V
    .locals 19

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    const-string v2, "rules"

    invoke-static {v0, v2}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "apk"

    move-object/from16 v10, p2

    invoke-static {v10, v2}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "callBack"

    invoke-static {v1, v2}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    iget-object v3, v0, Lcom/miui/packageInstaller/model/CloudParams;->secureWarningTip:Lcom/miui/packageInstaller/model/WarningCardInfo;

    const/4 v4, 0x0

    if-eqz v3, :cond_0

    iget-object v3, v3, Lcom/miui/packageInstaller/model/WarningCardInfo;->title:Ljava/lang/String;

    goto :goto_0

    :cond_0
    move-object v3, v4

    :goto_0
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, v0, Lcom/miui/packageInstaller/model/CloudParams;->secureWarningTip:Lcom/miui/packageInstaller/model/WarningCardInfo;

    if-eqz v3, :cond_1

    iget-object v3, v3, Lcom/miui/packageInstaller/model/WarningCardInfo;->text:Ljava/lang/String;

    goto :goto_1

    :cond_1
    move-object v3, v4

    :goto_1
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    new-instance v4, Lcom/miui/packageInstaller/ui/listcomponets/BundleAppTipsViewObject;

    invoke-virtual/range {p0 .. p0}, Lb6/r;->u()Landroid/content/Context;

    move-result-object v13

    iget-object v14, v0, Lcom/miui/packageInstaller/model/CloudParams;->secureWarningTip:Lcom/miui/packageInstaller/model/WarningCardInfo;

    const-string v0, "rules.secureWarningTip"

    invoke-static {v14, v0}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0xc

    const/16 v18, 0x0

    move-object v12, v4

    invoke-direct/range {v12 .. v18}, Lcom/miui/packageInstaller/ui/listcomponets/BundleAppTipsViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/WarningCardInfo;Ll6/c;Lm6/b;ILm8/g;)V

    :cond_2
    move-object v0, v4

    new-instance v12, Lcom/miui/packageInstaller/ui/listcomponets/k;

    invoke-virtual/range {p0 .. p0}, Lb6/r;->u()Landroid/content/Context;

    move-result-object v4

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0xc

    const/4 v9, 0x0

    move-object v3, v12

    move-object/from16 v5, p2

    invoke-direct/range {v3 .. v9}, Lcom/miui/packageInstaller/ui/listcomponets/k;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/ApkInfo;Ll6/c;Lm6/b;ILm8/g;)V

    invoke-interface {v2, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    if-eqz v0, :cond_3

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    new-instance v0, Lcom/miui/packageInstaller/ui/listcomponets/BundleAppViewObject;

    invoke-virtual/range {p0 .. p0}, Lb6/r;->u()Landroid/content/Context;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lb6/r;->t()Ll6/c;

    move-result-object v6

    const/4 v7, 0x0

    const/16 v8, 0x8

    const/4 v9, 0x0

    move-object v3, v0

    move-object/from16 v5, p2

    invoke-direct/range {v3 .. v9}, Lcom/miui/packageInstaller/ui/listcomponets/BundleAppViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/ApkInfo;Ll6/c;Lm6/b;ILm8/g;)V

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/miui/packageInstaller/ui/listcomponets/FootViewObject;

    invoke-virtual/range {p0 .. p0}, Lb6/r;->u()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/miui/packageInstaller/ui/listcomponets/FootViewObject;-><init>(Landroid/content/Context;)V

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object v0

    new-instance v3, Lb6/i;

    invoke-direct {v3, v1, v2, v11}, Lb6/i;-><init>(Lb6/r$a;Ljava/util/List;Ljava/util/List;)V

    invoke-virtual {v0, v3}, Lf6/z;->e(Ljava/lang/Runnable;)V

    return-void
.end method

.method public l(Lcom/miui/packageInstaller/model/CloudParams;Lcom/miui/packageInstaller/model/ApkInfo;Lb6/r$a;)V
    .locals 13

    move-object/from16 v0, p3

    const-string v1, "rules"

    move-object v5, p1

    invoke-static {p1, v1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "apkInfo"

    move-object v10, p2

    invoke-static {p2, v1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "callBack"

    invoke-static {v0, v1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    new-instance v12, Lcom/miui/packageInstaller/ui/listcomponets/l;

    invoke-virtual {p0}, Lb6/r;->u()Landroid/content/Context;

    move-result-object v3

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x18

    const/4 v9, 0x0

    move-object v2, v12

    move-object v4, p2

    invoke-direct/range {v2 .. v9}, Lcom/miui/packageInstaller/ui/listcomponets/l;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/ApkInfo;Lcom/miui/packageInstaller/model/CloudParams;Ll6/c;Lm6/b;ILm8/g;)V

    invoke-interface {v1, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v9, Lcom/miui/packageInstaller/ui/listcomponets/BundleAppViewObject;

    invoke-virtual {p0}, Lb6/r;->u()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {p0}, Lb6/r;->t()Ll6/c;

    move-result-object v5

    const/16 v7, 0x8

    const/4 v8, 0x0

    move-object v2, v9

    invoke-direct/range {v2 .. v8}, Lcom/miui/packageInstaller/ui/listcomponets/BundleAppViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/ApkInfo;Ll6/c;Lm6/b;ILm8/g;)V

    invoke-interface {v1, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v2, Lcom/miui/packageInstaller/ui/listcomponets/FootViewObject;

    invoke-virtual {p0}, Lb6/r;->u()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/miui/packageInstaller/ui/listcomponets/FootViewObject;-><init>(Landroid/content/Context;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object v2

    new-instance v3, Lb6/k;

    invoke-direct {v3, v0, v1, v11}, Lb6/k;-><init>(Lb6/r$a;Ljava/util/List;Ljava/util/List;)V

    invoke-virtual {v2, v3}, Lf6/z;->e(Ljava/lang/Runnable;)V

    return-void
.end method

.method public o(Lcom/miui/packageInstaller/model/CloudParams;Lcom/miui/packageInstaller/model/ApkInfo;Lb6/r$a;)V
    .locals 12

    const-string v0, "rules"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "apk"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "callBack"

    invoke-static {p3, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    new-instance v10, Lcom/miui/packageInstaller/ui/listcomponets/l;

    invoke-virtual {p0}, Lb6/r;->u()Landroid/content/Context;

    move-result-object v3

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x18

    const/4 v9, 0x0

    move-object v2, v10

    move-object v4, p2

    move-object v5, p1

    invoke-direct/range {v2 .. v9}, Lcom/miui/packageInstaller/ui/listcomponets/l;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/ApkInfo;Lcom/miui/packageInstaller/model/CloudParams;Ll6/c;Lm6/b;ILm8/g;)V

    invoke-interface {v0, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v2, p1, Lcom/miui/packageInstaller/model/CloudParams;->bit64Tip:Lcom/miui/packageInstaller/model/BitTip;

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/miui/packageInstaller/model/BitTip;->getText()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-lez v2, :cond_0

    move v2, v3

    goto :goto_0

    :cond_0
    move v2, v4

    :goto_0
    if-ne v2, v3, :cond_1

    goto :goto_1

    :cond_1
    move v3, v4

    :goto_1
    if-eqz v3, :cond_2

    new-instance v2, Lcom/miui/packageInstaller/ui/listcomponets/CpuCompatibleViewObject;

    invoke-virtual {p0}, Lb6/r;->u()Landroid/content/Context;

    move-result-object v5

    iget-object v6, p1, Lcom/miui/packageInstaller/model/CloudParams;->bit64Tip:Lcom/miui/packageInstaller/model/BitTip;

    const-string v3, "rules.bit64Tip"

    invoke-static {v6, v3}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0xc

    const/4 v10, 0x0

    move-object v4, v2

    invoke-direct/range {v4 .. v10}, Lcom/miui/packageInstaller/ui/listcomponets/CpuCompatibleViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/BitTip;Ll6/c;Lm6/b;ILm8/g;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    iget-object v2, p1, Lcom/miui/packageInstaller/model/CloudParams;->secureWarningTip:Lcom/miui/packageInstaller/model/WarningCardInfo;

    const/4 v3, 0x0

    if-eqz v2, :cond_3

    iget-object v2, v2, Lcom/miui/packageInstaller/model/WarningCardInfo;->title:Ljava/lang/String;

    goto :goto_2

    :cond_3
    move-object v2, v3

    :goto_2
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_7

    iget-object v2, p1, Lcom/miui/packageInstaller/model/CloudParams;->secureWarningTip:Lcom/miui/packageInstaller/model/WarningCardInfo;

    if-eqz v2, :cond_4

    iget-object v2, v2, Lcom/miui/packageInstaller/model/WarningCardInfo;->text:Ljava/lang/String;

    goto :goto_3

    :cond_4
    move-object v2, v3

    :goto_3
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_7

    new-instance v6, Lcom/miui/packageInstaller/model/PureModeTip;

    invoke-direct {v6}, Lcom/miui/packageInstaller/model/PureModeTip;-><init>()V

    iget-object v2, p1, Lcom/miui/packageInstaller/model/CloudParams;->secureWarningTip:Lcom/miui/packageInstaller/model/WarningCardInfo;

    if-eqz v2, :cond_5

    iget-object v2, v2, Lcom/miui/packageInstaller/model/WarningCardInfo;->title:Ljava/lang/String;

    goto :goto_4

    :cond_5
    move-object v2, v3

    :goto_4
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, Lcom/miui/packageInstaller/model/PureModeTip;->setTitle(Ljava/lang/String;)V

    iget-object p1, p1, Lcom/miui/packageInstaller/model/CloudParams;->secureWarningTip:Lcom/miui/packageInstaller/model/WarningCardInfo;

    if-eqz p1, :cond_6

    iget-object v3, p1, Lcom/miui/packageInstaller/model/WarningCardInfo;->text:Ljava/lang/String;

    :cond_6
    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v6, p1}, Lcom/miui/packageInstaller/model/PureModeTip;->setMessage(Ljava/lang/String;)V

    new-instance p1, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject;

    invoke-virtual {p0}, Lb6/r;->u()Landroid/content/Context;

    move-result-object v5

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x18

    const/4 v11, 0x0

    move-object v4, p1

    move-object v7, p2

    invoke-direct/range {v4 .. v11}, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/PureModeTip;Lcom/miui/packageInstaller/model/ApkInfo;Ll6/c;Lm6/b;ILm8/g;)V

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_7
    new-instance p1, Lcom/miui/packageInstaller/ui/listcomponets/FootViewObject;

    invoke-virtual {p0}, Lb6/r;->u()Landroid/content/Context;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/miui/packageInstaller/ui/listcomponets/FootViewObject;-><init>(Landroid/content/Context;)V

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object p1

    new-instance p2, Lb6/h;

    invoke-direct {p2, p3, v0, v1}, Lb6/h;-><init>(Lb6/r$a;Ljava/util/List;Ljava/util/List;)V

    invoke-virtual {p1, p2}, Lf6/z;->e(Ljava/lang/Runnable;)V

    return-void
.end method

.method public q(Lb6/r$a;Lcom/miui/packageInstaller/model/ApkInfo;)V
    .locals 9

    const-string v0, "callBack"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "apk"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v8, Lcom/miui/packageInstaller/ui/listcomponets/k;

    invoke-virtual {p0}, Lb6/r;->u()Landroid/content/Context;

    move-result-object v2

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xc

    const/4 v7, 0x0

    move-object v1, v8

    move-object v3, p2

    invoke-direct/range {v1 .. v7}, Lcom/miui/packageInstaller/ui/listcomponets/k;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/ApkInfo;Ll6/c;Lm6/b;ILm8/g;)V

    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance p2, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeNetworkError;

    invoke-virtual {p0}, Lb6/r;->u()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lb6/r;->t()Ll6/c;

    move-result-object v2

    invoke-direct {p2, v1, v2}, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeNetworkError;-><init>(Landroid/content/Context;Ll6/c;)V

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance p2, Lcom/miui/packageInstaller/ui/listcomponets/FootViewObject;

    invoke-virtual {p0}, Lb6/r;->u()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p2, v1}, Lcom/miui/packageInstaller/ui/listcomponets/FootViewObject;-><init>(Landroid/content/Context;)V

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object v1

    new-instance v2, Lb6/f;

    invoke-direct {v2, p1, v0, p2}, Lb6/f;-><init>(Lb6/r$a;Ljava/util/List;Ljava/util/ArrayList;)V

    invoke-virtual {v1, v2}, Lf6/z;->e(Ljava/lang/Runnable;)V

    return-void
.end method

.method public r(Lcom/miui/packageInstaller/model/CloudParams;Lb6/r$a;Lcom/miui/packageInstaller/model/ApkInfo;)V
    .locals 9

    const-string p1, "callBack"

    invoke-static {p2, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "apkInfo"

    invoke-static {p3, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v8, Lcom/miui/packageInstaller/ui/listcomponets/k;

    invoke-virtual {p0}, Lb6/r;->u()Landroid/content/Context;

    move-result-object v2

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xc

    const/4 v7, 0x0

    move-object v1, v8

    move-object v3, p3

    invoke-direct/range {v1 .. v7}, Lcom/miui/packageInstaller/ui/listcomponets/k;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/ApkInfo;Ll6/c;Lm6/b;ILm8/g;)V

    invoke-interface {p1, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance p3, Lcom/miui/packageInstaller/ui/listcomponets/FootViewObject;

    invoke-virtual {p0}, Lb6/r;->u()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p3, v1}, Lcom/miui/packageInstaller/ui/listcomponets/FootViewObject;-><init>(Landroid/content/Context;)V

    invoke-interface {p1, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object p3

    new-instance v1, Lb6/g;

    invoke-direct {v1, p2, p1, v0}, Lb6/g;-><init>(Lb6/r$a;Ljava/util/List;Ljava/util/List;)V

    invoke-virtual {p3, v1}, Lf6/z;->e(Ljava/lang/Runnable;)V

    return-void
.end method
