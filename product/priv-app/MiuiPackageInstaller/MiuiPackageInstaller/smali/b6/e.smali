.class public final Lb6/e;
.super Lb6/r;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ll6/c;)V
    .locals 1

    const-string v0, "mContext"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mActionDelegateProvider"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2}, Lb6/r;-><init>(Landroid/content/Context;Ll6/c;)V

    return-void
.end method

.method private final A(Lcom/miui/packageInstaller/model/CloudParams;Lcom/miui/packageInstaller/model/ApkInfo;Lb6/r$a;)V
    .locals 18

    move-object/from16 v8, p1

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual/range {p0 .. p2}, Lb6/r;->g(Lcom/miui/packageInstaller/model/CloudParams;Lcom/miui/packageInstaller/model/ApkInfo;)Ljava/util/List;

    move-result-object v10

    new-instance v11, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject;

    invoke-virtual/range {p0 .. p0}, Lb6/r;->u()Landroid/content/Context;

    move-result-object v1

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x18

    const/4 v7, 0x0

    move-object v0, v11

    move-object/from16 v2, p2

    move-object/from16 v3, p1

    invoke-direct/range {v0 .. v7}, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/ApkInfo;Lcom/miui/packageInstaller/model/CloudParams;Ll6/c;Lm6/b;ILm8/g;)V

    invoke-interface {v9, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, v8, Lcom/miui/packageInstaller/model/CloudParams;->bit64Tip:Lcom/miui/packageInstaller/model/BitTip;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/BitTip;->getText()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    move v0, v2

    :goto_0
    if-ne v0, v1, :cond_1

    goto :goto_1

    :cond_1
    move v1, v2

    :goto_1
    if-eqz v1, :cond_2

    new-instance v0, Lcom/miui/packageInstaller/ui/listcomponets/CpuCompatibleViewObject;

    invoke-virtual/range {p0 .. p0}, Lb6/r;->u()Landroid/content/Context;

    move-result-object v12

    iget-object v13, v8, Lcom/miui/packageInstaller/model/CloudParams;->bit64Tip:Lcom/miui/packageInstaller/model/BitTip;

    const-string v1, "rules.bit64Tip"

    invoke-static {v13, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0xc

    const/16 v17, 0x0

    move-object v11, v0

    invoke-direct/range {v11 .. v17}, Lcom/miui/packageInstaller/ui/listcomponets/CpuCompatibleViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/BitTip;Ll6/c;Lm6/b;ILm8/g;)V

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    iget-object v0, v8, Lcom/miui/packageInstaller/model/CloudParams;->secureWarningTip:Lcom/miui/packageInstaller/model/WarningCardInfo;

    const/4 v1, 0x0

    if-eqz v0, :cond_3

    iget-object v0, v0, Lcom/miui/packageInstaller/model/WarningCardInfo;->title:Ljava/lang/String;

    goto :goto_2

    :cond_3
    move-object v0, v1

    :goto_2
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, v8, Lcom/miui/packageInstaller/model/CloudParams;->secureWarningTip:Lcom/miui/packageInstaller/model/WarningCardInfo;

    if-eqz v0, :cond_4

    iget-object v1, v0, Lcom/miui/packageInstaller/model/WarningCardInfo;->text:Ljava/lang/String;

    :cond_4
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    new-instance v7, Lcom/miui/packageInstaller/ui/listcomponets/WarningInfoViewObject;

    invoke-virtual/range {p0 .. p0}, Lb6/r;->u()Landroid/content/Context;

    move-result-object v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xc

    const/4 v6, 0x0

    move-object v0, v7

    move-object/from16 v2, p1

    invoke-direct/range {v0 .. v6}, Lcom/miui/packageInstaller/ui/listcomponets/WarningInfoViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/CloudParams;Ll6/c;Lm6/b;ILm8/g;)V

    invoke-interface {v9, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_5
    iget-boolean v0, v8, Lcom/miui/packageInstaller/model/CloudParams;->showSafeModeTip:Z

    if-eqz v0, :cond_7

    iget-boolean v0, v8, Lcom/miui/packageInstaller/model/CloudParams;->seniorsApk:Z

    if-eqz v0, :cond_6

    new-instance v7, Lcom/miui/packageInstaller/ui/listcomponets/PureModeElderTipViewObject;

    invoke-virtual/range {p0 .. p0}, Lb6/r;->u()Landroid/content/Context;

    move-result-object v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xc

    const/4 v6, 0x0

    move-object v0, v7

    move-object/from16 v2, p1

    invoke-direct/range {v0 .. v6}, Lcom/miui/packageInstaller/ui/listcomponets/PureModeElderTipViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/CloudParams;Ll6/c;Lm6/b;ILm8/g;)V

    goto :goto_3

    :cond_6
    new-instance v7, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject;

    invoke-virtual/range {p0 .. p0}, Lb6/r;->u()Landroid/content/Context;

    move-result-object v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xc

    const/4 v6, 0x0

    move-object v0, v7

    move-object/from16 v2, p1

    invoke-direct/range {v0 .. v6}, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/CloudParams;Ll6/c;Lm6/b;ILm8/g;)V

    :goto_3
    invoke-interface {v9, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_7
    new-instance v0, Lcom/miui/packageInstaller/ui/listcomponets/VirusInfoViewObject;

    invoke-virtual/range {p0 .. p0}, Lb6/r;->u()Landroid/content/Context;

    move-result-object v12

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0xe

    const/16 v17, 0x0

    move-object v11, v0

    invoke-direct/range {v11 .. v17}, Lcom/miui/packageInstaller/ui/listcomponets/VirusInfoViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/Virus;Ll6/c;Lm6/b;ILm8/g;)V

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    if-eqz v10, :cond_8

    invoke-interface {v9, v10}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_4

    :cond_8
    new-instance v7, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoPicViewObject;

    invoke-virtual/range {p0 .. p0}, Lb6/r;->u()Landroid/content/Context;

    move-result-object v1

    invoke-virtual/range {p0 .. p0}, Lb6/r;->t()Ll6/c;

    move-result-object v3

    const/4 v4, 0x0

    const/16 v5, 0x8

    const/4 v6, 0x0

    move-object v0, v7

    move-object/from16 v2, p1

    invoke-direct/range {v0 .. v6}, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoPicViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/CloudParams;Ll6/c;Lm6/b;ILm8/g;)V

    invoke-interface {v9, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v7, Lcom/miui/packageInstaller/ui/listcomponets/AppDesViewObject;

    invoke-virtual/range {p0 .. p0}, Lb6/r;->u()Landroid/content/Context;

    move-result-object v1

    invoke-virtual/range {p0 .. p0}, Lb6/r;->t()Ll6/c;

    move-result-object v3

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/miui/packageInstaller/ui/listcomponets/AppDesViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/CloudParams;Ll6/c;Lm6/b;ILm8/g;)V

    invoke-interface {v9, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_4
    new-instance v0, Lcom/miui/packageInstaller/ui/listcomponets/FootViewObject;

    invoke-virtual/range {p0 .. p0}, Lb6/r;->u()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/miui/packageInstaller/ui/listcomponets/FootViewObject;-><init>(Landroid/content/Context;)V

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object v0

    new-instance v1, Lb6/c;

    move-object/from16 v2, p3

    invoke-direct {v1, v2, v9}, Lb6/c;-><init>(Lb6/r$a;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Lf6/z;->e(Ljava/lang/Runnable;)V

    return-void
.end method

.method private static final B(Lb6/r$a;Ljava/util/List;)V
    .locals 1

    const-string v0, "$callBack"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$layout"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-interface {p0, p1, v0}, Lb6/r$a;->a(Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method public static synthetic w(Lb6/r$a;Ljava/util/List;Ljava/util/List;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lb6/e;->z(Lb6/r$a;Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method public static synthetic x(Lb6/r$a;Ljava/util/List;)V
    .locals 0

    invoke-static {p0, p1}, Lb6/e;->B(Lb6/r$a;Ljava/util/List;)V

    return-void
.end method

.method private final y(Lcom/miui/packageInstaller/model/CloudParams;Lcom/miui/packageInstaller/model/ApkInfo;Lb6/r$a;)V
    .locals 21

    move-object/from16 v7, p1

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual/range {p0 .. p2}, Lb6/r;->g(Lcom/miui/packageInstaller/model/CloudParams;Lcom/miui/packageInstaller/model/ApkInfo;)Ljava/util/List;

    move-result-object v9

    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, v7, Lcom/miui/packageInstaller/model/CloudParams;->secureWarningTip:Lcom/miui/packageInstaller/model/WarningCardInfo;

    const/4 v11, 0x0

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/miui/packageInstaller/model/WarningCardInfo;->title:Ljava/lang/String;

    goto :goto_0

    :cond_0
    move-object v0, v11

    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, v7, Lcom/miui/packageInstaller/model/CloudParams;->secureWarningTip:Lcom/miui/packageInstaller/model/WarningCardInfo;

    if-eqz v0, :cond_1

    iget-object v0, v0, Lcom/miui/packageInstaller/model/WarningCardInfo;->text:Ljava/lang/String;

    goto :goto_1

    :cond_1
    move-object v0, v11

    :goto_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    new-instance v12, Lcom/miui/packageInstaller/ui/listcomponets/WarningInfoViewObject;

    invoke-virtual/range {p0 .. p0}, Lb6/r;->u()Landroid/content/Context;

    move-result-object v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xc

    const/4 v6, 0x0

    move-object v0, v12

    move-object/from16 v2, p1

    invoke-direct/range {v0 .. v6}, Lcom/miui/packageInstaller/ui/listcomponets/WarningInfoViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/CloudParams;Ll6/c;Lm6/b;ILm8/g;)V

    goto :goto_2

    :cond_2
    move-object v12, v11

    :goto_2
    iget-boolean v0, v7, Lcom/miui/packageInstaller/model/CloudParams;->showSafeModeTip:Z

    if-eqz v0, :cond_4

    iget-boolean v0, v7, Lcom/miui/packageInstaller/model/CloudParams;->seniorsApk:Z

    if-eqz v0, :cond_3

    new-instance v11, Lcom/miui/packageInstaller/ui/listcomponets/PureModeElderTipViewObject;

    invoke-virtual/range {p0 .. p0}, Lb6/r;->u()Landroid/content/Context;

    move-result-object v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xc

    const/4 v6, 0x0

    move-object v0, v11

    move-object/from16 v2, p1

    invoke-direct/range {v0 .. v6}, Lcom/miui/packageInstaller/ui/listcomponets/PureModeElderTipViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/CloudParams;Ll6/c;Lm6/b;ILm8/g;)V

    goto :goto_3

    :cond_3
    new-instance v11, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject;

    invoke-virtual/range {p0 .. p0}, Lb6/r;->u()Landroid/content/Context;

    move-result-object v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xc

    const/4 v6, 0x0

    move-object v0, v11

    move-object/from16 v2, p1

    invoke-direct/range {v0 .. v6}, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/CloudParams;Ll6/c;Lm6/b;ILm8/g;)V

    :cond_4
    :goto_3
    new-instance v13, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoViewObject;

    invoke-virtual/range {p0 .. p0}, Lb6/r;->u()Landroid/content/Context;

    move-result-object v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xc

    const/4 v6, 0x0

    move-object v0, v13

    move-object/from16 v2, p2

    invoke-direct/range {v0 .. v6}, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/ApkInfo;Ll6/c;Lm6/b;ILm8/g;)V

    invoke-interface {v8, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/miui/packageInstaller/ui/listcomponets/AppPermissionsInfoViewObject;

    invoke-virtual/range {p0 .. p0}, Lb6/r;->u()Landroid/content/Context;

    move-result-object v15

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0xc

    const/16 v20, 0x0

    move-object v14, v0

    move-object/from16 v16, p2

    invoke-direct/range {v14 .. v20}, Lcom/miui/packageInstaller/ui/listcomponets/AppPermissionsInfoViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/ApkInfo;Ll6/c;Lm6/b;ILm8/g;)V

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, v7, Lcom/miui/packageInstaller/model/CloudParams;->bit64Tip:Lcom/miui/packageInstaller/model/BitTip;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/BitTip;->getText()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_5

    move v0, v1

    goto :goto_4

    :cond_5
    move v0, v2

    :goto_4
    if-ne v0, v1, :cond_6

    goto :goto_5

    :cond_6
    move v1, v2

    :goto_5
    if-eqz v1, :cond_7

    new-instance v0, Lcom/miui/packageInstaller/ui/listcomponets/CpuCompatibleViewObject;

    invoke-virtual/range {p0 .. p0}, Lb6/r;->u()Landroid/content/Context;

    move-result-object v14

    iget-object v15, v7, Lcom/miui/packageInstaller/model/CloudParams;->bit64Tip:Lcom/miui/packageInstaller/model/BitTip;

    const-string v1, "rules.bit64Tip"

    invoke-static {v15, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0xc

    const/16 v19, 0x0

    move-object v13, v0

    invoke-direct/range {v13 .. v19}, Lcom/miui/packageInstaller/ui/listcomponets/CpuCompatibleViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/BitTip;Ll6/c;Lm6/b;ILm8/g;)V

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_7
    if-eqz v12, :cond_8

    invoke-interface {v8, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_8
    if-eqz v11, :cond_9

    invoke-interface {v8, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_9
    new-instance v0, Lcom/miui/packageInstaller/ui/listcomponets/VirusInfoViewObject;

    invoke-virtual/range {p0 .. p0}, Lb6/r;->u()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xe

    const/4 v7, 0x0

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/miui/packageInstaller/ui/listcomponets/VirusInfoViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/Virus;Ll6/c;Lm6/b;ILm8/g;)V

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    if-eqz v9, :cond_a

    invoke-interface {v8, v9}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_a
    new-instance v0, Lcom/miui/packageInstaller/ui/listcomponets/FootViewObject;

    invoke-virtual/range {p0 .. p0}, Lb6/r;->u()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/miui/packageInstaller/ui/listcomponets/FootViewObject;-><init>(Landroid/content/Context;)V

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object v0

    new-instance v1, Lb6/d;

    move-object/from16 v2, p3

    invoke-direct {v1, v2, v8, v10}, Lb6/d;-><init>(Lb6/r$a;Ljava/util/List;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Lf6/z;->e(Ljava/lang/Runnable;)V

    return-void
.end method

.method private static final z(Lb6/r$a;Ljava/util/List;Ljava/util/List;)V
    .locals 1

    const-string v0, "$callBack"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$layout"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$bottomLayout"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p0, p1, p2}, Lb6/r$a;->a(Ljava/util/List;Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method public h(Lcom/miui/packageInstaller/model/CloudParams;Lcom/miui/packageInstaller/model/ApkInfo;Lb6/r$a;)V
    .locals 2

    const-string v0, "rules"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "apkInfo"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "callBack"

    invoke-static {p3, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lb6/r;->u()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    invoke-direct {p0, p1, p2, p3}, Lb6/e;->y(Lcom/miui/packageInstaller/model/CloudParams;Lcom/miui/packageInstaller/model/ApkInfo;Lb6/r$a;)V

    goto :goto_0

    :cond_0
    invoke-super {p0, p1, p2, p3}, Lb6/r;->h(Lcom/miui/packageInstaller/model/CloudParams;Lcom/miui/packageInstaller/model/ApkInfo;Lb6/r$a;)V

    :goto_0
    return-void
.end method

.method public o(Lcom/miui/packageInstaller/model/CloudParams;Lcom/miui/packageInstaller/model/ApkInfo;Lb6/r$a;)V
    .locals 2

    const-string v0, "rules"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "apkInfo"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "callBack"

    invoke-static {p3, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lb6/r;->u()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    invoke-direct {p0, p1, p2, p3}, Lb6/e;->A(Lcom/miui/packageInstaller/model/CloudParams;Lcom/miui/packageInstaller/model/ApkInfo;Lb6/r$a;)V

    goto :goto_0

    :cond_0
    invoke-super {p0, p1, p2, p3}, Lb6/r;->o(Lcom/miui/packageInstaller/model/CloudParams;Lcom/miui/packageInstaller/model/ApkInfo;Lb6/r$a;)V

    :goto_0
    return-void
.end method
