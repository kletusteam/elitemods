.class public Lb6/r;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lb6/r$a;
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ll6/c;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ll6/c;)V
    .locals 1

    const-string v0, "mContext"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mActionDelegateProvider"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lb6/r;->a:Landroid/content/Context;

    iput-object p2, p0, Lb6/r;->b:Ll6/c;

    return-void
.end method

.method public static synthetic a(Lb6/r$a;Ljava/util/List;Ljava/util/List;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lb6/r;->p(Lb6/r$a;Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method public static synthetic b(Lb6/r$a;Ljava/util/List;Ljava/util/List;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lb6/r;->s(Lb6/r$a;Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method public static synthetic c(Lb6/r$a;Ljava/util/List;Ljava/util/List;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lb6/r;->i(Lb6/r$a;Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method public static synthetic d(Lb6/r$a;Ljava/util/List;Ljava/util/List;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lb6/r;->m(Lb6/r$a;Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method public static synthetic e(Lb6/r$a;Ljava/util/List;Ljava/util/List;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lb6/r;->k(Lb6/r$a;Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method private static final i(Lb6/r$a;Ljava/util/List;Ljava/util/List;)V
    .locals 1

    const-string v0, "$callBack"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$layout"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$bottomLayout"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p0, p1, p2}, Lb6/r$a;->a(Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method private static final k(Lb6/r$a;Ljava/util/List;Ljava/util/List;)V
    .locals 1

    const-string v0, "$callBack"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$layout"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$bottomLayout"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p0, p1, p2}, Lb6/r$a;->a(Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method private static final m(Lb6/r$a;Ljava/util/List;Ljava/util/List;)V
    .locals 1

    const-string v0, "$callBack"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$layout"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$bottomLayout"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p0, p1, p2}, Lb6/r$a;->a(Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method private static final p(Lb6/r$a;Ljava/util/List;Ljava/util/List;)V
    .locals 1

    const-string v0, "$callBack"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$layout"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$bottomLayout"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p0, p1, p2}, Lb6/r$a;->a(Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method private static final s(Lb6/r$a;Ljava/util/List;Ljava/util/List;)V
    .locals 1

    const-string v0, "$callBack"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$layout"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$bottomLayout"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p0, p1, p2}, Lb6/r$a;->a(Ljava/util/List;Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method public final f(Lb6/r$a;)V
    .locals 4

    const-string v0, "callBack"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Lcom/miui/packageInstaller/ui/listcomponets/h;

    iget-object v2, p0, Lb6/r;->a:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/miui/packageInstaller/ui/listcomponets/h;-><init>(Landroid/content/Context;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/miui/packageInstaller/ui/listcomponets/d0;

    iget-object v2, p0, Lb6/r;->a:Landroid/content/Context;

    iget-object v3, p0, Lb6/r;->b:Ll6/c;

    invoke-direct {v1, v2, v3}, Lcom/miui/packageInstaller/ui/listcomponets/d0;-><init>(Landroid/content/Context;Ll6/c;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/miui/packageInstaller/ui/listcomponets/FootViewObject;

    iget-object v2, p0, Lb6/r;->a:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/miui/packageInstaller/ui/listcomponets/FootViewObject;-><init>(Landroid/content/Context;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object v1

    new-instance v2, Lb6/r$b;

    invoke-direct {v2, p1, v0}, Lb6/r$b;-><init>(Lb6/r$a;Ljava/util/List;)V

    invoke-virtual {v1, v2}, Lf6/z;->e(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final g(Lcom/miui/packageInstaller/model/CloudParams;Lcom/miui/packageInstaller/model/ApkInfo;)Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/miui/packageInstaller/model/CloudParams;",
            "Lcom/miui/packageInstaller/model/ApkInfo;",
            ")",
            "Ljava/util/List<",
            "Lm6/a<",
            "*>;>;"
        }
    .end annotation

    const-string v0, "apkInfo"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lb6/r;->v(Lcom/miui/packageInstaller/model/CloudParams;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Ln5/e;->a:Ln5/e;

    const-string v1, "08-1"

    invoke-virtual {v0, v1}, Ln5/e;->c(Ljava/lang/String;)Lcom/miui/packageInstaller/model/AdModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/AdModel;->getData()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    new-instance v6, Lcom/miui/packageInstaller/model/AdTitleModel;

    invoke-direct {v6}, Lcom/miui/packageInstaller/model/AdTitleModel;-><init>()V

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/packageInstaller/model/AdModel$DesData;

    invoke-virtual {v2}, Lcom/miui/packageInstaller/model/AdModel$DesData;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, Lcom/miui/packageInstaller/model/AdTitleModel;->setTitle(Ljava/lang/String;)V

    new-instance v11, Ljava/util/ArrayList;

    const/4 v2, 0x2

    invoke-direct {v11, v2}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v2, Lcom/miui/packageInstaller/ui/listcomponets/AdTitleViewObject;

    iget-object v5, p0, Lb6/r;->a:Landroid/content/Context;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0xc

    const/4 v10, 0x0

    move-object v4, v2

    invoke-direct/range {v4 .. v10}, Lcom/miui/packageInstaller/ui/listcomponets/AdTitleViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/AdTitleModel;Ll6/c;Lm6/b;ILm8/g;)V

    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v9, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;

    iget-object v2, p0, Lb6/r;->a:Landroid/content/Context;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/miui/packageInstaller/model/AdModel$DesData;

    iget-object v5, p0, Lb6/r;->b:Ll6/c;

    const/4 v6, 0x0

    const/16 v7, 0x10

    move-object v1, v9

    move-object v3, p2

    invoke-direct/range {v1 .. v8}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/ApkInfo;Lcom/miui/packageInstaller/model/AdModel$DesData;Ll6/c;Lm6/b;ILm8/g;)V

    invoke-virtual {v11, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object v11

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public h(Lcom/miui/packageInstaller/model/CloudParams;Lcom/miui/packageInstaller/model/ApkInfo;Lb6/r$a;)V
    .locals 24

    move-object/from16 v0, p0

    move-object/from16 v8, p1

    move-object/from16 v9, p3

    const-string v1, "rules"

    invoke-static {v8, v1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "apk"

    move-object/from16 v15, p2

    invoke-static {v15, v1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "callBack"

    invoke-static {v9, v1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual/range {p0 .. p2}, Lb6/r;->g(Lcom/miui/packageInstaller/model/CloudParams;Lcom/miui/packageInstaller/model/ApkInfo;)Ljava/util/List;

    move-result-object v13

    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, v8, Lcom/miui/packageInstaller/model/CloudParams;->secureWarningTip:Lcom/miui/packageInstaller/model/WarningCardInfo;

    const/4 v10, 0x0

    if-eqz v1, :cond_0

    iget-object v1, v1, Lcom/miui/packageInstaller/model/WarningCardInfo;->title:Ljava/lang/String;

    goto :goto_0

    :cond_0
    move-object v1, v10

    :goto_0
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, v8, Lcom/miui/packageInstaller/model/CloudParams;->secureWarningTip:Lcom/miui/packageInstaller/model/WarningCardInfo;

    if-eqz v1, :cond_1

    iget-object v1, v1, Lcom/miui/packageInstaller/model/WarningCardInfo;->text:Ljava/lang/String;

    goto :goto_1

    :cond_1
    move-object v1, v10

    :goto_1
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    new-instance v11, Lcom/miui/packageInstaller/ui/listcomponets/WarningInfoViewObject;

    iget-object v2, v0, Lb6/r;->a:Landroid/content/Context;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xc

    const/4 v7, 0x0

    move-object v1, v11

    move-object/from16 v3, p1

    invoke-direct/range {v1 .. v7}, Lcom/miui/packageInstaller/ui/listcomponets/WarningInfoViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/CloudParams;Ll6/c;Lm6/b;ILm8/g;)V

    goto :goto_2

    :cond_2
    move-object v11, v10

    :goto_2
    iget-boolean v1, v8, Lcom/miui/packageInstaller/model/CloudParams;->showSafeModeTip:Z

    if-eqz v1, :cond_4

    iget-boolean v1, v8, Lcom/miui/packageInstaller/model/CloudParams;->seniorsApk:Z

    if-eqz v1, :cond_3

    new-instance v10, Lcom/miui/packageInstaller/ui/listcomponets/PureModeElderTipViewObject;

    iget-object v2, v0, Lb6/r;->a:Landroid/content/Context;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xc

    const/4 v7, 0x0

    move-object v1, v10

    move-object/from16 v3, p1

    invoke-direct/range {v1 .. v7}, Lcom/miui/packageInstaller/ui/listcomponets/PureModeElderTipViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/CloudParams;Ll6/c;Lm6/b;ILm8/g;)V

    goto :goto_3

    :cond_3
    new-instance v10, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject;

    iget-object v2, v0, Lb6/r;->a:Landroid/content/Context;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xc

    const/4 v7, 0x0

    move-object v1, v10

    move-object/from16 v3, p1

    invoke-direct/range {v1 .. v7}, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/CloudParams;Ll6/c;Lm6/b;ILm8/g;)V

    :cond_4
    :goto_3
    move-object v1, v10

    new-instance v2, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoViewObject;

    iget-object v3, v0, Lb6/r;->a:Landroid/content/Context;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xc

    const/16 v16, 0x0

    move-object v10, v2

    move-object v7, v11

    move-object v11, v3

    move-object v3, v12

    move-object/from16 v12, p2

    move-object/from16 v17, v13

    move-object v13, v4

    move-object v4, v14

    move-object v14, v5

    move v15, v6

    invoke-direct/range {v10 .. v16}, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/ApkInfo;Ll6/c;Lm6/b;ILm8/g;)V

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v2, Lcom/miui/packageInstaller/ui/listcomponets/AppPermissionsInfoViewObject;

    iget-object v11, v0, Lb6/r;->a:Landroid/content/Context;

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0xc

    move-object v10, v2

    invoke-direct/range {v10 .. v16}, Lcom/miui/packageInstaller/ui/listcomponets/AppPermissionsInfoViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/ApkInfo;Ll6/c;Lm6/b;ILm8/g;)V

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v2, v8, Lcom/miui/packageInstaller/model/CloudParams;->bit64Tip:Lcom/miui/packageInstaller/model/BitTip;

    const/4 v5, 0x1

    const/4 v6, 0x0

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Lcom/miui/packageInstaller/model/BitTip;->getText()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_6

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-lez v2, :cond_5

    move v2, v5

    goto :goto_4

    :cond_5
    move v2, v6

    :goto_4
    if-ne v2, v5, :cond_6

    goto :goto_5

    :cond_6
    move v5, v6

    :goto_5
    if-eqz v5, :cond_7

    new-instance v2, Lcom/miui/packageInstaller/ui/listcomponets/CpuCompatibleViewObject;

    iget-object v11, v0, Lb6/r;->a:Landroid/content/Context;

    iget-object v12, v8, Lcom/miui/packageInstaller/model/CloudParams;->bit64Tip:Lcom/miui/packageInstaller/model/BitTip;

    const-string v5, "rules.bit64Tip"

    invoke-static {v12, v5}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0xc

    const/16 v16, 0x0

    move-object v10, v2

    invoke-direct/range {v10 .. v16}, Lcom/miui/packageInstaller/ui/listcomponets/CpuCompatibleViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/BitTip;Ll6/c;Lm6/b;ILm8/g;)V

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_7
    move-object/from16 v2, v17

    if-eqz v2, :cond_9

    invoke-interface {v4, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    if-eqz v7, :cond_8

    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_8
    new-instance v2, Lcom/miui/packageInstaller/ui/listcomponets/VirusInfoViewObject;

    iget-object v11, v0, Lb6/r;->a:Landroid/content/Context;

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0xe

    const/16 v16, 0x0

    move-object v10, v2

    invoke-direct/range {v10 .. v16}, Lcom/miui/packageInstaller/ui/listcomponets/VirusInfoViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/Virus;Ll6/c;Lm6/b;ILm8/g;)V

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    if-eqz v1, :cond_b

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_6

    :cond_9
    if-eqz v7, :cond_a

    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_a
    new-instance v2, Lcom/miui/packageInstaller/ui/listcomponets/VirusInfoViewObject;

    iget-object v5, v0, Lb6/r;->a:Landroid/content/Context;

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0xe

    const/16 v23, 0x0

    move-object/from16 v17, v2

    move-object/from16 v18, v5

    invoke-direct/range {v17 .. v23}, Lcom/miui/packageInstaller/ui/listcomponets/VirusInfoViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/Virus;Ll6/c;Lm6/b;ILm8/g;)V

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    if-eqz v1, :cond_b

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_b
    :goto_6
    new-instance v1, Lcom/miui/packageInstaller/ui/listcomponets/FootViewObject;

    iget-object v2, v0, Lb6/r;->a:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/miui/packageInstaller/ui/listcomponets/FootViewObject;-><init>(Landroid/content/Context;)V

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object v1

    new-instance v2, Lb6/o;

    invoke-direct {v2, v9, v4, v3}, Lb6/o;-><init>(Lb6/r$a;Ljava/util/List;Ljava/util/List;)V

    invoke-virtual {v1, v2}, Lf6/z;->e(Ljava/lang/Runnable;)V

    return-void
.end method

.method public j(Lcom/miui/packageInstaller/model/CloudParams;Lcom/miui/packageInstaller/model/ApkInfo;Lb6/r$a;)V
    .locals 16

    move-object/from16 v0, p0

    move-object/from16 v3, p1

    move-object/from16 v8, p3

    const-string v1, "rules"

    invoke-static {v3, v1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "apk"

    move-object/from16 v15, p2

    invoke-static {v15, v1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "callBack"

    invoke-static {v8, v1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual/range {p0 .. p2}, Lb6/r;->g(Lcom/miui/packageInstaller/model/CloudParams;Lcom/miui/packageInstaller/model/ApkInfo;)Ljava/util/List;

    move-result-object v13

    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, v3, Lcom/miui/packageInstaller/model/CloudParams;->secureWarningTip:Lcom/miui/packageInstaller/model/WarningCardInfo;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    iget-object v1, v1, Lcom/miui/packageInstaller/model/WarningCardInfo;->title:Ljava/lang/String;

    goto :goto_0

    :cond_0
    move-object v1, v2

    :goto_0
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, v3, Lcom/miui/packageInstaller/model/CloudParams;->secureWarningTip:Lcom/miui/packageInstaller/model/WarningCardInfo;

    if-eqz v1, :cond_1

    iget-object v1, v1, Lcom/miui/packageInstaller/model/WarningCardInfo;->text:Ljava/lang/String;

    goto :goto_1

    :cond_1
    move-object v1, v2

    :goto_1
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    new-instance v9, Lcom/miui/packageInstaller/ui/listcomponets/WarningInfoViewObject;

    iget-object v2, v0, Lb6/r;->a:Landroid/content/Context;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xc

    const/4 v7, 0x0

    move-object v1, v9

    move-object/from16 v3, p1

    invoke-direct/range {v1 .. v7}, Lcom/miui/packageInstaller/ui/listcomponets/WarningInfoViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/CloudParams;Ll6/c;Lm6/b;ILm8/g;)V

    move-object v2, v9

    :cond_2
    new-instance v1, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoViewObject;

    iget-object v10, v0, Lb6/r;->a:Landroid/content/Context;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xc

    const/4 v6, 0x0

    move-object v9, v1

    move-object/from16 v11, p2

    move-object v7, v12

    move-object v12, v3

    move-object v3, v13

    move-object v13, v4

    move-object v4, v14

    move v14, v5

    move-object v15, v6

    invoke-direct/range {v9 .. v15}, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/ApkInfo;Ll6/c;Lm6/b;ILm8/g;)V

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/miui/packageInstaller/ui/listcomponets/AppPermissionsInfoViewObject;

    iget-object v10, v0, Lb6/r;->a:Landroid/content/Context;

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/16 v14, 0xc

    const/4 v15, 0x0

    move-object v9, v1

    invoke-direct/range {v9 .. v15}, Lcom/miui/packageInstaller/ui/listcomponets/AppPermissionsInfoViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/ApkInfo;Ll6/c;Lm6/b;ILm8/g;)V

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    if-eqz v3, :cond_3

    invoke-interface {v4, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_3
    if-eqz v2, :cond_4

    invoke-interface {v7, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    new-instance v1, Lcom/miui/packageInstaller/ui/listcomponets/BundleAppViewObject;

    iget-object v10, v0, Lb6/r;->a:Landroid/content/Context;

    iget-object v12, v0, Lb6/r;->b:Ll6/c;

    const/4 v13, 0x0

    const/16 v14, 0x8

    const/4 v15, 0x0

    move-object v9, v1

    move-object/from16 v11, p2

    invoke-direct/range {v9 .. v15}, Lcom/miui/packageInstaller/ui/listcomponets/BundleAppViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/ApkInfo;Ll6/c;Lm6/b;ILm8/g;)V

    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/miui/packageInstaller/ui/listcomponets/FootViewObject;

    iget-object v2, v0, Lb6/r;->a:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/miui/packageInstaller/ui/listcomponets/FootViewObject;-><init>(Landroid/content/Context;)V

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object v1

    new-instance v2, Lb6/q;

    invoke-direct {v2, v8, v4, v7}, Lb6/q;-><init>(Lb6/r$a;Ljava/util/List;Ljava/util/List;)V

    invoke-virtual {v1, v2}, Lf6/z;->e(Ljava/lang/Runnable;)V

    return-void
.end method

.method public l(Lcom/miui/packageInstaller/model/CloudParams;Lcom/miui/packageInstaller/model/ApkInfo;Lb6/r$a;)V
    .locals 16

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    const-string v2, "rules"

    move-object/from16 v11, p1

    invoke-static {v11, v2}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "apkInfo"

    move-object/from16 v12, p2

    invoke-static {v12, v2}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "callBack"

    invoke-static {v1, v2}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual/range {p0 .. p2}, Lb6/r;->g(Lcom/miui/packageInstaller/model/CloudParams;Lcom/miui/packageInstaller/model/ApkInfo;)Ljava/util/List;

    move-result-object v14

    new-instance v15, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject;

    iget-object v4, v0, Lb6/r;->a:Landroid/content/Context;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x18

    const/4 v10, 0x0

    move-object v3, v15

    move-object/from16 v5, p2

    move-object/from16 v6, p1

    invoke-direct/range {v3 .. v10}, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/ApkInfo;Lcom/miui/packageInstaller/model/CloudParams;Ll6/c;Lm6/b;ILm8/g;)V

    invoke-interface {v2, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    if-eqz v14, :cond_0

    invoke-interface {v2, v14}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/android/packageinstaller/utils/g;->v()Z

    move-result v3

    if-nez v3, :cond_1

    new-instance v10, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoPicViewObject;

    iget-object v4, v0, Lb6/r;->a:Landroid/content/Context;

    iget-object v6, v0, Lb6/r;->b:Ll6/c;

    const/4 v7, 0x0

    const/16 v8, 0x8

    const/4 v9, 0x0

    move-object v3, v10

    move-object/from16 v5, p1

    invoke-direct/range {v3 .. v9}, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoPicViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/CloudParams;Ll6/c;Lm6/b;ILm8/g;)V

    invoke-interface {v2, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v10, Lcom/miui/packageInstaller/ui/listcomponets/AppDesViewObject;

    iget-object v4, v0, Lb6/r;->a:Landroid/content/Context;

    iget-object v6, v0, Lb6/r;->b:Ll6/c;

    move-object v3, v10

    invoke-direct/range {v3 .. v9}, Lcom/miui/packageInstaller/ui/listcomponets/AppDesViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/CloudParams;Ll6/c;Lm6/b;ILm8/g;)V

    invoke-interface {v2, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    :goto_0
    new-instance v10, Lcom/miui/packageInstaller/ui/listcomponets/BundleAppViewObject;

    iget-object v4, v0, Lb6/r;->a:Landroid/content/Context;

    iget-object v6, v0, Lb6/r;->b:Ll6/c;

    const/4 v7, 0x0

    const/16 v8, 0x8

    const/4 v9, 0x0

    move-object v3, v10

    move-object/from16 v5, p2

    invoke-direct/range {v3 .. v9}, Lcom/miui/packageInstaller/ui/listcomponets/BundleAppViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/ApkInfo;Ll6/c;Lm6/b;ILm8/g;)V

    invoke-interface {v13, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v3, Lcom/miui/packageInstaller/ui/listcomponets/FootViewObject;

    iget-object v4, v0, Lb6/r;->a:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/miui/packageInstaller/ui/listcomponets/FootViewObject;-><init>(Landroid/content/Context;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object v3

    new-instance v4, Lb6/p;

    invoke-direct {v4, v1, v2, v13}, Lb6/p;-><init>(Lb6/r$a;Ljava/util/List;Ljava/util/List;)V

    invoke-virtual {v3, v4}, Lf6/z;->e(Ljava/lang/Runnable;)V

    return-void
.end method

.method public n(Lcom/miui/packageInstaller/model/CloudParams;Lcom/miui/packageInstaller/model/ApkInfo;Lb6/r$a;)V
    .locals 3

    const-string v0, "apkInfo"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "callBack"

    invoke-static {p3, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    if-nez p1, :cond_0

    invoke-virtual {p0, p3, p2}, Lb6/r;->q(Lb6/r$a;Lcom/miui/packageInstaller/model/ApkInfo;)V

    goto :goto_0

    :cond_0
    iget-object v0, p1, Lcom/miui/packageInstaller/model/CloudParams;->categoryAbbreviation:Ljava/lang/String;

    const-string v1, "500_error"

    invoke-static {v0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0, p3}, Lb6/r;->f(Lb6/r$a;)V

    goto :goto_0

    :cond_1
    iget-boolean v0, p1, Lcom/miui/packageInstaller/model/CloudParams;->useSystemAppRules:Z

    if-eqz v0, :cond_2

    invoke-virtual {p0, p1, p3, p2}, Lb6/r;->r(Lcom/miui/packageInstaller/model/CloudParams;Lb6/r$a;Lcom/miui/packageInstaller/model/ApkInfo;)V

    goto :goto_0

    :cond_2
    iget-boolean v0, p1, Lcom/miui/packageInstaller/model/CloudParams;->bundleApp:Z

    if-eqz v0, :cond_3

    iget-boolean v1, p1, Lcom/miui/packageInstaller/model/CloudParams;->storeListed:Z

    if-eqz v1, :cond_3

    iget-object v1, p1, Lcom/miui/packageInstaller/model/CloudParams;->appInfo:Lcom/miui/packageInstaller/model/MarketAppInfo;

    if-eqz v1, :cond_3

    invoke-virtual {p0, p1, p2, p3}, Lb6/r;->l(Lcom/miui/packageInstaller/model/CloudParams;Lcom/miui/packageInstaller/model/ApkInfo;Lb6/r$a;)V

    goto :goto_0

    :cond_3
    iget-boolean v1, p1, Lcom/miui/packageInstaller/model/CloudParams;->storeListed:Z

    if-eqz v1, :cond_4

    iget-object v2, p1, Lcom/miui/packageInstaller/model/CloudParams;->appInfo:Lcom/miui/packageInstaller/model/MarketAppInfo;

    if-eqz v2, :cond_4

    invoke-virtual {p0, p1, p2, p3}, Lb6/r;->o(Lcom/miui/packageInstaller/model/CloudParams;Lcom/miui/packageInstaller/model/ApkInfo;Lb6/r$a;)V

    goto :goto_0

    :cond_4
    if-eqz v0, :cond_5

    if-nez v1, :cond_5

    invoke-virtual {p0, p1, p2, p3}, Lb6/r;->j(Lcom/miui/packageInstaller/model/CloudParams;Lcom/miui/packageInstaller/model/ApkInfo;Lb6/r$a;)V

    goto :goto_0

    :cond_5
    invoke-virtual {p0, p1, p2, p3}, Lb6/r;->h(Lcom/miui/packageInstaller/model/CloudParams;Lcom/miui/packageInstaller/model/ApkInfo;Lb6/r$a;)V

    :goto_0
    return-void
.end method

.method public o(Lcom/miui/packageInstaller/model/CloudParams;Lcom/miui/packageInstaller/model/ApkInfo;Lb6/r$a;)V
    .locals 21

    move-object/from16 v0, p0

    move-object/from16 v9, p1

    move-object/from16 v10, p3

    const-string v1, "rules"

    invoke-static {v9, v1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "apk"

    move-object/from16 v3, p2

    invoke-static {v3, v1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "callBack"

    invoke-static {v10, v1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual/range {p0 .. p2}, Lb6/r;->g(Lcom/miui/packageInstaller/model/CloudParams;Lcom/miui/packageInstaller/model/ApkInfo;)Ljava/util/List;

    move-result-object v13

    new-instance v14, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject;

    iget-object v2, v0, Lb6/r;->a:Landroid/content/Context;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x18

    const/4 v8, 0x0

    move-object v1, v14

    move-object/from16 v4, p1

    invoke-direct/range {v1 .. v8}, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/ApkInfo;Lcom/miui/packageInstaller/model/CloudParams;Ll6/c;Lm6/b;ILm8/g;)V

    invoke-interface {v11, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v1, v9, Lcom/miui/packageInstaller/model/CloudParams;->bit64Tip:Lcom/miui/packageInstaller/model/BitTip;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/BitTip;->getText()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-lez v1, :cond_0

    move v1, v2

    goto :goto_0

    :cond_0
    move v1, v3

    :goto_0
    if-ne v1, v2, :cond_1

    goto :goto_1

    :cond_1
    move v2, v3

    :goto_1
    if-eqz v2, :cond_2

    new-instance v1, Lcom/miui/packageInstaller/ui/listcomponets/CpuCompatibleViewObject;

    iget-object v15, v0, Lb6/r;->a:Landroid/content/Context;

    iget-object v2, v9, Lcom/miui/packageInstaller/model/CloudParams;->bit64Tip:Lcom/miui/packageInstaller/model/BitTip;

    const-string v3, "rules.bit64Tip"

    invoke-static {v2, v3}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0xc

    const/16 v20, 0x0

    move-object v14, v1

    move-object/from16 v16, v2

    invoke-direct/range {v14 .. v20}, Lcom/miui/packageInstaller/ui/listcomponets/CpuCompatibleViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/BitTip;Ll6/c;Lm6/b;ILm8/g;)V

    invoke-interface {v11, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    if-eqz v13, :cond_3

    invoke-interface {v11, v13}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    :cond_3
    invoke-static {}, Lcom/android/packageinstaller/utils/g;->v()Z

    move-result v1

    if-nez v1, :cond_4

    new-instance v8, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoPicViewObject;

    iget-object v2, v0, Lb6/r;->a:Landroid/content/Context;

    iget-object v4, v0, Lb6/r;->b:Ll6/c;

    const/4 v5, 0x0

    const/16 v6, 0x8

    const/4 v7, 0x0

    move-object v1, v8

    move-object/from16 v3, p1

    invoke-direct/range {v1 .. v7}, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoPicViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/CloudParams;Ll6/c;Lm6/b;ILm8/g;)V

    invoke-interface {v11, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v8, Lcom/miui/packageInstaller/ui/listcomponets/AppDesViewObject;

    iget-object v2, v0, Lb6/r;->a:Landroid/content/Context;

    iget-object v4, v0, Lb6/r;->b:Ll6/c;

    move-object v1, v8

    invoke-direct/range {v1 .. v7}, Lcom/miui/packageInstaller/ui/listcomponets/AppDesViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/CloudParams;Ll6/c;Lm6/b;ILm8/g;)V

    invoke-interface {v11, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    :goto_2
    iget-object v1, v9, Lcom/miui/packageInstaller/model/CloudParams;->secureWarningTip:Lcom/miui/packageInstaller/model/WarningCardInfo;

    const/4 v2, 0x0

    if-eqz v1, :cond_5

    iget-object v1, v1, Lcom/miui/packageInstaller/model/WarningCardInfo;->title:Ljava/lang/String;

    goto :goto_3

    :cond_5
    move-object v1, v2

    :goto_3
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_7

    iget-object v1, v9, Lcom/miui/packageInstaller/model/CloudParams;->secureWarningTip:Lcom/miui/packageInstaller/model/WarningCardInfo;

    if-eqz v1, :cond_6

    iget-object v2, v1, Lcom/miui/packageInstaller/model/WarningCardInfo;->text:Ljava/lang/String;

    :cond_6
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_7

    new-instance v8, Lcom/miui/packageInstaller/ui/listcomponets/WarningInfoViewObject;

    iget-object v2, v0, Lb6/r;->a:Landroid/content/Context;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xc

    const/4 v7, 0x0

    move-object v1, v8

    move-object/from16 v3, p1

    invoke-direct/range {v1 .. v7}, Lcom/miui/packageInstaller/ui/listcomponets/WarningInfoViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/CloudParams;Ll6/c;Lm6/b;ILm8/g;)V

    invoke-interface {v12, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_7
    new-instance v1, Lcom/miui/packageInstaller/ui/listcomponets/VirusInfoViewObject;

    iget-object v14, v0, Lb6/r;->a:Landroid/content/Context;

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0xe

    const/16 v19, 0x0

    move-object v13, v1

    invoke-direct/range {v13 .. v19}, Lcom/miui/packageInstaller/ui/listcomponets/VirusInfoViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/Virus;Ll6/c;Lm6/b;ILm8/g;)V

    invoke-interface {v12, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-boolean v1, v9, Lcom/miui/packageInstaller/model/CloudParams;->showSafeModeTip:Z

    if-eqz v1, :cond_9

    iget-boolean v1, v9, Lcom/miui/packageInstaller/model/CloudParams;->seniorsApk:Z

    if-eqz v1, :cond_8

    new-instance v8, Lcom/miui/packageInstaller/ui/listcomponets/PureModeElderTipViewObject;

    iget-object v2, v0, Lb6/r;->a:Landroid/content/Context;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xc

    const/4 v7, 0x0

    move-object v1, v8

    move-object/from16 v3, p1

    invoke-direct/range {v1 .. v7}, Lcom/miui/packageInstaller/ui/listcomponets/PureModeElderTipViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/CloudParams;Ll6/c;Lm6/b;ILm8/g;)V

    goto :goto_4

    :cond_8
    new-instance v8, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject;

    iget-object v2, v0, Lb6/r;->a:Landroid/content/Context;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xc

    const/4 v7, 0x0

    move-object v1, v8

    move-object/from16 v3, p1

    invoke-direct/range {v1 .. v7}, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/CloudParams;Ll6/c;Lm6/b;ILm8/g;)V

    :goto_4
    invoke-interface {v12, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_9
    new-instance v1, Lcom/miui/packageInstaller/ui/listcomponets/FootViewObject;

    iget-object v2, v0, Lb6/r;->a:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/miui/packageInstaller/ui/listcomponets/FootViewObject;-><init>(Landroid/content/Context;)V

    invoke-interface {v11, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object v1

    new-instance v2, Lb6/m;

    invoke-direct {v2, v10, v11, v12}, Lb6/m;-><init>(Lb6/r$a;Ljava/util/List;Ljava/util/List;)V

    invoke-virtual {v1, v2}, Lf6/z;->e(Ljava/lang/Runnable;)V

    return-void
.end method

.method public q(Lb6/r$a;Lcom/miui/packageInstaller/model/ApkInfo;)V
    .locals 11

    const-string v0, "callBack"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "apk"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v9, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoViewObject;

    iget-object v2, p0, Lb6/r;->a:Landroid/content/Context;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xc

    const/4 v7, 0x0

    move-object v1, v9

    move-object v3, p2

    invoke-direct/range {v1 .. v7}, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/ApkInfo;Ll6/c;Lm6/b;ILm8/g;)V

    invoke-interface {v0, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v9, Lcom/miui/packageInstaller/ui/listcomponets/AppPermissionsInfoViewObject;

    iget-object v2, p0, Lb6/r;->a:Landroid/content/Context;

    move-object v1, v9

    invoke-direct/range {v1 .. v7}, Lcom/miui/packageInstaller/ui/listcomponets/AppPermissionsInfoViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/ApkInfo;Ll6/c;Lm6/b;ILm8/g;)V

    invoke-interface {v0, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/miui/packageInstaller/ui/listcomponets/NetworkErrorObject;

    iget-object v2, p0, Lb6/r;->a:Landroid/content/Context;

    iget-object v3, p0, Lb6/r;->b:Ll6/c;

    invoke-direct {v1, v2, v3}, Lcom/miui/packageInstaller/ui/listcomponets/NetworkErrorObject;-><init>(Landroid/content/Context;Ll6/c;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/miui/packageInstaller/ui/listcomponets/VirusInfoViewObject;

    iget-object v5, p0, Lb6/r;->a:Landroid/content/Context;

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/16 v9, 0xe

    const/4 v10, 0x0

    move-object v4, v1

    invoke-direct/range {v4 .. v10}, Lcom/miui/packageInstaller/ui/listcomponets/VirusInfoViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/Virus;Ll6/c;Lm6/b;ILm8/g;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/miui/packageInstaller/ui/listcomponets/FootViewObject;

    iget-object v2, p0, Lb6/r;->a:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/miui/packageInstaller/ui/listcomponets/FootViewObject;-><init>(Landroid/content/Context;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object v1

    new-instance v2, Lb6/r$c;

    invoke-direct {v2, p1, v0}, Lb6/r$c;-><init>(Lb6/r$a;Ljava/util/List;)V

    invoke-virtual {v1, v2}, Lf6/z;->e(Ljava/lang/Runnable;)V

    return-void
.end method

.method public r(Lcom/miui/packageInstaller/model/CloudParams;Lb6/r$a;Lcom/miui/packageInstaller/model/ApkInfo;)V
    .locals 16

    move-object/from16 v0, p0

    move-object/from16 v3, p1

    move-object/from16 v8, p2

    move-object/from16 v1, p3

    const-string v2, "callBack"

    invoke-static {v8, v2}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "apkInfo"

    invoke-static {v1, v2}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0, v3, v1}, Lb6/r;->g(Lcom/miui/packageInstaller/model/CloudParams;Lcom/miui/packageInstaller/model/ApkInfo;)Ljava/util/List;

    move-result-object v5

    new-instance v2, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoViewObject;

    iget-object v10, v0, Lb6/r;->a:Landroid/content/Context;

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/16 v14, 0xc

    const/4 v15, 0x0

    move-object v9, v2

    move-object/from16 v11, p3

    invoke-direct/range {v9 .. v15}, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/ApkInfo;Ll6/c;Lm6/b;ILm8/g;)V

    invoke-interface {v7, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v2, Lcom/miui/packageInstaller/ui/listcomponets/AppPermissionsInfoViewObject;

    iget-object v10, v0, Lb6/r;->a:Landroid/content/Context;

    move-object v9, v2

    invoke-direct/range {v9 .. v15}, Lcom/miui/packageInstaller/ui/listcomponets/AppPermissionsInfoViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/ApkInfo;Ll6/c;Lm6/b;ILm8/g;)V

    invoke-interface {v7, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v3, :cond_0

    iget-boolean v4, v3, Lcom/miui/packageInstaller/model/CloudParams;->showSafeModeTip:Z

    if-ne v4, v1, :cond_0

    goto :goto_0

    :cond_0
    move v1, v2

    :goto_0
    if-eqz v1, :cond_2

    iget-boolean v1, v3, Lcom/miui/packageInstaller/model/CloudParams;->seniorsApk:Z

    if-eqz v1, :cond_1

    new-instance v9, Lcom/miui/packageInstaller/ui/listcomponets/PureModeElderTipViewObject;

    iget-object v2, v0, Lb6/r;->a:Landroid/content/Context;

    const/4 v4, 0x0

    const/4 v10, 0x0

    const/16 v11, 0xc

    const/4 v12, 0x0

    move-object v1, v9

    move-object/from16 v3, p1

    move-object v13, v5

    move-object v5, v10

    move-object v10, v6

    move v6, v11

    move-object v11, v7

    move-object v7, v12

    invoke-direct/range {v1 .. v7}, Lcom/miui/packageInstaller/ui/listcomponets/PureModeElderTipViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/CloudParams;Ll6/c;Lm6/b;ILm8/g;)V

    goto :goto_1

    :cond_1
    move-object v13, v5

    move-object v10, v6

    move-object v11, v7

    new-instance v9, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject;

    iget-object v2, v0, Lb6/r;->a:Landroid/content/Context;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xc

    const/4 v7, 0x0

    move-object v1, v9

    move-object/from16 v3, p1

    invoke-direct/range {v1 .. v7}, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/CloudParams;Ll6/c;Lm6/b;ILm8/g;)V

    goto :goto_1

    :cond_2
    move-object v13, v5

    move-object v10, v6

    move-object v11, v7

    const/4 v9, 0x0

    :goto_1
    if-eqz v13, :cond_3

    invoke-interface {v11, v13}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    if-eqz v9, :cond_4

    invoke-interface {v10, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_3
    if-eqz v9, :cond_4

    invoke-interface {v11, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    :goto_2
    new-instance v1, Lcom/miui/packageInstaller/ui/listcomponets/FootViewObject;

    iget-object v2, v0, Lb6/r;->a:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/miui/packageInstaller/ui/listcomponets/FootViewObject;-><init>(Landroid/content/Context;)V

    invoke-interface {v11, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object v1

    new-instance v2, Lb6/n;

    invoke-direct {v2, v8, v11, v10}, Lb6/n;-><init>(Lb6/r$a;Ljava/util/List;Ljava/util/List;)V

    invoke-virtual {v1, v2}, Lf6/z;->e(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final t()Ll6/c;
    .locals 1

    iget-object v0, p0, Lb6/r;->b:Ll6/c;

    return-object v0
.end method

.method public final u()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lb6/r;->a:Landroid/content/Context;

    return-object v0
.end method

.method public v(Lcom/miui/packageInstaller/model/CloudParams;)Z
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    iget-boolean p1, p1, Lcom/miui/packageInstaller/model/CloudParams;->showAdsBefore:Z

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_0
    return v0
.end method
