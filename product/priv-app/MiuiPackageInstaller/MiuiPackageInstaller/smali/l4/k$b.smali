.class public final Ll4/k$b;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ll4/k;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation


# instance fields
.field private a:Ll4/d;

.field private b:Ll4/d;

.field private c:Ll4/d;

.field private d:Ll4/d;

.field private e:Ll4/c;

.field private f:Ll4/c;

.field private g:Ll4/c;

.field private h:Ll4/c;

.field private i:Ll4/f;

.field private j:Ll4/f;

.field private k:Ll4/f;

.field private l:Ll4/f;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Ll4/h;->b()Ll4/d;

    move-result-object v0

    iput-object v0, p0, Ll4/k$b;->a:Ll4/d;

    invoke-static {}, Ll4/h;->b()Ll4/d;

    move-result-object v0

    iput-object v0, p0, Ll4/k$b;->b:Ll4/d;

    invoke-static {}, Ll4/h;->b()Ll4/d;

    move-result-object v0

    iput-object v0, p0, Ll4/k$b;->c:Ll4/d;

    invoke-static {}, Ll4/h;->b()Ll4/d;

    move-result-object v0

    iput-object v0, p0, Ll4/k$b;->d:Ll4/d;

    new-instance v0, Ll4/a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ll4/a;-><init>(F)V

    iput-object v0, p0, Ll4/k$b;->e:Ll4/c;

    new-instance v0, Ll4/a;

    invoke-direct {v0, v1}, Ll4/a;-><init>(F)V

    iput-object v0, p0, Ll4/k$b;->f:Ll4/c;

    new-instance v0, Ll4/a;

    invoke-direct {v0, v1}, Ll4/a;-><init>(F)V

    iput-object v0, p0, Ll4/k$b;->g:Ll4/c;

    new-instance v0, Ll4/a;

    invoke-direct {v0, v1}, Ll4/a;-><init>(F)V

    iput-object v0, p0, Ll4/k$b;->h:Ll4/c;

    invoke-static {}, Ll4/h;->c()Ll4/f;

    move-result-object v0

    iput-object v0, p0, Ll4/k$b;->i:Ll4/f;

    invoke-static {}, Ll4/h;->c()Ll4/f;

    move-result-object v0

    iput-object v0, p0, Ll4/k$b;->j:Ll4/f;

    invoke-static {}, Ll4/h;->c()Ll4/f;

    move-result-object v0

    iput-object v0, p0, Ll4/k$b;->k:Ll4/f;

    invoke-static {}, Ll4/h;->c()Ll4/f;

    move-result-object v0

    iput-object v0, p0, Ll4/k$b;->l:Ll4/f;

    return-void
.end method

.method public constructor <init>(Ll4/k;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Ll4/h;->b()Ll4/d;

    move-result-object v0

    iput-object v0, p0, Ll4/k$b;->a:Ll4/d;

    invoke-static {}, Ll4/h;->b()Ll4/d;

    move-result-object v0

    iput-object v0, p0, Ll4/k$b;->b:Ll4/d;

    invoke-static {}, Ll4/h;->b()Ll4/d;

    move-result-object v0

    iput-object v0, p0, Ll4/k$b;->c:Ll4/d;

    invoke-static {}, Ll4/h;->b()Ll4/d;

    move-result-object v0

    iput-object v0, p0, Ll4/k$b;->d:Ll4/d;

    new-instance v0, Ll4/a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ll4/a;-><init>(F)V

    iput-object v0, p0, Ll4/k$b;->e:Ll4/c;

    new-instance v0, Ll4/a;

    invoke-direct {v0, v1}, Ll4/a;-><init>(F)V

    iput-object v0, p0, Ll4/k$b;->f:Ll4/c;

    new-instance v0, Ll4/a;

    invoke-direct {v0, v1}, Ll4/a;-><init>(F)V

    iput-object v0, p0, Ll4/k$b;->g:Ll4/c;

    new-instance v0, Ll4/a;

    invoke-direct {v0, v1}, Ll4/a;-><init>(F)V

    iput-object v0, p0, Ll4/k$b;->h:Ll4/c;

    invoke-static {}, Ll4/h;->c()Ll4/f;

    move-result-object v0

    iput-object v0, p0, Ll4/k$b;->i:Ll4/f;

    invoke-static {}, Ll4/h;->c()Ll4/f;

    move-result-object v0

    iput-object v0, p0, Ll4/k$b;->j:Ll4/f;

    invoke-static {}, Ll4/h;->c()Ll4/f;

    move-result-object v0

    iput-object v0, p0, Ll4/k$b;->k:Ll4/f;

    invoke-static {}, Ll4/h;->c()Ll4/f;

    move-result-object v0

    iput-object v0, p0, Ll4/k$b;->l:Ll4/f;

    iget-object v0, p1, Ll4/k;->a:Ll4/d;

    iput-object v0, p0, Ll4/k$b;->a:Ll4/d;

    iget-object v0, p1, Ll4/k;->b:Ll4/d;

    iput-object v0, p0, Ll4/k$b;->b:Ll4/d;

    iget-object v0, p1, Ll4/k;->c:Ll4/d;

    iput-object v0, p0, Ll4/k$b;->c:Ll4/d;

    iget-object v0, p1, Ll4/k;->d:Ll4/d;

    iput-object v0, p0, Ll4/k$b;->d:Ll4/d;

    iget-object v0, p1, Ll4/k;->e:Ll4/c;

    iput-object v0, p0, Ll4/k$b;->e:Ll4/c;

    iget-object v0, p1, Ll4/k;->f:Ll4/c;

    iput-object v0, p0, Ll4/k$b;->f:Ll4/c;

    iget-object v0, p1, Ll4/k;->g:Ll4/c;

    iput-object v0, p0, Ll4/k$b;->g:Ll4/c;

    iget-object v0, p1, Ll4/k;->h:Ll4/c;

    iput-object v0, p0, Ll4/k$b;->h:Ll4/c;

    iget-object v0, p1, Ll4/k;->i:Ll4/f;

    iput-object v0, p0, Ll4/k$b;->i:Ll4/f;

    iget-object v0, p1, Ll4/k;->j:Ll4/f;

    iput-object v0, p0, Ll4/k$b;->j:Ll4/f;

    iget-object v0, p1, Ll4/k;->k:Ll4/f;

    iput-object v0, p0, Ll4/k$b;->k:Ll4/f;

    iget-object p1, p1, Ll4/k;->l:Ll4/f;

    iput-object p1, p0, Ll4/k$b;->l:Ll4/f;

    return-void
.end method

.method static synthetic a(Ll4/k$b;)Ll4/d;
    .locals 0

    iget-object p0, p0, Ll4/k$b;->a:Ll4/d;

    return-object p0
.end method

.method static synthetic b(Ll4/k$b;)Ll4/f;
    .locals 0

    iget-object p0, p0, Ll4/k$b;->j:Ll4/f;

    return-object p0
.end method

.method static synthetic c(Ll4/k$b;)Ll4/f;
    .locals 0

    iget-object p0, p0, Ll4/k$b;->k:Ll4/f;

    return-object p0
.end method

.method static synthetic d(Ll4/k$b;)Ll4/f;
    .locals 0

    iget-object p0, p0, Ll4/k$b;->l:Ll4/f;

    return-object p0
.end method

.method static synthetic e(Ll4/k$b;)Ll4/d;
    .locals 0

    iget-object p0, p0, Ll4/k$b;->b:Ll4/d;

    return-object p0
.end method

.method static synthetic f(Ll4/k$b;)Ll4/d;
    .locals 0

    iget-object p0, p0, Ll4/k$b;->c:Ll4/d;

    return-object p0
.end method

.method static synthetic g(Ll4/k$b;)Ll4/d;
    .locals 0

    iget-object p0, p0, Ll4/k$b;->d:Ll4/d;

    return-object p0
.end method

.method static synthetic h(Ll4/k$b;)Ll4/c;
    .locals 0

    iget-object p0, p0, Ll4/k$b;->e:Ll4/c;

    return-object p0
.end method

.method static synthetic i(Ll4/k$b;)Ll4/c;
    .locals 0

    iget-object p0, p0, Ll4/k$b;->f:Ll4/c;

    return-object p0
.end method

.method static synthetic j(Ll4/k$b;)Ll4/c;
    .locals 0

    iget-object p0, p0, Ll4/k$b;->g:Ll4/c;

    return-object p0
.end method

.method static synthetic k(Ll4/k$b;)Ll4/c;
    .locals 0

    iget-object p0, p0, Ll4/k$b;->h:Ll4/c;

    return-object p0
.end method

.method static synthetic l(Ll4/k$b;)Ll4/f;
    .locals 0

    iget-object p0, p0, Ll4/k$b;->i:Ll4/f;

    return-object p0
.end method

.method private static n(Ll4/d;)F
    .locals 1

    instance-of v0, p0, Ll4/j;

    if-eqz v0, :cond_0

    check-cast p0, Ll4/j;

    iget p0, p0, Ll4/j;->a:F

    return p0

    :cond_0
    instance-of v0, p0, Ll4/e;

    if-eqz v0, :cond_1

    check-cast p0, Ll4/e;

    iget p0, p0, Ll4/e;->a:F

    return p0

    :cond_1
    const/high16 p0, -0x40800000    # -1.0f

    return p0
.end method


# virtual methods
.method public A(F)Ll4/k$b;
    .locals 1

    new-instance v0, Ll4/a;

    invoke-direct {v0, p1}, Ll4/a;-><init>(F)V

    iput-object v0, p0, Ll4/k$b;->e:Ll4/c;

    return-object p0
.end method

.method public B(Ll4/c;)Ll4/k$b;
    .locals 0

    iput-object p1, p0, Ll4/k$b;->e:Ll4/c;

    return-object p0
.end method

.method public C(ILl4/c;)Ll4/k$b;
    .locals 0

    invoke-static {p1}, Ll4/h;->a(I)Ll4/d;

    move-result-object p1

    invoke-virtual {p0, p1}, Ll4/k$b;->D(Ll4/d;)Ll4/k$b;

    move-result-object p1

    invoke-virtual {p1, p2}, Ll4/k$b;->F(Ll4/c;)Ll4/k$b;

    move-result-object p1

    return-object p1
.end method

.method public D(Ll4/d;)Ll4/k$b;
    .locals 1

    iput-object p1, p0, Ll4/k$b;->b:Ll4/d;

    invoke-static {p1}, Ll4/k$b;->n(Ll4/d;)F

    move-result p1

    const/high16 v0, -0x40800000    # -1.0f

    cmpl-float v0, p1, v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Ll4/k$b;->E(F)Ll4/k$b;

    :cond_0
    return-object p0
.end method

.method public E(F)Ll4/k$b;
    .locals 1

    new-instance v0, Ll4/a;

    invoke-direct {v0, p1}, Ll4/a;-><init>(F)V

    iput-object v0, p0, Ll4/k$b;->f:Ll4/c;

    return-object p0
.end method

.method public F(Ll4/c;)Ll4/k$b;
    .locals 0

    iput-object p1, p0, Ll4/k$b;->f:Ll4/c;

    return-object p0
.end method

.method public m()Ll4/k;
    .locals 2

    new-instance v0, Ll4/k;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Ll4/k;-><init>(Ll4/k$b;Ll4/k$a;)V

    return-object v0
.end method

.method public o(F)Ll4/k$b;
    .locals 1

    invoke-virtual {p0, p1}, Ll4/k$b;->A(F)Ll4/k$b;

    move-result-object v0

    invoke-virtual {v0, p1}, Ll4/k$b;->E(F)Ll4/k$b;

    move-result-object v0

    invoke-virtual {v0, p1}, Ll4/k$b;->w(F)Ll4/k$b;

    move-result-object v0

    invoke-virtual {v0, p1}, Ll4/k$b;->s(F)Ll4/k$b;

    move-result-object p1

    return-object p1
.end method

.method public p(Ll4/c;)Ll4/k$b;
    .locals 1

    invoke-virtual {p0, p1}, Ll4/k$b;->B(Ll4/c;)Ll4/k$b;

    move-result-object v0

    invoke-virtual {v0, p1}, Ll4/k$b;->F(Ll4/c;)Ll4/k$b;

    move-result-object v0

    invoke-virtual {v0, p1}, Ll4/k$b;->x(Ll4/c;)Ll4/k$b;

    move-result-object v0

    invoke-virtual {v0, p1}, Ll4/k$b;->t(Ll4/c;)Ll4/k$b;

    move-result-object p1

    return-object p1
.end method

.method public q(ILl4/c;)Ll4/k$b;
    .locals 0

    invoke-static {p1}, Ll4/h;->a(I)Ll4/d;

    move-result-object p1

    invoke-virtual {p0, p1}, Ll4/k$b;->r(Ll4/d;)Ll4/k$b;

    move-result-object p1

    invoke-virtual {p1, p2}, Ll4/k$b;->t(Ll4/c;)Ll4/k$b;

    move-result-object p1

    return-object p1
.end method

.method public r(Ll4/d;)Ll4/k$b;
    .locals 1

    iput-object p1, p0, Ll4/k$b;->d:Ll4/d;

    invoke-static {p1}, Ll4/k$b;->n(Ll4/d;)F

    move-result p1

    const/high16 v0, -0x40800000    # -1.0f

    cmpl-float v0, p1, v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Ll4/k$b;->s(F)Ll4/k$b;

    :cond_0
    return-object p0
.end method

.method public s(F)Ll4/k$b;
    .locals 1

    new-instance v0, Ll4/a;

    invoke-direct {v0, p1}, Ll4/a;-><init>(F)V

    iput-object v0, p0, Ll4/k$b;->h:Ll4/c;

    return-object p0
.end method

.method public t(Ll4/c;)Ll4/k$b;
    .locals 0

    iput-object p1, p0, Ll4/k$b;->h:Ll4/c;

    return-object p0
.end method

.method public u(ILl4/c;)Ll4/k$b;
    .locals 0

    invoke-static {p1}, Ll4/h;->a(I)Ll4/d;

    move-result-object p1

    invoke-virtual {p0, p1}, Ll4/k$b;->v(Ll4/d;)Ll4/k$b;

    move-result-object p1

    invoke-virtual {p1, p2}, Ll4/k$b;->x(Ll4/c;)Ll4/k$b;

    move-result-object p1

    return-object p1
.end method

.method public v(Ll4/d;)Ll4/k$b;
    .locals 1

    iput-object p1, p0, Ll4/k$b;->c:Ll4/d;

    invoke-static {p1}, Ll4/k$b;->n(Ll4/d;)F

    move-result p1

    const/high16 v0, -0x40800000    # -1.0f

    cmpl-float v0, p1, v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Ll4/k$b;->w(F)Ll4/k$b;

    :cond_0
    return-object p0
.end method

.method public w(F)Ll4/k$b;
    .locals 1

    new-instance v0, Ll4/a;

    invoke-direct {v0, p1}, Ll4/a;-><init>(F)V

    iput-object v0, p0, Ll4/k$b;->g:Ll4/c;

    return-object p0
.end method

.method public x(Ll4/c;)Ll4/k$b;
    .locals 0

    iput-object p1, p0, Ll4/k$b;->g:Ll4/c;

    return-object p0
.end method

.method public y(ILl4/c;)Ll4/k$b;
    .locals 0

    invoke-static {p1}, Ll4/h;->a(I)Ll4/d;

    move-result-object p1

    invoke-virtual {p0, p1}, Ll4/k$b;->z(Ll4/d;)Ll4/k$b;

    move-result-object p1

    invoke-virtual {p1, p2}, Ll4/k$b;->B(Ll4/c;)Ll4/k$b;

    move-result-object p1

    return-object p1
.end method

.method public z(Ll4/d;)Ll4/k$b;
    .locals 1

    iput-object p1, p0, Ll4/k$b;->a:Ll4/d;

    invoke-static {p1}, Ll4/k$b;->n(Ll4/d;)F

    move-result p1

    const/high16 v0, -0x40800000    # -1.0f

    cmpl-float v0, p1, v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Ll4/k$b;->A(F)Ll4/k$b;

    :cond_0
    return-object p0
.end method
