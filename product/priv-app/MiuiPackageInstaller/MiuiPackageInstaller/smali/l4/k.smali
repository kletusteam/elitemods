.class public Ll4/k;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ll4/k$c;,
        Ll4/k$b;
    }
.end annotation


# static fields
.field public static final m:Ll4/c;


# instance fields
.field a:Ll4/d;

.field b:Ll4/d;

.field c:Ll4/d;

.field d:Ll4/d;

.field e:Ll4/c;

.field f:Ll4/c;

.field g:Ll4/c;

.field h:Ll4/c;

.field i:Ll4/f;

.field j:Ll4/f;

.field k:Ll4/f;

.field l:Ll4/f;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ll4/i;

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-direct {v0, v1}, Ll4/i;-><init>(F)V

    sput-object v0, Ll4/k;->m:Ll4/c;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Ll4/h;->b()Ll4/d;

    move-result-object v0

    iput-object v0, p0, Ll4/k;->a:Ll4/d;

    invoke-static {}, Ll4/h;->b()Ll4/d;

    move-result-object v0

    iput-object v0, p0, Ll4/k;->b:Ll4/d;

    invoke-static {}, Ll4/h;->b()Ll4/d;

    move-result-object v0

    iput-object v0, p0, Ll4/k;->c:Ll4/d;

    invoke-static {}, Ll4/h;->b()Ll4/d;

    move-result-object v0

    iput-object v0, p0, Ll4/k;->d:Ll4/d;

    new-instance v0, Ll4/a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ll4/a;-><init>(F)V

    iput-object v0, p0, Ll4/k;->e:Ll4/c;

    new-instance v0, Ll4/a;

    invoke-direct {v0, v1}, Ll4/a;-><init>(F)V

    iput-object v0, p0, Ll4/k;->f:Ll4/c;

    new-instance v0, Ll4/a;

    invoke-direct {v0, v1}, Ll4/a;-><init>(F)V

    iput-object v0, p0, Ll4/k;->g:Ll4/c;

    new-instance v0, Ll4/a;

    invoke-direct {v0, v1}, Ll4/a;-><init>(F)V

    iput-object v0, p0, Ll4/k;->h:Ll4/c;

    invoke-static {}, Ll4/h;->c()Ll4/f;

    move-result-object v0

    iput-object v0, p0, Ll4/k;->i:Ll4/f;

    invoke-static {}, Ll4/h;->c()Ll4/f;

    move-result-object v0

    iput-object v0, p0, Ll4/k;->j:Ll4/f;

    invoke-static {}, Ll4/h;->c()Ll4/f;

    move-result-object v0

    iput-object v0, p0, Ll4/k;->k:Ll4/f;

    invoke-static {}, Ll4/h;->c()Ll4/f;

    move-result-object v0

    iput-object v0, p0, Ll4/k;->l:Ll4/f;

    return-void
.end method

.method private constructor <init>(Ll4/k$b;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Ll4/k$b;->a(Ll4/k$b;)Ll4/d;

    move-result-object v0

    iput-object v0, p0, Ll4/k;->a:Ll4/d;

    invoke-static {p1}, Ll4/k$b;->e(Ll4/k$b;)Ll4/d;

    move-result-object v0

    iput-object v0, p0, Ll4/k;->b:Ll4/d;

    invoke-static {p1}, Ll4/k$b;->f(Ll4/k$b;)Ll4/d;

    move-result-object v0

    iput-object v0, p0, Ll4/k;->c:Ll4/d;

    invoke-static {p1}, Ll4/k$b;->g(Ll4/k$b;)Ll4/d;

    move-result-object v0

    iput-object v0, p0, Ll4/k;->d:Ll4/d;

    invoke-static {p1}, Ll4/k$b;->h(Ll4/k$b;)Ll4/c;

    move-result-object v0

    iput-object v0, p0, Ll4/k;->e:Ll4/c;

    invoke-static {p1}, Ll4/k$b;->i(Ll4/k$b;)Ll4/c;

    move-result-object v0

    iput-object v0, p0, Ll4/k;->f:Ll4/c;

    invoke-static {p1}, Ll4/k$b;->j(Ll4/k$b;)Ll4/c;

    move-result-object v0

    iput-object v0, p0, Ll4/k;->g:Ll4/c;

    invoke-static {p1}, Ll4/k$b;->k(Ll4/k$b;)Ll4/c;

    move-result-object v0

    iput-object v0, p0, Ll4/k;->h:Ll4/c;

    invoke-static {p1}, Ll4/k$b;->l(Ll4/k$b;)Ll4/f;

    move-result-object v0

    iput-object v0, p0, Ll4/k;->i:Ll4/f;

    invoke-static {p1}, Ll4/k$b;->b(Ll4/k$b;)Ll4/f;

    move-result-object v0

    iput-object v0, p0, Ll4/k;->j:Ll4/f;

    invoke-static {p1}, Ll4/k$b;->c(Ll4/k$b;)Ll4/f;

    move-result-object v0

    iput-object v0, p0, Ll4/k;->k:Ll4/f;

    invoke-static {p1}, Ll4/k$b;->d(Ll4/k$b;)Ll4/f;

    move-result-object p1

    iput-object p1, p0, Ll4/k;->l:Ll4/f;

    return-void
.end method

.method synthetic constructor <init>(Ll4/k$b;Ll4/k$a;)V
    .locals 0

    invoke-direct {p0, p1}, Ll4/k;-><init>(Ll4/k$b;)V

    return-void
.end method

.method public static a()Ll4/k$b;
    .locals 1

    new-instance v0, Ll4/k$b;

    invoke-direct {v0}, Ll4/k$b;-><init>()V

    return-object v0
.end method

.method public static b(Landroid/content/Context;II)Ll4/k$b;
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Ll4/k;->c(Landroid/content/Context;III)Ll4/k$b;

    move-result-object p0

    return-object p0
.end method

.method private static c(Landroid/content/Context;III)Ll4/k$b;
    .locals 1

    new-instance v0, Ll4/a;

    int-to-float p3, p3

    invoke-direct {v0, p3}, Ll4/a;-><init>(F)V

    invoke-static {p0, p1, p2, v0}, Ll4/k;->d(Landroid/content/Context;IILl4/c;)Ll4/k$b;

    move-result-object p0

    return-object p0
.end method

.method private static d(Landroid/content/Context;IILl4/c;)Ll4/k$b;
    .locals 6

    if-eqz p2, :cond_0

    new-instance v0, Landroid/view/ContextThemeWrapper;

    invoke-direct {v0, p0, p1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    move p1, p2

    move-object p0, v0

    :cond_0
    sget-object p2, Lu3/k;->V3:[I

    invoke-virtual {p0, p1, p2}, Landroid/content/Context;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object p0

    :try_start_0
    sget p1, Lu3/k;->W3:I

    const/4 p2, 0x0

    invoke-virtual {p0, p1, p2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result p1

    sget p2, Lu3/k;->Z3:I

    invoke-virtual {p0, p2, p1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result p2

    sget v0, Lu3/k;->a4:I

    invoke-virtual {p0, v0, p1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    sget v1, Lu3/k;->Y3:I

    invoke-virtual {p0, v1, p1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    sget v2, Lu3/k;->X3:I

    invoke-virtual {p0, v2, p1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result p1

    sget v2, Lu3/k;->b4:I

    invoke-static {p0, v2, p3}, Ll4/k;->m(Landroid/content/res/TypedArray;ILl4/c;)Ll4/c;

    move-result-object p3

    sget v2, Lu3/k;->e4:I

    invoke-static {p0, v2, p3}, Ll4/k;->m(Landroid/content/res/TypedArray;ILl4/c;)Ll4/c;

    move-result-object v2

    sget v3, Lu3/k;->f4:I

    invoke-static {p0, v3, p3}, Ll4/k;->m(Landroid/content/res/TypedArray;ILl4/c;)Ll4/c;

    move-result-object v3

    sget v4, Lu3/k;->d4:I

    invoke-static {p0, v4, p3}, Ll4/k;->m(Landroid/content/res/TypedArray;ILl4/c;)Ll4/c;

    move-result-object v4

    sget v5, Lu3/k;->c4:I

    invoke-static {p0, v5, p3}, Ll4/k;->m(Landroid/content/res/TypedArray;ILl4/c;)Ll4/c;

    move-result-object p3

    new-instance v5, Ll4/k$b;

    invoke-direct {v5}, Ll4/k$b;-><init>()V

    invoke-virtual {v5, p2, v2}, Ll4/k$b;->y(ILl4/c;)Ll4/k$b;

    move-result-object p2

    invoke-virtual {p2, v0, v3}, Ll4/k$b;->C(ILl4/c;)Ll4/k$b;

    move-result-object p2

    invoke-virtual {p2, v1, v4}, Ll4/k$b;->u(ILl4/c;)Ll4/k$b;

    move-result-object p2

    invoke-virtual {p2, p1, p3}, Ll4/k$b;->q(ILl4/c;)Ll4/k$b;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Landroid/content/res/TypedArray;->recycle()V

    return-object p1

    :catchall_0
    move-exception p1

    invoke-virtual {p0}, Landroid/content/res/TypedArray;->recycle()V

    throw p1
.end method

.method public static e(Landroid/content/Context;Landroid/util/AttributeSet;II)Ll4/k$b;
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, p1, p2, p3, v0}, Ll4/k;->f(Landroid/content/Context;Landroid/util/AttributeSet;III)Ll4/k$b;

    move-result-object p0

    return-object p0
.end method

.method public static f(Landroid/content/Context;Landroid/util/AttributeSet;III)Ll4/k$b;
    .locals 1

    new-instance v0, Ll4/a;

    int-to-float p4, p4

    invoke-direct {v0, p4}, Ll4/a;-><init>(F)V

    invoke-static {p0, p1, p2, p3, v0}, Ll4/k;->g(Landroid/content/Context;Landroid/util/AttributeSet;IILl4/c;)Ll4/k$b;

    move-result-object p0

    return-object p0
.end method

.method public static g(Landroid/content/Context;Landroid/util/AttributeSet;IILl4/c;)Ll4/k$b;
    .locals 1

    sget-object v0, Lu3/k;->c3:[I

    invoke-virtual {p0, p1, v0, p2, p3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p1

    sget p2, Lu3/k;->d3:I

    const/4 p3, 0x0

    invoke-virtual {p1, p2, p3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result p2

    sget v0, Lu3/k;->e3:I

    invoke-virtual {p1, v0, p3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result p3

    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    invoke-static {p0, p2, p3, p4}, Ll4/k;->d(Landroid/content/Context;IILl4/c;)Ll4/k$b;

    move-result-object p0

    return-object p0
.end method

.method private static m(Landroid/content/res/TypedArray;ILl4/c;)Ll4/c;
    .locals 2

    invoke-virtual {p0, p1}, Landroid/content/res/TypedArray;->peekValue(I)Landroid/util/TypedValue;

    move-result-object p1

    if-nez p1, :cond_0

    return-object p2

    :cond_0
    iget v0, p1, Landroid/util/TypedValue;->type:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    new-instance p2, Ll4/a;

    iget p1, p1, Landroid/util/TypedValue;->data:I

    invoke-virtual {p0}, Landroid/content/res/TypedArray;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    invoke-virtual {p0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p0

    invoke-static {p1, p0}, Landroid/util/TypedValue;->complexToDimensionPixelSize(ILandroid/util/DisplayMetrics;)I

    move-result p0

    int-to-float p0, p0

    invoke-direct {p2, p0}, Ll4/a;-><init>(F)V

    return-object p2

    :cond_1
    const/4 p0, 0x6

    if-ne v0, p0, :cond_2

    new-instance p0, Ll4/i;

    const/high16 p2, 0x3f800000    # 1.0f

    invoke-virtual {p1, p2, p2}, Landroid/util/TypedValue;->getFraction(FF)F

    move-result p1

    invoke-direct {p0, p1}, Ll4/i;-><init>(F)V

    return-object p0

    :cond_2
    return-object p2
.end method


# virtual methods
.method public h()Ll4/f;
    .locals 1

    iget-object v0, p0, Ll4/k;->k:Ll4/f;

    return-object v0
.end method

.method public i()Ll4/d;
    .locals 1

    iget-object v0, p0, Ll4/k;->d:Ll4/d;

    return-object v0
.end method

.method public j()Ll4/c;
    .locals 1

    iget-object v0, p0, Ll4/k;->h:Ll4/c;

    return-object v0
.end method

.method public k()Ll4/d;
    .locals 1

    iget-object v0, p0, Ll4/k;->c:Ll4/d;

    return-object v0
.end method

.method public l()Ll4/c;
    .locals 1

    iget-object v0, p0, Ll4/k;->g:Ll4/c;

    return-object v0
.end method

.method public n()Ll4/f;
    .locals 1

    iget-object v0, p0, Ll4/k;->l:Ll4/f;

    return-object v0
.end method

.method public o()Ll4/f;
    .locals 1

    iget-object v0, p0, Ll4/k;->j:Ll4/f;

    return-object v0
.end method

.method public p()Ll4/f;
    .locals 1

    iget-object v0, p0, Ll4/k;->i:Ll4/f;

    return-object v0
.end method

.method public q()Ll4/d;
    .locals 1

    iget-object v0, p0, Ll4/k;->a:Ll4/d;

    return-object v0
.end method

.method public r()Ll4/c;
    .locals 1

    iget-object v0, p0, Ll4/k;->e:Ll4/c;

    return-object v0
.end method

.method public s()Ll4/d;
    .locals 1

    iget-object v0, p0, Ll4/k;->b:Ll4/d;

    return-object v0
.end method

.method public t()Ll4/c;
    .locals 1

    iget-object v0, p0, Ll4/k;->f:Ll4/c;

    return-object v0
.end method

.method public u(Landroid/graphics/RectF;)Z
    .locals 5

    const-class v0, Ll4/f;

    iget-object v1, p0, Ll4/k;->l:Ll4/f;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    iget-object v1, p0, Ll4/k;->j:Ll4/f;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Ll4/k;->i:Ll4/f;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Ll4/k;->k:Ll4/f;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v2

    goto :goto_0

    :cond_0
    move v0, v3

    :goto_0
    iget-object v1, p0, Ll4/k;->e:Ll4/c;

    invoke-interface {v1, p1}, Ll4/c;->a(Landroid/graphics/RectF;)F

    move-result v1

    iget-object v4, p0, Ll4/k;->f:Ll4/c;

    invoke-interface {v4, p1}, Ll4/c;->a(Landroid/graphics/RectF;)F

    move-result v4

    cmpl-float v4, v4, v1

    if-nez v4, :cond_1

    iget-object v4, p0, Ll4/k;->h:Ll4/c;

    invoke-interface {v4, p1}, Ll4/c;->a(Landroid/graphics/RectF;)F

    move-result v4

    cmpl-float v4, v4, v1

    if-nez v4, :cond_1

    iget-object v4, p0, Ll4/k;->g:Ll4/c;

    invoke-interface {v4, p1}, Ll4/c;->a(Landroid/graphics/RectF;)F

    move-result p1

    cmpl-float p1, p1, v1

    if-nez p1, :cond_1

    move p1, v2

    goto :goto_1

    :cond_1
    move p1, v3

    :goto_1
    iget-object v1, p0, Ll4/k;->b:Ll4/d;

    instance-of v1, v1, Ll4/j;

    if-eqz v1, :cond_2

    iget-object v1, p0, Ll4/k;->a:Ll4/d;

    instance-of v1, v1, Ll4/j;

    if-eqz v1, :cond_2

    iget-object v1, p0, Ll4/k;->c:Ll4/d;

    instance-of v1, v1, Ll4/j;

    if-eqz v1, :cond_2

    iget-object v1, p0, Ll4/k;->d:Ll4/d;

    instance-of v1, v1, Ll4/j;

    if-eqz v1, :cond_2

    move v1, v2

    goto :goto_2

    :cond_2
    move v1, v3

    :goto_2
    if-eqz v0, :cond_3

    if-eqz p1, :cond_3

    if-eqz v1, :cond_3

    goto :goto_3

    :cond_3
    move v2, v3

    :goto_3
    return v2
.end method

.method public v()Ll4/k$b;
    .locals 1

    new-instance v0, Ll4/k$b;

    invoke-direct {v0, p0}, Ll4/k$b;-><init>(Ll4/k;)V

    return-object v0
.end method

.method public w(F)Ll4/k;
    .locals 1

    invoke-virtual {p0}, Ll4/k;->v()Ll4/k$b;

    move-result-object v0

    invoke-virtual {v0, p1}, Ll4/k$b;->o(F)Ll4/k$b;

    move-result-object p1

    invoke-virtual {p1}, Ll4/k$b;->m()Ll4/k;

    move-result-object p1

    return-object p1
.end method

.method public x(Ll4/c;)Ll4/k;
    .locals 1

    invoke-virtual {p0}, Ll4/k;->v()Ll4/k$b;

    move-result-object v0

    invoke-virtual {v0, p1}, Ll4/k$b;->p(Ll4/c;)Ll4/k$b;

    move-result-object p1

    invoke-virtual {p1}, Ll4/k$b;->m()Ll4/k;

    move-result-object p1

    return-object p1
.end method

.method public y(Ll4/k$c;)Ll4/k;
    .locals 2

    invoke-virtual {p0}, Ll4/k;->v()Ll4/k$b;

    move-result-object v0

    invoke-virtual {p0}, Ll4/k;->r()Ll4/c;

    move-result-object v1

    invoke-interface {p1, v1}, Ll4/k$c;->a(Ll4/c;)Ll4/c;

    move-result-object v1

    invoke-virtual {v0, v1}, Ll4/k$b;->B(Ll4/c;)Ll4/k$b;

    move-result-object v0

    invoke-virtual {p0}, Ll4/k;->t()Ll4/c;

    move-result-object v1

    invoke-interface {p1, v1}, Ll4/k$c;->a(Ll4/c;)Ll4/c;

    move-result-object v1

    invoke-virtual {v0, v1}, Ll4/k$b;->F(Ll4/c;)Ll4/k$b;

    move-result-object v0

    invoke-virtual {p0}, Ll4/k;->j()Ll4/c;

    move-result-object v1

    invoke-interface {p1, v1}, Ll4/k$c;->a(Ll4/c;)Ll4/c;

    move-result-object v1

    invoke-virtual {v0, v1}, Ll4/k$b;->t(Ll4/c;)Ll4/k$b;

    move-result-object v0

    invoke-virtual {p0}, Ll4/k;->l()Ll4/c;

    move-result-object v1

    invoke-interface {p1, v1}, Ll4/k$c;->a(Ll4/c;)Ll4/c;

    move-result-object p1

    invoke-virtual {v0, p1}, Ll4/k$b;->x(Ll4/c;)Ll4/k$b;

    move-result-object p1

    invoke-virtual {p1}, Ll4/k$b;->m()Ll4/k;

    move-result-object p1

    return-object p1
.end method
