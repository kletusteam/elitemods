.class public final Lx5/h;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/app/Application$ActivityLifecycleCallbacks;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lx5/h$c;,
        Lx5/h$b;,
        Lx5/h$a;,
        Lx5/h$d;
    }
.end annotation


# static fields
.field public static final k:Lx5/h$c;


# instance fields
.field private a:Landroid/app/Activity;

.field private b:Landroid/app/Dialog;

.field private c:Landroid/view/ViewGroup;

.field private d:Landroid/view/ViewGroup;

.field private e:Lx5/h$d;

.field private f:Lx5/c;

.field private g:Landroid/view/View;

.field private h:Ljava/lang/CharSequence;

.field private i:Ljava/lang/CharSequence;

.field private j:Ljava/lang/CharSequence;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lx5/h$c;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lx5/h$c;-><init>(Lm8/g;)V

    sput-object v0, Lx5/h;->k:Lx5/h$c;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;)V
    .locals 1

    const-string v0, "mContext"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lx5/h;->a:Landroid/app/Activity;

    sget-object p1, Lx5/h$d;->b:Lx5/h$d;

    iput-object p1, p0, Lx5/h;->e:Lx5/h$d;

    return-void
.end method

.method public static synthetic a(Lx5/h;Landroid/content/DialogInterface;)V
    .locals 0

    invoke-static {p0, p1}, Lx5/h;->i(Lx5/h;Landroid/content/DialogInterface;)V

    return-void
.end method

.method public static synthetic b(Lx5/h;Landroid/content/DialogInterface;)V
    .locals 0

    invoke-static {p0, p1}, Lx5/h;->j(Lx5/h;Landroid/content/DialogInterface;)V

    return-void
.end method

.method public static final synthetic c(Lx5/h;)Landroid/app/Dialog;
    .locals 0

    iget-object p0, p0, Lx5/h;->b:Landroid/app/Dialog;

    return-object p0
.end method

.method public static final synthetic d(Lx5/h;Ljava/lang/CharSequence;)V
    .locals 0

    iput-object p1, p0, Lx5/h;->i:Ljava/lang/CharSequence;

    return-void
.end method

.method public static final synthetic e(Lx5/h;Ljava/lang/CharSequence;)V
    .locals 0

    iput-object p1, p0, Lx5/h;->h:Ljava/lang/CharSequence;

    return-void
.end method

.method public static final synthetic f(Lx5/h;Ljava/lang/CharSequence;)V
    .locals 0

    iput-object p1, p0, Lx5/h;->j:Ljava/lang/CharSequence;

    return-void
.end method

.method private static final i(Lx5/h;Landroid/content/DialogInterface;)V
    .locals 0

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p0, p0, Lx5/h;->f:Lx5/c;

    if-nez p0, :cond_0

    const-string p0, "mPasswordView"

    invoke-static {p0}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 p0, 0x0

    :cond_0
    invoke-interface {p0}, Lx5/c;->a()V

    return-void
.end method

.method private static final j(Lx5/h;Landroid/content/DialogInterface;)V
    .locals 1

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v0, 0x1d

    if-lt p1, v0, :cond_0

    iget-object p1, p0, Lx5/h;->a:Landroid/app/Activity;

    invoke-virtual {p1, p0}, Landroid/app/Activity;->unregisterActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    :cond_0
    iget-object p0, p0, Lx5/h;->f:Lx5/c;

    if-nez p0, :cond_1

    const-string p0, "mPasswordView"

    invoke-static {p0}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 p0, 0x0

    :cond_1
    invoke-interface {p0}, Lx5/c;->release()V

    return-void
.end method


# virtual methods
.method public final g()Z
    .locals 1

    iget-object v0, p0, Lx5/h;->b:Landroid/app/Dialog;

    if-nez v0, :cond_0

    const-string v0, "mDialog"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v0, 0x0

    :cond_0
    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    return v0
.end method

.method public final h()V
    .locals 7

    iget-object v0, p0, Lx5/h;->a:Landroid/app/Activity;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0d0055

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0a00ea

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const-string v4, "view.findViewById(R.id.content_layout)"

    invoke-static {v1, v4}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lx5/h;->d:Landroid/view/ViewGroup;

    const v1, 0x7f0a0082

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const-string v4, "view.findViewById(R.id.auth_container)"

    invoke-static {v1, v4}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lx5/h;->c:Landroid/view/ViewGroup;

    iget-object v1, p0, Lx5/h;->e:Lx5/h$d;

    sget-object v4, Lx5/h$d;->d:Lx5/h$d;

    if-ne v1, v4, :cond_0

    new-instance v1, Lx5/h$a;

    iget-object v4, p0, Lx5/h;->a:Landroid/app/Activity;

    invoke-direct {v1, v4}, Lx5/h$a;-><init>(Landroid/content/Context;)V

    const-string v4, "view"

    invoke-static {v0, v4}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Lx5/h$a;->g(Landroid/view/View;)V

    iput-object v1, p0, Lx5/h;->b:Landroid/app/Dialog;

    goto :goto_0

    :cond_0
    new-instance v1, Lmiuix/appcompat/app/i$b;

    iget-object v4, p0, Lx5/h;->a:Landroid/app/Activity;

    invoke-direct {v1, v4}, Lmiuix/appcompat/app/i$b;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, Lmiuix/appcompat/app/i$b;->v(Landroid/view/View;)Lmiuix/appcompat/app/i$b;

    move-result-object v0

    invoke-virtual {v0}, Lmiuix/appcompat/app/i$b;->a()Lmiuix/appcompat/app/i;

    move-result-object v0

    const-string v1, "Builder(mContext)\n      \u2026                .create()"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lx5/h;->b:Landroid/app/Dialog;

    :goto_0
    iget-object v0, p0, Lx5/h;->b:Landroid/app/Dialog;

    const-string v1, "mDialog"

    if-nez v0, :cond_1

    invoke-static {v1}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v2

    :cond_1
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    if-eqz v0, :cond_2

    const/16 v4, 0x2000

    invoke-virtual {v0, v4}, Landroid/view/Window;->addFlags(I)V

    :cond_2
    iget-object v0, p0, Lx5/h;->b:Landroid/app/Dialog;

    if-nez v0, :cond_3

    invoke-static {v1}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v2

    :cond_3
    new-instance v4, Lx5/f;

    invoke-direct {v4, p0}, Lx5/f;-><init>(Lx5/h;)V

    invoke-virtual {v0, v4}, Landroid/app/Dialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    iget-object v0, p0, Lx5/h;->b:Landroid/app/Dialog;

    if-nez v0, :cond_4

    invoke-static {v1}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v2

    :cond_4
    new-instance v1, Lx5/g;

    invoke-direct {v1, p0}, Lx5/g;-><init>(Lx5/h;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    sget-object v0, Lx5/h;->k:Lx5/h$c;

    iget-object v1, p0, Lx5/h;->e:Lx5/h$d;

    invoke-virtual {v0, v1}, Lx5/h$c;->b(Lx5/h$d;)I

    move-result v0

    iget-object v1, p0, Lx5/h;->a:Landroid/app/Activity;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    iget-object v4, p0, Lx5/h;->c:Landroid/view/ViewGroup;

    const-string v5, "mPasswordContentLayout"

    if-nez v4, :cond_5

    invoke-static {v5}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v4, v2

    :cond_5
    invoke-virtual {v1, v0, v4, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lx5/h;->g:Landroid/view/View;

    const/4 v3, -0x2

    const/4 v4, -0x1

    if-eqz v1, :cond_7

    iget-object v6, p0, Lx5/h;->d:Landroid/view/ViewGroup;

    if-nez v6, :cond_6

    const-string v6, "mContentLayout"

    invoke-static {v6}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v6, v2

    :cond_6
    invoke-virtual {v6, v1, v4, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;II)V

    :cond_7
    iget-object v1, p0, Lx5/h;->c:Landroid/view/ViewGroup;

    if-nez v1, :cond_8

    invoke-static {v5}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_1

    :cond_8
    move-object v2, v1

    :goto_1
    invoke-virtual {v2, v0, v4, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;II)V

    const-string v1, "null cannot be cast to non-null type com.miui.packageInstaller.secure.IPassWord"

    invoke-static {v0, v1}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lx5/c;

    iput-object v0, p0, Lx5/h;->f:Lx5/c;

    iget-object v1, p0, Lx5/h;->i:Ljava/lang/CharSequence;

    if-eqz v1, :cond_9

    invoke-interface {v0, v1}, Lx5/c;->setCancelButtonText(Ljava/lang/CharSequence;)V

    :cond_9
    iget-object v1, p0, Lx5/h;->h:Ljava/lang/CharSequence;

    if-eqz v1, :cond_a

    invoke-interface {v0, v1}, Lx5/c;->setConfirmButtonText(Ljava/lang/CharSequence;)V

    :cond_a
    iget-object v1, p0, Lx5/h;->j:Ljava/lang/CharSequence;

    if-eqz v1, :cond_b

    invoke-interface {v0, v1}, Lx5/c;->setTipMsgText(Ljava/lang/CharSequence;)V

    :cond_b
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1d

    if-lt v0, v1, :cond_c

    iget-object v0, p0, Lx5/h;->a:Landroid/app/Activity;

    invoke-virtual {v0, p0}, Landroid/app/Activity;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    :cond_c
    return-void
.end method

.method public final k(Landroid/view/View;)V
    .locals 1

    const-string v0, "view"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lx5/h;->g:Landroid/view/View;

    return-void
.end method

.method public final l(Lx5/h$d;)V
    .locals 1

    const-string v0, "type"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lx5/h;->e:Lx5/h$d;

    return-void
.end method

.method public final m(Ll8/l;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ll8/l<",
            "-",
            "Ljava/lang/Integer;",
            "La8/v;",
            ">;)V"
        }
    .end annotation

    const-string v0, "callBack"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    :try_start_0
    iget-object v0, p0, Lx5/h;->b:Landroid/app/Dialog;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const-string v0, "mDialog"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_0
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    iget-object v0, p0, Lx5/h;->f:Lx5/c;

    if-nez v0, :cond_1

    const-string v0, "mPasswordView"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move-object v1, v0

    :goto_0
    new-instance v0, Lx5/h$e;

    invoke-direct {v0, p0, p1}, Lx5/h$e;-><init>(Lx5/h;Ll8/l;)V

    invoke-interface {v1, p0, v0}, Lx5/c;->d(Lx5/h;Ll8/l;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v0}, Ll8/l;->j(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_1
    return-void
.end method

.method public onActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 0

    const-string p2, "p0"

    invoke-static {p1, p2}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public onActivityDestroyed(Landroid/app/Activity;)V
    .locals 1

    const-string v0, "p0"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public onActivityPaused(Landroid/app/Activity;)V
    .locals 1

    const-string v0, "p0"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public onActivityResumed(Landroid/app/Activity;)V
    .locals 1

    const-string v0, "p0"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public onActivitySaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 1

    const-string v0, "p0"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "p1"

    invoke-static {p2, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public onActivityStarted(Landroid/app/Activity;)V
    .locals 1

    const-string v0, "p0"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public onActivityStopped(Landroid/app/Activity;)V
    .locals 2

    const-string v0, "p0"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p0, Lx5/h;->b:Landroid/app/Dialog;

    const/4 v0, 0x0

    const-string v1, "mDialog"

    if-nez p1, :cond_0

    invoke-static {v1}, Lm8/i;->s(Ljava/lang/String;)V

    move-object p1, v0

    :cond_0
    invoke-virtual {p1}, Landroid/app/Dialog;->isShowing()Z

    move-result p1

    if-eqz p1, :cond_2

    iget-object p1, p0, Lx5/h;->b:Landroid/app/Dialog;

    if-nez p1, :cond_1

    invoke-static {v1}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move-object v0, p1

    :goto_0
    invoke-virtual {v0}, Landroid/app/Dialog;->cancel()V

    :cond_2
    return-void
.end method
