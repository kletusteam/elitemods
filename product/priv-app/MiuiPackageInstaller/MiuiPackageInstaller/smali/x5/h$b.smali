.class public final Lx5/h$b;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lx5/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lx5/h$b$a;
    }
.end annotation


# instance fields
.field private final a:Landroid/app/Activity;

.field private b:Ljava/lang/CharSequence;

.field private c:Ljava/lang/CharSequence;

.field private d:Ljava/lang/CharSequence;

.field private e:Ljava/lang/CharSequence;

.field private f:Ljava/lang/CharSequence;

.field private g:Lx5/a;

.field private h:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 1

    const-string v0, "mContext"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lx5/h$b;->a:Landroid/app/Activity;

    sget-object p1, Lx5/a;->c:Lx5/a;

    iput-object p1, p0, Lx5/h$b;->g:Lx5/a;

    return-void
.end method


# virtual methods
.method public final a()Lx5/h;
    .locals 5

    new-instance v0, Lx5/h;

    iget-object v1, p0, Lx5/h$b;->a:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lx5/h;-><init>(Landroid/app/Activity;)V

    iget-object v1, p0, Lx5/h$b;->g:Lx5/a;

    sget-object v2, Lx5/h$b$a;->a:[I

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v1, v2, v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_2

    const/4 v2, 0x2

    if-eq v1, v2, :cond_1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    sget-object v1, Lx5/h$d;->b:Lx5/h$d;

    goto :goto_0

    :cond_0
    sget-object v1, Lx5/h$d;->e:Lx5/h$d;

    goto :goto_0

    :cond_1
    sget-object v1, Lx5/h$d;->d:Lx5/h$d;

    goto :goto_0

    :cond_2
    sget-object v1, Lx5/h;->k:Lx5/h$c;

    iget-object v2, p0, Lx5/h$b;->a:Landroid/app/Activity;

    invoke-virtual {v1, v2}, Lx5/h$c;->a(Landroid/content/Context;)Lx5/h$d;

    move-result-object v1

    :goto_0
    invoke-virtual {v0, v1}, Lx5/h;->l(Lx5/h$d;)V

    iget-object v1, p0, Lx5/h$b;->h:Landroid/view/View;

    if-nez v1, :cond_4

    iget-object v1, p0, Lx5/h$b;->a:Landroid/app/Activity;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0d006e

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const-string v2, "from(mContext).inflate(R\u2026_authartion_header, null)"

    invoke-static {v1, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const v2, 0x7f0a0380

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    const v3, 0x7f0a0255

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iget-object v4, p0, Lx5/h$b;->b:Ljava/lang/CharSequence;

    if-eqz v4, :cond_3

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    iget-object v2, p0, Lx5/h$b;->f:Ljava/lang/CharSequence;

    if-eqz v2, :cond_4

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_4
    invoke-virtual {v0, v1}, Lx5/h;->k(Landroid/view/View;)V

    iget-object v1, p0, Lx5/h$b;->c:Ljava/lang/CharSequence;

    if-eqz v1, :cond_5

    invoke-static {v0, v1}, Lx5/h;->e(Lx5/h;Ljava/lang/CharSequence;)V

    :cond_5
    iget-object v1, p0, Lx5/h$b;->d:Ljava/lang/CharSequence;

    if-eqz v1, :cond_6

    invoke-static {v0, v1}, Lx5/h;->d(Lx5/h;Ljava/lang/CharSequence;)V

    :cond_6
    iget-object v1, p0, Lx5/h$b;->e:Ljava/lang/CharSequence;

    if-eqz v1, :cond_7

    invoke-static {v0, v1}, Lx5/h;->f(Lx5/h;Ljava/lang/CharSequence;)V

    :cond_7
    invoke-virtual {v0}, Lx5/h;->h()V

    return-object v0
.end method

.method public final b(Ljava/lang/CharSequence;)Lx5/h$b;
    .locals 1

    const-string v0, "text"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lx5/h$b;->d:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public final c(Ljava/lang/CharSequence;)Lx5/h$b;
    .locals 1

    const-string v0, "text"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lx5/h$b;->c:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public final d(Landroid/view/View;)Lx5/h$b;
    .locals 1

    const-string v0, "header"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lx5/h$b;->h:Landroid/view/View;

    return-object p0
.end method

.method public final e(Ljava/lang/CharSequence;)Lx5/h$b;
    .locals 1

    const-string v0, "msg"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lx5/h$b;->f:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public final f(Ljava/lang/CharSequence;)Lx5/h$b;
    .locals 1

    const-string v0, "text"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lx5/h$b;->e:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public final g(Ljava/lang/CharSequence;)Lx5/h$b;
    .locals 1

    const-string v0, "title"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lx5/h$b;->b:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public final h(Lx5/a;)Lx5/h$b;
    .locals 1

    const-string v0, "type"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lx5/h$b;->g:Lx5/a;

    return-object p0
.end method
