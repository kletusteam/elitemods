.class final Lx5/d$c;
.super Lm8/j;

# interfaces
.implements Ll8/p;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lx5/d;->c(Lx5/j$a;Ll8/p;Lx5/a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lm8/j;",
        "Ll8/p<",
        "Lx5/a;",
        "Ljava/lang/Integer;",
        "La8/v;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic b:Lx5/d;

.field final synthetic c:Ll8/p;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ll8/p<",
            "Lx5/a;",
            "Ljava/lang/Integer;",
            "La8/v;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lx5/d;Ll8/p;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lx5/d;",
            "Ll8/p<",
            "-",
            "Lx5/a;",
            "-",
            "Ljava/lang/Integer;",
            "La8/v;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lx5/d$c;->b:Lx5/d;

    iput-object p2, p0, Lx5/d$c;->c:Ll8/p;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lm8/j;-><init>(I)V

    return-void
.end method

.method public static synthetic b(Ll8/p;Lx5/a;I)V
    .locals 0

    invoke-static {p0, p1, p2}, Lx5/d$c;->e(Ll8/p;Lx5/a;I)V

    return-void
.end method

.method private static final e(Ll8/p;Lx5/a;I)V
    .locals 1

    const-string v0, "$callback"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$type"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-interface {p0, p1, p2}, Ll8/p;->g(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public final c(Lx5/a;I)V
    .locals 3

    const-string v0, "type"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object v0

    iget-object v1, p0, Lx5/d$c;->c:Ll8/p;

    new-instance v2, Lx5/e;

    invoke-direct {v2, v1, p1, p2}, Lx5/e;-><init>(Ll8/p;Lx5/a;I)V

    invoke-virtual {v0, v2}, Lf6/z;->e(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lx5/d$c;->b:Lx5/d;

    invoke-virtual {v0}, Lx5/d;->d()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->isSafeModelEnable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lx5/a;->f:Lx5/a;

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    if-ne p2, p1, :cond_0

    iget-object p1, p0, Lx5/d$c;->b:Lx5/d;

    invoke-static {p1}, Lx5/d;->b(Lx5/d;)V

    :cond_0
    return-void
.end method

.method public bridge synthetic g(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lx5/a;

    check-cast p2, Ljava/lang/Number;

    invoke-virtual {p2}, Ljava/lang/Number;->intValue()I

    move-result p2

    invoke-virtual {p0, p1, p2}, Lx5/d$c;->c(Lx5/a;I)V

    sget-object p1, La8/v;->a:La8/v;

    return-object p1
.end method
