.class public final Lx5/j$a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lx5/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field private final a:Landroid/app/Activity;

.field private b:Ljava/lang/CharSequence;

.field private c:Ljava/lang/CharSequence;

.field private d:Ljava/lang/CharSequence;

.field private e:Ljava/lang/CharSequence;

.field private f:Ljava/lang/CharSequence;

.field private g:Lx5/h$d;

.field private h:Landroid/view/View;

.field private i:Z

.field private j:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lx5/a;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ll8/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ll8/l<",
            "-",
            "Lx5/a;",
            "La8/v;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 1

    const-string v0, "mActivity"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lx5/j$a;->a:Landroid/app/Activity;

    sget-object p1, Lx5/h$d;->b:Lx5/h$d;

    iput-object p1, p0, Lx5/j$a;->g:Lx5/h$d;

    return-void
.end method


# virtual methods
.method public final a()Lx5/j;
    .locals 3

    new-instance v0, Lx5/j;

    iget-object v1, p0, Lx5/j$a;->a:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lx5/j;-><init>(Landroid/app/Activity;)V

    iget-object v1, p0, Lx5/j$a;->b:Ljava/lang/CharSequence;

    if-eqz v1, :cond_0

    invoke-static {v0, v1}, Lx5/j;->i(Lx5/j;Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v1, p0, Lx5/j$a;->c:Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Lx5/j;->d(Lx5/j;Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lx5/j$a;->d:Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Lx5/j;->c(Lx5/j;Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lx5/j$a;->e:Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Lx5/j;->h(Lx5/j;Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lx5/j$a;->f:Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Lx5/j;->f(Lx5/j;Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lx5/j$a;->g:Lx5/h$d;

    invoke-static {v0, v1}, Lx5/j;->j(Lx5/j;Lx5/h$d;)V

    iget-object v1, p0, Lx5/j$a;->h:Landroid/view/View;

    if-eqz v1, :cond_1

    invoke-static {v0, v1}, Lx5/j;->e(Lx5/j;Landroid/view/View;)V

    :cond_1
    iget-boolean v1, p0, Lx5/j$a;->i:Z

    invoke-static {v0, v1}, Lx5/j;->k(Lx5/j;Z)V

    iget-object v1, p0, Lx5/j$a;->j:Ljava/util/ArrayList;

    if-eqz v1, :cond_2

    invoke-static {v0}, Lx5/j;->b(Lx5/j;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_2
    iget-object v1, p0, Lx5/j$a;->k:Ll8/l;

    if-eqz v1, :cond_3

    invoke-static {v0, v1}, Lx5/j;->g(Lx5/j;Ll8/l;)V

    :cond_3
    return-object v0
.end method

.method public final b(Ll8/l;)Lx5/j$a;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ll8/l<",
            "-",
            "Lx5/a;",
            "La8/v;",
            ">;)",
            "Lx5/j$a;"
        }
    .end annotation

    iput-object p1, p0, Lx5/j$a;->k:Ll8/l;

    return-object p0
.end method

.method public final c(Ljava/util/ArrayList;)Lx5/j$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lx5/a;",
            ">;)",
            "Lx5/j$a;"
        }
    .end annotation

    const-string v0, "order"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lx5/j$a;->j:Ljava/util/ArrayList;

    return-object p0
.end method

.method public final varargs d([Lx5/a;)Lx5/j$a;
    .locals 2

    const-string v0, "order"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lx5/j$a;->j:Ljava/util/ArrayList;

    invoke-static {p1}, Lm8/b;->a([Ljava/lang/Object;)Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lx5/a;

    iget-object v1, p0, Lx5/j$a;->j:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object p0
.end method

.method public final e(I)Lx5/j$a;
    .locals 1

    iget-object v0, p0, Lx5/j$a;->a:Landroid/app/Activity;

    invoke-virtual {v0, p1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lx5/j$a;->d:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public final f(I)Lx5/j$a;
    .locals 1

    iget-object v0, p0, Lx5/j$a;->a:Landroid/app/Activity;

    invoke-virtual {v0, p1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lx5/j$a;->c:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public final g(Landroid/view/View;)Lx5/j$a;
    .locals 1

    const-string v0, "header"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lx5/j$a;->h:Landroid/view/View;

    return-object p0
.end method

.method public final h(I)Lx5/j$a;
    .locals 1

    iget-object v0, p0, Lx5/j$a;->a:Landroid/app/Activity;

    invoke-virtual {v0, p1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lx5/j$a;->f:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public final i(Ljava/lang/CharSequence;)Lx5/j$a;
    .locals 1

    const-string v0, "msg"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lx5/j$a;->f:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public final j(I)Lx5/j$a;
    .locals 1

    iget-object v0, p0, Lx5/j$a;->a:Landroid/app/Activity;

    invoke-virtual {v0, p1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lx5/j$a;->e:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public final k(Ljava/lang/CharSequence;)Lx5/j$a;
    .locals 1

    const-string v0, "text"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lx5/j$a;->e:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public final l(I)Lx5/j$a;
    .locals 1

    iget-object v0, p0, Lx5/j$a;->a:Landroid/app/Activity;

    invoke-virtual {v0, p1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lx5/j$a;->b:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public final m(Ljava/lang/CharSequence;)Lx5/j$a;
    .locals 1

    const-string v0, "title"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lx5/j$a;->b:Ljava/lang/CharSequence;

    return-object p0
.end method
