.class public final enum Lx5/a;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lx5/a$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lx5/a;",
        ">;"
    }
.end annotation


# static fields
.field public static final b:Lx5/a$a;

.field public static final enum c:Lx5/a;

.field public static final enum d:Lx5/a;

.field public static final enum e:Lx5/a;

.field public static final enum f:Lx5/a;

.field public static final enum g:Lx5/a;

.field private static final synthetic h:[Lx5/a;


# instance fields
.field private final a:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    new-instance v0, Lx5/a;

    const-string v1, "PASSWORD"

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lx5/a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lx5/a;->c:Lx5/a;

    new-instance v0, Lx5/a;

    const-string v1, "FINGER"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v3, v4}, Lx5/a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lx5/a;->d:Lx5/a;

    new-instance v0, Lx5/a;

    const-string v1, "FACE"

    const/4 v3, 0x3

    invoke-direct {v0, v1, v4, v3}, Lx5/a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lx5/a;->e:Lx5/a;

    new-instance v0, Lx5/a;

    const-string v1, "ACCOUNT"

    invoke-direct {v0, v1, v3, v2}, Lx5/a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lx5/a;->f:Lx5/a;

    new-instance v0, Lx5/a;

    const-string v1, "NONE"

    const/4 v2, 0x4

    const/4 v3, -0x1

    invoke-direct {v0, v1, v2, v3}, Lx5/a;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lx5/a;->g:Lx5/a;

    invoke-static {}, Lx5/a;->a()[Lx5/a;

    move-result-object v0

    sput-object v0, Lx5/a;->h:[Lx5/a;

    new-instance v0, Lx5/a$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lx5/a$a;-><init>(Lm8/g;)V

    sput-object v0, Lx5/a;->b:Lx5/a$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lx5/a;->a:I

    return-void
.end method

.method private static final synthetic a()[Lx5/a;
    .locals 3

    const/4 v0, 0x5

    new-array v0, v0, [Lx5/a;

    sget-object v1, Lx5/a;->c:Lx5/a;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lx5/a;->d:Lx5/a;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lx5/a;->e:Lx5/a;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lx5/a;->f:Lx5/a;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sget-object v1, Lx5/a;->g:Lx5/a;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    return-object v0
.end method

.method public static final b(I)Lx5/a;
    .locals 1

    sget-object v0, Lx5/a;->b:Lx5/a$a;

    invoke-virtual {v0, p0}, Lx5/a$a;->a(I)Lx5/a;

    move-result-object p0

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lx5/a;
    .locals 1

    const-class v0, Lx5/a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lx5/a;

    return-object p0
.end method

.method public static values()[Lx5/a;
    .locals 1

    sget-object v0, Lx5/a;->h:[Lx5/a;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lx5/a;

    return-object v0
.end method


# virtual methods
.method public final c()I
    .locals 1

    iget v0, p0, Lx5/a;->a:I

    return v0
.end method
