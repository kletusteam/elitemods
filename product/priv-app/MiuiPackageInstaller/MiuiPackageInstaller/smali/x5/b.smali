.class public final Lx5/b;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lx5/b$a;
    }
.end annotation


# static fields
.field public static final c:Lx5/b$a;


# instance fields
.field private a:Landroid/app/Activity;

.field private final b:Ly7/f;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lx5/b$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lx5/b$a;-><init>(Lm8/g;)V

    sput-object v0, Lx5/b;->c:Lx5/b$a;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;)V
    .locals 1

    const-string v0, "activity"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lx5/b;->a:Landroid/app/Activity;

    new-instance p1, Ly7/f;

    iget-object v0, p0, Lx5/b;->a:Landroid/app/Activity;

    invoke-direct {p1, v0}, Ly7/f;-><init>(Landroid/app/Activity;)V

    iput-object p1, p0, Lx5/b;->b:Ly7/f;

    invoke-virtual {p1}, Ly7/f;->k0()V

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Ly7/f;->p0(Z)Ly7/f;

    const-string v0, "531cf7c723d346e18a8d96d5c0588607"

    invoke-virtual {p1, v0}, Ly7/f;->q0(Ljava/lang/String;)Ly7/f;

    const-string v0, "installer_security_check"

    invoke-virtual {p1, v0}, Ly7/f;->o0(Ljava/lang/String;)Ly7/f;

    return-void
.end method

.method public static final synthetic a(Lx5/b;)Landroid/app/Activity;
    .locals 0

    iget-object p0, p0, Lx5/b;->a:Landroid/app/Activity;

    return-object p0
.end method


# virtual methods
.method public final b(Ll8/p;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ll8/p<",
            "-",
            "Ljava/lang/Integer;",
            "-",
            "Ljava/lang/String;",
            "La8/v;",
            ">;)V"
        }
    .end annotation

    const-string v0, "callback"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lx5/b;->b:Ly7/f;

    new-instance v1, Lx5/b$b;

    invoke-direct {v1, p1, p0}, Lx5/b$b;-><init>(Ll8/p;Lx5/b;)V

    invoke-virtual {v0, v1}, Ly7/f;->r0(Ly7/f$o;)Ly7/f;

    iget-object p1, p0, Lx5/b;->b:Ly7/f;

    invoke-virtual {p1}, Ly7/f;->x0()V

    return-void
.end method
