.class public final Lx5/h$c;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lx5/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lx5/h$c$a;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lm8/g;)V
    .locals 0

    invoke-direct {p0}, Lx5/h$c;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Lx5/h$d;
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lf6/m;

    invoke-direct {v0, p1}, Lf6/m;-><init>(Landroid/content/Context;)V

    invoke-static {}, Lcom/android/packageinstaller/compat/UserHandleCompat;->myUserId()I

    move-result p1

    invoke-virtual {v0, p1}, Lf6/m;->g(I)I

    move-result p1

    const/high16 v0, 0x10000

    if-eq p1, v0, :cond_1

    const/high16 v0, 0x20000

    if-eq p1, v0, :cond_0

    const/high16 v0, 0x30000

    if-eq p1, v0, :cond_0

    sget-object p1, Lx5/h$d;->b:Lx5/h$d;

    goto :goto_0

    :cond_0
    sget-object p1, Lx5/h$d;->a:Lx5/h$d;

    goto :goto_0

    :cond_1
    sget-object p1, Lx5/h$d;->c:Lx5/h$d;

    :goto_0
    return-object p1
.end method

.method public final b(Lx5/h$d;)I
    .locals 1

    const-string v0, "type"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lx5/h$c$a;->b:[I

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_4

    const/4 v0, 0x2

    if-eq p1, v0, :cond_4

    const/4 v0, 0x3

    if-eq p1, v0, :cond_2

    const/4 v0, 0x4

    if-eq p1, v0, :cond_1

    const/4 v0, 0x5

    if-ne p1, v0, :cond_0

    const p1, 0x7f0d0070

    goto :goto_0

    :cond_0
    new-instance p1, La8/k;

    invoke-direct {p1}, La8/k;-><init>()V

    throw p1

    :cond_1
    const p1, 0x7f0d0072

    goto :goto_0

    :cond_2
    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result p1

    if-eqz p1, :cond_3

    const p1, 0x7f0d008c

    goto :goto_0

    :cond_3
    const p1, 0x7f0d008b

    goto :goto_0

    :cond_4
    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result p1

    if-eqz p1, :cond_5

    const p1, 0x7f0d0094

    goto :goto_0

    :cond_5
    const p1, 0x7f0d0093

    :goto_0
    return p1
.end method

.method public final c(Lx5/a;)Z
    .locals 2

    const-string v0, "type"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Lcom/android/packageinstaller/InstallerApplication;->g()Lcom/android/packageinstaller/InstallerApplication;

    move-result-object v0

    invoke-static {v0}, Lh6/c;->b(Landroid/content/Context;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    sget-object v0, Lx5/h$c$a;->a:[I

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_3

    const/4 v0, 0x2

    if-eq p1, v0, :cond_2

    const/4 v0, 0x3

    if-eq p1, v0, :cond_1

    return v1

    :cond_1
    invoke-static {}, Lcom/android/packageinstaller/InstallerApplication;->g()Lcom/android/packageinstaller/InstallerApplication;

    move-result-object p1

    invoke-static {p1}, Lg6/c;->m(Landroid/content/Context;)Lg6/c;

    move-result-object p1

    invoke-virtual {p1}, Lg6/c;->r()Z

    move-result p1

    goto :goto_0

    :cond_2
    new-instance p1, Lh6/a;

    invoke-static {}, Lcom/android/packageinstaller/InstallerApplication;->g()Lcom/android/packageinstaller/InstallerApplication;

    move-result-object v0

    invoke-direct {p1, v0}, Lh6/a;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1}, Lh6/a;->b()Z

    move-result p1

    :goto_0
    return p1

    :cond_3
    return v0
.end method

.method public final d(Lx5/h;Landroid/view/ViewGroup;Ll8/l;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lx5/h;",
            "Landroid/view/ViewGroup;",
            "Ll8/l<",
            "-",
            "Ljava/lang/Integer;",
            "La8/v;",
            ">;)V"
        }
    .end annotation

    const-string v0, "dialog"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "container"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "callback"

    invoke-static {p3, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    const-string v4, "context"

    invoke-static {v2, v4}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v2}, Lx5/h$c;->a(Landroid/content/Context;)Lx5/h$d;

    move-result-object v2

    invoke-virtual {p0, v2}, Lx5/h$c;->b(Lx5/h$d;)I

    move-result v2

    invoke-virtual {v3, v2, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    invoke-virtual {p2, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    invoke-virtual {p2}, Landroid/view/ViewGroup;->requestLayout()V

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getWidth()I

    move-result v3

    const-string v4, "null cannot be cast to non-null type com.miui.packageInstaller.secure.IPassWord"

    invoke-static {v2, v4}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v4, v2

    check-cast v4, Lx5/c;

    invoke-interface {v4, p1, p3}, Lx5/c;->d(Lx5/h;Ll8/l;)V

    int-to-float p1, v3

    neg-float p3, p1

    invoke-virtual {v2, p3}, Landroid/view/View;->setTranslationX(F)V

    const/4 v3, 0x2

    new-array v4, v3, [F

    const/4 v5, 0x0

    aput v5, v4, v0

    const/4 v6, 0x1

    aput p3, v4, v6

    const-string p3, "translationX"

    invoke-static {v1, p3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    new-instance v7, Lmb/l;

    invoke-direct {v7}, Lmb/l;-><init>()V

    invoke-virtual {v4, v7}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    const-wide/16 v7, 0xe7

    invoke-virtual {v4, v7, v8}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    invoke-virtual {v4}, Landroid/animation/ObjectAnimator;->start()V

    new-instance v9, Lx5/h$c$b;

    invoke-direct {v9, p2, v1}, Lx5/h$c$b;-><init>(Landroid/view/ViewGroup;Landroid/view/View;)V

    invoke-virtual {v4, v9}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    new-array p2, v3, [F

    aput p1, p2, v0

    aput v5, p2, v6

    invoke-static {v2, p3, p2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object p1

    new-instance p2, Lmb/l;

    invoke-direct {p2}, Lmb/l;-><init>()V

    invoke-virtual {p1, p2}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    invoke-virtual {p1, v7, v8}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    invoke-virtual {p1}, Landroid/animation/ObjectAnimator;->start()V

    return-void
.end method
