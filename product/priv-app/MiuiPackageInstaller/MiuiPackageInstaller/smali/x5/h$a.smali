.class final Lx5/h$a;
.super La6/e;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lx5/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "a"
.end annotation


# instance fields
.field private final k:Landroid/content/Context;

.field private l:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const-string v0, "mContext"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const v0, 0x7f1205c5

    invoke-direct {p0, p1, v0}, La6/e;-><init>(Landroid/content/Context;I)V

    iput-object p1, p0, Lx5/h$a;->k:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public final g(Landroid/view/View;)V
    .locals 1

    const-string v0, "view"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lx5/h$a;->l:Landroid/view/View;

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    iget-object p1, p0, Lx5/h$a;->l:Landroid/view/View;

    if-eqz p1, :cond_0

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    :cond_0
    iget-object p1, p0, Lx5/h$a;->k:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const v0, 0x7f070106

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iget-object v0, p0, Lx5/h$a;->l:Landroid/view/View;

    if-eqz v0, :cond_1

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p1, p1, v1}, Landroid/view/View;->setPadding(IIII)V

    :cond_1
    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result p1

    if-eqz p1, :cond_2

    iget-object p1, p0, Lx5/h$a;->l:Landroid/view/View;

    if-eqz p1, :cond_2

    const v0, 0x7f080143

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    :cond_2
    return-void
.end method
