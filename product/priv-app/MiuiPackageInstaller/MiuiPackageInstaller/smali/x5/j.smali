.class public final Lx5/j;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lx5/j$b;,
        Lx5/j$a;,
        Lx5/j$c;
    }
.end annotation


# static fields
.field public static final l:Lx5/j$b;


# instance fields
.field private final a:Landroid/app/Activity;

.field private b:Z

.field private c:Ljava/lang/CharSequence;

.field private d:Ljava/lang/CharSequence;

.field private e:Ljava/lang/CharSequence;

.field private f:Ljava/lang/CharSequence;

.field private g:Ljava/lang/CharSequence;

.field private h:Lx5/h$d;

.field private i:Landroid/view/View;

.field private j:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lx5/a;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ll8/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ll8/l<",
            "-",
            "Lx5/a;",
            "La8/v;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lx5/j$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lx5/j$b;-><init>(Lm8/g;)V

    sput-object v0, Lx5/j;->l:Lx5/j$b;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;)V
    .locals 1

    const-string v0, "mActivity"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lx5/j;->a:Landroid/app/Activity;

    sget-object p1, Lx5/h$d;->b:Lx5/h$d;

    iput-object p1, p0, Lx5/j;->h:Lx5/h$d;

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lx5/j;->j:Ljava/util/ArrayList;

    return-void
.end method

.method public static synthetic a(Lx5/h$b;Ll8/l;)V
    .locals 0

    invoke-static {p0, p1}, Lx5/j;->n(Lx5/h$b;Ll8/l;)V

    return-void
.end method

.method public static final synthetic b(Lx5/j;)Ljava/util/ArrayList;
    .locals 0

    iget-object p0, p0, Lx5/j;->j:Ljava/util/ArrayList;

    return-object p0
.end method

.method public static final synthetic c(Lx5/j;Ljava/lang/CharSequence;)V
    .locals 0

    iput-object p1, p0, Lx5/j;->e:Ljava/lang/CharSequence;

    return-void
.end method

.method public static final synthetic d(Lx5/j;Ljava/lang/CharSequence;)V
    .locals 0

    iput-object p1, p0, Lx5/j;->d:Ljava/lang/CharSequence;

    return-void
.end method

.method public static final synthetic e(Lx5/j;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lx5/j;->i:Landroid/view/View;

    return-void
.end method

.method public static final synthetic f(Lx5/j;Ljava/lang/CharSequence;)V
    .locals 0

    iput-object p1, p0, Lx5/j;->g:Ljava/lang/CharSequence;

    return-void
.end method

.method public static final synthetic g(Lx5/j;Ll8/l;)V
    .locals 0

    iput-object p1, p0, Lx5/j;->k:Ll8/l;

    return-void
.end method

.method public static final synthetic h(Lx5/j;Ljava/lang/CharSequence;)V
    .locals 0

    iput-object p1, p0, Lx5/j;->f:Ljava/lang/CharSequence;

    return-void
.end method

.method public static final synthetic i(Lx5/j;Ljava/lang/CharSequence;)V
    .locals 0

    iput-object p1, p0, Lx5/j;->c:Ljava/lang/CharSequence;

    return-void
.end method

.method public static final synthetic j(Lx5/j;Lx5/h$d;)V
    .locals 0

    iput-object p1, p0, Lx5/j;->h:Lx5/h$d;

    return-void
.end method

.method public static final synthetic k(Lx5/j;Z)V
    .locals 0

    iput-boolean p1, p0, Lx5/j;->b:Z

    return-void
.end method

.method private final l(Ll8/p;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ll8/p<",
            "-",
            "Lx5/a;",
            "-",
            "Ljava/lang/Integer;",
            "La8/v;",
            ">;)V"
        }
    .end annotation

    new-instance v3, Lx5/j$d;

    invoke-direct {v3, p1}, Lx5/j$d;-><init>(Ll8/p;)V

    iget-object p1, p0, Lx5/j;->g:Ljava/lang/CharSequence;

    if-nez p1, :cond_0

    const-string p1, ""

    goto :goto_0

    :cond_0
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    :goto_0
    move-object v4, p1

    iget-boolean p1, p0, Lx5/j;->b:Z

    if-eqz p1, :cond_1

    new-instance p1, Lt5/k;

    invoke-direct {p1}, Lt5/k;-><init>()V

    iget-object v0, p0, Lx5/j;->a:Landroid/app/Activity;

    invoke-virtual {p1, v0, v3, v4}, Lt5/k;->i(Landroid/app/Activity;Lt5/k$a;Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    invoke-static {}, Lcom/android/packageinstaller/utils/a;->b()Lcom/android/packageinstaller/utils/a;

    move-result-object p1

    invoke-virtual {p1}, Lcom/android/packageinstaller/utils/a;->a()Landroid/accounts/Account;

    move-result-object v1

    if-eqz v1, :cond_2

    new-instance v0, Lt5/k;

    invoke-direct {v0}, Lt5/k;-><init>()V

    iget-object v2, p0, Lx5/j;->a:Landroid/app/Activity;

    const/4 v5, 0x0

    const/16 v6, 0x10

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lt5/k;->d(Lt5/k;Landroid/accounts/Account;Landroid/app/Activity;Lt5/k$a;Ljava/lang/String;Ljava/util/Map;ILjava/lang/Object;)V

    :cond_2
    :goto_1
    return-void
.end method

.method private final m(Lx5/a;Ll8/l;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lx5/a;",
            "Ll8/l<",
            "-",
            "Ljava/lang/Integer;",
            "La8/v;",
            ">;)V"
        }
    .end annotation

    new-instance v0, Lx5/h$b;

    iget-object v1, p0, Lx5/j;->a:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lx5/h$b;-><init>(Landroid/app/Activity;)V

    iget-object v1, p0, Lx5/j;->c:Ljava/lang/CharSequence;

    if-eqz v1, :cond_0

    invoke-virtual {v0, v1}, Lx5/h$b;->g(Ljava/lang/CharSequence;)Lx5/h$b;

    :cond_0
    iget-object v1, p0, Lx5/j;->d:Ljava/lang/CharSequence;

    if-eqz v1, :cond_1

    invoke-virtual {v0, v1}, Lx5/h$b;->c(Ljava/lang/CharSequence;)Lx5/h$b;

    :cond_1
    iget-object v1, p0, Lx5/j;->e:Ljava/lang/CharSequence;

    if-eqz v1, :cond_2

    invoke-virtual {v0, v1}, Lx5/h$b;->b(Ljava/lang/CharSequence;)Lx5/h$b;

    :cond_2
    iget-object v1, p0, Lx5/j;->f:Ljava/lang/CharSequence;

    if-eqz v1, :cond_3

    invoke-virtual {v0, v1}, Lx5/h$b;->f(Ljava/lang/CharSequence;)Lx5/h$b;

    :cond_3
    iget-object v1, p0, Lx5/j;->g:Ljava/lang/CharSequence;

    if-eqz v1, :cond_4

    invoke-virtual {v0, v1}, Lx5/h$b;->e(Ljava/lang/CharSequence;)Lx5/h$b;

    :cond_4
    iget-object v1, p0, Lx5/j;->i:Landroid/view/View;

    if-eqz v1, :cond_5

    invoke-virtual {v0, v1}, Lx5/h$b;->d(Landroid/view/View;)Lx5/h$b;

    :cond_5
    invoke-virtual {v0, p1}, Lx5/h$b;->h(Lx5/a;)Lx5/h$b;

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object p1

    new-instance v1, Lx5/i;

    invoke-direct {v1, v0, p2}, Lx5/i;-><init>(Lx5/h$b;Ll8/l;)V

    invoke-virtual {p1, v1}, Lf6/z;->e(Ljava/lang/Runnable;)V

    return-void
.end method

.method private static final n(Lx5/h$b;Ll8/l;)V
    .locals 1

    const-string v0, "$builder"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$callBack"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lx5/h$b;->a()Lx5/h;

    move-result-object p0

    invoke-virtual {p0, p1}, Lx5/h;->m(Ll8/l;)V

    return-void
.end method

.method private final o(Lx5/a;)Z
    .locals 2

    sget-object v0, Lx5/j$c;->a:[I

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    iget-boolean p1, p0, Lx5/j;->b:Z

    if-nez p1, :cond_2

    invoke-static {}, Lcom/android/packageinstaller/utils/a;->b()Lcom/android/packageinstaller/utils/a;

    move-result-object p1

    invoke-virtual {p1}, Lcom/android/packageinstaller/utils/a;->a()Landroid/accounts/Account;

    move-result-object p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    sget-object v0, Lx5/h;->k:Lx5/h$c;

    invoke-virtual {v0, p1}, Lx5/h$c;->c(Lx5/a;)Z

    move-result v1

    :cond_2
    :goto_0
    return v1
.end method

.method private final q(Lx5/a;Ll8/p;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lx5/a;",
            "Ll8/p<",
            "-",
            "Lx5/a;",
            "-",
            "Ljava/lang/Integer;",
            "La8/v;",
            ">;)V"
        }
    .end annotation

    new-instance v0, Lx5/j$e;

    invoke-direct {v0, p2, p1}, Lx5/j$e;-><init>(Ll8/p;Lx5/a;)V

    sget-object v1, Lx5/j$c;->a:[I

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result v2

    aget v1, v1, v2

    const/4 v2, 0x1

    if-eq v1, v2, :cond_2

    const/4 v2, 0x2

    if-eq v1, v2, :cond_1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_1

    const/4 v2, 0x4

    if-eq v1, v2, :cond_1

    const/4 p1, 0x5

    if-eq v1, p1, :cond_0

    goto :goto_0

    :cond_0
    sget-object p1, Lx5/a;->g:Lx5/a;

    const/4 v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p2, p1, v0}, Ll8/p;->g(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    invoke-direct {p0, p1, v0}, Lx5/j;->m(Lx5/a;Ll8/l;)V

    goto :goto_0

    :cond_2
    invoke-direct {p0, p2}, Lx5/j;->l(Ll8/p;)V

    :goto_0
    return-void
.end method


# virtual methods
.method public final p(Ll8/p;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ll8/p<",
            "-",
            "Lx5/a;",
            "-",
            "Ljava/lang/Integer;",
            "La8/v;",
            ">;)V"
        }
    .end annotation

    const-string v0, "callBack"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lx5/j;->j:Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lx5/a;

    invoke-direct {p0, v1}, Lx5/j;->o(Lx5/a;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v0, p0, Lx5/j;->k:Ll8/l;

    if-eqz v0, :cond_1

    invoke-interface {v0, v1}, Ll8/l;->j(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    invoke-direct {p0, v1, p1}, Lx5/j;->q(Lx5/a;Ll8/p;)V

    return-void

    :cond_2
    sget-object v0, Lx5/a;->g:Lx5/a;

    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Ll8/p;->g(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
