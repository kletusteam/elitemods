.class public final Lx5/d;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lx5/d$a;,
        Lx5/d$b;
    }
.end annotation


# static fields
.field public static final b:Lx5/d$a;

.field private static final c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lx5/a;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Landroid/app/Activity;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lx5/d$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lx5/d$a;-><init>(Lm8/g;)V

    sput-object v0, Lx5/d;->b:Lx5/d$a;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sget-object v1, Lx5/a;->f:Lx5/a;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v1, Lx5/a;->d:Lx5/a;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v1, Lx5/a;->e:Lx5/a;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v1, Lx5/a;->c:Lx5/a;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sput-object v0, Lx5/d;->c:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;)V
    .locals 1

    const-string v0, "mActivity"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lx5/d;->a:Landroid/app/Activity;

    return-void
.end method

.method public static final synthetic a()Ljava/util/ArrayList;
    .locals 1

    sget-object v0, Lx5/d;->c:Ljava/util/ArrayList;

    return-object v0
.end method

.method public static final synthetic b(Lx5/d;)V
    .locals 0

    invoke-direct {p0}, Lx5/d;->e()V

    return-void
.end method

.method private final c(Lx5/j$a;Ll8/p;Lx5/a;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lx5/j$a;",
            "Ll8/p<",
            "-",
            "Lx5/a;",
            "-",
            "Ljava/lang/Integer;",
            "La8/v;",
            ">;",
            "Lx5/a;",
            ")V"
        }
    .end annotation

    sget-object v0, Lx5/a;->g:Lx5/a;

    if-ne p3, v0, :cond_0

    const/4 p1, -0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {p2, p3, p1}, Ll8/p;->g(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void

    :cond_0
    sget-object v0, Lx5/d$b;->a:[I

    invoke-virtual {p3}, Ljava/lang/Enum;->ordinal()I

    move-result p3

    aget p3, v0, p3

    const/4 v0, 0x0

    const/4 v1, 0x4

    const/4 v2, 0x3

    const/4 v3, 0x2

    const/4 v4, 0x1

    if-eq p3, v4, :cond_5

    if-eq p3, v3, :cond_4

    if-eq p3, v2, :cond_3

    if-eq p3, v1, :cond_2

    const/4 v5, 0x5

    if-eq p3, v5, :cond_1

    goto/16 :goto_0

    :cond_1
    new-array p3, v1, [Lx5/a;

    sget-object v1, Lx5/a;->c:Lx5/a;

    aput-object v1, p3, v0

    sget-object v0, Lx5/a;->f:Lx5/a;

    aput-object v0, p3, v4

    sget-object v0, Lx5/a;->d:Lx5/a;

    aput-object v0, p3, v3

    sget-object v0, Lx5/a;->e:Lx5/a;

    aput-object v0, p3, v2

    invoke-virtual {p1, p3}, Lx5/j$a;->d([Lx5/a;)Lx5/j$a;

    goto/16 :goto_0

    :cond_2
    new-array p3, v1, [Lx5/a;

    sget-object v1, Lx5/a;->c:Lx5/a;

    aput-object v1, p3, v0

    sget-object v0, Lx5/a;->f:Lx5/a;

    aput-object v0, p3, v4

    sget-object v0, Lx5/a;->d:Lx5/a;

    aput-object v0, p3, v3

    sget-object v0, Lx5/a;->e:Lx5/a;

    aput-object v0, p3, v2

    invoke-virtual {p1, p3}, Lx5/j$a;->d([Lx5/a;)Lx5/j$a;

    goto :goto_0

    :cond_3
    new-array p3, v1, [Lx5/a;

    sget-object v1, Lx5/a;->e:Lx5/a;

    aput-object v1, p3, v0

    sget-object v0, Lx5/a;->f:Lx5/a;

    aput-object v0, p3, v4

    sget-object v0, Lx5/a;->d:Lx5/a;

    aput-object v0, p3, v3

    sget-object v0, Lx5/a;->c:Lx5/a;

    aput-object v0, p3, v2

    invoke-virtual {p1, p3}, Lx5/j$a;->d([Lx5/a;)Lx5/j$a;

    goto :goto_0

    :cond_4
    new-array p3, v1, [Lx5/a;

    sget-object v1, Lx5/a;->d:Lx5/a;

    aput-object v1, p3, v0

    sget-object v0, Lx5/a;->f:Lx5/a;

    aput-object v0, p3, v4

    sget-object v0, Lx5/a;->e:Lx5/a;

    aput-object v0, p3, v3

    sget-object v0, Lx5/a;->c:Lx5/a;

    aput-object v0, p3, v2

    invoke-virtual {p1, p3}, Lx5/j$a;->d([Lx5/a;)Lx5/j$a;

    goto :goto_0

    :cond_5
    iget-object p3, p0, Lx5/d;->a:Landroid/app/Activity;

    invoke-static {p3}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->isSafeModelEnable(Landroid/content/Context;)Z

    move-result p3

    if-eqz p3, :cond_6

    sget-object p3, Lf6/r;->a:Lf6/r$a;

    iget-object v5, p0, Lx5/d;->a:Landroid/app/Activity;

    invoke-virtual {p3, v5}, Lf6/r$a;->b(Landroid/content/Context;)Z

    move-result p3

    if-eqz p3, :cond_6

    new-array p3, v1, [Lx5/a;

    sget-object v1, Lx5/a;->d:Lx5/a;

    aput-object v1, p3, v0

    sget-object v0, Lx5/a;->e:Lx5/a;

    aput-object v0, p3, v4

    sget-object v0, Lx5/a;->c:Lx5/a;

    aput-object v0, p3, v3

    sget-object v0, Lx5/a;->f:Lx5/a;

    aput-object v0, p3, v2

    invoke-virtual {p1, p3}, Lx5/j$a;->d([Lx5/a;)Lx5/j$a;

    goto :goto_0

    :cond_6
    new-array p3, v1, [Lx5/a;

    sget-object v1, Lx5/a;->f:Lx5/a;

    aput-object v1, p3, v0

    sget-object v0, Lx5/a;->d:Lx5/a;

    aput-object v0, p3, v4

    sget-object v0, Lx5/a;->e:Lx5/a;

    aput-object v0, p3, v3

    sget-object v0, Lx5/a;->c:Lx5/a;

    aput-object v0, p3, v2

    invoke-virtual {p1, p3}, Lx5/j$a;->d([Lx5/a;)Lx5/j$a;

    :goto_0
    invoke-virtual {p1}, Lx5/j$a;->a()Lx5/j;

    move-result-object p1

    new-instance p3, Lx5/d$c;

    invoke-direct {p3, p0, p2}, Lx5/d$c;-><init>(Lx5/d;Ll8/p;)V

    invoke-virtual {p1, p3}, Lx5/j;->p(Ll8/p;)V

    return-void
.end method

.method private final e()V
    .locals 2

    sget-object v0, Lf6/r;->a:Lf6/r$a;

    iget-object v1, p0, Lx5/d;->a:Landroid/app/Activity;

    invoke-virtual {v0, v1}, Lf6/r$a;->f(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public final d()Landroid/app/Activity;
    .locals 1

    iget-object v0, p0, Lx5/d;->a:Landroid/app/Activity;

    return-object v0
.end method

.method public final f(Lcom/miui/packageInstaller/model/ApkInfo;Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;Ll8/p;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/miui/packageInstaller/model/ApkInfo;",
            "Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;",
            "Ll8/p<",
            "-",
            "Lx5/a;",
            "-",
            "Ljava/lang/Integer;",
            "La8/v;",
            ">;)V"
        }
    .end annotation

    const-string v0, "authorizeInfo"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "callback"

    invoke-static {p3, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/miui/packageInstaller/model/ApkInfo;->getLabel()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_0
    move-object v1, v0

    :goto_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/miui/packageInstaller/model/ApkInfo;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object p1

    goto :goto_1

    :cond_1
    move-object p1, v0

    :goto_1
    if-eqz p1, :cond_2

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lx5/d;->a:Landroid/app/Activity;

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f0d006d

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    const v2, 0x7f0a0073

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    invoke-virtual {v2, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    const p1, 0x7f0a0074

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const p1, 0x7f0a01ad

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iget-object v1, p0, Lx5/d;->a:Landroid/app/Activity;

    const v2, 0x7f1100de

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {p2}, Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;->getCallerLabel()Ljava/lang/String;

    move-result-object p2

    aput-object p2, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    new-instance p1, Lx5/j$a;

    iget-object p2, p0, Lx5/d;->a:Landroid/app/Activity;

    invoke-direct {p1, p2}, Lx5/j$a;-><init>(Landroid/app/Activity;)V

    const p2, 0x7f11030e

    invoke-virtual {p1, p2}, Lx5/j$a;->l(I)Lx5/j$a;

    move-result-object p1

    const p2, 0x7f1103e3

    invoke-virtual {p1, p2}, Lx5/j$a;->h(I)Lx5/j$a;

    move-result-object p1

    const p2, 0x7f110186

    invoke-virtual {p1, p2}, Lx5/j$a;->j(I)Lx5/j$a;

    move-result-object p1

    const p2, 0x7f1100b1

    invoke-virtual {p1, p2}, Lx5/j$a;->f(I)Lx5/j$a;

    move-result-object p1

    const p2, 0x7f110055

    invoke-virtual {p1, p2}, Lx5/j$a;->e(I)Lx5/j$a;

    move-result-object p1

    if-eqz v0, :cond_3

    invoke-virtual {p1, v0}, Lx5/j$a;->g(Landroid/view/View;)Lx5/j$a;

    :cond_3
    sget-object p2, Lx5/d;->b:Lx5/d$a;

    invoke-virtual {p2}, Lx5/d$a;->c()Lx5/a;

    move-result-object p2

    invoke-direct {p0, p1, p3, p2}, Lx5/d;->c(Lx5/j$a;Ll8/p;Lx5/a;)V

    return-void
.end method

.method public final g(Ll8/p;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ll8/p<",
            "-",
            "Lx5/a;",
            "-",
            "Ljava/lang/Integer;",
            "La8/v;",
            ">;)V"
        }
    .end annotation

    const-string v0, "callback"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lx5/j$a;

    iget-object v1, p0, Lx5/d;->a:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lx5/j$a;-><init>(Landroid/app/Activity;)V

    const v1, 0x7f11030e

    invoke-virtual {v0, v1}, Lx5/j$a;->l(I)Lx5/j$a;

    move-result-object v0

    const v1, 0x7f1103e3

    invoke-virtual {v0, v1}, Lx5/j$a;->h(I)Lx5/j$a;

    move-result-object v0

    const v1, 0x7f1100ae

    invoke-virtual {v0, v1}, Lx5/j$a;->f(I)Lx5/j$a;

    move-result-object v0

    const v1, 0x7f1100ad

    invoke-virtual {v0, v1}, Lx5/j$a;->e(I)Lx5/j$a;

    move-result-object v0

    sget-object v1, Lx5/d;->b:Lx5/d$a;

    invoke-virtual {v1}, Lx5/d$a;->c()Lx5/a;

    move-result-object v1

    invoke-direct {p0, v0, p1, v1}, Lx5/d;->c(Lx5/j$a;Ll8/p;Lx5/a;)V

    return-void
.end method

.method public final h(Ll8/p;Lx5/a;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ll8/p<",
            "-",
            "Lx5/a;",
            "-",
            "Ljava/lang/Integer;",
            "La8/v;",
            ">;",
            "Lx5/a;",
            ")V"
        }
    .end annotation

    const-string v0, "callback"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "type"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lx5/j$a;

    iget-object v1, p0, Lx5/d;->a:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lx5/j$a;-><init>(Landroid/app/Activity;)V

    const v1, 0x7f11030e

    invoke-virtual {v0, v1}, Lx5/j$a;->l(I)Lx5/j$a;

    move-result-object v0

    const v1, 0x7f1103e3

    invoke-virtual {v0, v1}, Lx5/j$a;->h(I)Lx5/j$a;

    move-result-object v0

    const v1, 0x7f1100ae

    invoke-virtual {v0, v1}, Lx5/j$a;->f(I)Lx5/j$a;

    move-result-object v0

    const v1, 0x7f1100ad

    invoke-virtual {v0, v1}, Lx5/j$a;->e(I)Lx5/j$a;

    move-result-object v0

    invoke-direct {p0, v0, p1, p2}, Lx5/d;->c(Lx5/j$a;Ll8/p;Lx5/a;)V

    return-void
.end method
