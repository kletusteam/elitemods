.class public Lr7/a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lr7/a$a;
    }
.end annotation


# static fields
.field private static a:Landroid/content/Context;

.field private static b:Lr7/a$a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lmicloud/compat/independent/request/RequestEnvBuilderCompat;->build()Lr7/a$a;

    move-result-object v0

    sput-object v0, Lr7/a;->b:Lr7/a$a;

    return-void
.end method

.method public static a()Landroid/content/Context;
    .locals 2

    sget-object v0, Lr7/a;->a:Landroid/content/Context;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "sContext=null! Please call Request.init(Context) at Application onCreate"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
