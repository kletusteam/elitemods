.class Lk1/c$a;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lk1/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field private a:Ll1/b;

.field private b:I

.field final synthetic c:Lk1/c;


# direct methods
.method constructor <init>(Lk1/c;Ll1/b;I)V
    .locals 0

    iput-object p1, p0, Lk1/c$a;->c:Lk1/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lk1/c$a;->a:Ll1/b;

    iput p3, p0, Lk1/c$a;->b:I

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    :cond_0
    const-wide/16 v0, 0x3e8

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    iget v0, p0, Lk1/c$a;->b:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lk1/c$a;->b:I

    if-gtz v0, :cond_0

    if-gtz v0, :cond_1

    iget-object v0, p0, Lk1/c$a;->c:Lk1/c;

    iget-object v1, p0, Lk1/c$a;->a:Ll1/b;

    invoke-static {v0, v1}, Lk1/c;->d(Lk1/c;Ll1/b;)V

    iget-object v0, p0, Lk1/c$a;->c:Lk1/c;

    invoke-static {v0}, Lk1/c;->a(Lk1/c;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lk1/c$a;->c:Lk1/c;

    invoke-static {v1}, Lk1/c;->h(Lk1/c;)Ll1/a;

    move-result-object v1

    iget-object v2, p0, Lk1/c$a;->c:Lk1/c;

    invoke-static {v2}, Lk1/c;->l(Lk1/c;)Ljava/util/List;

    move-result-object v2

    invoke-static {v0, v1, v2}, Ln1/a;->b(Landroid/content/Context;Ll1/a;Ljava/util/List;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_1
    const-string v1, "CrashDefend"

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :goto_0
    throw v0

    :catch_1
    :cond_1
    :goto_1
    return-void
.end method
