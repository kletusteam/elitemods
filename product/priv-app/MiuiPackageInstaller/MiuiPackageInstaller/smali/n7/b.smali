.class public Ln7/b;
.super Ljava/lang/Object;


# instance fields
.field private a:Landroid/content/Context;

.field private b:Ljava/lang/String;

.field private c:Lcom/miui/packageInstaller/model/IncrementPackageInfo;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ln7/a;

.field private g:J

.field private h:Landroid/app/DownloadManager;

.field private i:Landroid/content/BroadcastReceiver;

.field private j:Landroid/database/ContentObserver;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/miui/packageInstaller/model/IncrementPackageInfo;Ln7/a;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Ln7/b;->a:Landroid/content/Context;

    iput-object p2, p0, Ln7/b;->b:Ljava/lang/String;

    iput-object p3, p0, Ln7/b;->c:Lcom/miui/packageInstaller/model/IncrementPackageInfo;

    iput-object p4, p0, Ln7/b;->f:Ln7/a;

    new-instance p1, Ln7/b$a;

    const/4 p2, 0x0

    invoke-direct {p1, p0, p2}, Ln7/b$a;-><init>(Ln7/b;Landroid/os/Handler;)V

    iput-object p1, p0, Ln7/b;->j:Landroid/database/ContentObserver;

    new-instance p1, Ln7/b$b;

    invoke-direct {p1, p0}, Ln7/b$b;-><init>(Ln7/b;)V

    iput-object p1, p0, Ln7/b;->i:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic a(Ln7/b;)V
    .locals 0

    invoke-direct {p0}, Ln7/b;->i()V

    return-void
.end method

.method static synthetic b(Ln7/b;)V
    .locals 0

    invoke-direct {p0}, Ln7/b;->h()V

    return-void
.end method

.method private c()V
    .locals 4

    iget-object v0, p0, Ln7/b;->a:Landroid/content/Context;

    const-string v1, "download"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/DownloadManager;

    iput-object v0, p0, Ln7/b;->h:Landroid/app/DownloadManager;

    new-instance v0, Landroid/app/DownloadManager$Request;

    iget-object v1, p0, Ln7/b;->c:Lcom/miui/packageInstaller/model/IncrementPackageInfo;

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/IncrementPackageInfo;->getPatchUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/DownloadManager$Request;-><init>(Landroid/net/Uri;)V

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/app/DownloadManager$Request;->setNotificationVisibility(I)Landroid/app/DownloadManager$Request;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/DownloadManager$Request;->setVisibleInDownloadsUi(Z)Landroid/app/DownloadManager$Request;

    move-result-object v0

    iget-object v1, p0, Ln7/b;->a:Landroid/content/Context;

    sget-object v2, Landroid/os/Environment;->DIRECTORY_DOWNLOADS:Ljava/lang/String;

    invoke-direct {p0}, Ln7/b;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/DownloadManager$Request;->setDestinationInExternalFilesDir(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/app/DownloadManager$Request;

    move-result-object v0

    iget-object v1, p0, Ln7/b;->c:Lcom/miui/packageInstaller/model/IncrementPackageInfo;

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/IncrementPackageInfo;->getPatchFileSize()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/android/packageinstaller/compat/DownloadManagerCompat;->setFileSize(Landroid/app/DownloadManager$Request;J)Landroid/app/DownloadManager$Request;

    iget-object v1, p0, Ln7/b;->f:Ln7/a;

    if-eqz v1, :cond_0

    invoke-interface {v1}, Ln7/a;->e()V

    :cond_0
    iget-object v1, p0, Ln7/b;->h:Landroid/app/DownloadManager;

    invoke-virtual {v1, v0}, Landroid/app/DownloadManager;->enqueue(Landroid/app/DownloadManager$Request;)J

    move-result-wide v0

    iput-wide v0, p0, Ln7/b;->g:J

    iget-object v0, p0, Ln7/b;->j:Landroid/database/ContentObserver;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ln7/b;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://downloads/my_downloads"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    iget-object v3, p0, Ln7/b;->j:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    :cond_1
    return-void
.end method

.method private e()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Ln7/b;->c:Lcom/miui/packageInstaller/model/IncrementPackageInfo;

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/IncrementPackageInfo;->getCurrentPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Ln7/b;->c:Lcom/miui/packageInstaller/model/IncrementPackageInfo;

    invoke-virtual {v2}, Lcom/miui/packageInstaller/model/IncrementPackageInfo;->getCurrentVersionName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Ln7/b;->c:Lcom/miui/packageInstaller/model/IncrementPackageInfo;

    invoke-virtual {v2}, Lcom/miui/packageInstaller/model/IncrementPackageInfo;->getCurrentVersionCode()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Ln7/b;->c:Lcom/miui/packageInstaller/model/IncrementPackageInfo;

    invoke-virtual {v2}, Lcom/miui/packageInstaller/model/IncrementPackageInfo;->getTargetPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Ln7/b;->c:Lcom/miui/packageInstaller/model/IncrementPackageInfo;

    invoke-virtual {v2}, Lcom/miui/packageInstaller/model/IncrementPackageInfo;->getTargetVersionName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Ln7/b;->c:Lcom/miui/packageInstaller/model/IncrementPackageInfo;

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/IncrementPackageInfo;->getTargetVersionCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ".patch"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private f()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Ln7/b;->c:Lcom/miui/packageInstaller/model/IncrementPackageInfo;

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/IncrementPackageInfo;->getTargetPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Ln7/b;->c:Lcom/miui/packageInstaller/model/IncrementPackageInfo;

    invoke-virtual {v2}, Lcom/miui/packageInstaller/model/IncrementPackageInfo;->getTargetVersionName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Ln7/b;->c:Lcom/miui/packageInstaller/model/IncrementPackageInfo;

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/IncrementPackageInfo;->getTargetVersionCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ".apk"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private g(I)V
    .locals 3

    iget-object v0, p0, Ln7/b;->f:Ln7/a;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v1, 0x1

    if-eq p1, v1, :cond_2

    const/4 v1, 0x2

    if-eq p1, v1, :cond_2

    const/4 v1, 0x3

    if-eq p1, v1, :cond_2

    const/4 v1, 0x4

    const/16 v2, -0x3e8

    if-eq p1, v1, :cond_1

    packed-switch p1, :pswitch_data_0

    packed-switch p1, :pswitch_data_1

    goto :goto_1

    :pswitch_0
    const/16 p1, -0x3ec

    goto :goto_0

    :pswitch_1
    const/16 p1, -0x3ed

    goto :goto_0

    :pswitch_2
    const/16 p1, -0x3eb

    goto :goto_0

    :pswitch_3
    const/16 p1, -0x3ea

    :goto_0
    invoke-interface {v0, p1}, Ln7/a;->g(I)V

    goto :goto_2

    :cond_1
    :goto_1
    :pswitch_4
    invoke-interface {v0, v2}, Ln7/a;->g(I)V

    goto :goto_2

    :cond_2
    :pswitch_5
    invoke-interface {v0}, Ln7/a;->c()V

    :goto_2
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x3e8
        :pswitch_4
        :pswitch_4
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x3ec
        :pswitch_5
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method

.method private h()V
    .locals 8

    const-string v0, ""

    const/4 v1, 0x0

    :try_start_0
    iget-object v2, p0, Ln7/b;->h:Landroid/app/DownloadManager;

    new-instance v3, Landroid/app/DownloadManager$Query;

    invoke-direct {v3}, Landroid/app/DownloadManager$Query;-><init>()V

    const/4 v4, 0x1

    new-array v4, v4, [J

    const/4 v5, 0x0

    iget-wide v6, p0, Ln7/b;->g:J

    aput-wide v6, v4, v5

    invoke-virtual {v3, v4}, Landroid/app/DownloadManager$Query;->setFilterById([J)Landroid/app/DownloadManager$Query;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/DownloadManager;->query(Landroid/app/DownloadManager$Query;)Landroid/database/Cursor;

    move-result-object v1

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    const-string v2, "local_uri"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {v1}, Lcom/android/packageinstaller/utils/k;->a(Ljava/lang/AutoCloseable;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    goto/16 :goto_2

    :catch_0
    move-exception v2

    :try_start_1
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-static {v1}, Lcom/android/packageinstaller/utils/k;->a(Ljava/lang/AutoCloseable;)V

    move-object v2, v0

    :goto_0
    const-string v1, "ReplacePkg"

    if-nez v2, :cond_0

    const-string v0, "uriString: cursor getString is null"

    invoke-static {v1, v0}, Lf6/o;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    return-void

    :cond_0
    new-instance v3, Ljava/io/File;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "patchFile path: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lf6/o;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "patchFile size: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lf6/o;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    iget-object v2, p0, Ln7/b;->f:Ln7/a;

    if-eqz v2, :cond_1

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4}, Ln7/a;->d(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v4

    iget-object v2, p0, Ln7/b;->c:Lcom/miui/packageInstaller/model/IncrementPackageInfo;

    invoke-virtual {v2}, Lcom/miui/packageInstaller/model/IncrementPackageInfo;->getPatchFileSize()J

    move-result-wide v6

    cmp-long v2, v4, v6

    if-eqz v2, :cond_3

    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    iget-object v0, p0, Ln7/b;->f:Ln7/a;

    if-eqz v0, :cond_2

    const/16 v1, -0x3f0

    invoke-interface {v0, v1}, Ln7/a;->g(I)V

    :cond_2
    return-void

    :cond_3
    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/packageinstaller/utils/h;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Ln7/b;->c:Lcom/miui/packageInstaller/model/IncrementPackageInfo;

    invoke-virtual {v4}, Lcom/miui/packageInstaller/model/IncrementPackageInfo;->getPatchMD5()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    iget-object v0, p0, Ln7/b;->f:Ln7/a;

    if-eqz v0, :cond_4

    const/16 v1, -0x3f1

    invoke-interface {v0, v1}, Ln7/a;->g(I)V

    :cond_4
    return-void

    :cond_5
    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Ln7/b;->d:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ln7/b;->e:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Ln7/b;->e:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ln7/b;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mOldApkPath: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Ln7/b;->b:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lf6/o;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mPatchPath: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Ln7/b;->d:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lf6/o;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "targetApkPath: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lf6/o;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    iget-object v1, p0, Ln7/b;->b:Ljava/lang/String;

    iget-object v2, p0, Ln7/b;->d:Ljava/lang/String;

    invoke-static {v1, v0, v2}, Lcom/xiaomi/market/data/Patcher;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v0}, Lcom/android/packageinstaller/utils/h;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Ln7/b;->c:Lcom/miui/packageInstaller/model/IncrementPackageInfo;

    invoke-virtual {v2}, Lcom/miui/packageInstaller/model/IncrementPackageInfo;->getTargetMD5()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_8

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    goto :goto_1

    :cond_6
    iget-object v1, p0, Ln7/b;->f:Ln7/a;

    if-eqz v1, :cond_7

    invoke-interface {v1, v0}, Ln7/a;->b(Ljava/lang/String;)V

    :cond_7
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    return-void

    :cond_8
    :goto_1
    iget-object v0, p0, Ln7/b;->f:Ln7/a;

    if-eqz v0, :cond_9

    const/16 v1, -0x3f2

    invoke-interface {v0, v1}, Ln7/a;->h(I)V

    :cond_9
    return-void

    :goto_2
    invoke-static {v1}, Lcom/android/packageinstaller/utils/k;->a(Ljava/lang/AutoCloseable;)V

    throw v0
.end method

.method private i()V
    .locals 8

    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    :try_start_0
    iget-object v3, p0, Ln7/b;->h:Landroid/app/DownloadManager;

    new-instance v4, Landroid/app/DownloadManager$Query;

    invoke-direct {v4}, Landroid/app/DownloadManager$Query;-><init>()V

    new-array v5, v1, [J

    iget-wide v6, p0, Ln7/b;->g:J

    aput-wide v6, v5, v0

    invoke-virtual {v4, v5}, Landroid/app/DownloadManager$Query;->setFilterById([J)Landroid/app/DownloadManager$Query;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/DownloadManager;->query(Landroid/app/DownloadManager$Query;)Landroid/database/Cursor;

    move-result-object v2

    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_4

    const-string v3, "status"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    const-string v4, "reason"

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    if-eq v3, v1, :cond_4

    const/4 v5, 0x2

    if-eq v3, v5, :cond_3

    const/4 v5, 0x4

    if-eq v3, v5, :cond_1

    const/16 v5, 0x8

    if-eq v3, v5, :cond_4

    const/16 v5, 0x10

    if-eq v3, v5, :cond_0

    iget-object v3, p0, Ln7/b;->f:Ln7/a;

    const/16 v4, -0x3e8

    invoke-interface {v3, v4}, Ln7/a;->g(I)V

    iget-object v3, p0, Ln7/b;->h:Landroid/app/DownloadManager;

    new-array v4, v1, [J

    iget-wide v5, p0, Ln7/b;->g:J

    aput-wide v5, v4, v0

    invoke-virtual {v3, v4}, Landroid/app/DownloadManager;->remove([J)I

    goto :goto_0

    :cond_0
    invoke-direct {p0, v4}, Ln7/b;->g(I)V

    goto :goto_0

    :cond_1
    const/4 v3, 0x3

    if-ne v4, v3, :cond_2

    iget-object v3, p0, Ln7/b;->f:Ln7/a;

    invoke-interface {v3, v4}, Ln7/a;->a(I)V

    goto :goto_0

    :cond_2
    invoke-direct {p0, v4}, Ln7/b;->g(I)V

    iget-object v3, p0, Ln7/b;->h:Landroid/app/DownloadManager;

    new-array v4, v1, [J

    iget-wide v5, p0, Ln7/b;->g:J

    aput-wide v5, v4, v0

    invoke-virtual {v3, v4}, Landroid/app/DownloadManager;->remove([J)I

    goto :goto_0

    :cond_3
    const-string v3, "bytes_so_far"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    const-string v5, "total_size"

    invoke-interface {v2, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v2, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    iget-object v7, p0, Ln7/b;->f:Ln7/a;

    invoke-interface {v7, v3, v4, v5, v6}, Ln7/a;->f(JJ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    goto :goto_1

    :catch_0
    const/4 v3, -0x1

    :try_start_1
    invoke-direct {p0, v3}, Ln7/b;->g(I)V

    iget-object v3, p0, Ln7/b;->h:Landroid/app/DownloadManager;

    new-array v1, v1, [J

    iget-wide v4, p0, Ln7/b;->g:J

    aput-wide v4, v1, v0

    invoke-virtual {v3, v1}, Landroid/app/DownloadManager;->remove([J)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_4
    :goto_0
    invoke-static {v2}, Lcom/android/packageinstaller/utils/k;->a(Ljava/lang/AutoCloseable;)V

    return-void

    :goto_1
    invoke-static {v2}, Lcom/android/packageinstaller/utils/k;->a(Ljava/lang/AutoCloseable;)V

    throw v0
.end method


# virtual methods
.method public d()V
    .locals 4

    iget-object v0, p0, Ln7/b;->a:Landroid/content/Context;

    if-eqz v0, :cond_8

    iget-object v0, p0, Ln7/b;->c:Lcom/miui/packageInstaller/model/IncrementPackageInfo;

    if-nez v0, :cond_0

    goto :goto_1

    :cond_0
    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/IncrementPackageInfo;->getPatchUrl()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Ln7/b;->f:Ln7/a;

    if-eqz v0, :cond_1

    const/16 v1, -0x3ee

    invoke-interface {v0, v1}, Ln7/a;->g(I)V

    :cond_1
    return-void

    :cond_2
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Ln7/b;->b:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Ln7/b;->b:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_3

    goto :goto_0

    :cond_3
    invoke-static {}, Lq2/e;->a()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Ln7/b;->f:Ln7/a;

    if-eqz v0, :cond_4

    invoke-interface {v0}, Ln7/a;->c()V

    :cond_4
    return-void

    :cond_5
    iget-object v0, p0, Ln7/b;->a:Landroid/content/Context;

    iget-object v1, p0, Ln7/b;->i:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.intent.action.DOWNLOAD_COMPLETE"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-direct {p0}, Ln7/b;->c()V

    return-void

    :cond_6
    :goto_0
    iget-object v0, p0, Ln7/b;->f:Ln7/a;

    if-eqz v0, :cond_7

    const/16 v1, -0x3ef

    invoke-interface {v0, v1}, Ln7/a;->g(I)V

    :cond_7
    return-void

    :cond_8
    :goto_1
    iget-object v0, p0, Ln7/b;->f:Ln7/a;

    if-eqz v0, :cond_9

    const/16 v1, -0x3e8

    invoke-interface {v0, v1}, Ln7/a;->g(I)V

    :cond_9
    return-void
.end method
