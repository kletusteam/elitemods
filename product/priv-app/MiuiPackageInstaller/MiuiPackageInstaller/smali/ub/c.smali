.class public final Lub/c;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lub/c$a;,
        Lub/c$b;
    }
.end annotation


# instance fields
.field private a:Z

.field private final b:Lub/f;

.field private final c:Lub/e;

.field private final d:Lpb/p;

.field private final e:Lub/d;

.field private final f:Lvb/d;


# direct methods
.method public constructor <init>(Lub/e;Lpb/p;Lub/d;Lvb/d;)V
    .locals 1

    const-string v0, "call"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "eventListener"

    invoke-static {p2, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "finder"

    invoke-static {p3, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "codec"

    invoke-static {p4, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lub/c;->c:Lub/e;

    iput-object p2, p0, Lub/c;->d:Lpb/p;

    iput-object p3, p0, Lub/c;->e:Lub/d;

    iput-object p4, p0, Lub/c;->f:Lvb/d;

    invoke-interface {p4}, Lvb/d;->h()Lub/f;

    move-result-object p1

    iput-object p1, p0, Lub/c;->b:Lub/f;

    return-void
.end method

.method private final s(Ljava/io/IOException;)V
    .locals 2

    iget-object v0, p0, Lub/c;->e:Lub/d;

    invoke-virtual {v0, p1}, Lub/d;->i(Ljava/io/IOException;)V

    iget-object v0, p0, Lub/c;->f:Lvb/d;

    invoke-interface {v0}, Lvb/d;->h()Lub/f;

    move-result-object v0

    iget-object v1, p0, Lub/c;->c:Lub/e;

    invoke-virtual {v0, v1, p1}, Lub/f;->I(Lub/e;Ljava/io/IOException;)V

    return-void
.end method


# virtual methods
.method public final a(JZZLjava/io/IOException;)Ljava/io/IOException;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/io/IOException;",
            ">(JZZTE;)TE;"
        }
    .end annotation

    if-eqz p5, :cond_0

    invoke-direct {p0, p5}, Lub/c;->s(Ljava/io/IOException;)V

    :cond_0
    if-eqz p4, :cond_2

    iget-object v0, p0, Lub/c;->d:Lpb/p;

    iget-object v1, p0, Lub/c;->c:Lub/e;

    if-eqz p5, :cond_1

    invoke-virtual {v0, v1, p5}, Lpb/p;->p(Lpb/e;Ljava/io/IOException;)V

    goto :goto_0

    :cond_1
    invoke-virtual {v0, v1, p1, p2}, Lpb/p;->n(Lpb/e;J)V

    :cond_2
    :goto_0
    if-eqz p3, :cond_4

    if-eqz p5, :cond_3

    iget-object p1, p0, Lub/c;->d:Lpb/p;

    iget-object p2, p0, Lub/c;->c:Lub/e;

    invoke-virtual {p1, p2, p5}, Lpb/p;->u(Lpb/e;Ljava/io/IOException;)V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lub/c;->d:Lpb/p;

    iget-object v1, p0, Lub/c;->c:Lub/e;

    invoke-virtual {v0, v1, p1, p2}, Lpb/p;->s(Lpb/e;J)V

    :cond_4
    :goto_1
    iget-object p1, p0, Lub/c;->c:Lub/e;

    invoke-virtual {p1, p0, p4, p3, p5}, Lub/e;->r(Lub/c;ZZLjava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    return-object p1
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lub/c;->f:Lvb/d;

    invoke-interface {v0}, Lvb/d;->cancel()V

    return-void
.end method

.method public final c(Lpb/z;Z)Ldc/w;
    .locals 3

    const-string v0, "request"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    iput-boolean p2, p0, Lub/c;->a:Z

    invoke-virtual {p1}, Lpb/z;->a()Lpb/a0;

    move-result-object p2

    if-nez p2, :cond_0

    invoke-static {}, Lm8/i;->o()V

    :cond_0
    invoke-virtual {p2}, Lpb/a0;->a()J

    move-result-wide v0

    iget-object p2, p0, Lub/c;->d:Lpb/p;

    iget-object v2, p0, Lub/c;->c:Lub/e;

    invoke-virtual {p2, v2}, Lpb/p;->o(Lpb/e;)V

    iget-object p2, p0, Lub/c;->f:Lvb/d;

    invoke-interface {p2, p1, v0, v1}, Lvb/d;->f(Lpb/z;J)Ldc/w;

    move-result-object p1

    new-instance p2, Lub/c$a;

    invoke-direct {p2, p0, p1, v0, v1}, Lub/c$a;-><init>(Lub/c;Ldc/w;J)V

    return-object p2
.end method

.method public final d()V
    .locals 3

    iget-object v0, p0, Lub/c;->f:Lvb/d;

    invoke-interface {v0}, Lvb/d;->cancel()V

    iget-object v0, p0, Lub/c;->c:Lub/e;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, p0, v1, v1, v2}, Lub/e;->r(Lub/c;ZZLjava/io/IOException;)Ljava/io/IOException;

    return-void
.end method

.method public final e()V
    .locals 3

    :try_start_0
    iget-object v0, p0, Lub/c;->f:Lvb/d;

    invoke-interface {v0}, Lvb/d;->c()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Lub/c;->d:Lpb/p;

    iget-object v2, p0, Lub/c;->c:Lub/e;

    invoke-virtual {v1, v2, v0}, Lpb/p;->p(Lpb/e;Ljava/io/IOException;)V

    invoke-direct {p0, v0}, Lub/c;->s(Ljava/io/IOException;)V

    throw v0
.end method

.method public final f()V
    .locals 3

    :try_start_0
    iget-object v0, p0, Lub/c;->f:Lvb/d;

    invoke-interface {v0}, Lvb/d;->d()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Lub/c;->d:Lpb/p;

    iget-object v2, p0, Lub/c;->c:Lub/e;

    invoke-virtual {v1, v2, v0}, Lpb/p;->p(Lpb/e;Ljava/io/IOException;)V

    invoke-direct {p0, v0}, Lub/c;->s(Ljava/io/IOException;)V

    throw v0
.end method

.method public final g()Lub/e;
    .locals 1

    iget-object v0, p0, Lub/c;->c:Lub/e;

    return-object v0
.end method

.method public final h()Lub/f;
    .locals 1

    iget-object v0, p0, Lub/c;->b:Lub/f;

    return-object v0
.end method

.method public final i()Lpb/p;
    .locals 1

    iget-object v0, p0, Lub/c;->d:Lpb/p;

    return-object v0
.end method

.method public final j()Lub/d;
    .locals 1

    iget-object v0, p0, Lub/c;->e:Lub/d;

    return-object v0
.end method

.method public final k()Z
    .locals 2

    iget-object v0, p0, Lub/c;->e:Lub/d;

    invoke-virtual {v0}, Lub/d;->e()Lpb/a;

    move-result-object v0

    invoke-virtual {v0}, Lpb/a;->l()Lpb/t;

    move-result-object v0

    invoke-virtual {v0}, Lpb/t;->i()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lub/c;->b:Lub/f;

    invoke-virtual {v1}, Lub/f;->b()Lpb/d0;

    move-result-object v1

    invoke-virtual {v1}, Lpb/d0;->a()Lpb/a;

    move-result-object v1

    invoke-virtual {v1}, Lpb/a;->l()Lpb/t;

    move-result-object v1

    invoke-virtual {v1}, Lpb/t;->i()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public final l()Z
    .locals 1

    iget-boolean v0, p0, Lub/c;->a:Z

    return v0
.end method

.method public final m()V
    .locals 1

    iget-object v0, p0, Lub/c;->f:Lvb/d;

    invoke-interface {v0}, Lvb/d;->h()Lub/f;

    move-result-object v0

    invoke-virtual {v0}, Lub/f;->A()V

    return-void
.end method

.method public final n()V
    .locals 4

    iget-object v0, p0, Lub/c;->c:Lub/e;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, p0, v1, v2, v3}, Lub/e;->r(Lub/c;ZZLjava/io/IOException;)Ljava/io/IOException;

    return-void
.end method

.method public final o(Lpb/b0;)Lpb/c0;
    .locals 4

    const-string v0, "response"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    :try_start_0
    const-string v0, "Content-Type"

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {p1, v0, v2, v1, v2}, Lpb/b0;->y(Lpb/b0;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lub/c;->f:Lvb/d;

    invoke-interface {v1, p1}, Lvb/d;->a(Lpb/b0;)J

    move-result-wide v1

    iget-object v3, p0, Lub/c;->f:Lvb/d;

    invoke-interface {v3, p1}, Lvb/d;->e(Lpb/b0;)Ldc/y;

    move-result-object p1

    new-instance v3, Lub/c$b;

    invoke-direct {v3, p0, p1, v1, v2}, Lub/c$b;-><init>(Lub/c;Ldc/y;J)V

    new-instance p1, Lvb/h;

    invoke-static {v3}, Ldc/o;->b(Ldc/y;)Ldc/g;

    move-result-object v3

    invoke-direct {p1, v0, v1, v2, v3}, Lvb/h;-><init>(Ljava/lang/String;JLdc/g;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    iget-object v0, p0, Lub/c;->d:Lpb/p;

    iget-object v1, p0, Lub/c;->c:Lub/e;

    invoke-virtual {v0, v1, p1}, Lpb/p;->u(Lpb/e;Ljava/io/IOException;)V

    invoke-direct {p0, p1}, Lub/c;->s(Ljava/io/IOException;)V

    throw p1
.end method

.method public final p(Z)Lpb/b0$a;
    .locals 2

    :try_start_0
    iget-object v0, p0, Lub/c;->f:Lvb/d;

    invoke-interface {v0, p1}, Lvb/d;->g(Z)Lpb/b0$a;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p1, p0}, Lpb/b0$a;->l(Lub/c;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-object p1

    :catch_0
    move-exception p1

    iget-object v0, p0, Lub/c;->d:Lpb/p;

    iget-object v1, p0, Lub/c;->c:Lub/e;

    invoke-virtual {v0, v1, p1}, Lpb/p;->u(Lpb/e;Ljava/io/IOException;)V

    invoke-direct {p0, p1}, Lub/c;->s(Ljava/io/IOException;)V

    throw p1
.end method

.method public final q(Lpb/b0;)V
    .locals 2

    const-string v0, "response"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lub/c;->d:Lpb/p;

    iget-object v1, p0, Lub/c;->c:Lub/e;

    invoke-virtual {v0, v1, p1}, Lpb/p;->v(Lpb/e;Lpb/b0;)V

    return-void
.end method

.method public final r()V
    .locals 2

    iget-object v0, p0, Lub/c;->d:Lpb/p;

    iget-object v1, p0, Lub/c;->c:Lub/e;

    invoke-virtual {v0, v1}, Lpb/p;->w(Lpb/e;)V

    return-void
.end method

.method public final t(Lpb/z;)V
    .locals 2

    const-string v0, "request"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    :try_start_0
    iget-object v0, p0, Lub/c;->d:Lpb/p;

    iget-object v1, p0, Lub/c;->c:Lub/e;

    invoke-virtual {v0, v1}, Lpb/p;->r(Lpb/e;)V

    iget-object v0, p0, Lub/c;->f:Lvb/d;

    invoke-interface {v0, p1}, Lvb/d;->b(Lpb/z;)V

    iget-object v0, p0, Lub/c;->d:Lpb/p;

    iget-object v1, p0, Lub/c;->c:Lub/e;

    invoke-virtual {v0, v1, p1}, Lpb/p;->q(Lpb/e;Lpb/z;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    iget-object v0, p0, Lub/c;->d:Lpb/p;

    iget-object v1, p0, Lub/c;->c:Lub/e;

    invoke-virtual {v0, v1, p1}, Lpb/p;->p(Lpb/e;Ljava/io/IOException;)V

    invoke-direct {p0, p1}, Lub/c;->s(Ljava/io/IOException;)V

    throw p1
.end method
