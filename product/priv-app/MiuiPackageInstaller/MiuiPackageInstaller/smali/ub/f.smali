.class public final Lub/f;
.super Lxb/f$d;

# interfaces
.implements Lpb/i;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lub/f$a;
    }
.end annotation


# static fields
.field public static final t:Lub/f$a;


# instance fields
.field private c:Ljava/net/Socket;

.field private d:Ljava/net/Socket;

.field private e:Lpb/r;

.field private f:Lpb/y;

.field private g:Lxb/f;

.field private h:Ldc/g;

.field private i:Ldc/f;

.field private j:Z

.field private k:Z

.field private l:I

.field private m:I

.field private n:I

.field private o:I

.field private final p:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/ref/Reference<",
            "Lub/e;",
            ">;>;"
        }
    .end annotation
.end field

.field private q:J

.field private final r:Lub/h;

.field private final s:Lpb/d0;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lub/f$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lub/f$a;-><init>(Lm8/g;)V

    sput-object v0, Lub/f;->t:Lub/f$a;

    return-void
.end method

.method public constructor <init>(Lub/h;Lpb/d0;)V
    .locals 1

    const-string v0, "connectionPool"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "route"

    invoke-static {p2, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lxb/f$d;-><init>()V

    iput-object p1, p0, Lub/f;->r:Lub/h;

    iput-object p2, p0, Lub/f;->s:Lpb/d0;

    const/4 p1, 0x1

    iput p1, p0, Lub/f;->o:I

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lub/f;->p:Ljava/util/List;

    const-wide p1, 0x7fffffffffffffffL

    iput-wide p1, p0, Lub/f;->q:J

    return-void
.end method

.method private final B(Ljava/util/List;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lpb/d0;",
            ">;)Z"
        }
    .end annotation

    instance-of v0, p1, Ljava/util/Collection;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v1, v2

    goto :goto_1

    :cond_1
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpb/d0;

    invoke-virtual {v0}, Lpb/d0;->b()Ljava/net/Proxy;

    move-result-object v3

    invoke-virtual {v3}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v3

    sget-object v4, Ljava/net/Proxy$Type;->DIRECT:Ljava/net/Proxy$Type;

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lub/f;->s:Lpb/d0;

    invoke-virtual {v3}, Lpb/d0;->b()Ljava/net/Proxy;

    move-result-object v3

    invoke-virtual {v3}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v3

    sget-object v4, Ljava/net/Proxy$Type;->DIRECT:Ljava/net/Proxy$Type;

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lub/f;->s:Lpb/d0;

    invoke-virtual {v3}, Lpb/d0;->d()Ljava/net/InetSocketAddress;

    move-result-object v3

    invoke-virtual {v0}, Lpb/d0;->d()Ljava/net/InetSocketAddress;

    move-result-object v0

    invoke-static {v3, v0}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v2

    :goto_0
    if-eqz v0, :cond_2

    :goto_1
    return v1
.end method

.method private final G(I)V
    .locals 7

    iget-object v0, p0, Lub/f;->d:Ljava/net/Socket;

    if-nez v0, :cond_0

    invoke-static {}, Lm8/i;->o()V

    :cond_0
    iget-object v1, p0, Lub/f;->h:Ldc/g;

    if-nez v1, :cond_1

    invoke-static {}, Lm8/i;->o()V

    :cond_1
    iget-object v2, p0, Lub/f;->i:Ldc/f;

    if-nez v2, :cond_2

    invoke-static {}, Lm8/i;->o()V

    :cond_2
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Ljava/net/Socket;->setSoTimeout(I)V

    new-instance v4, Lxb/f$b;

    sget-object v5, Ltb/d;->h:Ltb/d;

    const/4 v6, 0x1

    invoke-direct {v4, v6, v5}, Lxb/f$b;-><init>(ZLtb/d;)V

    iget-object v5, p0, Lub/f;->s:Lpb/d0;

    invoke-virtual {v5}, Lpb/d0;->a()Lpb/a;

    move-result-object v5

    invoke-virtual {v5}, Lpb/a;->l()Lpb/t;

    move-result-object v5

    invoke-virtual {v5}, Lpb/t;->i()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v0, v5, v1, v2}, Lxb/f$b;->m(Ljava/net/Socket;Ljava/lang/String;Ldc/g;Ldc/f;)Lxb/f$b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lxb/f$b;->k(Lxb/f$d;)Lxb/f$b;

    move-result-object v0

    invoke-virtual {v0, p1}, Lxb/f$b;->l(I)Lxb/f$b;

    move-result-object p1

    invoke-virtual {p1}, Lxb/f$b;->a()Lxb/f;

    move-result-object p1

    iput-object p1, p0, Lub/f;->g:Lxb/f;

    sget-object v0, Lxb/f;->D:Lxb/f$c;

    invoke-virtual {v0}, Lxb/f$c;->a()Lxb/m;

    move-result-object v0

    invoke-virtual {v0}, Lxb/m;->d()I

    move-result v0

    iput v0, p0, Lub/f;->o:I

    const/4 v0, 0x0

    invoke-static {p1, v3, v6, v0}, Lxb/f;->w0(Lxb/f;ZILjava/lang/Object;)V

    return-void
.end method

.method public static final synthetic e(Lub/f;)Lpb/r;
    .locals 0

    iget-object p0, p0, Lub/f;->e:Lpb/r;

    return-object p0
.end method

.method private final g(Lpb/t;Lpb/r;)Z
    .locals 3

    invoke-virtual {p2}, Lpb/r;->d()Ljava/util/List;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    sget-object v0, Lbc/d;->a:Lbc/d;

    invoke-virtual {p1}, Lpb/t;->i()Ljava/lang/String;

    move-result-object p1

    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    if-eqz p2, :cond_0

    check-cast p2, Ljava/security/cert/X509Certificate;

    invoke-virtual {v0, p1, p2}, Lbc/d;->c(Ljava/lang/String;Ljava/security/cert/X509Certificate;)Z

    move-result p1

    if-eqz p1, :cond_1

    goto :goto_0

    :cond_0
    new-instance p1, La8/s;

    const-string p2, "null cannot be cast to non-null type java.security.cert.X509Certificate"

    invoke-direct {p1, p2}, La8/s;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    move v1, v2

    :goto_0
    return v1
.end method

.method private final j(IILpb/e;Lpb/p;)V
    .locals 4

    iget-object v0, p0, Lub/f;->s:Lpb/d0;

    invoke-virtual {v0}, Lpb/d0;->b()Ljava/net/Proxy;

    move-result-object v0

    iget-object v1, p0, Lub/f;->s:Lpb/d0;

    invoke-virtual {v1}, Lpb/d0;->a()Lpb/a;

    move-result-object v1

    invoke-virtual {v0}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v2

    if-nez v2, :cond_0

    goto :goto_0

    :cond_0
    sget-object v3, Lub/g;->a:[I

    invoke-virtual {v2}, Ljava/net/Proxy$Type;->ordinal()I

    move-result v2

    aget v2, v3, v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_1

    const/4 v3, 0x2

    if-eq v2, v3, :cond_1

    :goto_0
    new-instance v1, Ljava/net/Socket;

    invoke-direct {v1, v0}, Ljava/net/Socket;-><init>(Ljava/net/Proxy;)V

    goto :goto_1

    :cond_1
    invoke-virtual {v1}, Lpb/a;->j()Ljavax/net/SocketFactory;

    move-result-object v1

    invoke-virtual {v1}, Ljavax/net/SocketFactory;->createSocket()Ljava/net/Socket;

    move-result-object v1

    if-nez v1, :cond_2

    invoke-static {}, Lm8/i;->o()V

    :cond_2
    :goto_1
    iput-object v1, p0, Lub/f;->c:Ljava/net/Socket;

    iget-object v2, p0, Lub/f;->s:Lpb/d0;

    invoke-virtual {v2}, Lpb/d0;->d()Ljava/net/InetSocketAddress;

    move-result-object v2

    invoke-virtual {p4, p3, v2, v0}, Lpb/p;->g(Lpb/e;Ljava/net/InetSocketAddress;Ljava/net/Proxy;)V

    invoke-virtual {v1, p2}, Ljava/net/Socket;->setSoTimeout(I)V

    :try_start_0
    sget-object p2, Lyb/h;->c:Lyb/h$a;

    invoke-virtual {p2}, Lyb/h$a;->e()Lyb/h;

    move-result-object p2

    iget-object p3, p0, Lub/f;->s:Lpb/d0;

    invoke-virtual {p3}, Lpb/d0;->d()Ljava/net/InetSocketAddress;

    move-result-object p3

    invoke-virtual {p2, v1, p3, p1}, Lyb/h;->h(Ljava/net/Socket;Ljava/net/InetSocketAddress;I)V
    :try_end_0
    .catch Ljava/net/ConnectException; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    invoke-static {v1}, Ldc/o;->f(Ljava/net/Socket;)Ldc/y;

    move-result-object p1

    invoke-static {p1}, Ldc/o;->b(Ldc/y;)Ldc/g;

    move-result-object p1

    iput-object p1, p0, Lub/f;->h:Ldc/g;

    invoke-static {v1}, Ldc/o;->d(Ljava/net/Socket;)Ldc/w;

    move-result-object p1

    invoke-static {p1}, Ldc/o;->a(Ldc/w;)Ldc/f;

    move-result-object p1

    iput-object p1, p0, Lub/f;->i:Ldc/f;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object p2

    const-string p3, "throw with null exception"

    invoke-static {p2, p3}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    if-nez p2, :cond_3

    :goto_2
    return-void

    :cond_3
    new-instance p2, Ljava/io/IOException;

    invoke-direct {p2, p1}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw p2

    :catch_1
    move-exception p1

    new-instance p2, Ljava/net/ConnectException;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string p4, "Failed to connect to "

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p4, p0, Lub/f;->s:Lpb/d0;

    invoke-virtual {p4}, Lpb/d0;->d()Ljava/net/InetSocketAddress;

    move-result-object p4

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-direct {p2, p3}, Ljava/net/ConnectException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, p1}, Ljava/net/ConnectException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    throw p2
.end method

.method private final k(Lub/b;)V
    .locals 10

    iget-object v0, p0, Lub/f;->s:Lpb/d0;

    invoke-virtual {v0}, Lpb/d0;->a()Lpb/a;

    move-result-object v0

    invoke-virtual {v0}, Lpb/a;->k()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v1

    const/4 v2, 0x0

    if-nez v1, :cond_0

    :try_start_0
    invoke-static {}, Lm8/i;->o()V

    :cond_0
    iget-object v3, p0, Lub/f;->c:Ljava/net/Socket;

    invoke-virtual {v0}, Lpb/a;->l()Lpb/t;

    move-result-object v4

    invoke-virtual {v4}, Lpb/t;->i()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lpb/a;->l()Lpb/t;

    move-result-object v5

    invoke-virtual {v5}, Lpb/t;->n()I

    move-result v5

    const/4 v6, 0x1

    invoke-virtual {v1, v3, v4, v5, v6}, Ljavax/net/ssl/SSLSocketFactory;->createSocket(Ljava/net/Socket;Ljava/lang/String;IZ)Ljava/net/Socket;

    move-result-object v1

    if-eqz v1, :cond_9

    check-cast v1, Ljavax/net/ssl/SSLSocket;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    invoke-virtual {p1, v1}, Lub/b;->a(Ljavax/net/ssl/SSLSocket;)Lpb/k;

    move-result-object p1

    invoke-virtual {p1}, Lpb/k;->h()Z

    move-result v3

    if-eqz v3, :cond_1

    sget-object v3, Lyb/h;->c:Lyb/h$a;

    invoke-virtual {v3}, Lyb/h$a;->e()Lyb/h;

    move-result-object v3

    invoke-virtual {v0}, Lpb/a;->l()Lpb/t;

    move-result-object v4

    invoke-virtual {v4}, Lpb/t;->i()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lpb/a;->f()Ljava/util/List;

    move-result-object v5

    invoke-virtual {v3, v1, v4, v5}, Lyb/h;->f(Ljavax/net/ssl/SSLSocket;Ljava/lang/String;Ljava/util/List;)V

    :cond_1
    invoke-virtual {v1}, Ljavax/net/ssl/SSLSocket;->startHandshake()V

    invoke-virtual {v1}, Ljavax/net/ssl/SSLSocket;->getSession()Ljavax/net/ssl/SSLSession;

    move-result-object v3

    sget-object v4, Lpb/r;->f:Lpb/r$a;

    const-string v5, "sslSocketSession"

    invoke-static {v3, v5}, Lm8/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Lpb/r$a;->a(Ljavax/net/ssl/SSLSession;)Lpb/r;

    move-result-object v4

    invoke-virtual {v0}, Lpb/a;->e()Ljavax/net/ssl/HostnameVerifier;

    move-result-object v5

    if-nez v5, :cond_2

    invoke-static {}, Lm8/i;->o()V

    :cond_2
    invoke-virtual {v0}, Lpb/a;->l()Lpb/t;

    move-result-object v7

    invoke-virtual {v7}, Lpb/t;->i()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v5, v7, v3}, Ljavax/net/ssl/HostnameVerifier;->verify(Ljava/lang/String;Ljavax/net/ssl/SSLSession;)Z

    move-result v3

    if-nez v3, :cond_5

    invoke-virtual {v4}, Lpb/r;->d()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v3

    xor-int/2addr v3, v6

    if-eqz v3, :cond_4

    const/4 v3, 0x0

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    if-nez p1, :cond_3

    new-instance p1, La8/s;

    const-string v0, "null cannot be cast to non-null type java.security.cert.X509Certificate"

    invoke-direct {p1, v0}, La8/s;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_3
    check-cast p1, Ljava/security/cert/X509Certificate;

    new-instance v3, Ljavax/net/ssl/SSLPeerUnverifiedException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "\n              |Hostname "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lpb/a;->l()Lpb/t;

    move-result-object v0

    invoke-virtual {v0}, Lpb/t;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " not verified:\n              |    certificate: "

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v0, Lpb/g;->d:Lpb/g$b;

    invoke-virtual {v0, p1}, Lpb/g$b;->a(Ljava/security/cert/Certificate;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\n              |    DN: "

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/security/cert/X509Certificate;->getSubjectDN()Ljava/security/Principal;

    move-result-object v0

    const-string v5, "cert.subjectDN"

    invoke-static {v0, v5}, Lm8/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/security/Principal;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\n              |    subjectAltNames: "

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v0, Lbc/d;->a:Lbc/d;

    invoke-virtual {v0, p1}, Lbc/d;->a(Ljava/security/cert/X509Certificate;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, "\n              "

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1, v2, v6, v2}, Lu8/g;->e(Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v3, p1}, Ljavax/net/ssl/SSLPeerUnverifiedException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_4
    new-instance p1, Ljavax/net/ssl/SSLPeerUnverifiedException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Hostname "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lpb/a;->l()Lpb/t;

    move-result-object v0

    invoke-virtual {v0}, Lpb/t;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " not verified (no certificates)"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljavax/net/ssl/SSLPeerUnverifiedException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_5
    invoke-virtual {v0}, Lpb/a;->a()Lpb/g;

    move-result-object v3

    if-nez v3, :cond_6

    invoke-static {}, Lm8/i;->o()V

    :cond_6
    new-instance v5, Lpb/r;

    invoke-virtual {v4}, Lpb/r;->e()Lpb/e0;

    move-result-object v6

    invoke-virtual {v4}, Lpb/r;->a()Lpb/h;

    move-result-object v7

    invoke-virtual {v4}, Lpb/r;->c()Ljava/util/List;

    move-result-object v8

    new-instance v9, Lub/f$b;

    invoke-direct {v9, v3, v4, v0}, Lub/f$b;-><init>(Lpb/g;Lpb/r;Lpb/a;)V

    invoke-direct {v5, v6, v7, v8, v9}, Lpb/r;-><init>(Lpb/e0;Lpb/h;Ljava/util/List;Ll8/a;)V

    iput-object v5, p0, Lub/f;->e:Lpb/r;

    invoke-virtual {v0}, Lpb/a;->l()Lpb/t;

    move-result-object v0

    invoke-virtual {v0}, Lpb/t;->i()Ljava/lang/String;

    move-result-object v0

    new-instance v4, Lub/f$c;

    invoke-direct {v4, p0}, Lub/f$c;-><init>(Lub/f;)V

    invoke-virtual {v3, v0, v4}, Lpb/g;->b(Ljava/lang/String;Ll8/a;)V

    invoke-virtual {p1}, Lpb/k;->h()Z

    move-result p1

    if-eqz p1, :cond_7

    sget-object p1, Lyb/h;->c:Lyb/h$a;

    invoke-virtual {p1}, Lyb/h$a;->e()Lyb/h;

    move-result-object p1

    invoke-virtual {p1, v1}, Lyb/h;->i(Ljavax/net/ssl/SSLSocket;)Ljava/lang/String;

    move-result-object v2

    :cond_7
    iput-object v1, p0, Lub/f;->d:Ljava/net/Socket;

    invoke-static {v1}, Ldc/o;->f(Ljava/net/Socket;)Ldc/y;

    move-result-object p1

    invoke-static {p1}, Ldc/o;->b(Ldc/y;)Ldc/g;

    move-result-object p1

    iput-object p1, p0, Lub/f;->h:Ldc/g;

    invoke-static {v1}, Ldc/o;->d(Ljava/net/Socket;)Ldc/w;

    move-result-object p1

    invoke-static {p1}, Ldc/o;->a(Ldc/w;)Ldc/f;

    move-result-object p1

    iput-object p1, p0, Lub/f;->i:Ldc/f;

    if-eqz v2, :cond_8

    sget-object p1, Lpb/y;->i:Lpb/y$a;

    invoke-virtual {p1, v2}, Lpb/y$a;->a(Ljava/lang/String;)Lpb/y;

    move-result-object p1

    goto :goto_0

    :cond_8
    sget-object p1, Lpb/y;->c:Lpb/y;

    :goto_0
    iput-object p1, p0, Lub/f;->f:Lpb/y;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    sget-object p1, Lyb/h;->c:Lyb/h$a;

    invoke-virtual {p1}, Lyb/h$a;->e()Lyb/h;

    move-result-object p1

    invoke-virtual {p1, v1}, Lyb/h;->b(Ljavax/net/ssl/SSLSocket;)V

    return-void

    :catchall_0
    move-exception p1

    move-object v2, v1

    goto :goto_1

    :cond_9
    :try_start_2
    new-instance p1, La8/s;

    const-string v0, "null cannot be cast to non-null type javax.net.ssl.SSLSocket"

    invoke-direct {p1, v0}, La8/s;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :catchall_1
    move-exception p1

    :goto_1
    if-eqz v2, :cond_a

    sget-object v0, Lyb/h;->c:Lyb/h$a;

    invoke-virtual {v0}, Lyb/h$a;->e()Lyb/h;

    move-result-object v0

    invoke-virtual {v0, v2}, Lyb/h;->b(Ljavax/net/ssl/SSLSocket;)V

    :cond_a
    if-eqz v2, :cond_b

    invoke-static {v2}, Lrb/b;->k(Ljava/net/Socket;)V

    :cond_b
    throw p1
.end method

.method private final l(IIILpb/e;Lpb/p;)V
    .locals 6

    invoke-direct {p0}, Lub/f;->n()Lpb/z;

    move-result-object v0

    invoke-virtual {v0}, Lpb/z;->j()Lpb/t;

    move-result-object v1

    const/4 v2, 0x0

    :goto_0
    const/16 v3, 0x15

    if-ge v2, v3, :cond_1

    invoke-direct {p0, p1, p2, p4, p5}, Lub/f;->j(IILpb/e;Lpb/p;)V

    invoke-direct {p0, p2, p3, v0, v1}, Lub/f;->m(IILpb/z;Lpb/t;)Lpb/z;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v3, p0, Lub/f;->c:Ljava/net/Socket;

    if-eqz v3, :cond_0

    invoke-static {v3}, Lrb/b;->k(Ljava/net/Socket;)V

    :cond_0
    const/4 v3, 0x0

    iput-object v3, p0, Lub/f;->c:Ljava/net/Socket;

    iput-object v3, p0, Lub/f;->i:Ldc/f;

    iput-object v3, p0, Lub/f;->h:Ldc/g;

    iget-object v4, p0, Lub/f;->s:Lpb/d0;

    invoke-virtual {v4}, Lpb/d0;->d()Ljava/net/InetSocketAddress;

    move-result-object v4

    iget-object v5, p0, Lub/f;->s:Lpb/d0;

    invoke-virtual {v5}, Lpb/d0;->b()Ljava/net/Proxy;

    move-result-object v5

    invoke-virtual {p5, p4, v4, v5, v3}, Lpb/p;->e(Lpb/e;Ljava/net/InetSocketAddress;Ljava/net/Proxy;Lpb/y;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private final m(IILpb/z;Lpb/t;)Lpb/z;
    .locals 9

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CONNECT "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v1, 0x1

    invoke-static {p4, v1}, Lrb/b;->K(Lpb/t;Z)Ljava/lang/String;

    move-result-object p4

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p4, " HTTP/1.1"

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p4

    :goto_0
    iget-object v0, p0, Lub/f;->h:Ldc/g;

    if-nez v0, :cond_0

    invoke-static {}, Lm8/i;->o()V

    :cond_0
    iget-object v2, p0, Lub/f;->i:Ldc/f;

    if-nez v2, :cond_1

    invoke-static {}, Lm8/i;->o()V

    :cond_1
    new-instance v3, Lwb/a;

    const/4 v4, 0x0

    invoke-direct {v3, v4, p0, v0, v2}, Lwb/a;-><init>(Lpb/x;Lub/f;Ldc/g;Ldc/f;)V

    invoke-interface {v0}, Ldc/y;->d()Ldc/z;

    move-result-object v5

    int-to-long v6, p1

    sget-object v8, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v5, v6, v7, v8}, Ldc/z;->g(JLjava/util/concurrent/TimeUnit;)Ldc/z;

    invoke-interface {v2}, Ldc/w;->d()Ldc/z;

    move-result-object v5

    int-to-long v6, p2

    invoke-virtual {v5, v6, v7, v8}, Ldc/z;->g(JLjava/util/concurrent/TimeUnit;)Ldc/z;

    invoke-virtual {p3}, Lpb/z;->e()Lpb/s;

    move-result-object v5

    invoke-virtual {v3, v5, p4}, Lwb/a;->C(Lpb/s;Ljava/lang/String;)V

    invoke-virtual {v3}, Lwb/a;->c()V

    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Lwb/a;->g(Z)Lpb/b0$a;

    move-result-object v5

    if-nez v5, :cond_2

    invoke-static {}, Lm8/i;->o()V

    :cond_2
    invoke-virtual {v5, p3}, Lpb/b0$a;->s(Lpb/z;)Lpb/b0$a;

    move-result-object p3

    invoke-virtual {p3}, Lpb/b0$a;->c()Lpb/b0;

    move-result-object p3

    invoke-virtual {v3, p3}, Lwb/a;->B(Lpb/b0;)V

    invoke-virtual {p3}, Lpb/b0;->n()I

    move-result v3

    const/16 v5, 0xc8

    if-eq v3, v5, :cond_6

    const/16 v0, 0x197

    if-ne v3, v0, :cond_5

    iget-object v0, p0, Lub/f;->s:Lpb/d0;

    invoke-virtual {v0}, Lpb/d0;->a()Lpb/a;

    move-result-object v0

    invoke-virtual {v0}, Lpb/a;->h()Lpb/b;

    move-result-object v0

    iget-object v2, p0, Lub/f;->s:Lpb/d0;

    invoke-interface {v0, v2, p3}, Lpb/b;->a(Lpb/d0;Lpb/b0;)Lpb/z;

    move-result-object v0

    if-eqz v0, :cond_4

    const/4 v2, 0x2

    const-string v3, "Connection"

    invoke-static {p3, v3, v4, v2, v4}, Lpb/b0;->y(Lpb/b0;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object p3

    const-string v2, "close"

    invoke-static {v2, p3, v1}, Lu8/g;->j(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result p3

    if-eqz p3, :cond_3

    return-object v0

    :cond_3
    move-object p3, v0

    goto :goto_0

    :cond_4
    new-instance p1, Ljava/io/IOException;

    const-string p2, "Failed to authenticate with proxy"

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_5
    new-instance p1, Ljava/io/IOException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p4, "Unexpected response code for CONNECT: "

    invoke-virtual {p2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Lpb/b0;->n()I

    move-result p3

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_6
    invoke-interface {v0}, Ldc/g;->c()Ldc/e;

    move-result-object p1

    invoke-virtual {p1}, Ldc/e;->u()Z

    move-result p1

    if-eqz p1, :cond_7

    invoke-interface {v2}, Ldc/f;->c()Ldc/e;

    move-result-object p1

    invoke-virtual {p1}, Ldc/e;->u()Z

    move-result p1

    if-eqz p1, :cond_7

    return-object v4

    :cond_7
    new-instance p1, Ljava/io/IOException;

    const-string p2, "TLS tunnel buffered too many bytes!"

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private final n()Lpb/z;
    .locals 4

    new-instance v0, Lpb/z$a;

    invoke-direct {v0}, Lpb/z$a;-><init>()V

    iget-object v1, p0, Lub/f;->s:Lpb/d0;

    invoke-virtual {v1}, Lpb/d0;->a()Lpb/a;

    move-result-object v1

    invoke-virtual {v1}, Lpb/a;->l()Lpb/t;

    move-result-object v1

    invoke-virtual {v0, v1}, Lpb/z$a;->k(Lpb/t;)Lpb/z$a;

    move-result-object v0

    const-string v1, "CONNECT"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lpb/z$a;->f(Ljava/lang/String;Lpb/a0;)Lpb/z$a;

    move-result-object v0

    iget-object v1, p0, Lub/f;->s:Lpb/d0;

    invoke-virtual {v1}, Lpb/d0;->a()Lpb/a;

    move-result-object v1

    invoke-virtual {v1}, Lpb/a;->l()Lpb/t;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lrb/b;->K(Lpb/t;Z)Ljava/lang/String;

    move-result-object v1

    const-string v2, "Host"

    invoke-virtual {v0, v2, v1}, Lpb/z$a;->d(Ljava/lang/String;Ljava/lang/String;)Lpb/z$a;

    move-result-object v0

    const-string v1, "Proxy-Connection"

    const-string v2, "Keep-Alive"

    invoke-virtual {v0, v1, v2}, Lpb/z$a;->d(Ljava/lang/String;Ljava/lang/String;)Lpb/z$a;

    move-result-object v0

    const-string v1, "User-Agent"

    const-string v2, "okhttp/4.4.1"

    invoke-virtual {v0, v1, v2}, Lpb/z$a;->d(Ljava/lang/String;Ljava/lang/String;)Lpb/z$a;

    move-result-object v0

    invoke-virtual {v0}, Lpb/z$a;->b()Lpb/z;

    move-result-object v0

    new-instance v1, Lpb/b0$a;

    invoke-direct {v1}, Lpb/b0$a;-><init>()V

    invoke-virtual {v1, v0}, Lpb/b0$a;->s(Lpb/z;)Lpb/b0$a;

    move-result-object v1

    sget-object v2, Lpb/y;->c:Lpb/y;

    invoke-virtual {v1, v2}, Lpb/b0$a;->p(Lpb/y;)Lpb/b0$a;

    move-result-object v1

    const/16 v2, 0x197

    invoke-virtual {v1, v2}, Lpb/b0$a;->g(I)Lpb/b0$a;

    move-result-object v1

    const-string v2, "Preemptive Authenticate"

    invoke-virtual {v1, v2}, Lpb/b0$a;->m(Ljava/lang/String;)Lpb/b0$a;

    move-result-object v1

    sget-object v2, Lrb/b;->c:Lpb/c0;

    invoke-virtual {v1, v2}, Lpb/b0$a;->b(Lpb/c0;)Lpb/b0$a;

    move-result-object v1

    const-wide/16 v2, -0x1

    invoke-virtual {v1, v2, v3}, Lpb/b0$a;->t(J)Lpb/b0$a;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Lpb/b0$a;->q(J)Lpb/b0$a;

    move-result-object v1

    const-string v2, "Proxy-Authenticate"

    const-string v3, "OkHttp-Preemptive"

    invoke-virtual {v1, v2, v3}, Lpb/b0$a;->j(Ljava/lang/String;Ljava/lang/String;)Lpb/b0$a;

    move-result-object v1

    invoke-virtual {v1}, Lpb/b0$a;->c()Lpb/b0;

    move-result-object v1

    iget-object v2, p0, Lub/f;->s:Lpb/d0;

    invoke-virtual {v2}, Lpb/d0;->a()Lpb/a;

    move-result-object v2

    invoke-virtual {v2}, Lpb/a;->h()Lpb/b;

    move-result-object v2

    iget-object v3, p0, Lub/f;->s:Lpb/d0;

    invoke-interface {v2, v3, v1}, Lpb/b;->a(Lpb/d0;Lpb/b0;)Lpb/z;

    move-result-object v1

    if-eqz v1, :cond_0

    move-object v0, v1

    :cond_0
    return-object v0
.end method

.method private final o(Lub/b;ILpb/e;Lpb/p;)V
    .locals 1

    iget-object v0, p0, Lub/f;->s:Lpb/d0;

    invoke-virtual {v0}, Lpb/d0;->a()Lpb/a;

    move-result-object v0

    invoke-virtual {v0}, Lpb/a;->k()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object p1, p0, Lub/f;->s:Lpb/d0;

    invoke-virtual {p1}, Lpb/d0;->a()Lpb/a;

    move-result-object p1

    invoke-virtual {p1}, Lpb/a;->f()Ljava/util/List;

    move-result-object p1

    sget-object p3, Lpb/y;->f:Lpb/y;

    invoke-interface {p1, p3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lub/f;->c:Ljava/net/Socket;

    iput-object p1, p0, Lub/f;->d:Ljava/net/Socket;

    iput-object p3, p0, Lub/f;->f:Lpb/y;

    invoke-direct {p0, p2}, Lub/f;->G(I)V

    return-void

    :cond_0
    iget-object p1, p0, Lub/f;->c:Ljava/net/Socket;

    iput-object p1, p0, Lub/f;->d:Ljava/net/Socket;

    sget-object p1, Lpb/y;->c:Lpb/y;

    iput-object p1, p0, Lub/f;->f:Lpb/y;

    return-void

    :cond_1
    invoke-virtual {p4, p3}, Lpb/p;->y(Lpb/e;)V

    invoke-direct {p0, p1}, Lub/f;->k(Lub/b;)V

    iget-object p1, p0, Lub/f;->e:Lpb/r;

    invoke-virtual {p4, p3, p1}, Lpb/p;->x(Lpb/e;Lpb/r;)V

    iget-object p1, p0, Lub/f;->f:Lpb/y;

    sget-object p3, Lpb/y;->e:Lpb/y;

    if-ne p1, p3, :cond_2

    invoke-direct {p0, p2}, Lub/f;->G(I)V

    :cond_2
    return-void
.end method


# virtual methods
.method public final A()V
    .locals 5

    iget-object v0, p0, Lub/f;->r:Lub/h;

    sget-boolean v1, Lrb/b;->h:Z

    if-eqz v1, :cond_1

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0

    :cond_0
    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Thread "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    const-string v4, "Thread.currentThread()"

    invoke-static {v3, v4}, Lm8/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " MUST NOT hold lock on "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    :cond_1
    :goto_0
    iget-object v0, p0, Lub/f;->r:Lub/h;

    monitor-enter v0

    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Lub/f;->j:Z

    sget-object v1, La8/v;->a:La8/v;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public final C(J)V
    .locals 0

    iput-wide p1, p0, Lub/f;->q:J

    return-void
.end method

.method public final D(Z)V
    .locals 0

    iput-boolean p1, p0, Lub/f;->j:Z

    return-void
.end method

.method public final E(I)V
    .locals 0

    iput p1, p0, Lub/f;->m:I

    return-void
.end method

.method public F()Ljava/net/Socket;
    .locals 1

    iget-object v0, p0, Lub/f;->d:Ljava/net/Socket;

    if-nez v0, :cond_0

    invoke-static {}, Lm8/i;->o()V

    :cond_0
    return-object v0
.end method

.method public final H(Lpb/t;)Z
    .locals 4

    const-string v0, "url"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lub/f;->s:Lpb/d0;

    invoke-virtual {v0}, Lpb/d0;->a()Lpb/a;

    move-result-object v0

    invoke-virtual {v0}, Lpb/a;->l()Lpb/t;

    move-result-object v0

    invoke-virtual {p1}, Lpb/t;->n()I

    move-result v1

    invoke-virtual {v0}, Lpb/t;->n()I

    move-result v2

    const/4 v3, 0x0

    if-eq v1, v2, :cond_0

    return v3

    :cond_0
    invoke-virtual {p1}, Lpb/t;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lpb/t;->i()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    return v1

    :cond_1
    iget-boolean v0, p0, Lub/f;->k:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lub/f;->e:Lpb/r;

    if-eqz v0, :cond_3

    if-nez v0, :cond_2

    invoke-static {}, Lm8/i;->o()V

    :cond_2
    invoke-direct {p0, p1, v0}, Lub/f;->g(Lpb/t;Lpb/r;)Z

    move-result p1

    if-eqz p1, :cond_3

    move v3, v1

    :cond_3
    return v3
.end method

.method public final I(Lub/e;Ljava/io/IOException;)V
    .locals 4

    const-string v0, "call"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lub/f;->r:Lub/h;

    sget-boolean v1, Lrb/b;->h:Z

    if-eqz v1, :cond_1

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/AssertionError;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Thread "

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    const-string v2, "Thread.currentThread()"

    invoke-static {v1, v2}, Lm8/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " MUST NOT hold lock on "

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw p1

    :cond_1
    :goto_0
    iget-object v0, p0, Lub/f;->r:Lub/h;

    monitor-enter v0

    :try_start_0
    instance-of v1, p2, Lxb/n;

    const/4 v2, 0x1

    if-eqz v1, :cond_4

    move-object v1, p2

    check-cast v1, Lxb/n;

    iget-object v1, v1, Lxb/n;->a:Lxb/b;

    sget-object v3, Lxb/b;->i:Lxb/b;

    if-ne v1, v3, :cond_2

    iget p1, p0, Lub/f;->n:I

    add-int/2addr p1, v2

    iput p1, p0, Lub/f;->n:I

    if-le p1, v2, :cond_7

    iput-boolean v2, p0, Lub/f;->j:Z

    iget p1, p0, Lub/f;->l:I

    :goto_1
    add-int/2addr p1, v2

    iput p1, p0, Lub/f;->l:I

    goto :goto_2

    :cond_2
    check-cast p2, Lxb/n;

    iget-object p2, p2, Lxb/n;->a:Lxb/b;

    sget-object v1, Lxb/b;->j:Lxb/b;

    if-ne p2, v1, :cond_3

    invoke-virtual {p1}, Lub/e;->K()Z

    move-result p1

    if-eqz p1, :cond_3

    goto :goto_2

    :cond_3
    iput-boolean v2, p0, Lub/f;->j:Z

    iget p1, p0, Lub/f;->l:I

    goto :goto_1

    :cond_4
    invoke-virtual {p0}, Lub/f;->x()Z

    move-result v1

    if-eqz v1, :cond_5

    instance-of v1, p2, Lxb/a;

    if-eqz v1, :cond_7

    :cond_5
    iput-boolean v2, p0, Lub/f;->j:Z

    iget v1, p0, Lub/f;->m:I

    if-nez v1, :cond_7

    if-eqz p2, :cond_6

    invoke-virtual {p1}, Lub/e;->j()Lpb/x;

    move-result-object p1

    iget-object v1, p0, Lub/f;->s:Lpb/d0;

    invoke-virtual {p0, p1, v1, p2}, Lub/f;->i(Lpb/x;Lpb/d0;Ljava/io/IOException;)V

    :cond_6
    iget p1, p0, Lub/f;->l:I

    goto :goto_1

    :cond_7
    :goto_2
    sget-object p1, La8/v;->a:La8/v;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0

    throw p1
.end method

.method public a()Lpb/y;
    .locals 1

    iget-object v0, p0, Lub/f;->f:Lpb/y;

    if-nez v0, :cond_0

    invoke-static {}, Lm8/i;->o()V

    :cond_0
    return-object v0
.end method

.method public b()Lpb/d0;
    .locals 1

    iget-object v0, p0, Lub/f;->s:Lpb/d0;

    return-object v0
.end method

.method public c(Lxb/f;Lxb/m;)V
    .locals 1

    const-string v0, "connection"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "settings"

    invoke-static {p2, p1}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p0, Lub/f;->r:Lub/h;

    monitor-enter p1

    :try_start_0
    invoke-virtual {p2}, Lxb/m;->d()I

    move-result p2

    iput p2, p0, Lub/f;->o:I

    sget-object p2, La8/v;->a:La8/v;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p1

    return-void

    :catchall_0
    move-exception p2

    monitor-exit p1

    throw p2
.end method

.method public d(Lxb/i;)V
    .locals 2

    const-string v0, "stream"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lxb/b;->i:Lxb/b;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lxb/i;->d(Lxb/b;Ljava/io/IOException;)V

    return-void
.end method

.method public final f()V
    .locals 1

    iget-object v0, p0, Lub/f;->c:Ljava/net/Socket;

    if-eqz v0, :cond_0

    invoke-static {v0}, Lrb/b;->k(Ljava/net/Socket;)V

    :cond_0
    return-void
.end method

.method public final h(IIIIZLpb/e;Lpb/p;)V
    .locals 16

    move-object/from16 v7, p0

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    const-string v0, "call"

    invoke-static {v8, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "eventListener"

    invoke-static {v9, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, v7, Lub/f;->f:Lpb/y;

    const/4 v10, 0x1

    if-nez v0, :cond_0

    move v0, v10

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_d

    iget-object v0, v7, Lub/f;->s:Lpb/d0;

    invoke-virtual {v0}, Lpb/d0;->a()Lpb/a;

    move-result-object v0

    invoke-virtual {v0}, Lpb/a;->b()Ljava/util/List;

    move-result-object v0

    new-instance v11, Lub/b;

    invoke-direct {v11, v0}, Lub/b;-><init>(Ljava/util/List;)V

    iget-object v1, v7, Lub/f;->s:Lpb/d0;

    invoke-virtual {v1}, Lpb/d0;->a()Lpb/a;

    move-result-object v1

    invoke-virtual {v1}, Lpb/a;->k()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v1

    if-nez v1, :cond_3

    sget-object v1, Lpb/k;->j:Lpb/k;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, v7, Lub/f;->s:Lpb/d0;

    invoke-virtual {v0}, Lpb/d0;->a()Lpb/a;

    move-result-object v0

    invoke-virtual {v0}, Lpb/a;->l()Lpb/t;

    move-result-object v0

    invoke-virtual {v0}, Lpb/t;->i()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lyb/h;->c:Lyb/h$a;

    invoke-virtual {v1}, Lyb/h$a;->e()Lyb/h;

    move-result-object v1

    invoke-virtual {v1, v0}, Lyb/h;->k(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_1

    :cond_1
    new-instance v1, Lub/j;

    new-instance v2, Ljava/net/UnknownServiceException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "CLEARTEXT communication to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " not permitted by network security policy"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/net/UnknownServiceException;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Lub/j;-><init>(Ljava/io/IOException;)V

    throw v1

    :cond_2
    new-instance v0, Lub/j;

    new-instance v1, Ljava/net/UnknownServiceException;

    const-string v2, "CLEARTEXT communication not enabled for client"

    invoke-direct {v1, v2}, Ljava/net/UnknownServiceException;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lub/j;-><init>(Ljava/io/IOException;)V

    throw v0

    :cond_3
    iget-object v0, v7, Lub/f;->s:Lpb/d0;

    invoke-virtual {v0}, Lpb/d0;->a()Lpb/a;

    move-result-object v0

    invoke-virtual {v0}, Lpb/a;->f()Ljava/util/List;

    move-result-object v0

    sget-object v1, Lpb/y;->f:Lpb/y;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_c

    :goto_1
    const/4 v12, 0x0

    move-object v13, v12

    :goto_2
    :try_start_0
    iget-object v0, v7, Lub/f;->s:Lpb/d0;

    invoke-virtual {v0}, Lpb/d0;->c()Z

    move-result v0

    if-eqz v0, :cond_5

    move-object/from16 v1, p0

    move/from16 v2, p1

    move/from16 v3, p2

    move/from16 v4, p3

    move-object/from16 v5, p6

    move-object/from16 v6, p7

    invoke-direct/range {v1 .. v6}, Lub/f;->l(IIILpb/e;Lpb/p;)V

    iget-object v0, v7, Lub/f;->c:Ljava/net/Socket;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    if-nez v0, :cond_4

    goto :goto_4

    :cond_4
    move/from16 v14, p1

    move/from16 v15, p2

    goto :goto_3

    :cond_5
    move/from16 v14, p1

    move/from16 v15, p2

    :try_start_1
    invoke-direct {v7, v14, v15, v8, v9}, Lub/f;->j(IILpb/e;Lpb/p;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_3
    move/from16 v6, p4

    :try_start_2
    invoke-direct {v7, v11, v6, v8, v9}, Lub/f;->o(Lub/b;ILpb/e;Lpb/p;)V

    iget-object v0, v7, Lub/f;->s:Lpb/d0;

    invoke-virtual {v0}, Lpb/d0;->d()Ljava/net/InetSocketAddress;

    move-result-object v0

    iget-object v1, v7, Lub/f;->s:Lpb/d0;

    invoke-virtual {v1}, Lpb/d0;->b()Ljava/net/Proxy;

    move-result-object v1

    iget-object v2, v7, Lub/f;->f:Lpb/y;

    invoke-virtual {v9, v8, v0, v1, v2}, Lpb/p;->e(Lpb/e;Ljava/net/InetSocketAddress;Ljava/net/Proxy;Lpb/y;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :goto_4
    iget-object v0, v7, Lub/f;->s:Lpb/d0;

    invoke-virtual {v0}, Lpb/d0;->c()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, v7, Lub/f;->c:Ljava/net/Socket;

    if-eqz v0, :cond_6

    goto :goto_5

    :cond_6
    new-instance v0, Lub/j;

    new-instance v1, Ljava/net/ProtocolException;

    const-string v2, "Too many tunnel connections attempted: 21"

    invoke-direct {v1, v2}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lub/j;-><init>(Ljava/io/IOException;)V

    throw v0

    :cond_7
    :goto_5
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iput-wide v0, v7, Lub/f;->q:J

    return-void

    :catch_0
    move-exception v0

    goto :goto_7

    :catch_1
    move-exception v0

    goto :goto_6

    :catch_2
    move-exception v0

    move/from16 v14, p1

    move/from16 v15, p2

    :goto_6
    move/from16 v6, p4

    :goto_7
    iget-object v1, v7, Lub/f;->d:Ljava/net/Socket;

    if-eqz v1, :cond_8

    invoke-static {v1}, Lrb/b;->k(Ljava/net/Socket;)V

    :cond_8
    iget-object v1, v7, Lub/f;->c:Ljava/net/Socket;

    if-eqz v1, :cond_9

    invoke-static {v1}, Lrb/b;->k(Ljava/net/Socket;)V

    :cond_9
    iput-object v12, v7, Lub/f;->d:Ljava/net/Socket;

    iput-object v12, v7, Lub/f;->c:Ljava/net/Socket;

    iput-object v12, v7, Lub/f;->h:Ldc/g;

    iput-object v12, v7, Lub/f;->i:Ldc/f;

    iput-object v12, v7, Lub/f;->e:Lpb/r;

    iput-object v12, v7, Lub/f;->f:Lpb/y;

    iput-object v12, v7, Lub/f;->g:Lxb/f;

    iput v10, v7, Lub/f;->o:I

    iget-object v1, v7, Lub/f;->s:Lpb/d0;

    invoke-virtual {v1}, Lpb/d0;->d()Ljava/net/InetSocketAddress;

    move-result-object v3

    iget-object v1, v7, Lub/f;->s:Lpb/d0;

    invoke-virtual {v1}, Lpb/d0;->b()Ljava/net/Proxy;

    move-result-object v4

    const/4 v5, 0x0

    move-object/from16 v1, p7

    move-object/from16 v2, p6

    move-object v6, v0

    invoke-virtual/range {v1 .. v6}, Lpb/p;->f(Lpb/e;Ljava/net/InetSocketAddress;Ljava/net/Proxy;Lpb/y;Ljava/io/IOException;)V

    if-nez v13, :cond_a

    new-instance v13, Lub/j;

    invoke-direct {v13, v0}, Lub/j;-><init>(Ljava/io/IOException;)V

    goto :goto_8

    :cond_a
    invoke-virtual {v13, v0}, Lub/j;->a(Ljava/io/IOException;)V

    :goto_8
    if-eqz p5, :cond_b

    invoke-virtual {v11, v0}, Lub/b;->b(Ljava/io/IOException;)Z

    move-result v0

    if-eqz v0, :cond_b

    goto/16 :goto_2

    :cond_b
    throw v13

    :cond_c
    new-instance v0, Lub/j;

    new-instance v1, Ljava/net/UnknownServiceException;

    const-string v2, "H2_PRIOR_KNOWLEDGE cannot be used with HTTPS"

    invoke-direct {v1, v2}, Ljava/net/UnknownServiceException;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lub/j;-><init>(Ljava/io/IOException;)V

    throw v0

    :cond_d
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "already connected"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final i(Lpb/x;Lpb/d0;Ljava/io/IOException;)V
    .locals 3

    const-string v0, "client"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "failedRoute"

    invoke-static {p2, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "failure"

    invoke-static {p3, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Lpb/d0;->b()Ljava/net/Proxy;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v0

    sget-object v1, Ljava/net/Proxy$Type;->DIRECT:Ljava/net/Proxy$Type;

    if-eq v0, v1, :cond_0

    invoke-virtual {p2}, Lpb/d0;->a()Lpb/a;

    move-result-object v0

    invoke-virtual {v0}, Lpb/a;->i()Ljava/net/ProxySelector;

    move-result-object v1

    invoke-virtual {v0}, Lpb/a;->l()Lpb/t;

    move-result-object v0

    invoke-virtual {v0}, Lpb/t;->s()Ljava/net/URI;

    move-result-object v0

    invoke-virtual {p2}, Lpb/d0;->b()Ljava/net/Proxy;

    move-result-object v2

    invoke-virtual {v2}, Ljava/net/Proxy;->address()Ljava/net/SocketAddress;

    move-result-object v2

    invoke-virtual {v1, v0, v2, p3}, Ljava/net/ProxySelector;->connectFailed(Ljava/net/URI;Ljava/net/SocketAddress;Ljava/io/IOException;)V

    :cond_0
    invoke-virtual {p1}, Lpb/x;->r()Lub/i;

    move-result-object p1

    invoke-virtual {p1, p2}, Lub/i;->b(Lpb/d0;)V

    return-void
.end method

.method public final p()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/ref/Reference<",
            "Lub/e;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lub/f;->p:Ljava/util/List;

    return-object v0
.end method

.method public final q()J
    .locals 2

    iget-wide v0, p0, Lub/f;->q:J

    return-wide v0
.end method

.method public final r()Z
    .locals 1

    iget-boolean v0, p0, Lub/f;->j:Z

    return v0
.end method

.method public final s()I
    .locals 1

    iget v0, p0, Lub/f;->l:I

    return v0
.end method

.method public final t()I
    .locals 1

    iget v0, p0, Lub/f;->m:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Connection{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lub/f;->s:Lpb/d0;

    invoke-virtual {v1}, Lpb/d0;->a()Lpb/a;

    move-result-object v1

    invoke-virtual {v1}, Lpb/a;->l()Lpb/t;

    move-result-object v1

    invoke-virtual {v1}, Lpb/t;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x3a

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lub/f;->s:Lpb/d0;

    invoke-virtual {v1}, Lpb/d0;->a()Lpb/a;

    move-result-object v1

    invoke-virtual {v1}, Lpb/a;->l()Lpb/t;

    move-result-object v1

    invoke-virtual {v1}, Lpb/t;->n()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v1, 0x2c

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, " proxy="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lub/f;->s:Lpb/d0;

    invoke-virtual {v1}, Lpb/d0;->b()Ljava/net/Proxy;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, " hostAddress="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lub/f;->s:Lpb/d0;

    invoke-virtual {v1}, Lpb/d0;->d()Ljava/net/InetSocketAddress;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, " cipherSuite="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lub/f;->e:Lpb/r;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lpb/r;->a()Lpb/h;

    move-result-object v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const-string v1, "none"

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, " protocol="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lub/f;->f:Lpb/y;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public u()Lpb/r;
    .locals 1

    iget-object v0, p0, Lub/f;->e:Lpb/r;

    return-object v0
.end method

.method public final v(Lpb/a;Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lpb/a;",
            "Ljava/util/List<",
            "Lpb/d0;",
            ">;)Z"
        }
    .end annotation

    const-string v0, "address"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lub/f;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget v1, p0, Lub/f;->o:I

    const/4 v2, 0x0

    if-ge v0, v1, :cond_9

    iget-boolean v0, p0, Lub/f;->j:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lub/f;->s:Lpb/d0;

    invoke-virtual {v0}, Lpb/d0;->a()Lpb/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lpb/a;->d(Lpb/a;)Z

    move-result v0

    if-nez v0, :cond_1

    return v2

    :cond_1
    invoke-virtual {p1}, Lpb/a;->l()Lpb/t;

    move-result-object v0

    invoke-virtual {v0}, Lpb/t;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lub/f;->b()Lpb/d0;

    move-result-object v1

    invoke-virtual {v1}, Lpb/d0;->a()Lpb/a;

    move-result-object v1

    invoke-virtual {v1}, Lpb/a;->l()Lpb/t;

    move-result-object v1

    invoke-virtual {v1}, Lpb/t;->i()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_2

    return v1

    :cond_2
    iget-object v0, p0, Lub/f;->g:Lxb/f;

    if-nez v0, :cond_3

    return v2

    :cond_3
    if-eqz p2, :cond_9

    invoke-direct {p0, p2}, Lub/f;->B(Ljava/util/List;)Z

    move-result p2

    if-nez p2, :cond_4

    goto :goto_0

    :cond_4
    invoke-virtual {p1}, Lpb/a;->e()Ljavax/net/ssl/HostnameVerifier;

    move-result-object p2

    sget-object v0, Lbc/d;->a:Lbc/d;

    if-eq p2, v0, :cond_5

    return v2

    :cond_5
    invoke-virtual {p1}, Lpb/a;->l()Lpb/t;

    move-result-object p2

    invoke-virtual {p0, p2}, Lub/f;->H(Lpb/t;)Z

    move-result p2

    if-nez p2, :cond_6

    return v2

    :cond_6
    :try_start_0
    invoke-virtual {p1}, Lpb/a;->a()Lpb/g;

    move-result-object p2

    if-nez p2, :cond_7

    invoke-static {}, Lm8/i;->o()V

    :cond_7
    invoke-virtual {p1}, Lpb/a;->l()Lpb/t;

    move-result-object p1

    invoke-virtual {p1}, Lpb/t;->i()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0}, Lub/f;->u()Lpb/r;

    move-result-object v0

    if-nez v0, :cond_8

    invoke-static {}, Lm8/i;->o()V

    :cond_8
    invoke-virtual {v0}, Lpb/r;->d()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p2, p1, v0}, Lpb/g;->a(Ljava/lang/String;Ljava/util/List;)V
    :try_end_0
    .catch Ljavax/net/ssl/SSLPeerUnverifiedException; {:try_start_0 .. :try_end_0} :catch_0

    return v1

    :catch_0
    :cond_9
    :goto_0
    return v2
.end method

.method public final w(Z)Z
    .locals 6

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iget-object v2, p0, Lub/f;->d:Ljava/net/Socket;

    if-nez v2, :cond_0

    invoke-static {}, Lm8/i;->o()V

    :cond_0
    iget-object v3, p0, Lub/f;->h:Ldc/g;

    if-nez v3, :cond_1

    invoke-static {}, Lm8/i;->o()V

    :cond_1
    invoke-virtual {v2}, Ljava/net/Socket;->isClosed()Z

    move-result v4

    if-nez v4, :cond_5

    invoke-virtual {v2}, Ljava/net/Socket;->isInputShutdown()Z

    move-result v4

    if-nez v4, :cond_5

    invoke-virtual {v2}, Ljava/net/Socket;->isOutputShutdown()Z

    move-result v4

    if-eqz v4, :cond_2

    goto :goto_0

    :cond_2
    iget-object v4, p0, Lub/f;->g:Lxb/f;

    if-eqz v4, :cond_3

    invoke-virtual {v4, v0, v1}, Lxb/f;->i0(J)Z

    move-result p1

    return p1

    :cond_3
    iget-wide v4, p0, Lub/f;->q:J

    sub-long/2addr v0, v4

    const-wide v4, 0x2540be400L

    cmp-long v0, v0, v4

    if-ltz v0, :cond_4

    if-eqz p1, :cond_4

    invoke-static {v2, v3}, Lrb/b;->C(Ljava/net/Socket;Ldc/g;)Z

    move-result p1

    return p1

    :cond_4
    const/4 p1, 0x1

    return p1

    :cond_5
    :goto_0
    const/4 p1, 0x0

    return p1
.end method

.method public final x()Z
    .locals 1

    iget-object v0, p0, Lub/f;->g:Lxb/f;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final y(Lpb/x;Lvb/g;)Lvb/d;
    .locals 6

    const-string v0, "client"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "chain"

    invoke-static {p2, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lub/f;->d:Ljava/net/Socket;

    if-nez v0, :cond_0

    invoke-static {}, Lm8/i;->o()V

    :cond_0
    iget-object v1, p0, Lub/f;->h:Ldc/g;

    if-nez v1, :cond_1

    invoke-static {}, Lm8/i;->o()V

    :cond_1
    iget-object v2, p0, Lub/f;->i:Ldc/f;

    if-nez v2, :cond_2

    invoke-static {}, Lm8/i;->o()V

    :cond_2
    iget-object v3, p0, Lub/f;->g:Lxb/f;

    if-eqz v3, :cond_3

    new-instance v0, Lxb/g;

    invoke-direct {v0, p1, p0, p2, v3}, Lxb/g;-><init>(Lpb/x;Lub/f;Lvb/g;Lxb/f;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p2}, Lvb/g;->o()I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/net/Socket;->setSoTimeout(I)V

    invoke-interface {v1}, Ldc/y;->d()Ldc/z;

    move-result-object v0

    invoke-virtual {p2}, Lvb/g;->l()I

    move-result v3

    int-to-long v3, v3

    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v3, v4, v5}, Ldc/z;->g(JLjava/util/concurrent/TimeUnit;)Ldc/z;

    invoke-interface {v2}, Ldc/w;->d()Ldc/z;

    move-result-object v0

    invoke-virtual {p2}, Lvb/g;->n()I

    move-result p2

    int-to-long v3, p2

    invoke-virtual {v0, v3, v4, v5}, Ldc/z;->g(JLjava/util/concurrent/TimeUnit;)Ldc/z;

    new-instance v0, Lwb/a;

    invoke-direct {v0, p1, p0, v1, v2}, Lwb/a;-><init>(Lpb/x;Lub/f;Ldc/g;Ldc/f;)V

    :goto_0
    return-object v0
.end method

.method public final z()V
    .locals 5

    iget-object v0, p0, Lub/f;->r:Lub/h;

    sget-boolean v1, Lrb/b;->h:Z

    if-eqz v1, :cond_1

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0

    :cond_0
    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Thread "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    const-string v4, "Thread.currentThread()"

    invoke-static {v3, v4}, Lm8/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " MUST NOT hold lock on "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    :cond_1
    :goto_0
    iget-object v0, p0, Lub/f;->r:Lub/h;

    monitor-enter v0

    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Lub/f;->k:Z

    sget-object v1, La8/v;->a:La8/v;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method
