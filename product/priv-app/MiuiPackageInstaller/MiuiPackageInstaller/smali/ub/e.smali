.class public final Lub/e;
.super Ljava/lang/Object;

# interfaces
.implements Lpb/e;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lub/e$a;,
        Lub/e$b;
    }
.end annotation


# instance fields
.field private final a:Lub/h;

.field private final b:Lpb/p;

.field private final c:Lub/e$c;

.field private d:Ljava/lang/Object;

.field private e:Lub/d;

.field private f:Lub/f;

.field private g:Lub/c;

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Lub/c;

.field private final o:Lpb/x;

.field private final p:Lpb/z;

.field private final q:Z


# direct methods
.method public constructor <init>(Lpb/x;Lpb/z;Z)V
    .locals 2

    const-string v0, "client"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "originalRequest"

    invoke-static {p2, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lub/e;->o:Lpb/x;

    iput-object p2, p0, Lub/e;->p:Lpb/z;

    iput-boolean p3, p0, Lub/e;->q:Z

    invoke-virtual {p1}, Lpb/x;->j()Lpb/j;

    move-result-object p2

    invoke-virtual {p2}, Lpb/j;->a()Lub/h;

    move-result-object p2

    iput-object p2, p0, Lub/e;->a:Lub/h;

    invoke-virtual {p1}, Lpb/x;->o()Lpb/p$c;

    move-result-object p2

    invoke-interface {p2, p0}, Lpb/p$c;->a(Lpb/e;)Lpb/p;

    move-result-object p2

    iput-object p2, p0, Lub/e;->b:Lpb/p;

    new-instance p2, Lub/e$c;

    invoke-direct {p2, p0}, Lub/e$c;-><init>(Lub/e;)V

    invoke-virtual {p1}, Lpb/x;->g()I

    move-result p1

    int-to-long v0, p1

    sget-object p1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p2, v0, v1, p1}, Ldc/z;->g(JLjava/util/concurrent/TimeUnit;)Ldc/z;

    iput-object p2, p0, Lub/e;->c:Lub/e$c;

    return-void
.end method

.method public static final synthetic a(Lub/e;)Lub/e$c;
    .locals 0

    iget-object p0, p0, Lub/e;->c:Lub/e$c;

    return-object p0
.end method

.method public static final synthetic c(Lub/e;)Ljava/lang/String;
    .locals 0

    invoke-direct {p0}, Lub/e;->y()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private final e()V
    .locals 2

    sget-object v0, Lyb/h;->c:Lyb/h$a;

    invoke-virtual {v0}, Lyb/h$a;->e()Lyb/h;

    move-result-object v0

    const-string v1, "response.body().close()"

    invoke-virtual {v0, v1}, Lyb/h;->j(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lub/e;->d:Ljava/lang/Object;

    iget-object v0, p0, Lub/e;->b:Lpb/p;

    invoke-virtual {v0, p0}, Lpb/p;->c(Lpb/e;)V

    return-void
.end method

.method private final g(Lpb/t;)Lpb/a;
    .locals 17

    move-object/from16 v0, p0

    invoke-virtual/range {p1 .. p1}, Lpb/t;->j()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    iget-object v1, v0, Lub/e;->o:Lpb/x;

    invoke-virtual {v1}, Lpb/x;->D()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v2

    iget-object v1, v0, Lub/e;->o:Lpb/x;

    invoke-virtual {v1}, Lpb/x;->s()Ljavax/net/ssl/HostnameVerifier;

    move-result-object v1

    iget-object v3, v0, Lub/e;->o:Lpb/x;

    invoke-virtual {v3}, Lpb/x;->h()Lpb/g;

    move-result-object v3

    move-object v10, v1

    move-object v9, v2

    move-object v11, v3

    goto :goto_0

    :cond_0
    move-object v9, v2

    move-object v10, v9

    move-object v11, v10

    :goto_0
    new-instance v1, Lpb/a;

    invoke-virtual/range {p1 .. p1}, Lpb/t;->i()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Lpb/t;->n()I

    move-result v6

    iget-object v2, v0, Lub/e;->o:Lpb/x;

    invoke-virtual {v2}, Lpb/x;->n()Lpb/o;

    move-result-object v7

    iget-object v2, v0, Lub/e;->o:Lpb/x;

    invoke-virtual {v2}, Lpb/x;->C()Ljavax/net/SocketFactory;

    move-result-object v8

    iget-object v2, v0, Lub/e;->o:Lpb/x;

    invoke-virtual {v2}, Lpb/x;->y()Lpb/b;

    move-result-object v12

    iget-object v2, v0, Lub/e;->o:Lpb/x;

    invoke-virtual {v2}, Lpb/x;->x()Ljava/net/Proxy;

    move-result-object v13

    iget-object v2, v0, Lub/e;->o:Lpb/x;

    invoke-virtual {v2}, Lpb/x;->w()Ljava/util/List;

    move-result-object v14

    iget-object v2, v0, Lub/e;->o:Lpb/x;

    invoke-virtual {v2}, Lpb/x;->k()Ljava/util/List;

    move-result-object v15

    iget-object v2, v0, Lub/e;->o:Lpb/x;

    invoke-virtual {v2}, Lpb/x;->z()Ljava/net/ProxySelector;

    move-result-object v16

    move-object v4, v1

    invoke-direct/range {v4 .. v16}, Lpb/a;-><init>(Ljava/lang/String;ILpb/o;Ljavax/net/SocketFactory;Ljavax/net/ssl/SSLSocketFactory;Ljavax/net/ssl/HostnameVerifier;Lpb/g;Lpb/b;Ljava/net/Proxy;Ljava/util/List;Ljava/util/List;Ljava/net/ProxySelector;)V

    return-object v1
.end method

.method private final q(Ljava/io/IOException;Z)Ljava/io/IOException;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/io/IOException;",
            ">(TE;Z)TE;"
        }
    .end annotation

    new-instance v0, Lm8/t;

    invoke-direct {v0}, Lm8/t;-><init>()V

    iget-object v1, p0, Lub/e;->a:Lub/h;

    monitor-enter v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz p2, :cond_1

    :try_start_0
    iget-object v4, p0, Lub/e;->g:Lub/c;

    if-nez v4, :cond_0

    goto :goto_0

    :cond_0
    move v4, v2

    goto :goto_1

    :catchall_0
    move-exception p1

    goto/16 :goto_5

    :cond_1
    :goto_0
    move v4, v3

    :goto_1
    if-eqz v4, :cond_d

    iget-object v4, p0, Lub/e;->f:Lub/f;

    iput-object v4, v0, Lm8/t;->a:Ljava/lang/Object;

    const/4 v5, 0x0

    if-eqz v4, :cond_3

    iget-object v4, p0, Lub/e;->g:Lub/c;

    if-nez v4, :cond_3

    if-nez p2, :cond_2

    iget-boolean p2, p0, Lub/e;->l:Z

    if-eqz p2, :cond_3

    :cond_2
    invoke-virtual {p0}, Lub/e;->u()Ljava/net/Socket;

    move-result-object p2

    goto :goto_2

    :cond_3
    move-object p2, v5

    :goto_2
    iget-object v4, p0, Lub/e;->f:Lub/f;

    if-eqz v4, :cond_4

    iput-object v5, v0, Lm8/t;->a:Ljava/lang/Object;

    :cond_4
    iget-boolean v4, p0, Lub/e;->l:Z

    if-eqz v4, :cond_5

    iget-object v4, p0, Lub/e;->g:Lub/c;

    if-nez v4, :cond_5

    move v4, v3

    goto :goto_3

    :cond_5
    move v4, v2

    :goto_3
    sget-object v5, La8/v;->a:La8/v;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    if-eqz p2, :cond_6

    invoke-static {p2}, Lrb/b;->k(Ljava/net/Socket;)V

    :cond_6
    iget-object p2, v0, Lm8/t;->a:Ljava/lang/Object;

    move-object v0, p2

    check-cast v0, Lpb/i;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lub/e;->b:Lpb/p;

    check-cast p2, Lpb/i;

    if-nez p2, :cond_7

    invoke-static {}, Lm8/i;->o()V

    :cond_7
    invoke-virtual {v0, p0, p2}, Lpb/p;->i(Lpb/e;Lpb/i;)V

    :cond_8
    if-eqz v4, :cond_c

    if-eqz p1, :cond_9

    move v2, v3

    :cond_9
    invoke-direct {p0, p1}, Lub/e;->x(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    iget-object p2, p0, Lub/e;->b:Lpb/p;

    if-eqz v2, :cond_b

    if-nez p1, :cond_a

    invoke-static {}, Lm8/i;->o()V

    :cond_a
    invoke-virtual {p2, p0, p1}, Lpb/p;->b(Lpb/e;Ljava/io/IOException;)V

    goto :goto_4

    :cond_b
    invoke-virtual {p2, p0}, Lpb/p;->a(Lpb/e;)V

    :cond_c
    :goto_4
    return-object p1

    :cond_d
    :try_start_1
    const-string p1, "cannot release connection while it is in use"

    new-instance p2, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_5
    monitor-exit v1

    throw p1
.end method

.method private final x(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/io/IOException;",
            ">(TE;)TE;"
        }
    .end annotation

    iget-boolean v0, p0, Lub/e;->k:Z

    if-eqz v0, :cond_0

    return-object p1

    :cond_0
    iget-object v0, p0, Lub/e;->c:Lub/e$c;

    invoke-virtual {v0}, Ldc/d;->s()Z

    move-result v0

    if-nez v0, :cond_1

    return-object p1

    :cond_1
    new-instance v0, Ljava/io/InterruptedIOException;

    const-string v1, "timeout"

    invoke-direct {v0, v1}, Ljava/io/InterruptedIOException;-><init>(Ljava/lang/String;)V

    if-eqz p1, :cond_2

    invoke-virtual {v0, p1}, Ljava/io/InterruptedIOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    :cond_2
    return-object v0
.end method

.method private final y()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lub/e;->K()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "canceled "

    goto :goto_0

    :cond_0
    const-string v1, ""

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lub/e;->q:Z

    if-eqz v1, :cond_1

    const-string v1, "web socket"

    goto :goto_1

    :cond_1
    const-string v1, "call"

    :goto_1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " to "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lub/e;->t()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public J()Lpb/z;
    .locals 1

    iget-object v0, p0, Lub/e;->p:Lpb/z;

    return-object v0
.end method

.method public K()Z
    .locals 2

    iget-object v0, p0, Lub/e;->a:Lub/h;

    monitor-enter v0

    :try_start_0
    iget-boolean v1, p0, Lub/e;->j:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public b(Lpb/f;)V
    .locals 2

    const-string v0, "responseCallback"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lub/e;->m:Z

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    if-eqz v0, :cond_0

    iput-boolean v1, p0, Lub/e;->m:Z

    sget-object v0, La8/v;->a:La8/v;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    invoke-direct {p0}, Lub/e;->e()V

    iget-object v0, p0, Lub/e;->o:Lpb/x;

    invoke-virtual {v0}, Lpb/x;->m()Lpb/n;

    move-result-object v0

    new-instance v1, Lub/e$a;

    invoke-direct {v1, p0, p1}, Lub/e$a;-><init>(Lub/e;Lpb/f;)V

    invoke-virtual {v0, v1}, Lpb/n;->a(Lub/e$a;)V

    return-void

    :cond_0
    :try_start_1
    const-string p1, "Already Executed"

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public cancel()V
    .locals 4

    iget-object v0, p0, Lub/e;->a:Lub/h;

    monitor-enter v0

    :try_start_0
    iget-boolean v1, p0, Lub/e;->j:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    monitor-exit v0

    return-void

    :cond_0
    const/4 v1, 0x1

    :try_start_1
    iput-boolean v1, p0, Lub/e;->j:Z

    iget-object v1, p0, Lub/e;->g:Lub/c;

    iget-object v2, p0, Lub/e;->e:Lub/d;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lub/d;->a()Lub/f;

    move-result-object v2

    if-eqz v2, :cond_1

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lub/e;->f:Lub/f;

    :goto_0
    sget-object v3, La8/v;->a:La8/v;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v0

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lub/c;->b()V

    goto :goto_1

    :cond_2
    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lub/f;->f()V

    :cond_3
    :goto_1
    iget-object v0, p0, Lub/e;->b:Lpb/p;

    invoke-virtual {v0, p0}, Lpb/p;->d(Lpb/e;)V

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lub/e;->f()Lub/e;

    move-result-object v0

    return-object v0
.end method

.method public final d(Lub/f;)V
    .locals 4

    const-string v0, "connection"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lub/e;->a:Lub/h;

    sget-boolean v1, Lrb/b;->h:Z

    if-eqz v1, :cond_1

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Thread "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    const-string v3, "Thread.currentThread()"

    invoke-static {v2, v3}, Lm8/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " MUST hold lock on "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw p1

    :cond_1
    :goto_0
    iget-object v0, p0, Lub/e;->f:Lub/f;

    if-nez v0, :cond_2

    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    :goto_1
    if-eqz v0, :cond_3

    iput-object p1, p0, Lub/e;->f:Lub/f;

    invoke-virtual {p1}, Lub/f;->p()Ljava/util/List;

    move-result-object p1

    new-instance v0, Lub/e$b;

    iget-object v1, p0, Lub/e;->d:Ljava/lang/Object;

    invoke-direct {v0, p0, v1}, Lub/e$b;-><init>(Lub/e;Ljava/lang/Object;)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void

    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Check failed."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public f()Lub/e;
    .locals 4

    new-instance v0, Lub/e;

    iget-object v1, p0, Lub/e;->o:Lpb/x;

    iget-object v2, p0, Lub/e;->p:Lpb/z;

    iget-boolean v3, p0, Lub/e;->q:Z

    invoke-direct {v0, v1, v2, v3}, Lub/e;-><init>(Lpb/x;Lpb/z;Z)V

    return-object v0
.end method

.method public final h(Lpb/z;Z)V
    .locals 3

    const-string v0, "request"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lub/e;->n:Lub/c;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez v0, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    move v0, v2

    :goto_0
    if-eqz v0, :cond_4

    iget-object v0, p0, Lub/e;->g:Lub/c;

    if-nez v0, :cond_1

    goto :goto_1

    :cond_1
    move v1, v2

    :goto_1
    if-eqz v1, :cond_3

    if-eqz p2, :cond_2

    new-instance p2, Lub/d;

    iget-object v0, p0, Lub/e;->a:Lub/h;

    invoke-virtual {p1}, Lpb/z;->j()Lpb/t;

    move-result-object p1

    invoke-direct {p0, p1}, Lub/e;->g(Lpb/t;)Lpb/a;

    move-result-object p1

    iget-object v1, p0, Lub/e;->b:Lpb/p;

    invoke-direct {p2, v0, p1, p0, v1}, Lub/d;-><init>(Lub/h;Lpb/a;Lub/e;Lpb/p;)V

    iput-object p2, p0, Lub/e;->e:Lub/d;

    :cond_2
    return-void

    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "cannot make a new request because the previous response is still open: please call response.close()"

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_4
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Check failed."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final i(Z)V
    .locals 2

    iget-boolean v0, p0, Lub/e;->l:Z

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    if-eqz v0, :cond_4

    if-eqz p1, :cond_3

    iget-object p1, p0, Lub/e;->g:Lub/c;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lub/c;->d()V

    :cond_0
    iget-object p1, p0, Lub/e;->g:Lub/c;

    if-nez p1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_2

    goto :goto_1

    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Check failed."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_3
    :goto_1
    const/4 p1, 0x0

    iput-object p1, p0, Lub/e;->n:Lub/c;

    return-void

    :cond_4
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "released"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final j()Lpb/x;
    .locals 1

    iget-object v0, p0, Lub/e;->o:Lpb/x;

    return-object v0
.end method

.method public final k()Lub/f;
    .locals 1

    iget-object v0, p0, Lub/e;->f:Lub/f;

    return-object v0
.end method

.method public final l()Z
    .locals 1

    iget-boolean v0, p0, Lub/e;->q:Z

    return v0
.end method

.method public final m()Lub/c;
    .locals 1

    iget-object v0, p0, Lub/e;->n:Lub/c;

    return-object v0
.end method

.method public final n()Lpb/z;
    .locals 1

    iget-object v0, p0, Lub/e;->p:Lpb/z;

    return-object v0
.end method

.method public final o()Lpb/b0;
    .locals 12

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lub/e;->o:Lpb/x;

    invoke-virtual {v0}, Lpb/x;->t()Ljava/util/List;

    move-result-object v0

    invoke-static {v2, v0}, Lb8/j;->r(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    new-instance v0, Lvb/j;

    iget-object v1, p0, Lub/e;->o:Lpb/x;

    invoke-direct {v0, v1}, Lvb/j;-><init>(Lpb/x;)V

    invoke-interface {v2, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    new-instance v0, Lvb/a;

    iget-object v1, p0, Lub/e;->o:Lpb/x;

    invoke-virtual {v1}, Lpb/x;->l()Lpb/m;

    move-result-object v1

    invoke-direct {v0, v1}, Lvb/a;-><init>(Lpb/m;)V

    invoke-interface {v2, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    new-instance v0, Lsb/a;

    iget-object v1, p0, Lub/e;->o:Lpb/x;

    invoke-virtual {v1}, Lpb/x;->f()Lpb/c;

    const/4 v9, 0x0

    invoke-direct {v0, v9}, Lsb/a;-><init>(Lpb/c;)V

    invoke-interface {v2, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    sget-object v0, Lub/a;->a:Lub/a;

    invoke-interface {v2, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    iget-boolean v0, p0, Lub/e;->q:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lub/e;->o:Lpb/x;

    invoke-virtual {v0}, Lpb/x;->u()Ljava/util/List;

    move-result-object v0

    invoke-static {v2, v0}, Lb8/j;->r(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    :cond_0
    new-instance v0, Lvb/b;

    iget-boolean v1, p0, Lub/e;->q:Z

    invoke-direct {v0, v1}, Lvb/b;-><init>(Z)V

    invoke-interface {v2, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    new-instance v10, Lvb/g;

    const/4 v3, 0x0

    const/4 v4, 0x0

    iget-object v5, p0, Lub/e;->p:Lpb/z;

    iget-object v0, p0, Lub/e;->o:Lpb/x;

    invoke-virtual {v0}, Lpb/x;->i()I

    move-result v6

    iget-object v0, p0, Lub/e;->o:Lpb/x;

    invoke-virtual {v0}, Lpb/x;->A()I

    move-result v7

    iget-object v0, p0, Lub/e;->o:Lpb/x;

    invoke-virtual {v0}, Lpb/x;->E()I

    move-result v8

    move-object v0, v10

    move-object v1, p0

    invoke-direct/range {v0 .. v8}, Lvb/g;-><init>(Lub/e;Ljava/util/List;ILub/c;Lpb/z;III)V

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lub/e;->p:Lpb/z;

    invoke-virtual {v10, v1}, Lvb/g;->e(Lpb/z;)Lpb/b0;

    move-result-object v1

    invoke-virtual {p0}, Lub/e;->K()Z

    move-result v2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_1

    invoke-virtual {p0, v9}, Lub/e;->s(Ljava/io/IOException;)Ljava/io/IOException;

    return-object v1

    :cond_1
    :try_start_1
    invoke-static {v1}, Lrb/b;->j(Ljava/io/Closeable;)V

    new-instance v1, Ljava/io/IOException;

    const-string v2, "Canceled"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v1

    goto :goto_0

    :catch_0
    move-exception v0

    const/4 v1, 0x1

    :try_start_2
    invoke-virtual {p0, v0}, Lub/e;->s(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v0, La8/s;

    const-string v2, "null cannot be cast to non-null type kotlin.Throwable"

    invoke-direct {v0, v2}, La8/s;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :catchall_1
    move-exception v0

    move v11, v1

    move-object v1, v0

    move v0, v11

    :goto_0
    if-nez v0, :cond_3

    invoke-virtual {p0, v9}, Lub/e;->s(Ljava/io/IOException;)Ljava/io/IOException;

    :cond_3
    throw v1
.end method

.method public final p(Lvb/g;)Lub/c;
    .locals 4

    const-string v0, "chain"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lub/e;->a:Lub/h;

    monitor-enter v0

    :try_start_0
    iget-boolean v1, p0, Lub/e;->l:Z

    const/4 v2, 0x1

    xor-int/2addr v1, v2

    if-eqz v1, :cond_4

    iget-object v1, p0, Lub/e;->g:Lub/c;

    const/4 v3, 0x0

    if-nez v1, :cond_0

    goto :goto_0

    :cond_0
    move v2, v3

    :goto_0
    if-eqz v2, :cond_3

    sget-object v1, La8/v;->a:La8/v;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    monitor-exit v0

    iget-object v0, p0, Lub/e;->e:Lub/d;

    if-nez v0, :cond_1

    invoke-static {}, Lm8/i;->o()V

    :cond_1
    iget-object v1, p0, Lub/e;->o:Lpb/x;

    invoke-virtual {v0, v1, p1}, Lub/d;->b(Lpb/x;Lvb/g;)Lvb/d;

    move-result-object p1

    new-instance v0, Lub/c;

    iget-object v1, p0, Lub/e;->b:Lpb/p;

    iget-object v2, p0, Lub/e;->e:Lub/d;

    if-nez v2, :cond_2

    invoke-static {}, Lm8/i;->o()V

    :cond_2
    invoke-direct {v0, p0, v1, v2, p1}, Lub/c;-><init>(Lub/e;Lpb/p;Lub/d;Lvb/d;)V

    iput-object v0, p0, Lub/e;->n:Lub/c;

    iget-object p1, p0, Lub/e;->a:Lub/h;

    monitor-enter p1

    :try_start_1
    iput-object v0, p0, Lub/e;->g:Lub/c;

    iput-boolean v3, p0, Lub/e;->h:Z

    iput-boolean v3, p0, Lub/e;->i:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p1

    throw v0

    :cond_3
    :try_start_2
    const-string p1, "Check failed."

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_4
    const-string p1, "released"

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :catchall_1
    move-exception p1

    monitor-exit v0

    throw p1
.end method

.method public final r(Lub/c;ZZLjava/io/IOException;)Ljava/io/IOException;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/io/IOException;",
            ">(",
            "Lub/c;",
            "ZZTE;)TE;"
        }
    .end annotation

    const-string v0, "exchange"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lub/e;->a:Lub/h;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Lub/e;->g:Lub/c;

    invoke-static {p1, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x1

    xor-int/2addr p1, v1

    if-eqz p1, :cond_0

    monitor-exit v0

    return-object p4

    :cond_0
    const/4 p1, 0x0

    if-eqz p2, :cond_1

    :try_start_1
    iget-boolean p2, p0, Lub/e;->h:Z

    xor-int/2addr p2, v1

    iput-boolean v1, p0, Lub/e;->h:Z

    goto :goto_0

    :cond_1
    move p2, p1

    :goto_0
    if-eqz p3, :cond_3

    iget-boolean p3, p0, Lub/e;->i:Z

    if-nez p3, :cond_2

    move p2, v1

    :cond_2
    iput-boolean v1, p0, Lub/e;->i:Z

    :cond_3
    iget-boolean p3, p0, Lub/e;->h:Z

    if-eqz p3, :cond_5

    iget-boolean p3, p0, Lub/e;->i:Z

    if-eqz p3, :cond_5

    if-eqz p2, :cond_5

    iget-object p2, p0, Lub/e;->g:Lub/c;

    if-nez p2, :cond_4

    invoke-static {}, Lm8/i;->o()V

    :cond_4
    invoke-virtual {p2}, Lub/c;->h()Lub/f;

    move-result-object p2

    invoke-virtual {p2}, Lub/f;->t()I

    move-result p3

    add-int/2addr p3, v1

    invoke-virtual {p2, p3}, Lub/f;->E(I)V

    const/4 p2, 0x0

    iput-object p2, p0, Lub/e;->g:Lub/c;

    goto :goto_1

    :cond_5
    move v1, p1

    :goto_1
    sget-object p2, La8/v;->a:La8/v;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v0

    if-eqz v1, :cond_6

    invoke-direct {p0, p4, p1}, Lub/e;->q(Ljava/io/IOException;Z)Ljava/io/IOException;

    move-result-object p4

    :cond_6
    return-object p4

    :catchall_0
    move-exception p1

    monitor-exit v0

    throw p1
.end method

.method public final s(Ljava/io/IOException;)Ljava/io/IOException;
    .locals 2

    iget-object v0, p0, Lub/e;->a:Lub/h;

    monitor-enter v0

    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Lub/e;->l:Z

    sget-object v1, La8/v;->a:La8/v;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lub/e;->q(Ljava/io/IOException;Z)Ljava/io/IOException;

    move-result-object p1

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit v0

    throw p1
.end method

.method public final t()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lub/e;->p:Lpb/z;

    invoke-virtual {v0}, Lpb/z;->j()Lpb/t;

    move-result-object v0

    invoke-virtual {v0}, Lpb/t;->p()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final u()Ljava/net/Socket;
    .locals 5

    iget-object v0, p0, Lub/e;->a:Lub/h;

    sget-boolean v1, Lrb/b;->h:Z

    if-eqz v1, :cond_1

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Thread "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    const-string v4, "Thread.currentThread()"

    invoke-static {v3, v4}, Lm8/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " MUST hold lock on "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    :cond_1
    :goto_0
    iget-object v0, p0, Lub/e;->f:Lub/f;

    if-nez v0, :cond_2

    invoke-static {}, Lm8/i;->o()V

    :cond_2
    invoke-virtual {v0}, Lub/f;->p()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    const/4 v4, -0x1

    if-eqz v3, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/ref/Reference;

    invoke-virtual {v3}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lub/e;

    invoke-static {v3, p0}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_2

    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_4
    move v2, v4

    :goto_2
    if-eq v2, v4, :cond_5

    const/4 v1, 0x1

    :cond_5
    if-eqz v1, :cond_8

    iget-object v0, p0, Lub/e;->f:Lub/f;

    if-nez v0, :cond_6

    invoke-static {}, Lm8/i;->o()V

    :cond_6
    invoke-virtual {v0}, Lub/f;->p()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    const/4 v1, 0x0

    iput-object v1, p0, Lub/e;->f:Lub/f;

    invoke-virtual {v0}, Lub/f;->p()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lub/f;->C(J)V

    iget-object v2, p0, Lub/e;->a:Lub/h;

    invoke-virtual {v2, v0}, Lub/h;->c(Lub/f;)Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-virtual {v0}, Lub/f;->F()Ljava/net/Socket;

    move-result-object v0

    return-object v0

    :cond_7
    return-object v1

    :cond_8
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Check failed."

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final v()Z
    .locals 1

    iget-object v0, p0, Lub/e;->e:Lub/d;

    if-nez v0, :cond_0

    invoke-static {}, Lm8/i;->o()V

    :cond_0
    invoke-virtual {v0}, Lub/d;->f()Z

    move-result v0

    return v0
.end method

.method public final w()V
    .locals 2

    iget-boolean v0, p0, Lub/e;->k:Z

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    if-eqz v0, :cond_0

    iput-boolean v1, p0, Lub/e;->k:Z

    iget-object v0, p0, Lub/e;->c:Lub/e$c;

    invoke-virtual {v0}, Ldc/d;->s()Z

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Check failed."

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
