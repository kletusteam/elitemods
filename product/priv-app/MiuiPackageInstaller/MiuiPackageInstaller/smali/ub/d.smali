.class public final Lub/d;
.super Ljava/lang/Object;


# instance fields
.field private a:Lub/k$b;

.field private b:Lub/k;

.field private c:Lub/f;

.field private d:I

.field private e:I

.field private f:I

.field private g:Lpb/d0;

.field private final h:Lub/h;

.field private final i:Lpb/a;

.field private final j:Lub/e;

.field private final k:Lpb/p;


# direct methods
.method public constructor <init>(Lub/h;Lpb/a;Lub/e;Lpb/p;)V
    .locals 1

    const-string v0, "connectionPool"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "address"

    invoke-static {p2, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "call"

    invoke-static {p3, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "eventListener"

    invoke-static {p4, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lub/d;->h:Lub/h;

    iput-object p2, p0, Lub/d;->i:Lpb/a;

    iput-object p3, p0, Lub/d;->j:Lub/e;

    iput-object p4, p0, Lub/d;->k:Lpb/p;

    return-void
.end method

.method private final c(IIIIZ)Lub/f;
    .locals 18

    move-object/from16 v1, p0

    new-instance v0, Lm8/t;

    invoke-direct {v0}, Lm8/t;-><init>()V

    iget-object v2, v1, Lub/d;->h:Lub/h;

    monitor-enter v2

    :try_start_0
    iget-object v3, v1, Lub/d;->j:Lub/e;

    invoke-virtual {v3}, Lub/e;->K()Z

    move-result v3

    if-nez v3, :cond_1f

    iget-object v3, v1, Lub/d;->j:Lub/e;

    invoke-virtual {v3}, Lub/e;->k()Lub/f;

    move-result-object v3

    iput-object v3, v0, Lm8/t;->a:Ljava/lang/Object;

    const/4 v4, 0x0

    if-eqz v3, :cond_1

    invoke-virtual {v3}, Lub/f;->r()Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {v3}, Lub/f;->b()Lpb/d0;

    move-result-object v3

    invoke-virtual {v3}, Lpb/d0;->a()Lpb/a;

    move-result-object v3

    invoke-virtual {v3}, Lpb/a;->l()Lpb/t;

    move-result-object v3

    invoke-virtual {v1, v3}, Lub/d;->h(Lpb/t;)Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    iget-object v3, v1, Lub/d;->j:Lub/e;

    invoke-virtual {v3}, Lub/e;->u()Ljava/net/Socket;

    move-result-object v3

    goto :goto_0

    :cond_1
    move-object v3, v4

    :goto_0
    iget-object v5, v1, Lub/d;->j:Lub/e;

    invoke-virtual {v5}, Lub/e;->k()Lub/f;

    move-result-object v5

    if-eqz v5, :cond_2

    iget-object v5, v1, Lub/d;->j:Lub/e;

    invoke-virtual {v5}, Lub/e;->k()Lub/f;

    move-result-object v5

    iput-object v4, v0, Lm8/t;->a:Ljava/lang/Object;

    goto :goto_1

    :cond_2
    move-object v5, v4

    :goto_1
    const/4 v6, 0x1

    const/4 v7, 0x0

    if-nez v5, :cond_4

    iput v7, v1, Lub/d;->d:I

    iput v7, v1, Lub/d;->e:I

    iput v7, v1, Lub/d;->f:I

    iget-object v8, v1, Lub/d;->h:Lub/h;

    iget-object v9, v1, Lub/d;->i:Lpb/a;

    iget-object v10, v1, Lub/d;->j:Lub/e;

    invoke-virtual {v8, v9, v10, v4, v7}, Lub/h;->a(Lpb/a;Lub/e;Ljava/util/List;Z)Z

    move-result v8

    if-eqz v8, :cond_3

    iget-object v5, v1, Lub/d;->j:Lub/e;

    invoke-virtual {v5}, Lub/e;->k()Lub/f;

    move-result-object v5

    move-object v8, v4

    move v9, v6

    goto :goto_3

    :cond_3
    iget-object v8, v1, Lub/d;->g:Lpb/d0;

    if-eqz v8, :cond_4

    iput-object v4, v1, Lub/d;->g:Lpb/d0;

    goto :goto_2

    :cond_4
    move-object v8, v4

    :goto_2
    move v9, v7

    :goto_3
    sget-object v10, La8/v;->a:La8/v;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    monitor-exit v2

    if-eqz v3, :cond_5

    invoke-static {v3}, Lrb/b;->k(Ljava/net/Socket;)V

    :cond_5
    iget-object v0, v0, Lm8/t;->a:Ljava/lang/Object;

    move-object v2, v0

    check-cast v2, Lub/f;

    if-eqz v2, :cond_7

    iget-object v2, v1, Lub/d;->k:Lpb/p;

    iget-object v3, v1, Lub/d;->j:Lub/e;

    check-cast v0, Lub/f;

    if-nez v0, :cond_6

    invoke-static {}, Lm8/i;->o()V

    :cond_6
    invoke-virtual {v2, v3, v0}, Lpb/p;->i(Lpb/e;Lpb/i;)V

    :cond_7
    if-eqz v9, :cond_9

    iget-object v0, v1, Lub/d;->k:Lpb/p;

    iget-object v2, v1, Lub/d;->j:Lub/e;

    if-nez v5, :cond_8

    invoke-static {}, Lm8/i;->o()V

    :cond_8
    invoke-virtual {v0, v2, v5}, Lpb/p;->h(Lpb/e;Lpb/i;)V

    :cond_9
    if-eqz v5, :cond_a

    return-object v5

    :cond_a
    if-nez v8, :cond_e

    iget-object v0, v1, Lub/d;->a:Lub/k$b;

    if-eqz v0, :cond_c

    if-nez v0, :cond_b

    invoke-static {}, Lm8/i;->o()V

    :cond_b
    invoke-virtual {v0}, Lub/k$b;->b()Z

    move-result v0

    if-nez v0, :cond_e

    :cond_c
    iget-object v0, v1, Lub/d;->b:Lub/k;

    if-nez v0, :cond_d

    new-instance v0, Lub/k;

    iget-object v2, v1, Lub/d;->i:Lpb/a;

    iget-object v3, v1, Lub/d;->j:Lub/e;

    invoke-virtual {v3}, Lub/e;->j()Lpb/x;

    move-result-object v3

    invoke-virtual {v3}, Lpb/x;->r()Lub/i;

    move-result-object v3

    iget-object v10, v1, Lub/d;->j:Lub/e;

    iget-object v11, v1, Lub/d;->k:Lpb/p;

    invoke-direct {v0, v2, v3, v10, v11}, Lub/k;-><init>(Lpb/a;Lub/i;Lpb/e;Lpb/p;)V

    iput-object v0, v1, Lub/d;->b:Lub/k;

    :cond_d
    invoke-virtual {v0}, Lub/k;->d()Lub/k$b;

    move-result-object v0

    iput-object v0, v1, Lub/d;->a:Lub/k$b;

    move v0, v6

    goto :goto_4

    :cond_e
    move v0, v7

    :goto_4
    iget-object v2, v1, Lub/d;->h:Lub/h;

    monitor-enter v2

    :try_start_1
    iget-object v3, v1, Lub/d;->j:Lub/e;

    invoke-virtual {v3}, Lub/e;->K()Z

    move-result v3

    if-nez v3, :cond_1e

    if-eqz v0, :cond_10

    iget-object v0, v1, Lub/d;->a:Lub/k$b;

    if-nez v0, :cond_f

    invoke-static {}, Lm8/i;->o()V

    :cond_f
    invoke-virtual {v0}, Lub/k$b;->a()Ljava/util/List;

    move-result-object v0

    iget-object v3, v1, Lub/d;->h:Lub/h;

    iget-object v10, v1, Lub/d;->i:Lpb/a;

    iget-object v11, v1, Lub/d;->j:Lub/e;

    invoke-virtual {v3, v10, v11, v0, v7}, Lub/h;->a(Lpb/a;Lub/e;Ljava/util/List;Z)Z

    move-result v3

    if-eqz v3, :cond_11

    iget-object v3, v1, Lub/d;->j:Lub/e;

    invoke-virtual {v3}, Lub/e;->k()Lub/f;

    move-result-object v5

    move v9, v6

    goto :goto_5

    :cond_10
    move-object v0, v4

    :cond_11
    :goto_5
    if-nez v9, :cond_15

    if-nez v8, :cond_13

    iget-object v3, v1, Lub/d;->a:Lub/k$b;

    if-nez v3, :cond_12

    invoke-static {}, Lm8/i;->o()V

    :cond_12
    invoke-virtual {v3}, Lub/k$b;->c()Lpb/d0;

    move-result-object v3

    move-object v8, v3

    :cond_13
    new-instance v5, Lub/f;

    iget-object v3, v1, Lub/d;->h:Lub/h;

    if-nez v8, :cond_14

    invoke-static {}, Lm8/i;->o()V

    :cond_14
    invoke-direct {v5, v3, v8}, Lub/f;-><init>(Lub/h;Lpb/d0;)V

    iput-object v5, v1, Lub/d;->c:Lub/f;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_15
    monitor-exit v2

    if-eqz v9, :cond_18

    iget-object v0, v1, Lub/d;->k:Lpb/p;

    iget-object v2, v1, Lub/d;->j:Lub/e;

    if-nez v5, :cond_16

    invoke-static {}, Lm8/i;->o()V

    :cond_16
    invoke-virtual {v0, v2, v5}, Lpb/p;->h(Lpb/e;Lpb/i;)V

    if-nez v5, :cond_17

    invoke-static {}, Lm8/i;->o()V

    :cond_17
    return-object v5

    :cond_18
    if-nez v5, :cond_19

    invoke-static {}, Lm8/i;->o()V

    :cond_19
    iget-object v2, v1, Lub/d;->j:Lub/e;

    iget-object v3, v1, Lub/d;->k:Lpb/p;

    move-object v10, v5

    move/from16 v11, p1

    move/from16 v12, p2

    move/from16 v13, p3

    move/from16 v14, p4

    move/from16 v15, p5

    move-object/from16 v16, v2

    move-object/from16 v17, v3

    invoke-virtual/range {v10 .. v17}, Lub/f;->h(IIIIZLpb/e;Lpb/p;)V

    iget-object v2, v1, Lub/d;->j:Lub/e;

    invoke-virtual {v2}, Lub/e;->j()Lpb/x;

    move-result-object v2

    invoke-virtual {v2}, Lpb/x;->r()Lub/i;

    move-result-object v2

    invoke-virtual {v5}, Lub/f;->b()Lpb/d0;

    move-result-object v3

    invoke-virtual {v2, v3}, Lub/i;->a(Lpb/d0;)V

    iget-object v2, v1, Lub/d;->h:Lub/h;

    monitor-enter v2

    :try_start_2
    iput-object v4, v1, Lub/d;->c:Lub/f;

    iget-object v3, v1, Lub/d;->h:Lub/h;

    iget-object v7, v1, Lub/d;->i:Lpb/a;

    iget-object v9, v1, Lub/d;->j:Lub/e;

    invoke-virtual {v3, v7, v9, v0, v6}, Lub/h;->a(Lpb/a;Lub/e;Ljava/util/List;Z)Z

    move-result v0

    if-eqz v0, :cond_1a

    invoke-virtual {v5, v6}, Lub/f;->D(Z)V

    invoke-virtual {v5}, Lub/f;->F()Ljava/net/Socket;

    move-result-object v4

    iget-object v0, v1, Lub/d;->j:Lub/e;

    invoke-virtual {v0}, Lub/e;->k()Lub/f;

    move-result-object v5

    iput-object v8, v1, Lub/d;->g:Lpb/d0;

    goto :goto_6

    :cond_1a
    iget-object v0, v1, Lub/d;->h:Lub/h;

    invoke-virtual {v0, v5}, Lub/h;->e(Lub/f;)V

    iget-object v0, v1, Lub/d;->j:Lub/e;

    invoke-virtual {v0, v5}, Lub/e;->d(Lub/f;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_6
    monitor-exit v2

    if-eqz v4, :cond_1b

    invoke-static {v4}, Lrb/b;->k(Ljava/net/Socket;)V

    :cond_1b
    iget-object v0, v1, Lub/d;->k:Lpb/p;

    iget-object v2, v1, Lub/d;->j:Lub/e;

    if-nez v5, :cond_1c

    invoke-static {}, Lm8/i;->o()V

    :cond_1c
    invoke-virtual {v0, v2, v5}, Lpb/p;->h(Lpb/e;Lpb/i;)V

    if-nez v5, :cond_1d

    invoke-static {}, Lm8/i;->o()V

    :cond_1d
    return-object v5

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_1e
    :try_start_3
    new-instance v0, Ljava/io/IOException;

    const-string v3, "Canceled"

    invoke-direct {v0, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_1f
    :try_start_4
    new-instance v0, Ljava/io/IOException;

    const-string v3, "Canceled"

    invoke-direct {v0, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    :catchall_2
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method private final d(IIIIZZ)Lub/f;
    .locals 2

    :goto_0
    invoke-direct/range {p0 .. p5}, Lub/d;->c(IIIIZ)Lub/f;

    move-result-object v0

    invoke-virtual {v0, p6}, Lub/f;->w(Z)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lub/f;->A()V

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method private final g()Z
    .locals 4

    iget v0, p0, Lub/d;->d:I

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-gt v0, v2, :cond_1

    iget v0, p0, Lub/d;->e:I

    if-gt v0, v2, :cond_1

    iget v0, p0, Lub/d;->f:I

    if-lez v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lub/d;->j:Lub/e;

    invoke-virtual {v0}, Lub/e;->k()Lub/f;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lub/f;->s()I

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v0}, Lub/f;->b()Lpb/d0;

    move-result-object v0

    invoke-virtual {v0}, Lpb/d0;->a()Lpb/a;

    move-result-object v0

    invoke-virtual {v0}, Lpb/a;->l()Lpb/t;

    move-result-object v0

    iget-object v3, p0, Lub/d;->i:Lpb/a;

    invoke-virtual {v3}, Lpb/a;->l()Lpb/t;

    move-result-object v3

    invoke-static {v0, v3}, Lrb/b;->g(Lpb/t;Lpb/t;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v1, v2

    :cond_1
    :goto_0
    return v1
.end method


# virtual methods
.method public final a()Lub/f;
    .locals 5

    iget-object v0, p0, Lub/d;->h:Lub/h;

    sget-boolean v1, Lrb/b;->h:Z

    if-eqz v1, :cond_1

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Thread "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    const-string v4, "Thread.currentThread()"

    invoke-static {v3, v4}, Lm8/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " MUST hold lock on "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    :cond_1
    :goto_0
    iget-object v0, p0, Lub/d;->c:Lub/f;

    return-object v0
.end method

.method public final b(Lpb/x;Lvb/g;)Lvb/d;
    .locals 8

    const-string v0, "client"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "chain"

    invoke-static {p2, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    :try_start_0
    invoke-virtual {p2}, Lvb/g;->j()I

    move-result v2

    invoke-virtual {p2}, Lvb/g;->l()I

    move-result v3

    invoke-virtual {p2}, Lvb/g;->n()I

    move-result v4

    invoke-virtual {p1}, Lpb/x;->v()I

    move-result v5

    invoke-virtual {p1}, Lpb/x;->B()Z

    move-result v6

    invoke-virtual {p2}, Lvb/g;->m()Lpb/z;

    move-result-object v0

    invoke-virtual {v0}, Lpb/z;->g()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GET"

    invoke-static {v0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v7, v0, 0x1

    move-object v1, p0

    invoke-direct/range {v1 .. v7}, Lub/d;->d(IIIIZZ)Lub/f;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lub/f;->y(Lpb/x;Lvb/g;)Lvb/d;

    move-result-object p1
    :try_end_0
    .catch Lub/j; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    invoke-virtual {p0, p1}, Lub/d;->i(Ljava/io/IOException;)V

    new-instance p2, Lub/j;

    invoke-direct {p2, p1}, Lub/j;-><init>(Ljava/io/IOException;)V

    throw p2

    :catch_1
    move-exception p1

    invoke-virtual {p1}, Lub/j;->c()Ljava/io/IOException;

    move-result-object p2

    invoke-virtual {p0, p2}, Lub/d;->i(Ljava/io/IOException;)V

    throw p1
.end method

.method public final e()Lpb/a;
    .locals 1

    iget-object v0, p0, Lub/d;->i:Lpb/a;

    return-object v0
.end method

.method public final f()Z
    .locals 3

    iget-object v0, p0, Lub/d;->h:Lub/h;

    monitor-enter v0

    :try_start_0
    iget v1, p0, Lub/d;->d:I

    if-nez v1, :cond_0

    iget v1, p0, Lub/d;->e:I

    if-nez v1, :cond_0

    iget v1, p0, Lub/d;->f:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    const/4 v1, 0x0

    monitor-exit v0

    return v1

    :cond_0
    :try_start_1
    iget-object v1, p0, Lub/d;->g:Lpb/d0;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    monitor-exit v0

    return v2

    :cond_1
    :try_start_2
    invoke-direct {p0}, Lub/d;->g()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lub/d;->j:Lub/e;

    invoke-virtual {v1}, Lub/e;->k()Lub/f;

    move-result-object v1

    if-nez v1, :cond_2

    invoke-static {}, Lm8/i;->o()V

    :cond_2
    invoke-virtual {v1}, Lub/f;->b()Lpb/d0;

    move-result-object v1

    iput-object v1, p0, Lub/d;->g:Lpb/d0;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit v0

    return v2

    :cond_3
    :try_start_3
    iget-object v1, p0, Lub/d;->a:Lub/k$b;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lub/k$b;->b()Z

    move-result v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-ne v1, v2, :cond_4

    monitor-exit v0

    return v2

    :cond_4
    :try_start_4
    iget-object v1, p0, Lub/d;->b:Lub/k;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lub/k;->b()Z

    move-result v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    monitor-exit v0

    return v1

    :cond_5
    monitor-exit v0

    return v2

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public final h(Lpb/t;)Z
    .locals 3

    const-string v0, "url"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lub/d;->i:Lpb/a;

    invoke-virtual {v0}, Lpb/a;->l()Lpb/t;

    move-result-object v0

    invoke-virtual {p1}, Lpb/t;->n()I

    move-result v1

    invoke-virtual {v0}, Lpb/t;->n()I

    move-result v2

    if-ne v1, v2, :cond_0

    invoke-virtual {p1}, Lpb/t;->i()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0}, Lpb/t;->i()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public final i(Ljava/io/IOException;)V
    .locals 4

    const-string v0, "e"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lub/d;->h:Lub/h;

    sget-boolean v1, Lrb/b;->h:Z

    if-eqz v1, :cond_1

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Thread "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    const-string v3, "Thread.currentThread()"

    invoke-static {v2, v3}, Lm8/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " MUST NOT hold lock on "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw p1

    :cond_1
    :goto_0
    iget-object v0, p0, Lub/d;->h:Lub/h;

    monitor-enter v0

    const/4 v1, 0x0

    :try_start_0
    iput-object v1, p0, Lub/d;->g:Lpb/d0;

    instance-of v1, p1, Lxb/n;

    if-eqz v1, :cond_2

    move-object v1, p1

    check-cast v1, Lxb/n;

    iget-object v1, v1, Lxb/n;->a:Lxb/b;

    sget-object v2, Lxb/b;->i:Lxb/b;

    if-ne v1, v2, :cond_2

    iget p1, p0, Lub/d;->d:I

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, Lub/d;->d:I

    goto :goto_1

    :cond_2
    instance-of p1, p1, Lxb/a;

    if-eqz p1, :cond_3

    iget p1, p0, Lub/d;->e:I

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, Lub/d;->e:I

    goto :goto_1

    :cond_3
    iget p1, p0, Lub/d;->f:I

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, Lub/d;->f:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_1
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0

    throw p1
.end method
