.class public final Lr6/d;
.super Lr6/b;

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation runtime La5/k;
    value = "source_authority_info"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lr6/d$a;
    }
.end annotation


# static fields
.field public static final e:Lr6/d$a;


# instance fields
.field private a:Ljava/lang/String;
    .annotation runtime La5/c;
        value = "package_name"
    .end annotation

    .annotation runtime La5/j;
        value = .enum Lc5/a;->a:Lc5/a;
    .end annotation
.end field

.field private b:Ljava/lang/Integer;
    .annotation runtime La5/c;
        value = "uid"
    .end annotation
.end field

.field private c:Ljava/lang/String;
    .annotation runtime La5/c;
        value = "name"
    .end annotation
.end field

.field private d:J
    .annotation runtime La5/c;
        value = "allow_time"
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lr6/d$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lr6/d$a;-><init>(Lm8/g;)V

    sput-object v0, Lr6/d;->e:Lr6/d$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lr6/b;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lr6/d;->a:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lr6/d;->b:Ljava/lang/Integer;

    iput-object v0, p0, Lr6/d;->c:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    iget-wide v0, p0, Lr6/d;->d:J

    return-wide v0
.end method

.method public final b(J)V
    .locals 0

    iput-wide p1, p0, Lr6/d;->d:J

    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lr6/d;->c:Ljava/lang/String;

    return-void
.end method

.method public final d(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lr6/d;->a:Ljava/lang/String;

    return-void
.end method

.method public final e(Ljava/lang/Integer;)V
    .locals 0

    iput-object p1, p0, Lr6/d;->b:Ljava/lang/Integer;

    return-void
.end method
