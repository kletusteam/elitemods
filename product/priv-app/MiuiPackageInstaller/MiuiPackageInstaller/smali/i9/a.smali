.class public Li9/a;
.super Li9/b;


# direct methods
.method public static i(Lmiuix/animation/b;Lf9/c;)V
    .locals 2

    invoke-static {p0}, Li9/a;->j(Lmiuix/animation/b;)Landroid/view/View;

    move-result-object p0

    invoke-static {p0}, Li9/a;->k(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    invoke-static {p0}, Li9/c;->h(Landroid/view/View;)Li9/c;

    move-result-object p0

    iget-object p1, p1, Lf9/c;->f:Le9/c;

    iget-wide v0, p1, Le9/c;->i:D

    double-to-int p1, v0

    if-eqz p0, :cond_1

    invoke-static {p1}, Landroid/graphics/Color;->alpha(I)I

    move-result p1

    if-nez p1, :cond_1

    invoke-virtual {p0}, Li9/c;->u()V

    :cond_1
    return-void
.end method

.method private static j(Lmiuix/animation/b;)Landroid/view/View;
    .locals 1

    instance-of v0, p0, Lmiuix/animation/ViewTarget;

    if-eqz v0, :cond_0

    check-cast p0, Lmiuix/animation/ViewTarget;

    invoke-virtual {p0}, Lmiuix/animation/ViewTarget;->z()Landroid/view/View;

    move-result-object p0

    return-object p0

    :cond_0
    const/4 p0, 0x0

    return-object p0
.end method

.method private static k(Landroid/view/View;)Z
    .locals 0

    if-eqz p0, :cond_0

    const/4 p0, 0x0

    goto :goto_0

    :cond_0
    const/4 p0, 0x1

    :goto_0
    return p0
.end method

.method public static l(Lmiuix/animation/b;Lf9/c;)V
    .locals 2

    invoke-static {p0}, Li9/a;->j(Lmiuix/animation/b;)Landroid/view/View;

    move-result-object p0

    invoke-static {p0}, Li9/a;->k(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object p1, p1, Lf9/c;->f:Le9/c;

    iget p1, p1, Le9/c;->f:I

    invoke-static {p0}, Li9/c;->v(Landroid/view/View;)Li9/c;

    move-result-object v0

    sget v1, Lmiuix/animation/m;->e:I

    invoke-virtual {p0, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object p0

    instance-of v1, p0, Ljava/lang/Float;

    if-nez v1, :cond_1

    instance-of v1, p0, Ljava/lang/Integer;

    if-eqz v1, :cond_2

    :cond_1
    check-cast p0, Ljava/lang/Float;

    invoke-virtual {p0}, Ljava/lang/Float;->floatValue()F

    move-result p0

    invoke-virtual {v0, p0}, Li9/c;->y(F)V

    :cond_2
    invoke-static {}, Lj9/b;->i()I

    move-result p0

    const/4 v1, -0x1

    if-nez p0, :cond_3

    if-ne p1, v1, :cond_3

    const/4 p1, 0x1

    goto :goto_0

    :cond_3
    if-ne p1, v1, :cond_4

    const/4 p1, 0x0

    :cond_4
    :goto_0
    and-int/lit8 p0, p1, 0x3

    invoke-virtual {v0, p0}, Li9/c;->p(I)V

    return-void
.end method
