.class public Li9/b;
.super Ljava/lang/Object;


# static fields
.field static final a:Lg9/j;

.field static final b:Lg9/a;

.field static final c:Lg9/e;

.field static d:Lg9/d;

.field static final e:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal<",
            "Lg9/d;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lg9/j;

    invoke-direct {v0}, Lg9/j;-><init>()V

    sput-object v0, Li9/b;->a:Lg9/j;

    new-instance v0, Lg9/a;

    invoke-direct {v0}, Lg9/a;-><init>()V

    sput-object v0, Li9/b;->b:Lg9/a;

    new-instance v0, Lg9/e;

    invoke-direct {v0}, Lg9/e;-><init>()V

    sput-object v0, Li9/b;->c:Lg9/e;

    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    sput-object v0, Li9/b;->e:Ljava/lang/ThreadLocal;

    return-void
.end method

.method public static a(Lmiuix/animation/b;Le9/b;JJJ)V
    .locals 8

    iget-wide v2, p1, Le9/b;->i:J

    sub-long v2, p2, v2

    iget-object v0, p1, Le9/b;->f:Lj9/c$a;

    iget v0, v0, Lj9/c$a;->a:I

    invoke-static {v0}, Lj9/c;->f(I)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, p0

    move-object v1, p1

    move-wide v4, p4

    move-wide v6, p6

    invoke-static/range {v0 .. v7}, Li9/b;->h(Lmiuix/animation/b;Le9/b;JJJ)V

    goto :goto_0

    :cond_0
    invoke-static {p1, v2, v3}, Li9/b;->g(Le9/b;J)V

    :goto_0
    return-void
.end method

.method private static b(Le9/b;D)V
    .locals 13

    iget-wide v1, p0, Le9/b;->b:D

    iget-object v0, p0, Le9/b;->f:Lj9/c$a;

    iget v0, v0, Lj9/c$a;->a:I

    invoke-static {v0}, Li9/b;->c(I)Lg9/f;

    move-result-object v0

    if-eqz v0, :cond_1

    instance-of v3, v0, Lg9/j;

    if-eqz v3, :cond_0

    iget-wide v3, p0, Le9/b;->m:D

    invoke-static {v3, v4}, Le9/j;->e(D)Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_0

    :cond_0
    iget-object v3, p0, Le9/b;->f:Lj9/c$a;

    iget-object v3, v3, Lj9/c$a;->c:[D

    const/4 v4, 0x0

    aget-wide v5, v3, v4

    const/4 v7, 0x1

    aget-wide v8, v3, v7

    const/4 v3, 0x2

    new-array v10, v3, [D

    iget-wide v11, p0, Le9/b;->m:D

    aput-wide v11, v10, v4

    iget-wide v3, p0, Le9/b;->n:D

    aput-wide v3, v10, v7

    move-wide v3, v5

    move-wide v5, v8

    move-wide v7, p1

    move-object v9, v10

    invoke-interface/range {v0 .. v9}, Lg9/f;->a(DDDD[D)D

    move-result-wide v0

    iget-wide v2, p0, Le9/b;->n:D

    iget-wide v4, p0, Le9/b;->b:D

    add-double/2addr v4, v0

    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v4, v6

    mul-double/2addr v4, p1

    add-double/2addr v2, v4

    iput-wide v2, p0, Le9/b;->n:D

    iput-wide v0, p0, Le9/b;->b:D

    goto :goto_1

    :cond_1
    :goto_0
    iget-wide p1, p0, Le9/b;->m:D

    iput-wide p1, p0, Le9/b;->n:D

    const-wide/16 p1, 0x0

    iput-wide p1, p0, Le9/b;->b:D

    :goto_1
    return-void
.end method

.method public static c(I)Lg9/f;
    .locals 1

    const/4 v0, -0x4

    if-eq p0, v0, :cond_2

    const/4 v0, -0x3

    if-eq p0, v0, :cond_1

    const/4 v0, -0x2

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    :cond_0
    sget-object p0, Li9/b;->a:Lg9/j;

    return-object p0

    :cond_1
    sget-object p0, Li9/b;->b:Lg9/a;

    return-object p0

    :cond_2
    sget-object p0, Li9/b;->c:Lg9/e;

    return-object p0
.end method

.method static d(Lg9/d;Lh9/b;IDDJ)Z
    .locals 6

    move-object v0, p0

    move v1, p2

    move-wide v2, p3

    move-wide v4, p5

    invoke-virtual/range {v0 .. v5}, Lg9/d;->c(IDD)Z

    move-result p0

    xor-int/lit8 p0, p0, 0x1

    const/4 p2, 0x0

    if-eqz p0, :cond_1

    const-wide/16 p3, 0x2710

    cmp-long p3, p7, p3

    if-lez p3, :cond_1

    invoke-static {}, Lj9/f;->d()Z

    move-result p0

    if-eqz p0, :cond_0

    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "animation for "

    invoke-virtual {p0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/util/Property;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " stopped for running time too long, totalTime = "

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p7, p8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    new-array p1, p2, [Ljava/lang/Object;

    invoke-static {p0, p1}, Lj9/f;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    move p0, p2

    :cond_1
    return p0
.end method

.method private static e(Le9/b;)Z
    .locals 1

    iget-object p0, p0, Le9/b;->f:Lj9/c$a;

    iget p0, p0, Lj9/c$a;->a:I

    const/4 v0, -0x2

    if-ne p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private static f(Le9/b;)V
    .locals 2

    invoke-static {p0}, Li9/b;->e(Le9/b;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-wide v0, p0, Le9/b;->m:D

    iput-wide v0, p0, Le9/b;->n:D

    return-void
.end method

.method private static g(Le9/b;J)V
    .locals 4

    iget-object v0, p0, Le9/b;->f:Lj9/c$a;

    check-cast v0, Lj9/c$b;

    invoke-static {v0}, Lj9/c;->c(Lj9/c$b;)Landroid/animation/TimeInterpolator;

    move-result-object v1

    iget-wide v2, v0, Lj9/c$b;->d:J

    cmp-long v0, p1, v2

    if-gez v0, :cond_0

    long-to-float p1, p1

    long-to-float p2, v2

    div-float/2addr p1, p2

    invoke-interface {v1, p1}, Landroid/animation/TimeInterpolator;->getInterpolation(F)F

    move-result p1

    float-to-double p1, p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x3

    invoke-virtual {p0, p1}, Le9/b;->d(B)V

    const-wide/high16 p1, 0x3ff0000000000000L    # 1.0

    :goto_0
    iput-wide p1, p0, Le9/b;->k:D

    iput-wide p1, p0, Le9/b;->n:D

    return-void
.end method

.method private static h(Lmiuix/animation/b;Le9/b;JJJ)V
    .locals 15

    move-object/from16 v0, p1

    move-wide/from16 v1, p4

    move-wide/from16 v3, p6

    cmp-long v5, v1, v3

    if-lez v5, :cond_0

    long-to-float v1, v1

    long-to-float v2, v3

    div-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x1

    :goto_0
    long-to-double v2, v3

    const-wide v4, 0x408f400000000000L    # 1000.0

    div-double/2addr v2, v4

    sget-object v4, Li9/b;->e:Ljava/lang/ThreadLocal;

    const-class v5, Lg9/d;

    invoke-static {v4, v5}, Lj9/a;->e(Ljava/lang/ThreadLocal;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lg9/d;

    sput-object v4, Li9/b;->d:Lg9/d;

    iget-object v5, v0, Le9/b;->a:Lh9/b;

    iget-wide v6, v0, Le9/b;->m:D

    move-object v8, p0

    invoke-virtual {v4, p0, v5, v6, v7}, Lg9/d;->a(Lmiuix/animation/b;Lh9/b;D)V

    const/4 v4, 0x0

    move v5, v4

    :goto_1
    if-ge v5, v1, :cond_3

    invoke-static {v0, v2, v3}, Li9/b;->b(Le9/b;D)V

    sget-object v6, Li9/b;->d:Lg9/d;

    iget-object v7, v0, Le9/b;->a:Lh9/b;

    iget-object v8, v0, Le9/b;->f:Lj9/c$a;

    iget v8, v8, Lj9/c$a;->a:I

    iget-wide v9, v0, Le9/b;->n:D

    iget-wide v11, v0, Le9/b;->b:D

    move-wide/from16 v13, p2

    invoke-static/range {v6 .. v14}, Li9/b;->d(Lg9/d;Lh9/b;IDDJ)Z

    move-result v6

    if-nez v6, :cond_2

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Le9/b;->d(B)V

    invoke-static {}, Lj9/f;->d()Z

    move-result v1

    if-eqz v1, :cond_1

    new-array v1, v4, [Ljava/lang/Object;

    const-string v2, "----- updatePhysicsAnim data.setOp(AnimTask.OP_END)"

    invoke-static {v2, v1}, Lj9/f;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_1
    invoke-static/range {p1 .. p1}, Li9/b;->f(Le9/b;)V

    goto :goto_2

    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_3
    :goto_2
    return-void
.end method
