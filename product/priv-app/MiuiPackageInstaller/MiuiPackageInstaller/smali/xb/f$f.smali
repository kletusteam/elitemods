.class public final Lxb/f$f;
.super Ltb/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lxb/f;->l0(ILdc/g;IZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic e:Ljava/lang/String;

.field final synthetic f:Z

.field final synthetic g:Lxb/f;

.field final synthetic h:I

.field final synthetic i:Ldc/e;

.field final synthetic j:I

.field final synthetic k:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;ZLjava/lang/String;ZLxb/f;ILdc/e;IZ)V
    .locals 0

    iput-object p1, p0, Lxb/f$f;->e:Ljava/lang/String;

    iput-boolean p2, p0, Lxb/f$f;->f:Z

    iput-object p5, p0, Lxb/f$f;->g:Lxb/f;

    iput p6, p0, Lxb/f$f;->h:I

    iput-object p7, p0, Lxb/f$f;->i:Ldc/e;

    iput p8, p0, Lxb/f$f;->j:I

    iput-boolean p9, p0, Lxb/f$f;->k:Z

    invoke-direct {p0, p3, p4}, Ltb/a;-><init>(Ljava/lang/String;Z)V

    return-void
.end method


# virtual methods
.method public f()J
    .locals 5

    :try_start_0
    iget-object v0, p0, Lxb/f$f;->g:Lxb/f;

    invoke-static {v0}, Lxb/f;->y(Lxb/f;)Lxb/l;

    move-result-object v0

    iget v1, p0, Lxb/f$f;->h:I

    iget-object v2, p0, Lxb/f$f;->i:Ldc/e;

    iget v3, p0, Lxb/f$f;->j:I

    iget-boolean v4, p0, Lxb/f$f;->k:Z

    invoke-interface {v0, v1, v2, v3, v4}, Lxb/l;->c(ILdc/g;IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lxb/f$f;->g:Lxb/f;

    invoke-virtual {v1}, Lxb/f;->h0()Lxb/j;

    move-result-object v1

    iget v2, p0, Lxb/f$f;->h:I

    sget-object v3, Lxb/b;->j:Lxb/b;

    invoke-virtual {v1, v2, v3}, Lxb/j;->C(ILxb/b;)V

    :cond_0
    if-nez v0, :cond_1

    iget-boolean v0, p0, Lxb/f$f;->k:Z

    if-eqz v0, :cond_2

    :cond_1
    iget-object v0, p0, Lxb/f$f;->g:Lxb/f;

    monitor-enter v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v1, p0, Lxb/f$f;->g:Lxb/f;

    invoke-static {v1}, Lxb/f;->m(Lxb/f;)Ljava/util/Set;

    move-result-object v1

    iget v2, p0, Lxb/f$f;->h:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    :cond_2
    :goto_0
    const-wide/16 v0, -0x1

    return-wide v0
.end method
