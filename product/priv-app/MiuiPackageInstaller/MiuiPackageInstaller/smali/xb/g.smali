.class public final Lxb/g;
.super Ljava/lang/Object;

# interfaces
.implements Lvb/d;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lxb/g$a;
    }
.end annotation


# static fields
.field private static final g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final i:Lxb/g$a;


# instance fields
.field private volatile a:Lxb/i;

.field private final b:Lpb/y;

.field private volatile c:Z

.field private final d:Lub/f;

.field private final e:Lvb/g;

.field private final f:Lxb/f;


# direct methods
.method static constructor <clinit>()V
    .locals 14

    new-instance v0, Lxb/g$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lxb/g$a;-><init>(Lm8/g;)V

    sput-object v0, Lxb/g;->i:Lxb/g$a;

    const-string v2, "connection"

    const-string v3, "host"

    const-string v4, "keep-alive"

    const-string v5, "proxy-connection"

    const-string v6, "te"

    const-string v7, "transfer-encoding"

    const-string v8, "encoding"

    const-string v9, "upgrade"

    const-string v10, ":method"

    const-string v11, ":path"

    const-string v12, ":scheme"

    const-string v13, ":authority"

    filled-new-array/range {v2 .. v13}, [Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lrb/b;->t([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lxb/g;->g:Ljava/util/List;

    const-string v1, "connection"

    const-string v2, "host"

    const-string v3, "keep-alive"

    const-string v4, "proxy-connection"

    const-string v5, "te"

    const-string v6, "transfer-encoding"

    const-string v7, "encoding"

    const-string v8, "upgrade"

    filled-new-array/range {v1 .. v8}, [Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lrb/b;->t([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lxb/g;->h:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Lpb/x;Lub/f;Lvb/g;Lxb/f;)V
    .locals 1

    const-string v0, "client"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "connection"

    invoke-static {p2, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "chain"

    invoke-static {p3, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "http2Connection"

    invoke-static {p4, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lxb/g;->d:Lub/f;

    iput-object p3, p0, Lxb/g;->e:Lvb/g;

    iput-object p4, p0, Lxb/g;->f:Lxb/f;

    invoke-virtual {p1}, Lpb/x;->w()Ljava/util/List;

    move-result-object p1

    sget-object p2, Lpb/y;->f:Lpb/y;

    invoke-interface {p1, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    sget-object p2, Lpb/y;->e:Lpb/y;

    :goto_0
    iput-object p2, p0, Lxb/g;->b:Lpb/y;

    return-void
.end method

.method public static final synthetic i()Ljava/util/List;
    .locals 1

    sget-object v0, Lxb/g;->g:Ljava/util/List;

    return-object v0
.end method

.method public static final synthetic j()Ljava/util/List;
    .locals 1

    sget-object v0, Lxb/g;->h:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public a(Lpb/b0;)J
    .locals 2

    const-string v0, "response"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lvb/e;->a(Lpb/b0;)Z

    move-result v0

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-static {p1}, Lrb/b;->s(Lpb/b0;)J

    move-result-wide v0

    :goto_0
    return-wide v0
.end method

.method public b(Lpb/z;)V
    .locals 3

    const-string v0, "request"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lxb/g;->a:Lxb/i;

    if-eqz v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p1}, Lpb/z;->a()Lpb/a0;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    sget-object v1, Lxb/g;->i:Lxb/g$a;

    invoke-virtual {v1, p1}, Lxb/g$a;->a(Lpb/z;)Ljava/util/List;

    move-result-object p1

    iget-object v1, p0, Lxb/g;->f:Lxb/f;

    invoke-virtual {v1, p1, v0}, Lxb/f;->k0(Ljava/util/List;Z)Lxb/i;

    move-result-object p1

    iput-object p1, p0, Lxb/g;->a:Lxb/i;

    iget-boolean p1, p0, Lxb/g;->c:Z

    if-eqz p1, :cond_3

    iget-object p1, p0, Lxb/g;->a:Lxb/i;

    if-nez p1, :cond_2

    invoke-static {}, Lm8/i;->o()V

    :cond_2
    sget-object v0, Lxb/b;->j:Lxb/b;

    invoke-virtual {p1, v0}, Lxb/i;->f(Lxb/b;)V

    new-instance p1, Ljava/io/IOException;

    const-string v0, "Canceled"

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_3
    iget-object p1, p0, Lxb/g;->a:Lxb/i;

    if-nez p1, :cond_4

    invoke-static {}, Lm8/i;->o()V

    :cond_4
    invoke-virtual {p1}, Lxb/i;->v()Ldc/z;

    move-result-object p1

    iget-object v0, p0, Lxb/g;->e:Lvb/g;

    invoke-virtual {v0}, Lvb/g;->l()I

    move-result v0

    int-to-long v0, v0

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, v0, v1, v2}, Ldc/z;->g(JLjava/util/concurrent/TimeUnit;)Ldc/z;

    iget-object p1, p0, Lxb/g;->a:Lxb/i;

    if-nez p1, :cond_5

    invoke-static {}, Lm8/i;->o()V

    :cond_5
    invoke-virtual {p1}, Lxb/i;->E()Ldc/z;

    move-result-object p1

    iget-object v0, p0, Lxb/g;->e:Lvb/g;

    invoke-virtual {v0}, Lvb/g;->n()I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {p1, v0, v1, v2}, Ldc/z;->g(JLjava/util/concurrent/TimeUnit;)Ldc/z;

    return-void
.end method

.method public c()V
    .locals 1

    iget-object v0, p0, Lxb/g;->a:Lxb/i;

    if-nez v0, :cond_0

    invoke-static {}, Lm8/i;->o()V

    :cond_0
    invoke-virtual {v0}, Lxb/i;->n()Ldc/w;

    move-result-object v0

    invoke-interface {v0}, Ldc/w;->close()V

    return-void
.end method

.method public cancel()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lxb/g;->c:Z

    iget-object v0, p0, Lxb/g;->a:Lxb/i;

    if-eqz v0, :cond_0

    sget-object v1, Lxb/b;->j:Lxb/b;

    invoke-virtual {v0, v1}, Lxb/i;->f(Lxb/b;)V

    :cond_0
    return-void
.end method

.method public d()V
    .locals 1

    iget-object v0, p0, Lxb/g;->f:Lxb/f;

    invoke-virtual {v0}, Lxb/f;->flush()V

    return-void
.end method

.method public e(Lpb/b0;)Ldc/y;
    .locals 1

    const-string v0, "response"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p0, Lxb/g;->a:Lxb/i;

    if-nez p1, :cond_0

    invoke-static {}, Lm8/i;->o()V

    :cond_0
    invoke-virtual {p1}, Lxb/i;->p()Lxb/i$c;

    move-result-object p1

    return-object p1
.end method

.method public f(Lpb/z;J)Ldc/w;
    .locals 0

    const-string p2, "request"

    invoke-static {p1, p2}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p0, Lxb/g;->a:Lxb/i;

    if-nez p1, :cond_0

    invoke-static {}, Lm8/i;->o()V

    :cond_0
    invoke-virtual {p1}, Lxb/i;->n()Ldc/w;

    move-result-object p1

    return-object p1
.end method

.method public g(Z)Lpb/b0$a;
    .locals 3

    iget-object v0, p0, Lxb/g;->a:Lxb/i;

    if-nez v0, :cond_0

    invoke-static {}, Lm8/i;->o()V

    :cond_0
    invoke-virtual {v0}, Lxb/i;->C()Lpb/s;

    move-result-object v0

    sget-object v1, Lxb/g;->i:Lxb/g$a;

    iget-object v2, p0, Lxb/g;->b:Lpb/y;

    invoke-virtual {v1, v0, v2}, Lxb/g$a;->b(Lpb/s;Lpb/y;)Lpb/b0$a;

    move-result-object v0

    if-eqz p1, :cond_1

    invoke-virtual {v0}, Lpb/b0$a;->h()I

    move-result p1

    const/16 v1, 0x64

    if-ne p1, v1, :cond_1

    const/4 v0, 0x0

    :cond_1
    return-object v0
.end method

.method public h()Lub/f;
    .locals 1

    iget-object v0, p0, Lxb/g;->d:Lub/f;

    return-object v0
.end method
