.class public final enum Lxb/b;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lxb/b$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lxb/b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum b:Lxb/b;

.field public static final enum c:Lxb/b;

.field public static final enum d:Lxb/b;

.field public static final enum e:Lxb/b;

.field public static final enum f:Lxb/b;

.field public static final enum g:Lxb/b;

.field public static final enum h:Lxb/b;

.field public static final enum i:Lxb/b;

.field public static final enum j:Lxb/b;

.field public static final enum k:Lxb/b;

.field public static final enum l:Lxb/b;

.field public static final enum m:Lxb/b;

.field public static final enum n:Lxb/b;

.field public static final enum o:Lxb/b;

.field private static final synthetic p:[Lxb/b;

.field public static final q:Lxb/b$a;


# instance fields
.field private final a:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/16 v0, 0xe

    new-array v0, v0, [Lxb/b;

    new-instance v1, Lxb/b;

    const-string v2, "NO_ERROR"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3, v3}, Lxb/b;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lxb/b;->b:Lxb/b;

    aput-object v1, v0, v3

    new-instance v1, Lxb/b;

    const-string v2, "PROTOCOL_ERROR"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3, v3}, Lxb/b;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lxb/b;->c:Lxb/b;

    aput-object v1, v0, v3

    new-instance v1, Lxb/b;

    const-string v2, "INTERNAL_ERROR"

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3, v3}, Lxb/b;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lxb/b;->d:Lxb/b;

    aput-object v1, v0, v3

    new-instance v1, Lxb/b;

    const-string v2, "FLOW_CONTROL_ERROR"

    const/4 v3, 0x3

    invoke-direct {v1, v2, v3, v3}, Lxb/b;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lxb/b;->e:Lxb/b;

    aput-object v1, v0, v3

    new-instance v1, Lxb/b;

    const-string v2, "SETTINGS_TIMEOUT"

    const/4 v3, 0x4

    invoke-direct {v1, v2, v3, v3}, Lxb/b;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lxb/b;->f:Lxb/b;

    aput-object v1, v0, v3

    new-instance v1, Lxb/b;

    const-string v2, "STREAM_CLOSED"

    const/4 v3, 0x5

    invoke-direct {v1, v2, v3, v3}, Lxb/b;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lxb/b;->g:Lxb/b;

    aput-object v1, v0, v3

    new-instance v1, Lxb/b;

    const-string v2, "FRAME_SIZE_ERROR"

    const/4 v3, 0x6

    invoke-direct {v1, v2, v3, v3}, Lxb/b;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lxb/b;->h:Lxb/b;

    aput-object v1, v0, v3

    new-instance v1, Lxb/b;

    const-string v2, "REFUSED_STREAM"

    const/4 v3, 0x7

    invoke-direct {v1, v2, v3, v3}, Lxb/b;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lxb/b;->i:Lxb/b;

    aput-object v1, v0, v3

    new-instance v1, Lxb/b;

    const-string v2, "CANCEL"

    const/16 v3, 0x8

    invoke-direct {v1, v2, v3, v3}, Lxb/b;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lxb/b;->j:Lxb/b;

    aput-object v1, v0, v3

    new-instance v1, Lxb/b;

    const-string v2, "COMPRESSION_ERROR"

    const/16 v3, 0x9

    invoke-direct {v1, v2, v3, v3}, Lxb/b;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lxb/b;->k:Lxb/b;

    aput-object v1, v0, v3

    new-instance v1, Lxb/b;

    const-string v2, "CONNECT_ERROR"

    const/16 v3, 0xa

    invoke-direct {v1, v2, v3, v3}, Lxb/b;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lxb/b;->l:Lxb/b;

    aput-object v1, v0, v3

    new-instance v1, Lxb/b;

    const-string v2, "ENHANCE_YOUR_CALM"

    const/16 v3, 0xb

    invoke-direct {v1, v2, v3, v3}, Lxb/b;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lxb/b;->m:Lxb/b;

    aput-object v1, v0, v3

    new-instance v1, Lxb/b;

    const-string v2, "INADEQUATE_SECURITY"

    const/16 v3, 0xc

    invoke-direct {v1, v2, v3, v3}, Lxb/b;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lxb/b;->n:Lxb/b;

    aput-object v1, v0, v3

    new-instance v1, Lxb/b;

    const-string v2, "HTTP_1_1_REQUIRED"

    const/16 v3, 0xd

    invoke-direct {v1, v2, v3, v3}, Lxb/b;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lxb/b;->o:Lxb/b;

    aput-object v1, v0, v3

    sput-object v0, Lxb/b;->p:[Lxb/b;

    new-instance v0, Lxb/b$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lxb/b$a;-><init>(Lm8/g;)V

    sput-object v0, Lxb/b;->q:Lxb/b$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lxb/b;->a:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lxb/b;
    .locals 1

    const-class v0, Lxb/b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lxb/b;

    return-object p0
.end method

.method public static values()[Lxb/b;
    .locals 1

    sget-object v0, Lxb/b;->p:[Lxb/b;

    invoke-virtual {v0}, [Lxb/b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lxb/b;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Lxb/b;->a:I

    return v0
.end method
