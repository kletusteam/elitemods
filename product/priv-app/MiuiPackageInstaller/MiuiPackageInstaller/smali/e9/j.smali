.class public Le9/j;
.super Ljava/lang/Object;


# direct methods
.method private static a(Lmiuix/animation/b;Lh9/b;D)D
    .locals 4

    invoke-static {p2, p3}, Ljava/lang/Math;->signum(D)D

    move-result-wide v0

    invoke-static {p2, p3}, Ljava/lang/Math;->abs(D)D

    move-result-wide p2

    const-wide v2, 0x412e848000000000L    # 1000000.0

    cmpl-double v2, p2, v2

    if-nez v2, :cond_0

    invoke-static {p0, p1}, Lj9/a;->f(Lmiuix/animation/b;Lh9/b;)F

    move-result p0

    float-to-double p0, p0

    mul-double/2addr v0, p0

    return-wide v0

    :cond_0
    instance-of v2, p1, Lh9/c;

    if-eqz v2, :cond_1

    check-cast p1, Lh9/c;

    invoke-virtual {p0, p1}, Lmiuix/animation/b;->e(Lh9/c;)I

    move-result p0

    int-to-double p0, p0

    goto :goto_0

    :cond_1
    invoke-virtual {p0, p1}, Lmiuix/animation/b;->i(Lh9/b;)F

    move-result p0

    float-to-double p0, p0

    :goto_0
    const-wide v2, 0x412e854800000000L    # 1000100.0

    cmpl-double p2, p2, v2

    if-nez p2, :cond_2

    mul-double/2addr p0, v0

    :cond_2
    return-wide p0
.end method

.method public static b(Lmiuix/animation/b;Lh9/b;D)D
    .locals 1

    instance-of v0, p1, Lh9/d;

    if-eqz v0, :cond_0

    check-cast p1, Lh9/d;

    double-to-float p0, p2

    invoke-interface {p1, p0}, Lh9/d;->a(F)F

    move-result p0

    float-to-double p0, p0

    return-wide p0

    :cond_0
    invoke-static {p0, p1, p2, p3}, Le9/j;->a(Lmiuix/animation/b;Lh9/b;D)D

    move-result-wide p0

    return-wide p0
.end method

.method public static c(Lmiuix/animation/b;Lh9/b;D)D
    .locals 2

    const-wide v0, 0x41dfffffffc00000L    # 2.147483647E9

    cmpl-double v0, p2, v0

    if-nez v0, :cond_0

    check-cast p1, Lh9/c;

    invoke-virtual {p0, p1}, Lmiuix/animation/b;->e(Lh9/c;)I

    move-result p0

    int-to-double p0, p0

    return-wide p0

    :cond_0
    const-wide v0, 0x47efffffe0000000L    # 3.4028234663852886E38

    cmpl-double v0, p2, v0

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Lmiuix/animation/b;->i(Lh9/b;)F

    move-result p0

    float-to-double p0, p0

    return-wide p0

    :cond_1
    invoke-static {p0, p1, p2, p3}, Le9/j;->b(Lmiuix/animation/b;Lh9/b;D)D

    move-result-wide p0

    return-wide p0
.end method

.method public static d(Lf9/c;)Z
    .locals 3

    iget-object v0, p0, Lf9/c;->f:Le9/c;

    iget-wide v0, v0, Le9/c;->j:D

    invoke-static {v0, v1}, Le9/j;->e(D)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lf9/c;->f:Le9/c;

    iget-wide v1, v0, Le9/c;->j:D

    iput-wide v1, v0, Le9/c;->i:D

    iget-object p0, p0, Lf9/c;->f:Le9/c;

    const-wide v0, 0x7fefffffffffffffL    # Double.MAX_VALUE

    iput-wide v0, p0, Le9/c;->j:D

    const/4 p0, 0x1

    return p0

    :cond_0
    const/4 p0, 0x0

    return p0
.end method

.method public static e(D)Z
    .locals 2

    const-wide v0, 0x7fefffffffffffffL    # Double.MAX_VALUE

    cmpl-double v0, p0, v0

    if-eqz v0, :cond_1

    const-wide v0, 0x47efffffe0000000L    # 3.4028234663852886E38

    cmpl-double v0, p0, v0

    if-eqz v0, :cond_1

    const-wide v0, 0x41dfffffffc00000L    # 2.147483647E9

    cmpl-double p0, p0, v0

    if-nez p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method
