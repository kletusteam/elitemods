.class Le9/o$c;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Le9/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "c"
.end annotation


# instance fields
.field a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lmiuix/animation/b;",
            ">;"
        }
    .end annotation
.end field

.field b:Lh9/b;

.field c:Le9/o$b;


# direct methods
.method constructor <init>(Le9/o$b;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Le9/o$c;->c:Le9/o$b;

    return-void
.end method


# virtual methods
.method a(Lmiuix/animation/b;Lh9/b;)V
    .locals 2

    goto/32 :goto_9

    nop

    :goto_0
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_7

    nop

    :goto_1
    return-void

    :goto_2
    invoke-virtual {v0, p0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto/32 :goto_6

    nop

    :goto_3
    iput-object v0, p0, Le9/o$c;->a:Ljava/lang/ref/WeakReference;

    :goto_4
    goto/32 :goto_d

    nop

    :goto_5
    invoke-virtual {p1, p0, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/32 :goto_1

    nop

    :goto_6
    iget-object v0, p0, Le9/o$c;->a:Ljava/lang/ref/WeakReference;

    goto/32 :goto_b

    nop

    :goto_7
    if-ne v0, p1, :cond_0

    goto/32 :goto_4

    :cond_0
    :goto_8
    goto/32 :goto_c

    nop

    :goto_9
    iget-object v0, p1, Lmiuix/animation/b;->a:Le9/n;

    goto/32 :goto_2

    nop

    :goto_a
    const-wide/16 v0, 0x258

    goto/32 :goto_5

    nop

    :goto_b
    if-nez v0, :cond_1

    goto/32 :goto_8

    :cond_1
    goto/32 :goto_0

    nop

    :goto_c
    new-instance v0, Ljava/lang/ref/WeakReference;

    goto/32 :goto_e

    nop

    :goto_d
    iput-object p2, p0, Le9/o$c;->b:Lh9/b;

    goto/32 :goto_f

    nop

    :goto_e
    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    goto/32 :goto_3

    nop

    :goto_f
    iget-object p1, p1, Lmiuix/animation/b;->a:Le9/n;

    goto/32 :goto_a

    nop
.end method

.method public run()V
    .locals 4

    iget-object v0, p0, Le9/o$c;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiuix/animation/b;

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    new-array v1, v1, [Lh9/b;

    const/4 v2, 0x0

    iget-object v3, p0, Le9/o$c;->b:Lh9/b;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lmiuix/animation/b;->k([Lh9/b;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Le9/o$c;->b:Lh9/b;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lmiuix/animation/b;->t(Lh9/b;D)V

    :cond_0
    iget-object v0, p0, Le9/o$c;->c:Le9/o$b;

    iget-object v0, v0, Le9/o$b;->a:Lj9/i;

    invoke-virtual {v0}, Lj9/i;->c()V

    :cond_1
    return-void
.end method
