.class public final Le9/n;
.super Landroid/os/Handler;


# instance fields
.field private final a:Lmiuix/animation/b;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Le9/q;",
            ">;"
        }
    .end annotation
.end field

.field public final c:J


# direct methods
.method public constructor <init>(Lmiuix/animation/b;)V
    .locals 2

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Le9/n;->b:Ljava/util/List;

    iput-object p1, p0, Le9/n;->a:Lmiuix/animation/b;

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object p1

    invoke-virtual {p1}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Thread;->getId()J

    move-result-wide v0

    iput-wide v0, p0, Le9/n;->c:J

    return-void
.end method

.method private static a(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lf9/c;",
            ">;)V"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lf9/c;

    iget-object v3, v2, Lf9/c;->f:Le9/c;

    iget-wide v3, v3, Le9/c;->i:D

    invoke-static {v3, v4}, Le9/j;->e(D)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_2

    invoke-interface {p0, v0}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    :cond_2
    return-void
.end method

.method private static c(Le9/q;Z)V
    .locals 4

    invoke-virtual {p0}, Le9/q;->e()I

    move-result v0

    const/16 v1, 0xfa0

    if-le v0, v1, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Le9/q;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf9/c;

    iget-object v2, v1, Lf9/c;->a:Lh9/b;

    sget-object v3, Lh9/i;->a:Lh9/i$c;

    if-eq v2, v3, :cond_1

    goto :goto_0

    :cond_1
    iget-object v2, p0, Le9/q;->c:Lmiuix/animation/b;

    if-eqz p1, :cond_2

    invoke-static {v2, v1}, Li9/a;->l(Lmiuix/animation/b;Lf9/c;)V

    goto :goto_0

    :cond_2
    invoke-static {v2, v1}, Li9/a;->i(Lmiuix/animation/b;Lf9/c;)V

    goto :goto_0

    :cond_3
    return-void
.end method

.method private d(Le9/q;I)V
    .locals 4

    invoke-static {}, Lj9/f;->d()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "<<< onEnd, info.id = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p1, Le9/q;->b:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ", info.key = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p1, Le9/q;->e:Ljava/lang/Object;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p1, Le9/q;->e:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ", info.startTime = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, p1, Le9/q;->i:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v2, ", target = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Le9/n;->a:Lmiuix/animation/b;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v2, v1, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lj9/f;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    invoke-direct {p0, v1, p1}, Le9/n;->j(ZLe9/q;)V

    invoke-static {p1, v1}, Le9/n;->c(Le9/q;Z)V

    iget-object v0, p1, Le9/q;->c:Lmiuix/animation/b;

    iget-object v0, v0, Lmiuix/animation/b;->b:Le9/d;

    invoke-virtual {v0, p1}, Le9/d;->k(Le9/q;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x4

    if-ne p2, v0, :cond_1

    iget-object p2, p1, Le9/q;->c:Lmiuix/animation/b;

    invoke-virtual {p2}, Lmiuix/animation/b;->g()Lf9/a;

    move-result-object p2

    iget-object v0, p1, Le9/q;->e:Ljava/lang/Object;

    iget-object v1, p1, Le9/q;->d:Ljava/lang/Object;

    invoke-virtual {p2, v0, v1}, Lf9/a;->e(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    iget-object p2, p1, Le9/q;->c:Lmiuix/animation/b;

    invoke-virtual {p2}, Lmiuix/animation/b;->g()Lf9/a;

    move-result-object p2

    iget-object v0, p1, Le9/q;->e:Ljava/lang/Object;

    iget-object v1, p1, Le9/q;->d:Ljava/lang/Object;

    invoke-virtual {p2, v0, v1}, Lf9/a;->f(Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_0
    iget-object p2, p1, Le9/q;->c:Lmiuix/animation/b;

    invoke-virtual {p2}, Lmiuix/animation/b;->g()Lf9/a;

    move-result-object p2

    iget-object p1, p1, Le9/q;->e:Ljava/lang/Object;

    invoke-virtual {p2, p1}, Lf9/a;->m(Ljava/lang/Object;)V

    :cond_2
    return-void
.end method

.method private e(Le9/q;)V
    .locals 4

    invoke-static {}, Lj9/f;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "<<< onReplaced, info.id = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p1, Le9/q;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", info.key = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Le9/q;->e:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Le9/q;->e:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", info.startTime = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p1, Le9/q;->i:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", target = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Le9/n;->a:Lmiuix/animation/b;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lj9/f;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    iget-object v0, p1, Le9/q;->c:Lmiuix/animation/b;

    iget-object v0, v0, Lmiuix/animation/b;->b:Le9/d;

    invoke-virtual {v0, p1}, Le9/d;->k(Le9/q;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Le9/q;->e()I

    move-result v0

    const/16 v1, 0xfa0

    if-gt v0, v1, :cond_1

    iget-object v0, p0, Le9/n;->a:Lmiuix/animation/b;

    invoke-virtual {v0}, Lmiuix/animation/b;->g()Lf9/a;

    move-result-object v0

    iget-object v1, p1, Le9/q;->e:Ljava/lang/Object;

    iget-object v2, p1, Le9/q;->d:Ljava/lang/Object;

    iget-object v3, p1, Le9/q;->j:Ljava/util/List;

    invoke-virtual {v0, v1, v2, v3}, Lf9/a;->j(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Collection;)V

    :cond_1
    iget-object v0, p0, Le9/n;->a:Lmiuix/animation/b;

    invoke-virtual {v0}, Lmiuix/animation/b;->g()Lf9/a;

    move-result-object v0

    iget-object v1, p1, Le9/q;->e:Ljava/lang/Object;

    iget-object v2, p1, Le9/q;->d:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lf9/a;->e(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v0, p0, Le9/n;->a:Lmiuix/animation/b;

    invoke-virtual {v0}, Lmiuix/animation/b;->g()Lf9/a;

    move-result-object v0

    iget-object p1, p1, Le9/q;->e:Ljava/lang/Object;

    invoke-virtual {v0, p1}, Lf9/a;->m(Ljava/lang/Object;)V

    :cond_2
    return-void
.end method

.method private f(Le9/q;)V
    .locals 4

    invoke-static {}, Lj9/f;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ">>> onStart, info.id = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p1, Le9/q;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", info.key = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Le9/q;->e:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", info.starTime = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p1, Le9/q;->i:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", mRunningInfo.contains = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Le9/q;->c:Lmiuix/animation/b;

    iget-object v1, v1, Lmiuix/animation/b;->b:Le9/d;

    iget-object v1, v1, Le9/d;->e:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/ConcurrentHashMap;->contains(Ljava/lang/Object;)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", target = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Le9/n;->a:Lmiuix/animation/b;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lj9/f;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    iget-object v0, p1, Le9/q;->c:Lmiuix/animation/b;

    invoke-virtual {v0}, Lmiuix/animation/b;->g()Lf9/a;

    move-result-object v0

    iget-object v1, p1, Le9/q;->e:Ljava/lang/Object;

    iget-object v2, p1, Le9/q;->f:Lc9/a;

    invoke-virtual {v0, v1, v2}, Lf9/a;->a(Ljava/lang/Object;Lc9/a;)Z

    iget-object v0, p1, Le9/q;->c:Lmiuix/animation/b;

    invoke-virtual {v0}, Lmiuix/animation/b;->g()Lf9/a;

    move-result-object v0

    iget-object v1, p1, Le9/q;->e:Ljava/lang/Object;

    iget-object v2, p1, Le9/q;->d:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lf9/a;->d(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v0, p1, Le9/q;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/16 v2, 0xfa0

    if-gt v1, v2, :cond_1

    iget-object v1, p1, Le9/q;->c:Lmiuix/animation/b;

    invoke-virtual {v1}, Lmiuix/animation/b;->g()Lf9/a;

    move-result-object v1

    iget-object v2, p1, Le9/q;->e:Ljava/lang/Object;

    iget-object v3, p1, Le9/q;->d:Ljava/lang/Object;

    invoke-virtual {v1, v2, v3, v0}, Lf9/a;->i(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Collection;)V

    :cond_1
    const/4 v0, 0x1

    invoke-static {p1, v0}, Le9/n;->c(Le9/q;Z)V

    return-void
.end method

.method private static h(Lmiuix/animation/b;Ljava/lang/Object;Ljava/lang/Object;Ljava/util/List;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmiuix/animation/b;",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            "Ljava/util/List<",
            "Lf9/c;",
            ">;Z)V"
        }
    .end annotation

    if-eqz p4, :cond_0

    instance-of p4, p0, Lmiuix/animation/ViewTarget;

    if-eqz p4, :cond_1

    :cond_0
    invoke-static {p0, p3}, Le9/n;->k(Lmiuix/animation/b;Ljava/util/List;)V

    :cond_1
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result p4

    const v0, 0x9c40

    if-le p4, v0, :cond_2

    invoke-virtual {p0}, Lmiuix/animation/b;->g()Lf9/a;

    move-result-object p0

    invoke-virtual {p0, p1, p2}, Lf9/a;->h(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lmiuix/animation/b;->g()Lf9/a;

    move-result-object p4

    invoke-virtual {p4, p1, p2, p3}, Lf9/a;->j(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Collection;)V

    invoke-virtual {p0}, Lmiuix/animation/b;->g()Lf9/a;

    move-result-object p0

    invoke-virtual {p0, p1, p2, p3}, Lf9/a;->k(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Collection;)V

    :goto_0
    return-void
.end method

.method private j(ZLe9/q;)V
    .locals 3

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p2, Le9/q;->j:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Le9/n;->a(Ljava/util/List;)V

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p2, Le9/q;->c:Lmiuix/animation/b;

    iget-object v2, p2, Le9/q;->e:Ljava/lang/Object;

    iget-object p2, p2, Le9/q;->d:Ljava/lang/Object;

    invoke-static {v1, v2, p2, v0, p1}, Le9/n;->h(Lmiuix/animation/b;Ljava/lang/Object;Ljava/lang/Object;Ljava/util/List;Z)V

    :cond_0
    return-void
.end method

.method private static k(Lmiuix/animation/b;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmiuix/animation/b;",
            "Ljava/util/List<",
            "Lf9/c;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf9/c;

    iget-object v1, v0, Lf9/c;->f:Le9/c;

    iget-wide v1, v1, Le9/c;->i:D

    invoke-static {v1, v2}, Le9/j;->e(D)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {v0, p0}, Lf9/c;->f(Lmiuix/animation/b;)V

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method public b()Z
    .locals 2

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {p0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public g(Z)V
    .locals 2

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {p0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    if-eq v0, v1, :cond_0

    new-instance v0, Le9/n$a;

    invoke-direct {v0, p0, p1}, Le9/n$a;-><init>(Le9/n;Z)V

    invoke-virtual {p0, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p1}, Le9/n;->i(Z)V

    :goto_0
    return-void
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 7

    iget v0, p1, Landroid/os/Message;->what:I

    if-eqz v0, :cond_8

    const/4 v1, 0x2

    const/4 v2, 0x0

    const-string v3, ", target "

    const-string v4, ", obj info = "

    const-string v5, ", info.id = "

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    const/4 v1, 0x5

    if-eq v0, v1, :cond_5

    goto/16 :goto_0

    :cond_0
    sget-object v0, Le9/q;->m:Ljava/util/Map;

    iget p1, p1, Landroid/os/Message;->arg1:I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Le9/q;

    if-eqz p1, :cond_9

    iget-object v0, p0, Le9/n;->a:Lmiuix/animation/b;

    invoke-virtual {v0}, Lmiuix/animation/b;->g()Lf9/a;

    move-result-object v0

    iget-object v1, p1, Le9/q;->e:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lf9/a;->m(Ljava/lang/Object;)V

    iget-object v0, p0, Le9/n;->a:Lmiuix/animation/b;

    invoke-virtual {v0}, Lmiuix/animation/b;->g()Lf9/a;

    move-result-object v0

    iget-object v1, p1, Le9/q;->e:Ljava/lang/Object;

    iget-object p1, p1, Le9/q;->f:Lc9/a;

    invoke-virtual {v0, v1, p1}, Lf9/a;->a(Ljava/lang/Object;Lc9/a;)Z

    goto/16 :goto_0

    :cond_1
    iget-object p1, p0, Le9/n;->a:Lmiuix/animation/b;

    iget-object p1, p1, Lmiuix/animation/b;->b:Le9/d;

    iget-object p1, p1, Le9/d;->g:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->clear()V

    goto/16 :goto_0

    :cond_2
    sget-object v0, Le9/q;->m:Ljava/util/Map;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Le9/q;

    invoke-static {}, Lj9/f;->d()Z

    move-result v1

    if-eqz v1, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "<<< handler ANIM_MSG_END, , info = "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v6, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, p0, Le9/n;->a:Lmiuix/animation/b;

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v6, v2, [Ljava/lang/Object;

    invoke-static {v1, v6}, Lj9/f;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_3
    if-nez v0, :cond_4

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Le9/q;

    :cond_4
    if-eqz v0, :cond_5

    iget v1, p1, Landroid/os/Message;->arg2:I

    invoke-direct {p0, v0, v1}, Le9/n;->d(Le9/q;I)V

    :cond_5
    sget-object v0, Le9/q;->m:Ljava/util/Map;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Le9/q;

    invoke-static {}, Lj9/f;->d()Z

    move-result v1

    if-eqz v1, :cond_6

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "<<< handler ANIM_MSG_REPLACED, , info = "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v5, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Le9/n;->a:Lmiuix/animation/b;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lj9/f;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_6
    if-nez v0, :cond_7

    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object v0, p1

    check-cast v0, Le9/q;

    :cond_7
    if-eqz v0, :cond_9

    invoke-direct {p0, v0}, Le9/n;->e(Le9/q;)V

    goto :goto_0

    :cond_8
    sget-object v0, Le9/q;->m:Ljava/util/Map;

    iget p1, p1, Landroid/os/Message;->arg1:I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Le9/q;

    if-eqz p1, :cond_9

    invoke-direct {p0, p1}, Le9/n;->f(Le9/q;)V

    :cond_9
    :goto_0
    return-void
.end method

.method public i(Z)V
    .locals 2

    iget-object v0, p0, Le9/n;->a:Lmiuix/animation/b;

    iget-object v0, v0, Lmiuix/animation/b;->b:Le9/d;

    iget-object v1, p0, Le9/n;->b:Ljava/util/List;

    invoke-virtual {v0, v1}, Le9/d;->f(Ljava/util/List;)V

    iget-object v0, p0, Le9/n;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Le9/q;

    invoke-direct {p0, p1, v1}, Le9/n;->j(ZLe9/q;)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Le9/n;->b:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->clear()V

    return-void
.end method
