.class public Le9/d;
.super Ljava/lang/Object;

# interfaces
.implements Le9/q$a;


# instance fields
.field a:Lmiuix/animation/b;

.field final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field final d:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Lh9/b;",
            "Lf9/c;",
            ">;"
        }
    .end annotation
.end field

.field final e:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/Object;",
            "Le9/q;",
            ">;"
        }
    .end annotation
.end field

.field final f:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/Object;",
            "Le9/q;",
            ">;"
        }
    .end annotation
.end field

.field final g:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue<",
            "Le9/q;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lf9/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Le9/d;->b:Ljava/util/Set;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Le9/d;->c:Ljava/util/Set;

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Le9/d;->d:Ljava/util/concurrent/ConcurrentHashMap;

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Le9/d;->e:Ljava/util/concurrent/ConcurrentHashMap;

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Le9/d;->f:Ljava/util/concurrent/ConcurrentHashMap;

    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Le9/d;->g:Ljava/util/concurrent/ConcurrentLinkedQueue;

    return-void
.end method

.method private c()V
    .locals 3

    iget-object v0, p0, Le9/d;->e:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Le9/q;

    sget-object v2, Le9/q;->m:Ljava/util/Map;

    iget v1, v1, Le9/q;->b:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Le9/d;->e:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    return-void
.end method

.method private varargs d(Le9/q;[Lh9/b;)Z
    .locals 4

    array-length v0, p2

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v0, :cond_1

    aget-object v3, p2, v2

    invoke-virtual {p1, v3}, Le9/q;->c(Lh9/b;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return v1
.end method

.method private j(Le9/q;)Z
    .locals 4

    iget-object v0, p1, Le9/q;->h:Ld9/a;

    iget-wide v0, v0, Ld9/a;->d:J

    const-wide/16 v2, 0x1

    invoke-static {v0, v1, v2, v3}, Lj9/a;->h(JJ)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Le9/d;->g:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method private m(Le9/q;)V
    .locals 6

    iget-object v0, p0, Le9/d;->e:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Le9/q;

    if-ne v1, p1, :cond_1

    goto :goto_0

    :cond_1
    iget-object v2, v1, Le9/q;->j:Ljava/util/List;

    iget-object v3, p0, Le9/d;->h:Ljava/util/List;

    if-nez v3, :cond_2

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Le9/d;->h:Ljava/util/List;

    :cond_2
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lf9/c;

    iget-object v4, p1, Le9/q;->h:Ld9/a;

    iget-object v5, v3, Lf9/c;->a:Lh9/b;

    invoke-virtual {v4, v5}, Ld9/a;->e(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    iget-object v4, p0, Le9/d;->h:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    iget-object v2, p0, Le9/d;->h:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_5

    iget v2, v1, Le9/q;->b:I

    iget v3, p1, Le9/q;->b:I

    if-eq v2, v3, :cond_0

    const/4 v2, 0x5

    const/4 v3, 0x4

    invoke-virtual {p0, v1, v2, v3}, Le9/d;->i(Le9/q;II)V

    goto :goto_0

    :cond_5
    iget-object v2, p0, Le9/d;->h:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    iget-object v3, v1, Le9/q;->j:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-eq v2, v3, :cond_6

    iget-object v2, p0, Le9/d;->h:Ljava/util/List;

    iput-object v2, v1, Le9/q;->j:Ljava/util/List;

    const/4 v2, 0x0

    iput-object v2, p0, Le9/d;->h:Ljava/util/List;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Le9/q;->j(Z)V

    goto :goto_0

    :cond_6
    iget-object v1, p0, Le9/d;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    goto :goto_0

    :cond_7
    return-void
.end method

.method private o(Ld9/a;Lc9/b;)V
    .locals 7

    invoke-virtual {p1}, Ld9/a;->n()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p1, v1}, Ld9/a;->m(Ljava/lang/Object;)Lh9/b;

    move-result-object v1

    iget-object v2, p0, Le9/d;->a:Lmiuix/animation/b;

    invoke-virtual {p1, v2, v1}, Ld9/a;->f(Lmiuix/animation/b;Lh9/b;)D

    move-result-wide v2

    iget-object v4, p0, Le9/d;->a:Lmiuix/animation/b;

    iget-object v4, v4, Lmiuix/animation/b;->b:Le9/d;

    iget-object v4, v4, Le9/d;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v4, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lf9/c;

    if-eqz v4, :cond_0

    iget-object v4, v4, Lf9/c;->f:Le9/c;

    iput-wide v2, v4, Le9/c;->j:D

    :cond_0
    instance-of v4, v1, Lh9/c;

    if-eqz v4, :cond_1

    iget-object v4, p0, Le9/d;->a:Lmiuix/animation/b;

    move-object v5, v1

    check-cast v5, Lh9/c;

    double-to-int v6, v2

    invoke-virtual {v4, v5, v6}, Lmiuix/animation/b;->p(Lh9/c;I)V

    goto :goto_1

    :cond_1
    iget-object v4, p0, Le9/d;->a:Lmiuix/animation/b;

    double-to-float v5, v2

    invoke-virtual {v4, v1, v5}, Lmiuix/animation/b;->s(Lh9/b;F)V

    :goto_1
    iget-object v4, p0, Le9/d;->a:Lmiuix/animation/b;

    invoke-virtual {v4, v1, v2, v3}, Lmiuix/animation/b;->u(Lh9/b;D)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Le9/d;->a:Lmiuix/animation/b;

    invoke-virtual {v0, p1, p2}, Lmiuix/animation/b;->r(Ld9/a;Lc9/b;)V

    return-void
.end method


# virtual methods
.method public a(Lh9/b;)Lf9/c;
    .locals 2

    iget-object v0, p0, Le9/d;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf9/c;

    if-nez v0, :cond_0

    new-instance v0, Lf9/c;

    invoke-direct {v0, p1}, Lf9/c;-><init>(Lh9/b;)V

    iget-object v1, p0, Le9/d;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lf9/c;

    if-eqz p1, :cond_0

    move-object v0, p1

    :cond_0
    return-object v0
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Le9/d;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    iget-object v0, p0, Le9/d;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    iget-object v0, p0, Le9/d;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    invoke-direct {p0}, Le9/d;->c()V

    iget-object v0, p0, Le9/d;->f:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    iget-object v0, p0, Le9/d;->g:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->clear()V

    return-void
.end method

.method public e()I
    .locals 3

    iget-object v0, p0, Le9/d;->e:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Le9/q;

    invoke-virtual {v2}, Le9/q;->e()I

    move-result v2

    add-int/2addr v1, v2

    goto :goto_0

    :cond_0
    return v1
.end method

.method f(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Le9/q;",
            ">;)V"
        }
    .end annotation

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Le9/d;->e:Ljava/util/concurrent/ConcurrentHashMap;

    goto/32 :goto_7

    nop

    :goto_1
    if-eqz v2, :cond_0

    goto/32 :goto_d

    :cond_0
    goto/32 :goto_a

    nop

    :goto_2
    check-cast v1, Le9/q;

    goto/32 :goto_3

    nop

    :goto_3
    iget-object v2, v1, Le9/q;->j:Ljava/util/List;

    goto/32 :goto_b

    nop

    :goto_4
    iget-object v2, v1, Le9/q;->j:Ljava/util/List;

    goto/32 :goto_9

    nop

    :goto_5
    return-void

    :goto_6
    if-nez v1, :cond_1

    goto/32 :goto_f

    :cond_1
    goto/32 :goto_8

    nop

    :goto_7
    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    goto/32 :goto_c

    nop

    :goto_8
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_2

    nop

    :goto_9
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    goto/32 :goto_1

    nop

    :goto_a
    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/32 :goto_e

    nop

    :goto_b
    if-nez v2, :cond_2

    goto/32 :goto_d

    :cond_2
    goto/32 :goto_4

    nop

    :goto_c
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_d
    goto/32 :goto_10

    nop

    :goto_e
    goto :goto_d

    :goto_f
    goto/32 :goto_5

    nop

    :goto_10
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_6

    nop
.end method

.method public varargs g([Lh9/b;)Z
    .locals 3

    invoke-static {p1}, Lj9/a;->j([Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    iget-object v0, p0, Le9/d;->e:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Le9/d;->g:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    return v1

    :cond_1
    iget-object v0, p0, Le9/d;->e:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Le9/q;

    invoke-direct {p0, v2, p1}, Le9/d;->d(Le9/q;[Lh9/b;)Z

    move-result v2

    if-eqz v2, :cond_2

    return v1

    :cond_3
    const/4 p1, 0x0

    return p1
.end method

.method public h()Z
    .locals 2

    sget-object v0, Le9/f;->h:Le9/m;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    return v1

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method i(Le9/q;II)V
    .locals 4

    goto/32 :goto_9

    nop

    :goto_0
    return-void

    :goto_1
    invoke-static {v0, v1}, Lj9/f;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_2
    goto/32 :goto_4

    nop

    :goto_3
    const-string v1, ", info = "

    goto/32 :goto_29

    nop

    :goto_4
    iget-object v0, p0, Le9/d;->c:Ljava/util/Set;

    goto/32 :goto_22

    nop

    :goto_5
    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/32 :goto_30

    nop

    :goto_6
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_23

    nop

    :goto_7
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/32 :goto_a

    nop

    :goto_8
    const/4 v0, 0x1

    goto/32 :goto_1b

    nop

    :goto_9
    iget-object v0, p0, Le9/d;->b:Ljava/util/Set;

    goto/32 :goto_27

    nop

    :goto_a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_c

    nop

    :goto_b
    new-instance p3, Ljava/lang/StringBuilder;

    goto/32 :goto_1d

    nop

    :goto_c
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto/32 :goto_6

    nop

    :goto_d
    new-array p3, v2, [Ljava/lang/Object;

    goto/32 :goto_20

    nop

    :goto_e
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_7

    nop

    :goto_f
    const-string v0, "-- notifyTransitionEnd 1, msg = "

    goto/32 :goto_14

    nop

    :goto_10
    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto/32 :goto_18

    nop

    :goto_11
    if-nez p3, :cond_0

    goto/32 :goto_21

    :cond_0
    goto/32 :goto_b

    nop

    :goto_12
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto/32 :goto_17

    nop

    :goto_13
    iget v1, p1, Le9/q;->b:I

    goto/32 :goto_1e

    nop

    :goto_14
    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_5

    nop

    :goto_15
    invoke-static {}, Lj9/f;->d()Z

    move-result p3

    goto/32 :goto_11

    nop

    :goto_16
    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v0

    goto/32 :goto_3

    nop

    :goto_17
    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/32 :goto_2d

    nop

    :goto_18
    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    goto/32 :goto_d

    nop

    :goto_19
    sget-object v0, Le9/q;->m:Ljava/util/Map;

    goto/32 :goto_2f

    nop

    :goto_1a
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_2b

    nop

    :goto_1b
    invoke-virtual {p0, p1, v0}, Le9/d;->l(Le9/q;Z)Z

    goto/32 :goto_19

    nop

    :goto_1c
    if-nez v0, :cond_1

    goto/32 :goto_32

    :cond_1
    goto/32 :goto_2a

    nop

    :goto_1d
    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_f

    nop

    :goto_1e
    invoke-virtual {v0, p2, v1, p3, p1}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object p1

    goto/32 :goto_28

    nop

    :goto_1f
    const-string v3, "-- notifyTransitionEnd 0, msg = "

    goto/32 :goto_e

    nop

    :goto_20
    invoke-static {p2, p3}, Lj9/f;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_21
    goto/32 :goto_25

    nop

    :goto_22
    iget-object v1, p1, Le9/q;->e:Ljava/lang/Object;

    goto/32 :goto_24

    nop

    :goto_23
    new-array v1, v2, [Ljava/lang/Object;

    goto/32 :goto_1

    nop

    :goto_24
    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto/32 :goto_8

    nop

    :goto_25
    invoke-virtual {p0, p1, v2}, Le9/d;->l(Le9/q;Z)Z

    :goto_26
    goto/32 :goto_0

    nop

    :goto_27
    iget-object v1, p1, Le9/q;->e:Ljava/lang/Object;

    goto/32 :goto_16

    nop

    :goto_28
    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    goto/32 :goto_31

    nop

    :goto_29
    const/4 v2, 0x0

    goto/32 :goto_1c

    nop

    :goto_2a
    invoke-static {}, Lj9/f;->d()Z

    move-result v0

    goto/32 :goto_2e

    nop

    :goto_2b
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_1f

    nop

    :goto_2c
    iget-object v0, v0, Lmiuix/animation/b;->a:Le9/n;

    goto/32 :goto_13

    nop

    :goto_2d
    iget-object v0, p0, Le9/d;->a:Lmiuix/animation/b;

    goto/32 :goto_2c

    nop

    :goto_2e
    if-nez v0, :cond_2

    goto/32 :goto_2

    :cond_2
    goto/32 :goto_1a

    nop

    :goto_2f
    iget v1, p1, Le9/q;->b:I

    goto/32 :goto_12

    nop

    :goto_30
    invoke-virtual {p3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_10

    nop

    :goto_31
    goto :goto_26

    :goto_32
    goto/32 :goto_15

    nop
.end method

.method k(Le9/q;)Z
    .locals 6

    goto/32 :goto_38

    nop

    :goto_0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_26

    nop

    :goto_1
    invoke-virtual {v4}, Ljava/lang/Object;->hashCode()I

    move-result v4

    goto/32 :goto_17

    nop

    :goto_2
    aput-object v2, v3, v1

    goto/32 :goto_1b

    nop

    :goto_3
    check-cast v0, Le9/q;

    goto/32 :goto_4

    nop

    :goto_4
    const/4 v1, 0x1

    goto/32 :goto_34

    nop

    :goto_5
    const-string p1, "----- removePendingRemovedInfo"

    goto/32 :goto_c

    nop

    :goto_6
    new-instance v2, Ljava/lang/StringBuilder;

    goto/32 :goto_19

    nop

    :goto_7
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_b

    nop

    :goto_8
    const-string v4, ", info.startTime = "

    goto/32 :goto_1f

    nop

    :goto_9
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_14

    nop

    :goto_a
    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_3

    nop

    :goto_b
    const-string v4, ", key = "

    goto/32 :goto_31

    nop

    :goto_c
    invoke-static {p1, v3}, Lj9/f;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_d
    goto/32 :goto_3e

    nop

    :goto_e
    new-instance v4, Ljava/lang/StringBuilder;

    goto/32 :goto_11

    nop

    :goto_f
    invoke-static {}, Lj9/f;->d()Z

    move-result v3

    goto/32 :goto_25

    nop

    :goto_10
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_49

    nop

    :goto_11
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_47

    nop

    :goto_12
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/32 :goto_1d

    nop

    :goto_13
    const/4 v3, 0x5

    goto/32 :goto_24

    nop

    :goto_14
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    goto/32 :goto_35

    nop

    :goto_15
    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    goto/32 :goto_0

    nop

    :goto_16
    aput-object v2, v3, v1

    goto/32 :goto_1e

    nop

    :goto_17
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/32 :goto_48

    nop

    :goto_18
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_3c

    nop

    :goto_19
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_1a

    nop

    :goto_1a
    const-string v4, ", mRunningInfo.size = "

    goto/32 :goto_18

    nop

    :goto_1b
    const/4 v1, 0x2

    goto/32 :goto_41

    nop

    :goto_1c
    const-string v4, " "

    goto/32 :goto_3b

    nop

    :goto_1d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_2e

    nop

    :goto_1e
    const/4 v1, 0x3

    goto/32 :goto_6

    nop

    :goto_1f
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_23

    nop

    :goto_20
    new-instance v2, Ljava/lang/StringBuilder;

    goto/32 :goto_28

    nop

    :goto_21
    iget-object v0, v0, Le9/q;->e:Ljava/lang/Object;

    goto/32 :goto_3a

    nop

    :goto_22
    iget-object v3, p0, Le9/d;->f:Ljava/util/concurrent/ConcurrentHashMap;

    goto/32 :goto_21

    nop

    :goto_23
    iget-wide v4, p1, Le9/q;->i:J

    goto/32 :goto_15

    nop

    :goto_24
    new-array v3, v3, [Ljava/lang/Object;

    goto/32 :goto_e

    nop

    :goto_25
    if-nez v3, :cond_0

    goto/32 :goto_d

    :cond_0
    goto/32 :goto_13

    nop

    :goto_26
    aput-object p1, v3, v1

    goto/32 :goto_5

    nop

    :goto_27
    new-instance v2, Ljava/lang/StringBuilder;

    goto/32 :goto_3f

    nop

    :goto_28
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_8

    nop

    :goto_29
    aput-object v4, v3, v2

    goto/32 :goto_27

    nop

    :goto_2a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_2

    nop

    :goto_2b
    if-nez v0, :cond_1

    goto/32 :goto_45

    :cond_1
    goto/32 :goto_3d

    nop

    :goto_2c
    move v0, v2

    :goto_2d
    goto/32 :goto_f

    nop

    :goto_2e
    aput-object v2, v3, v1

    goto/32 :goto_43

    nop

    :goto_2f
    iget v4, p1, Le9/q;->b:I

    goto/32 :goto_39

    nop

    :goto_30
    iget-object v4, p1, Le9/q;->e:Ljava/lang/Object;

    goto/32 :goto_42

    nop

    :goto_31
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_30

    nop

    :goto_32
    move v0, v1

    goto/32 :goto_44

    nop

    :goto_33
    iget-object v4, p1, Le9/q;->e:Ljava/lang/Object;

    goto/32 :goto_1

    nop

    :goto_34
    const/4 v2, 0x0

    goto/32 :goto_2b

    nop

    :goto_35
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto/32 :goto_29

    nop

    :goto_36
    iget-object v1, p1, Le9/q;->e:Ljava/lang/Object;

    goto/32 :goto_a

    nop

    :goto_37
    invoke-virtual {v4}, Ljava/util/concurrent/ConcurrentHashMap;->size()I

    move-result v4

    goto/32 :goto_12

    nop

    :goto_38
    iget-object v0, p0, Le9/d;->f:Ljava/util/concurrent/ConcurrentHashMap;

    goto/32 :goto_36

    nop

    :goto_39
    if-eq v3, v4, :cond_2

    goto/32 :goto_45

    :cond_2
    goto/32 :goto_22

    nop

    :goto_3a
    invoke-virtual {v3, v0}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto/32 :goto_32

    nop

    :goto_3b
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_33

    nop

    :goto_3c
    iget-object v4, p0, Le9/d;->e:Ljava/util/concurrent/ConcurrentHashMap;

    goto/32 :goto_37

    nop

    :goto_3d
    iget v3, v0, Le9/q;->b:I

    goto/32 :goto_2f

    nop

    :goto_3e
    return v0

    :goto_3f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_46

    nop

    :goto_40
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/32 :goto_2a

    nop

    :goto_41
    new-instance v2, Ljava/lang/StringBuilder;

    goto/32 :goto_7

    nop

    :goto_42
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto/32 :goto_1c

    nop

    :goto_43
    const/4 v1, 0x4

    goto/32 :goto_20

    nop

    :goto_44
    goto/16 :goto_2d

    :goto_45
    goto/32 :goto_2c

    nop

    :goto_46
    const-string v4, ", id = "

    goto/32 :goto_10

    nop

    :goto_47
    const-string v5, " removed = "

    goto/32 :goto_9

    nop

    :goto_48
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_16

    nop

    :goto_49
    iget v4, p1, Le9/q;->b:I

    goto/32 :goto_40

    nop
.end method

.method l(Le9/q;Z)Z
    .locals 8

    goto/32 :goto_5

    nop

    :goto_0
    new-array v1, v2, [Ljava/lang/Object;

    goto/32 :goto_6f

    nop

    :goto_1
    new-array v4, v4, [Ljava/lang/Object;

    goto/32 :goto_d

    nop

    :goto_2
    const/4 v1, 0x2

    goto/32 :goto_3e

    nop

    :goto_3
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_5f

    nop

    :goto_4
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_32

    nop

    :goto_5
    iget-object v0, p0, Le9/d;->e:Ljava/util/concurrent/ConcurrentHashMap;

    goto/32 :goto_31

    nop

    :goto_6
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_19

    nop

    :goto_7
    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_34

    nop

    :goto_8
    iget-object v3, p0, Le9/d;->a:Lmiuix/animation/b;

    goto/32 :goto_40

    nop

    :goto_9
    return v0

    :goto_a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/32 :goto_3f

    nop

    :goto_b
    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    goto/32 :goto_79

    nop

    :goto_c
    iget-object v6, p0, Le9/d;->e:Ljava/util/concurrent/ConcurrentHashMap;

    goto/32 :goto_30

    nop

    :goto_d
    new-instance v5, Ljava/lang/StringBuilder;

    goto/32 :goto_25

    nop

    :goto_e
    new-instance v1, Ljava/lang/StringBuilder;

    goto/32 :goto_68

    nop

    :goto_f
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_10
    goto/32 :goto_67

    nop

    :goto_11
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_65

    nop

    :goto_12
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_60

    nop

    :goto_13
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_6b

    nop

    :goto_14
    new-instance v5, Ljava/lang/StringBuilder;

    goto/32 :goto_52

    nop

    :goto_15
    aput-object v1, v4, p1

    goto/32 :goto_75

    nop

    :goto_16
    aput-object v1, v4, p1

    goto/32 :goto_56

    nop

    :goto_17
    const-string v6, ", mRunningInfo.size = "

    goto/32 :goto_35

    nop

    :goto_18
    new-instance v5, Ljava/lang/StringBuilder;

    goto/32 :goto_11

    nop

    :goto_19
    const-string v5, "----- removeRunningInfo, pending = "

    goto/32 :goto_12

    nop

    :goto_1a
    iget-object v4, p0, Le9/d;->d:Ljava/util/concurrent/ConcurrentHashMap;

    goto/32 :goto_28

    nop

    :goto_1b
    iget-object v3, p0, Le9/d;->e:Ljava/util/concurrent/ConcurrentHashMap;

    goto/32 :goto_21

    nop

    :goto_1c
    new-instance v1, Ljava/lang/StringBuilder;

    goto/32 :goto_3b

    nop

    :goto_1d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto/32 :goto_5d

    nop

    :goto_1e
    iget v3, v0, Le9/q;->b:I

    goto/32 :goto_7b

    nop

    :goto_1f
    const/4 v1, 0x1

    goto/32 :goto_78

    nop

    :goto_20
    if-nez p2, :cond_0

    goto/32 :goto_45

    :cond_0
    goto/32 :goto_5c

    nop

    :goto_21
    iget-object v4, p1, Le9/q;->e:Ljava/lang/Object;

    goto/32 :goto_33

    nop

    :goto_22
    invoke-virtual {v6}, Ljava/lang/Object;->hashCode()I

    move-result v6

    goto/32 :goto_a

    nop

    :goto_23
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/32 :goto_3d

    nop

    :goto_24
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_64

    nop

    :goto_25
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_46

    nop

    :goto_26
    new-array v3, v2, [Lh9/b;

    goto/32 :goto_58

    nop

    :goto_27
    if-nez v4, :cond_1

    goto/32 :goto_50

    :cond_1
    goto/32 :goto_54

    nop

    :goto_28
    invoke-virtual {v4}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    :goto_29
    goto/32 :goto_5a

    nop

    :goto_2a
    const-string v5, ", isAnimRunning = "

    goto/32 :goto_3

    nop

    :goto_2b
    iget-object v6, p1, Le9/q;->e:Ljava/lang/Object;

    goto/32 :goto_1d

    nop

    :goto_2c
    if-nez v0, :cond_2

    goto/32 :goto_4e

    :cond_2
    goto/32 :goto_1e

    nop

    :goto_2d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/32 :goto_23

    nop

    :goto_2e
    if-eq v3, v4, :cond_3

    goto/32 :goto_4e

    :cond_3
    goto/32 :goto_1b

    nop

    :goto_2f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/32 :goto_4a

    nop

    :goto_30
    invoke-virtual {v6}, Ljava/util/concurrent/ConcurrentHashMap;->size()I

    move-result v6

    goto/32 :goto_2d

    nop

    :goto_31
    iget-object v1, p1, Le9/q;->e:Ljava/lang/Object;

    goto/32 :goto_7

    nop

    :goto_32
    iget v6, p1, Le9/q;->b:I

    goto/32 :goto_2f

    nop

    :goto_33
    invoke-virtual {v3, v4}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto/32 :goto_20

    nop

    :goto_34
    check-cast v0, Le9/q;

    goto/32 :goto_1f

    nop

    :goto_35
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_c

    nop

    :goto_36
    move v0, v1

    goto/32 :goto_4d

    nop

    :goto_37
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/32 :goto_6e

    nop

    :goto_38
    aput-object p1, v4, v1

    goto/32 :goto_47

    nop

    :goto_39
    move v0, v2

    :goto_3a
    goto/32 :goto_26

    nop

    :goto_3b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_2a

    nop

    :goto_3c
    iget-wide v6, p1, Le9/q;->i:J

    goto/32 :goto_b

    nop

    :goto_3d
    aput-object v5, v4, v1

    goto/32 :goto_55

    nop

    :goto_3e
    new-instance v5, Ljava/lang/StringBuilder;

    goto/32 :goto_66

    nop

    :goto_3f
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/32 :goto_74

    nop

    :goto_40
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto/32 :goto_57

    nop

    :goto_41
    aput-object v5, v4, v1

    goto/32 :goto_2

    nop

    :goto_42
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    goto/32 :goto_37

    nop

    :goto_43
    check-cast p2, Le9/q;

    goto/32 :goto_62

    nop

    :goto_44
    invoke-virtual {v3, v4, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_45
    goto/32 :goto_36

    nop

    :goto_46
    const-string v6, " removed = "

    goto/32 :goto_71

    nop

    :goto_47
    const/4 p1, 0x5

    goto/32 :goto_1c

    nop

    :goto_48
    invoke-virtual {p1}, Ljava/util/concurrent/ConcurrentHashMap;->size()I

    move-result p1

    goto/32 :goto_59

    nop

    :goto_49
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_2b

    nop

    :goto_4a
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/32 :goto_41

    nop

    :goto_4b
    new-instance v5, Ljava/lang/StringBuilder;

    goto/32 :goto_7c

    nop

    :goto_4c
    const-string v6, ", info.startTime = "

    goto/32 :goto_7d

    nop

    :goto_4d
    goto :goto_3a

    :goto_4e
    goto/32 :goto_39

    nop

    :goto_4f
    goto/16 :goto_10

    :goto_50
    goto/32 :goto_9

    nop

    :goto_51
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_15

    nop

    :goto_52
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_17

    nop

    :goto_53
    if-eqz v3, :cond_4

    goto/32 :goto_29

    :cond_4
    goto/32 :goto_1a

    nop

    :goto_54
    new-instance v4, Ljava/lang/StringBuilder;

    goto/32 :goto_6

    nop

    :goto_55
    const/4 v1, 0x4

    goto/32 :goto_4b

    nop

    :goto_56
    invoke-static {p2, v4}, Lj9/f;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/32 :goto_76

    nop

    :goto_57
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_16

    nop

    :goto_58
    invoke-virtual {p0, v3}, Le9/d;->g([Lh9/b;)Z

    move-result v3

    goto/32 :goto_53

    nop

    :goto_59
    if-gtz p1, :cond_5

    goto/32 :goto_50

    :cond_5
    goto/32 :goto_6c

    nop

    :goto_5a
    invoke-static {}, Lj9/f;->d()Z

    move-result v4

    goto/32 :goto_27

    nop

    :goto_5b
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_8

    nop

    :goto_5c
    iget-object v3, p0, Le9/d;->f:Ljava/util/concurrent/ConcurrentHashMap;

    goto/32 :goto_73

    nop

    :goto_5d
    const-string v6, " "

    goto/32 :goto_24

    nop

    :goto_5e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_6d

    nop

    :goto_5f
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    goto/32 :goto_51

    nop

    :goto_60
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    goto/32 :goto_77

    nop

    :goto_61
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    goto/32 :goto_43

    nop

    :goto_62
    new-instance v1, Ljava/lang/StringBuilder;

    goto/32 :goto_5e

    nop

    :goto_63
    const-string v3, ", target = "

    goto/32 :goto_5b

    nop

    :goto_64
    iget-object v6, p1, Le9/q;->e:Ljava/lang/Object;

    goto/32 :goto_22

    nop

    :goto_65
    const-string v6, ", id = "

    goto/32 :goto_4

    nop

    :goto_66
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_72

    nop

    :goto_67
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    goto/32 :goto_7e

    nop

    :goto_68
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_63

    nop

    :goto_69
    const/4 v4, 0x7

    goto/32 :goto_1

    nop

    :goto_6a
    const/4 v1, 0x3

    goto/32 :goto_14

    nop

    :goto_6b
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto/32 :goto_7a

    nop

    :goto_6c
    iget-object p1, p0, Le9/d;->e:Ljava/util/concurrent/ConcurrentHashMap;

    goto/32 :goto_70

    nop

    :goto_6d
    const-string v3, "------ after remove resetRunInfo = "

    goto/32 :goto_13

    nop

    :goto_6e
    aput-object v5, v4, v2

    goto/32 :goto_18

    nop

    :goto_6f
    invoke-static {p2, v1}, Lj9/f;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/32 :goto_4f

    nop

    :goto_70
    invoke-virtual {p1}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object p1

    goto/32 :goto_f

    nop

    :goto_71
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_42

    nop

    :goto_72
    const-string v6, ", key = "

    goto/32 :goto_49

    nop

    :goto_73
    iget-object v4, p1, Le9/q;->e:Ljava/lang/Object;

    goto/32 :goto_44

    nop

    :goto_74
    aput-object v5, v4, v1

    goto/32 :goto_6a

    nop

    :goto_75
    const/4 p1, 0x6

    goto/32 :goto_e

    nop

    :goto_76
    iget-object p1, p0, Le9/d;->e:Ljava/util/concurrent/ConcurrentHashMap;

    goto/32 :goto_48

    nop

    :goto_77
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    goto/32 :goto_69

    nop

    :goto_78
    const/4 v2, 0x0

    goto/32 :goto_2c

    nop

    :goto_79
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_38

    nop

    :goto_7a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    goto/32 :goto_0

    nop

    :goto_7b
    iget v4, p1, Le9/q;->b:I

    goto/32 :goto_2e

    nop

    :goto_7c
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_4c

    nop

    :goto_7d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_3c

    nop

    :goto_7e
    if-nez p2, :cond_6

    goto/32 :goto_50

    :cond_6
    goto/32 :goto_61

    nop
.end method

.method public n(Lmiuix/animation/b;)V
    .locals 0

    iput-object p1, p0, Le9/d;->a:Lmiuix/animation/b;

    return-void
.end method

.method public p(Ld9/a;Lc9/b;)V
    .locals 5

    invoke-static {}, Lj9/f;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "setTo, target = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Le9/d;->a:Lmiuix/animation/b;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "to = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lj9/f;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    invoke-virtual {p1}, Ld9/a;->n()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    const/16 v1, 0x96

    if-le v0, v1, :cond_1

    sget-object p2, Le9/f;->h:Le9/m;

    iget-object v0, p0, Le9/d;->a:Lmiuix/animation/b;

    invoke-virtual {p2, v0, p1}, Le9/m;->b(Lmiuix/animation/b;Ld9/a;)V

    goto :goto_0

    :cond_1
    invoke-direct {p0, p1, p2}, Le9/d;->o(Ld9/a;Lc9/b;)V

    :goto_0
    return-void
.end method

.method public q(Lh9/b;F)V
    .locals 2

    invoke-virtual {p0, p1}, Le9/d;->a(Lh9/b;)Lf9/c;

    move-result-object p1

    float-to-double v0, p2

    iput-wide v0, p1, Lf9/c;->c:D

    return-void
.end method

.method r(Le9/q;)V
    .locals 4

    goto/32 :goto_1f

    nop

    :goto_0
    invoke-static {v0, v2}, Lj9/f;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_1
    goto/32 :goto_19

    nop

    :goto_2
    invoke-direct {p0, p1}, Le9/d;->m(Le9/q;)V

    goto/32 :goto_d

    nop

    :goto_3
    invoke-interface {v0, v2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/32 :goto_1b

    nop

    :goto_4
    const/4 v0, 0x1

    goto/32 :goto_17

    nop

    :goto_5
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto/32 :goto_b

    nop

    :goto_6
    return-void

    :goto_7
    iget-object v0, v0, Lmiuix/animation/b;->b:Le9/d;

    goto/32 :goto_15

    nop

    :goto_8
    const-string v2, "-- setupTransition "

    goto/32 :goto_12

    nop

    :goto_9
    invoke-virtual {v0, v2, p1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/32 :goto_28

    nop

    :goto_a
    const/4 v1, 0x0

    goto/32 :goto_1c

    nop

    :goto_b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_1d

    nop

    :goto_c
    invoke-interface {v0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto/32 :goto_23

    nop

    :goto_d
    iget-object v0, p1, Le9/q;->c:Lmiuix/animation/b;

    goto/32 :goto_7

    nop

    :goto_e
    sget-object v0, Le9/q;->m:Ljava/util/Map;

    goto/32 :goto_27

    nop

    :goto_f
    iget-object v2, p1, Le9/q;->e:Ljava/lang/Object;

    goto/32 :goto_c

    nop

    :goto_10
    iget-object v2, v2, Lc9/a;->i:Ljava/util/HashSet;

    goto/32 :goto_26

    nop

    :goto_11
    invoke-virtual {v0, v2, v3, v1, p1}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object p1

    goto/32 :goto_21

    nop

    :goto_12
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_5

    nop

    :goto_13
    const/4 v2, 0x4

    goto/32 :goto_24

    nop

    :goto_14
    iget-object v2, p1, Le9/q;->e:Ljava/lang/Object;

    goto/32 :goto_9

    nop

    :goto_15
    iget-object v0, v0, Le9/d;->b:Ljava/util/Set;

    goto/32 :goto_f

    nop

    :goto_16
    if-eqz v2, :cond_0

    goto/32 :goto_22

    :cond_0
    goto/32 :goto_1a

    nop

    :goto_17
    invoke-virtual {p1, v0}, Le9/q;->j(Z)V

    goto/32 :goto_2

    nop

    :goto_18
    iget-object v0, v0, Lmiuix/animation/b;->a:Le9/n;

    goto/32 :goto_13

    nop

    :goto_19
    iget-object v0, p0, Le9/d;->e:Ljava/util/concurrent/ConcurrentHashMap;

    goto/32 :goto_14

    nop

    :goto_1a
    if-nez v0, :cond_1

    goto/32 :goto_22

    :cond_1
    goto/32 :goto_e

    nop

    :goto_1b
    iget-object v0, p1, Le9/q;->c:Lmiuix/animation/b;

    goto/32 :goto_18

    nop

    :goto_1c
    if-nez v0, :cond_2

    goto/32 :goto_1

    :cond_2
    goto/32 :goto_1e

    nop

    :goto_1d
    new-array v2, v1, [Ljava/lang/Object;

    goto/32 :goto_0

    nop

    :goto_1e
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_25

    nop

    :goto_1f
    invoke-static {}, Lj9/f;->d()Z

    move-result v0

    goto/32 :goto_a

    nop

    :goto_20
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto/32 :goto_3

    nop

    :goto_21
    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    :goto_22
    goto/32 :goto_6

    nop

    :goto_23
    iget-object v2, p1, Le9/q;->f:Lc9/a;

    goto/32 :goto_10

    nop

    :goto_24
    iget v3, p1, Le9/q;->b:I

    goto/32 :goto_11

    nop

    :goto_25
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_8

    nop

    :goto_26
    invoke-virtual {v2}, Ljava/util/HashSet;->isEmpty()Z

    move-result v2

    goto/32 :goto_16

    nop

    :goto_27
    iget v2, p1, Le9/q;->b:I

    goto/32 :goto_20

    nop

    :goto_28
    invoke-virtual {p1, p0}, Le9/q;->h(Le9/q$a;)V

    goto/32 :goto_4

    nop
.end method

.method public s(Le9/q;)V
    .locals 3

    invoke-static {}, Lj9/f;->d()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    new-array v0, v1, [Ljava/lang/Object;

    const-string v2, "- AnimManager.startAnim "

    invoke-static {v2, v0}, Lj9/f;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    invoke-direct {p0, p1}, Le9/d;->j(Le9/q;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, ".startAnim, pendState"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-array v0, v1, [Ljava/lang/Object;

    invoke-static {p1, v0}, Lj9/f;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    :cond_1
    sget-object v0, Le9/q;->m:Ljava/util/Map;

    iget v2, p1, Le9/q;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Le9/f;->h:Le9/m;

    const/4 v2, 0x1

    iget p1, p1, Le9/q;->b:I

    invoke-virtual {v0, v2, p1, v1}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object p1

    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    return-void
.end method

.method public t(Z)V
    .locals 1

    iget-object v0, p0, Le9/d;->a:Lmiuix/animation/b;

    iget-object v0, v0, Lmiuix/animation/b;->a:Le9/n;

    invoke-virtual {v0, p1}, Le9/n;->g(Z)V

    return-void
.end method
