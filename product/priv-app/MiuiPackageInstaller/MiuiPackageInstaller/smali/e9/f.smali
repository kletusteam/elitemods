.class public Le9/f;
.super Ljava/lang/Object;

# interfaces
.implements Lg9/b$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Le9/f$c;
    }
.end annotation


# static fields
.field private static final g:Landroid/os/HandlerThread;

.field public static final h:Le9/m;

.field static volatile i:Landroid/os/Handler;


# instance fields
.field private volatile a:J

.field private b:J

.field private c:[J

.field private d:I

.field private volatile e:Z

.field private f:F


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "AnimRunnerThread"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    sput-object v0, Le9/f;->g:Landroid/os/HandlerThread;

    invoke-static {}, Lmiuix/animation/a;->o()Landroid/os/Looper;

    move-result-object v1

    invoke-static {v1}, Le9/f;->g(Landroid/os/Looper;)V

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    new-instance v1, Le9/m;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v1, v0}, Le9/m;-><init>(Landroid/os/Looper;)V

    sput-object v1, Le9/f;->h:Le9/m;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x10

    iput-wide v0, p0, Le9/f;->a:J

    const/4 v0, 0x5

    new-array v0, v0, [J

    fill-array-data v0, :array_0

    iput-object v0, p0, Le9/f;->c:[J

    const/4 v0, 0x0

    iput v0, p0, Le9/f;->d:I

    return-void

    nop

    :array_0
    .array-data 8
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data
.end method

.method synthetic constructor <init>(Le9/f$a;)V
    .locals 0

    invoke-direct {p0}, Le9/f;-><init>()V

    return-void
.end method

.method static synthetic b()V
    .locals 0

    invoke-static {}, Le9/f;->r()V

    return-void
.end method

.method static synthetic c()V
    .locals 0

    invoke-static {}, Le9/f;->k()V

    return-void
.end method

.method private d([J)J
    .locals 9

    array-length v0, p1

    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    move v4, v1

    move-wide v5, v2

    :goto_0
    if-ge v1, v0, :cond_1

    aget-wide v7, p1, v1

    add-long/2addr v5, v7

    cmp-long v7, v7, v2

    if-lez v7, :cond_0

    add-int/lit8 v4, v4, 0x1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    if-lez v4, :cond_2

    int-to-long v0, v4

    div-long v2, v5, v0

    :cond_2
    return-wide v2
.end method

.method private e(J)J
    .locals 5

    iget-object v0, p0, Le9/f;->c:[J

    invoke-direct {p0, v0}, Le9/f;->d([J)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    move-wide p1, v0

    :cond_0
    cmp-long v0, p1, v2

    const-wide/16 v1, 0x10

    if-eqz v0, :cond_1

    cmp-long v0, p1, v1

    if-lez v0, :cond_2

    :cond_1
    move-wide p1, v1

    :cond_2
    long-to-float p1, p1

    iget p2, p0, Le9/f;->f:F

    div-float/2addr p1, p2

    float-to-double p1, p1

    invoke-static {p1, p2}, Ljava/lang/Math;->ceil(D)D

    move-result-wide p1

    double-to-long p1, p1

    return-wide p1
.end method

.method public static g(Landroid/os/Looper;)V
    .locals 1

    if-nez p0, :cond_0

    return-void

    :cond_0
    new-instance v0, Le9/f$a;

    invoke-direct {v0, p0}, Le9/f$a;-><init>(Landroid/os/Looper;)V

    sput-object v0, Le9/f;->i:Landroid/os/Handler;

    return-void
.end method

.method private static k()V
    .locals 4

    invoke-static {}, Le9/f;->m()Le9/f;

    move-result-object v0

    iget-boolean v1, v0, Le9/f;->e:Z

    if-nez v1, :cond_0

    return-void

    :cond_0
    invoke-static {}, Lj9/f;->d()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    new-array v1, v2, [Ljava/lang/Object;

    const-string v3, "AnimRunner.endAnimation"

    invoke-static {v3, v1}, Lj9/f;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_1
    iput-boolean v2, v0, Le9/f;->e:Z

    invoke-static {}, Lg9/b;->i()Lg9/b;

    move-result-object v1

    invoke-virtual {v1, v0}, Lg9/b;->m(Lg9/b$b;)V

    return-void
.end method

.method public static m()Le9/f;
    .locals 1

    sget-object v0, Le9/f$c;->a:Le9/f;

    return-object v0
.end method

.method public static n()Landroid/os/Handler;
    .locals 1

    sget-object v0, Le9/f;->i:Landroid/os/Handler;

    return-object v0
.end method

.method private static r()V
    .locals 4

    invoke-static {}, Le9/f;->m()Le9/f;

    move-result-object v0

    iget-boolean v1, v0, Le9/f;->e:Z

    if-eqz v1, :cond_0

    return-void

    :cond_0
    invoke-static {}, Lj9/f;->d()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "AnimRunner.start"

    invoke-static {v2, v1}, Lj9/f;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_1
    invoke-static {}, Lmiuix/animation/a;->s()F

    move-result v1

    iput v1, v0, Le9/f;->f:F

    const/4 v1, 0x1

    iput-boolean v1, v0, Le9/f;->e:Z

    invoke-static {}, Lg9/b;->i()Lg9/b;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v0, v2, v3}, Lg9/b;->f(Lg9/b$b;J)V

    return-void
.end method

.method private static s(Ljava/util/Collection;Z)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lmiuix/animation/b;",
            ">;Z)V"
        }
    .end annotation

    invoke-interface {p0}, Ljava/util/Collection;->size()I

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Le9/f;->h:Le9/m;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_1
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiuix/animation/b;

    iget-object v1, v0, Lmiuix/animation/b;->b:Le9/d;

    const/4 v2, 0x0

    new-array v3, v2, [Lh9/b;

    invoke-virtual {v1, v3}, Le9/d;->g([Lh9/b;)Z

    move-result v1

    iget-object v3, v0, Lmiuix/animation/b;->b:Le9/d;

    invoke-virtual {v3}, Le9/d;->h()Z

    move-result v3

    invoke-virtual {v0}, Lmiuix/animation/b;->m()Z

    move-result v4

    if-eqz v1, :cond_2

    iget-object v0, v0, Lmiuix/animation/b;->b:Le9/d;

    invoke-virtual {v0, p1}, Le9/d;->t(Z)V

    goto :goto_0

    :cond_2
    if-nez v3, :cond_1

    if-nez v1, :cond_1

    const-wide/16 v5, 0x1

    invoke-virtual {v0, v5, v6}, Lmiuix/animation/b;->j(J)Z

    move-result v1

    if-eqz v1, :cond_1

    if-eqz v4, :cond_1

    const/4 v1, 0x1

    new-array v1, v1, [Lmiuix/animation/b;

    aput-object v0, v1, v2

    invoke-static {v1}, Lmiuix/animation/a;->f([Ljava/lang/Object;)V

    goto :goto_0

    :cond_3
    return-void
.end method

.method private t(J)V
    .locals 5

    iget-wide v0, p0, Le9/f;->b:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    goto :goto_0

    :cond_0
    sub-long v2, p1, v0

    :goto_0
    iput-wide p1, p0, Le9/f;->b:J

    iget p1, p0, Le9/f;->d:I

    rem-int/lit8 p2, p1, 0x5

    iget-object v0, p0, Le9/f;->c:[J

    aput-wide v2, v0, p2

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, Le9/f;->d:I

    invoke-direct {p0, v2, v3}, Le9/f;->e(J)J

    move-result-wide p1

    iput-wide p1, p0, Le9/f;->a:J

    return-void
.end method


# virtual methods
.method public a(J)Z
    .locals 5

    invoke-direct {p0, p1, p2}, Le9/f;->t(J)V

    iget-boolean p1, p0, Le9/f;->e:Z

    if-eqz p1, :cond_6

    invoke-static {}, Lmiuix/animation/a;->r()Ljava/util/Collection;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p2

    const/4 v0, 0x0

    move v1, v0

    :cond_0
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiuix/animation/b;

    iget-object v3, v2, Lmiuix/animation/b;->b:Le9/d;

    new-array v4, v0, [Lh9/b;

    invoke-virtual {v3, v4}, Le9/d;->g([Lh9/b;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v2, v2, Lmiuix/animation/b;->b:Le9/d;

    invoke-virtual {v2}, Le9/d;->e()I

    move-result v2

    add-int/2addr v1, v2

    goto :goto_0

    :cond_1
    const/16 p2, 0x1f4

    if-le v1, p2, :cond_2

    const/4 v0, 0x1

    :cond_2
    if-nez v0, :cond_3

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result p2

    if-gtz p2, :cond_4

    :cond_3
    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result p2

    if-nez p2, :cond_5

    :cond_4
    invoke-static {p1, v0}, Le9/f;->s(Ljava/util/Collection;Z)V

    :cond_5
    sget-object p2, Le9/f;->h:Le9/m;

    invoke-virtual {p2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    const/4 v2, 0x3

    iput v2, v1, Landroid/os/Message;->what:I

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {p2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    if-eqz v0, :cond_6

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result p2

    if-lez p2, :cond_6

    invoke-static {p1, v0}, Le9/f;->s(Ljava/util/Collection;Z)V

    :cond_6
    iget-boolean p1, p0, Le9/f;->e:Z

    return p1
.end method

.method public varargs f(Lmiuix/animation/b;[Lh9/b;)V
    .locals 4

    sget-object v0, Le9/f;->h:Le9/m;

    new-instance v1, Le9/e;

    const/4 v2, 0x4

    const/4 v3, 0x0

    invoke-direct {v1, p1, v2, v3, p2}, Le9/e;-><init>(Lmiuix/animation/b;B[Ljava/lang/String;[Lh9/b;)V

    invoke-virtual {v0, v1}, Le9/m;->m(Le9/e;)V

    return-void
.end method

.method h()V
    .locals 2

    goto/32 :goto_f

    nop

    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_e

    nop

    :goto_1
    if-nez v0, :cond_0

    goto/32 :goto_a

    :cond_0
    goto/32 :goto_6

    nop

    :goto_2
    const-string v1, "AnimRunner.end handler is null! looper: "

    goto/32 :goto_16

    nop

    :goto_3
    if-eq v0, v1, :cond_1

    goto/32 :goto_c

    :cond_1
    goto/32 :goto_5

    nop

    :goto_4
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    goto/32 :goto_7

    nop

    :goto_5
    invoke-static {}, Le9/f;->k()V

    goto/32 :goto_b

    nop

    :goto_6
    const/4 v1, 0x1

    goto/32 :goto_12

    nop

    :goto_7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto/32 :goto_0

    nop

    :goto_8
    return-void

    :goto_9
    goto :goto_15

    :goto_a
    goto/32 :goto_11

    nop

    :goto_b
    goto :goto_15

    :goto_c
    goto/32 :goto_13

    nop

    :goto_d
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    goto/32 :goto_3

    nop

    :goto_e
    const-string v1, "miuix_anim"

    goto/32 :goto_14

    nop

    :goto_f
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    goto/32 :goto_d

    nop

    :goto_10
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_2

    nop

    :goto_11
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_10

    nop

    :goto_12
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/32 :goto_9

    nop

    :goto_13
    invoke-static {}, Le9/f;->n()Landroid/os/Handler;

    move-result-object v0

    goto/32 :goto_1

    nop

    :goto_14
    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_15
    goto/32 :goto_8

    nop

    :goto_16
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_4

    nop
.end method

.method public varargs i(Lmiuix/animation/b;[Lh9/b;)V
    .locals 4

    invoke-static {p2}, Lj9/a;->j([Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x3

    if-eqz v0, :cond_0

    iget-object v0, p1, Lmiuix/animation/b;->a:Le9/n;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    sget-object v0, Le9/f;->h:Le9/m;

    new-instance v2, Le9/e;

    const/4 v3, 0x0

    invoke-direct {v2, p1, v1, v3, p2}, Le9/e;-><init>(Lmiuix/animation/b;B[Ljava/lang/String;[Lh9/b;)V

    invoke-virtual {v0, v2}, Le9/m;->m(Le9/e;)V

    return-void
.end method

.method public varargs j(Lmiuix/animation/b;[Ljava/lang/String;)V
    .locals 4

    invoke-static {p2}, Lj9/a;->j([Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x3

    if-eqz v0, :cond_0

    iget-object v0, p1, Lmiuix/animation/b;->a:Le9/n;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    sget-object v0, Le9/f;->h:Le9/m;

    new-instance v2, Le9/e;

    const/4 v3, 0x0

    invoke-direct {v2, p1, v1, p2, v3}, Le9/e;-><init>(Lmiuix/animation/b;B[Ljava/lang/String;[Lh9/b;)V

    invoke-virtual {v0, v2}, Le9/m;->m(Le9/e;)V

    return-void
.end method

.method public l()J
    .locals 2

    iget-wide v0, p0, Le9/f;->a:J

    return-wide v0
.end method

.method public o(Le9/q;)V
    .locals 2

    invoke-static {}, Lj9/f;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "- AnimManager.run"

    invoke-static {v1, v0}, Lj9/f;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    iget-object v0, p1, Le9/q;->c:Lmiuix/animation/b;

    new-instance v1, Le9/f$b;

    invoke-direct {v1, p0, p1}, Le9/f$b;-><init>(Le9/f;Le9/q;)V

    invoke-virtual {v0, v1}, Lmiuix/animation/b;->b(Ljava/lang/Runnable;)V

    return-void
.end method

.method public p(Lmiuix/animation/b;Ld9/a;Ld9/a;Lc9/b;)V
    .locals 1

    new-instance v0, Le9/q;

    invoke-direct {v0, p1, p2, p3, p4}, Le9/q;-><init>(Lmiuix/animation/b;Ld9/a;Ld9/a;Lc9/b;)V

    invoke-virtual {p0, v0}, Le9/f;->o(Le9/q;)V

    return-void
.end method

.method q()V
    .locals 2

    goto/32 :goto_8

    nop

    :goto_0
    if-eq v0, v1, :cond_0

    goto/32 :goto_e

    :cond_0
    goto/32 :goto_10

    nop

    :goto_1
    const-string v1, "miuix_anim"

    goto/32 :goto_2

    nop

    :goto_2
    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_3
    goto/32 :goto_b

    nop

    :goto_4
    invoke-static {}, Le9/f;->n()Landroid/os/Handler;

    move-result-object v0

    goto/32 :goto_f

    nop

    :goto_5
    const-string v1, "AnimRunner.start handler is null! looper: "

    goto/32 :goto_a

    nop

    :goto_6
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto/32 :goto_c

    nop

    :goto_7
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    goto/32 :goto_6

    nop

    :goto_8
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    goto/32 :goto_9

    nop

    :goto_9
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    goto/32 :goto_0

    nop

    :goto_a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_7

    nop

    :goto_b
    return-void

    :goto_c
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_1

    nop

    :goto_d
    goto :goto_3

    :goto_e
    goto/32 :goto_4

    nop

    :goto_f
    if-nez v0, :cond_1

    goto/32 :goto_13

    :cond_1
    goto/32 :goto_14

    nop

    :goto_10
    invoke-static {}, Le9/f;->r()V

    goto/32 :goto_d

    nop

    :goto_11
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_16

    nop

    :goto_12
    goto :goto_3

    :goto_13
    goto/32 :goto_11

    nop

    :goto_14
    const/4 v1, 0x0

    goto/32 :goto_15

    nop

    :goto_15
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/32 :goto_12

    nop

    :goto_16
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_5

    nop
.end method
