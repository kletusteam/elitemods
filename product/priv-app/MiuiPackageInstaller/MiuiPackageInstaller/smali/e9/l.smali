.class public Le9/l;
.super Ljava/lang/Object;


# static fields
.field private static final a:Le9/q$a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Le9/l$a;

    invoke-direct {v0}, Le9/l$a;-><init>()V

    sput-object v0, Le9/l;->a:Le9/q$a;

    return-void
.end method

.method public static a(Lmiuix/animation/b;Ld9/a;Ld9/a;Lc9/b;)J
    .locals 9

    new-instance v0, Le9/q;

    invoke-direct {v0, p0, p1, p2, p3}, Le9/q;-><init>(Lmiuix/animation/b;Ld9/a;Ld9/a;Lc9/b;)V

    sget-object p0, Le9/l;->a:Le9/q$a;

    invoke-virtual {v0, p0}, Le9/q;->h(Le9/q$a;)V

    const/4 p0, 0x1

    invoke-virtual {v0, p0}, Le9/q;->j(Z)V

    invoke-static {}, Le9/f;->m()Le9/f;

    move-result-object p0

    invoke-virtual {p0}, Le9/f;->l()J

    move-result-wide p0

    move-wide p2, p0

    :goto_0
    iget-object v1, v0, Le9/q;->k:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Le9/i;

    const/4 v6, 0x0

    const/4 v7, 0x1

    move-wide v2, p2

    move-wide v4, p0

    invoke-static/range {v1 .. v7}, Le9/g;->a(Le9/i;JJZZ)V

    goto :goto_1

    :cond_0
    invoke-virtual {v0}, Le9/q;->f()Le9/h;

    move-result-object v1

    invoke-virtual {v1}, Le9/h;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    add-long/2addr p2, p0

    goto :goto_0

    :cond_1
    return-wide p2
.end method
