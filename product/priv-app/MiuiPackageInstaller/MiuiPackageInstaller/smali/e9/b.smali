.class public Le9/b;
.super Ljava/lang/Object;


# instance fields
.field public a:Lh9/b;

.field public b:D

.field public c:I

.field public d:Z

.field public e:B

.field public f:Lj9/c$a;

.field public g:J

.field public h:J

.field public i:J

.field public j:I

.field public k:D

.field public l:D

.field public m:D

.field public n:D

.field o:Z

.field p:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Le9/b;->j:I

    const-wide v0, 0x7fefffffffffffffL    # Double.MAX_VALUE

    iput-wide v0, p0, Le9/b;->l:D

    iput-wide v0, p0, Le9/b;->m:D

    iput-wide v0, p0, Le9/b;->n:D

    return-void
.end method


# virtual methods
.method a()V
    .locals 1

    goto/32 :goto_3

    nop

    :goto_0
    iput-object v0, p0, Le9/b;->a:Lh9/b;

    goto/32 :goto_1

    nop

    :goto_1
    iput-object v0, p0, Le9/b;->f:Lj9/c$a;

    goto/32 :goto_2

    nop

    :goto_2
    return-void

    :goto_3
    const/4 v0, 0x0

    goto/32 :goto_0

    nop
.end method

.method b(Lf9/c;Lc9/a;Lc9/c;)V
    .locals 2

    goto/32 :goto_5

    nop

    :goto_0
    iput-wide v0, p0, Le9/b;->l:D

    goto/32 :goto_1f

    nop

    :goto_1
    iget-object v0, p1, Lf9/c;->f:Le9/c;

    goto/32 :goto_d

    nop

    :goto_2
    iput-wide p1, p0, Le9/b;->g:J

    goto/32 :goto_f

    nop

    :goto_3
    iget-wide v0, p1, Lf9/c;->c:D

    goto/32 :goto_a

    nop

    :goto_4
    iput v0, p0, Le9/b;->c:I

    goto/32 :goto_1

    nop

    :goto_5
    iget-object v0, p1, Lf9/c;->a:Lh9/b;

    goto/32 :goto_20

    nop

    :goto_6
    invoke-static {p2, p3}, Le9/a;->e(Lc9/a;Lc9/c;)I

    move-result p1

    goto/32 :goto_13

    nop

    :goto_7
    iget-boolean p1, p1, Le9/c;->k:Z

    goto/32 :goto_1e

    nop

    :goto_8
    iget-wide v0, v0, Le9/c;->g:D

    goto/32 :goto_0

    nop

    :goto_9
    iput-object p1, p0, Le9/b;->f:Lj9/c$a;

    goto/32 :goto_1d

    nop

    :goto_a
    iput-wide v0, p0, Le9/b;->b:D

    goto/32 :goto_15

    nop

    :goto_b
    iput-wide v0, p0, Le9/b;->i:J

    goto/32 :goto_12

    nop

    :goto_c
    iput-byte v0, p0, Le9/b;->e:B

    goto/32 :goto_21

    nop

    :goto_d
    iget-byte v0, v0, Le9/c;->a:B

    goto/32 :goto_c

    nop

    :goto_e
    iget-object v0, p1, Lf9/c;->f:Le9/c;

    goto/32 :goto_8

    nop

    :goto_f
    return-void

    :goto_10
    iget-wide v0, v0, Le9/c;->i:D

    goto/32 :goto_18

    nop

    :goto_11
    iput-boolean v0, p0, Le9/b;->d:Z

    goto/32 :goto_1a

    nop

    :goto_12
    iget-object v0, p1, Lf9/c;->f:Le9/c;

    goto/32 :goto_14

    nop

    :goto_13
    iput p1, p0, Le9/b;->j:I

    goto/32 :goto_1c

    nop

    :goto_14
    iget-wide v0, v0, Le9/c;->e:D

    goto/32 :goto_26

    nop

    :goto_15
    iget v0, p1, Lf9/c;->d:I

    goto/32 :goto_4

    nop

    :goto_16
    iget-object v0, p1, Lf9/c;->f:Le9/c;

    goto/32 :goto_25

    nop

    :goto_17
    iput-wide v0, p0, Le9/b;->m:D

    goto/32 :goto_22

    nop

    :goto_18
    iput-wide v0, p0, Le9/b;->n:D

    goto/32 :goto_19

    nop

    :goto_19
    iget-boolean v0, p1, Lf9/c;->e:Z

    goto/32 :goto_11

    nop

    :goto_1a
    iget-object p1, p1, Lf9/c;->f:Le9/c;

    goto/32 :goto_7

    nop

    :goto_1b
    iget-wide v0, v0, Le9/c;->h:D

    goto/32 :goto_17

    nop

    :goto_1c
    invoke-static {p2, p3}, Le9/a;->c(Lc9/a;Lc9/c;)Lj9/c$a;

    move-result-object p1

    goto/32 :goto_9

    nop

    :goto_1d
    invoke-static {p2, p3}, Le9/a;->b(Lc9/a;Lc9/c;)J

    move-result-wide p1

    goto/32 :goto_2

    nop

    :goto_1e
    iput-boolean p1, p0, Le9/b;->o:Z

    goto/32 :goto_6

    nop

    :goto_1f
    iget-object v0, p1, Lf9/c;->f:Le9/c;

    goto/32 :goto_1b

    nop

    :goto_20
    iput-object v0, p0, Le9/b;->a:Lh9/b;

    goto/32 :goto_3

    nop

    :goto_21
    iget-object v0, p1, Lf9/c;->f:Le9/c;

    goto/32 :goto_23

    nop

    :goto_22
    iget-object v0, p1, Lf9/c;->f:Le9/c;

    goto/32 :goto_10

    nop

    :goto_23
    iget-wide v0, v0, Le9/c;->c:J

    goto/32 :goto_24

    nop

    :goto_24
    iput-wide v0, p0, Le9/b;->h:J

    goto/32 :goto_16

    nop

    :goto_25
    iget-wide v0, v0, Le9/c;->d:J

    goto/32 :goto_b

    nop

    :goto_26
    iput-wide v0, p0, Le9/b;->k:D

    goto/32 :goto_e

    nop
.end method

.method c()V
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    iput-boolean v0, p0, Le9/b;->d:Z

    goto/32 :goto_2

    nop

    :goto_1
    const/4 v0, 0x0

    goto/32 :goto_0

    nop

    :goto_2
    iput v0, p0, Le9/b;->c:I

    goto/32 :goto_4

    nop

    :goto_3
    return-void

    :goto_4
    iput-boolean v0, p0, Le9/b;->o:Z

    goto/32 :goto_3

    nop
.end method

.method public d(B)V
    .locals 1

    iput-byte p1, p0, Le9/b;->e:B

    if-eqz p1, :cond_1

    const/4 v0, 0x2

    if-le p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    iput-boolean p1, p0, Le9/b;->d:Z

    return-void
.end method

.method e(Lf9/c;)V
    .locals 3

    goto/32 :goto_24

    nop

    :goto_0
    iget-object v0, p1, Lf9/c;->f:Le9/c;

    goto/32 :goto_23

    nop

    :goto_1
    iget-object v0, p1, Lf9/c;->f:Le9/c;

    goto/32 :goto_10

    nop

    :goto_2
    iput-wide v1, v0, Le9/c;->h:D

    goto/32 :goto_a

    nop

    :goto_3
    iget-wide v1, p0, Le9/b;->m:D

    goto/32 :goto_2

    nop

    :goto_4
    iput v0, p1, Lf9/c;->d:I

    goto/32 :goto_18

    nop

    :goto_5
    iget-byte v1, p0, Le9/b;->e:B

    goto/32 :goto_8

    nop

    :goto_6
    iput-boolean v0, p1, Lf9/c;->e:Z

    goto/32 :goto_11

    nop

    :goto_7
    iget-object v0, p1, Lf9/c;->f:Le9/c;

    goto/32 :goto_1b

    nop

    :goto_8
    iput-byte v1, v0, Le9/c;->a:B

    goto/32 :goto_7

    nop

    :goto_9
    iput-wide v1, v0, Le9/c;->b:J

    goto/32 :goto_1

    nop

    :goto_a
    iget-boolean v0, p0, Le9/b;->d:Z

    goto/32 :goto_6

    nop

    :goto_b
    iput-wide v1, v0, Le9/c;->g:D

    goto/32 :goto_15

    nop

    :goto_c
    iput v1, v0, Le9/c;->f:I

    goto/32 :goto_21

    nop

    :goto_d
    iget-wide v1, p0, Le9/b;->n:D

    goto/32 :goto_22

    nop

    :goto_e
    iget-wide v0, p0, Le9/b;->b:D

    goto/32 :goto_20

    nop

    :goto_f
    iget-wide v1, p0, Le9/b;->h:J

    goto/32 :goto_14

    nop

    :goto_10
    iget v1, p0, Le9/b;->j:I

    goto/32 :goto_c

    nop

    :goto_11
    iget-object v0, p1, Lf9/c;->f:Le9/c;

    goto/32 :goto_d

    nop

    :goto_12
    iget-wide v1, p0, Le9/b;->k:D

    goto/32 :goto_1e

    nop

    :goto_13
    return-void

    :goto_14
    iput-wide v1, v0, Le9/c;->c:J

    goto/32 :goto_1d

    nop

    :goto_15
    iget-object v0, p1, Lf9/c;->f:Le9/c;

    goto/32 :goto_3

    nop

    :goto_16
    iget-boolean v0, p0, Le9/b;->o:Z

    goto/32 :goto_19

    nop

    :goto_17
    iget-object v0, p1, Lf9/c;->f:Le9/c;

    goto/32 :goto_12

    nop

    :goto_18
    iget-object v0, p1, Lf9/c;->f:Le9/c;

    goto/32 :goto_5

    nop

    :goto_19
    iput-boolean v0, p1, Le9/c;->k:Z

    goto/32 :goto_1a

    nop

    :goto_1a
    invoke-virtual {p0}, Le9/b;->a()V

    goto/32 :goto_13

    nop

    :goto_1b
    iget-wide v1, p0, Le9/b;->g:J

    goto/32 :goto_9

    nop

    :goto_1c
    iput-wide v1, v0, Le9/c;->d:J

    goto/32 :goto_17

    nop

    :goto_1d
    iget-object v0, p1, Lf9/c;->f:Le9/c;

    goto/32 :goto_1f

    nop

    :goto_1e
    iput-wide v1, v0, Le9/c;->e:D

    goto/32 :goto_0

    nop

    :goto_1f
    iget-wide v1, p0, Le9/b;->i:J

    goto/32 :goto_1c

    nop

    :goto_20
    iput-wide v0, p1, Lf9/c;->c:D

    goto/32 :goto_25

    nop

    :goto_21
    iget-object v0, p1, Lf9/c;->f:Le9/c;

    goto/32 :goto_f

    nop

    :goto_22
    iput-wide v1, v0, Le9/c;->i:D

    goto/32 :goto_e

    nop

    :goto_23
    iget-wide v1, p0, Le9/b;->l:D

    goto/32 :goto_b

    nop

    :goto_24
    iget v0, p0, Le9/b;->c:I

    goto/32 :goto_4

    nop

    :goto_25
    iget-object p1, p1, Lf9/c;->f:Le9/c;

    goto/32 :goto_16

    nop
.end method
