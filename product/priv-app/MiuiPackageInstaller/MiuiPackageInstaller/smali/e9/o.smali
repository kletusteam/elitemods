.class public Le9/o;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Le9/o$c;,
        Le9/o$b;
    }
.end annotation


# instance fields
.field private a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lh9/b;",
            "Le9/o$b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Le9/o;->a:Ljava/util/Map;

    return-void
.end method

.method private a(Lh9/b;)Le9/o$b;
    .locals 2

    iget-object v0, p0, Le9/o;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Le9/o$b;

    if-nez v0, :cond_0

    new-instance v0, Le9/o$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Le9/o$b;-><init>(Le9/o$a;)V

    iget-object v1, p0, Le9/o;->a:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object v0
.end method


# virtual methods
.method public b(Lmiuix/animation/b;Lh9/b;D)V
    .locals 4

    invoke-direct {p0, p2}, Le9/o;->a(Lh9/b;)Le9/o$b;

    move-result-object v0

    iget-object v1, v0, Le9/o$b;->a:Lj9/i;

    const/4 v2, 0x1

    new-array v2, v2, [D

    const/4 v3, 0x0

    aput-wide p3, v2, v3

    invoke-virtual {v1, v2}, Lj9/i;->h([D)V

    iget-object p3, v0, Le9/o$b;->a:Lj9/i;

    invoke-virtual {p3, v3}, Lj9/i;->g(I)F

    move-result p3

    const/4 p4, 0x0

    cmpl-float p4, p3, p4

    if-eqz p4, :cond_0

    iget-object p4, v0, Le9/o$b;->b:Le9/o$c;

    invoke-virtual {p4, p1, p2}, Le9/o$c;->a(Lmiuix/animation/b;Lh9/b;)V

    float-to-double p3, p3

    invoke-virtual {p1, p2, p3, p4}, Lmiuix/animation/b;->t(Lh9/b;D)V

    :cond_0
    return-void
.end method
