.class Le9/g;
.super Ljava/lang/Object;


# static fields
.field static final a:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal<",
            "Le9/b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    sput-object v0, Le9/g;->a:Ljava/lang/ThreadLocal;

    return-void
.end method

.method static a(Le9/i;JJZZ)V
    .locals 20

    sget-object v0, Le9/g;->a:Ljava/lang/ThreadLocal;

    const-class v1, Le9/b;

    invoke-static {v0, v1}, Lj9/a;->e(Ljava/lang/ThreadLocal;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Le9/b;

    invoke-static {}, Lj9/f;->d()Z

    move-result v1

    iput-boolean v1, v0, Le9/b;->p:Z

    invoke-static {}, Le9/f;->m()Le9/f;

    move-result-object v1

    invoke-virtual {v1}, Le9/f;->l()J

    move-result-wide v10

    move-object/from16 v12, p0

    :goto_0
    if-eqz v12, :cond_6

    iget-object v1, v12, Le9/i;->b:Le9/h;

    const/4 v2, 0x0

    iput v2, v1, Le9/h;->d:I

    invoke-virtual {v1}, Le9/h;->c()Z

    move-result v1

    const/4 v13, 0x1

    xor-int/lit8 v14, v1, 0x1

    iget-object v1, v12, Le9/i;->d:Le9/q;

    iget-object v15, v1, Le9/q;->j:Ljava/util/List;

    iget-object v1, v12, Le9/i;->d:Le9/q;

    iget-object v1, v1, Le9/q;->c:Lmiuix/animation/b;

    instance-of v9, v1, Lmiuix/animation/ViewTarget;

    iget v1, v12, Le9/i;->c:I

    invoke-virtual {v12}, Le9/i;->c()I

    move-result v2

    add-int v7, v1, v2

    move v8, v1

    :goto_1
    if-ge v8, v7, :cond_5

    invoke-interface {v15, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lf9/c;

    if-nez v5, :cond_0

    move/from16 v17, v7

    move/from16 v18, v8

    move/from16 v19, v9

    goto/16 :goto_4

    :cond_0
    iget-object v1, v12, Le9/i;->d:Le9/q;

    iget-object v1, v1, Le9/q;->f:Lc9/a;

    iget-object v2, v5, Lf9/c;->a:Lh9/b;

    invoke-virtual {v2}, Landroid/util/Property;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lc9/a;->e(Ljava/lang/String;)Lc9/c;

    move-result-object v4

    iget-object v1, v12, Le9/i;->d:Le9/q;

    iget-object v1, v1, Le9/q;->f:Lc9/a;

    invoke-virtual {v0, v5, v1, v4}, Le9/b;->b(Lf9/c;Lc9/a;Lc9/c;)V

    if-eqz v14, :cond_1

    iget-object v3, v12, Le9/i;->d:Le9/q;

    move-object v1, v12

    move-object v2, v0

    move-object/from16 v16, v5

    move-wide/from16 v5, p1

    move/from16 v17, v7

    move/from16 v18, v8

    move-wide/from16 v7, p3

    invoke-static/range {v1 .. v8}, Le9/g;->j(Le9/i;Le9/b;Le9/q;Lc9/c;JJ)V

    goto :goto_2

    :cond_1
    move-object/from16 v16, v5

    move/from16 v17, v7

    move/from16 v18, v8

    :goto_2
    iget-byte v1, v0, Le9/b;->e:B

    if-ne v1, v13, :cond_2

    iget-object v3, v12, Le9/i;->d:Le9/q;

    move-object v1, v12

    move-object v2, v0

    move-wide/from16 v4, p1

    move-wide/from16 v6, p3

    invoke-static/range {v1 .. v7}, Le9/g;->k(Le9/i;Le9/b;Le9/q;JJ)V

    :cond_2
    iget-byte v1, v0, Le9/b;->e:B

    const/4 v2, 0x2

    if-ne v1, v2, :cond_3

    iget-object v3, v12, Le9/i;->d:Le9/q;

    move-object v1, v12

    move-object v2, v0

    move-wide/from16 v4, p1

    move-wide/from16 v6, p3

    move/from16 v19, v9

    move-wide v8, v10

    invoke-static/range {v1 .. v9}, Le9/g;->l(Le9/i;Le9/b;Le9/q;JJJ)V

    goto :goto_3

    :cond_3
    move/from16 v19, v9

    :goto_3
    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Le9/b;->e(Lf9/c;)V

    if-eqz p5, :cond_4

    if-eqz p6, :cond_4

    if-nez v19, :cond_4

    iget-wide v2, v0, Le9/b;->n:D

    invoke-static {v2, v3}, Le9/j;->e(D)Z

    move-result v2

    if-nez v2, :cond_4

    iget-object v2, v12, Le9/i;->d:Le9/q;

    iget-object v2, v2, Le9/q;->c:Lmiuix/animation/b;

    invoke-virtual {v1, v2}, Lf9/c;->f(Lmiuix/animation/b;)V

    :cond_4
    :goto_4
    add-int/lit8 v8, v18, 0x1

    move/from16 v7, v17

    move/from16 v9, v19

    goto/16 :goto_1

    :cond_5
    invoke-virtual {v12}, Lj9/e;->b()Lj9/e;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Le9/i;

    goto/16 :goto_0

    :cond_6
    return-void
.end method

.method private static b(Le9/b;F)D
    .locals 4

    iget-object v0, p0, Le9/b;->a:Lh9/b;

    invoke-static {v0}, Le9/g;->d(Lh9/b;)Landroid/animation/TypeEvaluator;

    move-result-object v0

    instance-of v1, v0, Landroid/animation/IntEvaluator;

    if-eqz v1, :cond_0

    check-cast v0, Landroid/animation/IntEvaluator;

    iget-wide v1, p0, Le9/b;->l:D

    double-to-int v1, v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-wide v2, p0, Le9/b;->m:D

    double-to-int p0, v2

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {v0, p1, v1, p0}, Landroid/animation/IntEvaluator;->evaluate(FLjava/lang/Integer;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/Integer;->doubleValue()D

    move-result-wide p0

    return-wide p0

    :cond_0
    check-cast v0, Landroid/animation/FloatEvaluator;

    iget-wide v1, p0, Le9/b;->l:D

    double-to-float v1, v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iget-wide v2, p0, Le9/b;->m:D

    double-to-float p0, v2

    invoke-static {p0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p0

    invoke-virtual {v0, p1, v1, p0}, Landroid/animation/FloatEvaluator;->evaluate(FLjava/lang/Number;Ljava/lang/Number;)Ljava/lang/Float;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/Float;->doubleValue()D

    move-result-wide p0

    return-wide p0
.end method

.method private static c(Le9/i;Le9/b;)V
    .locals 1

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Le9/b;->d(B)V

    iget-object p0, p0, Le9/i;->b:Le9/h;

    iget p1, p0, Le9/h;->c:I

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, Le9/h;->c:I

    return-void
.end method

.method private static d(Lh9/b;)Landroid/animation/TypeEvaluator;
    .locals 1

    sget-object v0, Lh9/i;->b:Lh9/i$b;

    if-ne p0, v0, :cond_0

    instance-of v0, p0, Lh9/a;

    if-eqz v0, :cond_0

    sget-object p0, Lj9/a;->a:Landroid/animation/ArgbEvaluator;

    return-object p0

    :cond_0
    instance-of p0, p0, Lh9/c;

    if-eqz p0, :cond_1

    new-instance p0, Landroid/animation/IntEvaluator;

    invoke-direct {p0}, Landroid/animation/IntEvaluator;-><init>()V

    return-object p0

    :cond_1
    new-instance p0, Landroid/animation/FloatEvaluator;

    invoke-direct {p0}, Landroid/animation/FloatEvaluator;-><init>()V

    return-object p0
.end method

.method private static e(Le9/i;Le9/b;JJ)Z
    .locals 5

    invoke-static {p1}, Le9/g;->i(Le9/b;)Z

    move-result v0

    const-string v1, ", value = "

    const-string v2, ", property = "

    const-string v3, "miuix_anim"

    const/4 v4, 0x0

    if-nez v0, :cond_1

    iget-boolean p2, p1, Le9/b;->p:Z

    if-eqz p2, :cond_0

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "StartTask, set start value failed, break, tag = "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p3, p0, Le9/i;->d:Le9/q;

    iget-object p3, p3, Le9/q;->e:Ljava/lang/Object;

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p3, p1, Le9/b;->a:Lh9/b;

    invoke-virtual {p3}, Landroid/util/Property;->getName()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p3, ", start value = "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide p3, p1, Le9/b;->l:D

    invoke-virtual {p2, p3, p4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string p3, ", target value = "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide p3, p1, Le9/b;->m:D

    invoke-virtual {p2, p3, p4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide p3, p1, Le9/b;->n:D

    invoke-virtual {p2, p3, p4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {v3, p2}, Lj9/f;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-static {p0, p1}, Le9/g;->c(Le9/i;Le9/b;)V

    return v4

    :cond_1
    invoke-static {p1}, Le9/g;->f(Le9/b;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-boolean p2, p1, Le9/b;->p:Z

    if-eqz p2, :cond_2

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "StartTask, values invalid, break, tag = "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p3, p0, Le9/i;->d:Le9/q;

    iget-object p3, p3, Le9/q;->e:Ljava/lang/Object;

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p3, p1, Le9/b;->a:Lh9/b;

    invoke-virtual {p3}, Landroid/util/Property;->getName()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p3, ", startValue = "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide p3, p1, Le9/b;->l:D

    invoke-virtual {p2, p3, p4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string p3, ", targetValue = "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide p3, p1, Le9/b;->m:D

    invoke-virtual {p2, p3, p4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide p3, p1, Le9/b;->n:D

    invoke-virtual {p2, p3, p4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string p3, ", velocity = "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide p3, p1, Le9/b;->b:D

    invoke-virtual {p2, p3, p4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {v3, p2}, Lj9/f;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    invoke-virtual {p1}, Le9/b;->c()V

    iget-wide p2, p1, Le9/b;->l:D

    iput-wide p2, p1, Le9/b;->n:D

    invoke-static {p0, p1}, Le9/g;->c(Le9/i;Le9/b;)V

    return v4

    :cond_3
    sub-long/2addr p2, p4

    iput-wide p2, p1, Le9/b;->i:J

    iput v4, p1, Le9/b;->c:I

    const/4 p0, 0x2

    invoke-virtual {p1, p0}, Le9/b;->d(B)V

    const/4 p0, 0x1

    return p0
.end method

.method private static f(Le9/b;)Z
    .locals 4

    iget-wide v0, p0, Le9/b;->l:D

    iget-wide v2, p0, Le9/b;->m:D

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    iget-wide v0, p0, Le9/b;->b:D

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    const-wide v2, 0x4030aaaaa0000000L    # 16.66666603088379

    cmpg-double p0, v0, v2

    if-gez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private static g(F)F
    .locals 2

    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float v1, p0, v0

    if-lez v1, :cond_0

    return v0

    :cond_0
    const/4 v0, 0x0

    cmpg-float v1, p0, v0

    if-gez v1, :cond_1

    return v0

    :cond_1
    return p0
.end method

.method private static h(Le9/i;Le9/b;)V
    .locals 3

    const-wide/16 v0, 0x0

    iput-wide v0, p1, Le9/b;->k:D

    invoke-virtual {p1}, Le9/b;->c()V

    iget-boolean v0, p1, Le9/b;->p:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "+++++ start anim, target = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Le9/i;->d:Le9/q;

    iget-object v1, v1, Le9/q;->c:Lmiuix/animation/b;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", tag = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p0, p0, Le9/i;->d:Le9/q;

    iget-object p0, p0, Le9/q;->e:Ljava/lang/Object;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p0, ", property = "

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p0, p1, Le9/b;->a:Lh9/b;

    invoke-virtual {p0}, Landroid/util/Property;->getName()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, ", op = "

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-byte p0, p1, Le9/b;->e:B

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p0, ", ease = "

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p0, p1, Le9/b;->f:Lj9/c$a;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p0, ", delay = "

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p1, Le9/b;->g:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string p0, ", start value = "

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p1, Le9/b;->l:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string p0, ", target value = "

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p1, Le9/b;->m:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string p0, ", value = "

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p1, Le9/b;->n:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string p0, ", progress = "

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p1, Le9/b;->k:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string p0, ", velocity = "

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide p0, p1, Le9/b;->b:D

    invoke-virtual {v0, p0, p1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const/4 p1, 0x0

    new-array p1, p1, [Ljava/lang/Object;

    invoke-static {p0, p1}, Lj9/f;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private static i(Le9/b;)Z
    .locals 4

    iget-wide v0, p0, Le9/b;->n:D

    invoke-static {v0, v1}, Le9/j;->e(D)Z

    move-result v0

    const/4 v1, 0x1

    iget-wide v2, p0, Le9/b;->l:D

    if-nez v0, :cond_1

    invoke-static {v2, v3}, Le9/j;->e(D)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v2, p0, Le9/b;->n:D

    iput-wide v2, p0, Le9/b;->l:D

    :cond_0
    return v1

    :cond_1
    invoke-static {v2, v3}, Le9/j;->e(D)Z

    move-result v0

    if-nez v0, :cond_2

    iget-wide v2, p0, Le9/b;->l:D

    iput-wide v2, p0, Le9/b;->n:D

    return v1

    :cond_2
    const/4 p0, 0x0

    return p0
.end method

.method static j(Le9/i;Le9/b;Le9/q;Lc9/c;JJ)V
    .locals 5

    iget-wide v0, p1, Le9/b;->l:D

    invoke-static {v0, v1}, Le9/j;->e(D)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p1, Le9/b;->l:D

    iget-object v2, p2, Le9/q;->c:Lmiuix/animation/b;

    iget-object v3, p1, Le9/b;->a:Lh9/b;

    invoke-static {v2, v3, v0, v1}, Le9/j;->b(Lmiuix/animation/b;Lh9/b;D)D

    move-result-wide v0

    iput-wide v0, p1, Le9/b;->l:D

    :cond_0
    sub-long/2addr p4, p6

    iput-wide p4, p1, Le9/b;->h:J

    iget-object p6, p0, Le9/i;->b:Le9/h;

    iget p7, p6, Le9/h;->b:I

    const/4 v0, 0x1

    add-int/2addr p7, v0

    iput p7, p6, Le9/h;->b:I

    iget-byte p7, p1, Le9/b;->e:B

    const/4 v1, 0x2

    if-ne p7, v1, :cond_2

    iget-wide v1, p1, Le9/b;->g:J

    const-wide/16 v3, 0x0

    cmp-long p7, v1, v3

    if-lez p7, :cond_1

    goto :goto_0

    :cond_1
    iput-wide p4, p1, Le9/b;->i:J

    iput-wide v3, p1, Le9/b;->g:J

    iget p2, p6, Le9/h;->a:I

    sub-int/2addr p2, v0

    iput p2, p6, Le9/h;->a:I

    invoke-static {p0, p1}, Le9/g;->h(Le9/i;Le9/b;)V

    goto :goto_1

    :cond_2
    :goto_0
    invoke-virtual {p1, v0}, Le9/b;->d(B)V

    iget-object p0, p2, Le9/q;->f:Lc9/a;

    invoke-static {p0, p3}, Le9/a;->d(Lc9/a;Lc9/c;)F

    move-result p0

    const p2, 0x7f7fffff    # Float.MAX_VALUE

    cmpl-float p2, p0, p2

    if-eqz p2, :cond_3

    float-to-double p2, p0

    iput-wide p2, p1, Le9/b;->b:D

    :cond_3
    :goto_1
    return-void
.end method

.method static k(Le9/i;Le9/b;Le9/q;JJ)V
    .locals 6

    iget-wide v0, p1, Le9/b;->g:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    iget-boolean v0, p1, Le9/b;->p:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "StartTask, tag = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Le9/i;->d:Le9/q;

    iget-object v1, v1, Le9/q;->e:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", property = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p1, Le9/b;->a:Lh9/b;

    invoke-virtual {v1}, Landroid/util/Property;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", delay = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p1, Le9/b;->g:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", initTime = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p1, Le9/b;->h:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", totalT = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lj9/f;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    iget-wide v0, p1, Le9/b;->h:J

    iget-wide v2, p1, Le9/b;->g:J

    add-long/2addr v0, v2

    cmp-long v0, p3, v0

    if-gez v0, :cond_1

    return-void

    :cond_1
    iget-object p2, p2, Le9/q;->c:Lmiuix/animation/b;

    iget-object v0, p1, Le9/b;->a:Lh9/b;

    const-wide v1, 0x7fefffffffffffffL    # Double.MAX_VALUE

    invoke-static {p2, v0, v1, v2}, Le9/j;->b(Lmiuix/animation/b;Lh9/b;D)D

    move-result-wide v3

    cmpl-double p2, v3, v1

    if-eqz p2, :cond_2

    iput-wide v3, p1, Le9/b;->l:D

    :cond_2
    iget-object p2, p0, Le9/i;->b:Le9/h;

    iget v0, p2, Le9/h;->a:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p2, Le9/h;->a:I

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p3

    move-wide v4, p5

    invoke-static/range {v0 .. v5}, Le9/g;->e(Le9/i;Le9/b;JJ)Z

    move-result p2

    if-nez p2, :cond_3

    return-void

    :cond_3
    invoke-static {p0, p1}, Le9/g;->h(Le9/i;Le9/b;)V

    return-void
.end method

.method private static l(Le9/i;Le9/b;Le9/q;JJJ)V
    .locals 15

    move-object v0, p0

    move-object/from16 v9, p1

    move-object/from16 v1, p2

    iget-object v2, v0, Le9/i;->b:Le9/h;

    iget v3, v2, Le9/h;->d:I

    const/4 v10, 0x1

    add-int/2addr v3, v10

    iput v3, v2, Le9/h;->d:I

    iget v2, v9, Le9/b;->c:I

    add-int/2addr v2, v10

    iput v2, v9, Le9/b;->c:I

    iget-object v2, v9, Le9/b;->a:Lh9/b;

    sget-object v3, Lh9/i;->a:Lh9/i$c;

    if-eq v2, v3, :cond_1

    sget-object v3, Lh9/i;->b:Lh9/i$b;

    if-eq v2, v3, :cond_1

    instance-of v2, v2, Lh9/a;

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_0
    iget-object v1, v1, Le9/q;->c:Lmiuix/animation/b;

    move-object/from16 v2, p1

    move-wide/from16 v3, p3

    move-wide/from16 v5, p5

    move-wide/from16 v7, p7

    invoke-static/range {v1 .. v8}, Li9/b;->a(Lmiuix/animation/b;Le9/b;JJJ)V

    iget-object v1, v9, Le9/b;->f:Lj9/c$a;

    iget v1, v1, Lj9/c$a;->a:I

    invoke-static {v1}, Lj9/c;->f(I)Z

    move-result v1

    if-nez v1, :cond_2

    iget-wide v1, v9, Le9/b;->k:D

    double-to-float v1, v1

    invoke-static {v9, v1}, Le9/g;->b(Le9/b;F)D

    move-result-wide v1

    goto :goto_1

    :cond_1
    :goto_0
    iget-wide v11, v9, Le9/b;->l:D

    iget-wide v13, v9, Le9/b;->m:D

    const-wide/16 v2, 0x0

    iput-wide v2, v9, Le9/b;->l:D

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    iput-wide v2, v9, Le9/b;->m:D

    iget-wide v2, v9, Le9/b;->k:D

    iput-wide v2, v9, Le9/b;->n:D

    iget-object v1, v1, Le9/q;->c:Lmiuix/animation/b;

    move-object/from16 v2, p1

    move-wide/from16 v3, p3

    move-wide/from16 v5, p5

    move-wide/from16 v7, p7

    invoke-static/range {v1 .. v8}, Li9/b;->a(Lmiuix/animation/b;Le9/b;JJJ)V

    iget-wide v1, v9, Le9/b;->n:D

    double-to-float v1, v1

    invoke-static {v1}, Le9/g;->g(F)F

    move-result v1

    float-to-double v1, v1

    iput-wide v1, v9, Le9/b;->k:D

    iput-wide v11, v9, Le9/b;->l:D

    iput-wide v13, v9, Le9/b;->m:D

    sget-object v3, Lj9/a;->a:Landroid/animation/ArgbEvaluator;

    double-to-float v1, v1

    double-to-int v2, v11

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-wide v4, v9, Le9/b;->m:D

    double-to-int v4, v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v1, v2, v4}, Landroid/animation/ArgbEvaluator;->evaluate(FLjava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->doubleValue()D

    move-result-wide v1

    :goto_1
    iput-wide v1, v9, Le9/b;->n:D

    :cond_2
    iget-byte v1, v9, Le9/b;->e:B

    const/4 v2, 0x3

    if-ne v1, v2, :cond_3

    iput-boolean v10, v9, Le9/b;->o:Z

    iget-object v1, v0, Le9/i;->b:Le9/h;

    iget v2, v1, Le9/h;->f:I

    add-int/2addr v2, v10

    iput v2, v1, Le9/h;->f:I

    :cond_3
    iget-boolean v1, v9, Le9/b;->p:Z

    if-eqz v1, :cond_4

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "----- update anim, target = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, v0, Le9/i;->d:Le9/q;

    iget-object v2, v2, Le9/q;->c:Lmiuix/animation/b;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, ", info.id = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, v0, Le9/i;->d:Le9/q;

    iget v2, v2, Le9/q;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ", tag = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, v0, Le9/i;->d:Le9/q;

    iget-object v2, v2, Le9/q;->e:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, v0, Le9/i;->d:Le9/q;

    iget-object v0, v0, Le9/q;->e:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, ", property = "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, v9, Le9/b;->a:Lh9/b;

    invoke-virtual {v0}, Landroid/util/Property;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ", op = "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-byte v0, v9, Le9/b;->e:B

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, ", justEnd = "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v0, v9, Le9/b;->o:Z

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v0, ", init time = "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, v9, Le9/b;->h:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v0, ", start time = "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, v9, Le9/b;->i:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v0, ", start value = "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, v9, Le9/b;->l:D

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string v0, ", target value = "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, v9, Le9/b;->m:D

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string v0, ", value = "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, v9, Le9/b;->n:D

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string v0, ", progress = "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, v9, Le9/b;->k:D

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string v0, ", velocity = "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, v9, Le9/b;->b:D

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string v0, ", delta = "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-wide/from16 v2, p5

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lj9/f;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_4
    return-void
.end method
