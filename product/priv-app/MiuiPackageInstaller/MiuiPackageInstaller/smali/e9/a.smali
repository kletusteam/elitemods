.class public Le9/a;
.super Ljava/lang/Object;


# direct methods
.method public static a(FF)F
    .locals 2

    float-to-double v0, p0

    invoke-static {v0, v1}, Le9/j;->e(D)Z

    move-result v0

    if-eqz v0, :cond_0

    return p1

    :cond_0
    float-to-double v0, p1

    invoke-static {v0, v1}, Le9/j;->e(D)Z

    move-result v0

    if-eqz v0, :cond_1

    return p0

    :cond_1
    invoke-static {p0, p1}, Ljava/lang/Math;->max(FF)F

    move-result p0

    return p0
.end method

.method static b(Lc9/a;Lc9/c;)J
    .locals 2

    iget-wide v0, p0, Lc9/a;->a:J

    if-eqz p1, :cond_0

    iget-wide p0, p1, Lc9/a;->a:J

    goto :goto_0

    :cond_0
    const-wide/16 p0, 0x0

    :goto_0
    invoke-static {v0, v1, p0, p1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide p0

    return-wide p0
.end method

.method static c(Lc9/a;Lc9/c;)Lj9/c$a;
    .locals 1

    if-eqz p1, :cond_0

    iget-object p1, p1, Lc9/a;->d:Lj9/c$a;

    if-eqz p1, :cond_0

    sget-object v0, Lc9/a;->j:Lj9/c$a;

    if-eq p1, v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lc9/a;->d:Lj9/c$a;

    :goto_0
    if-nez p1, :cond_1

    sget-object p1, Lc9/a;->j:Lj9/c$a;

    :cond_1
    return-object p1
.end method

.method static d(Lc9/a;Lc9/c;)F
    .locals 2

    if-eqz p1, :cond_0

    iget v0, p1, Lc9/a;->c:F

    float-to-double v0, v0

    invoke-static {v0, v1}, Le9/j;->e(D)Z

    move-result v0

    if-nez v0, :cond_0

    iget p0, p1, Lc9/a;->c:F

    return p0

    :cond_0
    iget p0, p0, Lc9/a;->c:F

    return p0
.end method

.method static e(Lc9/a;Lc9/c;)I
    .locals 0

    iget p0, p0, Lc9/a;->f:I

    if-eqz p1, :cond_0

    iget p1, p1, Lc9/a;->f:I

    goto :goto_0

    :cond_0
    const/4 p1, -0x1

    :goto_0
    invoke-static {p0, p1}, Ljava/lang/Math;->max(II)I

    move-result p0

    return p0
.end method
