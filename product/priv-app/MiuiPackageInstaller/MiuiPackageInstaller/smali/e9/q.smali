.class Le9/q;
.super Lj9/e;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Le9/q$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lj9/e<",
        "Le9/q;",
        ">;"
    }
.end annotation


# static fields
.field public static final m:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Le9/q;",
            ">;"
        }
    .end annotation
.end field

.field private static final n:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field public final b:I

.field public final c:Lmiuix/animation/b;

.field public final d:Ljava/lang/Object;

.field public volatile e:Ljava/lang/Object;

.field public volatile f:Lc9/a;

.field public volatile g:Ld9/a;

.field public volatile h:Ld9/a;

.field public volatile i:J

.field public volatile j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lf9/c;",
            ">;"
        }
    .end annotation
.end field

.field public k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Le9/i;",
            ">;"
        }
    .end annotation
.end field

.field private final l:Le9/h;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Le9/q;->m:Ljava/util/Map;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    sput-object v0, Le9/q;->n:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method public constructor <init>(Lmiuix/animation/b;Ld9/a;Ld9/a;Lc9/b;)V
    .locals 2

    invoke-direct {p0}, Lj9/e;-><init>()V

    sget-object v0, Le9/q;->n:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    iput v0, p0, Le9/q;->b:I

    new-instance v1, Lc9/a;

    invoke-direct {v1}, Lc9/a;-><init>()V

    iput-object v1, p0, Le9/q;->f:Lc9/a;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Le9/q;->k:Ljava/util/List;

    new-instance v1, Le9/h;

    invoke-direct {v1}, Le9/h;-><init>()V

    iput-object v1, p0, Le9/q;->l:Le9/h;

    iput-object p1, p0, Le9/q;->c:Lmiuix/animation/b;

    invoke-direct {p0, p2}, Le9/q;->g(Ld9/a;)Ld9/a;

    move-result-object p2

    iput-object p2, p0, Le9/q;->g:Ld9/a;

    invoke-direct {p0, p3}, Le9/q;->g(Ld9/a;)Ld9/a;

    move-result-object p2

    iput-object p2, p0, Le9/q;->h:Ld9/a;

    iget-object p2, p0, Le9/q;->h:Ld9/a;

    invoke-virtual {p2}, Ld9/a;->l()Ljava/lang/Object;

    move-result-object p2

    iput-object p2, p0, Le9/q;->d:Ljava/lang/Object;

    iget-boolean v1, p3, Ld9/a;->c:Z

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    :cond_0
    iput-object p2, p0, Le9/q;->e:Ljava/lang/Object;

    const/4 p2, 0x0

    iput-object p2, p0, Le9/q;->j:Ljava/util/List;

    invoke-direct {p0}, Le9/q;->i()V

    iget-object p2, p0, Le9/q;->f:Lc9/a;

    invoke-virtual {p3}, Ld9/a;->g()Lc9/a;

    move-result-object p3

    invoke-virtual {p2, p3}, Lc9/a;->d(Lc9/a;)V

    if-eqz p4, :cond_1

    iget-object p2, p0, Le9/q;->f:Lc9/a;

    invoke-virtual {p4, p2}, Lc9/b;->c(Lc9/a;)V

    :cond_1
    invoke-virtual {p1}, Lmiuix/animation/b;->g()Lf9/a;

    move-result-object p1

    iget-object p2, p0, Le9/q;->e:Ljava/lang/Object;

    iget-object p3, p0, Le9/q;->f:Lc9/a;

    invoke-virtual {p1, p2, p3}, Lf9/a;->a(Ljava/lang/Object;Lc9/a;)Z

    return-void
.end method

.method static d(Le9/i;Le9/h;Lf9/c;B)V
    .locals 3

    if-eqz p0, :cond_0

    const/4 v0, 0x1

    if-ne p3, v0, :cond_0

    iget-object p2, p2, Lf9/c;->f:Le9/c;

    iget-wide p2, p2, Le9/c;->b:J

    const-wide/16 v1, 0x0

    cmp-long p2, p2, v1

    if-lez p2, :cond_0

    iget-object p0, p0, Le9/i;->b:Le9/h;

    iget p2, p0, Le9/h;->a:I

    if-lez p2, :cond_0

    sub-int/2addr p2, v0

    iput p2, p0, Le9/h;->a:I

    iget p0, p1, Le9/h;->a:I

    sub-int/2addr p0, v0

    iput p0, p1, Le9/h;->a:I

    :cond_0
    return-void
.end method

.method private g(Ld9/a;)Ld9/a;
    .locals 1

    if-eqz p1, :cond_0

    iget-boolean v0, p1, Ld9/a;->c:Z

    if-eqz v0, :cond_0

    new-instance v0, Ld9/a;

    invoke-direct {v0}, Ld9/a;-><init>()V

    invoke-virtual {v0, p1}, Ld9/a;->p(Ld9/a;)V

    return-object v0

    :cond_0
    return-object p1
.end method

.method private i()V
    .locals 5

    iget-object v0, p0, Le9/q;->g:Ld9/a;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Le9/q;->h:Ld9/a;

    invoke-virtual {v0}, Ld9/a;->n()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, Le9/q;->h:Ld9/a;

    invoke-virtual {v2, v1}, Ld9/a;->m(Ljava/lang/Object;)Lh9/b;

    move-result-object v1

    instance-of v2, v1, Lh9/a;

    if-nez v2, :cond_2

    goto :goto_0

    :cond_2
    iget-object v2, p0, Le9/q;->c:Lmiuix/animation/b;

    const-wide v3, 0x7fefffffffffffffL    # Double.MAX_VALUE

    invoke-static {v2, v1, v3, v4}, Le9/j;->c(Lmiuix/animation/b;Lh9/b;D)D

    move-result-wide v2

    invoke-static {v2, v3}, Le9/j;->e(D)Z

    move-result v2

    if-nez v2, :cond_3

    goto :goto_0

    :cond_3
    iget-object v2, p0, Le9/q;->g:Ld9/a;

    iget-object v3, p0, Le9/q;->c:Lmiuix/animation/b;

    invoke-virtual {v2, v3, v1}, Ld9/a;->f(Lmiuix/animation/b;Lh9/b;)D

    move-result-wide v2

    invoke-static {v2, v3}, Le9/j;->e(D)Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Le9/q;->c:Lmiuix/animation/b;

    check-cast v1, Lh9/a;

    double-to-int v2, v2

    invoke-virtual {v4, v1, v2}, Lmiuix/animation/b;->p(Lh9/c;I)V

    goto :goto_0

    :cond_4
    return-void
.end method


# virtual methods
.method public c(Lh9/b;)Z
    .locals 1

    iget-object v0, p0, Le9/q;->h:Ld9/a;

    invoke-virtual {v0, p1}, Ld9/a;->e(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public e()I
    .locals 1

    iget-object v0, p0, Le9/q;->h:Ld9/a;

    invoke-virtual {v0}, Ld9/a;->n()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    return v0
.end method

.method public f()Le9/h;
    .locals 3

    iget-object v0, p0, Le9/q;->l:Le9/h;

    invoke-virtual {v0}, Le9/h;->clear()V

    iget-object v0, p0, Le9/q;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Le9/i;

    iget-object v2, p0, Le9/q;->l:Le9/h;

    iget-object v1, v1, Le9/i;->b:Le9/h;

    invoke-virtual {v2, v1}, Le9/h;->a(Le9/h;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Le9/q;->l:Le9/h;

    return-object v0
.end method

.method public h(Le9/q$a;)V
    .locals 11

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iput-wide v0, p0, Le9/q;->i:J

    iget-object v0, p0, Le9/q;->g:Ld9/a;

    iget-object v1, p0, Le9/q;->h:Ld9/a;

    invoke-static {}, Lj9/f;->d()Z

    move-result v2

    const/4 v3, 0x0

    if-eqz v2, :cond_0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "-- doSetup, id = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v5, p0, Le9/q;->b:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v5, ", key = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, p0, Le9/q;->e:Ljava/lang/Object;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, p0, Le9/q;->e:Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/Object;->hashCode()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v5, ", startTime = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v5, p0, Le9/q;->i:J

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v5, ", target = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, p0, Le9/q;->c:Lmiuix/animation/b;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v5, ", f = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v5, ", t = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v5, "\nconfig = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, p0, Le9/q;->f:Lc9/a;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-array v5, v3, [Ljava/lang/Object;

    invoke-static {v4, v5}, Lj9/f;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v1}, Ld9/a;->n()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v1, v6}, Ld9/a;->k(Ljava/lang/Object;)Lh9/b;

    move-result-object v6

    invoke-interface {p1, v6}, Le9/q$a;->a(Lh9/b;)Lf9/c;

    move-result-object v7

    if-nez v7, :cond_2

    goto :goto_0

    :cond_2
    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v8, v7, Lf9/c;->f:Le9/c;

    iget-object v9, p0, Le9/q;->c:Lmiuix/animation/b;

    invoke-virtual {v1, v9, v6}, Ld9/a;->f(Lmiuix/animation/b;Lh9/b;)D

    move-result-wide v9

    iput-wide v9, v8, Le9/c;->h:D

    iget-object v8, v7, Lf9/c;->f:Le9/c;

    if-eqz v0, :cond_3

    iget-object v9, p0, Le9/q;->c:Lmiuix/animation/b;

    invoke-virtual {v0, v9, v6}, Ld9/a;->f(Lmiuix/animation/b;Lh9/b;)D

    move-result-wide v9

    iput-wide v9, v8, Le9/c;->g:D

    goto :goto_1

    :cond_3
    iget-wide v8, v8, Le9/c;->g:D

    iget-object v10, p0, Le9/q;->c:Lmiuix/animation/b;

    invoke-static {v10, v6, v8, v9}, Le9/j;->c(Lmiuix/animation/b;Lh9/b;D)D

    move-result-wide v8

    invoke-static {v8, v9}, Le9/j;->e(D)Z

    move-result v10

    if-nez v10, :cond_4

    iget-object v10, v7, Lf9/c;->f:Le9/c;

    iput-wide v8, v10, Le9/c;->g:D

    :cond_4
    :goto_1
    invoke-static {v7}, Le9/j;->d(Lf9/c;)Z

    if-eqz v2, :cond_1

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "-- doSetup, target = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v9, p0, Le9/q;->c:Lmiuix/animation/b;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v9, ", property = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Landroid/util/Property;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, ", startValue = "

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, v7, Lf9/c;->f:Le9/c;

    iget-wide v9, v6, Le9/c;->g:D

    invoke-virtual {v8, v9, v10}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string v6, ", targetValue = "

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, v7, Lf9/c;->f:Le9/c;

    iget-wide v9, v6, Le9/c;->h:D

    invoke-virtual {v8, v9, v10}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string v6, ", value = "

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, v7, Lf9/c;->f:Le9/c;

    iget-wide v6, v6, Le9/c;->i:D

    invoke-virtual {v8, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    new-array v7, v3, [Ljava/lang/Object;

    invoke-static {v6, v7}, Lj9/f;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_5
    iput-object v4, p0, Le9/q;->j:Ljava/util/List;

    return-void
.end method

.method public j(Z)V
    .locals 6

    iget-object v0, p0, Le9/q;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    div-int/lit16 v1, v0, 0xfa0

    const/4 v2, 0x1

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    int-to-float v2, v0

    int-to-float v3, v1

    div-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    iget-object v3, p0, Le9/q;->k:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-le v3, v1, :cond_0

    iget-object v3, p0, Le9/q;->k:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    invoke-interface {v3, v1, v4}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->clear()V

    goto :goto_1

    :cond_0
    iget-object v3, p0, Le9/q;->k:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    :goto_0
    if-ge v3, v1, :cond_1

    iget-object v4, p0, Le9/q;->k:Ljava/util/List;

    new-instance v5, Le9/i;

    invoke-direct {v5}, Le9/i;-><init>()V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    const/4 v1, 0x0

    iget-object v3, p0, Le9/q;->k:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Le9/i;

    iput-object p0, v4, Le9/i;->d:Le9/q;

    add-int v5, v1, v2

    if-le v5, v0, :cond_2

    sub-int v5, v0, v1

    goto :goto_3

    :cond_2
    move v5, v2

    :goto_3
    invoke-virtual {v4, v1, v5}, Le9/i;->f(II)V

    if-eqz p1, :cond_3

    iget-object v4, v4, Le9/i;->b:Le9/h;

    iput v5, v4, Le9/h;->a:I

    goto :goto_4

    :cond_3
    invoke-virtual {v4}, Le9/i;->h()V

    :goto_4
    add-int/2addr v1, v5

    goto :goto_2

    :cond_4
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TransitionInfo{id = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Le9/q;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", key = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Le9/q;->e:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Le9/q;->e:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", startTime = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Le9/q;->i:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", target = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Le9/q;->c:Lmiuix/animation/b;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lmiuix/animation/b;->h()Ljava/lang/Object;

    move-result-object v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", propSize = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Le9/q;->h:Ld9/a;

    invoke-virtual {v1}, Ld9/a;->n()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", next = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lj9/e;->a:Lj9/e;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
