.class public Le9/k;
.super Ljava/lang/Object;


# instance fields
.field a:Lf9/a;

.field b:Lf9/a;

.field c:Lmiuix/animation/b;

.field private d:Lc9/a;


# direct methods
.method public constructor <init>(Lmiuix/animation/b;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lc9/a;

    invoke-direct {v0}, Lc9/a;-><init>()V

    iput-object v0, p0, Le9/k;->d:Lc9/a;

    iput-object p1, p0, Le9/k;->c:Lmiuix/animation/b;

    new-instance v0, Lf9/a;

    invoke-direct {v0, p1}, Lf9/a;-><init>(Lmiuix/animation/b;)V

    iput-object v0, p0, Le9/k;->a:Lf9/a;

    new-instance v0, Lf9/a;

    invoke-direct {v0, p1}, Lf9/a;-><init>(Lmiuix/animation/b;)V

    iput-object v0, p0, Le9/k;->b:Lf9/a;

    return-void
.end method


# virtual methods
.method public a()Lf9/a;
    .locals 1

    iget-object v0, p0, Le9/k;->b:Lf9/a;

    return-object v0
.end method

.method public b(Ld9/a;Lc9/b;)V
    .locals 2

    if-nez p2, :cond_0

    return-void

    :cond_0
    invoke-virtual {p1}, Ld9/a;->l()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Le9/k;->d:Lc9/a;

    invoke-virtual {p1}, Ld9/a;->g()Lc9/a;

    move-result-object p1

    invoke-virtual {v1, p1}, Lc9/a;->d(Lc9/a;)V

    iget-object p1, p0, Le9/k;->d:Lc9/a;

    invoke-virtual {p2, p1}, Lc9/b;->c(Lc9/a;)V

    iget-object p1, p0, Le9/k;->a:Lf9/a;

    iget-object p2, p0, Le9/k;->d:Lc9/a;

    invoke-virtual {p1, v0, p2}, Lf9/a;->a(Ljava/lang/Object;Lc9/a;)Z

    move-result p1

    if-nez p1, :cond_1

    :goto_0
    iget-object p1, p0, Le9/k;->d:Lc9/a;

    invoke-virtual {p1}, Lc9/a;->c()V

    return-void

    :cond_1
    iget-object p1, p0, Le9/k;->a:Lf9/a;

    invoke-virtual {p1, v0, v0}, Lf9/a;->d(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object p1, p0, Le9/k;->c:Lmiuix/animation/b;

    iget-object p1, p1, Lmiuix/animation/b;->b:Le9/d;

    iget-object p1, p1, Le9/d;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p1}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object p1

    iget-object p2, p0, Le9/k;->a:Lf9/a;

    invoke-virtual {p2, v0, v0, p1}, Lf9/a;->i(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Collection;)V

    iget-object p2, p0, Le9/k;->a:Lf9/a;

    invoke-virtual {p2, v0, v0, p1}, Lf9/a;->k(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Collection;)V

    iget-object p2, p0, Le9/k;->a:Lf9/a;

    invoke-virtual {p2, v0, v0, p1}, Lf9/a;->j(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Collection;)V

    iget-object p1, p0, Le9/k;->a:Lf9/a;

    invoke-virtual {p1, v0, v0}, Lf9/a;->f(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object p1, p0, Le9/k;->a:Lf9/a;

    invoke-virtual {p1, v0}, Lf9/a;->m(Ljava/lang/Object;)V

    goto :goto_0
.end method
