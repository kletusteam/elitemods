.class Le9/h;
.super Ljava/lang/Object;

# interfaces
.implements Lj9/g$c;


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field public g:I


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Le9/h;)V
    .locals 2

    iget v0, p0, Le9/h;->g:I

    iget v1, p1, Le9/h;->g:I

    add-int/2addr v0, v1

    iput v0, p0, Le9/h;->g:I

    iget v0, p0, Le9/h;->a:I

    iget v1, p1, Le9/h;->a:I

    add-int/2addr v0, v1

    iput v0, p0, Le9/h;->a:I

    iget v0, p0, Le9/h;->b:I

    iget v1, p1, Le9/h;->b:I

    add-int/2addr v0, v1

    iput v0, p0, Le9/h;->b:I

    iget v0, p0, Le9/h;->c:I

    iget v1, p1, Le9/h;->c:I

    add-int/2addr v0, v1

    iput v0, p0, Le9/h;->c:I

    iget v0, p0, Le9/h;->d:I

    iget v1, p1, Le9/h;->d:I

    add-int/2addr v0, v1

    iput v0, p0, Le9/h;->d:I

    iget v0, p0, Le9/h;->e:I

    iget v1, p1, Le9/h;->e:I

    add-int/2addr v0, v1

    iput v0, p0, Le9/h;->e:I

    iget v0, p0, Le9/h;->f:I

    iget p1, p1, Le9/h;->f:I

    add-int/2addr v0, p1

    iput v0, p0, Le9/h;->f:I

    return-void
.end method

.method public b()Z
    .locals 2

    invoke-virtual {p0}, Le9/h;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Le9/h;->e:I

    iget v1, p0, Le9/h;->f:I

    add-int/2addr v0, v1

    iget v1, p0, Le9/h;->c:I

    add-int/2addr v0, v1

    iget v1, p0, Le9/h;->g:I

    if-ge v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public c()Z
    .locals 1

    iget v0, p0, Le9/h;->b:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public clear()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Le9/h;->g:I

    iput v0, p0, Le9/h;->a:I

    iput v0, p0, Le9/h;->b:I

    iput v0, p0, Le9/h;->c:I

    iput v0, p0, Le9/h;->d:I

    iput v0, p0, Le9/h;->e:I

    iput v0, p0, Le9/h;->f:I

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AnimStats{animCount = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Le9/h;->g:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", startCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Le9/h;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", startedCount = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Le9/h;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", failCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Le9/h;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", updateCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Le9/h;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", cancelCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Le9/h;->e:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", endCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Le9/h;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
