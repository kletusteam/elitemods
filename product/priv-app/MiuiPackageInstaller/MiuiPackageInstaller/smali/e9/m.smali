.class Le9/m;
.super Landroid/os/Handler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Le9/m$b;
    }
.end annotation


# instance fields
.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lmiuix/animation/b;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lmiuix/animation/b;",
            "Le9/e;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lmiuix/animation/b;",
            "Le9/q;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Le9/i;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lmiuix/animation/b;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Le9/q;",
            ">;"
        }
    .end annotation
.end field

.field private g:Z

.field private h:J

.field private i:J

.field private j:I

.field private k:Z

.field private l:Z

.field private final m:[I


# direct methods
.method public constructor <init>(Landroid/os/Looper;)V
    .locals 2

    invoke-direct {p0, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance p1, Ljava/util/HashSet;

    invoke-direct {p1}, Ljava/util/HashSet;-><init>()V

    iput-object p1, p0, Le9/m;->a:Ljava/util/Set;

    new-instance p1, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {p1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object p1, p0, Le9/m;->b:Ljava/util/Map;

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Le9/m;->c:Ljava/util/Map;

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Le9/m;->d:Ljava/util/List;

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Le9/m;->e:Ljava/util/List;

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Le9/m;->f:Ljava/util/List;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Le9/m;->h:J

    iput-wide v0, p0, Le9/m;->i:J

    const/4 p1, 0x0

    iput p1, p0, Le9/m;->j:I

    const/4 p1, 0x2

    new-array p1, p1, [I

    iput-object p1, p0, Le9/m;->m:[I

    return-void
.end method

.method private a(Ljava/util/List;II)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Le9/q;",
            ">;II)V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Le9/q;

    iget-object v0, v0, Le9/q;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Le9/i;

    invoke-direct {p0}, Le9/m;->f()Le9/i;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v3, p0, Le9/m;->d:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v3, p3, :cond_1

    invoke-virtual {v2}, Le9/i;->d()I

    move-result v3

    invoke-virtual {v1}, Le9/i;->c()I

    move-result v4

    add-int/2addr v3, v4

    if-le v3, p2, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {v2, v1}, Lj9/e;->a(Lj9/e;)V

    goto :goto_0

    :cond_2
    :goto_1
    iget-object v2, p0, Le9/m;->d:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    return-void
.end method

.method private c(Lmiuix/animation/b;Lj9/e;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lj9/e;",
            ">(",
            "Lmiuix/animation/b;",
            "TT;",
            "Ljava/util/Map<",
            "Lmiuix/animation/b;",
            "TT;>;)V"
        }
    .end annotation

    invoke-interface {p3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lj9/e;

    if-nez v0, :cond_0

    invoke-interface {p3, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    invoke-virtual {v0, p2}, Lj9/e;->a(Lj9/e;)V

    :goto_0
    return-void
.end method

.method private static d(Le9/i;Le9/h;Lf9/c;Le9/e;)V
    .locals 5

    iget-object v0, p2, Lf9/c;->f:Le9/c;

    iget-byte v0, v0, Le9/c;->a:B

    invoke-static {v0}, Le9/i;->e(B)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-byte v1, p3, Le9/e;->b:B

    if-eqz v1, :cond_4

    iget-object v1, p3, Le9/e;->c:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v2, p2, Lf9/c;->a:Lh9/b;

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_0
    iget-object v1, p2, Lf9/c;->f:Le9/c;

    iget-byte v1, v1, Le9/c;->a:B

    invoke-static {v1}, Le9/i;->e(B)Z

    move-result v1

    if-eqz v1, :cond_4

    iget v1, p3, Le9/e;->e:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p3, Le9/e;->e:I

    iget-byte v1, p3, Le9/e;->b:B

    const/4 v2, 0x3

    if-ne v1, v2, :cond_2

    iget-object v1, p2, Lf9/c;->f:Le9/c;

    iget-wide v1, v1, Le9/c;->h:D

    const-wide v3, 0x7fefffffffffffffL    # Double.MAX_VALUE

    cmpl-double v1, v1, v3

    if-eqz v1, :cond_1

    iget-object v1, p2, Lf9/c;->f:Le9/c;

    iget-wide v2, v1, Le9/c;->h:D

    iput-wide v2, v1, Le9/c;->i:D

    :cond_1
    iget-object v1, p0, Le9/i;->b:Le9/h;

    iget v2, v1, Le9/h;->f:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Le9/h;->f:I

    iget v1, p1, Le9/h;->f:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p1, Le9/h;->f:I

    goto :goto_0

    :cond_2
    const/4 v2, 0x4

    if-ne v1, v2, :cond_3

    iget-object v1, p0, Le9/i;->b:Le9/h;

    iget v2, v1, Le9/h;->e:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Le9/h;->e:I

    iget v1, p1, Le9/h;->e:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p1, Le9/h;->e:I

    :cond_3
    :goto_0
    iget-byte p3, p3, Le9/e;->b:B

    invoke-virtual {p2, p3}, Lf9/c;->e(B)V

    invoke-static {p0, p1, p2, v0}, Le9/q;->d(Le9/i;Le9/h;Lf9/c;B)V

    :cond_4
    invoke-static {}, Lj9/f;->d()Z

    move-result p1

    if-eqz p1, :cond_5

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "---- RunnerHandler handleUpdate doSetOperation "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, " taskInfo "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p0, p0, Le9/i;->d:Le9/q;

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const/4 p1, 0x0

    new-array p1, p1, [Ljava/lang/Object;

    invoke-static {p0, p1}, Lj9/f;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_5
    return-void
.end method

.method private e()V
    .locals 4

    iget-object v0, p0, Le9/m;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Le9/q;

    iget-object v2, p0, Le9/m;->a:Ljava/util/Set;

    iget-object v3, v1, Le9/q;->c:Lmiuix/animation/b;

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-object v2, v1, Le9/q;->c:Lmiuix/animation/b;

    iget-object v2, v2, Lmiuix/animation/b;->b:Le9/d;

    invoke-virtual {v2, v1}, Le9/d;->r(Le9/q;)V

    invoke-virtual {v1}, Lj9/e;->b()Lj9/e;

    move-result-object v1

    check-cast v1, Le9/q;

    if-nez v1, :cond_0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Le9/m;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iget-boolean v0, p0, Le9/m;->l:Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    iput-boolean v0, p0, Le9/m;->l:Z

    invoke-static {}, Le9/f;->m()Le9/f;

    move-result-object v0

    invoke-virtual {v0}, Le9/f;->q()V

    :cond_2
    return-void
.end method

.method private f()Le9/i;
    .locals 5

    iget-object v0, p0, Le9/m;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    const v2, 0x7fffffff

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Le9/i;

    invoke-virtual {v3}, Le9/i;->d()I

    move-result v4

    if-ge v4, v2, :cond_0

    move-object v1, v3

    move v2, v4

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method private g()I
    .locals 3

    iget-object v0, p0, Le9/m;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiuix/animation/b;

    iget-object v2, v2, Lmiuix/animation/b;->b:Le9/d;

    invoke-virtual {v2}, Le9/d;->e()I

    move-result v2

    add-int/2addr v1, v2

    goto :goto_0

    :cond_0
    return v1
.end method

.method private static h(Le9/i;Le9/h;Lf9/c;)Z
    .locals 3

    invoke-static {p2}, Le9/j;->d(Lf9/c;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p0, 0x0

    return p0

    :cond_0
    iget-object v0, p2, Lf9/c;->f:Le9/c;

    iget-byte v0, v0, Le9/c;->a:B

    invoke-static {v0}, Le9/i;->e(B)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    iget-object v0, p0, Le9/i;->b:Le9/h;

    iget v2, v0, Le9/h;->e:I

    add-int/2addr v2, v1

    iput v2, v0, Le9/h;->e:I

    iget v0, p1, Le9/h;->e:I

    add-int/2addr v0, v1

    iput v0, p1, Le9/h;->e:I

    const/4 v0, 0x4

    invoke-virtual {p2, v0}, Lf9/c;->e(B)V

    iget-object v0, p2, Lf9/c;->f:Le9/c;

    iget-byte v0, v0, Le9/c;->a:B

    invoke-static {p0, p1, p2, v0}, Le9/q;->d(Le9/i;Le9/h;Lf9/c;B)V

    :cond_1
    return v1
.end method

.method private static i(Le9/q;Le9/e;Le9/h;)V
    .locals 8

    iget-object v0, p0, Le9/q;->c:Lmiuix/animation/b;

    iget-object v0, v0, Lmiuix/animation/b;->b:Le9/d;

    iget-object v0, v0, Le9/d;->b:Ljava/util/Set;

    iget-object v1, p0, Le9/q;->e:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    iget-object v1, p0, Le9/q;->k:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Le9/i;

    iget-object v3, p0, Le9/q;->j:Ljava/util/List;

    iget v4, v2, Le9/i;->c:I

    invoke-virtual {v2}, Le9/i;->c()I

    move-result v5

    add-int/2addr v5, v4

    :goto_0
    if-ge v4, v5, :cond_0

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lf9/c;

    if-eqz v6, :cond_1

    invoke-static {v2, p2, v6}, Le9/m;->h(Le9/i;Le9/h;Lf9/c;)Z

    move-result v7

    if-nez v7, :cond_1

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    invoke-static {v2, p2, v6, p1}, Le9/m;->d(Le9/i;Le9/h;Lf9/c;Le9/e;)V

    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_2
    if-nez v0, :cond_3

    iget-object p1, p0, Le9/q;->c:Lmiuix/animation/b;

    iget-object p1, p1, Lmiuix/animation/b;->b:Le9/d;

    iget-object p1, p1, Le9/d;->b:Ljava/util/Set;

    iget-object v0, p0, Le9/q;->e:Ljava/lang/Object;

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_3
    invoke-virtual {p2}, Le9/h;->b()Z

    move-result p1

    if-eqz p1, :cond_5

    iget p1, p2, Le9/h;->d:I

    if-lez p1, :cond_5

    iget-object p1, p0, Le9/q;->c:Lmiuix/animation/b;

    iget-object p1, p1, Lmiuix/animation/b;->b:Le9/d;

    iget-object p1, p1, Le9/d;->c:Ljava/util/Set;

    iget-object p2, p0, Le9/q;->e:Ljava/lang/Object;

    invoke-interface {p1, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_5

    sget-object p1, Le9/q;->m:Ljava/util/Map;

    iget p2, p0, Le9/q;->b:I

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-interface {p1, p2, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Lj9/f;->d()Z

    move-result p1

    const/4 p2, 0x0

    if-eqz p1, :cond_4

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "---- RunnerHandler handleUpdate ANIM_MSG_START_TAG "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-array v0, p2, [Ljava/lang/Object;

    invoke-static {p1, v0}, Lj9/f;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_4
    iget-object p1, p0, Le9/q;->c:Lmiuix/animation/b;

    iget-object p1, p1, Lmiuix/animation/b;->a:Le9/n;

    iget p0, p0, Le9/q;->b:I

    invoke-virtual {p1, p2, p0, p2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object p0

    invoke-virtual {p0}, Landroid/os/Message;->sendToTarget()V

    :cond_5
    return-void
.end method

.method private j(Le9/q;)Z
    .locals 2

    iget-object v0, p0, Le9/m;->c:Ljava/util/Map;

    iget-object v1, p1, Le9/q;->c:Lmiuix/animation/b;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    :goto_0
    check-cast v0, Le9/q;

    if-eqz v0, :cond_1

    if-ne v0, p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    iget-object v0, v0, Lj9/e;->a:Lj9/e;

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    return p1
.end method

.method private k(Le9/m$b;)V
    .locals 6

    iget-object v0, p1, Le9/m$b;->a:Lmiuix/animation/b;

    instance-of v0, v0, Lmiuix/animation/ViewTarget;

    iget-object v1, p1, Le9/m$b;->b:Ld9/a;

    invoke-virtual {v1}, Ld9/a;->n()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p1, Le9/m$b;->b:Ld9/a;

    invoke-virtual {v3, v2}, Ld9/a;->k(Ljava/lang/Object;)Lh9/b;

    move-result-object v2

    iget-object v3, p1, Le9/m$b;->a:Lmiuix/animation/b;

    iget-object v3, v3, Lmiuix/animation/b;->b:Le9/d;

    iget-object v3, v3, Le9/d;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lf9/c;

    if-nez v3, :cond_1

    goto :goto_0

    :cond_1
    iget-object v4, p1, Le9/m$b;->b:Ld9/a;

    iget-object v5, p1, Le9/m$b;->a:Lmiuix/animation/b;

    invoke-virtual {v4, v5, v2}, Ld9/a;->f(Lmiuix/animation/b;Lh9/b;)D

    move-result-wide v4

    iget-object v2, v3, Lf9/c;->f:Le9/c;

    iput-wide v4, v2, Le9/c;->j:D

    if-nez v0, :cond_0

    iget-object v2, p1, Le9/m$b;->a:Lmiuix/animation/b;

    invoke-virtual {v3, v2}, Lf9/c;->f(Lmiuix/animation/b;)V

    goto :goto_0

    :cond_2
    iget-object v0, p1, Le9/m$b;->a:Lmiuix/animation/b;

    const/4 v1, 0x0

    new-array v1, v1, [Lh9/b;

    invoke-virtual {v0, v1}, Lmiuix/animation/b;->k([Lh9/b;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object p1, p1, Le9/m$b;->a:Lmiuix/animation/b;

    iget-object p1, p1, Lmiuix/animation/b;->b:Le9/d;

    iget-object p1, p1, Le9/d;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p1}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    :cond_3
    return-void
.end method

.method private l(JJZ)V
    .locals 9

    iget-object v0, p0, Le9/m;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Le9/m;->o()V

    return-void

    :cond_0
    iput-wide p1, p0, Le9/m;->h:J

    invoke-static {}, Le9/f;->m()Le9/f;

    move-result-object p1

    invoke-virtual {p1}, Le9/f;->l()J

    move-result-wide p1

    iget v0, p0, Le9/m;->j:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    const-wide/16 v2, 0x2

    mul-long/2addr v2, p1

    cmp-long v2, p3, v2

    if-lez v2, :cond_1

    move-wide p3, p1

    :cond_1
    iget-wide p1, p0, Le9/m;->i:J

    add-long/2addr p1, p3

    iput-wide p1, p0, Le9/m;->i:J

    add-int/2addr v0, v1

    iput v0, p0, Le9/m;->j:I

    invoke-direct {p0}, Le9/m;->g()I

    move-result p1

    iget-object p2, p0, Le9/m;->m:[I

    invoke-static {p1, p2}, Le9/p;->b(I[I)V

    iget-object p1, p0, Le9/m;->m:[I

    const/4 p2, 0x0

    aget p2, p1, p2

    aget p1, p1, v1

    iget-object v0, p0, Le9/m;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiuix/animation/b;

    iget-object v2, v2, Lmiuix/animation/b;->b:Le9/d;

    iget-object v3, p0, Le9/m;->f:Ljava/util/List;

    invoke-virtual {v2, v3}, Le9/d;->f(Ljava/util/List;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Le9/m;->f:Ljava/util/List;

    invoke-direct {p0, v0, p1, p2}, Le9/m;->a(Ljava/util/List;II)V

    iget-object p1, p0, Le9/m;->d:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p1

    xor-int/2addr p1, v1

    iput-boolean p1, p0, Le9/m;->g:Z

    sget-object p1, Le9/i;->h:Ljava/util/concurrent/atomic/AtomicInteger;

    iget-object p2, p0, Le9/m;->d:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    invoke-virtual {p1, p2}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    iget-object p1, p0, Le9/m;->d:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    move-object v3, p2

    check-cast v3, Le9/i;

    iget-wide v4, p0, Le9/m;->i:J

    move-wide v6, p3

    move v8, p5

    invoke-virtual/range {v3 .. v8}, Le9/i;->g(JJZ)V

    goto :goto_1

    :cond_3
    iget-object p1, p0, Le9/m;->f:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->clear()V

    iget-object p1, p0, Le9/m;->d:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->clear()V

    return-void
.end method

.method private n(Lmiuix/animation/b;)Z
    .locals 2

    iget-object p1, p1, Lmiuix/animation/b;->b:Le9/d;

    iget-object p1, p1, Le9/d;->g:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Le9/q;

    if-eqz p1, :cond_0

    iget-object v0, p1, Le9/q;->c:Lmiuix/animation/b;

    iget-object v1, p0, Le9/m;->c:Ljava/util/Map;

    invoke-direct {p0, v0, p1, v1}, Le9/m;->c(Lmiuix/animation/b;Lj9/e;Ljava/util/Map;)V

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method private o()V
    .locals 5

    iget-boolean v0, p0, Le9/m;->k:Z

    if-eqz v0, :cond_1

    invoke-static {}, Lj9/f;->d()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "total time = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v3, p0, Le9/m;->i:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v2, 0x1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "frame count = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v4, p0, Le9/m;->j:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    const-string v2, "-- stopAnimRunner"

    invoke-static {v2, v0}, Lj9/f;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    iput-boolean v1, p0, Le9/m;->k:Z

    iput-boolean v1, p0, Le9/m;->l:Z

    const-wide/16 v2, 0x0

    iput-wide v2, p0, Le9/m;->i:J

    iput v1, p0, Le9/m;->j:I

    invoke-static {}, Le9/f;->m()Le9/f;

    move-result-object v0

    invoke-virtual {v0}, Le9/f;->h()V

    :cond_1
    return-void
.end method

.method private p()V
    .locals 6

    const/4 v0, 0x0

    iput-boolean v0, p0, Le9/m;->g:Z

    iget-object v1, p0, Le9/m;->a:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    move v2, v0

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    const/4 v4, 0x1

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lmiuix/animation/b;

    iget-object v5, p0, Le9/m;->f:Ljava/util/List;

    invoke-direct {p0, v3, v5}, Le9/m;->q(Lmiuix/animation/b;Ljava/util/List;)Z

    move-result v5

    if-nez v5, :cond_1

    invoke-direct {p0, v3}, Le9/m;->n(Lmiuix/animation/b;)Z

    move-result v5

    if-eqz v5, :cond_0

    goto :goto_1

    :cond_0
    iget-object v4, p0, Le9/m;->e:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_1
    :goto_1
    move v2, v4

    :goto_2
    iget-object v3, p0, Le9/m;->f:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Le9/m;->a:Ljava/util/Set;

    iget-object v3, p0, Le9/m;->e:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    iget-object v1, p0, Le9/m;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    iget-object v1, p0, Le9/m;->c:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    invoke-static {}, Lj9/f;->d()Z

    move-result v1

    if-eqz v1, :cond_3

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "- updateAnim->doSetup"

    invoke-static {v1, v0}, Lj9/f;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_3
    invoke-direct {p0}, Le9/m;->e()V

    move v2, v4

    :cond_4
    if-nez v2, :cond_5

    invoke-direct {p0}, Le9/m;->o()V

    :cond_5
    return-void
.end method

.method private q(Lmiuix/animation/b;Ljava/util/List;)Z
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmiuix/animation/b;",
            "Ljava/util/List<",
            "Le9/q;",
            ">;)Z"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    iget-object v2, v1, Lmiuix/animation/b;->b:Le9/d;

    move-object/from16 v3, p2

    invoke-virtual {v2, v3}, Le9/d;->f(Ljava/util/List;)V

    iget-object v2, v0, Le9/m;->b:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Le9/e;

    invoke-interface/range {p2 .. p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    const/4 v5, 0x0

    move v6, v5

    move v7, v6

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    const/4 v9, 0x1

    if-eqz v8, :cond_7

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Le9/q;

    invoke-direct {v0, v8}, Le9/m;->j(Le9/q;)Z

    move-result v10

    if-eqz v10, :cond_0

    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    :cond_0
    if-eqz v2, :cond_1

    iget-wide v11, v8, Le9/q;->i:J

    iget-wide v13, v2, Le9/e;->d:J

    cmp-long v11, v11, v13

    if-lez v11, :cond_1

    add-int/lit8 v6, v6, 0x1

    const/4 v11, 0x0

    goto :goto_1

    :cond_1
    move-object v11, v2

    :goto_1
    invoke-virtual {v8}, Le9/q;->f()Le9/h;

    move-result-object v12

    invoke-virtual {v12}, Le9/h;->c()Z

    move-result v13

    if-eqz v13, :cond_2

    invoke-static {v8, v11, v12}, Le9/m;->i(Le9/q;Le9/e;Le9/h;)V

    :cond_2
    invoke-static {}, Lj9/f;->d()Z

    move-result v13

    if-eqz v13, :cond_4

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "---- updateAnim, target = "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v13, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x7

    new-array v14, v14, [Ljava/lang/Object;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "id = "

    invoke-virtual {v15, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v10, v8, Le9/q;->b:I

    invoke-virtual {v15, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v14, v5

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "key = "

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v15, v8, Le9/q;->e:Ljava/lang/Object;

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v14, v9

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "useOp = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x2

    aput-object v9, v14, v10

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "info.startTime = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v10, v8, Le9/q;->i:J

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x3

    aput-object v9, v14, v10

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "opInfo.time = "

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v15, v6

    if-eqz v2, :cond_3

    iget-wide v5, v2, Le9/e;->d:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    goto :goto_2

    :cond_3
    const/4 v5, 0x0

    :goto_2
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x4

    aput-object v5, v14, v6

    const/4 v5, 0x5

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "stats.isRunning = "

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v12}, Le9/h;->b()Z

    move-result v6

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v14, v5

    const/4 v5, 0x6

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "stats = "

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v14, v5

    invoke-static {v13, v14}, Lj9/f;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_3

    :cond_4
    move v15, v6

    const/4 v10, 0x3

    :goto_3
    invoke-virtual {v12}, Le9/h;->b()Z

    move-result v5

    if-nez v5, :cond_6

    iget-object v5, v1, Lmiuix/animation/b;->b:Le9/d;

    iget v6, v12, Le9/h;->e:I

    iget v9, v12, Le9/h;->f:I

    if-le v6, v9, :cond_5

    const/4 v6, 0x2

    const/4 v14, 0x4

    goto :goto_4

    :cond_5
    move v14, v10

    const/4 v6, 0x2

    :goto_4
    invoke-virtual {v5, v8, v6, v14}, Le9/d;->i(Le9/q;II)V

    goto :goto_5

    :cond_6
    add-int/lit8 v7, v7, 0x1

    :goto_5
    move v6, v15

    const/4 v5, 0x0

    goto/16 :goto_0

    :cond_7
    if-eqz v2, :cond_9

    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v4

    if-eq v6, v4, :cond_8

    invoke-virtual {v2}, Le9/e;->a()Z

    move-result v2

    if-eqz v2, :cond_9

    :cond_8
    iget-object v2, v0, Le9/m;->b:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_9
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->clear()V

    if-lez v7, :cond_a

    move v5, v9

    goto :goto_6

    :cond_a
    const/4 v5, 0x0

    :goto_6
    return v5
.end method


# virtual methods
.method public b(Lmiuix/animation/b;Ld9/a;)V
    .locals 2

    new-instance v0, Le9/m$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Le9/m$b;-><init>(Le9/m$a;)V

    iput-object p1, v0, Le9/m$b;->a:Lmiuix/animation/b;

    iget-boolean p1, p2, Ld9/a;->c:Z

    if-eqz p1, :cond_0

    new-instance p1, Ld9/a;

    invoke-direct {p1}, Ld9/a;-><init>()V

    iput-object p1, v0, Le9/m$b;->b:Ld9/a;

    invoke-virtual {p1, p2}, Ld9/a;->p(Ld9/a;)V

    goto :goto_0

    :cond_0
    iput-object p2, v0, Le9/m$b;->b:Ld9/a;

    :goto_0
    const/4 p1, 0x4

    invoke-virtual {p0, p1, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object p1

    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    return-void
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 9

    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eq v0, v2, :cond_5

    const/4 v3, 0x2

    if-eq v0, v3, :cond_4

    const/4 v3, 0x3

    if-eq v0, v3, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    goto/16 :goto_1

    :cond_0
    iget-object v0, p0, Le9/m;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    invoke-direct {p0}, Le9/m;->o()V

    goto :goto_1

    :cond_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Le9/m$b;

    invoke-direct {p0, v0}, Le9/m;->k(Le9/m$b;)V

    goto :goto_1

    :cond_2
    iget-boolean v0, p0, Le9/m;->l:Z

    if-eqz v0, :cond_7

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {}, Le9/f;->m()Le9/f;

    move-result-object v0

    invoke-virtual {v0}, Le9/f;->l()J

    move-result-wide v6

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    iget-boolean v0, p0, Le9/m;->k:Z

    if-nez v0, :cond_3

    iput-boolean v2, p0, Le9/m;->k:Z

    const-wide/16 v2, 0x0

    iput-wide v2, p0, Le9/m;->i:J

    iput v1, p0, Le9/m;->j:I

    :goto_0
    move-object v3, p0

    invoke-direct/range {v3 .. v8}, Le9/m;->l(JJZ)V

    goto :goto_1

    :cond_3
    iget-boolean v0, p0, Le9/m;->g:Z

    if-nez v0, :cond_7

    iget-wide v0, p0, Le9/m;->h:J

    sub-long v6, v4, v0

    goto :goto_0

    :cond_4
    invoke-direct {p0}, Le9/m;->p()V

    goto :goto_1

    :cond_5
    sget-object v0, Le9/q;->m:Ljava/util/Map;

    iget v2, p1, Landroid/os/Message;->arg1:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Le9/q;

    if-eqz v0, :cond_7

    iget-object v2, v0, Le9/q;->c:Lmiuix/animation/b;

    iget-object v3, p0, Le9/m;->c:Ljava/util/Map;

    invoke-direct {p0, v2, v0, v3}, Le9/m;->c(Lmiuix/animation/b;Lj9/e;Ljava/util/Map;)V

    iget-boolean v0, p0, Le9/m;->g:Z

    if-nez v0, :cond_7

    invoke-static {}, Lj9/f;->d()Z

    move-result v0

    if-eqz v0, :cond_6

    new-array v0, v1, [Ljava/lang/Object;

    const-string v1, "- ANIM_MSG_SETUP->doSetup"

    invoke-static {v1, v0}, Lj9/f;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_6
    invoke-direct {p0}, Le9/m;->e()V

    :cond_7
    :goto_1
    const/4 v0, 0x0

    iput-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    return-void
.end method

.method public m(Le9/e;)V
    .locals 2

    iget-object v0, p1, Le9/e;->a:Lmiuix/animation/b;

    const/4 v1, 0x0

    new-array v1, v1, [Lh9/b;

    invoke-virtual {v0, v1}, Lmiuix/animation/b;->k([Lh9/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iput-wide v0, p1, Le9/e;->d:J

    iget-object v0, p0, Le9/m;->b:Ljava/util/Map;

    iget-object v1, p1, Le9/e;->a:Lmiuix/animation/b;

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method
