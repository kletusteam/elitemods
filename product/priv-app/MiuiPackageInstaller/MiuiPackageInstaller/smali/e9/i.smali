.class public Le9/i;
.super Lj9/e;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lj9/e<",
        "Le9/i;",
        ">;",
        "Ljava/lang/Runnable;"
    }
.end annotation


# static fields
.field public static final h:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field public final b:Le9/h;

.field public volatile c:I

.field public volatile d:Le9/q;

.field public volatile e:J

.field public volatile f:J

.field public volatile g:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    sput-object v0, Le9/i;->h:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lj9/e;-><init>()V

    new-instance v0, Le9/h;

    invoke-direct {v0}, Le9/h;-><init>()V

    iput-object v0, p0, Le9/i;->b:Le9/h;

    return-void
.end method

.method public static e(B)Z
    .locals 2

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v1, 0x2

    if-ne p0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :cond_1
    :goto_0
    return v0
.end method


# virtual methods
.method public c()I
    .locals 1

    iget-object v0, p0, Le9/i;->b:Le9/h;

    iget v0, v0, Le9/h;->g:I

    return v0
.end method

.method public d()I
    .locals 3

    const/4 v0, 0x0

    move-object v1, p0

    :goto_0
    if-eqz v1, :cond_0

    iget-object v2, v1, Le9/i;->b:Le9/h;

    iget v2, v2, Le9/h;->g:I

    add-int/2addr v0, v2

    iget-object v1, v1, Lj9/e;->a:Lj9/e;

    check-cast v1, Le9/i;

    goto :goto_0

    :cond_0
    return v0
.end method

.method public f(II)V
    .locals 1

    iget-object v0, p0, Le9/i;->b:Le9/h;

    invoke-virtual {v0}, Le9/h;->clear()V

    iget-object v0, p0, Le9/i;->b:Le9/h;

    iput p2, v0, Le9/h;->g:I

    iput p1, p0, Le9/i;->c:I

    return-void
.end method

.method public g(JJZ)V
    .locals 0

    iput-wide p1, p0, Le9/i;->e:J

    iput-wide p3, p0, Le9/i;->f:J

    iput-boolean p5, p0, Le9/i;->g:Z

    invoke-static {p0}, Le9/p;->d(Ljava/lang/Runnable;)V

    return-void
.end method

.method h()V
    .locals 6

    goto/32 :goto_2d

    nop

    :goto_0
    add-int/2addr v5, v4

    goto/32 :goto_3b

    nop

    :goto_1
    goto :goto_e

    :goto_2
    goto/32 :goto_b

    nop

    :goto_3
    add-int/2addr v3, v4

    goto/32 :goto_2f

    nop

    :goto_4
    add-int/2addr v1, v2

    :goto_5
    goto/32 :goto_38

    nop

    :goto_6
    goto :goto_e

    :goto_7
    goto/32 :goto_c

    nop

    :goto_8
    const/4 v3, 0x4

    goto/32 :goto_3f

    nop

    :goto_9
    goto :goto_e

    :goto_a
    goto/32 :goto_34

    nop

    :goto_b
    iget-object v2, p0, Le9/i;->b:Le9/h;

    goto/32 :goto_3c

    nop

    :goto_c
    iget-object v2, p0, Le9/i;->b:Le9/h;

    goto/32 :goto_25

    nop

    :goto_d
    iput v3, v2, Le9/h;->a:I

    :goto_e
    goto/32 :goto_1c

    nop

    :goto_f
    iput v3, v2, Le9/h;->c:I

    goto/32 :goto_6

    nop

    :goto_10
    if-eqz v2, :cond_0

    goto/32 :goto_3e

    :cond_0
    goto/32 :goto_3d

    nop

    :goto_11
    goto :goto_e

    :goto_12
    goto/32 :goto_24

    nop

    :goto_13
    const/4 v3, 0x5

    goto/32 :goto_37

    nop

    :goto_14
    iget v3, v2, Le9/h;->c:I

    goto/32 :goto_1d

    nop

    :goto_15
    if-eq v3, v4, :cond_1

    goto/32 :goto_1b

    :cond_1
    goto/32 :goto_1a

    nop

    :goto_16
    check-cast v2, Lf9/c;

    goto/32 :goto_10

    nop

    :goto_17
    iget-byte v2, v2, Le9/c;->a:B

    goto/32 :goto_27

    nop

    :goto_18
    iget-object v3, v2, Lf9/c;->f:Le9/c;

    goto/32 :goto_26

    nop

    :goto_19
    iget-byte v3, v3, Le9/c;->a:B

    goto/32 :goto_31

    nop

    :goto_1a
    goto :goto_2

    :goto_1b
    goto/32 :goto_32

    nop

    :goto_1c
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_2a

    nop

    :goto_1d
    add-int/2addr v3, v4

    goto/32 :goto_f

    nop

    :goto_1e
    iget v2, v2, Le9/h;->g:I

    goto/32 :goto_4

    nop

    :goto_1f
    if-ne v2, v3, :cond_2

    goto/32 :goto_a

    :cond_2
    goto/32 :goto_8

    nop

    :goto_20
    iget v3, v2, Le9/h;->f:I

    goto/32 :goto_22

    nop

    :goto_21
    iget-object v2, p0, Le9/i;->d:Le9/q;

    goto/32 :goto_33

    nop

    :goto_22
    add-int/2addr v3, v4

    goto/32 :goto_23

    nop

    :goto_23
    iput v3, v2, Le9/h;->f:I

    goto/32 :goto_1

    nop

    :goto_24
    iget-object v2, p0, Le9/i;->b:Le9/h;

    goto/32 :goto_14

    nop

    :goto_25
    iget v3, v2, Le9/h;->e:I

    goto/32 :goto_3

    nop

    :goto_26
    iget-byte v3, v3, Le9/c;->a:B

    goto/32 :goto_15

    nop

    :goto_27
    const/4 v3, 0x3

    goto/32 :goto_1f

    nop

    :goto_28
    iget-object v2, v2, Lf9/c;->f:Le9/c;

    goto/32 :goto_17

    nop

    :goto_29
    iget v5, v3, Le9/h;->b:I

    goto/32 :goto_0

    nop

    :goto_2a
    goto/16 :goto_5

    :goto_2b
    goto/32 :goto_39

    nop

    :goto_2c
    add-int/2addr v3, v4

    goto/32 :goto_d

    nop

    :goto_2d
    iget v0, p0, Le9/i;->c:I

    goto/32 :goto_2e

    nop

    :goto_2e
    iget v1, p0, Le9/i;->c:I

    goto/32 :goto_35

    nop

    :goto_2f
    iput v3, v2, Le9/h;->e:I

    goto/32 :goto_9

    nop

    :goto_30
    if-nez v3, :cond_3

    goto/32 :goto_2

    :cond_3
    goto/32 :goto_18

    nop

    :goto_31
    const/4 v4, 0x1

    goto/32 :goto_30

    nop

    :goto_32
    iget-object v3, p0, Le9/i;->b:Le9/h;

    goto/32 :goto_29

    nop

    :goto_33
    iget-object v2, v2, Le9/q;->j:Ljava/util/List;

    goto/32 :goto_36

    nop

    :goto_34
    iget-object v2, p0, Le9/i;->b:Le9/h;

    goto/32 :goto_20

    nop

    :goto_35
    iget-object v2, p0, Le9/i;->b:Le9/h;

    goto/32 :goto_1e

    nop

    :goto_36
    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_16

    nop

    :goto_37
    if-ne v2, v3, :cond_4

    goto/32 :goto_12

    :cond_4
    goto/32 :goto_11

    nop

    :goto_38
    if-lt v0, v1, :cond_5

    goto/32 :goto_2b

    :cond_5
    goto/32 :goto_21

    nop

    :goto_39
    return-void

    :goto_3a
    iget-object v3, v2, Lf9/c;->f:Le9/c;

    goto/32 :goto_19

    nop

    :goto_3b
    iput v5, v3, Le9/h;->b:I

    goto/32 :goto_28

    nop

    :goto_3c
    iget v3, v2, Le9/h;->a:I

    goto/32 :goto_2c

    nop

    :goto_3d
    goto/16 :goto_e

    :goto_3e
    goto/32 :goto_3a

    nop

    :goto_3f
    if-ne v2, v3, :cond_6

    goto/32 :goto_7

    :cond_6
    goto/32 :goto_13

    nop
.end method

.method public run()V
    .locals 7

    :try_start_0
    iget-wide v1, p0, Le9/i;->e:J

    iget-wide v3, p0, Le9/i;->f:J

    const/4 v5, 0x1

    iget-boolean v6, p0, Le9/i;->g:Z

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Le9/g;->a(Le9/i;JJZZ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "miuix_anim"

    const-string v2, "doAnimationFrame failed"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_0
    sget-object v0, Le9/i;->h:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Le9/f;->h:Le9/m;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    return-void
.end method
