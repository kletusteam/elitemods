.class public Lh9/i$c;
.super Lh9/h;

# interfaces
.implements Lh9/c;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lh9/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lh9/h;",
        "Lh9/c<",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 1

    const-string v0, "foreground"

    invoke-direct {p0, v0}, Lh9/h;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method synthetic constructor <init>(Lh9/i$a;)V
    .locals 0

    invoke-direct {p0}, Lh9/i$c;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)I
    .locals 0

    check-cast p1, Landroid/view/View;

    invoke-virtual {p0, p1}, Lh9/i$c;->i(Landroid/view/View;)I

    move-result p1

    return p1
.end method

.method public bridge synthetic b(Ljava/lang/Object;I)V
    .locals 0

    check-cast p1, Landroid/view/View;

    invoke-virtual {p0, p1, p2}, Lh9/i$c;->k(Landroid/view/View;I)V

    return-void
.end method

.method public bridge synthetic d(Ljava/lang/Object;)F
    .locals 0

    check-cast p1, Landroid/view/View;

    invoke-virtual {p0, p1}, Lh9/i$c;->j(Landroid/view/View;)F

    move-result p1

    return p1
.end method

.method public bridge synthetic f(Ljava/lang/Object;F)V
    .locals 0

    check-cast p1, Landroid/view/View;

    invoke-virtual {p0, p1, p2}, Lh9/i$c;->l(Landroid/view/View;F)V

    return-void
.end method

.method public i(Landroid/view/View;)I
    .locals 1

    sget v0, Lmiuix/animation/m;->a:I

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object p1

    instance-of v0, p1, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public j(Landroid/view/View;)F
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public k(Landroid/view/View;I)V
    .locals 1

    sget v0, Lmiuix/animation/m;->a:I

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p1, v0, p2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    invoke-virtual {p1}, Landroid/view/View;->getForeground()Landroid/graphics/drawable/Drawable;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->invalidateSelf()V

    :cond_0
    return-void
.end method

.method public l(Landroid/view/View;F)V
    .locals 0

    return-void
.end method
