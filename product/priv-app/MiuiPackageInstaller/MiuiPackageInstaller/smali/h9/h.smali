.class public abstract Lh9/h;
.super Lh9/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lh9/b<",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field public static final b:Lh9/h;

.field public static final c:Lh9/h;

.field public static final d:Lh9/h;

.field public static final e:Lh9/h;

.field public static final f:Lh9/h;

.field public static final g:Lh9/h;

.field public static final h:Lh9/h;

.field public static final i:Lh9/h;

.field public static final j:Lh9/h;

.field public static final k:Lh9/h;

.field public static final l:Lh9/h;

.field public static final m:Lh9/h;

.field public static final n:Lh9/h;

.field public static final o:Lh9/h;

.field public static final p:Lh9/h;

.field public static final q:Lh9/h;

.field public static final r:Lh9/h;

.field public static final s:Lh9/h;

.field public static final t:Lh9/h;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lh9/h$k;

    const-string v1, "translationX"

    invoke-direct {v0, v1}, Lh9/h$k;-><init>(Ljava/lang/String;)V

    sput-object v0, Lh9/h;->b:Lh9/h;

    new-instance v0, Lh9/h$l;

    const-string v1, "translationY"

    invoke-direct {v0, v1}, Lh9/h$l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lh9/h;->c:Lh9/h;

    new-instance v0, Lh9/h$m;

    const-string v1, "translationZ"

    invoke-direct {v0, v1}, Lh9/h$m;-><init>(Ljava/lang/String;)V

    sput-object v0, Lh9/h;->d:Lh9/h;

    new-instance v0, Lh9/h$n;

    const-string v1, "scaleX"

    invoke-direct {v0, v1}, Lh9/h$n;-><init>(Ljava/lang/String;)V

    sput-object v0, Lh9/h;->e:Lh9/h;

    new-instance v0, Lh9/h$o;

    const-string v1, "scaleY"

    invoke-direct {v0, v1}, Lh9/h$o;-><init>(Ljava/lang/String;)V

    sput-object v0, Lh9/h;->f:Lh9/h;

    new-instance v0, Lh9/h$p;

    const-string v1, "rotation"

    invoke-direct {v0, v1}, Lh9/h$p;-><init>(Ljava/lang/String;)V

    sput-object v0, Lh9/h;->g:Lh9/h;

    new-instance v0, Lh9/h$q;

    const-string v1, "rotationX"

    invoke-direct {v0, v1}, Lh9/h$q;-><init>(Ljava/lang/String;)V

    sput-object v0, Lh9/h;->h:Lh9/h;

    new-instance v0, Lh9/h$r;

    const-string v1, "rotationY"

    invoke-direct {v0, v1}, Lh9/h$r;-><init>(Ljava/lang/String;)V

    sput-object v0, Lh9/h;->i:Lh9/h;

    new-instance v0, Lh9/h$s;

    const-string v1, "x"

    invoke-direct {v0, v1}, Lh9/h$s;-><init>(Ljava/lang/String;)V

    sput-object v0, Lh9/h;->j:Lh9/h;

    new-instance v0, Lh9/h$a;

    const-string v1, "y"

    invoke-direct {v0, v1}, Lh9/h$a;-><init>(Ljava/lang/String;)V

    sput-object v0, Lh9/h;->k:Lh9/h;

    new-instance v0, Lh9/h$b;

    const-string v1, "z"

    invoke-direct {v0, v1}, Lh9/h$b;-><init>(Ljava/lang/String;)V

    sput-object v0, Lh9/h;->l:Lh9/h;

    new-instance v0, Lh9/h$c;

    const-string v1, "height"

    invoke-direct {v0, v1}, Lh9/h$c;-><init>(Ljava/lang/String;)V

    sput-object v0, Lh9/h;->m:Lh9/h;

    new-instance v0, Lh9/h$d;

    const-string v1, "width"

    invoke-direct {v0, v1}, Lh9/h$d;-><init>(Ljava/lang/String;)V

    sput-object v0, Lh9/h;->n:Lh9/h;

    new-instance v0, Lh9/h$e;

    const-string v1, "alpha"

    invoke-direct {v0, v1}, Lh9/h$e;-><init>(Ljava/lang/String;)V

    sput-object v0, Lh9/h;->o:Lh9/h;

    new-instance v0, Lh9/h$f;

    const-string v1, "autoAlpha"

    invoke-direct {v0, v1}, Lh9/h$f;-><init>(Ljava/lang/String;)V

    sput-object v0, Lh9/h;->p:Lh9/h;

    new-instance v0, Lh9/h$g;

    const-string v1, "scrollX"

    invoke-direct {v0, v1}, Lh9/h$g;-><init>(Ljava/lang/String;)V

    sput-object v0, Lh9/h;->q:Lh9/h;

    new-instance v0, Lh9/h$h;

    const-string v1, "scrollY"

    invoke-direct {v0, v1}, Lh9/h$h;-><init>(Ljava/lang/String;)V

    sput-object v0, Lh9/h;->r:Lh9/h;

    new-instance v0, Lh9/h$i;

    const-string v1, "deprecated_foreground"

    invoke-direct {v0, v1}, Lh9/h$i;-><init>(Ljava/lang/String;)V

    sput-object v0, Lh9/h;->s:Lh9/h;

    new-instance v0, Lh9/h$j;

    const-string v1, "deprecated_background"

    invoke-direct {v0, v1}, Lh9/h$j;-><init>(Ljava/lang/String;)V

    sput-object v0, Lh9/h;->t:Lh9/h;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lh9/b;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic g(Landroid/view/View;)Z
    .locals 0

    invoke-static {p0}, Lh9/h;->h(Landroid/view/View;)Z

    move-result p0

    return p0
.end method

.method private static h(Landroid/view/View;)Z
    .locals 1

    sget v0, Lmiuix/animation/m;->b:I

    invoke-virtual {p0, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ViewProperty{mPropertyName=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lh9/b;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
