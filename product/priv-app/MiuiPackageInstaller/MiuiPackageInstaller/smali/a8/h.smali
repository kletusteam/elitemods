.class La8/h;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        La8/h$a;
    }
.end annotation


# direct methods
.method public static a(La8/j;Ll8/a;)La8/f;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "La8/j;",
            "Ll8/a<",
            "+TT;>;)",
            "La8/f<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "mode"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "initializer"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, La8/h$a;->a:[I

    invoke-virtual {p0}, Ljava/lang/Enum;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x1

    const/4 v1, 0x2

    if-eq p0, v0, :cond_2

    if-eq p0, v1, :cond_1

    const/4 v0, 0x3

    if-ne p0, v0, :cond_0

    new-instance p0, La8/w;

    invoke-direct {p0, p1}, La8/w;-><init>(Ll8/a;)V

    goto :goto_0

    :cond_0
    new-instance p0, La8/k;

    invoke-direct {p0}, La8/k;-><init>()V

    throw p0

    :cond_1
    new-instance p0, La8/p;

    invoke-direct {p0, p1}, La8/p;-><init>(Ll8/a;)V

    goto :goto_0

    :cond_2
    new-instance p0, La8/q;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, v1, v0}, La8/q;-><init>(Ll8/a;Ljava/lang/Object;ILm8/g;)V

    :goto_0
    return-object p0
.end method

.method public static b(Ll8/a;)La8/f;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ll8/a<",
            "+TT;>;)",
            "La8/f<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "initializer"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, La8/q;

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-direct {v0, p0, v1, v2, v1}, La8/q;-><init>(Ll8/a;Ljava/lang/Object;ILm8/g;)V

    return-object v0
.end method
