.class public final enum La8/j;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "La8/j;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:La8/j;

.field public static final enum b:La8/j;

.field public static final enum c:La8/j;

.field private static final synthetic d:[La8/j;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, La8/j;

    const-string v1, "SYNCHRONIZED"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, La8/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, La8/j;->a:La8/j;

    new-instance v0, La8/j;

    const-string v1, "PUBLICATION"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, La8/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, La8/j;->b:La8/j;

    new-instance v0, La8/j;

    const-string v1, "NONE"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, La8/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, La8/j;->c:La8/j;

    invoke-static {}, La8/j;->a()[La8/j;

    move-result-object v0

    sput-object v0, La8/j;->d:[La8/j;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method private static final synthetic a()[La8/j;
    .locals 3

    const/4 v0, 0x3

    new-array v0, v0, [La8/j;

    sget-object v1, La8/j;->a:La8/j;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, La8/j;->b:La8/j;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, La8/j;->c:La8/j;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)La8/j;
    .locals 1

    const-class v0, La8/j;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, La8/j;

    return-object p0
.end method

.method public static values()[La8/j;
    .locals 1

    sget-object v0, La8/j;->d:[La8/j;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [La8/j;

    return-object v0
.end method
