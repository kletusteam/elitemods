.class Lh3/a$b;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lh3/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "b"
.end annotation


# instance fields
.field private final a:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue<",
            "Lt2/d;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    invoke-static {v0}, Lq3/k;->e(I)Ljava/util/Queue;

    move-result-object v0

    iput-object v0, p0, Lh3/a$b;->a:Ljava/util/Queue;

    return-void
.end method


# virtual methods
.method declared-synchronized a(Ljava/nio/ByteBuffer;)Lt2/d;
    .locals 1

    goto/32 :goto_3

    nop

    :goto_0
    return-object p1

    :catchall_0
    move-exception p1

    goto/32 :goto_4

    nop

    :goto_1
    throw p1

    :goto_2
    monitor-exit p0

    goto/32 :goto_0

    nop

    :goto_3
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lh3/a$b;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lt2/d;

    if-nez v0, :cond_0

    new-instance v0, Lt2/d;

    invoke-direct {v0}, Lt2/d;-><init>()V

    :cond_0
    invoke-virtual {v0, p1}, Lt2/d;->p(Ljava/nio/ByteBuffer;)Lt2/d;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/32 :goto_2

    nop

    :goto_4
    monitor-exit p0

    goto/32 :goto_1

    nop
.end method

.method declared-synchronized b(Lt2/d;)V
    .locals 1

    goto/32 :goto_3

    nop

    :goto_0
    monitor-exit p0

    goto/32 :goto_2

    nop

    :goto_1
    return-void

    :catchall_0
    move-exception p1

    goto/32 :goto_0

    nop

    :goto_2
    throw p1

    :goto_3
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Lt2/d;->a()V

    iget-object v0, p0, Lh3/a$b;->a:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/32 :goto_4

    nop

    :goto_4
    monitor-exit p0

    goto/32 :goto_1

    nop
.end method
