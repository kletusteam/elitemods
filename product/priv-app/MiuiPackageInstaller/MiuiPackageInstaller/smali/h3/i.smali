.class public final Lh3/i;
.super Ljava/lang/Object;


# static fields
.field public static final a:Lu2/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lu2/g<",
            "Lu2/b;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Lu2/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lu2/g<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    sget-object v0, Lu2/b;->c:Lu2/b;

    const-string v1, "com.bumptech.glide.load.resource.gif.GifOptions.DecodeFormat"

    invoke-static {v1, v0}, Lu2/g;->f(Ljava/lang/String;Ljava/lang/Object;)Lu2/g;

    move-result-object v0

    sput-object v0, Lh3/i;->a:Lu2/g;

    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    const-string v1, "com.bumptech.glide.load.resource.gif.GifOptions.DisableAnimation"

    invoke-static {v1, v0}, Lu2/g;->f(Ljava/lang/String;Ljava/lang/Object;)Lu2/g;

    move-result-object v0

    sput-object v0, Lh3/i;->b:Lu2/g;

    return-void
.end method
