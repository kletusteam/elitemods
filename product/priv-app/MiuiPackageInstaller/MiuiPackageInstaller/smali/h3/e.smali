.class public Lh3/e;
.super Lf3/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf3/b<",
        "Lh3/c;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lh3/c;)V
    .locals 0

    invoke-direct {p0, p1}, Lf3/b;-><init>(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lf3/b;->a:Landroid/graphics/drawable/Drawable;

    check-cast v0, Lh3/c;

    invoke-virtual {v0}, Lh3/c;->stop()V

    iget-object v0, p0, Lf3/b;->a:Landroid/graphics/drawable/Drawable;

    check-cast v0, Lh3/c;

    invoke-virtual {v0}, Lh3/c;->k()V

    return-void
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lf3/b;->a:Landroid/graphics/drawable/Drawable;

    check-cast v0, Lh3/c;

    invoke-virtual {v0}, Lh3/c;->e()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->prepareToDraw()V

    return-void
.end method

.method public c()I
    .locals 1

    iget-object v0, p0, Lf3/b;->a:Landroid/graphics/drawable/Drawable;

    check-cast v0, Lh3/c;

    invoke-virtual {v0}, Lh3/c;->i()I

    move-result v0

    return v0
.end method

.method public e()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "Lh3/c;",
            ">;"
        }
    .end annotation

    const-class v0, Lh3/c;

    return-object v0
.end method
