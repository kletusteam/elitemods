.class Lh3/g;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lh3/g$d;,
        Lh3/g$a;,
        Lh3/g$c;,
        Lh3/g$b;
    }
.end annotation


# instance fields
.field private final a:Lt2/a;

.field private final b:Landroid/os/Handler;

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lh3/g$b;",
            ">;"
        }
    .end annotation
.end field

.field final d:Lcom/bumptech/glide/k;

.field private final e:Lx2/d;

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Lcom/bumptech/glide/j;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/bumptech/glide/j<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private j:Lh3/g$a;

.field private k:Z

.field private l:Lh3/g$a;

.field private m:Landroid/graphics/Bitmap;

.field private n:Lu2/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lu2/l<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private o:Lh3/g$a;

.field private p:Lh3/g$d;

.field private q:I

.field private r:I

.field private s:I


# direct methods
.method constructor <init>(Lcom/bumptech/glide/b;Lt2/a;IILu2/l;Landroid/graphics/Bitmap;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/bumptech/glide/b;",
            "Lt2/a;",
            "II",
            "Lu2/l<",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Landroid/graphics/Bitmap;",
            ")V"
        }
    .end annotation

    invoke-virtual {p1}, Lcom/bumptech/glide/b;->f()Lx2/d;

    move-result-object v1

    invoke-virtual {p1}, Lcom/bumptech/glide/b;->h()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/bumptech/glide/b;->t(Landroid/content/Context;)Lcom/bumptech/glide/k;

    move-result-object v2

    invoke-virtual {p1}, Lcom/bumptech/glide/b;->h()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/bumptech/glide/b;->t(Landroid/content/Context;)Lcom/bumptech/glide/k;

    move-result-object p1

    invoke-static {p1, p3, p4}, Lh3/g;->i(Lcom/bumptech/glide/k;II)Lcom/bumptech/glide/j;

    move-result-object v5

    const/4 v4, 0x0

    move-object v0, p0

    move-object v3, p2

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lh3/g;-><init>(Lx2/d;Lcom/bumptech/glide/k;Lt2/a;Landroid/os/Handler;Lcom/bumptech/glide/j;Lu2/l;Landroid/graphics/Bitmap;)V

    return-void
.end method

.method constructor <init>(Lx2/d;Lcom/bumptech/glide/k;Lt2/a;Landroid/os/Handler;Lcom/bumptech/glide/j;Lu2/l;Landroid/graphics/Bitmap;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lx2/d;",
            "Lcom/bumptech/glide/k;",
            "Lt2/a;",
            "Landroid/os/Handler;",
            "Lcom/bumptech/glide/j<",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Lu2/l<",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Landroid/graphics/Bitmap;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lh3/g;->c:Ljava/util/List;

    iput-object p2, p0, Lh3/g;->d:Lcom/bumptech/glide/k;

    if-nez p4, :cond_0

    new-instance p4, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object p2

    new-instance v0, Lh3/g$c;

    invoke-direct {v0, p0}, Lh3/g$c;-><init>(Lh3/g;)V

    invoke-direct {p4, p2, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    :cond_0
    iput-object p1, p0, Lh3/g;->e:Lx2/d;

    iput-object p4, p0, Lh3/g;->b:Landroid/os/Handler;

    iput-object p5, p0, Lh3/g;->i:Lcom/bumptech/glide/j;

    iput-object p3, p0, Lh3/g;->a:Lt2/a;

    invoke-virtual {p0, p6, p7}, Lh3/g;->o(Lu2/l;Landroid/graphics/Bitmap;)V

    return-void
.end method

.method private static g()Lu2/f;
    .locals 3

    new-instance v0, Lp3/d;

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-direct {v0, v1}, Lp3/d;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method private static i(Lcom/bumptech/glide/k;II)Lcom/bumptech/glide/j;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/bumptech/glide/k;",
            "II)",
            "Lcom/bumptech/glide/j<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/bumptech/glide/k;->m()Lcom/bumptech/glide/j;

    move-result-object p0

    sget-object v0, Lw2/j;->b:Lw2/j;

    invoke-static {v0}, Lm3/f;->m0(Lw2/j;)Lm3/f;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lm3/a;->k0(Z)Lm3/a;

    move-result-object v0

    check-cast v0, Lm3/f;

    invoke-virtual {v0, v1}, Lm3/a;->f0(Z)Lm3/a;

    move-result-object v0

    check-cast v0, Lm3/f;

    invoke-virtual {v0, p1, p2}, Lm3/a;->X(II)Lm3/a;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/bumptech/glide/j;->m0(Lm3/a;)Lcom/bumptech/glide/j;

    move-result-object p0

    return-object p0
.end method

.method private l()V
    .locals 5

    iget-boolean v0, p0, Lh3/g;->f:Z

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lh3/g;->g:Z

    if-eqz v0, :cond_0

    goto :goto_1

    :cond_0
    iget-boolean v0, p0, Lh3/g;->h:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_2

    iget-object v0, p0, Lh3/g;->o:Lh3/g$a;

    const/4 v2, 0x0

    if-nez v0, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v2

    :goto_0
    const-string v3, "Pending target must be null when starting from the first frame"

    invoke-static {v0, v3}, Lq3/j;->a(ZLjava/lang/String;)V

    iget-object v0, p0, Lh3/g;->a:Lt2/a;

    invoke-interface {v0}, Lt2/a;->i()V

    iput-boolean v2, p0, Lh3/g;->h:Z

    :cond_2
    iget-object v0, p0, Lh3/g;->o:Lh3/g$a;

    if-eqz v0, :cond_3

    const/4 v1, 0x0

    iput-object v1, p0, Lh3/g;->o:Lh3/g$a;

    invoke-virtual {p0, v0}, Lh3/g;->m(Lh3/g$a;)V

    return-void

    :cond_3
    iput-boolean v1, p0, Lh3/g;->g:Z

    iget-object v0, p0, Lh3/g;->a:Lt2/a;

    invoke-interface {v0}, Lt2/a;->e()I

    move-result v0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    int-to-long v3, v0

    add-long/2addr v1, v3

    iget-object v0, p0, Lh3/g;->a:Lt2/a;

    invoke-interface {v0}, Lt2/a;->c()V

    new-instance v0, Lh3/g$a;

    iget-object v3, p0, Lh3/g;->b:Landroid/os/Handler;

    iget-object v4, p0, Lh3/g;->a:Lt2/a;

    invoke-interface {v4}, Lt2/a;->a()I

    move-result v4

    invoke-direct {v0, v3, v4, v1, v2}, Lh3/g$a;-><init>(Landroid/os/Handler;IJ)V

    iput-object v0, p0, Lh3/g;->l:Lh3/g$a;

    iget-object v0, p0, Lh3/g;->i:Lcom/bumptech/glide/j;

    invoke-static {}, Lh3/g;->g()Lu2/f;

    move-result-object v1

    invoke-static {v1}, Lm3/f;->n0(Lu2/f;)Lm3/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/bumptech/glide/j;->m0(Lm3/a;)Lcom/bumptech/glide/j;

    move-result-object v0

    iget-object v1, p0, Lh3/g;->a:Lt2/a;

    invoke-virtual {v0, v1}, Lcom/bumptech/glide/j;->z0(Ljava/lang/Object;)Lcom/bumptech/glide/j;

    move-result-object v0

    iget-object v1, p0, Lh3/g;->l:Lh3/g$a;

    invoke-virtual {v0, v1}, Lcom/bumptech/glide/j;->t0(Ln3/h;)Ln3/h;

    :cond_4
    :goto_1
    return-void
.end method

.method private n()V
    .locals 2

    iget-object v0, p0, Lh3/g;->m:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lh3/g;->e:Lx2/d;

    invoke-interface {v1, v0}, Lx2/d;->d(Landroid/graphics/Bitmap;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lh3/g;->m:Landroid/graphics/Bitmap;

    :cond_0
    return-void
.end method

.method private p()V
    .locals 1

    iget-boolean v0, p0, Lh3/g;->f:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lh3/g;->f:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lh3/g;->k:Z

    invoke-direct {p0}, Lh3/g;->l()V

    return-void
.end method

.method private q()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lh3/g;->f:Z

    return-void
.end method


# virtual methods
.method a()V
    .locals 3

    goto/32 :goto_19

    nop

    :goto_0
    invoke-interface {v0}, Lt2/a;->clear()V

    goto/32 :goto_1a

    nop

    :goto_1
    iget-object v0, p0, Lh3/g;->o:Lh3/g$a;

    goto/32 :goto_7

    nop

    :goto_2
    if-nez v0, :cond_0

    goto/32 :goto_13

    :cond_0
    goto/32 :goto_10

    nop

    :goto_3
    iget-object v0, p0, Lh3/g;->l:Lh3/g$a;

    goto/32 :goto_2

    nop

    :goto_4
    iget-object v2, p0, Lh3/g;->d:Lcom/bumptech/glide/k;

    goto/32 :goto_1b

    nop

    :goto_5
    invoke-virtual {v2, v0}, Lcom/bumptech/glide/k;->o(Ln3/h;)V

    goto/32 :goto_12

    nop

    :goto_6
    iget-object v2, p0, Lh3/g;->d:Lcom/bumptech/glide/k;

    goto/32 :goto_18

    nop

    :goto_7
    if-nez v0, :cond_1

    goto/32 :goto_16

    :cond_1
    goto/32 :goto_6

    nop

    :goto_8
    invoke-interface {v0}, Ljava/util/List;->clear()V

    goto/32 :goto_17

    nop

    :goto_9
    return-void

    :goto_a
    iget-object v0, p0, Lh3/g;->j:Lh3/g$a;

    goto/32 :goto_b

    nop

    :goto_b
    const/4 v1, 0x0

    goto/32 :goto_f

    nop

    :goto_c
    invoke-direct {p0}, Lh3/g;->q()V

    goto/32 :goto_a

    nop

    :goto_d
    iput-object v1, p0, Lh3/g;->j:Lh3/g$a;

    :goto_e
    goto/32 :goto_3

    nop

    :goto_f
    if-nez v0, :cond_2

    goto/32 :goto_e

    :cond_2
    goto/32 :goto_4

    nop

    :goto_10
    iget-object v2, p0, Lh3/g;->d:Lcom/bumptech/glide/k;

    goto/32 :goto_5

    nop

    :goto_11
    iput-boolean v0, p0, Lh3/g;->k:Z

    goto/32 :goto_9

    nop

    :goto_12
    iput-object v1, p0, Lh3/g;->l:Lh3/g$a;

    :goto_13
    goto/32 :goto_1

    nop

    :goto_14
    iget-object v0, p0, Lh3/g;->a:Lt2/a;

    goto/32 :goto_0

    nop

    :goto_15
    iput-object v1, p0, Lh3/g;->o:Lh3/g$a;

    :goto_16
    goto/32 :goto_14

    nop

    :goto_17
    invoke-direct {p0}, Lh3/g;->n()V

    goto/32 :goto_c

    nop

    :goto_18
    invoke-virtual {v2, v0}, Lcom/bumptech/glide/k;->o(Ln3/h;)V

    goto/32 :goto_15

    nop

    :goto_19
    iget-object v0, p0, Lh3/g;->c:Ljava/util/List;

    goto/32 :goto_8

    nop

    :goto_1a
    const/4 v0, 0x1

    goto/32 :goto_11

    nop

    :goto_1b
    invoke-virtual {v2, v0}, Lcom/bumptech/glide/k;->o(Ln3/h;)V

    goto/32 :goto_d

    nop
.end method

.method b()Ljava/nio/ByteBuffer;
    .locals 1

    goto/32 :goto_3

    nop

    :goto_0
    return-object v0

    :goto_1
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asReadOnlyBuffer()Ljava/nio/ByteBuffer;

    move-result-object v0

    goto/32 :goto_0

    nop

    :goto_2
    invoke-interface {v0}, Lt2/a;->h()Ljava/nio/ByteBuffer;

    move-result-object v0

    goto/32 :goto_1

    nop

    :goto_3
    iget-object v0, p0, Lh3/g;->a:Lt2/a;

    goto/32 :goto_2

    nop
.end method

.method c()Landroid/graphics/Bitmap;
    .locals 1

    goto/32 :goto_5

    nop

    :goto_0
    invoke-virtual {v0}, Lh3/g$a;->l()Landroid/graphics/Bitmap;

    move-result-object v0

    goto/32 :goto_3

    nop

    :goto_1
    return-object v0

    :goto_2
    if-nez v0, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_0

    nop

    :goto_3
    goto :goto_7

    :goto_4
    goto/32 :goto_6

    nop

    :goto_5
    iget-object v0, p0, Lh3/g;->j:Lh3/g$a;

    goto/32 :goto_2

    nop

    :goto_6
    iget-object v0, p0, Lh3/g;->m:Landroid/graphics/Bitmap;

    :goto_7
    goto/32 :goto_1

    nop
.end method

.method d()I
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Lh3/g;->j:Lh3/g$a;

    goto/32 :goto_5

    nop

    :goto_1
    goto :goto_7

    :goto_2
    goto/32 :goto_6

    nop

    :goto_3
    iget v0, v0, Lh3/g$a;->e:I

    goto/32 :goto_1

    nop

    :goto_4
    return v0

    :goto_5
    if-nez v0, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_3

    nop

    :goto_6
    const/4 v0, -0x1

    :goto_7
    goto/32 :goto_4

    nop
.end method

.method e()Landroid/graphics/Bitmap;
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return-object v0

    :goto_1
    iget-object v0, p0, Lh3/g;->m:Landroid/graphics/Bitmap;

    goto/32 :goto_0

    nop
.end method

.method f()I
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return v0

    :goto_1
    iget-object v0, p0, Lh3/g;->a:Lt2/a;

    goto/32 :goto_2

    nop

    :goto_2
    invoke-interface {v0}, Lt2/a;->d()I

    move-result v0

    goto/32 :goto_0

    nop
.end method

.method h()I
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget v0, p0, Lh3/g;->s:I

    goto/32 :goto_1

    nop

    :goto_1
    return v0
.end method

.method j()I
    .locals 2

    goto/32 :goto_2

    nop

    :goto_0
    iget v1, p0, Lh3/g;->q:I

    goto/32 :goto_4

    nop

    :goto_1
    return v0

    :goto_2
    iget-object v0, p0, Lh3/g;->a:Lt2/a;

    goto/32 :goto_3

    nop

    :goto_3
    invoke-interface {v0}, Lt2/a;->f()I

    move-result v0

    goto/32 :goto_0

    nop

    :goto_4
    add-int/2addr v0, v1

    goto/32 :goto_1

    nop
.end method

.method k()I
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget v0, p0, Lh3/g;->r:I

    goto/32 :goto_1

    nop

    :goto_1
    return v0
.end method

.method m(Lh3/g$a;)V
    .locals 3

    goto/32 :goto_8

    nop

    :goto_0
    iput-object p1, p0, Lh3/g;->j:Lh3/g$a;

    goto/32 :goto_22

    nop

    :goto_1
    goto :goto_c

    :goto_2
    goto/32 :goto_b

    nop

    :goto_3
    goto :goto_16

    :goto_4
    goto/32 :goto_2e

    nop

    :goto_5
    return-void

    :goto_6
    const/4 v1, 0x2

    goto/32 :goto_17

    nop

    :goto_7
    if-gez p1, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_29

    nop

    :goto_8
    iget-object v0, p0, Lh3/g;->p:Lh3/g$d;

    goto/32 :goto_a

    nop

    :goto_9
    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    goto/32 :goto_1

    nop

    :goto_a
    if-nez v0, :cond_1

    goto/32 :goto_10

    :cond_1
    goto/32 :goto_f

    nop

    :goto_b
    iput-object p1, p0, Lh3/g;->o:Lh3/g$a;

    :goto_c
    goto/32 :goto_27

    nop

    :goto_d
    invoke-virtual {p1, v1, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object p1

    goto/32 :goto_20

    nop

    :goto_e
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    goto/32 :goto_15

    nop

    :goto_f
    invoke-interface {v0}, Lh3/g$d;->a()V

    :goto_10
    goto/32 :goto_32

    nop

    :goto_11
    invoke-virtual {p1}, Lh3/g$a;->l()Landroid/graphics/Bitmap;

    move-result-object v0

    goto/32 :goto_12

    nop

    :goto_12
    if-nez v0, :cond_2

    goto/32 :goto_21

    :cond_2
    goto/32 :goto_14

    nop

    :goto_13
    iget-object v0, p0, Lh3/g;->b:Landroid/os/Handler;

    goto/32 :goto_1f

    nop

    :goto_14
    invoke-direct {p0}, Lh3/g;->n()V

    goto/32 :goto_30

    nop

    :goto_15
    add-int/lit8 p1, p1, -0x1

    :goto_16
    goto/32 :goto_7

    nop

    :goto_17
    if-nez v0, :cond_3

    goto/32 :goto_1d

    :cond_3
    goto/32 :goto_26

    nop

    :goto_18
    check-cast v2, Lh3/g$b;

    goto/32 :goto_1b

    nop

    :goto_19
    iget-boolean v0, p0, Lh3/g;->f:Z

    goto/32 :goto_31

    nop

    :goto_1a
    iget-boolean v0, p0, Lh3/g;->k:Z

    goto/32 :goto_6

    nop

    :goto_1b
    invoke-interface {v2}, Lh3/g$b;->a()V

    goto/32 :goto_2a

    nop

    :goto_1c
    return-void

    :goto_1d
    goto/32 :goto_19

    nop

    :goto_1e
    if-nez v0, :cond_4

    goto/32 :goto_2

    :cond_4
    goto/32 :goto_13

    nop

    :goto_1f
    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object p1

    goto/32 :goto_9

    nop

    :goto_20
    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    :goto_21
    goto/32 :goto_23

    nop

    :goto_22
    iget-object p1, p0, Lh3/g;->c:Ljava/util/List;

    goto/32 :goto_e

    nop

    :goto_23
    invoke-direct {p0}, Lh3/g;->l()V

    goto/32 :goto_5

    nop

    :goto_24
    iget-object p1, p0, Lh3/g;->b:Landroid/os/Handler;

    goto/32 :goto_d

    nop

    :goto_25
    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    goto/32 :goto_1c

    nop

    :goto_26
    iget-object v0, p0, Lh3/g;->b:Landroid/os/Handler;

    goto/32 :goto_2c

    nop

    :goto_27
    return-void

    :goto_28
    goto/32 :goto_11

    nop

    :goto_29
    iget-object v2, p0, Lh3/g;->c:Ljava/util/List;

    goto/32 :goto_2b

    nop

    :goto_2a
    add-int/lit8 p1, p1, -0x1

    goto/32 :goto_3

    nop

    :goto_2b
    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_18

    nop

    :goto_2c
    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object p1

    goto/32 :goto_25

    nop

    :goto_2d
    iget-boolean v0, p0, Lh3/g;->h:Z

    goto/32 :goto_1e

    nop

    :goto_2e
    if-nez v0, :cond_5

    goto/32 :goto_21

    :cond_5
    goto/32 :goto_24

    nop

    :goto_2f
    iput-boolean v0, p0, Lh3/g;->g:Z

    goto/32 :goto_1a

    nop

    :goto_30
    iget-object v0, p0, Lh3/g;->j:Lh3/g$a;

    goto/32 :goto_0

    nop

    :goto_31
    if-eqz v0, :cond_6

    goto/32 :goto_28

    :cond_6
    goto/32 :goto_2d

    nop

    :goto_32
    const/4 v0, 0x0

    goto/32 :goto_2f

    nop
.end method

.method o(Lu2/l;Landroid/graphics/Bitmap;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lu2/l<",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Landroid/graphics/Bitmap;",
            ")V"
        }
    .end annotation

    goto/32 :goto_7

    nop

    :goto_0
    new-instance v1, Lm3/f;

    goto/32 :goto_5

    nop

    :goto_1
    iget-object v0, p0, Lh3/g;->i:Lcom/bumptech/glide/j;

    goto/32 :goto_0

    nop

    :goto_2
    return-void

    :goto_3
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result p1

    goto/32 :goto_d

    nop

    :goto_4
    check-cast v0, Lu2/l;

    goto/32 :goto_6

    nop

    :goto_5
    invoke-direct {v1}, Lm3/f;-><init>()V

    goto/32 :goto_a

    nop

    :goto_6
    iput-object v0, p0, Lh3/g;->n:Lu2/l;

    goto/32 :goto_e

    nop

    :goto_7
    invoke-static {p1}, Lq3/j;->d(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_4

    nop

    :goto_8
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result p1

    goto/32 :goto_11

    nop

    :goto_9
    iput p1, p0, Lh3/g;->q:I

    goto/32 :goto_3

    nop

    :goto_a
    invoke-virtual {v1, p1}, Lm3/a;->i0(Lu2/l;)Lm3/a;

    move-result-object p1

    goto/32 :goto_12

    nop

    :goto_b
    iput-object p1, p0, Lh3/g;->i:Lcom/bumptech/glide/j;

    goto/32 :goto_f

    nop

    :goto_c
    check-cast v0, Landroid/graphics/Bitmap;

    goto/32 :goto_10

    nop

    :goto_d
    iput p1, p0, Lh3/g;->r:I

    goto/32 :goto_8

    nop

    :goto_e
    invoke-static {p2}, Lq3/j;->d(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_c

    nop

    :goto_f
    invoke-static {p2}, Lq3/k;->g(Landroid/graphics/Bitmap;)I

    move-result p1

    goto/32 :goto_9

    nop

    :goto_10
    iput-object v0, p0, Lh3/g;->m:Landroid/graphics/Bitmap;

    goto/32 :goto_1

    nop

    :goto_11
    iput p1, p0, Lh3/g;->s:I

    goto/32 :goto_2

    nop

    :goto_12
    invoke-virtual {v0, p1}, Lcom/bumptech/glide/j;->m0(Lm3/a;)Lcom/bumptech/glide/j;

    move-result-object p1

    goto/32 :goto_b

    nop
.end method

.method r(Lh3/g$b;)V
    .locals 2

    goto/32 :goto_a

    nop

    :goto_0
    const-string v0, "Cannot subscribe to a cleared frame loader"

    goto/32 :goto_16

    nop

    :goto_1
    iget-object v1, p0, Lh3/g;->c:Ljava/util/List;

    goto/32 :goto_14

    nop

    :goto_2
    iget-object v0, p0, Lh3/g;->c:Ljava/util/List;

    goto/32 :goto_11

    nop

    :goto_3
    throw p1

    :goto_4
    goto/32 :goto_9

    nop

    :goto_5
    iget-object v0, p0, Lh3/g;->c:Ljava/util/List;

    goto/32 :goto_6

    nop

    :goto_6
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    goto/32 :goto_1

    nop

    :goto_7
    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_3

    nop

    :goto_8
    if-nez v0, :cond_0

    goto/32 :goto_13

    :cond_0
    goto/32 :goto_12

    nop

    :goto_9
    new-instance p1, Ljava/lang/IllegalStateException;

    goto/32 :goto_0

    nop

    :goto_a
    iget-boolean v0, p0, Lh3/g;->k:Z

    goto/32 :goto_15

    nop

    :goto_b
    const-string v0, "Cannot subscribe twice in a row"

    goto/32 :goto_7

    nop

    :goto_c
    if-eqz v0, :cond_1

    goto/32 :goto_e

    :cond_1
    goto/32 :goto_5

    nop

    :goto_d
    return-void

    :goto_e
    goto/32 :goto_10

    nop

    :goto_f
    throw p1

    :goto_10
    new-instance p1, Ljava/lang/IllegalStateException;

    goto/32 :goto_b

    nop

    :goto_11
    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto/32 :goto_c

    nop

    :goto_12
    invoke-direct {p0}, Lh3/g;->p()V

    :goto_13
    goto/32 :goto_d

    nop

    :goto_14
    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/32 :goto_8

    nop

    :goto_15
    if-eqz v0, :cond_2

    goto/32 :goto_4

    :cond_2
    goto/32 :goto_2

    nop

    :goto_16
    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_f

    nop
.end method

.method s(Lh3/g$b;)V
    .locals 1

    goto/32 :goto_5

    nop

    :goto_0
    invoke-direct {p0}, Lh3/g;->q()V

    :goto_1
    goto/32 :goto_3

    nop

    :goto_2
    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto/32 :goto_4

    nop

    :goto_3
    return-void

    :goto_4
    iget-object p1, p0, Lh3/g;->c:Ljava/util/List;

    goto/32 :goto_7

    nop

    :goto_5
    iget-object v0, p0, Lh3/g;->c:Ljava/util/List;

    goto/32 :goto_2

    nop

    :goto_6
    if-nez p1, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_0

    nop

    :goto_7
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p1

    goto/32 :goto_6

    nop
.end method
