.class public final Lc6/m;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity$b;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static synthetic b(Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity$a;Landroid/content/DialogInterface;)V
    .locals 0

    invoke-static {p0, p1}, Lc6/m;->g(Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity$a;Landroid/content/DialogInterface;)V

    return-void
.end method

.method public static synthetic c(Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;Lm5/e;Landroid/content/DialogInterface;I)V
    .locals 0

    invoke-static {p0, p1, p2, p3}, Lc6/m;->f(Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;Lm5/e;Landroid/content/DialogInterface;I)V

    return-void
.end method

.method public static synthetic d(Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;Lm5/e;Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity$a;Landroid/content/DialogInterface;I)V
    .locals 0

    invoke-static {p0, p1, p2, p3, p4}, Lc6/m;->e(Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;Lm5/e;Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity$a;Landroid/content/DialogInterface;I)V

    return-void
.end method

.method private static final e(Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;Lm5/e;Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity$a;Landroid/content/DialogInterface;I)V
    .locals 2

    const-string p4, "$activity"

    invoke-static {p0, p4}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p4, "$callingPackage"

    invoke-static {p1, p4}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p4, "$callback"

    invoke-static {p2, p4}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p4, Lp5/b;

    const-string v0, "safe_mode_prompt_popup_continue_btn"

    const-string v1, "button"

    invoke-direct {p4, v0, v1, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p1}, Lm5/e;->j()Ljava/lang/String;

    move-result-object p0

    const-string p1, "related_package_name"

    invoke-virtual {p4, p1, p0}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p0

    invoke-virtual {p0}, Lp5/f;->c()Z

    invoke-interface {p2}, Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity$a;->b()V

    invoke-interface {p3}, Landroid/content/DialogInterface;->dismiss()V

    return-void
.end method

.method private static final f(Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;Lm5/e;Landroid/content/DialogInterface;I)V
    .locals 2

    const-string p3, "$activity"

    invoke-static {p0, p3}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p3, "$callingPackage"

    invoke-static {p1, p3}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p3, Lp5/b;

    const-string v0, "safe_mode_prompt_popup_cancel_btn"

    const-string v1, "button"

    invoke-direct {p3, v0, v1, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p1}, Lm5/e;->j()Ljava/lang/String;

    move-result-object p0

    const-string p1, "related_package_name"

    invoke-virtual {p3, p1, p0}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p0

    invoke-virtual {p0}, Lp5/f;->c()Z

    invoke-interface {p2}, Landroid/content/DialogInterface;->cancel()V

    return-void
.end method

.method private static final g(Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity$a;Landroid/content/DialogInterface;)V
    .locals 0

    const-string p1, "$callback"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p0}, Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity$a;->a()V

    return-void
.end method


# virtual methods
.method public a(Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity$a;)V
    .locals 13

    const-string v0, "activity"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "callback"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;->Q0()Lm5/e;

    move-result-object v0

    const-string v1, "other_app_launch"

    invoke-virtual {p1, v1}, Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;->T0(Ljava/lang/String;)V

    new-instance v1, Lp5/g;

    const-string v2, "safe_mode_prompt_popup"

    const-string v3, "popup"

    invoke-direct {v1, v2, v3, p1}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lm5/e;->j()Ljava/lang/String;

    move-result-object v2

    const-string v3, "related_package_name"

    invoke-virtual {v1, v3, v2}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object v1

    invoke-virtual {v1}, Lp5/f;->c()Z

    new-instance v1, Lp5/g;

    const-string v2, "safe_mode_prompt_popup_continue_btn"

    const-string v4, "button"

    invoke-direct {v1, v2, v4, p1}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lm5/e;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object v1

    invoke-virtual {v1}, Lp5/f;->c()Z

    new-instance v1, Lp5/g;

    const-string v2, "safe_mode_prompt_popup_cancel_btn"

    invoke-direct {v1, v2, v4, p1}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lm5/e;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object v1

    invoke-virtual {v1}, Lp5/f;->c()Z

    sget-object v1, Lm2/a;->b:Lm2/a$b;

    invoke-virtual {v1}, Lm2/a$b;->a()Lm2/a;

    move-result-object v1

    invoke-virtual {v1}, Lm2/a;->d()Lcom/miui/packageInstaller/model/ThirdPartyInvokeInstallerPopTips;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/ThirdPartyInvokeInstallerPopTips;->getTitle()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    move-result v4

    if-lez v4, :cond_0

    move v4, v2

    goto :goto_0

    :cond_0
    move v4, v3

    :goto_0
    if-ne v4, v2, :cond_1

    move v4, v2

    goto :goto_1

    :cond_1
    move v4, v3

    :goto_1
    const-string v5, "callingPackage.name"

    if-eqz v4, :cond_2

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/ThirdPartyInvokeInstallerPopTips;->getTitle()Ljava/lang/String;

    move-result-object v6

    iget-object v8, v0, Lm5/e;->e:Ljava/lang/String;

    invoke-static {v8, v5}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v9, 0x0

    const/4 v10, 0x4

    const/4 v11, 0x0

    const-string v7, "*"

    invoke-static/range {v6 .. v11}, Lu8/g;->r(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_2

    :cond_2
    sget-object v4, Lm8/w;->a:Lm8/w;

    const v4, 0x7f1102c8

    invoke-virtual {p1, v4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v6, "activity.getString(R.str\u2026re_mode_tip_dialog_title)"

    invoke-static {v4, v6}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    new-array v6, v2, [Ljava/lang/Object;

    iget-object v7, v0, Lm5/e;->e:Ljava/lang/String;

    aput-object v7, v6, v3

    invoke-static {v6, v2}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v6

    invoke-static {v4, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const-string v6, "format(format, *args)"

    invoke-static {v4, v6}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_2
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/ThirdPartyInvokeInstallerPopTips;->getSubTitle()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_4

    invoke-interface {v6}, Ljava/lang/CharSequence;->length()I

    move-result v6

    if-lez v6, :cond_3

    move v6, v2

    goto :goto_3

    :cond_3
    move v6, v3

    :goto_3
    if-ne v6, v2, :cond_4

    move v6, v2

    goto :goto_4

    :cond_4
    move v6, v3

    :goto_4
    if-eqz v6, :cond_5

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/ThirdPartyInvokeInstallerPopTips;->getSubTitle()Ljava/lang/String;

    move-result-object v7

    iget-object v9, v0, Lm5/e;->e:Ljava/lang/String;

    invoke-static {v9, v5}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v10, 0x0

    const/4 v11, 0x4

    const/4 v12, 0x0

    const-string v8, "*"

    invoke-static/range {v7 .. v12}, Lu8/g;->r(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    goto :goto_5

    :cond_5
    const v5, 0x7f1102c4

    invoke-virtual {p1, v5}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v6, "activity.getString(R.str\u2026pure_mode_tip_dialog_des)"

    invoke-static {v5, v6}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_5
    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/ThirdPartyInvokeInstallerPopTips;->getButtonLeft()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_7

    invoke-interface {v6}, Ljava/lang/CharSequence;->length()I

    move-result v6

    if-lez v6, :cond_6

    move v6, v2

    goto :goto_6

    :cond_6
    move v6, v3

    :goto_6
    if-ne v6, v2, :cond_7

    move v6, v2

    goto :goto_7

    :cond_7
    move v6, v3

    :goto_7
    if-eqz v6, :cond_8

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/ThirdPartyInvokeInstallerPopTips;->getButtonLeft()Ljava/lang/String;

    move-result-object v6

    goto :goto_8

    :cond_8
    const v6, 0x7f110381

    invoke-virtual {p1, v6}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v7, "activity.getString(R.string.start_install)"

    invoke-static {v6, v7}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_8
    if-eqz v1, :cond_a

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/ThirdPartyInvokeInstallerPopTips;->getButtonRight()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_a

    invoke-interface {v7}, Ljava/lang/CharSequence;->length()I

    move-result v7

    if-lez v7, :cond_9

    move v7, v2

    goto :goto_9

    :cond_9
    move v7, v3

    :goto_9
    if-ne v7, v2, :cond_a

    goto :goto_a

    :cond_a
    move v2, v3

    :goto_a
    if-eqz v2, :cond_b

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/ThirdPartyInvokeInstallerPopTips;->getButtonRight()Ljava/lang/String;

    move-result-object v1

    goto :goto_b

    :cond_b
    const v1, 0x7f1100b5

    invoke-virtual {p1, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "activity.getString(R.str\u2026g.cta_button_text_cancel)"

    invoke-static {v1, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_b
    new-instance v2, Lmiuix/appcompat/app/i$b;

    invoke-direct {v2, p1}, Lmiuix/appcompat/app/i$b;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v4}, Lmiuix/appcompat/app/i$b;->t(Ljava/lang/CharSequence;)Lmiuix/appcompat/app/i$b;

    invoke-virtual {v2, v5}, Lmiuix/appcompat/app/i$b;->f(Ljava/lang/CharSequence;)Lmiuix/appcompat/app/i$b;

    new-instance v3, Lc6/l;

    invoke-direct {v3, p1, v0, p2}, Lc6/l;-><init>(Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;Lm5/e;Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity$a;)V

    invoke-virtual {v2, v6, v3}, Lmiuix/appcompat/app/i$b;->k(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/i$b;

    new-instance v3, Lc6/k;

    invoke-direct {v3, p1, v0}, Lc6/k;-><init>(Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;Lm5/e;)V

    invoke-virtual {v2, v1, v3}, Lmiuix/appcompat/app/i$b;->i(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/i$b;

    new-instance p1, Lc6/j;

    invoke-direct {p1, p2}, Lc6/j;-><init>(Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity$a;)V

    invoke-virtual {v2, p1}, Lmiuix/appcompat/app/i$b;->l(Landroid/content/DialogInterface$OnCancelListener;)Lmiuix/appcompat/app/i$b;

    invoke-virtual {v2}, Lmiuix/appcompat/app/i$b;->a()Lmiuix/appcompat/app/i;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/Dialog;->show()V

    return-void
.end method
