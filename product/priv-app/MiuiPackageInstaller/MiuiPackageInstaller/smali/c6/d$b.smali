.class public final Lc6/d$b;
.super Ljava/lang/Object;

# interfaces
.implements Lf6/b0$a$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lc6/d;->a(Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity$a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;

.field final synthetic b:Lm5/e;

.field final synthetic c:Lc6/d;

.field final synthetic d:Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity$a;

.field final synthetic e:Lmiuix/appcompat/app/i;


# direct methods
.method constructor <init>(Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;Lm5/e;Lc6/d;Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity$a;Lmiuix/appcompat/app/i;)V
    .locals 0

    iput-object p1, p0, Lc6/d$b;->a:Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;

    iput-object p2, p0, Lc6/d$b;->b:Lm5/e;

    iput-object p3, p0, Lc6/d$b;->c:Lc6/d;

    iput-object p4, p0, Lc6/d$b;->d:Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity$a;

    iput-object p5, p0, Lc6/d$b;->e:Lmiuix/appcompat/app/i;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;)V
    .locals 4

    const-string v0, "widget"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Lp5/b;

    iget-object v0, p0, Lc6/d$b;->a:Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;

    const-string v1, "protect_mode_prompt_popup_setting_btn"

    const-string v2, "button"

    invoke-direct {p1, v1, v2, v0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    iget-object v0, p0, Lc6/d$b;->b:Lm5/e;

    invoke-virtual {v0}, Lm5/e;->j()Ljava/lang/String;

    move-result-object v0

    const-string v1, "related_package_name"

    invoke-virtual {p1, v1, v0}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p1

    invoke-virtual {p1}, Lp5/f;->c()Z

    new-instance p1, Landroid/content/Intent;

    iget-object v0, p0, Lc6/d$b;->a:Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p1, v0}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    new-instance v0, Lo5/b;

    const-string v1, "elderly_install_tip_dialog"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lo5/b;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "fromPage"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    iget-object v0, p0, Lc6/d$b;->b:Lm5/e;

    const-string v1, "caller"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v0, "safe_mode_type"

    const-string v1, "packageinstaller"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "safe_mode_ref"

    const-string v1, "packageinstaller_elderly_type"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Lc6/d$b;->c:Lc6/d;

    iget-object v1, p0, Lc6/d$b;->a:Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;

    iget-object v2, p0, Lc6/d$b;->b:Lm5/e;

    const-class v3, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;

    invoke-virtual {v0, v1, p1, v2, v3}, Lc6/d;->f(Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;Landroid/content/Intent;Lm5/e;Ljava/lang/Class;)V

    iget-object p1, p0, Lc6/d$b;->d:Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity$a;

    invoke-interface {p1}, Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity$a;->a()V

    iget-object p1, p0, Lc6/d$b;->e:Lmiuix/appcompat/app/i;

    invoke-virtual {p1}, Lmiuix/appcompat/app/i;->dismiss()V

    return-void
.end method
