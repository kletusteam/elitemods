.class public final Lc6/i;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lc6/i$a;
    }
.end annotation


# instance fields
.field private a:Lx5/a;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Lx5/a;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lc6/i;->a:Lx5/a;

    return-void
.end method

.method public static final synthetic b(Lc6/i;Lx5/a;)Ljava/lang/String;
    .locals 0

    invoke-direct {p0, p1}, Lc6/i;->c(Lx5/a;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private final c(Lx5/a;)Ljava/lang/String;
    .locals 1

    sget-object v0, Lc6/i$a;->a:[I

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_3

    const/4 v0, 0x2

    if-eq p1, v0, :cond_2

    const/4 v0, 0x3

    if-eq p1, v0, :cond_1

    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    const-string p1, "none"

    goto :goto_0

    :cond_0
    const-string p1, "face_password"

    goto :goto_0

    :cond_1
    const-string p1, "fingerprint_password"

    goto :goto_0

    :cond_2
    const-string p1, "screen_password"

    goto :goto_0

    :cond_3
    const-string p1, "mi_account"

    :goto_0
    return-object p1
.end method


# virtual methods
.method public a(Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity$a;)V
    .locals 3

    const-string v0, "activity"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "callback"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const v0, 0x7f11030e

    invoke-virtual {p1, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lc6/i;->b:Ljava/lang/String;

    const v0, 0x7f1103e3

    invoke-virtual {p1, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lc6/i;->c:Ljava/lang/String;

    sget-object v0, Lx5/d;->b:Lx5/d$a;

    invoke-virtual {v0}, Lx5/d$a;->b()Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lc6/i;->a:Lx5/a;

    if-eqz v1, :cond_0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    :cond_0
    new-instance v1, Lx5/j$a;

    invoke-direct {v1, p1}, Lx5/j$a;-><init>(Landroid/app/Activity;)V

    const v2, 0x7f1100ae

    invoke-virtual {v1, v2}, Lx5/j$a;->f(I)Lx5/j$a;

    move-result-object v1

    const v2, 0x7f1100ad

    invoke-virtual {v1, v2}, Lx5/j$a;->e(I)Lx5/j$a;

    move-result-object v1

    new-instance v2, Lc6/i$c;

    invoke-direct {v2, p1, p0}, Lc6/i$c;-><init>(Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;Lc6/i;)V

    invoke-virtual {v1, v2}, Lx5/j$a;->b(Ll8/l;)Lx5/j$a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lx5/j$a;->c(Ljava/util/ArrayList;)Lx5/j$a;

    move-result-object v0

    iget-object v1, p0, Lc6/i;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v0, v1}, Lx5/j$a;->m(Ljava/lang/CharSequence;)Lx5/j$a;

    :cond_1
    iget-object v1, p0, Lc6/i;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v0, v1}, Lx5/j$a;->i(Ljava/lang/CharSequence;)Lx5/j$a;

    :cond_2
    iget-object v1, p0, Lc6/i;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v0, v1}, Lx5/j$a;->k(Ljava/lang/CharSequence;)Lx5/j$a;

    :cond_3
    invoke-virtual {v0}, Lx5/j$a;->a()Lx5/j;

    move-result-object v0

    new-instance v1, Lc6/i$b;

    invoke-direct {v1, p2, p1, p0}, Lc6/i$b;-><init>(Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity$a;Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;Lc6/i;)V

    invoke-virtual {v0, v1}, Lx5/j;->p(Ll8/p;)V

    return-void
.end method
