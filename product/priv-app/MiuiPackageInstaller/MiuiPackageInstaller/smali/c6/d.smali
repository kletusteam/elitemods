.class public final Lc6/d;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lc6/d$a;
    }
.end annotation


# static fields
.field public static final a:Lc6/d$a;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lc6/d$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lc6/d$a;-><init>(Lm8/g;)V

    sput-object v0, Lc6/d;->a:Lc6/d$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static synthetic b(Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;Lm5/e;Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity$a;Landroid/content/DialogInterface;I)V
    .locals 0

    invoke-static {p0, p1, p2, p3, p4}, Lc6/d;->d(Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;Lm5/e;Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity$a;Landroid/content/DialogInterface;I)V

    return-void
.end method

.method public static synthetic c(Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity$a;Landroid/content/DialogInterface;)V
    .locals 0

    invoke-static {p0, p1}, Lc6/d;->e(Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity$a;Landroid/content/DialogInterface;)V

    return-void
.end method

.method private static final d(Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;Lm5/e;Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity$a;Landroid/content/DialogInterface;I)V
    .locals 2

    const-string p4, "$activity"

    invoke-static {p0, p4}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p4, "$callingPackage"

    invoke-static {p1, p4}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p4, "$callback"

    invoke-static {p2, p4}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p4, Lp5/b;

    const-string v0, "protect_mode_prompt_popup_know_btn"

    const-string v1, "button"

    invoke-direct {p4, v0, v1, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p1}, Lm5/e;->j()Ljava/lang/String;

    move-result-object p0

    const-string p1, "related_package_name"

    invoke-virtual {p4, p1, p0}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p0

    invoke-virtual {p0}, Lp5/f;->c()Z

    invoke-interface {p3}, Landroid/content/DialogInterface;->dismiss()V

    invoke-interface {p2}, Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity$a;->a()V

    return-void
.end method

.method private static final e(Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity$a;Landroid/content/DialogInterface;)V
    .locals 0

    const-string p1, "$callback"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p0}, Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity$a;->a()V

    return-void
.end method


# virtual methods
.method public a(Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity$a;)V
    .locals 13

    const-string v0, "activity"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "callback"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "other_app_launch"

    invoke-virtual {p1, v0}, Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;->T0(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;->Q0()Lm5/e;

    move-result-object v2

    new-instance v0, Lp5/g;

    const-string v3, "protect_mode_prompt_popup"

    const-string v5, "popup"

    invoke-direct {v0, v3, v5, p1}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v2}, Lm5/e;->j()Ljava/lang/String;

    move-result-object v3

    const-string v5, "related_package_name"

    invoke-virtual {v0, v5, v3}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object v0

    invoke-virtual {v0}, Lp5/f;->c()Z

    new-instance v0, Lp5/g;

    const-string v3, "protect_mode_prompt_popup_setting_btn"

    const-string v6, "button"

    invoke-direct {v0, v3, v6, p1}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v2}, Lm5/e;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v5, v3}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object v0

    invoke-virtual {v0}, Lp5/f;->c()Z

    new-instance v0, Lp5/g;

    const-string v3, "protect_mode_prompt_popup_know_btn"

    invoke-direct {v0, v3, v6, p1}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v2}, Lm5/e;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v5, v3}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object v0

    invoke-virtual {v0}, Lp5/f;->c()Z

    new-instance v0, Lmiuix/appcompat/app/i$b;

    invoke-direct {v0, p1}, Lmiuix/appcompat/app/i$b;-><init>(Landroid/content/Context;)V

    sget-object v3, Lm8/w;->a:Lm8/w;

    const v3, 0x7f1102c8

    invoke-virtual {p1, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v5, "activity.getString(R.str\u2026re_mode_tip_dialog_title)"

    invoke-static {v3, v5}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v5, 0x1

    new-array v6, v5, [Ljava/lang/Object;

    iget-object v7, v2, Lm5/e;->e:Ljava/lang/String;

    const/4 v8, 0x0

    aput-object v7, v6, v8

    invoke-static {v6, v5}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v5

    invoke-static {v3, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v5, "format(format, *args)"

    invoke-static {v3, v5}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Lmiuix/appcompat/app/i$b;->t(Ljava/lang/CharSequence;)Lmiuix/appcompat/app/i$b;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    const v5, 0x7f0d0056

    const/4 v6, 0x0

    invoke-virtual {v3, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v0, v3}, Lmiuix/appcompat/app/i$b;->v(Landroid/view/View;)Lmiuix/appcompat/app/i$b;

    const v5, 0x7f1100df

    invoke-virtual {p1, v5}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Lc6/c;

    invoke-direct {v6, p1, v2, p2}, Lc6/c;-><init>(Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;Lm5/e;Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity$a;)V

    invoke-virtual {v0, v5, v6}, Lmiuix/appcompat/app/i$b;->i(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/i$b;

    new-instance v5, Lc6/b;

    invoke-direct {v5, p2}, Lc6/b;-><init>(Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity$a;)V

    invoke-virtual {v0, v5}, Lmiuix/appcompat/app/i$b;->l(Landroid/content/DialogInterface$OnCancelListener;)Lmiuix/appcompat/app/i$b;

    invoke-virtual {v0}, Lmiuix/appcompat/app/i$b;->a()Lmiuix/appcompat/app/i;

    move-result-object v5

    const-string v0, "alertDialog.create()"

    invoke-static {v5, v0}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v5}, Landroid/app/Dialog;->show()V

    invoke-static {v5, p1}, Lf6/c;->a(Landroid/app/Dialog;Landroid/app/Activity;)V

    const v0, 0x7f0a010a

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Landroidx/appcompat/widget/AppCompatTextView;

    sget-object v6, Lf6/b0;->a:Lf6/b0$a;

    const-string v0, "textView"

    invoke-static {v7, v0}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const v0, 0x7f1102c6

    invoke-virtual {p1, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v8

    const-string v0, "activity.getString(R.str\u2026e_tip_dialog_elderly_set)"

    invoke-static {v8, v0}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const v0, 0x7f1102c7

    invoke-virtual {p1, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v9

    const-string v0, "activity.getString(R.str\u2026dialog_elderly_set_click)"

    invoke-static {v9, v0}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const v0, 0x7f060030

    invoke-virtual {p1, v0}, Landroid/app/Activity;->getColor(I)I

    move-result v10

    invoke-virtual {p1, v0}, Landroid/app/Activity;->getColor(I)I

    move-result v11

    new-instance v12, Lc6/d$b;

    move-object v0, v12

    move-object v1, p1

    move-object v3, p0

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lc6/d$b;-><init>(Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;Lm5/e;Lc6/d;Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity$a;Lmiuix/appcompat/app/i;)V

    invoke-virtual/range {v6 .. v12}, Lf6/b0$a;->a(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;IILf6/b0$a$a;)V

    return-void
.end method

.method public final f(Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;Landroid/content/Intent;Lm5/e;Ljava/lang/Class;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;",
            "Landroid/content/Intent;",
            "Lm5/e;",
            "Ljava/lang/Class<",
            "+",
            "Landroid/app/Activity;",
            ">;)V"
        }
    .end annotation

    const-string v0, "activity"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "intent"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "caller"

    invoke-static {p3, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cls"

    invoke-static {p4, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p2}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    const/high16 v1, 0x2000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v2, 0x1

    const/16 v3, 0x1d

    if-le v1, v3, :cond_0

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    goto :goto_0

    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    const-string v3, "content"

    invoke-static {v3, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :try_start_0
    invoke-virtual {p1}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object p2

    invoke-virtual {p1, v1, p2, v2}, Landroid/app/Activity;->grantUriPermission(Ljava/lang/String;Landroid/net/Uri;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p2

    invoke-virtual {p2}, Ljava/lang/Exception;->printStackTrace()V

    :cond_1
    :goto_0
    const-string p2, "EXTRA_CALLING_PACKAGE"

    invoke-virtual {v0, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const/high16 p2, 0x10000000

    invoke-virtual {v0, p2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-virtual {v0, p1, p4}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    :try_start_1
    invoke-virtual {p1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception p2

    const-string p3, "SecurityModeDialog"

    const-string p4, "start next Activity error : "

    invoke-static {p3, p4, p2}, Lf6/o;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-virtual {p2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p2

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object p3

    const-string p4, "getDefault()"

    invoke-static {p3, p4}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2, p3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p2

    const-string p3, "this as java.lang.String).toLowerCase(locale)"

    invoke-static {p2, p3}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p3, 0x2

    const/4 p4, 0x0

    const-string v0, "not have permission"

    const/4 v1, 0x0

    invoke-static {p2, v0, v1, p3, p4}, Lu8/g;->A(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZILjava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_2

    const p2, 0x7f1103dd

    invoke-static {p1, p2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    :cond_2
    :goto_1
    return-void
.end method
