.class public final Lc6/h;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lc6/h$a;
    }
.end annotation


# static fields
.field public static final d:Lc6/h$a;


# instance fields
.field private final a:Lcom/miui/packageInstaller/model/RiskControlRules;

.field private final b:Ljava/lang/String;

.field private c:Lmiuix/appcompat/app/i;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lc6/h$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lc6/h$a;-><init>(Lm8/g;)V

    sput-object v0, Lc6/h;->d:Lc6/h$a;

    return-void
.end method

.method public constructor <init>(Lcom/miui/packageInstaller/model/RiskControlRules;Ljava/lang/String;)V
    .locals 1

    const-string v0, "rules"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "authType"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lc6/h;->a:Lcom/miui/packageInstaller/model/RiskControlRules;

    iput-object p2, p0, Lc6/h;->b:Ljava/lang/String;

    return-void
.end method

.method public static synthetic b(Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity$a;Landroid/content/DialogInterface;)V
    .locals 0

    invoke-static {p0, p1}, Lc6/h;->e(Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity$a;Landroid/content/DialogInterface;)V

    return-void
.end method

.method public static synthetic c(Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity$a;Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;Landroid/content/DialogInterface;I)V
    .locals 0

    invoke-static {p0, p1, p2, p3}, Lc6/h;->f(Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity$a;Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;Landroid/content/DialogInterface;I)V

    return-void
.end method

.method public static synthetic d(Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity$a;Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;Landroid/content/DialogInterface;I)V
    .locals 0

    invoke-static {p0, p1, p2, p3}, Lc6/h;->g(Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity$a;Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;Landroid/content/DialogInterface;I)V

    return-void
.end method

.method private static final e(Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity$a;Landroid/content/DialogInterface;)V
    .locals 0

    const-string p1, "$callback"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p0}, Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity$a;->a()V

    return-void
.end method

.method private static final f(Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity$a;Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;Landroid/content/DialogInterface;I)V
    .locals 0

    const-string p2, "$callback"

    invoke-static {p0, p2}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "$activity"

    invoke-static {p1, p2}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p0}, Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity$a;->b()V

    new-instance p0, Lp5/b;

    const-string p2, "frequent_launch_warning_popup_verify_btn"

    const-string p3, "button"

    invoke-direct {p0, p2, p3, p1}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p0}, Lp5/f;->c()Z

    return-void
.end method

.method private static final g(Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity$a;Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;Landroid/content/DialogInterface;I)V
    .locals 0

    const-string p2, "$callback"

    invoke-static {p0, p2}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "$activity"

    invoke-static {p1, p2}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p0}, Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity$a;->a()V

    new-instance p0, Lp5/b;

    const-string p2, "frequent_launch_warning_popup_cancel_btn"

    const-string p3, "button"

    invoke-direct {p0, p2, p3, p1}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p0}, Lp5/f;->c()Z

    return-void
.end method

.method private final h(Lo5/a;)V
    .locals 3

    new-instance v0, Lp5/g;

    const-string v1, "frequent_launch_warning_popup"

    const-string v2, "popup"

    invoke-direct {v0, v1, v2, p1}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    new-instance v0, Lp5/g;

    const-string v1, "frequent_launch_warning_popup_verify_btn"

    const-string v2, "button"

    invoke-direct {v0, v1, v2, p1}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    new-instance v0, Lp5/g;

    const-string v1, "frequent_launch_warning_popup_cancel_btn"

    invoke-direct {v0, v1, v2, p1}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    return-void
.end method


# virtual methods
.method public a(Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity$a;)V
    .locals 20

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    const-string v3, "activity"

    invoke-static {v1, v3}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "callback"

    invoke-static {v2, v3}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "other_app_launch"

    invoke-virtual {v1, v3}, Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;->T0(Ljava/lang/String;)V

    invoke-static/range {p1 .. p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f0d0091

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    const v4, 0x7f0a0380

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iget-object v6, v0, Lc6/h;->a:Lcom/miui/packageInstaller/model/RiskControlRules;

    invoke-virtual {v6}, Lcom/miui/packageInstaller/model/RiskControlRules;->getRiskControlLaunchTitle()Ljava/lang/String;

    move-result-object v7

    const-string v6, "activity.obtainCallingPackage().name"

    if-eqz v7, :cond_0

    invoke-virtual/range {p1 .. p1}, Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;->Q0()Lm5/e;

    move-result-object v8

    iget-object v9, v8, Lm5/e;->e:Ljava/lang/String;

    invoke-static {v9, v6}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v10, 0x0

    const/4 v11, 0x4

    const/4 v12, 0x0

    const-string v8, "#app"

    invoke-static/range {v7 .. v12}, Lu8/g;->r(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    goto :goto_0

    :cond_0
    move-object v7, v5

    :goto_0
    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v4, 0x7f0a0255

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iget-object v7, v0, Lc6/h;->a:Lcom/miui/packageInstaller/model/RiskControlRules;

    invoke-virtual {v7}, Lcom/miui/packageInstaller/model/RiskControlRules;->getRiskControlLaunchContent()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_1

    invoke-virtual/range {p1 .. p1}, Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;->Q0()Lm5/e;

    move-result-object v7

    iget-object v10, v7, Lm5/e;->e:Ljava/lang/String;

    invoke-static {v10, v6}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v11, 0x0

    const/4 v12, 0x4

    const/4 v13, 0x0

    const-string v9, "#app"

    invoke-static/range {v8 .. v13}, Lu8/g;->r(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    if-eqz v14, :cond_1

    iget-object v6, v0, Lc6/h;->b:Ljava/lang/String;

    const/16 v17, 0x0

    const/16 v18, 0x4

    const/16 v19, 0x0

    const-string v15, "#auth_type"

    move-object/from16 v16, v6

    invoke-static/range {v14 .. v19}, Lu8/g;->r(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    goto :goto_1

    :cond_1
    move-object v6, v5

    :goto_1
    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v4, Lmiuix/appcompat/app/i$b;

    invoke-direct {v4, v1}, Lmiuix/appcompat/app/i$b;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4, v3}, Lmiuix/appcompat/app/i$b;->v(Landroid/view/View;)Lmiuix/appcompat/app/i$b;

    move-result-object v3

    new-instance v4, Lc6/e;

    invoke-direct {v4, v2}, Lc6/e;-><init>(Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity$a;)V

    invoke-virtual {v3, v4}, Lmiuix/appcompat/app/i$b;->l(Landroid/content/DialogInterface$OnCancelListener;)Lmiuix/appcompat/app/i$b;

    move-result-object v3

    iget-object v4, v0, Lc6/h;->a:Lcom/miui/packageInstaller/model/RiskControlRules;

    invoke-virtual {v4}, Lcom/miui/packageInstaller/model/RiskControlRules;->getRiskControlLaunchLeftButton()Ljava/lang/String;

    move-result-object v4

    new-instance v6, Lc6/f;

    invoke-direct {v6, v2, v1}, Lc6/f;-><init>(Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity$a;Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;)V

    invoke-virtual {v3, v4, v6}, Lmiuix/appcompat/app/i$b;->i(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/i$b;

    move-result-object v3

    iget-object v4, v0, Lc6/h;->a:Lcom/miui/packageInstaller/model/RiskControlRules;

    invoke-virtual {v4}, Lcom/miui/packageInstaller/model/RiskControlRules;->getRiskControlLaunchRightButton()Ljava/lang/String;

    move-result-object v4

    new-instance v6, Lc6/g;

    invoke-direct {v6, v2, v1}, Lc6/g;-><init>(Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity$a;Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;)V

    invoke-virtual {v3, v4, v6}, Lmiuix/appcompat/app/i$b;->p(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/i$b;

    move-result-object v3

    invoke-virtual {v3}, Lmiuix/appcompat/app/i$b;->a()Lmiuix/appcompat/app/i;

    move-result-object v3

    const-string v4, "Builder(activity)\n      \u2026  }\n            .create()"

    invoke-static {v3, v4}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v3, v0, Lc6/h;->c:Lmiuix/appcompat/app/i;

    const-string v4, "mDialog"

    if-nez v3, :cond_2

    :try_start_0
    invoke-static {v4}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v3, v5

    :cond_2
    invoke-virtual {v3}, Landroid/app/Dialog;->show()V

    iget-object v3, v0, Lc6/h;->c:Lmiuix/appcompat/app/i;

    if-nez v3, :cond_3

    invoke-static {v4}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_2

    :cond_3
    move-object v5, v3

    :goto_2
    invoke-static {v5, v1}, Lf6/c;->a(Landroid/app/Dialog;Landroid/app/Activity;)V

    invoke-direct/range {p0 .. p1}, Lc6/h;->h(Lo5/a;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    :catch_0
    invoke-interface/range {p2 .. p2}, Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity$a;->a()V

    :goto_3
    return-void
.end method
