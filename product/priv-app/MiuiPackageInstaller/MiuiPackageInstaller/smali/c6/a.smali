.class public final Lc6/a;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity$b;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private final b(Lo5/a;)V
    .locals 3

    new-instance v0, Lp5/g;

    const-string v1, "risk_verifying_popup"

    const-string v2, "popup"

    invoke-direct {v0, v1, v2, p1}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    const-string p1, "verify_method"

    const-string v1, "risk_verify"

    invoke-virtual {v0, p1, v1}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p1

    invoke-virtual {p1}, Lp5/f;->c()Z

    return-void
.end method


# virtual methods
.method public a(Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity$a;)V
    .locals 2

    const-string v0, "activity"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "callback"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "other_app_launch"

    invoke-virtual {p1, v0}, Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;->T0(Ljava/lang/String;)V

    new-instance v0, Lx5/b;

    invoke-direct {v0, p1}, Lx5/b;-><init>(Landroid/app/Activity;)V

    new-instance v1, Lc6/a$a;

    invoke-direct {v1, p2, p1}, Lc6/a$a;-><init>(Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity$a;Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;)V

    invoke-virtual {v0, v1}, Lx5/b;->b(Ll8/p;)V

    invoke-direct {p0, p1}, Lc6/a;->b(Lo5/a;)V

    return-void
.end method
