.class final Lj3/e;
.super Ljava/lang/Object;

# interfaces
.implements Lj3/c;


# instance fields
.field private final a:Landroid/content/Context;

.field final b:Lj3/c$a;

.field c:Z

.field private d:Z

.field private final e:Landroid/content/BroadcastReceiver;


# direct methods
.method constructor <init>(Landroid/content/Context;Lj3/c$a;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lj3/e$a;

    invoke-direct {v0, p0}, Lj3/e$a;-><init>(Lj3/e;)V

    iput-object v0, p0, Lj3/e;->e:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lj3/e;->a:Landroid/content/Context;

    iput-object p2, p0, Lj3/e;->b:Lj3/c$a;

    return-void
.end method

.method private m()V
    .locals 4

    iget-boolean v0, p0, Lj3/e;->d:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lj3/e;->a:Landroid/content/Context;

    invoke-virtual {p0, v0}, Lj3/e;->l(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lj3/e;->c:Z

    :try_start_0
    iget-object v0, p0, Lj3/e;->a:Landroid/content/Context;

    iget-object v1, p0, Lj3/e;->e:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lj3/e;->d:Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const/4 v1, 0x5

    const-string v2, "ConnectivityMonitor"

    invoke-static {v2, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "Failed to register"

    invoke-static {v2, v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_1
    :goto_0
    return-void
.end method

.method private n()V
    .locals 2

    iget-boolean v0, p0, Lj3/e;->d:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lj3/e;->a:Landroid/content/Context;

    iget-object v1, p0, Lj3/e;->e:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lj3/e;->d:Z

    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    invoke-direct {p0}, Lj3/e;->n()V

    return-void
.end method

.method public b()V
    .locals 0

    invoke-direct {p0}, Lj3/e;->m()V

    return-void
.end method

.method public k()V
    .locals 0

    return-void
.end method

.method l(Landroid/content/Context;)Z
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "MissingPermission"
        }
    .end annotation

    goto/32 :goto_a

    nop

    :goto_0
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    goto/32 :goto_1

    nop

    :goto_1
    check-cast p1, Landroid/net/ConnectivityManager;

    goto/32 :goto_11

    nop

    :goto_2
    check-cast p1, Landroid/net/ConnectivityManager;

    goto/32 :goto_9

    nop

    :goto_3
    invoke-static {v2, v1, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_4
    goto/32 :goto_13

    nop

    :goto_5
    invoke-virtual {p1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result p1

    goto/32 :goto_10

    nop

    :goto_6
    goto :goto_f

    :goto_7
    goto/32 :goto_e

    nop

    :goto_8
    invoke-static {v2, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    goto/32 :goto_12

    nop

    :goto_9
    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_c

    nop

    :goto_a
    const-string v0, "connectivity"

    goto/32 :goto_0

    nop

    :goto_b
    const-string v2, "ConnectivityMonitor"

    goto/32 :goto_8

    nop

    :goto_c
    if-nez p1, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_5

    nop

    :goto_d
    const-string v1, "Failed to determine connectivity status when connectivity changed"

    goto/32 :goto_3

    nop

    :goto_e
    const/4 v0, 0x0

    :goto_f
    goto/32 :goto_14

    nop

    :goto_10
    if-nez p1, :cond_1

    goto/32 :goto_7

    :cond_1
    goto/32 :goto_6

    nop

    :goto_11
    invoke-static {p1}, Lq3/j;->d(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    goto/32 :goto_2

    nop

    :goto_12
    if-nez v1, :cond_2

    goto/32 :goto_4

    :cond_2
    goto/32 :goto_d

    nop

    :goto_13
    return v0

    :goto_14
    return v0

    :catch_0
    move-exception p1

    goto/32 :goto_15

    nop

    :goto_15
    const/4 v1, 0x5

    goto/32 :goto_b

    nop
.end method
