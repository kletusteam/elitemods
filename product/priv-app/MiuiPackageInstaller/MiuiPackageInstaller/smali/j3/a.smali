.class Lj3/a;
.super Ljava/lang/Object;

# interfaces
.implements Lj3/l;


# instance fields
.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lj3/m;",
            ">;"
        }
    .end annotation
.end field

.field private b:Z

.field private c:Z


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lj3/a;->a:Ljava/util/Set;

    return-void
.end method


# virtual methods
.method public a(Lj3/m;)V
    .locals 1

    iget-object v0, p0, Lj3/a;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-boolean v0, p0, Lj3/a;->c:Z

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lj3/m;->k()V

    goto :goto_0

    :cond_0
    iget-boolean v0, p0, Lj3/a;->b:Z

    if-eqz v0, :cond_1

    invoke-interface {p1}, Lj3/m;->b()V

    goto :goto_0

    :cond_1
    invoke-interface {p1}, Lj3/m;->a()V

    :goto_0
    return-void
.end method

.method public b(Lj3/m;)V
    .locals 1

    iget-object v0, p0, Lj3/a;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method c()V
    .locals 2

    goto/32 :goto_c

    nop

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_7

    nop

    :goto_1
    iget-object v0, p0, Lj3/a;->a:Ljava/util/Set;

    goto/32 :goto_9

    nop

    :goto_2
    invoke-interface {v1}, Lj3/m;->k()V

    goto/32 :goto_a

    nop

    :goto_3
    if-nez v1, :cond_0

    goto/32 :goto_b

    :cond_0
    goto/32 :goto_0

    nop

    :goto_4
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_3

    nop

    :goto_5
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_6
    goto/32 :goto_4

    nop

    :goto_7
    check-cast v1, Lj3/m;

    goto/32 :goto_2

    nop

    :goto_8
    return-void

    :goto_9
    invoke-static {v0}, Lq3/k;->i(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    goto/32 :goto_5

    nop

    :goto_a
    goto :goto_6

    :goto_b
    goto/32 :goto_8

    nop

    :goto_c
    const/4 v0, 0x1

    goto/32 :goto_d

    nop

    :goto_d
    iput-boolean v0, p0, Lj3/a;->c:Z

    goto/32 :goto_1

    nop
.end method

.method d()V
    .locals 2

    goto/32 :goto_5

    nop

    :goto_0
    if-nez v1, :cond_0

    goto/32 :goto_9

    :cond_0
    goto/32 :goto_4

    nop

    :goto_1
    iput-boolean v0, p0, Lj3/a;->b:Z

    goto/32 :goto_a

    nop

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_0

    nop

    :goto_3
    check-cast v1, Lj3/m;

    goto/32 :goto_b

    nop

    :goto_4
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_3

    nop

    :goto_5
    const/4 v0, 0x1

    goto/32 :goto_1

    nop

    :goto_6
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_7
    goto/32 :goto_2

    nop

    :goto_8
    goto :goto_7

    :goto_9
    goto/32 :goto_d

    nop

    :goto_a
    iget-object v0, p0, Lj3/a;->a:Ljava/util/Set;

    goto/32 :goto_c

    nop

    :goto_b
    invoke-interface {v1}, Lj3/m;->b()V

    goto/32 :goto_8

    nop

    :goto_c
    invoke-static {v0}, Lq3/k;->i(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    goto/32 :goto_6

    nop

    :goto_d
    return-void
.end method

.method e()V
    .locals 2

    goto/32 :goto_0

    nop

    :goto_0
    const/4 v0, 0x0

    goto/32 :goto_4

    nop

    :goto_1
    check-cast v1, Lj3/m;

    goto/32 :goto_6

    nop

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_3

    nop

    :goto_3
    if-nez v1, :cond_0

    goto/32 :goto_c

    :cond_0
    goto/32 :goto_5

    nop

    :goto_4
    iput-boolean v0, p0, Lj3/a;->b:Z

    goto/32 :goto_d

    nop

    :goto_5
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_1

    nop

    :goto_6
    invoke-interface {v1}, Lj3/m;->a()V

    goto/32 :goto_b

    nop

    :goto_7
    return-void

    :goto_8
    invoke-static {v0}, Lq3/k;->i(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    goto/32 :goto_9

    nop

    :goto_9
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_a
    goto/32 :goto_2

    nop

    :goto_b
    goto :goto_a

    :goto_c
    goto/32 :goto_7

    nop

    :goto_d
    iget-object v0, p0, Lj3/a;->a:Ljava/util/Set;

    goto/32 :goto_8

    nop
.end method
