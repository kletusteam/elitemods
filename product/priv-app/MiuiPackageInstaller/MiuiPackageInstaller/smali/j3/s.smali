.class public Lj3/s;
.super Landroidx/fragment/app/Fragment;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lj3/s$a;
    }
.end annotation


# instance fields
.field private final e0:Lj3/a;

.field private final f0:Lj3/q;

.field private final g0:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lj3/s;",
            ">;"
        }
    .end annotation
.end field

.field private h0:Lj3/s;

.field private i0:Lcom/bumptech/glide/k;

.field private j0:Landroidx/fragment/app/Fragment;


# direct methods
.method public constructor <init>()V
    .locals 1

    new-instance v0, Lj3/a;

    invoke-direct {v0}, Lj3/a;-><init>()V

    invoke-direct {p0, v0}, Lj3/s;-><init>(Lj3/a;)V

    return-void
.end method

.method public constructor <init>(Lj3/a;)V
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ValidFragment"
        }
    .end annotation

    invoke-direct {p0}, Landroidx/fragment/app/Fragment;-><init>()V

    new-instance v0, Lj3/s$a;

    invoke-direct {v0, p0}, Lj3/s$a;-><init>(Lj3/s;)V

    iput-object v0, p0, Lj3/s;->f0:Lj3/q;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lj3/s;->g0:Ljava/util/Set;

    iput-object p1, p0, Lj3/s;->e0:Lj3/a;

    return-void
.end method

.method private H1(Lj3/s;)V
    .locals 1

    iget-object v0, p0, Lj3/s;->g0:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private K1()Landroidx/fragment/app/Fragment;
    .locals 1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->I()Landroidx/fragment/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lj3/s;->j0:Landroidx/fragment/app/Fragment;

    :goto_0
    return-object v0
.end method

.method private static N1(Landroidx/fragment/app/Fragment;)Landroidx/fragment/app/m;
    .locals 1

    :goto_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->I()Landroidx/fragment/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->I()Landroidx/fragment/app/Fragment;

    move-result-object p0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->C()Landroidx/fragment/app/m;

    move-result-object p0

    return-object p0
.end method

.method private O1(Landroidx/fragment/app/Fragment;)Z
    .locals 2

    invoke-direct {p0}, Lj3/s;->K1()Landroidx/fragment/app/Fragment;

    move-result-object v0

    :goto_0
    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->I()Landroidx/fragment/app/Fragment;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1, v0}, Landroidx/fragment/app/Fragment;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->I()Landroidx/fragment/app/Fragment;

    move-result-object p1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    return p1
.end method

.method private P1(Landroid/content/Context;Landroidx/fragment/app/m;)V
    .locals 0

    invoke-direct {p0}, Lj3/s;->T1()V

    invoke-static {p1}, Lcom/bumptech/glide/b;->c(Landroid/content/Context;)Lcom/bumptech/glide/b;

    move-result-object p1

    invoke-virtual {p1}, Lcom/bumptech/glide/b;->k()Lj3/p;

    move-result-object p1

    invoke-virtual {p1, p2}, Lj3/p;->k(Landroidx/fragment/app/m;)Lj3/s;

    move-result-object p1

    iput-object p1, p0, Lj3/s;->h0:Lj3/s;

    invoke-virtual {p0, p1}, Landroidx/fragment/app/Fragment;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    iget-object p1, p0, Lj3/s;->h0:Lj3/s;

    invoke-direct {p1, p0}, Lj3/s;->H1(Lj3/s;)V

    :cond_0
    return-void
.end method

.method private Q1(Lj3/s;)V
    .locals 1

    iget-object v0, p0, Lj3/s;->g0:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method private T1()V
    .locals 1

    iget-object v0, p0, Lj3/s;->h0:Lj3/s;

    if-eqz v0, :cond_0

    invoke-direct {v0, p0}, Lj3/s;->Q1(Lj3/s;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lj3/s;->h0:Lj3/s;

    :cond_0
    return-void
.end method


# virtual methods
.method I1()Ljava/util/Set;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lj3/s;",
            ">;"
        }
    .end annotation

    goto/32 :goto_13

    nop

    :goto_0
    if-eqz v0, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_1

    nop

    :goto_1
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    goto/32 :goto_2

    nop

    :goto_2
    return-object v0

    :goto_3
    goto/32 :goto_4

    nop

    :goto_4
    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto/32 :goto_12

    nop

    :goto_5
    return-object v0

    :goto_6
    invoke-virtual {v1}, Lj3/s;->I1()Ljava/util/Set;

    move-result-object v1

    goto/32 :goto_a

    nop

    :goto_7
    iget-object v0, p0, Lj3/s;->g0:Ljava/util/Set;

    goto/32 :goto_e

    nop

    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_1b

    nop

    :goto_9
    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/32 :goto_10

    nop

    :goto_a
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_b
    goto/32 :goto_15

    nop

    :goto_c
    invoke-direct {v2}, Lj3/s;->K1()Landroidx/fragment/app/Fragment;

    move-result-object v3

    goto/32 :goto_d

    nop

    :goto_d
    invoke-direct {p0, v3}, Lj3/s;->O1(Landroidx/fragment/app/Fragment;)Z

    move-result v3

    goto/32 :goto_1c

    nop

    :goto_e
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    goto/32 :goto_18

    nop

    :goto_f
    new-instance v0, Ljava/util/HashSet;

    goto/32 :goto_14

    nop

    :goto_10
    goto :goto_b

    :goto_11
    goto/32 :goto_1a

    nop

    :goto_12
    if-nez v0, :cond_1

    goto/32 :goto_19

    :cond_1
    goto/32 :goto_7

    nop

    :goto_13
    iget-object v0, p0, Lj3/s;->h0:Lj3/s;

    goto/32 :goto_0

    nop

    :goto_14
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    goto/32 :goto_17

    nop

    :goto_15
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    goto/32 :goto_16

    nop

    :goto_16
    if-nez v2, :cond_2

    goto/32 :goto_11

    :cond_2
    goto/32 :goto_8

    nop

    :goto_17
    iget-object v1, p0, Lj3/s;->h0:Lj3/s;

    goto/32 :goto_6

    nop

    :goto_18
    return-object v0

    :goto_19
    goto/32 :goto_f

    nop

    :goto_1a
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    goto/32 :goto_5

    nop

    :goto_1b
    check-cast v2, Lj3/s;

    goto/32 :goto_c

    nop

    :goto_1c
    if-nez v3, :cond_3

    goto/32 :goto_b

    :cond_3
    goto/32 :goto_9

    nop
.end method

.method J1()Lj3/a;
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Lj3/s;->e0:Lj3/a;

    goto/32 :goto_1

    nop

    :goto_1
    return-object v0
.end method

.method public L0()V
    .locals 1

    invoke-super {p0}, Landroidx/fragment/app/Fragment;->L0()V

    iget-object v0, p0, Lj3/s;->e0:Lj3/a;

    invoke-virtual {v0}, Lj3/a;->d()V

    return-void
.end method

.method public L1()Lcom/bumptech/glide/k;
    .locals 1

    iget-object v0, p0, Lj3/s;->i0:Lcom/bumptech/glide/k;

    return-object v0
.end method

.method public M0()V
    .locals 1

    invoke-super {p0}, Landroidx/fragment/app/Fragment;->M0()V

    iget-object v0, p0, Lj3/s;->e0:Lj3/a;

    invoke-virtual {v0}, Lj3/a;->e()V

    return-void
.end method

.method public M1()Lj3/q;
    .locals 1

    iget-object v0, p0, Lj3/s;->f0:Lj3/q;

    return-object v0
.end method

.method R1(Landroidx/fragment/app/Fragment;)V
    .locals 1

    goto/32 :goto_d

    nop

    :goto_0
    return-void

    :goto_1
    goto/32 :goto_3

    nop

    :goto_2
    if-eqz v0, :cond_0

    goto/32 :goto_8

    :cond_0
    goto/32 :goto_7

    nop

    :goto_3
    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->w()Landroid/content/Context;

    move-result-object p1

    goto/32 :goto_9

    nop

    :goto_4
    return-void

    :goto_5
    if-nez p1, :cond_1

    goto/32 :goto_a

    :cond_1
    goto/32 :goto_b

    nop

    :goto_6
    if-eqz v0, :cond_2

    goto/32 :goto_1

    :cond_2
    goto/32 :goto_0

    nop

    :goto_7
    goto :goto_a

    :goto_8
    goto/32 :goto_c

    nop

    :goto_9
    invoke-direct {p0, p1, v0}, Lj3/s;->P1(Landroid/content/Context;Landroidx/fragment/app/m;)V

    :goto_a
    goto/32 :goto_4

    nop

    :goto_b
    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->w()Landroid/content/Context;

    move-result-object v0

    goto/32 :goto_2

    nop

    :goto_c
    invoke-static {p1}, Lj3/s;->N1(Landroidx/fragment/app/Fragment;)Landroidx/fragment/app/m;

    move-result-object v0

    goto/32 :goto_6

    nop

    :goto_d
    iput-object p1, p0, Lj3/s;->j0:Landroidx/fragment/app/Fragment;

    goto/32 :goto_5

    nop
.end method

.method public S1(Lcom/bumptech/glide/k;)V
    .locals 0

    iput-object p1, p0, Lj3/s;->i0:Lcom/bumptech/glide/k;

    return-void
.end method

.method public l0(Landroid/content/Context;)V
    .locals 3

    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->l0(Landroid/content/Context;)V

    invoke-static {p0}, Lj3/s;->N1(Landroidx/fragment/app/Fragment;)Landroidx/fragment/app/m;

    move-result-object p1

    const/4 v0, 0x5

    const-string v1, "SupportRMFragment"

    if-nez p1, :cond_1

    invoke-static {v1, v0}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result p1

    if-eqz p1, :cond_0

    const-string p1, "Unable to register fragment with root, ancestor detached"

    invoke-static {v1, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void

    :cond_1
    :try_start_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->w()Landroid/content/Context;

    move-result-object v2

    invoke-direct {p0, v2, p1}, Lj3/s;->P1(Landroid/content/Context;Landroidx/fragment/app/m;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-static {v1, v0}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "Unable to register fragment with root"

    invoke-static {v1, v0, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_2
    :goto_0
    return-void
.end method

.method public t0()V
    .locals 1

    invoke-super {p0}, Landroidx/fragment/app/Fragment;->t0()V

    iget-object v0, p0, Lj3/s;->e0:Lj3/a;

    invoke-virtual {v0}, Lj3/a;->c()V

    invoke-direct {p0}, Lj3/s;->T1()V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Landroidx/fragment/app/Fragment;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "{parent="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-direct {p0}, Lj3/s;->K1()Landroidx/fragment/app/Fragment;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public w0()V
    .locals 1

    invoke-super {p0}, Landroidx/fragment/app/Fragment;->w0()V

    const/4 v0, 0x0

    iput-object v0, p0, Lj3/s;->j0:Landroidx/fragment/app/Fragment;

    invoke-direct {p0}, Lj3/s;->T1()V

    return-void
.end method
