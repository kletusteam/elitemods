.class public Lj3/o;
.super Landroid/app/Fragment;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lj3/o$a;
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final a:Lj3/a;

.field private final b:Lj3/q;

.field private final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lj3/o;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/bumptech/glide/k;

.field private e:Lj3/o;

.field private f:Landroid/app/Fragment;


# direct methods
.method public constructor <init>()V
    .locals 1

    new-instance v0, Lj3/a;

    invoke-direct {v0}, Lj3/a;-><init>()V

    invoke-direct {p0, v0}, Lj3/o;-><init>(Lj3/a;)V

    return-void
.end method

.method constructor <init>(Lj3/a;)V
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ValidFragment"
        }
    .end annotation

    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    new-instance v0, Lj3/o$a;

    invoke-direct {v0, p0}, Lj3/o$a;-><init>(Lj3/o;)V

    iput-object v0, p0, Lj3/o;->b:Lj3/q;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lj3/o;->c:Ljava/util/Set;

    iput-object p1, p0, Lj3/o;->a:Lj3/a;

    return-void
.end method

.method private a(Lj3/o;)V
    .locals 1

    iget-object v0, p0, Lj3/o;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private d()Landroid/app/Fragment;
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    invoke-virtual {p0}, Landroid/app/Fragment;->getParentFragment()Landroid/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lj3/o;->f:Landroid/app/Fragment;

    :goto_0
    return-object v0
.end method

.method private g(Landroid/app/Fragment;)Z
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    invoke-virtual {p0}, Landroid/app/Fragment;->getParentFragment()Landroid/app/Fragment;

    move-result-object v0

    :goto_0
    invoke-virtual {p1}, Landroid/app/Fragment;->getParentFragment()Landroid/app/Fragment;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1, v0}, Landroid/app/Fragment;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    invoke-virtual {p1}, Landroid/app/Fragment;->getParentFragment()Landroid/app/Fragment;

    move-result-object p1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    return p1
.end method

.method private h(Landroid/app/Activity;)V
    .locals 1

    invoke-direct {p0}, Lj3/o;->l()V

    invoke-static {p1}, Lcom/bumptech/glide/b;->c(Landroid/content/Context;)Lcom/bumptech/glide/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/bumptech/glide/b;->k()Lj3/p;

    move-result-object v0

    invoke-virtual {v0, p1}, Lj3/p;->i(Landroid/app/Activity;)Lj3/o;

    move-result-object p1

    iput-object p1, p0, Lj3/o;->e:Lj3/o;

    invoke-virtual {p0, p1}, Landroid/app/Fragment;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    iget-object p1, p0, Lj3/o;->e:Lj3/o;

    invoke-direct {p1, p0}, Lj3/o;->a(Lj3/o;)V

    :cond_0
    return-void
.end method

.method private i(Lj3/o;)V
    .locals 1

    iget-object v0, p0, Lj3/o;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method private l()V
    .locals 1

    iget-object v0, p0, Lj3/o;->e:Lj3/o;

    if-eqz v0, :cond_0

    invoke-direct {v0, p0}, Lj3/o;->i(Lj3/o;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lj3/o;->e:Lj3/o;

    :cond_0
    return-void
.end method


# virtual methods
.method b()Ljava/util/Set;
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lj3/o;",
            ">;"
        }
    .end annotation

    goto/32 :goto_f

    nop

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    goto/32 :goto_17

    nop

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_a

    nop

    :goto_2
    invoke-virtual {v1}, Lj3/o;->b()Ljava/util/Set;

    move-result-object v1

    goto/32 :goto_8

    nop

    :goto_3
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    goto/32 :goto_16

    nop

    :goto_4
    invoke-virtual {p0, v0}, Landroid/app/Fragment;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto/32 :goto_e

    nop

    :goto_5
    return-object v0

    :goto_6
    goto/32 :goto_12

    nop

    :goto_7
    iget-object v0, p0, Lj3/o;->c:Ljava/util/Set;

    goto/32 :goto_10

    nop

    :goto_8
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_9
    goto/32 :goto_0

    nop

    :goto_a
    check-cast v2, Lj3/o;

    goto/32 :goto_1d

    nop

    :goto_b
    return-object v0

    :goto_c
    goto/32 :goto_1b

    nop

    :goto_d
    invoke-direct {p0, v3}, Lj3/o;->g(Landroid/app/Fragment;)Z

    move-result v3

    goto/32 :goto_1c

    nop

    :goto_e
    if-nez v0, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_7

    nop

    :goto_f
    iget-object v0, p0, Lj3/o;->e:Lj3/o;

    goto/32 :goto_4

    nop

    :goto_10
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    goto/32 :goto_5

    nop

    :goto_11
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    goto/32 :goto_b

    nop

    :goto_12
    iget-object v0, p0, Lj3/o;->e:Lj3/o;

    goto/32 :goto_1a

    nop

    :goto_13
    goto :goto_9

    :goto_14
    goto/32 :goto_11

    nop

    :goto_15
    return-object v0

    :goto_16
    iget-object v1, p0, Lj3/o;->e:Lj3/o;

    goto/32 :goto_2

    nop

    :goto_17
    if-nez v2, :cond_1

    goto/32 :goto_14

    :cond_1
    goto/32 :goto_1

    nop

    :goto_18
    new-instance v0, Ljava/util/HashSet;

    goto/32 :goto_3

    nop

    :goto_19
    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/32 :goto_13

    nop

    :goto_1a
    if-nez v0, :cond_2

    goto/32 :goto_c

    :cond_2
    goto/32 :goto_18

    nop

    :goto_1b
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    goto/32 :goto_15

    nop

    :goto_1c
    if-nez v3, :cond_3

    goto/32 :goto_9

    :cond_3
    goto/32 :goto_19

    nop

    :goto_1d
    invoke-virtual {v2}, Landroid/app/Fragment;->getParentFragment()Landroid/app/Fragment;

    move-result-object v3

    goto/32 :goto_d

    nop
.end method

.method c()Lj3/a;
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return-object v0

    :goto_1
    iget-object v0, p0, Lj3/o;->a:Lj3/a;

    goto/32 :goto_0

    nop
.end method

.method public e()Lcom/bumptech/glide/k;
    .locals 1

    iget-object v0, p0, Lj3/o;->d:Lcom/bumptech/glide/k;

    return-object v0
.end method

.method public f()Lj3/q;
    .locals 1

    iget-object v0, p0, Lj3/o;->b:Lj3/q;

    return-object v0
.end method

.method j(Landroid/app/Fragment;)V
    .locals 1

    goto/32 :goto_5

    nop

    :goto_0
    invoke-virtual {p1}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object p1

    goto/32 :goto_3

    nop

    :goto_1
    if-nez p1, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_6

    nop

    :goto_2
    return-void

    :goto_3
    invoke-direct {p0, p1}, Lj3/o;->h(Landroid/app/Activity;)V

    :goto_4
    goto/32 :goto_2

    nop

    :goto_5
    iput-object p1, p0, Lj3/o;->f:Landroid/app/Fragment;

    goto/32 :goto_1

    nop

    :goto_6
    invoke-virtual {p1}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    goto/32 :goto_7

    nop

    :goto_7
    if-nez v0, :cond_1

    goto/32 :goto_4

    :cond_1
    goto/32 :goto_0

    nop
.end method

.method public k(Lcom/bumptech/glide/k;)V
    .locals 0

    iput-object p1, p0, Lj3/o;->d:Lcom/bumptech/glide/k;

    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/app/Fragment;->onAttach(Landroid/app/Activity;)V

    :try_start_0
    invoke-direct {p0, p1}, Lj3/o;->h(Landroid/app/Activity;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    const/4 v0, 0x5

    const-string v1, "RMFragment"

    invoke-static {v1, v0}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Unable to register fragment with root"

    invoke-static {v1, v0, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    :goto_0
    return-void
.end method

.method public onDestroy()V
    .locals 1

    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    iget-object v0, p0, Lj3/o;->a:Lj3/a;

    invoke-virtual {v0}, Lj3/a;->c()V

    invoke-direct {p0}, Lj3/o;->l()V

    return-void
.end method

.method public onDetach()V
    .locals 0

    invoke-super {p0}, Landroid/app/Fragment;->onDetach()V

    invoke-direct {p0}, Lj3/o;->l()V

    return-void
.end method

.method public onStart()V
    .locals 1

    invoke-super {p0}, Landroid/app/Fragment;->onStart()V

    iget-object v0, p0, Lj3/o;->a:Lj3/a;

    invoke-virtual {v0}, Lj3/a;->d()V

    return-void
.end method

.method public onStop()V
    .locals 1

    invoke-super {p0}, Landroid/app/Fragment;->onStop()V

    iget-object v0, p0, Lj3/o;->a:Lj3/a;

    invoke-virtual {v0}, Lj3/a;->e()V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Landroid/app/Fragment;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "{parent="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-direct {p0}, Lj3/o;->d()Landroid/app/Fragment;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
