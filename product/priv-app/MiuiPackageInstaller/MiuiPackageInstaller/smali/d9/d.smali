.class public Ld9/d;
.super Ld9/b;

# interfaces
.implements Lmiuix/animation/k;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ld9/d$a;
    }
.end annotation


# instance fields
.field private b:I

.field private c:Lc9/a;

.field private d:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v0, 0x0

    new-array v1, v0, [Lmiuix/animation/b;

    invoke-direct {p0, v1}, Ld9/b;-><init>([Lmiuix/animation/b;)V

    new-instance v1, Lc9/a;

    invoke-direct {v1}, Lc9/a;-><init>()V

    iput-object v1, p0, Ld9/d;->c:Lc9/a;

    const/4 v2, 0x3

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    invoke-static {v0, v2}, Lj9/c;->e(I[F)Lj9/c$a;

    move-result-object v0

    invoke-virtual {v1, v0}, Lc9/a;->l(Lj9/c$a;)Lc9/a;

    return-void

    nop

    :array_0
    .array-data 4
        0x43af0000    # 350.0f
        0x3f666666    # 0.9f
        0x3f5c28f6    # 0.86f
    .end array-data
.end method


# virtual methods
.method public varargs J(I[Lc9/a;)Lmiuix/animation/k;
    .locals 5

    iget-object v0, p0, Ld9/b;->a:Ld9/h;

    if-eqz v0, :cond_2

    iget-boolean v1, p0, Ld9/d;->d:Z

    const/4 v2, 0x1

    if-nez v1, :cond_0

    iput-boolean v2, p0, Ld9/d;->d:Z

    sget-object v1, Ld9/d$a;->a:Ld9/d$a;

    invoke-interface {v0, v1}, Lmiuix/animation/h;->C(Ljava/lang/Object;)Lmiuix/animation/h;

    :cond_0
    new-array v0, v2, [Lc9/a;

    const/4 v1, 0x0

    iget-object v2, p0, Ld9/d;->c:Lc9/a;

    aput-object v2, v0, v1

    invoke-static {p2, v0}, Lj9/a;->m([Ljava/lang/Object;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p2

    check-cast p2, [Lc9/a;

    iget v0, p0, Ld9/d;->b:I

    if-ne v0, p1, :cond_1

    iget-object p1, p0, Ld9/b;->a:Ld9/h;

    sget-object v0, Ld9/d$a;->a:Ld9/d$a;

    invoke-interface {p1, v0, p2}, Lmiuix/animation/h;->s(Ljava/lang/Object;[Lc9/a;)Lmiuix/animation/h;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Ld9/b;->a:Ld9/h;

    sget-object v1, Ld9/d$a;->b:Ld9/d$a;

    invoke-interface {v0, v1}, Ld9/h;->H(Ljava/lang/Object;)Ld9/a;

    move-result-object v0

    const/4 v2, 0x0

    int-to-double v3, p1

    invoke-virtual {v0, v2, v3, v4}, Ld9/a;->a(Ljava/lang/Object;D)Ld9/a;

    iget-object p1, p0, Ld9/b;->a:Ld9/h;

    invoke-interface {p1, v1, p2}, Lmiuix/animation/h;->s(Ljava/lang/Object;[Lc9/a;)Lmiuix/animation/h;

    :cond_2
    :goto_0
    return-object p0
.end method

.method public n()V
    .locals 1

    invoke-super {p0}, Ld9/b;->n()V

    const/4 v0, 0x0

    iput-object v0, p0, Ld9/b;->a:Ld9/h;

    const/4 v0, 0x0

    iput v0, p0, Ld9/d;->b:I

    return-void
.end method
