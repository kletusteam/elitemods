.class public Ld9/g;
.super Ld9/b;

# interfaces
.implements Lmiuix/animation/j;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ld9/g$h;,
        Ld9/g$f;,
        Ld9/g$e;,
        Ld9/g$g;
    }
.end annotation


# static fields
.field private static x:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap<",
            "Landroid/view/View;",
            "Ld9/g$f;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private b:Ld9/d;

.field private c:I

.field private d:I

.field private e:Landroid/view/View$OnClickListener;

.field private f:Landroid/view/View$OnLongClickListener;

.field private g:I

.field private h:F

.field private i:F

.field private j:Z

.field private k:Z

.field private l:Landroid/graphics/Rect;

.field private m:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lmiuix/animation/j$b;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private n:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private o:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private p:F

.field private q:Lc9/a;

.field private r:Lc9/a;

.field private s:Z

.field private t:Z

.field private u:Lf9/b;

.field private v:Ld9/g$h;

.field private w:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    sput-object v0, Ld9/g;->x:Ljava/util/WeakHashMap;

    return-void
.end method

.method public varargs constructor <init>([Lmiuix/animation/b;)V
    .locals 5

    invoke-direct {p0, p1}, Ld9/b;-><init>([Lmiuix/animation/b;)V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Ld9/g;->l:Landroid/graphics/Rect;

    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Ld9/g;->m:Ljava/util/Map;

    new-instance v0, Lc9/a;

    invoke-direct {v0}, Lc9/a;-><init>()V

    iput-object v0, p0, Ld9/g;->q:Lc9/a;

    new-instance v0, Lc9/a;

    invoke-direct {v0}, Lc9/a;-><init>()V

    iput-object v0, p0, Ld9/g;->r:Lc9/a;

    const/4 v0, 0x0

    iput-boolean v0, p0, Ld9/g;->t:Z

    new-instance v1, Ld9/g$a;

    invoke-direct {v1, p0}, Ld9/g$a;-><init>(Ld9/g;)V

    iput-object v1, p0, Ld9/g;->u:Lf9/b;

    array-length v1, p1

    if-lez v1, :cond_0

    aget-object p1, p1, v0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-direct {p0, p1}, Ld9/g;->d0(Lmiuix/animation/b;)V

    sget-object p1, Lh9/h;->e:Lh9/h;

    sget-object v1, Lh9/h;->f:Lh9/h;

    iget-object v2, p0, Ld9/b;->a:Ld9/h;

    sget-object v3, Lmiuix/animation/j$b;->a:Lmiuix/animation/j$b;

    invoke-interface {v2, v3}, Ld9/h;->H(Ljava/lang/Object;)Ld9/a;

    move-result-object v2

    const-wide/high16 v3, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v2, p1, v3, v4}, Ld9/a;->a(Ljava/lang/Object;D)Ld9/a;

    move-result-object p1

    invoke-virtual {p1, v1, v3, v4}, Ld9/a;->a(Ljava/lang/Object;D)Ld9/a;

    invoke-direct {p0}, Ld9/g;->t0()V

    iget-object p1, p0, Ld9/g;->q:Lc9/a;

    const/4 v1, 0x2

    new-array v2, v1, [F

    fill-array-data v2, :array_0

    const/4 v3, -0x2

    invoke-static {v3, v2}, Lj9/c;->e(I[F)Lj9/c$a;

    move-result-object v2

    invoke-virtual {p1, v2}, Lc9/a;->l(Lj9/c$a;)Lc9/a;

    iget-object p1, p0, Ld9/g;->q:Lc9/a;

    const/4 v2, 0x1

    new-array v2, v2, [Lf9/b;

    iget-object v4, p0, Ld9/g;->u:Lf9/b;

    aput-object v4, v2, v0

    invoke-virtual {p1, v2}, Lc9/a;->a([Lf9/b;)Lc9/a;

    iget-object p1, p0, Ld9/g;->r:Lc9/a;

    new-array v0, v1, [F

    fill-array-data v0, :array_1

    invoke-virtual {p1, v3, v0}, Lc9/a;->k(I[F)Lc9/a;

    move-result-object p1

    sget-object v0, Lh9/h;->o:Lh9/h;

    const-wide/16 v2, -0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_2

    invoke-virtual {p1, v0, v2, v3, v1}, Lc9/a;->n(Lh9/b;J[F)Lc9/a;

    return-void

    nop

    :array_0
    .array-data 4
        0x3f7d70a4    # 0.99f
        0x3e19999a    # 0.15f
    .end array-data

    :array_1
    .array-data 4
        0x3f7d70a4    # 0.99f
        0x3e99999a    # 0.3f
    .end array-data

    :array_2
    .array-data 4
        0x3f666666    # 0.9f
        0x3e4ccccd    # 0.2f
    .end array-data
.end method

.method static synthetic J(Ld9/g;Landroid/view/View;Z[Lc9/a;)Z
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Ld9/g;->R(Landroid/view/View;Z[Lc9/a;)Z

    move-result p0

    return p0
.end method

.method static synthetic K(Ld9/g;Landroid/view/View;Z)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ld9/g;->p0(Landroid/view/View;Z)V

    return-void
.end method

.method static synthetic L(Ld9/g;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Ld9/g;->e0(Landroid/view/View;)V

    return-void
.end method

.method static synthetic M(Ld9/g;)Z
    .locals 0

    iget-boolean p0, p0, Ld9/g;->w:Z

    return p0
.end method

.method static synthetic N(Ld9/g;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Ld9/g;->f0(Landroid/view/View;)V

    return-void
.end method

.method static synthetic O(Ld9/g;[Lc9/a;)V
    .locals 0

    invoke-direct {p0, p1}, Ld9/g;->l0([Lc9/a;)V

    return-void
.end method

.method static synthetic P(Ld9/g;Landroid/view/View;Landroid/view/MotionEvent;[Lc9/a;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Ld9/g;->a0(Landroid/view/View;Landroid/view/MotionEvent;[Lc9/a;)V

    return-void
.end method

.method static synthetic Q(Ld9/g;)Landroid/view/View$OnLongClickListener;
    .locals 0

    iget-object p0, p0, Ld9/g;->f:Landroid/view/View$OnLongClickListener;

    return-object p0
.end method

.method private varargs R(Landroid/view/View;Z[Lc9/a;)Z
    .locals 4

    iget-object v0, p0, Ld9/b;->a:Ld9/h;

    invoke-interface {v0}, Ld9/h;->g()Lmiuix/animation/b;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    invoke-direct {p0, p1}, Ld9/g;->U(Landroid/view/View;)Ld9/g$g;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v2, v0, Ld9/g$g;->a:Landroid/widget/AbsListView;

    if-eqz v2, :cond_1

    invoke-static {}, Lj9/f;->d()Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleListViewTouch for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v2, v1}, Lj9/f;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    iget-object v0, v0, Ld9/g$g;->a:Landroid/widget/AbsListView;

    invoke-direct {p0, v0, p1, p2, p3}, Ld9/g;->Z(Landroid/widget/AbsListView;Landroid/view/View;Z[Lc9/a;)V

    const/4 p1, 0x1

    return p1

    :cond_1
    return v1
.end method

.method private varargs S(Landroid/view/View;Landroid/view/View$OnClickListener;Landroid/view/View$OnLongClickListener;Z[Lc9/a;)V
    .locals 6

    invoke-direct {p0, p2, p3}, Ld9/g;->q0(Landroid/view/View$OnClickListener;Landroid/view/View$OnLongClickListener;)V

    invoke-direct {p0, p1, p5}, Ld9/g;->c0(Landroid/view/View;[Lc9/a;)V

    invoke-direct {p0, p1}, Ld9/g;->u0(Landroid/view/View;)Z

    move-result p2

    if-eqz p2, :cond_1

    invoke-static {}, Lj9/f;->d()Z

    move-result p2

    if-eqz p2, :cond_0

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "handleViewTouch for "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    const/4 p3, 0x0

    new-array p3, p3, [Ljava/lang/Object;

    invoke-static {p2, p3}, Lj9/f;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->isClickable()Z

    move-result v5

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Landroid/view/View;->setClickable(Z)V

    new-instance p2, Ld9/g$b;

    move-object v0, p2

    move-object v1, p0

    move v2, p4

    move-object v3, p1

    move-object v4, p5

    invoke-direct/range {v0 .. v5}, Ld9/g$b;-><init>(Ld9/g;ZLandroid/view/View;[Lc9/a;Z)V

    invoke-static {p1, p2}, Lj9/a;->p(Landroid/view/View;Ljava/lang/Runnable;)V

    :cond_1
    return-void
.end method

.method private varargs T([Lc9/a;)[Lc9/a;
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Lc9/a;

    iget-object v1, p0, Ld9/g;->q:Lc9/a;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static {p1, v0}, Lj9/a;->m([Ljava/lang/Object;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Lc9/a;

    return-object p1
.end method

.method private U(Landroid/view/View;)Ld9/g$g;
    .locals 4

    new-instance v0, Ld9/g$g;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ld9/g$g;-><init>(Ld9/g$a;)V

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    :goto_0
    if-eqz v2, :cond_2

    instance-of v3, v2, Landroid/widget/AbsListView;

    if-eqz v3, :cond_0

    move-object v1, v2

    check-cast v1, Landroid/widget/AbsListView;

    goto :goto_1

    :cond_0
    instance-of v3, v2, Landroid/view/View;

    if-eqz v3, :cond_1

    move-object p1, v2

    check-cast p1, Landroid/view/View;

    :cond_1
    invoke-interface {v2}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    goto :goto_0

    :cond_2
    :goto_1
    if-eqz v1, :cond_3

    new-instance v2, Ljava/lang/ref/WeakReference;

    iget-object v3, v0, Ld9/g$g;->a:Landroid/widget/AbsListView;

    invoke-direct {v2, v3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v2, p0, Ld9/g;->o:Ljava/lang/ref/WeakReference;

    iput-object v1, v0, Ld9/g$g;->a:Landroid/widget/AbsListView;

    iput-object p1, v0, Ld9/g$g;->b:Landroid/view/View;

    :cond_3
    return-object v0
.end method

.method public static V(Landroid/widget/AbsListView;)Ld9/i;
    .locals 1

    sget v0, Lfa/b;->a:I

    invoke-virtual {p0, v0}, Landroid/widget/AbsListView;->getTag(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ld9/i;

    return-object p0
.end method

.method private varargs W([Lmiuix/animation/j$b;)Lmiuix/animation/j$b;
    .locals 1

    array-length v0, p1

    if-lez v0, :cond_0

    const/4 v0, 0x0

    aget-object p1, p1, v0

    goto :goto_0

    :cond_0
    sget-object p1, Lmiuix/animation/j$b;->b:Lmiuix/animation/j$b;

    :goto_0
    return-object p1
.end method

.method private varargs X([Lc9/a;)[Lc9/a;
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Lc9/a;

    iget-object v1, p0, Ld9/g;->r:Lc9/a;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static {p1, v0}, Lj9/a;->m([Ljava/lang/Object;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Lc9/a;

    return-object p1
.end method

.method private Y(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 2

    iget-boolean v0, p0, Ld9/g;->k:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Ld9/g;->e:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    iget v0, p0, Ld9/g;->g:I

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Ld9/b;->a:Ld9/h;

    invoke-interface {v0}, Ld9/h;->g()Lmiuix/animation/b;

    move-result-object v0

    instance-of v1, v0, Lmiuix/animation/ViewTarget;

    if-eqz v1, :cond_0

    invoke-direct {p0, p1, p2}, Ld9/g;->g0(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result p1

    if-eqz p1, :cond_0

    check-cast v0, Lmiuix/animation/ViewTarget;

    invoke-virtual {v0}, Lmiuix/animation/ViewTarget;->z()Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/View;->performClick()Z

    invoke-direct {p0, p1}, Ld9/g;->e0(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method private varargs Z(Landroid/widget/AbsListView;Landroid/view/View;Z[Lc9/a;)V
    .locals 2

    invoke-static {p1}, Ld9/g;->V(Landroid/widget/AbsListView;)Ld9/i;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ld9/i;

    invoke-direct {v0, p1}, Ld9/i;-><init>(Landroid/widget/AbsListView;)V

    sget v1, Lfa/b;->a:I

    invoke-virtual {p1, v1, v0}, Landroid/widget/AbsListView;->setTag(ILjava/lang/Object;)V

    :cond_0
    if-eqz p3, :cond_1

    invoke-virtual {p1, v0}, Landroid/widget/AbsListView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    :cond_1
    new-instance p1, Ld9/g$e;

    invoke-direct {p1, p0, p4}, Ld9/g$e;-><init>(Ld9/g;[Lc9/a;)V

    invoke-virtual {v0, p2, p1}, Ld9/i;->c(Landroid/view/View;Landroid/view/View$OnTouchListener;)V

    return-void
.end method

.method private varargs a0(Landroid/view/View;Landroid/view/MotionEvent;[Lc9/a;)V
    .locals 2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    invoke-direct {p0, p2, p1, p3}, Ld9/g;->k0(Landroid/view/MotionEvent;Landroid/view/View;[Lc9/a;)V

    goto :goto_1

    :cond_1
    invoke-direct {p0, p1, p2}, Ld9/g;->Y(Landroid/view/View;Landroid/view/MotionEvent;)V

    :goto_0
    invoke-direct {p0, p3}, Ld9/g;->l0([Lc9/a;)V

    goto :goto_1

    :cond_2
    invoke-direct {p0, p2}, Ld9/g;->m0(Landroid/view/MotionEvent;)V

    invoke-direct {p0, p3}, Ld9/g;->j0([Lc9/a;)V

    :goto_1
    return-void
.end method

.method private varargs c0(Landroid/view/View;[Lc9/a;)V
    .locals 2

    sget-object v0, Ld9/g;->x:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ld9/g$f;

    if-nez v0, :cond_0

    new-instance v0, Ld9/g$f;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ld9/g$f;-><init>(Ld9/g$a;)V

    sget-object v1, Ld9/g;->x:Ljava/util/WeakHashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-virtual {p1, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    invoke-virtual {v0, p0, p2}, Ld9/g$f;->a(Ld9/g;[Lc9/a;)V

    return-void
.end method

.method private d0(Lmiuix/animation/b;)V
    .locals 2

    instance-of v0, p1, Lmiuix/animation/ViewTarget;

    if-eqz v0, :cond_0

    check-cast p1, Lmiuix/animation/ViewTarget;

    invoke-virtual {p1}, Lmiuix/animation/ViewTarget;->z()Landroid/view/View;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_1

    const/4 v0, 0x1

    const/high16 v1, 0x41200000    # 10.0f

    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p1

    invoke-static {v0, v1, p1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result p1

    iput p1, p0, Ld9/g;->p:F

    :cond_1
    return-void
.end method

.method private e0(Landroid/view/View;)V
    .locals 1

    iget-boolean v0, p0, Ld9/g;->j:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Ld9/g;->w:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Ld9/g;->j:Z

    iget-object v0, p0, Ld9/g;->e:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method private f0(Landroid/view/View;)V
    .locals 1

    iget-boolean v0, p0, Ld9/g;->w:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Ld9/g;->w:Z

    iget-object v0, p0, Ld9/g;->f:Landroid/view/View$OnLongClickListener;

    invoke-interface {v0, p1}, Landroid/view/View$OnLongClickListener;->onLongClick(Landroid/view/View;)Z

    :cond_0
    return-void
.end method

.method private g0(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result p2

    iget v1, p0, Ld9/g;->h:F

    iget v2, p0, Ld9/g;->i:F

    invoke-static {v1, v2, v0, p2}, Lj9/a;->d(FFFF)D

    move-result-wide v0

    invoke-static {p1}, Lj9/a;->g(Landroid/view/View;)F

    move-result p1

    float-to-double p1, p1

    cmpg-double p1, v0, p1

    if-gez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method static h0(Landroid/view/View;Landroid/graphics/Rect;Landroid/view/MotionEvent;)Z
    .locals 0

    if-eqz p0, :cond_0

    invoke-virtual {p0, p1}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result p0

    float-to-int p0, p0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result p2

    float-to-int p2, p2

    invoke-virtual {p1, p0, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result p0

    return p0

    :cond_0
    const/4 p0, 0x1

    return p0
.end method

.method private i0(Lmiuix/animation/j$b;)Z
    .locals 2

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v1, p0, Ld9/g;->m:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method private varargs j0([Lc9/a;)V
    .locals 2

    invoke-static {}, Lj9/f;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "onEventDown, touchDown"

    invoke-static {v1, v0}, Lj9/f;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Ld9/g;->k:Z

    invoke-virtual {p0, p1}, Ld9/g;->q([Lc9/a;)V

    return-void
.end method

.method private varargs k0(Landroid/view/MotionEvent;Landroid/view/View;[Lc9/a;)V
    .locals 3

    iget-boolean v0, p0, Ld9/g;->k:Z

    if-eqz v0, :cond_3

    invoke-static {}, Lj9/f;->d()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    new-array v0, v1, [Ljava/lang/Object;

    const-string v2, "onEventMove"

    invoke-static {v2, v0}, Lj9/f;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    iget-object v0, p0, Ld9/g;->l:Landroid/graphics/Rect;

    invoke-static {p2, v0, p1}, Ld9/g;->h0(Landroid/view/View;Landroid/graphics/Rect;Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {}, Lj9/f;->d()Z

    move-result p2

    if-eqz p2, :cond_1

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "onEventMove -> touchUp isInTouchSlop(view, event) "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-array p2, v1, [Ljava/lang/Object;

    invoke-static {p1, p2}, Lj9/f;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_1
    invoke-virtual {p0, p3}, Ld9/g;->h([Lc9/a;)V

    invoke-direct {p0}, Ld9/g;->n0()V

    goto :goto_0

    :cond_2
    iget-object p3, p0, Ld9/g;->v:Ld9/g$h;

    if-eqz p3, :cond_3

    invoke-direct {p0, p2, p1}, Ld9/g;->g0(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result p1

    if-nez p1, :cond_3

    iget-object p1, p0, Ld9/g;->v:Ld9/g$h;

    invoke-virtual {p1, p0}, Ld9/g$h;->b(Ld9/g;)V

    :cond_3
    :goto_0
    return-void
.end method

.method private varargs l0([Lc9/a;)V
    .locals 2

    iget-boolean v0, p0, Ld9/g;->k:Z

    if-eqz v0, :cond_1

    invoke-static {}, Lj9/f;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "onEventUp, touchUp"

    invoke-static {v1, v0}, Lj9/f;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    invoke-virtual {p0, p1}, Ld9/g;->h([Lc9/a;)V

    invoke-direct {p0}, Ld9/g;->n0()V

    :cond_1
    return-void
.end method

.method private m0(Landroid/view/MotionEvent;)V
    .locals 1

    iget-object v0, p0, Ld9/g;->e:Landroid/view/View$OnClickListener;

    if-nez v0, :cond_0

    iget-object v0, p0, Ld9/g;->f:Landroid/view/View$OnLongClickListener;

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v0

    iput v0, p0, Ld9/g;->g:I

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    iput v0, p0, Ld9/g;->h:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result p1

    iput p1, p0, Ld9/g;->i:F

    const/4 p1, 0x0

    iput-boolean p1, p0, Ld9/g;->j:Z

    iput-boolean p1, p0, Ld9/g;->w:Z

    invoke-direct {p0}, Ld9/g;->v0()V

    :cond_1
    return-void
.end method

.method private n0()V
    .locals 1

    iget-object v0, p0, Ld9/g;->v:Ld9/g$h;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, Ld9/g$h;->b(Ld9/g;)V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Ld9/g;->k:Z

    iput v0, p0, Ld9/g;->g:I

    const/4 v0, 0x0

    iput v0, p0, Ld9/g;->h:F

    iput v0, p0, Ld9/g;->i:F

    return-void
.end method

.method private o0(Ljava/lang/ref/WeakReference;)Landroid/view/View;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/WeakReference<",
            "Landroid/view/View;",
            ">;)",
            "Landroid/view/View;"
        }
    .end annotation

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    :cond_0
    return-object p1
.end method

.method private p0(Landroid/view/View;Z)V
    .locals 0

    invoke-virtual {p1, p2}, Landroid/view/View;->setClickable(Z)V

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    return-void
.end method

.method private q0(Landroid/view/View$OnClickListener;Landroid/view/View$OnLongClickListener;)V
    .locals 3

    iget-object v0, p0, Ld9/b;->a:Ld9/h;

    invoke-interface {v0}, Ld9/h;->g()Lmiuix/animation/b;

    move-result-object v0

    instance-of v1, v0, Lmiuix/animation/ViewTarget;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    check-cast v0, Lmiuix/animation/ViewTarget;

    invoke-virtual {v0}, Lmiuix/animation/ViewTarget;->z()Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v2

    :goto_0
    if-nez v0, :cond_1

    return-void

    :cond_1
    iget-object v1, p0, Ld9/g;->e:Landroid/view/View$OnClickListener;

    if-eqz v1, :cond_2

    if-nez p1, :cond_2

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    :cond_2
    if-eqz p1, :cond_3

    new-instance v1, Ld9/g$c;

    invoke-direct {v1, p0}, Ld9/g$c;-><init>(Ld9/g;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_3
    :goto_1
    iput-object p1, p0, Ld9/g;->e:Landroid/view/View$OnClickListener;

    iget-object p1, p0, Ld9/g;->f:Landroid/view/View$OnLongClickListener;

    if-eqz p1, :cond_4

    if-nez p2, :cond_4

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    goto :goto_2

    :cond_4
    if-eqz p2, :cond_5

    new-instance p1, Ld9/g$d;

    invoke-direct {p1, p0}, Ld9/g$d;-><init>(Ld9/g;)V

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    :cond_5
    :goto_2
    iput-object p2, p0, Ld9/g;->f:Landroid/view/View$OnLongClickListener;

    return-void
.end method

.method private s0(F)V
    .locals 2

    iget-object v0, p0, Ld9/b;->a:Ld9/h;

    invoke-interface {v0}, Ld9/h;->g()Lmiuix/animation/b;

    move-result-object v0

    invoke-virtual {v0}, Lmiuix/animation/b;->h()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Landroid/view/View;

    if-eqz v1, :cond_0

    check-cast v0, Landroid/view/View;

    sget v1, Lmiuix/animation/m;->e:I

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private t0()V
    .locals 5

    iget-boolean v0, p0, Ld9/g;->s:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Ld9/g;->t:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/16 v0, 0x14

    const/4 v1, 0x0

    invoke-static {v0, v1, v1, v1}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    iget-object v1, p0, Ld9/b;->a:Ld9/h;

    invoke-interface {v1}, Ld9/h;->g()Lmiuix/animation/b;

    move-result-object v1

    invoke-virtual {v1}, Lmiuix/animation/b;->h()Ljava/lang/Object;

    move-result-object v1

    instance-of v2, v1, Landroid/view/View;

    if-eqz v2, :cond_1

    check-cast v1, Landroid/view/View;

    sget v0, Lfa/a;->b:I

    invoke-virtual {v1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    :cond_1
    sget-object v1, Lh9/i;->a:Lh9/i$c;

    iget-object v2, p0, Ld9/b;->a:Ld9/h;

    sget-object v3, Lmiuix/animation/j$b;->b:Lmiuix/animation/j$b;

    invoke-interface {v2, v3}, Ld9/h;->H(Ljava/lang/Object;)Ld9/a;

    move-result-object v2

    int-to-double v3, v0

    invoke-virtual {v2, v1, v3, v4}, Ld9/a;->a(Ljava/lang/Object;D)Ld9/a;

    iget-object v0, p0, Ld9/b;->a:Ld9/h;

    sget-object v2, Lmiuix/animation/j$b;->a:Lmiuix/animation/j$b;

    invoke-interface {v0, v2}, Ld9/h;->H(Ljava/lang/Object;)Ld9/a;

    move-result-object v0

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Ld9/a;->a(Ljava/lang/Object;D)Ld9/a;

    :cond_2
    :goto_0
    return-void
.end method

.method private u0(Landroid/view/View;)Z
    .locals 1

    iget-object v0, p0, Ld9/g;->n:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-ne v0, p1, :cond_1

    const/4 p1, 0x0

    return p1

    :cond_1
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Ld9/g;->n:Ljava/lang/ref/WeakReference;

    const/4 p1, 0x1

    return p1
.end method

.method private v0()V
    .locals 2

    iget-object v0, p0, Ld9/g;->f:Landroid/view/View$OnLongClickListener;

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Ld9/g;->v:Ld9/g$h;

    if-nez v0, :cond_1

    new-instance v0, Ld9/g$h;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ld9/g$h;-><init>(Ld9/g$a;)V

    iput-object v0, p0, Ld9/g;->v:Ld9/g$h;

    :cond_1
    iget-object v0, p0, Ld9/g;->v:Ld9/g$h;

    invoke-virtual {v0, p0}, Ld9/g$h;->a(Ld9/g;)V

    return-void
.end method


# virtual methods
.method public F(I)Lmiuix/animation/j;
    .locals 1

    iget-object v0, p0, Ld9/g;->q:Lc9/a;

    invoke-virtual {v0, p1}, Lc9/a;->r(I)Lc9/a;

    iget-object v0, p0, Ld9/g;->r:Lc9/a;

    invoke-virtual {v0, p1}, Lc9/a;->r(I)Lc9/a;

    return-object p0
.end method

.method public varargs G(F[Lmiuix/animation/j$b;)Lmiuix/animation/j;
    .locals 3

    invoke-direct {p0, p2}, Ld9/g;->W([Lmiuix/animation/j$b;)Lmiuix/animation/j$b;

    move-result-object p2

    iget-object v0, p0, Ld9/g;->m:Ljava/util/Map;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-interface {v0, p2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Ld9/b;->a:Ld9/h;

    invoke-interface {v0, p2}, Ld9/h;->H(Ljava/lang/Object;)Ld9/a;

    move-result-object p2

    sget-object v0, Lh9/h;->e:Lh9/h;

    float-to-double v1, p1

    invoke-virtual {p2, v0, v1, v2}, Ld9/a;->a(Ljava/lang/Object;D)Ld9/a;

    move-result-object p1

    sget-object p2, Lh9/h;->f:Lh9/h;

    invoke-virtual {p1, p2, v1, v2}, Ld9/a;->a(Ljava/lang/Object;D)Ld9/a;

    return-object p0
.end method

.method public a(FFFF)Lmiuix/animation/j;
    .locals 1

    const/high16 v0, 0x437f0000    # 255.0f

    mul-float/2addr p1, v0

    float-to-int p1, p1

    mul-float/2addr p2, v0

    float-to-int p2, p2

    mul-float/2addr p3, v0

    float-to-int p3, p3

    mul-float/2addr p4, v0

    float-to-int p4, p4

    invoke-static {p1, p2, p3, p4}, Landroid/graphics/Color;->argb(IIII)I

    move-result p1

    invoke-virtual {p0, p1}, Ld9/g;->m(I)Lmiuix/animation/j;

    move-result-object p1

    return-object p1
.end method

.method public b(FFFF)Lmiuix/animation/j;
    .locals 1

    const/high16 v0, 0x437f0000    # 255.0f

    mul-float/2addr p1, v0

    float-to-int p1, p1

    mul-float/2addr p2, v0

    float-to-int p2, p2

    mul-float/2addr p3, v0

    float-to-int p3, p3

    mul-float/2addr p4, v0

    float-to-int p4, p4

    invoke-static {p1, p2, p3, p4}, Landroid/graphics/Color;->argb(IIII)I

    move-result p1

    invoke-virtual {p0, p1}, Ld9/g;->i(I)Lmiuix/animation/j;

    move-result-object p1

    return-object p1
.end method

.method public varargs b0(Landroid/view/View;Z[Lc9/a;)V
    .locals 6

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Ld9/g;->S(Landroid/view/View;Landroid/view/View$OnClickListener;Landroid/view/View$OnLongClickListener;Z[Lc9/a;)V

    return-void
.end method

.method public varargs d(F[Lmiuix/animation/j$b;)Lmiuix/animation/j;
    .locals 3

    iget-object v0, p0, Ld9/b;->a:Ld9/h;

    invoke-direct {p0, p2}, Ld9/g;->W([Lmiuix/animation/j$b;)Lmiuix/animation/j$b;

    move-result-object p2

    invoke-interface {v0, p2}, Ld9/h;->H(Ljava/lang/Object;)Ld9/a;

    move-result-object p2

    sget-object v0, Lh9/h;->o:Lh9/h;

    float-to-double v1, p1

    invoke-virtual {p2, v0, v1, v2}, Ld9/a;->a(Ljava/lang/Object;D)Ld9/a;

    return-object p0
.end method

.method public varargs h([Lc9/a;)V
    .locals 2

    invoke-direct {p0, p1}, Ld9/g;->X([Lc9/a;)[Lc9/a;

    move-result-object p1

    iget-object v0, p0, Ld9/g;->b:Ld9/d;

    if-eqz v0, :cond_0

    iget v1, p0, Ld9/g;->c:I

    invoke-virtual {v0, v1, p1}, Ld9/d;->J(I[Lc9/a;)Lmiuix/animation/k;

    :cond_0
    iget-object v0, p0, Ld9/b;->a:Ld9/h;

    sget-object v1, Lmiuix/animation/j$b;->a:Lmiuix/animation/j$b;

    invoke-interface {v0, v1}, Ld9/h;->H(Ljava/lang/Object;)Ld9/a;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lmiuix/animation/h;->s(Ljava/lang/Object;[Lc9/a;)Lmiuix/animation/h;

    return-void
.end method

.method public i(I)Lmiuix/animation/j;
    .locals 4

    sget-object v0, Lh9/i;->b:Lh9/i$b;

    iget-object v1, p0, Ld9/b;->a:Ld9/h;

    sget-object v2, Lmiuix/animation/j$b;->b:Lmiuix/animation/j$b;

    invoke-interface {v1, v2}, Ld9/h;->H(Ljava/lang/Object;)Ld9/a;

    move-result-object v1

    int-to-double v2, p1

    invoke-virtual {v1, v0, v2, v3}, Ld9/a;->a(Ljava/lang/Object;D)Ld9/a;

    iget-object p1, p0, Ld9/b;->a:Ld9/h;

    sget-object v1, Lmiuix/animation/j$b;->a:Lmiuix/animation/j$b;

    invoke-interface {p1, v1}, Ld9/h;->H(Ljava/lang/Object;)Ld9/a;

    move-result-object p1

    iget-object v1, p0, Ld9/b;->a:Ld9/h;

    invoke-interface {v1}, Ld9/h;->g()Lmiuix/animation/b;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-static {v1, v0, v2, v3}, Le9/j;->c(Lmiuix/animation/b;Lh9/b;D)D

    move-result-wide v1

    double-to-int v1, v1

    int-to-double v1, v1

    invoke-virtual {p1, v0, v1, v2}, Ld9/a;->a(Ljava/lang/Object;D)Ld9/a;

    return-object p0
.end method

.method public m(I)Lmiuix/animation/j;
    .locals 4

    const/4 v0, 0x1

    iput-boolean v0, p0, Ld9/g;->s:Z

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Ld9/g;->t:Z

    iget-object v0, p0, Ld9/b;->a:Ld9/h;

    sget-object v1, Lmiuix/animation/j$b;->b:Lmiuix/animation/j$b;

    invoke-interface {v0, v1}, Ld9/h;->H(Ljava/lang/Object;)Ld9/a;

    move-result-object v0

    sget-object v1, Lh9/i;->a:Lh9/i$c;

    int-to-double v2, p1

    invoke-virtual {v0, v1, v2, v3}, Ld9/a;->a(Ljava/lang/Object;D)Ld9/a;

    return-object p0
.end method

.method public n()V
    .locals 3

    invoke-super {p0}, Ld9/b;->n()V

    iget-object v0, p0, Ld9/g;->b:Ld9/d;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ld9/d;->n()V

    :cond_0
    iget-object v0, p0, Ld9/g;->m:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iget-object v0, p0, Ld9/g;->n:Ljava/lang/ref/WeakReference;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    invoke-direct {p0, v0}, Ld9/g;->o0(Ljava/lang/ref/WeakReference;)Landroid/view/View;

    iput-object v1, p0, Ld9/g;->n:Ljava/lang/ref/WeakReference;

    :cond_1
    iget-object v0, p0, Ld9/g;->o:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_3

    invoke-direct {p0, v0}, Ld9/g;->o0(Ljava/lang/ref/WeakReference;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    sget v2, Lfa/b;->a:I

    invoke-virtual {v0, v2, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    :cond_2
    iput-object v1, p0, Ld9/g;->o:Ljava/lang/ref/WeakReference;

    :cond_3
    invoke-direct {p0}, Ld9/g;->n0()V

    return-void
.end method

.method public varargs q([Lc9/a;)V
    .locals 5

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Ld9/g;->s0(F)V

    invoke-direct {p0}, Ld9/g;->t0()V

    invoke-direct {p0, p1}, Ld9/g;->T([Lc9/a;)[Lc9/a;

    move-result-object p1

    iget-object v0, p0, Ld9/g;->b:Ld9/d;

    if-eqz v0, :cond_0

    iget v1, p0, Ld9/g;->d:I

    invoke-virtual {v0, v1, p1}, Ld9/d;->J(I[Lc9/a;)Lmiuix/animation/k;

    :cond_0
    iget-object v0, p0, Ld9/b;->a:Ld9/h;

    sget-object v1, Lmiuix/animation/j$b;->b:Lmiuix/animation/j$b;

    invoke-interface {v0, v1}, Ld9/h;->H(Ljava/lang/Object;)Ld9/a;

    move-result-object v0

    invoke-direct {p0, v1}, Ld9/g;->i0(Lmiuix/animation/j$b;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Ld9/b;->a:Ld9/h;

    invoke-interface {v1}, Ld9/h;->g()Lmiuix/animation/b;

    move-result-object v1

    sget-object v2, Lh9/h;->n:Lh9/h;

    invoke-virtual {v1, v2}, Lmiuix/animation/b;->i(Lh9/b;)F

    move-result v2

    sget-object v3, Lh9/h;->m:Lh9/h;

    invoke-virtual {v1, v3}, Lmiuix/animation/b;->i(Lh9/b;)F

    move-result v1

    invoke-static {v2, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    iget v2, p0, Ld9/g;->p:F

    sub-float v2, v1, v2

    div-float/2addr v2, v1

    const v1, 0x3f666666    # 0.9f

    invoke-static {v2, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    sget-object v2, Lh9/h;->e:Lh9/h;

    float-to-double v3, v1

    invoke-virtual {v0, v2, v3, v4}, Ld9/a;->a(Ljava/lang/Object;D)Ld9/a;

    move-result-object v1

    sget-object v2, Lh9/h;->f:Lh9/h;

    invoke-virtual {v1, v2, v3, v4}, Ld9/a;->a(Ljava/lang/Object;D)Ld9/a;

    :cond_1
    iget-object v1, p0, Ld9/b;->a:Ld9/h;

    invoke-interface {v1, v0, p1}, Lmiuix/animation/h;->s(Ljava/lang/Object;[Lc9/a;)Lmiuix/animation/h;

    return-void
.end method

.method public r0(Ld9/d;)V
    .locals 0

    iput-object p1, p0, Ld9/g;->b:Ld9/d;

    return-void
.end method

.method public varargs z(Landroid/view/View;[Lc9/a;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Ld9/g;->b0(Landroid/view/View;Z[Lc9/a;)V

    return-void
.end method
