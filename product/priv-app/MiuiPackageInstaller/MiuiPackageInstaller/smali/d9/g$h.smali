.class final Ld9/g$h;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ld9/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "h"
.end annotation


# instance fields
.field private a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Ld9/g;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Ld9/g$a;)V
    .locals 0

    invoke-direct {p0}, Ld9/g$h;-><init>()V

    return-void
.end method


# virtual methods
.method a(Ld9/g;)V
    .locals 3

    goto/32 :goto_2

    nop

    :goto_0
    invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    goto/32 :goto_6

    nop

    :goto_1
    new-instance v1, Ljava/lang/ref/WeakReference;

    goto/32 :goto_0

    nop

    :goto_2
    iget-object v0, p1, Ld9/b;->a:Ld9/h;

    goto/32 :goto_d

    nop

    :goto_3
    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result p1

    goto/32 :goto_e

    nop

    :goto_4
    if-nez v0, :cond_0

    goto/32 :goto_b

    :cond_0
    goto/32 :goto_1

    nop

    :goto_5
    invoke-virtual {v0}, Lmiuix/animation/ViewTarget;->z()Landroid/view/View;

    move-result-object v0

    goto/32 :goto_4

    nop

    :goto_6
    iput-object v1, p0, Ld9/g$h;->a:Ljava/lang/ref/WeakReference;

    goto/32 :goto_3

    nop

    :goto_7
    instance-of v1, v0, Lmiuix/animation/ViewTarget;

    goto/32 :goto_9

    nop

    :goto_8
    return-void

    :goto_9
    if-nez v1, :cond_1

    goto/32 :goto_b

    :cond_1
    goto/32 :goto_c

    nop

    :goto_a
    invoke-virtual {v0, p0, v1, v2}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    :goto_b
    goto/32 :goto_8

    nop

    :goto_c
    check-cast v0, Lmiuix/animation/ViewTarget;

    goto/32 :goto_5

    nop

    :goto_d
    invoke-interface {v0}, Ld9/h;->g()Lmiuix/animation/b;

    move-result-object v0

    goto/32 :goto_7

    nop

    :goto_e
    int-to-long v1, p1

    goto/32 :goto_a

    nop
.end method

.method b(Ld9/g;)V
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    if-nez p1, :cond_0

    goto/32 :goto_8

    :cond_0
    goto/32 :goto_7

    nop

    :goto_1
    check-cast p1, Lmiuix/animation/ViewTarget;

    goto/32 :goto_9

    nop

    :goto_2
    iget-object p1, p1, Ld9/b;->a:Ld9/h;

    goto/32 :goto_6

    nop

    :goto_3
    instance-of v0, p1, Lmiuix/animation/ViewTarget;

    goto/32 :goto_4

    nop

    :goto_4
    if-nez v0, :cond_1

    goto/32 :goto_8

    :cond_1
    goto/32 :goto_1

    nop

    :goto_5
    return-void

    :goto_6
    invoke-interface {p1}, Ld9/h;->g()Lmiuix/animation/b;

    move-result-object p1

    goto/32 :goto_3

    nop

    :goto_7
    invoke-virtual {p1, p0}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    :goto_8
    goto/32 :goto_5

    nop

    :goto_9
    invoke-virtual {p1}, Lmiuix/animation/ViewTarget;->z()Landroid/view/View;

    move-result-object p1

    goto/32 :goto_0

    nop
.end method

.method public run()V
    .locals 3

    iget-object v0, p0, Ld9/g$h;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ld9/g;

    if-eqz v0, :cond_0

    iget-object v1, v0, Ld9/b;->a:Ld9/h;

    invoke-interface {v1}, Ld9/h;->g()Lmiuix/animation/b;

    move-result-object v1

    instance-of v2, v1, Lmiuix/animation/ViewTarget;

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lmiuix/animation/b;->h()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    if-eqz v1, :cond_0

    invoke-static {v0}, Ld9/g;->Q(Ld9/g;)Landroid/view/View$OnLongClickListener;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->performLongClick()Z

    invoke-static {v0, v1}, Ld9/g;->N(Ld9/g;Landroid/view/View;)V

    :cond_0
    return-void
.end method
