.class public Ld9/j;
.super Ljava/lang/Object;


# static fields
.field private static final a:Lj9/h$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lj9/h$b<",
            "Ld9/h;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ld9/j$a;

    invoke-direct {v0}, Ld9/j$a;-><init>()V

    sput-object v0, Ld9/j;->a:Lj9/h$b;

    return-void
.end method

.method public static varargs a([Lmiuix/animation/b;)Ld9/h;
    .locals 4

    if-eqz p0, :cond_3

    array-length v0, p0

    if-nez v0, :cond_0

    goto :goto_1

    :cond_0
    array-length v0, p0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    new-instance v0, Ld9/f;

    aget-object p0, p0, v1

    invoke-direct {v0, p0}, Ld9/f;-><init>(Lmiuix/animation/b;)V

    return-object v0

    :cond_1
    array-length v0, p0

    new-array v0, v0, [Ld9/f;

    :goto_0
    array-length v2, p0

    if-ge v1, v2, :cond_2

    new-instance v2, Ld9/f;

    aget-object v3, p0, v1

    invoke-direct {v2, v3}, Ld9/f;-><init>(Lmiuix/animation/b;)V

    aput-object v2, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    const-class p0, Ld9/h;

    sget-object v1, Ld9/j;->a:Lj9/h$b;

    invoke-static {p0, v1, v0}, Lj9/h;->a(Ljava/lang/Class;Lj9/h$b;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ld9/h;

    return-object p0

    :cond_3
    :goto_1
    const/4 p0, 0x0

    return-object p0
.end method
