.class Ld9/g$e;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ld9/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "e"
.end annotation


# instance fields
.field private a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Ld9/g;",
            ">;"
        }
    .end annotation
.end field

.field private b:[Lc9/a;


# direct methods
.method varargs constructor <init>(Ld9/g;[Lc9/a;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Ld9/g$e;->a:Ljava/lang/ref/WeakReference;

    iput-object p2, p0, Ld9/g$e;->b:[Lc9/a;

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2

    iget-object v0, p0, Ld9/g$e;->a:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ld9/g;

    :goto_0
    if-eqz v0, :cond_2

    if-nez p2, :cond_1

    iget-object p1, p0, Ld9/g$e;->b:[Lc9/a;

    invoke-static {v0, p1}, Ld9/g;->O(Ld9/g;[Lc9/a;)V

    goto :goto_1

    :cond_1
    iget-object v1, p0, Ld9/g$e;->b:[Lc9/a;

    invoke-static {v0, p1, p2, v1}, Ld9/g;->P(Ld9/g;Landroid/view/View;Landroid/view/MotionEvent;[Lc9/a;)V

    :cond_2
    :goto_1
    const/4 p1, 0x0

    return p1
.end method
