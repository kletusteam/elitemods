.class public Ld9/e;
.super Ld9/b;

# interfaces
.implements Lmiuix/animation/f;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ld9/e$c;
    }
.end annotation


# static fields
.field private static v:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap<",
            "Landroid/view/View;",
            "Ld9/e$c;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private b:F

.field private c:Lc9/a;

.field private d:Lc9/a;

.field private e:Lc9/a;

.field private f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lmiuix/animation/f$b;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lmiuix/animation/f$b;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lmiuix/animation/f$a;

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:[I

.field private n:F

.field private o:I

.field private p:I

.field private q:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private r:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private s:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private t:Ljava/lang/String;

.field private u:Lf9/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    sput-object v0, Ld9/e;->v:Ljava/util/WeakHashMap;

    return-void
.end method

.method public varargs constructor <init>([Lmiuix/animation/b;)V
    .locals 5

    invoke-direct {p0, p1}, Ld9/b;-><init>([Lmiuix/animation/b;)V

    const v0, 0x7f7fffff    # Float.MAX_VALUE

    iput v0, p0, Ld9/e;->b:F

    new-instance v0, Lc9/a;

    invoke-direct {v0}, Lc9/a;-><init>()V

    const/4 v1, 0x2

    new-array v2, v1, [F

    fill-array-data v2, :array_0

    const/4 v3, -0x2

    invoke-static {v3, v2}, Lj9/c;->e(I[F)Lj9/c$a;

    move-result-object v2

    invoke-virtual {v0, v2}, Lc9/a;->l(Lj9/c$a;)Lc9/a;

    move-result-object v0

    iput-object v0, p0, Ld9/e;->c:Lc9/a;

    new-instance v0, Lc9/a;

    invoke-direct {v0}, Lc9/a;-><init>()V

    iput-object v0, p0, Ld9/e;->d:Lc9/a;

    new-instance v0, Lc9/a;

    invoke-direct {v0}, Lc9/a;-><init>()V

    iput-object v0, p0, Ld9/e;->e:Lc9/a;

    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Ld9/e;->f:Ljava/util/Map;

    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Ld9/e;->g:Ljava/util/Map;

    sget-object v0, Lmiuix/animation/f$a;->a:Lmiuix/animation/f$a;

    iput-object v0, p0, Ld9/e;->h:Lmiuix/animation/f$a;

    const/4 v0, 0x0

    iput-boolean v0, p0, Ld9/e;->i:Z

    iput-boolean v0, p0, Ld9/e;->k:Z

    new-array v2, v1, [I

    iput-object v2, p0, Ld9/e;->m:[I

    const/4 v2, 0x0

    iput v2, p0, Ld9/e;->n:F

    iput v0, p0, Ld9/e;->o:I

    iput v0, p0, Ld9/e;->p:I

    const-string v2, "MOVE"

    iput-object v2, p0, Ld9/e;->t:Ljava/lang/String;

    new-instance v2, Ld9/e$a;

    invoke-direct {v2, p0}, Ld9/e$a;-><init>(Ld9/e;)V

    iput-object v2, p0, Ld9/e;->u:Lf9/b;

    array-length v2, p1

    if-lez v2, :cond_0

    aget-object p1, p1, v0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-direct {p0, p1}, Ld9/e;->Y(Lmiuix/animation/b;)V

    iget-object p1, p0, Ld9/e;->h:Lmiuix/animation/f$a;

    invoke-direct {p0, p1}, Ld9/e;->x0(Lmiuix/animation/f$a;)V

    iget-object p1, p0, Ld9/e;->d:Lc9/a;

    new-array v2, v1, [F

    fill-array-data v2, :array_1

    invoke-static {v3, v2}, Lj9/c;->e(I[F)Lj9/c$a;

    move-result-object v2

    invoke-virtual {p1, v2}, Lc9/a;->l(Lj9/c$a;)Lc9/a;

    iget-object p1, p0, Ld9/e;->d:Lc9/a;

    const/4 v2, 0x1

    new-array v2, v2, [Lf9/b;

    iget-object v4, p0, Ld9/e;->u:Lf9/b;

    aput-object v4, v2, v0

    invoke-virtual {p1, v2}, Lc9/a;->a([Lf9/b;)Lc9/a;

    iget-object p1, p0, Ld9/e;->e:Lc9/a;

    new-array v0, v1, [F

    fill-array-data v0, :array_2

    invoke-virtual {p1, v3, v0}, Lc9/a;->k(I[F)Lc9/a;

    move-result-object p1

    sget-object v0, Lh9/h;->o:Lh9/h;

    const-wide/16 v2, -0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_3

    invoke-virtual {p1, v0, v2, v3, v1}, Lc9/a;->n(Lh9/b;J[F)Lc9/a;

    return-void

    :array_0
    .array-data 4
        0x3f666666    # 0.9f
        0x3ecccccd    # 0.4f
    .end array-data

    :array_1
    .array-data 4
        0x3f7d70a4    # 0.99f
        0x3f19999a    # 0.6f
    .end array-data

    :array_2
    .array-data 4
        0x3f7d70a4    # 0.99f
        0x3ecccccd    # 0.4f
    .end array-data

    :array_3
    .array-data 4
        0x3f666666    # 0.9f
        0x3e4ccccd    # 0.2f
    .end array-data
.end method

.method static synthetic J(Ld9/e;Landroid/view/View;Landroid/view/MotionEvent;[Lc9/a;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Ld9/e;->S(Landroid/view/View;Landroid/view/MotionEvent;[Lc9/a;)V

    return-void
.end method

.method private K(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 7

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result p2

    iget-object v1, p0, Ld9/e;->m:[I

    invoke-virtual {p1, v1}, Landroid/view/View;->getLocationOnScreen([I)V

    iget-object v1, p0, Ld9/e;->m:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    int-to-float v1, v1

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v3

    int-to-float v3, v3

    const/high16 v4, 0x3f000000    # 0.5f

    mul-float/2addr v3, v4

    add-float/2addr v1, v3

    iget-object v3, p0, Ld9/e;->m:[I

    const/4 v5, 0x1

    aget v3, v3, v5

    int-to-float v3, v3

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v6, v4

    add-float/2addr v3, v6

    sub-float/2addr v0, v1

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    sub-float/2addr p2, v3

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result p1

    int-to-float p1, p1

    div-float/2addr p2, p1

    const/high16 p1, 0x3f800000    # 1.0f

    invoke-static {p1, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    const/high16 v1, -0x40800000    # -1.0f

    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    invoke-static {p1, p2}, Ljava/lang/Math;->min(FF)F

    move-result p2

    invoke-static {v1, p2}, Ljava/lang/Math;->max(FF)F

    move-result p2

    iget v1, p0, Ld9/e;->b:F

    const v3, 0x7f7fffff    # Float.MAX_VALUE

    cmpl-float v4, v1, v3

    if-nez v4, :cond_0

    move v4, p1

    goto :goto_0

    :cond_0
    move v4, v1

    :goto_0
    mul-float/2addr v0, v4

    cmpl-float v3, v1, v3

    if-nez v3, :cond_1

    goto :goto_1

    :cond_1
    move p1, v1

    :goto_1
    mul-float/2addr p2, p1

    iget-object p1, p0, Ld9/b;->a:Ld9/h;

    iget-object v1, p0, Ld9/e;->t:Ljava/lang/String;

    invoke-interface {p1, v1}, Ld9/h;->H(Ljava/lang/Object;)Ld9/a;

    move-result-object p1

    sget-object v1, Lh9/h;->b:Lh9/h;

    float-to-double v3, v0

    invoke-virtual {p1, v1, v3, v4}, Ld9/a;->a(Ljava/lang/Object;D)Ld9/a;

    move-result-object p1

    sget-object v0, Lh9/h;->c:Lh9/h;

    float-to-double v3, p2

    invoke-virtual {p1, v0, v3, v4}, Ld9/a;->a(Ljava/lang/Object;D)Ld9/a;

    move-result-object p1

    iget-object p2, p0, Ld9/b;->a:Ld9/h;

    new-array v0, v5, [Lc9/a;

    iget-object v1, p0, Ld9/e;->c:Lc9/a;

    aput-object v1, v0, v2

    invoke-interface {p2, p1, v0}, Lmiuix/animation/h;->s(Ljava/lang/Object;[Lc9/a;)Lmiuix/animation/h;

    return-void
.end method

.method private L()V
    .locals 0

    return-void
.end method

.method private M()V
    .locals 3

    sget-object v0, Lmiuix/animation/f$b;->a:Lmiuix/animation/f$b;

    invoke-direct {p0, v0}, Ld9/e;->b0(Lmiuix/animation/f$b;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Ld9/b;->a:Ld9/h;

    invoke-interface {v1, v0}, Ld9/h;->H(Ljava/lang/Object;)Ld9/a;

    move-result-object v1

    sget-object v2, Lh9/h;->e:Lh9/h;

    invoke-virtual {v1, v2}, Ld9/a;->o(Ljava/lang/Object;)Ld9/a;

    iget-object v1, p0, Ld9/b;->a:Ld9/h;

    invoke-interface {v1, v0}, Ld9/h;->H(Ljava/lang/Object;)Ld9/a;

    move-result-object v0

    sget-object v1, Lh9/h;->f:Lh9/h;

    invoke-virtual {v0, v1}, Ld9/a;->o(Ljava/lang/Object;)Ld9/a;

    :cond_0
    sget-object v0, Lmiuix/animation/f$b;->b:Lmiuix/animation/f$b;

    invoke-direct {p0, v0}, Ld9/e;->b0(Lmiuix/animation/f$b;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Ld9/b;->a:Ld9/h;

    invoke-interface {v1, v0}, Ld9/h;->H(Ljava/lang/Object;)Ld9/a;

    move-result-object v1

    sget-object v2, Lh9/h;->e:Lh9/h;

    invoke-virtual {v1, v2}, Ld9/a;->o(Ljava/lang/Object;)Ld9/a;

    iget-object v1, p0, Ld9/b;->a:Ld9/h;

    invoke-interface {v1, v0}, Ld9/h;->H(Ljava/lang/Object;)Ld9/a;

    move-result-object v0

    sget-object v1, Lh9/h;->f:Lh9/h;

    invoke-virtual {v0, v1}, Ld9/a;->o(Ljava/lang/Object;)Ld9/a;

    :cond_1
    iget-object v0, p0, Ld9/e;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    return-void
.end method

.method private O()V
    .locals 3

    const/4 v0, 0x0

    iput-boolean v0, p0, Ld9/e;->i:Z

    sget-object v0, Lmiuix/animation/f$b;->a:Lmiuix/animation/f$b;

    invoke-direct {p0, v0}, Ld9/e;->c0(Lmiuix/animation/f$b;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Ld9/b;->a:Ld9/h;

    invoke-interface {v1, v0}, Ld9/h;->H(Ljava/lang/Object;)Ld9/a;

    move-result-object v1

    sget-object v2, Lh9/h;->b:Lh9/h;

    invoke-virtual {v1, v2}, Ld9/a;->o(Ljava/lang/Object;)Ld9/a;

    iget-object v1, p0, Ld9/b;->a:Ld9/h;

    invoke-interface {v1, v0}, Ld9/h;->H(Ljava/lang/Object;)Ld9/a;

    move-result-object v0

    sget-object v1, Lh9/h;->c:Lh9/h;

    invoke-virtual {v0, v1}, Ld9/a;->o(Ljava/lang/Object;)Ld9/a;

    :cond_0
    sget-object v0, Lmiuix/animation/f$b;->b:Lmiuix/animation/f$b;

    invoke-direct {p0, v0}, Ld9/e;->c0(Lmiuix/animation/f$b;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Ld9/b;->a:Ld9/h;

    invoke-interface {v1, v0}, Ld9/h;->H(Ljava/lang/Object;)Ld9/a;

    move-result-object v1

    sget-object v2, Lh9/h;->b:Lh9/h;

    invoke-virtual {v1, v2}, Ld9/a;->o(Ljava/lang/Object;)Ld9/a;

    iget-object v1, p0, Ld9/b;->a:Ld9/h;

    invoke-interface {v1, v0}, Ld9/h;->H(Ljava/lang/Object;)Ld9/a;

    move-result-object v0

    sget-object v1, Lh9/h;->c:Lh9/h;

    invoke-virtual {v0, v1}, Ld9/a;->o(Ljava/lang/Object;)Ld9/a;

    :cond_1
    iget-object v0, p0, Ld9/e;->g:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    return-void
.end method

.method private varargs P(Landroid/view/View;[Lc9/a;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Ld9/e;->T(Landroid/view/View;[Lc9/a;)V

    invoke-direct {p0, p1}, Ld9/e;->p0(Landroid/view/View;)Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-static {}, Lj9/f;->d()Z

    move-result p2

    if-eqz p2, :cond_0

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "handleViewHover for "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x0

    new-array p2, p2, [Ljava/lang/Object;

    invoke-static {p1, p2}, Lj9/f;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private varargs Q([Lc9/a;)[Lc9/a;
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Lc9/a;

    iget-object v1, p0, Ld9/e;->d:Lc9/a;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static {p1, v0}, Lj9/a;->m([Ljava/lang/Object;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Lc9/a;

    return-object p1
.end method

.method private varargs R([Lc9/a;)[Lc9/a;
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Lc9/a;

    iget-object v1, p0, Ld9/e;->e:Lc9/a;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static {p1, v0}, Lj9/a;->m([Ljava/lang/Object;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Lc9/a;

    return-object p1
.end method

.method private varargs S(Landroid/view/View;Landroid/view/MotionEvent;[Lc9/a;)V
    .locals 2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    const/4 v1, 0x7

    if-eq v0, v1, :cond_2

    const/16 p1, 0x9

    if-eq v0, p1, :cond_1

    const/16 p1, 0xa

    if-eq v0, p1, :cond_0

    goto :goto_0

    :cond_0
    invoke-direct {p0, p2, p3}, Ld9/e;->e0(Landroid/view/MotionEvent;[Lc9/a;)V

    goto :goto_0

    :cond_1
    invoke-direct {p0, p2, p3}, Ld9/e;->d0(Landroid/view/MotionEvent;[Lc9/a;)V

    goto :goto_0

    :cond_2
    invoke-direct {p0, p1, p2, p3}, Ld9/e;->f0(Landroid/view/View;Landroid/view/MotionEvent;[Lc9/a;)V

    :goto_0
    return-void
.end method

.method private varargs T(Landroid/view/View;[Lc9/a;)V
    .locals 2

    sget-object v0, Ld9/e;->v:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ld9/e$c;

    if-nez v0, :cond_0

    new-instance v0, Ld9/e$c;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ld9/e$c;-><init>(Ld9/e$a;)V

    sget-object v1, Ld9/e;->v:Ljava/util/WeakHashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-virtual {p1, v0}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    invoke-virtual {v0, p0, p2}, Ld9/e$c;->a(Ld9/e;[Lc9/a;)V

    return-void
.end method

.method private varargs V(Z[Lc9/a;)V
    .locals 5

    iput-boolean p1, p0, Ld9/e;->i:Z

    const/4 p1, 0x1

    iput-boolean p1, p0, Ld9/e;->l:Z

    iget-object v0, p0, Ld9/e;->h:Lmiuix/animation/f$a;

    sget-object v1, Lmiuix/animation/f$a;->c:Lmiuix/animation/f$a;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Ld9/e;->q:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    invoke-static {v0, p1}, Ld9/e;->q0(Landroid/view/View;Z)V

    invoke-static {v0, p1}, Ld9/e;->w0(Landroid/view/View;Z)V

    :cond_1
    invoke-virtual {p0}, Ld9/e;->Z()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0, p1}, Ld9/e;->r0(Z)V

    invoke-virtual {p0, p1}, Ld9/e;->t0(Z)V

    :cond_2
    iget v0, p0, Ld9/e;->n:F

    invoke-virtual {p0, v0}, Ld9/e;->n0(F)Lmiuix/animation/f;

    invoke-direct {p0}, Ld9/e;->v0()V

    invoke-direct {p0, p2}, Ld9/e;->Q([Lc9/a;)[Lc9/a;

    move-result-object p2

    iget-object v0, p0, Ld9/b;->a:Ld9/h;

    sget-object v1, Lmiuix/animation/f$b;->a:Lmiuix/animation/f$b;

    invoke-interface {v0, v1}, Ld9/h;->H(Ljava/lang/Object;)Ld9/a;

    move-result-object v0

    invoke-direct {p0, v1}, Ld9/e;->b0(Lmiuix/animation/f$b;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Ld9/b;->a:Ld9/h;

    invoke-interface {v1}, Ld9/h;->g()Lmiuix/animation/b;

    move-result-object v1

    sget-object v2, Lh9/h;->n:Lh9/h;

    invoke-virtual {v1, v2}, Lmiuix/animation/b;->i(Lh9/b;)F

    move-result v2

    sget-object v3, Lh9/h;->m:Lh9/h;

    invoke-virtual {v1, v3}, Lmiuix/animation/b;->i(Lh9/b;)F

    move-result v1

    invoke-static {v2, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    const/high16 v2, 0x41400000    # 12.0f

    add-float/2addr v2, v1

    div-float/2addr v2, v1

    const v1, 0x3f933333    # 1.15f

    invoke-static {v2, v1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    sget-object v2, Lh9/h;->e:Lh9/h;

    float-to-double v3, v1

    invoke-virtual {v0, v2, v3, v4}, Ld9/a;->a(Ljava/lang/Object;D)Ld9/a;

    move-result-object v1

    sget-object v2, Lh9/h;->f:Lh9/h;

    invoke-virtual {v1, v2, v3, v4}, Ld9/a;->a(Ljava/lang/Object;D)Ld9/a;

    :cond_3
    iget-object v1, p0, Ld9/e;->s:Ljava/lang/ref/WeakReference;

    if-eqz v1, :cond_4

    new-array p1, p1, [Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    aput-object v1, p1, v2

    invoke-static {p1}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object p1

    invoke-interface {p1}, Lmiuix/animation/e;->b()Lmiuix/animation/h;

    move-result-object p1

    sget-object v1, Lh9/h;->e:Lh9/h;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-interface {p1, v1, v2}, Lmiuix/animation/h;->B(Lh9/b;F)Lmiuix/animation/h;

    move-result-object p1

    sget-object v1, Lh9/h;->f:Lh9/h;

    invoke-interface {p1, v1, v2}, Lmiuix/animation/h;->B(Lh9/b;F)Lmiuix/animation/h;

    move-result-object p1

    invoke-interface {p1, p2}, Lmiuix/animation/h;->t([Lc9/a;)Lmiuix/animation/h;

    :cond_4
    iget-object p1, p0, Ld9/b;->a:Ld9/h;

    invoke-interface {p1, v0, p2}, Lmiuix/animation/h;->s(Ljava/lang/Object;[Lc9/a;)Lmiuix/animation/h;

    return-void
.end method

.method private varargs W(I[Lc9/a;)V
    .locals 1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v0, 0x3

    if-eq p1, v0, :cond_2

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x4

    if-eq p1, v0, :cond_1

    const/4 v0, 0x2

    if-ne p1, v0, :cond_3

    :cond_1
    const/4 p1, 0x0

    invoke-direct {p0, p1, p2}, Ld9/e;->V(Z[Lc9/a;)V

    goto :goto_1

    :cond_2
    :goto_0
    invoke-virtual {p0, p2}, Ld9/e;->A([Lc9/a;)V

    :cond_3
    :goto_1
    return-void
.end method

.method private Y(Lmiuix/animation/b;)V
    .locals 6

    instance-of v0, p1, Lmiuix/animation/ViewTarget;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lmiuix/animation/ViewTarget;

    invoke-virtual {v0}, Lmiuix/animation/ViewTarget;->z()Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_3

    sget-object v1, Lh9/h;->n:Lh9/h;

    invoke-virtual {p1, v1}, Lmiuix/animation/b;->i(Lh9/b;)F

    move-result v1

    sget-object v2, Lh9/h;->m:Lh9/h;

    invoke-virtual {p1, v2}, Lmiuix/animation/b;->i(Lh9/b;)F

    move-result p1

    invoke-static {v1, p1}, Ljava/lang/Math;->max(FF)F

    move-result p1

    const/high16 v1, 0x41400000    # 12.0f

    add-float/2addr v1, p1

    div-float/2addr v1, p1

    const p1, 0x3f933333    # 1.15f

    invoke-static {v1, p1}, Ljava/lang/Math;->min(FF)F

    move-result p1

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v1

    iput v1, p0, Ld9/e;->o:I

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    iput v0, p0, Ld9/e;->p:I

    iget v1, p0, Ld9/e;->o:I

    add-int/lit8 v1, v1, -0x28

    add-int/lit8 v0, v0, -0x28

    int-to-float v1, v1

    const/4 v2, 0x0

    const/high16 v3, 0x43b40000    # 360.0f

    invoke-direct {p0, v1, v2, v3}, Ld9/e;->g0(FFF)F

    move-result v1

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-static {v4, v1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-static {v2, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    const/high16 v5, 0x41700000    # 15.0f

    invoke-direct {p0, v1, v5, v2}, Ld9/e;->y0(FFF)F

    move-result v1

    invoke-static {v5, v1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    int-to-float v0, v0

    invoke-direct {p0, v0, v2, v3}, Ld9/e;->g0(FFF)F

    move-result v0

    invoke-static {v4, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    invoke-direct {p0, v0, v5, v2}, Ld9/e;->y0(FFF)F

    move-result v0

    invoke-static {v5, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    cmpl-float p1, p1, v4

    if-nez p1, :cond_1

    goto :goto_1

    :cond_1
    invoke-static {v1, v0}, Ljava/lang/Math;->min(FF)F

    move-result v2

    :goto_1
    iput v2, p0, Ld9/e;->b:F

    iget p1, p0, Ld9/e;->o:I

    iget v0, p0, Ld9/e;->p:I

    if-ne p1, v0, :cond_2

    const/16 v1, 0x64

    if-ge p1, v1, :cond_2

    if-ge v0, v1, :cond_2

    int-to-float p1, p1

    const/high16 v0, 0x3f000000    # 0.5f

    mul-float/2addr p1, v0

    float-to-int p1, p1

    int-to-float p1, p1

    goto :goto_2

    :cond_2
    const/high16 p1, 0x42100000    # 36.0f

    :goto_2
    invoke-virtual {p0, p1}, Ld9/e;->n0(F)Lmiuix/animation/f;

    :cond_3
    return-void
.end method

.method static a0(Landroid/view/View;[ILandroid/view/MotionEvent;)Z
    .locals 5

    const/4 v0, 0x1

    if-eqz p0, :cond_1

    invoke-virtual {p0, p1}, Landroid/view/View;->getLocationOnScreen([I)V

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result p2

    float-to-int p2, p2

    const/4 v2, 0x0

    aget v3, p1, v2

    if-lt v1, v3, :cond_0

    aget v3, p1, v2

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v4

    add-int/2addr v3, v4

    if-gt v1, v3, :cond_0

    aget v1, p1, v0

    if-lt p2, v1, :cond_0

    aget p1, p1, v0

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result p0

    add-int/2addr p1, p0

    if-gt p2, p1, :cond_0

    goto :goto_0

    :cond_0
    move v0, v2

    :cond_1
    :goto_0
    return v0
.end method

.method private b0(Lmiuix/animation/f$b;)Z
    .locals 2

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v1, p0, Ld9/e;->f:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method private c0(Lmiuix/animation/f$b;)Z
    .locals 2

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v1, p0, Ld9/e;->g:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method private varargs d0(Landroid/view/MotionEvent;[Lc9/a;)V
    .locals 2

    invoke-static {}, Lj9/f;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "onEventEnter, touchEnter"

    invoke-static {v1, v0}, Lj9/f;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    invoke-virtual {p0, p1, p2}, Ld9/e;->U(Landroid/view/MotionEvent;[Lc9/a;)V

    return-void
.end method

.method private varargs e0(Landroid/view/MotionEvent;[Lc9/a;)V
    .locals 2

    iget-boolean v0, p0, Ld9/e;->l:Z

    if-eqz v0, :cond_1

    invoke-static {}, Lj9/f;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "onEventExit, touchExit"

    invoke-static {v1, v0}, Lj9/f;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    invoke-virtual {p0, p1, p2}, Ld9/e;->X(Landroid/view/MotionEvent;[Lc9/a;)V

    invoke-direct {p0}, Ld9/e;->h0()V

    :cond_1
    return-void
.end method

.method private varargs f0(Landroid/view/View;Landroid/view/MotionEvent;[Lc9/a;)V
    .locals 0

    iget-boolean p3, p0, Ld9/e;->l:Z

    if-eqz p3, :cond_0

    if-eqz p1, :cond_0

    sget-object p3, Lmiuix/animation/f$b;->a:Lmiuix/animation/f$b;

    invoke-direct {p0, p3}, Ld9/e;->c0(Lmiuix/animation/f$b;)Z

    move-result p3

    if-eqz p3, :cond_0

    iget-boolean p3, p0, Ld9/e;->i:Z

    if-eqz p3, :cond_0

    invoke-direct {p0, p1, p2}, Ld9/e;->K(Landroid/view/View;Landroid/view/MotionEvent;)V

    :cond_0
    return-void
.end method

.method private g0(FFF)F
    .locals 0

    sub-float/2addr p1, p2

    sub-float/2addr p3, p2

    div-float/2addr p1, p3

    return p1
.end method

.method private h0()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Ld9/e;->l:Z

    return-void
.end method

.method private i0(Ljava/lang/ref/WeakReference;)Landroid/view/View;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/WeakReference<",
            "Landroid/view/View;",
            ">;)",
            "Landroid/view/View;"
        }
    .end annotation

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    :cond_0
    return-object p1
.end method

.method private j0()V
    .locals 0

    return-void
.end method

.method private k0()V
    .locals 4

    iget-object v0, p0, Ld9/e;->f:Ljava/util/Map;

    sget-object v1, Lmiuix/animation/f$b;->a:Lmiuix/animation/f$b;

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Ld9/e;->f:Ljava/util/Map;

    sget-object v1, Lmiuix/animation/f$b;->b:Lmiuix/animation/f$b;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Ld9/b;->a:Ld9/h;

    invoke-interface {v0, v1}, Ld9/h;->H(Ljava/lang/Object;)Ld9/a;

    move-result-object v0

    sget-object v1, Lh9/h;->e:Lh9/h;

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v0, v1, v2, v3}, Ld9/a;->a(Ljava/lang/Object;D)Ld9/a;

    move-result-object v0

    sget-object v1, Lh9/h;->f:Lh9/h;

    invoke-virtual {v0, v1, v2, v3}, Ld9/a;->a(Ljava/lang/Object;D)Ld9/a;

    return-void
.end method

.method private l0()V
    .locals 4

    const/4 v0, 0x1

    iput-boolean v0, p0, Ld9/e;->i:Z

    iget-object v0, p0, Ld9/e;->g:Ljava/util/Map;

    sget-object v1, Lmiuix/animation/f$b;->a:Lmiuix/animation/f$b;

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Ld9/e;->g:Ljava/util/Map;

    sget-object v1, Lmiuix/animation/f$b;->b:Lmiuix/animation/f$b;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Ld9/b;->a:Ld9/h;

    invoke-interface {v0, v1}, Ld9/h;->H(Ljava/lang/Object;)Ld9/a;

    move-result-object v0

    sget-object v1, Lh9/h;->b:Lh9/h;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Ld9/a;->a(Ljava/lang/Object;D)Ld9/a;

    move-result-object v0

    sget-object v1, Lh9/h;->c:Lh9/h;

    invoke-virtual {v0, v1, v2, v3}, Ld9/a;->a(Ljava/lang/Object;D)Ld9/a;

    return-void
.end method

.method private static o0(Landroid/view/View;F)V
    .locals 5

    const/4 v0, 0x1

    :try_start_0
    new-array v1, v0, [Ljava/lang/Class;

    sget-object v2, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "android.view.View"

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    const-string v4, "setFeedbackRadius"

    invoke-virtual {v2, v4, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {v1, p0, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "setFeedbackRadius failed , e:"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string p1, ""

    invoke-static {p1, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method

.method private p0(Landroid/view/View;)Z
    .locals 1

    iget-object v0, p0, Ld9/e;->q:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-ne v0, p1, :cond_1

    const/4 p1, 0x0

    return p1

    :cond_1
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Ld9/e;->q:Ljava/lang/ref/WeakReference;

    const/4 p1, 0x1

    return p1
.end method

.method private static q0(Landroid/view/View;Z)V
    .locals 5

    const/4 v0, 0x1

    :try_start_0
    new-array v1, v0, [Ljava/lang/Class;

    sget-object v2, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "android.view.View"

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    const-string v4, "setMagicView"

    invoke-virtual {v2, v4, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {v1, p0, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "setMagicView failed , e:"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string p1, ""

    invoke-static {p1, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method

.method private static s0(Landroid/view/View;Z)V
    .locals 5

    const/4 v0, 0x1

    :try_start_0
    new-array v1, v0, [Ljava/lang/Class;

    sget-object v2, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "android.view.View"

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    const-string v4, "setPointerHide"

    invoke-virtual {v2, v4, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {v1, p0, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "setPointerHide failed , e:"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string p1, ""

    invoke-static {p1, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method

.method private v0()V
    .locals 5

    iget-boolean v0, p0, Ld9/e;->j:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Ld9/e;->k:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/16 v0, 0x14

    const/4 v1, 0x0

    invoke-static {v0, v1, v1, v1}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    iget-object v1, p0, Ld9/b;->a:Ld9/h;

    invoke-interface {v1}, Ld9/h;->g()Lmiuix/animation/b;

    move-result-object v1

    invoke-virtual {v1}, Lmiuix/animation/b;->h()Ljava/lang/Object;

    move-result-object v1

    instance-of v2, v1, Landroid/view/View;

    if-eqz v2, :cond_1

    check-cast v1, Landroid/view/View;

    sget v0, Lfa/a;->b:I

    invoke-virtual {v1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    :cond_1
    sget-object v1, Lh9/i;->a:Lh9/i$c;

    iget-object v2, p0, Ld9/b;->a:Ld9/h;

    sget-object v3, Lmiuix/animation/f$b;->a:Lmiuix/animation/f$b;

    invoke-interface {v2, v3}, Ld9/h;->H(Ljava/lang/Object;)Ld9/a;

    move-result-object v2

    int-to-double v3, v0

    invoke-virtual {v2, v1, v3, v4}, Ld9/a;->a(Ljava/lang/Object;D)Ld9/a;

    iget-object v0, p0, Ld9/b;->a:Ld9/h;

    sget-object v2, Lmiuix/animation/f$b;->b:Lmiuix/animation/f$b;

    invoke-interface {v0, v2}, Ld9/h;->H(Ljava/lang/Object;)Ld9/a;

    move-result-object v0

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Ld9/a;->a(Ljava/lang/Object;D)Ld9/a;

    :cond_2
    :goto_0
    return-void
.end method

.method private static w0(Landroid/view/View;Z)V
    .locals 5

    const/4 v0, 0x1

    :try_start_0
    new-array v1, v0, [Ljava/lang/Class;

    sget-object v2, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "android.view.View"

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    const-string v4, "setWrapped"

    invoke-virtual {v2, v4, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {v1, p0, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "setWrapped failed , e:"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string p1, ""

    invoke-static {p1, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method

.method private x0(Lmiuix/animation/f$a;)V
    .locals 2

    sget-object v0, Ld9/e$b;->a:[I

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_5

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    goto :goto_2

    :cond_0
    iget-object v0, p0, Ld9/e;->h:Lmiuix/animation/f$a;

    sget-object v1, Lmiuix/animation/f$a;->a:Lmiuix/animation/f$a;

    if-eq v0, v1, :cond_1

    sget-object v1, Lmiuix/animation/f$a;->b:Lmiuix/animation/f$a;

    if-ne v0, v1, :cond_2

    :cond_1
    invoke-virtual {p0}, Ld9/e;->N()Lmiuix/animation/f;

    :cond_2
    invoke-direct {p0}, Ld9/e;->k0()V

    invoke-direct {p0}, Ld9/e;->l0()V

    invoke-direct {p0}, Ld9/e;->j0()V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Ld9/e;->h:Lmiuix/animation/f$a;

    sget-object v1, Lmiuix/animation/f$a;->c:Lmiuix/animation/f$a;

    if-ne v0, v1, :cond_4

    invoke-direct {p0}, Ld9/e;->L()V

    :cond_4
    invoke-direct {p0}, Ld9/e;->v0()V

    invoke-direct {p0}, Ld9/e;->k0()V

    invoke-direct {p0}, Ld9/e;->l0()V

    :goto_0
    iput-object p1, p0, Ld9/e;->h:Lmiuix/animation/f$a;

    goto :goto_2

    :cond_5
    iget-object v0, p0, Ld9/e;->h:Lmiuix/animation/f$a;

    sget-object v1, Lmiuix/animation/f$a;->b:Lmiuix/animation/f$a;

    if-ne v0, v1, :cond_6

    invoke-direct {p0}, Ld9/e;->M()V

    invoke-direct {p0}, Ld9/e;->O()V

    goto :goto_1

    :cond_6
    sget-object v1, Lmiuix/animation/f$a;->c:Lmiuix/animation/f$a;

    if-ne v0, v1, :cond_7

    invoke-direct {p0}, Ld9/e;->M()V

    invoke-direct {p0}, Ld9/e;->O()V

    invoke-direct {p0}, Ld9/e;->L()V

    :cond_7
    :goto_1
    invoke-direct {p0}, Ld9/e;->v0()V

    goto :goto_0

    :goto_2
    return-void
.end method

.method private y0(FFF)F
    .locals 0

    sub-float/2addr p3, p2

    mul-float/2addr p3, p1

    add-float/2addr p2, p3

    return p2
.end method


# virtual methods
.method public varargs A([Lc9/a;)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Ld9/e;->V(Z[Lc9/a;)V

    return-void
.end method

.method public varargs E([Lc9/a;)V
    .locals 2

    invoke-direct {p0, p1}, Ld9/e;->R([Lc9/a;)[Lc9/a;

    move-result-object p1

    iget-object v0, p0, Ld9/b;->a:Ld9/h;

    sget-object v1, Lmiuix/animation/f$b;->b:Lmiuix/animation/f$b;

    invoke-interface {v0, v1}, Ld9/h;->H(Ljava/lang/Object;)Ld9/a;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lmiuix/animation/h;->s(Ljava/lang/Object;[Lc9/a;)Lmiuix/animation/h;

    return-void
.end method

.method public N()Lmiuix/animation/f;
    .locals 3

    const/4 v0, 0x1

    iput-boolean v0, p0, Ld9/e;->k:Z

    sget-object v0, Lh9/i;->a:Lh9/i$c;

    iget-object v1, p0, Ld9/b;->a:Ld9/h;

    sget-object v2, Lmiuix/animation/f$b;->a:Lmiuix/animation/f$b;

    invoke-interface {v1, v2}, Ld9/h;->H(Ljava/lang/Object;)Ld9/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Ld9/a;->o(Ljava/lang/Object;)Ld9/a;

    iget-object v1, p0, Ld9/b;->a:Ld9/h;

    sget-object v2, Lmiuix/animation/f$b;->b:Lmiuix/animation/f$b;

    invoke-interface {v1, v2}, Ld9/h;->H(Ljava/lang/Object;)Ld9/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Ld9/a;->o(Ljava/lang/Object;)Ld9/a;

    return-object p0
.end method

.method public varargs U(Landroid/view/MotionEvent;[Lc9/a;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result p1

    invoke-direct {p0, p1, p2}, Ld9/e;->W(I[Lc9/a;)V

    return-void
.end method

.method public varargs X(Landroid/view/MotionEvent;[Lc9/a;)V
    .locals 3

    iget-object v0, p0, Ld9/e;->s:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ld9/e;->q:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iget-object v1, p0, Ld9/e;->m:[I

    invoke-static {v0, v1, p1}, Ld9/e;->a0(Landroid/view/View;[ILandroid/view/MotionEvent;)Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    new-array v0, p1, [Landroid/view/View;

    iget-object v1, p0, Ld9/e;->s:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static {v0}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object v0

    invoke-interface {v0}, Lmiuix/animation/e;->a()Lmiuix/animation/f;

    move-result-object v0

    new-array p1, p1, [Lc9/a;

    iget-object v1, p0, Ld9/e;->d:Lc9/a;

    aput-object v1, p1, v2

    invoke-interface {v0, p1}, Lmiuix/animation/f;->A([Lc9/a;)V

    :cond_0
    sget-object p1, Lmiuix/animation/f$b;->b:Lmiuix/animation/f$b;

    invoke-direct {p0, p1}, Ld9/e;->c0(Lmiuix/animation/f$b;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Ld9/e;->i:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Ld9/b;->a:Ld9/h;

    invoke-interface {v0, p1}, Ld9/h;->H(Ljava/lang/Object;)Ld9/a;

    move-result-object p1

    sget-object v0, Lh9/h;->b:Lh9/h;

    const-wide/16 v1, 0x0

    invoke-virtual {p1, v0, v1, v2}, Ld9/a;->a(Ljava/lang/Object;D)Ld9/a;

    move-result-object p1

    sget-object v0, Lh9/h;->c:Lh9/h;

    invoke-virtual {p1, v0, v1, v2}, Ld9/a;->a(Ljava/lang/Object;D)Ld9/a;

    :cond_1
    invoke-virtual {p0, p2}, Ld9/e;->E([Lc9/a;)V

    return-void
.end method

.method public Z()Z
    .locals 2

    iget v0, p0, Ld9/e;->o:I

    const/16 v1, 0x64

    if-ge v0, v1, :cond_1

    iget v0, p0, Ld9/e;->p:I

    if-ge v0, v1, :cond_1

    iget-boolean v0, p0, Ld9/e;->i:Z

    if-eqz v0, :cond_0

    if-eqz v0, :cond_1

    iget-object v0, p0, Ld9/e;->h:Lmiuix/animation/f$a;

    sget-object v1, Lmiuix/animation/f$a;->b:Lmiuix/animation/f$a;

    if-eq v0, v1, :cond_0

    sget-object v1, Lmiuix/animation/f$a;->c:Lmiuix/animation/f$a;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public a(FFFF)Lmiuix/animation/f;
    .locals 1

    const/high16 v0, 0x437f0000    # 255.0f

    mul-float/2addr p1, v0

    float-to-int p1, p1

    mul-float/2addr p2, v0

    float-to-int p2, p2

    mul-float/2addr p3, v0

    float-to-int p3, p3

    mul-float/2addr p4, v0

    float-to-int p4, p4

    invoke-static {p1, p2, p3, p4}, Landroid/graphics/Color;->argb(IIII)I

    move-result p1

    invoke-virtual {p0, p1}, Ld9/e;->u0(I)Lmiuix/animation/f;

    move-result-object p1

    return-object p1
.end method

.method public b(FFFF)Lmiuix/animation/f;
    .locals 1

    const/high16 v0, 0x437f0000    # 255.0f

    mul-float/2addr p1, v0

    float-to-int p1, p1

    mul-float/2addr p2, v0

    float-to-int p2, p2

    mul-float/2addr p3, v0

    float-to-int p3, p3

    mul-float/2addr p4, v0

    float-to-int p4, p4

    invoke-static {p1, p2, p3, p4}, Landroid/graphics/Color;->argb(IIII)I

    move-result p1

    invoke-virtual {p0, p1}, Ld9/e;->m0(I)Lmiuix/animation/f;

    move-result-object p1

    return-object p1
.end method

.method public e(Lmiuix/animation/f$a;)Lmiuix/animation/f;
    .locals 0

    invoke-direct {p0, p1}, Ld9/e;->x0(Lmiuix/animation/f$a;)V

    return-object p0
.end method

.method public m0(I)Lmiuix/animation/f;
    .locals 4

    sget-object v0, Lh9/i;->b:Lh9/i$b;

    iget-object v1, p0, Ld9/b;->a:Ld9/h;

    sget-object v2, Lmiuix/animation/f$b;->a:Lmiuix/animation/f$b;

    invoke-interface {v1, v2}, Ld9/h;->H(Ljava/lang/Object;)Ld9/a;

    move-result-object v1

    int-to-double v2, p1

    invoke-virtual {v1, v0, v2, v3}, Ld9/a;->a(Ljava/lang/Object;D)Ld9/a;

    iget-object p1, p0, Ld9/b;->a:Ld9/h;

    sget-object v1, Lmiuix/animation/f$b;->b:Lmiuix/animation/f$b;

    invoke-interface {p1, v1}, Ld9/h;->H(Ljava/lang/Object;)Ld9/a;

    move-result-object p1

    iget-object v1, p0, Ld9/b;->a:Ld9/h;

    invoke-interface {v1}, Ld9/h;->g()Lmiuix/animation/b;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-static {v1, v0, v2, v3}, Le9/j;->c(Lmiuix/animation/b;Lh9/b;D)D

    move-result-wide v1

    double-to-int v1, v1

    int-to-double v1, v1

    invoke-virtual {p1, v0, v1, v2}, Ld9/a;->a(Ljava/lang/Object;D)Ld9/a;

    return-object p0
.end method

.method public n()V
    .locals 2

    invoke-super {p0}, Ld9/b;->n()V

    iget-object v0, p0, Ld9/e;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iget-object v0, p0, Ld9/e;->q:Ljava/lang/ref/WeakReference;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v0}, Ld9/e;->i0(Ljava/lang/ref/WeakReference;)Landroid/view/View;

    iput-object v1, p0, Ld9/e;->q:Ljava/lang/ref/WeakReference;

    :cond_0
    iget-object v0, p0, Ld9/e;->r:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_1

    invoke-direct {p0, v0}, Ld9/e;->i0(Ljava/lang/ref/WeakReference;)Landroid/view/View;

    iput-object v1, p0, Ld9/e;->r:Ljava/lang/ref/WeakReference;

    :cond_1
    iget-object v0, p0, Ld9/e;->s:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_2

    invoke-direct {p0, v0}, Ld9/e;->i0(Ljava/lang/ref/WeakReference;)Landroid/view/View;

    iput-object v1, p0, Ld9/e;->s:Ljava/lang/ref/WeakReference;

    :cond_2
    return-void
.end method

.method public n0(F)Lmiuix/animation/f;
    .locals 2

    iput p1, p0, Ld9/e;->n:F

    iget-object v0, p0, Ld9/b;->a:Ld9/h;

    invoke-interface {v0}, Ld9/h;->g()Lmiuix/animation/b;

    move-result-object v0

    invoke-virtual {v0}, Lmiuix/animation/b;->h()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Landroid/view/View;

    if-eqz v1, :cond_0

    check-cast v0, Landroid/view/View;

    sget v1, Lmiuix/animation/m;->e:I

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    :cond_0
    return-object p0
.end method

.method public r(F)V
    .locals 2

    iget-object v0, p0, Ld9/b;->a:Ld9/h;

    invoke-interface {v0}, Ld9/h;->g()Lmiuix/animation/b;

    move-result-object v0

    invoke-virtual {v0}, Lmiuix/animation/b;->h()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Landroid/view/View;

    if-eqz v1, :cond_0

    check-cast v0, Landroid/view/View;

    invoke-static {v0, p1}, Ld9/e;->o0(Landroid/view/View;F)V

    :cond_0
    return-void
.end method

.method public r0(Z)V
    .locals 2

    iget-object v0, p0, Ld9/b;->a:Ld9/h;

    invoke-interface {v0}, Ld9/h;->g()Lmiuix/animation/b;

    move-result-object v0

    invoke-virtual {v0}, Lmiuix/animation/b;->h()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Landroid/view/View;

    if-eqz v1, :cond_0

    check-cast v0, Landroid/view/View;

    invoke-static {v0, p1}, Ld9/e;->q0(Landroid/view/View;Z)V

    :cond_0
    return-void
.end method

.method public t0(Z)V
    .locals 2

    iget-object v0, p0, Ld9/b;->a:Ld9/h;

    invoke-interface {v0}, Ld9/h;->g()Lmiuix/animation/b;

    move-result-object v0

    invoke-virtual {v0}, Lmiuix/animation/b;->h()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Landroid/view/View;

    if-eqz v1, :cond_0

    check-cast v0, Landroid/view/View;

    invoke-static {v0, p1}, Ld9/e;->s0(Landroid/view/View;Z)V

    :cond_0
    return-void
.end method

.method public u0(I)Lmiuix/animation/f;
    .locals 4

    const/4 v0, 0x1

    iput-boolean v0, p0, Ld9/e;->j:Z

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Ld9/e;->k:Z

    iget-object v0, p0, Ld9/b;->a:Ld9/h;

    sget-object v1, Lmiuix/animation/f$b;->a:Lmiuix/animation/f$b;

    invoke-interface {v0, v1}, Ld9/h;->H(Ljava/lang/Object;)Ld9/a;

    move-result-object v0

    sget-object v1, Lh9/i;->a:Lh9/i$c;

    int-to-double v2, p1

    invoke-virtual {v0, v1, v2, v3}, Ld9/a;->a(Ljava/lang/Object;D)Ld9/a;

    return-object p0
.end method

.method public varargs x(Landroid/view/View;[Lc9/a;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ld9/e;->P(Landroid/view/View;[Lc9/a;)V

    return-void
.end method
