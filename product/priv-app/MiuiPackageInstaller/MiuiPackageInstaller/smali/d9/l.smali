.class Ld9/l;
.super Ljava/lang/Object;


# instance fields
.field final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Object;",
            "Ld9/a;",
            ">;"
        }
    .end annotation
.end field

.field b:Ljava/lang/Object;

.field final c:Ld9/a;

.field final d:Ld9/a;

.field final e:Ld9/a;

.field f:Ld9/k;


# direct methods
.method constructor <init>()V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    iput-object v0, p0, Ld9/l;->a:Ljava/util/Map;

    new-instance v0, Ld9/a;

    const-string v1, "defaultTo"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Ld9/a;-><init>(Ljava/lang/Object;Z)V

    iput-object v0, p0, Ld9/l;->c:Ld9/a;

    new-instance v0, Ld9/a;

    const-string v1, "defaultSetTo"

    invoke-direct {v0, v1, v2}, Ld9/a;-><init>(Ljava/lang/Object;Z)V

    iput-object v0, p0, Ld9/l;->d:Ld9/a;

    new-instance v0, Ld9/a;

    const-string v1, "autoSetTo"

    invoke-direct {v0, v1, v2}, Ld9/a;-><init>(Ljava/lang/Object;Z)V

    iput-object v0, p0, Ld9/l;->e:Ld9/a;

    new-instance v0, Ld9/k;

    invoke-direct {v0}, Ld9/k;-><init>()V

    iput-object v0, p0, Ld9/l;->f:Ld9/k;

    return-void
.end method

.method private i(Ljava/lang/Object;Z)Ld9/a;
    .locals 1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    instance-of v0, p1, Ld9/a;

    if-eqz v0, :cond_1

    check-cast p1, Ld9/a;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Ld9/l;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ld9/a;

    if-nez v0, :cond_2

    if-eqz p2, :cond_2

    new-instance p2, Ld9/a;

    invoke-direct {p2, p1}, Ld9/a;-><init>(Ljava/lang/Object;)V

    invoke-virtual {p0, p2}, Ld9/l;->c(Ld9/a;)V

    move-object p1, p2

    goto :goto_0

    :cond_2
    move-object p1, v0

    :goto_0
    return-object p1
.end method

.method private varargs j(Ljava/lang/Object;[Ljava/lang/Object;)Ld9/a;
    .locals 2

    array-length v0, p2

    if-lez v0, :cond_0

    const/4 v0, 0x0

    aget-object v1, p2, v0

    invoke-direct {p0, v1, v0}, Ld9/l;->i(Ljava/lang/Object;Z)Ld9/a;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-direct {p0, p2}, Ld9/l;->k([Ljava/lang/Object;)Ld9/a;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :cond_1
    :goto_0
    if-nez v0, :cond_2

    invoke-virtual {p0, p1}, Ld9/l;->h(Ljava/lang/Object;)Ld9/a;

    move-result-object v0

    :cond_2
    return-object v0
.end method

.method private varargs k([Ljava/lang/Object;)Ld9/a;
    .locals 4

    const/4 v0, 0x0

    aget-object v0, p1, v0

    array-length v1, p1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-le v1, v3, :cond_0

    aget-object p1, p1, v3

    goto :goto_0

    :cond_0
    move-object p1, v2

    :goto_0
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_1

    instance-of p1, p1, Ljava/lang/String;

    if-eqz p1, :cond_1

    invoke-direct {p0, v0, v3}, Ld9/l;->i(Ljava/lang/Object;Z)Ld9/a;

    move-result-object p1

    return-object p1

    :cond_1
    return-object v2
.end method

.method private varargs n(Lmiuix/animation/b;Ld9/a;Lc9/b;[Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Ld9/l;->f:Ld9/k;

    invoke-virtual {v0, p1, p2, p3, p4}, Ld9/k;->h(Lmiuix/animation/b;Ld9/a;Lc9/b;[Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public a(Lh9/b;F)V
    .locals 3

    invoke-virtual {p0}, Ld9/l;->f()Ld9/a;

    move-result-object v0

    float-to-double v1, p2

    invoke-virtual {v0, p1, v1, v2}, Ld9/a;->a(Ljava/lang/Object;D)Ld9/a;

    return-void
.end method

.method public b(Ljava/lang/String;I)V
    .locals 3

    invoke-virtual {p0}, Ld9/l;->f()Ld9/a;

    move-result-object v0

    int-to-double v1, p2

    invoke-virtual {v0, p1, v1, v2}, Ld9/a;->a(Ljava/lang/Object;D)Ld9/a;

    return-void
.end method

.method public c(Ld9/a;)V
    .locals 2

    iget-object v0, p0, Ld9/l;->a:Ljava/util/Map;

    invoke-virtual {p1}, Ld9/a;->l()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public d(Ld9/a;Lc9/b;)V
    .locals 1

    iget-object v0, p0, Ld9/l;->c:Ld9/a;

    if-eq p1, v0, :cond_0

    invoke-virtual {v0}, Ld9/a;->g()Lc9/a;

    move-result-object p1

    const/4 v0, 0x0

    new-array v0, v0, [Z

    invoke-virtual {p2, p1, v0}, Lc9/b;->a(Lc9/a;[Z)V

    :cond_0
    return-void
.end method

.method public e(Ld9/a;)V
    .locals 1

    iget-object v0, p0, Ld9/l;->c:Ld9/a;

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Ld9/l;->d:Ld9/a;

    if-ne p1, v0, :cond_1

    :cond_0
    invoke-virtual {p1}, Ld9/a;->d()V

    :cond_1
    return-void
.end method

.method public f()Ld9/a;
    .locals 1

    iget-object v0, p0, Ld9/l;->b:Ljava/lang/Object;

    if-nez v0, :cond_0

    iget-object v0, p0, Ld9/l;->c:Ld9/a;

    iput-object v0, p0, Ld9/l;->b:Ljava/lang/Object;

    :cond_0
    iget-object v0, p0, Ld9/l;->b:Ljava/lang/Object;

    invoke-virtual {p0, v0}, Ld9/l;->h(Ljava/lang/Object;)Ld9/a;

    move-result-object v0

    return-object v0
.end method

.method public varargs g(Lmiuix/animation/b;Lc9/b;[Ljava/lang/Object;)Ld9/a;
    .locals 1

    iget-object v0, p0, Ld9/l;->d:Ld9/a;

    invoke-direct {p0, v0, p3}, Ld9/l;->j(Ljava/lang/Object;[Ljava/lang/Object;)Ld9/a;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2, p3}, Ld9/l;->n(Lmiuix/animation/b;Ld9/a;Lc9/b;[Ljava/lang/Object;)V

    return-object v0
.end method

.method public h(Ljava/lang/Object;)Ld9/a;
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Ld9/l;->i(Ljava/lang/Object;Z)Ld9/a;

    move-result-object p1

    return-object p1
.end method

.method public varargs l(Lmiuix/animation/b;Lc9/b;[Ljava/lang/Object;)Ld9/a;
    .locals 1

    invoke-virtual {p0}, Ld9/l;->f()Ld9/a;

    move-result-object v0

    invoke-direct {p0, v0, p3}, Ld9/l;->j(Ljava/lang/Object;[Ljava/lang/Object;)Ld9/a;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2, p3}, Ld9/l;->n(Lmiuix/animation/b;Ld9/a;Lc9/b;[Ljava/lang/Object;)V

    return-object v0
.end method

.method public m(Ljava/lang/Object;)Z
    .locals 1

    iget-object v0, p0, Ld9/l;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public o(Ljava/lang/Object;)Ld9/a;
    .locals 1

    instance-of v0, p1, Ld9/a;

    if-eqz v0, :cond_0

    check-cast p1, Ld9/a;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Ld9/l;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ld9/a;

    if-nez v0, :cond_1

    new-instance v0, Ld9/a;

    invoke-direct {v0, p1}, Ld9/a;-><init>(Ljava/lang/Object;)V

    invoke-virtual {p0, v0}, Ld9/l;->c(Ld9/a;)V

    :cond_1
    move-object p1, v0

    :goto_0
    iput-object p1, p0, Ld9/l;->b:Ljava/lang/Object;

    return-object p1
.end method
