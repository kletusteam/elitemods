.class public Ld9/a;
.super Ljava/lang/Object;


# static fields
.field private static final h:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field a:Lh9/f;

.field b:Lh9/e;

.field public final c:Z

.field public d:J

.field private volatile e:Ljava/lang/Object;

.field private final f:Lc9/a;

.field private final g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Object;",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    sput-object v0, Ld9/a;->h:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Ld9/a;-><init>(Ljava/lang/Object;Z)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Ld9/a;-><init>(Ljava/lang/Object;Z)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;Z)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lh9/f;

    const-string v1, ""

    invoke-direct {v0, v1}, Lh9/f;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Ld9/a;->a:Lh9/f;

    new-instance v0, Lh9/e;

    invoke-direct {v0, v1}, Lh9/e;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Ld9/a;->b:Lh9/e;

    new-instance v0, Lc9/a;

    invoke-direct {v0}, Lc9/a;-><init>()V

    iput-object v0, p0, Ld9/a;->f:Lc9/a;

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Ld9/a;->g:Ljava/util/Map;

    invoke-virtual {p0, p1}, Ld9/a;->s(Ljava/lang/Object;)V

    iput-boolean p2, p0, Ld9/a;->c:Z

    return-void
.end method

.method public static b(Ld9/a;Ljava/util/Collection;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ld9/a;",
            "Ljava/util/Collection<",
            "Lf9/c;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf9/c;

    iget-object v2, v1, Lf9/c;->a:Lh9/b;

    invoke-virtual {p0, v2}, Ld9/a;->e(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-boolean v2, v1, Lf9/c;->b:Z

    if-eqz v2, :cond_1

    iget-object v2, v1, Lf9/c;->a:Lh9/b;

    iget-object v1, v1, Lf9/c;->f:Le9/c;

    iget-wide v3, v1, Le9/c;->g:D

    double-to-int v1, v3

    int-to-double v3, v1

    goto :goto_1

    :cond_1
    iget-object v2, v1, Lf9/c;->a:Lh9/b;

    iget-object v1, v1, Lf9/c;->f:Le9/c;

    iget-wide v3, v1, Le9/c;->g:D

    double-to-float v1, v3

    float-to-double v3, v1

    :goto_1
    invoke-virtual {p0, v2, v3, v4}, Ld9/a;->a(Ljava/lang/Object;D)Ld9/a;

    goto :goto_0

    :cond_2
    const-class v0, Ljava/util/ArrayList;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lj9/g;->b(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-virtual {p0}, Ld9/a;->n()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    instance-of v3, v2, Lh9/b;

    if-eqz v3, :cond_4

    move-object v3, v2

    check-cast v3, Lh9/b;

    invoke-static {p1, v3}, Lf9/c;->a(Ljava/util/Collection;Lh9/b;)Lf9/c;

    move-result-object v3

    goto :goto_3

    :cond_4
    move-object v3, v2

    check-cast v3, Ljava/lang/String;

    invoke-static {p1, v3}, Lf9/c;->b(Ljava/util/Collection;Ljava/lang/String;)Lf9/c;

    move-result-object v3

    :goto_3
    if-nez v3, :cond_3

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_5
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_4
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, v1}, Ld9/a;->o(Ljava/lang/Object;)Ld9/a;

    goto :goto_4

    :cond_6
    invoke-static {v0}, Lj9/g;->g(Ljava/lang/Object;)V

    return-void
.end method

.method private c(Ld9/a;)V
    .locals 2

    iget-object v0, p0, Ld9/a;->f:Lc9/a;

    iget-object v1, p1, Ld9/a;->f:Lc9/a;

    invoke-virtual {v0, v1}, Lc9/a;->d(Lc9/a;)V

    iget-object v0, p0, Ld9/a;->g:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iget-object v0, p0, Ld9/a;->g:Ljava/util/Map;

    iget-object p1, p1, Ld9/a;->g:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    return-void
.end method

.method private i(Ljava/lang/Object;)Ljava/lang/Double;
    .locals 2

    iget-object v0, p0, Ld9/a;->g:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    if-nez v0, :cond_0

    instance-of v1, p1, Lh9/b;

    if-eqz v1, :cond_0

    iget-object v0, p0, Ld9/a;->g:Ljava/util/Map;

    check-cast p1, Lh9/b;

    invoke-virtual {p1}, Landroid/util/Property;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    move-object v0, p1

    check-cast v0, Ljava/lang/Double;

    :cond_0
    return-object v0
.end method

.method private j(Lmiuix/animation/b;Lh9/b;D)D
    .locals 7

    invoke-virtual {p0, p2}, Ld9/a;->h(Ljava/lang/Object;)J

    move-result-wide v0

    const-wide/16 v2, 0x1

    invoke-static {v0, v1, v2, v3}, Lj9/a;->h(JJ)Z

    move-result v2

    if-nez v2, :cond_1

    const-wide v3, 0x412e848000000000L    # 1000000.0

    cmpl-double v3, p3, v3

    if-eqz v3, :cond_1

    const-wide v3, 0x412e854800000000L    # 1000100.0

    cmpl-double v3, p3, v3

    if-eqz v3, :cond_1

    instance-of v3, p2, Lh9/d;

    if-eqz v3, :cond_0

    goto :goto_0

    :cond_0
    return-wide p3

    :cond_1
    :goto_0
    invoke-static {p1, p2, p3, p4}, Le9/j;->b(Lmiuix/animation/b;Lh9/b;D)D

    move-result-wide v3

    if-eqz v2, :cond_2

    invoke-static {p3, p4}, Le9/j;->e(D)Z

    move-result p1

    if-nez p1, :cond_2

    const-wide/16 v5, -0x2

    and-long/2addr v0, v5

    invoke-virtual {p0, p2, v0, v1}, Ld9/a;->q(Ljava/lang/Object;J)V

    add-double/2addr v3, p3

    invoke-direct {p0, p2, v3, v4}, Ld9/a;->r(Ljava/lang/Object;D)V

    :cond_2
    return-wide v3
.end method

.method private r(Ljava/lang/Object;D)V
    .locals 3

    instance-of v0, p1, Lh9/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ld9/a;->g:Ljava/util/Map;

    move-object v1, p1

    check-cast v1, Lh9/b;

    invoke-virtual {v1}, Landroid/util/Property;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p1, p0, Ld9/a;->g:Ljava/util/Map;

    invoke-virtual {v1}, Landroid/util/Property;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, p3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p2

    invoke-interface {p1, v0, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Ld9/a;->g:Ljava/util/Map;

    invoke-static {p2, p3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p2

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;D)Ld9/a;
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Ld9/a;->r(Ljava/lang/Object;D)V

    return-object p0
.end method

.method public d()V
    .locals 1

    iget-object v0, p0, Ld9/a;->f:Lc9/a;

    invoke-virtual {v0}, Lc9/a;->c()V

    iget-object v0, p0, Ld9/a;->g:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    return-void
.end method

.method public e(Ljava/lang/Object;)Z
    .locals 2

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    :cond_0
    iget-object v1, p0, Ld9/a;->g:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 p1, 0x1

    return p1

    :cond_1
    instance-of v1, p1, Lh9/b;

    if-eqz v1, :cond_2

    iget-object v0, p0, Ld9/a;->g:Ljava/util/Map;

    check-cast p1, Lh9/b;

    invoke-virtual {p1}, Landroid/util/Property;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    return p1

    :cond_2
    return v0
.end method

.method public f(Lmiuix/animation/b;Lh9/b;)D
    .locals 2

    invoke-direct {p0, p2}, Ld9/a;->i(Ljava/lang/Object;)Ljava/lang/Double;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    invoke-direct {p0, p1, p2, v0, v1}, Ld9/a;->j(Lmiuix/animation/b;Lh9/b;D)D

    move-result-wide p1

    return-wide p1

    :cond_0
    const-wide p1, 0x7fefffffffffffffL    # Double.MAX_VALUE

    return-wide p1
.end method

.method public g()Lc9/a;
    .locals 1

    iget-object v0, p0, Ld9/a;->f:Lc9/a;

    return-object v0
.end method

.method public h(Ljava/lang/Object;)J
    .locals 2

    instance-of v0, p1, Lh9/b;

    if-eqz v0, :cond_0

    check-cast p1, Lh9/b;

    invoke-virtual {p1}, Landroid/util/Property;->getName()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    check-cast p1, Ljava/lang/String;

    :goto_0
    iget-object v0, p0, Ld9/a;->f:Lc9/a;

    invoke-virtual {v0, p1}, Lc9/a;->e(Ljava/lang/String;)Lc9/c;

    move-result-object p1

    if-eqz p1, :cond_1

    iget-wide v0, p1, Lc9/a;->h:J

    goto :goto_1

    :cond_1
    const-wide/16 v0, 0x0

    :goto_1
    return-wide v0
.end method

.method public k(Ljava/lang/Object;)Lh9/b;
    .locals 4

    instance-of v0, p1, Lh9/b;

    if-eqz v0, :cond_0

    check-cast p1, Lh9/b;

    return-object p1

    :cond_0
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Ld9/a;->h(Ljava/lang/Object;)J

    move-result-wide v0

    const-wide/16 v2, 0x4

    invoke-static {v0, v1, v2, v3}, Lj9/a;->h(JJ)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lh9/e;

    invoke-direct {v0, p1}, Lh9/e;-><init>(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    new-instance v0, Lh9/f;

    invoke-direct {v0, p1}, Lh9/f;-><init>(Ljava/lang/String;)V

    :goto_0
    return-object v0
.end method

.method public l()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Ld9/a;->e:Ljava/lang/Object;

    return-object v0
.end method

.method public m(Ljava/lang/Object;)Lh9/b;
    .locals 4

    instance-of v0, p1, Lh9/b;

    if-eqz v0, :cond_0

    check-cast p1, Lh9/b;

    return-object p1

    :cond_0
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Ld9/a;->h(Ljava/lang/Object;)J

    move-result-wide v0

    const-wide/16 v2, 0x4

    invoke-static {v0, v1, v2, v3}, Lj9/a;->h(JJ)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Ld9/a;->b:Lh9/e;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Ld9/a;->a:Lh9/f;

    :goto_0
    invoke-virtual {v0, p1}, Lh9/f;->g(Ljava/lang/String;)V

    return-object v0
.end method

.method public n()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Ld9/a;->g:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public o(Ljava/lang/Object;)Ld9/a;
    .locals 1

    iget-object v0, p0, Ld9/a;->g:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    instance-of v0, p1, Lh9/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ld9/a;->g:Ljava/util/Map;

    check-cast p1, Lh9/b;

    invoke-virtual {p1}, Landroid/util/Property;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object p0
.end method

.method public p(Ld9/a;)V
    .locals 1

    if-nez p1, :cond_0

    return-void

    :cond_0
    iget-object v0, p1, Ld9/a;->e:Ljava/lang/Object;

    invoke-virtual {p0, v0}, Ld9/a;->s(Ljava/lang/Object;)V

    invoke-direct {p0, p1}, Ld9/a;->c(Ld9/a;)V

    return-void
.end method

.method public q(Ljava/lang/Object;J)V
    .locals 1

    instance-of v0, p1, Lh9/b;

    if-eqz v0, :cond_0

    check-cast p1, Lh9/b;

    invoke-virtual {p1}, Landroid/util/Property;->getName()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    check-cast p1, Ljava/lang/String;

    :goto_0
    iget-object v0, p0, Ld9/a;->f:Lc9/a;

    invoke-virtual {v0, p1}, Lc9/a;->g(Ljava/lang/String;)Lc9/c;

    move-result-object p1

    iput-wide p2, p1, Lc9/a;->h:J

    return-void
.end method

.method public final s(Ljava/lang/Object;)V
    .locals 1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "TAG_"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v0, Ld9/a;->h:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :goto_0
    iput-object p1, p0, Ld9/a;->e:Ljava/lang/Object;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\nAnimState{mTag=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Ld9/a;->e:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, ", flags:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Ld9/a;->d:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", mConfig:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Ld9/a;->f:Lc9/a;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", mMaps="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Ld9/a;->g:Ljava/util/Map;

    const-string v2, "    "

    invoke-static {v1, v2}, Lj9/a;->l(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
