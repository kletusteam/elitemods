.class public Ld9/c;
.super Ld9/b;

# interfaces
.implements Lmiuix/animation/c;


# instance fields
.field private b:J

.field private c:I

.field private d:Lc9/a;

.field private e:Lc9/a;

.field private f:Lc9/a;

.field private g:Ljava/lang/Runnable;

.field h:I


# direct methods
.method public varargs constructor <init>([Lmiuix/animation/b;)V
    .locals 5

    invoke-direct {p0, p1}, Ld9/b;-><init>([Lmiuix/animation/b;)V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ld9/c;->b:J

    const/4 p1, 0x1

    iput p1, p0, Ld9/c;->c:I

    new-instance v0, Lc9/a;

    invoke-direct {v0}, Lc9/a;-><init>()V

    new-array v1, p1, [F

    const/high16 v2, 0x44160000    # 600.0f

    const/4 v3, 0x0

    aput v2, v1, v3

    const/4 v2, 0x6

    invoke-virtual {v0, v2, v1}, Lc9/a;->k(I[F)Lc9/a;

    move-result-object v0

    iput-object v0, p0, Ld9/c;->d:Lc9/a;

    new-instance v0, Lc9/a;

    invoke-direct {v0}, Lc9/a;-><init>()V

    new-array v1, p1, [F

    const/high16 v4, 0x43c80000    # 400.0f

    aput v4, v1, v3

    const/16 v4, 0x10

    invoke-virtual {v0, v4, v1}, Lc9/a;->k(I[F)Lc9/a;

    move-result-object v0

    iput-object v0, p0, Ld9/c;->e:Lc9/a;

    new-instance v0, Lc9/a;

    invoke-direct {v0}, Lc9/a;-><init>()V

    new-array v1, p1, [F

    const/high16 v4, 0x42c80000    # 100.0f

    aput v4, v1, v3

    invoke-virtual {v0, v2, v1}, Lc9/a;->k(I[F)Lc9/a;

    move-result-object v0

    iput-object v0, p0, Ld9/c;->f:Lc9/a;

    new-instance v0, Ld9/c$a;

    invoke-direct {v0, p0}, Ld9/c$a;-><init>(Ld9/c;)V

    iput-object v0, p0, Ld9/c;->g:Ljava/lang/Runnable;

    iput v3, p0, Ld9/c;->h:I

    invoke-direct {p0}, Ld9/c;->M()V

    iget-object v0, p0, Ld9/c;->e:Lc9/a;

    new-array v1, p1, [Lf9/b;

    new-instance v2, Ld9/c$d;

    invoke-direct {v2, p0}, Ld9/c$d;-><init>(Ld9/c;)V

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lc9/a;->a([Lf9/b;)Lc9/a;

    iget-object v0, p0, Ld9/c;->d:Lc9/a;

    new-array v1, p1, [Lf9/b;

    new-instance v2, Ld9/c$e;

    invoke-direct {v2, p0}, Ld9/c$e;-><init>(Ld9/c;)V

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lc9/a;->a([Lf9/b;)Lc9/a;

    iget-object v0, p0, Ld9/c;->f:Lc9/a;

    new-array p1, p1, [Lf9/b;

    new-instance v1, Ld9/c$f;

    invoke-direct {v1, p0}, Ld9/c$f;-><init>(Ld9/c;)V

    aput-object v1, p1, v3

    invoke-virtual {v0, p1}, Lc9/a;->a([Lf9/b;)Lc9/a;

    return-void
.end method

.method static synthetic J(Ld9/c;)Lc9/a;
    .locals 0

    iget-object p0, p0, Ld9/c;->d:Lc9/a;

    return-object p0
.end method

.method static synthetic K(Ld9/c;)Lc9/a;
    .locals 0

    iget-object p0, p0, Ld9/c;->e:Lc9/a;

    return-object p0
.end method

.method static synthetic L(Ld9/c;)I
    .locals 0

    iget p0, p0, Ld9/c;->c:I

    return p0
.end method

.method private M()V
    .locals 5

    const/16 v0, 0x14

    const/4 v1, 0x0

    invoke-static {v0, v1, v1, v1}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    iget-object v1, p0, Ld9/b;->a:Ld9/h;

    invoke-interface {v1}, Ld9/h;->g()Lmiuix/animation/b;

    move-result-object v1

    invoke-virtual {v1}, Lmiuix/animation/b;->h()Ljava/lang/Object;

    move-result-object v1

    instance-of v2, v1, Landroid/view/View;

    if-eqz v2, :cond_0

    check-cast v1, Landroid/view/View;

    sget v0, Lfa/a;->a:I

    invoke-virtual {v1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    :cond_0
    sget-object v1, Lh9/i;->a:Lh9/i$c;

    iget-object v2, p0, Ld9/b;->a:Ld9/h;

    sget-object v3, Lmiuix/animation/c$a;->a:Lmiuix/animation/c$a;

    invoke-interface {v2, v3}, Ld9/h;->H(Ljava/lang/Object;)Ld9/a;

    move-result-object v2

    int-to-double v3, v0

    invoke-virtual {v2, v1, v3, v4}, Ld9/a;->a(Ljava/lang/Object;D)Ld9/a;

    iget-object v0, p0, Ld9/b;->a:Ld9/h;

    sget-object v2, Lmiuix/animation/c$a;->b:Lmiuix/animation/c$a;

    invoke-interface {v0, v2}, Ld9/h;->H(Ljava/lang/Object;)Ld9/a;

    move-result-object v0

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Ld9/a;->a(Ljava/lang/Object;D)Ld9/a;

    return-void
.end method


# virtual methods
.method public N(Lc9/a;)Lmiuix/animation/c;
    .locals 3

    iput-object p1, p0, Ld9/c;->d:Lc9/a;

    const/4 v0, 0x1

    new-array v0, v0, [Lf9/b;

    new-instance v1, Ld9/c$b;

    invoke-direct {v1, p0}, Ld9/c$b;-><init>(Ld9/c;)V

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-virtual {p1, v0}, Lc9/a;->a([Lf9/b;)Lc9/a;

    return-object p0
.end method

.method public O(Lc9/a;)Lmiuix/animation/c;
    .locals 3

    iput-object p1, p0, Ld9/c;->e:Lc9/a;

    const/4 v0, 0x1

    new-array v0, v0, [Lf9/b;

    new-instance v1, Ld9/c$c;

    invoke-direct {v1, p0}, Ld9/c$c;-><init>(Ld9/c;)V

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-virtual {p1, v0}, Lc9/a;->a([Lf9/b;)Lc9/a;

    return-object p0
.end method

.method public varargs P([Lc9/a;)V
    .locals 3

    array-length v0, p1

    if-lez v0, :cond_0

    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-virtual {p0, v0}, Ld9/c;->N(Lc9/a;)Lmiuix/animation/c;

    array-length v0, p1

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    aget-object p1, p1, v1

    invoke-virtual {p0, p1}, Ld9/c;->O(Lc9/a;)Lmiuix/animation/c;

    :cond_0
    iget-object p1, p0, Ld9/c;->g:Ljava/lang/Runnable;

    if-eqz p1, :cond_2

    iget-object p1, p0, Ld9/b;->a:Ld9/h;

    invoke-interface {p1}, Ld9/h;->g()Lmiuix/animation/b;

    move-result-object p1

    iget-object p1, p1, Lmiuix/animation/b;->a:Le9/n;

    iget-object v0, p0, Ld9/c;->g:Ljava/lang/Runnable;

    iget v1, p0, Ld9/c;->h:I

    if-nez v1, :cond_1

    const-wide/16 v1, 0x0

    goto :goto_0

    :cond_1
    iget-wide v1, p0, Ld9/c;->b:J

    :goto_0
    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_2
    return-void
.end method

.method public varargs l(I[Lc9/a;)V
    .locals 2

    iput p1, p0, Ld9/c;->c:I

    array-length p1, p2

    const/4 v0, 0x0

    if-lez p1, :cond_0

    aget-object p1, p2, v0

    invoke-virtual {p0, p1}, Ld9/c;->N(Lc9/a;)Lmiuix/animation/c;

    array-length p1, p2

    const/4 v1, 0x1

    if-le p1, v1, :cond_0

    aget-object p1, p2, v1

    invoke-virtual {p0, p1}, Ld9/c;->O(Lc9/a;)Lmiuix/animation/c;

    :cond_0
    new-array p1, v0, [Lc9/a;

    invoke-virtual {p0, p1}, Ld9/c;->P([Lc9/a;)V

    return-void
.end method

.method public u()V
    .locals 5

    iget-object v0, p0, Ld9/b;->a:Ld9/h;

    invoke-interface {v0}, Ld9/h;->g()Lmiuix/animation/b;

    move-result-object v0

    iget-object v0, v0, Lmiuix/animation/b;->a:Le9/n;

    iget-object v1, p0, Ld9/c;->g:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Ld9/b;->a:Ld9/h;

    sget-object v1, Lmiuix/animation/c$a;->b:Lmiuix/animation/c$a;

    invoke-interface {v0, v1}, Ld9/h;->H(Ljava/lang/Object;)Ld9/a;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Lc9/a;

    iget-object v3, p0, Ld9/c;->f:Lc9/a;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-interface {v0, v1, v2}, Lmiuix/animation/h;->s(Ljava/lang/Object;[Lc9/a;)Lmiuix/animation/h;

    return-void
.end method
