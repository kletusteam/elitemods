.class public Ld9/f;
.super Ljava/lang/Object;

# interfaces
.implements Ld9/h;


# instance fields
.field a:Lmiuix/animation/b;

.field b:Ld9/l;

.field c:Lc9/b;

.field private d:Z


# direct methods
.method constructor <init>(Lmiuix/animation/b;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ld9/l;

    invoke-direct {v0}, Ld9/l;-><init>()V

    iput-object v0, p0, Ld9/f;->b:Ld9/l;

    new-instance v0, Lc9/b;

    invoke-direct {v0}, Lc9/b;-><init>()V

    iput-object v0, p0, Ld9/f;->c:Lc9/b;

    const/4 v0, 0x1

    iput-boolean v0, p0, Ld9/f;->d:Z

    iput-object p1, p0, Ld9/f;->a:Lmiuix/animation/b;

    return-void
.end method

.method private J(Ljava/lang/Object;Ljava/lang/Object;Lc9/b;)Lmiuix/animation/h;
    .locals 3

    iget-boolean v0, p0, Ld9/f;->d:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Ld9/f;->b:Ld9/l;

    invoke-virtual {v0, p2}, Ld9/l;->o(Ljava/lang/Object;)Ld9/a;

    if-eqz p1, :cond_0

    invoke-virtual {p0, p1}, Ld9/f;->C(Ljava/lang/Object;)Lmiuix/animation/h;

    :cond_0
    invoke-virtual {p0, p2}, Ld9/f;->H(Ljava/lang/Object;)Ld9/a;

    move-result-object v0

    iget-object v1, p0, Ld9/f;->b:Ld9/l;

    invoke-virtual {v1, v0, p3}, Ld9/l;->d(Ld9/a;Lc9/b;)V

    invoke-static {}, Le9/f;->m()Le9/f;

    move-result-object v1

    iget-object v2, p0, Ld9/f;->a:Lmiuix/animation/b;

    invoke-virtual {p0, p1}, Ld9/f;->H(Ljava/lang/Object;)Ld9/a;

    move-result-object p1

    invoke-virtual {p0, p2}, Ld9/f;->H(Ljava/lang/Object;)Ld9/a;

    move-result-object p2

    invoke-virtual {v1, v2, p1, p2, p3}, Le9/f;->p(Lmiuix/animation/b;Ld9/a;Ld9/a;Lc9/b;)V

    iget-object p1, p0, Ld9/f;->b:Ld9/l;

    invoke-virtual {p1, v0}, Ld9/l;->e(Ld9/a;)V

    invoke-virtual {p3}, Lc9/b;->d()V

    :cond_1
    return-object p0
.end method

.method private K()Lc9/b;
    .locals 1

    iget-object v0, p0, Ld9/f;->c:Lc9/b;

    return-object v0
.end method

.method private L(Ljava/lang/Object;Lc9/b;)Lmiuix/animation/h;
    .locals 2

    iget-object v0, p0, Ld9/f;->a:Lmiuix/animation/b;

    if-nez v0, :cond_0

    return-object p0

    :cond_0
    instance-of v1, p1, Ljava/lang/Integer;

    if-nez v1, :cond_2

    instance-of v1, p1, Ljava/lang/Float;

    if-eqz v1, :cond_1

    goto :goto_0

    :cond_1
    new-instance v1, Ld9/f$a;

    invoke-direct {v1, p0, p1, p2}, Ld9/f$a;-><init>(Ld9/f;Ljava/lang/Object;Lc9/b;)V

    invoke-virtual {v0, v1}, Lmiuix/animation/b;->b(Ljava/lang/Runnable;)V

    return-object p0

    :cond_2
    :goto_0
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 p1, 0x1

    aput-object p2, v0, p1

    invoke-virtual {p0, v0}, Ld9/f;->D([Ljava/lang/Object;)Lmiuix/animation/h;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public B(Lh9/b;F)Lmiuix/animation/h;
    .locals 1

    iget-object v0, p0, Ld9/f;->b:Ld9/l;

    invoke-virtual {v0, p1, p2}, Ld9/l;->a(Lh9/b;F)V

    return-object p0
.end method

.method public C(Ljava/lang/Object;)Lmiuix/animation/h;
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lc9/a;

    invoke-virtual {p0, p1, v0}, Ld9/f;->M(Ljava/lang/Object;[Lc9/a;)Lmiuix/animation/h;

    move-result-object p1

    return-object p1
.end method

.method public varargs D([Ljava/lang/Object;)Lmiuix/animation/h;
    .locals 3

    invoke-direct {p0}, Ld9/f;->K()Lc9/b;

    move-result-object v0

    iget-object v1, p0, Ld9/f;->b:Ld9/l;

    invoke-virtual {p0}, Ld9/f;->g()Lmiuix/animation/b;

    move-result-object v2

    invoke-virtual {v1, v2, v0, p1}, Ld9/l;->g(Lmiuix/animation/b;Lc9/b;[Ljava/lang/Object;)Ld9/a;

    move-result-object p1

    invoke-direct {p0, p1, v0}, Ld9/f;->L(Ljava/lang/Object;Lc9/b;)Lmiuix/animation/h;

    return-object p0
.end method

.method public H(Ljava/lang/Object;)Ld9/a;
    .locals 1

    iget-object v0, p0, Ld9/f;->b:Ld9/l;

    invoke-virtual {v0, p1}, Ld9/l;->h(Ljava/lang/Object;)Ld9/a;

    move-result-object p1

    return-object p1
.end method

.method public I(J)Lmiuix/animation/h;
    .locals 1

    invoke-virtual {p0}, Ld9/f;->g()Lmiuix/animation/b;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lmiuix/animation/b;->o(J)V

    return-object p0
.end method

.method public varargs M(Ljava/lang/Object;[Lc9/a;)Lmiuix/animation/h;
    .locals 0

    invoke-static {p2}, Lc9/b;->g([Lc9/a;)Lc9/b;

    move-result-object p2

    invoke-direct {p0, p1, p2}, Ld9/f;->L(Ljava/lang/Object;Lc9/b;)Lmiuix/animation/h;

    move-result-object p1

    return-object p1
.end method

.method public varargs c(Ljava/lang/Object;Ljava/lang/Object;[Lc9/a;)Lmiuix/animation/h;
    .locals 6

    invoke-direct {p0}, Ld9/f;->K()Lc9/b;

    move-result-object v0

    array-length v1, p3

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v1, :cond_0

    aget-object v4, p3, v3

    new-array v5, v2, [Z

    invoke-virtual {v0, v4, v5}, Lc9/b;->a(Lc9/a;[Z)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    invoke-direct {p0, p1, p2, v0}, Ld9/f;->J(Ljava/lang/Object;Ljava/lang/Object;Lc9/b;)Lmiuix/animation/h;

    move-result-object p1

    return-object p1
.end method

.method public cancel()V
    .locals 3

    invoke-static {}, Le9/f;->m()Le9/f;

    move-result-object v0

    iget-object v1, p0, Ld9/f;->a:Lmiuix/animation/b;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Le9/f;->f(Lmiuix/animation/b;[Lh9/b;)V

    return-void
.end method

.method public f(Ld9/a;)V
    .locals 1

    iget-object v0, p0, Ld9/f;->b:Ld9/l;

    invoke-virtual {v0, p1}, Ld9/l;->c(Ld9/a;)V

    return-void
.end method

.method public g()Lmiuix/animation/b;
    .locals 1

    iget-object v0, p0, Ld9/f;->a:Lmiuix/animation/b;

    return-object v0
.end method

.method public j()Ld9/a;
    .locals 1

    iget-object v0, p0, Ld9/f;->b:Ld9/l;

    invoke-virtual {v0}, Ld9/l;->f()Ld9/a;

    move-result-object v0

    return-object v0
.end method

.method public varargs k([Ljava/lang/Object;)V
    .locals 3

    array-length v0, p1

    if-lez v0, :cond_1

    const/4 v0, 0x0

    aget-object v1, p1, v0

    instance-of v1, v1, Lh9/b;

    if-eqz v1, :cond_0

    array-length v1, p1

    new-array v1, v1, [Lh9/b;

    array-length v2, p1

    invoke-static {p1, v0, v1, v0, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-static {}, Le9/f;->m()Le9/f;

    move-result-object p1

    iget-object v0, p0, Ld9/f;->a:Lmiuix/animation/b;

    invoke-virtual {p1, v0, v1}, Le9/f;->i(Lmiuix/animation/b;[Lh9/b;)V

    goto :goto_0

    :cond_0
    array-length v1, p1

    new-array v1, v1, [Ljava/lang/String;

    array-length v2, p1

    invoke-static {p1, v0, v1, v0, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-static {}, Le9/f;->m()Le9/f;

    move-result-object p1

    iget-object v0, p0, Ld9/f;->a:Lmiuix/animation/b;

    invoke-virtual {p1, v0, v1}, Le9/f;->j(Lmiuix/animation/b;[Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public n()V
    .locals 0

    invoke-virtual {p0}, Ld9/f;->cancel()V

    return-void
.end method

.method public o(Ljava/lang/String;I)Lmiuix/animation/h;
    .locals 1

    iget-object v0, p0, Ld9/f;->b:Ld9/l;

    invoke-virtual {v0, p1, p2}, Ld9/l;->b(Ljava/lang/String;I)V

    return-object p0
.end method

.method public varargs p([Ljava/lang/Object;)J
    .locals 4

    invoke-virtual {p0}, Ld9/f;->g()Lmiuix/animation/b;

    move-result-object v0

    invoke-direct {p0}, Ld9/f;->K()Lc9/b;

    move-result-object v1

    iget-object v2, p0, Ld9/f;->b:Ld9/l;

    invoke-virtual {v2, v0, v1, p1}, Ld9/l;->l(Lmiuix/animation/b;Lc9/b;[Ljava/lang/Object;)Ld9/a;

    move-result-object p1

    const/4 v2, 0x0

    invoke-static {v0, v2, p1, v1}, Le9/l;->a(Lmiuix/animation/b;Ld9/a;Ld9/a;Lc9/b;)J

    move-result-wide v2

    iget-object v0, p0, Ld9/f;->b:Ld9/l;

    invoke-virtual {v0, p1}, Ld9/l;->e(Ld9/a;)V

    invoke-virtual {v1}, Lc9/b;->d()V

    return-wide v2
.end method

.method public varargs s(Ljava/lang/Object;[Lc9/a;)Lmiuix/animation/h;
    .locals 3

    instance-of v0, p1, Ld9/a;

    if-nez v0, :cond_2

    iget-object v0, p0, Ld9/f;->b:Ld9/l;

    invoke-virtual {v0, p1}, Ld9/l;->m(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->isArray()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    invoke-static {p1}, Ljava/lang/reflect/Array;->getLength(Ljava/lang/Object;)I

    move-result v0

    array-length v2, p2

    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    array-length p1, p2

    invoke-static {p2, v1, v2, v0, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-virtual {p0, v2}, Ld9/f;->v([Ljava/lang/Object;)Lmiuix/animation/h;

    move-result-object p1

    return-object p1

    :cond_1
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v1

    const/4 p1, 0x1

    aput-object p2, v0, p1

    invoke-virtual {p0, v0}, Ld9/f;->v([Ljava/lang/Object;)Lmiuix/animation/h;

    move-result-object p1

    return-object p1

    :cond_2
    :goto_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1}, Ld9/f;->H(Ljava/lang/Object;)Ld9/a;

    move-result-object p1

    invoke-virtual {p0, v0, p1, p2}, Ld9/f;->c(Ljava/lang/Object;Ljava/lang/Object;[Lc9/a;)Lmiuix/animation/h;

    move-result-object p1

    return-object p1
.end method

.method public varargs t([Lc9/a;)Lmiuix/animation/h;
    .locals 1

    invoke-virtual {p0}, Ld9/f;->j()Ld9/a;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Ld9/f;->s(Ljava/lang/Object;[Lc9/a;)Lmiuix/animation/h;

    move-result-object p1

    return-object p1
.end method

.method public varargs v([Ljava/lang/Object;)Lmiuix/animation/h;
    .locals 3

    iget-object v0, p0, Ld9/f;->b:Ld9/l;

    invoke-virtual {p0}, Ld9/f;->g()Lmiuix/animation/b;

    move-result-object v1

    invoke-direct {p0}, Ld9/f;->K()Lc9/b;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p1}, Ld9/l;->l(Lmiuix/animation/b;Lc9/b;[Ljava/lang/Object;)Ld9/a;

    move-result-object p1

    const/4 v0, 0x0

    new-array v0, v0, [Lc9/a;

    const/4 v1, 0x0

    invoke-virtual {p0, v1, p1, v0}, Ld9/f;->c(Ljava/lang/Object;Ljava/lang/Object;[Lc9/a;)Lmiuix/animation/h;

    move-result-object p1

    return-object p1
.end method

.method public varargs w([Lh9/b;)V
    .locals 2

    invoke-static {}, Le9/f;->m()Le9/f;

    move-result-object v0

    iget-object v1, p0, Ld9/f;->a:Lmiuix/animation/b;

    invoke-virtual {v0, v1, p1}, Le9/f;->f(Lmiuix/animation/b;[Lh9/b;)V

    return-void
.end method

.method public y(Ljava/lang/Object;)Lmiuix/animation/h;
    .locals 1

    iget-object v0, p0, Ld9/f;->b:Ld9/l;

    invoke-virtual {v0, p1}, Ld9/l;->o(Ljava/lang/Object;)Ld9/a;

    return-object p0
.end method
