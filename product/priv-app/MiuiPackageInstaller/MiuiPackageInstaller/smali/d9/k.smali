.class Ld9/k;
.super Ljava/lang/Object;


# static fields
.field static final a:Lh9/f;

.field static final b:Lh9/e;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lh9/f;

    const-string v1, "defaultProperty"

    invoke-direct {v0, v1}, Lh9/f;-><init>(Ljava/lang/String;)V

    sput-object v0, Ld9/k;->a:Lh9/f;

    new-instance v0, Lh9/e;

    const-string v1, "defaultIntProperty"

    invoke-direct {v0, v1}, Lh9/e;-><init>(Ljava/lang/String;)V

    sput-object v0, Ld9/k;->b:Lh9/e;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lc9/b;Ljava/lang/Object;)Z
    .locals 2

    instance-of v0, p2, Lc9/a;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    check-cast p2, Lc9/a;

    new-array v0, v1, [Z

    invoke-virtual {p1, p2, v0}, Lc9/b;->a(Lc9/a;[Z)V

    const/4 p1, 0x1

    return p1

    :cond_0
    instance-of v0, p2, Lc9/b;

    if-eqz v0, :cond_1

    check-cast p2, Lc9/b;

    new-array v0, v1, [Z

    invoke-virtual {p1, p2, v0}, Lc9/b;->b(Lc9/b;[Z)V

    :cond_1
    return v1
.end method

.method private varargs b(Lmiuix/animation/b;Ld9/a;Lh9/b;I[Ljava/lang/Object;)I
    .locals 2

    const/4 v0, 0x1

    if-eqz p3, :cond_0

    invoke-direct {p0, p4, p5}, Ld9/k;->f(I[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-direct {p0, p2, p3, v1}, Ld9/k;->c(Ld9/a;Lh9/b;Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_0

    add-int/2addr p4, v0

    invoke-direct {p0, p1, p3, p4, p5}, Ld9/k;->i(Lmiuix/animation/b;Lh9/b;I[Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :cond_1
    :goto_0
    return v0
.end method

.method private c(Ld9/a;Lh9/b;Ljava/lang/Object;)Z
    .locals 2

    instance-of v0, p3, Ljava/lang/Integer;

    if-nez v0, :cond_1

    instance-of v1, p3, Ljava/lang/Float;

    if-nez v1, :cond_1

    instance-of v1, p3, Ljava/lang/Double;

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    instance-of v1, p2, Lh9/c;

    if-eqz v1, :cond_2

    invoke-direct {p0, p3, v0}, Ld9/k;->m(Ljava/lang/Object;Z)I

    move-result p3

    int-to-double v0, p3

    goto :goto_1

    :cond_2
    invoke-direct {p0, p3, v0}, Ld9/k;->l(Ljava/lang/Object;Z)F

    move-result p3

    float-to-double v0, p3

    :goto_1
    invoke-virtual {p1, p2, v0, v1}, Ld9/a;->a(Ljava/lang/Object;D)Ld9/a;

    const/4 p1, 0x1

    return p1
.end method

.method private d(Lc9/b;Ljava/lang/Object;)Z
    .locals 6

    instance-of v0, p2, Lf9/b;

    const/4 v1, 0x1

    if-nez v0, :cond_5

    instance-of v0, p2, Lj9/c$a;

    if-eqz v0, :cond_0

    goto :goto_3

    :cond_0
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->isArray()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {p2}, Ljava/lang/reflect/Array;->getLength(Ljava/lang/Object;)I

    move-result v0

    const/4 v2, 0x0

    move v3, v2

    move v4, v3

    :goto_0
    if-ge v3, v0, :cond_3

    invoke-static {p2, v3}, Ljava/lang/reflect/Array;->get(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v5

    invoke-direct {p0, p1, v5}, Ld9/k;->a(Lc9/b;Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    if-eqz v4, :cond_1

    goto :goto_1

    :cond_1
    move v4, v2

    goto :goto_2

    :cond_2
    :goto_1
    move v4, v1

    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_3
    return v4

    :cond_4
    invoke-direct {p0, p1, p2}, Ld9/k;->a(Lc9/b;Ljava/lang/Object;)Z

    move-result p1

    return p1

    :cond_5
    :goto_3
    invoke-virtual {p1}, Lc9/b;->f()Lc9/a;

    move-result-object p1

    invoke-direct {p0, p1, p2}, Ld9/k;->k(Lc9/a;Ljava/lang/Object;)V

    return v1
.end method

.method private e(Lmiuix/animation/b;Ljava/lang/Object;Ljava/lang/Object;)Lh9/b;
    .locals 2

    instance-of v0, p2, Lh9/b;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    move-object v1, p2

    check-cast v1, Lh9/b;

    goto :goto_0

    :cond_0
    instance-of v0, p2, Ljava/lang/String;

    if-eqz v0, :cond_2

    instance-of v0, p1, Lmiuix/animation/n;

    if-eqz v0, :cond_2

    if-eqz p3, :cond_1

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    :cond_1
    check-cast p1, Lmiuix/animation/n;

    check-cast p2, Ljava/lang/String;

    invoke-virtual {p1, p2, v1}, Lmiuix/animation/n;->v(Ljava/lang/String;Ljava/lang/Class;)Lh9/b;

    move-result-object v1

    goto :goto_0

    :cond_2
    instance-of p1, p2, Ljava/lang/Float;

    if-eqz p1, :cond_3

    sget-object v1, Ld9/k;->a:Lh9/f;

    :cond_3
    :goto_0
    return-object v1
.end method

.method private varargs f(I[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    array-length v0, p2

    if-ge p1, v0, :cond_0

    aget-object p1, p2, p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method private g(Lh9/b;)Z
    .locals 1

    sget-object v0, Ld9/k;->a:Lh9/f;

    if-eq p1, v0, :cond_1

    sget-object v0, Ld9/k;->b:Lh9/e;

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method private varargs i(Lmiuix/animation/b;Lh9/b;I[Ljava/lang/Object;)Z
    .locals 2

    array-length v0, p4

    const/4 v1, 0x0

    if-lt p3, v0, :cond_0

    return v1

    :cond_0
    aget-object p3, p4, p3

    instance-of p4, p3, Ljava/lang/Float;

    if-eqz p4, :cond_1

    check-cast p3, Ljava/lang/Float;

    invoke-virtual {p3}, Ljava/lang/Float;->floatValue()F

    move-result p3

    float-to-double p3, p3

    invoke-virtual {p1, p2, p3, p4}, Lmiuix/animation/b;->t(Lh9/b;D)V

    const/4 p1, 0x1

    return p1

    :cond_1
    return v1
.end method

.method private varargs j(Lmiuix/animation/b;Ld9/a;Lc9/b;Ljava/lang/Object;Ljava/lang/Object;I[Ljava/lang/Object;)I
    .locals 6

    invoke-direct {p0, p3, p4}, Ld9/k;->d(Lc9/b;Ljava/lang/Object;)Z

    move-result p3

    if-nez p3, :cond_1

    invoke-direct {p0, p1, p4, p5}, Ld9/k;->e(Lmiuix/animation/b;Ljava/lang/Object;Ljava/lang/Object;)Lh9/b;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-direct {p0, v3}, Ld9/k;->g(Lh9/b;)Z

    move-result p3

    if-eqz p3, :cond_0

    goto :goto_0

    :cond_0
    add-int/lit8 p6, p6, 0x1

    :goto_0
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v4, p6

    move-object v5, p7

    invoke-direct/range {v0 .. v5}, Ld9/k;->b(Lmiuix/animation/b;Ld9/a;Lh9/b;I[Ljava/lang/Object;)I

    move-result p1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    :goto_1
    if-lez p1, :cond_2

    add-int/2addr p6, p1

    goto :goto_2

    :cond_2
    add-int/lit8 p6, p6, 0x1

    :goto_2
    return p6
.end method

.method private k(Lc9/a;Ljava/lang/Object;)V
    .locals 2

    instance-of v0, p2, Lf9/b;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    new-array v0, v0, [Lf9/b;

    const/4 v1, 0x0

    check-cast p2, Lf9/b;

    aput-object p2, v0, v1

    invoke-virtual {p1, v0}, Lc9/a;->a([Lf9/b;)Lc9/a;

    goto :goto_0

    :cond_0
    instance-of v0, p2, Lj9/c$a;

    if-eqz v0, :cond_1

    check-cast p2, Lj9/c$a;

    invoke-virtual {p1, p2}, Lc9/a;->l(Lj9/c$a;)Lc9/a;

    :cond_1
    :goto_0
    return-void
.end method

.method private l(Ljava/lang/Object;Z)F
    .locals 0

    if-eqz p2, :cond_0

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    int-to-float p1, p1

    goto :goto_0

    :cond_0
    check-cast p1, Ljava/lang/Float;

    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result p1

    :goto_0
    return p1
.end method

.method private m(Ljava/lang/Object;Z)I
    .locals 0

    if-eqz p2, :cond_0

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    goto :goto_0

    :cond_0
    check-cast p1, Ljava/lang/Float;

    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result p1

    float-to-int p1, p1

    :goto_0
    return p1
.end method


# virtual methods
.method varargs h(Lmiuix/animation/b;Ld9/a;Lc9/b;[Ljava/lang/Object;)V
    .locals 9

    goto/32 :goto_4

    nop

    :goto_0
    goto :goto_1b

    :goto_1
    goto/32 :goto_17

    nop

    :goto_2
    if-lt v0, v1, :cond_0

    goto/32 :goto_16

    :cond_0
    goto/32 :goto_3

    nop

    :goto_3
    aget-object v1, p4, v0

    goto/32 :goto_15

    nop

    :goto_4
    array-length v0, p4

    goto/32 :goto_18

    nop

    :goto_5
    const/4 v1, 0x0

    :goto_6
    goto/32 :goto_22

    nop

    :goto_7
    return-void

    :goto_8
    goto/32 :goto_13

    nop

    :goto_9
    instance-of v1, v5, Ljava/lang/String;

    goto/32 :goto_1e

    nop

    :goto_a
    aget-object v0, p4, v0

    goto/32 :goto_21

    nop

    :goto_b
    return-void

    :goto_c
    aget-object v5, p4, v7

    goto/32 :goto_19

    nop

    :goto_d
    move-object v3, p2

    goto/32 :goto_11

    nop

    :goto_e
    move v7, v0

    :goto_f
    goto/32 :goto_23

    nop

    :goto_10
    move-object v8, p4

    goto/32 :goto_25

    nop

    :goto_11
    move-object v4, p3

    goto/32 :goto_10

    nop

    :goto_12
    if-nez v1, :cond_1

    goto/32 :goto_1

    :cond_1
    goto/32 :goto_0

    nop

    :goto_13
    const/4 v0, 0x0

    goto/32 :goto_a

    nop

    :goto_14
    array-length v1, p4

    goto/32 :goto_2

    nop

    :goto_15
    goto :goto_6

    :goto_16
    goto/32 :goto_5

    nop

    :goto_17
    move-object v1, p0

    goto/32 :goto_1f

    nop

    :goto_18
    if-eqz v0, :cond_2

    goto/32 :goto_8

    :cond_2
    goto/32 :goto_7

    nop

    :goto_19
    add-int/lit8 v0, v7, 0x1

    goto/32 :goto_14

    nop

    :goto_1a
    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    :goto_1b
    goto/32 :goto_e

    nop

    :goto_1c
    goto :goto_f

    :goto_1d
    goto/32 :goto_b

    nop

    :goto_1e
    if-nez v1, :cond_3

    goto/32 :goto_1

    :cond_3
    goto/32 :goto_20

    nop

    :goto_1f
    move-object v2, p1

    goto/32 :goto_d

    nop

    :goto_20
    instance-of v1, v6, Ljava/lang/String;

    goto/32 :goto_12

    nop

    :goto_21
    invoke-virtual {p2}, Ld9/a;->l()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_1a

    nop

    :goto_22
    move-object v6, v1

    goto/32 :goto_9

    nop

    :goto_23
    array-length v0, p4

    goto/32 :goto_24

    nop

    :goto_24
    if-lt v7, v0, :cond_4

    goto/32 :goto_1d

    :cond_4
    goto/32 :goto_c

    nop

    :goto_25
    invoke-direct/range {v1 .. v8}, Ld9/k;->j(Lmiuix/animation/b;Ld9/a;Lc9/b;Ljava/lang/Object;Ljava/lang/Object;I[Ljava/lang/Object;)I

    move-result v7

    goto/32 :goto_1c

    nop
.end method
