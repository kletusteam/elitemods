.class abstract Landroidx/recyclerview/widget/o;
.super Landroidx/recyclerview/widget/RecyclerView;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroidx/recyclerview/widget/o$a;
    }
.end annotation


# instance fields
.field private L0:I

.field private M0:Lj9/i;

.field private N0:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    sget v0, Lza/a;->a:I

    invoke-direct {p0, p1, p2, v0}, Landroidx/recyclerview/widget/o;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Landroidx/recyclerview/widget/RecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 p1, -0x1

    iput p1, p0, Landroidx/recyclerview/widget/o;->L0:I

    const/4 p1, 0x1

    iput-boolean p1, p0, Landroidx/recyclerview/widget/o;->N0:Z

    return-void
.end method

.method static synthetic A1(Landroidx/recyclerview/widget/o;)Lj9/i;
    .locals 0

    iget-object p0, p0, Landroidx/recyclerview/widget/o;->M0:Lj9/i;

    return-object p0
.end method

.method private C1(Landroid/view/MotionEvent;)V
    .locals 3

    iget-object v0, p0, Landroidx/recyclerview/widget/o;->M0:Lj9/i;

    if-nez v0, :cond_0

    new-instance v0, Lj9/i;

    invoke-direct {v0}, Lj9/i;-><init>()V

    iput-object v0, p0, Landroidx/recyclerview/widget/o;->M0:Lj9/i;

    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v1

    if-eqz v0, :cond_5

    const/4 v2, 0x2

    if-eq v0, v2, :cond_3

    const/4 v2, 0x5

    if-eq v0, v2, :cond_6

    const/4 v2, 0x6

    if-eq v0, v2, :cond_1

    goto :goto_2

    :cond_1
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    iget v2, p0, Landroidx/recyclerview/widget/o;->L0:I

    if-ne v0, v2, :cond_7

    if-nez v1, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    goto :goto_1

    :cond_3
    iget v0, p0, Landroidx/recyclerview/widget/o;->L0:I

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v0

    if-gez v0, :cond_4

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Error processing scroll; pointer index for id "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v0, p0, Landroidx/recyclerview/widget/o;->L0:I

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, " not found. Did any MotionEvents get skipped?"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "RecyclerView"

    invoke-static {v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_4
    invoke-direct {p0, p1, v0}, Landroidx/recyclerview/widget/o;->D1(Landroid/view/MotionEvent;I)V

    goto :goto_2

    :cond_5
    iget-object v0, p0, Landroidx/recyclerview/widget/o;->M0:Lj9/i;

    invoke-virtual {v0}, Lj9/i;->c()V

    :cond_6
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    :goto_1
    iput v0, p0, Landroidx/recyclerview/widget/o;->L0:I

    invoke-direct {p0, p1, v1}, Landroidx/recyclerview/widget/o;->D1(Landroid/view/MotionEvent;I)V

    :cond_7
    :goto_2
    return-void
.end method

.method private D1(Landroid/view/MotionEvent;I)V
    .locals 6

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x2

    const/16 v4, 0x1d

    if-lt v0, v4, :cond_0

    iget-object v0, p0, Landroidx/recyclerview/widget/o;->M0:Lj9/i;

    new-array v3, v3, [D

    invoke-virtual {p1, p2}, Landroid/view/MotionEvent;->getRawX(I)F

    move-result v4

    float-to-double v4, v4

    aput-wide v4, v3, v2

    invoke-virtual {p1, p2}, Landroid/view/MotionEvent;->getRawY(I)F

    move-result p1

    float-to-double p1, p1

    aput-wide p1, v3, v1

    invoke-virtual {v0, v3}, Lj9/i;->h([D)V

    goto :goto_0

    :cond_0
    iget-object p2, p0, Landroidx/recyclerview/widget/o;->M0:Lj9/i;

    new-array v0, v3, [D

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v3

    float-to-double v3, v3

    aput-wide v3, v0, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result p1

    float-to-double v2, p1

    aput-wide v2, v0, v1

    invoke-virtual {p2, v0}, Lj9/i;->h([D)V

    :goto_0
    return-void
.end method

.method static synthetic y1(Landroidx/recyclerview/widget/o;)Z
    .locals 0

    invoke-virtual {p0}, Landroid/view/ViewGroup;->awakenScrollBars()Z

    move-result p0

    return p0
.end method

.method static synthetic z1(Landroidx/recyclerview/widget/o;)Z
    .locals 0

    iget-boolean p0, p0, Landroidx/recyclerview/widget/o;->N0:Z

    return p0
.end method


# virtual methods
.method protected B1()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getSpringEnabled()Z
    .locals 1

    iget-boolean v0, p0, Landroidx/recyclerview/widget/o;->N0:Z

    return v0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 0

    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/o;->C1(Landroid/view/MotionEvent;)V

    invoke-super {p0, p1}, Landroidx/recyclerview/widget/RecyclerView;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 0

    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/o;->C1(Landroid/view/MotionEvent;)V

    invoke-super {p0, p1}, Landroidx/recyclerview/widget/RecyclerView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1
.end method

.method public setOverScrollMode(I)V
    .locals 1

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->setOverScrollMode(I)V

    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    const/4 p1, 0x0

    iput-boolean p1, p0, Landroidx/recyclerview/widget/o;->N0:Z

    :cond_0
    return-void
.end method

.method public setSpringEnabled(Z)V
    .locals 0

    iput-boolean p1, p0, Landroidx/recyclerview/widget/o;->N0:Z

    return-void
.end method
