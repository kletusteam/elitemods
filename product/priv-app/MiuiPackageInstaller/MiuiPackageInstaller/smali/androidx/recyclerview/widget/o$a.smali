.class Landroidx/recyclerview/widget/o$a;
.super Landroidx/recyclerview/widget/RecyclerView$c0;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroidx/recyclerview/widget/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "a"
.end annotation


# instance fields
.field private h:I

.field private i:I

.field j:Loa/c;

.field k:Landroid/view/animation/Interpolator;

.field private l:Z

.field private m:Z

.field private n:Z

.field o:I

.field p:I

.field q:Z

.field private r:Llb/a;

.field final synthetic s:Landroidx/recyclerview/widget/o;


# direct methods
.method constructor <init>(Landroidx/recyclerview/widget/o;)V
    .locals 2

    iput-object p1, p0, Landroidx/recyclerview/widget/o$a;->s:Landroidx/recyclerview/widget/o;

    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$c0;-><init>(Landroidx/recyclerview/widget/RecyclerView;)V

    sget-object v0, Landroidx/recyclerview/widget/RecyclerView;->K0:Landroid/view/animation/Interpolator;

    iput-object v0, p0, Landroidx/recyclerview/widget/o$a;->k:Landroid/view/animation/Interpolator;

    const/4 v1, 0x0

    iput-boolean v1, p0, Landroidx/recyclerview/widget/o$a;->l:Z

    iput-boolean v1, p0, Landroidx/recyclerview/widget/o$a;->m:Z

    iput v1, p0, Landroidx/recyclerview/widget/o$a;->o:I

    iput v1, p0, Landroidx/recyclerview/widget/o$a;->p:I

    iput-boolean v1, p0, Landroidx/recyclerview/widget/o$a;->q:Z

    new-instance v1, Loa/c;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-direct {v1, p1, v0}, Loa/c;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    iput-object v1, p0, Landroidx/recyclerview/widget/o$a;->j:Loa/c;

    return-void
.end method

.method private a(IIII)I
    .locals 4

    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v0

    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result v1

    if-le v0, v1, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    mul-int/2addr p3, p3

    mul-int/2addr p4, p4

    add-int/2addr p3, p4

    int-to-double p3, p3

    invoke-static {p3, p4}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide p3

    double-to-int p3, p3

    mul-int/2addr p1, p1

    mul-int/2addr p2, p2

    add-int/2addr p1, p2

    int-to-double p1, p1

    invoke-static {p1, p2}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide p1

    double-to-int p1, p1

    iget-object p2, p0, Landroidx/recyclerview/widget/o$a;->s:Landroidx/recyclerview/widget/o;

    if-eqz v2, :cond_1

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getWidth()I

    move-result p2

    goto :goto_1

    :cond_1
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getHeight()I

    move-result p2

    :goto_1
    div-int/lit8 p4, p2, 0x2

    int-to-float p1, p1

    const/high16 v3, 0x3f800000    # 1.0f

    mul-float/2addr p1, v3

    int-to-float p2, p2

    div-float/2addr p1, p2

    invoke-static {v3, p1}, Ljava/lang/Math;->min(FF)F

    move-result p1

    int-to-float p4, p4

    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/o$a;->b(F)F

    move-result p1

    mul-float/2addr p1, p4

    add-float/2addr p4, p1

    if-lez p3, :cond_2

    const/high16 p1, 0x447a0000    # 1000.0f

    int-to-float p2, p3

    div-float/2addr p4, p2

    invoke-static {p4}, Ljava/lang/Math;->abs(F)F

    move-result p2

    mul-float/2addr p2, p1

    invoke-static {p2}, Ljava/lang/Math;->round(F)I

    move-result p1

    mul-int/lit8 p1, p1, 0x4

    goto :goto_3

    :cond_2
    if-eqz v2, :cond_3

    goto :goto_2

    :cond_3
    move v0, v1

    :goto_2
    int-to-float p1, v0

    div-float/2addr p1, p2

    add-float/2addr p1, v3

    const/high16 p2, 0x43960000    # 300.0f

    mul-float/2addr p1, p2

    float-to-int p1, p1

    :goto_3
    const/16 p2, 0x7d0

    invoke-static {p1, p2}, Ljava/lang/Math;->min(II)I

    move-result p1

    return p1
.end method

.method private b(F)F
    .locals 2

    const/high16 v0, 0x3f000000    # 0.5f

    sub-float/2addr p1, v0

    const v0, 0x3ef1463b

    mul-float/2addr p1, v0

    float-to-double v0, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    double-to-float p1, v0

    return p1
.end method

.method private d()V
    .locals 1

    iget-object v0, p0, Landroidx/recyclerview/widget/o$a;->s:Landroidx/recyclerview/widget/o;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Landroidx/recyclerview/widget/o$a;->s:Landroidx/recyclerview/widget/o;

    invoke-static {v0, p0}, Landroidx/core/view/t;->a0(Landroid/view/View;Ljava/lang/Runnable;)V

    return-void
.end method

.method private h()Llb/a;
    .locals 2

    iget-object v0, p0, Landroidx/recyclerview/widget/o$a;->r:Llb/a;

    if-nez v0, :cond_0

    new-instance v0, Llb/a;

    iget-object v1, p0, Landroidx/recyclerview/widget/o$a;->s:Landroidx/recyclerview/widget/o;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Llb/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Landroidx/recyclerview/widget/o$a;->r:Llb/a;

    :cond_0
    iget-object v0, p0, Landroidx/recyclerview/widget/o$a;->r:Llb/a;

    return-object v0
.end method


# virtual methods
.method public c(II)V
    .locals 11

    iget-object v0, p0, Landroidx/recyclerview/widget/o$a;->s:Landroidx/recyclerview/widget/o;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setScrollState(I)V

    const/4 v0, 0x0

    iput v0, p0, Landroidx/recyclerview/widget/o$a;->i:I

    iput v0, p0, Landroidx/recyclerview/widget/o$a;->h:I

    iget-object v2, p0, Landroidx/recyclerview/widget/o$a;->k:Landroid/view/animation/Interpolator;

    sget-object v3, Landroidx/recyclerview/widget/RecyclerView;->K0:Landroid/view/animation/Interpolator;

    if-eq v2, v3, :cond_0

    iput-object v3, p0, Landroidx/recyclerview/widget/o$a;->k:Landroid/view/animation/Interpolator;

    new-instance v2, Loa/c;

    iget-object v4, p0, Landroidx/recyclerview/widget/o$a;->s:Landroidx/recyclerview/widget/o;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v2, v4, v3}, Loa/c;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    iput-object v2, p0, Landroidx/recyclerview/widget/o$a;->j:Loa/c;

    :cond_0
    if-eqz p1, :cond_1

    iget-object p1, p0, Landroidx/recyclerview/widget/o$a;->s:Landroidx/recyclerview/widget/o;

    invoke-static {p1}, Landroidx/recyclerview/widget/o;->A1(Landroidx/recyclerview/widget/o;)Lj9/i;

    move-result-object p1

    invoke-virtual {p1, v0}, Lj9/i;->g(I)F

    move-result p1

    float-to-int p1, p1

    neg-int p1, p1

    :cond_1
    move v5, p1

    const/4 p1, 0x1

    if-eqz p2, :cond_2

    iget-object p2, p0, Landroidx/recyclerview/widget/o$a;->s:Landroidx/recyclerview/widget/o;

    invoke-static {p2}, Landroidx/recyclerview/widget/o;->A1(Landroidx/recyclerview/widget/o;)Lj9/i;

    move-result-object p2

    invoke-virtual {p2, p1}, Lj9/i;->g(I)F

    move-result p2

    float-to-int p2, p2

    neg-int p2, p2

    :cond_2
    move v6, p2

    iget-object p2, p0, Landroidx/recyclerview/widget/o$a;->s:Landroidx/recyclerview/widget/o;

    iget-object p2, p2, Landroidx/recyclerview/widget/RecyclerView;->m:Landroidx/recyclerview/widget/RecyclerView$o;

    invoke-virtual {p2}, Landroidx/recyclerview/widget/RecyclerView$o;->k()Z

    move-result p2

    iget-object v0, p0, Landroidx/recyclerview/widget/o$a;->s:Landroidx/recyclerview/widget/o;

    iget-object v0, v0, Landroidx/recyclerview/widget/RecyclerView;->m:Landroidx/recyclerview/widget/RecyclerView$o;

    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView$o;->l()Z

    move-result v0

    if-eqz v0, :cond_3

    or-int/lit8 p2, p2, 0x2

    :cond_3
    const/4 v0, -0x1

    if-ne p2, v1, :cond_5

    if-lez v6, :cond_4

    move v0, p1

    :cond_4
    iget-object p2, p0, Landroidx/recyclerview/widget/o$a;->s:Landroidx/recyclerview/widget/o;

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->canScrollVertically(I)Z

    move-result p2

    :goto_0
    xor-int/2addr p1, p2

    iput-boolean p1, p0, Landroidx/recyclerview/widget/o$a;->n:Z

    goto :goto_1

    :cond_5
    if-ne p2, p1, :cond_7

    if-lez v5, :cond_6

    move v0, p1

    :cond_6
    iget-object p2, p0, Landroidx/recyclerview/widget/o$a;->s:Landroidx/recyclerview/widget/o;

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->canScrollHorizontally(I)Z

    move-result p2

    goto :goto_0

    :cond_7
    :goto_1
    iget-object v2, p0, Landroidx/recyclerview/widget/o$a;->j:Loa/c;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/high16 v7, -0x80000000

    const v8, 0x7fffffff

    const/high16 v9, -0x80000000

    const v10, 0x7fffffff

    invoke-virtual/range {v2 .. v10}, Loa/c;->c(IIIIIIII)V

    invoke-virtual {p0}, Landroidx/recyclerview/widget/o$a;->e()V

    return-void
.end method

.method e()V
    .locals 1

    iget-boolean v0, p0, Landroidx/recyclerview/widget/o$a;->l:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroidx/recyclerview/widget/o$a;->m:Z

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Landroidx/recyclerview/widget/o$a;->d()V

    :goto_0
    return-void
.end method

.method public f(IIILandroid/view/animation/Interpolator;)V
    .locals 8

    iget-object v0, p0, Landroidx/recyclerview/widget/o$a;->s:Landroidx/recyclerview/widget/o;

    invoke-virtual {v0}, Landroidx/recyclerview/widget/o;->B1()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/high16 v0, -0x80000000

    const/4 v1, 0x0

    if-ne p3, v0, :cond_1

    invoke-direct {p0, p1, p2, v1, v1}, Landroidx/recyclerview/widget/o$a;->a(IIII)I

    :cond_1
    if-nez p4, :cond_2

    sget-object p4, Landroidx/recyclerview/widget/RecyclerView;->K0:Landroid/view/animation/Interpolator;

    :cond_2
    iget-object p3, p0, Landroidx/recyclerview/widget/o$a;->j:Loa/c;

    invoke-virtual {p3}, Loa/c;->l()I

    move-result p3

    const/4 v0, 0x2

    if-ne p3, v0, :cond_3

    iget-boolean p3, p0, Landroidx/recyclerview/widget/o$a;->q:Z

    if-nez p3, :cond_3

    iget-object p3, p0, Landroidx/recyclerview/widget/o$a;->j:Loa/c;

    invoke-virtual {p3}, Loa/c;->g()F

    move-result p3

    float-to-int p3, p3

    iput p3, p0, Landroidx/recyclerview/widget/o$a;->p:I

    iget-object p3, p0, Landroidx/recyclerview/widget/o$a;->j:Loa/c;

    invoke-virtual {p3}, Loa/c;->f()F

    move-result p3

    float-to-int p3, p3

    iput p3, p0, Landroidx/recyclerview/widget/o$a;->o:I

    :cond_3
    iget-object p3, p0, Landroidx/recyclerview/widget/o$a;->s:Landroidx/recyclerview/widget/o;

    iget-object p3, p3, Landroidx/recyclerview/widget/RecyclerView;->m:Landroidx/recyclerview/widget/RecyclerView$o;

    iget-object p3, p3, Landroidx/recyclerview/widget/RecyclerView$o;->g:Landroidx/recyclerview/widget/RecyclerView$z;

    instance-of v2, p3, Landroidx/recyclerview/widget/i;

    if-eqz v2, :cond_4

    const v2, 0x3f99999a    # 1.2f

    move-object v3, p3

    check-cast v3, Landroidx/recyclerview/widget/i;

    iget v3, v3, Landroidx/recyclerview/widget/i;->o:I

    int-to-float v3, v3

    mul-float/2addr v3, v2

    check-cast p3, Landroidx/recyclerview/widget/i;

    iget p3, p3, Landroidx/recyclerview/widget/i;->p:I

    int-to-float p3, p3

    mul-float/2addr p3, v2

    int-to-float v2, p1

    cmpl-float v2, v3, v2

    if-nez v2, :cond_4

    int-to-float v2, p2

    cmpl-float p3, p3, v2

    if-nez p3, :cond_4

    const/4 p3, 0x1

    goto :goto_0

    :cond_4
    move p3, v1

    :goto_0
    iput-boolean p3, p0, Landroidx/recyclerview/widget/o$a;->q:Z

    iget-object p3, p0, Landroidx/recyclerview/widget/o$a;->k:Landroid/view/animation/Interpolator;

    if-eq p3, p4, :cond_5

    iput-object p4, p0, Landroidx/recyclerview/widget/o$a;->k:Landroid/view/animation/Interpolator;

    new-instance p3, Loa/c;

    iget-object v2, p0, Landroidx/recyclerview/widget/o$a;->s:Landroidx/recyclerview/widget/o;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {p3, v2, p4}, Loa/c;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    iput-object p3, p0, Landroidx/recyclerview/widget/o$a;->j:Loa/c;

    :cond_5
    iput v1, p0, Landroidx/recyclerview/widget/o$a;->i:I

    iput v1, p0, Landroidx/recyclerview/widget/o$a;->h:I

    iget-object p3, p0, Landroidx/recyclerview/widget/o$a;->s:Landroidx/recyclerview/widget/o;

    invoke-virtual {p3, v0}, Landroidx/recyclerview/widget/RecyclerView;->setScrollState(I)V

    iget-object v1, p0, Landroidx/recyclerview/widget/o$a;->j:Loa/c;

    const/4 v2, 0x0

    const/4 v3, 0x0

    iget v6, p0, Landroidx/recyclerview/widget/o$a;->o:I

    iget v7, p0, Landroidx/recyclerview/widget/o$a;->p:I

    move v4, p1

    move v5, p2

    invoke-virtual/range {v1 .. v7}, Loa/c;->u(IIIIII)V

    invoke-virtual {p0}, Landroidx/recyclerview/widget/o$a;->e()V

    return-void
.end method

.method public g()V
    .locals 1

    iget-object v0, p0, Landroidx/recyclerview/widget/o$a;->s:Landroidx/recyclerview/widget/o;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Landroidx/recyclerview/widget/o$a;->j:Loa/c;

    invoke-virtual {v0}, Loa/c;->a()V

    return-void
.end method

.method i()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Landroidx/recyclerview/widget/o$a;->i:I

    iput v0, p0, Landroidx/recyclerview/widget/o$a;->h:I

    return-void
.end method

.method public run()V
    .locals 22

    move-object/from16 v0, p0

    iget-object v1, v0, Landroidx/recyclerview/widget/o$a;->s:Landroidx/recyclerview/widget/o;

    iget-object v2, v1, Landroidx/recyclerview/widget/RecyclerView;->m:Landroidx/recyclerview/widget/RecyclerView$o;

    if-nez v2, :cond_0

    invoke-virtual/range {p0 .. p0}, Landroidx/recyclerview/widget/o$a;->g()V

    return-void

    :cond_0
    const/4 v2, 0x0

    iput-boolean v2, v0, Landroidx/recyclerview/widget/o$a;->m:Z

    const/4 v3, 0x1

    iput-boolean v3, v0, Landroidx/recyclerview/widget/o$a;->l:Z

    invoke-virtual {v1}, Landroidx/recyclerview/widget/RecyclerView;->u()V

    iget-object v1, v0, Landroidx/recyclerview/widget/o$a;->j:Loa/c;

    invoke-virtual {v1}, Loa/c;->b()Z

    move-result v4

    if-eqz v4, :cond_21

    invoke-virtual {v1}, Loa/c;->h()I

    move-result v4

    invoke-virtual {v1}, Loa/c;->i()I

    move-result v5

    iget-object v6, v0, Landroidx/recyclerview/widget/o$a;->j:Loa/c;

    invoke-virtual {v6}, Loa/c;->l()I

    move-result v6

    if-ne v6, v3, :cond_1

    invoke-virtual {v1}, Loa/c;->f()F

    move-result v6

    float-to-int v6, v6

    iput v6, v0, Landroidx/recyclerview/widget/o$a;->o:I

    invoke-virtual {v1}, Loa/c;->g()F

    move-result v6

    float-to-int v6, v6

    iput v6, v0, Landroidx/recyclerview/widget/o$a;->p:I

    :cond_1
    iget v6, v0, Landroidx/recyclerview/widget/o$a;->h:I

    sub-int v6, v4, v6

    iget v7, v0, Landroidx/recyclerview/widget/o$a;->i:I

    sub-int v13, v5, v7

    iput v4, v0, Landroidx/recyclerview/widget/o$a;->h:I

    iput v5, v0, Landroidx/recyclerview/widget/o$a;->i:I

    iget-object v4, v0, Landroidx/recyclerview/widget/o$a;->s:Landroidx/recyclerview/widget/o;

    iget-object v4, v4, Landroidx/recyclerview/widget/RecyclerView;->y0:[I

    aput v2, v4, v2

    aput v2, v4, v3

    iget-object v4, v0, Landroidx/recyclerview/widget/o$a;->j:Loa/c;

    invoke-virtual {v4}, Loa/c;->l()I

    move-result v4

    if-ne v4, v3, :cond_4

    iget-object v4, v0, Landroidx/recyclerview/widget/o$a;->s:Landroidx/recyclerview/widget/o;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    iget-object v5, v0, Landroidx/recyclerview/widget/o$a;->s:Landroidx/recyclerview/widget/o;

    invoke-virtual {v5}, Landroid/view/ViewGroup;->getRootView()Landroid/view/View;

    move-result-object v5

    const v7, 0x1020002

    invoke-virtual {v5, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    :goto_0
    if-eqz v4, :cond_4

    instance-of v7, v4, Lda/a;

    if-eqz v7, :cond_2

    move-object v7, v4

    check-cast v7, Lda/a;

    iget-object v8, v0, Landroidx/recyclerview/widget/o$a;->j:Loa/c;

    invoke-virtual {v8}, Loa/c;->f()F

    move-result v8

    iget-object v9, v0, Landroidx/recyclerview/widget/o$a;->j:Loa/c;

    invoke-virtual {v9}, Loa/c;->g()F

    move-result v9

    invoke-interface {v7, v8, v9}, Lda/a;->a(FF)Z

    move-result v7

    if-eqz v7, :cond_2

    goto :goto_1

    :cond_2
    instance-of v7, v4, Landroid/view/ViewGroup;

    if-eqz v7, :cond_3

    if-ne v4, v5, :cond_3

    goto :goto_1

    :cond_3
    invoke-interface {v4}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    goto :goto_0

    :cond_4
    :goto_1
    iget-object v7, v0, Landroidx/recyclerview/widget/o$a;->s:Landroidx/recyclerview/widget/o;

    iget-object v10, v7, Landroidx/recyclerview/widget/RecyclerView;->y0:[I

    const/4 v11, 0x0

    const/4 v12, 0x1

    move v8, v6

    move v9, v13

    invoke-virtual/range {v7 .. v12}, Landroidx/recyclerview/widget/RecyclerView;->F(II[I[II)Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v4, v0, Landroidx/recyclerview/widget/o$a;->s:Landroidx/recyclerview/widget/o;

    iget-object v4, v4, Landroidx/recyclerview/widget/RecyclerView;->y0:[I

    aget v5, v4, v2

    sub-int/2addr v6, v5

    aget v4, v4, v3

    sub-int/2addr v13, v4

    :cond_5
    iget-object v4, v0, Landroidx/recyclerview/widget/o$a;->s:Landroidx/recyclerview/widget/o;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getOverScrollMode()I

    move-result v4

    const/4 v5, 0x2

    if-eq v4, v5, :cond_6

    iget-object v4, v0, Landroidx/recyclerview/widget/o$a;->s:Landroidx/recyclerview/widget/o;

    invoke-virtual {v4, v6, v13}, Landroidx/recyclerview/widget/RecyclerView;->t(II)V

    :cond_6
    iget-object v4, v0, Landroidx/recyclerview/widget/o$a;->s:Landroidx/recyclerview/widget/o;

    iget-object v7, v4, Landroidx/recyclerview/widget/RecyclerView;->l:Landroidx/recyclerview/widget/RecyclerView$g;

    if-eqz v7, :cond_9

    iget-object v7, v4, Landroidx/recyclerview/widget/RecyclerView;->y0:[I

    aput v2, v7, v2

    aput v2, v7, v3

    invoke-virtual {v4, v6, v13, v7}, Landroidx/recyclerview/widget/RecyclerView;->h1(II[I)V

    iget-object v4, v0, Landroidx/recyclerview/widget/o$a;->s:Landroidx/recyclerview/widget/o;

    iget-object v7, v4, Landroidx/recyclerview/widget/RecyclerView;->y0:[I

    aget v8, v7, v2

    aget v7, v7, v3

    sub-int/2addr v6, v8

    sub-int/2addr v13, v7

    iget-object v4, v4, Landroidx/recyclerview/widget/RecyclerView;->m:Landroidx/recyclerview/widget/RecyclerView$o;

    iget-object v4, v4, Landroidx/recyclerview/widget/RecyclerView$o;->g:Landroidx/recyclerview/widget/RecyclerView$z;

    if-eqz v4, :cond_a

    invoke-virtual {v4}, Landroidx/recyclerview/widget/RecyclerView$z;->g()Z

    move-result v9

    if-nez v9, :cond_a

    invoke-virtual {v4}, Landroidx/recyclerview/widget/RecyclerView$z;->h()Z

    move-result v9

    if-eqz v9, :cond_a

    iget-object v9, v0, Landroidx/recyclerview/widget/o$a;->s:Landroidx/recyclerview/widget/o;

    iget-object v9, v9, Landroidx/recyclerview/widget/RecyclerView;->l0:Landroidx/recyclerview/widget/RecyclerView$a0;

    invoke-virtual {v9}, Landroidx/recyclerview/widget/RecyclerView$a0;->b()I

    move-result v9

    if-nez v9, :cond_7

    invoke-virtual {v4}, Landroidx/recyclerview/widget/RecyclerView$z;->r()V

    goto :goto_2

    :cond_7
    invoke-virtual {v4}, Landroidx/recyclerview/widget/RecyclerView$z;->f()I

    move-result v10

    if-lt v10, v9, :cond_8

    sub-int/2addr v9, v3

    invoke-virtual {v4, v9}, Landroidx/recyclerview/widget/RecyclerView$z;->p(I)V

    :cond_8
    invoke-virtual {v4, v8, v7}, Landroidx/recyclerview/widget/RecyclerView$z;->j(II)V

    goto :goto_2

    :cond_9
    move v7, v2

    move v8, v7

    :cond_a
    :goto_2
    iget-object v4, v0, Landroidx/recyclerview/widget/o$a;->s:Landroidx/recyclerview/widget/o;

    iget-object v4, v4, Landroidx/recyclerview/widget/RecyclerView;->o:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_b

    iget-object v4, v0, Landroidx/recyclerview/widget/o$a;->s:Landroidx/recyclerview/widget/o;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->invalidate()V

    :cond_b
    iget-object v14, v0, Landroidx/recyclerview/widget/o$a;->s:Landroidx/recyclerview/widget/o;

    iget-object v4, v14, Landroidx/recyclerview/widget/RecyclerView;->y0:[I

    aput v2, v4, v2

    aput v2, v4, v3

    const/16 v19, 0x0

    const/16 v20, 0x1

    move v15, v8

    move/from16 v16, v7

    move/from16 v17, v6

    move/from16 v18, v13

    move-object/from16 v21, v4

    invoke-virtual/range {v14 .. v21}, Landroidx/recyclerview/widget/RecyclerView;->G(IIII[II[I)V

    iget-object v4, v0, Landroidx/recyclerview/widget/o$a;->s:Landroidx/recyclerview/widget/o;

    iget-object v9, v4, Landroidx/recyclerview/widget/RecyclerView;->y0:[I

    aget v10, v9, v2

    sub-int/2addr v6, v10

    aget v9, v9, v3

    sub-int/2addr v13, v9

    if-nez v8, :cond_c

    if-eqz v7, :cond_d

    :cond_c
    invoke-virtual {v4, v8, v7}, Landroidx/recyclerview/widget/RecyclerView;->I(II)V

    :cond_d
    iget-object v4, v0, Landroidx/recyclerview/widget/o$a;->s:Landroidx/recyclerview/widget/o;

    invoke-static {v4}, Landroidx/recyclerview/widget/o;->y1(Landroidx/recyclerview/widget/o;)Z

    move-result v4

    if-nez v4, :cond_e

    iget-object v4, v0, Landroidx/recyclerview/widget/o$a;->s:Landroidx/recyclerview/widget/o;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->invalidate()V

    :cond_e
    invoke-virtual {v1}, Loa/c;->h()I

    move-result v4

    invoke-virtual {v1}, Loa/c;->j()I

    move-result v9

    if-ne v4, v9, :cond_f

    move v4, v3

    goto :goto_3

    :cond_f
    move v4, v2

    :goto_3
    invoke-virtual {v1}, Loa/c;->i()I

    move-result v9

    invoke-virtual {v1}, Loa/c;->k()I

    move-result v10

    if-ne v9, v10, :cond_10

    move v9, v3

    goto :goto_4

    :cond_10
    move v9, v2

    :goto_4
    invoke-virtual {v1}, Loa/c;->o()Z

    move-result v10

    if-nez v10, :cond_13

    if-nez v4, :cond_11

    if-eqz v6, :cond_12

    :cond_11
    if-nez v9, :cond_13

    if-eqz v13, :cond_12

    goto :goto_5

    :cond_12
    move v4, v2

    goto :goto_6

    :cond_13
    :goto_5
    move v4, v3

    :goto_6
    iget-object v9, v0, Landroidx/recyclerview/widget/o$a;->s:Landroidx/recyclerview/widget/o;

    iget-object v9, v9, Landroidx/recyclerview/widget/RecyclerView;->m:Landroidx/recyclerview/widget/RecyclerView$o;

    iget-object v9, v9, Landroidx/recyclerview/widget/RecyclerView$o;->g:Landroidx/recyclerview/widget/RecyclerView$z;

    if-eqz v9, :cond_14

    invoke-virtual {v9}, Landroidx/recyclerview/widget/RecyclerView$z;->g()Z

    move-result v9

    if-eqz v9, :cond_14

    move v9, v3

    goto :goto_7

    :cond_14
    move v9, v2

    :goto_7
    if-nez v9, :cond_20

    if-eqz v4, :cond_20

    iget-object v4, v0, Landroidx/recyclerview/widget/o$a;->s:Landroidx/recyclerview/widget/o;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getOverScrollMode()I

    move-result v4

    if-eq v4, v5, :cond_19

    invoke-virtual {v1}, Loa/c;->e()F

    move-result v1

    float-to-int v1, v1

    if-gez v6, :cond_15

    neg-int v4, v1

    goto :goto_8

    :cond_15
    if-lez v6, :cond_16

    move v4, v1

    goto :goto_8

    :cond_16
    move v4, v2

    :goto_8
    if-gez v13, :cond_17

    neg-int v1, v1

    goto :goto_9

    :cond_17
    if-lez v13, :cond_18

    goto :goto_9

    :cond_18
    move v1, v2

    :goto_9
    iget-object v5, v0, Landroidx/recyclerview/widget/o$a;->s:Landroidx/recyclerview/widget/o;

    invoke-virtual {v5, v4, v1}, Landroidx/recyclerview/widget/RecyclerView;->a(II)V

    :cond_19
    iget-object v1, v0, Landroidx/recyclerview/widget/o$a;->s:Landroidx/recyclerview/widget/o;

    iget-object v1, v1, Landroidx/recyclerview/widget/RecyclerView;->m:Landroidx/recyclerview/widget/RecyclerView$o;

    invoke-virtual {v1}, Landroidx/recyclerview/widget/RecyclerView$o;->l()Z

    move-result v1

    const/4 v4, -0x1

    if-eqz v1, :cond_1b

    iget-object v1, v0, Landroidx/recyclerview/widget/o$a;->j:Loa/c;

    invoke-virtual {v1}, Loa/c;->k()I

    move-result v1

    iget-object v5, v0, Landroidx/recyclerview/widget/o$a;->j:Loa/c;

    invoke-virtual {v5}, Loa/c;->n()I

    move-result v5

    if-le v1, v5, :cond_1a

    move v4, v3

    :cond_1a
    iget-object v1, v0, Landroidx/recyclerview/widget/o$a;->s:Landroidx/recyclerview/widget/o;

    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->canScrollVertically(I)Z

    move-result v1

    :goto_a
    xor-int/2addr v1, v3

    goto :goto_b

    :cond_1b
    iget-object v1, v0, Landroidx/recyclerview/widget/o$a;->s:Landroidx/recyclerview/widget/o;

    iget-object v1, v1, Landroidx/recyclerview/widget/RecyclerView;->m:Landroidx/recyclerview/widget/RecyclerView$o;

    invoke-virtual {v1}, Landroidx/recyclerview/widget/RecyclerView$o;->k()Z

    move-result v1

    if-eqz v1, :cond_1d

    iget-object v1, v0, Landroidx/recyclerview/widget/o$a;->j:Loa/c;

    invoke-virtual {v1}, Loa/c;->j()I

    move-result v1

    iget-object v5, v0, Landroidx/recyclerview/widget/o$a;->j:Loa/c;

    invoke-virtual {v5}, Loa/c;->m()I

    move-result v5

    if-le v1, v5, :cond_1c

    move v4, v3

    :cond_1c
    iget-object v1, v0, Landroidx/recyclerview/widget/o$a;->s:Landroidx/recyclerview/widget/o;

    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->canScrollHorizontally(I)Z

    move-result v1

    goto :goto_a

    :cond_1d
    move v1, v2

    :goto_b
    iget-object v4, v0, Landroidx/recyclerview/widget/o$a;->s:Landroidx/recyclerview/widget/o;

    invoke-static {v4}, Landroidx/recyclerview/widget/o;->z1(Landroidx/recyclerview/widget/o;)Z

    move-result v4

    if-nez v4, :cond_1f

    iget-object v4, v0, Landroidx/recyclerview/widget/o$a;->j:Loa/c;

    invoke-virtual {v4}, Loa/c;->l()I

    move-result v4

    if-ne v4, v3, :cond_1f

    iget-boolean v4, v0, Landroidx/recyclerview/widget/o$a;->n:Z

    if-nez v4, :cond_1f

    if-eqz v1, :cond_1f

    const-string v1, "2.0"

    invoke-static {v1}, Lmiuix/view/HapticCompat;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1e

    invoke-direct/range {p0 .. p0}, Landroidx/recyclerview/widget/o$a;->h()Llb/a;

    move-result-object v1

    const/16 v4, 0xc9

    invoke-virtual {v1, v4}, Llb/a;->a(I)Z

    goto :goto_c

    :cond_1e
    iget-object v1, v0, Landroidx/recyclerview/widget/o$a;->s:Landroidx/recyclerview/widget/o;

    sget v4, Lmiuix/view/c;->q:I

    invoke-static {v1, v4}, Lmiuix/view/HapticCompat;->performHapticFeedbackAsync(Landroid/view/View;I)V

    :cond_1f
    :goto_c
    sget-boolean v1, Landroidx/recyclerview/widget/RecyclerView;->G0:Z

    if-eqz v1, :cond_21

    iget-object v1, v0, Landroidx/recyclerview/widget/o$a;->s:Landroidx/recyclerview/widget/o;

    iget-object v1, v1, Landroidx/recyclerview/widget/RecyclerView;->k0:Landroidx/recyclerview/widget/g$b;

    invoke-virtual {v1}, Landroidx/recyclerview/widget/g$b;->b()V

    goto :goto_d

    :cond_20
    invoke-virtual/range {p0 .. p0}, Landroidx/recyclerview/widget/o$a;->e()V

    iget-object v1, v0, Landroidx/recyclerview/widget/o$a;->s:Landroidx/recyclerview/widget/o;

    iget-object v4, v1, Landroidx/recyclerview/widget/RecyclerView;->j0:Landroidx/recyclerview/widget/g;

    if-eqz v4, :cond_21

    invoke-virtual {v4, v1, v8, v7}, Landroidx/recyclerview/widget/g;->f(Landroidx/recyclerview/widget/RecyclerView;II)V

    :cond_21
    :goto_d
    iget-object v1, v0, Landroidx/recyclerview/widget/o$a;->s:Landroidx/recyclerview/widget/o;

    iget-object v1, v1, Landroidx/recyclerview/widget/RecyclerView;->m:Landroidx/recyclerview/widget/RecyclerView$o;

    iget-object v1, v1, Landroidx/recyclerview/widget/RecyclerView$o;->g:Landroidx/recyclerview/widget/RecyclerView$z;

    if-eqz v1, :cond_22

    invoke-virtual {v1}, Landroidx/recyclerview/widget/RecyclerView$z;->g()Z

    move-result v4

    if-eqz v4, :cond_22

    invoke-virtual {v1, v2, v2}, Landroidx/recyclerview/widget/RecyclerView$z;->j(II)V

    :cond_22
    iput-boolean v2, v0, Landroidx/recyclerview/widget/o$a;->l:Z

    iget-boolean v1, v0, Landroidx/recyclerview/widget/o$a;->m:Z

    if-eqz v1, :cond_23

    invoke-direct/range {p0 .. p0}, Landroidx/recyclerview/widget/o$a;->d()V

    goto :goto_e

    :cond_23
    iget-object v1, v0, Landroidx/recyclerview/widget/o$a;->s:Landroidx/recyclerview/widget/o;

    invoke-virtual {v1, v2}, Landroidx/recyclerview/widget/RecyclerView;->setScrollState(I)V

    iget-object v1, v0, Landroidx/recyclerview/widget/o$a;->s:Landroidx/recyclerview/widget/o;

    invoke-virtual {v1, v3}, Landroidx/recyclerview/widget/RecyclerView;->u1(I)V

    iput v2, v0, Landroidx/recyclerview/widget/o$a;->p:I

    iput v2, v0, Landroidx/recyclerview/widget/o$a;->o:I

    :goto_e
    return-void
.end method
