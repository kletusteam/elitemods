.class Landroidx/core/view/d0$j;
.super Landroidx/core/view/d0$i;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroidx/core/view/d0;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "j"
.end annotation


# instance fields
.field private n:Lx/b;

.field private o:Lx/b;

.field private p:Lx/b;


# direct methods
.method constructor <init>(Landroidx/core/view/d0;Landroid/view/WindowInsets;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroidx/core/view/d0$i;-><init>(Landroidx/core/view/d0;Landroid/view/WindowInsets;)V

    const/4 p1, 0x0

    iput-object p1, p0, Landroidx/core/view/d0$j;->n:Lx/b;

    iput-object p1, p0, Landroidx/core/view/d0$j;->o:Lx/b;

    iput-object p1, p0, Landroidx/core/view/d0$j;->p:Lx/b;

    return-void
.end method

.method constructor <init>(Landroidx/core/view/d0;Landroidx/core/view/d0$j;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroidx/core/view/d0$i;-><init>(Landroidx/core/view/d0;Landroidx/core/view/d0$i;)V

    const/4 p1, 0x0

    iput-object p1, p0, Landroidx/core/view/d0$j;->n:Lx/b;

    iput-object p1, p0, Landroidx/core/view/d0$j;->o:Lx/b;

    iput-object p1, p0, Landroidx/core/view/d0$j;->p:Lx/b;

    return-void
.end method


# virtual methods
.method h()Lx/b;
    .locals 1

    iget-object v0, p0, Landroidx/core/view/d0$j;->o:Lx/b;

    if-nez v0, :cond_0

    iget-object v0, p0, Landroidx/core/view/d0$g;->c:Landroid/view/WindowInsets;

    invoke-virtual {v0}, Landroid/view/WindowInsets;->getMandatorySystemGestureInsets()Landroid/graphics/Insets;

    move-result-object v0

    invoke-static {v0}, Lx/b;->d(Landroid/graphics/Insets;)Lx/b;

    move-result-object v0

    iput-object v0, p0, Landroidx/core/view/d0$j;->o:Lx/b;

    :cond_0
    iget-object v0, p0, Landroidx/core/view/d0$j;->o:Lx/b;

    return-object v0
.end method

.method j()Lx/b;
    .locals 1

    iget-object v0, p0, Landroidx/core/view/d0$j;->n:Lx/b;

    if-nez v0, :cond_0

    iget-object v0, p0, Landroidx/core/view/d0$g;->c:Landroid/view/WindowInsets;

    invoke-virtual {v0}, Landroid/view/WindowInsets;->getSystemGestureInsets()Landroid/graphics/Insets;

    move-result-object v0

    invoke-static {v0}, Lx/b;->d(Landroid/graphics/Insets;)Lx/b;

    move-result-object v0

    iput-object v0, p0, Landroidx/core/view/d0$j;->n:Lx/b;

    :cond_0
    iget-object v0, p0, Landroidx/core/view/d0$j;->n:Lx/b;

    return-object v0
.end method

.method l()Lx/b;
    .locals 1

    iget-object v0, p0, Landroidx/core/view/d0$j;->p:Lx/b;

    if-nez v0, :cond_0

    iget-object v0, p0, Landroidx/core/view/d0$g;->c:Landroid/view/WindowInsets;

    invoke-virtual {v0}, Landroid/view/WindowInsets;->getTappableElementInsets()Landroid/graphics/Insets;

    move-result-object v0

    invoke-static {v0}, Lx/b;->d(Landroid/graphics/Insets;)Lx/b;

    move-result-object v0

    iput-object v0, p0, Landroidx/core/view/d0$j;->p:Lx/b;

    :cond_0
    iget-object v0, p0, Landroidx/core/view/d0$j;->p:Lx/b;

    return-object v0
.end method

.method m(IIII)Landroidx/core/view/d0;
    .locals 1

    iget-object v0, p0, Landroidx/core/view/d0$g;->c:Landroid/view/WindowInsets;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/view/WindowInsets;->inset(IIII)Landroid/view/WindowInsets;

    move-result-object p1

    invoke-static {p1}, Landroidx/core/view/d0;->u(Landroid/view/WindowInsets;)Landroidx/core/view/d0;

    move-result-object p1

    return-object p1
.end method

.method public s(Lx/b;)V
    .locals 0

    return-void
.end method
