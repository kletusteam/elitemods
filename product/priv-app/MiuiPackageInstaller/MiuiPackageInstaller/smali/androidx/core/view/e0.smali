.class public final Landroidx/core/view/e0;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroidx/core/view/e0$d;,
        Landroidx/core/view/e0$c;,
        Landroidx/core/view/e0$b;,
        Landroidx/core/view/e0$a;,
        Landroidx/core/view/e0$e;
    }
.end annotation


# instance fields
.field private final a:Landroidx/core/view/e0$e;


# direct methods
.method public constructor <init>(Landroid/view/Window;Landroid/view/View;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1e

    if-lt v0, v1, :cond_0

    new-instance p2, Landroidx/core/view/e0$d;

    invoke-direct {p2, p1, p0}, Landroidx/core/view/e0$d;-><init>(Landroid/view/Window;Landroidx/core/view/e0;)V

    iput-object p2, p0, Landroidx/core/view/e0;->a:Landroidx/core/view/e0$e;

    goto :goto_0

    :cond_0
    new-instance v0, Landroidx/core/view/e0$c;

    invoke-direct {v0, p1, p2}, Landroidx/core/view/e0$c;-><init>(Landroid/view/Window;Landroid/view/View;)V

    iput-object v0, p0, Landroidx/core/view/e0;->a:Landroidx/core/view/e0$e;

    :goto_0
    return-void
.end method

.method private constructor <init>(Landroid/view/WindowInsetsController;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1e

    if-lt v0, v1, :cond_0

    new-instance v0, Landroidx/core/view/e0$d;

    invoke-direct {v0, p1, p0}, Landroidx/core/view/e0$d;-><init>(Landroid/view/WindowInsetsController;Landroidx/core/view/e0;)V

    iput-object v0, p0, Landroidx/core/view/e0;->a:Landroidx/core/view/e0$e;

    goto :goto_0

    :cond_0
    new-instance p1, Landroidx/core/view/e0$e;

    invoke-direct {p1}, Landroidx/core/view/e0$e;-><init>()V

    iput-object p1, p0, Landroidx/core/view/e0;->a:Landroidx/core/view/e0$e;

    :goto_0
    return-void
.end method

.method public static c(Landroid/view/WindowInsetsController;)Landroidx/core/view/e0;
    .locals 1

    new-instance v0, Landroidx/core/view/e0;

    invoke-direct {v0, p0}, Landroidx/core/view/e0;-><init>(Landroid/view/WindowInsetsController;)V

    return-object v0
.end method


# virtual methods
.method public a(Z)V
    .locals 1

    iget-object v0, p0, Landroidx/core/view/e0;->a:Landroidx/core/view/e0$e;

    invoke-virtual {v0, p1}, Landroidx/core/view/e0$e;->a(Z)V

    return-void
.end method

.method public b(Z)V
    .locals 1

    iget-object v0, p0, Landroidx/core/view/e0;->a:Landroidx/core/view/e0$e;

    invoke-virtual {v0, p1}, Landroidx/core/view/e0$e;->b(Z)V

    return-void
.end method
