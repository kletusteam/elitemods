.class public final Lo5/b;
.super Ljava/lang/Object;

# interfaces
.implements Lo5/a;
.implements Ljava/io/Serializable;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/String;

.field private q:Z

.field private r:Ljava/lang/String;

.field private s:Ljava/lang/String;

.field private t:Ljava/lang/String;

.field private u:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    const-string v0, "ref"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lo5/b;->d:Ljava/lang/String;

    const-string v1, "null"

    iput-object v1, p0, Lo5/b;->e:Ljava/lang/String;

    iput-object v0, p0, Lo5/b;->f:Ljava/lang/String;

    iput-object v1, p0, Lo5/b;->g:Ljava/lang/String;

    iput-object v0, p0, Lo5/b;->h:Ljava/lang/String;

    iput-object v1, p0, Lo5/b;->i:Ljava/lang/String;

    iput-object v1, p0, Lo5/b;->j:Ljava/lang/String;

    iput-object v1, p0, Lo5/b;->k:Ljava/lang/String;

    iput-object v0, p0, Lo5/b;->l:Ljava/lang/String;

    iput-object v0, p0, Lo5/b;->m:Ljava/lang/String;

    iput-object v0, p0, Lo5/b;->n:Ljava/lang/String;

    iput-object v0, p0, Lo5/b;->o:Ljava/lang/String;

    iput-object v0, p0, Lo5/b;->p:Ljava/lang/String;

    iput-object v0, p0, Lo5/b;->r:Ljava/lang/String;

    iput-object v0, p0, Lo5/b;->s:Ljava/lang/String;

    iput-object v0, p0, Lo5/b;->t:Ljava/lang/String;

    iput-object v0, p0, Lo5/b;->u:Ljava/lang/String;

    iput-object p1, p0, Lo5/b;->a:Ljava/lang/String;

    iput-object p1, p0, Lo5/b;->b:Ljava/lang/String;

    iput-object v0, p0, Lo5/b;->c:Ljava/lang/String;

    if-eqz p2, :cond_0

    iput-object p2, p0, Lo5/b;->m:Ljava/lang/String;

    const-string p1, "installProcess"

    invoke-static {p1, p2}, Lf6/o;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    :cond_0
    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;ILm8/g;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    :cond_0
    invoke-direct {p0, p1, p2}, Lo5/b;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lo5/a;Ljava/lang/String;)V
    .locals 2

    const-string v0, "ref"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "from"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lo5/b;->b:Ljava/lang/String;

    iput-object v0, p0, Lo5/b;->d:Ljava/lang/String;

    const-string v1, "null"

    iput-object v1, p0, Lo5/b;->e:Ljava/lang/String;

    iput-object v0, p0, Lo5/b;->f:Ljava/lang/String;

    iput-object v1, p0, Lo5/b;->g:Ljava/lang/String;

    iput-object v0, p0, Lo5/b;->h:Ljava/lang/String;

    iput-object v1, p0, Lo5/b;->i:Ljava/lang/String;

    iput-object v1, p0, Lo5/b;->j:Ljava/lang/String;

    iput-object v1, p0, Lo5/b;->k:Ljava/lang/String;

    iput-object v0, p0, Lo5/b;->l:Ljava/lang/String;

    iput-object v0, p0, Lo5/b;->m:Ljava/lang/String;

    iput-object v0, p0, Lo5/b;->n:Ljava/lang/String;

    iput-object v0, p0, Lo5/b;->o:Ljava/lang/String;

    iput-object v0, p0, Lo5/b;->p:Ljava/lang/String;

    iput-object v0, p0, Lo5/b;->r:Ljava/lang/String;

    iput-object v0, p0, Lo5/b;->s:Ljava/lang/String;

    iput-object v0, p0, Lo5/b;->t:Ljava/lang/String;

    iput-object v0, p0, Lo5/b;->u:Ljava/lang/String;

    iput-object p1, p0, Lo5/b;->a:Ljava/lang/String;

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p2}, Lo5/a;->n()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v0, 0x2d

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lo5/b;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lo5/b;->b:Ljava/lang/String;

    invoke-interface {p2}, Lo5/a;->getRef()Ljava/lang/String;

    move-result-object p1

    const-string v0, "from.ref"

    invoke-static {p1, v0}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lo5/b;->c:Ljava/lang/String;

    invoke-interface {p2}, Lo5/a;->z()Ljava/lang/String;

    move-result-object p1

    const-string v0, "from.launchRef"

    invoke-static {p1, v0}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lo5/b;->d:Ljava/lang/String;

    invoke-interface {p2}, Lo5/a;->x()Ljava/lang/String;

    move-result-object p1

    const-string v0, "from.appPackageName"

    invoke-static {p1, v0}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lo5/b;->t:Ljava/lang/String;

    invoke-interface {p2}, Lo5/a;->F()Ljava/lang/String;

    move-result-object p1

    const-string v0, "from.expId"

    invoke-static {p1, v0}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lo5/b;->e:Ljava/lang/String;

    invoke-interface {p2}, Lo5/a;->H()Ljava/lang/String;

    move-result-object p1

    const-string v0, "from.installType"

    invoke-static {p1, v0}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lo5/b;->l:Ljava/lang/String;

    invoke-interface {p2}, Lo5/a;->b()Ljava/lang/String;

    move-result-object p1

    const-string v0, "from.appName"

    invoke-static {p1, v0}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lo5/b;->p:Ljava/lang/String;

    invoke-interface {p2}, Lo5/a;->I()Ljava/lang/String;

    move-result-object p1

    const-string v0, "from.sourceLabel"

    invoke-static {p1, v0}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lo5/b;->f:Ljava/lang/String;

    invoke-interface {p2}, Lo5/a;->E()Ljava/lang/String;

    move-result-object p1

    const-string v0, "from.appType"

    invoke-static {p1, v0}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lo5/b;->n:Ljava/lang/String;

    invoke-interface {p2}, Lo5/a;->Q()Ljava/lang/String;

    move-result-object p1

    const-string v0, "from.appId"

    invoke-static {p1, v0}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lo5/b;->o:Ljava/lang/String;

    invoke-interface {p2}, Lo5/a;->i()Ljava/lang/String;

    move-result-object p1

    const-string v0, "from.appRiskType"

    invoke-static {p1, v0}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lo5/b;->r:Ljava/lang/String;

    invoke-interface {p2}, Lo5/a;->A()Ljava/lang/String;

    move-result-object p1

    const-string v0, "from.appLocalScene"

    invoke-static {p1, v0}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lo5/b;->s:Ljava/lang/String;

    invoke-interface {p2}, Lo5/a;->getVersionName()Ljava/lang/String;

    move-result-object p1

    const-string v0, "from.versionName"

    invoke-static {p1, v0}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lo5/b;->u:Ljava/lang/String;

    invoke-interface {p2}, Lo5/a;->t()Ljava/lang/Boolean;

    move-result-object p1

    const-string v0, "from.isBundleApp"

    invoke-static {p1, v0}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    iput-boolean p1, p0, Lo5/b;->q:Z

    if-eqz p3, :cond_1

    invoke-interface {p3}, Ljava/lang/CharSequence;->length()I

    move-result p1

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    if-eqz p1, :cond_2

    invoke-interface {p2}, Lo5/a;->v()Ljava/lang/String;

    move-result-object p3

    const-string p1, "from.installProcess"

    invoke-static {p3, p1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_2
    iput-object p3, p0, Lo5/b;->m:Ljava/lang/String;

    const-string p1, "installProcess"

    invoke-static {p1, p3}, Lf6/o;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    return-void
.end method


# virtual methods
.method public A()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lo5/b;->s:Ljava/lang/String;

    return-object v0
.end method

.method public B(Ljava/lang/String;)V
    .locals 2

    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_2

    return-void

    :cond_2
    iget-object v0, p0, Lo5/b;->a:Ljava/lang/String;

    iput-object v0, p0, Lo5/b;->c:Ljava/lang/String;

    iput-object p1, p0, Lo5/b;->a:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lo5/b;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x2d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lo5/b;->b:Ljava/lang/String;

    return-void
.end method

.method public E()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lo5/b;->n:Ljava/lang/String;

    return-object v0
.end method

.method public F()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lo5/b;->e:Ljava/lang/String;

    return-object v0
.end method

.method public H()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lo5/b;->l:Ljava/lang/String;

    return-object v0
.end method

.method public I()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lo5/b;->f:Ljava/lang/String;

    return-object v0
.end method

.method public Q()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lo5/b;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    const-string v0, "expId"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lo5/b;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lo5/b;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x2c

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lo5/b;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_0
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lo5/b;->e:Ljava/lang/String;

    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lo5/b;->p:Ljava/lang/String;

    return-object v0
.end method

.method public final c()V
    .locals 1

    iget-object v0, p0, Lo5/b;->i:Ljava/lang/String;

    iput-object v0, p0, Lo5/b;->a:Ljava/lang/String;

    iget-object v0, p0, Lo5/b;->k:Ljava/lang/String;

    iput-object v0, p0, Lo5/b;->c:Ljava/lang/String;

    iget-object v0, p0, Lo5/b;->j:Ljava/lang/String;

    iput-object v0, p0, Lo5/b;->b:Ljava/lang/String;

    return-void
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Lo5/b;->a:Ljava/lang/String;

    iput-object v0, p0, Lo5/b;->i:Ljava/lang/String;

    iget-object v0, p0, Lo5/b;->c:Ljava/lang/String;

    iput-object v0, p0, Lo5/b;->k:Ljava/lang/String;

    iget-object v0, p0, Lo5/b;->b:Ljava/lang/String;

    iput-object v0, p0, Lo5/b;->j:Ljava/lang/String;

    return-void
.end method

.method public final e(Ljava/lang/String;)V
    .locals 1

    const-string v0, "appId"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lo5/b;->o:Ljava/lang/String;

    return-void
.end method

.method public final f(Ljava/lang/String;)V
    .locals 1

    const-string v0, "appLocalScene"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lo5/b;->s:Ljava/lang/String;

    return-void
.end method

.method public final g(Ljava/lang/String;)V
    .locals 1

    const-string v0, "appName"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lo5/b;->p:Ljava/lang/String;

    return-void
.end method

.method public getRef()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lo5/b;->a:Ljava/lang/String;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    const-string v0, ""

    :cond_1
    return-object v0
.end method

.method public getVersionName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lo5/b;->u:Ljava/lang/String;

    return-object v0
.end method

.method public final h(Ljava/lang/String;)V
    .locals 1

    const-string v0, "packageName"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lo5/b;->t:Ljava/lang/String;

    return-void
.end method

.method public i()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lo5/b;->r:Ljava/lang/String;

    return-object v0
.end method

.method public final j(Ljava/lang/String;)V
    .locals 1

    const-string v0, "appRiskType"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lo5/b;->r:Ljava/lang/String;

    return-void
.end method

.method public k()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lo5/b;->c:Ljava/lang/String;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    const-string v0, ""

    :cond_1
    return-object v0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lo5/b;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final m(Ljava/lang/String;)V
    .locals 1

    const-string v0, "appType"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lo5/b;->n:Ljava/lang/String;

    return-void
.end method

.method public n()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lo5/b;->b:Ljava/lang/String;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    const-string v0, ""

    :cond_1
    return-object v0
.end method

.method public final o(Z)V
    .locals 0

    iput-boolean p1, p0, Lo5/b;->q:Z

    return-void
.end method

.method public final p(Ljava/lang/String;)V
    .locals 1

    const-string v0, "installType"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lo5/b;->l:Ljava/lang/String;

    return-void
.end method

.method public final q(Ljava/lang/String;)V
    .locals 1

    const-string v0, "ref"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lo5/b;->d:Ljava/lang/String;

    return-void
.end method

.method public final r(Ljava/lang/String;)V
    .locals 1

    const-string v0, "miPackageName"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lo5/b;->h:Ljava/lang/String;

    return-void
.end method

.method public final s(Ljava/lang/String;)V
    .locals 1

    const-string v0, "fromRef"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lo5/b;->c:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 p1, 0x2d

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lo5/b;->getRef()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lo5/b;->b:Ljava/lang/String;

    return-void
.end method

.method public t()Ljava/lang/Boolean;
    .locals 1

    iget-boolean v0, p0, Lo5/b;->q:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final u(Ljava/lang/String;)V
    .locals 1

    const-string v0, "sourceLabel"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lo5/b;->f:Ljava/lang/String;

    return-void
.end method

.method public v()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lo5/b;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final w(Ljava/lang/String;)V
    .locals 1

    const-string v0, "versionName"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lo5/b;->u:Ljava/lang/String;

    return-void
.end method

.method public x()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lo5/b;->t:Ljava/lang/String;

    return-object v0
.end method

.method public y(Ljava/lang/String;)V
    .locals 1

    if-eqz p1, :cond_0

    const-string v0, "installProcess"

    invoke-static {v0, p1}, Lf6/o;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    iput-object p1, p0, Lo5/b;->m:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public z()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lo5/b;->d:Ljava/lang/String;

    return-object v0
.end method
