.class public Lc9/a;
.super Ljava/lang/Object;


# static fields
.field public static final j:Lj9/c$a;


# instance fields
.field public a:J

.field public b:J
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public c:F

.field public d:Lj9/c$a;

.field private final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lc9/c;",
            ">;"
        }
    .end annotation
.end field

.field public f:I

.field public g:Ljava/lang/Object;

.field public h:J

.field public final i:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Lf9/b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    const/4 v1, -0x2

    invoke-static {v1, v0}, Lj9/c;->e(I[F)Lj9/c$a;

    move-result-object v0

    sput-object v0, Lc9/a;->j:Lj9/c$a;

    return-void

    :array_0
    .array-data 4
        0x3f59999a    # 0.85f
        0x3e99999a    # 0.3f
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lc9/a;-><init>(Z)V

    return-void
.end method

.method public constructor <init>(Lc9/a;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lc9/a;-><init>(Z)V

    invoke-virtual {p0, p1}, Lc9/a;->d(Lc9/a;)V

    return-void
.end method

.method public constructor <init>(Z)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const v0, 0x7f7fffff    # Float.MAX_VALUE

    iput v0, p0, Lc9/a;->c:F

    const/4 v0, -0x1

    iput v0, p0, Lc9/a;->f:I

    if-nez p1, :cond_0

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lc9/a;->e:Ljava/util/Map;

    new-instance p1, Ljava/util/HashSet;

    invoke-direct {p1}, Ljava/util/HashSet;-><init>()V

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    iput-object p1, p0, Lc9/a;->e:Ljava/util/Map;

    :goto_0
    iput-object p1, p0, Lc9/a;->i:Ljava/util/HashSet;

    return-void
.end method

.method private f(Lh9/b;Z)Lc9/c;
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    invoke-virtual {p1}, Landroid/util/Property;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1, p2}, Lc9/a;->h(Ljava/lang/String;Z)Lc9/c;

    move-result-object p1

    return-object p1
.end method

.method private h(Ljava/lang/String;Z)Lc9/c;
    .locals 1

    iget-object v0, p0, Lc9/a;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lc9/c;

    if-nez v0, :cond_0

    if-eqz p2, :cond_0

    new-instance v0, Lc9/c;

    invoke-direct {v0}, Lc9/c;-><init>()V

    iget-object p2, p0, Lc9/a;->e:Ljava/util/Map;

    invoke-interface {p2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object v0
.end method


# virtual methods
.method public varargs a([Lf9/b;)Lc9/a;
    .locals 1

    iget-object v0, p0, Lc9/a;->i:Ljava/util/HashSet;

    invoke-static {v0, p1}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    return-object p0
.end method

.method public b(Lc9/a;)V
    .locals 1

    iget-object v0, p0, Lc9/a;->e:Ljava/util/Map;

    iget-object p1, p1, Lc9/a;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    return-void
.end method

.method public c()V
    .locals 4

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lc9/a;->a:J

    const/4 v2, 0x0

    iput-object v2, p0, Lc9/a;->d:Lj9/c$a;

    iget-object v3, p0, Lc9/a;->i:Ljava/util/HashSet;

    invoke-virtual {v3}, Ljava/util/HashSet;->clear()V

    iput-object v2, p0, Lc9/a;->g:Ljava/lang/Object;

    iput-wide v0, p0, Lc9/a;->h:J

    const v2, 0x7f7fffff    # Float.MAX_VALUE

    iput v2, p0, Lc9/a;->c:F

    iput-wide v0, p0, Lc9/a;->b:J

    const/4 v0, -0x1

    iput v0, p0, Lc9/a;->f:I

    iget-object v0, p0, Lc9/a;->e:Ljava/util/Map;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    :cond_0
    return-void
.end method

.method public d(Lc9/a;)V
    .locals 2

    if-eqz p1, :cond_0

    if-eq p1, p0, :cond_0

    iget-wide v0, p1, Lc9/a;->a:J

    iput-wide v0, p0, Lc9/a;->a:J

    iget-object v0, p1, Lc9/a;->d:Lj9/c$a;

    iput-object v0, p0, Lc9/a;->d:Lj9/c$a;

    iget-object v0, p0, Lc9/a;->i:Ljava/util/HashSet;

    iget-object v1, p1, Lc9/a;->i:Ljava/util/HashSet;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    iget-object v0, p1, Lc9/a;->g:Ljava/lang/Object;

    iput-object v0, p0, Lc9/a;->g:Ljava/lang/Object;

    iget-wide v0, p1, Lc9/a;->h:J

    iput-wide v0, p0, Lc9/a;->h:J

    iget v0, p1, Lc9/a;->c:F

    iput v0, p0, Lc9/a;->c:F

    iget-wide v0, p1, Lc9/a;->b:J

    iput-wide v0, p0, Lc9/a;->b:J

    iget v0, p1, Lc9/a;->f:I

    iput v0, p0, Lc9/a;->f:I

    iget-object v0, p0, Lc9/a;->e:Ljava/util/Map;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iget-object v0, p0, Lc9/a;->e:Ljava/util/Map;

    iget-object p1, p1, Lc9/a;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    :cond_0
    return-void
.end method

.method public e(Ljava/lang/String;)Lc9/c;
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lc9/a;->h(Ljava/lang/String;Z)Lc9/c;

    move-result-object p1

    return-object p1
.end method

.method public g(Ljava/lang/String;)Lc9/c;
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lc9/a;->h(Ljava/lang/String;Z)Lc9/c;

    move-result-object p1

    return-object p1
.end method

.method public varargs i([Lf9/b;)Lc9/a;
    .locals 1

    array-length v0, p1

    if-nez v0, :cond_0

    iget-object p1, p0, Lc9/a;->i:Ljava/util/HashSet;

    invoke-virtual {p1}, Ljava/util/HashSet;->clear()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lc9/a;->i:Ljava/util/HashSet;

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->removeAll(Ljava/util/Collection;)Z

    :goto_0
    return-object p0
.end method

.method public j(J)Lc9/a;
    .locals 0

    iput-wide p1, p0, Lc9/a;->a:J

    return-object p0
.end method

.method public varargs k(I[F)Lc9/a;
    .locals 0

    invoke-static {p1, p2}, Lj9/c;->e(I[F)Lj9/c$a;

    move-result-object p1

    iput-object p1, p0, Lc9/a;->d:Lj9/c$a;

    return-object p0
.end method

.method public l(Lj9/c$a;)Lc9/a;
    .locals 0

    iput-object p1, p0, Lc9/a;->d:Lj9/c$a;

    return-object p0
.end method

.method public m(F)Lc9/a;
    .locals 0

    iput p1, p0, Lc9/a;->c:F

    return-object p0
.end method

.method public varargs n(Lh9/b;J[F)Lc9/a;
    .locals 6

    const/4 v2, 0x0

    move-object v0, p0

    move-object v1, p1

    move-wide v3, p2

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lc9/a;->p(Lh9/b;Lj9/c$a;J[F)Lc9/a;

    move-result-object p1

    return-object p1
.end method

.method public o(Lh9/b;Lc9/c;)Lc9/a;
    .locals 1

    if-eqz p2, :cond_0

    iget-object v0, p0, Lc9/a;->e:Ljava/util/Map;

    invoke-virtual {p1}, Landroid/util/Property;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    iget-object p2, p0, Lc9/a;->e:Ljava/util/Map;

    invoke-virtual {p1}, Landroid/util/Property;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-interface {p2, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    return-object p0
.end method

.method public varargs p(Lh9/b;Lj9/c$a;J[F)Lc9/a;
    .locals 7

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lc9/a;->f(Lh9/b;Z)Lc9/c;

    move-result-object v2

    move-object v1, p0

    move-object v3, p2

    move-wide v4, p3

    move-object v6, p5

    invoke-virtual/range {v1 .. v6}, Lc9/a;->q(Lc9/c;Lj9/c$a;J[F)V

    return-object p0
.end method

.method varargs q(Lc9/c;Lj9/c$a;J[F)V
    .locals 2

    goto/32 :goto_e

    nop

    :goto_0
    return-void

    :goto_1
    invoke-virtual {p1, p2}, Lc9/a;->m(F)Lc9/a;

    :goto_2
    goto/32 :goto_0

    nop

    :goto_3
    invoke-virtual {p1, p2}, Lc9/a;->l(Lj9/c$a;)Lc9/a;

    :goto_4
    goto/32 :goto_d

    nop

    :goto_5
    cmp-long p2, p3, v0

    goto/32 :goto_6

    nop

    :goto_6
    if-gtz p2, :cond_0

    goto/32 :goto_8

    :cond_0
    goto/32 :goto_7

    nop

    :goto_7
    invoke-virtual {p1, p3, p4}, Lc9/a;->j(J)Lc9/a;

    :goto_8
    goto/32 :goto_a

    nop

    :goto_9
    const/4 p2, 0x0

    goto/32 :goto_b

    nop

    :goto_a
    array-length p2, p5

    goto/32 :goto_c

    nop

    :goto_b
    aget p2, p5, p2

    goto/32 :goto_1

    nop

    :goto_c
    if-gtz p2, :cond_1

    goto/32 :goto_2

    :cond_1
    goto/32 :goto_9

    nop

    :goto_d
    const-wide/16 v0, 0x0

    goto/32 :goto_5

    nop

    :goto_e
    if-nez p2, :cond_2

    goto/32 :goto_4

    :cond_2
    goto/32 :goto_3

    nop
.end method

.method public r(I)Lc9/a;
    .locals 0

    iput p1, p0, Lc9/a;->f:I

    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AnimConfig{delay="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lc9/a;->a:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", minDuration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lc9/a;->b:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", ease="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lc9/a;->d:Lj9/c$a;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", fromSpeed="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lc9/a;->c:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", tintMode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lc9/a;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", tag="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lc9/a;->g:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", flags="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lc9/a;->h:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", listeners="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lc9/a;->i:Ljava/util/HashSet;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", specialNameMap = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lc9/a;->e:Ljava/util/Map;

    const-string v2, "    "

    invoke-static {v1, v2}, Lj9/a;->l(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
