.class final Lw2/a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lw2/a$d;,
        Lw2/a$c;
    }
.end annotation


# instance fields
.field private final a:Z

.field private final b:Ljava/util/concurrent/Executor;

.field final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lu2/f;",
            "Lw2/a$d;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/lang/ref/ReferenceQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/ReferenceQueue<",
            "Lw2/p<",
            "*>;>;"
        }
    .end annotation
.end field

.field private e:Lw2/p$a;

.field private volatile f:Z

.field private volatile g:Lw2/a$c;


# direct methods
.method constructor <init>(Z)V
    .locals 1

    new-instance v0, Lw2/a$a;

    invoke-direct {v0}, Lw2/a$a;-><init>()V

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lw2/a;-><init>(ZLjava/util/concurrent/Executor;)V

    return-void
.end method

.method constructor <init>(ZLjava/util/concurrent/Executor;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lw2/a;->c:Ljava/util/Map;

    new-instance v0, Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v0}, Ljava/lang/ref/ReferenceQueue;-><init>()V

    iput-object v0, p0, Lw2/a;->d:Ljava/lang/ref/ReferenceQueue;

    iput-boolean p1, p0, Lw2/a;->a:Z

    iput-object p2, p0, Lw2/a;->b:Ljava/util/concurrent/Executor;

    new-instance p1, Lw2/a$b;

    invoke-direct {p1, p0}, Lw2/a$b;-><init>(Lw2/a;)V

    invoke-interface {p2, p1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method


# virtual methods
.method declared-synchronized a(Lu2/f;Lw2/p;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lu2/f;",
            "Lw2/p<",
            "*>;)V"
        }
    .end annotation

    goto/32 :goto_3

    nop

    :goto_0
    return-void

    :catchall_0
    move-exception p1

    goto/32 :goto_2

    nop

    :goto_1
    throw p1

    :goto_2
    monitor-exit p0

    goto/32 :goto_1

    nop

    :goto_3
    monitor-enter p0

    :try_start_0
    new-instance v0, Lw2/a$d;

    iget-object v1, p0, Lw2/a;->d:Ljava/lang/ref/ReferenceQueue;

    iget-boolean v2, p0, Lw2/a;->a:Z

    invoke-direct {v0, p1, p2, v1, v2}, Lw2/a$d;-><init>(Lu2/f;Lw2/p;Ljava/lang/ref/ReferenceQueue;Z)V

    iget-object p2, p0, Lw2/a;->c:Ljava/util/Map;

    invoke-interface {p2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lw2/a$d;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lw2/a$d;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    goto/32 :goto_4

    nop

    :goto_4
    monitor-exit p0

    goto/32 :goto_0

    nop
.end method

.method b()V
    .locals 1

    :cond_0
    :goto_0
    goto/32 :goto_4

    nop

    :goto_1
    goto :goto_0

    :goto_2
    goto/32 :goto_8

    nop

    :goto_3
    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto/32 :goto_1

    nop

    :goto_4
    iget-boolean v0, p0, Lw2/a;->f:Z

    goto/32 :goto_5

    nop

    :goto_5
    if-eqz v0, :cond_1

    goto/32 :goto_2

    :cond_1
    :try_start_0
    iget-object v0, p0, Lw2/a;->d:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/ref/ReferenceQueue;->remove()Ljava/lang/ref/Reference;

    move-result-object v0

    check-cast v0, Lw2/a$d;

    invoke-virtual {p0, v0}, Lw2/a;->c(Lw2/a$d;)V

    iget-object v0, p0, Lw2/a;->g:Lw2/a$c;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lw2/a$c;->a()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_6

    nop

    :goto_6
    goto :goto_0

    :catch_0
    goto/32 :goto_7

    nop

    :goto_7
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    goto/32 :goto_3

    nop

    :goto_8
    return-void
.end method

.method c(Lw2/a$d;)V
    .locals 7

    goto/32 :goto_b

    nop

    :goto_0
    return-void

    :cond_0
    :goto_1
    :try_start_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/32 :goto_3

    nop

    :goto_2
    invoke-direct/range {v1 .. v6}, Lw2/p;-><init>(Lw2/v;ZZLu2/f;Lw2/p$a;)V

    goto/32 :goto_8

    nop

    :goto_3
    throw p1

    :goto_4
    iget-object p1, p1, Lw2/a$d;->a:Lu2/f;

    goto/32 :goto_5

    nop

    :goto_5
    invoke-interface {v1, p1, v0}, Lw2/p$a;->d(Lu2/f;Lw2/p;)V

    goto/32 :goto_0

    nop

    :goto_6
    move-object v1, v0

    goto/32 :goto_2

    nop

    :goto_7
    iget-object v6, p0, Lw2/a;->e:Lw2/p$a;

    goto/32 :goto_6

    nop

    :goto_8
    iget-object v1, p0, Lw2/a;->e:Lw2/p$a;

    goto/32 :goto_4

    nop

    :goto_9
    new-instance v0, Lw2/p;

    goto/32 :goto_c

    nop

    :goto_a
    iget-object v5, p1, Lw2/a$d;->a:Lu2/f;

    goto/32 :goto_7

    nop

    :goto_b
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lw2/a;->c:Ljava/util/Map;

    iget-object v1, p1, Lw2/a$d;->a:Lu2/f;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-boolean v0, p1, Lw2/a$d;->b:Z

    if-eqz v0, :cond_0

    iget-object v2, p1, Lw2/a$d;->c:Lw2/v;

    if-nez v2, :cond_1

    goto :goto_1

    :cond_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/32 :goto_9

    nop

    :goto_c
    const/4 v3, 0x1

    goto/32 :goto_d

    nop

    :goto_d
    const/4 v4, 0x0

    goto/32 :goto_a

    nop
.end method

.method declared-synchronized d(Lu2/f;)V
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    throw p1

    :goto_1
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lw2/a;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lw2/a$d;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lw2/a$d;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    goto/32 :goto_2

    nop

    :goto_2
    monitor-exit p0

    goto/32 :goto_3

    nop

    :goto_3
    return-void

    :catchall_0
    move-exception p1

    goto/32 :goto_4

    nop

    :goto_4
    monitor-exit p0

    goto/32 :goto_0

    nop
.end method

.method declared-synchronized e(Lu2/f;)Lw2/p;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lu2/f;",
            ")",
            "Lw2/p<",
            "*>;"
        }
    .end annotation

    goto/32 :goto_0

    nop

    :goto_0
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lw2/a;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lw2/a$d;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/32 :goto_9

    nop

    :goto_1
    return-object v0

    :catchall_0
    move-exception p1

    goto/32 :goto_8

    nop

    :goto_2
    monitor-exit p0

    goto/32 :goto_6

    nop

    :goto_3
    monitor-exit p0

    goto/32 :goto_1

    nop

    :goto_4
    throw p1

    :goto_5
    const/4 p1, 0x0

    goto/32 :goto_2

    nop

    :goto_6
    return-object p1

    :goto_7
    :try_start_1
    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lw2/p;

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lw2/a;->c(Lw2/a$d;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    goto/32 :goto_3

    nop

    :goto_8
    monitor-exit p0

    goto/32 :goto_4

    nop

    :goto_9
    if-eqz p1, :cond_1

    goto/32 :goto_7

    :cond_1
    goto/32 :goto_5

    nop
.end method

.method f(Lw2/p$a;)V
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    monitor-enter p1

    :try_start_0
    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iput-object p1, p0, Lw2/a;->e:Lw2/p$a;

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto/32 :goto_2

    nop

    :goto_1
    throw v0

    :goto_2
    return-void

    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0

    :catchall_1
    move-exception v0

    monitor-exit p1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto/32 :goto_1

    nop
.end method
