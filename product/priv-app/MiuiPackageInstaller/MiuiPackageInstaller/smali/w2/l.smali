.class Lw2/l;
.super Ljava/lang/Object;

# interfaces
.implements Lw2/h$b;
.implements Lr3/a$f;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lw2/l$c;,
        Lw2/l$d;,
        Lw2/l$e;,
        Lw2/l$b;,
        Lw2/l$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lw2/h$b<",
        "TR;>;",
        "Lr3/a$f;"
    }
.end annotation


# static fields
.field private static final z:Lw2/l$c;


# instance fields
.field final a:Lw2/l$e;

.field private final b:Lr3/c;

.field private final c:Lw2/p$a;

.field private final d:Le0/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Le0/e<",
            "Lw2/l<",
            "*>;>;"
        }
    .end annotation
.end field

.field private final e:Lw2/l$c;

.field private final f:Lw2/m;

.field private final g:Lz2/a;

.field private final h:Lz2/a;

.field private final i:Lz2/a;

.field private final j:Lz2/a;

.field private final k:Ljava/util/concurrent/atomic/AtomicInteger;

.field private l:Lu2/f;

.field private m:Z

.field private n:Z

.field private o:Z

.field private p:Z

.field private q:Lw2/v;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lw2/v<",
            "*>;"
        }
    .end annotation
.end field

.field r:Lu2/a;

.field private s:Z

.field t:Lw2/q;

.field private u:Z

.field v:Lw2/p;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lw2/p<",
            "*>;"
        }
    .end annotation
.end field

.field private w:Lw2/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lw2/h<",
            "TR;>;"
        }
    .end annotation
.end field

.field private volatile x:Z

.field private y:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lw2/l$c;

    invoke-direct {v0}, Lw2/l$c;-><init>()V

    sput-object v0, Lw2/l;->z:Lw2/l$c;

    return-void
.end method

.method constructor <init>(Lz2/a;Lz2/a;Lz2/a;Lz2/a;Lw2/m;Lw2/p$a;Le0/e;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lz2/a;",
            "Lz2/a;",
            "Lz2/a;",
            "Lz2/a;",
            "Lw2/m;",
            "Lw2/p$a;",
            "Le0/e<",
            "Lw2/l<",
            "*>;>;)V"
        }
    .end annotation

    sget-object v8, Lw2/l;->z:Lw2/l$c;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lw2/l;-><init>(Lz2/a;Lz2/a;Lz2/a;Lz2/a;Lw2/m;Lw2/p$a;Le0/e;Lw2/l$c;)V

    return-void
.end method

.method constructor <init>(Lz2/a;Lz2/a;Lz2/a;Lz2/a;Lw2/m;Lw2/p$a;Le0/e;Lw2/l$c;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lz2/a;",
            "Lz2/a;",
            "Lz2/a;",
            "Lz2/a;",
            "Lw2/m;",
            "Lw2/p$a;",
            "Le0/e<",
            "Lw2/l<",
            "*>;>;",
            "Lw2/l$c;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lw2/l$e;

    invoke-direct {v0}, Lw2/l$e;-><init>()V

    iput-object v0, p0, Lw2/l;->a:Lw2/l$e;

    invoke-static {}, Lr3/c;->a()Lr3/c;

    move-result-object v0

    iput-object v0, p0, Lw2/l;->b:Lr3/c;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lw2/l;->k:Ljava/util/concurrent/atomic/AtomicInteger;

    iput-object p1, p0, Lw2/l;->g:Lz2/a;

    iput-object p2, p0, Lw2/l;->h:Lz2/a;

    iput-object p3, p0, Lw2/l;->i:Lz2/a;

    iput-object p4, p0, Lw2/l;->j:Lz2/a;

    iput-object p5, p0, Lw2/l;->f:Lw2/m;

    iput-object p6, p0, Lw2/l;->c:Lw2/p$a;

    iput-object p7, p0, Lw2/l;->d:Le0/e;

    iput-object p8, p0, Lw2/l;->e:Lw2/l$c;

    return-void
.end method

.method private j()Lz2/a;
    .locals 1

    iget-boolean v0, p0, Lw2/l;->n:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lw2/l;->i:Lz2/a;

    goto :goto_0

    :cond_0
    iget-boolean v0, p0, Lw2/l;->o:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lw2/l;->j:Lz2/a;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lw2/l;->h:Lz2/a;

    :goto_0
    return-object v0
.end method

.method private m()Z
    .locals 1

    iget-boolean v0, p0, Lw2/l;->u:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lw2/l;->s:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lw2/l;->x:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method private declared-synchronized q()V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lw2/l;->l:Lu2/f;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lw2/l;->a:Lw2/l$e;

    invoke-virtual {v0}, Lw2/l$e;->clear()V

    const/4 v0, 0x0

    iput-object v0, p0, Lw2/l;->l:Lu2/f;

    iput-object v0, p0, Lw2/l;->v:Lw2/p;

    iput-object v0, p0, Lw2/l;->q:Lw2/v;

    const/4 v1, 0x0

    iput-boolean v1, p0, Lw2/l;->u:Z

    iput-boolean v1, p0, Lw2/l;->x:Z

    iput-boolean v1, p0, Lw2/l;->s:Z

    iput-boolean v1, p0, Lw2/l;->y:Z

    iget-object v2, p0, Lw2/l;->w:Lw2/h;

    invoke-virtual {v2, v1}, Lw2/h;->w(Z)V

    iput-object v0, p0, Lw2/l;->w:Lw2/h;

    iput-object v0, p0, Lw2/l;->t:Lw2/q;

    iput-object v0, p0, Lw2/l;->r:Lu2/a;

    iget-object v0, p0, Lw2/l;->d:Le0/e;

    invoke-interface {v0, p0}, Le0/e;->a(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public a(Lw2/h;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lw2/h<",
            "*>;)V"
        }
    .end annotation

    invoke-direct {p0}, Lw2/l;->j()Lz2/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lz2/a;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public b(Lw2/q;)V
    .locals 0

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lw2/l;->t:Lw2/q;

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Lw2/l;->n()V

    return-void

    :catchall_0
    move-exception p1

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method public c(Lw2/v;Lu2/a;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lw2/v<",
            "TR;>;",
            "Lu2/a;",
            "Z)V"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lw2/l;->q:Lw2/v;

    iput-object p2, p0, Lw2/l;->r:Lu2/a;

    iput-boolean p3, p0, Lw2/l;->y:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Lw2/l;->o()V

    return-void

    :catchall_0
    move-exception p1

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method public d()Lr3/c;
    .locals 1

    iget-object v0, p0, Lw2/l;->b:Lr3/c;

    return-object v0
.end method

.method declared-synchronized e(Lm3/g;Ljava/util/concurrent/Executor;)V
    .locals 2

    goto/32 :goto_3

    nop

    :goto_0
    monitor-exit p0

    goto/32 :goto_1

    nop

    :goto_1
    return-void

    :catchall_0
    move-exception p1

    goto/32 :goto_7

    nop

    :goto_2
    throw p1

    :goto_3
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lw2/l;->b:Lr3/c;

    invoke-virtual {v0}, Lr3/c;->c()V

    iget-object v0, p0, Lw2/l;->a:Lw2/l$e;

    invoke-virtual {v0, p1, p2}, Lw2/l$e;->a(Lm3/g;Ljava/util/concurrent/Executor;)V

    iget-boolean v0, p0, Lw2/l;->s:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    invoke-virtual {p0, v1}, Lw2/l;->k(I)V

    new-instance v0, Lw2/l$b;

    invoke-direct {v0, p0, p1}, Lw2/l$b;-><init>(Lw2/l;Lm3/g;)V

    :goto_4
    invoke-interface {p2, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_6

    :cond_0
    iget-boolean v0, p0, Lw2/l;->u:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0, v1}, Lw2/l;->k(I)V

    new-instance v0, Lw2/l$a;

    invoke-direct {v0, p0, p1}, Lw2/l$a;-><init>(Lw2/l;Lm3/g;)V

    goto :goto_4

    :cond_1
    iget-boolean p1, p0, Lw2/l;->x:Z

    if-nez p1, :cond_2

    goto :goto_5

    :cond_2
    const/4 v1, 0x0

    :goto_5
    const-string p1, "Cannot add callbacks to a cancelled EngineJob"

    invoke-static {v1, p1}, Lq3/j;->a(ZLjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_6
    goto/32 :goto_0

    nop

    :goto_7
    monitor-exit p0

    goto/32 :goto_2

    nop
.end method

.method f(Lm3/g;)V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lw2/l;->t:Lw2/q;

    invoke-interface {p1, v0}, Lm3/g;->b(Lw2/q;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/32 :goto_2

    nop

    :goto_0
    throw v0

    :goto_1
    new-instance v0, Lw2/b;

    goto/32 :goto_3

    nop

    :goto_2
    return-void

    :catchall_0
    move-exception p1

    goto/32 :goto_1

    nop

    :goto_3
    invoke-direct {v0, p1}, Lw2/b;-><init>(Ljava/lang/Throwable;)V

    goto/32 :goto_0

    nop
.end method

.method g(Lm3/g;)V
    .locals 3

    :try_start_0
    iget-object v0, p0, Lw2/l;->v:Lw2/p;

    iget-object v1, p0, Lw2/l;->r:Lu2/a;

    iget-boolean v2, p0, Lw2/l;->y:Z

    invoke-interface {p1, v0, v1, v2}, Lm3/g;->c(Lw2/v;Lu2/a;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/32 :goto_0

    nop

    :goto_0
    return-void

    :catchall_0
    move-exception p1

    goto/32 :goto_1

    nop

    :goto_1
    new-instance v0, Lw2/b;

    goto/32 :goto_3

    nop

    :goto_2
    throw v0

    :goto_3
    invoke-direct {v0, p1}, Lw2/b;-><init>(Ljava/lang/Throwable;)V

    goto/32 :goto_2

    nop
.end method

.method h()V
    .locals 2

    goto/32 :goto_6

    nop

    :goto_0
    return-void

    :goto_1
    iget-object v0, p0, Lw2/l;->w:Lw2/h;

    goto/32 :goto_7

    nop

    :goto_2
    return-void

    :goto_3
    goto/32 :goto_a

    nop

    :goto_4
    iput-boolean v0, p0, Lw2/l;->x:Z

    goto/32 :goto_1

    nop

    :goto_5
    invoke-interface {v0, p0, v1}, Lw2/m;->a(Lw2/l;Lu2/f;)V

    goto/32 :goto_0

    nop

    :goto_6
    invoke-direct {p0}, Lw2/l;->m()Z

    move-result v0

    goto/32 :goto_9

    nop

    :goto_7
    invoke-virtual {v0}, Lw2/h;->e()V

    goto/32 :goto_8

    nop

    :goto_8
    iget-object v0, p0, Lw2/l;->f:Lw2/m;

    goto/32 :goto_b

    nop

    :goto_9
    if-nez v0, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_2

    nop

    :goto_a
    const/4 v0, 0x1

    goto/32 :goto_4

    nop

    :goto_b
    iget-object v1, p0, Lw2/l;->l:Lu2/f;

    goto/32 :goto_5

    nop
.end method

.method i()V
    .locals 3

    goto/32 :goto_5

    nop

    :goto_0
    throw v0

    :goto_1
    return-void

    :catchall_0
    move-exception v0

    :try_start_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/32 :goto_0

    nop

    :goto_2
    invoke-virtual {v0}, Lw2/p;->g()V

    :goto_3
    goto/32 :goto_1

    nop

    :goto_4
    if-nez v0, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_2

    nop

    :goto_5
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lw2/l;->b:Lr3/c;

    invoke-virtual {v0}, Lr3/c;->c()V

    invoke-direct {p0}, Lw2/l;->m()Z

    move-result v0

    const-string v1, "Not yet complete!"

    invoke-static {v0, v1}, Lq3/j;->a(ZLjava/lang/String;)V

    iget-object v0, p0, Lw2/l;->k:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    if-ltz v0, :cond_1

    const/4 v1, 0x1

    goto :goto_6

    :cond_1
    const/4 v1, 0x0

    :goto_6
    const-string v2, "Can\'t decrement below 0"

    invoke-static {v1, v2}, Lq3/j;->a(ZLjava/lang/String;)V

    if-nez v0, :cond_2

    iget-object v0, p0, Lw2/l;->v:Lw2/p;

    invoke-direct {p0}, Lw2/l;->q()V

    goto :goto_7

    :cond_2
    const/4 v0, 0x0

    :goto_7
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/32 :goto_4

    nop
.end method

.method declared-synchronized k(I)V
    .locals 2

    goto/32 :goto_2

    nop

    :goto_0
    monitor-exit p0

    goto/32 :goto_3

    nop

    :goto_1
    return-void

    :catchall_0
    move-exception p1

    goto/32 :goto_0

    nop

    :goto_2
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lw2/l;->m()Z

    move-result v0

    const-string v1, "Not yet complete!"

    invoke-static {v0, v1}, Lq3/j;->a(ZLjava/lang/String;)V

    iget-object v0, p0, Lw2/l;->k:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndAdd(I)I

    move-result p1

    if-nez p1, :cond_0

    iget-object p1, p0, Lw2/l;->v:Lw2/p;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lw2/p;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    goto/32 :goto_4

    nop

    :goto_3
    throw p1

    :goto_4
    monitor-exit p0

    goto/32 :goto_1

    nop
.end method

.method declared-synchronized l(Lu2/f;ZZZZ)Lw2/l;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lu2/f;",
            "ZZZZ)",
            "Lw2/l<",
            "TR;>;"
        }
    .end annotation

    goto/32 :goto_0

    nop

    :goto_0
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lw2/l;->l:Lu2/f;

    iput-boolean p2, p0, Lw2/l;->m:Z

    iput-boolean p3, p0, Lw2/l;->n:Z

    iput-boolean p4, p0, Lw2/l;->o:Z

    iput-boolean p5, p0, Lw2/l;->p:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/32 :goto_4

    nop

    :goto_1
    throw p1

    :goto_2
    monitor-exit p0

    goto/32 :goto_1

    nop

    :goto_3
    return-object p0

    :catchall_0
    move-exception p1

    goto/32 :goto_2

    nop

    :goto_4
    monitor-exit p0

    goto/32 :goto_3

    nop
.end method

.method n()V
    .locals 4

    goto/32 :goto_c

    nop

    :goto_0
    if-nez v1, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_2

    nop

    :goto_1
    check-cast v1, Lw2/l$d;

    goto/32 :goto_9

    nop

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_1

    nop

    :goto_3
    invoke-virtual {p0}, Lw2/l;->i()V

    goto/32 :goto_d

    nop

    :goto_4
    goto :goto_b

    :goto_5
    goto/32 :goto_3

    nop

    :goto_6
    const/4 v3, 0x0

    goto/32 :goto_10

    nop

    :goto_7
    throw v0

    :goto_8
    new-instance v3, Lw2/l$a;

    goto/32 :goto_f

    nop

    :goto_9
    iget-object v2, v1, Lw2/l$d;->b:Ljava/util/concurrent/Executor;

    goto/32 :goto_8

    nop

    :goto_a
    invoke-virtual {v2}, Lw2/l$e;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_b
    goto/32 :goto_e

    nop

    :goto_c
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lw2/l;->b:Lr3/c;

    invoke-virtual {v0}, Lr3/c;->c()V

    iget-boolean v0, p0, Lw2/l;->x:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lw2/l;->q()V

    monitor-exit p0

    return-void

    :cond_1
    iget-object v0, p0, Lw2/l;->a:Lw2/l$e;

    invoke-virtual {v0}, Lw2/l$e;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lw2/l;->u:Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lw2/l;->u:Z

    iget-object v1, p0, Lw2/l;->l:Lu2/f;

    iget-object v2, p0, Lw2/l;->a:Lw2/l$e;

    invoke-virtual {v2}, Lw2/l$e;->c()Lw2/l$e;

    move-result-object v2

    invoke-virtual {v2}, Lw2/l$e;->size()I

    move-result v3

    add-int/2addr v3, v0

    invoke-virtual {p0, v3}, Lw2/l;->k(I)V

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/32 :goto_13

    nop

    :goto_d
    return-void

    :cond_2
    :try_start_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Already failed once"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Received an exception without any callbacks to notify"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/32 :goto_7

    nop

    :goto_e
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_0

    nop

    :goto_f
    iget-object v1, v1, Lw2/l$d;->a:Lm3/g;

    goto/32 :goto_12

    nop

    :goto_10
    invoke-interface {v0, p0, v1, v3}, Lw2/m;->b(Lw2/l;Lu2/f;Lw2/p;)V

    goto/32 :goto_a

    nop

    :goto_11
    invoke-interface {v2, v3}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto/32 :goto_4

    nop

    :goto_12
    invoke-direct {v3, p0, v1}, Lw2/l$a;-><init>(Lw2/l;Lm3/g;)V

    goto/32 :goto_11

    nop

    :goto_13
    iget-object v0, p0, Lw2/l;->f:Lw2/m;

    goto/32 :goto_6

    nop
.end method

.method o()V
    .locals 5

    goto/32 :goto_7

    nop

    :goto_0
    iget-object v3, p0, Lw2/l;->f:Lw2/m;

    goto/32 :goto_2

    nop

    :goto_1
    new-instance v3, Lw2/l$b;

    goto/32 :goto_12

    nop

    :goto_2
    invoke-interface {v3, p0, v0, v2}, Lw2/m;->b(Lw2/l;Lu2/f;Lw2/p;)V

    goto/32 :goto_5

    nop

    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_10

    nop

    :goto_4
    invoke-interface {v2, v3}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto/32 :goto_b

    nop

    :goto_5
    invoke-virtual {v1}, Lw2/l$e;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_6
    goto/32 :goto_9

    nop

    :goto_7
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lw2/l;->b:Lr3/c;

    invoke-virtual {v0}, Lr3/c;->c()V

    iget-boolean v0, p0, Lw2/l;->x:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lw2/l;->q:Lw2/v;

    invoke-interface {v0}, Lw2/v;->a()V

    invoke-direct {p0}, Lw2/l;->q()V

    monitor-exit p0

    return-void

    :cond_0
    iget-object v0, p0, Lw2/l;->a:Lw2/l$e;

    invoke-virtual {v0}, Lw2/l$e;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lw2/l;->s:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lw2/l;->e:Lw2/l$c;

    iget-object v1, p0, Lw2/l;->q:Lw2/v;

    iget-boolean v2, p0, Lw2/l;->m:Z

    iget-object v3, p0, Lw2/l;->l:Lu2/f;

    iget-object v4, p0, Lw2/l;->c:Lw2/p$a;

    invoke-virtual {v0, v1, v2, v3, v4}, Lw2/l$c;->a(Lw2/v;ZLu2/f;Lw2/p$a;)Lw2/p;

    move-result-object v0

    iput-object v0, p0, Lw2/l;->v:Lw2/p;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lw2/l;->s:Z

    iget-object v1, p0, Lw2/l;->a:Lw2/l$e;

    invoke-virtual {v1}, Lw2/l$e;->c()Lw2/l$e;

    move-result-object v1

    invoke-virtual {v1}, Lw2/l$e;->size()I

    move-result v2

    add-int/2addr v2, v0

    invoke-virtual {p0, v2}, Lw2/l;->k(I)V

    iget-object v0, p0, Lw2/l;->l:Lu2/f;

    iget-object v2, p0, Lw2/l;->v:Lw2/p;

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/32 :goto_0

    nop

    :goto_8
    if-nez v1, :cond_1

    goto/32 :goto_c

    :cond_1
    goto/32 :goto_3

    nop

    :goto_9
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_8

    nop

    :goto_a
    iget-object v2, v1, Lw2/l$d;->b:Ljava/util/concurrent/Executor;

    goto/32 :goto_1

    nop

    :goto_b
    goto :goto_6

    :goto_c
    goto/32 :goto_d

    nop

    :goto_d
    invoke-virtual {p0}, Lw2/l;->i()V

    goto/32 :goto_f

    nop

    :goto_e
    invoke-direct {v3, p0, v1}, Lw2/l$b;-><init>(Lw2/l;Lm3/g;)V

    goto/32 :goto_4

    nop

    :goto_f
    return-void

    :cond_2
    :try_start_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Already have resource"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Received a resource without any callbacks to notify"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/32 :goto_11

    nop

    :goto_10
    check-cast v1, Lw2/l$d;

    goto/32 :goto_a

    nop

    :goto_11
    throw v0

    :goto_12
    iget-object v1, v1, Lw2/l$d;->a:Lm3/g;

    goto/32 :goto_e

    nop
.end method

.method p()Z
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-boolean v0, p0, Lw2/l;->p:Z

    goto/32 :goto_1

    nop

    :goto_1
    return v0
.end method

.method declared-synchronized r(Lm3/g;)V
    .locals 1

    goto/32 :goto_3

    nop

    :goto_0
    throw p1

    :goto_1
    return-void

    :catchall_0
    move-exception p1

    goto/32 :goto_2

    nop

    :goto_2
    monitor-exit p0

    goto/32 :goto_0

    nop

    :goto_3
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lw2/l;->b:Lr3/c;

    invoke-virtual {v0}, Lr3/c;->c()V

    iget-object v0, p0, Lw2/l;->a:Lw2/l$e;

    invoke-virtual {v0, p1}, Lw2/l$e;->e(Lm3/g;)V

    iget-object p1, p0, Lw2/l;->a:Lw2/l$e;

    invoke-virtual {p1}, Lw2/l$e;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_2

    invoke-virtual {p0}, Lw2/l;->h()V

    iget-boolean p1, p0, Lw2/l;->s:Z

    if-nez p1, :cond_1

    iget-boolean p1, p0, Lw2/l;->u:Z

    if-eqz p1, :cond_0

    goto :goto_4

    :cond_0
    const/4 p1, 0x0

    goto :goto_5

    :cond_1
    :goto_4
    const/4 p1, 0x1

    :goto_5
    if-eqz p1, :cond_2

    iget-object p1, p0, Lw2/l;->k:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result p1

    if-nez p1, :cond_2

    invoke-direct {p0}, Lw2/l;->q()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    goto/32 :goto_6

    nop

    :goto_6
    monitor-exit p0

    goto/32 :goto_1

    nop
.end method

.method public declared-synchronized s(Lw2/h;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lw2/h<",
            "TR;>;)V"
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lw2/l;->w:Lw2/h;

    invoke-virtual {p1}, Lw2/h;->C()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lw2/l;->g:Lz2/a;

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lw2/l;->j()Lz2/a;

    move-result-object v0

    :goto_0
    invoke-virtual {v0, p1}, Lz2/a;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method
