.class Lw2/h;
.super Ljava/lang/Object;

# interfaces
.implements Lw2/f$a;
.implements Ljava/lang/Runnable;
.implements Ljava/lang/Comparable;
.implements Lr3/a$f;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lw2/h$h;,
        Lw2/h$g;,
        Lw2/h$e;,
        Lw2/h$b;,
        Lw2/h$d;,
        Lw2/h$f;,
        Lw2/h$c;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lw2/f$a;",
        "Ljava/lang/Runnable;",
        "Ljava/lang/Comparable<",
        "Lw2/h<",
        "*>;>;",
        "Lr3/a$f;"
    }
.end annotation


# instance fields
.field private A:Lu2/a;

.field private B:Lcom/bumptech/glide/load/data/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/bumptech/glide/load/data/d<",
            "*>;"
        }
    .end annotation
.end field

.field private volatile C:Lw2/f;

.field private volatile D:Z

.field private volatile E:Z

.field private F:Z

.field private final a:Lw2/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lw2/g<",
            "TR;>;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Throwable;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lr3/c;

.field private final d:Lw2/h$e;

.field private final e:Le0/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Le0/e<",
            "Lw2/h<",
            "*>;>;"
        }
    .end annotation
.end field

.field private final f:Lw2/h$d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lw2/h$d<",
            "*>;"
        }
    .end annotation
.end field

.field private final g:Lw2/h$f;

.field private h:Lcom/bumptech/glide/d;

.field private i:Lu2/f;

.field private j:Lcom/bumptech/glide/g;

.field private k:Lw2/n;

.field private l:I

.field private m:I

.field private n:Lw2/j;

.field private o:Lu2/h;

.field private p:Lw2/h$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lw2/h$b<",
            "TR;>;"
        }
    .end annotation
.end field

.field private q:I

.field private r:Lw2/h$h;

.field private s:Lw2/h$g;

.field private t:J

.field private u:Z

.field private v:Ljava/lang/Object;

.field private w:Ljava/lang/Thread;

.field private x:Lu2/f;

.field private y:Lu2/f;

.field private z:Ljava/lang/Object;


# direct methods
.method constructor <init>(Lw2/h$e;Le0/e;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lw2/h$e;",
            "Le0/e<",
            "Lw2/h<",
            "*>;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lw2/g;

    invoke-direct {v0}, Lw2/g;-><init>()V

    iput-object v0, p0, Lw2/h;->a:Lw2/g;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lw2/h;->b:Ljava/util/List;

    invoke-static {}, Lr3/c;->a()Lr3/c;

    move-result-object v0

    iput-object v0, p0, Lw2/h;->c:Lr3/c;

    new-instance v0, Lw2/h$d;

    invoke-direct {v0}, Lw2/h$d;-><init>()V

    iput-object v0, p0, Lw2/h;->f:Lw2/h$d;

    new-instance v0, Lw2/h$f;

    invoke-direct {v0}, Lw2/h$f;-><init>()V

    iput-object v0, p0, Lw2/h;->g:Lw2/h$f;

    iput-object p1, p0, Lw2/h;->d:Lw2/h$e;

    iput-object p2, p0, Lw2/h;->e:Le0/e;

    return-void
.end method

.method private A()V
    .locals 3

    sget-object v0, Lw2/h$a;->a:[I

    iget-object v1, p0, Lw2/h;->s:Lw2/h$g;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lw2/h;->i()V

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unrecognized run reason: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lw2/h;->s:Lw2/h$g;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    sget-object v0, Lw2/h$h;->a:Lw2/h$h;

    invoke-direct {p0, v0}, Lw2/h;->k(Lw2/h$h;)Lw2/h$h;

    move-result-object v0

    iput-object v0, p0, Lw2/h;->r:Lw2/h$h;

    invoke-direct {p0}, Lw2/h;->j()Lw2/f;

    move-result-object v0

    iput-object v0, p0, Lw2/h;->C:Lw2/f;

    :cond_2
    invoke-direct {p0}, Lw2/h;->y()V

    :goto_0
    return-void
.end method

.method private B()V
    .locals 3

    iget-object v0, p0, Lw2/h;->c:Lr3/c;

    invoke-virtual {v0}, Lr3/c;->c()V

    iget-boolean v0, p0, Lw2/h;->D:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    iget-object v0, p0, Lw2/h;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lw2/h;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    sub-int/2addr v2, v1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    :goto_0
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Already notified"

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :cond_1
    iput-boolean v1, p0, Lw2/h;->D:Z

    return-void
.end method

.method private g(Lcom/bumptech/glide/load/data/d;Ljava/lang/Object;Lu2/a;)Lw2/v;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Data:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/bumptech/glide/load/data/d<",
            "*>;TData;",
            "Lu2/a;",
            ")",
            "Lw2/v<",
            "TR;>;"
        }
    .end annotation

    if-nez p2, :cond_0

    const/4 p2, 0x0

    invoke-interface {p1}, Lcom/bumptech/glide/load/data/d;->b()V

    return-object p2

    :cond_0
    :try_start_0
    invoke-static {}, Lq3/f;->b()J

    move-result-wide v0

    invoke-direct {p0, p2, p3}, Lw2/h;->h(Ljava/lang/Object;Lu2/a;)Lw2/v;

    move-result-object p2

    const-string p3, "DecodeJob"

    const/4 v2, 0x2

    invoke-static {p3, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result p3

    if-eqz p3, :cond_1

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Decoded result "

    invoke-virtual {p3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-direct {p0, p3, v0, v1}, Lw2/h;->o(Ljava/lang/String;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    invoke-interface {p1}, Lcom/bumptech/glide/load/data/d;->b()V

    return-object p2

    :catchall_0
    move-exception p2

    invoke-interface {p1}, Lcom/bumptech/glide/load/data/d;->b()V

    throw p2
.end method

.method private h(Ljava/lang/Object;Lu2/a;)Lw2/v;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Data:",
            "Ljava/lang/Object;",
            ">(TData;",
            "Lu2/a;",
            ")",
            "Lw2/v<",
            "TR;>;"
        }
    .end annotation

    iget-object v0, p0, Lw2/h;->a:Lw2/g;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Lw2/g;->h(Ljava/lang/Class;)Lw2/t;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lw2/h;->z(Ljava/lang/Object;Lu2/a;Lw2/t;)Lw2/v;

    move-result-object p1

    return-object p1
.end method

.method private i()V
    .locals 4

    const-string v0, "DecodeJob"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lw2/h;->t:J

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "data: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lw2/h;->z:Ljava/lang/Object;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v3, ", cache key: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lw2/h;->x:Lu2/f;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v3, ", fetcher: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lw2/h;->B:Lcom/bumptech/glide/load/data/d;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Retrieved data"

    invoke-direct {p0, v3, v0, v1, v2}, Lw2/h;->p(Ljava/lang/String;JLjava/lang/String;)V

    :cond_0
    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lw2/h;->B:Lcom/bumptech/glide/load/data/d;

    iget-object v2, p0, Lw2/h;->z:Ljava/lang/Object;

    iget-object v3, p0, Lw2/h;->A:Lu2/a;

    invoke-direct {p0, v1, v2, v3}, Lw2/h;->g(Lcom/bumptech/glide/load/data/d;Ljava/lang/Object;Lu2/a;)Lw2/v;

    move-result-object v0
    :try_end_0
    .catch Lw2/q; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    iget-object v2, p0, Lw2/h;->y:Lu2/f;

    iget-object v3, p0, Lw2/h;->A:Lu2/a;

    invoke-virtual {v1, v2, v3}, Lw2/q;->i(Lu2/f;Lu2/a;)V

    iget-object v2, p0, Lw2/h;->b:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_0
    if-eqz v0, :cond_1

    iget-object v1, p0, Lw2/h;->A:Lu2/a;

    iget-boolean v2, p0, Lw2/h;->F:Z

    invoke-direct {p0, v0, v1, v2}, Lw2/h;->r(Lw2/v;Lu2/a;Z)V

    goto :goto_1

    :cond_1
    invoke-direct {p0}, Lw2/h;->y()V

    :goto_1
    return-void
.end method

.method private j()Lw2/f;
    .locals 3

    sget-object v0, Lw2/h$a;->b:[I

    iget-object v1, p0, Lw2/h;->r:Lw2/h$h;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unrecognized stage: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lw2/h;->r:Lw2/h$h;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    new-instance v0, Lw2/z;

    iget-object v1, p0, Lw2/h;->a:Lw2/g;

    invoke-direct {v0, v1, p0}, Lw2/z;-><init>(Lw2/g;Lw2/f$a;)V

    return-object v0

    :cond_2
    new-instance v0, Lw2/c;

    iget-object v1, p0, Lw2/h;->a:Lw2/g;

    invoke-direct {v0, v1, p0}, Lw2/c;-><init>(Lw2/g;Lw2/f$a;)V

    return-object v0

    :cond_3
    new-instance v0, Lw2/w;

    iget-object v1, p0, Lw2/h;->a:Lw2/g;

    invoke-direct {v0, v1, p0}, Lw2/w;-><init>(Lw2/g;Lw2/f$a;)V

    return-object v0
.end method

.method private k(Lw2/h$h;)Lw2/h$h;
    .locals 3

    sget-object v0, Lw2/h$a;->b:[I

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_5

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    iget-object p1, p0, Lw2/h;->n:Lw2/j;

    invoke-virtual {p1}, Lw2/j;->b()Z

    move-result p1

    if-eqz p1, :cond_0

    sget-object p1, Lw2/h$h;->b:Lw2/h$h;

    goto :goto_0

    :cond_0
    sget-object p1, Lw2/h$h;->b:Lw2/h$h;

    invoke-direct {p0, p1}, Lw2/h;->k(Lw2/h$h;)Lw2/h$h;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unrecognized stage: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    sget-object p1, Lw2/h$h;->f:Lw2/h$h;

    return-object p1

    :cond_3
    iget-boolean p1, p0, Lw2/h;->u:Z

    if-eqz p1, :cond_4

    sget-object p1, Lw2/h$h;->f:Lw2/h$h;

    goto :goto_1

    :cond_4
    sget-object p1, Lw2/h$h;->d:Lw2/h$h;

    :goto_1
    return-object p1

    :cond_5
    iget-object p1, p0, Lw2/h;->n:Lw2/j;

    invoke-virtual {p1}, Lw2/j;->a()Z

    move-result p1

    if-eqz p1, :cond_6

    sget-object p1, Lw2/h$h;->c:Lw2/h$h;

    goto :goto_2

    :cond_6
    sget-object p1, Lw2/h$h;->c:Lw2/h$h;

    invoke-direct {p0, p1}, Lw2/h;->k(Lw2/h$h;)Lw2/h$h;

    move-result-object p1

    :goto_2
    return-object p1
.end method

.method private l(Lu2/a;)Lu2/h;
    .locals 3

    iget-object v0, p0, Lw2/h;->o:Lu2/h;

    sget-object v1, Lu2/a;->d:Lu2/a;

    if-eq p1, v1, :cond_1

    iget-object p1, p0, Lw2/h;->a:Lw2/g;

    invoke-virtual {p1}, Lw2/g;->w()Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    sget-object v1, Ld3/n;->j:Lu2/g;

    invoke-virtual {v0, v1}, Lu2/h;->c(Lu2/g;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_2

    if-eqz p1, :cond_3

    :cond_2
    return-object v0

    :cond_3
    new-instance v0, Lu2/h;

    invoke-direct {v0}, Lu2/h;-><init>()V

    iget-object v2, p0, Lw2/h;->o:Lu2/h;

    invoke-virtual {v0, v2}, Lu2/h;->d(Lu2/h;)V

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lu2/h;->e(Lu2/g;Ljava/lang/Object;)Lu2/h;

    return-object v0
.end method

.method private m()I
    .locals 1

    iget-object v0, p0, Lw2/h;->j:Lcom/bumptech/glide/g;

    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    return v0
.end method

.method private o(Ljava/lang/String;J)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lw2/h;->p(Ljava/lang/String;JLjava/lang/String;)V

    return-void
.end method

.method private p(Ljava/lang/String;JLjava/lang/String;)V
    .locals 1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " in "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p2, p3}, Lq3/f;->a(J)D

    move-result-wide p1

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string p1, ", load key: "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lw2/h;->k:Lw2/n;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    if-eqz p4, :cond_0

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, ", "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const-string p1, ""

    :goto_0
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ", thread: "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "DecodeJob"

    invoke-static {p2, p1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private q(Lw2/v;Lu2/a;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lw2/v<",
            "TR;>;",
            "Lu2/a;",
            "Z)V"
        }
    .end annotation

    invoke-direct {p0}, Lw2/h;->B()V

    iget-object v0, p0, Lw2/h;->p:Lw2/h$b;

    invoke-interface {v0, p1, p2, p3}, Lw2/h$b;->c(Lw2/v;Lu2/a;Z)V

    return-void
.end method

.method private r(Lw2/v;Lu2/a;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lw2/v<",
            "TR;>;",
            "Lu2/a;",
            "Z)V"
        }
    .end annotation

    instance-of v0, p1, Lw2/r;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lw2/r;

    invoke-interface {v0}, Lw2/r;->b()V

    :cond_0
    const/4 v0, 0x0

    iget-object v1, p0, Lw2/h;->f:Lw2/h$d;

    invoke-virtual {v1}, Lw2/h$d;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {p1}, Lw2/u;->f(Lw2/v;)Lw2/u;

    move-result-object p1

    move-object v0, p1

    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lw2/h;->q(Lw2/v;Lu2/a;Z)V

    sget-object p1, Lw2/h$h;->e:Lw2/h$h;

    iput-object p1, p0, Lw2/h;->r:Lw2/h$h;

    :try_start_0
    iget-object p1, p0, Lw2/h;->f:Lw2/h$d;

    invoke-virtual {p1}, Lw2/h$d;->c()Z

    move-result p1

    if-eqz p1, :cond_2

    iget-object p1, p0, Lw2/h;->f:Lw2/h$d;

    iget-object p2, p0, Lw2/h;->d:Lw2/h$e;

    iget-object p3, p0, Lw2/h;->o:Lu2/h;

    invoke-virtual {p1, p2, p3}, Lw2/h$d;->b(Lw2/h$e;Lu2/h;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lw2/u;->h()V

    :cond_3
    invoke-direct {p0}, Lw2/h;->t()V

    return-void

    :catchall_0
    move-exception p1

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lw2/u;->h()V

    :cond_4
    throw p1
.end method

.method private s()V
    .locals 3

    invoke-direct {p0}, Lw2/h;->B()V

    new-instance v0, Lw2/q;

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lw2/h;->b:Ljava/util/List;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    const-string v2, "Failed to load resource"

    invoke-direct {v0, v2, v1}, Lw2/q;-><init>(Ljava/lang/String;Ljava/util/List;)V

    iget-object v1, p0, Lw2/h;->p:Lw2/h$b;

    invoke-interface {v1, v0}, Lw2/h$b;->b(Lw2/q;)V

    invoke-direct {p0}, Lw2/h;->u()V

    return-void
.end method

.method private t()V
    .locals 1

    iget-object v0, p0, Lw2/h;->g:Lw2/h$f;

    invoke-virtual {v0}, Lw2/h$f;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lw2/h;->x()V

    :cond_0
    return-void
.end method

.method private u()V
    .locals 1

    iget-object v0, p0, Lw2/h;->g:Lw2/h$f;

    invoke-virtual {v0}, Lw2/h$f;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lw2/h;->x()V

    :cond_0
    return-void
.end method

.method private x()V
    .locals 4

    iget-object v0, p0, Lw2/h;->g:Lw2/h$f;

    invoke-virtual {v0}, Lw2/h$f;->e()V

    iget-object v0, p0, Lw2/h;->f:Lw2/h$d;

    invoke-virtual {v0}, Lw2/h$d;->a()V

    iget-object v0, p0, Lw2/h;->a:Lw2/g;

    invoke-virtual {v0}, Lw2/g;->a()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lw2/h;->D:Z

    const/4 v1, 0x0

    iput-object v1, p0, Lw2/h;->h:Lcom/bumptech/glide/d;

    iput-object v1, p0, Lw2/h;->i:Lu2/f;

    iput-object v1, p0, Lw2/h;->o:Lu2/h;

    iput-object v1, p0, Lw2/h;->j:Lcom/bumptech/glide/g;

    iput-object v1, p0, Lw2/h;->k:Lw2/n;

    iput-object v1, p0, Lw2/h;->p:Lw2/h$b;

    iput-object v1, p0, Lw2/h;->r:Lw2/h$h;

    iput-object v1, p0, Lw2/h;->C:Lw2/f;

    iput-object v1, p0, Lw2/h;->w:Ljava/lang/Thread;

    iput-object v1, p0, Lw2/h;->x:Lu2/f;

    iput-object v1, p0, Lw2/h;->z:Ljava/lang/Object;

    iput-object v1, p0, Lw2/h;->A:Lu2/a;

    iput-object v1, p0, Lw2/h;->B:Lcom/bumptech/glide/load/data/d;

    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lw2/h;->t:J

    iput-boolean v0, p0, Lw2/h;->E:Z

    iput-object v1, p0, Lw2/h;->v:Ljava/lang/Object;

    iget-object v0, p0, Lw2/h;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lw2/h;->e:Le0/e;

    invoke-interface {v0, p0}, Le0/e;->a(Ljava/lang/Object;)Z

    return-void
.end method

.method private y()V
    .locals 3

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iput-object v0, p0, Lw2/h;->w:Ljava/lang/Thread;

    invoke-static {}, Lq3/f;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lw2/h;->t:J

    const/4 v0, 0x0

    :cond_0
    iget-boolean v1, p0, Lw2/h;->E:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lw2/h;->C:Lw2/f;

    if-eqz v1, :cond_1

    iget-object v0, p0, Lw2/h;->C:Lw2/f;

    invoke-interface {v0}, Lw2/f;->e()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v1, p0, Lw2/h;->r:Lw2/h$h;

    invoke-direct {p0, v1}, Lw2/h;->k(Lw2/h$h;)Lw2/h$h;

    move-result-object v1

    iput-object v1, p0, Lw2/h;->r:Lw2/h$h;

    invoke-direct {p0}, Lw2/h;->j()Lw2/f;

    move-result-object v1

    iput-object v1, p0, Lw2/h;->C:Lw2/f;

    iget-object v1, p0, Lw2/h;->r:Lw2/h$h;

    sget-object v2, Lw2/h$h;->d:Lw2/h$h;

    if-ne v1, v2, :cond_0

    invoke-virtual {p0}, Lw2/h;->b()V

    return-void

    :cond_1
    iget-object v1, p0, Lw2/h;->r:Lw2/h$h;

    sget-object v2, Lw2/h$h;->f:Lw2/h$h;

    if-eq v1, v2, :cond_2

    iget-boolean v1, p0, Lw2/h;->E:Z

    if-eqz v1, :cond_3

    :cond_2
    if-nez v0, :cond_3

    invoke-direct {p0}, Lw2/h;->s()V

    :cond_3
    return-void
.end method

.method private z(Ljava/lang/Object;Lu2/a;Lw2/t;)Lw2/v;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Data:",
            "Ljava/lang/Object;",
            "ResourceType:",
            "Ljava/lang/Object;",
            ">(TData;",
            "Lu2/a;",
            "Lw2/t<",
            "TData;TResourceType;TR;>;)",
            "Lw2/v<",
            "TR;>;"
        }
    .end annotation

    invoke-direct {p0, p2}, Lw2/h;->l(Lu2/a;)Lu2/h;

    move-result-object v2

    iget-object v0, p0, Lw2/h;->h:Lcom/bumptech/glide/d;

    invoke-virtual {v0}, Lcom/bumptech/glide/d;->i()Lcom/bumptech/glide/i;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/bumptech/glide/i;->l(Ljava/lang/Object;)Lcom/bumptech/glide/load/data/e;

    move-result-object p1

    :try_start_0
    iget v3, p0, Lw2/h;->l:I

    iget v4, p0, Lw2/h;->m:I

    new-instance v5, Lw2/h$c;

    invoke-direct {v5, p0, p2}, Lw2/h$c;-><init>(Lw2/h;Lu2/a;)V

    move-object v0, p3

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lw2/t;->a(Lcom/bumptech/glide/load/data/e;Lu2/h;IILw2/i$a;)Lw2/v;

    move-result-object p2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-interface {p1}, Lcom/bumptech/glide/load/data/e;->b()V

    return-object p2

    :catchall_0
    move-exception p2

    invoke-interface {p1}, Lcom/bumptech/glide/load/data/e;->b()V

    throw p2
.end method


# virtual methods
.method C()Z
    .locals 2

    goto/32 :goto_1

    nop

    :goto_0
    invoke-direct {p0, v0}, Lw2/h;->k(Lw2/h$h;)Lw2/h$h;

    move-result-object v0

    goto/32 :goto_4

    nop

    :goto_1
    sget-object v0, Lw2/h$h;->a:Lw2/h$h;

    goto/32 :goto_0

    nop

    :goto_2
    goto :goto_6

    :goto_3
    goto/32 :goto_5

    nop

    :goto_4
    sget-object v1, Lw2/h$h;->b:Lw2/h$h;

    goto/32 :goto_7

    nop

    :goto_5
    const/4 v0, 0x1

    :goto_6
    goto/32 :goto_c

    nop

    :goto_7
    if-ne v0, v1, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_d

    nop

    :goto_8
    const/4 v0, 0x0

    goto/32 :goto_2

    nop

    :goto_9
    goto :goto_3

    :goto_a
    goto/32 :goto_8

    nop

    :goto_b
    if-eq v0, v1, :cond_1

    goto/32 :goto_a

    :cond_1
    goto/32 :goto_9

    nop

    :goto_c
    return v0

    :goto_d
    sget-object v1, Lw2/h$h;->c:Lw2/h$h;

    goto/32 :goto_b

    nop
.end method

.method public a(Lu2/f;Ljava/lang/Object;Lcom/bumptech/glide/load/data/d;Lu2/a;Lu2/f;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lu2/f;",
            "Ljava/lang/Object;",
            "Lcom/bumptech/glide/load/data/d<",
            "*>;",
            "Lu2/a;",
            "Lu2/f;",
            ")V"
        }
    .end annotation

    iput-object p1, p0, Lw2/h;->x:Lu2/f;

    iput-object p2, p0, Lw2/h;->z:Ljava/lang/Object;

    iput-object p3, p0, Lw2/h;->B:Lcom/bumptech/glide/load/data/d;

    iput-object p4, p0, Lw2/h;->A:Lu2/a;

    iput-object p5, p0, Lw2/h;->y:Lu2/f;

    iget-object p2, p0, Lw2/h;->a:Lw2/g;

    invoke-virtual {p2}, Lw2/g;->c()Ljava/util/List;

    move-result-object p2

    const/4 p3, 0x0

    invoke-interface {p2, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    if-eq p1, p2, :cond_0

    const/4 p3, 0x1

    :cond_0
    iput-boolean p3, p0, Lw2/h;->F:Z

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object p1

    iget-object p2, p0, Lw2/h;->w:Ljava/lang/Thread;

    if-eq p1, p2, :cond_1

    sget-object p1, Lw2/h$g;->c:Lw2/h$g;

    iput-object p1, p0, Lw2/h;->s:Lw2/h$g;

    iget-object p1, p0, Lw2/h;->p:Lw2/h$b;

    invoke-interface {p1, p0}, Lw2/h$b;->a(Lw2/h;)V

    goto :goto_0

    :cond_1
    const-string p1, "DecodeJob.decodeFromRetrievedData"

    invoke-static {p1}, Lr3/b;->a(Ljava/lang/String;)V

    :try_start_0
    invoke-direct {p0}, Lw2/h;->i()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {}, Lr3/b;->d()V

    :goto_0
    return-void

    :catchall_0
    move-exception p1

    invoke-static {}, Lr3/b;->d()V

    throw p1
.end method

.method public b()V
    .locals 1

    sget-object v0, Lw2/h$g;->b:Lw2/h$g;

    iput-object v0, p0, Lw2/h;->s:Lw2/h$g;

    iget-object v0, p0, Lw2/h;->p:Lw2/h$b;

    invoke-interface {v0, p0}, Lw2/h$b;->a(Lw2/h;)V

    return-void
.end method

.method public c(Lu2/f;Ljava/lang/Exception;Lcom/bumptech/glide/load/data/d;Lu2/a;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lu2/f;",
            "Ljava/lang/Exception;",
            "Lcom/bumptech/glide/load/data/d<",
            "*>;",
            "Lu2/a;",
            ")V"
        }
    .end annotation

    invoke-interface {p3}, Lcom/bumptech/glide/load/data/d;->b()V

    new-instance v0, Lw2/q;

    const-string v1, "Fetching data failed"

    invoke-direct {v0, v1, p2}, Lw2/q;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-interface {p3}, Lcom/bumptech/glide/load/data/d;->a()Ljava/lang/Class;

    move-result-object p2

    invoke-virtual {v0, p1, p4, p2}, Lw2/q;->j(Lu2/f;Lu2/a;Ljava/lang/Class;)V

    iget-object p1, p0, Lw2/h;->b:Ljava/util/List;

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object p1

    iget-object p2, p0, Lw2/h;->w:Ljava/lang/Thread;

    if-eq p1, p2, :cond_0

    sget-object p1, Lw2/h$g;->b:Lw2/h$g;

    iput-object p1, p0, Lw2/h;->s:Lw2/h$g;

    iget-object p1, p0, Lw2/h;->p:Lw2/h$b;

    invoke-interface {p1, p0}, Lw2/h$b;->a(Lw2/h;)V

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lw2/h;->y()V

    :goto_0
    return-void
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    check-cast p1, Lw2/h;

    invoke-virtual {p0, p1}, Lw2/h;->f(Lw2/h;)I

    move-result p1

    return p1
.end method

.method public d()Lr3/c;
    .locals 1

    iget-object v0, p0, Lw2/h;->c:Lr3/c;

    return-object v0
.end method

.method public e()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lw2/h;->E:Z

    iget-object v0, p0, Lw2/h;->C:Lw2/f;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lw2/f;->cancel()V

    :cond_0
    return-void
.end method

.method public f(Lw2/h;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lw2/h<",
            "*>;)I"
        }
    .end annotation

    invoke-direct {p0}, Lw2/h;->m()I

    move-result v0

    invoke-direct {p1}, Lw2/h;->m()I

    move-result v1

    sub-int/2addr v0, v1

    if-nez v0, :cond_0

    iget v0, p0, Lw2/h;->q:I

    iget p1, p1, Lw2/h;->q:I

    sub-int/2addr v0, p1

    :cond_0
    return v0
.end method

.method n(Lcom/bumptech/glide/d;Ljava/lang/Object;Lw2/n;Lu2/f;IILjava/lang/Class;Ljava/lang/Class;Lcom/bumptech/glide/g;Lw2/j;Ljava/util/Map;ZZZLu2/h;Lw2/h$b;I)Lw2/h;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/bumptech/glide/d;",
            "Ljava/lang/Object;",
            "Lw2/n;",
            "Lu2/f;",
            "II",
            "Ljava/lang/Class<",
            "*>;",
            "Ljava/lang/Class<",
            "TR;>;",
            "Lcom/bumptech/glide/g;",
            "Lw2/j;",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lu2/l<",
            "*>;>;ZZZ",
            "Lu2/h;",
            "Lw2/h$b<",
            "TR;>;I)",
            "Lw2/h<",
            "TR;>;"
        }
    .end annotation

    goto/32 :goto_22

    nop

    :goto_0
    iput-object v1, v0, Lw2/h;->i:Lu2/f;

    goto/32 :goto_1c

    nop

    :goto_1
    move-object/from16 v4, p4

    goto/32 :goto_1f

    nop

    :goto_2
    move-object/from16 v1, p2

    goto/32 :goto_29

    nop

    :goto_3
    move/from16 v1, p17

    goto/32 :goto_7

    nop

    :goto_4
    move/from16 v1, p14

    goto/32 :goto_1d

    nop

    :goto_5
    iput-object v1, v0, Lw2/h;->n:Lw2/j;

    goto/32 :goto_4

    nop

    :goto_6
    move-object/from16 v8, p7

    goto/32 :goto_e

    nop

    :goto_7
    iput v1, v0, Lw2/h;->q:I

    goto/32 :goto_1a

    nop

    :goto_8
    move-object/from16 v1, p15

    goto/32 :goto_20

    nop

    :goto_9
    move/from16 v1, p6

    goto/32 :goto_28

    nop

    :goto_a
    iput-object v1, v0, Lw2/h;->s:Lw2/h$g;

    goto/32 :goto_2

    nop

    :goto_b
    move-object/from16 v11, p15

    goto/32 :goto_11

    nop

    :goto_c
    iput-object v1, v0, Lw2/h;->p:Lw2/h$b;

    goto/32 :goto_3

    nop

    :goto_d
    iget-object v1, v0, Lw2/h;->a:Lw2/g;

    goto/32 :goto_1b

    nop

    :goto_e
    move-object/from16 v9, p8

    goto/32 :goto_10

    nop

    :goto_f
    return-object v0

    :goto_10
    move-object/from16 v10, p9

    goto/32 :goto_b

    nop

    :goto_11
    move-object/from16 v12, p11

    goto/32 :goto_17

    nop

    :goto_12
    move-object/from16 v1, p3

    goto/32 :goto_26

    nop

    :goto_13
    iput v1, v0, Lw2/h;->l:I

    goto/32 :goto_9

    nop

    :goto_14
    move-object/from16 v1, p1

    goto/32 :goto_2b

    nop

    :goto_15
    move-object/from16 v1, p10

    goto/32 :goto_5

    nop

    :goto_16
    move/from16 v14, p13

    goto/32 :goto_18

    nop

    :goto_17
    move/from16 v13, p12

    goto/32 :goto_16

    nop

    :goto_18
    invoke-virtual/range {v1 .. v15}, Lw2/g;->u(Lcom/bumptech/glide/d;Ljava/lang/Object;Lu2/f;IILw2/j;Ljava/lang/Class;Ljava/lang/Class;Lcom/bumptech/glide/g;Lu2/h;Ljava/util/Map;ZZLw2/h$e;)V

    goto/32 :goto_14

    nop

    :goto_19
    move-object/from16 v2, p1

    goto/32 :goto_2a

    nop

    :goto_1a
    sget-object v1, Lw2/h$g;->a:Lw2/h$g;

    goto/32 :goto_a

    nop

    :goto_1b
    iget-object v15, v0, Lw2/h;->d:Lw2/h$e;

    goto/32 :goto_19

    nop

    :goto_1c
    move-object/from16 v1, p9

    goto/32 :goto_1e

    nop

    :goto_1d
    iput-boolean v1, v0, Lw2/h;->u:Z

    goto/32 :goto_8

    nop

    :goto_1e
    iput-object v1, v0, Lw2/h;->j:Lcom/bumptech/glide/g;

    goto/32 :goto_12

    nop

    :goto_1f
    move/from16 v5, p5

    goto/32 :goto_21

    nop

    :goto_20
    iput-object v1, v0, Lw2/h;->o:Lu2/h;

    goto/32 :goto_24

    nop

    :goto_21
    move/from16 v6, p6

    goto/32 :goto_25

    nop

    :goto_22
    move-object/from16 v0, p0

    goto/32 :goto_d

    nop

    :goto_23
    move/from16 v1, p5

    goto/32 :goto_13

    nop

    :goto_24
    move-object/from16 v1, p16

    goto/32 :goto_c

    nop

    :goto_25
    move-object/from16 v7, p10

    goto/32 :goto_6

    nop

    :goto_26
    iput-object v1, v0, Lw2/h;->k:Lw2/n;

    goto/32 :goto_23

    nop

    :goto_27
    move-object/from16 v1, p4

    goto/32 :goto_0

    nop

    :goto_28
    iput v1, v0, Lw2/h;->m:I

    goto/32 :goto_15

    nop

    :goto_29
    iput-object v1, v0, Lw2/h;->v:Ljava/lang/Object;

    goto/32 :goto_f

    nop

    :goto_2a
    move-object/from16 v3, p2

    goto/32 :goto_1

    nop

    :goto_2b
    iput-object v1, v0, Lw2/h;->h:Lcom/bumptech/glide/d;

    goto/32 :goto_27

    nop
.end method

.method public run()V
    .locals 5

    const-string v0, "DecodeJob"

    iget-object v1, p0, Lw2/h;->v:Ljava/lang/Object;

    const-string v2, "DecodeJob#run(model=%s)"

    invoke-static {v2, v1}, Lr3/b;->b(Ljava/lang/String;Ljava/lang/Object;)V

    iget-object v1, p0, Lw2/h;->B:Lcom/bumptech/glide/load/data/d;

    :try_start_0
    iget-boolean v2, p0, Lw2/h;->E:Z

    if-eqz v2, :cond_1

    invoke-direct {p0}, Lw2/h;->s()V
    :try_end_0
    .catch Lw2/b; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    invoke-interface {v1}, Lcom/bumptech/glide/load/data/d;->b()V

    :cond_0
    invoke-static {}, Lr3/b;->d()V

    return-void

    :cond_1
    :try_start_1
    invoke-direct {p0}, Lw2/h;->A()V
    :try_end_1
    .catch Lw2/b; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v1, :cond_2

    invoke-interface {v1}, Lcom/bumptech/glide/load/data/d;->b()V

    :cond_2
    invoke-static {}, Lr3/b;->d()V

    return-void

    :catchall_0
    move-exception v2

    const/4 v3, 0x3

    :try_start_2
    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DecodeJob threw unexpectedly, isCancelled: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v4, p0, Lw2/h;->E:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v4, ", stage: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lw2/h;->r:Lw2/h$h;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_3
    iget-object v0, p0, Lw2/h;->r:Lw2/h$h;

    sget-object v3, Lw2/h$h;->e:Lw2/h$h;

    if-eq v0, v3, :cond_4

    iget-object v0, p0, Lw2/h;->b:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-direct {p0}, Lw2/h;->s()V

    :cond_4
    iget-boolean v0, p0, Lw2/h;->E:Z

    if-nez v0, :cond_5

    throw v2

    :cond_5
    throw v2

    :catch_0
    move-exception v0

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :catchall_1
    move-exception v0

    if-eqz v1, :cond_6

    invoke-interface {v1}, Lcom/bumptech/glide/load/data/d;->b()V

    :cond_6
    invoke-static {}, Lr3/b;->d()V

    throw v0
.end method

.method v(Lu2/a;Lw2/v;)Lw2/v;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Z:",
            "Ljava/lang/Object;",
            ">(",
            "Lu2/a;",
            "Lw2/v<",
            "TZ;>;)",
            "Lw2/v<",
            "TZ;>;"
        }
    .end annotation

    goto/32 :goto_3f

    nop

    :goto_0
    aget p1, p1, v1

    goto/32 :goto_42

    nop

    :goto_1
    iget-object v1, p0, Lw2/h;->i:Lu2/f;

    goto/32 :goto_25

    nop

    :goto_2
    sget-object v0, Lu2/a;->d:Lu2/a;

    goto/32 :goto_36

    nop

    :goto_3
    if-nez p2, :cond_0

    goto/32 :goto_a

    :cond_0
    goto/32 :goto_1b

    nop

    :goto_4
    iget-object v2, p0, Lw2/h;->h:Lcom/bumptech/glide/d;

    goto/32 :goto_19

    nop

    :goto_5
    move-object v10, v1

    goto/32 :goto_33

    nop

    :goto_6
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto/32 :goto_1a

    nop

    :goto_7
    iget v6, p0, Lw2/h;->m:I

    goto/32 :goto_30

    nop

    :goto_8
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_4b

    nop

    :goto_9
    goto/16 :goto_23

    :goto_a
    goto/32 :goto_22

    nop

    :goto_b
    return-object v0

    :goto_c
    if-eq p1, v1, :cond_1

    goto/32 :goto_3d

    :cond_1
    goto/32 :goto_24

    nop

    :goto_d
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    goto/32 :goto_2

    nop

    :goto_e
    if-nez p1, :cond_2

    goto/32 :goto_17

    :cond_2
    goto/32 :goto_34

    nop

    :goto_f
    invoke-virtual {p2, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    goto/32 :goto_3a

    nop

    :goto_10
    iget-object v4, p0, Lw2/h;->i:Lu2/f;

    goto/32 :goto_13

    nop

    :goto_11
    invoke-virtual {p2, v0}, Lw2/g;->v(Lw2/v;)Z

    move-result p2

    goto/32 :goto_3

    nop

    :goto_12
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_8

    nop

    :goto_13
    iget v5, p0, Lw2/h;->l:I

    goto/32 :goto_7

    nop

    :goto_14
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_6

    nop

    :goto_15
    iget-object v3, p0, Lw2/h;->x:Lu2/f;

    goto/32 :goto_10

    nop

    :goto_16
    throw p1

    :goto_17
    goto/32 :goto_b

    nop

    :goto_18
    move-object v0, p2

    goto/32 :goto_1c

    nop

    :goto_19
    iget v3, p0, Lw2/h;->l:I

    goto/32 :goto_4f

    nop

    :goto_1a
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    goto/32 :goto_32

    nop

    :goto_1b
    iget-object p2, p0, Lw2/h;->a:Lw2/g;

    goto/32 :goto_56

    nop

    :goto_1c
    move-object v7, v1

    :goto_1d
    goto/32 :goto_f

    nop

    :goto_1e
    iget-object p2, p0, Lw2/h;->x:Lu2/f;

    goto/32 :goto_1

    nop

    :goto_1f
    invoke-interface {v0, v2, p2, v3, v4}, Lu2/l;->b(Landroid/content/Context;Lw2/v;II)Lw2/v;

    move-result-object v2

    goto/32 :goto_35

    nop

    :goto_20
    iget-object p2, p0, Lw2/h;->f:Lw2/h$d;

    goto/32 :goto_21

    nop

    :goto_21
    invoke-virtual {p2, p1, v10, v0}, Lw2/h$d;->d(Lu2/f;Lu2/k;Lw2/u;)V

    goto/32 :goto_52

    nop

    :goto_22
    sget-object p2, Lu2/c;->c:Lu2/c;

    :goto_23
    goto/32 :goto_5

    nop

    :goto_24
    new-instance p1, Lw2/x;

    goto/32 :goto_44

    nop

    :goto_25
    invoke-direct {p1, p2, v1}, Lw2/d;-><init>(Lu2/f;Lu2/f;)V

    :goto_26
    goto/32 :goto_55

    nop

    :goto_27
    invoke-virtual {v3, v1, p1, p2}, Lw2/j;->d(ZLu2/a;Lu2/c;)Z

    move-result p1

    goto/32 :goto_e

    nop

    :goto_28
    move-object v0, v2

    goto/32 :goto_45

    nop

    :goto_29
    iget-object p2, p0, Lw2/h;->a:Lw2/g;

    goto/32 :goto_11

    nop

    :goto_2a
    invoke-interface {v0}, Lw2/v;->get()Ljava/lang/Object;

    move-result-object p2

    goto/32 :goto_3e

    nop

    :goto_2b
    throw p1

    :goto_2c
    goto/32 :goto_4c

    nop

    :goto_2d
    move-object v1, p1

    goto/32 :goto_43

    nop

    :goto_2e
    invoke-virtual {v1, v2}, Lw2/g;->x(Lu2/f;)Z

    move-result v1

    goto/32 :goto_47

    nop

    :goto_2f
    invoke-virtual {p2}, Lw2/g;->b()Lx2/b;

    move-result-object v2

    goto/32 :goto_15

    nop

    :goto_30
    iget-object v9, p0, Lw2/h;->o:Lu2/h;

    goto/32 :goto_2d

    nop

    :goto_31
    if-ne p1, v0, :cond_3

    goto/32 :goto_46

    :cond_3
    goto/32 :goto_51

    nop

    :goto_32
    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_2b

    nop

    :goto_33
    iget-object v1, p0, Lw2/h;->a:Lw2/g;

    goto/32 :goto_49

    nop

    :goto_34
    if-nez v10, :cond_4

    goto/32 :goto_53

    :cond_4
    goto/32 :goto_57

    nop

    :goto_35
    move-object v7, v0

    goto/32 :goto_28

    nop

    :goto_36
    const/4 v1, 0x0

    goto/32 :goto_31

    nop

    :goto_37
    new-instance p1, Lcom/bumptech/glide/i$d;

    goto/32 :goto_2a

    nop

    :goto_38
    invoke-interface {p2}, Lw2/v;->a()V

    :goto_39
    goto/32 :goto_29

    nop

    :goto_3a
    if-eqz v2, :cond_5

    goto/32 :goto_39

    :cond_5
    goto/32 :goto_38

    nop

    :goto_3b
    xor-int/2addr v1, v2

    goto/32 :goto_4d

    nop

    :goto_3c
    goto/16 :goto_26

    :goto_3d
    goto/32 :goto_40

    nop

    :goto_3e
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p2

    goto/32 :goto_50

    nop

    :goto_3f
    invoke-interface {p2}, Lw2/v;->get()Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_d

    nop

    :goto_40
    new-instance p1, Ljava/lang/IllegalArgumentException;

    goto/32 :goto_12

    nop

    :goto_41
    iget-object p2, p0, Lw2/h;->o:Lu2/h;

    goto/32 :goto_48

    nop

    :goto_42
    if-ne p1, v2, :cond_6

    goto/32 :goto_2c

    :cond_6
    goto/32 :goto_54

    nop

    :goto_43
    invoke-direct/range {v1 .. v9}, Lw2/x;-><init>(Lx2/b;Lu2/f;Lu2/f;IILu2/l;Ljava/lang/Class;Lu2/h;)V

    goto/32 :goto_3c

    nop

    :goto_44
    iget-object p2, p0, Lw2/h;->a:Lw2/g;

    goto/32 :goto_2f

    nop

    :goto_45
    goto/16 :goto_1d

    :goto_46
    goto/32 :goto_18

    nop

    :goto_47
    const/4 v2, 0x1

    goto/32 :goto_3b

    nop

    :goto_48
    invoke-interface {v1, p2}, Lu2/k;->b(Lu2/h;)Lu2/c;

    move-result-object p2

    goto/32 :goto_9

    nop

    :goto_49
    iget-object v2, p0, Lw2/h;->x:Lu2/f;

    goto/32 :goto_2e

    nop

    :goto_4a
    invoke-virtual {v0, v8}, Lw2/g;->r(Ljava/lang/Class;)Lu2/l;

    move-result-object v0

    goto/32 :goto_4

    nop

    :goto_4b
    const-string v1, "Unknown strategy: "

    goto/32 :goto_14

    nop

    :goto_4c
    new-instance p1, Lw2/d;

    goto/32 :goto_1e

    nop

    :goto_4d
    iget-object v3, p0, Lw2/h;->n:Lw2/j;

    goto/32 :goto_27

    nop

    :goto_4e
    invoke-virtual {p2}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    goto/32 :goto_0

    nop

    :goto_4f
    iget v4, p0, Lw2/h;->m:I

    goto/32 :goto_1f

    nop

    :goto_50
    invoke-direct {p1, p2}, Lcom/bumptech/glide/i$d;-><init>(Ljava/lang/Class;)V

    goto/32 :goto_16

    nop

    :goto_51
    iget-object v0, p0, Lw2/h;->a:Lw2/g;

    goto/32 :goto_4a

    nop

    :goto_52
    goto/16 :goto_17

    :goto_53
    goto/32 :goto_37

    nop

    :goto_54
    const/4 v1, 0x2

    goto/32 :goto_c

    nop

    :goto_55
    invoke-static {v0}, Lw2/u;->f(Lw2/v;)Lw2/u;

    move-result-object v0

    goto/32 :goto_20

    nop

    :goto_56
    invoke-virtual {p2, v0}, Lw2/g;->n(Lw2/v;)Lu2/k;

    move-result-object v1

    goto/32 :goto_41

    nop

    :goto_57
    sget-object p1, Lw2/h$a;->c:[I

    goto/32 :goto_4e

    nop
.end method

.method w(Z)V
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    invoke-virtual {v0, p1}, Lw2/h$f;->d(Z)Z

    move-result p1

    goto/32 :goto_5

    nop

    :goto_1
    iget-object v0, p0, Lw2/h;->g:Lw2/h$f;

    goto/32 :goto_0

    nop

    :goto_2
    return-void

    :goto_3
    invoke-direct {p0}, Lw2/h;->x()V

    :goto_4
    goto/32 :goto_2

    nop

    :goto_5
    if-nez p1, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_3

    nop
.end method
