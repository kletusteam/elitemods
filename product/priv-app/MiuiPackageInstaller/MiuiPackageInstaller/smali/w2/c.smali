.class Lw2/c;
.super Ljava/lang/Object;

# interfaces
.implements Lw2/f;
.implements Lcom/bumptech/glide/load/data/d$a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lw2/f;",
        "Lcom/bumptech/glide/load/data/d$a<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lu2/f;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lw2/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lw2/g<",
            "*>;"
        }
    .end annotation
.end field

.field private final c:Lw2/f$a;

.field private d:I

.field private e:Lu2/f;

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "La3/n<",
            "Ljava/io/File;",
            "*>;>;"
        }
    .end annotation
.end field

.field private g:I

.field private volatile h:La3/n$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La3/n$a<",
            "*>;"
        }
    .end annotation
.end field

.field private i:Ljava/io/File;


# direct methods
.method constructor <init>(Ljava/util/List;Lw2/g;Lw2/f$a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lu2/f;",
            ">;",
            "Lw2/g<",
            "*>;",
            "Lw2/f$a;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lw2/c;->d:I

    iput-object p1, p0, Lw2/c;->a:Ljava/util/List;

    iput-object p2, p0, Lw2/c;->b:Lw2/g;

    iput-object p3, p0, Lw2/c;->c:Lw2/f$a;

    return-void
.end method

.method constructor <init>(Lw2/g;Lw2/f$a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lw2/g<",
            "*>;",
            "Lw2/f$a;",
            ")V"
        }
    .end annotation

    invoke-virtual {p1}, Lw2/g;->c()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0, p1, p2}, Lw2/c;-><init>(Ljava/util/List;Lw2/g;Lw2/f$a;)V

    return-void
.end method

.method private a()Z
    .locals 2

    iget v0, p0, Lw2/c;->g:I

    iget-object v1, p0, Lw2/c;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method


# virtual methods
.method public c(Ljava/lang/Exception;)V
    .locals 4

    iget-object v0, p0, Lw2/c;->c:Lw2/f$a;

    iget-object v1, p0, Lw2/c;->e:Lu2/f;

    iget-object v2, p0, Lw2/c;->h:La3/n$a;

    iget-object v2, v2, La3/n$a;->c:Lcom/bumptech/glide/load/data/d;

    sget-object v3, Lu2/a;->c:Lu2/a;

    invoke-interface {v0, v1, p1, v2, v3}, Lw2/f$a;->c(Lu2/f;Ljava/lang/Exception;Lcom/bumptech/glide/load/data/d;Lu2/a;)V

    return-void
.end method

.method public cancel()V
    .locals 1

    iget-object v0, p0, Lw2/c;->h:La3/n$a;

    if-eqz v0, :cond_0

    iget-object v0, v0, La3/n$a;->c:Lcom/bumptech/glide/load/data/d;

    invoke-interface {v0}, Lcom/bumptech/glide/load/data/d;->cancel()V

    :cond_0
    return-void
.end method

.method public d(Ljava/lang/Object;)V
    .locals 6

    iget-object v0, p0, Lw2/c;->c:Lw2/f$a;

    iget-object v1, p0, Lw2/c;->e:Lu2/f;

    iget-object v2, p0, Lw2/c;->h:La3/n$a;

    iget-object v3, v2, La3/n$a;->c:Lcom/bumptech/glide/load/data/d;

    sget-object v4, Lu2/a;->c:Lu2/a;

    iget-object v5, p0, Lw2/c;->e:Lu2/f;

    move-object v2, p1

    invoke-interface/range {v0 .. v5}, Lw2/f$a;->a(Lu2/f;Ljava/lang/Object;Lcom/bumptech/glide/load/data/d;Lu2/a;Lu2/f;)V

    return-void
.end method

.method public e()Z
    .locals 7

    :cond_0
    :goto_0
    iget-object v0, p0, Lw2/c;->f:Ljava/util/List;

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_4

    invoke-direct {p0}, Lw2/c;->a()Z

    move-result v0

    if-nez v0, :cond_1

    goto :goto_2

    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lw2/c;->h:La3/n$a;

    :cond_2
    :goto_1
    if-nez v1, :cond_3

    invoke-direct {p0}, Lw2/c;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lw2/c;->f:Ljava/util/List;

    iget v3, p0, Lw2/c;->g:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lw2/c;->g:I

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, La3/n;

    iget-object v3, p0, Lw2/c;->i:Ljava/io/File;

    iget-object v4, p0, Lw2/c;->b:Lw2/g;

    invoke-virtual {v4}, Lw2/g;->s()I

    move-result v4

    iget-object v5, p0, Lw2/c;->b:Lw2/g;

    invoke-virtual {v5}, Lw2/g;->f()I

    move-result v5

    iget-object v6, p0, Lw2/c;->b:Lw2/g;

    invoke-virtual {v6}, Lw2/g;->k()Lu2/h;

    move-result-object v6

    invoke-interface {v0, v3, v4, v5, v6}, La3/n;->a(Ljava/lang/Object;IILu2/h;)La3/n$a;

    move-result-object v0

    iput-object v0, p0, Lw2/c;->h:La3/n$a;

    iget-object v0, p0, Lw2/c;->h:La3/n$a;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lw2/c;->b:Lw2/g;

    iget-object v3, p0, Lw2/c;->h:La3/n$a;

    iget-object v3, v3, La3/n$a;->c:Lcom/bumptech/glide/load/data/d;

    invoke-interface {v3}, Lcom/bumptech/glide/load/data/d;->a()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v0, v3}, Lw2/g;->t(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lw2/c;->h:La3/n$a;

    iget-object v0, v0, La3/n$a;->c:Lcom/bumptech/glide/load/data/d;

    iget-object v1, p0, Lw2/c;->b:Lw2/g;

    invoke-virtual {v1}, Lw2/g;->l()Lcom/bumptech/glide/g;

    move-result-object v1

    invoke-interface {v0, v1, p0}, Lcom/bumptech/glide/load/data/d;->f(Lcom/bumptech/glide/g;Lcom/bumptech/glide/load/data/d$a;)V

    move v1, v2

    goto :goto_1

    :cond_3
    return v1

    :cond_4
    :goto_2
    iget v0, p0, Lw2/c;->d:I

    add-int/2addr v0, v2

    iput v0, p0, Lw2/c;->d:I

    iget-object v2, p0, Lw2/c;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lt v0, v2, :cond_5

    return v1

    :cond_5
    iget-object v0, p0, Lw2/c;->a:Ljava/util/List;

    iget v2, p0, Lw2/c;->d:I

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lu2/f;

    new-instance v2, Lw2/d;

    iget-object v3, p0, Lw2/c;->b:Lw2/g;

    invoke-virtual {v3}, Lw2/g;->o()Lu2/f;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Lw2/d;-><init>(Lu2/f;Lu2/f;)V

    iget-object v3, p0, Lw2/c;->b:Lw2/g;

    invoke-virtual {v3}, Lw2/g;->d()Ly2/a;

    move-result-object v3

    invoke-interface {v3, v2}, Ly2/a;->b(Lu2/f;)Ljava/io/File;

    move-result-object v2

    iput-object v2, p0, Lw2/c;->i:Ljava/io/File;

    if-eqz v2, :cond_0

    iput-object v0, p0, Lw2/c;->e:Lu2/f;

    iget-object v0, p0, Lw2/c;->b:Lw2/g;

    invoke-virtual {v0, v2}, Lw2/g;->j(Ljava/io/File;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lw2/c;->f:Ljava/util/List;

    iput v1, p0, Lw2/c;->g:I

    goto/16 :goto_0
.end method
