.class Lw2/z;
.super Ljava/lang/Object;

# interfaces
.implements Lw2/f;
.implements Lw2/f$a;


# instance fields
.field private final a:Lw2/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lw2/g<",
            "*>;"
        }
    .end annotation
.end field

.field private final b:Lw2/f$a;

.field private c:I

.field private d:Lw2/c;

.field private e:Ljava/lang/Object;

.field private volatile f:La3/n$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La3/n$a<",
            "*>;"
        }
    .end annotation
.end field

.field private g:Lw2/d;


# direct methods
.method constructor <init>(Lw2/g;Lw2/f$a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lw2/g<",
            "*>;",
            "Lw2/f$a;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lw2/z;->a:Lw2/g;

    iput-object p2, p0, Lw2/z;->b:Lw2/f$a;

    return-void
.end method

.method private d(Ljava/lang/Object;)V
    .locals 8

    const-string v0, "SourceGenerator"

    invoke-static {}, Lq3/f;->b()J

    move-result-wide v1

    :try_start_0
    iget-object v3, p0, Lw2/z;->a:Lw2/g;

    invoke-virtual {v3, p1}, Lw2/g;->p(Ljava/lang/Object;)Lu2/d;

    move-result-object v3

    new-instance v4, Lw2/e;

    iget-object v5, p0, Lw2/z;->a:Lw2/g;

    invoke-virtual {v5}, Lw2/g;->k()Lu2/h;

    move-result-object v5

    invoke-direct {v4, v3, p1, v5}, Lw2/e;-><init>(Lu2/d;Ljava/lang/Object;Lu2/h;)V

    new-instance v5, Lw2/d;

    iget-object v6, p0, Lw2/z;->f:La3/n$a;

    iget-object v6, v6, La3/n$a;->a:Lu2/f;

    iget-object v7, p0, Lw2/z;->a:Lw2/g;

    invoke-virtual {v7}, Lw2/g;->o()Lu2/f;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lw2/d;-><init>(Lu2/f;Lu2/f;)V

    iput-object v5, p0, Lw2/z;->g:Lw2/d;

    iget-object v5, p0, Lw2/z;->a:Lw2/g;

    invoke-virtual {v5}, Lw2/g;->d()Ly2/a;

    move-result-object v5

    iget-object v6, p0, Lw2/z;->g:Lw2/d;

    invoke-interface {v5, v6, v4}, Ly2/a;->a(Lu2/f;Ly2/a$b;)V

    const/4 v4, 0x2

    invoke-static {v0, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Finished encoding source to cache, key: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, p0, Lw2/z;->g:Lw2/d;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v5, ", data: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, ", encoder: "

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, ", duration: "

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v1, v2}, Lq3/f;->a(J)D

    move-result-wide v1

    invoke-virtual {v4, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    iget-object p1, p0, Lw2/z;->f:La3/n$a;

    iget-object p1, p1, La3/n$a;->c:Lcom/bumptech/glide/load/data/d;

    invoke-interface {p1}, Lcom/bumptech/glide/load/data/d;->b()V

    new-instance p1, Lw2/c;

    iget-object v0, p0, Lw2/z;->f:La3/n$a;

    iget-object v0, v0, La3/n$a;->a:Lu2/f;

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lw2/z;->a:Lw2/g;

    invoke-direct {p1, v0, v1, p0}, Lw2/c;-><init>(Ljava/util/List;Lw2/g;Lw2/f$a;)V

    iput-object p1, p0, Lw2/z;->d:Lw2/c;

    return-void

    :catchall_0
    move-exception p1

    iget-object v0, p0, Lw2/z;->f:La3/n$a;

    iget-object v0, v0, La3/n$a;->c:Lcom/bumptech/glide/load/data/d;

    invoke-interface {v0}, Lcom/bumptech/glide/load/data/d;->b()V

    throw p1
.end method

.method private f()Z
    .locals 2

    iget v0, p0, Lw2/z;->c:I

    iget-object v1, p0, Lw2/z;->a:Lw2/g;

    invoke-virtual {v1}, Lw2/g;->g()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private j(La3/n$a;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "La3/n$a<",
            "*>;)V"
        }
    .end annotation

    iget-object v0, p0, Lw2/z;->f:La3/n$a;

    iget-object v0, v0, La3/n$a;->c:Lcom/bumptech/glide/load/data/d;

    iget-object v1, p0, Lw2/z;->a:Lw2/g;

    invoke-virtual {v1}, Lw2/g;->l()Lcom/bumptech/glide/g;

    move-result-object v1

    new-instance v2, Lw2/z$a;

    invoke-direct {v2, p0, p1}, Lw2/z$a;-><init>(Lw2/z;La3/n$a;)V

    invoke-interface {v0, v1, v2}, Lcom/bumptech/glide/load/data/d;->f(Lcom/bumptech/glide/g;Lcom/bumptech/glide/load/data/d$a;)V

    return-void
.end method


# virtual methods
.method public a(Lu2/f;Ljava/lang/Object;Lcom/bumptech/glide/load/data/d;Lu2/a;Lu2/f;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lu2/f;",
            "Ljava/lang/Object;",
            "Lcom/bumptech/glide/load/data/d<",
            "*>;",
            "Lu2/a;",
            "Lu2/f;",
            ")V"
        }
    .end annotation

    iget-object v0, p0, Lw2/z;->b:Lw2/f$a;

    iget-object p4, p0, Lw2/z;->f:La3/n$a;

    iget-object p4, p4, La3/n$a;->c:Lcom/bumptech/glide/load/data/d;

    invoke-interface {p4}, Lcom/bumptech/glide/load/data/d;->e()Lu2/a;

    move-result-object v4

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p1

    invoke-interface/range {v0 .. v5}, Lw2/f$a;->a(Lu2/f;Ljava/lang/Object;Lcom/bumptech/glide/load/data/d;Lu2/a;Lu2/f;)V

    return-void
.end method

.method public b()V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public c(Lu2/f;Ljava/lang/Exception;Lcom/bumptech/glide/load/data/d;Lu2/a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lu2/f;",
            "Ljava/lang/Exception;",
            "Lcom/bumptech/glide/load/data/d<",
            "*>;",
            "Lu2/a;",
            ")V"
        }
    .end annotation

    iget-object p4, p0, Lw2/z;->b:Lw2/f$a;

    iget-object v0, p0, Lw2/z;->f:La3/n$a;

    iget-object v0, v0, La3/n$a;->c:Lcom/bumptech/glide/load/data/d;

    invoke-interface {v0}, Lcom/bumptech/glide/load/data/d;->e()Lu2/a;

    move-result-object v0

    invoke-interface {p4, p1, p2, p3, v0}, Lw2/f$a;->c(Lu2/f;Ljava/lang/Exception;Lcom/bumptech/glide/load/data/d;Lu2/a;)V

    return-void
.end method

.method public cancel()V
    .locals 1

    iget-object v0, p0, Lw2/z;->f:La3/n$a;

    if-eqz v0, :cond_0

    iget-object v0, v0, La3/n$a;->c:Lcom/bumptech/glide/load/data/d;

    invoke-interface {v0}, Lcom/bumptech/glide/load/data/d;->cancel()V

    :cond_0
    return-void
.end method

.method public e()Z
    .locals 5

    iget-object v0, p0, Lw2/z;->e:Ljava/lang/Object;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iput-object v1, p0, Lw2/z;->e:Ljava/lang/Object;

    invoke-direct {p0, v0}, Lw2/z;->d(Ljava/lang/Object;)V

    :cond_0
    iget-object v0, p0, Lw2/z;->d:Lw2/c;

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lw2/c;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    return v2

    :cond_1
    iput-object v1, p0, Lw2/z;->d:Lw2/c;

    iput-object v1, p0, Lw2/z;->f:La3/n$a;

    const/4 v0, 0x0

    :cond_2
    :goto_0
    if-nez v0, :cond_4

    invoke-direct {p0}, Lw2/z;->f()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lw2/z;->a:Lw2/g;

    invoke-virtual {v1}, Lw2/g;->g()Ljava/util/List;

    move-result-object v1

    iget v3, p0, Lw2/z;->c:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lw2/z;->c:I

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, La3/n$a;

    iput-object v1, p0, Lw2/z;->f:La3/n$a;

    iget-object v1, p0, Lw2/z;->f:La3/n$a;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lw2/z;->a:Lw2/g;

    invoke-virtual {v1}, Lw2/g;->e()Lw2/j;

    move-result-object v1

    iget-object v3, p0, Lw2/z;->f:La3/n$a;

    iget-object v3, v3, La3/n$a;->c:Lcom/bumptech/glide/load/data/d;

    invoke-interface {v3}, Lcom/bumptech/glide/load/data/d;->e()Lu2/a;

    move-result-object v3

    invoke-virtual {v1, v3}, Lw2/j;->c(Lu2/a;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lw2/z;->a:Lw2/g;

    iget-object v3, p0, Lw2/z;->f:La3/n$a;

    iget-object v3, v3, La3/n$a;->c:Lcom/bumptech/glide/load/data/d;

    invoke-interface {v3}, Lcom/bumptech/glide/load/data/d;->a()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v1, v3}, Lw2/g;->t(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_3
    iget-object v0, p0, Lw2/z;->f:La3/n$a;

    invoke-direct {p0, v0}, Lw2/z;->j(La3/n$a;)V

    move v0, v2

    goto :goto_0

    :cond_4
    return v0
.end method

.method g(La3/n$a;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "La3/n$a<",
            "*>;)Z"
        }
    .end annotation

    goto/32 :goto_8

    nop

    :goto_0
    const/4 p1, 0x1

    goto/32 :goto_1

    nop

    :goto_1
    goto :goto_5

    :goto_2
    goto/32 :goto_4

    nop

    :goto_3
    if-nez v0, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_6

    nop

    :goto_4
    const/4 p1, 0x0

    :goto_5
    goto/32 :goto_7

    nop

    :goto_6
    if-eq v0, p1, :cond_1

    goto/32 :goto_2

    :cond_1
    goto/32 :goto_0

    nop

    :goto_7
    return p1

    :goto_8
    iget-object v0, p0, Lw2/z;->f:La3/n$a;

    goto/32 :goto_3

    nop
.end method

.method h(La3/n$a;Ljava/lang/Object;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "La3/n$a<",
            "*>;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    goto/32 :goto_e

    nop

    :goto_0
    return-void

    :goto_1
    iget-object p1, p0, Lw2/z;->b:Lw2/f$a;

    goto/32 :goto_d

    nop

    :goto_2
    goto :goto_13

    :goto_3
    goto/32 :goto_14

    nop

    :goto_4
    move-object v2, p2

    goto/32 :goto_12

    nop

    :goto_5
    iget-object v1, p1, La3/n$a;->a:Lu2/f;

    goto/32 :goto_c

    nop

    :goto_6
    iget-object v1, p1, La3/n$a;->c:Lcom/bumptech/glide/load/data/d;

    goto/32 :goto_f

    nop

    :goto_7
    invoke-virtual {v0}, Lw2/g;->e()Lw2/j;

    move-result-object v0

    goto/32 :goto_11

    nop

    :goto_8
    iget-object v5, p0, Lw2/z;->g:Lw2/d;

    goto/32 :goto_4

    nop

    :goto_9
    invoke-virtual {v0, v1}, Lw2/j;->c(Lu2/a;)Z

    move-result v0

    goto/32 :goto_b

    nop

    :goto_a
    iput-object p2, p0, Lw2/z;->e:Ljava/lang/Object;

    goto/32 :goto_1

    nop

    :goto_b
    if-nez v0, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_a

    nop

    :goto_c
    iget-object v3, p1, La3/n$a;->c:Lcom/bumptech/glide/load/data/d;

    goto/32 :goto_10

    nop

    :goto_d
    invoke-interface {p1}, Lw2/f$a;->b()V

    goto/32 :goto_2

    nop

    :goto_e
    iget-object v0, p0, Lw2/z;->a:Lw2/g;

    goto/32 :goto_7

    nop

    :goto_f
    invoke-interface {v1}, Lcom/bumptech/glide/load/data/d;->e()Lu2/a;

    move-result-object v1

    goto/32 :goto_9

    nop

    :goto_10
    invoke-interface {v3}, Lcom/bumptech/glide/load/data/d;->e()Lu2/a;

    move-result-object v4

    goto/32 :goto_8

    nop

    :goto_11
    if-nez p2, :cond_1

    goto/32 :goto_3

    :cond_1
    goto/32 :goto_6

    nop

    :goto_12
    invoke-interface/range {v0 .. v5}, Lw2/f$a;->a(Lu2/f;Ljava/lang/Object;Lcom/bumptech/glide/load/data/d;Lu2/a;Lu2/f;)V

    :goto_13
    goto/32 :goto_0

    nop

    :goto_14
    iget-object v0, p0, Lw2/z;->b:Lw2/f$a;

    goto/32 :goto_5

    nop
.end method

.method i(La3/n$a;Ljava/lang/Exception;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "La3/n$a<",
            "*>;",
            "Ljava/lang/Exception;",
            ")V"
        }
    .end annotation

    goto/32 :goto_4

    nop

    :goto_0
    iget-object v1, p0, Lw2/z;->g:Lw2/d;

    goto/32 :goto_2

    nop

    :goto_1
    return-void

    :goto_2
    iget-object p1, p1, La3/n$a;->c:Lcom/bumptech/glide/load/data/d;

    goto/32 :goto_5

    nop

    :goto_3
    invoke-interface {v0, v1, p2, p1, v2}, Lw2/f$a;->c(Lu2/f;Ljava/lang/Exception;Lcom/bumptech/glide/load/data/d;Lu2/a;)V

    goto/32 :goto_1

    nop

    :goto_4
    iget-object v0, p0, Lw2/z;->b:Lw2/f$a;

    goto/32 :goto_0

    nop

    :goto_5
    invoke-interface {p1}, Lcom/bumptech/glide/load/data/d;->e()Lu2/a;

    move-result-object v2

    goto/32 :goto_3

    nop
.end method
