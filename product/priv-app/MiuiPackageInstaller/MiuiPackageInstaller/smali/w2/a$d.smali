.class final Lw2/a$d;
.super Ljava/lang/ref/WeakReference;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lw2/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "d"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/ref/WeakReference<",
        "Lw2/p<",
        "*>;>;"
    }
.end annotation


# instance fields
.field final a:Lu2/f;

.field final b:Z

.field c:Lw2/v;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lw2/v<",
            "*>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lu2/f;Lw2/p;Ljava/lang/ref/ReferenceQueue;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lu2/f;",
            "Lw2/p<",
            "*>;",
            "Ljava/lang/ref/ReferenceQueue<",
            "-",
            "Lw2/p<",
            "*>;>;Z)V"
        }
    .end annotation

    invoke-direct {p0, p2, p3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;Ljava/lang/ref/ReferenceQueue;)V

    invoke-static {p1}, Lq3/j;->d(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lu2/f;

    iput-object p1, p0, Lw2/a$d;->a:Lu2/f;

    invoke-virtual {p2}, Lw2/p;->f()Z

    move-result p1

    if-eqz p1, :cond_0

    if-eqz p4, :cond_0

    invoke-virtual {p2}, Lw2/p;->d()Lw2/v;

    move-result-object p1

    invoke-static {p1}, Lq3/j;->d(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lw2/v;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput-object p1, p0, Lw2/a$d;->c:Lw2/v;

    invoke-virtual {p2}, Lw2/p;->f()Z

    move-result p1

    iput-boolean p1, p0, Lw2/a$d;->b:Z

    return-void
.end method


# virtual methods
.method a()V
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    const/4 v0, 0x0

    goto/32 :goto_2

    nop

    :goto_1
    return-void

    :goto_2
    iput-object v0, p0, Lw2/a$d;->c:Lw2/v;

    goto/32 :goto_3

    nop

    :goto_3
    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->clear()V

    goto/32 :goto_1

    nop
.end method
