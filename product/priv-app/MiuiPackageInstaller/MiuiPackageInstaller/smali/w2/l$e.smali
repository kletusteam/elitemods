.class final Lw2/l$e;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lw2/l;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "e"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable<",
        "Lw2/l$d;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lw2/l$d;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 2

    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    invoke-direct {p0, v0}, Lw2/l$e;-><init>(Ljava/util/List;)V

    return-void
.end method

.method constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lw2/l$d;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lw2/l$e;->a:Ljava/util/List;

    return-void
.end method

.method private static d(Lm3/g;)Lw2/l$d;
    .locals 2

    new-instance v0, Lw2/l$d;

    invoke-static {}, Lq3/e;->a()Ljava/util/concurrent/Executor;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lw2/l$d;-><init>(Lm3/g;Ljava/util/concurrent/Executor;)V

    return-object v0
.end method


# virtual methods
.method a(Lm3/g;Ljava/util/concurrent/Executor;)V
    .locals 2

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    iget-object v0, p0, Lw2/l$e;->a:Ljava/util/List;

    goto/32 :goto_3

    nop

    :goto_2
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/32 :goto_0

    nop

    :goto_3
    new-instance v1, Lw2/l$d;

    goto/32 :goto_4

    nop

    :goto_4
    invoke-direct {v1, p1, p2}, Lw2/l$d;-><init>(Lm3/g;Ljava/util/concurrent/Executor;)V

    goto/32 :goto_2

    nop
.end method

.method b(Lm3/g;)Z
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p1

    goto/32 :goto_3

    nop

    :goto_1
    invoke-static {p1}, Lw2/l$e;->d(Lm3/g;)Lw2/l$d;

    move-result-object p1

    goto/32 :goto_0

    nop

    :goto_2
    iget-object v0, p0, Lw2/l$e;->a:Ljava/util/List;

    goto/32 :goto_1

    nop

    :goto_3
    return p1
.end method

.method c()Lw2/l$e;
    .locals 3

    goto/32 :goto_0

    nop

    :goto_0
    new-instance v0, Lw2/l$e;

    goto/32 :goto_3

    nop

    :goto_1
    invoke-direct {v0, v1}, Lw2/l$e;-><init>(Ljava/util/List;)V

    goto/32 :goto_4

    nop

    :goto_2
    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    goto/32 :goto_1

    nop

    :goto_3
    new-instance v1, Ljava/util/ArrayList;

    goto/32 :goto_5

    nop

    :goto_4
    return-object v0

    :goto_5
    iget-object v2, p0, Lw2/l$e;->a:Ljava/util/List;

    goto/32 :goto_2

    nop
.end method

.method clear()V
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    invoke-interface {v0}, Ljava/util/List;->clear()V

    goto/32 :goto_2

    nop

    :goto_1
    iget-object v0, p0, Lw2/l$e;->a:Ljava/util/List;

    goto/32 :goto_0

    nop

    :goto_2
    return-void
.end method

.method e(Lm3/g;)V
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Lw2/l$e;->a:Ljava/util/List;

    goto/32 :goto_1

    nop

    :goto_1
    invoke-static {p1}, Lw2/l$e;->d(Lm3/g;)Lw2/l$d;

    move-result-object p1

    goto/32 :goto_2

    nop

    :goto_2
    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto/32 :goto_3

    nop

    :goto_3
    return-void
.end method

.method isEmpty()Z
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return v0

    :goto_1
    iget-object v0, p0, Lw2/l$e;->a:Ljava/util/List;

    goto/32 :goto_2

    nop

    :goto_2
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    goto/32 :goto_0

    nop
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "Lw2/l$d;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lw2/l$e;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method size()I
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Lw2/l$e;->a:Ljava/util/List;

    goto/32 :goto_1

    nop

    :goto_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto/32 :goto_2

    nop

    :goto_2
    return v0
.end method
