.class Lw2/h$d;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lw2/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "d"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Z:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private a:Lu2/f;

.field private b:Lu2/k;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lu2/k<",
            "TZ;>;"
        }
    .end annotation
.end field

.field private c:Lw2/u;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lw2/u<",
            "TZ;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method a()V
    .locals 1

    goto/32 :goto_3

    nop

    :goto_0
    iput-object v0, p0, Lw2/h$d;->b:Lu2/k;

    goto/32 :goto_2

    nop

    :goto_1
    iput-object v0, p0, Lw2/h$d;->a:Lu2/f;

    goto/32 :goto_0

    nop

    :goto_2
    iput-object v0, p0, Lw2/h$d;->c:Lw2/u;

    goto/32 :goto_4

    nop

    :goto_3
    const/4 v0, 0x0

    goto/32 :goto_1

    nop

    :goto_4
    return-void
.end method

.method b(Lw2/h$e;Lu2/h;)V
    .locals 4

    goto/32 :goto_8

    nop

    :goto_0
    iget-object p2, p0, Lw2/h$d;->c:Lw2/u;

    goto/32 :goto_7

    nop

    :goto_1
    iget-object p1, p0, Lw2/h$d;->c:Lw2/u;

    goto/32 :goto_6

    nop

    :goto_2
    invoke-static {v0}, Lr3/b;->a(Ljava/lang/String;)V

    :try_start_0
    invoke-interface {p1}, Lw2/h$e;->a()Ly2/a;

    move-result-object p1

    iget-object v0, p0, Lw2/h$d;->a:Lu2/f;

    new-instance v1, Lw2/e;

    iget-object v2, p0, Lw2/h$d;->b:Lu2/k;

    iget-object v3, p0, Lw2/h$d;->c:Lw2/u;

    invoke-direct {v1, v2, v3, p2}, Lw2/e;-><init>(Lu2/d;Ljava/lang/Object;Lu2/h;)V

    invoke-interface {p1, v0, v1}, Ly2/a;->a(Lu2/f;Ly2/a$b;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/32 :goto_1

    nop

    :goto_3
    throw p1

    :goto_4
    return-void

    :catchall_0
    move-exception p1

    goto/32 :goto_0

    nop

    :goto_5
    invoke-static {}, Lr3/b;->d()V

    goto/32 :goto_4

    nop

    :goto_6
    invoke-virtual {p1}, Lw2/u;->h()V

    goto/32 :goto_5

    nop

    :goto_7
    invoke-virtual {p2}, Lw2/u;->h()V

    goto/32 :goto_9

    nop

    :goto_8
    const-string v0, "DecodeJob.encode"

    goto/32 :goto_2

    nop

    :goto_9
    invoke-static {}, Lr3/b;->d()V

    goto/32 :goto_3

    nop
.end method

.method c()Z
    .locals 1

    goto/32 :goto_3

    nop

    :goto_0
    const/4 v0, 0x1

    goto/32 :goto_6

    nop

    :goto_1
    const/4 v0, 0x0

    :goto_2
    goto/32 :goto_5

    nop

    :goto_3
    iget-object v0, p0, Lw2/h$d;->c:Lw2/u;

    goto/32 :goto_4

    nop

    :goto_4
    if-nez v0, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_0

    nop

    :goto_5
    return v0

    :goto_6
    goto :goto_2

    :goto_7
    goto/32 :goto_1

    nop
.end method

.method d(Lu2/f;Lu2/k;Lw2/u;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<X:",
            "Ljava/lang/Object;",
            ">(",
            "Lu2/f;",
            "Lu2/k<",
            "TX;>;",
            "Lw2/u<",
            "TX;>;)V"
        }
    .end annotation

    goto/32 :goto_1

    nop

    :goto_0
    iput-object p2, p0, Lw2/h$d;->b:Lu2/k;

    goto/32 :goto_3

    nop

    :goto_1
    iput-object p1, p0, Lw2/h$d;->a:Lu2/f;

    goto/32 :goto_0

    nop

    :goto_2
    return-void

    :goto_3
    iput-object p3, p0, Lw2/h$d;->c:Lw2/u;

    goto/32 :goto_2

    nop
.end method
