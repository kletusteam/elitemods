.class Lw2/p;
.super Ljava/lang/Object;

# interfaces
.implements Lw2/v;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lw2/p$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Z:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lw2/v<",
        "TZ;>;"
    }
.end annotation


# instance fields
.field private final a:Z

.field private final b:Z

.field private final c:Lw2/v;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lw2/v<",
            "TZ;>;"
        }
    .end annotation
.end field

.field private final d:Lw2/p$a;

.field private final e:Lu2/f;

.field private f:I

.field private g:Z


# direct methods
.method constructor <init>(Lw2/v;ZZLu2/f;Lw2/p$a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lw2/v<",
            "TZ;>;ZZ",
            "Lu2/f;",
            "Lw2/p$a;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lq3/j;->d(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lw2/v;

    iput-object p1, p0, Lw2/p;->c:Lw2/v;

    iput-boolean p2, p0, Lw2/p;->a:Z

    iput-boolean p3, p0, Lw2/p;->b:Z

    iput-object p4, p0, Lw2/p;->e:Lu2/f;

    invoke-static {p5}, Lq3/j;->d(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lw2/p$a;

    iput-object p1, p0, Lw2/p;->d:Lw2/p$a;

    return-void
.end method


# virtual methods
.method public declared-synchronized a()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lw2/p;->f:I

    if-gtz v0, :cond_2

    iget-boolean v0, p0, Lw2/p;->g:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lw2/p;->g:Z

    iget-boolean v0, p0, Lw2/p;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lw2/p;->c:Lw2/v;

    invoke-interface {v0}, Lw2/v;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot recycle a resource that has already been recycled"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot recycle a resource while it is still acquired"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized b()V
    .locals 2

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :cond_0
    :try_start_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot acquire a recycled resource"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    goto/32 :goto_4

    nop

    :goto_1
    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, Lw2/p;->g:Z

    if-nez v0, :cond_0

    iget v0, p0, Lw2/p;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lw2/p;->f:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/32 :goto_3

    nop

    :goto_2
    throw v0

    :goto_3
    monitor-exit p0

    goto/32 :goto_0

    nop

    :goto_4
    monitor-exit p0

    goto/32 :goto_2

    nop
.end method

.method public c()I
    .locals 1

    iget-object v0, p0, Lw2/p;->c:Lw2/v;

    invoke-interface {v0}, Lw2/v;->c()I

    move-result v0

    return v0
.end method

.method d()Lw2/v;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lw2/v<",
            "TZ;>;"
        }
    .end annotation

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Lw2/p;->c:Lw2/v;

    goto/32 :goto_1

    nop

    :goto_1
    return-object v0
.end method

.method public e()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "TZ;>;"
        }
    .end annotation

    iget-object v0, p0, Lw2/p;->c:Lw2/v;

    invoke-interface {v0}, Lw2/v;->e()Ljava/lang/Class;

    move-result-object v0

    return-object v0
.end method

.method f()Z
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-boolean v0, p0, Lw2/p;->a:Z

    goto/32 :goto_1

    nop

    :goto_1
    return v0
.end method

.method g()V
    .locals 2

    goto/32 :goto_3

    nop

    :goto_0
    iget-object v0, p0, Lw2/p;->d:Lw2/p$a;

    goto/32 :goto_8

    nop

    :goto_1
    invoke-interface {v0, v1, p0}, Lw2/p$a;->d(Lu2/f;Lw2/p;)V

    :goto_2
    goto/32 :goto_6

    nop

    :goto_3
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lw2/p;->f:I

    if-lez v0, :cond_2

    const/4 v1, 0x1

    sub-int/2addr v0, v1

    iput v0, p0, Lw2/p;->f:I

    if-nez v0, :cond_0

    goto :goto_4

    :cond_0
    const/4 v1, 0x0

    :goto_4
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/32 :goto_5

    nop

    :goto_5
    if-nez v1, :cond_1

    goto/32 :goto_2

    :cond_1
    goto/32 :goto_0

    nop

    :goto_6
    return-void

    :cond_2
    :try_start_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot release a recycled or not yet acquired resource"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/32 :goto_7

    nop

    :goto_7
    throw v0

    :goto_8
    iget-object v1, p0, Lw2/p;->e:Lu2/f;

    goto/32 :goto_1

    nop
.end method

.method public get()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TZ;"
        }
    .end annotation

    iget-object v0, p0, Lw2/p;->c:Lw2/v;

    invoke-interface {v0}, Lw2/v;->get()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized toString()Ljava/lang/String;
    .locals 2

    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "EngineResource{isMemoryCacheable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lw2/p;->a:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", listener="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lw2/p;->d:Lw2/p$a;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", key="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lw2/p;->e:Lu2/f;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", acquired="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lw2/p;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", isRecycled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lw2/p;->g:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", resource="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lw2/p;->c:Lw2/v;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
