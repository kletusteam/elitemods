.class final Lw2/g;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Transcode:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "La3/n$a<",
            "*>;>;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lu2/f;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcom/bumptech/glide/d;

.field private d:Ljava/lang/Object;

.field private e:I

.field private f:I

.field private g:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation
.end field

.field private h:Lw2/h$e;

.field private i:Lu2/h;

.field private j:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lu2/l<",
            "*>;>;"
        }
    .end annotation
.end field

.field private k:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "TTranscode;>;"
        }
    .end annotation
.end field

.field private l:Z

.field private m:Z

.field private n:Lu2/f;

.field private o:Lcom/bumptech/glide/g;

.field private p:Lw2/j;

.field private q:Z

.field private r:Z


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lw2/g;->a:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lw2/g;->b:Ljava/util/List;

    return-void
.end method


# virtual methods
.method a()V
    .locals 2

    goto/32 :goto_10

    nop

    :goto_0
    iput-object v0, p0, Lw2/g;->n:Lu2/f;

    goto/32 :goto_e

    nop

    :goto_1
    iput-object v0, p0, Lw2/g;->k:Ljava/lang/Class;

    goto/32 :goto_2

    nop

    :goto_2
    iput-object v0, p0, Lw2/g;->i:Lu2/h;

    goto/32 :goto_7

    nop

    :goto_3
    iput-object v0, p0, Lw2/g;->c:Lcom/bumptech/glide/d;

    goto/32 :goto_11

    nop

    :goto_4
    iput-boolean v0, p0, Lw2/g;->m:Z

    goto/32 :goto_9

    nop

    :goto_5
    iget-object v0, p0, Lw2/g;->a:Ljava/util/List;

    goto/32 :goto_f

    nop

    :goto_6
    iput-object v0, p0, Lw2/g;->j:Ljava/util/Map;

    goto/32 :goto_d

    nop

    :goto_7
    iput-object v0, p0, Lw2/g;->o:Lcom/bumptech/glide/g;

    goto/32 :goto_6

    nop

    :goto_8
    iget-object v1, p0, Lw2/g;->b:Ljava/util/List;

    goto/32 :goto_b

    nop

    :goto_9
    return-void

    :goto_a
    iput-boolean v0, p0, Lw2/g;->l:Z

    goto/32 :goto_8

    nop

    :goto_b
    invoke-interface {v1}, Ljava/util/List;->clear()V

    goto/32 :goto_4

    nop

    :goto_c
    const/4 v0, 0x0

    goto/32 :goto_a

    nop

    :goto_d
    iput-object v0, p0, Lw2/g;->p:Lw2/j;

    goto/32 :goto_5

    nop

    :goto_e
    iput-object v0, p0, Lw2/g;->g:Ljava/lang/Class;

    goto/32 :goto_1

    nop

    :goto_f
    invoke-interface {v0}, Ljava/util/List;->clear()V

    goto/32 :goto_c

    nop

    :goto_10
    const/4 v0, 0x0

    goto/32 :goto_3

    nop

    :goto_11
    iput-object v0, p0, Lw2/g;->d:Ljava/lang/Object;

    goto/32 :goto_0

    nop
.end method

.method b()Lx2/b;
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    return-object v0

    :goto_1
    invoke-virtual {v0}, Lcom/bumptech/glide/d;->b()Lx2/b;

    move-result-object v0

    goto/32 :goto_0

    nop

    :goto_2
    iget-object v0, p0, Lw2/g;->c:Lcom/bumptech/glide/d;

    goto/32 :goto_1

    nop
.end method

.method c()Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lu2/f;",
            ">;"
        }
    .end annotation

    goto/32 :goto_12

    nop

    :goto_0
    iget-object v6, v4, La3/n$a;->a:Lu2/f;

    goto/32 :goto_24

    nop

    :goto_1
    return-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/List;->clear()V

    goto/32 :goto_14

    nop

    :goto_3
    iget-object v6, v4, La3/n$a;->a:Lu2/f;

    goto/32 :goto_10

    nop

    :goto_4
    const/4 v0, 0x1

    goto/32 :goto_1a

    nop

    :goto_5
    if-lt v5, v6, :cond_0

    goto/32 :goto_22

    :cond_0
    goto/32 :goto_2b

    nop

    :goto_6
    if-lt v3, v1, :cond_1

    goto/32 :goto_16

    :cond_1
    goto/32 :goto_26

    nop

    :goto_7
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    goto/32 :goto_c

    nop

    :goto_8
    iget-object v5, p0, Lw2/g;->b:Ljava/util/List;

    goto/32 :goto_0

    nop

    :goto_9
    invoke-interface {v7, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    goto/32 :goto_27

    nop

    :goto_a
    iget-object v7, v4, La3/n$a;->b:Ljava/util/List;

    goto/32 :goto_9

    nop

    :goto_b
    invoke-interface {v7, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    goto/32 :goto_1c

    nop

    :goto_c
    const/4 v2, 0x0

    goto/32 :goto_1e

    nop

    :goto_d
    move v5, v2

    :goto_e
    goto/32 :goto_13

    nop

    :goto_f
    iget-object v7, v4, La3/n$a;->b:Ljava/util/List;

    goto/32 :goto_b

    nop

    :goto_10
    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_11
    goto/32 :goto_d

    nop

    :goto_12
    iget-boolean v0, p0, Lw2/g;->m:Z

    goto/32 :goto_2a

    nop

    :goto_13
    iget-object v6, v4, La3/n$a;->b:Ljava/util/List;

    goto/32 :goto_25

    nop

    :goto_14
    invoke-virtual {p0}, Lw2/g;->g()Ljava/util/List;

    move-result-object v0

    goto/32 :goto_7

    nop

    :goto_15
    goto :goto_1f

    :goto_16
    goto/32 :goto_18

    nop

    :goto_17
    add-int/lit8 v5, v5, 0x1

    goto/32 :goto_21

    nop

    :goto_18
    iget-object v0, p0, Lw2/g;->b:Ljava/util/List;

    goto/32 :goto_1

    nop

    :goto_19
    iget-object v6, p0, Lw2/g;->b:Ljava/util/List;

    goto/32 :goto_f

    nop

    :goto_1a
    iput-boolean v0, p0, Lw2/g;->m:Z

    goto/32 :goto_2c

    nop

    :goto_1b
    if-eqz v6, :cond_2

    goto/32 :goto_1d

    :cond_2
    goto/32 :goto_19

    nop

    :goto_1c
    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_1d
    goto/32 :goto_17

    nop

    :goto_1e
    move v3, v2

    :goto_1f
    goto/32 :goto_6

    nop

    :goto_20
    iget-object v5, p0, Lw2/g;->b:Ljava/util/List;

    goto/32 :goto_3

    nop

    :goto_21
    goto :goto_e

    :goto_22
    goto/32 :goto_23

    nop

    :goto_23
    add-int/lit8 v3, v3, 0x1

    goto/32 :goto_15

    nop

    :goto_24
    invoke-interface {v5, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    goto/32 :goto_29

    nop

    :goto_25
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    goto/32 :goto_5

    nop

    :goto_26
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    goto/32 :goto_28

    nop

    :goto_27
    invoke-interface {v6, v7}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v6

    goto/32 :goto_1b

    nop

    :goto_28
    check-cast v4, La3/n$a;

    goto/32 :goto_8

    nop

    :goto_29
    if-eqz v5, :cond_3

    goto/32 :goto_11

    :cond_3
    goto/32 :goto_20

    nop

    :goto_2a
    if-eqz v0, :cond_4

    goto/32 :goto_16

    :cond_4
    goto/32 :goto_4

    nop

    :goto_2b
    iget-object v6, p0, Lw2/g;->b:Ljava/util/List;

    goto/32 :goto_a

    nop

    :goto_2c
    iget-object v0, p0, Lw2/g;->b:Ljava/util/List;

    goto/32 :goto_2

    nop
.end method

.method d()Ly2/a;
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Lw2/g;->h:Lw2/h$e;

    goto/32 :goto_1

    nop

    :goto_1
    invoke-interface {v0}, Lw2/h$e;->a()Ly2/a;

    move-result-object v0

    goto/32 :goto_2

    nop

    :goto_2
    return-object v0
.end method

.method e()Lw2/j;
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Lw2/g;->p:Lw2/j;

    goto/32 :goto_1

    nop

    :goto_1
    return-object v0
.end method

.method f()I
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget v0, p0, Lw2/g;->f:I

    goto/32 :goto_1

    nop

    :goto_1
    return v0
.end method

.method g()Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "La3/n$a<",
            "*>;>;"
        }
    .end annotation

    goto/32 :goto_f

    nop

    :goto_0
    iget-object v7, p0, Lw2/g;->i:Lu2/h;

    goto/32 :goto_1b

    nop

    :goto_1
    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_2
    goto/32 :goto_1a

    nop

    :goto_3
    if-nez v3, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_10

    nop

    :goto_4
    iget-object v0, p0, Lw2/g;->a:Ljava/util/List;

    goto/32 :goto_6

    nop

    :goto_5
    invoke-virtual {v0, v1}, Lcom/bumptech/glide/i;->i(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto/32 :goto_18

    nop

    :goto_6
    invoke-interface {v0}, Ljava/util/List;->clear()V

    goto/32 :goto_14

    nop

    :goto_7
    check-cast v3, La3/n;

    goto/32 :goto_c

    nop

    :goto_8
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    :goto_9
    goto/32 :goto_1c

    nop

    :goto_a
    iget-object v1, p0, Lw2/g;->d:Ljava/lang/Object;

    goto/32 :goto_5

    nop

    :goto_b
    iget-object v0, p0, Lw2/g;->a:Ljava/util/List;

    goto/32 :goto_13

    nop

    :goto_c
    iget-object v4, p0, Lw2/g;->d:Ljava/lang/Object;

    goto/32 :goto_12

    nop

    :goto_d
    const/4 v0, 0x1

    goto/32 :goto_e

    nop

    :goto_e
    iput-boolean v0, p0, Lw2/g;->l:Z

    goto/32 :goto_4

    nop

    :goto_f
    iget-boolean v0, p0, Lw2/g;->l:Z

    goto/32 :goto_15

    nop

    :goto_10
    iget-object v4, p0, Lw2/g;->a:Ljava/util/List;

    goto/32 :goto_1

    nop

    :goto_11
    iget v6, p0, Lw2/g;->f:I

    goto/32 :goto_0

    nop

    :goto_12
    iget v5, p0, Lw2/g;->e:I

    goto/32 :goto_11

    nop

    :goto_13
    return-object v0

    :goto_14
    iget-object v0, p0, Lw2/g;->c:Lcom/bumptech/glide/d;

    goto/32 :goto_1d

    nop

    :goto_15
    if-eqz v0, :cond_1

    goto/32 :goto_17

    :cond_1
    goto/32 :goto_d

    nop

    :goto_16
    goto :goto_9

    :goto_17
    goto/32 :goto_b

    nop

    :goto_18
    const/4 v1, 0x0

    goto/32 :goto_8

    nop

    :goto_19
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    goto/32 :goto_7

    nop

    :goto_1a
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_16

    nop

    :goto_1b
    invoke-interface {v3, v4, v5, v6, v7}, La3/n;->a(Ljava/lang/Object;IILu2/h;)La3/n$a;

    move-result-object v3

    goto/32 :goto_3

    nop

    :goto_1c
    if-lt v1, v2, :cond_2

    goto/32 :goto_17

    :cond_2
    goto/32 :goto_19

    nop

    :goto_1d
    invoke-virtual {v0}, Lcom/bumptech/glide/d;->i()Lcom/bumptech/glide/i;

    move-result-object v0

    goto/32 :goto_a

    nop
.end method

.method h(Ljava/lang/Class;)Lw2/t;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Data:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class<",
            "TData;>;)",
            "Lw2/t<",
            "TData;*TTranscode;>;"
        }
    .end annotation

    goto/32 :goto_3

    nop

    :goto_0
    invoke-virtual {v0, p1, v1, v2}, Lcom/bumptech/glide/i;->h(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/Class;)Lw2/t;

    move-result-object p1

    goto/32 :goto_5

    nop

    :goto_1
    invoke-virtual {v0}, Lcom/bumptech/glide/d;->i()Lcom/bumptech/glide/i;

    move-result-object v0

    goto/32 :goto_2

    nop

    :goto_2
    iget-object v1, p0, Lw2/g;->g:Ljava/lang/Class;

    goto/32 :goto_4

    nop

    :goto_3
    iget-object v0, p0, Lw2/g;->c:Lcom/bumptech/glide/d;

    goto/32 :goto_1

    nop

    :goto_4
    iget-object v2, p0, Lw2/g;->k:Ljava/lang/Class;

    goto/32 :goto_0

    nop

    :goto_5
    return-object p1
.end method

.method i()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    goto/32 :goto_2

    nop

    :goto_0
    return-object v0

    :goto_1
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    goto/32 :goto_0

    nop

    :goto_2
    iget-object v0, p0, Lw2/g;->d:Ljava/lang/Object;

    goto/32 :goto_1

    nop
.end method

.method j(Ljava/io/File;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Ljava/util/List<",
            "La3/n<",
            "Ljava/io/File;",
            "*>;>;"
        }
    .end annotation

    goto/32 :goto_2

    nop

    :goto_0
    invoke-virtual {v0}, Lcom/bumptech/glide/d;->i()Lcom/bumptech/glide/i;

    move-result-object v0

    goto/32 :goto_1

    nop

    :goto_1
    invoke-virtual {v0, p1}, Lcom/bumptech/glide/i;->i(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    goto/32 :goto_3

    nop

    :goto_2
    iget-object v0, p0, Lw2/g;->c:Lcom/bumptech/glide/d;

    goto/32 :goto_0

    nop

    :goto_3
    return-object p1
.end method

.method k()Lu2/h;
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return-object v0

    :goto_1
    iget-object v0, p0, Lw2/g;->i:Lu2/h;

    goto/32 :goto_0

    nop
.end method

.method l()Lcom/bumptech/glide/g;
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return-object v0

    :goto_1
    iget-object v0, p0, Lw2/g;->o:Lcom/bumptech/glide/g;

    goto/32 :goto_0

    nop
.end method

.method m()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Class<",
            "*>;>;"
        }
    .end annotation

    goto/32 :goto_1

    nop

    :goto_0
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    goto/32 :goto_4

    nop

    :goto_1
    iget-object v0, p0, Lw2/g;->c:Lcom/bumptech/glide/d;

    goto/32 :goto_3

    nop

    :goto_2
    iget-object v1, p0, Lw2/g;->d:Ljava/lang/Object;

    goto/32 :goto_0

    nop

    :goto_3
    invoke-virtual {v0}, Lcom/bumptech/glide/d;->i()Lcom/bumptech/glide/i;

    move-result-object v0

    goto/32 :goto_2

    nop

    :goto_4
    iget-object v2, p0, Lw2/g;->g:Ljava/lang/Class;

    goto/32 :goto_6

    nop

    :goto_5
    invoke-virtual {v0, v1, v2, v3}, Lcom/bumptech/glide/i;->j(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v0

    goto/32 :goto_7

    nop

    :goto_6
    iget-object v3, p0, Lw2/g;->k:Ljava/lang/Class;

    goto/32 :goto_5

    nop

    :goto_7
    return-object v0
.end method

.method n(Lw2/v;)Lu2/k;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Z:",
            "Ljava/lang/Object;",
            ">(",
            "Lw2/v<",
            "TZ;>;)",
            "Lu2/k<",
            "TZ;>;"
        }
    .end annotation

    goto/32 :goto_3

    nop

    :goto_0
    invoke-virtual {v0, p1}, Lcom/bumptech/glide/i;->k(Lw2/v;)Lu2/k;

    move-result-object p1

    goto/32 :goto_1

    nop

    :goto_1
    return-object p1

    :goto_2
    invoke-virtual {v0}, Lcom/bumptech/glide/d;->i()Lcom/bumptech/glide/i;

    move-result-object v0

    goto/32 :goto_0

    nop

    :goto_3
    iget-object v0, p0, Lw2/g;->c:Lcom/bumptech/glide/d;

    goto/32 :goto_2

    nop
.end method

.method o()Lu2/f;
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Lw2/g;->n:Lu2/f;

    goto/32 :goto_1

    nop

    :goto_1
    return-object v0
.end method

.method p(Ljava/lang/Object;)Lu2/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<X:",
            "Ljava/lang/Object;",
            ">(TX;)",
            "Lu2/d<",
            "TX;>;"
        }
    .end annotation

    goto/32 :goto_1

    nop

    :goto_0
    invoke-virtual {v0, p1}, Lcom/bumptech/glide/i;->m(Ljava/lang/Object;)Lu2/d;

    move-result-object p1

    goto/32 :goto_3

    nop

    :goto_1
    iget-object v0, p0, Lw2/g;->c:Lcom/bumptech/glide/d;

    goto/32 :goto_2

    nop

    :goto_2
    invoke-virtual {v0}, Lcom/bumptech/glide/d;->i()Lcom/bumptech/glide/i;

    move-result-object v0

    goto/32 :goto_0

    nop

    :goto_3
    return-object p1
.end method

.method q()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Lw2/g;->k:Ljava/lang/Class;

    goto/32 :goto_1

    nop

    :goto_1
    return-object v0
.end method

.method r(Ljava/lang/Class;)Lu2/l;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Z:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class<",
            "TZ;>;)",
            "Lu2/l<",
            "TZ;>;"
        }
    .end annotation

    goto/32 :goto_14

    nop

    :goto_0
    const-string v2, "Missing transformation for "

    goto/32 :goto_1b

    nop

    :goto_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_2

    nop

    :goto_2
    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_21

    nop

    :goto_3
    if-eqz v0, :cond_0

    goto/32 :goto_16

    :cond_0
    goto/32 :goto_15

    nop

    :goto_4
    iget-boolean v0, p0, Lw2/g;->q:Z

    goto/32 :goto_3

    nop

    :goto_5
    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    goto/32 :goto_e

    nop

    :goto_6
    if-nez v3, :cond_1

    goto/32 :goto_f

    :cond_1
    goto/32 :goto_10

    nop

    :goto_7
    return-object p1

    :goto_8
    goto/32 :goto_d

    nop

    :goto_9
    check-cast v3, Ljava/lang/Class;

    goto/32 :goto_c

    nop

    :goto_a
    check-cast v0, Lu2/l;

    goto/32 :goto_17

    nop

    :goto_b
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto/32 :goto_13

    nop

    :goto_c
    invoke-virtual {v3, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v3

    goto/32 :goto_6

    nop

    :goto_d
    return-object v0

    :goto_e
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_f
    goto/32 :goto_18

    nop

    :goto_10
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_23

    nop

    :goto_11
    iget-object v1, p0, Lw2/g;->j:Ljava/util/Map;

    goto/32 :goto_5

    nop

    :goto_12
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_1

    nop

    :goto_13
    const-string p1, ". If you wish to ignore unknown resource types, use the optional transformation methods."

    goto/32 :goto_12

    nop

    :goto_14
    iget-object v0, p0, Lw2/g;->j:Ljava/util/Map;

    goto/32 :goto_25

    nop

    :goto_15
    goto :goto_22

    :goto_16
    goto/32 :goto_29

    nop

    :goto_17
    if-eqz v0, :cond_2

    goto/32 :goto_24

    :cond_2
    goto/32 :goto_11

    nop

    :goto_18
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    goto/32 :goto_28

    nop

    :goto_19
    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    goto/32 :goto_1f

    nop

    :goto_1a
    iget-object v0, p0, Lw2/g;->j:Ljava/util/Map;

    goto/32 :goto_19

    nop

    :goto_1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_b

    nop

    :goto_1c
    check-cast v2, Ljava/util/Map$Entry;

    goto/32 :goto_27

    nop

    :goto_1d
    if-eqz v0, :cond_3

    goto/32 :goto_8

    :cond_3
    goto/32 :goto_1a

    nop

    :goto_1e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_0

    nop

    :goto_1f
    if-nez v0, :cond_4

    goto/32 :goto_22

    :cond_4
    goto/32 :goto_4

    nop

    :goto_20
    new-instance v1, Ljava/lang/StringBuilder;

    goto/32 :goto_1e

    nop

    :goto_21
    throw v0

    :goto_22
    goto/32 :goto_2a

    nop

    :goto_23
    check-cast v0, Lu2/l;

    :goto_24
    goto/32 :goto_1d

    nop

    :goto_25
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_a

    nop

    :goto_26
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_1c

    nop

    :goto_27
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    goto/32 :goto_9

    nop

    :goto_28
    if-nez v2, :cond_5

    goto/32 :goto_24

    :cond_5
    goto/32 :goto_26

    nop

    :goto_29
    new-instance v0, Ljava/lang/IllegalArgumentException;

    goto/32 :goto_20

    nop

    :goto_2a
    invoke-static {}, Lc3/c;->c()Lc3/c;

    move-result-object p1

    goto/32 :goto_7

    nop
.end method

.method s()I
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget v0, p0, Lw2/g;->e:I

    goto/32 :goto_1

    nop

    :goto_1
    return v0
.end method

.method t(Ljava/lang/Class;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "*>;)Z"
        }
    .end annotation

    goto/32 :goto_5

    nop

    :goto_0
    if-nez p1, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_4

    nop

    :goto_1
    return p1

    :goto_2
    goto :goto_7

    :goto_3
    goto/32 :goto_6

    nop

    :goto_4
    const/4 p1, 0x1

    goto/32 :goto_2

    nop

    :goto_5
    invoke-virtual {p0, p1}, Lw2/g;->h(Ljava/lang/Class;)Lw2/t;

    move-result-object p1

    goto/32 :goto_0

    nop

    :goto_6
    const/4 p1, 0x0

    :goto_7
    goto/32 :goto_1

    nop
.end method

.method u(Lcom/bumptech/glide/d;Ljava/lang/Object;Lu2/f;IILw2/j;Ljava/lang/Class;Ljava/lang/Class;Lcom/bumptech/glide/g;Lu2/h;Ljava/util/Map;ZZLw2/h$e;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/bumptech/glide/d;",
            "Ljava/lang/Object;",
            "Lu2/f;",
            "II",
            "Lw2/j;",
            "Ljava/lang/Class<",
            "*>;",
            "Ljava/lang/Class<",
            "TR;>;",
            "Lcom/bumptech/glide/g;",
            "Lu2/h;",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lu2/l<",
            "*>;>;ZZ",
            "Lw2/h$e;",
            ")V"
        }
    .end annotation

    goto/32 :goto_e

    nop

    :goto_0
    iput-object p8, p0, Lw2/g;->k:Ljava/lang/Class;

    goto/32 :goto_4

    nop

    :goto_1
    iput-boolean p12, p0, Lw2/g;->q:Z

    goto/32 :goto_b

    nop

    :goto_2
    iput p4, p0, Lw2/g;->e:I

    goto/32 :goto_8

    nop

    :goto_3
    iput-object p11, p0, Lw2/g;->j:Ljava/util/Map;

    goto/32 :goto_1

    nop

    :goto_4
    iput-object p9, p0, Lw2/g;->o:Lcom/bumptech/glide/g;

    goto/32 :goto_5

    nop

    :goto_5
    iput-object p10, p0, Lw2/g;->i:Lu2/h;

    goto/32 :goto_3

    nop

    :goto_6
    return-void

    :goto_7
    iput-object p14, p0, Lw2/g;->h:Lw2/h$e;

    goto/32 :goto_0

    nop

    :goto_8
    iput p5, p0, Lw2/g;->f:I

    goto/32 :goto_9

    nop

    :goto_9
    iput-object p6, p0, Lw2/g;->p:Lw2/j;

    goto/32 :goto_d

    nop

    :goto_a
    iput-object p3, p0, Lw2/g;->n:Lu2/f;

    goto/32 :goto_2

    nop

    :goto_b
    iput-boolean p13, p0, Lw2/g;->r:Z

    goto/32 :goto_6

    nop

    :goto_c
    iput-object p2, p0, Lw2/g;->d:Ljava/lang/Object;

    goto/32 :goto_a

    nop

    :goto_d
    iput-object p7, p0, Lw2/g;->g:Ljava/lang/Class;

    goto/32 :goto_7

    nop

    :goto_e
    iput-object p1, p0, Lw2/g;->c:Lcom/bumptech/glide/d;

    goto/32 :goto_c

    nop
.end method

.method v(Lw2/v;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lw2/v<",
            "*>;)Z"
        }
    .end annotation

    goto/32 :goto_3

    nop

    :goto_0
    invoke-virtual {v0, p1}, Lcom/bumptech/glide/i;->n(Lw2/v;)Z

    move-result p1

    goto/32 :goto_1

    nop

    :goto_1
    return p1

    :goto_2
    invoke-virtual {v0}, Lcom/bumptech/glide/d;->i()Lcom/bumptech/glide/i;

    move-result-object v0

    goto/32 :goto_0

    nop

    :goto_3
    iget-object v0, p0, Lw2/g;->c:Lcom/bumptech/glide/d;

    goto/32 :goto_2

    nop
.end method

.method w()Z
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return v0

    :goto_1
    iget-boolean v0, p0, Lw2/g;->r:Z

    goto/32 :goto_0

    nop
.end method

.method x(Lu2/f;)Z
    .locals 5

    goto/32 :goto_7

    nop

    :goto_0
    invoke-interface {v4, p1}, Lu2/f;->equals(Ljava/lang/Object;)Z

    move-result v4

    goto/32 :goto_3

    nop

    :goto_1
    goto :goto_f

    :goto_2
    goto/32 :goto_6

    nop

    :goto_3
    if-nez v4, :cond_0

    goto/32 :goto_9

    :cond_0
    goto/32 :goto_d

    nop

    :goto_4
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    goto/32 :goto_10

    nop

    :goto_5
    if-lt v3, v1, :cond_1

    goto/32 :goto_2

    :cond_1
    goto/32 :goto_a

    nop

    :goto_6
    return v2

    :goto_7
    invoke-virtual {p0}, Lw2/g;->g()Ljava/util/List;

    move-result-object v0

    goto/32 :goto_4

    nop

    :goto_8
    return p1

    :goto_9
    goto/32 :goto_11

    nop

    :goto_a
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    goto/32 :goto_c

    nop

    :goto_b
    iget-object v4, v4, La3/n$a;->a:Lu2/f;

    goto/32 :goto_0

    nop

    :goto_c
    check-cast v4, La3/n$a;

    goto/32 :goto_b

    nop

    :goto_d
    const/4 p1, 0x1

    goto/32 :goto_8

    nop

    :goto_e
    move v3, v2

    :goto_f
    goto/32 :goto_5

    nop

    :goto_10
    const/4 v2, 0x0

    goto/32 :goto_e

    nop

    :goto_11
    add-int/lit8 v3, v3, 0x1

    goto/32 :goto_1

    nop
.end method
