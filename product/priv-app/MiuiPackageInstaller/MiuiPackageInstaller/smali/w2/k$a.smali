.class Lw2/k$a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lw2/k;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "a"
.end annotation


# instance fields
.field final a:Lw2/h$e;

.field final b:Le0/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Le0/e<",
            "Lw2/h<",
            "*>;>;"
        }
    .end annotation
.end field

.field private c:I


# direct methods
.method constructor <init>(Lw2/h$e;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lw2/k$a$a;

    invoke-direct {v0, p0}, Lw2/k$a$a;-><init>(Lw2/k$a;)V

    const/16 v1, 0x96

    invoke-static {v1, v0}, Lr3/a;->d(ILr3/a$d;)Le0/e;

    move-result-object v0

    iput-object v0, p0, Lw2/k$a;->b:Le0/e;

    iput-object p1, p0, Lw2/k$a;->a:Lw2/h$e;

    return-void
.end method


# virtual methods
.method a(Lcom/bumptech/glide/d;Ljava/lang/Object;Lw2/n;Lu2/f;IILjava/lang/Class;Ljava/lang/Class;Lcom/bumptech/glide/g;Lw2/j;Ljava/util/Map;ZZZLu2/h;Lw2/h$b;)Lw2/h;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/bumptech/glide/d;",
            "Ljava/lang/Object;",
            "Lw2/n;",
            "Lu2/f;",
            "II",
            "Ljava/lang/Class<",
            "*>;",
            "Ljava/lang/Class<",
            "TR;>;",
            "Lcom/bumptech/glide/g;",
            "Lw2/j;",
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lu2/l<",
            "*>;>;ZZZ",
            "Lu2/h;",
            "Lw2/h$b<",
            "TR;>;)",
            "Lw2/h<",
            "TR;>;"
        }
    .end annotation

    goto/32 :goto_5

    nop

    :goto_0
    move/from16 v15, p14

    goto/32 :goto_c

    nop

    :goto_1
    move/from16 v7, p6

    goto/32 :goto_10

    nop

    :goto_2
    return-object v1

    :goto_3
    move-object/from16 v3, p2

    goto/32 :goto_16

    nop

    :goto_4
    move-object/from16 v9, p8

    goto/32 :goto_14

    nop

    :goto_5
    move-object/from16 v0, p0

    goto/32 :goto_1d

    nop

    :goto_6
    move-object/from16 p1, v1

    goto/32 :goto_d

    nop

    :goto_7
    move/from16 v13, p12

    goto/32 :goto_e

    nop

    :goto_8
    invoke-static {v1}, Lq3/j;->d(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_18

    nop

    :goto_9
    invoke-virtual/range {v1 .. v18}, Lw2/h;->n(Lcom/bumptech/glide/d;Ljava/lang/Object;Lw2/n;Lu2/f;IILjava/lang/Class;Ljava/lang/Class;Lcom/bumptech/glide/g;Lw2/j;Ljava/util/Map;ZZZLu2/h;Lw2/h$b;I)Lw2/h;

    move-result-object v1

    goto/32 :goto_2

    nop

    :goto_a
    move-object/from16 v12, p11

    goto/32 :goto_7

    nop

    :goto_b
    move-object/from16 v11, p10

    goto/32 :goto_a

    nop

    :goto_c
    move-object/from16 v16, p15

    goto/32 :goto_15

    nop

    :goto_d
    iget v1, v0, Lw2/k$a;->c:I

    goto/32 :goto_17

    nop

    :goto_e
    move/from16 v14, p13

    goto/32 :goto_0

    nop

    :goto_f
    iget-object v1, v0, Lw2/k$a;->b:Le0/e;

    goto/32 :goto_11

    nop

    :goto_10
    move-object/from16 v8, p7

    goto/32 :goto_4

    nop

    :goto_11
    invoke-interface {v1}, Le0/e;->b()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_1b

    nop

    :goto_12
    move/from16 v6, p5

    goto/32 :goto_1

    nop

    :goto_13
    move-object/from16 v1, p1

    goto/32 :goto_9

    nop

    :goto_14
    move-object/from16 v10, p9

    goto/32 :goto_b

    nop

    :goto_15
    move-object/from16 v17, p16

    goto/32 :goto_f

    nop

    :goto_16
    move-object/from16 v4, p3

    goto/32 :goto_1a

    nop

    :goto_17
    move/from16 v18, v1

    goto/32 :goto_1c

    nop

    :goto_18
    check-cast v1, Lw2/h;

    goto/32 :goto_6

    nop

    :goto_19
    iput v1, v0, Lw2/k$a;->c:I

    goto/32 :goto_13

    nop

    :goto_1a
    move-object/from16 v5, p4

    goto/32 :goto_12

    nop

    :goto_1b
    check-cast v1, Lw2/h;

    goto/32 :goto_8

    nop

    :goto_1c
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_19

    nop

    :goto_1d
    move-object/from16 v2, p1

    goto/32 :goto_3

    nop
.end method
