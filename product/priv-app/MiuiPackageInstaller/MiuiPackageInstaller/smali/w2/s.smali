.class final Lw2/s;
.super Ljava/lang/Object;


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lu2/f;",
            "Lw2/l<",
            "*>;>;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lu2/f;",
            "Lw2/l<",
            "*>;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lw2/s;->a:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lw2/s;->b:Ljava/util/Map;

    return-void
.end method

.method private b(Z)Ljava/util/Map;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/Map<",
            "Lu2/f;",
            "Lw2/l<",
            "*>;>;"
        }
    .end annotation

    if-eqz p1, :cond_0

    iget-object p1, p0, Lw2/s;->b:Ljava/util/Map;

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lw2/s;->a:Ljava/util/Map;

    :goto_0
    return-object p1
.end method


# virtual methods
.method a(Lu2/f;Z)Lw2/l;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lu2/f;",
            "Z)",
            "Lw2/l<",
            "*>;"
        }
    .end annotation

    goto/32 :goto_0

    nop

    :goto_0
    invoke-direct {p0, p2}, Lw2/s;->b(Z)Ljava/util/Map;

    move-result-object p2

    goto/32 :goto_2

    nop

    :goto_1
    check-cast p1, Lw2/l;

    goto/32 :goto_3

    nop

    :goto_2
    invoke-interface {p2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    goto/32 :goto_1

    nop

    :goto_3
    return-object p1
.end method

.method c(Lu2/f;Lw2/l;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lu2/f;",
            "Lw2/l<",
            "*>;)V"
        }
    .end annotation

    goto/32 :goto_0

    nop

    :goto_0
    invoke-virtual {p2}, Lw2/l;->p()Z

    move-result v0

    goto/32 :goto_2

    nop

    :goto_1
    return-void

    :goto_2
    invoke-direct {p0, v0}, Lw2/s;->b(Z)Ljava/util/Map;

    move-result-object v0

    goto/32 :goto_3

    nop

    :goto_3
    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/32 :goto_1

    nop
.end method

.method d(Lu2/f;Lw2/l;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lu2/f;",
            "Lw2/l<",
            "*>;)V"
        }
    .end annotation

    goto/32 :goto_0

    nop

    :goto_0
    invoke-virtual {p2}, Lw2/l;->p()Z

    move-result v0

    goto/32 :goto_5

    nop

    :goto_1
    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_2
    goto/32 :goto_4

    nop

    :goto_3
    if-nez p2, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_1

    nop

    :goto_4
    return-void

    :goto_5
    invoke-direct {p0, v0}, Lw2/s;->b(Z)Ljava/util/Map;

    move-result-object v0

    goto/32 :goto_6

    nop

    :goto_6
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_7

    nop

    :goto_7
    invoke-virtual {p2, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p2

    goto/32 :goto_3

    nop
.end method
