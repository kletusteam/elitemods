.class public final Lm7/b;
.super Ljava/lang/Object;

# interfaces
.implements Lgc/f;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lm7/b$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lgc/f<",
        "TT;",
        "Lpb/a0;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lm7/b$a;

.field private static final b:Lpb/v;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lm7/b$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lm7/b$a;-><init>(Lm8/g;)V

    sput-object v0, Lm7/b;->a:Lm7/b$a;

    sget-object v0, Lpb/v;->g:Lpb/v$a;

    const-string v1, "application/json; charset=UTF-8"

    invoke-virtual {v0, v1}, Lpb/v$a;->b(Ljava/lang/String;)Lpb/v;

    move-result-object v0

    sput-object v0, Lm7/b;->b:Lpb/v;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    invoke-virtual {p0, p1}, Lm7/b;->b(Ljava/lang/Object;)Lpb/a0;

    move-result-object p1

    return-object p1
.end method

.method public b(Ljava/lang/Object;)Lpb/a0;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Lpb/a0;"
        }
    .end annotation

    sget-object v0, Lpb/a0;->a:Lpb/a0$a;

    sget-object v1, Lm7/b;->b:Lpb/v;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lpb/a0$a;->d(Lpb/v;Ljava/lang/String;)Lpb/a0;

    move-result-object p1

    return-object p1
.end method
