.class public Lb1/i;
.super Ljava/lang/Object;

# interfaces
.implements Lb1/m;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lb1/m<",
        "Landroid/graphics/PointF;",
        "Landroid/graphics/PointF;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lb1/b;

.field private final b:Lb1/b;


# direct methods
.method public constructor <init>(Lb1/b;Lb1/b;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lb1/i;->a:Lb1/b;

    iput-object p2, p0, Lb1/i;->b:Lb1/b;

    return-void
.end method


# virtual methods
.method public a()Ly0/a;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ly0/a<",
            "Landroid/graphics/PointF;",
            "Landroid/graphics/PointF;",
            ">;"
        }
    .end annotation

    new-instance v0, Ly0/m;

    iget-object v1, p0, Lb1/i;->a:Lb1/b;

    invoke-virtual {v1}, Lb1/b;->a()Ly0/a;

    move-result-object v1

    iget-object v2, p0, Lb1/i;->b:Lb1/b;

    invoke-virtual {v2}, Lb1/b;->a()Ly0/a;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ly0/m;-><init>(Ly0/a;Ly0/a;)V

    return-object v0
.end method

.method public b()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Li1/a<",
            "Landroid/graphics/PointF;",
            ">;>;"
        }
    .end annotation

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Cannot call getKeyframes on AnimatableSplitDimensionPathValue."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public c()Z
    .locals 1

    iget-object v0, p0, Lb1/i;->a:Lb1/b;

    invoke-virtual {v0}, Lb1/b;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lb1/i;->b:Lb1/b;

    invoke-virtual {v0}, Lb1/b;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
