.class public Lv1/i;
.super Ljava/lang/Object;

# interfaces
.implements Lv1/g$a;


# instance fields
.field private a:Ls1/d;

.field private b:Ljava/lang/String;

.field private c:Z


# direct methods
.method public constructor <init>(Ls1/d;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lv1/i;->c:Z

    iput-object p1, p0, Lv1/i;->a:Ls1/d;

    return-void
.end method

.method private d(Lv1/d;Z)V
    .locals 1

    iget-boolean v0, p0, Lv1/i;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lv1/i;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lv1/d;->c(Ljava/lang/String;)V

    const/4 p1, 0x0

    iput-boolean p1, p0, Lv1/i;->c:Z

    if-nez p2, :cond_0

    iget-object p1, p0, Lv1/i;->a:Ls1/d;

    invoke-virtual {p1}, Ls1/d;->c()V

    :cond_0
    return-void
.end method


# virtual methods
.method public a(Lv1/d;)V
    .locals 3

    invoke-static {}, La2/a;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lv1/d;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lv1/i;->b:Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lv1/i;->c:Z

    iget-object v0, p0, Lv1/i;->a:Ls1/d;

    invoke-virtual {v0}, Ls1/d;->w()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lz1/a;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "origin ip is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lv1/i;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " change to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lz1/a;->b(Ljava/lang/String;)V

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "]"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lv1/d;->c(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public b(Lv1/d;Ljava/lang/Throwable;)V
    .locals 0

    const/4 p2, 0x0

    invoke-direct {p0, p1, p2}, Lv1/i;->d(Lv1/d;Z)V

    return-void
.end method

.method public c(Lv1/d;Ljava/lang/Object;)V
    .locals 0

    const/4 p2, 0x1

    invoke-direct {p0, p1, p2}, Lv1/i;->d(Lv1/d;Z)V

    return-void
.end method
