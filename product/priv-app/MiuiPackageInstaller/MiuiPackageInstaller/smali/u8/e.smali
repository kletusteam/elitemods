.class final Lu8/e;
.super Ljava/lang/Object;

# interfaces
.implements Lt8/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lt8/f<",
        "Lr8/c;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/CharSequence;

.field private final b:I

.field private final c:I

.field private final d:Ll8/p;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ll8/p<",
            "Ljava/lang/CharSequence;",
            "Ljava/lang/Integer;",
            "La8/l<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/CharSequence;IILl8/p;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            "II",
            "Ll8/p<",
            "-",
            "Ljava/lang/CharSequence;",
            "-",
            "Ljava/lang/Integer;",
            "La8/l<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "getNextMatch"

    invoke-static {p4, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lu8/e;->a:Ljava/lang/CharSequence;

    iput p2, p0, Lu8/e;->b:I

    iput p3, p0, Lu8/e;->c:I

    iput-object p4, p0, Lu8/e;->d:Ll8/p;

    return-void
.end method

.method public static final synthetic b(Lu8/e;)Ll8/p;
    .locals 0

    iget-object p0, p0, Lu8/e;->d:Ll8/p;

    return-object p0
.end method

.method public static final synthetic c(Lu8/e;)Ljava/lang/CharSequence;
    .locals 0

    iget-object p0, p0, Lu8/e;->a:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public static final synthetic d(Lu8/e;)I
    .locals 0

    iget p0, p0, Lu8/e;->c:I

    return p0
.end method

.method public static final synthetic e(Lu8/e;)I
    .locals 0

    iget p0, p0, Lu8/e;->b:I

    return p0
.end method


# virtual methods
.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "Lr8/c;",
            ">;"
        }
    .end annotation

    new-instance v0, Lu8/e$a;

    invoke-direct {v0, p0}, Lu8/e$a;-><init>(Lu8/e;)V

    return-object v0
.end method
