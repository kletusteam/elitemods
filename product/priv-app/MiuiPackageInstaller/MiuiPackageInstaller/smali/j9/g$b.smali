.class Lj9/g$b;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lj9/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation


# instance fields
.field final a:Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentLinkedQueue<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field final b:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/Object;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field final c:Ljava/lang/Runnable;


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lj9/g$b;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lj9/g$b;->b:Ljava/util/concurrent/ConcurrentHashMap;

    new-instance v0, Lj9/g$b$a;

    invoke-direct {v0, p0}, Lj9/g$b$a;-><init>(Lj9/g$b;)V

    iput-object v0, p0, Lj9/g$b;->c:Ljava/lang/Runnable;

    return-void
.end method

.method synthetic constructor <init>(Lj9/g$a;)V
    .locals 0

    invoke-direct {p0}, Lj9/g$b;-><init>()V

    return-void
.end method


# virtual methods
.method varargs a(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;[",
            "Ljava/lang/Object;",
            ")TT;"
        }
    .end annotation

    goto/32 :goto_5

    nop

    :goto_0
    goto :goto_3

    :goto_1
    goto/32 :goto_a

    nop

    :goto_2
    invoke-static {p1, p2}, Lj9/g;->a(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    :goto_3
    goto/32 :goto_8

    nop

    :goto_4
    invoke-virtual {p1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto/32 :goto_0

    nop

    :goto_5
    iget-object v0, p0, Lj9/g$b;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    goto/32 :goto_9

    nop

    :goto_6
    iget-object p1, p0, Lj9/g$b;->b:Ljava/util/concurrent/ConcurrentHashMap;

    goto/32 :goto_4

    nop

    :goto_7
    if-nez v0, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_6

    nop

    :goto_8
    return-object v0

    :goto_9
    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_7

    nop

    :goto_a
    if-nez p1, :cond_1

    goto/32 :goto_3

    :cond_1
    goto/32 :goto_2

    nop
.end method

.method b(Ljava/lang/Object;)V
    .locals 3

    goto/32 :goto_2

    nop

    :goto_0
    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/32 :goto_8

    nop

    :goto_1
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_3

    nop

    :goto_2
    iget-object v0, p0, Lj9/g$b;->b:Ljava/util/concurrent/ConcurrentHashMap;

    goto/32 :goto_7

    nop

    :goto_3
    const-string v0, "ObjectPool.releaseObject handler is null! looper: "

    goto/32 :goto_1f

    nop

    :goto_4
    if-gt v0, v1, :cond_0

    goto/32 :goto_21

    :cond_0
    goto/32 :goto_d

    nop

    :goto_5
    const-wide/16 v1, 0x1388

    goto/32 :goto_0

    nop

    :goto_6
    new-instance p1, Ljava/lang/StringBuilder;

    goto/32 :goto_1

    nop

    :goto_7
    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    goto/32 :goto_e

    nop

    :goto_8
    goto/16 :goto_21

    :goto_9
    goto/32 :goto_6

    nop

    :goto_a
    const/16 v1, 0xa

    goto/32 :goto_4

    nop

    :goto_b
    return-void

    :goto_c
    iget-object v0, p0, Lj9/g$b;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    goto/32 :goto_1d

    nop

    :goto_d
    iget-object v0, p0, Lj9/g$b;->c:Ljava/lang/Runnable;

    goto/32 :goto_5

    nop

    :goto_e
    invoke-virtual {v0, p1, v1}, Ljava/util/concurrent/ConcurrentHashMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_15

    nop

    :goto_f
    invoke-virtual {p1, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto/32 :goto_c

    nop

    :goto_10
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto/32 :goto_11

    nop

    :goto_11
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_12

    nop

    :goto_12
    const-string v0, "miuix_anim"

    goto/32 :goto_17

    nop

    :goto_13
    iget-object v0, p0, Lj9/g$b;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    goto/32 :goto_16

    nop

    :goto_14
    iget-object v0, p0, Lj9/g$b;->c:Ljava/lang/Runnable;

    goto/32 :goto_f

    nop

    :goto_15
    if-nez v0, :cond_1

    goto/32 :goto_19

    :cond_1
    goto/32 :goto_18

    nop

    :goto_16
    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    goto/32 :goto_1b

    nop

    :goto_17
    invoke-static {v0, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/32 :goto_1e

    nop

    :goto_18
    return-void

    :goto_19
    goto/32 :goto_13

    nop

    :goto_1a
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    goto/32 :goto_10

    nop

    :goto_1b
    invoke-static {}, Lj9/g;->e()Landroid/os/Handler;

    move-result-object p1

    goto/32 :goto_1c

    nop

    :goto_1c
    if-nez p1, :cond_2

    goto/32 :goto_9

    :cond_2
    goto/32 :goto_14

    nop

    :goto_1d
    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->size()I

    move-result v0

    goto/32 :goto_a

    nop

    :goto_1e
    iget-object p1, p0, Lj9/g$b;->c:Ljava/lang/Runnable;

    goto/32 :goto_20

    nop

    :goto_1f
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_1a

    nop

    :goto_20
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    :goto_21
    goto/32 :goto_b

    nop
.end method

.method c()V
    .locals 2

    :goto_0
    goto/32 :goto_c

    nop

    :goto_1
    goto :goto_0

    :goto_2
    goto/32 :goto_4

    nop

    :goto_3
    if-eqz v0, :cond_0

    goto/32 :goto_b

    :cond_0
    goto/32 :goto_a

    nop

    :goto_4
    return-void

    :goto_5
    invoke-virtual {v1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto/32 :goto_1

    nop

    :goto_6
    if-gt v0, v1, :cond_1

    goto/32 :goto_2

    :cond_1
    goto/32 :goto_d

    nop

    :goto_7
    iget-object v1, p0, Lj9/g$b;->b:Ljava/util/concurrent/ConcurrentHashMap;

    goto/32 :goto_5

    nop

    :goto_8
    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->size()I

    move-result v0

    goto/32 :goto_e

    nop

    :goto_9
    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_3

    nop

    :goto_a
    goto :goto_2

    :goto_b
    goto/32 :goto_7

    nop

    :goto_c
    iget-object v0, p0, Lj9/g$b;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    goto/32 :goto_8

    nop

    :goto_d
    iget-object v0, p0, Lj9/g$b;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    goto/32 :goto_9

    nop

    :goto_e
    const/16 v1, 0xa

    goto/32 :goto_6

    nop
.end method
