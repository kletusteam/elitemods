.class public final enum Le5/b;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Le5/b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum b:Le5/b;

.field public static final enum c:Le5/b;

.field public static final enum d:Le5/b;

.field public static final enum e:Le5/b;

.field public static final enum f:Le5/b;

.field public static final enum g:Le5/b;

.field private static final synthetic h:[Le5/b;


# instance fields
.field private a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 14

    new-instance v0, Le5/b;

    const-string v1, "None"

    const/4 v2, 0x0

    const-string v3, " "

    invoke-direct {v0, v1, v2, v3}, Le5/b;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Le5/b;->b:Le5/b;

    new-instance v1, Le5/b;

    const-string v3, "Rollback"

    const/4 v4, 0x1

    const-string v5, " OR ROLLBACK "

    invoke-direct {v1, v3, v4, v5}, Le5/b;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Le5/b;->c:Le5/b;

    new-instance v3, Le5/b;

    const-string v5, "Abort"

    const/4 v6, 0x2

    const-string v7, " OR ABORT "

    invoke-direct {v3, v5, v6, v7}, Le5/b;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Le5/b;->d:Le5/b;

    new-instance v5, Le5/b;

    const-string v7, "Fail"

    const/4 v8, 0x3

    const-string v9, " OR FAIL "

    invoke-direct {v5, v7, v8, v9}, Le5/b;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Le5/b;->e:Le5/b;

    new-instance v7, Le5/b;

    const-string v9, "Ignore"

    const/4 v10, 0x4

    const-string v11, " OR IGNORE "

    invoke-direct {v7, v9, v10, v11}, Le5/b;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Le5/b;->f:Le5/b;

    new-instance v9, Le5/b;

    const-string v11, "Replace"

    const/4 v12, 0x5

    const-string v13, " OR REPLACE "

    invoke-direct {v9, v11, v12, v13}, Le5/b;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v9, Le5/b;->g:Le5/b;

    const/4 v11, 0x6

    new-array v11, v11, [Le5/b;

    aput-object v0, v11, v2

    aput-object v1, v11, v4

    aput-object v3, v11, v6

    aput-object v5, v11, v8

    aput-object v7, v11, v10

    aput-object v9, v11, v12

    sput-object v11, Le5/b;->h:[Le5/b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Le5/b;->a:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Le5/b;
    .locals 1

    const-class v0, Le5/b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Le5/b;

    return-object p0
.end method

.method public static values()[Le5/b;
    .locals 1

    sget-object v0, Le5/b;->h:[Le5/b;

    invoke-virtual {v0}, [Le5/b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Le5/b;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Le5/b;->a:Ljava/lang/String;

    return-object v0
.end method
