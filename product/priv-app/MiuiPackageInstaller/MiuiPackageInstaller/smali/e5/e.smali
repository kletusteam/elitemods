.class public Le5/e;
.super Le5/g;


# instance fields
.field public d:Lc5/b;


# direct methods
.method public constructor <init>(Le5/g;Lc5/b;)V
    .locals 1

    iget-object v0, p1, Le5/g;->a:Ljava/lang/String;

    iget-object p1, p1, Le5/g;->b:Ljava/lang/reflect/Field;

    invoke-direct {p0, v0, p1, p2}, Le5/e;-><init>(Ljava/lang/String;Ljava/lang/reflect/Field;Lc5/b;)V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/reflect/Field;Lc5/b;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Le5/g;-><init>(Ljava/lang/String;Ljava/lang/reflect/Field;)V

    iput-object p3, p0, Le5/e;->d:Lc5/b;

    return-void
.end method


# virtual methods
.method public a()Z
    .locals 2

    iget-object v0, p0, Le5/e;->d:Lc5/b;

    sget-object v1, Lc5/b;->a:Lc5/b;

    if-eq v0, v1, :cond_1

    sget-object v1, Lc5/b;->b:Lc5/b;

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public b()Z
    .locals 2

    iget-object v0, p0, Le5/e;->d:Lc5/b;

    sget-object v1, Lc5/b;->c:Lc5/b;

    if-eq v0, v1, :cond_1

    sget-object v1, Lc5/b;->d:Lc5/b;

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method
