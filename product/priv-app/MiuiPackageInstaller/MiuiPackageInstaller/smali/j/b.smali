.class abstract Lj/b;
.super Ljava/lang/Object;


# instance fields
.field final a:Landroid/content/Context;

.field private b:Ln/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ln/g<",
            "Lz/b;",
            "Landroid/view/MenuItem;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ln/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ln/g<",
            "Lz/c;",
            "Landroid/view/SubMenu;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lj/b;->a:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method final c(Landroid/view/MenuItem;)Landroid/view/MenuItem;
    .locals 2

    goto/32 :goto_c

    nop

    :goto_0
    if-eqz v1, :cond_0

    goto/32 :goto_9

    :cond_0
    goto/32 :goto_10

    nop

    :goto_1
    invoke-direct {p1, v1, v0}, Lj/c;-><init>(Landroid/content/Context;Lz/b;)V

    goto/32 :goto_5

    nop

    :goto_2
    iget-object v1, p0, Lj/b;->b:Ln/g;

    goto/32 :goto_0

    nop

    :goto_3
    check-cast p1, Landroid/view/MenuItem;

    goto/32 :goto_4

    nop

    :goto_4
    if-eqz p1, :cond_1

    goto/32 :goto_b

    :cond_1
    goto/32 :goto_13

    nop

    :goto_5
    iget-object v1, p0, Lj/b;->b:Ln/g;

    goto/32 :goto_a

    nop

    :goto_6
    invoke-virtual {v1, p1}, Ln/g;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    goto/32 :goto_3

    nop

    :goto_7
    iget-object v1, p0, Lj/b;->b:Ln/g;

    goto/32 :goto_6

    nop

    :goto_8
    iput-object v1, p0, Lj/b;->b:Ln/g;

    :goto_9
    goto/32 :goto_7

    nop

    :goto_a
    invoke-virtual {v1, v0, p1}, Ln/g;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_b
    goto/32 :goto_e

    nop

    :goto_c
    instance-of v0, p1, Lz/b;

    goto/32 :goto_f

    nop

    :goto_d
    invoke-direct {v1}, Ln/g;-><init>()V

    goto/32 :goto_8

    nop

    :goto_e
    return-object p1

    :goto_f
    if-nez v0, :cond_2

    goto/32 :goto_b

    :cond_2
    goto/32 :goto_11

    nop

    :goto_10
    new-instance v1, Ln/g;

    goto/32 :goto_d

    nop

    :goto_11
    move-object v0, p1

    goto/32 :goto_14

    nop

    :goto_12
    iget-object v1, p0, Lj/b;->a:Landroid/content/Context;

    goto/32 :goto_1

    nop

    :goto_13
    new-instance p1, Lj/c;

    goto/32 :goto_12

    nop

    :goto_14
    check-cast v0, Lz/b;

    goto/32 :goto_2

    nop
.end method

.method final d(Landroid/view/SubMenu;)Landroid/view/SubMenu;
    .locals 2

    goto/32 :goto_9

    nop

    :goto_0
    return-object v0

    :goto_1
    goto/32 :goto_10

    nop

    :goto_2
    iget-object v1, p0, Lj/b;->a:Landroid/content/Context;

    goto/32 :goto_8

    nop

    :goto_3
    iget-object v0, p0, Lj/b;->c:Ln/g;

    goto/32 :goto_4

    nop

    :goto_4
    if-eqz v0, :cond_0

    goto/32 :goto_c

    :cond_0
    goto/32 :goto_15

    nop

    :goto_5
    invoke-virtual {v0, p1}, Ln/g;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_7

    nop

    :goto_6
    invoke-direct {v0}, Ln/g;-><init>()V

    goto/32 :goto_b

    nop

    :goto_7
    check-cast v0, Landroid/view/SubMenu;

    goto/32 :goto_f

    nop

    :goto_8
    invoke-direct {v0, v1, p1}, Lj/f;-><init>(Landroid/content/Context;Lz/c;)V

    goto/32 :goto_14

    nop

    :goto_9
    instance-of v0, p1, Lz/c;

    goto/32 :goto_e

    nop

    :goto_a
    new-instance v0, Lj/f;

    goto/32 :goto_2

    nop

    :goto_b
    iput-object v0, p0, Lj/b;->c:Ln/g;

    :goto_c
    goto/32 :goto_11

    nop

    :goto_d
    check-cast p1, Lz/c;

    goto/32 :goto_3

    nop

    :goto_e
    if-nez v0, :cond_1

    goto/32 :goto_1

    :cond_1
    goto/32 :goto_d

    nop

    :goto_f
    if-eqz v0, :cond_2

    goto/32 :goto_13

    :cond_2
    goto/32 :goto_a

    nop

    :goto_10
    return-object p1

    :goto_11
    iget-object v0, p0, Lj/b;->c:Ln/g;

    goto/32 :goto_5

    nop

    :goto_12
    invoke-virtual {v1, p1, v0}, Ln/g;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_13
    goto/32 :goto_0

    nop

    :goto_14
    iget-object v1, p0, Lj/b;->c:Ln/g;

    goto/32 :goto_12

    nop

    :goto_15
    new-instance v0, Ln/g;

    goto/32 :goto_6

    nop
.end method

.method final e()V
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    return-void

    :goto_1
    if-nez v0, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_3

    nop

    :goto_2
    iget-object v0, p0, Lj/b;->b:Ln/g;

    goto/32 :goto_1

    nop

    :goto_3
    invoke-virtual {v0}, Ln/g;->clear()V

    :goto_4
    goto/32 :goto_6

    nop

    :goto_5
    if-nez v0, :cond_1

    goto/32 :goto_8

    :cond_1
    goto/32 :goto_7

    nop

    :goto_6
    iget-object v0, p0, Lj/b;->c:Ln/g;

    goto/32 :goto_5

    nop

    :goto_7
    invoke-virtual {v0}, Ln/g;->clear()V

    :goto_8
    goto/32 :goto_0

    nop
.end method

.method final f(I)V
    .locals 2

    goto/32 :goto_c

    nop

    :goto_0
    return-void

    :goto_1
    iget-object v1, p0, Lj/b;->b:Ln/g;

    goto/32 :goto_d

    nop

    :goto_2
    invoke-virtual {v1}, Ln/g;->size()I

    move-result v1

    goto/32 :goto_a

    nop

    :goto_3
    return-void

    :goto_4
    goto/32 :goto_7

    nop

    :goto_5
    if-eqz v0, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_3

    nop

    :goto_6
    check-cast v1, Lz/b;

    goto/32 :goto_11

    nop

    :goto_7
    const/4 v0, 0x0

    :goto_8
    goto/32 :goto_9

    nop

    :goto_9
    iget-object v1, p0, Lj/b;->b:Ln/g;

    goto/32 :goto_2

    nop

    :goto_a
    if-lt v0, v1, :cond_1

    goto/32 :goto_f

    :cond_1
    goto/32 :goto_1

    nop

    :goto_b
    invoke-virtual {v1, v0}, Ln/g;->k(I)Ljava/lang/Object;

    goto/32 :goto_12

    nop

    :goto_c
    iget-object v0, p0, Lj/b;->b:Ln/g;

    goto/32 :goto_5

    nop

    :goto_d
    invoke-virtual {v1, v0}, Ln/g;->i(I)Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_6

    nop

    :goto_e
    goto :goto_8

    :goto_f
    goto/32 :goto_0

    nop

    :goto_10
    if-eq v1, p1, :cond_2

    goto/32 :goto_13

    :cond_2
    goto/32 :goto_15

    nop

    :goto_11
    invoke-interface {v1}, Landroid/view/MenuItem;->getGroupId()I

    move-result v1

    goto/32 :goto_10

    nop

    :goto_12
    add-int/lit8 v0, v0, -0x1

    :goto_13
    goto/32 :goto_14

    nop

    :goto_14
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_e

    nop

    :goto_15
    iget-object v1, p0, Lj/b;->b:Ln/g;

    goto/32 :goto_b

    nop
.end method

.method final g(I)V
    .locals 2

    goto/32 :goto_6

    nop

    :goto_0
    check-cast v1, Lz/b;

    goto/32 :goto_d

    nop

    :goto_1
    goto :goto_11

    :goto_2
    goto/32 :goto_8

    nop

    :goto_3
    return-void

    :goto_4
    goto/32 :goto_10

    nop

    :goto_5
    invoke-virtual {p1, v0}, Ln/g;->k(I)Ljava/lang/Object;

    goto/32 :goto_14

    nop

    :goto_6
    iget-object v0, p0, Lj/b;->b:Ln/g;

    goto/32 :goto_c

    nop

    :goto_7
    iget-object p1, p0, Lj/b;->b:Ln/g;

    goto/32 :goto_5

    nop

    :goto_8
    return-void

    :goto_9
    if-eq v1, p1, :cond_0

    goto/32 :goto_15

    :cond_0
    goto/32 :goto_7

    nop

    :goto_a
    invoke-virtual {v1, v0}, Ln/g;->i(I)Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_0

    nop

    :goto_b
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_1

    nop

    :goto_c
    if-eqz v0, :cond_1

    goto/32 :goto_4

    :cond_1
    goto/32 :goto_3

    nop

    :goto_d
    invoke-interface {v1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    goto/32 :goto_9

    nop

    :goto_e
    if-lt v0, v1, :cond_2

    goto/32 :goto_2

    :cond_2
    goto/32 :goto_12

    nop

    :goto_f
    iget-object v1, p0, Lj/b;->b:Ln/g;

    goto/32 :goto_13

    nop

    :goto_10
    const/4 v0, 0x0

    :goto_11
    goto/32 :goto_f

    nop

    :goto_12
    iget-object v1, p0, Lj/b;->b:Ln/g;

    goto/32 :goto_a

    nop

    :goto_13
    invoke-virtual {v1}, Ln/g;->size()I

    move-result v1

    goto/32 :goto_e

    nop

    :goto_14
    goto :goto_2

    :goto_15
    goto/32 :goto_b

    nop
.end method
