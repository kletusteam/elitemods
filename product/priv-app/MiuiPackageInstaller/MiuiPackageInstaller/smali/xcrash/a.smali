.class Lxcrash/a;
.super Ljava/lang/Object;


# static fields
.field private static final c:Lxcrash/a;


# instance fields
.field private a:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private b:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lxcrash/a;

    invoke-direct {v0}, Lxcrash/a;-><init>()V

    sput-object v0, Lxcrash/a;->c:Lxcrash/a;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lxcrash/a;->a:Ljava/util/LinkedList;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lxcrash/a;->b:Z

    return-void
.end method

.method static synthetic a(Lxcrash/a;)Ljava/util/LinkedList;
    .locals 0

    iget-object p0, p0, Lxcrash/a;->a:Ljava/util/LinkedList;

    return-object p0
.end method

.method static synthetic b(Lxcrash/a;Z)Z
    .locals 0

    iput-boolean p1, p0, Lxcrash/a;->b:Z

    return p1
.end method

.method static d()Lxcrash/a;
    .locals 1

    sget-object v0, Lxcrash/a;->c:Lxcrash/a;

    return-object v0
.end method


# virtual methods
.method c()V
    .locals 2

    goto/32 :goto_a

    nop

    :goto_0
    check-cast v1, Landroid/app/Activity;

    goto/32 :goto_b

    nop

    :goto_1
    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    :goto_2
    goto/32 :goto_c

    nop

    :goto_3
    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_4
    goto/32 :goto_d

    nop

    :goto_5
    iget-object v0, p0, Lxcrash/a;->a:Ljava/util/LinkedList;

    goto/32 :goto_1

    nop

    :goto_6
    goto :goto_4

    :goto_7
    goto/32 :goto_5

    nop

    :goto_8
    if-nez v0, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_3

    nop

    :goto_9
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_0

    nop

    :goto_a
    iget-object v0, p0, Lxcrash/a;->a:Ljava/util/LinkedList;

    goto/32 :goto_8

    nop

    :goto_b
    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    goto/32 :goto_6

    nop

    :goto_c
    return-void

    :goto_d
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_e

    nop

    :goto_e
    if-nez v1, :cond_1

    goto/32 :goto_7

    :cond_1
    goto/32 :goto_9

    nop
.end method

.method e(Landroid/app/Application;)V
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    new-instance v0, Ljava/util/LinkedList;

    goto/32 :goto_4

    nop

    :goto_1
    invoke-virtual {p1, v0}, Landroid/app/Application;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    goto/32 :goto_2

    nop

    :goto_2
    return-void

    :goto_3
    invoke-direct {v0, p0}, Lxcrash/a$a;-><init>(Lxcrash/a;)V

    goto/32 :goto_1

    nop

    :goto_4
    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    goto/32 :goto_6

    nop

    :goto_5
    new-instance v0, Lxcrash/a$a;

    goto/32 :goto_3

    nop

    :goto_6
    iput-object v0, p0, Lxcrash/a;->a:Ljava/util/LinkedList;

    goto/32 :goto_5

    nop
.end method

.method f()Z
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-boolean v0, p0, Lxcrash/a;->b:Z

    goto/32 :goto_1

    nop

    :goto_1
    return v0
.end method
