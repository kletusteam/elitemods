.class public Ld1/b;
.super Ld1/a;


# instance fields
.field private final A:Landroid/graphics/RectF;

.field private B:Landroid/graphics/Paint;

.field private x:Ly0/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ly0/a<",
            "Ljava/lang/Float;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private final y:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ld1/a;",
            ">;"
        }
    .end annotation
.end field

.field private final z:Landroid/graphics/RectF;


# direct methods
.method public constructor <init>(Lcom/airbnb/lottie/a;Ld1/d;Ljava/util/List;Lv0/d;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/airbnb/lottie/a;",
            "Ld1/d;",
            "Ljava/util/List<",
            "Ld1/d;",
            ">;",
            "Lv0/d;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ld1/a;-><init>(Lcom/airbnb/lottie/a;Ld1/d;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ld1/b;->y:Ljava/util/List;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Ld1/b;->z:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Ld1/b;->A:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Ld1/b;->B:Landroid/graphics/Paint;

    invoke-virtual {p2}, Ld1/d;->s()Lb1/b;

    move-result-object p2

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lb1/b;->a()Ly0/a;

    move-result-object p2

    iput-object p2, p0, Ld1/b;->x:Ly0/a;

    invoke-virtual {p0, p2}, Ld1/a;->j(Ly0/a;)V

    iget-object p2, p0, Ld1/b;->x:Ly0/a;

    invoke-virtual {p2, p0}, Ly0/a;->a(Ly0/a$b;)V

    goto :goto_0

    :cond_0
    iput-object v0, p0, Ld1/b;->x:Ly0/a;

    :goto_0
    new-instance p2, Ln/d;

    invoke-virtual {p4}, Lv0/d;->j()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {p2, v1}, Ln/d;-><init>(I)V

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    sub-int/2addr v1, v2

    move-object v3, v0

    :goto_1
    const/4 v4, 0x0

    if-ltz v1, :cond_4

    invoke-interface {p3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ld1/d;

    invoke-static {v5, p1, p4}, Ld1/a;->v(Ld1/d;Lcom/airbnb/lottie/a;Lv0/d;)Ld1/a;

    move-result-object v6

    if-nez v6, :cond_1

    goto :goto_2

    :cond_1
    invoke-virtual {v6}, Ld1/a;->w()Ld1/d;

    move-result-object v7

    invoke-virtual {v7}, Ld1/d;->b()J

    move-result-wide v7

    invoke-virtual {p2, v7, v8, v6}, Ln/d;->j(JLjava/lang/Object;)V

    if-eqz v3, :cond_2

    invoke-virtual {v3, v6}, Ld1/a;->F(Ld1/a;)V

    move-object v3, v0

    goto :goto_2

    :cond_2
    iget-object v7, p0, Ld1/b;->y:Ljava/util/List;

    invoke-interface {v7, v4, v6}, Ljava/util/List;->add(ILjava/lang/Object;)V

    sget-object v4, Ld1/b$a;->a:[I

    invoke-virtual {v5}, Ld1/d;->f()Ld1/d$b;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Enum;->ordinal()I

    move-result v5

    aget v4, v4, v5

    if-eq v4, v2, :cond_3

    const/4 v5, 0x2

    if-eq v4, v5, :cond_3

    goto :goto_2

    :cond_3
    move-object v3, v6

    :goto_2
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    :cond_4
    :goto_3
    invoke-virtual {p2}, Ln/d;->m()I

    move-result p1

    if-ge v4, p1, :cond_7

    invoke-virtual {p2, v4}, Ln/d;->i(I)J

    move-result-wide p3

    invoke-virtual {p2, p3, p4}, Ln/d;->f(J)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ld1/a;

    if-nez p1, :cond_5

    goto :goto_4

    :cond_5
    invoke-virtual {p1}, Ld1/a;->w()Ld1/d;

    move-result-object p3

    invoke-virtual {p3}, Ld1/d;->h()J

    move-result-wide p3

    invoke-virtual {p2, p3, p4}, Ln/d;->f(J)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ld1/a;

    if-eqz p3, :cond_6

    invoke-virtual {p1, p3}, Ld1/a;->G(Ld1/a;)V

    :cond_6
    :goto_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    :cond_7
    return-void
.end method


# virtual methods
.method protected E(La1/e;ILjava/util/List;La1/e;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "La1/e;",
            "I",
            "Ljava/util/List<",
            "La1/e;",
            ">;",
            "La1/e;",
            ")V"
        }
    .end annotation

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ld1/b;->y:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Ld1/b;->y:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ld1/a;

    invoke-virtual {v1, p1, p2, p3, p4}, Ld1/a;->f(La1/e;ILjava/util/List;La1/e;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public H(F)V
    .locals 3

    invoke-super {p0, p1}, Ld1/a;->H(F)V

    iget-object v0, p0, Ld1/b;->x:Ly0/a;

    if-eqz v0, :cond_0

    iget-object p1, p0, Ld1/a;->n:Lcom/airbnb/lottie/a;

    invoke-virtual {p1}, Lcom/airbnb/lottie/a;->n()Lv0/d;

    move-result-object p1

    invoke-virtual {p1}, Lv0/d;->e()F

    move-result p1

    const v0, 0x3c23d70a    # 0.01f

    add-float/2addr p1, v0

    iget-object v0, p0, Ld1/a;->o:Ld1/d;

    invoke-virtual {v0}, Ld1/d;->a()Lv0/d;

    move-result-object v0

    invoke-virtual {v0}, Lv0/d;->o()F

    move-result v0

    iget-object v1, p0, Ld1/b;->x:Ly0/a;

    invoke-virtual {v1}, Ly0/a;->h()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget-object v2, p0, Ld1/a;->o:Ld1/d;

    invoke-virtual {v2}, Ld1/d;->a()Lv0/d;

    move-result-object v2

    invoke-virtual {v2}, Lv0/d;->h()F

    move-result v2

    mul-float/2addr v1, v2

    sub-float/2addr v1, v0

    div-float p1, v1, p1

    :cond_0
    iget-object v0, p0, Ld1/b;->x:Ly0/a;

    if-nez v0, :cond_1

    iget-object v0, p0, Ld1/a;->o:Ld1/d;

    invoke-virtual {v0}, Ld1/d;->p()F

    move-result v0

    sub-float/2addr p1, v0

    :cond_1
    iget-object v0, p0, Ld1/a;->o:Ld1/d;

    invoke-virtual {v0}, Ld1/d;->t()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_2

    iget-object v0, p0, Ld1/a;->o:Ld1/d;

    invoke-virtual {v0}, Ld1/d;->t()F

    move-result v0

    div-float/2addr p1, v0

    :cond_2
    iget-object v0, p0, Ld1/b;->y:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_3

    iget-object v1, p0, Ld1/b;->y:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ld1/a;

    invoke-virtual {v1, p1}, Ld1/a;->H(F)V

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_3
    return-void
.end method

.method public a(Landroid/graphics/RectF;Landroid/graphics/Matrix;Z)V
    .locals 3

    invoke-super {p0, p1, p2, p3}, Ld1/a;->a(Landroid/graphics/RectF;Landroid/graphics/Matrix;Z)V

    iget-object p2, p0, Ld1/b;->y:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    const/4 p3, 0x1

    sub-int/2addr p2, p3

    :goto_0
    if-ltz p2, :cond_0

    iget-object v0, p0, Ld1/b;->z:Landroid/graphics/RectF;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/graphics/RectF;->set(FFFF)V

    iget-object v0, p0, Ld1/b;->y:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ld1/a;

    iget-object v1, p0, Ld1/b;->z:Landroid/graphics/RectF;

    iget-object v2, p0, Ld1/a;->m:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1, v2, p3}, Ld1/a;->a(Landroid/graphics/RectF;Landroid/graphics/Matrix;Z)V

    iget-object v0, p0, Ld1/b;->z:Landroid/graphics/RectF;

    invoke-virtual {p1, v0}, Landroid/graphics/RectF;->union(Landroid/graphics/RectF;)V

    add-int/lit8 p2, p2, -0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public h(Ljava/lang/Object;Li1/c;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;",
            "Li1/c<",
            "TT;>;)V"
        }
    .end annotation

    invoke-super {p0, p1, p2}, Ld1/a;->h(Ljava/lang/Object;Li1/c;)V

    sget-object v0, Lv0/j;->A:Ljava/lang/Float;

    if-ne p1, v0, :cond_1

    if-nez p2, :cond_0

    iget-object p1, p0, Ld1/b;->x:Ly0/a;

    if-eqz p1, :cond_1

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Ly0/a;->m(Li1/c;)V

    goto :goto_0

    :cond_0
    new-instance p1, Ly0/p;

    invoke-direct {p1, p2}, Ly0/p;-><init>(Li1/c;)V

    iput-object p1, p0, Ld1/b;->x:Ly0/a;

    invoke-virtual {p1, p0}, Ly0/a;->a(Ly0/a$b;)V

    iget-object p1, p0, Ld1/b;->x:Ly0/a;

    invoke-virtual {p0, p1}, Ld1/a;->j(Ly0/a;)V

    :cond_1
    :goto_0
    return-void
.end method

.method u(Landroid/graphics/Canvas;Landroid/graphics/Matrix;I)V
    .locals 6

    goto/32 :goto_4

    nop

    :goto_0
    move v1, v3

    goto/32 :goto_9

    nop

    :goto_1
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_1e

    nop

    :goto_2
    if-nez v1, :cond_0

    goto/32 :goto_34

    :cond_0
    goto/32 :goto_33

    nop

    :goto_3
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    goto/32 :goto_16

    nop

    :goto_4
    const-string v0, "CompositionLayer#draw"

    goto/32 :goto_3c

    nop

    :goto_5
    return-void

    :goto_6
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    goto/32 :goto_2d

    nop

    :goto_7
    if-nez v1, :cond_1

    goto/32 :goto_a

    :cond_1
    goto/32 :goto_c

    nop

    :goto_8
    if-eqz v2, :cond_2

    goto/32 :goto_14

    :cond_2
    goto/32 :goto_3f

    nop

    :goto_9
    goto :goto_1d

    :goto_a
    goto/32 :goto_1c

    nop

    :goto_b
    if-nez v2, :cond_3

    goto/32 :goto_12

    :cond_3
    goto/32 :goto_1a

    nop

    :goto_c
    iget-object v1, p0, Ld1/b;->y:Ljava/util/List;

    goto/32 :goto_3

    nop

    :goto_d
    const/4 v3, 0x1

    goto/32 :goto_7

    nop

    :goto_e
    int-to-float v3, v3

    goto/32 :goto_19

    nop

    :goto_f
    iget-object v2, p0, Ld1/b;->A:Landroid/graphics/RectF;

    goto/32 :goto_3b

    nop

    :goto_10
    iget-object v3, p0, Ld1/a;->o:Ld1/d;

    goto/32 :goto_20

    nop

    :goto_11
    invoke-virtual {v2, p1, p2, p3}, Ld1/a;->e(Landroid/graphics/Canvas;Landroid/graphics/Matrix;I)V

    :goto_12
    goto/32 :goto_28

    nop

    :goto_13
    goto :goto_24

    :goto_14
    goto/32 :goto_23

    nop

    :goto_15
    if-ne p3, v2, :cond_4

    goto/32 :goto_a

    :cond_4
    goto/32 :goto_0

    nop

    :goto_16
    if-gt v1, v3, :cond_5

    goto/32 :goto_a

    :cond_5
    goto/32 :goto_15

    nop

    :goto_17
    goto :goto_2e

    :goto_18
    goto/32 :goto_27

    nop

    :goto_19
    const/4 v4, 0x0

    goto/32 :goto_31

    nop

    :goto_1a
    iget-object v2, p0, Ld1/b;->y:Ljava/util/List;

    goto/32 :goto_1

    nop

    :goto_1b
    iget-object v4, p0, Ld1/b;->A:Landroid/graphics/RectF;

    goto/32 :goto_26

    nop

    :goto_1c
    const/4 v1, 0x0

    :goto_1d
    goto/32 :goto_25

    nop

    :goto_1e
    check-cast v2, Ld1/a;

    goto/32 :goto_11

    nop

    :goto_1f
    iget-object v1, p0, Ld1/a;->n:Lcom/airbnb/lottie/a;

    goto/32 :goto_37

    nop

    :goto_20
    invoke-virtual {v3}, Ld1/d;->i()I

    move-result v3

    goto/32 :goto_e

    nop

    :goto_21
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    :goto_22
    goto/32 :goto_2

    nop

    :goto_23
    move v2, v3

    :goto_24
    goto/32 :goto_b

    nop

    :goto_25
    if-nez v1, :cond_6

    goto/32 :goto_2a

    :cond_6
    goto/32 :goto_2b

    nop

    :goto_26
    iget-object v5, p0, Ld1/b;->B:Landroid/graphics/Paint;

    goto/32 :goto_36

    nop

    :goto_27
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto/32 :goto_32

    nop

    :goto_28
    add-int/lit8 v1, v1, -0x1

    goto/32 :goto_17

    nop

    :goto_29
    goto :goto_22

    :goto_2a
    goto/32 :goto_21

    nop

    :goto_2b
    iget-object v4, p0, Ld1/b;->B:Landroid/graphics/Paint;

    goto/32 :goto_41

    nop

    :goto_2c
    const/16 v2, 0xff

    goto/32 :goto_d

    nop

    :goto_2d
    sub-int/2addr v1, v3

    :goto_2e
    goto/32 :goto_3a

    nop

    :goto_2f
    iget-object v1, p0, Ld1/b;->A:Landroid/graphics/RectF;

    goto/32 :goto_40

    nop

    :goto_30
    invoke-virtual {v2}, Ld1/d;->j()I

    move-result v2

    goto/32 :goto_38

    nop

    :goto_31
    invoke-virtual {v1, v4, v4, v2, v3}, Landroid/graphics/RectF;->set(FFFF)V

    goto/32 :goto_3e

    nop

    :goto_32
    invoke-static {v0}, Lv0/c;->b(Ljava/lang/String;)F

    goto/32 :goto_5

    nop

    :goto_33
    move p3, v2

    :goto_34
    goto/32 :goto_35

    nop

    :goto_35
    iget-object v1, p0, Ld1/b;->y:Ljava/util/List;

    goto/32 :goto_6

    nop

    :goto_36
    invoke-static {p1, v4, v5}, Lh1/h;->m(Landroid/graphics/Canvas;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    goto/32 :goto_29

    nop

    :goto_37
    invoke-virtual {v1}, Lcom/airbnb/lottie/a;->G()Z

    move-result v1

    goto/32 :goto_2c

    nop

    :goto_38
    int-to-float v2, v2

    goto/32 :goto_10

    nop

    :goto_39
    invoke-virtual {p2, v1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    goto/32 :goto_1f

    nop

    :goto_3a
    if-gez v1, :cond_7

    goto/32 :goto_18

    :cond_7
    goto/32 :goto_f

    nop

    :goto_3b
    invoke-virtual {v2}, Landroid/graphics/RectF;->isEmpty()Z

    move-result v2

    goto/32 :goto_8

    nop

    :goto_3c
    invoke-static {v0}, Lv0/c;->a(Ljava/lang/String;)V

    goto/32 :goto_2f

    nop

    :goto_3d
    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/RectF;)Z

    move-result v2

    goto/32 :goto_13

    nop

    :goto_3e
    iget-object v1, p0, Ld1/b;->A:Landroid/graphics/RectF;

    goto/32 :goto_39

    nop

    :goto_3f
    iget-object v2, p0, Ld1/b;->A:Landroid/graphics/RectF;

    goto/32 :goto_3d

    nop

    :goto_40
    iget-object v2, p0, Ld1/a;->o:Ld1/d;

    goto/32 :goto_30

    nop

    :goto_41
    invoke-virtual {v4, p3}, Landroid/graphics/Paint;->setAlpha(I)V

    goto/32 :goto_1b

    nop
.end method
