.class public Ld1/h;
.super Ld1/a;


# instance fields
.field private final A:Landroid/graphics/Paint;

.field private final B:Landroid/graphics/Paint;

.field private final C:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "La1/d;",
            "Ljava/util/List<",
            "Lx0/d;",
            ">;>;"
        }
    .end annotation
.end field

.field private final D:Ln/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ln/d<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final E:Ly0/n;

.field private final F:Lcom/airbnb/lottie/a;

.field private final G:Lv0/d;

.field private H:Ly0/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ly0/a<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private I:Ly0/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ly0/a<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private J:Ly0/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ly0/a<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private K:Ly0/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ly0/a<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private L:Ly0/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ly0/a<",
            "Ljava/lang/Float;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private M:Ly0/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ly0/a<",
            "Ljava/lang/Float;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private N:Ly0/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ly0/a<",
            "Ljava/lang/Float;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private O:Ly0/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ly0/a<",
            "Ljava/lang/Float;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private P:Ly0/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ly0/a<",
            "Ljava/lang/Float;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private Q:Ly0/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ly0/a<",
            "Ljava/lang/Float;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private final x:Ljava/lang/StringBuilder;

.field private final y:Landroid/graphics/RectF;

.field private final z:Landroid/graphics/Matrix;


# direct methods
.method constructor <init>(Lcom/airbnb/lottie/a;Ld1/d;)V
    .locals 2

    invoke-direct {p0, p1, p2}, Ld1/a;-><init>(Lcom/airbnb/lottie/a;Ld1/d;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    iput-object v0, p0, Ld1/h;->x:Ljava/lang/StringBuilder;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Ld1/h;->y:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Ld1/h;->z:Landroid/graphics/Matrix;

    new-instance v0, Ld1/h$a;

    const/4 v1, 0x1

    invoke-direct {v0, p0, v1}, Ld1/h$a;-><init>(Ld1/h;I)V

    iput-object v0, p0, Ld1/h;->A:Landroid/graphics/Paint;

    new-instance v0, Ld1/h$b;

    invoke-direct {v0, p0, v1}, Ld1/h$b;-><init>(Ld1/h;I)V

    iput-object v0, p0, Ld1/h;->B:Landroid/graphics/Paint;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Ld1/h;->C:Ljava/util/Map;

    new-instance v0, Ln/d;

    invoke-direct {v0}, Ln/d;-><init>()V

    iput-object v0, p0, Ld1/h;->D:Ln/d;

    iput-object p1, p0, Ld1/h;->F:Lcom/airbnb/lottie/a;

    invoke-virtual {p2}, Ld1/d;->a()Lv0/d;

    move-result-object p1

    iput-object p1, p0, Ld1/h;->G:Lv0/d;

    invoke-virtual {p2}, Ld1/d;->q()Lb1/j;

    move-result-object p1

    invoke-virtual {p1}, Lb1/j;->d()Ly0/n;

    move-result-object p1

    iput-object p1, p0, Ld1/h;->E:Ly0/n;

    invoke-virtual {p1, p0}, Ly0/a;->a(Ly0/a$b;)V

    invoke-virtual {p0, p1}, Ld1/a;->j(Ly0/a;)V

    invoke-virtual {p2}, Ld1/d;->r()Lb1/k;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object p2, p1, Lb1/k;->a:Lb1/a;

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lb1/a;->a()Ly0/a;

    move-result-object p2

    iput-object p2, p0, Ld1/h;->H:Ly0/a;

    invoke-virtual {p2, p0}, Ly0/a;->a(Ly0/a$b;)V

    iget-object p2, p0, Ld1/h;->H:Ly0/a;

    invoke-virtual {p0, p2}, Ld1/a;->j(Ly0/a;)V

    :cond_0
    if-eqz p1, :cond_1

    iget-object p2, p1, Lb1/k;->b:Lb1/a;

    if-eqz p2, :cond_1

    invoke-virtual {p2}, Lb1/a;->a()Ly0/a;

    move-result-object p2

    iput-object p2, p0, Ld1/h;->J:Ly0/a;

    invoke-virtual {p2, p0}, Ly0/a;->a(Ly0/a$b;)V

    iget-object p2, p0, Ld1/h;->J:Ly0/a;

    invoke-virtual {p0, p2}, Ld1/a;->j(Ly0/a;)V

    :cond_1
    if-eqz p1, :cond_2

    iget-object p2, p1, Lb1/k;->c:Lb1/b;

    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lb1/b;->a()Ly0/a;

    move-result-object p2

    iput-object p2, p0, Ld1/h;->L:Ly0/a;

    invoke-virtual {p2, p0}, Ly0/a;->a(Ly0/a$b;)V

    iget-object p2, p0, Ld1/h;->L:Ly0/a;

    invoke-virtual {p0, p2}, Ld1/a;->j(Ly0/a;)V

    :cond_2
    if-eqz p1, :cond_3

    iget-object p1, p1, Lb1/k;->d:Lb1/b;

    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lb1/b;->a()Ly0/a;

    move-result-object p1

    iput-object p1, p0, Ld1/h;->N:Ly0/a;

    invoke-virtual {p1, p0}, Ly0/a;->a(Ly0/a$b;)V

    iget-object p1, p0, Ld1/h;->N:Ly0/a;

    invoke-virtual {p0, p1}, Ld1/a;->j(Ly0/a;)V

    :cond_3
    return-void
.end method

.method private K(La1/b$a;Landroid/graphics/Canvas;F)V
    .locals 2

    sget-object v0, Ld1/h$c;->a:[I

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x2

    const/4 v1, 0x0

    if-eq p1, v0, :cond_1

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    goto :goto_1

    :cond_0
    neg-float p1, p3

    const/high16 p3, 0x40000000    # 2.0f

    div-float/2addr p1, p3

    goto :goto_0

    :cond_1
    neg-float p1, p3

    :goto_0
    invoke-virtual {p2, p1, v1}, Landroid/graphics/Canvas;->translate(FF)V

    :goto_1
    return-void
.end method

.method private L(Ljava/lang/String;I)Ljava/lang/String;
    .locals 5

    invoke-virtual {p1, p2}, Ljava/lang/String;->codePointAt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->charCount(I)I

    move-result v1

    add-int/2addr v1, p2

    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v1, v2, :cond_1

    invoke-virtual {p1, v1}, Ljava/lang/String;->codePointAt(I)I

    move-result v2

    invoke-direct {p0, v2}, Ld1/h;->X(I)Z

    move-result v3

    if-nez v3, :cond_0

    goto :goto_1

    :cond_0
    invoke-static {v2}, Ljava/lang/Character;->charCount(I)I

    move-result v3

    add-int/2addr v1, v3

    mul-int/lit8 v0, v0, 0x1f

    add-int/2addr v0, v2

    goto :goto_0

    :cond_1
    :goto_1
    iget-object v2, p0, Ld1/h;->D:Ln/d;

    int-to-long v3, v0

    invoke-virtual {v2, v3, v4}, Ln/d;->d(J)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object p1, p0, Ld1/h;->D:Ln/d;

    invoke-virtual {p1, v3, v4}, Ln/d;->f(J)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    return-object p1

    :cond_2
    iget-object v0, p0, Ld1/h;->x:Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    :goto_2
    if-ge p2, v1, :cond_3

    invoke-virtual {p1, p2}, Ljava/lang/String;->codePointAt(I)I

    move-result v0

    iget-object v2, p0, Ld1/h;->x:Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->appendCodePoint(I)Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/Character;->charCount(I)I

    move-result v0

    add-int/2addr p2, v0

    goto :goto_2

    :cond_3
    iget-object p1, p0, Ld1/h;->x:Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iget-object p2, p0, Ld1/h;->D:Ln/d;

    invoke-virtual {p2, v3, v4, p1}, Ln/d;->j(JLjava/lang/Object;)V

    return-object p1
.end method

.method private M(Ljava/lang/String;Landroid/graphics/Paint;Landroid/graphics/Canvas;)V
    .locals 8

    invoke-virtual {p2}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p2}, Landroid/graphics/Paint;->getStyle()Landroid/graphics/Paint$Style;

    move-result-object v0

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    if-ne v0, v1, :cond_1

    invoke-virtual {p2}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    return-void

    :cond_1
    const/4 v3, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v1, p3

    move-object v2, p1

    move-object v7, p2

    invoke-virtual/range {v1 .. v7}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;IIFFLandroid/graphics/Paint;)V

    return-void
.end method

.method private N(La1/d;Landroid/graphics/Matrix;FLa1/b;Landroid/graphics/Canvas;)V
    .locals 7

    invoke-direct {p0, p1}, Ld1/h;->U(La1/d;)Ljava/util/List;

    move-result-object p1

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lx0/d;

    invoke-virtual {v2}, Lx0/d;->g()Landroid/graphics/Path;

    move-result-object v2

    iget-object v3, p0, Ld1/h;->y:Landroid/graphics/RectF;

    invoke-virtual {v2, v3, v0}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    iget-object v3, p0, Ld1/h;->z:Landroid/graphics/Matrix;

    invoke-virtual {v3, p2}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    iget-object v3, p0, Ld1/h;->z:Landroid/graphics/Matrix;

    const/4 v4, 0x0

    iget v5, p4, La1/b;->g:F

    neg-float v5, v5

    invoke-static {}, Lh1/h;->e()F

    move-result v6

    mul-float/2addr v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Matrix;->preTranslate(FF)Z

    iget-object v3, p0, Ld1/h;->z:Landroid/graphics/Matrix;

    invoke-virtual {v3, p3, p3}, Landroid/graphics/Matrix;->preScale(FF)Z

    iget-object v3, p0, Ld1/h;->z:Landroid/graphics/Matrix;

    invoke-virtual {v2, v3}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    iget-boolean v3, p4, La1/b;->k:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Ld1/h;->A:Landroid/graphics/Paint;

    invoke-direct {p0, v2, v3, p5}, Ld1/h;->Q(Landroid/graphics/Path;Landroid/graphics/Paint;Landroid/graphics/Canvas;)V

    iget-object v3, p0, Ld1/h;->B:Landroid/graphics/Paint;

    goto :goto_1

    :cond_0
    iget-object v3, p0, Ld1/h;->B:Landroid/graphics/Paint;

    invoke-direct {p0, v2, v3, p5}, Ld1/h;->Q(Landroid/graphics/Path;Landroid/graphics/Paint;Landroid/graphics/Canvas;)V

    iget-object v3, p0, Ld1/h;->A:Landroid/graphics/Paint;

    :goto_1
    invoke-direct {p0, v2, v3, p5}, Ld1/h;->Q(Landroid/graphics/Path;Landroid/graphics/Paint;Landroid/graphics/Canvas;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private O(Ljava/lang/String;La1/b;Landroid/graphics/Canvas;)V
    .locals 0

    iget-boolean p2, p2, La1/b;->k:Z

    if-eqz p2, :cond_0

    iget-object p2, p0, Ld1/h;->A:Landroid/graphics/Paint;

    invoke-direct {p0, p1, p2, p3}, Ld1/h;->M(Ljava/lang/String;Landroid/graphics/Paint;Landroid/graphics/Canvas;)V

    iget-object p2, p0, Ld1/h;->B:Landroid/graphics/Paint;

    goto :goto_0

    :cond_0
    iget-object p2, p0, Ld1/h;->B:Landroid/graphics/Paint;

    invoke-direct {p0, p1, p2, p3}, Ld1/h;->M(Ljava/lang/String;Landroid/graphics/Paint;Landroid/graphics/Canvas;)V

    iget-object p2, p0, Ld1/h;->A:Landroid/graphics/Paint;

    :goto_0
    invoke-direct {p0, p1, p2, p3}, Ld1/h;->M(Ljava/lang/String;Landroid/graphics/Paint;Landroid/graphics/Canvas;)V

    return-void
.end method

.method private P(Ljava/lang/String;La1/b;Landroid/graphics/Canvas;F)V
    .locals 5

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v1, v2, :cond_2

    invoke-direct {p0, p1, v1}, Ld1/h;->L(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v1, v3

    invoke-direct {p0, v2, p2, p3}, Ld1/h;->O(Ljava/lang/String;La1/b;Landroid/graphics/Canvas;)V

    iget-object v3, p0, Ld1/h;->A:Landroid/graphics/Paint;

    const/4 v4, 0x1

    invoke-virtual {v3, v2, v0, v4}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;II)F

    move-result v2

    iget v3, p2, La1/b;->e:I

    int-to-float v3, v3

    const/high16 v4, 0x41200000    # 10.0f

    div-float/2addr v3, v4

    iget-object v4, p0, Ld1/h;->O:Ly0/a;

    if-eqz v4, :cond_0

    :goto_1
    invoke-virtual {v4}, Ly0/a;->h()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    add-float/2addr v3, v4

    goto :goto_2

    :cond_0
    iget-object v4, p0, Ld1/h;->N:Ly0/a;

    if-eqz v4, :cond_1

    goto :goto_1

    :cond_1
    :goto_2
    mul-float/2addr v3, p4

    add-float/2addr v2, v3

    const/4 v3, 0x0

    invoke-virtual {p3, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    goto :goto_0

    :cond_2
    return-void
.end method

.method private Q(Landroid/graphics/Path;Landroid/graphics/Paint;Landroid/graphics/Canvas;)V
    .locals 2

    invoke-virtual {p2}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p2}, Landroid/graphics/Paint;->getStyle()Landroid/graphics/Paint$Style;

    move-result-object v0

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    if-ne v0, v1, :cond_1

    invoke-virtual {p2}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    return-void

    :cond_1
    invoke-virtual {p3, p1, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    return-void
.end method

.method private R(Ljava/lang/String;La1/b;Landroid/graphics/Matrix;La1/c;Landroid/graphics/Canvas;FF)V
    .locals 8

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v0, v1, :cond_3

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-virtual {p4}, La1/c;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p4}, La1/c;->c()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, La1/d;->c(CLjava/lang/String;Ljava/lang/String;)I

    move-result v1

    iget-object v2, p0, Ld1/h;->G:Lv0/d;

    invoke-virtual {v2}, Lv0/d;->c()Ln/h;

    move-result-object v2

    invoke-virtual {v2, v1}, Ln/h;->f(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, La1/d;

    if-nez v1, :cond_0

    goto :goto_3

    :cond_0
    move-object v2, p0

    move-object v3, v1

    move-object v4, p3

    move v5, p7

    move-object v6, p2

    move-object v7, p5

    invoke-direct/range {v2 .. v7}, Ld1/h;->N(La1/d;Landroid/graphics/Matrix;FLa1/b;Landroid/graphics/Canvas;)V

    invoke-virtual {v1}, La1/d;->b()D

    move-result-wide v1

    double-to-float v1, v1

    mul-float/2addr v1, p7

    invoke-static {}, Lh1/h;->e()F

    move-result v2

    mul-float/2addr v1, v2

    mul-float/2addr v1, p6

    iget v2, p2, La1/b;->e:I

    int-to-float v2, v2

    const/high16 v3, 0x41200000    # 10.0f

    div-float/2addr v2, v3

    iget-object v3, p0, Ld1/h;->O:Ly0/a;

    if-eqz v3, :cond_1

    :goto_1
    invoke-virtual {v3}, Ly0/a;->h()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    add-float/2addr v2, v3

    goto :goto_2

    :cond_1
    iget-object v3, p0, Ld1/h;->N:Ly0/a;

    if-eqz v3, :cond_2

    goto :goto_1

    :cond_2
    :goto_2
    mul-float/2addr v2, p6

    add-float/2addr v1, v2

    const/4 v2, 0x0

    invoke-virtual {p5, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    return-void
.end method

.method private S(La1/b;Landroid/graphics/Matrix;La1/c;Landroid/graphics/Canvas;)V
    .locals 17

    move-object/from16 v8, p0

    move-object/from16 v9, p1

    move-object/from16 v10, p4

    iget-object v0, v8, Ld1/h;->Q:Ly0/a;

    if-eqz v0, :cond_0

    :goto_0
    invoke-virtual {v0}, Ly0/a;->h()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    goto :goto_1

    :cond_0
    iget-object v0, v8, Ld1/h;->P:Ly0/a;

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    iget v0, v9, La1/b;->c:F

    :goto_1
    const/high16 v1, 0x42c80000    # 100.0f

    div-float v11, v0, v1

    invoke-static/range {p2 .. p2}, Lh1/h;->g(Landroid/graphics/Matrix;)F

    move-result v12

    iget-object v0, v9, La1/b;->a:Ljava/lang/String;

    iget v1, v9, La1/b;->f:F

    invoke-static {}, Lh1/h;->e()F

    move-result v2

    mul-float v13, v1, v2

    invoke-direct {v8, v0}, Ld1/h;->W(Ljava/lang/String;)Ljava/util/List;

    move-result-object v14

    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v15

    const/4 v0, 0x0

    move v7, v0

    :goto_2
    if-ge v7, v15, :cond_2

    invoke-interface {v14, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Ljava/lang/String;

    move-object/from16 v6, p3

    invoke-direct {v8, v1, v6, v11, v12}, Ld1/h;->V(Ljava/lang/String;La1/c;FF)F

    move-result v0

    invoke-virtual/range {p4 .. p4}, Landroid/graphics/Canvas;->save()I

    iget-object v2, v9, La1/b;->d:La1/b$a;

    invoke-direct {v8, v2, v10, v0}, Ld1/h;->K(La1/b$a;Landroid/graphics/Canvas;F)V

    add-int/lit8 v0, v15, -0x1

    int-to-float v0, v0

    mul-float/2addr v0, v13

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v0, v2

    int-to-float v2, v7

    mul-float/2addr v2, v13

    sub-float/2addr v2, v0

    const/4 v0, 0x0

    invoke-virtual {v10, v0, v2}, Landroid/graphics/Canvas;->translate(FF)V

    move-object/from16 v0, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move v6, v12

    move/from16 v16, v7

    move v7, v11

    invoke-direct/range {v0 .. v7}, Ld1/h;->R(Ljava/lang/String;La1/b;Landroid/graphics/Matrix;La1/c;Landroid/graphics/Canvas;FF)V

    invoke-virtual/range {p4 .. p4}, Landroid/graphics/Canvas;->restore()V

    add-int/lit8 v7, v16, 0x1

    goto :goto_2

    :cond_2
    return-void
.end method

.method private T(La1/b;La1/c;Landroid/graphics/Matrix;Landroid/graphics/Canvas;)V
    .locals 7

    invoke-static {p3}, Lh1/h;->g(Landroid/graphics/Matrix;)F

    move-result v0

    iget-object v1, p0, Ld1/h;->F:Lcom/airbnb/lottie/a;

    invoke-virtual {p2}, La1/c;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, La1/c;->c()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v1, v2, p2}, Lcom/airbnb/lottie/a;->E(Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object p2

    if-nez p2, :cond_0

    return-void

    :cond_0
    iget-object v1, p1, La1/b;->a:Ljava/lang/String;

    iget-object v2, p0, Ld1/h;->F:Lcom/airbnb/lottie/a;

    invoke-virtual {v2}, Lcom/airbnb/lottie/a;->D()Lv0/q;

    iget-object v2, p0, Ld1/h;->A:Landroid/graphics/Paint;

    invoke-virtual {v2, p2}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    iget-object p2, p0, Ld1/h;->Q:Ly0/a;

    if-eqz p2, :cond_1

    :goto_0
    invoke-virtual {p2}, Ly0/a;->h()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/Float;

    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result p2

    goto :goto_1

    :cond_1
    iget-object p2, p0, Ld1/h;->P:Ly0/a;

    if-eqz p2, :cond_2

    goto :goto_0

    :cond_2
    iget p2, p1, La1/b;->c:F

    :goto_1
    iget-object v2, p0, Ld1/h;->A:Landroid/graphics/Paint;

    invoke-static {}, Lh1/h;->e()F

    move-result v3

    mul-float/2addr p2, v3

    invoke-virtual {v2, p2}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object p2, p0, Ld1/h;->B:Landroid/graphics/Paint;

    iget-object v2, p0, Ld1/h;->A:Landroid/graphics/Paint;

    invoke-virtual {v2}, Landroid/graphics/Paint;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {p2, v2}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    iget-object p2, p0, Ld1/h;->B:Landroid/graphics/Paint;

    iget-object v2, p0, Ld1/h;->A:Landroid/graphics/Paint;

    invoke-virtual {v2}, Landroid/graphics/Paint;->getTextSize()F

    move-result v2

    invoke-virtual {p2, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    iget p2, p1, La1/b;->f:F

    invoke-static {}, Lh1/h;->e()F

    move-result v2

    mul-float/2addr p2, v2

    invoke-direct {p0, v1}, Ld1/h;->W(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x0

    :goto_2
    if-ge v3, v2, :cond_3

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    iget-object v5, p0, Ld1/h;->B:Landroid/graphics/Paint;

    invoke-virtual {v5, v4}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v5

    iget-object v6, p1, La1/b;->d:La1/b$a;

    invoke-direct {p0, v6, p4, v5}, Ld1/h;->K(La1/b$a;Landroid/graphics/Canvas;F)V

    add-int/lit8 v5, v2, -0x1

    int-to-float v5, v5

    mul-float/2addr v5, p2

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    int-to-float v6, v3

    mul-float/2addr v6, p2

    sub-float/2addr v6, v5

    const/4 v5, 0x0

    invoke-virtual {p4, v5, v6}, Landroid/graphics/Canvas;->translate(FF)V

    invoke-direct {p0, v4, p1, p4, v0}, Ld1/h;->P(Ljava/lang/String;La1/b;Landroid/graphics/Canvas;F)V

    invoke-virtual {p4, p3}, Landroid/graphics/Canvas;->setMatrix(Landroid/graphics/Matrix;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_3
    return-void
.end method

.method private U(La1/d;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "La1/d;",
            ")",
            "Ljava/util/List<",
            "Lx0/d;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Ld1/h;->C:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ld1/h;->C:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    return-object p1

    :cond_0
    invoke-virtual {p1}, La1/d;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_1

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lc1/n;

    new-instance v5, Lx0/d;

    iget-object v6, p0, Ld1/h;->F:Lcom/airbnb/lottie/a;

    invoke-direct {v5, v6, p0, v4}, Lx0/d;-><init>(Lcom/airbnb/lottie/a;Ld1/a;Lc1/n;)V

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Ld1/h;->C:Ljava/util/Map;

    invoke-interface {v0, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v2
.end method

.method private V(Ljava/lang/String;La1/c;FF)F
    .locals 9

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v1, v2, :cond_1

    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-virtual {p2}, La1/c;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, La1/c;->c()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, La1/d;->c(CLjava/lang/String;Ljava/lang/String;)I

    move-result v2

    iget-object v3, p0, Ld1/h;->G:Lv0/d;

    invoke-virtual {v3}, Lv0/d;->c()Ln/h;

    move-result-object v3

    invoke-virtual {v3, v2}, Ln/h;->f(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, La1/d;

    if-nez v2, :cond_0

    goto :goto_1

    :cond_0
    float-to-double v3, v0

    invoke-virtual {v2}, La1/d;->b()D

    move-result-wide v5

    float-to-double v7, p3

    mul-double/2addr v5, v7

    invoke-static {}, Lh1/h;->e()F

    move-result v0

    float-to-double v7, v0

    mul-double/2addr v5, v7

    float-to-double v7, p4

    mul-double/2addr v5, v7

    add-double/2addr v3, v5

    double-to-float v0, v3

    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return v0
.end method

.method private W(Ljava/lang/String;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const-string v0, "\r\n"

    const-string v1, "\r"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "\n"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method private X(I)Z
    .locals 2

    invoke-static {p1}, Ljava/lang/Character;->getType(I)I

    move-result v0

    const/16 v1, 0x10

    if-eq v0, v1, :cond_1

    invoke-static {p1}, Ljava/lang/Character;->getType(I)I

    move-result v0

    const/16 v1, 0x1b

    if-eq v0, v1, :cond_1

    invoke-static {p1}, Ljava/lang/Character;->getType(I)I

    move-result v0

    const/4 v1, 0x6

    if-eq v0, v1, :cond_1

    invoke-static {p1}, Ljava/lang/Character;->getType(I)I

    move-result v0

    const/16 v1, 0x1c

    if-eq v0, v1, :cond_1

    invoke-static {p1}, Ljava/lang/Character;->getType(I)I

    move-result p1

    const/16 v0, 0x13

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method


# virtual methods
.method public a(Landroid/graphics/RectF;Landroid/graphics/Matrix;Z)V
    .locals 1

    invoke-super {p0, p1, p2, p3}, Ld1/a;->a(Landroid/graphics/RectF;Landroid/graphics/Matrix;Z)V

    iget-object p2, p0, Ld1/h;->G:Lv0/d;

    invoke-virtual {p2}, Lv0/d;->b()Landroid/graphics/Rect;

    move-result-object p2

    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result p2

    int-to-float p2, p2

    iget-object p3, p0, Ld1/h;->G:Lv0/d;

    invoke-virtual {p3}, Lv0/d;->b()Landroid/graphics/Rect;

    move-result-object p3

    invoke-virtual {p3}, Landroid/graphics/Rect;->height()I

    move-result p3

    int-to-float p3, p3

    const/4 v0, 0x0

    invoke-virtual {p1, v0, v0, p2, p3}, Landroid/graphics/RectF;->set(FFFF)V

    return-void
.end method

.method public h(Ljava/lang/Object;Li1/c;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;",
            "Li1/c<",
            "TT;>;)V"
        }
    .end annotation

    invoke-super {p0, p1, p2}, Ld1/a;->h(Ljava/lang/Object;Li1/c;)V

    sget-object v0, Lv0/j;->a:Ljava/lang/Integer;

    const/4 v1, 0x0

    if-ne p1, v0, :cond_2

    iget-object p1, p0, Ld1/h;->I:Ly0/a;

    if-eqz p1, :cond_0

    invoke-virtual {p0, p1}, Ld1/a;->D(Ly0/a;)V

    :cond_0
    if-nez p2, :cond_1

    iput-object v1, p0, Ld1/h;->I:Ly0/a;

    goto/16 :goto_1

    :cond_1
    new-instance p1, Ly0/p;

    invoke-direct {p1, p2}, Ly0/p;-><init>(Li1/c;)V

    iput-object p1, p0, Ld1/h;->I:Ly0/a;

    invoke-virtual {p1, p0}, Ly0/a;->a(Ly0/a$b;)V

    iget-object p1, p0, Ld1/h;->I:Ly0/a;

    :goto_0
    invoke-virtual {p0, p1}, Ld1/a;->j(Ly0/a;)V

    goto/16 :goto_1

    :cond_2
    sget-object v0, Lv0/j;->b:Ljava/lang/Integer;

    if-ne p1, v0, :cond_5

    iget-object p1, p0, Ld1/h;->K:Ly0/a;

    if-eqz p1, :cond_3

    invoke-virtual {p0, p1}, Ld1/a;->D(Ly0/a;)V

    :cond_3
    if-nez p2, :cond_4

    iput-object v1, p0, Ld1/h;->K:Ly0/a;

    goto :goto_1

    :cond_4
    new-instance p1, Ly0/p;

    invoke-direct {p1, p2}, Ly0/p;-><init>(Li1/c;)V

    iput-object p1, p0, Ld1/h;->K:Ly0/a;

    invoke-virtual {p1, p0}, Ly0/a;->a(Ly0/a$b;)V

    iget-object p1, p0, Ld1/h;->K:Ly0/a;

    goto :goto_0

    :cond_5
    sget-object v0, Lv0/j;->o:Ljava/lang/Float;

    if-ne p1, v0, :cond_8

    iget-object p1, p0, Ld1/h;->M:Ly0/a;

    if-eqz p1, :cond_6

    invoke-virtual {p0, p1}, Ld1/a;->D(Ly0/a;)V

    :cond_6
    if-nez p2, :cond_7

    iput-object v1, p0, Ld1/h;->M:Ly0/a;

    goto :goto_1

    :cond_7
    new-instance p1, Ly0/p;

    invoke-direct {p1, p2}, Ly0/p;-><init>(Li1/c;)V

    iput-object p1, p0, Ld1/h;->M:Ly0/a;

    invoke-virtual {p1, p0}, Ly0/a;->a(Ly0/a$b;)V

    iget-object p1, p0, Ld1/h;->M:Ly0/a;

    goto :goto_0

    :cond_8
    sget-object v0, Lv0/j;->p:Ljava/lang/Float;

    if-ne p1, v0, :cond_b

    iget-object p1, p0, Ld1/h;->O:Ly0/a;

    if-eqz p1, :cond_9

    invoke-virtual {p0, p1}, Ld1/a;->D(Ly0/a;)V

    :cond_9
    if-nez p2, :cond_a

    iput-object v1, p0, Ld1/h;->O:Ly0/a;

    goto :goto_1

    :cond_a
    new-instance p1, Ly0/p;

    invoke-direct {p1, p2}, Ly0/p;-><init>(Li1/c;)V

    iput-object p1, p0, Ld1/h;->O:Ly0/a;

    invoke-virtual {p1, p0}, Ly0/a;->a(Ly0/a$b;)V

    iget-object p1, p0, Ld1/h;->O:Ly0/a;

    goto :goto_0

    :cond_b
    sget-object v0, Lv0/j;->B:Ljava/lang/Float;

    if-ne p1, v0, :cond_e

    iget-object p1, p0, Ld1/h;->Q:Ly0/a;

    if-eqz p1, :cond_c

    invoke-virtual {p0, p1}, Ld1/a;->D(Ly0/a;)V

    :cond_c
    if-nez p2, :cond_d

    iput-object v1, p0, Ld1/h;->Q:Ly0/a;

    goto :goto_1

    :cond_d
    new-instance p1, Ly0/p;

    invoke-direct {p1, p2}, Ly0/p;-><init>(Li1/c;)V

    iput-object p1, p0, Ld1/h;->Q:Ly0/a;

    invoke-virtual {p1, p0}, Ly0/a;->a(Ly0/a$b;)V

    iget-object p1, p0, Ld1/h;->Q:Ly0/a;

    goto :goto_0

    :cond_e
    :goto_1
    return-void
.end method

.method u(Landroid/graphics/Canvas;Landroid/graphics/Matrix;I)V
    .locals 5

    goto/32 :goto_3c

    nop

    :goto_0
    invoke-virtual {v1}, Ly0/o;->h()Ly0/a;

    move-result-object v1

    goto/32 :goto_4e

    nop

    :goto_1
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto/32 :goto_3d

    nop

    :goto_2
    iget-object p3, p0, Ld1/h;->F:Lcom/airbnb/lottie/a;

    goto/32 :goto_17

    nop

    :goto_3
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto/32 :goto_30

    nop

    :goto_4
    iget-object v1, p0, Ld1/h;->I:Ly0/a;

    goto/32 :goto_9

    nop

    :goto_5
    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setColor(I)V

    goto/32 :goto_5b

    nop

    :goto_6
    iget-object v2, p0, Ld1/h;->B:Landroid/graphics/Paint;

    goto/32 :goto_39

    nop

    :goto_7
    invoke-direct {p0, p3, v0, p2, p1}, Ld1/h;->T(La1/b;La1/c;Landroid/graphics/Matrix;Landroid/graphics/Canvas;)V

    :goto_8
    goto/32 :goto_44

    nop

    :goto_9
    if-nez v1, :cond_0

    goto/32 :goto_14

    :cond_0
    :goto_a
    goto/32 :goto_4a

    nop

    :goto_b
    if-nez v1, :cond_1

    goto/32 :goto_65

    :cond_1
    :goto_c
    goto/32 :goto_38

    nop

    :goto_d
    check-cast v1, Ljava/lang/Float;

    goto/32 :goto_1d

    nop

    :goto_e
    check-cast v0, La1/c;

    goto/32 :goto_3a

    nop

    :goto_f
    invoke-virtual {p3}, Ly0/a;->h()Ljava/lang/Object;

    move-result-object p3

    goto/32 :goto_41

    nop

    :goto_10
    goto :goto_c

    :goto_11
    goto/32 :goto_4d

    nop

    :goto_12
    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    goto/32 :goto_46

    nop

    :goto_13
    goto/16 :goto_54

    :goto_14
    goto/32 :goto_5f

    nop

    :goto_15
    mul-float/2addr v3, v4

    goto/32 :goto_26

    nop

    :goto_16
    iget-object v1, p0, Ld1/h;->K:Ly0/a;

    goto/32 :goto_56

    nop

    :goto_17
    invoke-virtual {p3}, Lcom/airbnb/lottie/a;->k0()Z

    move-result p3

    goto/32 :goto_2d

    nop

    :goto_18
    iget-object p3, p0, Ld1/h;->E:Ly0/n;

    goto/32 :goto_f

    nop

    :goto_19
    goto :goto_a

    :goto_1a
    goto/32 :goto_67

    nop

    :goto_1b
    check-cast v1, Ljava/lang/Integer;

    goto/32 :goto_4f

    nop

    :goto_1c
    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    goto/32 :goto_4b

    nop

    :goto_1d
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    goto/32 :goto_22

    nop

    :goto_1e
    invoke-virtual {v1}, Ly0/a;->h()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_2e

    nop

    :goto_1f
    return-void

    :goto_20
    invoke-virtual {v1}, Ly0/o;->h()Ly0/a;

    move-result-object v1

    goto/32 :goto_2f

    nop

    :goto_21
    iget-object v1, p0, Ld1/h;->F:Lcom/airbnb/lottie/a;

    goto/32 :goto_37

    nop

    :goto_22
    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    goto/32 :goto_64

    nop

    :goto_23
    iget-object v1, p0, Ld1/h;->B:Landroid/graphics/Paint;

    goto/32 :goto_58

    nop

    :goto_24
    iget-object v0, p0, Ld1/h;->G:Lv0/d;

    goto/32 :goto_49

    nop

    :goto_25
    invoke-virtual {v1}, Ly0/a;->h()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_1b

    nop

    :goto_26
    mul-float/2addr v3, v1

    goto/32 :goto_59

    nop

    :goto_27
    invoke-virtual {v1}, Ly0/a;->h()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_d

    nop

    :goto_28
    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    :goto_29
    goto/32 :goto_45

    nop

    :goto_2a
    if-nez v1, :cond_2

    goto/32 :goto_36

    :cond_2
    goto/32 :goto_50

    nop

    :goto_2b
    iget-object v1, p0, Ld1/h;->L:Ly0/a;

    goto/32 :goto_63

    nop

    :goto_2c
    check-cast v1, Ljava/lang/Integer;

    goto/32 :goto_61

    nop

    :goto_2d
    if-eqz p3, :cond_3

    goto/32 :goto_3f

    :cond_3
    goto/32 :goto_3e

    nop

    :goto_2e
    check-cast v1, Ljava/lang/Integer;

    goto/32 :goto_1

    nop

    :goto_2f
    const/16 v2, 0x64

    goto/32 :goto_32

    nop

    :goto_30
    return-void

    :goto_31
    goto/32 :goto_4

    nop

    :goto_32
    if-eqz v1, :cond_4

    goto/32 :goto_5e

    :cond_4
    goto/32 :goto_60

    nop

    :goto_33
    goto/16 :goto_57

    :goto_34
    goto/32 :goto_23

    nop

    :goto_35
    goto/16 :goto_8

    :goto_36
    goto/32 :goto_7

    nop

    :goto_37
    invoke-virtual {v1}, Lcom/airbnb/lottie/a;->k0()Z

    move-result v1

    goto/32 :goto_2a

    nop

    :goto_38
    iget-object v2, p0, Ld1/h;->B:Landroid/graphics/Paint;

    goto/32 :goto_27

    nop

    :goto_39
    iget v3, p3, La1/b;->j:F

    goto/32 :goto_68

    nop

    :goto_3a
    if-eqz v0, :cond_5

    goto/32 :goto_31

    :cond_5
    goto/32 :goto_3

    nop

    :goto_3b
    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_e

    nop

    :goto_3c
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    goto/32 :goto_2

    nop

    :goto_3d
    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setColor(I)V

    goto/32 :goto_13

    nop

    :goto_3e
    invoke-virtual {p1, p2}, Landroid/graphics/Canvas;->setMatrix(Landroid/graphics/Matrix;)V

    :goto_3f
    goto/32 :goto_18

    nop

    :goto_40
    if-nez v1, :cond_6

    goto/32 :goto_34

    :cond_6
    goto/32 :goto_33

    nop

    :goto_41
    check-cast p3, La1/b;

    goto/32 :goto_24

    nop

    :goto_42
    iget-object v2, p0, Ld1/h;->B:Landroid/graphics/Paint;

    goto/32 :goto_25

    nop

    :goto_43
    iget-object v1, p3, La1/b;->b:Ljava/lang/String;

    goto/32 :goto_3b

    nop

    :goto_44
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto/32 :goto_1f

    nop

    :goto_45
    iget-object v1, p0, Ld1/a;->v:Ly0/o;

    goto/32 :goto_20

    nop

    :goto_46
    iget-object v2, p0, Ld1/h;->B:Landroid/graphics/Paint;

    goto/32 :goto_1c

    nop

    :goto_47
    mul-int/lit16 v1, v1, 0xff

    goto/32 :goto_51

    nop

    :goto_48
    iget-object v1, p0, Ld1/a;->v:Ly0/o;

    goto/32 :goto_0

    nop

    :goto_49
    invoke-virtual {v0}, Lv0/d;->g()Ljava/util/Map;

    move-result-object v0

    goto/32 :goto_43

    nop

    :goto_4a
    iget-object v2, p0, Ld1/h;->A:Landroid/graphics/Paint;

    goto/32 :goto_1e

    nop

    :goto_4b
    iget-object v1, p0, Ld1/h;->M:Ly0/a;

    goto/32 :goto_b

    nop

    :goto_4c
    if-nez v1, :cond_7

    goto/32 :goto_1a

    :cond_7
    goto/32 :goto_19

    nop

    :goto_4d
    invoke-static {p2}, Lh1/h;->g(Landroid/graphics/Matrix;)F

    move-result v1

    goto/32 :goto_6

    nop

    :goto_4e
    invoke-virtual {v1}, Ly0/a;->h()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_2c

    nop

    :goto_4f
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto/32 :goto_5

    nop

    :goto_50
    invoke-direct {p0, p3, p2, v0, p1}, Ld1/h;->S(La1/b;Landroid/graphics/Matrix;La1/c;Landroid/graphics/Canvas;)V

    goto/32 :goto_35

    nop

    :goto_51
    div-int/2addr v1, v2

    goto/32 :goto_66

    nop

    :goto_52
    iget-object v1, p0, Ld1/h;->J:Ly0/a;

    goto/32 :goto_40

    nop

    :goto_53
    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    :goto_54
    goto/32 :goto_16

    nop

    :goto_55
    iget v2, p3, La1/b;->h:I

    goto/32 :goto_53

    nop

    :goto_56
    if-nez v1, :cond_8

    goto/32 :goto_5c

    :cond_8
    :goto_57
    goto/32 :goto_42

    nop

    :goto_58
    iget v2, p3, La1/b;->i:I

    goto/32 :goto_28

    nop

    :goto_59
    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    :goto_5a
    goto/32 :goto_21

    nop

    :goto_5b
    goto/16 :goto_29

    :goto_5c
    goto/32 :goto_52

    nop

    :goto_5d
    goto :goto_62

    :goto_5e
    goto/32 :goto_48

    nop

    :goto_5f
    iget-object v1, p0, Ld1/h;->H:Ly0/a;

    goto/32 :goto_4c

    nop

    :goto_60
    move v1, v2

    goto/32 :goto_5d

    nop

    :goto_61
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    :goto_62
    goto/32 :goto_47

    nop

    :goto_63
    if-nez v1, :cond_9

    goto/32 :goto_11

    :cond_9
    goto/32 :goto_10

    nop

    :goto_64
    goto :goto_5a

    :goto_65
    goto/32 :goto_2b

    nop

    :goto_66
    iget-object v2, p0, Ld1/h;->A:Landroid/graphics/Paint;

    goto/32 :goto_12

    nop

    :goto_67
    iget-object v1, p0, Ld1/h;->A:Landroid/graphics/Paint;

    goto/32 :goto_55

    nop

    :goto_68
    invoke-static {}, Lh1/h;->e()F

    move-result v4

    goto/32 :goto_15

    nop
.end method
