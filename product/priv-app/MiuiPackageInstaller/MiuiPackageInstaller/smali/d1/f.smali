.class public Ld1/f;
.super Ld1/a;


# instance fields
.field private final x:Lx0/d;


# direct methods
.method constructor <init>(Lcom/airbnb/lottie/a;Ld1/d;)V
    .locals 3

    invoke-direct {p0, p1, p2}, Ld1/a;-><init>(Lcom/airbnb/lottie/a;Ld1/d;)V

    new-instance v0, Lc1/n;

    invoke-virtual {p2}, Ld1/d;->l()Ljava/util/List;

    move-result-object p2

    const-string v1, "__container"

    const/4 v2, 0x0

    invoke-direct {v0, v1, p2, v2}, Lc1/n;-><init>(Ljava/lang/String;Ljava/util/List;Z)V

    new-instance p2, Lx0/d;

    invoke-direct {p2, p1, p0, v0}, Lx0/d;-><init>(Lcom/airbnb/lottie/a;Ld1/a;Lc1/n;)V

    iput-object p2, p0, Ld1/f;->x:Lx0/d;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p2, p1, v0}, Lx0/d;->c(Ljava/util/List;Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method protected E(La1/e;ILjava/util/List;La1/e;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "La1/e;",
            "I",
            "Ljava/util/List<",
            "La1/e;",
            ">;",
            "La1/e;",
            ")V"
        }
    .end annotation

    iget-object v0, p0, Ld1/f;->x:Lx0/d;

    invoke-virtual {v0, p1, p2, p3, p4}, Lx0/d;->f(La1/e;ILjava/util/List;La1/e;)V

    return-void
.end method

.method public a(Landroid/graphics/RectF;Landroid/graphics/Matrix;Z)V
    .locals 1

    invoke-super {p0, p1, p2, p3}, Ld1/a;->a(Landroid/graphics/RectF;Landroid/graphics/Matrix;Z)V

    iget-object p2, p0, Ld1/f;->x:Lx0/d;

    iget-object v0, p0, Ld1/a;->m:Landroid/graphics/Matrix;

    invoke-virtual {p2, p1, v0, p3}, Lx0/d;->a(Landroid/graphics/RectF;Landroid/graphics/Matrix;Z)V

    return-void
.end method

.method u(Landroid/graphics/Canvas;Landroid/graphics/Matrix;I)V
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    return-void

    :goto_1
    invoke-virtual {v0, p1, p2, p3}, Lx0/d;->e(Landroid/graphics/Canvas;Landroid/graphics/Matrix;I)V

    goto/32 :goto_0

    nop

    :goto_2
    iget-object v0, p0, Ld1/f;->x:Lx0/d;

    goto/32 :goto_1

    nop
.end method
