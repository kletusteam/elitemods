.class public final Laa/c;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Laa/c$g;,
        Laa/c$b;,
        Laa/c$f;,
        Laa/c$c;,
        Laa/c$d;,
        Laa/c$e;
    }
.end annotation


# static fields
.field private static final a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/Class<",
            "*>;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/Class<",
            "*>;",
            "Laa/c$f<",
            "*>;>;"
        }
    .end annotation
.end field

.field private static final c:Laa/c$e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laa/c$e<",
            "Ljava/lang/StringBuilder;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Laa/c;->a:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Laa/c;->b:Ljava/util/HashMap;

    new-instance v0, Laa/c$a;

    invoke-direct {v0}, Laa/c$a;-><init>()V

    const/4 v1, 0x4

    invoke-static {v0, v1}, Laa/c;->b(Laa/c$d;I)Laa/c$g;

    move-result-object v0

    sput-object v0, Laa/c;->c:Laa/c$e;

    return-void
.end method

.method static synthetic a()Ljava/util/HashMap;
    .locals 1

    sget-object v0, Laa/c;->b:Ljava/util/HashMap;

    return-object v0
.end method

.method public static b(Laa/c$d;I)Laa/c$g;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Laa/c$d<",
            "TT;>;I)",
            "Laa/c$g<",
            "TT;>;"
        }
    .end annotation

    new-instance v0, Laa/c$g;

    invoke-direct {v0, p0, p1}, Laa/c$g;-><init>(Laa/c$d;I)V

    return-object v0
.end method

.method public static c()Laa/c$e;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Laa/c$e<",
            "Ljava/lang/StringBuilder;",
            ">;"
        }
    .end annotation

    sget-object v0, Laa/c;->c:Laa/c$e;

    return-object v0
.end method

.method static d(Laa/c$f;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Laa/c$f<",
            "TT;>;I)V"
        }
    .end annotation

    sget-object v0, Laa/c;->b:Ljava/util/HashMap;

    monitor-enter v0

    neg-int p1, p1

    :try_start_0
    invoke-virtual {p0, p1}, Laa/c$f;->b(I)V

    monitor-exit v0

    return-void

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0
.end method

.method static e(Ljava/lang/Class;I)Laa/c$f;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;I)",
            "Laa/c$f<",
            "TT;>;"
        }
    .end annotation

    sget-object v0, Laa/c;->b:Ljava/util/HashMap;

    monitor-enter v0

    :try_start_0
    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Laa/c$f;

    if-nez v1, :cond_0

    new-instance v1, Laa/c$f;

    invoke-direct {v1, p0, p1}, Laa/c$f;-><init>(Ljava/lang/Class;I)V

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    invoke-virtual {v1, p1}, Laa/c$f;->b(I)V

    :goto_0
    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0
.end method
