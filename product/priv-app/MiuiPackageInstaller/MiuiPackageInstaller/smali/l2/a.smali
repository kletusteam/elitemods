.class public final Ll2/a;
.super Ljava/lang/Object;


# static fields
.field public static final a:Ll2/a;

.field private static b:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ll2/a;

    invoke-direct {v0}, Ll2/a;-><init>()V

    sput-object v0, Ll2/a;->a:Ll2/a;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private final a(Landroid/app/Activity;Lm5/e;)Z
    .locals 2

    invoke-static {p1, p2}, Lcom/android/packageinstaller/compat/UnknownSourceCompat;->checkUnknownSourcesEnabled(Landroid/app/Activity;Lm5/e;)Z

    move-result v0

    if-nez v0, :cond_4

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lm5/e;->l()I

    move-result v0

    const-string v1, "appops"

    invoke-virtual {p1, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    const-string v1, "null cannot be cast to non-null type android.app.AppOpsManager"

    invoke-static {p1, v1}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/app/AppOpsManager;

    const/16 v1, 0x42

    invoke-virtual {p2}, Lm5/e;->k()Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, v1, v0, p2}, Lcom/android/packageinstaller/compat/AppOpsManagerCompat;->checkOpNoThrow(Landroid/app/AppOpsManager;IILjava/lang/String;)I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    const/4 p2, 0x3

    const/4 v0, 0x0

    if-nez p1, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eq v1, p2, :cond_3

    :goto_1
    if-nez p1, :cond_2

    return v0

    :cond_2
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    :cond_3
    return v0

    :cond_4
    const/4 p1, 0x1

    return p1
.end method

.method public static synthetic c(Ll2/a;Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lm5/e;ILjava/lang/Object;)V
    .locals 7

    and-int/lit8 p7, p6, 0x4

    const/4 v0, 0x0

    if-eqz p7, :cond_0

    move-object v4, v0

    goto :goto_0

    :cond_0
    move-object v4, p3

    :goto_0
    and-int/lit8 p3, p6, 0x8

    if-eqz p3, :cond_1

    move-object v5, v0

    goto :goto_1

    :cond_1
    move-object v5, p4

    :goto_1
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v6, p5

    invoke-virtual/range {v1 .. v6}, Ll2/a;->b(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lm5/e;)V

    return-void
.end method


# virtual methods
.method public final b(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lm5/e;)V
    .locals 9

    const-string v0, "activity"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "installProcess"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    invoke-direct {p0, p1, p5}, Ll2/a;->a(Landroid/app/Activity;Lm5/e;)Z

    move-result p5

    xor-int/lit8 p5, p5, 0x1

    invoke-static {p5}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object p5

    const-string v1, "pass_authorize_popup"

    invoke-interface {v0, v1, p5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string p5, "install_prepare"

    invoke-static {p5, p2}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide p2

    sput-wide p2, Ll2/a;->b:J

    new-instance p2, Lp5/h;

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object v4, p1

    check-cast v4, Lo5/a;

    const/4 v5, 0x3

    const/4 v6, 0x0

    move-object v1, p2

    invoke-direct/range {v1 .. v6}, Lp5/h;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;ILm8/g;)V

    invoke-virtual {p2, v0}, Lp5/f;->g(Ljava/util/Map;)Lp5/f;

    move-result-object p1

    invoke-virtual {p1}, Lp5/f;->c()Z

    goto :goto_0

    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    sget-wide v3, Ll2/a;->b:J

    sub-long/2addr v1, v3

    new-instance p2, Lp5/h;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v6, p1

    check-cast v6, Lo5/a;

    const/4 v7, 0x3

    const/4 v8, 0x0

    move-object v3, p2

    invoke-direct/range {v3 .. v8}, Lp5/h;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;ILm8/g;)V

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p1

    const-string p5, "duration"

    invoke-virtual {p2, p5, p1}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p1

    invoke-virtual {p1, v0}, Lp5/f;->g(Ljava/util/Map;)Lp5/f;

    move-result-object p1

    if-eqz p3, :cond_1

    const-string p2, "install_type"

    invoke-virtual {p1, p2, p3}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    :cond_1
    if-eqz p4, :cond_2

    const-string p2, "install_fail_code"

    invoke-virtual {p1, p2, p4}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    :cond_2
    invoke-virtual {p1}, Lp5/f;->c()Z

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide p1

    sput-wide p1, Ll2/a;->b:J

    :goto_0
    return-void
.end method
