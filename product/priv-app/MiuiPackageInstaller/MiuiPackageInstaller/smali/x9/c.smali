.class public Lx9/c;
.super Ljava/lang/Object;

# interfaces
.implements Lx9/b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lx9/c$b;,
        Lx9/c$a;,
        Lx9/c$c;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private d(Landroid/view/View;Lx9/c$a;)V
    .locals 9

    sget-object v0, Lh9/h;->o:Lh9/h;

    const/4 v1, 0x2

    new-array v2, v1, [F

    fill-array-data v2, :array_0

    invoke-static {v0, v2}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v0

    invoke-direct {p0, p1}, Lx9/c;->f(Landroid/view/View;)F

    move-result v2

    sget-object v3, Lh9/h;->e:Lh9/h;

    new-array v4, v1, [F

    const/4 v5, 0x0

    const/high16 v6, 0x3f800000    # 1.0f

    aput v6, v4, v5

    const/4 v7, 0x1

    aput v2, v4, v7

    invoke-static {v3, v4}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v3

    sget-object v4, Lh9/h;->f:Lh9/h;

    new-array v8, v1, [F

    aput v6, v8, v5

    aput v2, v8, v7

    invoke-static {v4, v8}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v2

    const/4 v4, 0x3

    new-array v4, v4, [Landroid/animation/PropertyValuesHolder;

    aput-object v0, v4, v5

    aput-object v3, v4, v7

    aput-object v2, v4, v1

    invoke-static {p1, v4}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object p1

    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v1, 0x3fc00000    # 1.5f

    invoke-direct {v0, v1}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-virtual {p1, v0}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    invoke-virtual {p1, p2}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    const-wide/16 v0, 0xc8

    invoke-virtual {p1, v0, v1}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    invoke-virtual {p1}, Landroid/animation/ObjectAnimator;->start()V

    return-void

    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method private e(Landroid/view/View;Lmiuix/appcompat/app/i$d;)V
    .locals 9

    sget-object v0, Lh9/h;->o:Lh9/h;

    const/4 v1, 0x2

    new-array v2, v1, [F

    fill-array-data v2, :array_0

    invoke-static {v0, v2}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v0

    invoke-direct {p0, p1}, Lx9/c;->f(Landroid/view/View;)F

    move-result v2

    sget-object v3, Lh9/h;->e:Lh9/h;

    new-array v4, v1, [F

    const/4 v5, 0x0

    aput v2, v4, v5

    const/4 v6, 0x1

    const/high16 v7, 0x3f800000    # 1.0f

    aput v7, v4, v6

    invoke-static {v3, v4}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v3

    sget-object v4, Lh9/h;->f:Lh9/h;

    new-array v8, v1, [F

    aput v2, v8, v5

    aput v7, v8, v6

    invoke-static {v4, v8}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v2

    const/4 v4, 0x3

    new-array v4, v4, [Landroid/animation/PropertyValuesHolder;

    aput-object v0, v4, v5

    aput-object v3, v4, v6

    aput-object v2, v4, v1

    invoke-static {p1, v4}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v2, 0x3fc00000    # 1.5f

    invoke-direct {v1, v2}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    new-instance v1, Lx9/c$c;

    invoke-direct {v1, p0, p2, p1}, Lx9/c$c;-><init>(Lx9/c;Lmiuix/appcompat/app/i$d;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    const-wide/16 p1, 0x12c

    invoke-virtual {v0, p1, p2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    return-void

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private f(Landroid/view/View;)F
    .locals 1

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result p1

    invoke-static {v0, p1}, Ljava/lang/Math;->max(II)I

    move-result p1

    int-to-float p1, p1

    const/high16 v0, 0x42700000    # 60.0f

    div-float/2addr v0, p1

    const/high16 p1, 0x3f800000    # 1.0f

    sub-float/2addr p1, v0

    const v0, 0x3f4ccccd    # 0.8f

    invoke-static {v0, p1}, Ljava/lang/Math;->max(FF)F

    move-result p1

    return p1
.end method

.method private g(ZZLandroid/view/View;)Ld9/a;
    .locals 3

    new-instance v0, Ld9/a;

    invoke-direct {v0}, Ld9/a;-><init>()V

    const/high16 v1, 0x3f800000    # 1.0f

    if-eqz p1, :cond_1

    if-eqz p2, :cond_2

    :cond_0
    invoke-direct {p0, p3}, Lx9/c;->f(Landroid/view/View;)F

    move-result v1

    goto :goto_0

    :cond_1
    if-eqz p2, :cond_0

    :cond_2
    :goto_0
    if-eqz p1, :cond_3

    xor-int/lit8 p2, p2, 0x1

    :cond_3
    sget-object p1, Lh9/h;->e:Lh9/h;

    float-to-double v1, v1

    invoke-virtual {v0, p1, v1, v2}, Ld9/a;->a(Ljava/lang/Object;D)Ld9/a;

    sget-object p1, Lh9/h;->f:Lh9/h;

    invoke-virtual {v0, p1, v1, v2}, Ld9/a;->a(Ljava/lang/Object;D)Ld9/a;

    sget-object p1, Lh9/h;->o:Lh9/h;

    int-to-double p2, p2

    invoke-virtual {v0, p1, p2, p3}, Ld9/a;->a(Ljava/lang/Object;D)Ld9/a;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 0

    return-void
.end method

.method public b(Landroid/view/View;Landroid/view/View;Lw9/a$a;)V
    .locals 2

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    const-string v1, "hide"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Lx9/c$a;

    invoke-direct {v0, p0, p3, p1}, Lx9/c$a;-><init>(Lx9/c;Lw9/a$a;Landroid/view/View;)V

    invoke-direct {p0, p1, v0}, Lx9/c;->d(Landroid/view/View;Lx9/c$a;)V

    invoke-static {p2}, Lx9/a;->a(Landroid/view/View;)V

    return-void
.end method

.method public c(Landroid/view/View;Landroid/view/View;ZLmiuix/appcompat/app/i$d;)V
    .locals 4

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object p3

    const-string v0, "show"

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p3

    if-eqz p3, :cond_0

    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getScaleX()F

    move-result p3

    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float p3, p3, v0

    if-eqz p3, :cond_1

    invoke-virtual {p1, v0}, Landroid/view/View;->setScaleX(F)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setScaleY(F)V

    :cond_1
    invoke-static {}, Lia/c;->d()Z

    move-result p3

    if-eqz p3, :cond_2

    invoke-direct {p0, p1, p4}, Lx9/c;->e(Landroid/view/View;Lmiuix/appcompat/app/i$d;)V

    goto :goto_0

    :cond_2
    new-instance p3, Lc9/a;

    invoke-direct {p3}, Lc9/a;-><init>()V

    const/4 v0, -0x2

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    invoke-static {v0, v1}, Lj9/c;->e(I[F)Lj9/c$a;

    move-result-object v0

    invoke-virtual {p3, v0}, Lc9/a;->l(Lj9/c$a;)Lc9/a;

    const/4 v0, 0x1

    new-array v1, v0, [Lf9/b;

    new-instance v2, Lx9/c$b;

    invoke-direct {v2, p0, p4, p1}, Lx9/c$b;-><init>(Lx9/c;Lmiuix/appcompat/app/i$d;Landroid/view/View;)V

    const/4 p4, 0x0

    aput-object v2, v1, p4

    invoke-virtual {p3, v1}, Lc9/a;->a([Lf9/b;)Lc9/a;

    new-array v1, v0, [Landroid/view/View;

    aput-object p1, v1, p4

    invoke-static {v1}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object v1

    invoke-interface {v1}, Lmiuix/animation/e;->b()Lmiuix/animation/h;

    move-result-object v1

    const-wide/16 v2, 0x1

    invoke-interface {v1, v2, v3}, Lmiuix/animation/h;->I(J)Lmiuix/animation/h;

    move-result-object v1

    invoke-direct {p0, v0, v0, p1}, Lx9/c;->g(ZZLandroid/view/View;)Ld9/a;

    move-result-object v2

    invoke-direct {p0, v0, p4, p1}, Lx9/c;->g(ZZLandroid/view/View;)Ld9/a;

    move-result-object p1

    new-array v0, v0, [Lc9/a;

    aput-object p3, v0, p4

    invoke-interface {v1, v2, p1, v0}, Lmiuix/animation/h;->c(Ljava/lang/Object;Ljava/lang/Object;[Lc9/a;)Lmiuix/animation/h;

    :goto_0
    invoke-static {p2}, Lx9/a;->b(Landroid/view/View;)V

    return-void

    nop

    :array_0
    .array-data 4
        0x3f4ccccd    # 0.8f
        0x3e99999a    # 0.3f
    .end array-data
.end method
