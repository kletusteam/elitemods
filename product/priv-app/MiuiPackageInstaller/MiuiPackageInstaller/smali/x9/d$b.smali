.class Lx9/d$b;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lx9/d;->c(Landroid/view/View;Landroid/view/View;ZLmiuix/appcompat/app/i$d;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/view/View;

.field final synthetic b:Z

.field final synthetic c:Lmiuix/appcompat/app/i$d;

.field final synthetic d:Landroid/view/View$OnLayoutChangeListener;

.field final synthetic e:Lx9/d;


# direct methods
.method constructor <init>(Lx9/d;Landroid/view/View;ZLmiuix/appcompat/app/i$d;Landroid/view/View$OnLayoutChangeListener;)V
    .locals 0

    iput-object p1, p0, Lx9/d$b;->e:Lx9/d;

    iput-object p2, p0, Lx9/d$b;->a:Landroid/view/View;

    iput-boolean p3, p0, Lx9/d$b;->b:Z

    iput-object p4, p0, Lx9/d$b;->c:Lmiuix/appcompat/app/i$d;

    iput-object p5, p0, Lx9/d$b;->d:Landroid/view/View$OnLayoutChangeListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 6

    invoke-virtual {p1, p0}, Landroid/view/View;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    iget-object p2, p0, Lx9/d$b;->a:Landroid/view/View;

    invoke-virtual {p2}, Landroid/view/View;->getHeight()I

    move-result p4

    const/4 p2, 0x0

    invoke-static {p1, p4, p2}, Lx9/d;->f(Landroid/view/View;IZ)V

    iget-boolean p6, p0, Lx9/d$b;->b:Z

    new-instance p7, Lx9/d$f;

    iget-object v1, p0, Lx9/d$b;->e:Lx9/d;

    iget-object v2, p0, Lx9/d$b;->c:Lmiuix/appcompat/app/i$d;

    iget-object v3, p0, Lx9/d$b;->d:Landroid/view/View$OnLayoutChangeListener;

    const/4 v5, 0x0

    move-object v0, p7

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lx9/d$f;-><init>(Lx9/d;Lmiuix/appcompat/app/i$d;Landroid/view/View$OnLayoutChangeListener;Landroid/view/View;I)V

    new-instance p8, Lx9/d$g;

    iget-object p3, p0, Lx9/d$b;->e:Lx9/d;

    iget-boolean p5, p0, Lx9/d$b;->b:Z

    invoke-direct {p8, p3, p1, p5}, Lx9/d$g;-><init>(Lx9/d;Landroid/view/View;Z)V

    const/4 p5, 0x0

    move-object p3, p1

    invoke-static/range {p3 .. p8}, Lx9/d;->g(Landroid/view/View;IIZLx9/d$f;Lx9/d$g;)V

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method
