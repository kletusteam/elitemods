.class Lx9/d$c;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lx9/d;->c(Landroid/view/View;Landroid/view/View;ZLmiuix/appcompat/app/i$d;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Z

.field final synthetic b:Lmiuix/appcompat/app/i$d;

.field final synthetic c:Landroid/view/View$OnLayoutChangeListener;

.field final synthetic d:Lx9/d;


# direct methods
.method constructor <init>(Lx9/d;ZLmiuix/appcompat/app/i$d;Landroid/view/View$OnLayoutChangeListener;)V
    .locals 0

    iput-object p1, p0, Lx9/d$c;->d:Lx9/d;

    iput-boolean p2, p0, Lx9/d$c;->a:Z

    iput-object p3, p0, Lx9/d$c;->b:Lmiuix/appcompat/app/i$d;

    iput-object p4, p0, Lx9/d$c;->c:Landroid/view/View$OnLayoutChangeListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 6

    invoke-virtual {p1, p0}, Landroid/view/View;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    sub-int p2, p5, p3

    const/4 p3, 0x0

    invoke-static {p1, p2, p3}, Lx9/d;->f(Landroid/view/View;IZ)V

    iget-boolean p4, p0, Lx9/d$c;->a:Z

    new-instance p5, Lx9/d$f;

    iget-object v1, p0, Lx9/d$c;->d:Lx9/d;

    iget-object v2, p0, Lx9/d$c;->b:Lmiuix/appcompat/app/i$d;

    iget-object v3, p0, Lx9/d$c;->c:Landroid/view/View$OnLayoutChangeListener;

    const/4 v5, 0x0

    move-object v0, p5

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lx9/d$f;-><init>(Lx9/d;Lmiuix/appcompat/app/i$d;Landroid/view/View$OnLayoutChangeListener;Landroid/view/View;I)V

    new-instance p6, Lx9/d$g;

    iget-object p3, p0, Lx9/d$c;->d:Lx9/d;

    iget-boolean p7, p0, Lx9/d$c;->a:Z

    invoke-direct {p6, p3, p1, p7}, Lx9/d$g;-><init>(Lx9/d;Landroid/view/View;Z)V

    const/4 p3, 0x0

    invoke-static/range {p1 .. p6}, Lx9/d;->g(Landroid/view/View;IIZLx9/d$f;Lx9/d$g;)V

    return-void
.end method
