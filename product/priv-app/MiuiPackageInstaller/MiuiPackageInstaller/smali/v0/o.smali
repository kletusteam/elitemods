.class public final enum Lv0/o;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lv0/o;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lv0/o;

.field public static final enum b:Lv0/o;

.field public static final enum c:Lv0/o;

.field private static final synthetic d:[Lv0/o;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    new-instance v0, Lv0/o;

    const-string v1, "AUTOMATIC"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lv0/o;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lv0/o;->a:Lv0/o;

    new-instance v1, Lv0/o;

    const-string v3, "HARDWARE"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lv0/o;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lv0/o;->b:Lv0/o;

    new-instance v3, Lv0/o;

    const-string v5, "SOFTWARE"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lv0/o;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lv0/o;->c:Lv0/o;

    const/4 v5, 0x3

    new-array v5, v5, [Lv0/o;

    aput-object v0, v5, v2

    aput-object v1, v5, v4

    aput-object v3, v5, v6

    sput-object v5, Lv0/o;->d:[Lv0/o;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lv0/o;
    .locals 1

    const-class v0, Lv0/o;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lv0/o;

    return-object p0
.end method

.method public static values()[Lv0/o;
    .locals 1

    sget-object v0, Lv0/o;->d:[Lv0/o;

    invoke-virtual {v0}, [Lv0/o;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lv0/o;

    return-object v0
.end method
