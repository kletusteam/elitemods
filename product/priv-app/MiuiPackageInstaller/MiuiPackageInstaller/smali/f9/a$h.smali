.class Lf9/a$h;
.super Ljava/lang/Object;

# interfaces
.implements Lf9/a$d;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf9/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "h"
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private b(Ljava/lang/Object;Lf9/b;Lf9/c;)V
    .locals 9

    iget-object v0, p3, Lf9/c;->a:Lh9/b;

    invoke-virtual {p3}, Lf9/c;->c()F

    move-result v1

    iget-boolean v2, p3, Lf9/c;->e:Z

    invoke-virtual {p2, p1, v0, v1, v2}, Lf9/b;->h(Ljava/lang/Object;Lh9/b;FZ)V

    iget-boolean v0, p3, Lf9/c;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p3, Lf9/c;->a:Lh9/b;

    move-object v5, v0

    check-cast v5, Lh9/c;

    invoke-virtual {p3}, Lf9/c;->d()I

    move-result v6

    iget-wide v0, p3, Lf9/c;->c:D

    double-to-float v7, v0

    iget-boolean v8, p3, Lf9/c;->e:Z

    move-object v3, p2

    move-object v4, p1

    invoke-virtual/range {v3 .. v8}, Lf9/b;->i(Ljava/lang/Object;Lh9/c;IFZ)V

    goto :goto_0

    :cond_0
    iget-object v5, p3, Lf9/c;->a:Lh9/b;

    invoke-virtual {p3}, Lf9/c;->c()F

    move-result v6

    iget-wide v0, p3, Lf9/c;->c:D

    double-to-float v7, v0

    iget-boolean v8, p3, Lf9/c;->e:Z

    move-object v3, p2

    move-object v4, p1

    invoke-virtual/range {v3 .. v8}, Lf9/b;->g(Ljava/lang/Object;Lh9/b;FFZ)V

    :goto_0
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;Lf9/b;Ljava/util/Collection;Lf9/c;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Lf9/b;",
            "Ljava/util/Collection<",
            "Lf9/c;",
            ">;",
            "Lf9/c;",
            ")V"
        }
    .end annotation

    if-eqz p3, :cond_0

    invoke-interface {p3}, Ljava/util/Collection;->size()I

    move-result p4

    const/16 v0, 0xfa0

    if-gt p4, v0, :cond_0

    invoke-interface {p3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p4

    :goto_0
    invoke-interface {p4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf9/c;

    invoke-direct {p0, p1, p2, v0}, Lf9/a$h;->b(Ljava/lang/Object;Lf9/b;Lf9/c;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p2, p1, p3}, Lf9/b;->j(Ljava/lang/Object;Ljava/util/Collection;)V

    return-void
.end method
