.class public Lf9/c;
.super Ljava/lang/Object;


# instance fields
.field public final a:Lh9/b;

.field public final b:Z

.field public volatile c:D

.field public volatile d:I

.field public volatile e:Z

.field public final f:Le9/c;


# direct methods
.method public constructor <init>(Lh9/b;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Le9/c;

    invoke-direct {v0}, Le9/c;-><init>()V

    iput-object v0, p0, Lf9/c;->f:Le9/c;

    iput-object p1, p0, Lf9/c;->a:Lh9/b;

    instance-of p1, p1, Lh9/c;

    iput-boolean p1, p0, Lf9/c;->b:Z

    return-void
.end method

.method public static a(Ljava/util/Collection;Lh9/b;)Lf9/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lf9/c;",
            ">;",
            "Lh9/b;",
            ")",
            "Lf9/c;"
        }
    .end annotation

    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf9/c;

    iget-object v1, v0, Lf9/c;->a:Lh9/b;

    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    return-object v0

    :cond_1
    const/4 p0, 0x0

    return-object p0
.end method

.method public static b(Ljava/util/Collection;Ljava/lang/String;)Lf9/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lf9/c;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lf9/c;"
        }
    .end annotation

    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lf9/c;

    iget-object v1, v0, Lf9/c;->a:Lh9/b;

    invoke-virtual {v1}, Landroid/util/Property;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    return-object v0

    :cond_1
    const/4 p0, 0x0

    return-object p0
.end method


# virtual methods
.method public c()F
    .locals 5

    iget-object v0, p0, Lf9/c;->f:Le9/c;

    iget-wide v0, v0, Le9/c;->j:D

    const-wide v2, 0x7fefffffffffffffL    # Double.MAX_VALUE

    cmpl-double v4, v0, v2

    if-eqz v4, :cond_0

    double-to-float v0, v0

    return v0

    :cond_0
    iget-object v0, p0, Lf9/c;->f:Le9/c;

    iget-wide v0, v0, Le9/c;->i:D

    cmpl-double v0, v0, v2

    if-nez v0, :cond_1

    const v0, 0x7f7fffff    # Float.MAX_VALUE

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lf9/c;->f:Le9/c;

    iget-wide v0, v0, Le9/c;->i:D

    double-to-float v0, v0

    :goto_0
    return v0
.end method

.method public d()I
    .locals 5

    iget-object v0, p0, Lf9/c;->f:Le9/c;

    iget-wide v0, v0, Le9/c;->j:D

    const-wide v2, 0x7fefffffffffffffL    # Double.MAX_VALUE

    cmpl-double v4, v0, v2

    if-eqz v4, :cond_0

    double-to-int v0, v0

    return v0

    :cond_0
    iget-object v0, p0, Lf9/c;->f:Le9/c;

    iget-wide v0, v0, Le9/c;->i:D

    cmpl-double v0, v0, v2

    if-nez v0, :cond_1

    const v0, 0x7fffffff

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lf9/c;->f:Le9/c;

    iget-wide v0, v0, Le9/c;->i:D

    double-to-int v0, v0

    :goto_0
    return v0
.end method

.method public e(B)V
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-eqz p1, :cond_1

    const/4 v2, 0x2

    if-le p1, v2, :cond_0

    goto :goto_0

    :cond_0
    move v2, v0

    goto :goto_1

    :cond_1
    :goto_0
    move v2, v1

    :goto_1
    iput-boolean v2, p0, Lf9/c;->e:Z

    iget-boolean v2, p0, Lf9/c;->e:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lf9/c;->f:Le9/c;

    iget-byte v2, v2, Le9/c;->a:B

    invoke-static {v2}, Le9/i;->e(B)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lf9/c;->f:Le9/c;

    iput-boolean v1, v2, Le9/c;->k:Z

    :cond_2
    iget-object v1, p0, Lf9/c;->f:Le9/c;

    iput-byte p1, v1, Le9/c;->a:B

    invoke-static {}, Lj9/f;->d()Z

    move-result v1

    if-eqz v1, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "---- UpdateInfo setOp "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " justEnd "

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lf9/c;->f:Le9/c;

    iget-boolean p1, p1, Le9/c;->k:Z

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string p1, " isCompleted "

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean p1, p0, Lf9/c;->e:Z

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1, v0}, Lj9/f;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_3
    return-void
.end method

.method public f(Lmiuix/animation/b;)V
    .locals 2

    iget-boolean v0, p0, Lf9/c;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf9/c;->a:Lh9/b;

    check-cast v0, Lh9/c;

    invoke-virtual {p0}, Lf9/c;->d()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lmiuix/animation/b;->p(Lh9/c;I)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lf9/c;->a:Lh9/b;

    invoke-virtual {p0}, Lf9/c;->c()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lmiuix/animation/b;->s(Lh9/b;F)V

    :goto_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "UpdateInfo{, id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", property="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lf9/c;->a:Lh9/b;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", velocity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lf9/c;->c:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string v1, ", value = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lf9/c;->f:Le9/c;

    iget-wide v1, v1, Le9/c;->i:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string v1, ", useInt="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lf9/c;->b:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", frameCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lf9/c;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", isCompleted="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lf9/c;->e:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
