.class public Lf9/a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf9/a$c;,
        Lf9/a$b;,
        Lf9/a$g;,
        Lf9/a$h;,
        Lf9/a$e;,
        Lf9/a$f;,
        Lf9/a$a;,
        Lf9/a$d;
    }
.end annotation


# static fields
.field static final c:Lf9/a$a;

.field static final d:Lf9/a$f;

.field static final e:Lf9/a$e;

.field static final f:Lf9/a$h;

.field static final g:Lf9/a$g;

.field static final h:Lf9/a$b;

.field static final i:Lf9/a$c;


# instance fields
.field final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Object;",
            "Ljava/util/List<",
            "Lf9/b;",
            ">;>;"
        }
    .end annotation
.end field

.field final b:Lmiuix/animation/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lf9/a$a;

    invoke-direct {v0}, Lf9/a$a;-><init>()V

    sput-object v0, Lf9/a;->c:Lf9/a$a;

    new-instance v0, Lf9/a$f;

    invoke-direct {v0}, Lf9/a$f;-><init>()V

    sput-object v0, Lf9/a;->d:Lf9/a$f;

    new-instance v0, Lf9/a$e;

    invoke-direct {v0}, Lf9/a$e;-><init>()V

    sput-object v0, Lf9/a;->e:Lf9/a$e;

    new-instance v0, Lf9/a$h;

    invoke-direct {v0}, Lf9/a$h;-><init>()V

    sput-object v0, Lf9/a;->f:Lf9/a$h;

    new-instance v0, Lf9/a$g;

    invoke-direct {v0}, Lf9/a$g;-><init>()V

    sput-object v0, Lf9/a;->g:Lf9/a$g;

    new-instance v0, Lf9/a$b;

    invoke-direct {v0}, Lf9/a$b;-><init>()V

    sput-object v0, Lf9/a;->h:Lf9/a$b;

    new-instance v0, Lf9/a$c;

    invoke-direct {v0}, Lf9/a$c;-><init>()V

    sput-object v0, Lf9/a;->i:Lf9/a$c;

    return-void
.end method

.method public constructor <init>(Lmiuix/animation/b;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lf9/a;->a:Ljava/util/Map;

    iput-object p1, p0, Lf9/a;->b:Lmiuix/animation/b;

    return-void
.end method

.method private b(Ljava/lang/Object;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Ljava/util/List<",
            "Lf9/b;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lf9/a;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-nez v0, :cond_0

    const-class v0, Ljava/util/ArrayList;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lj9/g;->b(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iget-object v1, p0, Lf9/a;->a:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object v0
.end method

.method private c(Ljava/lang/Object;Ljava/lang/Object;Lf9/a$d;Ljava/util/Collection;Lf9/c;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            "Lf9/a$d;",
            "Ljava/util/Collection<",
            "Lf9/c;",
            ">;",
            "Lf9/c;",
            ")V"
        }
    .end annotation

    iget-object v0, p0, Lf9/a;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p2, p1, p3, p4, p5}, Lf9/a;->g(Ljava/lang/Object;Ljava/util/List;Lf9/a$d;Ljava/util/Collection;Lf9/c;)V

    :cond_0
    return-void
.end method

.method private static g(Ljava/lang/Object;Ljava/util/List;Lf9/a$d;Ljava/util/Collection;Lf9/c;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/util/List<",
            "Lf9/b;",
            ">;",
            "Lf9/a$d;",
            "Ljava/util/Collection<",
            "Lf9/c;",
            ">;",
            "Lf9/c;",
            ")V"
        }
    .end annotation

    const-class v0, Ljava/util/HashSet;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lj9/g;->b(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf9/b;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p2, p0, v1, p3, p4}, Lf9/a$d;->a(Ljava/lang/Object;Lf9/b;Ljava/util/Collection;Lf9/c;)V

    goto :goto_0

    :cond_1
    invoke-static {v0}, Lj9/g;->g(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;Lc9/a;)Z
    .locals 1

    iget-object v0, p2, Lc9/a;->i:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    invoke-direct {p0, p1}, Lf9/a;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    iget-object p2, p2, Lc9/a;->i:Ljava/util/HashSet;

    invoke-static {p2, p1}, Lj9/a;->a(Ljava/util/Collection;Ljava/util/Collection;)V

    const/4 p1, 0x1

    return p1
.end method

.method public d(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 6

    sget-object v3, Lf9/a;->c:Lf9/a$a;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lf9/a;->c(Ljava/lang/Object;Ljava/lang/Object;Lf9/a$d;Ljava/util/Collection;Lf9/c;)V

    return-void
.end method

.method public e(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 6

    sget-object v3, Lf9/a;->h:Lf9/a$b;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lf9/a;->c(Ljava/lang/Object;Ljava/lang/Object;Lf9/a$d;Ljava/util/Collection;Lf9/c;)V

    return-void
.end method

.method public f(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 6

    sget-object v3, Lf9/a;->i:Lf9/a$c;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lf9/a;->c(Ljava/lang/Object;Ljava/lang/Object;Lf9/a$d;Ljava/util/Collection;Lf9/c;)V

    return-void
.end method

.method public h(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 6

    sget-object v3, Lf9/a;->e:Lf9/a$e;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lf9/a;->c(Ljava/lang/Object;Ljava/lang/Object;Lf9/a$d;Ljava/util/Collection;Lf9/c;)V

    return-void
.end method

.method public i(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Collection;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            "Ljava/util/Collection<",
            "Lf9/c;",
            ">;)V"
        }
    .end annotation

    sget-object v3, Lf9/a;->d:Lf9/a$f;

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lf9/a;->c(Ljava/lang/Object;Ljava/lang/Object;Lf9/a$d;Ljava/util/Collection;Lf9/c;)V

    return-void
.end method

.method public j(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Collection;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            "Ljava/util/Collection<",
            "Lf9/c;",
            ">;)V"
        }
    .end annotation

    sget-object v3, Lf9/a;->g:Lf9/a$g;

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lf9/a;->c(Ljava/lang/Object;Ljava/lang/Object;Lf9/a$d;Ljava/util/Collection;Lf9/c;)V

    return-void
.end method

.method public k(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Collection;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            "Ljava/util/Collection<",
            "Lf9/c;",
            ">;)V"
        }
    .end annotation

    sget-object v3, Lf9/a;->f:Lf9/a$h;

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lf9/a;->c(Ljava/lang/Object;Ljava/lang/Object;Lf9/a$d;Ljava/util/Collection;Lf9/c;)V

    return-void
.end method

.method public l()V
    .locals 1

    iget-object v0, p0, Lf9/a;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, Lj9/g;->g(Ljava/lang/Object;)V

    iget-object v0, p0, Lf9/a;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    return-void
.end method

.method public m(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lf9/a;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    invoke-static {p1}, Lj9/g;->g(Ljava/lang/Object;)V

    return-void
.end method
