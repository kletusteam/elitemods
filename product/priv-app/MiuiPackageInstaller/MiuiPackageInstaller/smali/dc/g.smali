.class public interface abstract Ldc/g;
.super Ljava/lang/Object;

# interfaces
.implements Ldc/y;
.implements Ljava/nio/channels/ReadableByteChannel;


# virtual methods
.method public abstract E(J)Ljava/lang/String;
.end method

.method public abstract H(Ldc/h;)J
.end method

.method public abstract N(J)V
.end method

.method public abstract Q()J
.end method

.method public abstract R(Ljava/nio/charset/Charset;)Ljava/lang/String;
.end method

.method public abstract S()Ljava/io/InputStream;
.end method

.method public abstract T(Ldc/p;)I
.end method

.method public abstract a(J)V
.end method

.method public abstract c()Ldc/e;
.end method

.method public abstract i(J)Ldc/h;
.end method

.method public abstract j(Ldc/w;)J
.end method

.method public abstract l(J)Z
.end method

.method public abstract readByte()B
.end method

.method public abstract readInt()I
.end method

.method public abstract readShort()S
.end method

.method public abstract t()Ljava/lang/String;
.end method

.method public abstract u()Z
.end method

.method public abstract w(Ldc/h;)J
.end method

.method public abstract x(J)[B
.end method
