.class public final Ldc/o;
.super Ljava/lang/Object;


# direct methods
.method public static final a(Ldc/w;)Ldc/f;
    .locals 1

    const-string v0, "$this$buffer"

    invoke-static {p0, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Ldc/r;

    invoke-direct {v0, p0}, Ldc/r;-><init>(Ldc/w;)V

    return-object v0
.end method

.method public static final b(Ldc/y;)Ldc/g;
    .locals 1

    const-string v0, "$this$buffer"

    invoke-static {p0, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Ldc/s;

    invoke-direct {v0, p0}, Ldc/s;-><init>(Ldc/y;)V

    return-object v0
.end method

.method public static final c(Ljava/lang/AssertionError;)Z
    .locals 4

    const-string v0, "$this$isAndroidGetsocknameError"

    invoke-static {p0, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/AssertionError;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Ljava/lang/AssertionError;->getMessage()Ljava/lang/String;

    move-result-object p0

    if-eqz p0, :cond_0

    const/4 v0, 0x2

    const/4 v2, 0x0

    const-string v3, "getsockname failed"

    invoke-static {p0, v3, v1, v0, v2}, Lu8/g;->A(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZILjava/lang/Object;)Z

    move-result p0

    goto :goto_0

    :cond_0
    move p0, v1

    :goto_0
    if-eqz p0, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method public static final d(Ljava/net/Socket;)Ldc/w;
    .locals 3

    const-string v0, "$this$sink"

    invoke-static {p0, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Ldc/x;

    invoke-direct {v0, p0}, Ldc/x;-><init>(Ljava/net/Socket;)V

    new-instance v1, Ldc/q;

    invoke-virtual {p0}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object p0

    const-string v2, "getOutputStream()"

    invoke-static {p0, v2}, Lm8/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v1, p0, v0}, Ldc/q;-><init>(Ljava/io/OutputStream;Ldc/z;)V

    invoke-virtual {v0, v1}, Ldc/d;->v(Ldc/w;)Ldc/w;

    move-result-object p0

    return-object p0
.end method

.method public static final e(Ljava/io/InputStream;)Ldc/y;
    .locals 2

    const-string v0, "$this$source"

    invoke-static {p0, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Ldc/n;

    new-instance v1, Ldc/z;

    invoke-direct {v1}, Ldc/z;-><init>()V

    invoke-direct {v0, p0, v1}, Ldc/n;-><init>(Ljava/io/InputStream;Ldc/z;)V

    return-object v0
.end method

.method public static final f(Ljava/net/Socket;)Ldc/y;
    .locals 3

    const-string v0, "$this$source"

    invoke-static {p0, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Ldc/x;

    invoke-direct {v0, p0}, Ldc/x;-><init>(Ljava/net/Socket;)V

    new-instance v1, Ldc/n;

    invoke-virtual {p0}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object p0

    const-string v2, "getInputStream()"

    invoke-static {p0, v2}, Lm8/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v1, p0, v0}, Ldc/n;-><init>(Ljava/io/InputStream;Ldc/z;)V

    invoke-virtual {v0, v1}, Ldc/d;->w(Ldc/y;)Ldc/y;

    move-result-object p0

    return-object p0
.end method
