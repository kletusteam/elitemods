.class public Ldc/k;
.super Ldc/z;


# instance fields
.field private f:Ldc/z;


# direct methods
.method public constructor <init>(Ldc/z;)V
    .locals 1

    const-string v0, "delegate"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ldc/z;-><init>()V

    iput-object p1, p0, Ldc/k;->f:Ldc/z;

    return-void
.end method


# virtual methods
.method public a()Ldc/z;
    .locals 1

    iget-object v0, p0, Ldc/k;->f:Ldc/z;

    invoke-virtual {v0}, Ldc/z;->a()Ldc/z;

    move-result-object v0

    return-object v0
.end method

.method public b()Ldc/z;
    .locals 1

    iget-object v0, p0, Ldc/k;->f:Ldc/z;

    invoke-virtual {v0}, Ldc/z;->b()Ldc/z;

    move-result-object v0

    return-object v0
.end method

.method public c()J
    .locals 2

    iget-object v0, p0, Ldc/k;->f:Ldc/z;

    invoke-virtual {v0}, Ldc/z;->c()J

    move-result-wide v0

    return-wide v0
.end method

.method public d(J)Ldc/z;
    .locals 1

    iget-object v0, p0, Ldc/k;->f:Ldc/z;

    invoke-virtual {v0, p1, p2}, Ldc/z;->d(J)Ldc/z;

    move-result-object p1

    return-object p1
.end method

.method public e()Z
    .locals 1

    iget-object v0, p0, Ldc/k;->f:Ldc/z;

    invoke-virtual {v0}, Ldc/z;->e()Z

    move-result v0

    return v0
.end method

.method public f()V
    .locals 1

    iget-object v0, p0, Ldc/k;->f:Ldc/z;

    invoke-virtual {v0}, Ldc/z;->f()V

    return-void
.end method

.method public g(JLjava/util/concurrent/TimeUnit;)Ldc/z;
    .locals 1

    const-string v0, "unit"

    invoke-static {p3, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Ldc/k;->f:Ldc/z;

    invoke-virtual {v0, p1, p2, p3}, Ldc/z;->g(JLjava/util/concurrent/TimeUnit;)Ldc/z;

    move-result-object p1

    return-object p1
.end method

.method public final i()Ldc/z;
    .locals 1

    iget-object v0, p0, Ldc/k;->f:Ldc/z;

    return-object v0
.end method

.method public final j(Ldc/z;)Ldc/k;
    .locals 1

    const-string v0, "delegate"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Ldc/k;->f:Ldc/z;

    return-object p0
.end method
