.class public final Ldc/r;
.super Ljava/lang/Object;

# interfaces
.implements Ldc/f;


# instance fields
.field public final a:Ldc/e;

.field public b:Z

.field public final c:Ldc/w;


# direct methods
.method public constructor <init>(Ldc/w;)V
    .locals 1

    const-string v0, "sink"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Ldc/r;->c:Ldc/w;

    new-instance p1, Ldc/e;

    invoke-direct {p1}, Ldc/e;-><init>()V

    iput-object p1, p0, Ldc/r;->a:Ldc/e;

    return-void
.end method


# virtual methods
.method public A([B)Ldc/f;
    .locals 1

    const-string v0, "source"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    iget-boolean v0, p0, Ldc/r;->b:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldc/r;->a:Ldc/e;

    invoke-virtual {v0, p1}, Ldc/e;->i0([B)Ldc/e;

    invoke-virtual {p0}, Ldc/r;->b()Ldc/f;

    move-result-object p1

    return-object p1

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "closed"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public G(Ldc/h;)Ldc/f;
    .locals 1

    const-string v0, "byteString"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    iget-boolean v0, p0, Ldc/r;->b:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldc/r;->a:Ldc/e;

    invoke-virtual {v0, p1}, Ldc/e;->h0(Ldc/h;)Ldc/e;

    invoke-virtual {p0}, Ldc/r;->b()Ldc/f;

    move-result-object p1

    return-object p1

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "closed"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public I(Ldc/e;J)V
    .locals 1

    const-string v0, "source"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    iget-boolean v0, p0, Ldc/r;->b:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldc/r;->a:Ldc/e;

    invoke-virtual {v0, p1, p2, p3}, Ldc/e;->I(Ldc/e;J)V

    invoke-virtual {p0}, Ldc/r;->b()Ldc/f;

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "closed"

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public O(Ljava/lang/String;)Ldc/f;
    .locals 1

    const-string v0, "string"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    iget-boolean v0, p0, Ldc/r;->b:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldc/r;->a:Ldc/e;

    invoke-virtual {v0, p1}, Ldc/e;->r0(Ljava/lang/String;)Ldc/e;

    invoke-virtual {p0}, Ldc/r;->b()Ldc/f;

    move-result-object p1

    return-object p1

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "closed"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public P(J)Ldc/f;
    .locals 1

    iget-boolean v0, p0, Ldc/r;->b:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldc/r;->a:Ldc/e;

    invoke-virtual {v0, p1, p2}, Ldc/e;->m0(J)Ldc/e;

    invoke-virtual {p0}, Ldc/r;->b()Ldc/f;

    move-result-object p1

    return-object p1

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "closed"

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public b()Ldc/f;
    .locals 4

    iget-boolean v0, p0, Ldc/r;->b:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    iget-object v0, p0, Ldc/r;->a:Ldc/e;

    invoke-virtual {v0}, Ldc/e;->n()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    iget-object v2, p0, Ldc/r;->c:Ldc/w;

    iget-object v3, p0, Ldc/r;->a:Ldc/e;

    invoke-interface {v2, v3, v0, v1}, Ldc/w;->I(Ldc/e;J)V

    :cond_0
    return-object p0

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public c()Ldc/e;
    .locals 1

    iget-object v0, p0, Ldc/r;->a:Ldc/e;

    return-object v0
.end method

.method public close()V
    .locals 5

    iget-boolean v0, p0, Ldc/r;->b:Z

    if-eqz v0, :cond_0

    goto :goto_2

    :cond_0
    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Ldc/r;->a:Ldc/e;

    invoke-virtual {v1}, Ldc/e;->d0()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-lez v1, :cond_1

    iget-object v1, p0, Ldc/r;->c:Ldc/w;

    iget-object v2, p0, Ldc/r;->a:Ldc/e;

    invoke-virtual {v2}, Ldc/e;->d0()J

    move-result-wide v3

    invoke-interface {v1, v2, v3, v4}, Ldc/w;->I(Ldc/e;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    :cond_1
    :goto_0
    :try_start_1
    iget-object v1, p0, Ldc/r;->c:Ldc/w;

    invoke-interface {v1}, Ldc/w;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v1

    if-nez v0, :cond_2

    move-object v0, v1

    :cond_2
    :goto_1
    const/4 v1, 0x1

    iput-boolean v1, p0, Ldc/r;->b:Z

    if-nez v0, :cond_3

    :goto_2
    return-void

    :cond_3
    throw v0
.end method

.method public d()Ldc/z;
    .locals 1

    iget-object v0, p0, Ldc/r;->c:Ldc/w;

    invoke-interface {v0}, Ldc/w;->d()Ldc/z;

    move-result-object v0

    return-object v0
.end method

.method public e([BII)Ldc/f;
    .locals 1

    const-string v0, "source"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    iget-boolean v0, p0, Ldc/r;->b:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldc/r;->a:Ldc/e;

    invoke-virtual {v0, p1, p2, p3}, Ldc/e;->j0([BII)Ldc/e;

    invoke-virtual {p0}, Ldc/r;->b()Ldc/f;

    move-result-object p1

    return-object p1

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "closed"

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public f(Ljava/lang/String;II)Ldc/f;
    .locals 1

    const-string v0, "string"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    iget-boolean v0, p0, Ldc/r;->b:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldc/r;->a:Ldc/e;

    invoke-virtual {v0, p1, p2, p3}, Ldc/e;->s0(Ljava/lang/String;II)Ldc/e;

    invoke-virtual {p0}, Ldc/r;->b()Ldc/f;

    move-result-object p1

    return-object p1

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "closed"

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public flush()V
    .locals 4

    iget-boolean v0, p0, Ldc/r;->b:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    iget-object v0, p0, Ldc/r;->a:Ldc/e;

    invoke-virtual {v0}, Ldc/e;->d0()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-object v0, p0, Ldc/r;->c:Ldc/w;

    iget-object v1, p0, Ldc/r;->a:Ldc/e;

    invoke-virtual {v1}, Ldc/e;->d0()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Ldc/w;->I(Ldc/e;J)V

    :cond_0
    iget-object v0, p0, Ldc/r;->c:Ldc/w;

    invoke-interface {v0}, Ldc/w;->flush()V

    return-void

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public h(J)Ldc/f;
    .locals 1

    iget-boolean v0, p0, Ldc/r;->b:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldc/r;->a:Ldc/e;

    invoke-virtual {v0, p1, p2}, Ldc/e;->n0(J)Ldc/e;

    invoke-virtual {p0}, Ldc/r;->b()Ldc/f;

    move-result-object p1

    return-object p1

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "closed"

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public isOpen()Z
    .locals 1

    iget-boolean v0, p0, Ldc/r;->b:Z

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public k(I)Ldc/f;
    .locals 1

    iget-boolean v0, p0, Ldc/r;->b:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldc/r;->a:Ldc/e;

    invoke-virtual {v0, p1}, Ldc/e;->p0(I)Ldc/e;

    invoke-virtual {p0}, Ldc/r;->b()Ldc/f;

    move-result-object p1

    return-object p1

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "closed"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public o(I)Ldc/f;
    .locals 1

    iget-boolean v0, p0, Ldc/r;->b:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldc/r;->a:Ldc/e;

    invoke-virtual {v0, p1}, Ldc/e;->o0(I)Ldc/e;

    invoke-virtual {p0}, Ldc/r;->b()Ldc/f;

    move-result-object p1

    return-object p1

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "closed"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "buffer("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Ldc/r;->c:Ldc/w;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public v(I)Ldc/f;
    .locals 1

    iget-boolean v0, p0, Ldc/r;->b:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldc/r;->a:Ldc/e;

    invoke-virtual {v0, p1}, Ldc/e;->l0(I)Ldc/e;

    invoke-virtual {p0}, Ldc/r;->b()Ldc/f;

    move-result-object p1

    return-object p1

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "closed"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public write(Ljava/nio/ByteBuffer;)I
    .locals 1

    const-string v0, "source"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    iget-boolean v0, p0, Ldc/r;->b:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldc/r;->a:Ldc/e;

    invoke-virtual {v0, p1}, Ldc/e;->write(Ljava/nio/ByteBuffer;)I

    move-result p1

    invoke-virtual {p0}, Ldc/r;->b()Ldc/f;

    return p1

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "closed"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
