.class public final Ldc/p;
.super Lb8/b;

# interfaces
.implements Ljava/util/RandomAccess;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ldc/p$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lb8/b<",
        "Ldc/h;",
        ">;",
        "Ljava/util/RandomAccess;"
    }
.end annotation


# static fields
.field public static final d:Ldc/p$a;


# instance fields
.field private final b:[Ldc/h;

.field private final c:[I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ldc/p$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ldc/p$a;-><init>(Lm8/g;)V

    sput-object v0, Ldc/p;->d:Ldc/p$a;

    return-void
.end method

.method private constructor <init>([Ldc/h;[I)V
    .locals 0

    invoke-direct {p0}, Lb8/b;-><init>()V

    iput-object p1, p0, Ldc/p;->b:[Ldc/h;

    iput-object p2, p0, Ldc/p;->c:[I

    return-void
.end method

.method public synthetic constructor <init>([Ldc/h;[ILm8/g;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ldc/p;-><init>([Ldc/h;[I)V

    return-void
.end method

.method public static final varargs h([Ldc/h;)Ldc/p;
    .locals 1

    sget-object v0, Ldc/p;->d:Ldc/p$a;

    invoke-virtual {v0, p0}, Ldc/p$a;->d([Ldc/h;)Ldc/p;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public a()I
    .locals 1

    iget-object v0, p0, Ldc/p;->b:[Ldc/h;

    array-length v0, v0

    return v0
.end method

.method public bridge b(Ldc/h;)Z
    .locals 0

    invoke-super {p0, p1}, Lb8/a;->contains(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public c(I)Ldc/h;
    .locals 1

    iget-object v0, p0, Ldc/p;->b:[Ldc/h;

    aget-object p1, v0, p1

    return-object p1
.end method

.method public final bridge contains(Ljava/lang/Object;)Z
    .locals 1

    instance-of v0, p1, Ldc/h;

    if-eqz v0, :cond_0

    check-cast p1, Ldc/h;

    invoke-virtual {p0, p1}, Ldc/p;->b(Ldc/h;)Z

    move-result p1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public final d()[Ldc/h;
    .locals 1

    iget-object v0, p0, Ldc/p;->b:[Ldc/h;

    return-object v0
.end method

.method public final e()[I
    .locals 1

    iget-object v0, p0, Ldc/p;->c:[I

    return-object v0
.end method

.method public bridge f(Ldc/h;)I
    .locals 0

    invoke-super {p0, p1}, Lb8/b;->indexOf(Ljava/lang/Object;)I

    move-result p1

    return p1
.end method

.method public bridge g(Ldc/h;)I
    .locals 0

    invoke-super {p0, p1}, Lb8/b;->lastIndexOf(Ljava/lang/Object;)I

    move-result p1

    return p1
.end method

.method public bridge synthetic get(I)Ljava/lang/Object;
    .locals 0

    invoke-virtual {p0, p1}, Ldc/p;->c(I)Ldc/h;

    move-result-object p1

    return-object p1
.end method

.method public final bridge indexOf(Ljava/lang/Object;)I
    .locals 1

    instance-of v0, p1, Ldc/h;

    if-eqz v0, :cond_0

    check-cast p1, Ldc/h;

    invoke-virtual {p0, p1}, Ldc/p;->f(Ldc/h;)I

    move-result p1

    return p1

    :cond_0
    const/4 p1, -0x1

    return p1
.end method

.method public final bridge lastIndexOf(Ljava/lang/Object;)I
    .locals 1

    instance-of v0, p1, Ldc/h;

    if-eqz v0, :cond_0

    check-cast p1, Ldc/h;

    invoke-virtual {p0, p1}, Ldc/p;->g(Ldc/h;)I

    move-result p1

    return p1

    :cond_0
    const/4 p1, -0x1

    return p1
.end method
