.class public final Ldc/d$c;
.super Ljava/lang/Object;

# interfaces
.implements Ldc/w;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Ldc/d;->v(Ldc/w;)Ldc/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Ldc/d;

.field final synthetic b:Ldc/w;


# direct methods
.method constructor <init>(Ldc/d;Ldc/w;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldc/w;",
            ")V"
        }
    .end annotation

    iput-object p1, p0, Ldc/d$c;->a:Ldc/d;

    iput-object p2, p0, Ldc/d$c;->b:Ldc/w;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public I(Ldc/e;J)V
    .locals 7

    const-string v0, "source"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ldc/e;->d0()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    move-wide v5, p2

    invoke-static/range {v1 .. v6}, Ldc/c;->b(JJJ)V

    :goto_0
    const-wide/16 v0, 0x0

    cmp-long v2, p2, v0

    if-lez v2, :cond_5

    iget-object v2, p1, Ldc/e;->a:Ldc/t;

    if-nez v2, :cond_0

    :goto_1
    invoke-static {}, Lm8/i;->o()V

    :cond_0
    const/high16 v3, 0x10000

    int-to-long v3, v3

    cmp-long v3, v0, v3

    if-gez v3, :cond_2

    iget v3, v2, Ldc/t;->c:I

    iget v4, v2, Ldc/t;->b:I

    sub-int/2addr v3, v4

    int-to-long v3, v3

    add-long/2addr v0, v3

    cmp-long v3, v0, p2

    if-ltz v3, :cond_1

    move-wide v0, p2

    goto :goto_2

    :cond_1
    iget-object v2, v2, Ldc/t;->f:Ldc/t;

    if-nez v2, :cond_0

    goto :goto_1

    :cond_2
    :goto_2
    iget-object v2, p0, Ldc/d$c;->a:Ldc/d;

    invoke-virtual {v2}, Ldc/d;->r()V

    :try_start_0
    iget-object v3, p0, Ldc/d$c;->b:Ldc/w;

    invoke-interface {v3, p1, v0, v1}, Ldc/w;->I(Ldc/e;J)V

    sget-object v3, La8/v;->a:La8/v;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v2}, Ldc/d;->s()Z

    move-result v3

    if-nez v3, :cond_3

    sub-long/2addr p2, v0

    goto :goto_0

    :cond_3
    const/4 p1, 0x0

    invoke-virtual {v2, p1}, Ldc/d;->m(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    throw p1

    :catchall_0
    move-exception p1

    goto :goto_4

    :catch_0
    move-exception p1

    :try_start_1
    invoke-virtual {v2}, Ldc/d;->s()Z

    move-result p2

    if-nez p2, :cond_4

    goto :goto_3

    :cond_4
    invoke-virtual {v2, p1}, Ldc/d;->m(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object p1

    :goto_3
    throw p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_4
    invoke-virtual {v2}, Ldc/d;->s()Z

    move-result p2

    throw p1

    :cond_5
    return-void
.end method

.method public b()Ldc/d;
    .locals 1

    iget-object v0, p0, Ldc/d$c;->a:Ldc/d;

    return-object v0
.end method

.method public close()V
    .locals 3

    iget-object v0, p0, Ldc/d$c;->a:Ldc/d;

    invoke-virtual {v0}, Ldc/d;->r()V

    :try_start_0
    iget-object v1, p0, Ldc/d$c;->b:Ldc/w;

    invoke-interface {v1}, Ldc/w;->close()V

    sget-object v1, La8/v;->a:La8/v;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v0}, Ldc/d;->s()Z

    move-result v1

    if-nez v1, :cond_0

    return-void

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ldc/d;->m(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :catchall_0
    move-exception v1

    goto :goto_1

    :catch_0
    move-exception v1

    :try_start_1
    invoke-virtual {v0}, Ldc/d;->s()Z

    move-result v2

    if-nez v2, :cond_1

    goto :goto_0

    :cond_1
    invoke-virtual {v0, v1}, Ldc/d;->m(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v1

    :goto_0
    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_1
    invoke-virtual {v0}, Ldc/d;->s()Z

    move-result v0

    throw v1
.end method

.method public bridge synthetic d()Ldc/z;
    .locals 1

    invoke-virtual {p0}, Ldc/d$c;->b()Ldc/d;

    move-result-object v0

    return-object v0
.end method

.method public flush()V
    .locals 3

    iget-object v0, p0, Ldc/d$c;->a:Ldc/d;

    invoke-virtual {v0}, Ldc/d;->r()V

    :try_start_0
    iget-object v1, p0, Ldc/d$c;->b:Ldc/w;

    invoke-interface {v1}, Ldc/w;->flush()V

    sget-object v1, La8/v;->a:La8/v;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v0}, Ldc/d;->s()Z

    move-result v1

    if-nez v1, :cond_0

    return-void

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ldc/d;->m(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :catchall_0
    move-exception v1

    goto :goto_1

    :catch_0
    move-exception v1

    :try_start_1
    invoke-virtual {v0}, Ldc/d;->s()Z

    move-result v2

    if-nez v2, :cond_1

    goto :goto_0

    :cond_1
    invoke-virtual {v0, v1}, Ldc/d;->m(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v1

    :goto_0
    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_1
    invoke-virtual {v0}, Ldc/d;->s()Z

    move-result v0

    throw v1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AsyncTimeout.sink("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Ldc/d$c;->b:Ldc/w;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
