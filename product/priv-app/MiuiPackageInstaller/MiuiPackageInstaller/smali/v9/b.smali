.class public Lv9/b;
.super Lmiuix/appcompat/internal/view/menu/c;

# interfaces
.implements Landroid/view/ContextMenu;


# instance fields
.field y:Lv9/d;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lmiuix/appcompat/internal/view/menu/c;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public W(Landroid/view/View;Landroid/os/IBinder;)Lmiuix/appcompat/internal/view/menu/d;
    .locals 1

    if-eqz p1, :cond_0

    invoke-virtual {p1, p0}, Landroid/view/View;->createContextMenu(Landroid/view/ContextMenu;)V

    :cond_0
    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/c;->B()Ljava/util/ArrayList;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result p1

    if-lez p1, :cond_1

    const p1, 0xc351

    const/4 v0, 0x1

    invoke-static {p1, v0}, Landroid/util/EventLog;->writeEvent(II)I

    new-instance p1, Lmiuix/appcompat/internal/view/menu/d;

    invoke-direct {p1, p0}, Lmiuix/appcompat/internal/view/menu/d;-><init>(Lmiuix/appcompat/internal/view/menu/c;)V

    invoke-virtual {p1, p2}, Lmiuix/appcompat/internal/view/menu/d;->d(Landroid/os/IBinder;)V

    return-object p1

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method public X(Landroid/view/View;Landroid/os/IBinder;FF)Lv9/d;
    .locals 2

    if-eqz p1, :cond_0

    invoke-virtual {p1, p0}, Landroid/view/View;->createContextMenu(Landroid/view/ContextMenu;)V

    :cond_0
    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/c;->B()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    const v0, 0xc351

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(II)I

    new-instance v0, Lv9/d;

    invoke-direct {v0, p0}, Lv9/d;-><init>(Lmiuix/appcompat/internal/view/menu/c;)V

    iput-object v0, p0, Lv9/b;->y:Lv9/d;

    invoke-virtual {v0, p2, p1, p3, p4}, Lv9/d;->c(Landroid/os/IBinder;Landroid/view/View;FF)V

    iget-object p1, p0, Lv9/b;->y:Lv9/d;

    return-object p1

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method public close()V
    .locals 1

    invoke-super {p0}, Lmiuix/appcompat/internal/view/menu/c;->close()V

    iget-object v0, p0, Lv9/b;->y:Lv9/d;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lv9/d;->a()V

    const/4 v0, 0x0

    iput-object v0, p0, Lv9/b;->y:Lv9/d;

    :cond_0
    return-void
.end method

.method public setHeaderIcon(I)Landroid/view/ContextMenu;
    .locals 0

    invoke-super {p0, p1}, Lmiuix/appcompat/internal/view/menu/c;->N(I)Lmiuix/appcompat/internal/view/menu/c;

    move-result-object p1

    check-cast p1, Landroid/view/ContextMenu;

    return-object p1
.end method

.method public setHeaderIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/ContextMenu;
    .locals 0

    invoke-super {p0, p1}, Lmiuix/appcompat/internal/view/menu/c;->O(Landroid/graphics/drawable/Drawable;)Lmiuix/appcompat/internal/view/menu/c;

    move-result-object p1

    check-cast p1, Landroid/view/ContextMenu;

    return-object p1
.end method

.method public setHeaderTitle(I)Landroid/view/ContextMenu;
    .locals 0

    invoke-super {p0, p1}, Lmiuix/appcompat/internal/view/menu/c;->Q(I)Lmiuix/appcompat/internal/view/menu/c;

    move-result-object p1

    check-cast p1, Landroid/view/ContextMenu;

    return-object p1
.end method

.method public setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/ContextMenu;
    .locals 0

    invoke-super {p0, p1}, Lmiuix/appcompat/internal/view/menu/c;->R(Ljava/lang/CharSequence;)Lmiuix/appcompat/internal/view/menu/c;

    move-result-object p1

    check-cast p1, Landroid/view/ContextMenu;

    return-object p1
.end method

.method public setHeaderView(Landroid/view/View;)Landroid/view/ContextMenu;
    .locals 0

    invoke-super {p0, p1}, Lmiuix/appcompat/internal/view/menu/c;->S(Landroid/view/View;)Lmiuix/appcompat/internal/view/menu/c;

    move-result-object p1

    check-cast p1, Landroid/view/ContextMenu;

    return-object p1
.end method
