.class public Lv9/d;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/PopupWindow$OnDismissListener;


# instance fields
.field private a:Lmiuix/appcompat/internal/view/menu/c;

.field private b:Lmiuix/appcompat/internal/view/menu/g$a;

.field private c:Lv9/c;


# direct methods
.method public constructor <init>(Lmiuix/appcompat/internal/view/menu/c;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lv9/d;->a:Lmiuix/appcompat/internal/view/menu/c;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lv9/d;->c:Lv9/c;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lv9/c;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lv9/d;->c:Lv9/c;

    :cond_0
    return-void
.end method

.method public b(Lmiuix/appcompat/internal/view/menu/g$a;)V
    .locals 0

    iput-object p1, p0, Lv9/d;->b:Lmiuix/appcompat/internal/view/menu/g$a;

    return-void
.end method

.method public c(Landroid/os/IBinder;Landroid/view/View;FF)V
    .locals 2

    new-instance p1, Lv9/e;

    iget-object v0, p0, Lv9/d;->a:Lmiuix/appcompat/internal/view/menu/c;

    invoke-virtual {v0}, Lmiuix/appcompat/internal/view/menu/c;->r()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lv9/d;->a:Lmiuix/appcompat/internal/view/menu/c;

    invoke-direct {p1, v0, v1, p0}, Lv9/e;-><init>(Landroid/content/Context;Lmiuix/appcompat/internal/view/menu/c;Landroid/widget/PopupWindow$OnDismissListener;)V

    iput-object p1, p0, Lv9/d;->c:Lv9/c;

    invoke-virtual {p2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-interface {p1, p2, v0, p3, p4}, Lv9/c;->j(Landroid/view/View;Landroid/view/ViewGroup;FF)V

    return-void
.end method

.method public onDismiss()V
    .locals 3

    iget-object v0, p0, Lv9/d;->b:Lmiuix/appcompat/internal/view/menu/g$a;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lv9/d;->a:Lmiuix/appcompat/internal/view/menu/c;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lmiuix/appcompat/internal/view/menu/g$a;->b(Lmiuix/appcompat/internal/view/menu/c;Z)V

    :cond_0
    iget-object v0, p0, Lv9/d;->a:Lmiuix/appcompat/internal/view/menu/c;

    invoke-virtual {v0}, Lmiuix/appcompat/internal/view/menu/c;->d()V

    return-void
.end method
