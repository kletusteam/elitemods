.class Lv9/e$a;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lv9/e;-><init>(Landroid/content/Context;Lmiuix/appcompat/internal/view/menu/c;Landroid/widget/PopupWindow$OnDismissListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lv9/e;


# direct methods
.method constructor <init>(Lv9/e;)V
    .locals 0

    iput-object p1, p0, Lv9/e$a;->a:Lv9/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView<",
            "*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iget-object p1, p0, Lv9/e$a;->a:Lv9/e;

    invoke-static {p1}, Lv9/e;->W(Lv9/e;)Lv9/a;

    move-result-object p1

    invoke-virtual {p1, p3}, Lja/f;->c(I)Landroid/view/MenuItem;

    move-result-object p1

    iget-object p2, p0, Lv9/e$a;->a:Lv9/e;

    invoke-static {p2}, Lv9/e;->X(Lv9/e;)Lmiuix/appcompat/internal/view/menu/c;

    move-result-object p2

    const/4 p3, 0x0

    invoke-virtual {p2, p1, p3}, Lmiuix/appcompat/internal/view/menu/c;->H(Landroid/view/MenuItem;I)Z

    invoke-interface {p1}, Landroid/view/MenuItem;->hasSubMenu()Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-interface {p1}, Landroid/view/MenuItem;->getSubMenu()Landroid/view/SubMenu;

    move-result-object p1

    iget-object p2, p0, Lv9/e$a;->a:Lv9/e;

    new-instance p3, Lv9/e$a$a;

    invoke-direct {p3, p0, p1}, Lv9/e$a$a;-><init>(Lv9/e$a;Landroid/view/SubMenu;)V

    invoke-virtual {p2, p3}, Lja/e;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    :cond_0
    iget-object p1, p0, Lv9/e$a;->a:Lv9/e;

    invoke-virtual {p1}, Lja/e;->dismiss()V

    return-void
.end method
