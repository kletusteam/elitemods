.class Lv9/a;
.super Lja/f;


# instance fields
.field private d:Landroid/view/MenuItem;


# direct methods
.method protected constructor <init>(Landroid/content/Context;Landroid/view/Menu;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lja/f;-><init>(Landroid/content/Context;Landroid/view/Menu;)V

    return-void
.end method


# virtual methods
.method protected b(Landroid/view/MenuItem;)Z
    .locals 3

    invoke-super {p0, p1}, Lja/f;->b(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Landroid/view/MenuItem;->getOrder()I

    move-result v1

    const/high16 v2, 0x20000

    if-ne v1, v2, :cond_1

    iget-object v0, p0, Lv9/a;->d:Landroid/view/MenuItem;

    if-nez v0, :cond_0

    iput-object p1, p0, Lv9/a;->d:Landroid/view/MenuItem;

    const/4 p1, 0x0

    return p1

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Only one menu item is allowed to have CATEGORY_SYSTEM order!"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    return v0
.end method

.method e()Landroid/view/MenuItem;
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Lv9/a;->d:Landroid/view/MenuItem;

    goto/32 :goto_1

    nop

    :goto_1
    return-object v0
.end method
