.class public Lja/e;
.super Landroid/widget/PopupWindow;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lja/e$d;
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:Z

.field private d:Z

.field protected final e:Landroid/graphics/Rect;

.field private f:Landroid/content/Context;

.field protected g:Landroid/widget/FrameLayout;

.field protected h:Landroid/view/View;

.field private i:Landroid/widget/ListView;

.field private j:Landroid/widget/ListAdapter;

.field private k:Landroid/widget/AdapterView$OnItemClickListener;

.field private l:I

.field private m:I

.field private n:I

.field protected o:I

.field private p:I

.field private q:I

.field private r:Lja/e$d;

.field protected s:I

.field private t:Landroid/widget/PopupWindow$OnDismissListener;

.field private u:Z

.field private v:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private w:Landroid/database/DataSetObserver;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    invoke-direct {p0, p1}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;)V

    const v0, 0x800035

    iput v0, p0, Lja/e;->l:I

    const/4 v0, 0x0

    iput v0, p0, Lja/e;->q:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lja/e;->u:Z

    new-instance v1, Lja/e$a;

    invoke-direct {v1, p0}, Lja/e$a;-><init>(Lja/e;)V

    iput-object v1, p0, Lja/e;->w:Landroid/database/DataSetObserver;

    iput-object p1, p0, Lja/e;->f:Landroid/content/Context;

    const/4 v1, -0x2

    invoke-virtual {p0, v1}, Landroid/widget/PopupWindow;->setHeight(I)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    new-instance v2, Lia/f;

    iget-object v3, p0, Lja/e;->f:Landroid/content/Context;

    invoke-direct {v2, v3}, Lia/f;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2}, Lia/f;->d()I

    move-result v3

    sget v4, Lk9/e;->P:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    iput v3, p0, Lja/e;->m:I

    sget v3, Lk9/e;->Q:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, p0, Lja/e;->n:I

    invoke-virtual {v2}, Lia/f;->c()I

    move-result v3

    sget v4, Lk9/e;->O:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-static {v3, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    iput v1, p0, Lja/e;->o:I

    invoke-virtual {v2}, Lia/f;->b()F

    move-result v1

    const/high16 v2, 0x41000000    # 8.0f

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, Lja/e;->a:I

    iput v1, p0, Lja/e;->b:I

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lja/e;->e:Landroid/graphics/Rect;

    new-instance v1, Lja/e$d;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lja/e$d;-><init>(Lja/e$a;)V

    iput-object v1, p0, Lja/e;->r:Lja/e$d;

    invoke-virtual {p0, v0}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    invoke-virtual {p0, v0}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    new-instance v0, Lja/g;

    invoke-direct {v0, p1}, Lja/g;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lja/e;->g:Landroid/widget/FrameLayout;

    new-instance v1, Lja/a;

    invoke-direct {v1, p0}, Lja/a;-><init>(Lja/e;)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0, p1}, Lja/e;->K(Landroid/content/Context;)V

    sget v0, Lk9/k;->c:I

    invoke-virtual {p0, v0}, Landroid/widget/PopupWindow;->setAnimationStyle(I)V

    iget-object v0, p0, Lja/e;->f:Landroid/content/Context;

    sget v1, Lk9/b;->x:I

    invoke-static {v0, v1}, Lia/d;->g(Landroid/content/Context;I)I

    move-result v0

    iput v0, p0, Lja/e;->s:I

    new-instance v0, Lja/c;

    invoke-direct {v0, p0}, Lja/c;-><init>(Lja/e;)V

    invoke-super {p0, v0}, Landroid/widget/PopupWindow;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lk9/e;->x:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lja/e;->p:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v0, Lk9/e;->y:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, p0, Lja/e;->q:I

    return-void
.end method

.method private B()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lja/e;->v:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method private synthetic G(Landroid/view/View;)V
    .locals 0

    invoke-virtual {p0}, Lja/e;->dismiss()V

    return-void
.end method

.method private synthetic H()V
    .locals 1

    iget-object v0, p0, Lja/e;->t:Landroid/widget/PopupWindow$OnDismissListener;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/widget/PopupWindow$OnDismissListener;->onDismiss()V

    :cond_0
    return-void
.end method

.method private synthetic I(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 7

    iget-object v0, p0, Lja/e;->i:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v0

    sub-int v4, p3, v0

    iget-object p3, p0, Lja/e;->k:Landroid/widget/AdapterView$OnItemClickListener;

    if-eqz p3, :cond_0

    if-ltz v4, :cond_0

    iget-object p3, p0, Lja/e;->j:Landroid/widget/ListAdapter;

    invoke-interface {p3}, Landroid/widget/ListAdapter;->getCount()I

    move-result p3

    if-ge v4, p3, :cond_0

    iget-object v1, p0, Lja/e;->k:Landroid/widget/AdapterView$OnItemClickListener;

    move-object v2, p1

    move-object v3, p2

    move-wide v5, p4

    invoke-interface/range {v1 .. v6}, Landroid/widget/AdapterView$OnItemClickListener;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    :cond_0
    return-void
.end method

.method private J(Landroid/widget/ListAdapter;Landroid/view/ViewGroup;Landroid/content/Context;I)V
    .locals 10

    const/4 v0, 0x0

    invoke-static {v0, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-static {v0, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-interface {p1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v3

    const/4 v4, 0x0

    move v5, v0

    move v6, v5

    move v7, v6

    move-object v8, v4

    :goto_0
    if-ge v0, v3, :cond_5

    invoke-interface {p1, v0}, Landroid/widget/ListAdapter;->getItemViewType(I)I

    move-result v9

    if-eq v9, v5, :cond_0

    move-object v8, v4

    move v5, v9

    :cond_0
    if-nez p2, :cond_1

    new-instance p2, Landroid/widget/FrameLayout;

    invoke-direct {p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    :cond_1
    invoke-interface {p1, v0, v8, p2}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8, v1, v2}, Landroid/view/View;->measure(II)V

    invoke-virtual {v8}, Landroid/view/View;->getMeasuredHeight()I

    move-result v9

    add-int/2addr v7, v9

    iget-object v9, p0, Lja/e;->r:Lja/e$d;

    iget-boolean v9, v9, Lja/e$d;->c:Z

    if-eqz v9, :cond_2

    goto :goto_1

    :cond_2
    invoke-virtual {v8}, Landroid/view/View;->getMeasuredWidth()I

    move-result v9

    if-lt v9, p4, :cond_3

    iget-object v9, p0, Lja/e;->r:Lja/e$d;

    invoke-virtual {v9, p4}, Lja/e$d;->a(I)V

    goto :goto_1

    :cond_3
    if-le v9, v6, :cond_4

    move v6, v9

    :cond_4
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_5
    iget-object p1, p0, Lja/e;->r:Lja/e$d;

    iget-boolean p2, p1, Lja/e$d;->c:Z

    if-nez p2, :cond_6

    invoke-virtual {p1, v6}, Lja/e$d;->a(I)V

    :cond_6
    iget-object p1, p0, Lja/e;->r:Lja/e$d;

    iput v7, p1, Lja/e$d;->b:I

    return-void
.end method

.method private U()Z
    .locals 2

    iget-boolean v0, p0, Lja/e;->u:Z

    if-eqz v0, :cond_1

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1d

    if-gt v0, v1, :cond_0

    iget-object v0, p0, Lja/e;->f:Landroid/content/Context;

    invoke-static {v0}, Lia/a;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private V(Landroid/view/View;)V
    .locals 3

    invoke-direct {p0, p1}, Lja/e;->w(Landroid/view/View;)I

    move-result v0

    invoke-direct {p0, p1}, Lja/e;->v(Landroid/view/View;)I

    move-result v1

    iget v2, p0, Lja/e;->l:I

    invoke-virtual {p0, p1, v1, v0, v2}, Lja/e;->showAsDropDown(Landroid/view/View;III)V

    sget v0, Lmiuix/view/c;->A:I

    sget v1, Lmiuix/view/c;->n:I

    invoke-static {p1, v0, v1}, Lmiuix/view/HapticCompat;->e(Landroid/view/View;II)Z

    iget-object p1, p0, Lja/e;->g:Landroid/widget/FrameLayout;

    invoke-virtual {p1}, Landroid/widget/FrameLayout;->getRootView()Landroid/view/View;

    move-result-object p1

    invoke-static {p1}, Lja/e;->x(Landroid/view/View;)V

    return-void
.end method

.method public static synthetic n(Lja/e;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lja/e;->G(Landroid/view/View;)V

    return-void
.end method

.method public static synthetic o(Lja/e;Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0

    invoke-direct/range {p0 .. p5}, Lja/e;->I(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    return-void
.end method

.method public static synthetic p(Lja/e;)V
    .locals 0

    invoke-direct {p0}, Lja/e;->H()V

    return-void
.end method

.method static synthetic q(Lja/e;)Lja/e$d;
    .locals 0

    iget-object p0, p0, Lja/e;->r:Lja/e$d;

    return-object p0
.end method

.method static synthetic r(Lja/e;)Landroid/view/View;
    .locals 0

    invoke-direct {p0}, Lja/e;->B()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method static synthetic s(Lja/e;Landroid/view/View;)I
    .locals 0

    invoke-direct {p0, p1}, Lja/e;->v(Landroid/view/View;)I

    move-result p0

    return p0
.end method

.method static synthetic t(Lja/e;Landroid/view/View;)I
    .locals 0

    invoke-direct {p0, p1}, Lja/e;->w(Landroid/view/View;)I

    move-result p0

    return p0
.end method

.method static synthetic u(Lja/e;)Landroid/widget/ListView;
    .locals 0

    iget-object p0, p0, Lja/e;->i:Landroid/widget/ListView;

    return-object p0
.end method

.method private v(Landroid/view/View;)I
    .locals 5

    const/4 v0, 0x2

    new-array v0, v0, [I

    invoke-virtual {p1, v0}, Landroid/view/View;->getLocationInWindow([I)V

    invoke-static {p1}, Landroidx/appcompat/widget/s0;->b(Landroid/view/View;)Z

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    aget v1, v0, v3

    iget v4, p0, Lja/e;->a:I

    sub-int/2addr v1, v4

    invoke-virtual {p0}, Landroid/widget/PopupWindow;->getWidth()I

    move-result v4

    add-int/2addr v1, v4

    iget v4, p0, Lja/e;->p:I

    add-int/2addr v1, v4

    invoke-virtual {p1}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v4

    if-le v1, v4, :cond_1

    invoke-virtual {p1}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Landroid/widget/PopupWindow;->getWidth()I

    move-result v4

    sub-int/2addr v1, v4

    iget v4, p0, Lja/e;->p:I

    sub-int/2addr v1, v4

    aget v0, v0, v3

    goto :goto_0

    :cond_0
    aget v1, v0, v3

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v4

    add-int/2addr v1, v4

    iget v4, p0, Lja/e;->a:I

    add-int/2addr v1, v4

    invoke-virtual {p0}, Landroid/widget/PopupWindow;->getWidth()I

    move-result v4

    sub-int/2addr v1, v4

    iget v4, p0, Lja/e;->p:I

    sub-int/2addr v1, v4

    if-gez v1, :cond_1

    invoke-virtual {p0}, Landroid/widget/PopupWindow;->getWidth()I

    move-result v1

    iget v4, p0, Lja/e;->p:I

    add-int/2addr v1, v4

    aget v0, v0, v3

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v4

    add-int/2addr v0, v4

    :goto_0
    sub-int/2addr v1, v0

    goto :goto_1

    :cond_1
    move v1, v3

    move v2, v1

    :goto_1
    if-nez v2, :cond_5

    iget-boolean v0, p0, Lja/e;->c:Z

    if-eqz v0, :cond_2

    iget v3, p0, Lja/e;->a:I

    :cond_2
    if-eqz v3, :cond_4

    if-nez v0, :cond_4

    invoke-static {p1}, Landroidx/appcompat/widget/s0;->b(Landroid/view/View;)Z

    move-result p1

    if-eqz p1, :cond_3

    iget-object p1, p0, Lja/e;->e:Landroid/graphics/Rect;

    iget p1, p1, Landroid/graphics/Rect;->left:I

    iget v0, p0, Lja/e;->a:I

    sub-int/2addr p1, v0

    sub-int v1, v3, p1

    goto :goto_2

    :cond_3
    iget-object p1, p0, Lja/e;->e:Landroid/graphics/Rect;

    iget p1, p1, Landroid/graphics/Rect;->right:I

    iget v0, p0, Lja/e;->a:I

    sub-int/2addr p1, v0

    add-int v1, v3, p1

    goto :goto_2

    :cond_4
    move v1, v3

    :cond_5
    :goto_2
    return v1
.end method

.method private w(Landroid/view/View;)I
    .locals 5

    iget-boolean v0, p0, Lja/e;->d:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lja/e;->b:I

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v0

    neg-int v0, v0

    iget-object v1, p0, Lja/e;->e:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    sub-int/2addr v0, v1

    iget v1, p0, Lja/e;->b:I

    add-int/2addr v0, v1

    :goto_0
    const/4 v1, 0x2

    new-array v1, v1, [I

    invoke-virtual {p1, v1}, Landroid/view/View;->getLocationInWindow([I)V

    const/4 v2, 0x1

    aget v1, v1, v2

    int-to-float v1, v1

    iget-object v2, p0, Lja/e;->f:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-virtual {p0}, Lja/e;->y()I

    move-result v3

    if-lez v3, :cond_1

    iget-object v4, p0, Lja/e;->r:Lja/e$d;

    iget v4, v4, Lja/e$d;->b:I

    invoke-static {v4, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    goto :goto_1

    :cond_1
    iget-object v3, p0, Lja/e;->r:Lja/e$d;

    iget v3, v3, Lja/e$d;->b:I

    :goto_1
    if-ge v3, v2, :cond_3

    int-to-float v4, v0

    add-float/2addr v1, v4

    int-to-float v4, v3

    add-float/2addr v1, v4

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v1, v4

    int-to-float v2, v2

    cmpl-float v1, v1, v2

    if-lez v1, :cond_3

    iget-boolean v1, p0, Lja/e;->d:Z

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result p1

    goto :goto_2

    :cond_2
    const/4 p1, 0x0

    :goto_2
    add-int/2addr p1, v3

    sub-int/2addr v0, p1

    :cond_3
    return v0
.end method

.method public static x(Landroid/view/View;)V
    .locals 3

    if-nez p0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager$LayoutParams;

    if-nez v0, :cond_1

    return-void

    :cond_1
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/lit8 v1, v1, 0x2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    const v1, 0x3e99999a    # 0.3f

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->dimAmount:F

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "window"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    invoke-interface {v1, p0, v0}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method


# virtual methods
.method public A(Landroid/view/View;Landroid/view/ViewGroup;)V
    .locals 0

    invoke-virtual {p0}, Lja/e;->z()I

    move-result p2

    invoke-virtual {p0, p2}, Landroid/widget/PopupWindow;->setWidth(I)V

    invoke-direct {p0, p1}, Lja/e;->V(Landroid/view/View;)V

    return-void
.end method

.method public C()Landroid/widget/ListView;
    .locals 1

    iget-object v0, p0, Lja/e;->i:Landroid/widget/ListView;

    return-object v0
.end method

.method public D()I
    .locals 1

    iget v0, p0, Lja/e;->p:I

    return v0
.end method

.method public E()I
    .locals 1

    iget v0, p0, Lja/e;->q:I

    return v0
.end method

.method protected F()Z
    .locals 5

    iget-object v0, p0, Lja/e;->i:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    :cond_0
    iget-object v0, p0, Lja/e;->i:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getLastVisiblePosition()I

    move-result v0

    iget-object v2, p0, Lja/e;->i:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v2

    invoke-interface {v2}, Landroid/widget/ListAdapter;->getCount()I

    move-result v2

    sub-int/2addr v2, v1

    if-eq v0, v2, :cond_1

    return v1

    :cond_1
    const/4 v0, 0x0

    move v2, v0

    move v3, v2

    :goto_0
    iget-object v4, p0, Lja/e;->i:Landroid/widget/ListView;

    invoke-virtual {v4}, Landroid/widget/ListView;->getLastVisiblePosition()I

    move-result v4

    if-gt v2, v4, :cond_2

    iget-object v4, p0, Lja/e;->i:Landroid/widget/ListView;

    invoke-virtual {v4, v2}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v3, v4

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lja/e;->i:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getMeasuredHeight()I

    move-result v2

    if-ge v2, v3, :cond_3

    return v1

    :cond_3
    return v0
.end method

.method protected K(Landroid/content/Context;)V
    .locals 1

    iget-object p1, p0, Lja/e;->f:Landroid/content/Context;

    sget v0, Lk9/b;->t:I

    invoke-static {p1, v0}, Lia/d;->h(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lja/e;->e:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    iget-object v0, p0, Lja/e;->g:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    new-instance p1, Landroid/graphics/drawable/ColorDrawable;

    const/4 v0, 0x0

    invoke-direct {p1, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, p1}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object p1, p0, Lja/e;->g:Landroid/widget/FrameLayout;

    invoke-virtual {p0, p1}, Lja/e;->S(Landroid/view/View;)V

    return-void
.end method

.method protected L(Landroid/view/View;Landroid/view/ViewGroup;)Z
    .locals 4

    const-string p2, "ListPopupWindow"

    const/4 v0, 0x0

    if-nez p1, :cond_0

    const-string p1, "show: anchor is null"

    :goto_0
    invoke-static {p2, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return v0

    :cond_0
    iget-object v1, p0, Lja/e;->h:Landroid/view/View;

    if-nez v1, :cond_1

    iget-object v1, p0, Lja/e;->f:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    sget v2, Lk9/i;->x:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lja/e;->h:Landroid/view/View;

    new-instance v2, Lja/e$b;

    invoke-direct {v2, p0}, Lja/e$b;-><init>(Lja/e;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    :cond_1
    iget-object v1, p0, Lja/e;->g:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getChildCount()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lja/e;->g:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    iget-object v3, p0, Lja/e;->h:Landroid/view/View;

    if-eq v1, v3, :cond_3

    :cond_2
    iget-object v1, p0, Lja/e;->g:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->removeAllViews()V

    iget-object v1, p0, Lja/e;->g:Landroid/widget/FrameLayout;

    iget-object v3, p0, Lja/e;->h:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lja/e;->h:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v3, -0x1

    iput v3, v1, Landroid/widget/FrameLayout$LayoutParams;->width:I

    const/4 v3, -0x2

    iput v3, v1, Landroid/widget/FrameLayout$LayoutParams;->height:I

    const/16 v3, 0x10

    iput v3, v1, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    :cond_3
    invoke-direct {p0}, Lja/e;->U()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lja/e;->g:Landroid/widget/FrameLayout;

    iget v3, p0, Lja/e;->s:I

    int-to-float v3, v3

    invoke-virtual {v1, v3}, Landroid/widget/FrameLayout;->setElevation(F)V

    iget v1, p0, Lja/e;->s:I

    int-to-float v1, v1

    invoke-virtual {p0, v1}, Landroid/widget/PopupWindow;->setElevation(F)V

    iget-object v1, p0, Lja/e;->g:Landroid/widget/FrameLayout;

    invoke-virtual {p0, v1}, Lja/e;->R(Landroid/view/View;)V

    :cond_4
    iget-object v1, p0, Lja/e;->h:Landroid/view/View;

    const v3, 0x102000a

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lja/e;->i:Landroid/widget/ListView;

    if-nez v1, :cond_5

    const-string p1, "list not found"

    goto :goto_0

    :cond_5
    new-instance p2, Lja/b;

    invoke-direct {p2, p0}, Lja/b;-><init>(Lja/e;)V

    invoke-virtual {v1, p2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object p2, p0, Lja/e;->i:Landroid/widget/ListView;

    iget-object v1, p0, Lja/e;->j:Landroid/widget/ListAdapter;

    invoke-virtual {p2, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-virtual {p0}, Lja/e;->z()I

    move-result p2

    invoke-virtual {p0, p2}, Landroid/widget/PopupWindow;->setWidth(I)V

    invoke-virtual {p0}, Lja/e;->y()I

    move-result p2

    if-lez p2, :cond_6

    iget-object v1, p0, Lja/e;->r:Lja/e$d;

    iget v1, v1, Lja/e$d;->b:I

    if-le v1, p2, :cond_6

    invoke-virtual {p0, p2}, Landroid/widget/PopupWindow;->setHeight(I)V

    :cond_6
    iget-object p2, p0, Lja/e;->f:Landroid/content/Context;

    invoke-virtual {p2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p2

    const-string v1, "input_method"

    invoke-virtual {p2, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {p1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object p1

    invoke-virtual {p2, p1, v0}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    return v2
.end method

.method public M(I)V
    .locals 1

    iget-object v0, p0, Lja/e;->r:Lja/e$d;

    invoke-virtual {v0, p1}, Lja/e$d;->a(I)V

    return-void
.end method

.method public N(I)V
    .locals 0

    iput p1, p0, Lja/e;->l:I

    return-void
.end method

.method public O(Z)V
    .locals 0

    iput-boolean p1, p0, Lja/e;->u:Z

    return-void
.end method

.method public P(I)V
    .locals 0

    iput p1, p0, Lja/e;->o:I

    return-void
.end method

.method public Q(Landroid/widget/AdapterView$OnItemClickListener;)V
    .locals 0

    iput-object p1, p0, Lja/e;->k:Landroid/widget/AdapterView$OnItemClickListener;

    return-void
.end method

.method protected R(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lja/e;->f:Landroid/content/Context;

    invoke-static {v0}, Laa/b;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    new-instance v0, Lja/e$c;

    invoke-direct {v0, p0}, Lja/e$c;-><init>(Lja/e;)V

    :goto_0
    invoke-virtual {p1, v0}, Landroid/view/View;->setOutlineProvider(Landroid/view/ViewOutlineProvider;)V

    return-void
.end method

.method protected S(Landroid/view/View;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    return-void
.end method

.method protected T(Z)V
    .locals 1

    iget-object v0, p0, Lja/e;->h:Landroid/view/View;

    check-cast v0, Lmiuix/springback/view/SpringBackLayout;

    invoke-virtual {v0, p1}, Lmiuix/springback/view/SpringBackLayout;->setEnabled(Z)V

    return-void
.end method

.method public b()I
    .locals 1

    iget v0, p0, Lja/e;->a:I

    return v0
.end method

.method public d(I)V
    .locals 0

    iput p1, p0, Lja/e;->b:I

    const/4 p1, 0x1

    iput-boolean p1, p0, Lja/e;->d:Z

    return-void
.end method

.method public dismiss()V
    .locals 1

    invoke-super {p0}, Landroid/widget/PopupWindow;->dismiss()V

    iget-object v0, p0, Lja/e;->f:Landroid/content/Context;

    invoke-static {v0, p0}, Lr9/f;->d(Landroid/content/Context;Ljava/lang/Object;)V

    return-void
.end method

.method public f(I)V
    .locals 0

    iput p1, p0, Lja/e;->a:I

    const/4 p1, 0x1

    iput-boolean p1, p0, Lja/e;->c:Z

    return-void
.end method

.method public g()I
    .locals 1

    iget v0, p0, Lja/e;->b:I

    return v0
.end method

.method public i(Landroid/widget/ListAdapter;)V
    .locals 2

    iget-object v0, p0, Lja/e;->j:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lja/e;->w:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    :cond_0
    iput-object p1, p0, Lja/e;->j:Landroid/widget/ListAdapter;

    if-eqz p1, :cond_1

    iget-object v0, p0, Lja/e;->w:Landroid/database/DataSetObserver;

    invoke-interface {p1, v0}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    :cond_1
    return-void
.end method

.method public k(Landroid/view/View;Landroid/view/ViewGroup;)V
    .locals 0

    invoke-virtual {p0, p1, p2}, Lja/e;->L(Landroid/view/View;Landroid/view/ViewGroup;)Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-direct {p0, p1}, Lja/e;->V(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method public setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V
    .locals 0

    iput-object p1, p0, Lja/e;->t:Landroid/widget/PopupWindow$OnDismissListener;

    return-void
.end method

.method public showAsDropDown(Landroid/view/View;III)V
    .locals 0

    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/PopupWindow;->showAsDropDown(Landroid/view/View;III)V

    new-instance p2, Ljava/lang/ref/WeakReference;

    invoke-direct {p2, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object p2, p0, Lja/e;->v:Ljava/lang/ref/WeakReference;

    iget-object p1, p0, Lja/e;->f:Landroid/content/Context;

    invoke-static {p1, p0}, Lr9/f;->e(Landroid/content/Context;Ljava/lang/Object;)V

    return-void
.end method

.method public showAtLocation(Landroid/view/View;III)V
    .locals 0

    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    iget-object p1, p0, Lja/e;->f:Landroid/content/Context;

    invoke-static {p1, p0}, Lr9/f;->e(Landroid/content/Context;Ljava/lang/Object;)V

    return-void
.end method

.method protected y()I
    .locals 2

    new-instance v0, Lia/f;

    iget-object v1, p0, Lja/e;->f:Landroid/content/Context;

    invoke-direct {v0, v1}, Lia/f;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lia/f;->c()I

    move-result v0

    iget-object v1, p0, Lja/e;->f:Landroid/content/Context;

    invoke-static {v1}, Laa/b;->g(Landroid/content/Context;)I

    move-result v1

    sub-int/2addr v0, v1

    iget v1, p0, Lja/e;->o:I

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0
.end method

.method protected z()I
    .locals 4

    iget-object v0, p0, Lja/e;->r:Lja/e$d;

    iget-boolean v0, v0, Lja/e$d;->c:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lja/e;->j:Landroid/widget/ListAdapter;

    const/4 v1, 0x0

    iget-object v2, p0, Lja/e;->f:Landroid/content/Context;

    iget v3, p0, Lja/e;->m:I

    invoke-direct {p0, v0, v1, v2, v3}, Lja/e;->J(Landroid/widget/ListAdapter;Landroid/view/ViewGroup;Landroid/content/Context;I)V

    :cond_0
    iget-object v0, p0, Lja/e;->r:Lja/e$d;

    iget v0, v0, Lja/e$d;->a:I

    iget v1, p0, Lja/e;->n:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iget-object v1, p0, Lja/e;->e:Landroid/graphics/Rect;

    iget v2, v1, Landroid/graphics/Rect;->left:I

    add-int/2addr v0, v2

    iget v1, v1, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v1

    return v0
.end method
