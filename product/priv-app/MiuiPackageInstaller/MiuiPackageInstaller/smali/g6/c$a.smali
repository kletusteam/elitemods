.class Lg6/c$a;
.super Landroid/os/Handler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lg6/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lg6/c;


# direct methods
.method constructor <init>(Lg6/c;Landroid/os/Looper;)V
    .locals 0

    iput-object p1, p0, Lg6/c$a;->a:Lg6/c;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2

    iget p1, p1, Landroid/os/Message;->what:I

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    goto/16 :goto_0

    :pswitch_1
    iget-object p1, p0, Lg6/c$a;->a:Lg6/c;

    invoke-static {p1}, Lg6/c;->a(Lg6/c;)Lg6/b;

    move-result-object p1

    invoke-interface {p1}, Lg6/b;->a()V

    goto :goto_0

    :pswitch_2
    iget-object p1, p0, Lg6/c$a;->a:Lg6/c;

    invoke-static {p1}, Lg6/c;->a(Lg6/c;)Lg6/b;

    move-result-object p1

    invoke-interface {p1}, Lg6/b;->b()V

    goto :goto_0

    :pswitch_3
    iget-object p1, p0, Lg6/c$a;->a:Lg6/c;

    invoke-static {p1}, Lg6/c;->d(Lg6/c;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lg6/c$a;->a:Lg6/c;

    invoke-static {p1}, Lg6/c;->a(Lg6/c;)Lg6/b;

    move-result-object p1

    iget-object v0, p0, Lg6/c$a;->a:Lg6/c;

    invoke-static {v0}, Lg6/c;->c(Lg6/c;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f11010f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lg6/b;->d(Ljava/lang/String;)V

    :cond_0
    iget-object p1, p0, Lg6/c$a;->a:Lg6/c;

    invoke-static {p1}, Lg6/c;->a(Lg6/c;)Lg6/b;

    move-result-object p1

    iget-object v0, p0, Lg6/c$a;->a:Lg6/c;

    invoke-static {v0}, Lg6/c;->d(Lg6/c;)Z

    move-result v0

    invoke-interface {p1, v0}, Lg6/b;->c(Z)V

    goto :goto_0

    :pswitch_4
    iget-object p1, p0, Lg6/c$a;->a:Lg6/c;

    invoke-static {p1}, Lg6/c;->a(Lg6/c;)Lg6/b;

    move-result-object p1

    iget-object v0, p0, Lg6/c$a;->a:Lg6/c;

    invoke-static {v0}, Lg6/c;->c(Lg6/c;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lg6/c$a;->a:Lg6/c;

    invoke-static {v1}, Lg6/c;->b(Lg6/c;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lg6/b;->d(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_5
    iget-object p1, p0, Lg6/c$a;->a:Lg6/c;

    invoke-static {p1}, Lg6/c;->a(Lg6/c;)Lg6/b;

    move-result-object p1

    invoke-interface {p1}, Lg6/b;->f()V

    goto :goto_0

    :pswitch_6
    iget-object p1, p0, Lg6/c$a;->a:Lg6/c;

    invoke-static {p1}, Lg6/c;->a(Lg6/c;)Lg6/b;

    move-result-object p1

    invoke-interface {p1}, Lg6/b;->e()V

    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x3e9
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
