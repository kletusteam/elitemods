.class public final Lhc/a;
.super Lgc/f$a;


# instance fields
.field private final a:Lp4/e;


# direct methods
.method private constructor <init>(Lp4/e;)V
    .locals 0

    invoke-direct {p0}, Lgc/f$a;-><init>()V

    iput-object p1, p0, Lhc/a;->a:Lp4/e;

    return-void
.end method

.method public static f()Lhc/a;
    .locals 1

    new-instance v0, Lp4/e;

    invoke-direct {v0}, Lp4/e;-><init>()V

    invoke-static {v0}, Lhc/a;->g(Lp4/e;)Lhc/a;

    move-result-object v0

    return-object v0
.end method

.method public static g(Lp4/e;)Lhc/a;
    .locals 1

    const-string v0, "gson == null"

    invoke-static {p0, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    new-instance v0, Lhc/a;

    invoke-direct {v0, p0}, Lhc/a;-><init>(Lp4/e;)V

    return-object v0
.end method


# virtual methods
.method public c(Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;[Ljava/lang/annotation/Annotation;Lgc/u;)Lgc/f;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Type;",
            "[",
            "Ljava/lang/annotation/Annotation;",
            "[",
            "Ljava/lang/annotation/Annotation;",
            "Lgc/u;",
            ")",
            "Lgc/f<",
            "*",
            "Lpb/a0;",
            ">;"
        }
    .end annotation

    iget-object p2, p0, Lhc/a;->a:Lp4/e;

    invoke-static {p1}, Lw4/a;->b(Ljava/lang/reflect/Type;)Lw4/a;

    move-result-object p1

    invoke-virtual {p2, p1}, Lp4/e;->l(Lw4/a;)Lp4/v;

    move-result-object p1

    new-instance p2, Lhc/b;

    iget-object p3, p0, Lhc/a;->a:Lp4/e;

    invoke-direct {p2, p3, p1}, Lhc/b;-><init>(Lp4/e;Lp4/v;)V

    return-object p2
.end method

.method public d(Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;Lgc/u;)Lgc/f;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Type;",
            "[",
            "Ljava/lang/annotation/Annotation;",
            "Lgc/u;",
            ")",
            "Lgc/f<",
            "Lpb/c0;",
            "*>;"
        }
    .end annotation

    iget-object p2, p0, Lhc/a;->a:Lp4/e;

    invoke-static {p1}, Lw4/a;->b(Ljava/lang/reflect/Type;)Lw4/a;

    move-result-object p1

    invoke-virtual {p2, p1}, Lp4/e;->l(Lw4/a;)Lp4/v;

    move-result-object p1

    new-instance p2, Lhc/c;

    iget-object p3, p0, Lhc/a;->a:Lp4/e;

    invoke-direct {p2, p3, p1}, Lhc/c;-><init>(Lp4/e;Lp4/v;)V

    return-object p2
.end method
