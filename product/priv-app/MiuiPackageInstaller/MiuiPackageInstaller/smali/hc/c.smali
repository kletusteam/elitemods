.class final Lhc/c;
.super Ljava/lang/Object;

# interfaces
.implements Lgc/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lgc/f<",
        "Lpb/c0;",
        "TT;>;"
    }
.end annotation


# instance fields
.field private final a:Lp4/e;

.field private final b:Lp4/v;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lp4/v<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lp4/e;Lp4/v;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lp4/e;",
            "Lp4/v<",
            "TT;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lhc/c;->a:Lp4/e;

    iput-object p2, p0, Lhc/c;->b:Lp4/v;

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lpb/c0;

    invoke-virtual {p0, p1}, Lhc/c;->b(Lpb/c0;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public b(Lpb/c0;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lpb/c0;",
            ")TT;"
        }
    .end annotation

    iget-object v0, p0, Lhc/c;->a:Lp4/e;

    invoke-virtual {p1}, Lpb/c0;->b()Ljava/io/Reader;

    move-result-object v1

    invoke-virtual {v0, v1}, Lp4/e;->o(Ljava/io/Reader;)Lx4/a;

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lhc/c;->b:Lp4/v;

    invoke-virtual {v1, v0}, Lp4/v;->b(Lx4/a;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0}, Lx4/a;->a0()Lx4/b;

    move-result-object v0

    sget-object v2, Lx4/b;->j:Lx4/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v2, :cond_0

    invoke-virtual {p1}, Lpb/c0;->close()V

    return-object v1

    :cond_0
    :try_start_1
    new-instance v0, Lp4/k;

    const-string v1, "JSON document was not fully consumed."

    invoke-direct {v0, v1}, Lp4/k;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Lpb/c0;->close()V

    throw v0
.end method
