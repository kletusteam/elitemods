.class final Lhc/b;
.super Ljava/lang/Object;

# interfaces
.implements Lgc/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lgc/f<",
        "TT;",
        "Lpb/a0;",
        ">;"
    }
.end annotation


# static fields
.field private static final c:Lpb/v;

.field private static final d:Ljava/nio/charset/Charset;


# instance fields
.field private final a:Lp4/e;

.field private final b:Lp4/v;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lp4/v<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "application/json; charset=UTF-8"

    invoke-static {v0}, Lpb/v;->e(Ljava/lang/String;)Lpb/v;

    move-result-object v0

    sput-object v0, Lhc/b;->c:Lpb/v;

    const-string v0, "UTF-8"

    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lhc/b;->d:Ljava/nio/charset/Charset;

    return-void
.end method

.method constructor <init>(Lp4/e;Lp4/v;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lp4/e;",
            "Lp4/v<",
            "TT;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lhc/b;->a:Lp4/e;

    iput-object p2, p0, Lhc/b;->b:Lp4/v;

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    invoke-virtual {p0, p1}, Lhc/b;->b(Ljava/lang/Object;)Lpb/a0;

    move-result-object p1

    return-object p1
.end method

.method public b(Ljava/lang/Object;)Lpb/a0;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Lpb/a0;"
        }
    .end annotation

    new-instance v0, Ldc/e;

    invoke-direct {v0}, Ldc/e;-><init>()V

    new-instance v1, Ljava/io/OutputStreamWriter;

    invoke-virtual {v0}, Ldc/e;->C()Ljava/io/OutputStream;

    move-result-object v2

    sget-object v3, Lhc/b;->d:Ljava/nio/charset/Charset;

    invoke-direct {v1, v2, v3}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/nio/charset/Charset;)V

    iget-object v2, p0, Lhc/b;->a:Lp4/e;

    invoke-virtual {v2, v1}, Lp4/e;->p(Ljava/io/Writer;)Lx4/c;

    move-result-object v1

    iget-object v2, p0, Lhc/b;->b:Lp4/v;

    invoke-virtual {v2, v1, p1}, Lp4/v;->d(Lx4/c;Ljava/lang/Object;)V

    invoke-virtual {v1}, Lx4/c;->close()V

    sget-object p1, Lhc/b;->c:Lpb/v;

    invoke-virtual {v0}, Ldc/e;->U()Ldc/h;

    move-result-object v0

    invoke-static {p1, v0}, Lpb/a0;->c(Lpb/v;Ldc/h;)Lpb/a0;

    move-result-object p1

    return-object p1
.end method
