.class public final enum Lcom/commoncomponent/apimonitor/bean/NetState;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/commoncomponent/apimonitor/bean/NetState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/commoncomponent/apimonitor/bean/NetState;

.field public static final enum ETHERNET:Lcom/commoncomponent/apimonitor/bean/NetState;

.field public static final enum MOBILE_2G:Lcom/commoncomponent/apimonitor/bean/NetState;

.field public static final enum MOBILE_3G:Lcom/commoncomponent/apimonitor/bean/NetState;

.field public static final enum MOBILE_4G:Lcom/commoncomponent/apimonitor/bean/NetState;

.field public static final enum MOBILE_5G:Lcom/commoncomponent/apimonitor/bean/NetState;

.field public static final enum NOT_CONNECTED:Lcom/commoncomponent/apimonitor/bean/NetState;

.field public static final enum UNKNOWN:Lcom/commoncomponent/apimonitor/bean/NetState;

.field public static final enum WIFI:Lcom/commoncomponent/apimonitor/bean/NetState;


# instance fields
.field private a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    new-instance v0, Lcom/commoncomponent/apimonitor/bean/NetState;

    const-string v1, "NOT_CONNECTED"

    const/4 v2, 0x0

    const-string v3, "NONE"

    invoke-direct {v0, v1, v2, v3}, Lcom/commoncomponent/apimonitor/bean/NetState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/commoncomponent/apimonitor/bean/NetState;->NOT_CONNECTED:Lcom/commoncomponent/apimonitor/bean/NetState;

    new-instance v1, Lcom/commoncomponent/apimonitor/bean/NetState;

    const-string v3, "MOBILE_2G"

    const/4 v4, 0x1

    const-string v5, "2G"

    invoke-direct {v1, v3, v4, v5}, Lcom/commoncomponent/apimonitor/bean/NetState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/commoncomponent/apimonitor/bean/NetState;->MOBILE_2G:Lcom/commoncomponent/apimonitor/bean/NetState;

    new-instance v3, Lcom/commoncomponent/apimonitor/bean/NetState;

    const-string v5, "MOBILE_3G"

    const/4 v6, 0x2

    const-string v7, "3G"

    invoke-direct {v3, v5, v6, v7}, Lcom/commoncomponent/apimonitor/bean/NetState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/commoncomponent/apimonitor/bean/NetState;->MOBILE_3G:Lcom/commoncomponent/apimonitor/bean/NetState;

    new-instance v5, Lcom/commoncomponent/apimonitor/bean/NetState;

    const-string v7, "MOBILE_4G"

    const/4 v8, 0x3

    const-string v9, "4G"

    invoke-direct {v5, v7, v8, v9}, Lcom/commoncomponent/apimonitor/bean/NetState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/commoncomponent/apimonitor/bean/NetState;->MOBILE_4G:Lcom/commoncomponent/apimonitor/bean/NetState;

    new-instance v7, Lcom/commoncomponent/apimonitor/bean/NetState;

    const-string v9, "MOBILE_5G"

    const/4 v10, 0x4

    const-string v11, "5G"

    invoke-direct {v7, v9, v10, v11}, Lcom/commoncomponent/apimonitor/bean/NetState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/commoncomponent/apimonitor/bean/NetState;->MOBILE_5G:Lcom/commoncomponent/apimonitor/bean/NetState;

    new-instance v9, Lcom/commoncomponent/apimonitor/bean/NetState;

    const-string v11, "WIFI"

    const/4 v12, 0x5

    invoke-direct {v9, v11, v12, v11}, Lcom/commoncomponent/apimonitor/bean/NetState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v9, Lcom/commoncomponent/apimonitor/bean/NetState;->WIFI:Lcom/commoncomponent/apimonitor/bean/NetState;

    new-instance v11, Lcom/commoncomponent/apimonitor/bean/NetState;

    const-string v13, "ETHERNET"

    const/4 v14, 0x6

    invoke-direct {v11, v13, v14, v13}, Lcom/commoncomponent/apimonitor/bean/NetState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v11, Lcom/commoncomponent/apimonitor/bean/NetState;->ETHERNET:Lcom/commoncomponent/apimonitor/bean/NetState;

    new-instance v13, Lcom/commoncomponent/apimonitor/bean/NetState;

    const-string v15, "UNKNOWN"

    const/4 v14, 0x7

    invoke-direct {v13, v15, v14, v15}, Lcom/commoncomponent/apimonitor/bean/NetState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v13, Lcom/commoncomponent/apimonitor/bean/NetState;->UNKNOWN:Lcom/commoncomponent/apimonitor/bean/NetState;

    const/16 v15, 0x8

    new-array v15, v15, [Lcom/commoncomponent/apimonitor/bean/NetState;

    aput-object v0, v15, v2

    aput-object v1, v15, v4

    aput-object v3, v15, v6

    aput-object v5, v15, v8

    aput-object v7, v15, v10

    aput-object v9, v15, v12

    const/4 v0, 0x6

    aput-object v11, v15, v0

    aput-object v13, v15, v14

    sput-object v15, Lcom/commoncomponent/apimonitor/bean/NetState;->$VALUES:[Lcom/commoncomponent/apimonitor/bean/NetState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/commoncomponent/apimonitor/bean/NetState;->a:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/commoncomponent/apimonitor/bean/NetState;
    .locals 1

    const-class v0, Lcom/commoncomponent/apimonitor/bean/NetState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/commoncomponent/apimonitor/bean/NetState;

    return-object p0
.end method

.method public static values()[Lcom/commoncomponent/apimonitor/bean/NetState;
    .locals 1

    sget-object v0, Lcom/commoncomponent/apimonitor/bean/NetState;->$VALUES:[Lcom/commoncomponent/apimonitor/bean/NetState;

    invoke-virtual {v0}, [Lcom/commoncomponent/apimonitor/bean/NetState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/commoncomponent/apimonitor/bean/NetState;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/commoncomponent/apimonitor/bean/NetState;->a:Ljava/lang/String;

    return-object v0
.end method
