.class public Lcom/android/packageinstaller/utils/TailStatManager;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/packageinstaller/utils/TailStatManager$NetWorkResultModel;,
        Lcom/android/packageinstaller/utils/TailStatManager$GameStatItemModel;,
        Lcom/android/packageinstaller/utils/TailStatManager$GameStatModel;
    }
.end annotation


# static fields
.field private static c:Lcom/android/packageinstaller/utils/TailStatManager;


# instance fields
.field private a:Landroid/content/SharedPreferences;

.field private b:Lcom/android/packageinstaller/utils/TailStatManager$GameStatModel;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 3

    invoke-direct {p0}, Lcom/android/packageinstaller/utils/TailStatManager;->c()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, ""

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/packageinstaller/utils/TailStatManager;->a:Landroid/content/SharedPreferences;

    const-string v2, "preference_key_name_tail_stat"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1
.end method

.method public static declared-synchronized b()Lcom/android/packageinstaller/utils/TailStatManager;
    .locals 2

    const-class v0, Lcom/android/packageinstaller/utils/TailStatManager;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/android/packageinstaller/utils/TailStatManager;->c:Lcom/android/packageinstaller/utils/TailStatManager;

    if-nez v1, :cond_0

    new-instance v1, Lcom/android/packageinstaller/utils/TailStatManager;

    invoke-direct {v1}, Lcom/android/packageinstaller/utils/TailStatManager;-><init>()V

    sput-object v1, Lcom/android/packageinstaller/utils/TailStatManager;->c:Lcom/android/packageinstaller/utils/TailStatManager;

    :cond_0
    sget-object v1, Lcom/android/packageinstaller/utils/TailStatManager;->c:Lcom/android/packageinstaller/utils/TailStatManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method private c()Landroid/content/SharedPreferences;
    .locals 3

    iget-object v0, p0, Lcom/android/packageinstaller/utils/TailStatManager;->a:Landroid/content/SharedPreferences;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/android/packageinstaller/InstallerApplication;->g()Lcom/android/packageinstaller/InstallerApplication;

    move-result-object v0

    const/4 v1, 0x0

    const-string v2, "preference_name_tail_stat"

    invoke-virtual {v0, v2, v1}, Landroid/app/Application;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/android/packageinstaller/utils/TailStatManager;->a:Landroid/content/SharedPreferences;

    :cond_0
    iget-object v0, p0, Lcom/android/packageinstaller/utils/TailStatManager;->a:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method private d()Lcom/android/packageinstaller/utils/TailStatManager$GameStatModel;
    .locals 2

    iget-object v0, p0, Lcom/android/packageinstaller/utils/TailStatManager;->b:Lcom/android/packageinstaller/utils/TailStatManager$GameStatModel;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/android/packageinstaller/utils/TailStatManager;->e()Lcom/android/packageinstaller/utils/TailStatManager$GameStatModel;

    move-result-object v0

    iput-object v0, p0, Lcom/android/packageinstaller/utils/TailStatManager;->b:Lcom/android/packageinstaller/utils/TailStatManager$GameStatModel;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/packageinstaller/utils/TailStatManager$GameStatModel;

    invoke-direct {v0}, Lcom/android/packageinstaller/utils/TailStatManager$GameStatModel;-><init>()V

    iput-object v0, p0, Lcom/android/packageinstaller/utils/TailStatManager;->b:Lcom/android/packageinstaller/utils/TailStatManager$GameStatModel;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0, v1}, Lcom/android/packageinstaller/utils/TailStatManager$GameStatModel;->access$002(Lcom/android/packageinstaller/utils/TailStatManager$GameStatModel;Ljava/util/List;)Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/android/packageinstaller/utils/TailStatManager;->b:Lcom/android/packageinstaller/utils/TailStatManager$GameStatModel;

    return-object v0
.end method

.method private e()Lcom/android/packageinstaller/utils/TailStatManager$GameStatModel;
    .locals 2

    invoke-direct {p0}, Lcom/android/packageinstaller/utils/TailStatManager;->a()Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/android/packageinstaller/utils/TailStatManager$GameStatModel;

    invoke-static {v0, v1}, Lcom/android/packageinstaller/utils/j;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/packageinstaller/utils/TailStatManager$GameStatModel;

    return-object v0
.end method


# virtual methods
.method public f()V
    .locals 3

    invoke-direct {p0}, Lcom/android/packageinstaller/utils/TailStatManager;->d()Lcom/android/packageinstaller/utils/TailStatManager$GameStatModel;

    move-result-object v0

    invoke-static {v0}, Lcom/android/packageinstaller/utils/j;->c(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0}, Lcom/android/packageinstaller/utils/TailStatManager;->c()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "preference_key_name_tail_stat"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public g()V
    .locals 4

    :try_start_0
    invoke-direct {p0}, Lcom/android/packageinstaller/utils/TailStatManager;->d()Lcom/android/packageinstaller/utils/TailStatManager$GameStatModel;

    move-result-object v0

    invoke-static {v0}, Lcom/android/packageinstaller/utils/TailStatManager$GameStatModel;->access$000(Lcom/android/packageinstaller/utils/TailStatManager$GameStatModel;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-direct {p0}, Lcom/android/packageinstaller/utils/TailStatManager;->d()Lcom/android/packageinstaller/utils/TailStatManager$GameStatModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/packageinstaller/utils/TailStatManager$GameStatModel;->fillDefaultData()V

    invoke-direct {p0}, Lcom/android/packageinstaller/utils/TailStatManager;->d()Lcom/android/packageinstaller/utils/TailStatManager$GameStatModel;

    move-result-object v0

    const-string v1, "https://data.sec.miui.com/adv/tail"

    invoke-static {v0, v1}, Lcom/android/packageinstaller/utils/n;->m(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v1, "TailStatManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "result: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-class v1, Lcom/android/packageinstaller/utils/TailStatManager$NetWorkResultModel;

    invoke-static {v0, v1}, Lcom/android/packageinstaller/utils/j;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/packageinstaller/utils/TailStatManager$NetWorkResultModel;

    if-eqz v0, :cond_1

    invoke-static {v0}, Lcom/android/packageinstaller/utils/TailStatManager$NetWorkResultModel;->access$100(Lcom/android/packageinstaller/utils/TailStatManager$NetWorkResultModel;)I

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/android/packageinstaller/utils/TailStatManager;->d()Lcom/android/packageinstaller/utils/TailStatManager$GameStatModel;

    move-result-object v0

    invoke-static {v0}, Lcom/android/packageinstaller/utils/TailStatManager$GameStatModel;->access$000(Lcom/android/packageinstaller/utils/TailStatManager$GameStatModel;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    invoke-virtual {p0}, Lcom/android/packageinstaller/utils/TailStatManager;->f()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :cond_1
    :goto_0
    return-void
.end method
