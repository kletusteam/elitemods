.class Lcom/android/packageinstaller/job/TailStatJobService$a;
.super Landroid/os/AsyncTask;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/packageinstaller/job/TailStatJobService;->onStartJob(Landroid/app/job/JobParameters;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Landroid/app/job/JobParameters;

.field final synthetic b:Lcom/android/packageinstaller/job/TailStatJobService;


# direct methods
.method constructor <init>(Lcom/android/packageinstaller/job/TailStatJobService;Landroid/app/job/JobParameters;)V
    .locals 0

    iput-object p1, p0, Lcom/android/packageinstaller/job/TailStatJobService$a;->b:Lcom/android/packageinstaller/job/TailStatJobService;

    iput-object p2, p0, Lcom/android/packageinstaller/job/TailStatJobService$a;->a:Landroid/app/job/JobParameters;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 0

    invoke-static {}, Lcom/android/packageinstaller/utils/TailStatManager;->b()Lcom/android/packageinstaller/utils/TailStatManager;

    move-result-object p1

    invoke-virtual {p1}, Lcom/android/packageinstaller/utils/TailStatManager;->g()V

    const/4 p1, 0x0

    return-object p1
.end method

.method protected b(Ljava/lang/Void;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    iget-object p1, p0, Lcom/android/packageinstaller/job/TailStatJobService$a;->b:Lcom/android/packageinstaller/job/TailStatJobService;

    iget-object v0, p0, Lcom/android/packageinstaller/job/TailStatJobService$a;->a:Landroid/app/job/JobParameters;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/app/job/JobService;->jobFinished(Landroid/app/job/JobParameters;Z)V

    return-void
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/packageinstaller/job/TailStatJobService$a;->a([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/packageinstaller/job/TailStatJobService$a;->b(Ljava/lang/Void;)V

    return-void
.end method
