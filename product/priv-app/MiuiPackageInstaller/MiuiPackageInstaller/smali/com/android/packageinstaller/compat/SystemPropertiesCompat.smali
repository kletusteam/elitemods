.class public Lcom/android/packageinstaller/compat/SystemPropertiesCompat;
.super Ljava/lang/Object;


# static fields
.field private static final TAG:Ljava/lang/String; = "SystemPropertiesCompat"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 8

    const-class v0, Ljava/lang/String;

    :try_start_0
    const-string v1, "android.os.SystemProperties"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    const/4 v1, 0x2

    new-array v6, v1, [Ljava/lang/Class;

    const/4 v2, 0x0

    aput-object v0, v6, v2

    const/4 v4, 0x1

    aput-object v0, v6, v4

    const-string v0, "SystemPropertiesCompat"

    const-class v5, Ljava/lang/String;

    const-string v7, "get"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p0, v1, v2

    aput-object p1, v1, v4

    move-object v2, v0

    move-object v4, v5

    move-object v5, v7

    move-object v7, v1

    invoke-static/range {v2 .. v7}, Lcom/android/packageinstaller/utils/r;->c(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    invoke-virtual {p0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object p0

    const-string p1, "SystemPropertiesCompat"

    invoke-static {p1, p0}, Lf6/o;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    const/4 p0, 0x0

    return-object p0
.end method

.method public static getBoolean(Ljava/lang/String;Z)Z
    .locals 8

    const/4 v0, 0x0

    :try_start_0
    const-string v1, "android.os.SystemProperties"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    const/4 v1, 0x2

    new-array v6, v1, [Ljava/lang/Class;

    const-class v2, Ljava/lang/String;

    aput-object v2, v6, v0

    sget-object v4, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const/4 v2, 0x1

    aput-object v4, v6, v2

    const-string v5, "SystemPropertiesCompat"

    const-string v7, "getBoolean"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p0, v1, v0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    aput-object p0, v1, v2

    move-object v2, v5

    move-object v5, v7

    move-object v7, v1

    invoke-static/range {v2 .. v7}, Lcom/android/packageinstaller/utils/r;->c(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return p0

    :catch_0
    move-exception p0

    invoke-virtual {p0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object p0

    const-string p1, "SystemPropertiesCompat"

    invoke-static {p1, p0}, Lf6/o;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    return v0
.end method

.method public static getInt(Ljava/lang/String;I)I
    .locals 8

    const/4 v0, 0x0

    :try_start_0
    const-string v1, "android.os.SystemProperties"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    const/4 v1, 0x2

    new-array v6, v1, [Ljava/lang/Class;

    const-class v2, Ljava/lang/String;

    aput-object v2, v6, v0

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v2, 0x1

    aput-object v4, v6, v2

    const-string v5, "SystemPropertiesCompat"

    const-string v7, "getInt"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p0, v1, v0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    aput-object p0, v1, v2

    move-object v2, v5

    move-object v5, v7

    move-object v7, v1

    invoke-static/range {v2 .. v7}, Lcom/android/packageinstaller/utils/r;->c(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Integer;

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return p0

    :catch_0
    move-exception p0

    invoke-virtual {p0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object p0

    const-string p1, "SystemPropertiesCompat"

    invoke-static {p1, p0}, Lf6/o;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    return v0
.end method

.method public static getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 8

    const-class v0, Ljava/lang/String;

    :try_start_0
    const-string v1, "android.os.SystemProperties"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    const/4 v1, 0x2

    new-array v6, v1, [Ljava/lang/Class;

    const/4 v2, 0x0

    aput-object v0, v6, v2

    const/4 v4, 0x1

    aput-object v0, v6, v4

    const-string v0, "SystemPropertiesCompat"

    const-class v5, Ljava/lang/String;

    const-string v7, "get"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p0, v1, v2

    aput-object p1, v1, v4

    move-object v2, v0

    move-object v4, v5

    move-object v5, v7

    move-object v7, v1

    invoke-static/range {v2 .. v7}, Lcom/android/packageinstaller/utils/r;->c(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    invoke-virtual {p0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object p0

    const-string p1, "SystemPropertiesCompat"

    invoke-static {p1, p0}, Lf6/o;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    const/4 p0, 0x0

    return-object p0
.end method

.method public static set(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    const-class v0, Ljava/lang/String;

    const-string v1, "SystemPropertiesCompat"

    :try_start_0
    const-string v2, "android.os.SystemProperties"

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    const/4 v3, 0x2

    new-array v4, v3, [Ljava/lang/Class;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    const/4 v6, 0x1

    aput-object v0, v4, v6

    const-string v0, "set"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p0, v3, v5

    aput-object p1, v3, v6

    invoke-static {v1, v2, v0, v4, v3}, Lcom/android/packageinstaller/utils/r;->d(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    invoke-virtual {p0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v1, p0}, Lf6/o;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    :goto_0
    return-void
.end method
