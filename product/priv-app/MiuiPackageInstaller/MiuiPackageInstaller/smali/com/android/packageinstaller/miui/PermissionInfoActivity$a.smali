.class public Lcom/android/packageinstaller/miui/PermissionInfoActivity$a;
.super Lcom/android/packageinstaller/miui/a;

# interfaces
.implements Landroidx/preference/Preference$e;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/packageinstaller/miui/PermissionInfoActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field private C0:Lmiuix/preference/TextPreference;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/packageinstaller/miui/a;-><init>()V

    return-void
.end method

.method public static o2()Lcom/android/packageinstaller/miui/PermissionInfoActivity$a;
    .locals 1

    new-instance v0, Lcom/android/packageinstaller/miui/PermissionInfoActivity$a;

    invoke-direct {v0}, Lcom/android/packageinstaller/miui/PermissionInfoActivity$a;-><init>()V

    return-object v0
.end method


# virtual methods
.method public J0()V
    .locals 4

    invoke-super {p0}, Landroidx/fragment/app/Fragment;->J0()V

    new-instance v0, Lp5/g;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->p()Landroidx/fragment/app/e;

    move-result-object v1

    check-cast v1, Lo5/a;

    const-string v2, "permission_details_other_btn"

    const-string v3, "button"

    invoke-direct {v0, v2, v3, v1}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    return-void
.end method

.method public Q1(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 3

    invoke-super {p0, p1, p2}, Lcom/android/packageinstaller/miui/a;->Q1(Landroid/os/Bundle;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/android/packageinstaller/miui/a;->z0:Landroid/content/pm/PackageInfo;

    if-nez p1, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->w()Landroid/content/Context;

    move-result-object p1

    iget-object p2, p0, Lcom/android/packageinstaller/miui/a;->z0:Landroid/content/pm/PackageInfo;

    invoke-static {p1, p2}, Lcom/android/packageinstaller/miui/a;->n2(Landroid/content/Context;Landroid/content/pm/PackageInfo;)Lcom/android/packageinstaller/miui/a$a;

    move-result-object p1

    const-string p2, "privacy_relative"

    invoke-virtual {p0, p2}, Landroidx/preference/g;->g(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p2

    check-cast p2, Landroidx/preference/PreferenceCategory;

    const-string v0, "security_relative"

    invoke-virtual {p0, v0}, Landroidx/preference/g;->g(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    check-cast v0, Landroidx/preference/PreferenceCategory;

    const-string v1, "other_relative"

    invoke-virtual {p0, v1}, Landroidx/preference/g;->g(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v1

    check-cast v1, Landroidx/preference/PreferenceCategory;

    const-string v2, "other_relative_item"

    invoke-virtual {p0, v2}, Landroidx/preference/g;->g(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v2

    check-cast v2, Lmiuix/preference/TextPreference;

    iput-object v2, p0, Lcom/android/packageinstaller/miui/PermissionInfoActivity$a;->C0:Lmiuix/preference/TextPreference;

    invoke-virtual {p1}, Lcom/android/packageinstaller/miui/a$a;->d()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {p0}, Landroidx/preference/g;->M1()Landroidx/preference/PreferenceScreen;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroidx/preference/PreferenceGroup;->Q0(Landroidx/preference/Preference;)Z

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/android/packageinstaller/miui/PermissionInfoActivity$a;->C0:Lmiuix/preference/TextPreference;

    invoke-virtual {v1, p0}, Landroidx/preference/Preference;->v0(Landroidx/preference/Preference$e;)V

    :goto_0
    invoke-virtual {p1}, Lcom/android/packageinstaller/miui/a$a;->e()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {p0, v1, p2}, Lcom/android/packageinstaller/miui/a;->j2(Ljava/util/Map;Landroidx/preference/PreferenceCategory;)V

    invoke-virtual {p1}, Lcom/android/packageinstaller/miui/a$a;->f()Ljava/util/Map;

    move-result-object p2

    invoke-virtual {p0, p2, v0}, Lcom/android/packageinstaller/miui/a;->j2(Ljava/util/Map;Landroidx/preference/PreferenceCategory;)V

    invoke-virtual {p1}, Lcom/android/packageinstaller/miui/a$a;->f()Ljava/util/Map;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Map;->size()I

    move-result p2

    if-nez p2, :cond_2

    invoke-virtual {p1}, Lcom/android/packageinstaller/miui/a$a;->d()Ljava/util/Map;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result p1

    if-nez p1, :cond_2

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->p()Landroidx/fragment/app/e;

    move-result-object p1

    const p2, 0x1020002

    invoke-virtual {p1, p2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->p()Landroidx/fragment/app/e;

    move-result-object p2

    invoke-virtual {p2}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object p2

    const v0, 0x7f0d00bc

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :cond_2
    iget-object p1, p0, Lcom/android/packageinstaller/miui/a;->A0:Ljava/lang/String;

    if-eqz p1, :cond_3

    goto :goto_1

    :cond_3
    iget-object p1, p0, Lcom/android/packageinstaller/miui/a;->z0:Landroid/content/pm/PackageInfo;

    iget-object p1, p1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    if-eqz p1, :cond_4

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->w()Landroid/content/Context;

    move-result-object p2

    invoke-virtual {p2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object p1

    goto :goto_1

    :cond_4
    const-string p1, ""

    :goto_1
    iget-object p2, p0, Lcom/android/packageinstaller/miui/a;->B0:Lmiuix/appcompat/app/j;

    if-eqz p2, :cond_5

    invoke-virtual {p2}, Lmiuix/appcompat/app/j;->r0()Lmiuix/appcompat/app/a;

    move-result-object p2

    if-eqz p2, :cond_5

    const v0, 0x7f1101e1

    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->T(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Ld/a;->v(Ljava/lang/CharSequence;)V

    :cond_5
    return-void
.end method

.method public f(Landroidx/preference/Preference;)Z
    .locals 3

    iget-object v0, p0, Lcom/android/packageinstaller/miui/PermissionInfoActivity$a;->C0:Lmiuix/preference/TextPreference;

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->p()Landroidx/fragment/app/e;

    move-result-object p1

    iget-object v0, p0, Lcom/android/packageinstaller/miui/a;->z0:Landroid/content/pm/PackageInfo;

    invoke-static {p1, v0}, Lcom/android/packageinstaller/miui/OtherPermissionInfoActivity;->J0(Landroid/content/Context;Landroid/content/pm/PackageInfo;)V

    new-instance p1, Lp5/b;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->p()Landroidx/fragment/app/e;

    move-result-object v0

    check-cast v0, Lo5/a;

    const-string v1, "permission_details_other_btn"

    const-string v2, "button"

    invoke-direct {p1, v1, v2, v0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p1}, Lp5/f;->c()Z

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method protected m2()I
    .locals 1

    const v0, 0x7f140001

    return v0
.end method
