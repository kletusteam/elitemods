.class public Lcom/android/packageinstaller/compat/SessionInfoCompat;
.super Ljava/lang/Object;


# static fields
.field public static final TAG:Ljava/lang/String; = "SessionInfoCompat"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static resolvedBaseCodePath(Landroid/content/pm/PackageInstaller$SessionInfo;)Ljava/lang/String;
    .locals 3

    const-class v0, Ljava/lang/String;

    const-string v1, "SessionInfoCompat"

    const-string v2, "resolvedBaseCodePath"

    invoke-static {v1, p0, v2, v0}, Lcom/android/packageinstaller/utils/r;->e(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/String;

    return-object p0
.end method

.method public static sealed(Landroid/content/pm/PackageInstaller$SessionInfo;)Z
    .locals 3

    sget-object v0, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const-string v1, "SessionInfoCompat"

    const-string v2, "sealed"

    invoke-static {v1, p0, v2, v0}, Lcom/android/packageinstaller/utils/r;->e(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method
