.class public Lcom/android/packageinstaller/SettingsActivity;
.super Lq2/b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/packageinstaller/SettingsActivity$a;
    }
.end annotation


# instance fields
.field private u:Lm5/e;

.field private v:Lcom/miui/packageInstaller/model/ApkInfo;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lq2/b;-><init>()V

    return-void
.end method

.method static synthetic J0(Lcom/android/packageinstaller/SettingsActivity;)Lo5/b;
    .locals 0

    iget-object p0, p0, Lq2/b;->q:Lo5/b;

    return-object p0
.end method

.method static synthetic K0(Lcom/android/packageinstaller/SettingsActivity;)Lm5/e;
    .locals 0

    iget-object p0, p0, Lcom/android/packageinstaller/SettingsActivity;->u:Lm5/e;

    return-object p0
.end method

.method static synthetic L0(Lcom/android/packageinstaller/SettingsActivity;)Lcom/miui/packageInstaller/model/ApkInfo;
    .locals 0

    iget-object p0, p0, Lcom/android/packageinstaller/SettingsActivity;->v:Lcom/miui/packageInstaller/model/ApkInfo;

    return-object p0
.end method


# virtual methods
.method public E0()Ljava/lang/String;
    .locals 1

    const-string v0, "installing_settings"

    return-object v0
.end method

.method public onBackPressed()V
    .locals 3

    invoke-super {p0}, Lmiuix/appcompat/app/j;->onBackPressed()V

    new-instance v0, Lp5/b;

    const-string v1, "page_back_btn"

    const-string v2, "button"

    invoke-direct {v0, v1, v2, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    const-string v1, "back_type"

    const-string v2, "system"

    invoke-virtual {v0, v1, v2}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object v0

    invoke-virtual {v0}, Lp5/f;->c()Z

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lq2/b;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "apk_info"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Lcom/miui/packageInstaller/model/ApkInfo;

    iput-object p1, p0, Lcom/android/packageinstaller/SettingsActivity;->v:Lcom/miui/packageInstaller/model/ApkInfo;

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "caller"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Lm5/e;

    iput-object p1, p0, Lcom/android/packageinstaller/SettingsActivity;->u:Lm5/e;

    invoke-virtual {p0}, Landroidx/fragment/app/e;->X()Landroidx/fragment/app/m;

    move-result-object p1

    const-string v0, "SettingsActivity"

    invoke-virtual {p1, v0}, Landroidx/fragment/app/m;->h0(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/android/packageinstaller/SettingsActivity$a;->n2()Lcom/android/packageinstaller/SettingsActivity$a;

    move-result-object v1

    invoke-virtual {p1}, Landroidx/fragment/app/m;->l()Landroidx/fragment/app/v;

    move-result-object p1

    const v2, 0x1020002

    invoke-virtual {p1, v2, v1, v0}, Landroidx/fragment/app/v;->b(ILandroidx/fragment/app/Fragment;Ljava/lang/String;)Landroidx/fragment/app/v;

    invoke-virtual {p1}, Landroidx/fragment/app/v;->g()I

    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    new-instance v0, Lp5/b;

    const-string v1, "page_back_btn"

    const-string v2, "button"

    invoke-direct {v0, v1, v2, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    const-string v1, "back_type"

    const-string v2, "click_icon"

    invoke-virtual {v0, v1, v2}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object v0

    invoke-virtual {v0}, Lp5/f;->c()Z

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result p1

    return p1
.end method

.method protected onStop()V
    .locals 0

    invoke-super {p0}, Lq2/b;->onStop()V

    return-void
.end method
