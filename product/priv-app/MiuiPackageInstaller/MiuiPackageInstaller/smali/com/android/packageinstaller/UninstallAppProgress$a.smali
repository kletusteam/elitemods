.class Lcom/android/packageinstaller/UninstallAppProgress$a;
.super Landroid/os/Handler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/packageinstaller/UninstallAppProgress;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/android/packageinstaller/UninstallAppProgress;


# direct methods
.method constructor <init>(Lcom/android/packageinstaller/UninstallAppProgress;)V
    .locals 0

    iput-object p1, p0, Lcom/android/packageinstaller/UninstallAppProgress$a;->a:Lcom/android/packageinstaller/UninstallAppProgress;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 16

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    iget-object v0, v1, Lcom/android/packageinstaller/UninstallAppProgress$a;->a:Lcom/android/packageinstaller/UninstallAppProgress;

    invoke-virtual {v0}, Lmiuix/appcompat/app/j;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_15

    iget-object v0, v1, Lcom/android/packageinstaller/UninstallAppProgress$a;->a:Lcom/android/packageinstaller/UninstallAppProgress;

    invoke-virtual {v0}, Landroid/app/Activity;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_a

    :cond_0
    iget v0, v2, Landroid/os/Message;->what:I

    const/4 v3, 0x2

    const/4 v4, 0x1

    if-eq v0, v4, :cond_2

    if-eq v0, v3, :cond_1

    goto/16 :goto_a

    :cond_1
    iget-object v0, v1, Lcom/android/packageinstaller/UninstallAppProgress$a;->a:Lcom/android/packageinstaller/UninstallAppProgress;

    invoke-virtual {v0}, Lcom/android/packageinstaller/UninstallAppProgress;->S0()V

    goto/16 :goto_a

    :cond_2
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v5

    iget-object v0, v1, Lcom/android/packageinstaller/UninstallAppProgress$a;->a:Lcom/android/packageinstaller/UninstallAppProgress;

    invoke-static {v0}, Lcom/android/packageinstaller/UninstallAppProgress;->J0(Lcom/android/packageinstaller/UninstallAppProgress;)J

    move-result-wide v7

    sub-long/2addr v5, v7

    const-wide/16 v7, 0x64

    div-long/2addr v5, v7

    iget-object v0, v1, Lcom/android/packageinstaller/UninstallAppProgress$a;->a:Lcom/android/packageinstaller/UninstallAppProgress;

    invoke-static {v0}, Lcom/android/packageinstaller/UninstallAppProgress;->K0(Lcom/android/packageinstaller/UninstallAppProgress;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    iget v0, v2, Landroid/os/Message;->arg1:I

    if-eq v0, v4, :cond_3

    iget-object v0, v1, Lcom/android/packageinstaller/UninstallAppProgress$a;->a:Lcom/android/packageinstaller/UninstallAppProgress;

    invoke-virtual {v0}, Lcom/android/packageinstaller/UninstallAppProgress;->S0()V

    :cond_3
    iget-object v0, v1, Lcom/android/packageinstaller/UninstallAppProgress$a;->a:Lcom/android/packageinstaller/UninstallAppProgress;

    iget v3, v2, Landroid/os/Message;->arg1:I

    invoke-static {v0, v3}, Lcom/android/packageinstaller/UninstallAppProgress;->M0(Lcom/android/packageinstaller/UninstallAppProgress;I)I

    iget-object v0, v2, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object v3, v0

    check-cast v3, Ljava/lang/String;

    iget-object v0, v1, Lcom/android/packageinstaller/UninstallAppProgress$a;->a:Lcom/android/packageinstaller/UninstallAppProgress;

    invoke-static {v0}, Lcom/android/packageinstaller/UninstallAppProgress;->N0(Lcom/android/packageinstaller/UninstallAppProgress;)Landroid/os/IBinder;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, v1, Lcom/android/packageinstaller/UninstallAppProgress$a;->a:Lcom/android/packageinstaller/UninstallAppProgress;

    invoke-static {v0}, Lcom/android/packageinstaller/UninstallAppProgress;->N0(Lcom/android/packageinstaller/UninstallAppProgress;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/content/pm/IPackageDeleteObserver2$Stub;->asInterface(Landroid/os/IBinder;)Landroid/content/pm/IPackageDeleteObserver2;

    move-result-object v0

    :try_start_0
    iget-object v5, v1, Lcom/android/packageinstaller/UninstallAppProgress$a;->a:Lcom/android/packageinstaller/UninstallAppProgress;

    invoke-static {v5}, Lcom/android/packageinstaller/UninstallAppProgress;->O0(Lcom/android/packageinstaller/UninstallAppProgress;)Landroid/content/pm/ApplicationInfo;

    move-result-object v5

    iget-object v5, v5, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v6, v1, Lcom/android/packageinstaller/UninstallAppProgress$a;->a:Lcom/android/packageinstaller/UninstallAppProgress;

    invoke-static {v6}, Lcom/android/packageinstaller/UninstallAppProgress;->L0(Lcom/android/packageinstaller/UninstallAppProgress;)I

    move-result v6

    invoke-interface {v0, v5, v6, v3}, Landroid/content/pm/IPackageDeleteObserver2;->onPackageDeleted(Ljava/lang/String;ILjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_4
    iget-object v0, v1, Lcom/android/packageinstaller/UninstallAppProgress$a;->a:Lcom/android/packageinstaller/UninstallAppProgress;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v5, "android.intent.extra.RETURN_RESULT"

    const/4 v6, 0x0

    invoke-virtual {v0, v5, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_6

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iget-object v5, v1, Lcom/android/packageinstaller/UninstallAppProgress$a;->a:Lcom/android/packageinstaller/UninstallAppProgress;

    invoke-static {v5}, Lcom/android/packageinstaller/UninstallAppProgress;->L0(Lcom/android/packageinstaller/UninstallAppProgress;)I

    move-result v5

    const-string v7, "android.intent.extra.INSTALL_RESULT"

    invoke-virtual {v0, v7, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v5, v1, Lcom/android/packageinstaller/UninstallAppProgress$a;->a:Lcom/android/packageinstaller/UninstallAppProgress;

    invoke-static {v5}, Lcom/android/packageinstaller/UninstallAppProgress;->L0(Lcom/android/packageinstaller/UninstallAppProgress;)I

    move-result v7

    if-ne v7, v4, :cond_5

    const/4 v7, -0x1

    goto :goto_0

    :cond_5
    move v7, v4

    :goto_0
    invoke-virtual {v5, v7, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    :cond_6
    iget v0, v2, Landroid/os/Message;->arg1:I

    const/4 v5, -0x4

    const-string v7, "Failed to talk to package manager"

    const v8, 0x7f1103cc

    const-string v9, " with code "

    const-string v10, "Uninstall failed for "

    const-string v12, "user"

    const/16 v13, 0x8

    const-string v14, "UninstallAppProgress"

    if-eq v0, v5, :cond_d

    const/4 v5, -0x2

    if-eq v0, v5, :cond_8

    if-eq v0, v4, :cond_7

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, v2, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :goto_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v14, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, v1, Lcom/android/packageinstaller/UninstallAppProgress$a;->a:Lcom/android/packageinstaller/UninstallAppProgress;

    invoke-virtual {v0, v8}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_8

    :cond_7
    iget-object v0, v1, Lcom/android/packageinstaller/UninstallAppProgress$a;->a:Lcom/android/packageinstaller/UninstallAppProgress;

    const v3, 0x7f1103cb

    invoke-virtual {v0, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v3, v1, Lcom/android/packageinstaller/UninstallAppProgress$a;->a:Lcom/android/packageinstaller/UninstallAppProgress;

    invoke-virtual {v3}, Landroid/app/Activity;->getBaseContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v0, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto/16 :goto_8

    :cond_8
    iget-object v0, v1, Lcom/android/packageinstaller/UninstallAppProgress$a;->a:Lcom/android/packageinstaller/UninstallAppProgress;

    invoke-virtual {v0, v12}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Landroid/os/UserManager;

    const-string v0, "device_policy"

    invoke-static {v0}, Lcom/android/packageinstaller/compat/ServiceManagerCompat;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/app/admin/IDevicePolicyManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/admin/IDevicePolicyManager;

    move-result-object v8

    invoke-static {}, Lcom/android/packageinstaller/compat/UserHandleCompat;->myUserId()I

    move-result v9

    invoke-static {v5}, Lcom/android/packageinstaller/compat/UserManagerCompat;->getUsers(Landroid/os/UserManager;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_9
    :goto_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/UserInfo;

    iget-object v12, v1, Lcom/android/packageinstaller/UninstallAppProgress$a;->a:Lcom/android/packageinstaller/UninstallAppProgress;

    iget v15, v0, Landroid/content/pm/UserInfo;->id:I

    invoke-static {v12, v5, v9, v15}, Lcom/android/packageinstaller/UninstallAppProgress;->P0(Lcom/android/packageinstaller/UninstallAppProgress;Landroid/os/UserManager;II)Z

    move-result v12

    if-eqz v12, :cond_a

    goto :goto_2

    :cond_a
    :try_start_1
    iget v12, v0, Landroid/content/pm/UserInfo;->id:I

    invoke-interface {v8, v3, v12}, Landroid/app/admin/IDevicePolicyManager;->packageHasActiveAdmins(Ljava/lang/String;I)Z

    move-result v12
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    if-eqz v12, :cond_9

    goto :goto_3

    :catch_1
    move-exception v0

    invoke-static {v14, v7, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    :cond_b
    const/4 v0, 0x0

    :goto_3
    const-string v5, "Uninstall failed because "

    if-nez v0, :cond_c

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " is a device admin"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v14, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, v1, Lcom/android/packageinstaller/UninstallAppProgress$a;->a:Lcom/android/packageinstaller/UninstallAppProgress;

    invoke-static {v0}, Lcom/android/packageinstaller/UninstallAppProgress;->Q0(Lcom/android/packageinstaller/UninstallAppProgress;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, v1, Lcom/android/packageinstaller/UninstallAppProgress$a;->a:Lcom/android/packageinstaller/UninstallAppProgress;

    const v3, 0x7f1103cd

    :goto_4
    invoke-virtual {v0, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_8

    :cond_c
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " is a device admin of user "

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v14, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, v1, Lcom/android/packageinstaller/UninstallAppProgress$a;->a:Lcom/android/packageinstaller/UninstallAppProgress;

    invoke-static {v3}, Lcom/android/packageinstaller/UninstallAppProgress;->Q0(Lcom/android/packageinstaller/UninstallAppProgress;)Landroid/widget/Button;

    move-result-object v3

    invoke-virtual {v3, v13}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v3, v1, Lcom/android/packageinstaller/UninstallAppProgress$a;->a:Lcom/android/packageinstaller/UninstallAppProgress;

    const v5, 0x7f1103ce

    invoke-virtual {v3, v5}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v5, v4, [Ljava/lang/Object;

    iget-object v0, v0, Landroid/content/pm/UserInfo;->name:Ljava/lang/String;

    aput-object v0, v5, v6

    invoke-static {v3, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_8

    :cond_d
    iget-object v0, v1, Lcom/android/packageinstaller/UninstallAppProgress$a;->a:Lcom/android/packageinstaller/UninstallAppProgress;

    invoke-virtual {v0, v12}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Landroid/os/UserManager;

    invoke-static {v5}, Lcom/android/packageinstaller/compat/UserManagerCompat;->getUsers(Landroid/os/UserManager;)Ljava/util/List;

    move-result-object v12

    move v15, v6

    :goto_5
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v0

    const/16 v11, -0x2710

    if-ge v15, v0, :cond_f

    invoke-interface {v12, v15}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/UserInfo;

    :try_start_2
    iget v4, v0, Landroid/content/pm/UserInfo;->id:I

    invoke-static {v3, v4}, Lcom/android/packageinstaller/compat/IPackageManagerCompat;->getBlockUninstallForUser(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_e

    iget v0, v0, Landroid/content/pm/UserInfo;->id:I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_6

    :catch_2
    move-exception v0

    invoke-static {v14, v7, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_e
    add-int/lit8 v15, v15, 0x1

    const/4 v4, 0x1

    goto :goto_5

    :cond_f
    move v0, v11

    :goto_6
    invoke-static {}, Lcom/android/packageinstaller/compat/UserHandleCompat;->myUserId()I

    move-result v4

    iget-object v7, v1, Lcom/android/packageinstaller/UninstallAppProgress$a;->a:Lcom/android/packageinstaller/UninstallAppProgress;

    invoke-static {v7, v5, v4, v0}, Lcom/android/packageinstaller/UninstallAppProgress;->P0(Lcom/android/packageinstaller/UninstallAppProgress;Landroid/os/UserManager;II)Z

    move-result v4

    if-eqz v4, :cond_10

    iget-object v4, v1, Lcom/android/packageinstaller/UninstallAppProgress$a;->a:Lcom/android/packageinstaller/UninstallAppProgress;

    invoke-static {v4}, Lcom/android/packageinstaller/UninstallAppProgress;->Q0(Lcom/android/packageinstaller/UninstallAppProgress;)Landroid/widget/Button;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_7

    :cond_10
    iget-object v4, v1, Lcom/android/packageinstaller/UninstallAppProgress$a;->a:Lcom/android/packageinstaller/UninstallAppProgress;

    invoke-static {v4}, Lcom/android/packageinstaller/UninstallAppProgress;->Q0(Lcom/android/packageinstaller/UninstallAppProgress;)Landroid/widget/Button;

    move-result-object v4

    invoke-virtual {v4, v13}, Landroid/widget/Button;->setVisibility(I)V

    :goto_7
    if-nez v0, :cond_11

    iget-object v0, v1, Lcom/android/packageinstaller/UninstallAppProgress$a;->a:Lcom/android/packageinstaller/UninstallAppProgress;

    const v3, 0x7f1103c9

    goto/16 :goto_4

    :cond_11
    if-ne v0, v11, :cond_12

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, v2, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, " no blocking user"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    :cond_12
    iget-object v0, v1, Lcom/android/packageinstaller/UninstallAppProgress$a;->a:Lcom/android/packageinstaller/UninstallAppProgress;

    invoke-static {v0}, Lcom/android/packageinstaller/UninstallAppProgress;->R0(Lcom/android/packageinstaller/UninstallAppProgress;)Z

    move-result v0

    if-eqz v0, :cond_13

    iget-object v0, v1, Lcom/android/packageinstaller/UninstallAppProgress$a;->a:Lcom/android/packageinstaller/UninstallAppProgress;

    const v3, 0x7f1103c4

    goto/16 :goto_4

    :cond_13
    iget-object v0, v1, Lcom/android/packageinstaller/UninstallAppProgress$a;->a:Lcom/android/packageinstaller/UninstallAppProgress;

    const v3, 0x7f1103ca

    goto/16 :goto_4

    :goto_8
    iget-object v3, v1, Lcom/android/packageinstaller/UninstallAppProgress$a;->a:Lcom/android/packageinstaller/UninstallAppProgress;

    const v4, 0x7f0a02b7

    invoke-virtual {v3, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v13}, Landroid/view/View;->setVisibility(I)V

    iget-object v3, v1, Lcom/android/packageinstaller/UninstallAppProgress$a;->a:Lcom/android/packageinstaller/UninstallAppProgress;

    const v4, 0x7f0a0345

    invoke-virtual {v3, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    iget-object v3, v1, Lcom/android/packageinstaller/UninstallAppProgress$a;->a:Lcom/android/packageinstaller/UninstallAppProgress;

    const v4, 0x7f0a0286

    invoke-virtual {v3, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    iget v2, v2, Landroid/os/Message;->arg1:I

    const v3, 0x7f0a0343

    const/4 v4, 0x1

    if-ne v2, v4, :cond_14

    iget-object v0, v1, Lcom/android/packageinstaller/UninstallAppProgress$a;->a:Lcom/android/packageinstaller/UninstallAppProgress;

    const v2, 0x7f0a00d8

    invoke-virtual {v0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    new-array v2, v4, [Landroid/view/View;

    aput-object v0, v2, v6

    invoke-static {v2}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object v2

    invoke-interface {v2}, Lmiuix/animation/e;->c()Lmiuix/animation/j;

    move-result-object v2

    const/high16 v4, 0x3f800000    # 1.0f

    new-array v5, v6, [Lmiuix/animation/j$b;

    invoke-interface {v2, v4, v5}, Lmiuix/animation/j;->G(F[Lmiuix/animation/j$b;)Lmiuix/animation/j;

    move-result-object v2

    new-array v4, v6, [Lc9/a;

    invoke-interface {v2, v0, v4}, Lmiuix/animation/j;->z(Landroid/view/View;[Lc9/a;)V

    iget-object v2, v1, Lcom/android/packageinstaller/UninstallAppProgress$a;->a:Lcom/android/packageinstaller/UninstallAppProgress;

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, v1, Lcom/android/packageinstaller/UninstallAppProgress$a;->a:Lcom/android/packageinstaller/UninstallAppProgress;

    const v2, 0x7f0a0344

    invoke-virtual {v0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, v1, Lcom/android/packageinstaller/UninstallAppProgress$a;->a:Lcom/android/packageinstaller/UninstallAppProgress;

    invoke-virtual {v0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v2, 0x7f1101e4

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, v1, Lcom/android/packageinstaller/UninstallAppProgress$a;->a:Lcom/android/packageinstaller/UninstallAppProgress;

    const v2, 0x7f0a01ac

    invoke-virtual {v0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    const v2, 0x7f1101e6

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_9

    :cond_14
    iget-object v2, v1, Lcom/android/packageinstaller/UninstallAppProgress$a;->a:Lcom/android/packageinstaller/UninstallAppProgress;

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_9
    invoke-static {}, Lcom/android/packageinstaller/utils/g;->s()Z

    move-result v0

    if-eqz v0, :cond_15

    iget-object v0, v1, Lcom/android/packageinstaller/UninstallAppProgress$a;->a:Lcom/android/packageinstaller/UninstallAppProgress;

    invoke-static {v0}, Lcom/android/packageinstaller/UninstallAppProgress;->O0(Lcom/android/packageinstaller/UninstallAppProgress;)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_15

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v2, v1, Lcom/android/packageinstaller/UninstallAppProgress$a;->a:Lcom/android/packageinstaller/UninstallAppProgress;

    invoke-static {v2}, Lcom/android/packageinstaller/UninstallAppProgress;->O0(Lcom/android/packageinstaller/UninstallAppProgress;)Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    iget-object v2, v2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v2, v1, Lcom/android/packageinstaller/UninstallAppProgress$a;->a:Lcom/android/packageinstaller/UninstallAppProgress;

    const/4 v3, 0x0

    invoke-static {v2, v0, v3}, Lcom/miui/hybrid/accessory/sdk/HybridAccessoryClient;->showCreateIconDialog(Landroid/content/Context;Ljava/util/List;Ljava/util/Map;)V

    :cond_15
    :goto_a
    return-void
.end method
