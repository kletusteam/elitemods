.class public Lcom/android/packageinstaller/UninstallAppProgress;
.super Lq2/b;

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/packageinstaller/UninstallAppProgress$d;
    }
.end annotation


# instance fields
.field private A:Landroid/widget/Button;

.field private B:Landroid/widget/Button;

.field private volatile C:I

.field private D:Z

.field private E:J

.field private F:Landroid/os/Handler;

.field private final u:Ljava/lang/String;

.field private v:Landroid/content/pm/ApplicationInfo;

.field private w:Z

.field private x:Landroid/os/UserHandle;

.field private y:Landroid/os/IBinder;

.field private z:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lq2/b;-><init>()V

    const-string v0, "UninstallAppProgress"

    iput-object v0, p0, Lcom/android/packageinstaller/UninstallAppProgress;->u:Ljava/lang/String;

    const/16 v0, -0x64

    iput v0, p0, Lcom/android/packageinstaller/UninstallAppProgress;->C:I

    new-instance v0, Lcom/android/packageinstaller/UninstallAppProgress$a;

    invoke-direct {v0, p0}, Lcom/android/packageinstaller/UninstallAppProgress$a;-><init>(Lcom/android/packageinstaller/UninstallAppProgress;)V

    iput-object v0, p0, Lcom/android/packageinstaller/UninstallAppProgress;->F:Landroid/os/Handler;

    return-void
.end method

.method static synthetic J0(Lcom/android/packageinstaller/UninstallAppProgress;)J
    .locals 2

    iget-wide v0, p0, Lcom/android/packageinstaller/UninstallAppProgress;->E:J

    return-wide v0
.end method

.method static synthetic K0(Lcom/android/packageinstaller/UninstallAppProgress;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/android/packageinstaller/UninstallAppProgress;->F:Landroid/os/Handler;

    return-object p0
.end method

.method static synthetic L0(Lcom/android/packageinstaller/UninstallAppProgress;)I
    .locals 0

    iget p0, p0, Lcom/android/packageinstaller/UninstallAppProgress;->C:I

    return p0
.end method

.method static synthetic M0(Lcom/android/packageinstaller/UninstallAppProgress;I)I
    .locals 0

    iput p1, p0, Lcom/android/packageinstaller/UninstallAppProgress;->C:I

    return p1
.end method

.method static synthetic N0(Lcom/android/packageinstaller/UninstallAppProgress;)Landroid/os/IBinder;
    .locals 0

    iget-object p0, p0, Lcom/android/packageinstaller/UninstallAppProgress;->y:Landroid/os/IBinder;

    return-object p0
.end method

.method static synthetic O0(Lcom/android/packageinstaller/UninstallAppProgress;)Landroid/content/pm/ApplicationInfo;
    .locals 0

    iget-object p0, p0, Lcom/android/packageinstaller/UninstallAppProgress;->v:Landroid/content/pm/ApplicationInfo;

    return-object p0
.end method

.method static synthetic P0(Lcom/android/packageinstaller/UninstallAppProgress;Landroid/os/UserManager;II)Z
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/android/packageinstaller/UninstallAppProgress;->T0(Landroid/os/UserManager;II)Z

    move-result p0

    return p0
.end method

.method static synthetic Q0(Lcom/android/packageinstaller/UninstallAppProgress;)Landroid/widget/Button;
    .locals 0

    iget-object p0, p0, Lcom/android/packageinstaller/UninstallAppProgress;->A:Landroid/widget/Button;

    return-object p0
.end method

.method static synthetic R0(Lcom/android/packageinstaller/UninstallAppProgress;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/android/packageinstaller/UninstallAppProgress;->w:Z

    return p0
.end method

.method private T0(Landroid/os/UserManager;II)Z
    .locals 1

    const/4 v0, 0x1

    if-ne p2, p3, :cond_0

    return v0

    :cond_0
    invoke-static {p1, p3}, Lcom/android/packageinstaller/compat/UserManagerCompat;->getProfileParent(Landroid/os/UserManager;I)Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_1

    invoke-static {p1}, Lcom/android/packageinstaller/compat/UserManagerCompat;->id(Ljava/lang/Object;)I

    move-result p1

    if-ne p1, p2, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method


# virtual methods
.method public S0()V
    .locals 6

    iget-boolean v0, p0, Lcom/android/packageinstaller/UninstallAppProgress;->D:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/packageinstaller/UninstallAppProgress;->D:Z

    iget-object v1, p0, Lcom/android/packageinstaller/UninstallAppProgress;->v:Landroid/content/pm/ApplicationInfo;

    iget v1, v1, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit16 v1, v1, 0x80

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    move v1, v0

    goto :goto_0

    :cond_1
    move v1, v2

    :goto_0
    if-eqz v1, :cond_2

    const v1, 0x7f1103d1

    goto :goto_1

    :cond_2
    const v1, 0x7f1103c8

    :goto_1
    invoke-virtual {p0, v1}, Landroid/app/Activity;->setTitle(I)V

    const v1, 0x7f0d01c3

    invoke-virtual {p0, v1}, Lmiuix/appcompat/app/j;->setContentView(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-static {v1}, Lcom/android/packageinstaller/utils/u;->c(Landroid/view/Window;)V

    const v1, 0x7f0a007a

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v3, p0, Lcom/android/packageinstaller/UninstallAppProgress;->v:Landroid/content/pm/ApplicationInfo;

    invoke-static {p0, v3, v1}, Lj2/f;->m(Landroid/app/Activity;Landroid/content/pm/ApplicationInfo;Landroid/view/View;)Landroid/view/View;

    invoke-static {}, Landroid/os/Process;->myUserHandle()Landroid/os/UserHandle;

    move-result-object v3

    iget-object v4, p0, Lcom/android/packageinstaller/UninstallAppProgress;->x:Landroid/os/UserHandle;

    invoke-virtual {v3, v4}, Landroid/os/UserHandle;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    invoke-virtual {p0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    iget-object v4, p0, Lcom/android/packageinstaller/UninstallAppProgress;->v:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v4, v3}, Landroid/content/pm/ApplicationInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    iget-object v5, p0, Lcom/android/packageinstaller/UninstallAppProgress;->x:Landroid/os/UserHandle;

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getUserBadgedIcon(Landroid/graphics/drawable/Drawable;Landroid/os/UserHandle;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    const v4, 0x7f0a0073

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_3
    const v1, 0x7f0a0127

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/android/packageinstaller/UninstallAppProgress;->A:Landroid/widget/Button;

    const v1, 0x7f0a03db

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/android/packageinstaller/UninstallAppProgress;->B:Landroid/widget/Button;

    new-array v1, v0, [Landroid/view/View;

    iget-object v3, p0, Lcom/android/packageinstaller/UninstallAppProgress;->A:Landroid/widget/Button;

    aput-object v3, v1, v2

    invoke-static {v1}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object v1

    invoke-interface {v1}, Lmiuix/animation/e;->c()Lmiuix/animation/j;

    move-result-object v1

    new-array v3, v2, [Lmiuix/animation/j$b;

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-interface {v1, v4, v3}, Lmiuix/animation/j;->G(F[Lmiuix/animation/j$b;)Lmiuix/animation/j;

    move-result-object v1

    iget-object v3, p0, Lcom/android/packageinstaller/UninstallAppProgress;->A:Landroid/widget/Button;

    new-array v5, v2, [Lc9/a;

    invoke-interface {v1, v3, v5}, Lmiuix/animation/j;->z(Landroid/view/View;[Lc9/a;)V

    new-array v1, v0, [Landroid/view/View;

    iget-object v3, p0, Lcom/android/packageinstaller/UninstallAppProgress;->B:Landroid/widget/Button;

    aput-object v3, v1, v2

    invoke-static {v1}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object v1

    invoke-interface {v1}, Lmiuix/animation/e;->c()Lmiuix/animation/j;

    move-result-object v1

    new-array v3, v2, [Lmiuix/animation/j$b;

    invoke-interface {v1, v4, v3}, Lmiuix/animation/j;->G(F[Lmiuix/animation/j$b;)Lmiuix/animation/j;

    move-result-object v1

    iget-object v3, p0, Lcom/android/packageinstaller/UninstallAppProgress;->B:Landroid/widget/Button;

    new-array v5, v2, [Lc9/a;

    invoke-interface {v1, v3, v5}, Lmiuix/animation/j;->z(Landroid/view/View;[Lc9/a;)V

    iget-object v1, p0, Lcom/android/packageinstaller/UninstallAppProgress;->A:Landroid/widget/Button;

    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/packageinstaller/UninstallAppProgress;->A:Landroid/widget/Button;

    new-instance v5, Lcom/android/packageinstaller/UninstallAppProgress$b;

    invoke-direct {v5, p0}, Lcom/android/packageinstaller/UninstallAppProgress$b;-><init>(Lcom/android/packageinstaller/UninstallAppProgress;)V

    invoke-virtual {v1, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/android/packageinstaller/UninstallAppProgress;->B:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v1, p0, Lcom/android/packageinstaller/UninstallAppProgress;->B:Landroid/widget/Button;

    new-instance v3, Lcom/android/packageinstaller/UninstallAppProgress$c;

    invoke-direct {v3, p0}, Lcom/android/packageinstaller/UninstallAppProgress$c;-><init>(Lcom/android/packageinstaller/UninstallAppProgress;)V

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f0a0285

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/android/packageinstaller/UninstallAppProgress;->z:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-array v0, v0, [Landroid/view/View;

    iget-object v1, p0, Lcom/android/packageinstaller/UninstallAppProgress;->z:Landroid/widget/Button;

    aput-object v1, v0, v2

    invoke-static {v0}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object v0

    invoke-interface {v0}, Lmiuix/animation/e;->c()Lmiuix/animation/j;

    move-result-object v0

    new-array v1, v2, [Lmiuix/animation/j$b;

    invoke-interface {v0, v4, v1}, Lmiuix/animation/j;->G(F[Lmiuix/animation/j$b;)Lmiuix/animation/j;

    move-result-object v0

    iget-object v1, p0, Lcom/android/packageinstaller/UninstallAppProgress;->z:Landroid/widget/Button;

    new-array v2, v2, [Lc9/a;

    invoke-interface {v0, v1, v2}, Lmiuix/animation/j;->z(Landroid/view/View;[Lc9/a;)V

    return-void
.end method

.method U0(I)V
    .locals 0

    goto/32 :goto_2

    nop

    :goto_0
    return-void

    :goto_1
    invoke-virtual {p0}, Lmiuix/appcompat/app/j;->finish()V

    goto/32 :goto_0

    nop

    :goto_2
    invoke-virtual {p0, p1}, Landroid/app/Activity;->setResult(I)V

    goto/32 :goto_1

    nop
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 3

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/android/packageinstaller/UninstallAppProgress;->C:I

    const/16 v1, -0x64

    if-ne v0, v1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x0

    const-string v2, "android.intent.extra.RETURN_RESULT"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_1

    iget v0, p0, Lcom/android/packageinstaller/UninstallAppProgress;->C:I

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setResult(I)V

    :cond_1
    invoke-super {p0, p1}, Landroidx/core/app/c;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result p1

    return p1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const/4 v1, 0x0

    const v2, 0x7f0a00d8

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_0
    iget-object v2, p0, Lcom/android/packageinstaller/UninstallAppProgress;->z:Landroid/widget/Button;

    if-eq p1, v2, :cond_1

    if-eqz v0, :cond_4

    :cond_1
    if-eqz v0, :cond_2

    const-string p1, "000031"

    invoke-static {p0, p1}, Lq2/d;->e(Landroid/app/Activity;Ljava/lang/String;)V

    :cond_2
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Finished uninstalling pkg: "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/android/packageinstaller/UninstallAppProgress;->v:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "UninstallAppProgress"

    invoke-static {v0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "android.intent.extra.RETURN_RESULT"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result p1

    if-nez p1, :cond_3

    iget p1, p0, Lcom/android/packageinstaller/UninstallAppProgress;->C:I

    invoke-virtual {p0, p1}, Lcom/android/packageinstaller/UninstallAppProgress;->U0(I)V

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Lmiuix/appcompat/app/j;->finish()V

    :cond_4
    :goto_1
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    invoke-super {p0, p1}, Lq2/b;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.android.packageinstaller.applicationInfo"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/content/pm/ApplicationInfo;

    iput-object v1, p0, Lcom/android/packageinstaller/UninstallAppProgress;->v:Landroid/content/pm/ApplicationInfo;

    const-string v1, "android.content.pm.extra.CALLBACK"

    invoke-static {v0, v1}, Lcom/android/packageinstaller/compat/IntentCompat;->getIBinderExtra(Landroid/content/Intent;Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    iput-object v1, p0, Lcom/android/packageinstaller/UninstallAppProgress;->y:Landroid/os/IBinder;

    if-eqz p1, :cond_1

    const/4 p1, -0x1

    iput p1, p0, Lcom/android/packageinstaller/UninstallAppProgress;->C:I

    iget-object p1, p0, Lcom/android/packageinstaller/UninstallAppProgress;->y:Landroid/os/IBinder;

    if-eqz p1, :cond_0

    invoke-static {p1}, Landroid/content/pm/IPackageDeleteObserver2$Stub;->asInterface(Landroid/os/IBinder;)Landroid/content/pm/IPackageDeleteObserver2;

    move-result-object p1

    :try_start_0
    iget-object v0, p0, Lcom/android/packageinstaller/UninstallAppProgress;->v:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget v1, p0, Lcom/android/packageinstaller/UninstallAppProgress;->C:I

    const/4 v2, 0x0

    invoke-interface {p1, v0, v1, v2}, Landroid/content/pm/IPackageDeleteObserver2;->onPackageDeleted(Ljava/lang/String;ILjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    invoke-virtual {p0}, Lmiuix/appcompat/app/j;->finish()V

    goto :goto_0

    :cond_0
    iget p1, p0, Lcom/android/packageinstaller/UninstallAppProgress;->C:I

    invoke-virtual {p0, p1}, Lcom/android/packageinstaller/UninstallAppProgress;->U0(I)V

    :goto_0
    return-void

    :cond_1
    const-string p1, "android.intent.extra.UNINSTALL_ALL_USERS"

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result p1

    iput-boolean p1, p0, Lcom/android/packageinstaller/UninstallAppProgress;->w:Z

    const-string p1, "android.intent.extra.USER"

    invoke-virtual {v0, p1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Landroid/os/UserHandle;

    iput-object p1, p0, Lcom/android/packageinstaller/UninstallAppProgress;->x:Landroid/os/UserHandle;

    if-nez p1, :cond_2

    invoke-static {}, Landroid/os/Process;->myUserHandle()Landroid/os/UserHandle;

    move-result-object p1

    iput-object p1, p0, Lcom/android/packageinstaller/UninstallAppProgress;->x:Landroid/os/UserHandle;

    goto :goto_1

    :cond_2
    const-string p1, "user"

    invoke-virtual {p0, p1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/os/UserManager;

    invoke-virtual {p1}, Landroid/os/UserManager;->getUserProfiles()Ljava/util/List;

    move-result-object p1

    iget-object v0, p0, Lcom/android/packageinstaller/UninstallAppProgress;->x:Landroid/os/UserHandle;

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_4

    :goto_1
    new-instance p1, Lcom/android/packageinstaller/UninstallAppProgress$d;

    invoke-direct {p1, p0}, Lcom/android/packageinstaller/UninstallAppProgress$d;-><init>(Lcom/android/packageinstaller/UninstallAppProgress;)V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/packageinstaller/UninstallAppProgress;->E:J

    :try_start_1
    invoke-virtual {p0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v2, p0, Lcom/android/packageinstaller/UninstallAppProgress;->v:Landroid/content/pm/ApplicationInfo;

    iget-object v2, v2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/android/packageinstaller/UninstallAppProgress;->w:Z

    if-eqz v3, :cond_3

    const/4 v1, 0x2

    :cond_3
    iget-object v3, p0, Lcom/android/packageinstaller/UninstallAppProgress;->x:Landroid/os/UserHandle;

    invoke-static {v3}, Lcom/android/packageinstaller/compat/UserHandleCompat;->getIdentifier(Landroid/os/UserHandle;)I

    move-result v3

    invoke-static {v0, v2, p1, v1, v3}, Lcom/android/packageinstaller/compat/PackageManagerCompat;->deletePackageAsUser(Landroid/content/pm/PackageManager;Ljava/lang/String;Landroid/content/pm/IPackageDeleteObserver;II)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    :catch_1
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "ava.lang.IllegalArgumentException: Unknown package:"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/android/packageinstaller/UninstallAppProgress;->v:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "UninstallAppProgress"

    invoke-static {v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_2
    invoke-virtual {p0}, Lcom/android/packageinstaller/UninstallAppProgress;->S0()V

    return-void

    :cond_4
    new-instance p1, Ljava/lang/SecurityException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "User "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Landroid/os/Process;->myUserHandle()Landroid/os/UserHandle;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, " can\'t request uninstall for user "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/android/packageinstaller/UninstallAppProgress;->x:Landroid/os/UserHandle;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
