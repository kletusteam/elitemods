.class public Lcom/android/packageinstaller/utils/t;
.super Ljava/lang/Object;


# direct methods
.method public static a(Landroid/content/pm/PackageInstaller;)Ljava/lang/String;
    .locals 6

    const-class v2, Ljava/lang/String;

    const/4 v0, 0x0

    new-array v5, v0, [Ljava/lang/Object;

    const-string v0, "SpeedInstallUtil"

    const-string v3, "getSpeedInstallFilePath"

    const/4 v4, 0x0

    move-object v1, p0

    invoke-static/range {v0 .. v5}, Lcom/android/packageinstaller/utils/r;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/String;

    return-object p0
.end method

.method public static b(Landroid/content/Context;Landroid/content/pm/PackageInstaller;)Ljava/io/File;
    .locals 1

    invoke-static {p1}, Lcom/android/packageinstaller/utils/t;->c(Landroid/content/pm/PackageInstaller;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object p0

    return-object p0

    :cond_0
    invoke-static {p1}, Lcom/android/packageinstaller/utils/t;->a(Landroid/content/pm/PackageInstaller;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object p0

    goto :goto_0

    :cond_1
    new-instance p0, Ljava/io/File;

    invoke-direct {p0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    :goto_0
    return-object p0
.end method

.method public static c(Landroid/content/pm/PackageInstaller;)Z
    .locals 7

    sget-object v2, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const/4 v6, 0x0

    new-array v5, v6, [Ljava/lang/Object;

    const-string v0, "SpeedInstallUtil"

    const-string v3, "isSupportedSpeedInstall"

    const/4 v4, 0x0

    move-object v1, p0

    invoke-static/range {v0 .. v5}, Lcom/android/packageinstaller/utils/r;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Boolean;

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 v6, 0x1

    :cond_0
    return v6
.end method

.method public static d(Landroid/content/pm/PackageInstaller$Session;Ljava/lang/String;)Z
    .locals 8

    const/4 v0, 0x1

    new-array v5, v0, [Ljava/lang/Class;

    const-class v1, Ljava/lang/String;

    const/4 v7, 0x0

    aput-object v1, v5, v7

    sget-object v3, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    new-array v6, v0, [Ljava/lang/Object;

    aput-object p1, v6, v7

    const-string v1, "SpeedInstallUtil"

    const-string v4, "submitSpeedInstallFile"

    move-object v2, p0

    invoke-static/range {v1 .. v6}, Lcom/android/packageinstaller/utils/r;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Boolean;

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    move v0, v7

    :goto_0
    return v0
.end method
