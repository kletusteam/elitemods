.class public Lcom/android/packageinstaller/compat/ApplicationInfoCompat;
.super Ljava/lang/Object;


# static fields
.field public static final PRIVATE_FLAG_PRIVILEGED:I = 0x8

.field public static final TAG:Ljava/lang/String; = "ApplicationInfoCompat"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static privateFlags(Landroid/content/pm/ApplicationInfo;)I
    .locals 3

    sget-object v0, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const-string v1, "ApplicationInfoCompat"

    const-string v2, "privateFlags"

    invoke-static {v1, p0, v2, v0}, Lcom/android/packageinstaller/utils/r;->e(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Integer;

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    return p0
.end method
