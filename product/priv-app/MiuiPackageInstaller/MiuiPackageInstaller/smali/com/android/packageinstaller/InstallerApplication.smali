.class public Lcom/android/packageinstaller/InstallerApplication;
.super Landroid/app/Application;

# interfaces
.implements Lm5/b$b;


# static fields
.field public static c:Lcom/android/packageinstaller/InstallerApplication;

.field public static d:Ljava/lang/String;

.field public static e:Ljava/lang/String;


# instance fields
.field private a:Ljava/lang/Runnable;

.field private b:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    sget-object v0, Lj2/d;->a:Lj2/d;

    iput-object v0, p0, Lcom/android/packageinstaller/InstallerApplication;->a:Ljava/lang/Runnable;

    new-instance v0, Lj2/a;

    invoke-direct {v0, p0}, Lj2/a;-><init>(Lcom/android/packageinstaller/InstallerApplication;)V

    iput-object v0, p0, Lcom/android/packageinstaller/InstallerApplication;->b:Ljava/lang/Runnable;

    return-void
.end method

.method public static synthetic c()V
    .locals 0

    invoke-static {}, Lcom/android/packageinstaller/InstallerApplication;->k()V

    return-void
.end method

.method public static synthetic d(Lcom/android/packageinstaller/InstallerApplication;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/packageinstaller/InstallerApplication;->j()V

    return-void
.end method

.method public static synthetic e()V
    .locals 0

    invoke-static {}, Lcom/android/packageinstaller/InstallerApplication;->l()V

    return-void
.end method

.method public static synthetic f()V
    .locals 0

    invoke-static {}, Lcom/android/packageinstaller/InstallerApplication;->i()V

    return-void
.end method

.method public static g()Lcom/android/packageinstaller/InstallerApplication;
    .locals 1

    sget-object v0, Lcom/android/packageinstaller/InstallerApplication;->c:Lcom/android/packageinstaller/InstallerApplication;

    return-object v0
.end method

.method private static synthetic i()V
    .locals 6

    :try_start_0
    const-string v0, "android.view.WindowManagerGlobal"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    const-string v1, "getInstance"

    const/4 v2, 0x0

    new-array v3, v2, [Ljava/lang/Class;

    new-array v4, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v3, v4}, Lcom/android/packageinstaller/utils/q;->d(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    const-string v1, "trimMemory"

    const/4 v3, 0x1

    new-array v4, v3, [Ljava/lang/Class;

    sget-object v5, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v5, v4, v2

    new-array v3, v3, [Ljava/lang/Object;

    const/16 v5, 0x50

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v2

    invoke-static {v0, v1, v4, v3}, Lcom/android/packageinstaller/utils/q;->b(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Ljava/lang/System;->runFinalization()V

    invoke-static {}, Ljava/lang/System;->gc()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    return-void
.end method

.method private synthetic j()V
    .locals 1

    invoke-virtual {p0}, Lcom/android/packageinstaller/InstallerApplication;->h()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->killProcess(I)V

    :cond_0
    return-void
.end method

.method private static synthetic k()V
    .locals 0

    invoke-static {}, Lm5/i;->c()V

    return-void
.end method

.method private static synthetic l()V
    .locals 0

    invoke-static {}, Lt5/r;->e()V

    invoke-static {}, Lt5/f0;->c()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object v0

    iget-object v1, p0, Lcom/android/packageinstaller/InstallerApplication;->a:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lf6/z;->f(Ljava/lang/Runnable;)V

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object v0

    iget-object v1, p0, Lcom/android/packageinstaller/InstallerApplication;->b:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lf6/z;->f(Ljava/lang/Runnable;)V

    return-void
.end method

.method public b()V
    .locals 4

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object v0

    iget-object v1, p0, Lcom/android/packageinstaller/InstallerApplication;->a:Ljava/lang/Runnable;

    const-wide/16 v2, 0x7530

    invoke-virtual {v0, v1, v2, v3}, Lf6/z;->d(Ljava/lang/Runnable;J)V

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object v0

    iget-object v1, p0, Lcom/android/packageinstaller/InstallerApplication;->b:Ljava/lang/Runnable;

    const-wide/32 v2, 0x249f0

    invoke-virtual {v0, v1, v2, v3}, Lf6/z;->d(Ljava/lang/Runnable;J)V

    return-void
.end method

.method public h()Z
    .locals 3

    invoke-static {}, Lm5/b;->m()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    instance-of v2, v1, Lcom/miui/packageInstaller/InstallProgressActivity;

    if-eqz v2, :cond_0

    check-cast v1, Lcom/miui/packageInstaller/InstallProgressActivity;

    invoke-virtual {v1}, Lcom/miui/packageInstaller/a;->q1()Z

    move-result v0

    return v0

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public onCreate()V
    .locals 7

    sput-object p0, Lo6/a;->a:Landroid/content/Context;

    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    sput-object p0, Lcom/android/packageinstaller/InstallerApplication;->c:Lcom/android/packageinstaller/InstallerApplication;

    invoke-static {p0}, Lk5/a;->d(Landroid/content/Context;)V

    invoke-static {p0}, Lm5/b;->o(Landroid/app/Application;)V

    invoke-static {p0}, Lcom/android/packageinstaller/utils/m;->f(Landroid/content/Context;)Lcom/android/packageinstaller/utils/m;

    invoke-static {p0}, Lcom/android/packageinstaller/job/TailStatJobService;->a(Landroid/content/Context;)V

    invoke-static {p0}, Lcom/android/packageinstaller/job/AppSecuritySyncJobService;->b(Landroid/content/Context;)V

    invoke-static {p0}, Lq2/a;->e(Landroid/content/Context;)V

    invoke-static {}, Lm5/g;->c()V

    invoke-static {p0}, Lm5/b;->r(Lm5/b$b;)V

    sget-object v0, Lu5/a;->a:Lu5/a$a;

    sget-wide v4, Lm5/u1;->a:J

    const-string v2, "2882303761517529088"

    const/4 v3, 0x0

    const-string v6, "com.miui.packageinstaller"

    move-object v1, p0

    invoke-virtual/range {v0 .. v6}, Lu5/a$a;->c(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V

    sget-object v0, Lj2/b;->a:Lj2/b;

    invoke-static {v0}, Lu5/k;->b(Ljava/lang/Runnable;)V

    invoke-static {}, Lt5/n;->e()Lt5/n;

    move-result-object v0

    invoke-virtual {v0}, Lt5/n;->h()V

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object v0

    sget-object v1, Lj2/c;->a:Lj2/c;

    const-wide/16 v2, 0x7530

    invoke-virtual {v0, v1, v2, v3}, Lf6/z;->d(Ljava/lang/Runnable;J)V

    return-void
.end method
