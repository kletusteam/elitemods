.class public Lcom/android/packageinstaller/compat/BuildCompat;
.super Ljava/lang/Object;


# static fields
.field public static final PERMISSIONS_REVIEW_REQUIRED:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const-string v0, "ro.permission_review_required"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/packageinstaller/compat/SystemPropertiesCompat;->getInt(Ljava/lang/String;I)I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    move v1, v2

    :cond_0
    sput-boolean v1, Lcom/android/packageinstaller/compat/BuildCompat;->PERMISSIONS_REVIEW_REQUIRED:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
