.class public Lcom/android/packageinstaller/UninstallerActivity;
.super Lq2/b;

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/packageinstaller/UninstallerActivity$b;,
        Lcom/android/packageinstaller/UninstallerActivity$a;
    }
.end annotation


# instance fields
.field private u:Ljava/lang/String;

.field private v:Lcom/android/packageinstaller/UninstallerActivity$b;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lq2/b;-><init>()V

    return-void
.end method

.method private K0()V
    .locals 9

    const v0, 0x7f0d0037

    invoke-virtual {p0, v0}, Lmiuix/appcompat/app/j;->setContentView(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-static {v0}, Lcom/android/packageinstaller/utils/u;->c(Landroid/view/Window;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/packageinstaller/UninstallerActivity;->v:Lcom/android/packageinstaller/UninstallerActivity$b;

    iget-object v2, v1, Lcom/android/packageinstaller/UninstallerActivity$b;->a:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v2, v0}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, v1, Lcom/android/packageinstaller/UninstallerActivity$b;->b:Landroid/content/pm/ActivityInfo;

    const/4 v5, 0x1

    const/4 v6, 0x0

    if-eqz v4, :cond_0

    invoke-virtual {v4, v0}, Landroid/content/pm/ActivityInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    const v7, 0x7f1103c3

    new-array v8, v5, [Ljava/lang/Object;

    aput-object v4, v8, v6

    invoke-virtual {p0, v7, v8}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    const-string v4, ".\n\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    iget-object v4, v1, Lcom/android/packageinstaller/UninstallerActivity$b;->a:Landroid/content/pm/ApplicationInfo;

    iget v4, v4, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit16 v4, v4, 0x80

    if-eqz v4, :cond_1

    move v4, v5

    goto :goto_0

    :cond_1
    move v4, v6

    :goto_0
    invoke-static {p0}, Lcom/android/packageinstaller/compat/UserManagerCompat;->get(Landroid/content/Context;)Landroid/os/UserManager;

    move-result-object v7

    if-eqz v4, :cond_3

    invoke-direct {p0, v7}, Lcom/android/packageinstaller/UninstallerActivity;->L0(Landroid/os/UserManager;)Z

    move-result v4

    if-eqz v4, :cond_2

    const v4, 0x7f1103cf

    goto :goto_1

    :cond_2
    const v4, 0x7f1103d0

    :goto_1
    invoke-virtual {p0, v4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v4

    :goto_2
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    :cond_3
    iget-boolean v4, v1, Lcom/android/packageinstaller/UninstallerActivity$b;->c:Z

    if-eqz v4, :cond_4

    invoke-direct {p0, v7}, Lcom/android/packageinstaller/UninstallerActivity;->L0(Landroid/os/UserManager;)Z

    move-result v4

    if-nez v4, :cond_4

    const v4, 0x7f1103c6

    goto :goto_1

    :cond_4
    iget-object v4, v1, Lcom/android/packageinstaller/UninstallerActivity$b;->d:Landroid/os/UserHandle;

    invoke-static {}, Landroid/os/Process;->myUserHandle()Landroid/os/UserHandle;

    move-result-object v8

    invoke-virtual {v4, v8}, Landroid/os/UserHandle;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_5

    iget-object v4, v1, Lcom/android/packageinstaller/UninstallerActivity$b;->d:Landroid/os/UserHandle;

    invoke-static {v4}, Lcom/android/packageinstaller/compat/UserHandleCompat;->getIdentifier(Landroid/os/UserHandle;)I

    move-result v4

    invoke-static {v7, v4}, Lcom/android/packageinstaller/compat/UserManagerCompat;->getUserInfo(Landroid/os/UserManager;I)Ljava/lang/Object;

    move-result-object v4

    const v7, 0x7f1103c7

    new-array v8, v5, [Ljava/lang/Object;

    invoke-static {v4}, Lcom/android/packageinstaller/compat/UserManagerCompat;->name(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v8, v6

    invoke-virtual {p0, v7, v8}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_2

    :cond_5
    const v4, 0x7f1103c5

    goto :goto_1

    :goto_3
    iget-object v4, v1, Lcom/android/packageinstaller/UninstallerActivity$b;->a:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v4, v0}, Landroid/content/pm/ApplicationInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-static {}, Landroid/os/Process;->myUserHandle()Landroid/os/UserHandle;

    move-result-object v7

    iget-object v8, v1, Lcom/android/packageinstaller/UninstallerActivity$b;->d:Landroid/os/UserHandle;

    invoke-virtual {v7, v8}, Landroid/os/UserHandle;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_6

    iget-object v1, v1, Lcom/android/packageinstaller/UninstallerActivity$b;->d:Landroid/os/UserHandle;

    invoke-virtual {v0, v4, v1}, Landroid/content/pm/PackageManager;->getUserBadgedIcon(Landroid/graphics/drawable/Drawable;Landroid/os/UserHandle;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    :cond_6
    const v0, 0x7f0a007a

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0a0073

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    const v1, 0x7f0a0075

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0a0255

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0a00ef

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0a00b9

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-array v2, v5, [Landroid/view/View;

    aput-object v0, v2, v6

    invoke-static {v2}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object v2

    invoke-interface {v2}, Lmiuix/animation/e;->c()Lmiuix/animation/j;

    move-result-object v2

    new-array v3, v6, [Lmiuix/animation/j$b;

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-interface {v2, v4, v3}, Lmiuix/animation/j;->G(F[Lmiuix/animation/j$b;)Lmiuix/animation/j;

    move-result-object v2

    new-array v3, v6, [Lc9/a;

    invoke-interface {v2, v0, v3}, Lmiuix/animation/j;->z(Landroid/view/View;[Lc9/a;)V

    new-array v0, v5, [Landroid/view/View;

    aput-object v1, v0, v6

    invoke-static {v0}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object v0

    invoke-interface {v0}, Lmiuix/animation/e;->c()Lmiuix/animation/j;

    move-result-object v0

    new-array v2, v6, [Lmiuix/animation/j$b;

    invoke-interface {v0, v4, v2}, Lmiuix/animation/j;->G(F[Lmiuix/animation/j$b;)Lmiuix/animation/j;

    move-result-object v0

    new-array v2, v6, [Lc9/a;

    invoke-interface {v0, v1, v2}, Lmiuix/animation/j;->z(Landroid/view/View;[Lc9/a;)V

    return-void
.end method

.method private L0(Landroid/os/UserManager;)Z
    .locals 2

    invoke-virtual {p1}, Landroid/os/UserManager;->getUserCount()I

    move-result p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_1

    const/4 v1, 0x2

    if-ne p1, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :cond_1
    :goto_0
    return v0
.end method

.method private M0()V
    .locals 1

    new-instance v0, Lcom/android/packageinstaller/UninstallerActivity$a;

    invoke-direct {v0}, Lcom/android/packageinstaller/UninstallerActivity$a;-><init>()V

    invoke-direct {p0, v0}, Lcom/android/packageinstaller/UninstallerActivity;->O0(Landroid/app/DialogFragment;)V

    return-void
.end method

.method private N0()V
    .locals 0

    invoke-direct {p0}, Lcom/android/packageinstaller/UninstallerActivity;->K0()V

    return-void
.end method

.method private O0(Landroid/app/DialogFragment;)V
    .locals 3

    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "dialog"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    :cond_0
    invoke-virtual {p1, v0, v2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentTransaction;Ljava/lang/String;)I

    return-void
.end method

.method private P0(Landroid/app/DialogFragment;II)V
    .locals 4

    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "dialog"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    :cond_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    if-eqz p2, :cond_1

    const-string v3, "com.android.packageinstaller.arg.title"

    invoke-virtual {v1, v3, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_1
    const-string p2, "com.android.packageinstaller.arg.text"

    invoke-virtual {v1, p2, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {p1, v1}, Landroid/app/DialogFragment;->setArguments(Landroid/os/Bundle;)V

    invoke-virtual {p1, v0, v2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentTransaction;Ljava/lang/String;)I

    return-void
.end method

.method private Q0()V
    .locals 3

    new-instance v0, Lo2/a;

    invoke-direct {v0}, Lo2/a;-><init>()V

    const/4 v1, 0x0

    const v2, 0x7f1103de

    invoke-direct {p0, v0, v1, v2}, Lcom/android/packageinstaller/UninstallerActivity;->P0(Landroid/app/DialogFragment;II)V

    return-void
.end method


# virtual methods
.method public J0()V
    .locals 4

    iget-object v0, p0, Lcom/android/packageinstaller/UninstallerActivity;->v:Lcom/android/packageinstaller/UninstallerActivity$b;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/android/packageinstaller/UninstallerActivity$b;->e:Landroid/os/IBinder;

    if-eqz v0, :cond_0

    invoke-static {v0}, Landroid/content/pm/IPackageDeleteObserver2$Stub;->asInterface(Landroid/os/IBinder;)Landroid/content/pm/IPackageDeleteObserver2;

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/android/packageinstaller/UninstallerActivity;->u:Ljava/lang/String;

    const/4 v2, -0x5

    const-string v3, "Cancelled by user"

    invoke-interface {v0, v1, v2, v3}, Landroid/content/pm/IPackageDeleteObserver2;->onPackageDeleted(Ljava/lang/String;ILjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_0
    return-void
.end method

.method R0()V
    .locals 4

    goto/32 :goto_10

    nop

    :goto_0
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    goto/32 :goto_15

    nop

    :goto_1
    iget-object v1, v1, Lcom/android/packageinstaller/UninstallerActivity$b;->a:Landroid/content/pm/ApplicationInfo;

    goto/32 :goto_f

    nop

    :goto_2
    iget-object v1, v1, Lcom/android/packageinstaller/UninstallerActivity$b;->d:Landroid/os/UserHandle;

    goto/32 :goto_16

    nop

    :goto_3
    const/high16 v1, 0x2000000

    goto/32 :goto_1e

    nop

    :goto_4
    const-string v2, "android.intent.extra.RETURN_RESULT"

    goto/32 :goto_20

    nop

    :goto_5
    iget-boolean v1, v1, Lcom/android/packageinstaller/UninstallerActivity$b;->c:Z

    goto/32 :goto_19

    nop

    :goto_6
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto/32 :goto_11

    nop

    :goto_7
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    goto/32 :goto_9

    nop

    :goto_8
    iget-object v1, v1, Lcom/android/packageinstaller/UninstallerActivity$b;->e:Landroid/os/IBinder;

    goto/32 :goto_18

    nop

    :goto_9
    if-nez v1, :cond_0

    goto/32 :goto_1f

    :cond_0
    goto/32 :goto_a

    nop

    :goto_a
    const/4 v1, 0x1

    goto/32 :goto_b

    nop

    :goto_b
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto/32 :goto_3

    nop

    :goto_c
    const-class v1, Lcom/android/packageinstaller/UninstallAppProgress;

    goto/32 :goto_1c

    nop

    :goto_d
    return-void

    :goto_e
    iget-object v1, p0, Lcom/android/packageinstaller/UninstallerActivity;->v:Lcom/android/packageinstaller/UninstallerActivity$b;

    goto/32 :goto_8

    nop

    :goto_f
    const-string v2, "com.android.packageinstaller.applicationInfo"

    goto/32 :goto_1d

    nop

    :goto_10
    new-instance v0, Landroid/content/Intent;

    goto/32 :goto_13

    nop

    :goto_11
    iget-object v1, p0, Lcom/android/packageinstaller/UninstallerActivity;->v:Lcom/android/packageinstaller/UninstallerActivity$b;

    goto/32 :goto_5

    nop

    :goto_12
    iget-object v1, p0, Lcom/android/packageinstaller/UninstallerActivity;->v:Lcom/android/packageinstaller/UninstallerActivity$b;

    goto/32 :goto_1

    nop

    :goto_13
    const-string v1, "android.intent.action.VIEW"

    goto/32 :goto_0

    nop

    :goto_14
    invoke-static {v0, v2, v1}, Lcom/android/packageinstaller/compat/IntentCompat;->putExtra(Landroid/content/Intent;Ljava/lang/String;Landroid/os/IBinder;)V

    goto/32 :goto_12

    nop

    :goto_15
    iget-object v1, p0, Lcom/android/packageinstaller/UninstallerActivity;->v:Lcom/android/packageinstaller/UninstallerActivity$b;

    goto/32 :goto_2

    nop

    :goto_16
    const-string v2, "android.intent.extra.USER"

    goto/32 :goto_6

    nop

    :goto_17
    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto/32 :goto_d

    nop

    :goto_18
    const-string v2, "android.content.pm.extra.CALLBACK"

    goto/32 :goto_14

    nop

    :goto_19
    const-string v2, "android.intent.extra.UNINSTALL_ALL_USERS"

    goto/32 :goto_1b

    nop

    :goto_1a
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    goto/32 :goto_4

    nop

    :goto_1b
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto/32 :goto_e

    nop

    :goto_1c
    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto/32 :goto_17

    nop

    :goto_1d
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto/32 :goto_1a

    nop

    :goto_1e
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    :goto_1f
    goto/32 :goto_c

    nop

    :goto_20
    const/4 v3, 0x0

    goto/32 :goto_7

    nop
.end method

.method public onBackPressed()V
    .locals 0

    invoke-super {p0}, Lmiuix/appcompat/app/j;->onBackPressed()V

    invoke-virtual {p0}, Lcom/android/packageinstaller/UninstallerActivity;->J0()V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result p1

    const v0, 0x7f0a00b9

    if-eq p1, v0, :cond_1

    const v0, 0x7f0a00ef

    if-eq p1, v0, :cond_0

    goto :goto_1

    :cond_0
    invoke-virtual {p0}, Lcom/android/packageinstaller/UninstallerActivity;->R0()V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/android/packageinstaller/UninstallerActivity;->J0()V

    :goto_0
    invoke-virtual {p0}, Lmiuix/appcompat/app/j;->finish()V

    :goto_1
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7

    invoke-super {p0, p1}, Lq2/b;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    const-string v1, "UninstallerActivity"

    if-nez v0, :cond_0

    const-string p1, "No package URI in intent"

    invoke-static {v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/android/packageinstaller/UninstallerActivity;->M0()V

    return-void

    :cond_0
    invoke-virtual {v0}, Landroid/net/Uri;->getEncodedSchemeSpecificPart()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/android/packageinstaller/UninstallerActivity;->u:Ljava/lang/String;

    if-nez v2, :cond_1

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid package name in URI: "

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/android/packageinstaller/UninstallerActivity;->M0()V

    return-void

    :cond_1
    new-instance v2, Lcom/android/packageinstaller/UninstallerActivity$b;

    invoke-direct {v2}, Lcom/android/packageinstaller/UninstallerActivity$b;-><init>()V

    iput-object v2, p0, Lcom/android/packageinstaller/UninstallerActivity;->v:Lcom/android/packageinstaller/UninstallerActivity$b;

    const-string v3, "android.intent.extra.UNINSTALL_ALL_USERS"

    const/4 v4, 0x0

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, v2, Lcom/android/packageinstaller/UninstallerActivity$b;->c:Z

    iget-object v2, p0, Lcom/android/packageinstaller/UninstallerActivity;->v:Lcom/android/packageinstaller/UninstallerActivity$b;

    iget-boolean v2, v2, Lcom/android/packageinstaller/UninstallerActivity$b;->c:Z

    if-eqz v2, :cond_2

    invoke-static {p0}, Lcom/android/packageinstaller/compat/UserManagerCompat;->isAdminUser(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string p1, "Only admin user can request uninstall for all users"

    invoke-static {v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/android/packageinstaller/UninstallerActivity;->Q0()V

    return-void

    :cond_2
    iget-object v2, p0, Lcom/android/packageinstaller/UninstallerActivity;->v:Lcom/android/packageinstaller/UninstallerActivity$b;

    const-string v3, "android.intent.extra.USER"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Landroid/os/UserHandle;

    iput-object v3, v2, Lcom/android/packageinstaller/UninstallerActivity$b;->d:Landroid/os/UserHandle;

    iget-object v2, p0, Lcom/android/packageinstaller/UninstallerActivity;->v:Lcom/android/packageinstaller/UninstallerActivity$b;

    iget-object v3, v2, Lcom/android/packageinstaller/UninstallerActivity$b;->d:Landroid/os/UserHandle;

    if-nez v3, :cond_3

    invoke-static {}, Landroid/os/Process;->myUserHandle()Landroid/os/UserHandle;

    move-result-object v3

    iput-object v3, v2, Lcom/android/packageinstaller/UninstallerActivity$b;->d:Landroid/os/UserHandle;

    :cond_3
    const-string v2, "android.content.pm.extra.PACKAGE_NAME"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "keySet : "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/packageinstaller/UninstallerActivity;->v:Lcom/android/packageinstaller/UninstallerActivity$b;

    const-string v3, "android.content.pm.extra.CALLBACK"

    invoke-static {p1, v3}, Lcom/android/packageinstaller/compat/IntentCompat;->getIBinderExtra(Landroid/content/Intent;Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object p1

    iput-object p1, v2, Lcom/android/packageinstaller/UninstallerActivity$b;->e:Landroid/os/IBinder;

    :try_start_0
    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x21

    if-lt p1, v2, :cond_4

    iget-object p1, p0, Lcom/android/packageinstaller/UninstallerActivity;->v:Lcom/android/packageinstaller/UninstallerActivity$b;

    iget-object v2, p0, Lcom/android/packageinstaller/UninstallerActivity;->u:Ljava/lang/String;

    const-wide/16 v5, 0x2000

    iget-object v3, p1, Lcom/android/packageinstaller/UninstallerActivity$b;->d:Landroid/os/UserHandle;

    invoke-static {v3}, Lcom/android/packageinstaller/compat/UserHandleCompat;->getIdentifier(Landroid/os/UserHandle;)I

    move-result v3

    invoke-static {v2, v5, v6, v3}, Lcom/android/packageinstaller/compat/IPackageManagerCompat;->getApplicationInfoAndroidT(Ljava/lang/String;JI)Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    :goto_0
    iput-object v2, p1, Lcom/android/packageinstaller/UninstallerActivity$b;->a:Landroid/content/pm/ApplicationInfo;

    goto :goto_1

    :cond_4
    iget-object p1, p0, Lcom/android/packageinstaller/UninstallerActivity;->v:Lcom/android/packageinstaller/UninstallerActivity$b;

    iget-object v2, p0, Lcom/android/packageinstaller/UninstallerActivity;->u:Ljava/lang/String;

    const/16 v3, 0x2000

    iget-object v5, p1, Lcom/android/packageinstaller/UninstallerActivity$b;->d:Landroid/os/UserHandle;

    invoke-static {v5}, Lcom/android/packageinstaller/compat/UserHandleCompat;->getIdentifier(Landroid/os/UserHandle;)I

    move-result v5

    invoke-static {v2, v3, v5}, Lcom/android/packageinstaller/compat/IPackageManagerCompat;->getApplicationInfo(Ljava/lang/String;II)Landroid/content/pm/ApplicationInfo;

    move-result-object v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const-string p1, "Unable to get packageName. Package manager is dead?"

    invoke-static {v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    iget-object p1, p0, Lcom/android/packageinstaller/UninstallerActivity;->v:Lcom/android/packageinstaller/UninstallerActivity$b;

    iget-object p1, p1, Lcom/android/packageinstaller/UninstallerActivity$b;->a:Landroid/content/pm/ApplicationInfo;

    if-nez p1, :cond_5

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Invalid packageName: "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/android/packageinstaller/UninstallerActivity;->u:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/android/packageinstaller/UninstallerActivity;->M0()V

    return-void

    :cond_5
    iget-object p1, p0, Lcom/android/packageinstaller/UninstallerActivity;->u:Ljava/lang/String;

    invoke-static {p0, p1, v4}, Lcom/android/packageinstaller/compat/PreloadedAppPolicyCompat;->isProtectedDataApp(Landroid/content/Context;Ljava/lang/String;I)Z

    move-result p1

    if-eqz p1, :cond_6

    const p1, 0x7f1101df

    invoke-static {p0, p1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0}, Lcom/android/packageinstaller/UninstallerActivity;->J0()V

    invoke-virtual {p0}, Lmiuix/appcompat/app/j;->finish()V

    return-void

    :cond_6
    invoke-virtual {v0}, Landroid/net/Uri;->getFragment()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_7

    :try_start_1
    iget-object v0, p0, Lcom/android/packageinstaller/UninstallerActivity;->v:Lcom/android/packageinstaller/UninstallerActivity$b;

    new-instance v2, Landroid/content/ComponentName;

    iget-object v3, p0, Lcom/android/packageinstaller/UninstallerActivity;->u:Ljava/lang/String;

    invoke-direct {v2, v3, p1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/android/packageinstaller/UninstallerActivity;->v:Lcom/android/packageinstaller/UninstallerActivity$b;

    iget-object p1, p1, Lcom/android/packageinstaller/UninstallerActivity$b;->d:Landroid/os/UserHandle;

    invoke-static {p1}, Lcom/android/packageinstaller/compat/UserHandleCompat;->getIdentifier(Landroid/os/UserHandle;)I

    move-result p1

    invoke-static {v2, v4, p1}, Lcom/android/packageinstaller/compat/IPackageManagerCompat;->getActivityInfo(Landroid/content/ComponentName;II)Landroid/content/pm/ActivityInfo;

    move-result-object p1

    iput-object p1, v0, Lcom/android/packageinstaller/UninstallerActivity$b;->b:Landroid/content/pm/ActivityInfo;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    :catch_1
    const-string p1, "Unable to get className. Package manager is dead?"

    invoke-static {v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_7
    :goto_2
    invoke-direct {p0}, Lcom/android/packageinstaller/UninstallerActivity;->N0()V

    return-void
.end method
