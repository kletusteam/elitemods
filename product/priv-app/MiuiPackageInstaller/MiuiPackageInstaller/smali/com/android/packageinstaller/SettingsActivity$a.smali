.class public Lcom/android/packageinstaller/SettingsActivity$a;
.super Lva/i;

# interfaces
.implements Landroidx/preference/Preference$d;
.implements Landroidx/preference/Preference$e;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/packageinstaller/SettingsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/packageinstaller/SettingsActivity$a$a;
    }
.end annotation


# instance fields
.field private final A0:Ljava/lang/String;

.field private final B0:Ljava/lang/String;

.field private final C0:Ljava/lang/String;

.field private final D0:Ljava/lang/String;

.field private final E0:Ljava/lang/String;

.field private final F0:Ljava/lang/String;

.field private final G0:Ljava/lang/String;

.field private I0:Landroidx/preference/CheckBoxPreference;

.field private J0:Lm2/b;

.field private K0:Landroidx/preference/Preference;

.field private L0:Landroidx/preference/Preference;

.field private M0:Landroid/content/Context;

.field private N0:Lmiuix/preference/TextPreference;

.field private O0:Z

.field private P0:Lcom/android/packageinstaller/SettingsActivity$a$a;

.field private final z0:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lva/i;-><init>()V

    const-string v0, "pref_key_open_antivirus"

    iput-object v0, p0, Lcom/android/packageinstaller/SettingsActivity$a;->z0:Ljava/lang/String;

    const-string v0, "pref_key_open_ads"

    iput-object v0, p0, Lcom/android/packageinstaller/SettingsActivity$a;->A0:Ljava/lang/String;

    const-string v0, "pref_key_open_app_store_recommend"

    iput-object v0, p0, Lcom/android/packageinstaller/SettingsActivity$a;->B0:Ljava/lang/String;

    const-string v0, "pref_key_open_app_store_recommend_description"

    iput-object v0, p0, Lcom/android/packageinstaller/SettingsActivity$a;->C0:Ljava/lang/String;

    const-string v0, "pref_key_about_privacy"

    iput-object v0, p0, Lcom/android/packageinstaller/SettingsActivity$a;->D0:Ljava/lang/String;

    const-string v0, "pref_risk_app"

    iput-object v0, p0, Lcom/android/packageinstaller/SettingsActivity$a;->E0:Ljava/lang/String;

    const-string v0, "pref_key_delete_package"

    iput-object v0, p0, Lcom/android/packageinstaller/SettingsActivity$a;->F0:Ljava/lang/String;

    const-string v0, "pref_key_safe_install_mode"

    iput-object v0, p0, Lcom/android/packageinstaller/SettingsActivity$a;->G0:Ljava/lang/String;

    return-void
.end method

.method static synthetic j2(Lcom/android/packageinstaller/SettingsActivity$a;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/android/packageinstaller/SettingsActivity$a;->O0:Z

    return p1
.end method

.method static synthetic k2(Lcom/android/packageinstaller/SettingsActivity$a;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/packageinstaller/SettingsActivity$a;->o2()V

    return-void
.end method

.method static synthetic l2(Lcom/android/packageinstaller/SettingsActivity$a;)V
    .locals 0

    invoke-direct {p0}, Lcom/android/packageinstaller/SettingsActivity$a;->m2()V

    return-void
.end method

.method private m2()V
    .locals 2

    iget-object v0, p0, Lcom/android/packageinstaller/SettingsActivity$a;->M0:Landroid/content/Context;

    instance-of v1, v0, Lcom/android/packageinstaller/SettingsActivity;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/android/packageinstaller/SettingsActivity;

    invoke-virtual {v0}, Lmiuix/appcompat/app/j;->finish()V

    :cond_0
    return-void
.end method

.method public static n2()Lcom/android/packageinstaller/SettingsActivity$a;
    .locals 1

    new-instance v0, Lcom/android/packageinstaller/SettingsActivity$a;

    invoke-direct {v0}, Lcom/android/packageinstaller/SettingsActivity$a;-><init>()V

    return-object v0
.end method

.method private o2()V
    .locals 2

    iget-object v0, p0, Lcom/android/packageinstaller/SettingsActivity$a;->N0:Lmiuix/preference/TextPreference;

    iget-boolean v1, p0, Lcom/android/packageinstaller/SettingsActivity$a;->O0:Z

    if-eqz v1, :cond_0

    const v1, 0x7f1102e9

    goto :goto_0

    :cond_0
    const v1, 0x7f1102ea

    :goto_0
    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->T(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiuix/preference/TextPreference;->K0(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public J0()V
    .locals 4

    invoke-super {p0}, Landroidx/fragment/app/Fragment;->J0()V

    new-instance v0, Lp5/g;

    iget-object v1, p0, Lcom/android/packageinstaller/SettingsActivity$a;->M0:Landroid/content/Context;

    check-cast v1, Lo5/a;

    const-string v2, "installing_settings_privacy_btn"

    const-string v3, "button"

    invoke-direct {v0, v2, v3, v1}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    new-instance v0, Lp5/g;

    iget-object v1, p0, Lcom/android/packageinstaller/SettingsActivity$a;->M0:Landroid/content/Context;

    check-cast v1, Lo5/a;

    const-string v2, "installing_settings_safe_mode_btn"

    invoke-direct {v0, v2, v3, v1}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    new-instance v0, Lp5/g;

    iget-object v1, p0, Lcom/android/packageinstaller/SettingsActivity$a;->M0:Landroid/content/Context;

    check-cast v1, Lo5/a;

    const-string v2, "installing_settings_rec_switch"

    const-string v3, "switch"

    invoke-direct {v0, v2, v3, v1}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    iget-object v1, p0, Lcom/android/packageinstaller/SettingsActivity$a;->J0:Lm2/b;

    invoke-virtual {v1}, Lm2/b;->q()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "on"

    goto :goto_0

    :cond_0
    const-string v1, "off"

    :goto_0
    const-string v2, "switch_action"

    invoke-virtual {v0, v2, v1}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object v0

    invoke-virtual {v0}, Lp5/f;->c()Z

    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/packageinstaller/SettingsActivity$a;->N0:Lmiuix/preference/TextPreference;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/packageinstaller/SettingsActivity$a;->M0:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->isSafeModelEnable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/packageinstaller/SettingsActivity$a;->N0:Lmiuix/preference/TextPreference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->B0(Z)V

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/android/packageinstaller/SettingsActivity$a;->N0:Lmiuix/preference/TextPreference;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroidx/preference/Preference;->B0(Z)V

    iget-boolean v0, p0, Lcom/android/packageinstaller/SettingsActivity$a;->O0:Z

    iget-object v1, p0, Lcom/android/packageinstaller/SettingsActivity$a;->M0:Landroid/content/Context;

    invoke-static {v1}, Lm2/b;->g(Landroid/content/Context;)Lm2/b;

    move-result-object v1

    invoke-virtual {v1}, Lm2/b;->h()Z

    move-result v1

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/android/packageinstaller/SettingsActivity$a;->M0:Landroid/content/Context;

    invoke-static {v0}, Lm2/b;->g(Landroid/content/Context;)Lm2/b;

    move-result-object v0

    invoke-virtual {v0}, Lm2/b;->h()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/packageinstaller/SettingsActivity$a;->O0:Z

    invoke-direct {p0}, Lcom/android/packageinstaller/SettingsActivity$a;->o2()V

    :cond_2
    :goto_1
    return-void
.end method

.method public Q1(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 5

    const p1, 0x7f140006

    invoke-virtual {p0, p1, p2}, Landroidx/preference/g;->Y1(ILjava/lang/String;)V

    iget-object p1, p0, Lcom/android/packageinstaller/SettingsActivity$a;->M0:Landroid/content/Context;

    invoke-static {p1}, Lm2/b;->g(Landroid/content/Context;)Lm2/b;

    move-result-object p1

    iput-object p1, p0, Lcom/android/packageinstaller/SettingsActivity$a;->J0:Lm2/b;

    const-string p1, "pref_key_about_privacy"

    invoke-virtual {p0, p1}, Landroidx/preference/g;->g(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    iput-object p1, p0, Lcom/android/packageinstaller/SettingsActivity$a;->K0:Landroidx/preference/Preference;

    iget-object p1, p0, Lcom/android/packageinstaller/SettingsActivity$a;->K0:Landroidx/preference/Preference;

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->v0(Landroidx/preference/Preference$e;)V

    const-string p1, "pref_key_safe_install_mode"

    invoke-virtual {p0, p1}, Landroidx/preference/g;->g(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    iput-object p1, p0, Lcom/android/packageinstaller/SettingsActivity$a;->L0:Landroidx/preference/Preference;

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->v0(Landroidx/preference/Preference$e;)V

    iget-object p1, p0, Lcom/android/packageinstaller/SettingsActivity$a;->M0:Landroid/content/Context;

    invoke-static {p1}, Lm2/b;->g(Landroid/content/Context;)Lm2/b;

    move-result-object p1

    invoke-virtual {p1}, Lm2/b;->t()Z

    move-result p1

    iget-object p2, p0, Lcom/android/packageinstaller/SettingsActivity$a;->M0:Landroid/content/Context;

    invoke-static {p2}, Lm2/b;->g(Landroid/content/Context;)Lm2/b;

    move-result-object p2

    invoke-virtual {p2}, Lm2/b;->a()Z

    move-result p2

    iget-object v0, p0, Lcom/android/packageinstaller/SettingsActivity$a;->M0:Landroid/content/Context;

    invoke-static {v0}, Lm2/b;->g(Landroid/content/Context;)Lm2/b;

    move-result-object v0

    invoke-virtual {v0}, Lm2/b;->R()Z

    move-result v0

    invoke-static {}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->getSecureSettingLocation()Ljava/lang/String;

    move-result-object v1

    const-string v2, "settings"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    sget-boolean v4, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-nez v4, :cond_0

    if-eqz v1, :cond_1

    if-nez p1, :cond_1

    if-nez p2, :cond_1

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/android/packageinstaller/SettingsActivity$a;->L0:Landroidx/preference/Preference;

    invoke-virtual {p1, v2}, Landroidx/preference/Preference;->B0(Z)V

    goto :goto_1

    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/android/packageinstaller/SettingsActivity$a;->L0:Landroidx/preference/Preference;

    invoke-virtual {p1, v3}, Landroidx/preference/Preference;->B0(Z)V

    :goto_1
    const-string p1, "pref_key_delete_package"

    invoke-virtual {p0, p1}, Landroidx/preference/g;->g(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Landroidx/preference/CheckBoxPreference;

    iput-object p1, p0, Lcom/android/packageinstaller/SettingsActivity$a;->I0:Landroidx/preference/CheckBoxPreference;

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->u0(Landroidx/preference/Preference$d;)V

    iget-object p1, p0, Lcom/android/packageinstaller/SettingsActivity$a;->I0:Landroidx/preference/CheckBoxPreference;

    iget-object p2, p0, Lcom/android/packageinstaller/SettingsActivity$a;->J0:Lm2/b;

    invoke-virtual {p2}, Lm2/b;->m()Z

    move-result p2

    invoke-virtual {p1, p2}, Landroidx/preference/TwoStatePreference;->setChecked(Z)V

    const-string p1, "pref_risk_app"

    invoke-virtual {p0, p1}, Landroidx/preference/g;->g(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Lmiuix/preference/TextPreference;

    iput-object p1, p0, Lcom/android/packageinstaller/SettingsActivity$a;->N0:Lmiuix/preference/TextPreference;

    iget-object p1, p0, Lcom/android/packageinstaller/SettingsActivity$a;->M0:Landroid/content/Context;

    invoke-static {p1}, Lm2/b;->g(Landroid/content/Context;)Lm2/b;

    move-result-object p1

    invoke-virtual {p1}, Lm2/b;->h()Z

    move-result p1

    iput-boolean p1, p0, Lcom/android/packageinstaller/SettingsActivity$a;->O0:Z

    invoke-direct {p0}, Lcom/android/packageinstaller/SettingsActivity$a;->o2()V

    iget-object p1, p0, Lcom/android/packageinstaller/SettingsActivity$a;->N0:Lmiuix/preference/TextPreference;

    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->v0(Landroidx/preference/Preference$e;)V

    sget-boolean p1, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-nez p1, :cond_2

    iget-object p1, p0, Lcom/android/packageinstaller/SettingsActivity$a;->M0:Landroid/content/Context;

    invoke-static {p1}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->isSafeModelEnable(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_3

    :cond_2
    iget-object p1, p0, Lcom/android/packageinstaller/SettingsActivity$a;->N0:Lmiuix/preference/TextPreference;

    invoke-virtual {p1, v2}, Landroidx/preference/Preference;->B0(Z)V

    goto :goto_2

    :cond_3
    iget-object p1, p0, Lcom/android/packageinstaller/SettingsActivity$a;->N0:Lmiuix/preference/TextPreference;

    invoke-virtual {p1, v3}, Landroidx/preference/Preference;->B0(Z)V

    :goto_2
    new-instance p1, Lcom/android/packageinstaller/SettingsActivity$a$a;

    invoke-direct {p1, p0}, Lcom/android/packageinstaller/SettingsActivity$a$a;-><init>(Lcom/android/packageinstaller/SettingsActivity$a;)V

    iput-object p1, p0, Lcom/android/packageinstaller/SettingsActivity$a;->P0:Lcom/android/packageinstaller/SettingsActivity$a$a;

    new-instance p1, Landroid/content/IntentFilter;

    const-string p2, "risk_authorization_broadcast.Action"

    invoke-direct {p1, p2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const-string p2, "com.action.once_Authorize"

    invoke-virtual {p1, p2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object p2, p0, Lcom/android/packageinstaller/SettingsActivity$a;->M0:Landroid/content/Context;

    iget-object v0, p0, Lcom/android/packageinstaller/SettingsActivity$a;->P0:Lcom/android/packageinstaller/SettingsActivity$a$a;

    invoke-virtual {p2, v0, p1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method public c(Landroidx/preference/Preference;Ljava/lang/Object;)Z
    .locals 3

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    iget-object v0, p0, Lcom/android/packageinstaller/SettingsActivity$a;->I0:Landroidx/preference/CheckBoxPreference;

    if-ne p1, v0, :cond_0

    iget-object p1, p0, Lcom/android/packageinstaller/SettingsActivity$a;->J0:Lm2/b;

    invoke-virtual {p1, p2}, Lm2/b;->L(Z)V

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public f(Landroidx/preference/Preference;)Z
    .locals 3

    iget-object v0, p0, Lcom/android/packageinstaller/SettingsActivity$a;->K0:Landroidx/preference/Preference;

    const-string v1, "button"

    if-ne p1, v0, :cond_0

    new-instance p1, Lp5/b;

    iget-object v0, p0, Lcom/android/packageinstaller/SettingsActivity$a;->M0:Landroid/content/Context;

    check-cast v0, Lo5/a;

    const-string v2, "installing_settings_privacy_btn"

    invoke-direct {p1, v2, v1, v0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p1}, Lp5/f;->c()Z

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {}, Lmiui/os/Build;->getRegion()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "http://www.miui.com/res/doc/privacy.html?region="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "&lang="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    iget-object p1, p0, Lcom/android/packageinstaller/SettingsActivity$a;->M0:Landroid/content/Context;

    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_3

    :cond_0
    iget-object v0, p0, Lcom/android/packageinstaller/SettingsActivity$a;->N0:Lmiuix/preference/TextPreference;

    if-ne p1, v0, :cond_1

    new-instance p1, Landroid/content/Intent;

    invoke-direct {p1}, Landroid/content/Intent;-><init>()V

    iget-object v0, p0, Lcom/android/packageinstaller/SettingsActivity$a;->M0:Landroid/content/Context;

    const-class v1, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    iget-boolean v0, p0, Lcom/android/packageinstaller/SettingsActivity$a;->O0:Z

    const-string v1, "risk_authorization"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :goto_0
    iget-object v0, p0, Lcom/android/packageinstaller/SettingsActivity$a;->M0:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_3

    :cond_1
    iget-object v0, p0, Lcom/android/packageinstaller/SettingsActivity$a;->L0:Landroidx/preference/Preference;

    if-ne p1, v0, :cond_5

    new-instance p1, Lp5/b;

    iget-object v0, p0, Lcom/android/packageinstaller/SettingsActivity$a;->M0:Landroid/content/Context;

    check-cast v0, Lo5/a;

    const-string v2, "installing_settings_safe_mode_btn"

    invoke-direct {p1, v2, v1, v0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p1}, Lp5/f;->c()Z

    new-instance p1, Landroid/content/Intent;

    invoke-direct {p1}, Landroid/content/Intent;-><init>()V

    iget-object v0, p0, Lcom/android/packageinstaller/SettingsActivity$a;->M0:Landroid/content/Context;

    const-class v1, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/android/packageinstaller/SettingsActivity$a;->M0:Landroid/content/Context;

    instance-of v1, v0, Lcom/android/packageinstaller/SettingsActivity;

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    check-cast v0, Lcom/android/packageinstaller/SettingsActivity;

    invoke-static {v0}, Lcom/android/packageinstaller/SettingsActivity;->J0(Lcom/android/packageinstaller/SettingsActivity;)Lo5/b;

    move-result-object v0

    goto :goto_1

    :cond_2
    new-instance v0, Lo5/b;

    const-string v1, "settingActivity"

    invoke-direct {v0, v1, v2}, Lo5/b;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    const-string v1, "fromPage"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/android/packageinstaller/SettingsActivity$a;->M0:Landroid/content/Context;

    instance-of v1, v0, Lcom/android/packageinstaller/SettingsActivity;

    if-eqz v1, :cond_3

    check-cast v0, Lcom/android/packageinstaller/SettingsActivity;

    invoke-static {v0}, Lcom/android/packageinstaller/SettingsActivity;->K0(Lcom/android/packageinstaller/SettingsActivity;)Lm5/e;

    move-result-object v0

    goto :goto_2

    :cond_3
    move-object v0, v2

    :goto_2
    const-string v1, "caller"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/android/packageinstaller/SettingsActivity$a;->M0:Landroid/content/Context;

    instance-of v1, v0, Lcom/android/packageinstaller/SettingsActivity;

    if-eqz v1, :cond_4

    check-cast v0, Lcom/android/packageinstaller/SettingsActivity;

    invoke-static {v0}, Lcom/android/packageinstaller/SettingsActivity;->L0(Lcom/android/packageinstaller/SettingsActivity;)Lcom/miui/packageInstaller/model/ApkInfo;

    move-result-object v2

    :cond_4
    const-string v0, "apk_info"

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v0, "safe_mode_type"

    const-string v1, "packageinstaller"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "safe_mode_ref"

    const-string v1, "package_install_setting_from"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    :cond_5
    :goto_3
    const/4 p1, 0x1

    return p1
.end method

.method public l0(Landroid/content/Context;)V
    .locals 0

    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->l0(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/android/packageinstaller/SettingsActivity$a;->M0:Landroid/content/Context;

    return-void
.end method

.method public t0()V
    .locals 2

    invoke-super {p0}, Landroidx/fragment/app/Fragment;->t0()V

    iget-object v0, p0, Lcom/android/packageinstaller/SettingsActivity$a;->M0:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/packageinstaller/SettingsActivity$a;->P0:Lcom/android/packageinstaller/SettingsActivity$a$a;

    if-eqz v1, :cond_0

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_0
    return-void
.end method
