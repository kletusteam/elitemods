.class public Lcom/android/packageinstaller/compat/PreloadedAppPolicyCompat;
.super Ljava/lang/Object;


# static fields
.field public static final TAG:Ljava/lang/String; = "PreloadedAppPolicyCompat"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static isProtectedDataApp(Landroid/content/Context;Ljava/lang/String;I)Z
    .locals 9

    const/4 v0, 0x0

    :try_start_0
    const-string v1, "miui.content.pm.PreloadedAppPolicy"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    const/4 v1, 0x3

    new-array v6, v1, [Ljava/lang/Class;

    const-class v2, Landroid/content/Context;

    aput-object v2, v6, v0

    const-class v2, Ljava/lang/String;

    const/4 v4, 0x1

    aput-object v2, v6, v4

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v5, 0x2

    aput-object v2, v6, v5

    const-string v2, "PreloadedAppPolicyCompat"

    sget-object v7, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const-string v8, "isProtectedDataApp"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p0, v1, v0

    aput-object p1, v1, v4

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    aput-object p0, v1, v5

    move-object v4, v7

    move-object v5, v8

    move-object v7, v1

    invoke-static/range {v2 .. v7}, Lcom/android/packageinstaller/utils/r;->c(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    return p0

    :catch_0
    move-exception p0

    invoke-virtual {p0}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    return v0
.end method
