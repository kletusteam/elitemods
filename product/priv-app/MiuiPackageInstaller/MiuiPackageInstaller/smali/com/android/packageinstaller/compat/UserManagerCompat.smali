.class public Lcom/android/packageinstaller/compat/UserManagerCompat;
.super Ljava/lang/Object;


# static fields
.field public static final RESTRICTION_SOURCE_SYSTEM:I = 0x1

.field public static final TAG:Ljava/lang/String; = "UserManagerCompat"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static get(Landroid/content/Context;)Landroid/os/UserManager;
    .locals 7

    :try_start_0
    const-string v0, "android.os.UserManager"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    const/4 v0, 0x1

    new-array v5, v0, [Ljava/lang/Class;

    const-class v1, Landroid/content/Context;

    const/4 v3, 0x0

    aput-object v1, v5, v3

    const-string v1, "UserManagerCompat"

    const-class v4, Landroid/os/UserManager;

    const-string v6, "get"

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p0, v0, v3

    move-object v3, v4

    move-object v4, v6

    move-object v6, v0

    invoke-static/range {v1 .. v6}, Lcom/android/packageinstaller/utils/r;->c(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/os/UserManager;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    invoke-virtual {p0}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    const/4 p0, 0x0

    return-object p0
.end method

.method public static getProfileParent(Landroid/os/UserManager;I)Ljava/lang/Object;
    .locals 7

    const/4 v0, 0x1

    new-array v5, v0, [Ljava/lang/Class;

    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v2, 0x0

    aput-object v1, v5, v2

    const-class v3, Ljava/lang/Object;

    new-array v6, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v6, v2

    const-string v1, "UserManagerCompat"

    const-string v4, "getProfileParent"

    move-object v2, p0

    invoke-static/range {v1 .. v6}, Lcom/android/packageinstaller/utils/r;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method public static getUserInfo(Landroid/os/UserManager;I)Ljava/lang/Object;
    .locals 7

    const/4 v0, 0x1

    new-array v5, v0, [Ljava/lang/Class;

    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v2, 0x0

    aput-object v1, v5, v2

    const-class v3, Ljava/lang/Object;

    new-array v6, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v6, v2

    const-string v1, "UserManagerCompat"

    const-string v4, "getUserInfo"

    move-object v2, p0

    invoke-static/range {v1 .. v6}, Lcom/android/packageinstaller/utils/r;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method public static getUsers(Landroid/os/UserManager;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/UserManager;",
            ")",
            "Ljava/util/List<",
            "Landroid/content/pm/UserInfo;",
            ">;"
        }
    .end annotation

    const-class v2, Ljava/util/List;

    const/4 v0, 0x0

    new-array v4, v0, [Ljava/lang/Class;

    new-array v5, v0, [Ljava/lang/Object;

    const-string v0, "UserManagerCompat"

    const-string v3, "getUsers"

    move-object v1, p0

    invoke-static/range {v0 .. v5}, Lcom/android/packageinstaller/utils/r;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/util/List;

    return-object p0
.end method

.method public static id(Ljava/lang/Object;)I
    .locals 3

    sget-object v0, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const-string v1, "UserManagerCompat"

    const-string v2, "id"

    invoke-static {v1, p0, v2, v0}, Lcom/android/packageinstaller/utils/r;->e(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Integer;

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    return p0
.end method

.method public static isAdminUser(Landroid/content/Context;)Z
    .locals 6

    invoke-static {p0}, Lcom/android/packageinstaller/compat/UserManagerCompat;->get(Landroid/content/Context;)Landroid/os/UserManager;

    move-result-object v1

    sget-object v2, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const/4 p0, 0x0

    new-array v4, p0, [Ljava/lang/Class;

    new-array v5, p0, [Ljava/lang/Object;

    const-string v0, "UserManagerCompat"

    const-string v3, "isAdminUser"

    invoke-static/range {v0 .. v5}, Lcom/android/packageinstaller/utils/r;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method public static isManagedProfile(Landroid/os/UserManager;)Z
    .locals 6

    sget-object v2, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const/4 v0, 0x0

    new-array v4, v0, [Ljava/lang/Class;

    new-array v5, v0, [Ljava/lang/Object;

    const-string v0, "UserManagerCompat"

    const-string v3, "isManagedProfile"

    move-object v1, p0

    invoke-static/range {v0 .. v5}, Lcom/android/packageinstaller/utils/r;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method public static name(Ljava/lang/Object;)Ljava/lang/String;
    .locals 3

    const-class v0, Ljava/lang/String;

    const-string v1, "UserManagerCompat"

    const-string v2, "name"

    invoke-static {v1, p0, v2, v0}, Lcom/android/packageinstaller/utils/r;->e(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/String;

    return-object p0
.end method
