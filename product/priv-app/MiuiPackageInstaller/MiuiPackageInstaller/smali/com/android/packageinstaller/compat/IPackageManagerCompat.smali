.class public Lcom/android/packageinstaller/compat/IPackageManagerCompat;
.super Ljava/lang/Object;


# static fields
.field public static final TAG:Ljava/lang/String; = "IPackageManagerCompat"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static checkUidPermission(Ljava/lang/String;I)I
    .locals 7

    invoke-static {}, Lcom/android/packageinstaller/compat/IPackageManagerCompat;->getIPackageManager()Ljava/lang/Object;

    move-result-object v1

    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/Class;

    const-class v2, Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v4, v3

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v5, 0x1

    aput-object v2, v4, v5

    if-eqz v1, :cond_0

    new-array v6, v0, [Ljava/lang/Object;

    aput-object p0, v6, v3

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    aput-object p0, v6, v5

    const-string v0, "IPackageManagerCompat"

    const-string v3, "checkUidPermission"

    move-object v5, v6

    invoke-static/range {v0 .. v5}, Lcom/android/packageinstaller/utils/r;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Integer;

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    return p0

    :cond_0
    return v3
.end method

.method public static getActivityInfo(Landroid/content/ComponentName;II)Landroid/content/pm/ActivityInfo;
    .locals 8

    invoke-static {}, Lcom/android/packageinstaller/compat/IPackageManagerCompat;->getIPackageManager()Ljava/lang/Object;

    move-result-object v1

    const/4 v0, 0x3

    new-array v4, v0, [Ljava/lang/Class;

    const-class v2, Landroid/content/ComponentName;

    const/4 v3, 0x0

    aput-object v2, v4, v3

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v5, 0x1

    aput-object v2, v4, v5

    const/4 v6, 0x2

    aput-object v2, v4, v6

    if-eqz v1, :cond_0

    const-class v2, Landroid/content/pm/ActivityInfo;

    new-array v7, v0, [Ljava/lang/Object;

    aput-object p0, v7, v3

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    aput-object p0, v7, v5

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    aput-object p0, v7, v6

    const-string v0, "IPackageManagerCompat"

    const-string v3, "getActivityInfo"

    move-object v5, v7

    invoke-static/range {v0 .. v5}, Lcom/android/packageinstaller/utils/r;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/content/pm/ActivityInfo;

    return-object p0

    :cond_0
    const/4 p0, 0x0

    return-object p0
.end method

.method public static getAppOpPermissionPackages(Ljava/lang/String;)[Ljava/lang/String;
    .locals 6

    invoke-static {}, Lcom/android/packageinstaller/compat/IPackageManagerCompat;->getIPackageManager()Ljava/lang/Object;

    move-result-object v1

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/Class;

    const-class v2, Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v4, v3

    if-eqz v1, :cond_0

    const-class v2, [Ljava/lang/String;

    new-array v5, v0, [Ljava/lang/Object;

    aput-object p0, v5, v3

    const-string v0, "IPackageManagerCompat"

    const-string v3, "getAppOpPermissionPackages"

    invoke-static/range {v0 .. v5}, Lcom/android/packageinstaller/utils/r;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, [Ljava/lang/String;

    return-object p0

    :cond_0
    const/4 p0, 0x0

    return-object p0
.end method

.method public static getApplicationInfo(Ljava/lang/String;II)Landroid/content/pm/ApplicationInfo;
    .locals 8

    invoke-static {}, Lcom/android/packageinstaller/compat/IPackageManagerCompat;->getIPackageManager()Ljava/lang/Object;

    move-result-object v1

    const/4 v0, 0x3

    new-array v4, v0, [Ljava/lang/Class;

    const-class v2, Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v4, v3

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v5, 0x1

    aput-object v2, v4, v5

    const/4 v6, 0x2

    aput-object v2, v4, v6

    if-eqz v1, :cond_0

    const-class v2, Landroid/content/pm/ApplicationInfo;

    new-array v7, v0, [Ljava/lang/Object;

    aput-object p0, v7, v3

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    aput-object p0, v7, v5

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    aput-object p0, v7, v6

    const-string v0, "IPackageManagerCompat"

    const-string v3, "getApplicationInfo"

    move-object v5, v7

    invoke-static/range {v0 .. v5}, Lcom/android/packageinstaller/utils/r;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/content/pm/ApplicationInfo;

    return-object p0

    :cond_0
    const/4 p0, 0x0

    return-object p0
.end method

.method public static getApplicationInfoAndroidT(Ljava/lang/String;JI)Landroid/content/pm/ApplicationInfo;
    .locals 8

    invoke-static {}, Lcom/android/packageinstaller/compat/IPackageManagerCompat;->getIPackageManager()Ljava/lang/Object;

    move-result-object v1

    const/4 v0, 0x3

    new-array v4, v0, [Ljava/lang/Class;

    const-class v2, Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v4, v3

    sget-object v2, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    const/4 v5, 0x1

    aput-object v2, v4, v5

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v6, 0x2

    aput-object v2, v4, v6

    if-eqz v1, :cond_0

    const-class v2, Landroid/content/pm/ApplicationInfo;

    new-array v7, v0, [Ljava/lang/Object;

    aput-object p0, v7, v3

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p0

    aput-object p0, v7, v5

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    aput-object p0, v7, v6

    const-string v0, "IPackageManagerCompat"

    const-string v3, "getApplicationInfo"

    move-object v5, v7

    invoke-static/range {v0 .. v5}, Lcom/android/packageinstaller/utils/r;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/content/pm/ApplicationInfo;

    return-object p0

    :cond_0
    const/4 p0, 0x0

    return-object p0
.end method

.method public static getBlockUninstallForUser(Ljava/lang/String;I)Z
    .locals 7

    invoke-static {}, Lcom/android/packageinstaller/compat/IPackageManagerCompat;->getIPackageManager()Ljava/lang/Object;

    move-result-object v1

    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/Class;

    const-class v2, Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v4, v3

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v5, 0x1

    aput-object v2, v4, v5

    if-eqz v1, :cond_0

    sget-object v2, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    new-array v6, v0, [Ljava/lang/Object;

    aput-object p0, v6, v3

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    aput-object p0, v6, v5

    const-string v0, "IPackageManagerCompat"

    const-string v3, "getBlockUninstallForUser"

    move-object v5, v6

    invoke-static/range {v0 .. v5}, Lcom/android/packageinstaller/utils/r;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0

    :cond_0
    return v3
.end method

.method public static getIPackageManager()Ljava/lang/Object;
    .locals 7

    :try_start_0
    const-string v0, "IPackageManagerCompat"

    const-string v1, "android.app.ActivityThread"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const-class v2, Ljava/lang/Object;

    const-string v3, "getPackageManager"

    const/4 v4, 0x0

    new-array v5, v4, [Ljava/lang/Class;

    new-array v6, v4, [Ljava/lang/Object;

    move-object v4, v5

    move-object v5, v6

    invoke-static/range {v0 .. v5}, Lcom/android/packageinstaller/utils/r;->c(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    const/4 v0, 0x0

    return-object v0
.end method
