.class public Lcom/android/packageinstaller/miui/OtherPermissionInfoActivity$a;
.super Lcom/android/packageinstaller/miui/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/packageinstaller/miui/OtherPermissionInfoActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/packageinstaller/miui/a;-><init>()V

    return-void
.end method

.method public static o2()Lcom/android/packageinstaller/miui/OtherPermissionInfoActivity$a;
    .locals 1

    new-instance v0, Lcom/android/packageinstaller/miui/OtherPermissionInfoActivity$a;

    invoke-direct {v0}, Lcom/android/packageinstaller/miui/OtherPermissionInfoActivity$a;-><init>()V

    return-object v0
.end method


# virtual methods
.method public Q1(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    invoke-super {p0, p1, p2}, Lcom/android/packageinstaller/miui/a;->Q1(Landroid/os/Bundle;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/android/packageinstaller/miui/a;->z0:Landroid/content/pm/PackageInfo;

    if-nez p1, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->w()Landroid/content/Context;

    move-result-object p1

    iget-object p2, p0, Lcom/android/packageinstaller/miui/a;->z0:Landroid/content/pm/PackageInfo;

    invoke-static {p1, p2}, Lcom/android/packageinstaller/miui/a;->n2(Landroid/content/Context;Landroid/content/pm/PackageInfo;)Lcom/android/packageinstaller/miui/a$a;

    move-result-object p1

    const-string p2, "other_relative"

    invoke-virtual {p0, p2}, Landroidx/preference/g;->g(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p2

    check-cast p2, Landroidx/preference/PreferenceCategory;

    invoke-virtual {p1}, Lcom/android/packageinstaller/miui/a$a;->d()Ljava/util/Map;

    move-result-object p1

    invoke-virtual {p0, p1, p2}, Lcom/android/packageinstaller/miui/a;->j2(Ljava/util/Map;Landroidx/preference/PreferenceCategory;)V

    iget-object p1, p0, Lcom/android/packageinstaller/miui/a;->B0:Lmiuix/appcompat/app/j;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lmiuix/appcompat/app/j;->r0()Lmiuix/appcompat/app/a;

    move-result-object p1

    if-eqz p1, :cond_1

    const p2, 0x7f1101e0

    invoke-virtual {p0, p2}, Landroidx/fragment/app/Fragment;->T(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ld/a;->v(Ljava/lang/CharSequence;)V

    :cond_1
    return-void
.end method

.method protected m2()I
    .locals 1

    const/high16 v0, 0x7f140000

    return v0
.end method
