.class public Lcom/android/packageinstaller/compat/UnknownSourceCompat;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/packageinstaller/compat/UnknownSourceCompat$AppOpsManager;
    }
.end annotation


# static fields
.field public static final ANDROID_O_SDK_INT:I = 0x1a

.field public static final EXTRA_ORIGINATING_UID:Ljava/lang/String; = "android.intent.extra.ORIGINATING_UID"

.field public static final MIUI_MARKET:Ljava/lang/String; = "com.xiaomi.market"

.field public static final MIUI_MARKET_SIGNATURE:Ljava/lang/String; = "7b6dc7079c34739ce81159719fb5eb61d2a03225"

.field public static final PERMISSION_INSTALL_PACKAGES:Ljava/lang/String; = "android.permission.INSTALL_PACKAGES"

.field public static final PERMISSION_REQUEST_INSTALL_PACKAGES:Ljava/lang/String; = "android.permission.REQUEST_INSTALL_PACKAGES"

.field public static final TAG:Ljava/lang/String; = "UnknownSourceCompat"

.field public static final UID_UNKNOWN:I = -0x1


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static checkUnknownSourcesEnabled(Landroid/app/Activity;Lm5/e;)Z
    .locals 0

    invoke-static {p0, p1}, Lcom/android/packageinstaller/compat/UnknownSourceCompat;->isUnknownSourcesEnabled(Landroid/app/Activity;Lm5/e;)Z

    move-result p0

    return p0
.end method

.method public static checkUnknownSourcesEnabled(Lm5/z0;)Z
    .locals 0

    invoke-static {p0}, Lcom/android/packageinstaller/compat/UnknownSourceCompat;->isUnknownSourcesEnabled(Lm5/z0;)Z

    move-result p0

    return p0
.end method

.method public static getOriginatingPackage(ILandroid/content/pm/PackageManager;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    const/4 v0, -0x1

    if-eq p0, v0, :cond_0

    invoke-static {p1, p0, p2}, Lcom/android/packageinstaller/compat/UnknownSourceCompat;->getPackageNameForUid(Landroid/content/pm/PackageManager;ILjava/lang/String;)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return-object p0
.end method

.method public static getOriginatingUid(ILandroid/content/Intent;)I
    .locals 1

    const/4 p0, -0x1

    const-string v0, "android.intent.extra.ORIGINATING_UID"

    invoke-virtual {p1, v0, p0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result p0

    return p0
.end method

.method public static getPackageNameForUid(Landroid/content/pm/PackageManager;ILjava/lang/String;)Ljava/lang/String;
    .locals 5

    invoke-virtual {p0, p1}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object p0

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    :cond_0
    array-length v0, p0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-le v0, v2, :cond_3

    if-eqz p2, :cond_2

    array-length v0, p0

    move v2, v1

    :goto_0
    if-ge v2, v0, :cond_2

    aget-object v3, p0, v2

    invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    return-object v3

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Multiple packages found for source uid "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "UnknownSourceCompat"

    invoke-static {p2, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    aget-object p0, p0, v1

    return-object p0
.end method

.method public static isUnknownSourcesEnabled(Landroid/app/Activity;Lm5/e;)Z
    .locals 3

    const-string v0, "appops"

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/app/AppOpsManager;

    invoke-virtual {p1}, Lm5/e;->l()I

    move-result v0

    invoke-virtual {p1}, Lm5/e;->k()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x42

    invoke-static {p0, v2, v0, v1}, Lcom/android/packageinstaller/compat/AppOpsManagerCompat;->checkOpNoThrow(Landroid/app/AppOpsManager;IILjava/lang/String;)I

    move-result p0

    const/4 v0, 0x1

    const/4 v1, 0x3

    if-ne p0, v1, :cond_0

    :try_start_0
    const-string v1, "android.permission.REQUEST_INSTALL_PACKAGES"

    invoke-virtual {p1}, Lm5/e;->l()I

    move-result p1

    invoke-static {v1, p1}, Lcom/android/packageinstaller/compat/IPackageManagerCompat;->checkUidPermission(Ljava/lang/String;I)I

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez p1, :cond_0

    return v0

    :catch_0
    const-string p1, "UnknownSourceCompat"

    const-string v1, "Unable to talk to package manager"

    invoke-static {p1, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    if-nez p0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public static isUnknownSourcesEnabled(Lm5/z0;)Z
    .locals 4

    const-string v0, "appops"

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AppOpsManager;

    invoke-virtual {p0}, Lm5/z0;->c()Lm5/e;

    move-result-object v1

    invoke-virtual {v1}, Lm5/e;->l()I

    move-result v1

    invoke-virtual {p0}, Lm5/z0;->c()Lm5/e;

    move-result-object v2

    invoke-virtual {v2}, Lm5/e;->k()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x42

    invoke-static {v0, v3, v1, v2}, Lcom/android/packageinstaller/compat/AppOpsManagerCompat;->checkOpNoThrow(Landroid/app/AppOpsManager;IILjava/lang/String;)I

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x3

    if-ne v0, v2, :cond_0

    :try_start_0
    const-string v2, "android.permission.REQUEST_INSTALL_PACKAGES"

    invoke-virtual {p0}, Lm5/z0;->c()Lm5/e;

    move-result-object p0

    invoke-virtual {p0}, Lm5/e;->l()I

    move-result p0

    invoke-static {v2, p0}, Lcom/android/packageinstaller/compat/IPackageManagerCompat;->checkUidPermission(Ljava/lang/String;I)I

    move-result p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez p0, :cond_0

    return v1

    :catch_0
    const-string p0, "UnknownSourceCompat"

    const-string v2, "Unable to talk to package manager"

    invoke-static {p0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    if-nez v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public static isUnknownSourcesEnabled(ZLm5/z0;)Z
    .locals 0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    return p0

    :cond_0
    invoke-static {p1}, Lcom/android/packageinstaller/compat/UnknownSourceCompat;->isUnknownSourcesEnabled(Lm5/z0;)Z

    move-result p0

    return p0
.end method

.method public static setNonMarketAppsAllowed(Landroid/content/Context;ILjava/lang/String;)V
    .locals 2

    const-string v0, "appops"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/app/AppOpsManager;

    const/16 v0, 0x42

    const/4 v1, 0x2

    invoke-static {p0, v0, p1, p2, v1}, Lcom/android/packageinstaller/compat/AppOpsManagerCompat;->setMode(Landroid/app/AppOpsManager;IILjava/lang/String;I)V

    return-void
.end method
