.class public Lcom/android/packageinstaller/compat/NotificationCompat;
.super Ljava/lang/Object;


# static fields
.field private static final NOTIFICATION_CHANNEL:Ljava/lang/String; = "com.miui.packageinstaller"

.field private static final NOTIFY_ID_BASE:I = 0x64

.field public static final NOTIFY_ID_ELDER_SECURITY_MODE_GUIDE:I = 0x65

.field public static final NOTIFY_ID_SECURITY_MODE_OPEN:I = 0x65


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createNotificationChannel(Landroid/app/NotificationManager;Landroid/content/Context;)V
    .locals 0

    invoke-static {p1}, Lcom/android/packageinstaller/compat/NotificationCompat;->getChannel(Landroid/content/Context;)Landroid/app/NotificationChannel;

    move-result-object p1

    invoke-virtual {p0, p1}, Landroid/app/NotificationManager;->createNotificationChannel(Landroid/app/NotificationChannel;)V

    return-void
.end method

.method private static getChannel(Landroid/content/Context;)Landroid/app/NotificationChannel;
    .locals 3

    const/4 v0, 0x4

    new-instance v1, Landroid/app/NotificationChannel;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    const v2, 0x7f110023

    invoke-virtual {p0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    const-string v2, "com.miui.packageinstaller"

    invoke-direct {v1, v2, p0, v0}, Landroid/app/NotificationChannel;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;I)V

    return-object v1
.end method

.method public static getNotificationBuilder(Landroid/content/Context;)Landroid/app/Notification$Builder;
    .locals 2

    new-instance v0, Landroid/app/Notification$Builder;

    const-string v1, "com.miui.packageinstaller"

    invoke-direct {v0, p0, v1}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    return-object v0
.end method
