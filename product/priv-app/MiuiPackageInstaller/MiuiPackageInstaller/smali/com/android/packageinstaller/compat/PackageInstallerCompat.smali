.class public Lcom/android/packageinstaller/compat/PackageInstallerCompat;
.super Ljava/lang/Object;


# static fields
.field public static final ACTION_CONFIRM_INSTALL:Ljava/lang/String; = "android.content.pm.action.CONFIRM_INSTALL"

.field public static final ACTION_CONFIRM_PERMISSIONS:Ljava/lang/String; = "android.content.pm.action.CONFIRM_PERMISSIONS"

.field public static final EXTRA_CALLBACK:Ljava/lang/String; = "android.content.pm.extra.CALLBACK"

.field public static final EXTRA_LEGACY_STATUS:Ljava/lang/String; = "android.content.pm.extra.LEGACY_STATUS"

.field public static final TAG:Ljava/lang/String; = "PackageInstallerCompat"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getSessionAction()Ljava/lang/String;
    .locals 1

    const-string v0, "android.content.pm.action.CONFIRM_INSTALL"

    return-object v0
.end method

.method public static setFiledValue(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    const-string v0, "PackageInstallerCompat"

    invoke-static {v0, p0, p1, p2}, Lcom/android/packageinstaller/utils/r;->g(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method public static setHeaderHeight(Landroid/app/Activity;Landroid/view/ViewGroup;)V
    .locals 2

    invoke-static {p0}, Lcom/android/packageinstaller/compat/ActivityCompat;->isInMultiWindowMode(Landroid/app/Activity;)Z

    move-result v0

    const-string v1, "setHeaderHeight"

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v0, 0x437c0000    # 252.0f

    invoke-static {p0, v0}, Lcom/android/packageinstaller/utils/g;->a(Landroid/content/Context;F)F

    move-result p0

    float-to-int p0, p0

    iput p0, p1, Landroid/widget/LinearLayout$LayoutParams;->height:I

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "setHeaderHeight="

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_0
    const-string p0, "not in MultiWindow Mode"

    :goto_0
    invoke-static {v1, p0}, Lf6/o;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    const-string p0, "PackageInstallerCompat"

    const-string p1, "setHeaderHeight no action"

    invoke-static {p0, p1}, Lf6/o;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    return-void
.end method

.method public static setOriginatingUid(Landroid/content/pm/PackageInstaller$SessionParams;I)V
    .locals 1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const-string v0, "originatingUid"

    invoke-static {p0, v0, p1}, Lcom/android/packageinstaller/compat/PackageInstallerCompat;->setFiledValue(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method public static setPermissionsResult(Landroid/content/pm/PackageInstaller;IZ)V
    .locals 5

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/Class;

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    sget-object v2, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const/4 v4, 0x1

    aput-object v2, v1, v4

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    aput-object p1, v0, v4

    const-string p1, "PackageInstallerCompat"

    const-string p2, "setPermissionsResult"

    invoke-static {p1, p0, p2, v1, v0}, Lcom/android/packageinstaller/utils/r;->b(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
