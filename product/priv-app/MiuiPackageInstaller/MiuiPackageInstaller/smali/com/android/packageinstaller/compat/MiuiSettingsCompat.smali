.class public Lcom/android/packageinstaller/compat/MiuiSettingsCompat;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/packageinstaller/compat/MiuiSettingsCompat$SecurityModeStyle;
    }
.end annotation


# static fields
.field private static final FIRST_BOOT_TIME:Ljava/lang/String; = "packageinstaller_first_boot_time"

.field private static final INSTALL_MONITOR_ENABLED:Ljava/lang/String; = "virus_scan_install"

.field private static final INSTALL_RISK_ENABLED:Ljava/lang/String; = "app_safe_check_enable"

.field public static final PACKAGE_INSTALLER:Ljava/lang/String; = "pi"

.field private static final PURE_MODE_ACCUMULATE_TIME:Ljava/lang/String; = "pure_mode_accumulate_time"

.field private static final PURE_MODE_OPEN_TIME:Ljava/lang/String; = "pure_mode_open_time"

.field private static final PURE_MODE_PROTECT_COUNT:Ljava/lang/String; = "pure_mode_protect_count"

.field private static final SAFE_MODE_ENABLE:Ljava/lang/String; = "miui_safe_mode"

.field private static final SECURITY_MODE_RISK_APP_DOWNLOAD_WARN:Ljava/lang/String; = "miui_miprotect_risk_app_download_warn"

.field private static final SECURITY_MODE_RISK_APP_REMIND_WARN:Ljava/lang/String; = "miui_miprotect_risk_app_remind_warn"

.field private static final SECURITY_MODE_STYLE:Ljava/lang/String; = "miui_security_mode_style"

.field public static final SECURITY_MODE_STYLE_ELDER:Ljava/lang/String; = "elder"

.field public static final SECURITY_MODE_STYLE_NORMAL:Ljava/lang/String; = "normal"

.field public static final SECURITY_NODE_STYLE_CHILD:Ljava/lang/String; = "child_mode"

.field public static final SECURITY_NODE_STYLE_EASY_MODE:Ljava/lang/String; = "easy_mode"

.field public static final SETTINGS:Ljava/lang/String; = "settings"

.field public static final TAG:Ljava/lang/String; = "MiuiSettingsCompat"

.field private static firstBootTime:J

.field private static isGetFirstBootTime:Z

.field private static isGetPureOpenTime:Z

.field private static isInit:Z

.field private static isSafeMode:Z

.field private static sSecureSettingLocation:Ljava/lang/String;

.field private static securityModeOpenTime:J


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$002(Z)Z
    .locals 0

    sput-boolean p0, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->isSafeMode:Z

    return p0
.end method

.method public static calculateSecurityModeProtectDays()I
    .locals 11

    invoke-static {}, Lcom/android/packageinstaller/InstallerApplication;->g()Lcom/android/packageinstaller/InstallerApplication;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "pure_mode_accumulate_time"

    const-wide/16 v5, 0x0

    invoke-static {v3, v4, v5, v6}, Lya/a;->c(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v3

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "pure_mode_open_time"

    invoke-static {v7, v8, v1, v2}, Lya/a;->c(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v9

    cmp-long v7, v9, v1

    if-eqz v7, :cond_0

    cmp-long v7, v9, v5

    if-nez v7, :cond_1

    :cond_0
    invoke-static {v0}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->isSafeModelEnable(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    invoke-static {v7, v8, v1, v2}, Lya/a;->h(Landroid/content/ContentResolver;Ljava/lang/String;J)Z

    :cond_1
    sub-long v9, v1, v9

    cmp-long v7, v9, v5

    if-gez v7, :cond_2

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0, v8, v1, v2}, Lya/a;->c(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    goto :goto_0

    :cond_2
    move-wide v5, v9

    :goto_0
    add-long/2addr v3, v5

    const-wide/32 v0, 0x5265c00

    div-long/2addr v3, v0

    const-wide/16 v0, 0x1

    add-long/2addr v3, v0

    long-to-int v0, v3

    return v0
.end method

.method public static countSecurityModeProtectTimes()V
    .locals 7

    invoke-static {}, Lcom/android/packageinstaller/InstallerApplication;->g()Lcom/android/packageinstaller/InstallerApplication;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "pure_mode_protect_count"

    const-wide/16 v3, 0x0

    invoke-static {v1, v2, v3, v4}, Lya/a;->c(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v3

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-wide/16 v5, 0x1

    add-long/2addr v3, v5

    invoke-static {v0, v2, v3, v4}, Lya/a;->h(Landroid/content/ContentResolver;Ljava/lang/String;J)Z

    return-void
.end method

.method public static getCloudDataBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Z)Z
    .locals 10

    const-class v0, Ljava/lang/String;

    const/4 v1, 0x0

    :try_start_0
    const-string v2, "android.provider.MiuiSettings$SettingsCloudData"

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    const/4 v2, 0x4

    new-array v7, v2, [Ljava/lang/Class;

    const-class v3, Landroid/content/ContentResolver;

    aput-object v3, v7, v1

    const/4 v3, 0x1

    aput-object v0, v7, v3

    const/4 v5, 0x2

    aput-object v0, v7, v5

    sget-object v0, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const/4 v6, 0x3

    aput-object v0, v7, v6

    const-string v8, "MiuiSettingsCompat"

    const-string v9, "getCloudDataBoolean"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p0, v2, v1

    aput-object p1, v2, v3

    aput-object p2, v2, v5

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    aput-object p0, v2, v6

    move-object v3, v8

    move-object v5, v0

    move-object v6, v9

    move-object v8, v2

    invoke-static/range {v3 .. v8}, Lcom/android/packageinstaller/utils/r;->c(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    const-string p1, "MiuiSettingsCompat"

    const-string p2, "getCloudDataBoolean exception: "

    invoke-static {p1, p2, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_0
    return v1
.end method

.method public static getCloudDataList(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    :try_start_0
    const-string v0, "android.provider.MiuiSettings$SettingsCloudData"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    const/4 v0, 0x2

    new-array v5, v0, [Ljava/lang/Class;

    const-class v1, Landroid/content/ContentResolver;

    const/4 v3, 0x0

    aput-object v1, v5, v3

    const-class v1, Ljava/lang/String;

    const/4 v4, 0x1

    aput-object v1, v5, v4

    const-string v1, "MiuiSettingsCompat"

    const-class v6, Ljava/util/List;

    const-string v7, "getCloudDataList"

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p0, v0, v3

    aput-object p1, v0, v4

    move-object v3, v6

    move-object v4, v7

    move-object v6, v0

    invoke-static/range {v1 .. v6}, Lcom/android/packageinstaller/utils/r;->c(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    invoke-virtual {p0}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    const/4 p0, 0x0

    return-object p0
.end method

.method public static getCloudDataString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 10

    const-class v0, Ljava/lang/String;

    :try_start_0
    const-string v1, "android.provider.MiuiSettings$SettingsCloudData"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    const/4 v1, 0x4

    new-array v6, v1, [Ljava/lang/Class;

    const-class v2, Landroid/content/ContentResolver;

    const/4 v4, 0x0

    aput-object v2, v6, v4

    const/4 v2, 0x1

    aput-object v0, v6, v2

    const/4 v5, 0x2

    aput-object v0, v6, v5

    const/4 v7, 0x3

    aput-object v0, v6, v7

    const-string v0, "MiuiSettingsCompat"

    const-class v8, Ljava/lang/String;

    const-string v9, "getCloudDataString"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p0, v1, v4

    aput-object p1, v1, v2

    aput-object p2, v1, v5

    aput-object p3, v1, v7

    move-object v2, v0

    move-object v4, v8

    move-object v5, v9

    move-object v7, v1

    invoke-static/range {v2 .. v7}, Lcom/android/packageinstaller/utils/r;->c(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    const-string p1, "MiuiSettingsCompat"

    const-string p2, "getCloudDataString exception: "

    invoke-static {p1, p2, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const-string p0, ""

    :goto_0
    return-object p0
.end method

.method public static getFirstBootTime(Landroid/content/Context;)J
    .locals 4

    sget-boolean v0, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->isGetFirstBootTime:Z

    if-eqz v0, :cond_0

    sget-wide v0, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->firstBootTime:J

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    return-wide v0

    :cond_0
    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->isGetFirstBootTime:Z

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-wide/16 v0, -0x1

    const-string v2, "packageinstaller_first_boot_time"

    invoke-static {p0, v2, v0, v1}, Lya/a;->c(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v0

    sput-wide v0, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->firstBootTime:J

    return-wide v0
.end method

.method public static getSafeModelRiskAppDownloadWarn(Landroid/content/Context;)Z
    .locals 2

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string v0, "miui_miprotect_risk_app_download_warn"

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lya/a;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result p0

    return p0
.end method

.method public static getSafeModelRiskAppRemindWarn(Landroid/content/Context;)Z
    .locals 2

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string v0, "miui_miprotect_risk_app_remind_warn"

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lya/a;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result p0

    return p0
.end method

.method public static getSecureSettingLocation()Ljava/lang/String;
    .locals 4

    sget-object v0, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->sSecureSettingLocation:Ljava/lang/String;

    if-nez v0, :cond_1

    const-string v0, "pi"

    sput-object v0, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->sSecureSettingLocation:Ljava/lang/String;

    :try_start_0
    invoke-static {}, Lcom/android/packageinstaller/InstallerApplication;->g()Lcom/android/packageinstaller/InstallerApplication;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Application;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "com.android.settings"

    const/16 v3, 0x80

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    if-eqz v1, :cond_1

    const-string v2, "miui.support_safe_install_mode"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "settings"

    goto :goto_0

    :cond_0
    move-object v1, v0

    :goto_0
    sput-object v1, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->sSecureSettingLocation:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    sput-object v0, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->sSecureSettingLocation:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    :cond_1
    :goto_1
    sget-object v0, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->sSecureSettingLocation:Ljava/lang/String;

    return-object v0
.end method

.method public static getSecurityModeProtectTimes()J
    .locals 4

    invoke-static {}, Lcom/android/packageinstaller/InstallerApplication;->g()Lcom/android/packageinstaller/InstallerApplication;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "pure_mode_protect_count"

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lya/a;->c(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static getSecurityModeStyle(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string v0, "miui_security_mode_style"

    const-string v1, "normal"

    invoke-static {p0, v0, v1}, Lya/a;->d(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    const-string v0, "elder"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "easy_mode"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    move-object v1, p0

    :goto_0
    return-object v1
.end method

.method public static getSettingString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    invoke-static {p0, p1}, Lya/b;->c(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static isInstallMonitorEnabled(Landroid/content/Context;)Z
    .locals 2

    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v0, :cond_0

    const/4 p0, 0x0

    return p0

    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string v0, "virus_scan_install"

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lya/b;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result p0

    return p0
.end method

.method public static isInstallRiskEnabled(Landroid/content/Context;)Z
    .locals 2

    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v0, :cond_0

    const/4 p0, 0x0

    return p0

    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string v0, "virus_scan_install"

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lya/b;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result p0

    return p0
.end method

.method public static isPersonalizedAdEnabled()Z
    .locals 7

    const/4 v0, 0x1

    :try_start_0
    const-string v1, "android.provider.MiuiSettings$Ad"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const-string v2, "isPersonalizedAdEnabled"

    new-array v3, v0, [Ljava/lang/Class;

    const-class v4, Landroid/content/ContentResolver;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    new-array v4, v0, [Ljava/lang/Object;

    invoke-static {}, Lcom/android/packageinstaller/InstallerApplication;->g()Lcom/android/packageinstaller/InstallerApplication;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/Application;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v1, v2, v3, v4}, Lcom/android/packageinstaller/utils/q;->d(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    const-string v3, "MiuiSettingsCompat"

    invoke-static {v3, v2, v1}, Lf6/o;->f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    return v0
.end method

.method public static isSafeModelEnable(Landroid/content/Context;)Z
    .locals 3

    sget-boolean v0, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->isInit:Z

    if-eqz v0, :cond_0

    sget-boolean p0, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->isSafeMode:Z

    return p0

    :cond_0
    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-eqz v0, :cond_1

    const/4 p0, 0x0

    sput-boolean p0, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->isSafeMode:Z

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->isInit:Z

    return p0

    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    sget-object v0, Lf6/s;->a:Lf6/s$a;

    invoke-virtual {v0}, Lf6/s$a;->a()Lf6/s;

    move-result-object v1

    const-string v2, "safe_mode_is_change"

    invoke-virtual {v1, v2}, Lf6/s;->a(Ljava/lang/String;)Z

    move-result v1

    const/4 v2, 0x1

    if-nez v1, :cond_2

    invoke-virtual {v0}, Lf6/s$a;->a()Lf6/s;

    move-result-object v0

    const-string v1, "safe_mode_is_open_cloud_config"

    invoke-virtual {v0, v1}, Lf6/s;->e(Ljava/lang/String;)I

    move-result v0

    if-ne v0, v2, :cond_2

    move v0, v2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    const-string v1, "miui_safe_mode"

    invoke-static {p0, v1, v0}, Lya/a;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result p0

    sput-boolean p0, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->isSafeMode:Z

    sput-boolean v2, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->isInit:Z

    return p0
.end method

.method public static setFirstBootTime(Landroid/content/Context;J)V
    .locals 1

    sput-wide p1, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->firstBootTime:J

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string v0, "packageinstaller_first_boot_time"

    invoke-static {p0, v0, p1, p2}, Lya/a;->h(Landroid/content/ContentResolver;Ljava/lang/String;J)Z

    return-void
.end method

.method public static setInstallMonitorEnabled(Landroid/content/Context;Z)V
    .locals 1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string v0, "virus_scan_install"

    invoke-static {p0, v0, p1}, Lya/b;->d(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    return-void
.end method

.method public static setInstallRiskEnabled(Landroid/content/Context;Z)V
    .locals 1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string v0, "virus_scan_install"

    invoke-static {p0, v0, p1}, Lya/b;->d(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    return-void
.end method

.method private static setSafeContinuousOpenTime(Landroid/content/Context;J)V
    .locals 1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string v0, "pure_mode_open_time"

    invoke-static {p0, v0, p1, p2}, Lya/a;->h(Landroid/content/ContentResolver;Ljava/lang/String;J)Z

    return-void
.end method

.method public static setSafeModelEnabled(Landroid/content/Context;Z)V
    .locals 1

    const-string v0, "normal"

    invoke-static {p0, p1, v0}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->setSafeModelEnabled(Landroid/content/Context;ZLjava/lang/String;)V

    return-void
.end method

.method public static setSafeModelEnabled(Landroid/content/Context;ZLjava/lang/String;)V
    .locals 2

    sput-boolean p1, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->isSafeMode:Z

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "miui_safe_mode"

    invoke-static {v0, v1, p1}, Lya/a;->f(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "miui_security_mode_style"

    invoke-static {v0, v1, p2}, Lya/a;->i(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    invoke-static {}, Lf6/s;->d()Lf6/s;

    move-result-object p2

    const-string v0, "safe_mode_is_change"

    const/4 v1, 0x1

    invoke-virtual {p2, v0, v1}, Lf6/s;->h(Ljava/lang/String;Z)V

    invoke-static {p1}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->updateSecurityModeUseTime(Z)V

    invoke-static {p0}, Lm2/b;->g(Landroid/content/Context;)Lm2/b;

    move-result-object p1

    sget-boolean p2, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->isSafeMode:Z

    invoke-virtual {p1, p2}, Lm2/b;->I(Z)V

    invoke-static {p0}, Lm2/b;->g(Landroid/content/Context;)Lm2/b;

    move-result-object p0

    const/4 p1, 0x0

    invoke-static {p1}, Lx5/a;->b(I)Lx5/a;

    move-result-object p1

    invoke-virtual {p0, p1}, Lm2/b;->J(Lx5/a;)V

    return-void
.end method

.method public static setSafeModelRiskAppDownloadWarn(Landroid/content/Context;Z)V
    .locals 1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string v0, "miui_miprotect_risk_app_download_warn"

    invoke-static {p0, v0, p1}, Lya/a;->f(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    return-void
.end method

.method public static setSafeModelRiskAppRemindWarn(Landroid/content/Context;Z)V
    .locals 1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    const-string v0, "miui_miprotect_risk_app_remind_warn"

    invoke-static {p0, v0, p1}, Lya/a;->f(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    return-void
.end method

.method public static setSecurityModeStyle(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "miui_security_mode_style"

    invoke-static {v0, v1, p1}, Lya/a;->i(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    const/4 p1, 0x1

    invoke-static {p0, p1}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->setSafeModelRiskAppDownloadWarn(Landroid/content/Context;Z)V

    invoke-static {p0, p1}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->setSafeModelRiskAppRemindWarn(Landroid/content/Context;Z)V

    invoke-static {p0}, Lm2/b;->g(Landroid/content/Context;)Lm2/b;

    move-result-object p0

    invoke-virtual {p0, p1}, Lm2/b;->K(Z)V

    return-void
.end method

.method public static setSettingString(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    invoke-static {p0, p1, p2}, Lya/b;->f(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    return-void
.end method

.method private static updateSecurityModeUseTime(Z)V
    .locals 12

    invoke-static {}, Lcom/android/packageinstaller/InstallerApplication;->g()Lcom/android/packageinstaller/InstallerApplication;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "pure_mode_accumulate_time"

    const-wide/16 v5, 0x0

    invoke-static {v3, v4, v5, v6}, Lya/a;->c(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v7

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v9, "pure_mode_open_time"

    invoke-static {v3, v9, v1, v2}, Lya/a;->c(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v10

    cmp-long v3, v10, v5

    if-nez v3, :cond_0

    move-wide v10, v1

    :cond_0
    sub-long v10, v1, v10

    cmp-long v3, v10, v5

    if-gez v3, :cond_1

    move-wide v10, v5

    :cond_1
    if-eqz p0, :cond_2

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    add-long/2addr v7, v10

    invoke-static {p0, v4, v7, v8}, Lya/a;->h(Landroid/content/ContentResolver;Ljava/lang/String;J)Z

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    invoke-static {p0, v9, v1, v2}, Lya/a;->h(Landroid/content/ContentResolver;Ljava/lang/String;J)Z

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    add-long/2addr v7, v10

    invoke-static {p0, v4, v7, v8}, Lya/a;->h(Landroid/content/ContentResolver;Ljava/lang/String;J)Z

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p0

    invoke-static {p0, v9, v5, v6}, Lya/a;->h(Landroid/content/ContentResolver;Ljava/lang/String;J)Z

    :goto_0
    return-void
.end method
