.class public final enum Lcom/android/packageinstaller/utils/n$b;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/packageinstaller/utils/n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/android/packageinstaller/utils/n$b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/android/packageinstaller/utils/n$b;

.field public static final enum b:Lcom/android/packageinstaller/utils/n$b;

.field public static final enum c:Lcom/android/packageinstaller/utils/n$b;

.field public static final enum d:Lcom/android/packageinstaller/utils/n$b;

.field private static final synthetic e:[Lcom/android/packageinstaller/utils/n$b;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    new-instance v0, Lcom/android/packageinstaller/utils/n$b;

    const-string v1, "GET"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/android/packageinstaller/utils/n$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/packageinstaller/utils/n$b;->a:Lcom/android/packageinstaller/utils/n$b;

    new-instance v1, Lcom/android/packageinstaller/utils/n$b;

    const-string v3, "POST"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lcom/android/packageinstaller/utils/n$b;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/android/packageinstaller/utils/n$b;->b:Lcom/android/packageinstaller/utils/n$b;

    new-instance v3, Lcom/android/packageinstaller/utils/n$b;

    const-string v5, "PUT"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lcom/android/packageinstaller/utils/n$b;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lcom/android/packageinstaller/utils/n$b;->c:Lcom/android/packageinstaller/utils/n$b;

    new-instance v5, Lcom/android/packageinstaller/utils/n$b;

    const-string v7, "DELETE"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8}, Lcom/android/packageinstaller/utils/n$b;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lcom/android/packageinstaller/utils/n$b;->d:Lcom/android/packageinstaller/utils/n$b;

    const/4 v7, 0x4

    new-array v7, v7, [Lcom/android/packageinstaller/utils/n$b;

    aput-object v0, v7, v2

    aput-object v1, v7, v4

    aput-object v3, v7, v6

    aput-object v5, v7, v8

    sput-object v7, Lcom/android/packageinstaller/utils/n$b;->e:[Lcom/android/packageinstaller/utils/n$b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/packageinstaller/utils/n$b;
    .locals 1

    const-class v0, Lcom/android/packageinstaller/utils/n$b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/android/packageinstaller/utils/n$b;

    return-object p0
.end method

.method public static values()[Lcom/android/packageinstaller/utils/n$b;
    .locals 1

    sget-object v0, Lcom/android/packageinstaller/utils/n$b;->e:[Lcom/android/packageinstaller/utils/n$b;

    invoke-virtual {v0}, [Lcom/android/packageinstaller/utils/n$b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/packageinstaller/utils/n$b;

    return-object v0
.end method
