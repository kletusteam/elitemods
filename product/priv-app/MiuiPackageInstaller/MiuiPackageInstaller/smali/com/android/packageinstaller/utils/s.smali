.class public final Lcom/android/packageinstaller/utils/s;
.super Ljava/lang/Object;


# static fields
.field public static final a:Lcom/android/packageinstaller/utils/s;

.field private static b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/android/packageinstaller/utils/s;

    invoke-direct {v0}, Lcom/android/packageinstaller/utils/s;-><init>()V

    sput-object v0, Lcom/android/packageinstaller/utils/s;->a:Lcom/android/packageinstaller/utils/s;

    invoke-static {}, Lcom/android/packageinstaller/InstallerApplication;->g()Lcom/android/packageinstaller/InstallerApplication;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Application;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f030011

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    const-string v1, "getInstance().resources.\u2026on_risk_privacy\n        )"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    array-length v1, v0

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lb8/j;->h([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/android/packageinstaller/utils/s;->b:Ljava/util/List;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    const-string v0, "privacyApi"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Lcom/android/packageinstaller/InstallerApplication;->g()Lcom/android/packageinstaller/InstallerApplication;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Application;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget-object v1, Lcom/android/packageinstaller/utils/s;->b:Ljava/util/List;

    invoke-interface {v1, p0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1c

    const-string v1, "android.permission.SYSTEM_CAMERA"

    invoke-static {p0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const p0, 0x7f110286

    :goto_0
    invoke-virtual {v0, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_0
    const-string v1, "android.permission.MANAGE_OWN_CALLS"

    invoke-static {p0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const p0, 0x7f110278

    goto :goto_0

    :cond_1
    const-string v1, "android.permission.GET_TASKS"

    invoke-static {p0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const p0, 0x7f110277

    goto :goto_0

    :cond_2
    const-string v1, "android.permission.CALL_COMPANION_APP"

    invoke-static {p0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const p0, 0x7f110273

    goto :goto_0

    :cond_3
    const-string v1, "android.permission.WRITE_EXTERNAL_STORAGE"

    invoke-static {p0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    const p0, 0x7f110288

    goto :goto_0

    :cond_4
    const-string v1, "android.permission.WRITE_CALENDAR"

    invoke-static {p0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    const p0, 0x7f110287

    goto :goto_0

    :cond_5
    const-string v1, "android.permission.RECORD_AUDIO"

    invoke-static {p0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    const p0, 0x7f110285

    goto :goto_0

    :cond_6
    const-string v1, "android.permission.RECEIVE_WAP_PUSH"

    invoke-static {p0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    const p0, 0x7f110284

    goto :goto_0

    :cond_7
    const-string v1, "android.permission.RECEIVE_SMS"

    invoke-static {p0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    const p0, 0x7f110283

    goto :goto_0

    :cond_8
    const-string v1, "android.permission.RECEIVE_MMS"

    invoke-static {p0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    const p0, 0x7f110282

    goto :goto_0

    :cond_9
    const-string v1, "android.permission.READ_SMS"

    invoke-static {p0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    const p0, 0x7f110281

    goto :goto_0

    :cond_a
    const-string v1, "android.permission.READ_PHONE_STATE"

    invoke-static {p0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    const p0, 0x7f110280

    goto/16 :goto_0

    :cond_b
    const-string v1, "android.permission.READ_PHONE_NUMBERS"

    invoke-static {p0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    const p0, 0x7f11027f

    goto/16 :goto_0

    :cond_c
    const-string v1, "android.permission.READ_EXTERNAL_STORAGE"

    invoke-static {p0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    const p0, 0x7f11027e

    goto/16 :goto_0

    :cond_d
    const-string v1, "android.permission.READ_CONTACTS"

    invoke-static {p0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    const p0, 0x7f11027d

    goto/16 :goto_0

    :cond_e
    const-string v1, "android.permission.READ_CELL_BROADCASTS"

    invoke-static {p0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    const p0, 0x7f11027c

    goto/16 :goto_0

    :cond_f
    const-string v1, "android.permission.READ_CALL_LOG"

    invoke-static {p0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    const p0, 0x7f11027b

    goto/16 :goto_0

    :cond_10
    const-string v1, "android.permission.READ_CALENDAR"

    invoke-static {p0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11

    const p0, 0x7f11027a

    goto/16 :goto_0

    :cond_11
    const-string v1, "android.permission.GET_ACCOUNTS"

    invoke-static {p0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_12

    const p0, 0x7f110275

    goto/16 :goto_0

    :cond_12
    const-string v1, "android.permission.CAMERA"

    invoke-static {p0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_13

    const p0, 0x7f110274

    goto/16 :goto_0

    :cond_13
    const-string v1, "android.permission.BODY_SENSORS"

    invoke-static {p0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_14

    const p0, 0x7f110272

    goto/16 :goto_0

    :cond_14
    const-string v1, "android.permission.ACTIVITY_RECOGNITION"

    invoke-static {p0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_15

    const p0, 0x7f110271

    goto/16 :goto_0

    :cond_15
    const-string v1, "android.permission.ACCESS_MEDIA_LOCATION"

    invoke-static {p0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_16

    const p0, 0x7f11026f

    goto/16 :goto_0

    :cond_16
    const-string v1, "android.permission.ACCESS_FINE_LOCATION"

    invoke-static {p0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_17

    const p0, 0x7f11026e

    goto/16 :goto_0

    :cond_17
    const-string v1, "android.permission.ACCESS_COARSE_LOCATION"

    invoke-static {p0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_18

    const p0, 0x7f11026d

    goto/16 :goto_0

    :cond_18
    const-string v1, "android.permission.ACCESS_BACKGROUND_LOCATION"

    invoke-static {p0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_19

    const p0, 0x7f11026c

    goto/16 :goto_0

    :cond_19
    const-string v1, "android.permission.GET_INSTALLED_APPS"

    invoke-static {p0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1a

    const p0, 0x7f110276

    goto/16 :goto_0

    :cond_1a
    const-string v1, "android.permission.PERMLAB_BLUETOOTH_SCAN"

    invoke-static {p0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1b

    const p0, 0x7f110279

    goto/16 :goto_0

    :cond_1b
    const-string v1, "android.permission.ACCESS_NOTIFICATIONS"

    invoke-static {p0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1c

    const p0, 0x7f110270

    goto/16 :goto_0

    :cond_1c
    const/4 p0, 0x0

    return-object p0
.end method

.method public static final b(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    const-string v0, "privacyApi"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Lcom/android/packageinstaller/InstallerApplication;->g()Lcom/android/packageinstaller/InstallerApplication;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Application;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget-object v1, Lcom/android/packageinstaller/utils/s;->b:Ljava/util/List;

    invoke-interface {v1, p0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1c

    const-string v1, "android.permission.SYSTEM_CAMERA"

    invoke-static {p0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const p0, 0x7f1102a3

    :goto_0
    invoke-virtual {v0, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_0
    const-string v1, "android.permission.MANAGE_OWN_CALLS"

    invoke-static {p0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const p0, 0x7f110295

    goto :goto_0

    :cond_1
    const-string v1, "android.permission.GET_TASKS"

    invoke-static {p0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const p0, 0x7f110294

    goto :goto_0

    :cond_2
    const-string v1, "android.permission.CALL_COMPANION_APP"

    invoke-static {p0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const p0, 0x7f110290

    goto :goto_0

    :cond_3
    const-string v1, "android.permission.WRITE_EXTERNAL_STORAGE"

    invoke-static {p0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    const p0, 0x7f1102a5

    goto :goto_0

    :cond_4
    const-string v1, "android.permission.WRITE_CALENDAR"

    invoke-static {p0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    const p0, 0x7f1102a4

    goto :goto_0

    :cond_5
    const-string v1, "android.permission.RECORD_AUDIO"

    invoke-static {p0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    const p0, 0x7f1102a2

    goto :goto_0

    :cond_6
    const-string v1, "android.permission.RECEIVE_WAP_PUSH"

    invoke-static {p0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    const p0, 0x7f1102a1

    goto :goto_0

    :cond_7
    const-string v1, "android.permission.RECEIVE_SMS"

    invoke-static {p0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    const p0, 0x7f1102a0

    goto :goto_0

    :cond_8
    const-string v1, "android.permission.RECEIVE_MMS"

    invoke-static {p0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    const p0, 0x7f11029f

    goto :goto_0

    :cond_9
    const-string v1, "android.permission.READ_SMS"

    invoke-static {p0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    const p0, 0x7f11029e

    goto :goto_0

    :cond_a
    const-string v1, "android.permission.READ_PHONE_STATE"

    invoke-static {p0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    const p0, 0x7f11029d

    goto/16 :goto_0

    :cond_b
    const-string v1, "android.permission.READ_PHONE_NUMBERS"

    invoke-static {p0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    const p0, 0x7f11029c

    goto/16 :goto_0

    :cond_c
    const-string v1, "android.permission.READ_EXTERNAL_STORAGE"

    invoke-static {p0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    const p0, 0x7f11029b

    goto/16 :goto_0

    :cond_d
    const-string v1, "android.permission.READ_CONTACTS"

    invoke-static {p0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    const p0, 0x7f11029a

    goto/16 :goto_0

    :cond_e
    const-string v1, "android.permission.READ_CELL_BROADCASTS"

    invoke-static {p0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    const p0, 0x7f110299

    goto/16 :goto_0

    :cond_f
    const-string v1, "android.permission.READ_CALL_LOG"

    invoke-static {p0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    const p0, 0x7f110298

    goto/16 :goto_0

    :cond_10
    const-string v1, "android.permission.READ_CALENDAR"

    invoke-static {p0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11

    const p0, 0x7f110297

    goto/16 :goto_0

    :cond_11
    const-string v1, "android.permission.GET_ACCOUNTS"

    invoke-static {p0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_12

    const p0, 0x7f110292

    goto/16 :goto_0

    :cond_12
    const-string v1, "android.permission.CAMERA"

    invoke-static {p0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_13

    const p0, 0x7f110291

    goto/16 :goto_0

    :cond_13
    const-string v1, "android.permission.BODY_SENSORS"

    invoke-static {p0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_14

    const p0, 0x7f11028f

    goto/16 :goto_0

    :cond_14
    const-string v1, "android.permission.ACTIVITY_RECOGNITION"

    invoke-static {p0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_15

    const p0, 0x7f11028e

    goto/16 :goto_0

    :cond_15
    const-string v1, "android.permission.ACCESS_MEDIA_LOCATION"

    invoke-static {p0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_16

    const p0, 0x7f11028c

    goto/16 :goto_0

    :cond_16
    const-string v1, "android.permission.ACCESS_FINE_LOCATION"

    invoke-static {p0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_17

    const p0, 0x7f11028b

    goto/16 :goto_0

    :cond_17
    const-string v1, "android.permission.ACCESS_COARSE_LOCATION"

    invoke-static {p0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_18

    const p0, 0x7f11028a

    goto/16 :goto_0

    :cond_18
    const-string v1, "android.permission.ACCESS_BACKGROUND_LOCATION"

    invoke-static {p0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_19

    const p0, 0x7f110289

    goto/16 :goto_0

    :cond_19
    const-string v1, "android.permission.GET_INSTALLED_APPS"

    invoke-static {p0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1a

    const p0, 0x7f110293

    goto/16 :goto_0

    :cond_1a
    const-string v1, "android.permission.PERMLAB_BLUETOOTH_SCAN"

    invoke-static {p0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1b

    const p0, 0x7f110296

    goto/16 :goto_0

    :cond_1b
    const-string v1, "android.permission.ACCESS_NOTIFICATIONS"

    invoke-static {p0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1c

    const p0, 0x7f11028d

    goto/16 :goto_0

    :cond_1c
    const/4 p0, 0x0

    return-object p0
.end method

.method public static final c(Ljava/lang/String;)Ljava/lang/String;
    .locals 9

    const-string v0, "str"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/android/packageinstaller/InstallerApplication;->g()Lcom/android/packageinstaller/InstallerApplication;

    move-result-object v0

    const v1, 0x7f1100dc

    invoke-virtual {v0, v1}, Landroid/app/Application;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v2, "getInstance().getString(R.string.device_phone)"

    invoke-static {v0, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x0

    const/4 v4, 0x2

    const/4 v5, 0x0

    invoke-static {p0, v0, v3, v4, v5}, Lu8/g;->A(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZILjava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "replace\uff1a  "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v3, "SafeModePermissionUtils"

    invoke-static {v3, v0}, Lf6/o;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    invoke-static {}, Lcom/android/packageinstaller/InstallerApplication;->g()Lcom/android/packageinstaller/InstallerApplication;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/Application;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Lcom/android/packageinstaller/InstallerApplication;->g()Lcom/android/packageinstaller/InstallerApplication;

    move-result-object v0

    const v1, 0x7f1100db

    invoke-virtual {v0, v1}, Landroid/app/Application;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v0, "getInstance().getString(R.string.device_pad)"

    invoke-static {v5, v0}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v6, 0x0

    const/4 v7, 0x4

    const/4 v8, 0x0

    move-object v3, p0

    invoke-static/range {v3 .. v8}, Lu8/g;->r(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    :cond_1
    :goto_0
    return-object p0
.end method
