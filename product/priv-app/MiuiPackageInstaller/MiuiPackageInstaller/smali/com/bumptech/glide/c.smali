.class public final Lcom/bumptech/glide/c;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bumptech/glide/c$c;,
        Lcom/bumptech/glide/c$b;,
        Lcom/bumptech/glide/c$d;
    }
.end annotation


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/bumptech/glide/l<",
            "**>;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/bumptech/glide/e$a;

.field private c:Lw2/k;

.field private d:Lx2/d;

.field private e:Lx2/b;

.field private f:Ly2/h;

.field private g:Lz2/a;

.field private h:Lz2/a;

.field private i:Ly2/a$a;

.field private j:Ly2/i;

.field private k:Lj3/d;

.field private l:I

.field private m:Lcom/bumptech/glide/b$a;

.field private n:Lj3/p$b;

.field private o:Lz2/a;

.field private p:Z

.field private q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lm3/e<",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ln/a;

    invoke-direct {v0}, Ln/a;-><init>()V

    iput-object v0, p0, Lcom/bumptech/glide/c;->a:Ljava/util/Map;

    new-instance v0, Lcom/bumptech/glide/e$a;

    invoke-direct {v0}, Lcom/bumptech/glide/e$a;-><init>()V

    iput-object v0, p0, Lcom/bumptech/glide/c;->b:Lcom/bumptech/glide/e$a;

    const/4 v0, 0x4

    iput v0, p0, Lcom/bumptech/glide/c;->l:I

    new-instance v0, Lcom/bumptech/glide/c$a;

    invoke-direct {v0, p0}, Lcom/bumptech/glide/c$a;-><init>(Lcom/bumptech/glide/c;)V

    iput-object v0, p0, Lcom/bumptech/glide/c;->m:Lcom/bumptech/glide/b$a;

    return-void
.end method


# virtual methods
.method a(Landroid/content/Context;)Lcom/bumptech/glide/b;
    .locals 14

    iget-object v0, p0, Lcom/bumptech/glide/c;->g:Lz2/a;

    if-nez v0, :cond_0

    invoke-static {}, Lz2/a;->g()Lz2/a;

    move-result-object v0

    iput-object v0, p0, Lcom/bumptech/glide/c;->g:Lz2/a;

    :cond_0
    iget-object v0, p0, Lcom/bumptech/glide/c;->h:Lz2/a;

    if-nez v0, :cond_1

    invoke-static {}, Lz2/a;->e()Lz2/a;

    move-result-object v0

    iput-object v0, p0, Lcom/bumptech/glide/c;->h:Lz2/a;

    :cond_1
    iget-object v0, p0, Lcom/bumptech/glide/c;->o:Lz2/a;

    if-nez v0, :cond_2

    invoke-static {}, Lz2/a;->c()Lz2/a;

    move-result-object v0

    iput-object v0, p0, Lcom/bumptech/glide/c;->o:Lz2/a;

    :cond_2
    iget-object v0, p0, Lcom/bumptech/glide/c;->j:Ly2/i;

    if-nez v0, :cond_3

    new-instance v0, Ly2/i$a;

    invoke-direct {v0, p1}, Ly2/i$a;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Ly2/i$a;->a()Ly2/i;

    move-result-object v0

    iput-object v0, p0, Lcom/bumptech/glide/c;->j:Ly2/i;

    :cond_3
    iget-object v0, p0, Lcom/bumptech/glide/c;->k:Lj3/d;

    if-nez v0, :cond_4

    new-instance v0, Lj3/f;

    invoke-direct {v0}, Lj3/f;-><init>()V

    iput-object v0, p0, Lcom/bumptech/glide/c;->k:Lj3/d;

    :cond_4
    iget-object v0, p0, Lcom/bumptech/glide/c;->d:Lx2/d;

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/bumptech/glide/c;->j:Ly2/i;

    invoke-virtual {v0}, Ly2/i;->b()I

    move-result v0

    if-lez v0, :cond_5

    new-instance v1, Lx2/j;

    int-to-long v2, v0

    invoke-direct {v1, v2, v3}, Lx2/j;-><init>(J)V

    iput-object v1, p0, Lcom/bumptech/glide/c;->d:Lx2/d;

    goto :goto_0

    :cond_5
    new-instance v0, Lx2/e;

    invoke-direct {v0}, Lx2/e;-><init>()V

    iput-object v0, p0, Lcom/bumptech/glide/c;->d:Lx2/d;

    :cond_6
    :goto_0
    iget-object v0, p0, Lcom/bumptech/glide/c;->e:Lx2/b;

    if-nez v0, :cond_7

    new-instance v0, Lx2/i;

    iget-object v1, p0, Lcom/bumptech/glide/c;->j:Ly2/i;

    invoke-virtual {v1}, Ly2/i;->a()I

    move-result v1

    invoke-direct {v0, v1}, Lx2/i;-><init>(I)V

    iput-object v0, p0, Lcom/bumptech/glide/c;->e:Lx2/b;

    :cond_7
    iget-object v0, p0, Lcom/bumptech/glide/c;->f:Ly2/h;

    if-nez v0, :cond_8

    new-instance v0, Ly2/g;

    iget-object v1, p0, Lcom/bumptech/glide/c;->j:Ly2/i;

    invoke-virtual {v1}, Ly2/i;->d()I

    move-result v1

    int-to-long v1, v1

    invoke-direct {v0, v1, v2}, Ly2/g;-><init>(J)V

    iput-object v0, p0, Lcom/bumptech/glide/c;->f:Ly2/h;

    :cond_8
    iget-object v0, p0, Lcom/bumptech/glide/c;->i:Ly2/a$a;

    if-nez v0, :cond_9

    new-instance v0, Ly2/f;

    invoke-direct {v0, p1}, Ly2/f;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/bumptech/glide/c;->i:Ly2/a$a;

    :cond_9
    iget-object v0, p0, Lcom/bumptech/glide/c;->c:Lw2/k;

    if-nez v0, :cond_a

    new-instance v0, Lw2/k;

    iget-object v2, p0, Lcom/bumptech/glide/c;->f:Ly2/h;

    iget-object v3, p0, Lcom/bumptech/glide/c;->i:Ly2/a$a;

    iget-object v4, p0, Lcom/bumptech/glide/c;->h:Lz2/a;

    iget-object v5, p0, Lcom/bumptech/glide/c;->g:Lz2/a;

    invoke-static {}, Lz2/a;->h()Lz2/a;

    move-result-object v6

    iget-object v7, p0, Lcom/bumptech/glide/c;->o:Lz2/a;

    iget-boolean v8, p0, Lcom/bumptech/glide/c;->p:Z

    move-object v1, v0

    invoke-direct/range {v1 .. v8}, Lw2/k;-><init>(Ly2/h;Ly2/a$a;Lz2/a;Lz2/a;Lz2/a;Lz2/a;Z)V

    iput-object v0, p0, Lcom/bumptech/glide/c;->c:Lw2/k;

    :cond_a
    iget-object v0, p0, Lcom/bumptech/glide/c;->q:Ljava/util/List;

    if-nez v0, :cond_b

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_1

    :cond_b
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lcom/bumptech/glide/c;->q:Ljava/util/List;

    iget-object v0, p0, Lcom/bumptech/glide/c;->b:Lcom/bumptech/glide/e$a;

    invoke-virtual {v0}, Lcom/bumptech/glide/e$a;->b()Lcom/bumptech/glide/e;

    move-result-object v13

    new-instance v7, Lj3/p;

    iget-object v0, p0, Lcom/bumptech/glide/c;->n:Lj3/p$b;

    invoke-direct {v7, v0, v13}, Lj3/p;-><init>(Lj3/p$b;Lcom/bumptech/glide/e;)V

    new-instance v0, Lcom/bumptech/glide/b;

    iget-object v3, p0, Lcom/bumptech/glide/c;->c:Lw2/k;

    iget-object v4, p0, Lcom/bumptech/glide/c;->f:Ly2/h;

    iget-object v5, p0, Lcom/bumptech/glide/c;->d:Lx2/d;

    iget-object v6, p0, Lcom/bumptech/glide/c;->e:Lx2/b;

    iget-object v8, p0, Lcom/bumptech/glide/c;->k:Lj3/d;

    iget v9, p0, Lcom/bumptech/glide/c;->l:I

    iget-object v10, p0, Lcom/bumptech/glide/c;->m:Lcom/bumptech/glide/b$a;

    iget-object v11, p0, Lcom/bumptech/glide/c;->a:Ljava/util/Map;

    iget-object v12, p0, Lcom/bumptech/glide/c;->q:Ljava/util/List;

    move-object v1, v0

    move-object v2, p1

    invoke-direct/range {v1 .. v13}, Lcom/bumptech/glide/b;-><init>(Landroid/content/Context;Lw2/k;Ly2/h;Lx2/d;Lx2/b;Lj3/p;Lj3/d;ILcom/bumptech/glide/b$a;Ljava/util/Map;Ljava/util/List;Lcom/bumptech/glide/e;)V

    return-object v0
.end method

.method b(Lj3/p$b;)V
    .locals 0

    iput-object p1, p0, Lcom/bumptech/glide/c;->n:Lj3/p$b;

    return-void
.end method
