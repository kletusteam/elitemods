.class public final Lcom/xiaomi/market/h52native/view/downloadbutton/TextProgressBar;
.super Landroid/widget/ProgressBar;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/xiaomi/market/h52native/view/downloadbutton/TextProgressBar$a;
    }
.end annotation


# static fields
.field public static final j:Lcom/xiaomi/market/h52native/view/downloadbutton/TextProgressBar$a;


# instance fields
.field private a:Ljava/lang/String;

.field private final b:I

.field private c:Landroid/graphics/Paint;

.field private d:F

.field private e:I

.field private f:I

.field private g:F

.field private h:F

.field private i:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/xiaomi/market/h52native/view/downloadbutton/TextProgressBar$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/xiaomi/market/h52native/view/downloadbutton/TextProgressBar$a;-><init>(Lm8/g;)V

    sput-object v0, Lcom/xiaomi/market/h52native/view/downloadbutton/TextProgressBar;->j:Lcom/xiaomi/market/h52native/view/downloadbutton/TextProgressBar$a;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7

    const-string v0, "context"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v1 .. v6}, Lcom/xiaomi/market/h52native/view/downloadbutton/TextProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILm8/g;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const-string p1, "0.00%"

    iput-object p1, p0, Lcom/xiaomi/market/h52native/view/downloadbutton/TextProgressBar;->a:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/widget/ProgressBar;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p2, 0x7f070539

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, p0, Lcom/xiaomi/market/h52native/view/downloadbutton/TextProgressBar;->b:I

    new-instance p2, Landroid/graphics/Paint;

    const/4 p3, 0x1

    invoke-direct {p2, p3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object p2, p0, Lcom/xiaomi/market/h52native/view/downloadbutton/TextProgressBar;->c:Landroid/graphics/Paint;

    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/xiaomi/market/h52native/view/downloadbutton/TextProgressBar;->h:F

    invoke-virtual {p2, p3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object p2, p0, Lcom/xiaomi/market/h52native/view/downloadbutton/TextProgressBar;->c:Landroid/graphics/Paint;

    iget p3, p0, Lcom/xiaomi/market/h52native/view/downloadbutton/TextProgressBar;->e:I

    invoke-virtual {p2, p3}, Landroid/graphics/Paint;->setColor(I)V

    iget-object p2, p0, Lcom/xiaomi/market/h52native/view/downloadbutton/TextProgressBar;->c:Landroid/graphics/Paint;

    int-to-float p1, p1

    invoke-virtual {p2, p1}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object p1, p0, Lcom/xiaomi/market/h52native/view/downloadbutton/TextProgressBar;->c:Landroid/graphics/Paint;

    sget-object p2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object p1, p0, Lcom/xiaomi/market/h52native/view/downloadbutton/TextProgressBar;->c:Landroid/graphics/Paint;

    sget-object p2, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILm8/g;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/xiaomi/market/h52native/view/downloadbutton/TextProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method private final a(Landroid/graphics/Canvas;)V
    .locals 16

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/xiaomi/market/h52native/view/downloadbutton/TextProgressBar;->c:Landroid/graphics/Paint;

    iget-object v2, v0, Lcom/xiaomi/market/h52native/view/downloadbutton/TextProgressBar;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v1

    iget-object v2, v0, Lcom/xiaomi/market/h52native/view/downloadbutton/TextProgressBar;->c:Landroid/graphics/Paint;

    invoke-virtual {v2}, Landroid/graphics/Paint;->ascent()F

    move-result v2

    iget-object v3, v0, Lcom/xiaomi/market/h52native/view/downloadbutton/TextProgressBar;->c:Landroid/graphics/Paint;

    invoke-virtual {v3}, Landroid/graphics/Paint;->descent()F

    move-result v3

    iget-object v4, v0, Lcom/xiaomi/market/h52native/view/downloadbutton/TextProgressBar;->c:Landroid/graphics/Paint;

    invoke-virtual {v4}, Landroid/graphics/Paint;->getTextSize()F

    move-result v4

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v5

    int-to-float v5, v5

    const/4 v6, 0x2

    int-to-float v7, v6

    div-float/2addr v5, v7

    div-float/2addr v3, v7

    div-float/2addr v2, v7

    add-float/2addr v3, v2

    sub-float/2addr v5, v3

    iget-object v2, v0, Lcom/xiaomi/market/h52native/view/downloadbutton/TextProgressBar;->c:Landroid/graphics/Paint;

    invoke-virtual {v2}, Landroid/graphics/Paint;->getTextLocale()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Ljava/util/Locale;->CHINESE:Ljava/util/Locale;

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v2, 0x3c

    int-to-float v2, v2

    div-float/2addr v4, v2

    sub-float/2addr v5, v4

    :cond_0
    iput v1, v0, Lcom/xiaomi/market/h52native/view/downloadbutton/TextProgressBar;->g:F

    iget v1, v0, Lcom/xiaomi/market/h52native/view/downloadbutton/TextProgressBar;->h:F

    const/4 v2, 0x0

    cmpg-float v1, v1, v2

    if-gez v1, :cond_1

    iput v5, v0, Lcom/xiaomi/market/h52native/view/downloadbutton/TextProgressBar;->h:F

    :cond_1
    invoke-virtual/range {p0 .. p0}, Landroid/widget/ProgressBar;->getMeasuredWidth()I

    move-result v1

    int-to-float v1, v1

    iget v2, v0, Lcom/xiaomi/market/h52native/view/downloadbutton/TextProgressBar;->d:F

    mul-float/2addr v2, v1

    const/high16 v3, 0x42c80000    # 100.0f

    div-float/2addr v2, v3

    iget v3, v0, Lcom/xiaomi/market/h52native/view/downloadbutton/TextProgressBar;->g:F

    sub-float v4, v1, v3

    div-float/2addr v4, v7

    add-float/2addr v1, v3

    div-float/2addr v1, v7

    sub-float v5, v2, v4

    div-float/2addr v5, v3

    cmpg-float v3, v2, v4

    const/4 v4, 0x0

    if-gtz v3, :cond_2

    iget-object v1, v0, Lcom/xiaomi/market/h52native/view/downloadbutton/TextProgressBar;->c:Landroid/graphics/Paint;

    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    iget-object v1, v0, Lcom/xiaomi/market/h52native/view/downloadbutton/TextProgressBar;->c:Landroid/graphics/Paint;

    iget v2, v0, Lcom/xiaomi/market/h52native/view/downloadbutton/TextProgressBar;->e:I

    :goto_0
    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_1

    :cond_2
    cmpg-float v1, v2, v1

    if-gtz v1, :cond_3

    new-instance v1, Landroid/graphics/LinearGradient;

    invoke-virtual/range {p0 .. p0}, Landroid/widget/ProgressBar;->getMeasuredWidth()I

    move-result v2

    int-to-float v2, v2

    iget v3, v0, Lcom/xiaomi/market/h52native/view/downloadbutton/TextProgressBar;->g:F

    sub-float/2addr v2, v3

    div-float v9, v2, v7

    const/4 v10, 0x0

    invoke-virtual/range {p0 .. p0}, Landroid/widget/ProgressBar;->getMeasuredWidth()I

    move-result v2

    int-to-float v2, v2

    iget v3, v0, Lcom/xiaomi/market/h52native/view/downloadbutton/TextProgressBar;->g:F

    add-float/2addr v2, v3

    div-float v11, v2, v7

    const/4 v12, 0x0

    new-array v13, v6, [I

    iget v2, v0, Lcom/xiaomi/market/h52native/view/downloadbutton/TextProgressBar;->f:I

    const/4 v3, 0x0

    aput v2, v13, v3

    iget v2, v0, Lcom/xiaomi/market/h52native/view/downloadbutton/TextProgressBar;->e:I

    const/4 v4, 0x1

    aput v2, v13, v4

    new-array v14, v6, [F

    aput v5, v14, v3

    const v2, 0x3a83126f    # 0.001f

    add-float/2addr v5, v2

    aput v5, v14, v4

    sget-object v15, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    move-object v8, v1

    invoke-direct/range {v8 .. v15}, Landroid/graphics/LinearGradient;-><init>(FFFF[I[FLandroid/graphics/Shader$TileMode;)V

    iget-object v2, v0, Lcom/xiaomi/market/h52native/view/downloadbutton/TextProgressBar;->c:Landroid/graphics/Paint;

    iget v3, v0, Lcom/xiaomi/market/h52native/view/downloadbutton/TextProgressBar;->e:I

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v2, v0, Lcom/xiaomi/market/h52native/view/downloadbutton/TextProgressBar;->c:Landroid/graphics/Paint;

    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    goto :goto_1

    :cond_3
    iget-object v1, v0, Lcom/xiaomi/market/h52native/view/downloadbutton/TextProgressBar;->c:Landroid/graphics/Paint;

    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    iget-object v1, v0, Lcom/xiaomi/market/h52native/view/downloadbutton/TextProgressBar;->c:Landroid/graphics/Paint;

    iget v2, v0, Lcom/xiaomi/market/h52native/view/downloadbutton/TextProgressBar;->f:I

    goto :goto_0

    :goto_1
    iget-object v1, v0, Lcom/xiaomi/market/h52native/view/downloadbutton/TextProgressBar;->a:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Landroid/widget/ProgressBar;->getMeasuredWidth()I

    move-result v2

    int-to-float v2, v2

    iget v3, v0, Lcom/xiaomi/market/h52native/view/downloadbutton/TextProgressBar;->g:F

    sub-float/2addr v2, v3

    div-float/2addr v2, v7

    iget v3, v0, Lcom/xiaomi/market/h52native/view/downloadbutton/TextProgressBar;->h:F

    iget-object v4, v0, Lcom/xiaomi/market/h52native/view/downloadbutton/TextProgressBar;->c:Landroid/graphics/Paint;

    move-object/from16 v5, p1

    invoke-virtual {v5, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    const-string v0, "canvas"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Landroid/widget/ProgressBar;->onDraw(Landroid/graphics/Canvas;)V

    invoke-direct {p0, p1}, Lcom/xiaomi/market/h52native/view/downloadbutton/TextProgressBar;->a(Landroid/graphics/Canvas;)V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    invoke-super {p0}, Landroid/widget/ProgressBar;->onFinishInflate()V

    invoke-virtual {p0}, Landroid/widget/ProgressBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f06004a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/xiaomi/market/h52native/view/downloadbutton/TextProgressBar;->e:I

    invoke-virtual {p0}, Landroid/widget/ProgressBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f060513

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/xiaomi/market/h52native/view/downloadbutton/TextProgressBar;->f:I

    return-void
.end method

.method public final declared-synchronized setProgress(F)V
    .locals 5

    monitor-enter p0

    :try_start_0
    const-string v0, "TextProgressBar"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " progress : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput p1, p0, Lcom/xiaomi/market/h52native/view/downloadbutton/TextProgressBar;->d:F

    iget-wide v0, p0, Lcom/xiaomi/market/h52native/view/downloadbutton/TextProgressBar;->i:J

    long-to-float v0, v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x64

    int-to-float v1, v1

    div-float v1, p1, v1

    iget-wide v2, p0, Lcom/xiaomi/market/h52native/view/downloadbutton/TextProgressBar;->i:J

    long-to-float v2, v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "MB/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/xiaomi/market/h52native/view/downloadbutton/TextProgressBar;->i:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, "MB"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    sget-object v0, Lm8/w;->a:Lm8/w;

    const-string v0, "%.1f%%"

    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v2, v1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "format(format, *args)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    iput-object v0, p0, Lcom/xiaomi/market/h52native/view/downloadbutton/TextProgressBar;->a:Ljava/lang/String;

    float-to-int p1, p1

    invoke-super {p0, p1}, Landroid/widget/ProgressBar;->setProgress(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final setSize(J)V
    .locals 2

    const/16 v0, 0x400

    int-to-long v0, v0

    div-long/2addr p1, v0

    div-long/2addr p1, v0

    iput-wide p1, p0, Lcom/xiaomi/market/h52native/view/downloadbutton/TextProgressBar;->i:J

    return-void
.end method

.method public final setText(Ljava/lang/String;)V
    .locals 1

    const-string v0, "text"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/xiaomi/market/h52native/view/downloadbutton/TextProgressBar;->a:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/widget/ProgressBar;->invalidate()V

    return-void
.end method
