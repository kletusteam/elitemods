.class public abstract Lcom/xiaomi/analytics/Action;
.super Ljava/lang/Object;


# static fields
.field protected static final ACTION:Ljava/lang/String; = "_action_"

.field protected static final CATEGORY:Ljava/lang/String; = "_category_"

.field protected static final EVENT_ID:Ljava/lang/String; = "_event_id_"

.field protected static final LABEL:Ljava/lang/String; = "_label_"

.field protected static final VALUE:Ljava/lang/String; = "_value_"

.field private static c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Lorg/json/JSONObject;

.field private b:Lorg/json/JSONObject;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/xiaomi/analytics/Action;->c:Ljava/util/Set;

    const-string v1, "_event_id_"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/xiaomi/analytics/Action;->c:Ljava/util/Set;

    const-string v1, "_category_"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/xiaomi/analytics/Action;->c:Ljava/util/Set;

    const-string v1, "_action_"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/xiaomi/analytics/Action;->c:Ljava/util/Set;

    const-string v1, "_label_"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/xiaomi/analytics/Action;->c:Ljava/util/Set;

    const-string v1, "_value_"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/analytics/Action;->a:Lorg/json/JSONObject;

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    iput-object v0, p0, Lcom/xiaomi/analytics/Action;->b:Lorg/json/JSONObject;

    return-void
.end method

.method private f(Ljava/lang/String;)V
    .locals 3

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/xiaomi/analytics/Action;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "this key "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " is built-in, please pick another key."

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    :goto_0
    return-void
.end method


# virtual methods
.method a(Ljava/lang/String;I)V
    .locals 1

    goto/32 :goto_8

    nop

    :goto_0
    const-string p2, "Action"

    goto/32 :goto_6

    nop

    :goto_1
    const-string v0, "addContent int value e"

    goto/32 :goto_4

    nop

    :goto_2
    goto :goto_5

    :catch_0
    move-exception p1

    goto/32 :goto_0

    nop

    :goto_3
    return-void

    :goto_4
    invoke-static {p2, v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_5
    goto/32 :goto_3

    nop

    :goto_6
    invoke-static {p2}, Li7/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    goto/32 :goto_1

    nop

    :goto_7
    if-eqz v0, :cond_0

    goto/32 :goto_5

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/analytics/Action;->a:Lorg/json/JSONObject;

    invoke-virtual {v0, p1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_2

    nop

    :goto_8
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    goto/32 :goto_7

    nop
.end method

.method protected addEventId(Ljava/lang/String;)Lcom/xiaomi/analytics/Action;
    .locals 1

    const-string v0, "_event_id_"

    invoke-virtual {p0, v0, p1}, Lcom/xiaomi/analytics/Action;->c(Ljava/lang/String;Ljava/lang/Object;)V

    return-object p0
.end method

.method public addParam(Ljava/lang/String;I)Lcom/xiaomi/analytics/Action;
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/analytics/Action;->f(Ljava/lang/String;)V

    invoke-virtual {p0, p1, p2}, Lcom/xiaomi/analytics/Action;->a(Ljava/lang/String;I)V

    return-object p0
.end method

.method public addParam(Ljava/lang/String;J)Lcom/xiaomi/analytics/Action;
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/analytics/Action;->f(Ljava/lang/String;)V

    invoke-virtual {p0, p1, p2, p3}, Lcom/xiaomi/analytics/Action;->b(Ljava/lang/String;J)V

    return-object p0
.end method

.method public addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/xiaomi/analytics/Action;
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/analytics/Action;->f(Ljava/lang/String;)V

    invoke-virtual {p0, p1, p2}, Lcom/xiaomi/analytics/Action;->c(Ljava/lang/String;Ljava/lang/Object;)V

    return-object p0
.end method

.method public addParam(Ljava/lang/String;Lorg/json/JSONObject;)Lcom/xiaomi/analytics/Action;
    .locals 0

    invoke-direct {p0, p1}, Lcom/xiaomi/analytics/Action;->f(Ljava/lang/String;)V

    invoke-virtual {p0, p1, p2}, Lcom/xiaomi/analytics/Action;->c(Ljava/lang/String;Ljava/lang/Object;)V

    return-object p0
.end method

.method b(Ljava/lang/String;J)V
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    const-string p3, "addContent long value e"

    goto/32 :goto_4

    nop

    :goto_1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    goto/32 :goto_6

    nop

    :goto_2
    const-string p2, "Action"

    goto/32 :goto_8

    nop

    :goto_3
    return-void

    :goto_4
    invoke-static {p2, p3, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_5
    goto/32 :goto_3

    nop

    :goto_6
    if-eqz v0, :cond_0

    goto/32 :goto_5

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/analytics/Action;->a:Lorg/json/JSONObject;

    invoke-virtual {v0, p1, p2, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_7

    nop

    :goto_7
    goto :goto_5

    :catch_0
    move-exception p1

    goto/32 :goto_2

    nop

    :goto_8
    invoke-static {p2}, Li7/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    goto/32 :goto_0

    nop
.end method

.method c(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    goto/32 :goto_6

    nop

    :goto_0
    return-void

    :goto_1
    const-string v0, "addContent Object value e"

    goto/32 :goto_3

    nop

    :goto_2
    const-string p2, "Action"

    goto/32 :goto_5

    nop

    :goto_3
    invoke-static {p2, v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_4
    goto/32 :goto_0

    nop

    :goto_5
    invoke-static {p2}, Li7/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    goto/32 :goto_1

    nop

    :goto_6
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    goto/32 :goto_7

    nop

    :goto_7
    if-eqz v0, :cond_0

    goto/32 :goto_4

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/analytics/Action;->a:Lorg/json/JSONObject;

    invoke-virtual {v0, p1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_8

    nop

    :goto_8
    goto :goto_4

    :catch_0
    move-exception p1

    goto/32 :goto_2

    nop
.end method

.method d(Lorg/json/JSONObject;)V
    .locals 4

    goto/32 :goto_9

    nop

    :goto_0
    return-void

    :goto_1
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_7

    nop

    :goto_2
    goto :goto_6

    :catch_0
    move-exception v1

    goto/32 :goto_3

    nop

    :goto_3
    const-string v2, "Action"

    goto/32 :goto_4

    nop

    :goto_4
    invoke-static {v2}, Li7/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_f

    nop

    :goto_5
    invoke-virtual {p1}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v0

    :goto_6
    goto/32 :goto_e

    nop

    :goto_7
    invoke-direct {p0, v1}, Lcom/xiaomi/analytics/Action;->f(Ljava/lang/String;)V

    :try_start_0
    iget-object v2, p0, Lcom/xiaomi/analytics/Action;->a:Lorg/json/JSONObject;

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_2

    nop

    :goto_8
    if-nez v1, :cond_0

    goto/32 :goto_c

    :cond_0
    goto/32 :goto_d

    nop

    :goto_9
    if-nez p1, :cond_1

    goto/32 :goto_c

    :cond_1
    goto/32 :goto_5

    nop

    :goto_a
    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/32 :goto_b

    nop

    :goto_b
    goto :goto_6

    :goto_c
    goto/32 :goto_0

    nop

    :goto_d
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_1

    nop

    :goto_e
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_8

    nop

    :goto_f
    const-string v3, "addContent e"

    goto/32 :goto_a

    nop
.end method

.method e(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/xiaomi/analytics/Action;->b:Lorg/json/JSONObject;

    invoke-virtual {v0, p1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_1

    nop

    :goto_0
    const-string v0, "addExtra e"

    goto/32 :goto_4

    nop

    :goto_1
    goto :goto_5

    :catch_0
    move-exception p1

    goto/32 :goto_2

    nop

    :goto_2
    const-string p2, "Action"

    goto/32 :goto_3

    nop

    :goto_3
    invoke-static {p2}, Li7/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    goto/32 :goto_0

    nop

    :goto_4
    invoke-static {p2, v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_5
    goto/32 :goto_6

    nop

    :goto_6
    return-void
.end method

.method final g()Lorg/json/JSONObject;
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Lcom/xiaomi/analytics/Action;->a:Lorg/json/JSONObject;

    goto/32 :goto_1

    nop

    :goto_1
    return-object v0
.end method

.method final h()Lorg/json/JSONObject;
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return-object v0

    :goto_1
    iget-object v0, p0, Lcom/xiaomi/analytics/Action;->b:Lorg/json/JSONObject;

    goto/32 :goto_0

    nop
.end method
