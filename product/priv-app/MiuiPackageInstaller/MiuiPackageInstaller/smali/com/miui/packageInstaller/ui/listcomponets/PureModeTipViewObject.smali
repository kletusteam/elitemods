.class public final Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject;
.super Lcom/miui/packageInstaller/ui/listcomponets/f0;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/miui/packageInstaller/ui/listcomponets/f0<",
        "Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject$ViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private m:Lcom/miui/packageInstaller/model/PureModeTip;

.field private final n:Lcom/miui/packageInstaller/model/ApkInfo;

.field private o:Lcom/android/packageinstaller/miui/a$a;

.field private p:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/PureModeTip;Lcom/miui/packageInstaller/model/ApkInfo;Ll6/c;Lm6/b;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mData"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mApkInfo"

    invoke-static {p3, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2, p4, p5}, Lcom/miui/packageInstaller/ui/listcomponets/f0;-><init>(Landroid/content/Context;Ljava/lang/Object;Ll6/c;Lm6/b;)V

    iput-object p2, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject;->m:Lcom/miui/packageInstaller/model/PureModeTip;

    iput-object p3, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject;->n:Lcom/miui/packageInstaller/model/ApkInfo;

    invoke-virtual {p3}, Lcom/miui/packageInstaller/model/ApkInfo;->getPackageInfo()Landroid/content/pm/PackageInfo;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/android/packageinstaller/miui/a;->n2(Landroid/content/Context;Landroid/content/pm/PackageInfo;)Lcom/android/packageinstaller/miui/a$a;

    move-result-object p2

    iput-object p2, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject;->o:Lcom/android/packageinstaller/miui/a$a;

    instance-of p2, p1, Lo5/a;

    if-eqz p2, :cond_0

    new-instance p2, Lp5/g;

    check-cast p1, Lo5/a;

    const-string p3, "authority_btn"

    const-string p4, "button"

    invoke-direct {p2, p3, p4, p1}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p2}, Lp5/f;->c()Z

    :cond_0
    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/PureModeTip;Lcom/miui/packageInstaller/model/ApkInfo;Ll6/c;Lm6/b;ILm8/g;)V
    .locals 7

    and-int/lit8 p7, p6, 0x8

    const/4 v0, 0x0

    if-eqz p7, :cond_0

    move-object v5, v0

    goto :goto_0

    :cond_0
    move-object v5, p4

    :goto_0
    and-int/lit8 p4, p6, 0x10

    if-eqz p4, :cond_1

    move-object v6, v0

    goto :goto_1

    :cond_1
    move-object v6, p5

    :goto_1
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v1 .. v6}, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/PureModeTip;Lcom/miui/packageInstaller/model/ApkInfo;Ll6/c;Lm6/b;)V

    return-void
.end method

.method private static final D(Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject;Landroid/view/View;)V
    .locals 2

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Landroid/content/Intent;

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lcom/android/packageinstaller/miui/PermissionInfoActivity;

    invoke-direct {p1, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject;->n:Lcom/miui/packageInstaller/model/ApkInfo;

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/ApkInfo;->getPackageInfo()Landroid/content/pm/PackageInfo;

    move-result-object v0

    const-string v1, "extra_package_info"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject;->n:Lcom/miui/packageInstaller/model/ApkInfo;

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/ApkInfo;->getLabel()Ljava/lang/String;

    move-result-object v0

    const-string v1, "extra_package_name"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object p1

    instance-of p1, p1, Lo5/a;

    if-eqz p1, :cond_0

    new-instance p1, Lp5/b;

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object p0

    const-string v0, "null cannot be cast to non-null type com.miui.packageInstaller.analytics.IPage"

    invoke-static {p0, v0}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p0, Lo5/a;

    const-string v0, "authority_btn"

    const-string v1, "button"

    invoke-direct {p1, v0, v1, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p1}, Lp5/f;->c()Z

    :cond_0
    return-void
.end method

.method public static synthetic z(Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject;->D(Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject;Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public final A()Lcom/miui/packageInstaller/model/PureModeTip;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject;->m:Lcom/miui/packageInstaller/model/PureModeTip;

    return-object v0
.end method

.method public final B(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject;->p:Z

    return-void
.end method

.method public C(Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject$ViewHolder;)V
    .locals 12

    const-string v0, "viewHolder"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/miui/packageInstaller/ui/listcomponets/f0;->o(Landroidx/recyclerview/widget/RecyclerView$d0;)V

    iget-boolean v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject;->p:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v0

    instance-of v0, v0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v0

    const-string v1, "null cannot be cast to non-null type com.miui.packageInstaller.PurePackageInstallerActivity"

    invoke-static {v0, v1}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject$ViewHolder;->getOnceMessage()Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->J2(Landroid/widget/TextView;)V

    :cond_0
    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject;->m:Lcom/miui/packageInstaller/model/PureModeTip;

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/PureModeTip;->getLevel()I

    move-result v0

    const/4 v1, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-ne v0, v2, :cond_1

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject$ViewHolder;->getTipIcon()Landroid/widget/ImageView;

    move-result-object v0

    const v4, 0x7f080569

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject$ViewHolder;->getPermissionIcon()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject$ViewHolder;->getTipTItle()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f060070

    invoke-virtual {v4, v5}, Landroid/content/Context;->getColor(I)I

    move-result v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject$ViewHolder;->getTipTItle()Landroid/widget/TextView;

    move-result-object v0

    iget-object v4, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject;->m:Lcom/miui/packageInstaller/model/PureModeTip;

    invoke-virtual {v4}, Lcom/miui/packageInstaller/model/PureModeTip;->getTitle()Ljava/lang/String;

    move-result-object v4

    :goto_0
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject$ViewHolder;->getTipMessage()Landroid/widget/TextView;

    move-result-object v0

    iget-object v4, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject;->m:Lcom/miui/packageInstaller/model/PureModeTip;

    invoke-virtual {v4}, Lcom/miui/packageInstaller/model/PureModeTip;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;I)Landroid/text/Spanned;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject$ViewHolder;->getTipMessage()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_2

    :cond_1
    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject;->m:Lcom/miui/packageInstaller/model/PureModeTip;

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/PureModeTip;->getLevel()I

    move-result v0

    const/16 v4, 0x8

    const v5, 0x7f060069

    const v6, 0x7f08056a

    if-ne v0, v1, :cond_2

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject$ViewHolder;->getTipIcon()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject$ViewHolder;->getPermissionIcon()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject$ViewHolder;->getTipTItle()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6, v5}, Landroid/content/Context;->getColor(I)I

    move-result v5

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject$ViewHolder;->getTipTItle()Landroid/widget/TextView;

    move-result-object v0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v6

    const v7, 0x7f1102ca

    new-array v8, v2, [Ljava/lang/Object;

    iget-object v9, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject;->m:Lcom/miui/packageInstaller/model/PureModeTip;

    invoke-virtual {v9}, Lcom/miui/packageInstaller/model/PureModeTip;->getTitle()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v3

    invoke-virtual {v6, v7, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject;->m:Lcom/miui/packageInstaller/model/PureModeTip;

    invoke-virtual {v6}, Lcom/miui/packageInstaller/model/PureModeTip;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    :goto_1
    invoke-static {v5, v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;I)Landroid/text/Spanned;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject$ViewHolder;->getTipMessage()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject;->m:Lcom/miui/packageInstaller/model/PureModeTip;

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/PureModeTip;->getLevel()I

    move-result v0

    const/4 v7, 0x3

    if-ne v0, v7, :cond_3

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject$ViewHolder;->getTipIcon()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject$ViewHolder;->getPermissionIcon()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject$ViewHolder;->getTipTItle()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/content/Context;->getColor(I)I

    move-result v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject$ViewHolder;->getTipTItle()Landroid/widget/TextView;

    move-result-object v0

    iget-object v4, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject;->m:Lcom/miui/packageInstaller/model/PureModeTip;

    invoke-virtual {v4}, Lcom/miui/packageInstaller/model/PureModeTip;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;I)Landroid/text/Spanned;

    move-result-object v4

    goto/16 :goto_0

    :cond_3
    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject;->m:Lcom/miui/packageInstaller/model/PureModeTip;

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/PureModeTip;->getLevel()I

    move-result v0

    const/4 v7, 0x4

    if-ne v0, v7, :cond_4

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject$ViewHolder;->getTipIcon()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject$ViewHolder;->getPermissionIcon()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject$ViewHolder;->getTipTItle()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6, v5}, Landroid/content/Context;->getColor(I)I

    move-result v5

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject$ViewHolder;->getTipTItle()Landroid/widget/TextView;

    move-result-object v0

    iget-object v5, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject;->m:Lcom/miui/packageInstaller/model/PureModeTip;

    invoke-virtual {v5}, Lcom/miui/packageInstaller/model/PureModeTip;->getMessage()Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    :cond_4
    :goto_2
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject$ViewHolder;->getTipMessage()Landroid/widget/TextView;

    move-result-object v0

    new-instance v4, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject$a;

    invoke-direct {v4, p0}, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject$a;-><init>(Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject;)V

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    new-instance v0, Lm8/t;

    invoke-direct {v0}, Lm8/t;-><init>()V

    iget-object v4, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject;->o:Lcom/android/packageinstaller/miui/a$a;

    if-eqz v4, :cond_5

    invoke-virtual {v4}, Lcom/android/packageinstaller/miui/a$a;->e()Ljava/util/Map;

    move-result-object v4

    if-eqz v4, :cond_5

    invoke-interface {v4}, Ljava/util/Map;->size()I

    move-result v4

    if-nez v4, :cond_5

    move v4, v2

    goto :goto_3

    :cond_5
    move v4, v3

    :goto_3
    const/4 v5, 0x0

    if-eqz v4, :cond_6

    const-string v4, ""

    goto :goto_4

    :cond_6
    iget-object v4, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject;->o:Lcom/android/packageinstaller/miui/a$a;

    if-eqz v4, :cond_7

    invoke-virtual {v4}, Lcom/android/packageinstaller/miui/a$a;->e()Ljava/util/Map;

    move-result-object v4

    if-eqz v4, :cond_7

    invoke-interface {v4}, Ljava/util/Map;->size()I

    move-result v4

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0f000b

    new-array v8, v2, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v3

    invoke-virtual {v6, v7, v4, v8}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_4

    :cond_7
    move-object v4, v5

    :goto_4
    iput-object v4, v0, Lm8/t;->a:Ljava/lang/Object;

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f0f0009

    iget-object v7, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject;->o:Lcom/android/packageinstaller/miui/a$a;

    invoke-virtual {v7}, Lcom/android/packageinstaller/miui/a$a;->g()I

    move-result v7

    new-array v8, v2, [Ljava/lang/Object;

    iget-object v9, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject;->o:Lcom/android/packageinstaller/miui/a$a;

    invoke-virtual {v9}, Lcom/android/packageinstaller/miui/a$a;->g()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v3

    invoke-virtual {v4, v6, v7, v8}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const-string v6, "context.resources.getQua\u2026 mPermissionSet.length())"

    invoke-static {v4, v6}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject$ViewHolder;->getPerssionTitle()Landroid/widget/TextView;

    move-result-object v6

    if-nez v6, :cond_8

    goto :goto_5

    :cond_8
    iget-object v7, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject;->o:Lcom/android/packageinstaller/miui/a$a;

    if-eqz v7, :cond_9

    invoke-virtual {v7}, Lcom/android/packageinstaller/miui/a$a;->g()I

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v7, 0x7f110037

    new-array v8, v1, [Ljava/lang/Object;

    aput-object v4, v8, v3

    iget-object v0, v0, Lm8/t;->a:Ljava/lang/Object;

    aput-object v0, v8, v2

    invoke-virtual {v5, v7, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    :cond_9
    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_5
    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject;->o:Lcom/android/packageinstaller/miui/a$a;

    invoke-virtual {v0}, Lcom/android/packageinstaller/miui/a$a;->e()Ljava/util/Map;

    move-result-object v0

    const-string v4, "mPermissionSet.privacy"

    invoke-static {v0, v4}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    xor-int/2addr v0, v2

    if-eqz v0, :cond_e

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject$ViewHolder;->getPermissionLayout()Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject;->o:Lcom/android/packageinstaller/miui/a$a;

    invoke-virtual {v0}, Lcom/android/packageinstaller/miui/a$a;->e()Ljava/util/Map;

    move-result-object v0

    invoke-static {v0, v4}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_a
    :goto_6
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_e

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    const-string v6, "privacyData.key"

    invoke-static {v5, v6}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v5, Ljava/lang/CharSequence;

    invoke-interface {v5}, Ljava/lang/CharSequence;->length()I

    move-result v5

    if-nez v5, :cond_b

    move v5, v2

    goto :goto_7

    :cond_b
    move v5, v3

    :goto_7
    if-nez v5, :cond_a

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    const-string v6, "privacyData.value"

    invoke-static {v5, v6}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v5, Ljava/lang/CharSequence;

    invoke-interface {v5}, Ljava/lang/CharSequence;->length()I

    move-result v5

    if-nez v5, :cond_c

    move v5, v2

    goto :goto_8

    :cond_c
    move v5, v3

    :goto_8
    if-eqz v5, :cond_d

    goto :goto_6

    :cond_d
    new-instance v5, Landroid/widget/TextView;

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    const/high16 v6, 0x41400000    # 12.0f

    invoke-virtual {v5, v1, v6}, Landroid/widget/TextView;->setTextSize(IF)V

    new-instance v6, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v7, -0x2

    invoke-direct {v6, v7, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0700a8

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v7

    iput v7, v6, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    new-instance v7, Landroid/text/SpannableStringBuilder;

    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/CharSequence;

    invoke-direct {v7, v8}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    const-string v8, ": "

    invoke-virtual {v7, v8}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v7

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/CharSequence;

    invoke-virtual {v7, v8}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v7

    new-instance v8, Landroid/text/style/ForegroundColorSpan;

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v9

    const v10, 0x7f060030

    invoke-virtual {v9, v10}, Landroid/content/Context;->getColor(I)I

    move-result v9

    invoke-direct {v8, v9}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v9, v1

    const/16 v10, 0x22

    invoke-virtual {v7, v8, v3, v9, v10}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    new-instance v8, Landroid/text/style/ForegroundColorSpan;

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v9

    const v11, 0x7f06002d

    invoke-virtual {v9, v11}, Landroid/content/Context;->getColor(I)I

    move-result v9

    invoke-direct {v8, v9}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v9, v1

    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    add-int/2addr v11, v1

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v11, v4

    invoke-virtual {v7, v8, v9, v11, v10}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject$ViewHolder;->getPermissionLayout()Landroid/widget/LinearLayout;

    move-result-object v4

    invoke-virtual {v4, v5, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_6

    :cond_e
    new-instance v0, Lcom/miui/packageInstaller/ui/listcomponets/p;

    invoke-direct {v0, p0}, Lcom/miui/packageInstaller/ui/listcomponets/p;-><init>(Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject;)V

    invoke-virtual {p1, v0}, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject$ViewHolder;->setAllOnclickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final E(Lcom/miui/packageInstaller/model/PureModeTip;)V
    .locals 1

    const-string v0, "data"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject;->m:Lcom/miui/packageInstaller/model/PureModeTip;

    return-void
.end method

.method public a()V
    .locals 4

    invoke-super {p0}, Lcom/miui/packageInstaller/ui/listcomponets/f0;->a()V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject;->m:Lcom/miui/packageInstaller/model/PureModeTip;

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/PureModeTip;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/4 v1, 0x1

    if-lez v0, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject;->m:Lcom/miui/packageInstaller/model/PureModeTip;

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/PureModeTip;->getLevel()I

    move-result v0

    if-ne v0, v1, :cond_1

    new-instance v0, Lp5/g;

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v1

    const-string v2, "null cannot be cast to non-null type com.miui.packageInstaller.analytics.IPage"

    invoke-static {v1, v2}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lo5/a;

    const-string v2, "open_platform_btn"

    const-string v3, "button"

    invoke-direct {v0, v2, v3, v1}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    :cond_1
    return-void
.end method

.method public k()I
    .locals 1

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0d0174

    goto :goto_0

    :cond_0
    const v0, 0x7f0d0173

    :goto_0
    return v0
.end method

.method public bridge synthetic o(Landroidx/recyclerview/widget/RecyclerView$d0;)V
    .locals 0

    check-cast p1, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject$ViewHolder;

    invoke-virtual {p0, p1}, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject;->C(Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject$ViewHolder;)V

    return-void
.end method
