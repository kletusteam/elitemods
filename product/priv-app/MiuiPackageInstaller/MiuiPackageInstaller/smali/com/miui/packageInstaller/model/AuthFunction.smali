.class public final Lcom/miui/packageInstaller/model/AuthFunction;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private installerCloseSafetyModel:Z

.field private installerOpenSafetyModel:Z

.field private installerSingleAuth:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getInstallerCloseSafetyModel()Z
    .locals 1

    iget-boolean v0, p0, Lcom/miui/packageInstaller/model/AuthFunction;->installerCloseSafetyModel:Z

    return v0
.end method

.method public final getInstallerOpenSafetyModel()Z
    .locals 1

    iget-boolean v0, p0, Lcom/miui/packageInstaller/model/AuthFunction;->installerOpenSafetyModel:Z

    return v0
.end method

.method public final getInstallerSingleAuth()Z
    .locals 1

    iget-boolean v0, p0, Lcom/miui/packageInstaller/model/AuthFunction;->installerSingleAuth:Z

    return v0
.end method

.method public final setInstallerCloseSafetyModel(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/miui/packageInstaller/model/AuthFunction;->installerCloseSafetyModel:Z

    return-void
.end method

.method public final setInstallerOpenSafetyModel(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/miui/packageInstaller/model/AuthFunction;->installerOpenSafetyModel:Z

    return-void
.end method

.method public final setInstallerSingleAuth(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/miui/packageInstaller/model/AuthFunction;->installerSingleAuth:Z

    return-void
.end method
