.class public final Lcom/miui/packageInstaller/model/InstallSourceTips;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private button:Ljava/lang/String;

.field private installSourceAuthText:Ljava/lang/String;

.field private type:I

.field private warningText:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/miui/packageInstaller/model/InstallSourceTips;->installSourceAuthText:Ljava/lang/String;

    iput-object v0, p0, Lcom/miui/packageInstaller/model/InstallSourceTips;->warningText:Ljava/lang/String;

    iput-object v0, p0, Lcom/miui/packageInstaller/model/InstallSourceTips;->button:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final getButton()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/model/InstallSourceTips;->button:Ljava/lang/String;

    return-object v0
.end method

.method public final getInstallSourceAuthText()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/model/InstallSourceTips;->installSourceAuthText:Ljava/lang/String;

    return-object v0
.end method

.method public final getType()I
    .locals 1

    iget v0, p0, Lcom/miui/packageInstaller/model/InstallSourceTips;->type:I

    return v0
.end method

.method public final getWarningText()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/model/InstallSourceTips;->warningText:Ljava/lang/String;

    return-object v0
.end method

.method public final setButton(Ljava/lang/String;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/model/InstallSourceTips;->button:Ljava/lang/String;

    return-void
.end method

.method public final setInstallSourceAuthText(Ljava/lang/String;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/model/InstallSourceTips;->installSourceAuthText:Ljava/lang/String;

    return-void
.end method

.method public final setType(I)V
    .locals 0

    iput p1, p0, Lcom/miui/packageInstaller/model/InstallSourceTips;->type:I

    return-void
.end method

.method public final setWarningText(Ljava/lang/String;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/model/InstallSourceTips;->warningText:Ljava/lang/String;

    return-void
.end method
