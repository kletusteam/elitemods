.class public Lcom/miui/packageInstaller/a;
.super Lq2/b;

# interfaces
.implements Ls5/l$c;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/packageInstaller/a$a;
    }
.end annotation


# static fields
.field public static final U:Lcom/miui/packageInstaller/a$a;

.field private static final V:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/ref/WeakReference<",
            "Lcom/miui/packageInstaller/a;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field private A:I

.field private B:I

.field private C:Z

.field private D:Z

.field private E:I

.field private F:Ljava/lang/String;

.field private G:Z

.field private final H:I

.field private I:Ls5/l;

.field private J:Z

.field private K:Z

.field private L:Ljava/lang/String;

.field private M:Ljava/lang/Runnable;

.field private N:Ljava/lang/String;

.field private O:Landroid/view/View;

.field public P:Lj6/b;

.field private Q:Lo5/c;

.field public R:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/miui/packageInstaller/a;",
            ">;"
        }
    .end annotation
.end field

.field private S:I

.field private T:Z

.field private u:Lcom/miui/packageInstaller/model/CloudParams;

.field private v:Lcom/miui/packageInstaller/ui/InstallerActionBar;

.field private w:J

.field private x:Lm5/e;

.field private y:Lcom/miui/packageInstaller/model/ApkInfo;

.field private z:Lcom/miui/packageInstaller/model/Virus;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/miui/packageInstaller/a$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/miui/packageInstaller/a$a;-><init>(Lm8/g;)V

    sput-object v0, Lcom/miui/packageInstaller/a;->U:Lcom/miui/packageInstaller/a$a;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/miui/packageInstaller/a;->V:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lq2/b;-><init>()V

    const/16 v0, -0x3e8

    iput v0, p0, Lcom/miui/packageInstaller/a;->A:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/miui/packageInstaller/a;->E:I

    const-string v0, "null"

    iput-object v0, p0, Lcom/miui/packageInstaller/a;->F:Ljava/lang/String;

    const/16 v1, 0x7bc

    iput v1, p0, Lcom/miui/packageInstaller/a;->H:I

    iput-object v0, p0, Lcom/miui/packageInstaller/a;->L:Ljava/lang/String;

    new-instance v0, Lm5/w;

    invoke-direct {v0, p0}, Lm5/w;-><init>(Lcom/miui/packageInstaller/a;)V

    iput-object v0, p0, Lcom/miui/packageInstaller/a;->M:Ljava/lang/Runnable;

    const-string v0, ""

    iput-object v0, p0, Lcom/miui/packageInstaller/a;->N:Ljava/lang/String;

    new-instance v0, Lo5/c;

    invoke-direct {v0}, Lo5/c;-><init>()V

    iput-object v0, p0, Lcom/miui/packageInstaller/a;->Q:Lo5/c;

    const/16 v0, -0x64

    iput v0, p0, Lcom/miui/packageInstaller/a;->S:I

    return-void
.end method

.method private static final A1(Lcom/miui/packageInstaller/a;Landroid/view/View;)V
    .locals 2

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/miui/packageInstaller/a;->u:Lcom/miui/packageInstaller/model/CloudParams;

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    iget-object v1, p1, Lcom/miui/packageInstaller/model/CloudParams;->doneButtonTip:Lcom/miui/packageInstaller/model/DoneButtonTip;

    if-eqz v1, :cond_0

    iget-object v1, v1, Lcom/miui/packageInstaller/model/DoneButtonTip;->actionUrl:Ljava/lang/String;

    goto :goto_0

    :cond_0
    move-object v1, v0

    :goto_0
    if-eqz p1, :cond_1

    iget-object p1, p1, Lcom/miui/packageInstaller/model/CloudParams;->doneButtonTip:Lcom/miui/packageInstaller/model/DoneButtonTip;

    if-eqz p1, :cond_1

    iget-object v0, p1, Lcom/miui/packageInstaller/model/DoneButtonTip;->text:Ljava/lang/String;

    :cond_1
    invoke-direct {p0, v1, v0}, Lcom/miui/packageInstaller/a;->E1(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private static final B1(Lcom/miui/packageInstaller/a;Landroid/view/View;)V
    .locals 0

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/miui/packageInstaller/a;->t1()V

    return-void
.end method

.method private static final C1(Lcom/miui/packageInstaller/a;Ls5/l;Landroid/view/View;)V
    .locals 1

    const-string p2, "this$0"

    invoke-static {p0, p2}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "market click mStatus mStatus = "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v0, p0, Lcom/miui/packageInstaller/a;->S:I

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    const-string v0, "InstallProgress"

    invoke-static {v0, p2}, Lf6/o;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    iget p0, p0, Lcom/miui/packageInstaller/a;->S:I

    const/4 p2, 0x5

    if-ne p2, p0, :cond_0

    check-cast p1, Ls5/n;

    invoke-virtual {p1}, Ls5/n;->z()V

    goto :goto_0

    :cond_0
    const/4 p2, 0x7

    if-eq p2, p0, :cond_1

    const/16 p2, 0x9

    if-ne p2, p0, :cond_2

    :cond_1
    check-cast p1, Ls5/n;

    invoke-virtual {p1}, Ls5/n;->A()V

    :cond_2
    :goto_0
    return-void
.end method

.method private final E1(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    :try_start_0
    new-instance p2, Landroid/content/Intent;

    const-string v0, "android.intent.action.VIEW"

    invoke-direct {p2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v0, 0x10000000

    invoke-virtual {p2, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    invoke-virtual {p0, p2}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    invoke-virtual {p0}, Lmiuix/appcompat/app/j;->finish()V

    return-void
.end method

.method private final F1()V
    .locals 3

    new-instance v0, Lp5/g;

    const-string v1, "page_back_btn"

    const-string v2, "button"

    invoke-direct {v0, v1, v2, p0}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    new-instance v0, Lp5/g;

    const-string v1, "setting_btn"

    invoke-direct {v0, v1, v2, p0}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    return-void
.end method

.method private final I1()V
    .locals 4

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.miui.packageinstaller.ACTION_INSTALL_SUCCESS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/miui/packageInstaller/a;->x:Lm5/e;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lm5/e;->j()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_0
    move-object v1, v2

    :goto_0
    const-string v3, "extra_install_source"

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/miui/packageInstaller/a;->y:Lcom/miui/packageInstaller/model/ApkInfo;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/ApkInfo;->getPackageName()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_1
    move-object v1, v2

    :goto_1
    const-string v3, "extra_package_name"

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/miui/packageInstaller/a;->y:Lcom/miui/packageInstaller/model/ApkInfo;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/ApkInfo;->getNewInstall()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    :cond_2
    const-string v1, "extra_newinstall"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v1, "miui.packageinstaller.permission.ACTION_INFO"

    invoke-virtual {p0, v0, v1}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic J0(Lcom/miui/packageInstaller/a;)V
    .locals 0

    invoke-static {p0}, Lcom/miui/packageInstaller/a;->b1(Lcom/miui/packageInstaller/a;)V

    return-void
.end method

.method public static synthetic K0(Lcom/miui/packageInstaller/a;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/miui/packageInstaller/a;->y1(Lcom/miui/packageInstaller/a;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic L0(Lcom/miui/packageInstaller/a;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/miui/packageInstaller/a;->o1(Lcom/miui/packageInstaller/a;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic M0(Lcom/miui/packageInstaller/a;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/miui/packageInstaller/a;->v1(Lcom/miui/packageInstaller/a;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic N0(Lcom/miui/packageInstaller/a;Ls5/l;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/miui/packageInstaller/a;->C1(Lcom/miui/packageInstaller/a;Ls5/l;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic O0(Lcom/miui/packageInstaller/a;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/miui/packageInstaller/a;->u1(Lcom/miui/packageInstaller/a;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic P0(Lcom/miui/packageInstaller/a;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/miui/packageInstaller/a;->A1(Lcom/miui/packageInstaller/a;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic Q0(Lcom/miui/packageInstaller/a;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/miui/packageInstaller/a;->w1(Lcom/miui/packageInstaller/a;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic R0(Lcom/miui/packageInstaller/a;)V
    .locals 0

    invoke-static {p0}, Lcom/miui/packageInstaller/a;->Z0(Lcom/miui/packageInstaller/a;)V

    return-void
.end method

.method public static synthetic S0(Lcom/miui/packageInstaller/a;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/miui/packageInstaller/a;->B1(Lcom/miui/packageInstaller/a;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic T0(Lcom/miui/packageInstaller/a;Lm8/t;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/miui/packageInstaller/a;->x1(Lcom/miui/packageInstaller/a;Lm8/t;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic U0(Lcom/miui/packageInstaller/a;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/miui/packageInstaller/a;->n1(Lcom/miui/packageInstaller/a;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic V0(Lcom/miui/packageInstaller/a;Lm8/t;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/miui/packageInstaller/a;->z1(Lcom/miui/packageInstaller/a;Lm8/t;Landroid/view/View;)V

    return-void
.end method

.method public static final synthetic W0(Lcom/miui/packageInstaller/a;)Lo5/b;
    .locals 0

    iget-object p0, p0, Lq2/b;->q:Lo5/b;

    return-object p0
.end method

.method public static final synthetic X0()Ljava/util/ArrayList;
    .locals 1

    sget-object v0, Lcom/miui/packageInstaller/a;->V:Ljava/util/ArrayList;

    return-object v0
.end method

.method private static final Z0(Lcom/miui/packageInstaller/a;)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Ljava/io/File;

    iget-object p0, p0, Lcom/miui/packageInstaller/a;->y:Lcom/miui/packageInstaller/model/ApkInfo;

    invoke-static {p0}, Lm8/i;->c(Ljava/lang/Object;)V

    invoke-virtual {p0}, Lcom/miui/packageInstaller/model/ApkInfo;->getFileUri()Landroid/net/Uri;

    move-result-object p0

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    return-void
.end method

.method private final a1()V
    .locals 2

    iget-object v0, p0, Lcom/miui/packageInstaller/a;->y:Lcom/miui/packageInstaller/model/ApkInfo;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/miui/packageInstaller/a;->C:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/packageInstaller/a;->y:Lcom/miui/packageInstaller/model/ApkInfo;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/ApkInfo;->getOriginalFilePath()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-static {v0, v1}, Lcom/android/packageinstaller/utils/h;->b(Landroid/content/Context;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method private static final b1(Lcom/miui/packageInstaller/a;)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lmiuix/appcompat/app/j;->finish()V

    return-void
.end method

.method private final k1()I
    .locals 2

    iget v0, p0, Lcom/miui/packageInstaller/a;->B:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0d0077

    goto :goto_0

    :cond_0
    const v0, 0x7f0d0076

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f0d0079

    goto :goto_0

    :cond_2
    const v0, 0x7f0d0078

    :goto_0
    return v0
.end method

.method private static final n1(Lcom/miui/packageInstaller/a;Landroid/view/View;)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/miui/packageInstaller/a;->backIconClick(Landroid/view/View;)V

    return-void
.end method

.method private static final o1(Lcom/miui/packageInstaller/a;Landroid/view/View;)V
    .locals 2

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Lp5/b;

    const-string v0, "setting_btn"

    const-string v1, "button"

    invoke-direct {p1, v0, v1, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p1}, Lp5/f;->c()Z

    new-instance p1, Landroid/content/Intent;

    const-class v0, Lcom/android/packageinstaller/SettingsActivity;

    invoke-direct {p1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/a;->y:Lcom/miui/packageInstaller/model/ApkInfo;

    const-string v1, "apk_info"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/miui/packageInstaller/a;->x:Lm5/e;

    const-string v1, "caller"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-virtual {p0, p1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private static final u1(Lcom/miui/packageInstaller/a;Landroid/view/View;)V
    .locals 0

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/miui/packageInstaller/a;->t1()V

    return-void
.end method

.method private static final v1(Lcom/miui/packageInstaller/a;Landroid/view/View;)V
    .locals 2

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/miui/packageInstaller/a;->u:Lcom/miui/packageInstaller/model/CloudParams;

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    iget-object v1, p1, Lcom/miui/packageInstaller/model/CloudParams;->onlineFailButtonTip:Lcom/miui/packageInstaller/model/OnlineFailButtonTip;

    if-eqz v1, :cond_0

    iget-object v1, v1, Lcom/miui/packageInstaller/model/OnlineFailButtonTip;->actionUrl:Ljava/lang/String;

    goto :goto_0

    :cond_0
    move-object v1, v0

    :goto_0
    if-eqz p1, :cond_1

    iget-object p1, p1, Lcom/miui/packageInstaller/model/CloudParams;->onlineFailButtonTip:Lcom/miui/packageInstaller/model/OnlineFailButtonTip;

    if-eqz p1, :cond_1

    iget-object v0, p1, Lcom/miui/packageInstaller/model/OnlineFailButtonTip;->text:Ljava/lang/String;

    :cond_1
    invoke-direct {p0, v1, v0}, Lcom/miui/packageInstaller/a;->E1(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private static final w1(Lcom/miui/packageInstaller/a;Landroid/view/View;)V
    .locals 0

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/miui/packageInstaller/a;->t1()V

    return-void
.end method

.method private static final x1(Lcom/miui/packageInstaller/a;Lm8/t;Landroid/view/View;)V
    .locals 0

    const-string p2, "this$0"

    invoke-static {p0, p2}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "$launchIntent"

    invoke-static {p1, p2}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p1, Lm8/t;->a:Ljava/lang/Object;

    check-cast p1, Landroid/content/Intent;

    invoke-virtual {p0, p1}, Lcom/miui/packageInstaller/a;->D1(Landroid/content/Intent;)V

    return-void
.end method

.method private static final y1(Lcom/miui/packageInstaller/a;Landroid/view/View;)V
    .locals 0

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/miui/packageInstaller/a;->t1()V

    return-void
.end method

.method private static final z1(Lcom/miui/packageInstaller/a;Lm8/t;Landroid/view/View;)V
    .locals 0

    const-string p2, "this$0"

    invoke-static {p0, p2}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "$launchIntent"

    invoke-static {p1, p2}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p1, Lm8/t;->a:Ljava/lang/Object;

    check-cast p1, Landroid/content/Intent;

    invoke-virtual {p0, p1}, Lcom/miui/packageInstaller/a;->D1(Landroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method public C(Ls5/l;II)V
    .locals 17

    move-object/from16 v8, p0

    move-object/from16 v9, p1

    move/from16 v0, p2

    move/from16 v6, p3

    iget-object v1, v8, Lcom/miui/packageInstaller/a;->O:Landroid/view/View;

    if-nez v1, :cond_0

    const-string v1, "mBackIcon"

    invoke-static {v1}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v1, 0x0

    :cond_0
    const/4 v11, 0x0

    invoke-virtual {v1, v11}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, v8, Lcom/miui/packageInstaller/a;->x:Lm5/e;

    const/4 v12, 0x1

    if-eqz v1, :cond_2

    iget-object v2, v8, Lcom/miui/packageInstaller/a;->y:Lcom/miui/packageInstaller/model/ApkInfo;

    if-eqz v2, :cond_2

    sget-object v2, Lf6/h;->a:Lf6/h;

    invoke-static {v1}, Lm8/i;->c(Ljava/lang/Object;)V

    iget-object v3, v8, Lcom/miui/packageInstaller/a;->y:Lcom/miui/packageInstaller/model/ApkInfo;

    invoke-static {v3}, Lm8/i;->c(Ljava/lang/Object;)V

    if-nez v0, :cond_1

    move v4, v12

    goto :goto_0

    :cond_1
    move v4, v11

    :goto_0
    invoke-virtual {v2, v1, v3, v4}, Lf6/h;->h(Lm5/e;Lcom/miui/packageInstaller/model/ApkInfo;Z)V

    :cond_2
    iput v0, v8, Lcom/miui/packageInstaller/a;->A:I

    iput-boolean v11, v8, Lcom/miui/packageInstaller/a;->D:Z

    iget-object v1, v8, Lcom/miui/packageInstaller/a;->v:Lcom/miui/packageInstaller/ui/InstallerActionBar;

    const/16 v13, 0x8

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/miui/packageInstaller/ui/InstallerActionBar;->getMProgress()Lz5/j;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-interface {v1, v13}, Lz5/j;->setVisibility(I)V

    :cond_3
    iget-object v1, v8, Lcom/miui/packageInstaller/a;->v:Lcom/miui/packageInstaller/ui/InstallerActionBar;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/miui/packageInstaller/ui/InstallerActionBar;->getMDoneLayout()Landroid/view/View;

    move-result-object v1

    goto :goto_1

    :cond_4
    const/4 v1, 0x0

    :goto_1
    if-nez v1, :cond_5

    goto :goto_2

    :cond_5
    invoke-virtual {v1, v11}, Landroid/view/View;->setVisibility(I)V

    :goto_2
    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "android.intent.extra.RETURN_RESULT"

    invoke-virtual {v1, v2, v11}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_7

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "android.intent.extra.INSTALL_RESULT"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    if-nez v0, :cond_6

    const/4 v2, -0x1

    goto :goto_3

    :cond_6
    move v2, v12

    :goto_3
    invoke-virtual {v8, v2, v1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    :cond_7
    const-string v1, "pi"

    const-string v7, "appstore"

    const-string v14, "finish_btn"

    const v15, 0x7f11026a

    const-string v5, "button"

    if-nez v0, :cond_2f

    sget-object v0, Lt5/r;->a:Lt5/r;

    iget-object v2, v8, Lcom/miui/packageInstaller/a;->N:Ljava/lang/String;

    const-string v3, "success"

    invoke-virtual {v0, v2, v3}, Lt5/r;->j(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "install_finish"

    invoke-virtual {v8, v0}, Lq2/b;->I0(Ljava/lang/String;)V

    sget-object v0, Ll2/a;->a:Ll2/a;

    iget v2, v8, Lcom/miui/packageInstaller/a;->B:I

    if-ne v2, v12, :cond_8

    move-object v3, v7

    goto :goto_4

    :cond_8
    move-object v3, v1

    :goto_4
    const/4 v4, 0x0

    iget-object v6, v8, Lcom/miui/packageInstaller/a;->x:Lm5/e;

    const/16 v7, 0x8

    const/16 v16, 0x0

    const-string v2, "install_finish"

    move-object/from16 v1, p0

    move-object v10, v5

    move-object v5, v6

    move v6, v7

    move-object/from16 v7, v16

    invoke-static/range {v0 .. v7}, Ll2/a;->c(Ll2/a;Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lm5/e;ILjava/lang/Object;)V

    const-string v0, "APP_INSTALL_SUCCESS"

    iput-object v0, v8, Lcom/miui/packageInstaller/a;->F:Ljava/lang/String;

    invoke-direct/range {p0 .. p0}, Lcom/miui/packageInstaller/a;->I1()V

    invoke-virtual/range {p0 .. p0}, Lcom/miui/packageInstaller/a;->e1()Lj6/b;

    move-result-object v0

    invoke-virtual {v0}, Lj6/b;->R()Ljava/util/List;

    new-instance v0, Lm8/t;

    invoke-direct {v0}, Lm8/t;-><init>()V

    iget-object v1, v8, Lcom/miui/packageInstaller/a;->y:Lcom/miui/packageInstaller/model/ApkInfo;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/ApkInfo;->getPackageName()Ljava/lang/String;

    move-result-object v1

    goto :goto_5

    :cond_9
    const/4 v1, 0x0

    :goto_5
    invoke-virtual {v8, v1}, Lcom/miui/packageInstaller/a;->s1(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    iput-object v1, v0, Lm8/t;->a:Ljava/lang/Object;

    instance-of v1, v9, Ls5/n;

    const-string v2, "open_app_btn"

    const v3, 0x7f11026b

    if-eqz v1, :cond_17

    iget-object v1, v8, Lcom/miui/packageInstaller/a;->u:Lcom/miui/packageInstaller/model/CloudParams;

    if-eqz v1, :cond_17

    iget v1, v8, Lcom/miui/packageInstaller/a;->H:I

    iput v1, v8, Lcom/miui/packageInstaller/a;->S:I

    iget-object v1, v8, Lcom/miui/packageInstaller/a;->v:Lcom/miui/packageInstaller/ui/InstallerActionBar;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Lcom/miui/packageInstaller/ui/InstallerActionBar;->getMStartButton()Landroid/widget/Button;

    move-result-object v1

    goto :goto_6

    :cond_a
    const/4 v1, 0x0

    :goto_6
    if-nez v1, :cond_b

    goto :goto_7

    :cond_b
    invoke-virtual {v8, v15}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :goto_7
    new-instance v1, Lp5/g;

    invoke-direct {v1, v14, v10, v8}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v1}, Lp5/f;->c()Z

    iget-object v1, v8, Lcom/miui/packageInstaller/a;->v:Lcom/miui/packageInstaller/ui/InstallerActionBar;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Lcom/miui/packageInstaller/ui/InstallerActionBar;->getMStartButton()Landroid/widget/Button;

    move-result-object v1

    if-eqz v1, :cond_c

    new-instance v4, Lm5/c0;

    invoke-direct {v4, v8}, Lm5/c0;-><init>(Lcom/miui/packageInstaller/a;)V

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_c
    iget-object v1, v0, Lm8/t;->a:Ljava/lang/Object;

    if-nez v1, :cond_11

    iget-object v0, v8, Lcom/miui/packageInstaller/a;->v:Lcom/miui/packageInstaller/ui/InstallerActionBar;

    if-eqz v0, :cond_d

    invoke-virtual {v0}, Lcom/miui/packageInstaller/ui/InstallerActionBar;->getMTopButton()Landroid/widget/Button;

    move-result-object v0

    goto :goto_8

    :cond_d
    const/4 v0, 0x0

    :goto_8
    if-nez v0, :cond_e

    goto :goto_9

    :cond_e
    invoke-virtual {v0, v13}, Landroid/widget/Button;->setVisibility(I)V

    :goto_9
    iget-object v0, v8, Lcom/miui/packageInstaller/a;->v:Lcom/miui/packageInstaller/ui/InstallerActionBar;

    if-eqz v0, :cond_f

    invoke-virtual {v0}, Lcom/miui/packageInstaller/ui/InstallerActionBar;->getMEndButton()Landroid/widget/Button;

    move-result-object v10

    goto :goto_a

    :cond_f
    const/4 v10, 0x0

    :goto_a
    if-nez v10, :cond_10

    goto/16 :goto_1f

    :cond_10
    invoke-virtual {v10, v13}, Landroid/widget/Button;->setVisibility(I)V

    goto/16 :goto_1f

    :cond_11
    iget-object v1, v8, Lcom/miui/packageInstaller/a;->v:Lcom/miui/packageInstaller/ui/InstallerActionBar;

    if-eqz v1, :cond_12

    invoke-virtual {v1}, Lcom/miui/packageInstaller/ui/InstallerActionBar;->getMTopButton()Landroid/widget/Button;

    move-result-object v1

    goto :goto_b

    :cond_12
    const/4 v1, 0x0

    :goto_b
    if-nez v1, :cond_13

    goto :goto_c

    :cond_13
    invoke-virtual {v1, v13}, Landroid/widget/Button;->setVisibility(I)V

    :goto_c
    iget-object v1, v8, Lcom/miui/packageInstaller/a;->v:Lcom/miui/packageInstaller/ui/InstallerActionBar;

    if-eqz v1, :cond_14

    invoke-virtual {v1}, Lcom/miui/packageInstaller/ui/InstallerActionBar;->getMEndButton()Landroid/widget/Button;

    move-result-object v1

    goto :goto_d

    :cond_14
    const/4 v1, 0x0

    :goto_d
    if-nez v1, :cond_15

    goto :goto_e

    :cond_15
    invoke-virtual {v1, v11}, Landroid/widget/Button;->setVisibility(I)V

    :goto_e
    iget-object v1, v8, Lcom/miui/packageInstaller/a;->v:Lcom/miui/packageInstaller/ui/InstallerActionBar;

    if-eqz v1, :cond_16

    invoke-virtual {v1}, Lcom/miui/packageInstaller/ui/InstallerActionBar;->getMEndButton()Landroid/widget/Button;

    move-result-object v1

    if-eqz v1, :cond_16

    invoke-virtual {v8, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :cond_16
    new-instance v1, Lp5/g;

    invoke-direct {v1, v2, v10, v8}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v1}, Lp5/f;->c()Z

    iget-object v1, v8, Lcom/miui/packageInstaller/a;->v:Lcom/miui/packageInstaller/ui/InstallerActionBar;

    if-eqz v1, :cond_2e

    invoke-virtual {v1}, Lcom/miui/packageInstaller/ui/InstallerActionBar;->getMEndButton()Landroid/widget/Button;

    move-result-object v1

    if-eqz v1, :cond_2e

    new-instance v2, Lm5/f0;

    invoke-direct {v2, v8, v0}, Lm5/f0;-><init>(Lcom/miui/packageInstaller/a;Lm8/t;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_1f

    :cond_17
    invoke-direct/range {p0 .. p0}, Lcom/miui/packageInstaller/a;->a1()V

    iget-object v1, v8, Lcom/miui/packageInstaller/a;->v:Lcom/miui/packageInstaller/ui/InstallerActionBar;

    if-eqz v1, :cond_18

    invoke-virtual {v1}, Lcom/miui/packageInstaller/ui/InstallerActionBar;->getMStartButton()Landroid/widget/Button;

    move-result-object v1

    goto :goto_f

    :cond_18
    const/4 v1, 0x0

    :goto_f
    if-nez v1, :cond_19

    goto :goto_10

    :cond_19
    invoke-virtual {v8, v15}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :goto_10
    new-instance v1, Lp5/g;

    invoke-direct {v1, v14, v10, v8}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v1}, Lp5/f;->c()Z

    iget-object v1, v8, Lcom/miui/packageInstaller/a;->v:Lcom/miui/packageInstaller/ui/InstallerActionBar;

    if-eqz v1, :cond_1a

    invoke-virtual {v1}, Lcom/miui/packageInstaller/ui/InstallerActionBar;->getMStartButton()Landroid/widget/Button;

    move-result-object v1

    if-eqz v1, :cond_1a

    new-instance v4, Lm5/u;

    invoke-direct {v4, v8}, Lm5/u;-><init>(Lcom/miui/packageInstaller/a;)V

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1a
    iget-object v1, v0, Lm8/t;->a:Ljava/lang/Object;

    if-eqz v1, :cond_22

    iget-object v1, v8, Lcom/miui/packageInstaller/a;->u:Lcom/miui/packageInstaller/model/CloudParams;

    if-eqz v1, :cond_1b

    iget-boolean v4, v1, Lcom/miui/packageInstaller/model/CloudParams;->bundleApp:Z

    if-nez v4, :cond_1b

    move v4, v12

    goto :goto_11

    :cond_1b
    move v4, v11

    :goto_11
    if-eqz v4, :cond_22

    if-eqz v1, :cond_1c

    iget-boolean v4, v1, Lcom/miui/packageInstaller/model/CloudParams;->useSystemAppRules:Z

    if-ne v4, v12, :cond_1c

    move v4, v12

    goto :goto_12

    :cond_1c
    move v4, v11

    :goto_12
    if-nez v4, :cond_1f

    if-eqz v1, :cond_1d

    iget-boolean v4, v1, Lcom/miui/packageInstaller/model/CloudParams;->storeListed:Z

    if-ne v4, v12, :cond_1d

    move v4, v12

    goto :goto_13

    :cond_1d
    move v4, v11

    :goto_13
    if-nez v4, :cond_1f

    if-eqz v1, :cond_1e

    iget-boolean v1, v1, Lcom/miui/packageInstaller/model/CloudParams;->openButton:Z

    if-ne v1, v12, :cond_1e

    move v1, v12

    goto :goto_14

    :cond_1e
    move v1, v11

    :goto_14
    if-eqz v1, :cond_22

    :cond_1f
    iget-object v1, v8, Lcom/miui/packageInstaller/a;->v:Lcom/miui/packageInstaller/ui/InstallerActionBar;

    if-eqz v1, :cond_20

    invoke-virtual {v1}, Lcom/miui/packageInstaller/ui/InstallerActionBar;->getMEndButton()Landroid/widget/Button;

    move-result-object v1

    goto :goto_15

    :cond_20
    const/4 v1, 0x0

    :goto_15
    if-nez v1, :cond_21

    goto :goto_16

    :cond_21
    invoke-virtual {v8, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :goto_16
    new-instance v1, Lp5/g;

    invoke-direct {v1, v2, v10, v8}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v1}, Lp5/f;->c()Z

    iget-object v1, v8, Lcom/miui/packageInstaller/a;->v:Lcom/miui/packageInstaller/ui/InstallerActionBar;

    if-eqz v1, :cond_25

    invoke-virtual {v1}, Lcom/miui/packageInstaller/ui/InstallerActionBar;->getMEndButton()Landroid/widget/Button;

    move-result-object v1

    if-eqz v1, :cond_25

    new-instance v2, Lm5/g0;

    invoke-direct {v2, v8, v0}, Lm5/g0;-><init>(Lcom/miui/packageInstaller/a;Lm8/t;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_18

    :cond_22
    iget-object v0, v8, Lcom/miui/packageInstaller/a;->v:Lcom/miui/packageInstaller/ui/InstallerActionBar;

    if-eqz v0, :cond_23

    invoke-virtual {v0}, Lcom/miui/packageInstaller/ui/InstallerActionBar;->getMEndButton()Landroid/widget/Button;

    move-result-object v0

    goto :goto_17

    :cond_23
    const/4 v0, 0x0

    :goto_17
    if-nez v0, :cond_24

    goto :goto_18

    :cond_24
    invoke-virtual {v0, v13}, Landroid/widget/Button;->setVisibility(I)V

    :cond_25
    :goto_18
    iget-object v0, v8, Lcom/miui/packageInstaller/a;->u:Lcom/miui/packageInstaller/model/CloudParams;

    if-eqz v0, :cond_26

    iget-boolean v1, v0, Lcom/miui/packageInstaller/model/CloudParams;->useSystemAppRules:Z

    if-ne v1, v12, :cond_26

    move v11, v12

    :cond_26
    if-nez v11, :cond_2c

    if-eqz v0, :cond_27

    iget-object v0, v0, Lcom/miui/packageInstaller/model/CloudParams;->doneButtonTip:Lcom/miui/packageInstaller/model/DoneButtonTip;

    if-eqz v0, :cond_27

    iget-object v0, v0, Lcom/miui/packageInstaller/model/DoneButtonTip;->text:Ljava/lang/String;

    goto :goto_19

    :cond_27
    const/4 v0, 0x0

    :goto_19
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2c

    iget-object v0, v8, Lcom/miui/packageInstaller/a;->u:Lcom/miui/packageInstaller/model/CloudParams;

    if-eqz v0, :cond_28

    iget-object v0, v0, Lcom/miui/packageInstaller/model/CloudParams;->doneButtonTip:Lcom/miui/packageInstaller/model/DoneButtonTip;

    if-eqz v0, :cond_28

    iget-object v0, v0, Lcom/miui/packageInstaller/model/DoneButtonTip;->actionUrl:Ljava/lang/String;

    goto :goto_1a

    :cond_28
    const/4 v0, 0x0

    :goto_1a
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2c

    iget-object v0, v8, Lcom/miui/packageInstaller/a;->v:Lcom/miui/packageInstaller/ui/InstallerActionBar;

    if-eqz v0, :cond_29

    invoke-virtual {v0}, Lcom/miui/packageInstaller/ui/InstallerActionBar;->getMTopButton()Landroid/widget/Button;

    move-result-object v0

    goto :goto_1b

    :cond_29
    const/4 v0, 0x0

    :goto_1b
    if-nez v0, :cond_2a

    goto :goto_1d

    :cond_2a
    iget-object v1, v8, Lcom/miui/packageInstaller/a;->u:Lcom/miui/packageInstaller/model/CloudParams;

    if-eqz v1, :cond_2b

    iget-object v1, v1, Lcom/miui/packageInstaller/model/CloudParams;->doneButtonTip:Lcom/miui/packageInstaller/model/DoneButtonTip;

    if-eqz v1, :cond_2b

    iget-object v10, v1, Lcom/miui/packageInstaller/model/DoneButtonTip;->text:Ljava/lang/String;

    goto :goto_1c

    :cond_2b
    const/4 v10, 0x0

    :goto_1c
    invoke-virtual {v0, v10}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :goto_1d
    iget-object v0, v8, Lcom/miui/packageInstaller/a;->v:Lcom/miui/packageInstaller/ui/InstallerActionBar;

    if-eqz v0, :cond_2e

    invoke-virtual {v0}, Lcom/miui/packageInstaller/ui/InstallerActionBar;->getMTopButton()Landroid/widget/Button;

    move-result-object v0

    if-eqz v0, :cond_2e

    new-instance v1, Lm5/b0;

    invoke-direct {v1, v8}, Lm5/b0;-><init>(Lcom/miui/packageInstaller/a;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1f

    :cond_2c
    iget-object v0, v8, Lcom/miui/packageInstaller/a;->v:Lcom/miui/packageInstaller/ui/InstallerActionBar;

    if-eqz v0, :cond_2d

    invoke-virtual {v0}, Lcom/miui/packageInstaller/ui/InstallerActionBar;->getMTopButton()Landroid/widget/Button;

    move-result-object v10

    goto :goto_1e

    :cond_2d
    const/4 v10, 0x0

    :goto_1e
    if-nez v10, :cond_10

    :cond_2e
    :goto_1f
    iget-boolean v0, v8, Lcom/miui/packageInstaller/a;->G:Z

    if-nez v0, :cond_4f

    sget-object v0, Lcom/miui/packageInstaller/a;->V:Ljava/util/ArrayList;

    invoke-virtual/range {p0 .. p0}, Lcom/miui/packageInstaller/a;->d1()Ljava/lang/ref/WeakReference;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    sub-int/2addr v0, v12

    if-eq v1, v0, :cond_4f

    invoke-virtual/range {p0 .. p0}, Lmiuix/appcompat/app/j;->finish()V

    goto/16 :goto_3a

    :cond_2f
    move-object v10, v5

    iput-boolean v12, v8, Lcom/miui/packageInstaller/a;->K:Z

    sget-object v0, Lt5/r;->a:Lt5/r;

    iget-object v2, v8, Lcom/miui/packageInstaller/a;->N:Ljava/lang/String;

    const-string v3, "failed"

    invoke-virtual {v0, v2, v3}, Lt5/r;->j(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "install_fail"

    invoke-virtual {v8, v0}, Lq2/b;->I0(Ljava/lang/String;)V

    sget-object v0, Ll2/a;->a:Ll2/a;

    iget v2, v8, Lcom/miui/packageInstaller/a;->B:I

    if-ne v2, v12, :cond_30

    move-object v3, v7

    goto :goto_20

    :cond_30
    move-object v3, v1

    :goto_20
    invoke-static/range {p3 .. p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, v8, Lcom/miui/packageInstaller/a;->x:Lm5/e;

    const-string v2, "install_fail"

    move-object/from16 v1, p0

    invoke-virtual/range {v0 .. v5}, Ll2/a;->b(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lm5/e;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "APP_INSTALL_FAIL_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v8, Lcom/miui/packageInstaller/a;->F:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/miui/packageInstaller/a;->G1()V

    new-instance v0, Ls5/h;

    instance-of v1, v9, Ls5/n;

    iget-object v2, v8, Lcom/miui/packageInstaller/a;->y:Lcom/miui/packageInstaller/model/ApkInfo;

    if-eqz v2, :cond_31

    invoke-virtual {v2}, Lcom/miui/packageInstaller/model/ApkInfo;->getPackageName()Ljava/lang/String;

    move-result-object v2

    goto :goto_21

    :cond_31
    const/4 v2, 0x0

    :goto_21
    invoke-direct {v0, v8, v1, v2}, Ls5/h;-><init>(Landroid/content/Context;ZLjava/lang/String;)V

    iget-object v1, v8, Lcom/miui/packageInstaller/a;->y:Lcom/miui/packageInstaller/model/ApkInfo;

    if-eqz v1, :cond_32

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/ApkInfo;->getApkAbi()Ljava/lang/String;

    move-result-object v1

    goto :goto_22

    :cond_32
    const/4 v1, 0x0

    :goto_22
    iget-object v2, v8, Lcom/miui/packageInstaller/a;->u:Lcom/miui/packageInstaller/model/CloudParams;

    if-eqz v2, :cond_33

    iget-boolean v2, v2, Lcom/miui/packageInstaller/model/CloudParams;->adaptiveApp:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto :goto_23

    :cond_33
    const/4 v2, 0x0

    :goto_23
    if-eqz v2, :cond_34

    iget-object v2, v8, Lcom/miui/packageInstaller/a;->u:Lcom/miui/packageInstaller/model/CloudParams;

    invoke-static {v2}, Lm8/i;->c(Ljava/lang/Object;)V

    iget-boolean v2, v2, Lcom/miui/packageInstaller/model/CloudParams;->adaptiveApp:Z

    goto :goto_24

    :cond_34
    move v2, v11

    :goto_24
    invoke-virtual {v0, v6, v1, v2}, Ls5/h;->a(ILjava/lang/String;Z)Ls5/h$a;

    move-result-object v0

    invoke-virtual/range {p0 .. p0}, Lcom/miui/packageInstaller/a;->e1()Lj6/b;

    move-result-object v1

    invoke-virtual {v1}, Lj6/b;->R()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_35
    :goto_25
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_36

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lm6/a;

    instance-of v3, v2, Lcom/miui/packageInstaller/ui/listcomponets/v;

    if-eqz v3, :cond_35

    invoke-virtual {v2}, Lm6/a;->t()V

    goto :goto_25

    :cond_36
    invoke-virtual/range {p0 .. p0}, Lcom/miui/packageInstaller/a;->e1()Lj6/b;

    move-result-object v1

    invoke-virtual/range {p0 .. p0}, Lcom/miui/packageInstaller/a;->e1()Lj6/b;

    move-result-object v2

    invoke-virtual {v2}, Lj6/b;->f()I

    move-result v2

    if-le v2, v12, :cond_37

    invoke-virtual/range {p0 .. p0}, Lcom/miui/packageInstaller/a;->e1()Lj6/b;

    move-result-object v2

    invoke-virtual {v2}, Lj6/b;->f()I

    move-result v2

    add-int/lit8 v11, v2, -0x1

    :cond_37
    new-instance v2, Lcom/miui/packageInstaller/ui/listcomponets/InstallFailedViewObject;

    const-string v3, "failReasonModel"

    invoke-static {v0, v3}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x0

    invoke-direct {v2, v8, v0, v3, v3}, Lcom/miui/packageInstaller/ui/listcomponets/InstallFailedViewObject;-><init>(Landroid/content/Context;Ls5/h$a;Ll6/c;Lm6/b;)V

    invoke-virtual {v1, v11, v2}, Lj6/b;->G(ILm6/a;)I

    iget-object v0, v8, Lcom/miui/packageInstaller/a;->I:Ls5/l;

    instance-of v0, v0, Ls5/n;

    if-eqz v0, :cond_3e

    iget-object v0, v8, Lcom/miui/packageInstaller/a;->v:Lcom/miui/packageInstaller/ui/InstallerActionBar;

    if-eqz v0, :cond_38

    invoke-virtual {v0}, Lcom/miui/packageInstaller/ui/InstallerActionBar;->getMTopButton()Landroid/widget/Button;

    move-result-object v0

    goto :goto_26

    :cond_38
    move-object v0, v3

    :goto_26
    if-nez v0, :cond_39

    goto :goto_27

    :cond_39
    invoke-virtual {v0, v13}, Landroid/widget/Button;->setVisibility(I)V

    :goto_27
    iget-object v0, v8, Lcom/miui/packageInstaller/a;->v:Lcom/miui/packageInstaller/ui/InstallerActionBar;

    if-eqz v0, :cond_3a

    invoke-virtual {v0}, Lcom/miui/packageInstaller/ui/InstallerActionBar;->getMEndButton()Landroid/widget/Button;

    move-result-object v0

    goto :goto_28

    :cond_3a
    move-object v0, v3

    :goto_28
    if-nez v0, :cond_3b

    goto :goto_29

    :cond_3b
    invoke-virtual {v0, v13}, Landroid/widget/Button;->setVisibility(I)V

    :goto_29
    iget-object v0, v8, Lcom/miui/packageInstaller/a;->v:Lcom/miui/packageInstaller/ui/InstallerActionBar;

    if-eqz v0, :cond_3c

    invoke-virtual {v0}, Lcom/miui/packageInstaller/ui/InstallerActionBar;->getMStartButton()Landroid/widget/Button;

    move-result-object v0

    goto :goto_2a

    :cond_3c
    move-object v0, v3

    :goto_2a
    if-nez v0, :cond_3d

    goto :goto_2b

    :cond_3d
    invoke-virtual {v8, v15}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :goto_2b
    new-instance v0, Lp5/g;

    invoke-direct {v0, v14, v10, v8}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    iget-object v0, v8, Lcom/miui/packageInstaller/a;->v:Lcom/miui/packageInstaller/ui/InstallerActionBar;

    if-eqz v0, :cond_4e

    invoke-virtual {v0}, Lcom/miui/packageInstaller/ui/InstallerActionBar;->getMStartButton()Landroid/widget/Button;

    move-result-object v0

    if-eqz v0, :cond_4e

    new-instance v1, Lm5/d0;

    invoke-direct {v1, v8}, Lm5/d0;-><init>(Lcom/miui/packageInstaller/a;)V

    :goto_2c
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_39

    :cond_3e
    iget-object v0, v8, Lcom/miui/packageInstaller/a;->v:Lcom/miui/packageInstaller/ui/InstallerActionBar;

    if-eqz v0, :cond_3f

    invoke-virtual {v0}, Lcom/miui/packageInstaller/ui/InstallerActionBar;->getMStartButton()Landroid/widget/Button;

    move-result-object v0

    goto :goto_2d

    :cond_3f
    move-object v0, v3

    :goto_2d
    if-nez v0, :cond_40

    goto :goto_2e

    :cond_40
    invoke-virtual {v8, v15}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :goto_2e
    new-instance v0, Lp5/g;

    invoke-direct {v0, v14, v10, v8}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    iget-object v0, v8, Lcom/miui/packageInstaller/a;->v:Lcom/miui/packageInstaller/ui/InstallerActionBar;

    if-eqz v0, :cond_41

    invoke-virtual {v0}, Lcom/miui/packageInstaller/ui/InstallerActionBar;->getMStartButton()Landroid/widget/Button;

    move-result-object v0

    if-eqz v0, :cond_41

    new-instance v1, Lm5/a0;

    invoke-direct {v1, v8}, Lm5/a0;-><init>(Lcom/miui/packageInstaller/a;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_41
    iget-object v0, v8, Lcom/miui/packageInstaller/a;->u:Lcom/miui/packageInstaller/model/CloudParams;

    if-eqz v0, :cond_42

    iget-object v0, v0, Lcom/miui/packageInstaller/model/CloudParams;->onlineFailButtonTip:Lcom/miui/packageInstaller/model/OnlineFailButtonTip;

    if-eqz v0, :cond_42

    iget-object v0, v0, Lcom/miui/packageInstaller/model/OnlineFailButtonTip;->text:Ljava/lang/String;

    goto :goto_2f

    :cond_42
    move-object v0, v3

    :goto_2f
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_49

    iget-object v0, v8, Lcom/miui/packageInstaller/a;->u:Lcom/miui/packageInstaller/model/CloudParams;

    if-eqz v0, :cond_43

    iget-object v0, v0, Lcom/miui/packageInstaller/model/CloudParams;->onlineFailButtonTip:Lcom/miui/packageInstaller/model/OnlineFailButtonTip;

    if-eqz v0, :cond_43

    iget-object v0, v0, Lcom/miui/packageInstaller/model/OnlineFailButtonTip;->actionUrl:Ljava/lang/String;

    goto :goto_30

    :cond_43
    move-object v0, v3

    :goto_30
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_49

    iget-object v0, v8, Lcom/miui/packageInstaller/a;->v:Lcom/miui/packageInstaller/ui/InstallerActionBar;

    if-eqz v0, :cond_44

    invoke-virtual {v0}, Lcom/miui/packageInstaller/ui/InstallerActionBar;->getMTopButton()Landroid/widget/Button;

    move-result-object v0

    goto :goto_31

    :cond_44
    move-object v0, v3

    :goto_31
    if-nez v0, :cond_45

    goto :goto_32

    :cond_45
    invoke-virtual {v0, v13}, Landroid/widget/Button;->setVisibility(I)V

    :goto_32
    iget-object v0, v8, Lcom/miui/packageInstaller/a;->v:Lcom/miui/packageInstaller/ui/InstallerActionBar;

    if-eqz v0, :cond_46

    invoke-virtual {v0}, Lcom/miui/packageInstaller/ui/InstallerActionBar;->getMEndButton()Landroid/widget/Button;

    move-result-object v0

    goto :goto_33

    :cond_46
    move-object v0, v3

    :goto_33
    if-nez v0, :cond_47

    goto :goto_35

    :cond_47
    iget-object v1, v8, Lcom/miui/packageInstaller/a;->u:Lcom/miui/packageInstaller/model/CloudParams;

    if-eqz v1, :cond_48

    iget-object v1, v1, Lcom/miui/packageInstaller/model/CloudParams;->onlineFailButtonTip:Lcom/miui/packageInstaller/model/OnlineFailButtonTip;

    if-eqz v1, :cond_48

    iget-object v10, v1, Lcom/miui/packageInstaller/model/OnlineFailButtonTip;->text:Ljava/lang/String;

    goto :goto_34

    :cond_48
    move-object v10, v3

    :goto_34
    invoke-virtual {v0, v10}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :goto_35
    iget-object v0, v8, Lcom/miui/packageInstaller/a;->v:Lcom/miui/packageInstaller/ui/InstallerActionBar;

    if-eqz v0, :cond_4e

    invoke-virtual {v0}, Lcom/miui/packageInstaller/ui/InstallerActionBar;->getMEndButton()Landroid/widget/Button;

    move-result-object v0

    if-eqz v0, :cond_4e

    new-instance v1, Lm5/z;

    invoke-direct {v1, v8}, Lm5/z;-><init>(Lcom/miui/packageInstaller/a;)V

    goto/16 :goto_2c

    :cond_49
    iget-object v0, v8, Lcom/miui/packageInstaller/a;->v:Lcom/miui/packageInstaller/ui/InstallerActionBar;

    if-eqz v0, :cond_4a

    invoke-virtual {v0}, Lcom/miui/packageInstaller/ui/InstallerActionBar;->getMTopButton()Landroid/widget/Button;

    move-result-object v0

    goto :goto_36

    :cond_4a
    move-object v0, v3

    :goto_36
    if-nez v0, :cond_4b

    goto :goto_37

    :cond_4b
    invoke-virtual {v0, v13}, Landroid/widget/Button;->setVisibility(I)V

    :goto_37
    iget-object v0, v8, Lcom/miui/packageInstaller/a;->v:Lcom/miui/packageInstaller/ui/InstallerActionBar;

    if-eqz v0, :cond_4c

    invoke-virtual {v0}, Lcom/miui/packageInstaller/ui/InstallerActionBar;->getMEndButton()Landroid/widget/Button;

    move-result-object v10

    goto :goto_38

    :cond_4c
    move-object v10, v3

    :goto_38
    if-nez v10, :cond_4d

    goto :goto_39

    :cond_4d
    invoke-virtual {v10, v13}, Landroid/widget/Button;->setVisibility(I)V

    :cond_4e
    :goto_39
    packed-switch v6, :pswitch_data_0

    :pswitch_0
    goto :goto_3a

    :pswitch_1
    new-instance v9, Lp5/c;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v4, 0x3

    const/4 v5, 0x0

    move-object v0, v9

    move-object/from16 v3, p0

    invoke-direct/range {v0 .. v5}, Lp5/c;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;ILm8/g;)V

    const-string v0, "download_process"

    const-string v1, "download_finish"

    invoke-virtual {v9, v0, v1}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object v0

    const-string v1, "download_source"

    invoke-virtual {v0, v1, v7}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object v0

    const-string v1, "download_finish_status"

    const-string v2, "fail"

    invoke-virtual {v0, v1, v2}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object v0

    invoke-static/range {p3 .. p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "download_fail_code"

    invoke-virtual {v0, v2, v1}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object v0

    invoke-virtual {v0}, Lp5/f;->c()Z

    :cond_4f
    :goto_3a
    iput-boolean v12, v8, Lcom/miui/packageInstaller/a;->J:Z

    const-string v0, "installCompleted"

    const-string v1, "installCompleted installCompleted "

    invoke-static {v0, v1}, Lf6/o;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    invoke-virtual/range {p0 .. p0}, Lcom/miui/packageInstaller/a;->Y0()V

    return-void

    nop

    :pswitch_data_0
    .packed-switch -0xea66
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public D0()Ljava/lang/String;
    .locals 1

    const-string v0, "install_installing"

    return-object v0
.end method

.method public D1(Landroid/content/Intent;)V
    .locals 8

    const-string v0, "intent"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const/high16 v0, 0x10000000

    :try_start_0
    invoke-virtual {p1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, p1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    const-string v0, "InstallProgress"

    const-string v1, ""

    invoke-static {v0, v1, p1}, Lf6/o;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_0
    invoke-virtual {p0}, Lmiuix/appcompat/app/j;->finish()V

    new-instance p1, Lp5/b;

    const-string v0, "open_app_btn"

    const-string v1, "button"

    invoke-direct {p1, v0, v1, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p1}, Lp5/f;->c()Z

    new-instance p1, Lp5/a;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x3

    const/4 v7, 0x0

    move-object v2, p1

    move-object v5, p0

    invoke-direct/range {v2 .. v7}, Lp5/a;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;ILm8/g;)V

    invoke-virtual {p1}, Lp5/f;->c()Z

    return-void
.end method

.method public E0()Ljava/lang/String;
    .locals 1

    const-string v0, "install_during"

    return-object v0
.end method

.method public G()Lcom/miui/packageInstaller/model/ApkInfo;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/a;->y:Lcom/miui/packageInstaller/model/ApkInfo;

    return-object v0
.end method

.method public final G1()V
    .locals 3

    invoke-virtual {p0}, Lcom/miui/packageInstaller/a;->e1()Lj6/b;

    move-result-object v0

    invoke-virtual {v0}, Lj6/b;->R()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lm6/a;

    instance-of v2, v1, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;

    if-nez v2, :cond_1

    instance-of v2, v1, Lcom/miui/packageInstaller/ui/listcomponets/AdTitleViewObject;

    if-eqz v2, :cond_0

    :cond_1
    invoke-virtual {v1}, Lm6/a;->t()V

    goto :goto_0

    :cond_2
    return-void
.end method

.method public final H1(Lm5/e;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;)V
    .locals 10

    const-string v0, "caller"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0}, Landroidx/lifecycle/k;->a(Landroidx/lifecycle/j;)Landroidx/lifecycle/e;

    move-result-object v1

    invoke-static {}, Lv8/t0;->c()Lv8/v1;

    move-result-object v2

    new-instance v0, Lcom/miui/packageInstaller/a$b;

    const/4 v9, 0x0

    move-object v3, v0

    move-object v4, p0

    move-object v5, p1

    move-object v6, p2

    move-object v7, p3

    move-object v8, p4

    invoke-direct/range {v3 .. v9}, Lcom/miui/packageInstaller/a$b;-><init>(Lcom/miui/packageInstaller/a;Lm5/e;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ld8/d;)V

    const/4 v3, 0x0

    const/4 v5, 0x2

    const/4 v6, 0x0

    move-object v4, v0

    invoke-static/range {v1 .. v6}, Lv8/f;->d(Lv8/e0;Ld8/g;Lv8/g0;Ll8/p;ILjava/lang/Object;)Lv8/k1;

    return-void
.end method

.method public final J1(Ljava/lang/ref/WeakReference;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/WeakReference<",
            "Lcom/miui/packageInstaller/a;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/a;->R:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method public final K1(Lj6/b;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/a;->P:Lj6/b;

    return-void
.end method

.method public N(Ljava/lang/String;)Lo5/c;
    .locals 0

    iget-object p1, p0, Lcom/miui/packageInstaller/a;->Q:Lo5/c;

    return-object p1
.end method

.method public final Y0()V
    .locals 3

    iget-object v0, p0, Lcom/miui/packageInstaller/a;->y:Lcom/miui/packageInstaller/model/ApkInfo;

    if-eqz v0, :cond_3

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/ApkInfo;->getFileUri()Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/miui/packageInstaller/a;->y:Lcom/miui/packageInstaller/model/ApkInfo;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/ApkInfo;->getFileUri()Landroid/net/Uri;

    move-result-object v0

    goto :goto_1

    :cond_1
    move-object v0, v1

    :goto_1
    iget-object v2, p0, Lcom/miui/packageInstaller/a;->y:Lcom/miui/packageInstaller/model/ApkInfo;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/miui/packageInstaller/model/ApkInfo;->getOriginalUri()Landroid/net/Uri;

    move-result-object v1

    :cond_2
    invoke-static {v0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object v0

    new-instance v1, Lm5/x;

    invoke-direct {v1, p0}, Lm5/x;-><init>(Lcom/miui/packageInstaller/a;)V

    invoke-virtual {v0, v1}, Lf6/z;->g(Ljava/lang/Runnable;)V

    :cond_3
    return-void
.end method

.method public backIconClick(Landroid/view/View;)V
    .locals 2

    iget-boolean p1, p0, Lcom/miui/packageInstaller/a;->J:Z

    if-eqz p1, :cond_3

    new-instance p1, Lp5/b;

    const-string v0, "page_back_btn"

    const-string v1, "button"

    invoke-direct {p1, v0, v1, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    const-string v0, "back_type"

    const-string v1, "click_icon"

    invoke-virtual {p1, v0, v1}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p1

    invoke-virtual {p1}, Lp5/f;->c()Z

    iget-object p1, p0, Lcom/miui/packageInstaller/a;->u:Lcom/miui/packageInstaller/model/CloudParams;

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    iget-object p1, p1, Lcom/miui/packageInstaller/model/CloudParams;->backIconUri:Ljava/lang/String;

    goto :goto_0

    :cond_0
    move-object p1, v0

    :goto_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_2

    new-instance p1, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {p1, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    :try_start_0
    iget-object v1, p0, Lcom/miui/packageInstaller/a;->u:Lcom/miui/packageInstaller/model/CloudParams;

    if-eqz v1, :cond_1

    iget-object v0, v1, Lcom/miui/packageInstaller/model/CloudParams;->backIconUri:Ljava/lang/String;

    :cond_1
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    invoke-virtual {p0, p1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_2
    invoke-virtual {p0}, Lmiuix/appcompat/app/j;->finish()V

    :cond_3
    return-void
.end method

.method public final c1()V
    .locals 8

    iget-object v0, p0, Lcom/miui/packageInstaller/a;->I:Ls5/l;

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget v0, p0, Lcom/miui/packageInstaller/a;->B:I

    const-string v1, "null cannot be cast to non-null type kotlin.Int"

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-eqz v0, :cond_10

    if-eq v0, v3, :cond_1

    goto/16 :goto_14

    :cond_1
    const-string v0, "appstore"

    iput-object v0, p0, Lcom/miui/packageInstaller/a;->L:Ljava/lang/String;

    iget-object v0, p0, Lcom/miui/packageInstaller/a;->u:Lcom/miui/packageInstaller/model/CloudParams;

    if-eqz v0, :cond_2

    iget-boolean v5, v0, Lcom/miui/packageInstaller/model/CloudParams;->bundleApp:Z

    if-ne v5, v3, :cond_2

    move v5, v3

    goto :goto_0

    :cond_2
    move v5, v2

    :goto_0
    if-eqz v5, :cond_3

    if-eqz v0, :cond_4

    iget-object v0, v0, Lcom/miui/packageInstaller/model/CloudParams;->appInfo:Lcom/miui/packageInstaller/model/MarketAppInfo;

    if-eqz v0, :cond_4

    iget-object v0, v0, Lcom/miui/packageInstaller/model/MarketAppInfo;->packageName:Ljava/lang/String;

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/miui/packageInstaller/a;->y:Lcom/miui/packageInstaller/model/ApkInfo;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/ApkInfo;->getPackageName()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_4
    move-object v0, v4

    :goto_1
    if-eqz v0, :cond_f

    iget-object v5, p0, Lcom/miui/packageInstaller/a;->u:Lcom/miui/packageInstaller/model/CloudParams;

    if-eqz v5, :cond_5

    iget-object v6, v5, Lcom/miui/packageInstaller/model/CloudParams;->incrementPackageInfo:Lcom/miui/packageInstaller/model/HasIncrement;

    goto :goto_2

    :cond_5
    move-object v6, v4

    :goto_2
    if-eqz v6, :cond_a

    if-eqz v5, :cond_6

    iget-object v5, v5, Lcom/miui/packageInstaller/model/CloudParams;->incrementPackageInfo:Lcom/miui/packageInstaller/model/HasIncrement;

    if-eqz v5, :cond_6

    invoke-virtual {v5}, Lcom/miui/packageInstaller/model/HasIncrement;->getIncrePkgInfo()Lcom/miui/packageInstaller/model/IncrementPackageInfo;

    move-result-object v5

    goto :goto_3

    :cond_6
    move-object v5, v4

    :goto_3
    if-eqz v5, :cond_a

    new-instance v5, Ls5/l$a;

    invoke-direct {v5, p0}, Ls5/l$a;-><init>(Landroid/content/Context;)V

    invoke-virtual {v5}, Ls5/l$a;->b()Ls5/g$a;

    move-result-object v5

    iget-object v6, p0, Lcom/miui/packageInstaller/a;->u:Lcom/miui/packageInstaller/model/CloudParams;

    if-eqz v6, :cond_7

    iget-object v6, v6, Lcom/miui/packageInstaller/model/CloudParams;->incrementPackageInfo:Lcom/miui/packageInstaller/model/HasIncrement;

    if-eqz v6, :cond_7

    invoke-virtual {v6}, Lcom/miui/packageInstaller/model/HasIncrement;->getIncrePkgInfo()Lcom/miui/packageInstaller/model/IncrementPackageInfo;

    move-result-object v6

    goto :goto_4

    :cond_7
    move-object v6, v4

    :goto_4
    invoke-virtual {v5, v6}, Ls5/g$a;->k(Lcom/miui/packageInstaller/model/IncrementPackageInfo;)Ls5/g$a;

    move-result-object v5

    iget-object v6, p0, Lcom/miui/packageInstaller/a;->y:Lcom/miui/packageInstaller/model/ApkInfo;

    if-eqz v6, :cond_8

    invoke-virtual {v6}, Lcom/miui/packageInstaller/model/ApkInfo;->getFileUri()Landroid/net/Uri;

    move-result-object v6

    goto :goto_5

    :cond_8
    move-object v6, v4

    :goto_5
    invoke-static {v6}, Lm8/i;->c(Ljava/lang/Object;)V

    invoke-virtual {v5, v6}, Ls5/g$a;->j(Landroid/net/Uri;)Ls5/g$a;

    move-result-object v5

    iget-object v6, p0, Lcom/miui/packageInstaller/a;->x:Lm5/e;

    if-eqz v6, :cond_9

    invoke-virtual {v6}, Lm5/e;->l()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    goto :goto_6

    :cond_9
    move-object v6, v4

    :goto_6
    invoke-static {v6, v1}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v5, v1}, Ls5/d$a;->c(I)Ls5/d$a;

    move-result-object v1

    invoke-virtual {p0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/pm/PackageManager;->getPackageInstaller()Landroid/content/pm/PackageInstaller;

    move-result-object v5

    invoke-static {v5}, Lcom/android/packageinstaller/utils/t;->c(Landroid/content/pm/PackageInstaller;)Z

    move-result v5

    invoke-virtual {v1, v5}, Ls5/d$a;->g(Z)Ls5/d$a;

    move-result-object v1

    invoke-virtual {v1, v0}, Ls5/d$a;->e(Ljava/lang/String;)Ls5/d$a;

    move-result-object v0

    goto/16 :goto_13

    :cond_a
    new-instance v1, Ls5/l$a;

    invoke-direct {v1, p0}, Ls5/l$a;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Ls5/l$a;->c()Ls5/n$a;

    move-result-object v1

    invoke-virtual {v1, v0}, Ls5/n$a;->c(Ljava/lang/String;)Ls5/n$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/packageInstaller/a;->x:Lm5/e;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Lm5/e;->j()Ljava/lang/String;

    move-result-object v1

    goto :goto_7

    :cond_b
    move-object v1, v4

    :goto_7
    const-string v5, ""

    if-nez v1, :cond_c

    move-object v1, v5

    goto :goto_8

    :cond_c
    const-string v6, "mCallingPackage?.callingPackage?:\"\""

    invoke-static {v1, v6}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_8
    invoke-virtual {v0, v1}, Ls5/n$a;->d(Ljava/lang/String;)Ls5/n$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/packageInstaller/a;->u:Lcom/miui/packageInstaller/model/CloudParams;

    if-eqz v1, :cond_d

    iget-object v1, v1, Lcom/miui/packageInstaller/model/CloudParams;->positiveButtonTip:Lcom/miui/packageInstaller/model/PositiveButtonRules;

    if-eqz v1, :cond_d

    iget-object v1, v1, Lcom/miui/packageInstaller/model/PositiveButtonRules;->actionUrl:Ljava/lang/String;

    goto :goto_9

    :cond_d
    move-object v1, v4

    :goto_9
    if-nez v1, :cond_e

    goto :goto_a

    :cond_e
    const-string v5, "mCloudParams?.positiveButtonTip?.actionUrl?:\"\""

    invoke-static {v1, v5}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v5, v1

    :goto_a
    invoke-virtual {v0, v5}, Ls5/n$a;->b(Ljava/lang/String;)Ls5/n$a;

    move-result-object v0

    invoke-virtual {v0}, Ls5/n$a;->a()Ls5/l;

    move-result-object v0

    goto :goto_b

    :cond_f
    move-object v0, v4

    :goto_b
    iput-object v0, p0, Lcom/miui/packageInstaller/a;->I:Ls5/l;

    goto/16 :goto_14

    :cond_10
    const-string v0, "pi"

    iput-object v0, p0, Lcom/miui/packageInstaller/a;->L:Ljava/lang/String;

    new-instance v0, Ls5/l$a;

    invoke-direct {v0, p0}, Ls5/l$a;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Ls5/l$a;->a()Ls5/d$a;

    move-result-object v0

    iget-object v5, p0, Lcom/miui/packageInstaller/a;->y:Lcom/miui/packageInstaller/model/ApkInfo;

    if-eqz v5, :cond_11

    invoke-virtual {v5}, Lcom/miui/packageInstaller/model/ApkInfo;->getPackageName()Ljava/lang/String;

    move-result-object v5

    goto :goto_c

    :cond_11
    move-object v5, v4

    :goto_c
    invoke-virtual {v0, v5}, Ls5/d$a;->e(Ljava/lang/String;)Ls5/d$a;

    move-result-object v0

    iget-object v5, p0, Lcom/miui/packageInstaller/a;->y:Lcom/miui/packageInstaller/model/ApkInfo;

    if-eqz v5, :cond_12

    invoke-virtual {v5}, Lcom/miui/packageInstaller/model/ApkInfo;->getOriginalUri()Landroid/net/Uri;

    move-result-object v5

    goto :goto_d

    :cond_12
    move-object v5, v4

    :goto_d
    invoke-virtual {v0, v5}, Ls5/d$a;->d(Landroid/net/Uri;)Ls5/d$a;

    move-result-object v0

    iget-object v5, p0, Lcom/miui/packageInstaller/a;->x:Lm5/e;

    if-eqz v5, :cond_13

    iget-object v5, v5, Lm5/e;->g:Landroid/net/Uri;

    goto :goto_e

    :cond_13
    move-object v5, v4

    :goto_e
    invoke-virtual {v0, v5}, Ls5/d$a;->f(Landroid/net/Uri;)Ls5/d$a;

    move-result-object v0

    iget-object v5, p0, Lcom/miui/packageInstaller/a;->x:Lm5/e;

    if-eqz v5, :cond_14

    invoke-virtual {v5}, Lm5/e;->l()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    goto :goto_f

    :cond_14
    move-object v5, v4

    :goto_f
    invoke-static {v5, v1}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Ls5/d$a;->c(I)Ls5/d$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/packageInstaller/a;->y:Lcom/miui/packageInstaller/model/ApkInfo;

    if-eqz v1, :cond_15

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/ApkInfo;->getFileUri()Landroid/net/Uri;

    move-result-object v1

    goto :goto_10

    :cond_15
    move-object v1, v4

    :goto_10
    invoke-virtual {v0, v1}, Ls5/d$a;->h(Landroid/net/Uri;)Ls5/d$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/packageInstaller/a;->y:Lcom/miui/packageInstaller/model/ApkInfo;

    if-eqz v1, :cond_16

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/ApkInfo;->getApkMd5()Ljava/lang/String;

    move-result-object v1

    goto :goto_11

    :cond_16
    move-object v1, v4

    :goto_11
    invoke-virtual {v0, v1}, Ls5/d$a;->b(Ljava/lang/String;)Ls5/d$a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/packageInstaller/a;->y:Lcom/miui/packageInstaller/model/ApkInfo;

    if-eqz v1, :cond_17

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/ApkInfo;->getCurrentInstallVersionName()Ljava/lang/String;

    move-result-object v1

    goto :goto_12

    :cond_17
    move-object v1, v4

    :goto_12
    invoke-virtual {v0, v1}, Ls5/d$a;->i(Ljava/lang/String;)Ls5/d$a;

    move-result-object v0

    invoke-virtual {p0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/pm/PackageManager;->getPackageInstaller()Landroid/content/pm/PackageInstaller;

    move-result-object v1

    invoke-static {v1}, Lcom/android/packageinstaller/utils/t;->c(Landroid/content/pm/PackageInstaller;)Z

    move-result v1

    invoke-virtual {v0, v1}, Ls5/d$a;->g(Z)Ls5/d$a;

    move-result-object v0

    :goto_13
    invoke-virtual {v0}, Ls5/d$a;->a()Ls5/l;

    move-result-object v0

    goto/16 :goto_b

    :goto_14
    iget-object v0, p0, Lcom/miui/packageInstaller/a;->I:Ls5/l;

    if-eqz v0, :cond_18

    invoke-virtual {v0, p0}, Ls5/l;->m(Ls5/l$c;)V

    :cond_18
    sget-object v0, Lf6/a;->a:Lf6/a$a;

    invoke-virtual {v0, p0}, Lf6/a$a;->b(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    const-string v6, "packageManager"

    invoke-static {v5, v6}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    const-string v7, "intent"

    invoke-static {v6, v7}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p0, v5, v6, v1}, Lf6/a$a;->c(Landroid/app/Activity;Landroid/content/pm/PackageManager;Landroid/content/Intent;Ljava/lang/String;)I

    move-result v0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v5

    if-ne v0, v5, :cond_19

    invoke-static {p0}, Lj2/f;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x2

    invoke-static {v1, v6, v2, v7, v4}, Lu8/g;->k(Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_19

    iget-object v0, p0, Lcom/miui/packageInstaller/a;->I:Ls5/l;

    if-eqz v0, :cond_1b

    invoke-virtual {v0}, Ls5/l;->n()V

    goto :goto_15

    :cond_19
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Uid not match! call uid = "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v4, " bind uid = "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v4, "callingPackage = "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "  \u81ea\u8eab\u5305\u540d"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p0}, Lj2/f;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v4, "InstallProgress"

    invoke-static {v4, v2}, Lf6/o;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    iget-object v2, p0, Lcom/miui/packageInstaller/a;->v:Lcom/miui/packageInstaller/ui/InstallerActionBar;

    if-eqz v2, :cond_1a

    const v4, 0x7f110025

    invoke-virtual {p0, v4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/miui/packageInstaller/ui/InstallerActionBar;->setProgressText(Ljava/lang/CharSequence;)V

    :cond_1a
    iget-object v2, p0, Lcom/miui/packageInstaller/a;->x:Lm5/e;

    if-eqz v2, :cond_1b

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {p0, v2, v1, v0, v4}, Lcom/miui/packageInstaller/a;->H1(Lm5/e;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;)V

    :cond_1b
    :goto_15
    iput-boolean v3, p0, Lcom/miui/packageInstaller/a;->D:Z

    return-void
.end method

.method public final d1()Ljava/lang/ref/WeakReference;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/ref/WeakReference<",
            "Lcom/miui/packageInstaller/a;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/miui/packageInstaller/a;->R:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    const-string v0, "mActivityReference"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v0, 0x0

    return-object v0
.end method

.method public final e1()Lj6/b;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/a;->P:Lj6/b;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    const-string v0, "mAdapter"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v0, 0x0

    return-object v0
.end method

.method public final f1()Lcom/miui/packageInstaller/model/ApkInfo;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/a;->y:Lcom/miui/packageInstaller/model/ApkInfo;

    return-object v0
.end method

.method public final g1()Lcom/miui/packageInstaller/model/CloudParams;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/a;->u:Lcom/miui/packageInstaller/model/CloudParams;

    return-object v0
.end method

.method public final h1()I
    .locals 1

    iget v0, p0, Lcom/miui/packageInstaller/a;->B:I

    return v0
.end method

.method public final i1()Ls5/l;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/a;->I:Ls5/l;

    return-object v0
.end method

.method public final j1()Lcom/miui/packageInstaller/model/Virus;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/a;->z:Lcom/miui/packageInstaller/model/Virus;

    return-object v0
.end method

.method public final l1(Lcom/miui/packageInstaller/ui/InstallerActionBar;)V
    .locals 1

    iget-boolean v0, p0, Lcom/miui/packageInstaller/a;->T:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/packageInstaller/a;->T:Z

    iput-object p1, p0, Lcom/miui/packageInstaller/a;->v:Lcom/miui/packageInstaller/ui/InstallerActionBar;

    invoke-virtual {p0, p1}, Lcom/miui/packageInstaller/a;->r1(Lcom/miui/packageInstaller/ui/InstallerActionBar;)V

    invoke-virtual {p0}, Lcom/miui/packageInstaller/a;->c1()V

    return-void
.end method

.method public m1()V
    .locals 7

    const v0, 0x7f0a0089

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById<View>(R.id.back_icon)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/miui/packageInstaller/a;->O:Landroid/view/View;

    const/4 v1, 0x0

    const-string v2, "mBackIcon"

    if-nez v0, :cond_0

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_0
    new-instance v3, Lm5/e0;

    invoke-direct {v3, p0}, Lm5/e0;-><init>(Lcom/miui/packageInstaller/a;)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v0, 0x1

    new-array v3, v0, [Landroid/view/View;

    iget-object v4, p0, Lcom/miui/packageInstaller/a;->O:Landroid/view/View;

    if-nez v4, :cond_1

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v4, v1

    :cond_1
    const/4 v5, 0x0

    aput-object v4, v3, v5

    invoke-static {v3}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object v3

    invoke-interface {v3}, Lmiuix/animation/e;->c()Lmiuix/animation/j;

    move-result-object v3

    new-array v4, v5, [Lmiuix/animation/j$b;

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-interface {v3, v6, v4}, Lmiuix/animation/j;->G(F[Lmiuix/animation/j$b;)Lmiuix/animation/j;

    move-result-object v3

    iget-object v4, p0, Lcom/miui/packageInstaller/a;->O:Landroid/view/View;

    if-nez v4, :cond_2

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    move-object v1, v4

    :goto_0
    new-array v2, v5, [Lc9/a;

    invoke-interface {v3, v1, v2}, Lmiuix/animation/j;->z(Landroid/view/View;[Lc9/a;)V

    const v1, 0x7f0a030c

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, Lm5/y;

    invoke-direct {v2, p0}, Lm5/y;-><init>(Lcom/miui/packageInstaller/a;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-array v0, v0, [Landroid/view/View;

    aput-object v1, v0, v5

    invoke-static {v0}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object v0

    invoke-interface {v0}, Lmiuix/animation/e;->c()Lmiuix/animation/j;

    move-result-object v0

    new-array v2, v5, [Lmiuix/animation/j$b;

    invoke-interface {v0, v6, v2}, Lmiuix/animation/j;->G(F[Lmiuix/animation/j$b;)Lmiuix/animation/j;

    move-result-object v0

    new-array v2, v5, [Lc9/a;

    invoke-interface {v0, v1, v2}, Lmiuix/animation/j;->z(Landroid/view/View;[Lc9/a;)V

    return-void
.end method

.method public onBackPressed()V
    .locals 3

    iget-boolean v0, p0, Lcom/miui/packageInstaller/a;->J:Z

    if-eqz v0, :cond_3

    new-instance v0, Lp5/b;

    const-string v1, "page_back_btn"

    const-string v2, "button"

    invoke-direct {v0, v1, v2, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    const-string v1, "back_type"

    const-string v2, "system"

    invoke-virtual {v0, v1, v2}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object v0

    invoke-virtual {v0}, Lp5/f;->c()Z

    iget-object v0, p0, Lcom/miui/packageInstaller/a;->u:Lcom/miui/packageInstaller/model/CloudParams;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/miui/packageInstaller/model/CloudParams;->backButtonUri:Ljava/lang/String;

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    :try_start_0
    iget-object v2, p0, Lcom/miui/packageInstaller/a;->u:Lcom/miui/packageInstaller/model/CloudParams;

    if-eqz v2, :cond_1

    iget-object v1, v2, Lcom/miui/packageInstaller/model/CloudParams;->backButtonUri:Ljava/lang/String;

    :cond_1
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_2
    invoke-super {p0}, Lmiuix/appcompat/app/j;->onBackPressed()V

    :cond_3
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {p0, v0}, Lcom/miui/packageInstaller/a;->J1(Ljava/lang/ref/WeakReference;)V

    sget-object v0, Lcom/miui/packageInstaller/a;->V:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/miui/packageInstaller/a;->d1()Ljava/lang/ref/WeakReference;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-super {p0, p1}, Lq2/b;->onCreate(Landroid/os/Bundle;)V

    invoke-direct {p0}, Lcom/miui/packageInstaller/a;->F1()V

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_2

    const-string v2, "InstallProgress"

    const-string v3, "reOnCreate finish"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "status"

    const/16 v3, -0x3e8

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result p1

    if-eq p1, v3, :cond_0

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "android.intent.extra.INSTALL_RESULT"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    :cond_0
    if-nez p1, :cond_1

    const/4 v0, -0x1

    :cond_1
    invoke-virtual {p0, v0, v1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lmiuix/appcompat/app/j;->finish()V

    return-void

    :cond_2
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v2, "apk_info"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/miui/packageInstaller/model/ApkInfo;

    iput-object v2, p0, Lcom/miui/packageInstaller/a;->y:Lcom/miui/packageInstaller/model/ApkInfo;

    if-eqz v2, :cond_6

    invoke-static {v2}, Lm8/i;->c(Ljava/lang/Object;)V

    invoke-virtual {v2}, Lcom/miui/packageInstaller/model/ApkInfo;->getPackageInfo()Landroid/content/pm/PackageInfo;

    move-result-object v2

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/miui/packageInstaller/a;->y:Lcom/miui/packageInstaller/model/ApkInfo;

    invoke-static {v2}, Lm8/i;->c(Ljava/lang/Object;)V

    invoke-virtual {v2}, Lcom/miui/packageInstaller/model/ApkInfo;->getPackageInfo()Landroid/content/pm/PackageInfo;

    move-result-object v2

    invoke-static {v2}, Lm8/i;->c(Ljava/lang/Object;)V

    iget-object v2, v2, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/miui/packageInstaller/a;->y:Lcom/miui/packageInstaller/model/ApkInfo;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lcom/miui/packageInstaller/model/ApkInfo;->getOriginalUri()Landroid/net/Uri;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_3
    move-object v2, v1

    :goto_0
    const-string v3, "package"

    invoke-static {v2, v3}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    new-instance v2, Lj2/f$a;

    invoke-virtual {p0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    iget-object v4, p0, Lcom/miui/packageInstaller/a;->y:Lcom/miui/packageInstaller/model/ApkInfo;

    invoke-static {v4}, Lm8/i;->c(Ljava/lang/Object;)V

    invoke-virtual {v4}, Lcom/miui/packageInstaller/model/ApkInfo;->getPackageInfo()Landroid/content/pm/PackageInfo;

    move-result-object v4

    invoke-static {v4}, Lm8/i;->c(Ljava/lang/Object;)V

    iget-object v4, v4, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v3, v4}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {p0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    iget-object v5, p0, Lcom/miui/packageInstaller/a;->y:Lcom/miui/packageInstaller/model/ApkInfo;

    invoke-static {v5}, Lm8/i;->c(Ljava/lang/Object;)V

    invoke-virtual {v5}, Lcom/miui/packageInstaller/model/ApkInfo;->getPackageInfo()Landroid/content/pm/PackageInfo;

    move-result-object v5

    invoke-static {v5}, Lm8/i;->c(Ljava/lang/Object;)V

    iget-object v5, v5, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v4, v5}, Landroid/content/pm/PackageManager;->getApplicationIcon(Landroid/content/pm/ApplicationInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lj2/f$a;-><init>(Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    :cond_4
    move-object v2, v1

    :goto_1
    iget-object v3, p0, Lcom/miui/packageInstaller/a;->y:Lcom/miui/packageInstaller/model/ApkInfo;

    invoke-static {v3}, Lm8/i;->c(Ljava/lang/Object;)V

    invoke-virtual {v3}, Lcom/miui/packageInstaller/model/ApkInfo;->getFileUri()Landroid/net/Uri;

    move-result-object v3

    if-eqz v3, :cond_5

    iget-object v2, p0, Lcom/miui/packageInstaller/a;->y:Lcom/miui/packageInstaller/model/ApkInfo;

    invoke-static {v2}, Lm8/i;->c(Ljava/lang/Object;)V

    invoke-virtual {v2}, Lcom/miui/packageInstaller/model/ApkInfo;->getPackageInfo()Landroid/content/pm/PackageInfo;

    move-result-object v2

    invoke-static {v2}, Lm8/i;->c(Ljava/lang/Object;)V

    iget-object v2, v2, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    new-instance v3, Ljava/io/File;

    iget-object v4, p0, Lcom/miui/packageInstaller/a;->y:Lcom/miui/packageInstaller/model/ApkInfo;

    invoke-static {v4}, Lm8/i;->c(Ljava/lang/Object;)V

    invoke-virtual {v4}, Lcom/miui/packageInstaller/model/ApkInfo;->getFileUri()Landroid/net/Uri;

    move-result-object v4

    invoke-static {v4}, Lm8/i;->c(Ljava/lang/Object;)V

    invoke-virtual {v4}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {p0, v2, v3}, Lj2/f;->c(Landroid/content/Context;Landroid/content/pm/ApplicationInfo;Ljava/io/File;)Lj2/f$a;

    move-result-object v2

    :cond_5
    if-eqz v2, :cond_6

    iget-object v3, p0, Lcom/miui/packageInstaller/a;->y:Lcom/miui/packageInstaller/model/ApkInfo;

    invoke-static {v3}, Lm8/i;->c(Ljava/lang/Object;)V

    iget-object v2, v2, Lj2/f$a;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3, v2}, Lcom/miui/packageInstaller/model/ApkInfo;->setIcon(Landroid/graphics/drawable/Drawable;)V

    :cond_6
    const-string v2, "virus_data"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v2

    check-cast v2, Lcom/miui/packageInstaller/model/Virus;

    iput-object v2, p0, Lcom/miui/packageInstaller/a;->z:Lcom/miui/packageInstaller/model/Virus;

    const-string v2, "installId"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_7

    const-string v2, ""

    :cond_7
    iput-object v2, p0, Lcom/miui/packageInstaller/a;->N:Ljava/lang/String;

    iget-object v2, p0, Lcom/miui/packageInstaller/a;->y:Lcom/miui/packageInstaller/model/ApkInfo;

    if-eqz v2, :cond_8

    invoke-virtual {v2}, Lcom/miui/packageInstaller/model/ApkInfo;->getCloudParams()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v1

    :cond_8
    iput-object v1, p0, Lcom/miui/packageInstaller/a;->u:Lcom/miui/packageInstaller/model/CloudParams;

    const-string v1, "caller"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lm5/e;

    iput-object v1, p0, Lcom/miui/packageInstaller/a;->x:Lm5/e;

    const-string v1, "static_params_package"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lo5/c;

    iput-object v1, p0, Lcom/miui/packageInstaller/a;->Q:Lo5/c;

    const-string v1, "installType"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result p1

    iput p1, p0, Lcom/miui/packageInstaller/a;->B:I

    iget-object p1, p0, Lcom/miui/packageInstaller/a;->y:Lcom/miui/packageInstaller/model/ApkInfo;

    if-eqz p1, :cond_b

    iget-object p1, p0, Lcom/miui/packageInstaller/a;->x:Lm5/e;

    if-nez p1, :cond_9

    goto :goto_3

    :cond_9
    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lm2/b;->g(Landroid/content/Context;)Lm2/b;

    move-result-object p1

    invoke-virtual {p1}, Lm2/b;->m()Z

    move-result p1

    iput-boolean p1, p0, Lcom/miui/packageInstaller/a;->C:Z

    new-instance p1, Landroid/content/Intent;

    const-string v1, "com.miui.packageinstaller.INSTALL_PROGRESS_START_SUCCESS"

    invoke-direct {p1, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/miui/packageInstaller/a;->m1()V

    sget-object v2, Ll2/a;->a:Ll2/a;

    invoke-virtual {p0}, Lcom/miui/packageInstaller/a;->D0()Ljava/lang/String;

    move-result-object v4

    iget p1, p0, Lcom/miui/packageInstaller/a;->B:I

    if-ne p1, v0, :cond_a

    const-string p1, "appstore"

    goto :goto_2

    :cond_a
    const-string p1, "pi"

    :goto_2
    move-object v5, p1

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/miui/packageInstaller/a;->x:Lm5/e;

    const/16 v8, 0x8

    const/4 v9, 0x0

    move-object v3, p0

    invoke-static/range {v2 .. v9}, Ll2/a;->c(Ll2/a;Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lm5/e;ILjava/lang/Object;)V

    return-void

    :cond_b
    :goto_3
    invoke-virtual {p0}, Lmiuix/appcompat/app/j;->finish()V

    return-void
.end method

.method protected onDestroy()V
    .locals 2

    invoke-super {p0}, Landroidx/fragment/app/e;->onDestroy()V

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/packageInstaller/a;->M:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lf6/z;->f(Ljava/lang/Runnable;)V

    sget-object v0, Lcom/miui/packageInstaller/a;->V:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/miui/packageInstaller/a;->d1()Ljava/lang/ref/WeakReference;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/miui/packageInstaller/a;->I:Ls5/l;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ls5/l;->l()V

    :cond_0
    invoke-virtual {p0}, Lcom/miui/packageInstaller/a;->Y0()V

    return-void
.end method

.method protected onPause()V
    .locals 1

    invoke-super {p0}, Landroidx/fragment/app/e;->onPause()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/miui/packageInstaller/a;->G:Z

    return-void
.end method

.method protected onResume()V
    .locals 3

    invoke-super {p0}, Landroidx/fragment/app/e;->onResume()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/packageInstaller/a;->G:Z

    iget v1, p0, Lcom/miui/packageInstaller/a;->E:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    const-string v1, "activity"

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    const-string v2, "null cannot be cast to non-null type android.app.ActivityManager"

    invoke-static {v1, v2}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Landroid/app/ActivityManager;

    invoke-virtual {v1, v0}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningTaskInfo;

    iget v0, v0, Landroid/app/ActivityManager$RunningTaskInfo;->id:I

    iput v0, p0, Lcom/miui/packageInstaller/a;->E:I

    :cond_0
    sget-object v0, Lcom/miui/packageInstaller/a;->U:Lcom/miui/packageInstaller/a$a;

    invoke-virtual {v0, p0}, Lcom/miui/packageInstaller/a$a;->a(Lcom/miui/packageInstaller/a;)V

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "outState"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lmiuix/appcompat/app/j;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget v0, p0, Lcom/miui/packageInstaller/a;->A:I

    const-string v1, "status"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method

.method protected onStart()V
    .locals 2

    invoke-super {p0}, Landroidx/fragment/app/e;->onStart()V

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/packageInstaller/a;->M:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lf6/z;->f(Ljava/lang/Runnable;)V

    return-void
.end method

.method protected onStop()V
    .locals 4

    invoke-super {p0}, Lq2/b;->onStop()V

    iget-boolean v0, p0, Lcom/miui/packageInstaller/a;->J:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/packageInstaller/a;->M:Ljava/lang/Runnable;

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Lf6/z;->d(Ljava/lang/Runnable;J)V

    :cond_0
    return-void
.end method

.method public p()Lm5/e;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/a;->x:Lm5/e;

    return-object v0
.end method

.method public final p1()Z
    .locals 1

    iget v0, p0, Lcom/miui/packageInstaller/a;->A:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public q(Ls5/l;II)V
    .locals 6

    iput p2, p0, Lcom/miui/packageInstaller/a;->S:I

    instance-of p1, p1, Ls5/n;

    if-eqz p1, :cond_5

    const/4 v0, 0x1

    if-ne v0, p2, :cond_0

    iget-object p3, p0, Lcom/miui/packageInstaller/a;->v:Lcom/miui/packageInstaller/ui/InstallerActionBar;

    if-eqz p3, :cond_5

    const v0, 0x7f110025

    :goto_0
    invoke-virtual {p0, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Lcom/miui/packageInstaller/ui/InstallerActionBar;->setProgressText(Ljava/lang/CharSequence;)V

    goto :goto_3

    :cond_0
    const/4 v0, 0x5

    if-ne v0, p2, :cond_3

    iget-object v0, p0, Lcom/miui/packageInstaller/a;->u:Lcom/miui/packageInstaller/model/CloudParams;

    if-eqz v0, :cond_1

    iget-object v0, v0, Lcom/miui/packageInstaller/model/CloudParams;->appInfo:Lcom/miui/packageInstaller/model/MarketAppInfo;

    if-eqz v0, :cond_1

    iget-wide v0, v0, Lcom/miui/packageInstaller/model/MarketAppInfo;->apkSize:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/miui/packageInstaller/a;->u:Lcom/miui/packageInstaller/model/CloudParams;

    invoke-static {v0}, Lm8/i;->c(Ljava/lang/Object;)V

    iget-object v0, v0, Lcom/miui/packageInstaller/model/CloudParams;->appInfo:Lcom/miui/packageInstaller/model/MarketAppInfo;

    invoke-static {v0}, Lm8/i;->c(Ljava/lang/Object;)V

    iget-wide v0, v0, Lcom/miui/packageInstaller/model/MarketAppInfo;->apkSize:J

    goto :goto_2

    :cond_2
    const-wide/16 v0, 0x0

    :goto_2
    iget-object v2, p0, Lcom/miui/packageInstaller/a;->v:Lcom/miui/packageInstaller/ui/InstallerActionBar;

    if-eqz v2, :cond_5

    invoke-virtual {v2, p3, v0, v1}, Lcom/miui/packageInstaller/ui/InstallerActionBar;->a(IJ)V

    goto :goto_3

    :cond_3
    const/4 p3, 0x7

    if-eq p3, p2, :cond_4

    const/16 p3, 0x9

    if-ne p3, p2, :cond_5

    :cond_4
    iget-object p3, p0, Lcom/miui/packageInstaller/a;->v:Lcom/miui/packageInstaller/ui/InstallerActionBar;

    if-eqz p3, :cond_5

    const v0, 0x7f1100e9

    goto :goto_0

    :cond_5
    :goto_3
    const/16 p3, 0xc

    if-eq p2, p3, :cond_6

    goto :goto_4

    :cond_6
    if-eqz p1, :cond_7

    new-instance p1, Lp5/c;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v4, 0x3

    const/4 v5, 0x0

    move-object v0, p1

    move-object v3, p0

    invoke-direct/range {v0 .. v5}, Lp5/c;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;ILm8/g;)V

    const-string p2, "download_process"

    const-string p3, "download_finish"

    invoke-virtual {p1, p2, p3}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p1

    const-string p2, "download_source"

    const-string p3, "appstore"

    invoke-virtual {p1, p2, p3}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p1

    const-string p2, "download_finish_status"

    const-string p3, "success"

    invoke-virtual {p1, p2, p3}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p1

    invoke-virtual {p1}, Lp5/f;->c()Z

    :cond_7
    :goto_4
    return-void
.end method

.method public final q1()Z
    .locals 1

    iget-boolean v0, p0, Lcom/miui/packageInstaller/a;->D:Z

    return v0
.end method

.method public r1(Lcom/miui/packageInstaller/ui/InstallerActionBar;)V
    .locals 2

    invoke-direct {p0}, Lcom/miui/packageInstaller/a;->k1()I

    move-result p1

    iget-object v0, p0, Lcom/miui/packageInstaller/a;->v:Lcom/miui/packageInstaller/ui/InstallerActionBar;

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result v1

    if-eqz v1, :cond_0

    const v1, 0x7f0d0075

    goto :goto_0

    :cond_0
    const v1, 0x7f0d0074

    :goto_0
    invoke-virtual {v0, p1, v1}, Lcom/miui/packageInstaller/ui/InstallerActionBar;->b(II)V

    :cond_1
    return-void
.end method

.method public final s1(Ljava/lang/String;)Landroid/content/Intent;
    .locals 1

    invoke-virtual {p0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object p1

    return-object p1
.end method

.method public t1()V
    .locals 3

    new-instance v0, Lp5/b;

    const-string v1, "finish_btn"

    const-string v2, "button"

    invoke-direct {v0, v1, v2, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    invoke-virtual {p0}, Lmiuix/appcompat/app/j;->finish()V

    return-void
.end method

.method public u(Ls5/l;)V
    .locals 9

    instance-of v0, p1, Ls5/n;

    const/4 v1, 0x0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/miui/packageInstaller/a;->u:Lcom/miui/packageInstaller/model/CloudParams;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/miui/packageInstaller/model/CloudParams;->appInfo:Lcom/miui/packageInstaller/model/MarketAppInfo;

    if-eqz v0, :cond_0

    iget-wide v2, v0, Lcom/miui/packageInstaller/model/MarketAppInfo;->apkSize:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/miui/packageInstaller/a;->u:Lcom/miui/packageInstaller/model/CloudParams;

    invoke-static {v0}, Lm8/i;->c(Ljava/lang/Object;)V

    iget-object v0, v0, Lcom/miui/packageInstaller/model/CloudParams;->appInfo:Lcom/miui/packageInstaller/model/MarketAppInfo;

    invoke-static {v0}, Lm8/i;->c(Ljava/lang/Object;)V

    iget-wide v2, v0, Lcom/miui/packageInstaller/model/MarketAppInfo;->apkSize:J

    goto :goto_1

    :cond_1
    const-wide/16 v2, 0x0

    :goto_1
    iget-object v0, p0, Lcom/miui/packageInstaller/a;->v:Lcom/miui/packageInstaller/ui/InstallerActionBar;

    if-eqz v0, :cond_2

    const/4 v4, 0x0

    invoke-virtual {v0, v4, v2, v3}, Lcom/miui/packageInstaller/ui/InstallerActionBar;->a(IJ)V

    :cond_2
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/miui/packageInstaller/a;->w:J

    iget-object v0, p0, Lcom/miui/packageInstaller/a;->v:Lcom/miui/packageInstaller/ui/InstallerActionBar;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/miui/packageInstaller/ui/InstallerActionBar;->getMProgress()Lz5/j;

    move-result-object v0

    if-eqz v0, :cond_3

    new-instance v2, Lm5/v;

    invoke-direct {v2, p0, p1}, Lm5/v;-><init>(Lcom/miui/packageInstaller/a;Ls5/l;)V

    invoke-interface {v0, v2}, Lz5/j;->setClick(Landroid/view/View$OnClickListener;)V

    :cond_3
    new-instance p1, Lp5/c;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x3

    const/4 v8, 0x0

    move-object v3, p1

    move-object v6, p0

    invoke-direct/range {v3 .. v8}, Lp5/c;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;ILm8/g;)V

    const-string v0, "download_process"

    const-string v2, "download_start"

    invoke-virtual {p1, v0, v2}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p1

    const-string v0, "download_source"

    const-string v2, "appstore"

    invoke-virtual {p1, v0, v2}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p1

    invoke-virtual {p1}, Lp5/f;->c()Z

    goto :goto_2

    :cond_4
    iget-object p1, p0, Lcom/miui/packageInstaller/a;->v:Lcom/miui/packageInstaller/ui/InstallerActionBar;

    if-eqz p1, :cond_5

    const v0, 0x7f110025

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/miui/packageInstaller/ui/InstallerActionBar;->setProgressText(Ljava/lang/CharSequence;)V

    :cond_5
    :goto_2
    iget-object p1, p0, Lcom/miui/packageInstaller/a;->O:Landroid/view/View;

    if-nez p1, :cond_6

    const-string p1, "mBackIcon"

    invoke-static {p1}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_3

    :cond_6
    move-object v1, p1

    :goto_3
    const/16 p1, 0x8

    invoke-virtual {v1, p1}, Landroid/view/View;->setVisibility(I)V

    sget-object p1, Ld6/l0;->c:Ld6/l0$a;

    iget-object v0, p0, Lcom/miui/packageInstaller/a;->y:Lcom/miui/packageInstaller/model/ApkInfo;

    const-string v1, ""

    if-eqz v0, :cond_7

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/ApkInfo;->getPackageName()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_8

    :cond_7
    move-object v0, v1

    :cond_8
    iget-object v2, p0, Lcom/miui/packageInstaller/a;->y:Lcom/miui/packageInstaller/model/ApkInfo;

    if-eqz v2, :cond_a

    invoke-virtual {v2}, Lcom/miui/packageInstaller/model/ApkInfo;->getApkMd5()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_9

    goto :goto_4

    :cond_9
    move-object v1, v2

    :cond_a
    :goto_4
    invoke-virtual {p1, v0, v1}, Ld6/l0$a;->e(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
