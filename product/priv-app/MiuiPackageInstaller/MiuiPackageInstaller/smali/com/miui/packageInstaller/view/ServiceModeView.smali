.class public final Lcom/miui/packageInstaller/view/ServiceModeView;
.super Landroid/widget/LinearLayout;


# instance fields
.field private a:Landroidx/constraintlayout/widget/ConstraintLayout;

.field private b:Landroidx/constraintlayout/widget/ConstraintLayout;

.field private c:Landroid/view/View;

.field private d:Landroid/widget/ImageView;

.field private e:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-virtual {p0}, Lcom/miui/packageInstaller/view/ServiceModeView;->e()V

    return-void
.end method

.method public static synthetic a(Lcom/miui/packageInstaller/view/ServiceModeView;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/miui/packageInstaller/view/ServiceModeView;->g(Lcom/miui/packageInstaller/view/ServiceModeView;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic b(Lcom/miui/packageInstaller/view/ServiceModeView;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/miui/packageInstaller/view/ServiceModeView;->h(Lcom/miui/packageInstaller/view/ServiceModeView;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic c(Lcom/miui/packageInstaller/view/ServiceModeView;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/miui/packageInstaller/view/ServiceModeView;->j(Lcom/miui/packageInstaller/view/ServiceModeView;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic d(Lcom/miui/packageInstaller/view/ServiceModeView;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/miui/packageInstaller/view/ServiceModeView;->i(Lcom/miui/packageInstaller/view/ServiceModeView;Landroid/view/View;)V

    return-void
.end method

.method private static final g(Lcom/miui/packageInstaller/view/ServiceModeView;Landroid/view/View;)V
    .locals 4

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->getSecurityModeStyle(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "normal"

    invoke-static {p1, v0}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_4

    iget-object p1, p0, Lcom/miui/packageInstaller/view/ServiceModeView;->a:Landroidx/constraintlayout/widget/ConstraintLayout;

    const/4 v1, 0x0

    if-nez p1, :cond_0

    const-string p1, "clGeneralMode"

    invoke-static {p1}, Lm8/i;->s(Ljava/lang/String;)V

    move-object p1, v1

    :cond_0
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->setSelected(Z)V

    iget-object p1, p0, Lcom/miui/packageInstaller/view/ServiceModeView;->b:Landroidx/constraintlayout/widget/ConstraintLayout;

    if-nez p1, :cond_1

    const-string p1, "clElderMode"

    invoke-static {p1}, Lm8/i;->s(Ljava/lang/String;)V

    move-object p1, v1

    :cond_1
    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->setSelected(Z)V

    iget-object p1, p0, Lcom/miui/packageInstaller/view/ServiceModeView;->d:Landroid/widget/ImageView;

    if-nez p1, :cond_2

    const-string p1, "mGeneralArrowView"

    invoke-static {p1}, Lm8/i;->s(Ljava/lang/String;)V

    move-object p1, v1

    :cond_2
    invoke-virtual {p1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object p1, p0, Lcom/miui/packageInstaller/view/ServiceModeView;->e:Landroid/widget/ImageView;

    if-nez p1, :cond_3

    const-string p1, "mElderArrowView"

    invoke-static {p1}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    move-object v1, p1

    :goto_0
    const/4 p1, 0x4

    invoke-virtual {v1, p1}, Landroid/widget/ImageView;->setVisibility(I)V

    new-instance p1, Lp5/b;

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "null cannot be cast to non-null type com.miui.packageInstaller.analytics.IPage"

    invoke-static {v1, v2}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lo5/a;

    const-string v2, "protect_mode_normal_btn"

    const-string v3, "button"

    invoke-direct {p1, v2, v3, v1}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p1}, Lp5/f;->c()Z

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-static {p0, v0}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->setSecurityModeStyle(Landroid/content/Context;Ljava/lang/String;)V

    :cond_4
    return-void
.end method

.method private static final h(Lcom/miui/packageInstaller/view/ServiceModeView;Landroid/view/View;)V
    .locals 4

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->getSecurityModeStyle(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "elder"

    invoke-static {p1, v0}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_4

    iget-object p1, p0, Lcom/miui/packageInstaller/view/ServiceModeView;->a:Landroidx/constraintlayout/widget/ConstraintLayout;

    const/4 v1, 0x0

    if-nez p1, :cond_0

    const-string p1, "clGeneralMode"

    invoke-static {p1}, Lm8/i;->s(Ljava/lang/String;)V

    move-object p1, v1

    :cond_0
    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->setSelected(Z)V

    iget-object p1, p0, Lcom/miui/packageInstaller/view/ServiceModeView;->b:Landroidx/constraintlayout/widget/ConstraintLayout;

    if-nez p1, :cond_1

    const-string p1, "clElderMode"

    invoke-static {p1}, Lm8/i;->s(Ljava/lang/String;)V

    move-object p1, v1

    :cond_1
    const/4 v3, 0x1

    invoke-virtual {p1, v3}, Landroid/view/ViewGroup;->setSelected(Z)V

    iget-object p1, p0, Lcom/miui/packageInstaller/view/ServiceModeView;->e:Landroid/widget/ImageView;

    if-nez p1, :cond_2

    const-string p1, "mElderArrowView"

    invoke-static {p1}, Lm8/i;->s(Ljava/lang/String;)V

    move-object p1, v1

    :cond_2
    invoke-virtual {p1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object p1, p0, Lcom/miui/packageInstaller/view/ServiceModeView;->d:Landroid/widget/ImageView;

    if-nez p1, :cond_3

    const-string p1, "mGeneralArrowView"

    invoke-static {p1}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    move-object v1, p1

    :goto_0
    const/4 p1, 0x4

    invoke-virtual {v1, p1}, Landroid/widget/ImageView;->setVisibility(I)V

    new-instance p1, Lp5/b;

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "null cannot be cast to non-null type com.miui.packageInstaller.analytics.IPage"

    invoke-static {v1, v2}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lo5/a;

    const-string v2, "protect_mode_old_btn"

    const-string v3, "button"

    invoke-direct {p1, v2, v3, v1}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p1}, Lp5/f;->c()Z

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-static {p0, v0}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->setSecurityModeStyle(Landroid/content/Context;Ljava/lang/String;)V

    :cond_4
    return-void
.end method

.method private static final i(Lcom/miui/packageInstaller/view/ServiceModeView;Landroid/view/View;)V
    .locals 2

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Landroid/content/Intent;

    invoke-direct {p1}, Landroid/content/Intent;-><init>()V

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    sget-object v0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity$b;->a:Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity$b;

    const-string v1, "mode_type"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-virtual {p0, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private static final j(Lcom/miui/packageInstaller/view/ServiceModeView;Landroid/view/View;)V
    .locals 2

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Landroid/content/Intent;

    invoke-direct {p1}, Landroid/content/Intent;-><init>()V

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    sget-object v0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity$b;->b:Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity$b;

    const-string v1, "mode_type"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-virtual {p0, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method public final e()V
    .locals 2

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0d0092

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    invoke-virtual {p0}, Lcom/miui/packageInstaller/view/ServiceModeView;->f()V

    return-void
.end method

.method public final f()V
    .locals 10

    const v0, 0x7f0a0176

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.fl_general_mode)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroidx/constraintlayout/widget/ConstraintLayout;

    iput-object v0, p0, Lcom/miui/packageInstaller/view/ServiceModeView;->a:Landroidx/constraintlayout/widget/ConstraintLayout;

    const v0, 0x7f0a0175

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.fl_elder_mode)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroidx/constraintlayout/widget/ConstraintLayout;

    iput-object v0, p0, Lcom/miui/packageInstaller/view/ServiceModeView;->b:Landroidx/constraintlayout/widget/ConstraintLayout;

    const v0, 0x7f0a03dc

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.v_safe_mode_bottom_line)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/miui/packageInstaller/view/ServiceModeView;->c:Landroid/view/View;

    const v0, 0x7f0a01d1

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.iv_general_mode_arrow)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/miui/packageInstaller/view/ServiceModeView;->d:Landroid/widget/ImageView;

    const v0, 0x7f0a01cd

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.iv_elder_mode_arrow)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/miui/packageInstaller/view/ServiceModeView;->e:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/miui/packageInstaller/view/ServiceModeView;->c:Landroid/view/View;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const-string v0, "mBottomLineView"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_0
    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->getSecurityModeStyle(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "elder"

    invoke-static {v2, v3}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    const/16 v2, 0x8

    :goto_0
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->getSecurityModeStyle(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "normal"

    invoke-static {v0, v2}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const-string v2, "mElderArrowView"

    const-string v3, "mGeneralArrowView"

    const/4 v4, 0x4

    const-string v5, "clGeneralMode"

    const-string v6, "clElderMode"

    const/4 v7, 0x1

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/miui/packageInstaller/view/ServiceModeView;->a:Landroidx/constraintlayout/widget/ConstraintLayout;

    if-nez v0, :cond_2

    invoke-static {v5}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_2
    invoke-virtual {v0, v7}, Landroid/view/ViewGroup;->setSelected(Z)V

    iget-object v0, p0, Lcom/miui/packageInstaller/view/ServiceModeView;->e:Landroid/widget/ImageView;

    if-nez v0, :cond_5

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/miui/packageInstaller/view/ServiceModeView;->b:Landroidx/constraintlayout/widget/ConstraintLayout;

    if-nez v0, :cond_4

    invoke-static {v6}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_4
    invoke-virtual {v0, v7}, Landroid/view/ViewGroup;->setSelected(Z)V

    iget-object v0, p0, Lcom/miui/packageInstaller/view/ServiceModeView;->d:Landroid/widget/ImageView;

    if-nez v0, :cond_5

    invoke-static {v3}, Lm8/i;->s(Ljava/lang/String;)V

    :goto_1
    move-object v0, v1

    :cond_5
    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    new-instance v0, Lp5/g;

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string v7, "null cannot be cast to non-null type com.miui.packageInstaller.analytics.IPage"

    invoke-static {v4, v7}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v4, Lo5/a;

    const-string v8, "protect_mode_normal_btn"

    const-string v9, "button"

    invoke-direct {v0, v8, v9, v4}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    new-instance v0, Lp5/g;

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v7}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v4, Lo5/a;

    const-string v7, "protect_mode_old_btn"

    invoke-direct {v0, v7, v9, v4}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    iget-object v0, p0, Lcom/miui/packageInstaller/view/ServiceModeView;->a:Landroidx/constraintlayout/widget/ConstraintLayout;

    if-nez v0, :cond_6

    invoke-static {v5}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_6
    new-instance v4, Lcom/miui/packageInstaller/view/k;

    invoke-direct {v4, p0}, Lcom/miui/packageInstaller/view/k;-><init>(Lcom/miui/packageInstaller/view/ServiceModeView;)V

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/view/ServiceModeView;->b:Landroidx/constraintlayout/widget/ConstraintLayout;

    if-nez v0, :cond_7

    invoke-static {v6}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_7
    new-instance v4, Lcom/miui/packageInstaller/view/l;

    invoke-direct {v4, p0}, Lcom/miui/packageInstaller/view/l;-><init>(Lcom/miui/packageInstaller/view/ServiceModeView;)V

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/view/ServiceModeView;->d:Landroid/widget/ImageView;

    if-nez v0, :cond_8

    invoke-static {v3}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_8
    new-instance v3, Lcom/miui/packageInstaller/view/n;

    invoke-direct {v3, p0}, Lcom/miui/packageInstaller/view/n;-><init>(Lcom/miui/packageInstaller/view/ServiceModeView;)V

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/view/ServiceModeView;->e:Landroid/widget/ImageView;

    if-nez v0, :cond_9

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_2

    :cond_9
    move-object v1, v0

    :goto_2
    new-instance v0, Lcom/miui/packageInstaller/view/m;

    invoke-direct {v0, p0}, Lcom/miui/packageInstaller/view/m;-><init>(Lcom/miui/packageInstaller/view/ServiceModeView;)V

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
