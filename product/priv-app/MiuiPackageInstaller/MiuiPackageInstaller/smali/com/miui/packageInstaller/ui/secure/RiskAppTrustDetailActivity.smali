.class public final Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;
.super Lq2/b;

# interfaces
.implements Lm5/t;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity$a;
    }
.end annotation


# static fields
.field public static final C:Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity$a;


# instance fields
.field private A:Lmiuix/recyclerview/widget/RecyclerView;

.field private B:Lj6/b;

.field private u:Landroid/widget/TextView;

.field private v:Landroid/widget/Button;

.field private w:Landroid/widget/Button;

.field private x:Landroid/widget/TextView;

.field private y:Landroid/view/View;

.field private z:Lr6/c;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity$a;-><init>(Lm8/g;)V

    sput-object v0, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->C:Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lq2/b;-><init>()V

    return-void
.end method

.method public static synthetic J0(Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;)V
    .locals 0

    invoke-static {p0}, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->R0(Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;)V

    return-void
.end method

.method public static synthetic K0(Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->X0(Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic L0(Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->W0(Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic M0(Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;)V
    .locals 0

    invoke-static {p0}, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->c1(Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;)V

    return-void
.end method

.method public static synthetic N0(Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->Z0(Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic O0(Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->a1(Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;Landroid/view/View;)V

    return-void
.end method

.method public static final synthetic P0(Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->b1()V

    return-void
.end method

.method private final Q0()V
    .locals 4

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->z:Lr6/c;

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lr6/c;->v(Ljava/lang/Integer;)V

    :goto_0
    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->B:Lj6/b;

    const-string v1, "mAdapter"

    const/4 v2, 0x0

    if-nez v0, :cond_1

    invoke-static {v1}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v2

    :cond_1
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lj6/b;->T(I)Lm6/a;

    move-result-object v0

    instance-of v0, v0, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppTrustDownloadAppInfoViewObject;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->B:Lj6/b;

    if-nez v0, :cond_2

    invoke-static {v1}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v2

    :cond_2
    invoke-virtual {v0, v3}, Lj6/b;->T(I)Lm6/a;

    move-result-object v0

    invoke-virtual {v0}, Lm6/a;->m()V

    :cond_3
    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->v:Landroid/widget/Button;

    if-nez v0, :cond_4

    const-string v0, "btnFirstButton"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v2

    :cond_4
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->u:Landroid/widget/TextView;

    if-nez v0, :cond_5

    const-string v0, "tvRiskAppTips"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v2

    :cond_5
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->x:Landroid/widget/TextView;

    if-nez v0, :cond_6

    const-string v0, "tvBrowserTips"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v2

    :cond_6
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->w:Landroid/widget/Button;

    if-nez v0, :cond_7

    const-string v0, "btnSecondButton"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_1

    :cond_7
    move-object v2, v0

    :goto_1
    const v0, 0x7f1102e0

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setResult(I)V

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object v0

    new-instance v1, Ld6/t;

    invoke-direct {v1, p0}, Ld6/t;-><init>(Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;)V

    invoke-virtual {v0, v1}, Lf6/z;->g(Ljava/lang/Runnable;)V

    return-void
.end method

.method private static final R0(Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;)V
    .locals 3

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->z:Lr6/c;

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lr6/c;->w(Ljava/lang/Long;)V

    :goto_0
    sget-object v0, Lp6/a;->c:Lp6/a;

    iget-object p0, p0, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->z:Lr6/c;

    invoke-virtual {v0, p0}, Lp6/a;->j(Ljava/lang/Object;)V

    return-void
.end method

.method private final S0()Ljava/lang/String;
    .locals 5

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->z:Lr6/c;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lr6/c;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v0, :cond_2

    invoke-static {v0}, Lu8/g;->l(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    move v0, v2

    goto :goto_2

    :cond_2
    :goto_1
    move v0, v3

    :goto_2
    if-nez v0, :cond_4

    const v0, 0x7f1102e5

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->z:Lr6/c;

    if-eqz v4, :cond_3

    invoke-virtual {v4}, Lr6/c;->a()Ljava/lang/String;

    move-result-object v1

    :cond_3
    aput-object v1, v3, v2

    invoke-virtual {p0, v0, v3}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "{\n            getString(\u2026o?.displayName)\n        }"

    goto :goto_3

    :cond_4
    const v0, 0x7f1102e6

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "{\n            getString(\u2026t_tips_default)\n        }"

    :goto_3
    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final T0()V
    .locals 6

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->u:Landroid/widget/TextView;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const-string v0, "tvRiskAppTips"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_0
    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->S0()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->A:Lmiuix/recyclerview/widget/RecyclerView;

    const-string v2, "mRecyclerView"

    if-nez v0, :cond_1

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_1
    new-instance v3, Landroidx/recyclerview/widget/LinearLayoutManager;

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-direct {v3, p0, v4, v5}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    invoke-virtual {v0, v3}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$o;)V

    new-instance v0, Lj6/b;

    iget-object v3, p0, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->A:Lmiuix/recyclerview/widget/RecyclerView;

    if-nez v3, :cond_2

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v3, v1

    :cond_2
    invoke-direct {v0, v3}, Lj6/b;-><init>(Landroidx/recyclerview/widget/RecyclerView;)V

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->B:Lj6/b;

    iget-object v2, p0, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->z:Lr6/c;

    if-eqz v2, :cond_3

    new-instance v3, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppTrustDownloadAppInfoViewObject;

    invoke-direct {v3, p0, v2, v1, v1}, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppTrustDownloadAppInfoViewObject;-><init>(Landroid/content/Context;Lr6/c;Ll6/c;Lm6/b;)V

    invoke-virtual {v0, v3}, Lj6/b;->J(Lm6/a;)I

    :cond_3
    return-void
.end method

.method private final U0()V
    .locals 2

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "apk_info"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    const-string v1, "null cannot be cast to non-null type com.miui.security.model.SecurityModeRiskAppInfo"

    invoke-static {v0, v1}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lr6/c;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->z:Lr6/c;

    return-void
.end method

.method private final V0()V
    .locals 3

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->v:Landroid/widget/Button;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const-string v0, "btnFirstButton"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_0
    new-instance v2, Ld6/q;

    invoke-direct {v2, p0}, Ld6/q;-><init>(Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->w:Landroid/widget/Button;

    if-nez v0, :cond_1

    const-string v0, "btnSecondButton"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move-object v1, v0

    :goto_0
    new-instance v0, Ld6/p;

    invoke-direct {v0, p0}, Ld6/p;-><init>(Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;)V

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private static final W0(Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;Landroid/view/View;)V
    .locals 3

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->z:Lr6/c;

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lr6/c;->i()Ljava/lang/Integer;

    move-result-object p1

    goto :goto_0

    :cond_0
    move-object p1, v0

    :goto_0
    const/4 v1, 0x1

    if-nez p1, :cond_1

    goto :goto_2

    :cond_1
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v2, v1, :cond_2

    :goto_1
    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->Q0()V

    goto :goto_4

    :cond_2
    :goto_2
    const/4 v1, 0x2

    if-nez p1, :cond_3

    goto :goto_3

    :cond_3
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    if-eq p1, v1, :cond_5

    :goto_3
    new-instance p1, Lp5/b;

    const-string v1, "install_forbid_btn"

    const-string v2, "button"

    invoke-direct {p1, v1, v2, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    iget-object v1, p0, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->z:Lr6/c;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lr6/c;->f()Ljava/lang/String;

    move-result-object v0

    :cond_4
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "related_file_name"

    invoke-virtual {p1, v1, v0}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p1

    invoke-virtual {p1}, Lp5/f;->c()Z

    goto :goto_1

    :cond_5
    :goto_4
    return-void
.end method

.method private static final X0(Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;Landroid/view/View;)V
    .locals 6

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->z:Lr6/c;

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lr6/c;->i()Ljava/lang/Integer;

    move-result-object p1

    goto :goto_0

    :cond_0
    move-object p1, v0

    :goto_0
    const/4 v1, 0x1

    const-string v2, "finish_and_back"

    const-string v3, "related_file_name"

    const-string v4, "button"

    if-nez p1, :cond_1

    goto :goto_2

    :cond_1
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-ne v5, v1, :cond_3

    new-instance p1, Lp5/b;

    invoke-direct {p1, v2, v4, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    iget-object p0, p0, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->z:Lr6/c;

    if-eqz p0, :cond_2

    :goto_1
    invoke-virtual {p0}, Lr6/c;->f()Ljava/lang/String;

    move-result-object v0

    :cond_2
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, v3, p0}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p0

    invoke-virtual {p0}, Lp5/f;->c()Z

    invoke-static {}, Lm5/b;->l()V

    goto :goto_4

    :cond_3
    :goto_2
    const/4 v1, 0x2

    if-nez p1, :cond_4

    goto :goto_3

    :cond_4
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    if-ne p1, v1, :cond_5

    new-instance p1, Lp5/b;

    invoke-direct {p1, v2, v4, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    iget-object p0, p0, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->z:Lr6/c;

    if-eqz p0, :cond_2

    goto :goto_1

    :cond_5
    :goto_3
    new-instance p1, Lp5/b;

    const-string v1, "trust_current_btn"

    invoke-direct {p1, v1, v4, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    iget-object v1, p0, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->z:Lr6/c;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lr6/c;->f()Ljava/lang/String;

    move-result-object v0

    :cond_6
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p1

    invoke-virtual {p1}, Lp5/f;->c()Z

    new-instance p1, Lx5/d;

    invoke-direct {p1, p0}, Lx5/d;-><init>(Landroid/app/Activity;)V

    new-instance v0, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity$b;

    invoke-direct {v0, p0}, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity$b;-><init>(Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;)V

    invoke-virtual {p1, v0}, Lx5/d;->g(Ll8/p;)V

    :goto_4
    return-void
.end method

.method private final Y0()V
    .locals 16

    move-object/from16 v0, p0

    const v1, 0x7f0d0149

    invoke-virtual {v0, v1}, Lmiuix/appcompat/app/j;->setContentView(I)V

    const v1, 0x7f0a0216

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const-string v2, "findViewById(R.id.main_content)"

    invoke-static {v1, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lmiuix/recyclerview/widget/RecyclerView;

    iput-object v1, v0, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->A:Lmiuix/recyclerview/widget/RecyclerView;

    const v1, 0x7f0a02ce

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const-string v2, "findViewById(R.id.risk_app_tips)"

    invoke-static {v1, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v0, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->u:Landroid/widget/TextView;

    const v1, 0x7f0a016c

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const-string v2, "findViewById(R.id.first_button)"

    invoke-static {v1, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Landroid/widget/Button;

    iput-object v1, v0, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->v:Landroid/widget/Button;

    const v1, 0x7f0a02f4

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const-string v2, "findViewById(R.id.second_button)"

    invoke-static {v1, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Landroid/widget/Button;

    iput-object v1, v0, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->w:Landroid/widget/Button;

    const v1, 0x7f0a02cd

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const-string v2, "findViewById(R.id.risk_app_browser_tips)"

    invoke-static {v1, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v0, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->x:Landroid/widget/TextView;

    const v1, 0x7f0a0096

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const-string v2, "findViewById(R.id.bottom_space_line)"

    invoke-static {v1, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v1, v0, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->y:Landroid/view/View;

    iget-object v1, v0, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->z:Lr6/c;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lr6/c;->i()Ljava/lang/Integer;

    move-result-object v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    const/4 v3, 0x1

    const-string v4, "finish_and_back"

    const v5, 0x7f1102e0

    const-string v6, "install_forbid_btn"

    const-string v7, "vBottomSpaceLine"

    const-string v8, "tvBrowserTips"

    const-string v9, "tvRiskAppTips"

    const-string v10, "btnFirstButton"

    const-string v11, "btnSecondButton"

    const/16 v12, 0x8

    const-string v13, "related_file_name"

    const-string v14, "button"

    const/4 v15, 0x0

    if-nez v1, :cond_1

    goto/16 :goto_4

    :cond_1
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v2, v3, :cond_9

    iget-object v1, v0, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->v:Landroid/widget/Button;

    if-nez v1, :cond_2

    invoke-static {v10}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v1, 0x0

    :cond_2
    invoke-virtual {v1, v15}, Landroid/widget/Button;->setVisibility(I)V

    new-instance v1, Lp5/g;

    invoke-direct {v1, v6, v14, v0}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    iget-object v2, v0, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->z:Lr6/c;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lr6/c;->f()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_3
    const/4 v2, 0x0

    :goto_1
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v13, v2}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object v1

    invoke-virtual {v1}, Lp5/f;->c()Z

    iget-object v1, v0, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->w:Landroid/widget/Button;

    if-nez v1, :cond_4

    invoke-static {v11}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v1, 0x0

    :cond_4
    invoke-virtual {v0, v5}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    new-instance v1, Lp5/g;

    invoke-direct {v1, v4, v14, v0}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    iget-object v2, v0, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->z:Lr6/c;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Lr6/c;->f()Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    :cond_5
    const/4 v2, 0x0

    :goto_2
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v13, v2}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object v1

    invoke-virtual {v1}, Lp5/f;->c()Z

    iget-object v1, v0, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->u:Landroid/widget/TextView;

    if-nez v1, :cond_6

    invoke-static {v9}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v1, 0x0

    :cond_6
    invoke-virtual {v1, v15}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, v0, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->x:Landroid/widget/TextView;

    if-nez v1, :cond_7

    invoke-static {v8}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v1, 0x0

    :cond_7
    invoke-virtual {v1, v15}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, v0, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->y:Landroid/view/View;

    if-nez v1, :cond_8

    invoke-static {v7}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v2, 0x0

    goto :goto_3

    :cond_8
    move-object v2, v1

    :goto_3
    invoke-virtual {v2, v12}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_c

    :cond_9
    :goto_4
    const/4 v2, 0x2

    if-nez v1, :cond_a

    goto/16 :goto_8

    :cond_a
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v1, v2, :cond_12

    iget-object v1, v0, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->v:Landroid/widget/Button;

    if-nez v1, :cond_b

    invoke-static {v10}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v1, 0x0

    :cond_b
    invoke-virtual {v1, v12}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v1, v0, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->w:Landroid/widget/Button;

    if-nez v1, :cond_c

    invoke-static {v11}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v1, 0x0

    :cond_c
    invoke-virtual {v0, v5}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    new-instance v1, Lp5/g;

    invoke-direct {v1, v4, v14, v0}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    iget-object v2, v0, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->z:Lr6/c;

    if-eqz v2, :cond_d

    invoke-virtual {v2}, Lr6/c;->f()Ljava/lang/String;

    move-result-object v2

    goto :goto_5

    :cond_d
    const/4 v2, 0x0

    :goto_5
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v13, v2}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object v1

    invoke-virtual {v1}, Lp5/f;->c()Z

    iget-object v1, v0, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->u:Landroid/widget/TextView;

    if-nez v1, :cond_e

    invoke-static {v9}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v1, 0x0

    :cond_e
    invoke-virtual {v1, v12}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, v0, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->x:Landroid/widget/TextView;

    if-nez v1, :cond_f

    invoke-static {v8}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v1, 0x0

    :cond_f
    invoke-virtual {v1, v12}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, v0, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->y:Landroid/view/View;

    if-nez v1, :cond_10

    invoke-static {v7}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v1, 0x0

    :cond_10
    invoke-virtual {v1, v15}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, v0, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->w:Landroid/widget/Button;

    if-nez v1, :cond_11

    invoke-static {v11}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v2, 0x0

    goto :goto_6

    :cond_11
    move-object v2, v1

    :goto_6
    new-instance v1, Ld6/r;

    invoke-direct {v1, v0}, Ld6/r;-><init>(Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;)V

    :goto_7
    invoke-virtual {v2, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_c

    :cond_12
    :goto_8
    iget-object v1, v0, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->v:Landroid/widget/Button;

    if-nez v1, :cond_13

    invoke-static {v10}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v1, 0x0

    :cond_13
    invoke-virtual {v1, v15}, Landroid/widget/Button;->setVisibility(I)V

    new-instance v1, Lp5/g;

    invoke-direct {v1, v6, v14, v0}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    iget-object v2, v0, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->z:Lr6/c;

    if-eqz v2, :cond_14

    invoke-virtual {v2}, Lr6/c;->f()Ljava/lang/String;

    move-result-object v2

    goto :goto_9

    :cond_14
    const/4 v2, 0x0

    :goto_9
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v13, v2}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object v1

    invoke-virtual {v1}, Lp5/f;->c()Z

    new-instance v1, Lp5/g;

    const-string v2, "trust_current_btn"

    invoke-direct {v1, v2, v14, v0}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    iget-object v2, v0, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->z:Lr6/c;

    if-eqz v2, :cond_15

    invoke-virtual {v2}, Lr6/c;->f()Ljava/lang/String;

    move-result-object v2

    goto :goto_a

    :cond_15
    const/4 v2, 0x0

    :goto_a
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v13, v2}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object v1

    invoke-virtual {v1}, Lp5/f;->c()Z

    iget-object v1, v0, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->w:Landroid/widget/Button;

    if-nez v1, :cond_16

    invoke-static {v11}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v1, 0x0

    :cond_16
    const v2, 0x7f1102e3

    invoke-virtual {v0, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, v0, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->u:Landroid/widget/TextView;

    if-nez v1, :cond_17

    invoke-static {v9}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v1, 0x0

    :cond_17
    invoke-virtual {v1, v15}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, v0, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->x:Landroid/widget/TextView;

    if-nez v1, :cond_18

    invoke-static {v8}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v1, 0x0

    :cond_18
    invoke-virtual {v1, v12}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, v0, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->y:Landroid/view/View;

    if-nez v1, :cond_19

    invoke-static {v7}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v1, 0x0

    :cond_19
    invoke-virtual {v1, v15}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, v0, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->w:Landroid/widget/Button;

    if-nez v1, :cond_1a

    invoke-static {v11}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v2, 0x0

    goto :goto_b

    :cond_1a
    move-object v2, v1

    :goto_b
    new-instance v1, Ld6/s;

    invoke-direct {v1, v0}, Ld6/s;-><init>(Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;)V

    goto/16 :goto_7

    :goto_c
    return-void
.end method

.method private static final Z0(Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;Landroid/view/View;)V
    .locals 2

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Lp5/b;

    const-string v0, "finish_and_back"

    const-string v1, "button"

    invoke-direct {p1, v0, v1, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    iget-object p0, p0, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->z:Lr6/c;

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lr6/c;->f()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    const-string v0, "related_file_name"

    invoke-virtual {p1, v0, p0}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p0

    invoke-virtual {p0}, Lp5/f;->c()Z

    invoke-static {}, Lm5/b;->l()V

    return-void
.end method

.method private static final a1(Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;Landroid/view/View;)V
    .locals 2

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Lp5/b;

    const-string v0, "trust_current_btn"

    const-string v1, "button"

    invoke-direct {p1, v0, v1, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->z:Lr6/c;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lr6/c;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "related_file_name"

    invoke-virtual {p1, v1, v0}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p1

    invoke-virtual {p1}, Lp5/f;->c()Z

    new-instance p1, Lx5/d;

    invoke-direct {p1, p0}, Lx5/d;-><init>(Landroid/app/Activity;)V

    new-instance v0, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity$c;

    invoke-direct {v0, p0}, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity$c;-><init>(Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;)V

    invoke-virtual {p1, v0}, Lx5/d;->g(Ll8/p;)V

    return-void
.end method

.method private final b1()V
    .locals 4

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->z:Lr6/c;

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lr6/c;->v(Ljava/lang/Integer;)V

    :goto_0
    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->B:Lj6/b;

    const-string v1, "mAdapter"

    const/4 v2, 0x0

    if-nez v0, :cond_1

    invoke-static {v1}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v2

    :cond_1
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lj6/b;->T(I)Lm6/a;

    move-result-object v0

    instance-of v0, v0, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppTrustDownloadAppInfoViewObject;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->B:Lj6/b;

    if-nez v0, :cond_2

    invoke-static {v1}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v2

    :cond_2
    invoke-virtual {v0, v3}, Lj6/b;->T(I)Lm6/a;

    move-result-object v0

    invoke-virtual {v0}, Lm6/a;->m()V

    :cond_3
    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->w:Landroid/widget/Button;

    if-nez v0, :cond_4

    const-string v0, "btnSecondButton"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    move-object v2, v0

    :goto_1
    const v0, 0x7f1102e0

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setResult(I)V

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object v0

    new-instance v1, Ld6/u;

    invoke-direct {v1, p0}, Ld6/u;-><init>(Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;)V

    invoke-virtual {v0, v1}, Lf6/z;->g(Ljava/lang/Runnable;)V

    return-void
.end method

.method private static final c1(Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;)V
    .locals 7

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->z:Lr6/c;

    if-eqz v3, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v3, v0}, Lr6/c;->w(Ljava/lang/Long;)V

    sget-object v0, Lp6/a;->c:Lp6/a;

    iget-object v1, p0, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->z:Lr6/c;

    invoke-virtual {v0, v1}, Lp6/a;->j(Ljava/lang/Object;)V

    sget-object v1, La;->a:La$a;

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v2, p0

    invoke-static/range {v1 .. v6}, La$a;->f(La$a;Lm5/t;Lr6/c;Ljava/lang/String;ILjava/lang/Object;)Z

    :cond_0
    return-void
.end method


# virtual methods
.method public E0()Ljava/lang/String;
    .locals 1

    const-string v0, "install_authorize"

    return-object v0
.end method

.method public G0()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iget-object v1, p0, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->z:Lr6/c;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lr6/c;->f()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v2, "related_file_name"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    :cond_0
    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Lq2/b;->onCreate(Landroid/os/Bundle;)V

    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->U0()V

    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->Y0()V

    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->V0()V

    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;->T0()V

    return-void
.end method
