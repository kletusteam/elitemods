.class public final Lcom/miui/packageInstaller/ui/listcomponets/AppPIcViewObject$ViewHolder;
.super Landroidx/recyclerview/widget/RecyclerView$d0;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/packageInstaller/ui/listcomponets/AppPIcViewObject;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ViewHolder"
.end annotation


# instance fields
.field private imageP:Landroid/widget/ImageView;

.field private layoutParams:Landroid/view/ViewGroup$LayoutParams;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    const-string v0, "itemView"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$d0;-><init>(Landroid/view/View;)V

    const v0, 0x7f0a02a6

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppPIcViewObject$ViewHolder;->imageP:Landroid/widget/ImageView;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppPIcViewObject$ViewHolder;->layoutParams:Landroid/view/ViewGroup$LayoutParams;

    const/4 p1, 0x1

    new-array p1, p1, [Landroid/view/View;

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppPIcViewObject$ViewHolder;->imageP:Landroid/widget/ImageView;

    const/4 v1, 0x0

    aput-object v0, p1, v1

    invoke-static {p1}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object p1

    invoke-interface {p1}, Lmiuix/animation/e;->c()Lmiuix/animation/j;

    move-result-object p1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppPIcViewObject$ViewHolder;->imageP:Landroid/widget/ImageView;

    new-array v1, v1, [Lc9/a;

    invoke-interface {p1, v0, v1}, Lmiuix/animation/j;->z(Landroid/view/View;[Lc9/a;)V

    return-void
.end method


# virtual methods
.method public final getImageP()Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppPIcViewObject$ViewHolder;->imageP:Landroid/widget/ImageView;

    return-object v0
.end method

.method public final getLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppPIcViewObject$ViewHolder;->layoutParams:Landroid/view/ViewGroup$LayoutParams;

    return-object v0
.end method

.method public final setImageP(Landroid/widget/ImageView;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppPIcViewObject$ViewHolder;->imageP:Landroid/widget/ImageView;

    return-void
.end method

.method public final setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppPIcViewObject$ViewHolder;->layoutParams:Landroid/view/ViewGroup$LayoutParams;

    return-void
.end method
