.class final Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity$b;
.super Lf8/k;

# interfaces
.implements Ll8/p;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;->A2()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf8/k;",
        "Ll8/p<",
        "Lv8/e0;",
        "Ld8/d<",
        "-",
        "La8/v;",
        ">;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation runtime Lf8/f;
    c = "com.miui.packageInstaller.ui.ChildAccountInstallerActivity$getPassportIdentifyUrlAndJump$1"
    f = "ChildAccountInstallerActivity.kt"
    l = {
        0xbf
    }
    m = "invokeSuspend"
.end annotation


# instance fields
.field e:I

.field final synthetic f:Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;


# direct methods
.method constructor <init>(Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;Ld8/d;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;",
            "Ld8/d<",
            "-",
            "Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity$b;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity$b;->f:Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;

    const/4 p1, 0x2

    invoke-direct {p0, p1, p2}, Lf8/k;-><init>(ILd8/d;)V

    return-void
.end method


# virtual methods
.method public final b(Ljava/lang/Object;Ld8/d;)Ld8/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ld8/d<",
            "*>;)",
            "Ld8/d<",
            "La8/v;",
            ">;"
        }
    .end annotation

    new-instance p1, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity$b;

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity$b;->f:Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;

    invoke-direct {p1, v0, p2}, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity$b;-><init>(Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;Ld8/d;)V

    return-object p1
.end method

.method public bridge synthetic g(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lv8/e0;

    check-cast p2, Ld8/d;

    invoke-virtual {p0, p1, p2}, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity$b;->q(Lv8/e0;Ld8/d;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final n(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    invoke-static {}, Le8/b;->c()Ljava/lang/Object;

    move-result-object v0

    iget v1, p0, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity$b;->e:I

    const/4 v2, 0x0

    const v3, 0x7f1103ba

    const/4 v4, 0x1

    if-eqz v1, :cond_1

    if-ne v1, v4, :cond_0

    :try_start_0
    invoke-static {p1}, La8/n;->b(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    goto :goto_2

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    invoke-static {p1}, La8/n;->b(Ljava/lang/Object;)V

    :try_start_1
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    const-class v1, Lu5/b;

    invoke-static {v1}, Lu5/k;->f(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lu5/b;

    iput v4, p0, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity$b;->e:I

    invoke-interface {v1, p1, p0}, Lu5/b;->d(Ljava/util/Map;Ld8/d;)Ljava/lang/Object;

    move-result-object p1

    if-ne p1, v0, :cond_2

    return-object v0

    :cond_2
    :goto_0
    check-cast p1, Lgc/t;

    invoke-virtual {p1}, Lgc/t;->d()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lgc/t;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/miui/packageInstaller/model/PassportIdentityUrlModel;

    if-eqz p1, :cond_5

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity$b;->f:Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;

    invoke-virtual {p1}, Lcom/miui/packageInstaller/model/PassportIdentityUrlModel;->getCode()I

    move-result v1

    const/16 v5, 0xc8

    if-ne v1, v5, :cond_3

    invoke-virtual {p1}, Lcom/miui/packageInstaller/model/PassportIdentityUrlModel;->getData()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;->z2(Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;Ljava/lang/String;)V

    goto :goto_3

    :cond_3
    invoke-virtual {p1}, Lcom/miui/packageInstaller/model/PassportIdentityUrlModel;->getMsg()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object p1

    :goto_1
    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    goto :goto_3

    :cond_4
    iget-object p1, p0, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity$b;->f:Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;

    invoke-static {p1, v3, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :goto_2
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    const-string v0, "ChildAccountInstallerActivity"

    invoke-static {v0, p1}, Lf6/o;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    iget-object p1, p0, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity$b;->f:Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;

    invoke-static {p1, v3, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    :cond_5
    :goto_3
    sget-object p1, La8/v;->a:La8/v;

    return-object p1
.end method

.method public final q(Lv8/e0;Ld8/d;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lv8/e0;",
            "Ld8/d<",
            "-",
            "La8/v;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity$b;->b(Ljava/lang/Object;Ld8/d;)Ld8/d;

    move-result-object p1

    check-cast p1, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity$b;

    sget-object p2, La8/v;->a:La8/v;

    invoke-virtual {p1, p2}, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity$b;->n(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method
