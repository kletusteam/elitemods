.class public final Lcom/miui/packageInstaller/view/DevEditText;
.super Landroid/widget/FrameLayout;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/packageInstaller/view/DevEditText$a;,
        Lcom/miui/packageInstaller/view/DevEditText$b;
    }
.end annotation


# static fields
.field public static final i:Lcom/miui/packageInstaller/view/DevEditText$a;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:I

.field private d:Landroid/widget/TextView;

.field private e:Landroidx/appcompat/widget/AppCompatEditText;

.field private f:Landroid/widget/RelativeLayout;

.field private g:Lcom/miui/packageInstaller/view/DevEditText$b;

.field private final h:Landroid/graphics/Typeface;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/miui/packageInstaller/view/DevEditText$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/miui/packageInstaller/view/DevEditText$a;-><init>(Lm8/g;)V

    sput-object v0, Lcom/miui/packageInstaller/view/DevEditText;->i:Lcom/miui/packageInstaller/view/DevEditText$a;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    const-string v0, "context"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const-string v0, ""

    iput-object v0, p0, Lcom/miui/packageInstaller/view/DevEditText;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/miui/packageInstaller/view/DevEditText;->b:Ljava/lang/String;

    sget-object v0, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/packageInstaller/view/DevEditText;->h:Landroid/graphics/Typeface;

    invoke-virtual {p0}, Lcom/miui/packageInstaller/view/DevEditText;->e()V

    sget-object v0, Ln6/a;->b0:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p1

    const-string p2, "context.obtainStyledAttr\u2026 R.styleable.DevEditText)"

    invoke-static {p1, p2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    iput-object p2, p0, Lcom/miui/packageInstaller/view/DevEditText;->a:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    iput-object p2, p0, Lcom/miui/packageInstaller/view/DevEditText;->b:Ljava/lang/String;

    const/4 p2, 0x2

    invoke-virtual {p1, p2, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result p2

    iput p2, p0, Lcom/miui/packageInstaller/view/DevEditText;->c:I

    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method public static synthetic a(Lcom/miui/packageInstaller/view/DevEditText;Landroid/view/View;Z)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/miui/packageInstaller/view/DevEditText;->f(Lcom/miui/packageInstaller/view/DevEditText;Landroid/view/View;Z)V

    return-void
.end method

.method public static final synthetic b(Lcom/miui/packageInstaller/view/DevEditText;Ljava/lang/CharSequence;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/packageInstaller/view/DevEditText;->d(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public static final synthetic c(Lcom/miui/packageInstaller/view/DevEditText;)I
    .locals 0

    iget p0, p0, Lcom/miui/packageInstaller/view/DevEditText;->c:I

    return p0
.end method

.method private final d(Ljava/lang/CharSequence;)V
    .locals 4

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result p1

    const/4 v0, 0x0

    if-lez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    move p1, v0

    :goto_0
    const/4 v1, 0x0

    const-string v2, "mEditText"

    if-eqz p1, :cond_4

    iget-object p1, p0, Lcom/miui/packageInstaller/view/DevEditText;->e:Landroidx/appcompat/widget/AppCompatEditText;

    if-nez p1, :cond_1

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    move-object p1, v1

    :cond_1
    invoke-virtual {p1}, Landroid/widget/EditText;->getTypeface()Landroid/graphics/Typeface;

    move-result-object p1

    iget-object v3, p0, Lcom/miui/packageInstaller/view/DevEditText;->h:Landroid/graphics/Typeface;

    invoke-static {p1, v3}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_7

    iget-object p1, p0, Lcom/miui/packageInstaller/view/DevEditText;->e:Landroidx/appcompat/widget/AppCompatEditText;

    if-nez p1, :cond_2

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    move-object p1, v1

    :cond_2
    iget-object v3, p0, Lcom/miui/packageInstaller/view/DevEditText;->h:Landroid/graphics/Typeface;

    invoke-virtual {p1, v3}, Landroid/widget/EditText;->setTypeface(Landroid/graphics/Typeface;)V

    iget-object p1, p0, Lcom/miui/packageInstaller/view/DevEditText;->e:Landroidx/appcompat/widget/AppCompatEditText;

    if-nez p1, :cond_3

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    move-object v1, p1

    :goto_1
    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const v2, 0x7f0700d5

    goto :goto_3

    :cond_4
    const-string p1, "mipro-normal"

    invoke-static {p1, v0}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object p1

    iget-object v3, p0, Lcom/miui/packageInstaller/view/DevEditText;->e:Landroidx/appcompat/widget/AppCompatEditText;

    if-nez v3, :cond_5

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v3, v1

    :cond_5
    invoke-virtual {v3, p1}, Landroid/widget/EditText;->setTypeface(Landroid/graphics/Typeface;)V

    iget-object p1, p0, Lcom/miui/packageInstaller/view/DevEditText;->e:Landroidx/appcompat/widget/AppCompatEditText;

    if-nez p1, :cond_6

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_2

    :cond_6
    move-object v1, p1

    :goto_2
    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const v2, 0x7f0700c5

    :goto_3
    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    int-to-float p1, p1

    invoke-virtual {v1, v0, p1}, Landroid/widget/EditText;->setTextSize(IF)V

    :cond_7
    return-void
.end method

.method private static final f(Lcom/miui/packageInstaller/view/DevEditText;Landroid/view/View;Z)V
    .locals 3

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_0

    const p1, 0x7f080598

    goto :goto_0

    :cond_0
    const p1, 0x7f080597

    :goto_0
    iget-object v0, p0, Lcom/miui/packageInstaller/view/DevEditText;->f:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    if-nez v0, :cond_1

    const-string v0, "mRelLayout"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_1
    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    iget-object p1, p0, Lcom/miui/packageInstaller/view/DevEditText;->e:Landroidx/appcompat/widget/AppCompatEditText;

    if-nez p1, :cond_2

    const-string p1, "mEditText"

    invoke-static {p1}, Lm8/i;->s(Ljava/lang/String;)V

    move-object p1, v1

    :cond_2
    invoke-virtual {p1}, Landroidx/appcompat/widget/AppCompatEditText;->getText()Landroid/text/Editable;

    move-result-object p1

    if-eqz p1, :cond_3

    invoke-static {p1}, Lu8/g;->u0(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    :cond_3
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    iget v0, p0, Lcom/miui/packageInstaller/view/DevEditText;->c:I

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eq v0, v1, :cond_5

    const/4 v1, 0x2

    if-eq v0, v1, :cond_4

    goto :goto_2

    :cond_4
    if-nez p2, :cond_6

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p2

    if-nez p2, :cond_6

    sget-object p2, Lcom/miui/packageInstaller/view/DevEditText;->i:Lcom/miui/packageInstaller/view/DevEditText$a;

    invoke-virtual {p2, p1}, Lcom/miui/packageInstaller/view/DevEditText$a;->b(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_6

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object p0

    const p1, 0x7f1100cf

    goto :goto_1

    :cond_5
    if-nez p2, :cond_6

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p2

    if-nez p2, :cond_6

    sget-object p2, Lcom/miui/packageInstaller/view/DevEditText;->i:Lcom/miui/packageInstaller/view/DevEditText$a;

    invoke-virtual {p2, p1}, Lcom/miui/packageInstaller/view/DevEditText$a;->a(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_6

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object p0

    const p1, 0x7f1100cc

    :goto_1
    invoke-static {p0, p1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object p0

    invoke-virtual {p0}, Landroid/widget/Toast;->show()V

    :cond_6
    :goto_2
    return-void
.end method


# virtual methods
.method public final e()V
    .locals 2

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0d005d

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    return-void
.end method

.method public final getEditText()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/view/DevEditText;->e:Landroidx/appcompat/widget/AppCompatEditText;

    if-nez v0, :cond_0

    const-string v0, "mEditText"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v0, 0x0

    :cond_0
    invoke-virtual {v0}, Landroidx/appcompat/widget/AppCompatEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getMListener()Lcom/miui/packageInstaller/view/DevEditText$b;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/view/DevEditText;->g:Lcom/miui/packageInstaller/view/DevEditText$b;

    return-object v0
.end method

.method public final getTf()Landroid/graphics/Typeface;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/view/DevEditText;->h:Landroid/graphics/Typeface;

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 4

    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    const v0, 0x7f0a011d

    invoke-virtual {p0, v0}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.dev_com_edit_parent)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/miui/packageInstaller/view/DevEditText;->f:Landroid/widget/RelativeLayout;

    const v0, 0x7f0a011b

    invoke-virtual {p0, v0}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.dev_com_edit_lefttv)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/view/DevEditText;->d:Landroid/widget/TextView;

    const v0, 0x7f0a0117

    invoke-virtual {p0, v0}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.dev_com_edit)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroidx/appcompat/widget/AppCompatEditText;

    iput-object v0, p0, Lcom/miui/packageInstaller/view/DevEditText;->e:Landroidx/appcompat/widget/AppCompatEditText;

    iget-object v0, p0, Lcom/miui/packageInstaller/view/DevEditText;->d:Landroid/widget/TextView;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const-string v0, "mTextView"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_0
    iget-object v2, p0, Lcom/miui/packageInstaller/view/DevEditText;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/view/DevEditText;->e:Landroidx/appcompat/widget/AppCompatEditText;

    const-string v2, "mEditText"

    if-nez v0, :cond_1

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_1
    iget-object v3, p0, Lcom/miui/packageInstaller/view/DevEditText;->b:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    iget v0, p0, Lcom/miui/packageInstaller/view/DevEditText;->c:I

    const/4 v3, 0x2

    if-ne v0, v3, :cond_3

    iget-object v0, p0, Lcom/miui/packageInstaller/view/DevEditText;->e:Landroidx/appcompat/widget/AppCompatEditText;

    if-nez v0, :cond_2

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_2
    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setInputType(I)V

    :cond_3
    iget-object v0, p0, Lcom/miui/packageInstaller/view/DevEditText;->e:Landroidx/appcompat/widget/AppCompatEditText;

    if-nez v0, :cond_4

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_4
    new-instance v3, Lcom/miui/packageInstaller/view/b;

    invoke-direct {v3, p0}, Lcom/miui/packageInstaller/view/b;-><init>(Lcom/miui/packageInstaller/view/DevEditText;)V

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/view/DevEditText;->e:Landroidx/appcompat/widget/AppCompatEditText;

    if-nez v0, :cond_5

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    move-object v1, v0

    :goto_0
    new-instance v0, Lcom/miui/packageInstaller/view/DevEditText$c;

    invoke-direct {v0, p0}, Lcom/miui/packageInstaller/view/DevEditText$c;-><init>(Lcom/miui/packageInstaller/view/DevEditText;)V

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method

.method public final setMListener(Lcom/miui/packageInstaller/view/DevEditText$b;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/view/DevEditText;->g:Lcom/miui/packageInstaller/view/DevEditText$b;

    return-void
.end method

.method public final setText(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/view/DevEditText;->e:Landroidx/appcompat/widget/AppCompatEditText;

    if-nez v0, :cond_0

    const-string v0, "mEditText"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v0, 0x0

    :cond_0
    if-nez p1, :cond_1

    const-string p1, ""

    :cond_1
    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final setTextChangedListener(Lcom/miui/packageInstaller/view/DevEditText$b;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/view/DevEditText;->g:Lcom/miui/packageInstaller/view/DevEditText$b;

    return-void
.end method
