.class public final Lcom/miui/packageInstaller/ui/InstallerActionBar;
.super Landroid/widget/FrameLayout;

# interfaces
.implements Lz5/j;


# instance fields
.field private a:Lz5/j;

.field private b:Landroid/view/View;

.field private c:Landroid/widget/Button;

.field private d:Landroid/widget/Button;

.field private e:Landroid/widget/Button;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public a(IJ)V
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/InstallerActionBar;->a:Lz5/j;

    invoke-static {v0}, Lm8/i;->c(Ljava/lang/Object;)V

    invoke-interface {v0, p1, p2, p3}, Lz5/j;->a(IJ)V

    return-void
.end method

.method public final b(II)V
    .locals 4

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, p1, p0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    const/4 p1, -0x1

    if-eq p2, p1, :cond_0

    invoke-virtual {v0, p2, p0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    const p1, 0x7f0a0138

    invoke-virtual {p0, p1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/InstallerActionBar;->b:Landroid/view/View;

    :cond_0
    const p1, 0x7f0a01e0

    invoke-virtual {p0, p1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lz5/j;

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/InstallerActionBar;->a:Lz5/j;

    iget-object p1, p0, Lcom/miui/packageInstaller/ui/InstallerActionBar;->b:Landroid/view/View;

    if-eqz p1, :cond_4

    const/4 p2, 0x0

    if-eqz p1, :cond_1

    const v0, 0x7f0a038e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    goto :goto_0

    :cond_1
    move-object p1, p2

    :goto_0
    iput-object p1, p0, Lcom/miui/packageInstaller/ui/InstallerActionBar;->e:Landroid/widget/Button;

    iget-object p1, p0, Lcom/miui/packageInstaller/ui/InstallerActionBar;->b:Landroid/view/View;

    if-eqz p1, :cond_2

    const v0, 0x7f0a0338

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    goto :goto_1

    :cond_2
    move-object p1, p2

    :goto_1
    iput-object p1, p0, Lcom/miui/packageInstaller/ui/InstallerActionBar;->c:Landroid/widget/Button;

    iget-object p1, p0, Lcom/miui/packageInstaller/ui/InstallerActionBar;->b:Landroid/view/View;

    if-eqz p1, :cond_3

    const p2, 0x7f0a0152

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    move-object p2, p1

    check-cast p2, Landroid/widget/Button;

    :cond_3
    iput-object p2, p0, Lcom/miui/packageInstaller/ui/InstallerActionBar;->d:Landroid/widget/Button;

    :cond_4
    iget-object p1, p0, Lcom/miui/packageInstaller/ui/InstallerActionBar;->c:Landroid/widget/Button;

    const/high16 p2, 0x3f800000    # 1.0f

    const/4 v0, 0x0

    if-eqz p1, :cond_5

    new-array v2, v1, [Landroid/view/View;

    aput-object p1, v2, v0

    invoke-static {v2}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object p1

    invoke-interface {p1}, Lmiuix/animation/e;->c()Lmiuix/animation/j;

    move-result-object p1

    new-array v2, v0, [Lmiuix/animation/j$b;

    invoke-interface {p1, p2, v2}, Lmiuix/animation/j;->G(F[Lmiuix/animation/j$b;)Lmiuix/animation/j;

    move-result-object p1

    iget-object v2, p0, Lcom/miui/packageInstaller/ui/InstallerActionBar;->c:Landroid/widget/Button;

    new-array v3, v0, [Lc9/a;

    invoke-interface {p1, v2, v3}, Lmiuix/animation/j;->z(Landroid/view/View;[Lc9/a;)V

    :cond_5
    iget-object p1, p0, Lcom/miui/packageInstaller/ui/InstallerActionBar;->d:Landroid/widget/Button;

    if-eqz p1, :cond_6

    new-array v2, v1, [Landroid/view/View;

    aput-object p1, v2, v0

    invoke-static {v2}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object p1

    invoke-interface {p1}, Lmiuix/animation/e;->c()Lmiuix/animation/j;

    move-result-object p1

    new-array v2, v0, [Lmiuix/animation/j$b;

    invoke-interface {p1, p2, v2}, Lmiuix/animation/j;->G(F[Lmiuix/animation/j$b;)Lmiuix/animation/j;

    move-result-object p1

    iget-object v2, p0, Lcom/miui/packageInstaller/ui/InstallerActionBar;->d:Landroid/widget/Button;

    new-array v3, v0, [Lc9/a;

    invoke-interface {p1, v2, v3}, Lmiuix/animation/j;->z(Landroid/view/View;[Lc9/a;)V

    :cond_6
    iget-object p1, p0, Lcom/miui/packageInstaller/ui/InstallerActionBar;->e:Landroid/widget/Button;

    if-eqz p1, :cond_7

    new-array v1, v1, [Landroid/view/View;

    aput-object p1, v1, v0

    invoke-static {v1}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object p1

    invoke-interface {p1}, Lmiuix/animation/e;->c()Lmiuix/animation/j;

    move-result-object p1

    new-array v1, v0, [Lmiuix/animation/j$b;

    invoke-interface {p1, p2, v1}, Lmiuix/animation/j;->G(F[Lmiuix/animation/j$b;)Lmiuix/animation/j;

    move-result-object p1

    iget-object p2, p0, Lcom/miui/packageInstaller/ui/InstallerActionBar;->e:Landroid/widget/Button;

    new-array v0, v0, [Lc9/a;

    invoke-interface {p1, p2, v0}, Lmiuix/animation/j;->z(Landroid/view/View;[Lc9/a;)V

    :cond_7
    return-void
.end method

.method public final getMDoneLayout()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/InstallerActionBar;->b:Landroid/view/View;

    return-object v0
.end method

.method public final getMEndButton()Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/InstallerActionBar;->d:Landroid/widget/Button;

    return-object v0
.end method

.method public final getMProgress()Lz5/j;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/InstallerActionBar;->a:Lz5/j;

    return-object v0
.end method

.method public final getMStartButton()Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/InstallerActionBar;->c:Landroid/widget/Button;

    return-object v0
.end method

.method public final getMTopButton()Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/InstallerActionBar;->e:Landroid/widget/Button;

    return-object v0
.end method

.method public setClick(Landroid/view/View$OnClickListener;)V
    .locals 1

    const-string v0, "l"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/InstallerActionBar;->a:Lz5/j;

    invoke-static {v0}, Lm8/i;->c(Ljava/lang/Object;)V

    invoke-interface {v0, p1}, Lz5/j;->setClick(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final setMDoneLayout(Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/InstallerActionBar;->b:Landroid/view/View;

    return-void
.end method

.method public final setMEndButton(Landroid/widget/Button;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/InstallerActionBar;->d:Landroid/widget/Button;

    return-void
.end method

.method public final setMProgress(Lz5/j;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/InstallerActionBar;->a:Lz5/j;

    return-void
.end method

.method public final setMStartButton(Landroid/widget/Button;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/InstallerActionBar;->c:Landroid/widget/Button;

    return-void
.end method

.method public final setMTopButton(Landroid/widget/Button;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/InstallerActionBar;->e:Landroid/widget/Button;

    return-void
.end method

.method public setProgressText(Ljava/lang/CharSequence;)V
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/InstallerActionBar;->a:Lz5/j;

    invoke-static {v0}, Lm8/i;->c(Ljava/lang/Object;)V

    invoke-interface {v0, p1}, Lz5/j;->setProgressText(Ljava/lang/CharSequence;)V

    return-void
.end method
