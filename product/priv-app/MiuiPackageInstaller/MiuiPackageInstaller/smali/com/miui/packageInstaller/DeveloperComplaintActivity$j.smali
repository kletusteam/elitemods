.class public final Lcom/miui/packageInstaller/DeveloperComplaintActivity$j;
.super Ljava/lang/Object;

# interfaces
.implements Lgc/d;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/packageInstaller/DeveloperComplaintActivity;->e1(Ljava/util/HashMap;Lcom/miui/packageInstaller/DeveloperComplaintActivity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lgc/d<",
        "Lcom/miui/packageInstaller/model/RiskTypeResult;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/miui/packageInstaller/DeveloperComplaintActivity;


# direct methods
.method constructor <init>(Lcom/miui/packageInstaller/DeveloperComplaintActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/DeveloperComplaintActivity$j;->a:Lcom/miui/packageInstaller/DeveloperComplaintActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static synthetic c(Lcom/miui/packageInstaller/model/RiskTypeResult;)V
    .locals 0

    invoke-static {p0}, Lcom/miui/packageInstaller/DeveloperComplaintActivity$j;->h(Lcom/miui/packageInstaller/model/RiskTypeResult;)V

    return-void
.end method

.method public static synthetic d()V
    .locals 0

    invoke-static {}, Lcom/miui/packageInstaller/DeveloperComplaintActivity$j;->f()V

    return-void
.end method

.method public static synthetic e()V
    .locals 0

    invoke-static {}, Lcom/miui/packageInstaller/DeveloperComplaintActivity$j;->g()V

    return-void
.end method

.method private static final f()V
    .locals 3

    invoke-static {}, Lcom/android/packageinstaller/InstallerApplication;->g()Lcom/android/packageinstaller/InstallerApplication;

    move-result-object v0

    const v1, 0x7f110119

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method private static final g()V
    .locals 3

    invoke-static {}, Lcom/android/packageinstaller/InstallerApplication;->g()Lcom/android/packageinstaller/InstallerApplication;

    move-result-object v0

    const v1, 0x7f110119

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method private static final h(Lcom/miui/packageInstaller/model/RiskTypeResult;)V
    .locals 2

    invoke-static {}, Lcom/android/packageinstaller/InstallerApplication;->g()Lcom/android/packageinstaller/InstallerApplication;

    move-result-object v0

    invoke-virtual {p0}, Lcom/miui/packageInstaller/model/RiskTypeResult;->getMsg()Ljava/lang/String;

    move-result-object p0

    const/4 v1, 0x0

    invoke-static {v0, p0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object p0

    invoke-virtual {p0}, Landroid/widget/Toast;->show()V

    return-void
.end method


# virtual methods
.method public a(Lgc/b;Lgc/t;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lgc/b<",
            "Lcom/miui/packageInstaller/model/RiskTypeResult;",
            ">;",
            "Lgc/t<",
            "Lcom/miui/packageInstaller/model/RiskTypeResult;",
            ">;)V"
        }
    .end annotation

    const-string v0, "call"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "response"

    invoke-static {p2, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Lgc/t;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/miui/packageInstaller/model/RiskTypeResult;

    if-nez p1, :cond_0

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object p1

    sget-object p2, Lm5/q;->a:Lm5/q;

    invoke-virtual {p1, p2}, Lf6/z;->e(Ljava/lang/Runnable;)V

    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/miui/packageInstaller/model/RiskTypeResult;->getCode()I

    move-result p2

    const/16 v0, 0xc8

    if-ne p2, v0, :cond_1

    sget-object p1, Lf6/s;->a:Lf6/s$a;

    invoke-virtual {p1}, Lf6/s$a;->a()Lf6/s;

    move-result-object p2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-string v2, "dev_com_submit_time"

    invoke-virtual {p2, v2, v0, v1}, Lf6/s;->j(Ljava/lang/String;J)V

    invoke-virtual {p1}, Lf6/s$a;->a()Lf6/s;

    move-result-object p2

    const-string v0, "dev_com_submit_num"

    invoke-virtual {p2, v0}, Lf6/s;->f(Ljava/lang/String;)J

    move-result-wide v1

    invoke-virtual {p1}, Lf6/s$a;->a()Lf6/s;

    move-result-object p1

    const-wide/16 v3, 0x1

    add-long/2addr v1, v3

    invoke-virtual {p1, v0, v1, v2}, Lf6/s;->j(Ljava/lang/String;J)V

    new-instance p1, Landroid/content/Intent;

    invoke-static {}, Lcom/android/packageinstaller/InstallerApplication;->g()Lcom/android/packageinstaller/InstallerApplication;

    move-result-object p2

    const-class v0, Lcom/miui/packageInstaller/DeveloperComplaintResultActivity;

    invoke-direct {p1, p2, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object p2, p0, Lcom/miui/packageInstaller/DeveloperComplaintActivity$j;->a:Lcom/miui/packageInstaller/DeveloperComplaintActivity;

    invoke-virtual {p2, p1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    iget-object p1, p0, Lcom/miui/packageInstaller/DeveloperComplaintActivity$j;->a:Lcom/miui/packageInstaller/DeveloperComplaintActivity;

    invoke-virtual {p1}, Lmiuix/appcompat/app/j;->finish()V

    return-void

    :cond_1
    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object p2

    new-instance v0, Lm5/o;

    invoke-direct {v0, p1}, Lm5/o;-><init>(Lcom/miui/packageInstaller/model/RiskTypeResult;)V

    invoke-virtual {p2, v0}, Lf6/z;->e(Ljava/lang/Runnable;)V

    return-void
.end method

.method public b(Lgc/b;Ljava/lang/Throwable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lgc/b<",
            "Lcom/miui/packageInstaller/model/RiskTypeResult;",
            ">;",
            "Ljava/lang/Throwable;",
            ")V"
        }
    .end annotation

    const-string v0, "call"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "t"

    invoke-static {p2, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object p1

    sget-object p2, Lm5/p;->a:Lm5/p;

    invoke-virtual {p1, p2}, Lf6/z;->e(Ljava/lang/Runnable;)V

    return-void
.end method
