.class public final Lcom/miui/packageInstaller/secure/view/FingerPassword$b;
.super Ljava/lang/Object;

# interfaces
.implements Lh6/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/packageInstaller/secure/view/FingerPassword;->d(Lx5/h;Ll8/l;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/miui/packageInstaller/secure/view/FingerPassword;

.field final synthetic b:Ll8/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ll8/l<",
            "Ljava/lang/Integer;",
            "La8/v;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic c:Lx5/h;


# direct methods
.method constructor <init>(Lcom/miui/packageInstaller/secure/view/FingerPassword;Ll8/l;Lx5/h;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/miui/packageInstaller/secure/view/FingerPassword;",
            "Ll8/l<",
            "-",
            "Ljava/lang/Integer;",
            "La8/v;",
            ">;",
            "Lx5/h;",
            ")V"
        }
    .end annotation

    iput-object p1, p0, Lcom/miui/packageInstaller/secure/view/FingerPassword$b;->a:Lcom/miui/packageInstaller/secure/view/FingerPassword;

    iput-object p2, p0, Lcom/miui/packageInstaller/secure/view/FingerPassword$b;->b:Ll8/l;

    iput-object p3, p0, Lcom/miui/packageInstaller/secure/view/FingerPassword$b;->c:Lx5/h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static synthetic c(ILcom/miui/packageInstaller/secure/view/FingerPassword;Ljava/lang/String;Ll8/l;Lx5/h;)V
    .locals 0

    invoke-static {p0, p1, p2, p3, p4}, Lcom/miui/packageInstaller/secure/view/FingerPassword$b;->d(ILcom/miui/packageInstaller/secure/view/FingerPassword;Ljava/lang/String;Ll8/l;Lx5/h;)V

    return-void
.end method

.method private static final d(ILcom/miui/packageInstaller/secure/view/FingerPassword;Ljava/lang/String;Ll8/l;Lx5/h;)V
    .locals 3

    const-string v0, "this$0"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$callback"

    invoke-static {p3, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$dialog"

    invoke-static {p4, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v0, -0x64

    const-string v1, "tipsTextView"

    const/4 v2, 0x0

    if-eq p0, v0, :cond_2

    const/4 v0, 0x7

    if-eq p0, v0, :cond_0

    invoke-virtual {p1}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object p0

    const/4 p1, 0x0

    invoke-static {p0, p2, p1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object p0

    invoke-virtual {p0}, Landroid/widget/Toast;->show()V

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-interface {p3, p0}, Ll8/l;->j(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_0
    invoke-static {p1}, Lcom/miui/packageInstaller/secure/view/FingerPassword;->c(Lcom/miui/packageInstaller/secure/view/FingerPassword;)Landroid/widget/TextView;

    move-result-object p0

    if-nez p0, :cond_1

    invoke-static {v1}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move-object v2, p0

    :goto_0
    invoke-virtual {p1}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object p0

    const p2, 0x7f11011b

    invoke-virtual {p0, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v2, p0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance p0, Lcom/miui/packageInstaller/secure/view/FingerPassword$b$a;

    invoke-direct {p0, p1, p4, p3}, Lcom/miui/packageInstaller/secure/view/FingerPassword$b$a;-><init>(Lcom/miui/packageInstaller/secure/view/FingerPassword;Lx5/h;Ll8/l;)V

    invoke-virtual {p1, p0}, Lcom/miui/packageInstaller/secure/view/FingerPassword;->e(Ll8/a;)V

    goto :goto_1

    :cond_2
    invoke-static {p1}, Lcom/miui/packageInstaller/secure/view/FingerPassword;->c(Lcom/miui/packageInstaller/secure/view/FingerPassword;)Landroid/widget/TextView;

    move-result-object p0

    if-nez p0, :cond_3

    invoke-static {v1}, Lm8/i;->s(Ljava/lang/String;)V

    move-object p0, v2

    :cond_3
    invoke-virtual {p1}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object p2

    const p3, 0x7f110119

    invoke-virtual {p2, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 p0, 0x1

    invoke-static {p1, v2, p0, v2}, Lcom/miui/packageInstaller/secure/view/FingerPassword;->f(Lcom/miui/packageInstaller/secure/view/FingerPassword;Ll8/a;ILjava/lang/Object;)V

    :goto_1
    return-void
.end method


# virtual methods
.method public a(ILjava/lang/String;)V
    .locals 8

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object v0

    iget-object v3, p0, Lcom/miui/packageInstaller/secure/view/FingerPassword$b;->a:Lcom/miui/packageInstaller/secure/view/FingerPassword;

    iget-object v5, p0, Lcom/miui/packageInstaller/secure/view/FingerPassword$b;->b:Ll8/l;

    iget-object v6, p0, Lcom/miui/packageInstaller/secure/view/FingerPassword$b;->c:Lx5/h;

    new-instance v7, Ly5/g;

    move-object v1, v7

    move v2, p1

    move-object v4, p2

    invoke-direct/range {v1 .. v6}, Ly5/g;-><init>(ILcom/miui/packageInstaller/secure/view/FingerPassword;Ljava/lang/String;Ll8/l;Lx5/h;)V

    invoke-virtual {v0, v7}, Lf6/z;->e(Ljava/lang/Runnable;)V

    return-void
.end method

.method public b()V
    .locals 3

    iget-object v0, p0, Lcom/miui/packageInstaller/secure/view/FingerPassword$b;->a:Lcom/miui/packageInstaller/secure/view/FingerPassword;

    new-instance v1, Lcom/miui/packageInstaller/secure/view/FingerPassword$b$b;

    iget-object v2, p0, Lcom/miui/packageInstaller/secure/view/FingerPassword$b;->b:Ll8/l;

    invoke-direct {v1, v2}, Lcom/miui/packageInstaller/secure/view/FingerPassword$b$b;-><init>(Ll8/l;)V

    invoke-virtual {v0, v1}, Lcom/miui/packageInstaller/secure/view/FingerPassword;->g(Ll8/a;)V

    return-void
.end method
