.class public final Lcom/miui/packageInstaller/model/ApkInfo;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/packageInstaller/model/ApkInfo$Companion;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/miui/packageInstaller/model/ApkInfo;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/miui/packageInstaller/model/ApkInfo$Companion;


# instance fields
.field private apkAbi:Ljava/lang/String;

.field private apkMd5:Ljava/lang/String;

.field private apkSignature:Ljava/lang/String;

.field private apkSignature2:Ljava/lang/String;

.field private apkSignatureSha1:Ljava/lang/String;

.field private apkSignatureSha256:Ljava/lang/String;

.field private channelMessage:Ljava/lang/String;

.field private cloudParams:Lcom/miui/packageInstaller/model/CloudParams;

.field private currentInstallVersionName:Ljava/lang/String;

.field private fileSize:J

.field private fileSizeString:Ljava/lang/String;

.field private fileUri:Landroid/net/Uri;

.field private icon:Landroid/graphics/drawable/Drawable;

.field private installedPackageInfo:Landroid/content/pm/ApplicationInfo;

.field private installedVersionCode:I

.field private installedVersionName:Ljava/lang/String;

.field private label:Ljava/lang/String;

.field private newInstall:I

.field private originalFilePath:Ljava/lang/String;

.field private originalUri:Landroid/net/Uri;

.field private packageInfo:Landroid/content/pm/PackageInfo;

.field private realFileName:Ljava/lang/String;

.field private systemApp:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/miui/packageInstaller/model/ApkInfo$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/miui/packageInstaller/model/ApkInfo$Companion;-><init>(Lm8/g;)V

    sput-object v0, Lcom/miui/packageInstaller/model/ApkInfo;->Companion:Lcom/miui/packageInstaller/model/ApkInfo$Companion;

    new-instance v0, Lcom/miui/packageInstaller/model/ApkInfo$Companion$CREATOR$1;

    invoke-direct {v0}, Lcom/miui/packageInstaller/model/ApkInfo$Companion$CREATOR$1;-><init>()V

    sput-object v0, Lcom/miui/packageInstaller/model/ApkInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/miui/packageInstaller/model/ApkInfo;->newInstall:I

    const-string v0, ""

    iput-object v0, p0, Lcom/miui/packageInstaller/model/ApkInfo;->apkAbi:Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    const-string v0, "in"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/miui/packageInstaller/model/ApkInfo;->newInstall:I

    const-string v0, ""

    iput-object v0, p0, Lcom/miui/packageInstaller/model/ApkInfo;->apkAbi:Ljava/lang/String;

    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/miui/packageInstaller/model/ApkInfo;->fileUri:Landroid/net/Uri;

    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/miui/packageInstaller/model/ApkInfo;->originalUri:Landroid/net/Uri;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/packageInstaller/model/ApkInfo;->fileSizeString:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/miui/packageInstaller/model/ApkInfo;->fileSize:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/packageInstaller/model/ApkInfo;->apkMd5:Ljava/lang/String;

    const-class v0, Landroid/content/pm/PackageInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/pm/PackageInfo;

    iput-object v0, p0, Lcom/miui/packageInstaller/model/ApkInfo;->packageInfo:Landroid/content/pm/PackageInfo;

    const-class v0, Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ApplicationInfo;

    iput-object v0, p0, Lcom/miui/packageInstaller/model/ApkInfo;->installedPackageInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/miui/packageInstaller/model/ApkInfo;->installedVersionCode:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/packageInstaller/model/ApkInfo;->installedVersionName:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/packageInstaller/model/ApkInfo;->currentInstallVersionName:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/miui/packageInstaller/model/ApkInfo;->newInstall:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/packageInstaller/model/ApkInfo;->apkSignature:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/packageInstaller/model/ApkInfo;->apkSignature2:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/packageInstaller/model/ApkInfo;->apkSignatureSha1:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/packageInstaller/model/ApkInfo;->apkSignatureSha256:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/packageInstaller/model/ApkInfo;->label:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/packageInstaller/model/ApkInfo;->originalFilePath:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/packageInstaller/model/ApkInfo;->realFileName:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/miui/packageInstaller/model/CloudParams;

    iput-object v0, p0, Lcom/miui/packageInstaller/model/ApkInfo;->cloudParams:Lcom/miui/packageInstaller/model/CloudParams;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/miui/packageInstaller/model/ApkInfo;->channelMessage:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final getApkAbi()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/model/ApkInfo;->apkAbi:Ljava/lang/String;

    return-object v0
.end method

.method public final getApkMd5()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/model/ApkInfo;->apkMd5:Ljava/lang/String;

    return-object v0
.end method

.method public final getApkSignature()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/model/ApkInfo;->apkSignature:Ljava/lang/String;

    return-object v0
.end method

.method public final getApkSignature2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/model/ApkInfo;->apkSignature2:Ljava/lang/String;

    return-object v0
.end method

.method public final getApkSignatureSha1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/model/ApkInfo;->apkSignatureSha1:Ljava/lang/String;

    return-object v0
.end method

.method public final getApkSignatureSha256()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/model/ApkInfo;->apkSignatureSha256:Ljava/lang/String;

    return-object v0
.end method

.method public final getChannelMessage()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/model/ApkInfo;->channelMessage:Ljava/lang/String;

    return-object v0
.end method

.method public final getCloudParams()Lcom/miui/packageInstaller/model/CloudParams;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/model/ApkInfo;->cloudParams:Lcom/miui/packageInstaller/model/CloudParams;

    return-object v0
.end method

.method public final getCurrentInstallVersionName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/model/ApkInfo;->currentInstallVersionName:Ljava/lang/String;

    return-object v0
.end method

.method public final getFileSize()J
    .locals 2

    iget-wide v0, p0, Lcom/miui/packageInstaller/model/ApkInfo;->fileSize:J

    return-wide v0
.end method

.method public final getFileSizeString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/model/ApkInfo;->fileSizeString:Ljava/lang/String;

    return-object v0
.end method

.method public final getFileUri()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/model/ApkInfo;->fileUri:Landroid/net/Uri;

    return-object v0
.end method

.method public final getIcon()Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/model/ApkInfo;->icon:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public final getInstalledPackageInfo()Landroid/content/pm/ApplicationInfo;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/model/ApkInfo;->installedPackageInfo:Landroid/content/pm/ApplicationInfo;

    return-object v0
.end method

.method public final getInstalledVersionCode()I
    .locals 1

    iget v0, p0, Lcom/miui/packageInstaller/model/ApkInfo;->installedVersionCode:I

    return v0
.end method

.method public final getInstalledVersionName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/model/ApkInfo;->installedVersionName:Ljava/lang/String;

    return-object v0
.end method

.method public final getLabel()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/model/ApkInfo;->label:Ljava/lang/String;

    return-object v0
.end method

.method public final getNewInstall()I
    .locals 1

    iget v0, p0, Lcom/miui/packageInstaller/model/ApkInfo;->newInstall:I

    return v0
.end method

.method public final getOriginalFilePath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/model/ApkInfo;->originalFilePath:Ljava/lang/String;

    return-object v0
.end method

.method public final getOriginalUri()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/model/ApkInfo;->originalUri:Landroid/net/Uri;

    return-object v0
.end method

.method public final getPackageInfo()Landroid/content/pm/PackageInfo;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/model/ApkInfo;->packageInfo:Landroid/content/pm/PackageInfo;

    return-object v0
.end method

.method public final getPackageName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/model/ApkInfo;->packageInfo:Landroid/content/pm/PackageInfo;

    if-eqz v0, :cond_0

    invoke-static {v0}, Lm8/i;->c(Ljava/lang/Object;)V

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public final getRealFileName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/model/ApkInfo;->realFileName:Ljava/lang/String;

    return-object v0
.end method

.method public final getSystemApp()Z
    .locals 1

    iget-boolean v0, p0, Lcom/miui/packageInstaller/model/ApkInfo;->systemApp:Z

    return v0
.end method

.method public final getVersionCode()I
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/model/ApkInfo;->packageInfo:Landroid/content/pm/PackageInfo;

    if-eqz v0, :cond_0

    invoke-static {v0}, Lm8/i;->c(Ljava/lang/Object;)V

    iget v0, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final getVersionName()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/miui/packageInstaller/model/ApkInfo;->packageInfo:Landroid/content/pm/PackageInfo;

    if-eqz v0, :cond_0

    invoke-static {v0}, Lm8/i;->c(Ljava/lang/Object;)V

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/miui/packageInstaller/model/ApkInfo;->packageInfo:Landroid/content/pm/PackageInfo;

    invoke-static {v0}, Lm8/i;->c(Ljava/lang/Object;)V

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    const-string v1, "packageInfo!!.versionName"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    const-string v0, ""

    :goto_0
    return-object v0
.end method

.method public final isOtherVersionInstalled()Z
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/model/ApkInfo;->installedPackageInfo:Landroid/content/pm/ApplicationInfo;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final setApkAbi(Ljava/lang/String;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/model/ApkInfo;->apkAbi:Ljava/lang/String;

    return-void
.end method

.method public final setApkMd5(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/model/ApkInfo;->apkMd5:Ljava/lang/String;

    return-void
.end method

.method public final setApkSignature(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/model/ApkInfo;->apkSignature:Ljava/lang/String;

    return-void
.end method

.method public final setApkSignature2(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/model/ApkInfo;->apkSignature2:Ljava/lang/String;

    return-void
.end method

.method public final setApkSignatureSha1(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/model/ApkInfo;->apkSignatureSha1:Ljava/lang/String;

    return-void
.end method

.method public final setApkSignatureSha256(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/model/ApkInfo;->apkSignatureSha256:Ljava/lang/String;

    return-void
.end method

.method public final setChannelMessage(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/model/ApkInfo;->channelMessage:Ljava/lang/String;

    return-void
.end method

.method public final setCloudParams(Lcom/miui/packageInstaller/model/CloudParams;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/model/ApkInfo;->cloudParams:Lcom/miui/packageInstaller/model/CloudParams;

    return-void
.end method

.method public final setCurrentInstallVersionName(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/model/ApkInfo;->currentInstallVersionName:Ljava/lang/String;

    return-void
.end method

.method public final setFileSize(J)V
    .locals 0

    iput-wide p1, p0, Lcom/miui/packageInstaller/model/ApkInfo;->fileSize:J

    return-void
.end method

.method public final setFileSizeString(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/model/ApkInfo;->fileSizeString:Ljava/lang/String;

    return-void
.end method

.method public final setFileUri(Landroid/net/Uri;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/model/ApkInfo;->fileUri:Landroid/net/Uri;

    return-void
.end method

.method public final setIcon(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/model/ApkInfo;->icon:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method public final setInstalledPackageInfo(Landroid/content/pm/ApplicationInfo;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/model/ApkInfo;->installedPackageInfo:Landroid/content/pm/ApplicationInfo;

    return-void
.end method

.method public final setInstalledVersionCode(I)V
    .locals 0

    iput p1, p0, Lcom/miui/packageInstaller/model/ApkInfo;->installedVersionCode:I

    return-void
.end method

.method public final setInstalledVersionName(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/model/ApkInfo;->installedVersionName:Ljava/lang/String;

    return-void
.end method

.method public final setLabel(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/model/ApkInfo;->label:Ljava/lang/String;

    return-void
.end method

.method public final setNewInstall(I)V
    .locals 0

    iput p1, p0, Lcom/miui/packageInstaller/model/ApkInfo;->newInstall:I

    return-void
.end method

.method public final setOriginalFilePath(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/model/ApkInfo;->originalFilePath:Ljava/lang/String;

    return-void
.end method

.method public final setOriginalUri(Landroid/net/Uri;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/model/ApkInfo;->originalUri:Landroid/net/Uri;

    return-void
.end method

.method public final setPackageInfo(Landroid/content/pm/PackageInfo;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/model/ApkInfo;->packageInfo:Landroid/content/pm/PackageInfo;

    return-void
.end method

.method public final setRealFileName(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/model/ApkInfo;->realFileName:Ljava/lang/String;

    return-void
.end method

.method public final setSystemApp(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/miui/packageInstaller/model/ApkInfo;->systemApp:Z

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ApkInfo{fileUri="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/miui/packageInstaller/model/ApkInfo;->fileUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", fileSizeString=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/miui/packageInstaller/model/ApkInfo;->fileSizeString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\', fileSize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/miui/packageInstaller/model/ApkInfo;->fileSize:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", apkMd5=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/miui/packageInstaller/model/ApkInfo;->apkMd5:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\', packageInfo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/miui/packageInstaller/model/ApkInfo;->packageInfo:Landroid/content/pm/PackageInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", label="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/miui/packageInstaller/model/ApkInfo;->label:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", icon="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/miui/packageInstaller/model/ApkInfo;->icon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", installedPackageInfo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/miui/packageInstaller/model/ApkInfo;->installedPackageInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    const-string v0, "dest"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/model/ApkInfo;->fileUri:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/miui/packageInstaller/model/ApkInfo;->originalUri:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/miui/packageInstaller/model/ApkInfo;->fileSizeString:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-wide v0, p0, Lcom/miui/packageInstaller/model/ApkInfo;->fileSize:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget-object v0, p0, Lcom/miui/packageInstaller/model/ApkInfo;->apkMd5:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/model/ApkInfo;->packageInfo:Landroid/content/pm/PackageInfo;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/miui/packageInstaller/model/ApkInfo;->installedPackageInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget p2, p0, Lcom/miui/packageInstaller/model/ApkInfo;->installedVersionCode:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    iget-object p2, p0, Lcom/miui/packageInstaller/model/ApkInfo;->installedVersionName:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object p2, p0, Lcom/miui/packageInstaller/model/ApkInfo;->currentInstallVersionName:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget p2, p0, Lcom/miui/packageInstaller/model/ApkInfo;->newInstall:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    iget-object p2, p0, Lcom/miui/packageInstaller/model/ApkInfo;->apkSignature:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object p2, p0, Lcom/miui/packageInstaller/model/ApkInfo;->apkSignature2:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object p2, p0, Lcom/miui/packageInstaller/model/ApkInfo;->apkSignatureSha1:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object p2, p0, Lcom/miui/packageInstaller/model/ApkInfo;->apkSignatureSha256:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object p2, p0, Lcom/miui/packageInstaller/model/ApkInfo;->label:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object p2, p0, Lcom/miui/packageInstaller/model/ApkInfo;->originalFilePath:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object p2, p0, Lcom/miui/packageInstaller/model/ApkInfo;->realFileName:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object p2, p0, Lcom/miui/packageInstaller/model/ApkInfo;->cloudParams:Lcom/miui/packageInstaller/model/CloudParams;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    iget-object p2, p0, Lcom/miui/packageInstaller/model/ApkInfo;->channelMessage:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
