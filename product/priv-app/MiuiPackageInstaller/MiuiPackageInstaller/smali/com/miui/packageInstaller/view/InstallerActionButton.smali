.class public Lcom/miui/packageInstaller/view/InstallerActionButton;
.super Lcom/miui/packageInstaller/view/o;

# interfaces
.implements Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-static {p1}, Lm8/i;->c(Ljava/lang/Object;)V

    invoke-direct {p0, p1, p2}, Lcom/miui/packageInstaller/view/o;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public a()Landroid/view/View;
    .locals 0

    return-object p0
.end method

.method public getButtonText()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setButtonBackgroundResource(I)V
    .locals 0

    invoke-virtual {p0, p1}, Landroidx/appcompat/widget/AppCompatButton;->setBackgroundResource(I)V

    return-void
.end method

.method public setButtonText(Ljava/lang/CharSequence;)V
    .locals 0

    invoke-virtual {p0, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setButtonTextColor(I)V
    .locals 0

    invoke-virtual {p0, p1}, Landroid/widget/Button;->setTextColor(I)V

    return-void
.end method

.method public setClick(Landroid/view/View$OnClickListener;)V
    .locals 1

    const-string v0, "l"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setProgressVisibility(Z)V
    .locals 0

    invoke-static {p0, p1}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b$a;->a(Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;Z)V

    return-void
.end method
