.class public final Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$b;
.super Lva/i;

# interfaces
.implements Landroidx/preference/Preference$d;
.implements Landroidx/preference/Preference$e;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$b$a;
    }
.end annotation


# instance fields
.field private final A0:Ljava/lang/String;

.field private final B0:Ljava/lang/String;

.field private final C0:Ljava/lang/String;

.field private final D0:Ljava/lang/String;

.field private E0:Landroidx/preference/Preference;

.field private F0:Landroidx/preference/Preference;

.field private G0:Landroidx/preference/Preference;

.field private H0:Landroidx/preference/Preference;

.field private I0:Lmiuix/preference/TextPreference;

.field private J0:Z

.field private K0:Landroid/view/View;

.field private L0:Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$b$a;

.field private final z0:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lva/i;-><init>()V

    const-string v0, "pref_key_what_is_security_mode"

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$b;->z0:Ljava/lang/String;

    const-string v0, "pref_key_security_mode_introduce"

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$b;->A0:Ljava/lang/String;

    const-string v0, "pref_key_security_mode_trust_risk_app"

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$b;->B0:Ljava/lang/String;

    const-string v0, "pref_key_security_mode_service_mode"

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$b;->C0:Ljava/lang/String;

    const-string v0, "pref_key_security_mode_security_verify_risk_app"

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$b;->D0:Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$b;->J0:Z

    return-void
.end method

.method public static final synthetic j2(Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$b;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$b;->n2()V

    return-void
.end method

.method public static final synthetic k2(Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$b;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$b;->J0:Z

    return-void
.end method

.method private final l2(I)V
    .locals 2

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->p()Landroidx/fragment/app/e;

    move-result-object v0

    invoke-static {v0}, Lm2/b;->g(Landroid/content/Context;)Lm2/b;

    move-result-object v0

    if-eqz p1, :cond_1

    const/4 v1, 0x1

    if-eq p1, v1, :cond_0

    goto :goto_0

    :cond_0
    sget-object p1, Lx5/a;->c:Lx5/a;

    goto :goto_1

    :cond_1
    :goto_0
    sget-object p1, Lx5/a;->f:Lx5/a;

    :goto_1
    invoke-virtual {v0, p1}, Lm2/b;->J(Lx5/a;)V

    return-void
.end method

.method private final m2()V
    .locals 3

    new-instance v0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$b$a;

    invoke-direct {v0, p0}, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$b$a;-><init>(Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$b;)V

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$b;->L0:Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$b$a;

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "secure_mode_verification_broadcast.Action"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->p()Landroidx/fragment/app/e;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$b;->L0:Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$b$a;

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :cond_0
    return-void
.end method

.method private final n2()V
    .locals 2

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$b;->I0:Lmiuix/preference/TextPreference;

    if-eqz v0, :cond_1

    iget-boolean v1, p0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$b;->J0:Z

    if-eqz v1, :cond_0

    const v1, 0x7f1102e9

    goto :goto_0

    :cond_0
    const v1, 0x7f1102ea

    :goto_0
    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->T(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiuix/preference/TextPreference;->K0(Ljava/lang/String;)V

    :cond_1
    return-void
.end method


# virtual methods
.method public J0()V
    .locals 2

    invoke-super {p0}, Landroidx/fragment/app/Fragment;->J0()V

    invoke-virtual {p0}, Landroidx/preference/g;->K1()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOverScrollMode(I)V

    invoke-virtual {p0}, Landroidx/preference/g;->K1()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVerticalScrollBarEnabled(Z)V

    return-void
.end method

.method public Q1(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 6

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result p1

    if-eqz p1, :cond_0

    const p1, 0x7f140005

    goto :goto_0

    :cond_0
    const p1, 0x7f140004

    :goto_0
    invoke-virtual {p0, p1, p2}, Landroidx/preference/g;->Y1(ILjava/lang/String;)V

    iget-object p1, p0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$b;->z0:Ljava/lang/String;

    invoke-virtual {p0, p1}, Landroidx/preference/g;->g(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$b;->E0:Landroidx/preference/Preference;

    if-nez p1, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->v0(Landroidx/preference/Preference$e;)V

    :goto_1
    iget-object p1, p0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$b;->A0:Ljava/lang/String;

    invoke-virtual {p0, p1}, Landroidx/preference/g;->g(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$b;->F0:Landroidx/preference/Preference;

    if-nez p1, :cond_2

    goto :goto_2

    :cond_2
    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->v0(Landroidx/preference/Preference$e;)V

    :goto_2
    iget-object p1, p0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$b;->B0:Ljava/lang/String;

    invoke-virtual {p0, p1}, Landroidx/preference/g;->g(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$b;->G0:Landroidx/preference/Preference;

    if-nez p1, :cond_3

    goto :goto_3

    :cond_3
    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->v0(Landroidx/preference/Preference$e;)V

    :goto_3
    iget-object p1, p0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$b;->D0:Ljava/lang/String;

    invoke-virtual {p0, p1}, Landroidx/preference/g;->g(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    check-cast p1, Lmiuix/preference/TextPreference;

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$b;->I0:Lmiuix/preference/TextPreference;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->w()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->isSafeModelEnable(Landroid/content/Context;)Z

    move-result p1

    const-string p2, "null cannot be cast to non-null type com.miui.packageInstaller.analytics.IPage"

    const-string v0, "button"

    const/4 v1, 0x0

    if-eqz p1, :cond_5

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->w()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->getSecurityModeStyle(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    const-string v2, "elder"

    invoke-static {p1, v2}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    iget-object v2, p0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$b;->G0:Landroidx/preference/Preference;

    if-nez v2, :cond_4

    goto :goto_4

    :cond_4
    invoke-virtual {v2, p1}, Landroidx/preference/Preference;->B0(Z)V

    :goto_4
    if-eqz p1, :cond_8

    new-instance p1, Lp5/g;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->p()Landroidx/fragment/app/e;

    move-result-object v2

    invoke-static {v2, p2}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lo5/a;

    const-string v3, "trust_risk_app_btn"

    invoke-direct {p1, v3, v0, v2}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p1}, Lp5/f;->c()Z

    goto :goto_6

    :cond_5
    iget-object p1, p0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$b;->G0:Landroidx/preference/Preference;

    if-nez p1, :cond_6

    goto :goto_5

    :cond_6
    invoke-virtual {p1, v1}, Landroidx/preference/Preference;->B0(Z)V

    :goto_5
    iget-object p1, p0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$b;->I0:Lmiuix/preference/TextPreference;

    if-nez p1, :cond_7

    goto :goto_6

    :cond_7
    invoke-virtual {p1, v1}, Landroidx/preference/Preference;->B0(Z)V

    :cond_8
    :goto_6
    iget-object p1, p0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$b;->C0:Ljava/lang/String;

    invoke-virtual {p0, p1}, Landroidx/preference/g;->g(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object p1

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$b;->H0:Landroidx/preference/Preference;

    if-nez p1, :cond_9

    goto :goto_7

    :cond_9
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->w()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->isSafeModelEnable(Landroid/content/Context;)Z

    move-result v2

    invoke-virtual {p1, v2}, Landroidx/preference/Preference;->B0(Z)V

    :goto_7
    iget-object p1, p0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$b;->H0:Landroidx/preference/Preference;

    if-eqz p1, :cond_a

    invoke-virtual {p1}, Landroidx/preference/Preference;->q()I

    move-result p1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->w()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$b;->K0:Landroid/view/View;

    :cond_a
    iget-object p1, p0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$b;->I0:Lmiuix/preference/TextPreference;

    if-nez p1, :cond_b

    goto :goto_8

    :cond_b
    invoke-virtual {p1, p0}, Landroidx/preference/Preference;->v0(Landroidx/preference/Preference$e;)V

    :goto_8
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->w()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lm2/b;->g(Landroid/content/Context;)Lm2/b;

    move-result-object p1

    invoke-virtual {p1}, Lm2/b;->j()Z

    move-result p1

    if-nez p1, :cond_d

    sget-object p1, Lf6/s;->a:Lf6/s$a;

    invoke-virtual {p1}, Lf6/s$a;->a()Lf6/s;

    move-result-object v2

    const-string v3, "safe_mode_verify_type_cloud_config"

    invoke-virtual {v2, v3}, Lf6/s;->e(Ljava/lang/String;)I

    move-result v2

    const/4 v4, 0x2

    const/4 v5, 0x1

    if-eq v2, v4, :cond_c

    move v1, v5

    :cond_c
    iput-boolean v1, p0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$b;->J0:Z

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->w()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lm2/b;->g(Landroid/content/Context;)Lm2/b;

    move-result-object v1

    iget-boolean v2, p0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$b;->J0:Z

    invoke-virtual {v1, v2}, Lm2/b;->I(Z)V

    invoke-virtual {p1}, Lf6/s$a;->a()Lf6/s;

    move-result-object p1

    invoke-virtual {p1, v3}, Lf6/s;->e(Ljava/lang/String;)I

    move-result p1

    invoke-direct {p0, p1}, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$b;->l2(I)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->w()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lm2/b;->g(Landroid/content/Context;)Lm2/b;

    move-result-object p1

    invoke-virtual {p1, v5}, Lm2/b;->H(Z)V

    goto :goto_9

    :cond_d
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->w()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lm2/b;->g(Landroid/content/Context;)Lm2/b;

    move-result-object p1

    invoke-virtual {p1}, Lm2/b;->s()Z

    move-result p1

    iput-boolean p1, p0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$b;->J0:Z

    :goto_9
    new-instance p1, Lp5/g;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->p()Landroidx/fragment/app/e;

    move-result-object v1

    invoke-static {v1, p2}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lo5/a;

    const-string p2, "app_safe_verify_btn"

    invoke-direct {p1, p2, v0, v1}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p1}, Lp5/f;->c()Z

    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$b;->n2()V

    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$b;->m2()V

    return-void
.end method

.method public c(Landroidx/preference/Preference;Ljava/lang/Object;)Z
    .locals 0

    const/4 p1, 0x1

    return p1
.end method

.method public f(Landroidx/preference/Preference;)Z
    .locals 7

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$b;->E0:Landroidx/preference/Preference;

    invoke-static {p1, v0}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    const-string v2, "null cannot be cast to non-null type com.miui.packageInstaller.analytics.IPage"

    const-string v3, "button"

    if-eqz v0, :cond_1

    new-instance p1, Landroid/content/Intent;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->p()Landroidx/fragment/app/e;

    move-result-object v0

    const-class v4, Lcom/miui/packageInstaller/ui/secure/InstallerWebViewActivity;

    invoke-direct {p1, v0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v0, "hasTitle"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    sget-boolean v0, Lq2/c;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "http://fe.market.pt.xiaomi.com/hd/apm-h5-cdn/cdn-safe-guard.html"

    goto :goto_0

    :cond_0
    const-string v0, "https://app.market.xiaomi.com/hd/apm-h5-cdn/cdn-safe-guard.html"

    :goto_0
    const-string v4, "jump_url"

    invoke-virtual {p1, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, p1}, Landroidx/fragment/app/Fragment;->E1(Landroid/content/Intent;)V

    new-instance p1, Lp5/b;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->p()Landroidx/fragment/app/e;

    move-result-object v0

    invoke-static {v0, v2}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lo5/a;

    const-string v2, "safe_mode_define_btn"

    invoke-direct {p1, v2, v3, v0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    goto/16 :goto_2

    :cond_1
    iget-object v0, p0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$b;->F0:Landroidx/preference/Preference;

    invoke-static {p1, v0}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const-string v4, "null cannot be cast to non-null type com.miui.packageInstaller.ui.setting.SecurityModeIntroduceActivity"

    if-eqz v0, :cond_3

    new-instance p1, Landroid/content/Intent;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->p()Landroidx/fragment/app/e;

    move-result-object v0

    const-class v5, Lcom/miui/packageInstaller/ui/SecureModeAdvantageActivity;

    invoke-direct {p1, v0, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->w()Landroid/content/Context;

    move-result-object v0

    instance-of v0, v0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->w()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v4}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;

    invoke-static {v0}, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;->N0(Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;)Lo5/b;

    move-result-object v0

    goto :goto_1

    :cond_2
    new-instance v0, Lo5/b;

    const/4 v4, 0x2

    const-string v5, "SecurityModeIntroduceActivity"

    const/4 v6, 0x0

    invoke-direct {v0, v5, v6, v4, v6}, Lo5/b;-><init>(Ljava/lang/String;Ljava/lang/String;ILm8/g;)V

    :goto_1
    const-string v4, "fromPage"

    invoke-virtual {p1, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    invoke-virtual {p0, p1}, Landroidx/fragment/app/Fragment;->E1(Landroid/content/Intent;)V

    new-instance p1, Lp5/b;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->p()Landroidx/fragment/app/e;

    move-result-object v0

    invoke-static {v0, v2}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lo5/a;

    const-string v2, "safe_mode_intro_btn"

    invoke-direct {p1, v2, v3, v0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$b;->G0:Landroidx/preference/Preference;

    invoke-static {p1, v0}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance p1, Landroid/content/Intent;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->p()Landroidx/fragment/app/e;

    move-result-object v0

    const-class v5, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustListActivity;

    invoke-direct {p1, v0, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->p()Landroidx/fragment/app/e;

    move-result-object v0

    invoke-static {v0, v4}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;

    invoke-static {v0}, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;->M0(Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;)Lm5/e;

    move-result-object v0

    const-string v5, "caller"

    invoke-virtual {p1, v5, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->p()Landroidx/fragment/app/e;

    move-result-object v0

    invoke-static {v0, v4}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;

    invoke-static {v0}, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;->L0(Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;)Lcom/miui/packageInstaller/model/ApkInfo;

    move-result-object v0

    const-string v4, "apk_info"

    invoke-virtual {p1, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-virtual {p0, p1}, Landroidx/fragment/app/Fragment;->E1(Landroid/content/Intent;)V

    new-instance p1, Lp5/b;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->p()Landroidx/fragment/app/e;

    move-result-object v0

    invoke-static {v0, v2}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lo5/a;

    const-string v2, "trust_risk_app_btn"

    invoke-direct {p1, v2, v3, v0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    :goto_2
    invoke-virtual {p1}, Lp5/f;->c()Z

    goto :goto_3

    :cond_4
    iget-object v0, p0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$b;->I0:Lmiuix/preference/TextPreference;

    invoke-static {p1, v0}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_5

    new-instance p1, Landroid/content/Intent;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->p()Landroidx/fragment/app/e;

    move-result-object v0

    const-class v4, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity;

    invoke-direct {p1, v0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->w()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lm2/b;->g(Landroid/content/Context;)Lm2/b;

    move-result-object v0

    invoke-virtual {v0}, Lm2/b;->s()Z

    move-result v0

    const-string v4, "secure_mode_verification"

    invoke-virtual {p1, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0, p1}, Landroidx/fragment/app/Fragment;->E1(Landroid/content/Intent;)V

    new-instance p1, Lp5/b;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->p()Landroidx/fragment/app/e;

    move-result-object v0

    invoke-static {v0, v2}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lo5/a;

    const-string v2, "app_safe_verify_btn"

    invoke-direct {p1, v2, v3, v0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    goto :goto_2

    :cond_5
    :goto_3
    return v1
.end method

.method public t0()V
    .locals 2

    invoke-super {p0}, Landroidx/fragment/app/Fragment;->t0()V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$b;->L0:Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$b$a;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->p()Landroidx/fragment/app/e;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1, v0}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_0
    return-void
.end method
