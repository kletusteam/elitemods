.class public final Lcom/miui/packageInstaller/ui/MarketInstallProgress;
.super Landroid/widget/LinearLayout;

# interfaces
.implements Lz5/j;


# instance fields
.field private a:Lcom/xiaomi/market/h52native/view/downloadbutton/TextProgressBar;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public a(IJ)V
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/MarketInstallProgress;->a:Lcom/xiaomi/market/h52native/view/downloadbutton/TextProgressBar;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p2, p3}, Lcom/xiaomi/market/h52native/view/downloadbutton/TextProgressBar;->setSize(J)V

    :cond_0
    iget-object p2, p0, Lcom/miui/packageInstaller/ui/MarketInstallProgress;->a:Lcom/xiaomi/market/h52native/view/downloadbutton/TextProgressBar;

    if-eqz p2, :cond_1

    int-to-float p1, p1

    invoke-virtual {p2, p1}, Lcom/xiaomi/market/h52native/view/downloadbutton/TextProgressBar;->setProgress(F)V

    :cond_1
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    const v0, 0x7f0a036a

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/xiaomi/market/h52native/view/downloadbutton/TextProgressBar;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/MarketInstallProgress;->a:Lcom/xiaomi/market/h52native/view/downloadbutton/TextProgressBar;

    return-void
.end method

.method public setClick(Landroid/view/View$OnClickListener;)V
    .locals 1

    const-string v0, "l"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/MarketInstallProgress;->a:Lcom/xiaomi/market/h52native/view/downloadbutton/TextProgressBar;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void
.end method

.method public setProgressText(Ljava/lang/CharSequence;)V
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/MarketInstallProgress;->a:Lcom/xiaomi/market/h52native/view/downloadbutton/TextProgressBar;

    if-eqz v0, :cond_0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/xiaomi/market/h52native/view/downloadbutton/TextProgressBar;->setText(Ljava/lang/String;)V

    :cond_0
    return-void
.end method
