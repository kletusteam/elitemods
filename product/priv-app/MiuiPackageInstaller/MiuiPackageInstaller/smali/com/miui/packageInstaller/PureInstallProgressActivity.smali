.class public final Lcom/miui/packageInstaller/PureInstallProgressActivity;
.super Lcom/miui/packageInstaller/a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/packageInstaller/PureInstallProgressActivity$a;
    }
.end annotation


# static fields
.field public static final l0:Lcom/miui/packageInstaller/PureInstallProgressActivity$a;


# instance fields
.field private W:Ll6/d;

.field private X:Landroidx/recyclerview/widget/RecyclerView;

.field private Y:Lcom/miui/packageInstaller/ui/listcomponets/l;

.field private Z:I

.field private e0:Landroid/widget/TextView;

.field private f0:Landroid/widget/TextView;

.field private g0:I

.field private h0:Landroid/widget/FrameLayout;

.field private i0:Landroid/widget/ImageView;

.field private j0:Landroid/view/ViewGroup;

.field private k0:Landroid/view/ViewGroup;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/miui/packageInstaller/PureInstallProgressActivity$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/miui/packageInstaller/PureInstallProgressActivity$a;-><init>(Lm8/g;)V

    sput-object v0, Lcom/miui/packageInstaller/PureInstallProgressActivity;->l0:Lcom/miui/packageInstaller/PureInstallProgressActivity$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/miui/packageInstaller/a;-><init>()V

    new-instance v0, Ll6/d;

    invoke-direct {v0}, Ll6/d;-><init>()V

    iput-object v0, p0, Lcom/miui/packageInstaller/PureInstallProgressActivity;->W:Ll6/d;

    const/4 v0, 0x1

    iput v0, p0, Lcom/miui/packageInstaller/PureInstallProgressActivity;->Z:I

    iput v0, p0, Lcom/miui/packageInstaller/PureInstallProgressActivity;->g0:I

    return-void
.end method

.method private final M1()V
    .locals 5

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    const-string v2, "null cannot be cast to non-null type androidx.constraintlayout.widget.ConstraintLayout.LayoutParams"

    const-string v3, "null cannot be cast to non-null type android.widget.FrameLayout.LayoutParams"

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/miui/packageInstaller/PureInstallProgressActivity;->i0:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-static {v1, v3}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0700e0

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/widget/FrameLayout$LayoutParams;->setMarginEnd(I)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_0
    iget-object v0, p0, Lcom/miui/packageInstaller/PureInstallProgressActivity;->j0:Landroid/view/ViewGroup;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-static {v1, v2}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Landroidx/constraintlayout/widget/ConstraintLayout$b;

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070146

    :goto_0
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMarginStart(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMarginEnd(I)V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/miui/packageInstaller/PureInstallProgressActivity;->i0:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-static {v1, v3}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0700c1

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/widget/FrameLayout$LayoutParams;->setMarginEnd(I)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_2
    iget-object v0, p0, Lcom/miui/packageInstaller/PureInstallProgressActivity;->j0:Landroid/view/ViewGroup;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-static {v1, v2}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Landroidx/constraintlayout/widget/ConstraintLayout$b;

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0700dc

    goto :goto_0

    :cond_3
    :goto_1
    return-void
.end method


# virtual methods
.method public final L1()Ljava/util/List;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lm6/a<",
            "*>;>;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/miui/packageInstaller/a;->h1()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-ne v1, v3, :cond_0

    move v1, v3

    goto :goto_0

    :cond_0
    move v1, v2

    :goto_0
    const/4 v4, 0x0

    if-eqz v1, :cond_1

    new-instance v1, Lcom/miui/packageInstaller/ui/listcomponets/l;

    invoke-virtual {p0}, Lcom/miui/packageInstaller/a;->f1()Lcom/miui/packageInstaller/model/ApkInfo;

    move-result-object v7

    invoke-static {v7}, Lm8/i;->c(Ljava/lang/Object;)V

    invoke-virtual {p0}, Lcom/miui/packageInstaller/a;->g1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v8

    invoke-static {v8}, Lm8/i;->c(Ljava/lang/Object;)V

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v5, v1

    move-object v6, p0

    invoke-direct/range {v5 .. v10}, Lcom/miui/packageInstaller/ui/listcomponets/l;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/ApkInfo;Lcom/miui/packageInstaller/model/CloudParams;Ll6/c;Lm6/b;)V

    iput-object v1, p0, Lcom/miui/packageInstaller/PureInstallProgressActivity;->Y:Lcom/miui/packageInstaller/ui/listcomponets/l;

    invoke-virtual {v1, v2}, Lm6/a;->w(Z)V

    iget-object v1, p0, Lcom/miui/packageInstaller/PureInstallProgressActivity;->Y:Lcom/miui/packageInstaller/ui/listcomponets/l;

    invoke-static {v1}, Lm8/i;->c(Ljava/lang/Object;)V

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lcom/miui/packageInstaller/a;->f1()Lcom/miui/packageInstaller/model/ApkInfo;

    move-result-object v1

    if-eqz v1, :cond_2

    new-instance v1, Lcom/miui/packageInstaller/ui/listcomponets/k;

    invoke-virtual {p0}, Lcom/miui/packageInstaller/a;->f1()Lcom/miui/packageInstaller/model/ApkInfo;

    move-result-object v5

    invoke-static {v5}, Lm8/i;->c(Ljava/lang/Object;)V

    invoke-direct {v1, p0, v5, v4, v4}, Lcom/miui/packageInstaller/ui/listcomponets/k;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/ApkInfo;Ll6/c;Lm6/b;)V

    invoke-virtual {v1, v2}, Lm6/a;->w(Z)V

    :goto_1
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    iget v1, p0, Lcom/miui/packageInstaller/PureInstallProgressActivity;->Z:I

    if-eq v1, v3, :cond_a

    invoke-virtual {p0}, Lcom/miui/packageInstaller/a;->f1()Lcom/miui/packageInstaller/model/ApkInfo;

    move-result-object v8

    if-eqz v8, :cond_a

    new-instance v7, Lcom/miui/packageInstaller/model/PureModeTip;

    invoke-direct {v7}, Lcom/miui/packageInstaller/model/PureModeTip;-><init>()V

    invoke-virtual {p0}, Lcom/miui/packageInstaller/a;->j1()Lcom/miui/packageInstaller/model/Virus;

    move-result-object v1

    if-eqz v1, :cond_5

    const/4 v1, 0x2

    invoke-virtual {v7, v1}, Lcom/miui/packageInstaller/model/PureModeTip;->setLevel(I)V

    invoke-virtual {p0}, Lcom/miui/packageInstaller/a;->j1()Lcom/miui/packageInstaller/model/Virus;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, v1, Lcom/miui/packageInstaller/model/Virus;->name:Ljava/lang/String;

    goto :goto_2

    :cond_3
    move-object v1, v4

    :goto_2
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Lcom/miui/packageInstaller/model/PureModeTip;->setTitle(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/miui/packageInstaller/a;->j1()Lcom/miui/packageInstaller/model/Virus;

    move-result-object v1

    if-eqz v1, :cond_4

    iget-object v4, v1, Lcom/miui/packageInstaller/model/Virus;->virusInfo:Ljava/lang/String;

    :cond_4
    :goto_3
    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Lcom/miui/packageInstaller/model/PureModeTip;->setMessage(Ljava/lang/String;)V

    goto :goto_7

    :cond_5
    invoke-virtual {v7, v3}, Lcom/miui/packageInstaller/model/PureModeTip;->setLevel(I)V

    invoke-virtual {p0}, Lcom/miui/packageInstaller/a;->g1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v1

    if-eqz v1, :cond_6

    iget-boolean v1, v1, Lcom/miui/packageInstaller/model/CloudParams;->bundleApp:Z

    if-ne v1, v3, :cond_6

    goto :goto_4

    :cond_6
    move v3, v2

    :goto_4
    invoke-virtual {p0}, Lcom/miui/packageInstaller/a;->g1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v1

    if-eqz v3, :cond_7

    if-eqz v1, :cond_8

    iget-object v1, v1, Lcom/miui/packageInstaller/model/CloudParams;->originalBundleAppInfo:Lcom/miui/packageInstaller/model/OriginalBundleAppInfo;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/OriginalBundleAppInfo;->getSecureWarningTip()Lcom/miui/packageInstaller/model/WarningCardInfo;

    move-result-object v1

    goto :goto_5

    :cond_7
    if-eqz v1, :cond_8

    iget-object v1, v1, Lcom/miui/packageInstaller/model/CloudParams;->secureWarningTip:Lcom/miui/packageInstaller/model/WarningCardInfo;

    goto :goto_5

    :cond_8
    move-object v1, v4

    :goto_5
    if-eqz v1, :cond_9

    iget-object v3, v1, Lcom/miui/packageInstaller/model/WarningCardInfo;->title:Ljava/lang/String;

    goto :goto_6

    :cond_9
    move-object v3, v4

    :goto_6
    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v3}, Lcom/miui/packageInstaller/model/PureModeTip;->setTitle(Ljava/lang/String;)V

    if-eqz v1, :cond_4

    iget-object v4, v1, Lcom/miui/packageInstaller/model/WarningCardInfo;->text:Ljava/lang/String;

    goto :goto_3

    :goto_7
    new-instance v1, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject;

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/16 v11, 0x18

    const/4 v12, 0x0

    move-object v5, v1

    move-object v6, p0

    invoke-direct/range {v5 .. v12}, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/PureModeTip;Lcom/miui/packageInstaller/model/ApkInfo;Ll6/c;Lm6/b;ILm8/g;)V

    invoke-virtual {v1, v2}, Lm6/a;->w(Z)V

    invoke-virtual {v1}, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject;->a()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_a
    const v1, 0x7f0a01b0

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/miui/packageInstaller/ui/InstallerActionBar;

    invoke-virtual {p0, v1}, Lcom/miui/packageInstaller/a;->l1(Lcom/miui/packageInstaller/ui/InstallerActionBar;)V

    new-instance v1, Lcom/miui/packageInstaller/ui/listcomponets/FootViewObject;

    invoke-direct {v1, p0}, Lcom/miui/packageInstaller/ui/listcomponets/FootViewObject;-><init>(Landroid/content/Context;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method public final N1()V
    .locals 8

    iget-object v0, p0, Lcom/miui/packageInstaller/PureInstallProgressActivity;->f0:Landroid/widget/TextView;

    const/4 v1, 0x1

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f0017

    iget v4, p0, Lcom/miui/packageInstaller/PureInstallProgressActivity;->g0:I

    new-array v5, v1, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    iget v0, p0, Lcom/miui/packageInstaller/PureInstallProgressActivity;->Z:I

    const v2, 0x7f080575

    const v3, 0x7f080574

    if-ne v0, v1, :cond_3

    invoke-virtual {p0}, Lcom/miui/packageInstaller/a;->h1()I

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/miui/packageInstaller/PureInstallProgressActivity;->e0:Landroid/widget/TextView;

    if-nez v0, :cond_1

    goto :goto_1

    :cond_1
    const v1, 0x7f110254

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    iget-object v0, p0, Lcom/miui/packageInstaller/PureInstallProgressActivity;->h0:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_2

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    :cond_2
    iget-object v0, p0, Lcom/miui/packageInstaller/PureInstallProgressActivity;->i0:Landroid/widget/ImageView;

    if-eqz v0, :cond_15

    :goto_2
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_9

    :cond_3
    iget v0, p0, Lcom/miui/packageInstaller/PureInstallProgressActivity;->Z:I

    if-ne v0, v1, :cond_6

    invoke-virtual {p0}, Lcom/miui/packageInstaller/a;->h1()I

    move-result v0

    if-ne v0, v1, :cond_6

    iget-object v0, p0, Lcom/miui/packageInstaller/PureInstallProgressActivity;->e0:Landroid/widget/TextView;

    if-nez v0, :cond_4

    goto :goto_3

    :cond_4
    const v1, 0x7f110263

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_3
    iget-object v0, p0, Lcom/miui/packageInstaller/PureInstallProgressActivity;->h0:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_5

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    :cond_5
    iget-object v0, p0, Lcom/miui/packageInstaller/PureInstallProgressActivity;->i0:Landroid/widget/ImageView;

    if-eqz v0, :cond_15

    goto :goto_2

    :cond_6
    iget v0, p0, Lcom/miui/packageInstaller/PureInstallProgressActivity;->Z:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_9

    iget-object v0, p0, Lcom/miui/packageInstaller/PureInstallProgressActivity;->e0:Landroid/widget/TextView;

    if-nez v0, :cond_7

    goto :goto_4

    :cond_7
    const v1, 0x7f11005d

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_4
    iget-object v0, p0, Lcom/miui/packageInstaller/PureInstallProgressActivity;->h0:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_8

    const v1, 0x7f080533

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    :cond_8
    iget-object v0, p0, Lcom/miui/packageInstaller/PureInstallProgressActivity;->i0:Landroid/widget/ImageView;

    if-eqz v0, :cond_15

    const v1, 0x7f080535

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_9

    :cond_9
    const/4 v1, 0x3

    const v2, 0x7f0805a3

    const v3, 0x7f0805a1

    if-ne v0, v1, :cond_c

    iget-object v0, p0, Lcom/miui/packageInstaller/PureInstallProgressActivity;->e0:Landroid/widget/TextView;

    if-nez v0, :cond_a

    goto :goto_5

    :cond_a
    const v1, 0x7f1100c2

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_5
    iget-object v0, p0, Lcom/miui/packageInstaller/PureInstallProgressActivity;->h0:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_b

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    :cond_b
    iget-object v0, p0, Lcom/miui/packageInstaller/PureInstallProgressActivity;->i0:Landroid/widget/ImageView;

    if-eqz v0, :cond_15

    goto :goto_2

    :cond_c
    const/4 v1, 0x4

    if-ne v0, v1, :cond_f

    iget-object v0, p0, Lcom/miui/packageInstaller/PureInstallProgressActivity;->e0:Landroid/widget/TextView;

    if-nez v0, :cond_d

    goto :goto_6

    :cond_d
    const v1, 0x7f110042

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_6
    iget-object v0, p0, Lcom/miui/packageInstaller/PureInstallProgressActivity;->h0:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_e

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    :cond_e
    iget-object v0, p0, Lcom/miui/packageInstaller/PureInstallProgressActivity;->i0:Landroid/widget/ImageView;

    if-eqz v0, :cond_15

    goto/16 :goto_2

    :cond_f
    const/4 v1, 0x5

    if-ne v0, v1, :cond_12

    iget-object v0, p0, Lcom/miui/packageInstaller/PureInstallProgressActivity;->e0:Landroid/widget/TextView;

    if-nez v0, :cond_10

    goto :goto_7

    :cond_10
    const v1, 0x7f11005b

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_7
    iget-object v0, p0, Lcom/miui/packageInstaller/PureInstallProgressActivity;->h0:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_11

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    :cond_11
    iget-object v0, p0, Lcom/miui/packageInstaller/PureInstallProgressActivity;->i0:Landroid/widget/ImageView;

    if-eqz v0, :cond_15

    goto/16 :goto_2

    :cond_12
    const/4 v1, 0x6

    if-ne v0, v1, :cond_15

    iget-object v0, p0, Lcom/miui/packageInstaller/PureInstallProgressActivity;->e0:Landroid/widget/TextView;

    if-nez v0, :cond_13

    goto :goto_8

    :cond_13
    const v1, 0x7f11005c

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_8
    iget-object v0, p0, Lcom/miui/packageInstaller/PureInstallProgressActivity;->h0:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_14

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    :cond_14
    iget-object v0, p0, Lcom/miui/packageInstaller/PureInstallProgressActivity;->i0:Landroid/widget/ImageView;

    if-eqz v0, :cond_15

    goto/16 :goto_2

    :cond_15
    :goto_9
    return-void
.end method

.method public m1()V
    .locals 6

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "app_type_level"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/miui/packageInstaller/PureInstallProgressActivity;->Z:I

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0d0162

    goto :goto_0

    :cond_0
    const v0, 0x7f0d0161

    :goto_0
    invoke-virtual {p0, v0}, Lmiuix/appcompat/app/j;->setContentView(I)V

    invoke-super {p0}, Lcom/miui/packageInstaller/a;->m1()V

    const v0, 0x7f0a0383

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    invoke-static {}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->calculateSecurityModeProtectDays()I

    move-result v0

    iput v0, p0, Lcom/miui/packageInstaller/PureInstallProgressActivity;->g0:I

    const v0, 0x7f0a0384

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    const-string v3, "null cannot be cast to non-null type androidx.constraintlayout.widget.ConstraintLayout.LayoutParams"

    invoke-static {v0, v3}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroidx/constraintlayout/widget/ConstraintLayout$b;

    invoke-static {p0}, Lcom/android/packageinstaller/utils/u;->b(Landroid/app/Activity;)I

    move-result v3

    iput v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    const v0, 0x7f0a03c5

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/PureInstallProgressActivity;->e0:Landroid/widget/TextView;

    const v0, 0x7f0a03c6

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/PureInstallProgressActivity;->f0:Landroid/widget/TextView;

    const v0, 0x7f0a0173

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/miui/packageInstaller/PureInstallProgressActivity;->h0:Landroid/widget/FrameLayout;

    const v0, 0x7f0a01c9

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/miui/packageInstaller/PureInstallProgressActivity;->i0:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/miui/packageInstaller/PureInstallProgressActivity;->N1()V

    const v0, 0x7f0a0200

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/miui/packageInstaller/PureInstallProgressActivity;->j0:Landroid/view/ViewGroup;

    const v0, 0x7f0a0217

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/miui/packageInstaller/PureInstallProgressActivity;->k0:Landroid/view/ViewGroup;

    const v0, 0x7f0a0216

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v3, "findViewById(R.id.main_content)"

    invoke-static {v0, v3}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    iput-object v0, p0, Lcom/miui/packageInstaller/PureInstallProgressActivity;->X:Landroidx/recyclerview/widget/RecyclerView;

    const/4 v3, 0x0

    const-string v4, "mMainRecyclerView"

    if-nez v0, :cond_1

    invoke-static {v4}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v3

    :cond_1
    const/4 v5, 0x2

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->setOverScrollMode(I)V

    iget-object v0, p0, Lcom/miui/packageInstaller/PureInstallProgressActivity;->X:Landroidx/recyclerview/widget/RecyclerView;

    if-nez v0, :cond_2

    invoke-static {v4}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v3

    :cond_2
    new-instance v5, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-direct {v5, p0, v2, v1}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    invoke-virtual {v0, v5}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$o;)V

    new-instance v0, Lj6/b;

    iget-object v1, p0, Lcom/miui/packageInstaller/PureInstallProgressActivity;->X:Landroidx/recyclerview/widget/RecyclerView;

    if-nez v1, :cond_3

    invoke-static {v4}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    move-object v3, v1

    :goto_1
    invoke-direct {v0, v3}, Lj6/b;-><init>(Landroidx/recyclerview/widget/RecyclerView;)V

    invoke-virtual {p0, v0}, Lcom/miui/packageInstaller/a;->K1(Lj6/b;)V

    invoke-virtual {p0}, Lcom/miui/packageInstaller/a;->e1()Lj6/b;

    move-result-object v0

    invoke-virtual {p0}, Lcom/miui/packageInstaller/PureInstallProgressActivity;->L1()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lj6/b;->M(Ljava/util/List;)I

    invoke-direct {p0}, Lcom/miui/packageInstaller/PureInstallProgressActivity;->M1()V

    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    const-string v0, "newConfig"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lq2/b;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    invoke-direct {p0}, Lcom/miui/packageInstaller/PureInstallProgressActivity;->M1()V

    return-void
.end method
