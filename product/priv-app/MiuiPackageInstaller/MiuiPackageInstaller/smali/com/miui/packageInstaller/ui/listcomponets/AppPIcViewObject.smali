.class public final Lcom/miui/packageInstaller/ui/listcomponets/AppPIcViewObject;
.super Lm6/a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/packageInstaller/ui/listcomponets/AppPIcViewObject$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lm6/a<",
        "Lcom/miui/packageInstaller/ui/listcomponets/AppPIcViewObject$ViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private final l:Lcom/miui/packageInstaller/model/MarketAppInfo$DetailVideoAndScreenshot;

.field private final m:I

.field private final n:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/MarketAppInfo$DetailVideoAndScreenshot;Ll6/c;Lm6/b;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mData"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2, p3, p4}, Lm6/a;-><init>(Landroid/content/Context;Ljava/lang/Object;Ll6/c;Lm6/b;)V

    iput-object p2, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppPIcViewObject;->l:Lcom/miui/packageInstaller/model/MarketAppInfo$DetailVideoAndScreenshot;

    const/4 p1, 0x1

    iput p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppPIcViewObject;->m:I

    return-void
.end method

.method private static final B(Lcom/miui/packageInstaller/ui/listcomponets/AppPIcViewObject$ViewHolder;Lcom/miui/packageInstaller/ui/listcomponets/AppPIcViewObject;Landroid/view/View;)V
    .locals 1

    const-string p2, "this$0"

    invoke-static {p1, p2}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "viewHolder?.adapterPosition = "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$d0;->getAdapterPosition()I

    move-result v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    const-string v0, "AppPIcViewObject"

    invoke-static {v0, p2}, Lf6/o;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$d0;->getAdapterPosition()I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    const p2, 0x7f0a02a6

    invoke-virtual {p1, p2, p0}, Lm6/a;->s(ILjava/lang/Object;)V

    return-void
.end method

.method public static synthetic z(Lcom/miui/packageInstaller/ui/listcomponets/AppPIcViewObject$ViewHolder;Lcom/miui/packageInstaller/ui/listcomponets/AppPIcViewObject;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/miui/packageInstaller/ui/listcomponets/AppPIcViewObject;->B(Lcom/miui/packageInstaller/ui/listcomponets/AppPIcViewObject$ViewHolder;Lcom/miui/packageInstaller/ui/listcomponets/AppPIcViewObject;Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public A(Lcom/miui/packageInstaller/ui/listcomponets/AppPIcViewObject$ViewHolder;)V
    .locals 4

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppPIcViewObject;->l:Lcom/miui/packageInstaller/model/MarketAppInfo$DetailVideoAndScreenshot;

    iget v0, v0, Lcom/miui/packageInstaller/model/MarketAppInfo$DetailVideoAndScreenshot;->orientation:I

    iget v1, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppPIcViewObject;->m:I

    const/4 v2, 0x0

    if-ne v0, v1, :cond_4

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/AppPIcViewObject$ViewHolder;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v2

    :goto_0
    if-nez v0, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0700d0

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    :goto_1
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/AppPIcViewObject$ViewHolder;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    :cond_2
    if-nez v2, :cond_3

    goto :goto_5

    :cond_3
    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070109

    :goto_2
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    goto :goto_5

    :cond_4
    iget v1, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppPIcViewObject;->n:I

    if-ne v0, v1, :cond_9

    if-eqz p1, :cond_5

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/AppPIcViewObject$ViewHolder;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    goto :goto_3

    :cond_5
    move-object v0, v2

    :goto_3
    if-nez v0, :cond_6

    goto :goto_4

    :cond_6
    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0700f3

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    :goto_4
    if-eqz p1, :cond_7

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/AppPIcViewObject$ViewHolder;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    :cond_7
    if-nez v2, :cond_8

    goto :goto_5

    :cond_8
    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0700b5

    goto :goto_2

    :cond_9
    :goto_5
    if-eqz p1, :cond_a

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/AppPIcViewObject$ViewHolder;->getImageP()Landroid/widget/ImageView;

    move-result-object v0

    if-eqz v0, :cond_a

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/bumptech/glide/b;->t(Landroid/content/Context;)Lcom/bumptech/glide/k;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppPIcViewObject;->l:Lcom/miui/packageInstaller/model/MarketAppInfo$DetailVideoAndScreenshot;

    iget-object v2, v2, Lcom/miui/packageInstaller/model/MarketAppInfo$DetailVideoAndScreenshot;->screenshot:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/bumptech/glide/k;->t(Ljava/lang/String;)Lcom/bumptech/glide/j;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/bumptech/glide/j;->w0(Landroid/widget/ImageView;)Ln3/i;

    :cond_a
    if-eqz p1, :cond_b

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/AppPIcViewObject$ViewHolder;->getImageP()Landroid/widget/ImageView;

    move-result-object v0

    if-eqz v0, :cond_b

    new-instance v1, Lcom/miui/packageInstaller/ui/listcomponets/e;

    invoke-direct {v1, p1, p0}, Lcom/miui/packageInstaller/ui/listcomponets/e;-><init>(Lcom/miui/packageInstaller/ui/listcomponets/AppPIcViewObject$ViewHolder;Lcom/miui/packageInstaller/ui/listcomponets/AppPIcViewObject;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_b
    return-void
.end method

.method public k()I
    .locals 1

    const v0, 0x7f0d0031

    return v0
.end method

.method public bridge synthetic o(Landroidx/recyclerview/widget/RecyclerView$d0;)V
    .locals 0

    check-cast p1, Lcom/miui/packageInstaller/ui/listcomponets/AppPIcViewObject$ViewHolder;

    invoke-virtual {p0, p1}, Lcom/miui/packageInstaller/ui/listcomponets/AppPIcViewObject;->A(Lcom/miui/packageInstaller/ui/listcomponets/AppPIcViewObject$ViewHolder;)V

    return-void
.end method
