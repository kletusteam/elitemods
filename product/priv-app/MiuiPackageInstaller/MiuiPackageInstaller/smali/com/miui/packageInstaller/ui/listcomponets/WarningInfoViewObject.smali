.class public Lcom/miui/packageInstaller/ui/listcomponets/WarningInfoViewObject;
.super Lcom/miui/packageInstaller/ui/listcomponets/f0;

# interfaces
.implements Lcom/miui/packageInstaller/ui/listcomponets/u;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/packageInstaller/ui/listcomponets/WarningInfoViewObject$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/miui/packageInstaller/ui/listcomponets/f0<",
        "Lcom/miui/packageInstaller/ui/listcomponets/WarningInfoViewObject$ViewHolder;",
        ">;",
        "Lcom/miui/packageInstaller/ui/listcomponets/u;"
    }
.end annotation


# instance fields
.field private m:Lcom/miui/packageInstaller/model/CloudParams;

.field private n:Lcom/miui/packageInstaller/model/WarningCardInfo;

.field private o:Lcom/miui/packageInstaller/ui/listcomponets/WarningInfoViewObject$ViewHolder;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/CloudParams;Ll6/c;Lm6/b;)V
    .locals 2

    const-string v0, "context"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mData"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/miui/packageInstaller/ui/listcomponets/f0;-><init>(Landroid/content/Context;Ljava/lang/Object;Ll6/c;Lm6/b;)V

    iput-object p2, p0, Lcom/miui/packageInstaller/ui/listcomponets/WarningInfoViewObject;->m:Lcom/miui/packageInstaller/model/CloudParams;

    iget-object p3, p2, Lcom/miui/packageInstaller/model/CloudParams;->secureWarningTip:Lcom/miui/packageInstaller/model/WarningCardInfo;

    iput-object p3, p0, Lcom/miui/packageInstaller/ui/listcomponets/WarningInfoViewObject;->n:Lcom/miui/packageInstaller/model/WarningCardInfo;

    iget-boolean p2, p2, Lcom/miui/packageInstaller/model/CloudParams;->storeListed:Z

    if-nez p2, :cond_2

    const/4 p2, 0x1

    const/4 p4, 0x0

    if-eqz p3, :cond_0

    iget v0, p3, Lcom/miui/packageInstaller/model/WarningCardInfo;->level:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    move v0, p2

    goto :goto_0

    :cond_0
    move v0, p4

    :goto_0
    if-nez v0, :cond_2

    if-eqz p3, :cond_1

    iget p3, p3, Lcom/miui/packageInstaller/model/WarningCardInfo;->level:I

    const/4 v0, 0x4

    if-ne p3, v0, :cond_1

    goto :goto_1

    :cond_1
    move p2, p4

    :goto_1
    if-nez p2, :cond_2

    instance-of p2, p1, Lo5/a;

    if-eqz p2, :cond_2

    new-instance p2, Lp5/g;

    check-cast p1, Lo5/a;

    const-string p3, "know_risk_app_btn"

    const-string p4, "button"

    invoke-direct {p2, p3, p4, p1}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p2}, Lp5/f;->c()Z

    :cond_2
    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/CloudParams;Ll6/c;Lm6/b;ILm8/g;)V
    .locals 1

    and-int/lit8 p6, p5, 0x4

    const/4 v0, 0x0

    if-eqz p6, :cond_0

    move-object p3, v0

    :cond_0
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_1

    move-object p4, v0

    :cond_1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/miui/packageInstaller/ui/listcomponets/WarningInfoViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/CloudParams;Ll6/c;Lm6/b;)V

    return-void
.end method

.method private final A(Landroid/content/res/Configuration;)V
    .locals 3

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/WarningInfoViewObject;->o:Lcom/miui/packageInstaller/ui/listcomponets/WarningInfoViewObject$ViewHolder;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/WarningInfoViewObject$ViewHolder;->getRootView()Landroid/widget/FrameLayout;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0700ce

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070145

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0, v2, v1}, Landroid/widget/FrameLayout;->setPadding(IIII)V

    :cond_0
    return-void
.end method


# virtual methods
.method public k()I
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/WarningInfoViewObject;->n:Lcom/miui/packageInstaller/model/WarningCardInfo;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/miui/packageInstaller/model/WarningCardInfo;->title:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/WarningInfoViewObject;->m:Lcom/miui/packageInstaller/model/CloudParams;

    iget-boolean v0, v0, Lcom/miui/packageInstaller/model/CloudParams;->showSafeModeTip:Z

    if-nez v0, :cond_1

    const v0, 0x7f0d0082

    goto :goto_1

    :cond_1
    const v0, 0x7f0d0081

    :goto_1
    return v0
.end method

.method public bridge synthetic o(Landroidx/recyclerview/widget/RecyclerView$d0;)V
    .locals 0

    check-cast p1, Lcom/miui/packageInstaller/ui/listcomponets/WarningInfoViewObject$ViewHolder;

    invoke-virtual {p0, p1}, Lcom/miui/packageInstaller/ui/listcomponets/WarningInfoViewObject;->z(Lcom/miui/packageInstaller/ui/listcomponets/WarningInfoViewObject$ViewHolder;)V

    return-void
.end method

.method public z(Lcom/miui/packageInstaller/ui/listcomponets/WarningInfoViewObject$ViewHolder;)V
    .locals 9

    const-string v0, "viewHolder"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/miui/packageInstaller/ui/listcomponets/f0;->o(Landroidx/recyclerview/widget/RecyclerView$d0;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/WarningInfoViewObject;->o:Lcom/miui/packageInstaller/ui/listcomponets/WarningInfoViewObject$ViewHolder;

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/WarningInfoViewObject;->m:Lcom/miui/packageInstaller/model/CloudParams;

    iget-boolean v0, v0, Lcom/miui/packageInstaller/model/CloudParams;->storeListed:Z

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/WarningInfoViewObject$ViewHolder;->getIcon()Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x3

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-eqz v0, :cond_0

    const v5, 0x7f0801b5

    goto :goto_2

    :cond_0
    iget-object v5, p0, Lcom/miui/packageInstaller/ui/listcomponets/WarningInfoViewObject;->n:Lcom/miui/packageInstaller/model/WarningCardInfo;

    if-eqz v5, :cond_1

    iget v6, v5, Lcom/miui/packageInstaller/model/WarningCardInfo;->level:I

    if-ne v6, v2, :cond_1

    move v6, v3

    goto :goto_0

    :cond_1
    move v6, v4

    :goto_0
    if-eqz v6, :cond_2

    const v5, 0x7f08019a

    goto :goto_2

    :cond_2
    if-eqz v5, :cond_3

    iget v5, v5, Lcom/miui/packageInstaller/model/WarningCardInfo;->level:I

    const/4 v6, 0x4

    if-ne v5, v6, :cond_3

    move v5, v3

    goto :goto_1

    :cond_3
    move v5, v4

    :goto_1
    if-eqz v5, :cond_4

    const v5, 0x7f080114

    goto :goto_2

    :cond_4
    const v5, 0x7f08019b

    :goto_2
    invoke-virtual {v1, v5}, Landroid/view/View;->setBackgroundResource(I)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/WarningInfoViewObject$ViewHolder;->getTvTitle()Landroid/widget/TextView;

    move-result-object v1

    const/4 v5, 0x0

    if-nez v1, :cond_5

    goto :goto_4

    :cond_5
    iget-object v6, p0, Lcom/miui/packageInstaller/ui/listcomponets/WarningInfoViewObject;->n:Lcom/miui/packageInstaller/model/WarningCardInfo;

    if-eqz v6, :cond_6

    iget-object v6, v6, Lcom/miui/packageInstaller/model/WarningCardInfo;->title:Ljava/lang/String;

    goto :goto_3

    :cond_6
    move-object v6, v5

    :goto_3
    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_4
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/WarningInfoViewObject$ViewHolder;->getTvTitle()Landroid/widget/TextView;

    move-result-object v1

    if-eqz v1, :cond_a

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v6

    if-eqz v0, :cond_7

    const v7, 0x7f0601ee

    goto :goto_6

    :cond_7
    iget-object v7, p0, Lcom/miui/packageInstaller/ui/listcomponets/WarningInfoViewObject;->n:Lcom/miui/packageInstaller/model/WarningCardInfo;

    if-eqz v7, :cond_8

    iget v7, v7, Lcom/miui/packageInstaller/model/WarningCardInfo;->level:I

    if-ne v7, v2, :cond_8

    move v7, v3

    goto :goto_5

    :cond_8
    move v7, v4

    :goto_5
    if-eqz v7, :cond_9

    const v7, 0x7f060032

    goto :goto_6

    :cond_9
    const v7, 0x7f0601ef

    :goto_6
    invoke-virtual {v6, v7}, Landroid/content/Context;->getColor(I)I

    move-result v6

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_a
    iget-object v1, p0, Lcom/miui/packageInstaller/ui/listcomponets/WarningInfoViewObject;->n:Lcom/miui/packageInstaller/model/WarningCardInfo;

    if-eqz v1, :cond_b

    iget-object v1, v1, Lcom/miui/packageInstaller/model/WarningCardInfo;->text:Ljava/lang/String;

    goto :goto_7

    :cond_b
    move-object v1, v5

    :goto_7
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_12

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/WarningInfoViewObject$ViewHolder;->getTvMsg()Landroid/widget/TextView;

    move-result-object v1

    iget-object v6, p0, Lcom/miui/packageInstaller/ui/listcomponets/WarningInfoViewObject;->n:Lcom/miui/packageInstaller/model/WarningCardInfo;

    if-eqz v6, :cond_c

    iget-object v5, v6, Lcom/miui/packageInstaller/model/WarningCardInfo;->text:Ljava/lang/String;

    :cond_c
    invoke-static {v5}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/WarningInfoViewObject$ViewHolder;->getTvMsg()Landroid/widget/TextView;

    move-result-object v1

    new-instance v5, Lcom/miui/packageInstaller/ui/listcomponets/WarningInfoViewObject$a;

    invoke-direct {v5, p0}, Lcom/miui/packageInstaller/ui/listcomponets/WarningInfoViewObject$a;-><init>(Lcom/miui/packageInstaller/ui/listcomponets/WarningInfoViewObject;)V

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/WarningInfoViewObject$ViewHolder;->getTvMsg()Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f060030

    if-eqz v0, :cond_d

    goto :goto_a

    :cond_d
    iget-object v7, p0, Lcom/miui/packageInstaller/ui/listcomponets/WarningInfoViewObject;->n:Lcom/miui/packageInstaller/model/WarningCardInfo;

    if-eqz v7, :cond_e

    iget v8, v7, Lcom/miui/packageInstaller/model/WarningCardInfo;->level:I

    if-ne v8, v2, :cond_e

    move v8, v3

    goto :goto_8

    :cond_e
    move v8, v4

    :goto_8
    if-eqz v8, :cond_f

    goto :goto_a

    :cond_f
    if-eqz v7, :cond_10

    iget v6, v7, Lcom/miui/packageInstaller/model/WarningCardInfo;->level:I

    if-ne v6, v3, :cond_10

    move v6, v3

    goto :goto_9

    :cond_10
    move v6, v4

    :goto_9
    if-eqz v6, :cond_11

    const v6, 0x7f06002f

    goto :goto_a

    :cond_11
    const v6, 0x7f0601ed

    :goto_a
    invoke-virtual {v5, v6}, Landroid/content/Context;->getColor(I)I

    move-result v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_12
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/WarningInfoViewObject$ViewHolder;->getContainer()Landroid/view/View;

    move-result-object p1

    if-eqz v0, :cond_13

    const v0, 0x7f080122

    goto :goto_c

    :cond_13
    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/WarningInfoViewObject;->n:Lcom/miui/packageInstaller/model/WarningCardInfo;

    if-eqz v0, :cond_14

    iget v0, v0, Lcom/miui/packageInstaller/model/WarningCardInfo;->level:I

    if-ne v0, v2, :cond_14

    goto :goto_b

    :cond_14
    move v3, v4

    :goto_b
    if-eqz v3, :cond_15

    const v0, 0x7f080124

    goto :goto_c

    :cond_15
    const v0, 0x7f080123

    :goto_c
    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object p1

    const-string v0, "context.resources.configuration"

    invoke-static {p1, v0}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/miui/packageInstaller/ui/listcomponets/WarningInfoViewObject;->A(Landroid/content/res/Configuration;)V

    return-void
.end method
