.class final Lcom/miui/packageInstaller/view/h$b;
.super Lf8/k;

# interfaces
.implements Ll8/p;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/packageInstaller/view/h;->U1()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf8/k;",
        "Ll8/p<",
        "Lv8/e0;",
        "Ld8/d<",
        "-",
        "La8/v;",
        ">;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation runtime Lf8/f;
    c = "com.miui.packageInstaller.view.SafeModeFeedbackFragment$fetchData$1"
    f = "SafeModeFeedbackFragment.kt"
    l = {
        0x103
    }
    m = "invokeSuspend"
.end annotation


# instance fields
.field e:Ljava/lang/Object;

.field f:Ljava/lang/Object;

.field g:I

.field final synthetic h:Lcom/miui/packageInstaller/view/h;


# direct methods
.method constructor <init>(Lcom/miui/packageInstaller/view/h;Ld8/d;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/miui/packageInstaller/view/h;",
            "Ld8/d<",
            "-",
            "Lcom/miui/packageInstaller/view/h$b;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/miui/packageInstaller/view/h$b;->h:Lcom/miui/packageInstaller/view/h;

    const/4 p1, 0x2

    invoke-direct {p0, p1, p2}, Lf8/k;-><init>(ILd8/d;)V

    return-void
.end method


# virtual methods
.method public final b(Ljava/lang/Object;Ld8/d;)Ld8/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ld8/d<",
            "*>;)",
            "Ld8/d<",
            "La8/v;",
            ">;"
        }
    .end annotation

    new-instance p1, Lcom/miui/packageInstaller/view/h$b;

    iget-object v0, p0, Lcom/miui/packageInstaller/view/h$b;->h:Lcom/miui/packageInstaller/view/h;

    invoke-direct {p1, v0, p2}, Lcom/miui/packageInstaller/view/h$b;-><init>(Lcom/miui/packageInstaller/view/h;Ld8/d;)V

    return-object p1
.end method

.method public bridge synthetic g(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lv8/e0;

    check-cast p2, Ld8/d;

    invoke-virtual {p0, p1, p2}, Lcom/miui/packageInstaller/view/h$b;->q(Lv8/e0;Ld8/d;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final n(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 14

    invoke-static {}, Le8/b;->c()Ljava/lang/Object;

    move-result-object v0

    iget v1, p0, Lcom/miui/packageInstaller/view/h$b;->g:I

    const-string v2, "resources.getStringArray\u2026rray.default_reason_list)"

    const v3, 0x7f030009

    const-string v4, "reason = "

    const-string v5, "SafeModeFeedbackFragment"

    const/4 v6, 0x1

    const/4 v7, 0x0

    if-eqz v1, :cond_1

    if-ne v1, v6, :cond_0

    iget-object v0, p0, Lcom/miui/packageInstaller/view/h$b;->f:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    iget-object v1, p0, Lcom/miui/packageInstaller/view/h$b;->e:Ljava/lang/Object;

    check-cast v1, Lm8/t;

    :try_start_0
    invoke-static {p1}, La8/n;->b(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    goto/16 :goto_7

    :catch_0
    move-exception p1

    goto/16 :goto_4

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    invoke-static {p1}, La8/n;->b(Ljava/lang/Object;)V

    new-instance v1, Lm8/t;

    invoke-direct {v1}, Lm8/t;-><init>()V

    new-array p1, v7, [Ljava/lang/String;

    iput-object p1, v1, Lm8/t;->a:Ljava/lang/Object;

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    :try_start_1
    invoke-static {}, Lv8/t0;->b()Lv8/a0;

    move-result-object v8

    new-instance v9, Lcom/miui/packageInstaller/view/h$b$a;

    const/4 v10, 0x0

    invoke-direct {v9, v10}, Lcom/miui/packageInstaller/view/h$b$a;-><init>(Ld8/d;)V

    iput-object v1, p0, Lcom/miui/packageInstaller/view/h$b;->e:Ljava/lang/Object;

    iput-object p1, p0, Lcom/miui/packageInstaller/view/h$b;->f:Ljava/lang/Object;

    iput v6, p0, Lcom/miui/packageInstaller/view/h$b;->g:I

    invoke-static {v8, v9, p0}, Lv8/f;->e(Ld8/g;Ll8/p;Ld8/d;)Ljava/lang/Object;

    move-result-object v8
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-ne v8, v0, :cond_2

    return-object v0

    :cond_2
    move-object v0, p1

    move-object p1, v8

    :goto_0
    :try_start_2
    check-cast p1, Lgc/t;

    invoke-virtual {p1}, Lgc/t;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/miui/packageInstaller/model/CloseReasonListBean;

    if-eqz p1, :cond_5

    iget-object v8, p0, Lcom/miui/packageInstaller/view/h$b;->h:Lcom/miui/packageInstaller/view/h;

    invoke-virtual {p1}, Lcom/miui/packageInstaller/model/CloseReasonListBean;->getCode()I

    move-result v9

    const/16 v10, 0xc8

    if-ne v9, v10, :cond_4

    invoke-virtual {p1}, Lcom/miui/packageInstaller/model/CloseReasonListBean;->getData()[Ljava/lang/String;

    move-result-object v9

    array-length v9, v9

    if-nez v9, :cond_3

    move v9, v6

    goto :goto_1

    :cond_3
    move v9, v7

    :goto_1
    xor-int/2addr v6, v9

    if-eqz v6, :cond_4

    invoke-virtual {p1}, Lcom/miui/packageInstaller/model/CloseReasonListBean;->getData()[Ljava/lang/String;

    move-result-object p1

    iput-object p1, v1, Lm8/t;->a:Ljava/lang/Object;

    goto :goto_2

    :cond_4
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "code = "

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/miui/packageInstaller/model/CloseReasonListBean;->getCode()I

    move-result p1

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v5, p1}, Lf6/o;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    invoke-virtual {v8}, Landroidx/fragment/app/Fragment;->N()Landroid/content/res/Resources;

    move-result-object p1

    invoke-virtual {p1, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object p1

    invoke-static {p1, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, v1, Lm8/t;->a:Ljava/lang/Object;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_5
    :goto_2
    iget-object p1, p0, Lcom/miui/packageInstaller/view/h$b;->h:Lcom/miui/packageInstaller/view/h;

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->w()Landroid/content/Context;

    move-result-object p1

    if-eqz p1, :cond_a

    iget-object v2, p0, Lcom/miui/packageInstaller/view/h$b;->h:Lcom/miui/packageInstaller/view/h;

    iget-object v1, v1, Lm8/t;->a:Ljava/lang/Object;

    check-cast v1, [Ljava/lang/String;

    array-length v3, v1

    move v6, v7

    :goto_3
    if-ge v6, v3, :cond_7

    aget-object v8, v1, v6

    if-eqz v8, :cond_6

    new-instance v9, Lcom/miui/packageInstaller/model/CloseReasonItemData;

    invoke-direct {v9}, Lcom/miui/packageInstaller/model/CloseReasonItemData;-><init>()V

    invoke-virtual {v9, v8}, Lcom/miui/packageInstaller/model/CloseReasonItemData;->setReason(Ljava/lang/String;)V

    invoke-virtual {v9, v7}, Lcom/miui/packageInstaller/model/CloseReasonItemData;->setChecked(Z)V

    new-instance v10, Lcom/miui/packageInstaller/ui/listcomponets/CloseReasonViewObject;

    invoke-virtual {v2}, Lcom/miui/packageInstaller/view/h;->V1()Ll6/d;

    move-result-object v11

    invoke-direct {v10, p1, v9, v11}, Lcom/miui/packageInstaller/ui/listcomponets/CloseReasonViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/CloseReasonItemData;Ll6/c;)V

    invoke-interface {v0, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_6
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Lf6/o;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    :cond_7
    invoke-virtual {v2}, Lcom/miui/packageInstaller/view/h;->Y1()Lj6/b;

    move-result-object p1

    invoke-virtual {p1, v0}, Lj6/b;->M(Ljava/util/List;)I

    invoke-static {v2, v7}, Lcom/miui/packageInstaller/view/h;->S1(Lcom/miui/packageInstaller/view/h;Z)V

    goto/16 :goto_6

    :catchall_1
    move-exception v0

    move-object v13, v0

    move-object v0, p1

    move-object p1, v13

    goto :goto_7

    :catch_1
    move-exception v0

    move-object v13, v0

    move-object v0, p1

    move-object p1, v13

    :goto_4
    :try_start_3
    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_8

    const-string v6, ""

    :cond_8
    invoke-static {v5, v6, p1}, Lf6/o;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    iget-object v6, p0, Lcom/miui/packageInstaller/view/h$b;->h:Lcom/miui/packageInstaller/view/h;

    invoke-virtual {v6}, Landroidx/fragment/app/Fragment;->N()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v3, v1, Lm8/t;->a:Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    iget-object p1, p0, Lcom/miui/packageInstaller/view/h$b;->h:Lcom/miui/packageInstaller/view/h;

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->w()Landroid/content/Context;

    move-result-object p1

    if-eqz p1, :cond_a

    iget-object v2, p0, Lcom/miui/packageInstaller/view/h$b;->h:Lcom/miui/packageInstaller/view/h;

    iget-object v1, v1, Lm8/t;->a:Ljava/lang/Object;

    check-cast v1, [Ljava/lang/String;

    array-length v3, v1

    move v6, v7

    :goto_5
    if-ge v6, v3, :cond_7

    aget-object v8, v1, v6

    if-eqz v8, :cond_9

    new-instance v9, Lcom/miui/packageInstaller/model/CloseReasonItemData;

    invoke-direct {v9}, Lcom/miui/packageInstaller/model/CloseReasonItemData;-><init>()V

    invoke-virtual {v9, v8}, Lcom/miui/packageInstaller/model/CloseReasonItemData;->setReason(Ljava/lang/String;)V

    invoke-virtual {v9, v7}, Lcom/miui/packageInstaller/model/CloseReasonItemData;->setChecked(Z)V

    new-instance v10, Lcom/miui/packageInstaller/ui/listcomponets/CloseReasonViewObject;

    invoke-virtual {v2}, Lcom/miui/packageInstaller/view/h;->V1()Ll6/d;

    move-result-object v11

    invoke-direct {v10, p1, v9, v11}, Lcom/miui/packageInstaller/ui/listcomponets/CloseReasonViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/CloseReasonItemData;Ll6/c;)V

    invoke-interface {v0, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_9
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Lf6/o;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    add-int/lit8 v6, v6, 0x1

    goto :goto_5

    :cond_a
    :goto_6
    sget-object p1, La8/v;->a:La8/v;

    return-object p1

    :goto_7
    iget-object v2, p0, Lcom/miui/packageInstaller/view/h$b;->h:Lcom/miui/packageInstaller/view/h;

    invoke-virtual {v2}, Landroidx/fragment/app/Fragment;->w()Landroid/content/Context;

    move-result-object v2

    if-eqz v2, :cond_d

    iget-object v3, p0, Lcom/miui/packageInstaller/view/h$b;->h:Lcom/miui/packageInstaller/view/h;

    iget-object v1, v1, Lm8/t;->a:Ljava/lang/Object;

    check-cast v1, [Ljava/lang/String;

    array-length v6, v1

    move v8, v7

    :goto_8
    if-ge v8, v6, :cond_c

    aget-object v9, v1, v8

    if-eqz v9, :cond_b

    new-instance v10, Lcom/miui/packageInstaller/model/CloseReasonItemData;

    invoke-direct {v10}, Lcom/miui/packageInstaller/model/CloseReasonItemData;-><init>()V

    invoke-virtual {v10, v9}, Lcom/miui/packageInstaller/model/CloseReasonItemData;->setReason(Ljava/lang/String;)V

    invoke-virtual {v10, v7}, Lcom/miui/packageInstaller/model/CloseReasonItemData;->setChecked(Z)V

    new-instance v11, Lcom/miui/packageInstaller/ui/listcomponets/CloseReasonViewObject;

    invoke-virtual {v3}, Lcom/miui/packageInstaller/view/h;->V1()Ll6/d;

    move-result-object v12

    invoke-direct {v11, v2, v10, v12}, Lcom/miui/packageInstaller/ui/listcomponets/CloseReasonViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/CloseReasonItemData;Ll6/c;)V

    invoke-interface {v0, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_b
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v5, v9}, Lf6/o;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    add-int/lit8 v8, v8, 0x1

    goto :goto_8

    :cond_c
    invoke-virtual {v3}, Lcom/miui/packageInstaller/view/h;->Y1()Lj6/b;

    move-result-object v1

    invoke-virtual {v1, v0}, Lj6/b;->M(Ljava/util/List;)I

    invoke-static {v3, v7}, Lcom/miui/packageInstaller/view/h;->S1(Lcom/miui/packageInstaller/view/h;Z)V

    :cond_d
    throw p1
.end method

.method public final q(Lv8/e0;Ld8/d;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lv8/e0;",
            "Ld8/d<",
            "-",
            "La8/v;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/miui/packageInstaller/view/h$b;->b(Ljava/lang/Object;Ld8/d;)Ld8/d;

    move-result-object p1

    check-cast p1, Lcom/miui/packageInstaller/view/h$b;

    sget-object p2, La8/v;->a:La8/v;

    invoke-virtual {p1, p2}, Lcom/miui/packageInstaller/view/h$b;->n(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method
