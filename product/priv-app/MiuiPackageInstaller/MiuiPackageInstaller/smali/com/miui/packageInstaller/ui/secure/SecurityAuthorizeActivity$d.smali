.class final Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity$d;
.super Lm8/j;

# interfaces
.implements Ll8/p;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity;->J0(Lx5/a;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lm8/j;",
        "Ll8/p<",
        "Lx5/a;",
        "Ljava/lang/Integer;",
        "La8/v;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic b:Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity;


# direct methods
.method constructor <init>(Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity$d;->b:Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lm8/j;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final b(Lx5/a;I)V
    .locals 6

    const-string v0, "authorizeType"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, -0x1

    const/4 v1, 0x0

    if-eqz p2, :cond_0

    iget-object p1, p0, Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity$d;->b:Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity;

    invoke-virtual {p1, v0}, Landroid/app/Activity;->setResult(I)V

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity$d;->b:Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity;

    invoke-virtual {v2, v1}, Landroid/app/Activity;->setResult(I)V

    new-instance v2, Lp5/b;

    iget-object v3, p0, Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity$d;->b:Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity;

    invoke-static {v3}, Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity;->D0(Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity;)Lo5/b;

    move-result-object v3

    const-string v4, "risk_verifying_popup"

    const-string v5, "button"

    invoke-direct {v2, v4, v5, v3}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    iget-object v3, p0, Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity$d;->b:Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity;

    invoke-static {v3, p1}, Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity;->E0(Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity;Lx5/a;)Ljava/lang/String;

    move-result-object p1

    const-string v3, "verify_method"

    invoke-virtual {v2, v3, p1}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p1

    invoke-virtual {p1}, Lp5/f;->c()Z

    :goto_0
    iget-object p1, p0, Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity$d;->b:Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity;

    if-eqz p2, :cond_1

    goto :goto_1

    :cond_1
    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/app/Activity;->setResult(I)V

    iget-object p1, p0, Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity$d;->b:Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity;

    invoke-virtual {p1}, Lmiuix/appcompat/app/j;->finish()V

    return-void
.end method

.method public bridge synthetic g(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lx5/a;

    check-cast p2, Ljava/lang/Number;

    invoke-virtual {p2}, Ljava/lang/Number;->intValue()I

    move-result p2

    invoke-virtual {p0, p1, p2}, Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity$d;->b(Lx5/a;I)V

    sget-object p1, La8/v;->a:La8/v;

    return-object p1
.end method
