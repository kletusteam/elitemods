.class public final Lcom/miui/packageInstaller/ui/listcomponets/SecondAdTitleViewObject;
.super Lm6/a;

# interfaces
.implements Lcom/miui/packageInstaller/ui/listcomponets/v;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/packageInstaller/ui/listcomponets/SecondAdTitleViewObject$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lm6/a<",
        "Lcom/miui/packageInstaller/ui/listcomponets/SecondAdTitleViewObject$ViewHolder;",
        ">;",
        "Lcom/miui/packageInstaller/ui/listcomponets/v;"
    }
.end annotation


# instance fields
.field private final l:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ll6/c;Lm6/b;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mName"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2, p3, p4}, Lm6/a;-><init>(Landroid/content/Context;Ljava/lang/Object;Ll6/c;Lm6/b;)V

    iput-object p2, p0, Lcom/miui/packageInstaller/ui/listcomponets/SecondAdTitleViewObject;->l:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public k()I
    .locals 1

    const v0, 0x7f0d019a

    return v0
.end method

.method public bridge synthetic o(Landroidx/recyclerview/widget/RecyclerView$d0;)V
    .locals 0

    check-cast p1, Lcom/miui/packageInstaller/ui/listcomponets/SecondAdTitleViewObject$ViewHolder;

    invoke-virtual {p0, p1}, Lcom/miui/packageInstaller/ui/listcomponets/SecondAdTitleViewObject;->z(Lcom/miui/packageInstaller/ui/listcomponets/SecondAdTitleViewObject$ViewHolder;)V

    return-void
.end method

.method public z(Lcom/miui/packageInstaller/ui/listcomponets/SecondAdTitleViewObject$ViewHolder;)V
    .locals 5

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/SecondAdTitleViewObject$ViewHolder;->getAdDes()Landroid/widget/TextView;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-nez p1, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f110307

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/miui/packageInstaller/ui/listcomponets/SecondAdTitleViewObject;->l:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    return-void
.end method
