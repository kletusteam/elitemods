.class public final Lcom/miui/packageInstaller/view/h;
.super Landroidx/fragment/app/Fragment;

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/packageInstaller/view/h$a;
    }
.end annotation


# static fields
.field public static final A0:Lcom/miui/packageInstaller/view/h$a;


# instance fields
.field private e0:Landroid/view/ViewGroup;

.field private f0:Landroid/widget/FrameLayout;

.field private g0:Landroid/widget/TextView;

.field private h0:Landroid/widget/LinearLayout;

.field private i0:Landroid/widget/TextView;

.field private j0:Landroid/widget/TextView;

.field private k0:Lmiuix/core/widget/NestedScrollView;

.field private l0:Lmiuix/springback/view/SpringBackLayout;

.field private m0:Landroid/widget/TextView;

.field private n0:Landroidx/recyclerview/widget/RecyclerView;

.field private o0:Landroid/widget/CheckBox;

.field private p0:Landroid/widget/EditText;

.field private q0:Landroid/widget/FrameLayout;

.field private r0:Landroid/widget/TextView;

.field private s0:Landroid/widget/Button;

.field private t0:Lcom/airbnb/lottie/LottieAnimationView;

.field private u0:Landroid/widget/TextView;

.field private v0:Landroid/widget/ImageView;

.field private w0:Landroid/widget/LinearLayout;

.field private x0:Lf6/l;

.field public y0:Lj6/b;

.field private z0:Ll6/d;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/miui/packageInstaller/view/h$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/miui/packageInstaller/view/h$a;-><init>(Lm8/g;)V

    sput-object v0, Lcom/miui/packageInstaller/view/h;->A0:Lcom/miui/packageInstaller/view/h$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroidx/fragment/app/Fragment;-><init>()V

    new-instance v0, Ll6/d;

    invoke-direct {v0}, Ll6/d;-><init>()V

    iput-object v0, p0, Lcom/miui/packageInstaller/view/h;->z0:Ll6/d;

    return-void
.end method

.method public static synthetic H1(Lcom/miui/packageInstaller/view/h;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/miui/packageInstaller/view/h;->c2(Lcom/miui/packageInstaller/view/h;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic I1(Lcom/miui/packageInstaller/view/h;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/miui/packageInstaller/view/h;->b2(Lcom/miui/packageInstaller/view/h;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic J1(Lcom/miui/packageInstaller/view/h;Landroid/view/View;IIII)V
    .locals 0

    invoke-static/range {p0 .. p5}, Lcom/miui/packageInstaller/view/h;->d2(Lcom/miui/packageInstaller/view/h;Landroid/view/View;IIII)V

    return-void
.end method

.method public static synthetic K1(Lcom/miui/packageInstaller/view/h;)V
    .locals 0

    invoke-static {p0}, Lcom/miui/packageInstaller/view/h;->h2(Lcom/miui/packageInstaller/view/h;)V

    return-void
.end method

.method public static synthetic L1(Lcom/miui/packageInstaller/view/h;Landroid/content/Context;ILjava/lang/Object;Lm6/a;)V
    .locals 0

    invoke-static {p0, p1, p2, p3, p4}, Lcom/miui/packageInstaller/view/h;->a2(Lcom/miui/packageInstaller/view/h;Landroid/content/Context;ILjava/lang/Object;Lm6/a;)V

    return-void
.end method

.method public static final synthetic M1(Lcom/miui/packageInstaller/view/h;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/packageInstaller/view/h;->T1(Z)V

    return-void
.end method

.method public static final synthetic N1(Lcom/miui/packageInstaller/view/h;)Landroid/widget/ImageView;
    .locals 0

    iget-object p0, p0, Lcom/miui/packageInstaller/view/h;->v0:Landroid/widget/ImageView;

    return-object p0
.end method

.method public static final synthetic O1(Lcom/miui/packageInstaller/view/h;)Landroid/widget/LinearLayout;
    .locals 0

    iget-object p0, p0, Lcom/miui/packageInstaller/view/h;->w0:Landroid/widget/LinearLayout;

    return-object p0
.end method

.method public static final synthetic P1(Lcom/miui/packageInstaller/view/h;)Landroid/widget/CheckBox;
    .locals 0

    iget-object p0, p0, Lcom/miui/packageInstaller/view/h;->o0:Landroid/widget/CheckBox;

    return-object p0
.end method

.method public static final synthetic Q1(Lcom/miui/packageInstaller/view/h;)Landroid/view/ViewGroup;
    .locals 0

    iget-object p0, p0, Lcom/miui/packageInstaller/view/h;->e0:Landroid/view/ViewGroup;

    return-object p0
.end method

.method public static final synthetic R1(Lcom/miui/packageInstaller/view/h;)Lmiuix/core/widget/NestedScrollView;
    .locals 0

    iget-object p0, p0, Lcom/miui/packageInstaller/view/h;->k0:Lmiuix/core/widget/NestedScrollView;

    return-object p0
.end method

.method public static final synthetic S1(Lcom/miui/packageInstaller/view/h;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/packageInstaller/view/h;->i2(Z)V

    return-void
.end method

.method private final T1(Z)V
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/view/h;->s0:Landroid/widget/Button;

    if-nez v0, :cond_0

    const-string v0, "btnSubmit"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v0, 0x0

    :cond_0
    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    return-void
.end method

.method private final U1()V
    .locals 7

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/miui/packageInstaller/view/h;->i2(Z)V

    invoke-static {p0}, Landroidx/lifecycle/k;->a(Landroidx/lifecycle/j;)Landroidx/lifecycle/e;

    move-result-object v1

    invoke-static {}, Lv8/t0;->c()Lv8/v1;

    move-result-object v2

    new-instance v4, Lcom/miui/packageInstaller/view/h$b;

    const/4 v0, 0x0

    invoke-direct {v4, p0, v0}, Lcom/miui/packageInstaller/view/h$b;-><init>(Lcom/miui/packageInstaller/view/h;Ld8/d;)V

    const/4 v3, 0x0

    const/4 v5, 0x2

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lv8/f;->d(Lv8/e0;Ld8/g;Lv8/g0;Ll8/p;ILjava/lang/Object;)Lv8/k1;

    return-void
.end method

.method private final W1()Z
    .locals 8

    iget-object v0, p0, Lcom/miui/packageInstaller/view/h;->o0:Landroid/widget/CheckBox;

    const-string v1, "mCbReasonOther"

    const/4 v2, 0x0

    if-nez v0, :cond_0

    invoke-static {v1}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v2

    :cond_0
    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    const/4 v3, 0x1

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/miui/packageInstaller/view/h;->p0:Landroid/widget/EditText;

    if-nez v0, :cond_1

    const-string v0, "mEtReason"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v2

    :cond_1
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    const-string v4, "mEtReason.text"

    invoke-static {v0, v4}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lu8/g;->l(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/2addr v0, v3

    if-eqz v0, :cond_2

    return v3

    :cond_2
    invoke-virtual {p0}, Lcom/miui/packageInstaller/view/h;->Y1()Lj6/b;

    move-result-object v0

    invoke-virtual {v0}, Lj6/b;->P()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v4, 0x0

    move v5, v4

    :cond_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_7

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    instance-of v7, v6, Lcom/miui/packageInstaller/model/CloseReasonItemData;

    if-eqz v7, :cond_4

    check-cast v6, Lcom/miui/packageInstaller/model/CloseReasonItemData;

    invoke-virtual {v6}, Lcom/miui/packageInstaller/model/CloseReasonItemData;->isChecked()Z

    move-result v6

    if-eqz v6, :cond_4

    add-int/lit8 v5, v5, 0x1

    :cond_4
    if-gt v5, v3, :cond_6

    iget-object v6, p0, Lcom/miui/packageInstaller/view/h;->o0:Landroid/widget/CheckBox;

    if-nez v6, :cond_5

    invoke-static {v1}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v6, v2

    :cond_5
    invoke-virtual {v6}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v6

    if-eqz v6, :cond_3

    if-lez v5, :cond_3

    :cond_6
    return v3

    :cond_7
    return v4
.end method

.method private final X1()Ljava/lang/String;
    .locals 5

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/miui/packageInstaller/view/h;->Y1()Lj6/b;

    move-result-object v1

    invoke-virtual {v1}, Lj6/b;->P()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const-string v3, "|"

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    instance-of v4, v2, Lcom/miui/packageInstaller/model/CloseReasonItemData;

    if-eqz v4, :cond_0

    check-cast v2, Lcom/miui/packageInstaller/model/CloseReasonItemData;

    invoke-virtual {v2}, Lcom/miui/packageInstaller/model/CloseReasonItemData;->isChecked()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v2}, Lcom/miui/packageInstaller/model/CloseReasonItemData;->getReason()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/miui/packageInstaller/view/h;->o0:Landroid/widget/CheckBox;

    const/4 v2, 0x0

    const-string v4, "mCbReasonOther"

    if-nez v1, :cond_2

    invoke-static {v4}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v1, v2

    :cond_2
    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/miui/packageInstaller/view/h;->o0:Landroid/widget/CheckBox;

    if-nez v1, :cond_3

    invoke-static {v4}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    move-object v2, v1

    :goto_1
    invoke-virtual {v2}, Landroid/widget/CheckBox;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "stringBuilder.toString()"

    invoke-static {v1, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-lez v1, :cond_5

    move v1, v2

    goto :goto_2

    :cond_5
    move v1, v3

    :goto_2
    if-eqz v1, :cond_6

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    sub-int/2addr v1, v2

    invoke-virtual {v0, v3, v1}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_6
    const-string v0, ""

    return-object v0
.end method

.method private final Z1()V
    .locals 5

    iget-object v0, p0, Lcom/miui/packageInstaller/view/h;->z0:Ll6/d;

    new-instance v1, Lcom/miui/packageInstaller/view/g;

    invoke-direct {v1, p0}, Lcom/miui/packageInstaller/view/g;-><init>(Lcom/miui/packageInstaller/view/h;)V

    const v2, 0x7f0a00bc

    invoke-virtual {v0, v2, v1}, Ll6/d;->b(ILl6/e;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/view/h;->f0:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const-string v0, "flBackIcon"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_0
    new-instance v2, Lcom/miui/packageInstaller/view/d;

    invoke-direct {v2, p0}, Lcom/miui/packageInstaller/view/d;-><init>(Lcom/miui/packageInstaller/view/h;)V

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/view/h;->o0:Landroid/widget/CheckBox;

    if-nez v0, :cond_1

    const-string v0, "mCbReasonOther"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_1
    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/miui/packageInstaller/view/h;->T1(Z)V

    new-instance v0, Lp5/g;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->p()Landroidx/fragment/app/e;

    move-result-object v2

    const-string v3, "null cannot be cast to non-null type com.miui.packageInstaller.analytics.IPage"

    invoke-static {v2, v3}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lo5/a;

    const-string v3, "safe_mode_shut_research_submit_btn"

    const-string v4, "button"

    invoke-direct {v0, v3, v4, v2}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    iget-object v0, p0, Lcom/miui/packageInstaller/view/h;->s0:Landroid/widget/Button;

    if-nez v0, :cond_2

    const-string v0, "btnSubmit"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_2
    new-instance v2, Lcom/miui/packageInstaller/view/c;

    invoke-direct {v2, p0}, Lcom/miui/packageInstaller/view/c;-><init>(Lcom/miui/packageInstaller/view/h;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lm8/q;

    invoke-direct {v0}, Lm8/q;-><init>()V

    const/4 v2, 0x1

    iput-boolean v2, v0, Lm8/q;->a:Z

    iget-object v2, p0, Lcom/miui/packageInstaller/view/h;->p0:Landroid/widget/EditText;

    if-nez v2, :cond_3

    const-string v2, "mEtReason"

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v2, v1

    :cond_3
    new-instance v3, Lcom/miui/packageInstaller/view/h$c;

    invoke-direct {v3, p0, v0}, Lcom/miui/packageInstaller/view/h$c;-><init>(Lcom/miui/packageInstaller/view/h;Lm8/q;)V

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->p()Landroidx/fragment/app/e;

    move-result-object v0

    if-eqz v0, :cond_4

    new-instance v2, Lf6/l;

    invoke-direct {v2, v0}, Lf6/l;-><init>(Landroid/app/Activity;)V

    iput-object v2, p0, Lcom/miui/packageInstaller/view/h;->x0:Lf6/l;

    new-instance v0, Lcom/miui/packageInstaller/view/h$d;

    invoke-direct {v0, p0}, Lcom/miui/packageInstaller/view/h$d;-><init>(Lcom/miui/packageInstaller/view/h;)V

    invoke-virtual {v2, v0}, Lf6/l;->d(Lf6/l$b;)V

    :cond_4
    iget-object v0, p0, Lcom/miui/packageInstaller/view/h;->k0:Lmiuix/core/widget/NestedScrollView;

    if-nez v0, :cond_5

    const-string v0, "scrollView"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    move-object v1, v0

    :goto_0
    new-instance v0, Lcom/miui/packageInstaller/view/e;

    invoke-direct {v0, p0}, Lcom/miui/packageInstaller/view/e;-><init>(Lcom/miui/packageInstaller/view/h;)V

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setOnScrollChangeListener(Landroid/view/View$OnScrollChangeListener;)V

    return-void
.end method

.method private static final a2(Lcom/miui/packageInstaller/view/h;Landroid/content/Context;ILjava/lang/Object;Lm6/a;)V
    .locals 0

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/miui/packageInstaller/view/h;->W1()Z

    move-result p1

    invoke-direct {p0, p1}, Lcom/miui/packageInstaller/view/h;->T1(Z)V

    return-void
.end method

.method private static final b2(Lcom/miui/packageInstaller/view/h;Landroid/view/View;)V
    .locals 0

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->p()Landroidx/fragment/app/e;

    move-result-object p0

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroidx/activity/ComponentActivity;->onBackPressed()V

    :cond_0
    return-void
.end method

.method private static final c2(Lcom/miui/packageInstaller/view/h;Landroid/view/View;)V
    .locals 0

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/miui/packageInstaller/view/h;->l2()V

    return-void
.end method

.method private static final d2(Lcom/miui/packageInstaller/view/h;Landroid/view/View;IIII)V
    .locals 2

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->N()Landroid/content/res/Resources;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object p1

    iget p1, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 p2, 0x2

    if-ne p1, p2, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->N()Landroid/content/res/Resources;

    move-result-object p1

    const p2, 0x7f0700b9

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result p1

    int-to-float p1, p1

    int-to-float p2, p3

    cmpl-float p3, p2, p1

    const-string p4, "tvCollapsingSubtitle"

    const-string p5, "tvCollapsingTitle"

    const-string v0, "tvTitle"

    const/4 v1, 0x0

    if-ltz p3, :cond_3

    iget-object p1, p0, Lcom/miui/packageInstaller/view/h;->g0:Landroid/widget/TextView;

    if-nez p1, :cond_1

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object p1, v1

    :cond_1
    const/high16 p2, 0x3f800000    # 1.0f

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setAlpha(F)V

    iget-object p1, p0, Lcom/miui/packageInstaller/view/h;->i0:Landroid/widget/TextView;

    if-nez p1, :cond_2

    invoke-static {p5}, Lm8/i;->s(Ljava/lang/String;)V

    move-object p1, v1

    :cond_2
    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setAlpha(F)V

    iget-object p0, p0, Lcom/miui/packageInstaller/view/h;->j0:Landroid/widget/TextView;

    if-nez p0, :cond_6

    goto :goto_0

    :cond_3
    sub-float p2, p1, p2

    div-float/2addr p2, p1

    iget-object p1, p0, Lcom/miui/packageInstaller/view/h;->g0:Landroid/widget/TextView;

    if-nez p1, :cond_4

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object p1, v1

    :cond_4
    const/4 p3, 0x1

    int-to-float p3, p3

    sub-float/2addr p3, p2

    invoke-virtual {p1, p3}, Landroid/widget/TextView;->setAlpha(F)V

    iget-object p1, p0, Lcom/miui/packageInstaller/view/h;->i0:Landroid/widget/TextView;

    if-nez p1, :cond_5

    invoke-static {p5}, Lm8/i;->s(Ljava/lang/String;)V

    move-object p1, v1

    :cond_5
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setAlpha(F)V

    iget-object p0, p0, Lcom/miui/packageInstaller/view/h;->j0:Landroid/widget/TextView;

    if-nez p0, :cond_6

    :goto_0
    invoke-static {p4}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_1

    :cond_6
    move-object v1, p0

    :goto_1
    invoke-virtual {v1, p2}, Landroid/widget/TextView;->setAlpha(F)V

    return-void
.end method

.method private final e2()V
    .locals 8

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/miui/packageInstaller/view/h;->v0:Landroid/widget/ImageView;

    const-string v1, "null cannot be cast to non-null type android.widget.LinearLayout.LayoutParams"

    const/4 v2, 0x1

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    invoke-static {v3, v1}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->N()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget v4, v4, Landroid/content/res/Configuration;->orientation:I

    if-ne v4, v2, :cond_0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->N()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f070104

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->N()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0700f9

    :goto_0
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    iput v4, v3, Landroid/widget/LinearLayout$LayoutParams;->width:I

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->N()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget v4, v4, Landroid/content/res/Configuration;->orientation:I

    if-ne v4, v2, :cond_1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->N()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0700e9

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->N()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0700dd

    :goto_1
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    iput v4, v3, Landroid/widget/LinearLayout$LayoutParams;->height:I

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->N()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget v4, v4, Landroid/content/res/Configuration;->orientation:I

    if-ne v4, v2, :cond_2

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->N()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0700f6

    goto :goto_2

    :cond_2
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->N()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0700a9

    :goto_2
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    iput v4, v3, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_3
    iget-object v0, p0, Lcom/miui/packageInstaller/view/h;->e0:Landroid/view/ViewGroup;

    const/4 v3, 0x0

    if-nez v0, :cond_4

    const-string v0, "rootLayout"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v3

    :cond_4
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v4, Lcom/miui/packageInstaller/view/h$e;

    invoke-direct {v4, p0}, Lcom/miui/packageInstaller/view/h$e;-><init>(Lcom/miui/packageInstaller/view/h;)V

    invoke-virtual {v0, v4}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/view/h;->w0:Landroid/widget/LinearLayout;

    if-nez v0, :cond_5

    const-string v0, "llReason"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v3

    :cond_5
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    invoke-static {v4, v1}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v4, Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->N()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    if-ne v1, v2, :cond_6

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->N()Landroid/content/res/Resources;

    move-result-object v1

    const v5, 0x7f070113

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    goto :goto_3

    :cond_6
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->N()Landroid/content/res/Resources;

    move-result-object v1

    const v5, 0x7f0700b6

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    neg-int v1, v1

    :goto_3
    iput v1, v4, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->N()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const v5, 0x7f0700b4

    const v6, 0x7f070109

    if-ne v1, v2, :cond_7

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->N()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    goto :goto_4

    :cond_7
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->N()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    :goto_4
    invoke-virtual {v4, v1}, Landroid/widget/LinearLayout$LayoutParams;->setMarginStart(I)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->N()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    if-ne v1, v2, :cond_8

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->N()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    goto :goto_5

    :cond_8
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->N()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    :goto_5
    invoke-virtual {v4, v1}, Landroid/widget/LinearLayout$LayoutParams;->setMarginEnd(I)V

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->N()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    const-string v4, "tvTitle"

    const/high16 v5, 0x3f800000    # 1.0f

    const-string v6, "llCollapsingTitleView"

    const/4 v7, 0x0

    if-ne v0, v1, :cond_b

    iget-object v0, p0, Lcom/miui/packageInstaller/view/h;->h0:Landroid/widget/LinearLayout;

    if-nez v0, :cond_9

    invoke-static {v6}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v3

    :cond_9
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/miui/packageInstaller/view/h;->g0:Landroid/widget/TextView;

    if-nez v0, :cond_a

    invoke-static {v4}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v3

    :cond_a
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setAlpha(F)V

    goto :goto_6

    :cond_b
    iget-object v0, p0, Lcom/miui/packageInstaller/view/h;->h0:Landroid/widget/LinearLayout;

    if-nez v0, :cond_c

    invoke-static {v6}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v3

    :cond_c
    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :goto_6
    iget-object v0, p0, Lcom/miui/packageInstaller/view/h;->g0:Landroid/widget/TextView;

    if-nez v0, :cond_d

    invoke-static {v4}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v3

    :cond_d
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->N()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    if-ne v1, v2, :cond_e

    const/4 v5, 0x0

    :cond_e
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setAlpha(F)V

    iget-object v0, p0, Lcom/miui/packageInstaller/view/h;->k0:Lmiuix/core/widget/NestedScrollView;

    if-nez v0, :cond_f

    const-string v0, "scrollView"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_7

    :cond_f
    move-object v3, v0

    :goto_7
    invoke-virtual {v3, v7, v2}, Landroid/widget/FrameLayout;->scrollBy(II)V

    :cond_10
    return-void
.end method

.method private final f2()V
    .locals 5

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->w()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_1

    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1}, Landroid/text/SpannableStringBuilder;-><init>()V

    const v2, 0x7f110315

    invoke-virtual {p0, v2}, Landroidx/fragment/app/Fragment;->T(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "getString(R.string.security_mode_feedback_reason)"

    invoke-static {v2, v3}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const v3, 0x7f060023

    invoke-virtual {v0, v3}, Landroid/content/Context;->getColor(I)I

    move-result v3

    invoke-static {v2, v3}, Lcom/android/packageinstaller/utils/v;->b(Ljava/lang/String;I)Landroid/text/SpannableString;

    move-result-object v2

    const v3, 0x7f110314

    invoke-virtual {p0, v3}, Landroidx/fragment/app/Fragment;->T(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "getString(R.string.secur\u2026feedback_multiple_choice)"

    invoke-static {v3, v4}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const v4, 0x7f06002d

    invoke-virtual {v0, v4}, Landroid/content/Context;->getColor(I)I

    move-result v0

    invoke-static {v3, v0}, Lcom/android/packageinstaller/utils/v;->b(Ljava/lang/String;I)Landroid/text/SpannableString;

    move-result-object v0

    invoke-virtual {v1, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {v1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    iget-object v0, p0, Lcom/miui/packageInstaller/view/h;->m0:Landroid/widget/TextView;

    if-nez v0, :cond_0

    const-string v0, "mTvQuestion"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v0, 0x0

    :cond_0
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    return-void
.end method

.method private final g2(Landroid/view/View;)V
    .locals 6

    const v0, 0x7f0a02d2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "view.findViewById(R.id.root_layout)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/miui/packageInstaller/view/h;->e0:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const-string v0, "rootLayout"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->p()Landroidx/fragment/app/e;

    move-result-object v2

    invoke-static {v2}, Lcom/android/packageinstaller/utils/u;->b(Landroid/app/Activity;)I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v0, v3, v2, v3, v3}, Landroid/view/ViewGroup;->setPadding(IIII)V

    const v0, 0x7f0a0089

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v2, "view.findViewById(R.id.back_icon)"

    invoke-static {v0, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/miui/packageInstaller/view/h;->f0:Landroid/widget/FrameLayout;

    const v0, 0x7f0a03d3

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v2, "view.findViewById(R.id.tv_title)"

    invoke-static {v0, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/view/h;->g0:Landroid/widget/TextView;

    if-nez v0, :cond_1

    const-string v0, "tvTitle"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_1
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setAlpha(F)V

    const v0, 0x7f0a01f8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v2, "view.findViewById(R.id.ll_collapsing_title_view)"

    invoke-static {v0, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/miui/packageInstaller/view/h;->h0:Landroid/widget/LinearLayout;

    const v0, 0x7f0a03ae

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v2, "view.findViewById(R.id.tv_collapsing_title)"

    invoke-static {v0, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/view/h;->i0:Landroid/widget/TextView;

    const v0, 0x7f0a03ad

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v2, "view.findViewById(R.id.tv_collapsing_subtitle)"

    invoke-static {v0, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/view/h;->j0:Landroid/widget/TextView;

    const v0, 0x7f0a0163

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v2, "view.findViewById(R.id.feedback_layout)"

    invoke-static {v0, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lmiuix/core/widget/NestedScrollView;

    iput-object v0, p0, Lcom/miui/packageInstaller/view/h;->k0:Lmiuix/core/widget/NestedScrollView;

    const v0, 0x7f0a032e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v2, "view.findViewById(R.id.spring_back_layout)"

    invoke-static {v0, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lmiuix/springback/view/SpringBackLayout;

    iput-object v0, p0, Lcom/miui/packageInstaller/view/h;->l0:Lmiuix/springback/view/SpringBackLayout;

    const v0, 0x7f0a03bf

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v2, "view.findViewById(R.id.tv_question)"

    invoke-static {v0, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/view/h;->m0:Landroid/widget/TextView;

    const v0, 0x7f0a02d6

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v2, "view.findViewById(R.id.rv_reason_list)"

    invoke-static {v0, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    iput-object v0, p0, Lcom/miui/packageInstaller/view/h;->n0:Landroidx/recyclerview/widget/RecyclerView;

    const-string v2, "mRvReasonList"

    if-nez v0, :cond_2

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_2
    new-instance v4, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->w()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v4}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$o;)V

    new-instance v0, Lj6/b;

    iget-object v4, p0, Lcom/miui/packageInstaller/view/h;->n0:Landroidx/recyclerview/widget/RecyclerView;

    if-nez v4, :cond_3

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v4, v1

    :cond_3
    invoke-direct {v0, v4}, Lj6/b;-><init>(Landroidx/recyclerview/widget/RecyclerView;)V

    invoke-virtual {p0, v0}, Lcom/miui/packageInstaller/view/h;->k2(Lj6/b;)V

    const v0, 0x7f0a00bd

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v2, "view.findViewById(R.id.cb_reason_other)"

    invoke-static {v0, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/miui/packageInstaller/view/h;->o0:Landroid/widget/CheckBox;

    const v0, 0x7f0a02bf

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v2, "view.findViewById(R.id.reason_edit_text)"

    invoke-static {v0, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/miui/packageInstaller/view/h;->p0:Landroid/widget/EditText;

    const v0, 0x7f0a03b7

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v2, "view.findViewById(R.id.tv_input_count)"

    invoke-static {v0, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/view/h;->r0:Landroid/widget/TextView;

    const v0, 0x7f0a0177

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v2, "view.findViewById(R.id.fl_input)"

    invoke-static {v0, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/miui/packageInstaller/view/h;->q0:Landroid/widget/FrameLayout;

    const v0, 0x7f0a020d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/airbnb/lottie/LottieAnimationView;

    iput-object v0, p0, Lcom/miui/packageInstaller/view/h;->t0:Lcom/airbnb/lottie/LottieAnimationView;

    iget-object v0, p0, Lcom/miui/packageInstaller/view/h;->q0:Landroid/widget/FrameLayout;

    if-nez v0, :cond_4

    const-string v0, "flInputView"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    move-object v1, v0

    :goto_0
    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setVisibility(I)V

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->v()Z

    move-result v0

    if-nez v0, :cond_5

    const v0, 0x7f0a020b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/miui/packageInstaller/view/h;->v0:Landroid/widget/ImageView;

    :cond_5
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->w()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/packageinstaller/utils/g;->t(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/miui/packageInstaller/view/h;->t0:Lcom/airbnb/lottie/LottieAnimationView;

    if-eqz v0, :cond_7

    const-string v1, "dark_safe_mode_loading.json"

    goto :goto_1

    :cond_6
    iget-object v0, p0, Lcom/miui/packageInstaller/view/h;->t0:Lcom/airbnb/lottie/LottieAnimationView;

    if-eqz v0, :cond_7

    const-string v1, "safe_loading.json"

    :goto_1
    invoke-virtual {v0, v1}, Lcom/airbnb/lottie/LottieAnimationView;->setAnimation(Ljava/lang/String;)V

    :cond_7
    iget-object v0, p0, Lcom/miui/packageInstaller/view/h;->t0:Lcom/airbnb/lottie/LottieAnimationView;

    if-nez v0, :cond_8

    goto :goto_2

    :cond_8
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/airbnb/lottie/LottieAnimationView;->setRepeatMode(I)V

    :goto_2
    iget-object v0, p0, Lcom/miui/packageInstaller/view/h;->t0:Lcom/airbnb/lottie/LottieAnimationView;

    if-nez v0, :cond_9

    goto :goto_3

    :cond_9
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/airbnb/lottie/LottieAnimationView;->setRepeatCount(I)V

    :goto_3
    const v0, 0x7f0a0205

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "view.findViewById(R.id.loadingText)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/view/h;->u0:Landroid/widget/TextView;

    const v0, 0x7f0a01fc

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "view.findViewById(R.id.ll_reason)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/miui/packageInstaller/view/h;->w0:Landroid/widget/LinearLayout;

    const v0, 0x7f0a00ad

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v0, "view.findViewById(R.id.btn_submit)"

    invoke-static {p1, v0}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/Button;

    iput-object p1, p0, Lcom/miui/packageInstaller/view/h;->s0:Landroid/widget/Button;

    invoke-virtual {p0, v3}, Lcom/miui/packageInstaller/view/h;->j2(I)V

    invoke-direct {p0}, Lcom/miui/packageInstaller/view/h;->e2()V

    invoke-direct {p0}, Lcom/miui/packageInstaller/view/h;->f2()V

    return-void
.end method

.method private static final h2(Lcom/miui/packageInstaller/view/h;)V
    .locals 3

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/view/h;->k0:Lmiuix/core/widget/NestedScrollView;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const-string v0, "scrollView"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_0
    const/16 v2, 0x82

    invoke-virtual {v0, v2}, Lmiuix/core/widget/NestedScrollView;->t(I)Z

    iget-object v0, p0, Lcom/miui/packageInstaller/view/h;->p0:Landroid/widget/EditText;

    const-string v2, "mEtReason"

    if-nez v0, :cond_1

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_1
    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    iget-object p0, p0, Lcom/miui/packageInstaller/view/h;->p0:Landroid/widget/EditText;

    if-nez p0, :cond_2

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    move-object v1, p0

    :goto_0
    const/4 p0, 0x1

    invoke-virtual {v1, p0}, Landroid/widget/EditText;->setFocusableInTouchMode(Z)V

    return-void
.end method

.method private final i2(Z)V
    .locals 5

    iget-object v0, p0, Lcom/miui/packageInstaller/view/h;->t0:Lcom/airbnb/lottie/LottieAnimationView;

    if-eqz p1, :cond_0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/airbnb/lottie/LottieAnimationView;->p()V

    goto :goto_0

    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/airbnb/lottie/LottieAnimationView;->h()V

    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/miui/packageInstaller/view/h;->t0:Lcom/airbnb/lottie/LottieAnimationView;

    const/4 v1, 0x0

    const/16 v2, 0x8

    if-nez v0, :cond_2

    goto :goto_2

    :cond_2
    if-eqz p1, :cond_3

    move v3, v1

    goto :goto_1

    :cond_3
    move v3, v2

    :goto_1
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_2
    iget-object v0, p0, Lcom/miui/packageInstaller/view/h;->u0:Landroid/widget/TextView;

    const/4 v3, 0x0

    if-nez v0, :cond_4

    const-string v0, "loadingView"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v3

    :cond_4
    if-eqz p1, :cond_5

    move v4, v1

    goto :goto_3

    :cond_5
    move v4, v2

    :goto_3
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/miui/packageInstaller/view/h;->n0:Landroidx/recyclerview/widget/RecyclerView;

    if-nez v0, :cond_6

    const-string v0, "mRvReasonList"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v3

    :cond_6
    if-eqz p1, :cond_7

    move v4, v2

    goto :goto_4

    :cond_7
    move v4, v1

    :goto_4
    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/miui/packageInstaller/view/h;->o0:Landroid/widget/CheckBox;

    if-nez v0, :cond_8

    const-string v0, "mCbReasonOther"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_5

    :cond_8
    move-object v3, v0

    :goto_5
    if-eqz p1, :cond_9

    move v1, v2

    :cond_9
    invoke-virtual {v3, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    return-void
.end method

.method private final l2()V
    .locals 5

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->w()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->w()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    const v3, 0x7f11031b

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_0
    move-object v1, v2

    :goto_0
    const/4 v3, 0x0

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->w()Landroid/content/Context;

    move-result-object v0

    instance-of v0, v0, Lcom/miui/packageInstaller/ui/secure/SecurityModeFeedBackActivity;

    if-eqz v0, :cond_2

    new-instance v0, Lp5/b;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->p()Landroidx/fragment/app/e;

    move-result-object v1

    const-string v3, "null cannot be cast to non-null type com.miui.packageInstaller.analytics.IPage"

    invoke-static {v1, v3}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lo5/a;

    const-string v3, "safe_mode_shut_research_submit_btn"

    const-string v4, "button"

    invoke-direct {v0, v3, v4, v1}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-direct {p0}, Lcom/miui/packageInstaller/view/h;->X1()Ljava/lang/String;

    move-result-object v1

    const-string v3, "submit_result"

    invoke-virtual {v0, v3, v1}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/packageInstaller/view/h;->p0:Landroid/widget/EditText;

    if-nez v1, :cond_1

    const-string v1, "mEtReason"

    invoke-static {v1}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    move-object v2, v1

    :goto_1
    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "content_input"

    invoke-virtual {v0, v2, v1}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object v0

    invoke-virtual {v0}, Lp5/f;->c()Z

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->w()Landroid/content/Context;

    move-result-object v0

    const-string v1, "null cannot be cast to non-null type com.miui.packageInstaller.ui.secure.SecurityModeFeedBackActivity"

    invoke-static {v0, v1}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/miui/packageInstaller/ui/secure/SecurityModeFeedBackActivity;

    invoke-virtual {v0}, Lmiuix/appcompat/app/j;->finish()V

    :cond_2
    return-void
.end method


# virtual methods
.method public N0(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1

    const-string v0, "view"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1, p2}, Landroidx/fragment/app/Fragment;->N0(Landroid/view/View;Landroid/os/Bundle;)V

    invoke-direct {p0, p1}, Lcom/miui/packageInstaller/view/h;->g2(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/miui/packageInstaller/view/h;->Z1()V

    invoke-direct {p0}, Lcom/miui/packageInstaller/view/h;->U1()V

    return-void
.end method

.method public final V1()Ll6/d;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/view/h;->z0:Ll6/d;

    return-object v0
.end method

.method public final Y1()Lj6/b;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/view/h;->y0:Lj6/b;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    const-string v0, "mAdapter"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v0, 0x0

    return-object v0
.end method

.method public final j2(I)V
    .locals 5

    iget-object v0, p0, Lcom/miui/packageInstaller/view/h;->r0:Landroid/widget/TextView;

    if-nez v0, :cond_0

    const-string v0, "mTvInputCount"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v0, 0x0

    :cond_0
    sget-object v1, Lm8/w;->a:Lm8/w;

    const v1, 0x7f110313

    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->T(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "getString(R.string.secur\u2026ode_feedback_input_count)"

    invoke-static {v1, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x1

    new-array v3, v2, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v3, v4

    invoke-static {v3, v2}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object p1

    invoke-static {v1, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    const-string v1, "format(format, *args)"

    invoke-static {p1, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final k2(Lj6/b;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/view/h;->y0:Lj6/b;

    return-void
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 2

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/widget/CompoundButton;->getId()I

    move-result p1

    const v1, 0x7f0a00bd

    if-ne p1, v1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    move p1, v0

    :goto_0
    if-eqz p1, :cond_3

    invoke-direct {p0}, Lcom/miui/packageInstaller/view/h;->W1()Z

    move-result p1

    invoke-direct {p0, p1}, Lcom/miui/packageInstaller/view/h;->T1(Z)V

    iget-object p1, p0, Lcom/miui/packageInstaller/view/h;->q0:Landroid/widget/FrameLayout;

    if-nez p1, :cond_1

    const-string p1, "flInputView"

    invoke-static {p1}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 p1, 0x0

    :cond_1
    if-eqz p2, :cond_2

    goto :goto_1

    :cond_2
    const/16 v0, 0x8

    :goto_1
    invoke-virtual {p1, v0}, Landroid/widget/FrameLayout;->setVisibility(I)V

    if-eqz p2, :cond_3

    new-instance p1, Landroid/os/Handler;

    invoke-direct {p1}, Landroid/os/Handler;-><init>()V

    new-instance p2, Lcom/miui/packageInstaller/view/f;

    invoke-direct {p2, p0}, Lcom/miui/packageInstaller/view/f;-><init>(Lcom/miui/packageInstaller/view/h;)V

    invoke-virtual {p1, p2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_3
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    const-string v0, "newConfig"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    invoke-direct {p0}, Lcom/miui/packageInstaller/view/h;->e2()V

    return-void
.end method

.method public s0(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    const-string p3, "inflater"

    invoke-static {p1, p3}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result p3

    if-eqz p3, :cond_1

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->v()Z

    move-result p3

    if-eqz p3, :cond_0

    const p3, 0x7f0d0098

    goto :goto_0

    :cond_0
    const p3, 0x7f0d0096

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/android/packageinstaller/utils/g;->v()Z

    move-result p3

    if-eqz p3, :cond_2

    const p3, 0x7f0d0097

    goto :goto_0

    :cond_2
    const p3, 0x7f0d0095

    :goto_0
    const/4 v0, 0x0

    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public v0()V
    .locals 1

    invoke-super {p0}, Landroidx/fragment/app/Fragment;->v0()V

    iget-object v0, p0, Lcom/miui/packageInstaller/view/h;->x0:Lf6/l;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lf6/l;->b()V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/miui/packageInstaller/view/h;->x0:Lf6/l;

    iget-object v0, p0, Lcom/miui/packageInstaller/view/h;->t0:Lcom/airbnb/lottie/LottieAnimationView;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/airbnb/lottie/LottieAnimationView;->h()V

    :cond_1
    return-void
.end method
