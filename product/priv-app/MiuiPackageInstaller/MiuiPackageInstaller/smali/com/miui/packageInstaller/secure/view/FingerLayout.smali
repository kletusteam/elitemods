.class public final Lcom/miui/packageInstaller/secure/view/FingerLayout;
.super Landroid/widget/LinearLayout;


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Lcom/airbnb/lottie/LottieAnimationView;

.field private final c:I

.field private final d:I

.field private final e:I

.field private final f:I

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/miui/packageInstaller/secure/view/FingerLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p2, 0x7f07012c

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result p1

    iput p1, p0, Lcom/miui/packageInstaller/secure/view/FingerLayout;->c:I

    sget-boolean p1, Lcom/android/packageinstaller/utils/g;->h:Z

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p2, 0x7f070106

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result p1

    :goto_0
    iput p1, p0, Lcom/miui/packageInstaller/secure/view/FingerLayout;->d:I

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p2, 0x7f0700a8

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result p1

    iput p1, p0, Lcom/miui/packageInstaller/secure/view/FingerLayout;->e:I

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p2, 0x7f070137

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result p1

    iput p1, p0, Lcom/miui/packageInstaller/secure/view/FingerLayout;->f:I

    invoke-static {}, Lb8/j;->f()Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/miui/packageInstaller/secure/view/FingerLayout;->g:Ljava/util/List;

    invoke-static {}, Lb8/j;->f()Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/miui/packageInstaller/secure/view/FingerLayout;->h:Ljava/util/List;

    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 8

    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    sget-boolean v0, Lcom/android/packageinstaller/utils/g;->h:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v0, v0, v0}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    const v0, 0x7f0a016b

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.finger_tips)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/secure/view/FingerLayout;->a:Landroid/widget/TextView;

    const v0, 0x7f0a0169

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.finger_icon)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/airbnb/lottie/LottieAnimationView;

    iput-object v0, p0, Lcom/miui/packageInstaller/secure/view/FingerLayout;->b:Lcom/airbnb/lottie/LottieAnimationView;

    const/4 v1, 0x0

    const-string v2, "fingerIconView"

    if-nez v0, :cond_0

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_0
    const v3, 0x7f080191

    invoke-virtual {v0, v3}, Lcom/airbnb/lottie/LottieAnimationView;->setImageResource(I)V

    iget-object v0, p0, Lcom/miui/packageInstaller/secure/view/FingerLayout;->b:Lcom/airbnb/lottie/LottieAnimationView;

    if-nez v0, :cond_1

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move-object v1, v0

    :goto_0
    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    sget-object v2, Lcom/android/packageinstaller/utils/g;->i:Ljava/lang/String;

    const-string v0, "UNDER_SCREEN_FINGER_PRINT_X_Y"

    invoke-static {v2, v0}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, ","

    filled-new-array {v0}, [Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x6

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lu8/g;->g0(Ljava/lang/CharSequence;[Ljava/lang/String;ZIILjava/lang/Object;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/miui/packageInstaller/secure/view/FingerLayout;->g:Ljava/util/List;

    sget-object v2, Lcom/android/packageinstaller/utils/g;->j:Ljava/lang/String;

    const-string v1, "UNDER_SCREEN_FINGER_PRINT_SIZE"

    invoke-static {v2, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    filled-new-array {v0}, [Ljava/lang/String;

    move-result-object v3

    invoke-static/range {v2 .. v7}, Lu8/g;->g0(Ljava/lang/CharSequence;[Ljava/lang/String;ZIILjava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/packageInstaller/secure/view/FingerLayout;->h:Ljava/util/List;

    :cond_2
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DrawAllocation"
        }
    .end annotation

    invoke-super/range {p0 .. p5}, Landroid/widget/LinearLayout;->onLayout(ZIIII)V

    sget-boolean p1, Lcom/android/packageinstaller/utils/g;->h:Z

    if-eqz p1, :cond_5

    const/4 p1, 0x2

    new-array p1, p1, [I

    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->getLocationOnScreen([I)V

    iget-object p2, p0, Lcom/miui/packageInstaller/secure/view/FingerLayout;->g:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    const/4 p3, 0x1

    if-le p2, p3, :cond_5

    iget-object p2, p0, Lcom/miui/packageInstaller/secure/view/FingerLayout;->h:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    if-le p2, p3, :cond_5

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-static {p2}, Laa/b;->j(Landroid/content/Context;)Z

    move-result p2

    if-eqz p2, :cond_0

    iget-object p2, p0, Lcom/miui/packageInstaller/secure/view/FingerLayout;->g:Ljava/util/List;

    invoke-interface {p2, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p2

    aget p1, p1, p3

    sub-int/2addr p2, p1

    iget-object p1, p0, Lcom/miui/packageInstaller/secure/view/FingerLayout;->h:Ljava/util/List;

    invoke-interface {p1, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p1

    add-int/2addr p2, p1

    iget p1, p0, Lcom/miui/packageInstaller/secure/view/FingerLayout;->e:I

    add-int/2addr p2, p1

    goto :goto_0

    :cond_0
    iget-object p2, p0, Lcom/miui/packageInstaller/secure/view/FingerLayout;->g:Ljava/util/List;

    invoke-interface {p2, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p2

    aget p1, p1, p3

    sub-int/2addr p2, p1

    iget p1, p0, Lcom/miui/packageInstaller/secure/view/FingerLayout;->e:I

    sub-int/2addr p2, p1

    :goto_0
    iget-object p1, p0, Lcom/miui/packageInstaller/secure/view/FingerLayout;->a:Landroid/widget/TextView;

    const/4 p3, 0x0

    const-string p4, "tipsTextView"

    if-nez p1, :cond_1

    invoke-static {p4}, Lm8/i;->s(Ljava/lang/String;)V

    move-object p1, p3

    :cond_1
    iget-object p5, p0, Lcom/miui/packageInstaller/secure/view/FingerLayout;->a:Landroid/widget/TextView;

    if-nez p5, :cond_2

    invoke-static {p4}, Lm8/i;->s(Ljava/lang/String;)V

    move-object p5, p3

    :cond_2
    invoke-virtual {p5}, Landroid/widget/TextView;->getLeft()I

    move-result p5

    iget-object v0, p0, Lcom/miui/packageInstaller/secure/view/FingerLayout;->a:Landroid/widget/TextView;

    if-nez v0, :cond_3

    invoke-static {p4}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, p3

    :cond_3
    invoke-virtual {v0}, Landroid/widget/TextView;->getRight()I

    move-result v0

    iget-object v1, p0, Lcom/miui/packageInstaller/secure/view/FingerLayout;->a:Landroid/widget/TextView;

    if-nez v1, :cond_4

    invoke-static {p4}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    move-object p3, v1

    :goto_1
    invoke-virtual {p3}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result p3

    add-int/2addr p3, p2

    invoke-virtual {p1, p5, p2, v0, p3}, Landroid/widget/TextView;->layout(IIII)V

    :cond_5
    return-void
.end method

.method protected onMeasure(II)V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DrawAllocation"
        }
    .end annotation

    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    sget-boolean p2, Lcom/android/packageinstaller/utils/g;->h:Z

    if-eqz p2, :cond_0

    iget-object p2, p0, Lcom/miui/packageInstaller/secure/view/FingerLayout;->g:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    const/4 v0, 0x1

    if-le p2, v0, :cond_0

    iget-object p2, p0, Lcom/miui/packageInstaller/secure/view/FingerLayout;->h:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    if-le p2, v0, :cond_0

    invoke-static {}, Lf6/d;->d()I

    move-result p2

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lf6/d;->h(Landroid/content/Context;)I

    move-result v1

    add-int/2addr p2, v1

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Laa/b;->d(Landroid/content/Context;)I

    move-result v1

    add-int/2addr p2, v1

    iget-object v1, p0, Lcom/miui/packageInstaller/secure/view/FingerLayout;->g:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    neg-int v0, v0

    add-int/2addr p2, v0

    iget v0, p0, Lcom/miui/packageInstaller/secure/view/FingerLayout;->c:I

    add-int/2addr p2, v0

    iget v0, p0, Lcom/miui/packageInstaller/secure/view/FingerLayout;->d:I

    sub-int/2addr p2, v0

    iget v0, p0, Lcom/miui/packageInstaller/secure/view/FingerLayout;->f:I

    sub-int/2addr p2, v0

    invoke-virtual {p0, p1, p2}, Landroid/widget/LinearLayout;->setMeasuredDimension(II)V

    :cond_0
    return-void
.end method
