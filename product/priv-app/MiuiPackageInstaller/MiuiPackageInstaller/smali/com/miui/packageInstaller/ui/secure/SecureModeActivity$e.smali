.class final Lcom/miui/packageInstaller/ui/secure/SecureModeActivity$e;
.super Lf8/k;

# interfaces
.implements Ll8/p;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->M1()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf8/k;",
        "Ll8/p<",
        "Lv8/e0;",
        "Ld8/d<",
        "-",
        "La8/v;",
        ">;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation runtime Lf8/f;
    c = "com.miui.packageInstaller.ui.secure.SecureModeActivity$startSecurityModeOpenAnim$2"
    f = "SecureModeActivity.kt"
    l = {
        0x1f1
    }
    m = "invokeSuspend"
.end annotation


# instance fields
.field e:J

.field f:I

.field final synthetic g:Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;


# direct methods
.method constructor <init>(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;Ld8/d;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;",
            "Ld8/d<",
            "-",
            "Lcom/miui/packageInstaller/ui/secure/SecureModeActivity$e;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity$e;->g:Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;

    const/4 p1, 0x2

    invoke-direct {p0, p1, p2}, Lf8/k;-><init>(ILd8/d;)V

    return-void
.end method


# virtual methods
.method public final b(Ljava/lang/Object;Ld8/d;)Ld8/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ld8/d<",
            "*>;)",
            "Ld8/d<",
            "La8/v;",
            ">;"
        }
    .end annotation

    new-instance p1, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity$e;

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity$e;->g:Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;

    invoke-direct {p1, v0, p2}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity$e;-><init>(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;Ld8/d;)V

    return-object p1
.end method

.method public bridge synthetic g(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lv8/e0;

    check-cast p2, Ld8/d;

    invoke-virtual {p0, p1, p2}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity$e;->q(Lv8/e0;Ld8/d;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final n(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 11

    invoke-static {}, Le8/b;->c()Ljava/lang/Object;

    move-result-object v0

    iget v1, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity$e;->f:I

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    if-ne v1, v2, :cond_0

    iget-wide v0, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity$e;->e:J

    invoke-static {p1}, La8/n;->b(Ljava/lang/Object;)V

    goto :goto_1

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    invoke-static {p1}, La8/n;->b(Ljava/lang/Object;)V

    iget-object p1, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity$e;->g:Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;

    invoke-static {p1}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->Y0(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;)J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long p1, v3, v5

    const-wide/16 v3, 0x1

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity$e;->g:Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;

    invoke-static {p1}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->Y0(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;)J

    move-result-wide v5

    add-long/2addr v5, v3

    invoke-static {p1, v5, v6}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->e1(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;J)V

    invoke-static {p1}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->Y0(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;)J

    move-result-wide v3

    goto :goto_0

    :cond_2
    iget-object p1, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity$e;->g:Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;

    invoke-static {p1}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->Z0(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;)J

    move-result-wide v5

    add-long/2addr v5, v3

    invoke-static {p1, v5, v6}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->f1(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;J)V

    invoke-static {p1}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->Z0(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;)J

    move-result-wide v3

    :goto_0
    new-instance v5, Ld6/n0;

    iget-object p1, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity$e;->g:Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;

    invoke-direct {v5, p1}, Ld6/n0;-><init>(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;)V

    iget-object p1, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity$e;->g:Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;

    invoke-static {p1}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->Z0(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;)J

    move-result-wide v6

    iput-wide v3, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity$e;->e:J

    iput v2, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity$e;->f:I

    move-wide v8, v3

    move-object v10, p0

    invoke-virtual/range {v5 .. v10}, Ld6/n0;->d(JJLd8/d;)Ljava/lang/Object;

    move-result-object p1

    if-ne p1, v0, :cond_3

    return-object v0

    :cond_3
    move-wide v0, v3

    :goto_1
    iget-object p1, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity$e;->g:Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;

    invoke-static {p1, v0, v1}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->f1(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;J)V

    iget-object p1, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity$e;->g:Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;

    const-string v0, "safe_mode_openstate"

    invoke-static {p1, v0}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->d1(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;Ljava/lang/String;)V

    sget-object p1, La8/v;->a:La8/v;

    return-object p1
.end method

.method public final q(Lv8/e0;Ld8/d;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lv8/e0;",
            "Ld8/d<",
            "-",
            "La8/v;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity$e;->b(Ljava/lang/Object;Ld8/d;)Ld8/d;

    move-result-object p1

    check-cast p1, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity$e;

    sget-object p2, La8/v;->a:La8/v;

    invoke-virtual {p1, p2}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity$e;->n(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method
