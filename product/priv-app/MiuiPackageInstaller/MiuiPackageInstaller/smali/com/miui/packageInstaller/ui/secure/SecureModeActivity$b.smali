.class public final Lcom/miui/packageInstaller/ui/secure/SecureModeActivity$b;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->n1()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;


# direct methods
.method constructor <init>(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity$b;->a:Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 3

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity$b;->a:Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;

    invoke-static {v0}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->W0(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;)Landroid/view/ViewGroup;

    move-result-object v0

    const/4 v1, 0x0

    const-string v2, "contentLayout"

    if-nez v0, :cond_0

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_0
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity$b;->a:Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;

    invoke-static {v0}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->W0(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;)Landroid/view/ViewGroup;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move-object v1, v0

    :goto_0
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getWidth()I

    move-result v0

    invoke-static {}, Lf6/d;->g()I

    move-result v1

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity$b;->a:Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->g1(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;Z)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity$b;->a:Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;

    invoke-static {v0}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->b1(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;)V

    :cond_2
    return-void
.end method
