.class public Lcom/miui/packageInstaller/InstallStart;
.super Landroid/app/Activity;


# static fields
.field private static final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Landroid/content/pm/PackageManager;

.field private b:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/miui/packageInstaller/InstallStart;->c:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/miui/packageInstaller/InstallStart;->b:Z

    return-void
.end method

.method public static synthetic a(Lcom/miui/packageInstaller/InstallStart;Landroid/content/Intent;Lm5/e;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/miui/packageInstaller/InstallStart;->m(Landroid/content/Intent;Lm5/e;)V

    return-void
.end method

.method public static synthetic b(Lm5/e;)V
    .locals 0

    invoke-static {p0}, Lcom/miui/packageInstaller/InstallStart;->p(Lm5/e;)V

    return-void
.end method

.method public static synthetic c(Lcom/miui/packageInstaller/InstallStart;Lm5/e;Ljava/lang/String;Landroid/content/Intent;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/miui/packageInstaller/InstallStart;->q(Lm5/e;Ljava/lang/String;Landroid/content/Intent;)V

    return-void
.end method

.method public static synthetic d(Lcom/miui/packageInstaller/InstallStart;Lm5/e;Landroid/content/Intent;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/miui/packageInstaller/InstallStart;->n(Lm5/e;Landroid/content/Intent;)V

    return-void
.end method

.method public static synthetic e(Lcom/miui/packageInstaller/InstallStart;Landroid/content/Intent;Lm5/e;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/miui/packageInstaller/InstallStart;->o(Landroid/content/Intent;Lm5/e;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic f(Landroid/app/Activity;)V
    .locals 0

    invoke-static {p0}, Lcom/miui/packageInstaller/InstallStart;->l(Landroid/app/Activity;)V

    return-void
.end method

.method private g(Landroid/content/Intent;Lm5/e;)V
    .locals 2

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object v0

    new-instance v1, Lm5/j0;

    invoke-direct {v1, p0, p2, p1}, Lm5/j0;-><init>(Lcom/miui/packageInstaller/InstallStart;Lm5/e;Landroid/content/Intent;)V

    invoke-virtual {v0, v1}, Lf6/z;->g(Ljava/lang/Runnable;)V

    return-void
.end method

.method private h(ILjava/lang/String;)Z
    .locals 5

    const/4 v0, 0x0

    :try_start_0
    invoke-static {p2}, Lcom/android/packageinstaller/compat/IPackageManagerCompat;->getAppOpPermissionPackages(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p2

    if-nez p2, :cond_0

    return v0

    :cond_0
    array-length v1, p2

    move v2, v0

    :goto_0
    if-ge v2, v1, :cond_2

    aget-object v3, p2, v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    invoke-virtual {p0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    invoke-static {v4, v3, v0}, Lcom/android/packageinstaller/compat/PackageManagerCompat;->getPackageUid(Landroid/content/pm/PackageManager;Ljava/lang/String;I)I

    move-result v3
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    if-ne p1, v3, :cond_1

    const/4 p1, 0x1

    return p1

    :catch_0
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :catch_1
    :cond_2
    return v0
.end method

.method private i(I)I
    .locals 6

    invoke-virtual {p0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object p1

    const/4 v0, -0x1

    if-eqz p1, :cond_0

    array-length v1, p1

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v1, :cond_0

    aget-object v4, p1, v3

    :try_start_0
    invoke-virtual {p0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    invoke-virtual {v5, v4, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v4

    iget v4, v4, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    invoke-static {v0, v4}, Ljava/lang/Math;->max(II)I

    move-result v0
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    return v0
.end method

.method private j(Landroid/content/Intent;Landroid/content/pm/ApplicationInfo;)I
    .locals 6

    const-string v0, "android.intent.extra.ORIGINATING_UID"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result p1

    if-eqz p2, :cond_1

    if-eq p1, v1, :cond_0

    invoke-static {p2}, Lcom/android/packageinstaller/compat/ApplicationInfoCompat;->privateFlags(Landroid/content/pm/ApplicationInfo;)I

    move-result v0

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    return p1

    :cond_0
    iget p1, p2, Landroid/content/pm/ApplicationInfo;->uid:I

    return p1

    :cond_1
    :try_start_0
    invoke-static {p0}, Lcom/android/packageinstaller/compat/ActivityCompat;->getActivityToken(Landroid/app/Activity;)Landroid/os/IBinder;

    move-result-object p2

    invoke-static {p2}, Lcom/android/packageinstaller/compat/ActivityManagerCompat;->getLaunchedFromUid(Landroid/os/IBinder;)I

    move-result p2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    if-eq p1, v1, :cond_3

    iget-object v0, p0, Lcom/miui/packageInstaller/InstallStart;->a:Landroid/content/pm/PackageManager;

    invoke-virtual {v0, p2}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    array-length v1, v0

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v1, :cond_3

    aget-object v4, v0, v3

    :try_start_1
    iget-object v5, p0, Lcom/miui/packageInstaller/InstallStart;->a:Landroid/content/pm/PackageManager;

    invoke-virtual {v5, v4, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v4

    invoke-static {v4}, Lcom/android/packageinstaller/compat/ApplicationInfoCompat;->privateFlags(Landroid/content/pm/ApplicationInfo;)I

    move-result v4
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    and-int/lit8 v4, v4, 0x8

    if-eqz v4, :cond_2

    return p1

    :catch_0
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_3
    return p2

    :catch_1
    const-string p1, "InstallStart"

    const-string p2, "Could not determine the launching uid."

    invoke-static {p1, p2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/miui/packageInstaller/InstallStart;->b:Z

    return v1
.end method

.method private k(Ljava/lang/String;)Landroid/content/pm/ApplicationInfo;
    .locals 2

    if-eqz p1, :cond_0

    :try_start_0
    invoke-virtual {p0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object p1
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method private static synthetic l(Landroid/app/Activity;)V
    .locals 0

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method private synthetic m(Landroid/content/Intent;Lm5/e;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/miui/packageInstaller/InstallStart;->s(Landroid/content/Intent;Lm5/e;)V

    return-void
.end method

.method private synthetic n(Lm5/e;Landroid/content/Intent;)V
    .locals 4

    invoke-static {}, Lm5/b;->m()Ljava/util/List;

    move-result-object v0

    sget-object v1, Lm5/m0;->a:Lm5/m0;

    invoke-interface {v0, v1}, Ljava/util/List;->forEach(Ljava/util/function/Consumer;)V

    invoke-virtual {p1}, Lm5/e;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lt5/h;->b(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    const v0, 0x7f1102da

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p2, p1, v0}, Lcom/miui/packageInstaller/InstallStart;->r(Landroid/content/Intent;Lm5/e;Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Landroid/app/Activity;->setResult(I)V

    return-void

    :cond_0
    invoke-static {p1}, Lt5/z;->b(Lm5/e;)Lcom/miui/packageInstaller/model/UnknownSourceInstallPolicy;

    move-result-object v0

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/UnknownSourceInstallPolicy;->getAllowInstall()Z

    move-result v0

    if-nez v0, :cond_1

    const v0, 0x7f11003c

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p1, Lm5/e;->e:Ljava/lang/String;

    aput-object v3, v2, v1

    invoke-virtual {p0, v0, v2}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p2, p1, v0}, Lcom/miui/packageInstaller/InstallStart;->r(Landroid/content/Intent;Lm5/e;Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Landroid/app/Activity;->setResult(I)V

    return-void

    :cond_1
    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object v0

    new-instance v1, Lm5/i0;

    invoke-direct {v1, p0, p2, p1}, Lm5/i0;-><init>(Lcom/miui/packageInstaller/InstallStart;Landroid/content/Intent;Lm5/e;)V

    invoke-virtual {v0, v1}, Lf6/z;->e(Ljava/lang/Runnable;)V

    return-void
.end method

.method private synthetic o(Landroid/content/Intent;Lm5/e;Landroid/view/View;)V
    .locals 1

    const-string p3, "skip_unknown_source_dialog"

    const/4 v0, 0x1

    invoke-virtual {p1, p3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-direct {p0, p1, p2}, Lcom/miui/packageInstaller/InstallStart;->s(Landroid/content/Intent;Lm5/e;)V

    new-instance p1, Lp5/b;

    new-instance p2, Lo5/b;

    const-string p3, "other_app_launch"

    const-string v0, ""

    invoke-direct {p2, p3, v0}, Lo5/b;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string p3, "forbid_install_toast_install_btn"

    const-string v0, "button"

    invoke-direct {p1, p3, v0, p2}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p1}, Lp5/f;->c()Z

    return-void
.end method

.method private static synthetic p(Lm5/e;)V
    .locals 1

    sget-object v0, Lcom/miui/packageInstaller/InstallStart;->c:Ljava/util/List;

    invoke-virtual {p0}, Lm5/e;->j()Ljava/lang/String;

    move-result-object p0

    invoke-interface {v0, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method private synthetic q(Lm5/e;Ljava/lang/String;Landroid/content/Intent;)V
    .locals 9

    sget-object v0, Lcom/miui/packageInstaller/InstallStart;->c:Ljava/util/List;

    invoke-virtual {p1}, Lm5/e;->j()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/android/packageinstaller/InstallerApplication;->g()Lcom/android/packageinstaller/InstallerApplication;

    move-result-object v2

    const v4, 0x7f080588

    const v1, 0x7f11002a

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-instance v7, Lm5/h0;

    invoke-direct {v7, p0, p3, p1}, Lm5/h0;-><init>(Lcom/miui/packageInstaller/InstallStart;Landroid/content/Intent;Lm5/e;)V

    const/4 v8, 0x1

    move-object v3, p2

    move-object v6, p1

    invoke-static/range {v2 .. v8}, Lf6/j;->h(Landroid/content/Context;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lm5/e;Landroid/view/View$OnClickListener;I)Lf6/j;

    move-result-object p2

    invoke-virtual {p2}, Lf6/j;->o()V

    invoke-virtual {p1}, Lm5/e;->j()Ljava/lang/String;

    move-result-object p2

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object p2

    new-instance p3, Lm5/l0;

    invoke-direct {p3, p1}, Lm5/l0;-><init>(Lm5/e;)V

    const-wide/16 v0, 0x7d0

    invoke-virtual {p2, p3, v0, v1}, Lf6/z;->d(Ljava/lang/Runnable;J)V

    :cond_0
    return-void
.end method

.method private r(Landroid/content/Intent;Lm5/e;Ljava/lang/String;)V
    .locals 2

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object v0

    new-instance v1, Lm5/k0;

    invoke-direct {v1, p0, p2, p3, p1}, Lm5/k0;-><init>(Lcom/miui/packageInstaller/InstallStart;Lm5/e;Ljava/lang/String;Landroid/content/Intent;)V

    invoke-virtual {v0, v1}, Lf6/z;->e(Ljava/lang/Runnable;)V

    return-void
.end method

.method private s(Landroid/content/Intent;Lm5/e;)V
    .locals 1

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    const/high16 p1, 0x2000000

    invoke-virtual {v0, p1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string p1, "EXTRA_CALLING_PACKAGE"

    invoke-virtual {v0, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const/high16 p1, 0x10000000

    invoke-virtual {v0, p1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const-class p1, Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;

    invoke-virtual {v0, p0, p1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    :try_start_0
    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    const-string p2, "InstallStart"

    const-string v0, "start next Activity error : "

    invoke-static {p2, v0, p1}, Lf6/o;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    const-string p2, "not have permission"

    invoke-virtual {p1, p2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_0

    const p1, 0x7f1103dd

    const/4 p2, 0x0

    invoke-static {p0, p1, p2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    :cond_0
    :goto_0
    return-void
.end method


# virtual methods
.method public getCallingPackage()Ljava/lang/String;
    .locals 4

    invoke-super {p0}, Landroid/app/Activity;->getCallingPackage()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    :try_start_0
    const-class v1, Landroid/app/Activity;

    const-string v2, "mReferrer"

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    invoke-virtual {v1, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    :catch_0
    move-exception v1

    const-string v2, "InstallStart"

    const-string v3, "mReferrer invoke failed"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 16

    move-object/from16 v1, p0

    invoke-super/range {p0 .. p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v0, "skip_unknown_source_dialog"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    const-string v0, "android.content.pm.extra.SESSION_ID"

    const/4 v3, -0x1

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    iput-object v4, v1, Lcom/miui/packageInstaller/InstallStart;->a:Landroid/content/pm/PackageManager;

    const/4 v5, 0x0

    if-eq v0, v3, :cond_0

    sget-boolean v0, Lmiui/os/Build;->IS_INTERNATIONAL_BUILD:Z

    if-nez v0, :cond_0

    invoke-virtual {v4}, Landroid/content/pm/PackageManager;->getPackageInstaller()Landroid/content/pm/PackageInstaller;

    move-result-object v2

    invoke-static {v2, v0, v5}, Lcom/android/packageinstaller/compat/PackageInstallerCompat;->setPermissionsResult(Landroid/content/pm/PackageInstaller;IZ)V

    :goto_0
    invoke-virtual {v1, v5}, Landroid/app/Activity;->setResult(I)V

    :goto_1
    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->finish()V

    return-void

    :cond_0
    invoke-virtual {v2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v4

    const-string v6, "content"

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-static {v0}, Lf6/c0;->a(Landroid/net/Uri;)V

    :cond_1
    const-string v0, "sourcePackage"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v4, "sourcePackageName"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v6, "originatingUid"

    invoke-virtual {v2, v6, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    const-string v7, "expid"

    invoke-virtual {v2, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "dmCode"

    invoke-virtual {v2, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "sourcePackage = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v10, " expId = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v10, " dmCode = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v10, " sourcePackageName = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v11, " sourcePackageUid = "

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const-string v11, "InstallStart"

    invoke-static {v11, v9}, Lf6/o;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_2

    const-string v9, "message"

    invoke-static {v9, v0}, Lq2/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    new-instance v0, Lo5/b;

    const/4 v9, 0x0

    const-string v12, ""

    invoke-direct {v0, v12, v9}, Lo5/b;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_3

    invoke-virtual {v0, v7}, Lo5/b;->a(Ljava/lang/String;)V

    :cond_3
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "mns_code"

    invoke-static {v0, v8}, Lq2/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    const-string v0, "apk_download_url"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {p0 .. p0}, Lcom/miui/packageInstaller/InstallStart;->getCallingPackage()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v1, v8}, Lcom/miui/packageInstaller/InstallStart;->k(Ljava/lang/String;)Landroid/content/pm/ApplicationInfo;

    move-result-object v9

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {v1, v0, v9}, Lcom/miui/packageInstaller/InstallStart;->j(Landroid/content/Intent;Landroid/content/pm/ApplicationInfo;)I

    move-result v13

    invoke-direct {v1, v13}, Lcom/miui/packageInstaller/InstallStart;->i(I)I

    move-result v14

    const-string v0, "com.xiaomi.market"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    if-eq v6, v3, :cond_7

    :try_start_0
    iget-object v0, v1, Lcom/miui/packageInstaller/InstallStart;->a:Landroid/content/pm/PackageManager;

    const/16 v15, 0x3040

    invoke-virtual {v0, v8, v15}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    if-eqz v0, :cond_5

    array-length v15, v0

    if-lez v15, :cond_5

    aget-object v0, v0, v5

    invoke-virtual {v0}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v0

    invoke-static {v0}, Lcom/android/packageinstaller/utils/d;->e([B)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_5
    move-object v0, v12

    :goto_2
    const-string v15, "7b6dc7079c34739ce81159719fb5eb61d2a03225"

    invoke-virtual {v15, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_2

    if-eqz v0, :cond_6

    :try_start_1
    invoke-direct {v1, v6}, Lcom/miui/packageInstaller/InstallStart;->i(I)I

    move-result v14
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    move-object v8, v4

    goto :goto_3

    :catch_0
    move-exception v0

    goto :goto_4

    :cond_6
    move v6, v13

    :goto_3
    :try_start_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "miui market callingPackage = "

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v11, v0}, Lf6/o;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_5

    :catch_1
    move-exception v0

    move-object v4, v8

    goto :goto_4

    :catch_2
    move-exception v0

    move-object v4, v8

    move v6, v13

    :goto_4
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    move-object v8, v4

    :goto_5
    move v13, v6

    :cond_7
    if-eqz v9, :cond_8

    invoke-static {v9}, Lcom/android/packageinstaller/compat/ApplicationInfoCompat;->privateFlags(Landroid/content/pm/ApplicationInfo;)I

    move-result v0

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_8

    const-string v0, "android.intent.extra.NOT_UNKNOWN_SOURCE"

    invoke-virtual {v2, v0, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_6

    :cond_8
    move v0, v5

    :goto_6
    if-nez v0, :cond_b

    if-eq v13, v3, :cond_b

    const/4 v0, 0x1

    if-gez v14, :cond_9

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Cannot get target sdk version for uid "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v11, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_7
    iput-boolean v0, v1, Lcom/miui/packageInstaller/InstallStart;->b:Z

    goto :goto_9

    :cond_9
    const/16 v3, 0x1a

    if-lt v14, v3, :cond_b

    const-string v3, "android.permission.REQUEST_INSTALL_PACKAGES"

    invoke-direct {v1, v13, v3}, Lcom/miui/packageInstaller/InstallStart;->h(ILjava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_b

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Requesting uid "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v6, " needs to declare permission "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v11, v3}, Lf6/o;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    const v3, 0x7f110255

    invoke-virtual {v1, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v0, [Ljava/lang/Object;

    if-eqz v9, :cond_a

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    invoke-virtual {v9, v6}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    goto :goto_8

    :cond_a
    move-object v6, v12

    :goto_8
    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_7

    :cond_b
    :goto_9
    iget-boolean v0, v1, Lcom/miui/packageInstaller/InstallStart;->b:Z

    if-eqz v0, :cond_c

    goto/16 :goto_0

    :cond_c
    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {v13, v0, v8}, Lcom/android/packageinstaller/compat/UnknownSourceCompat;->getOriginatingPackage(ILandroid/content/pm/PackageManager;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v9, :cond_d

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {v9, v3}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v12

    :cond_d
    new-instance v3, Lm5/e;

    invoke-direct {v3}, Lm5/e;-><init>()V

    invoke-virtual {v3, v8}, Lm5/e;->n(Ljava/lang/String;)V

    iput-object v7, v3, Lm5/e;->h:Ljava/lang/String;

    invoke-virtual {v3, v0}, Lm5/e;->o(Ljava/lang/String;)V

    invoke-virtual {v3, v13}, Lm5/e;->p(I)V

    iput-object v12, v3, Lm5/e;->e:Ljava/lang/String;

    invoke-static {v1, v8}, Lj2/f;->p(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v3, v0}, Lm5/e;->q(Ljava/lang/Boolean;)V

    invoke-direct {v1, v2, v3}, Lcom/miui/packageInstaller/InstallStart;->g(Landroid/content/Intent;Lm5/e;)V

    goto/16 :goto_1
.end method
