.class public final Lcom/miui/packageInstaller/model/RiskControlConfig;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private rcibl:Ljava/lang/String;

.field private rcibr:Ljava/lang/String;

.field private rcic:Ljava/lang/String;

.field private rcit:Ljava/lang/String;

.field private rcivt:I

.field private rcl:Z

.field private rclbl:Ljava/lang/String;

.field private rclbr:Ljava/lang/String;

.field private rclc:Ljava/lang/String;

.field private rclhct:I

.field private rcllct:I

.field private rclmct:I

.field private rclt:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/miui/packageInstaller/model/RiskControlConfig;->rcit:Ljava/lang/String;

    iput-object v0, p0, Lcom/miui/packageInstaller/model/RiskControlConfig;->rcic:Ljava/lang/String;

    iput-object v0, p0, Lcom/miui/packageInstaller/model/RiskControlConfig;->rcibl:Ljava/lang/String;

    iput-object v0, p0, Lcom/miui/packageInstaller/model/RiskControlConfig;->rcibr:Ljava/lang/String;

    iput-object v0, p0, Lcom/miui/packageInstaller/model/RiskControlConfig;->rclt:Ljava/lang/String;

    iput-object v0, p0, Lcom/miui/packageInstaller/model/RiskControlConfig;->rclc:Ljava/lang/String;

    iput-object v0, p0, Lcom/miui/packageInstaller/model/RiskControlConfig;->rclbl:Ljava/lang/String;

    iput-object v0, p0, Lcom/miui/packageInstaller/model/RiskControlConfig;->rclbr:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final getRcibl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/model/RiskControlConfig;->rcibl:Ljava/lang/String;

    return-object v0
.end method

.method public final getRcibr()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/model/RiskControlConfig;->rcibr:Ljava/lang/String;

    return-object v0
.end method

.method public final getRcic()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/model/RiskControlConfig;->rcic:Ljava/lang/String;

    return-object v0
.end method

.method public final getRcit()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/model/RiskControlConfig;->rcit:Ljava/lang/String;

    return-object v0
.end method

.method public final getRcivt()I
    .locals 1

    iget v0, p0, Lcom/miui/packageInstaller/model/RiskControlConfig;->rcivt:I

    return v0
.end method

.method public final getRcl()Z
    .locals 1

    iget-boolean v0, p0, Lcom/miui/packageInstaller/model/RiskControlConfig;->rcl:Z

    return v0
.end method

.method public final getRclbl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/model/RiskControlConfig;->rclbl:Ljava/lang/String;

    return-object v0
.end method

.method public final getRclbr()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/model/RiskControlConfig;->rclbr:Ljava/lang/String;

    return-object v0
.end method

.method public final getRclc()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/model/RiskControlConfig;->rclc:Ljava/lang/String;

    return-object v0
.end method

.method public final getRclhct()I
    .locals 1

    iget v0, p0, Lcom/miui/packageInstaller/model/RiskControlConfig;->rclhct:I

    return v0
.end method

.method public final getRcllct()I
    .locals 1

    iget v0, p0, Lcom/miui/packageInstaller/model/RiskControlConfig;->rcllct:I

    return v0
.end method

.method public final getRclmct()I
    .locals 1

    iget v0, p0, Lcom/miui/packageInstaller/model/RiskControlConfig;->rclmct:I

    return v0
.end method

.method public final getRclt()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/model/RiskControlConfig;->rclt:Ljava/lang/String;

    return-object v0
.end method

.method public final setRcibl(Ljava/lang/String;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/model/RiskControlConfig;->rcibl:Ljava/lang/String;

    return-void
.end method

.method public final setRcibr(Ljava/lang/String;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/model/RiskControlConfig;->rcibr:Ljava/lang/String;

    return-void
.end method

.method public final setRcic(Ljava/lang/String;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/model/RiskControlConfig;->rcic:Ljava/lang/String;

    return-void
.end method

.method public final setRcit(Ljava/lang/String;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/model/RiskControlConfig;->rcit:Ljava/lang/String;

    return-void
.end method

.method public final setRcivt(I)V
    .locals 0

    iput p1, p0, Lcom/miui/packageInstaller/model/RiskControlConfig;->rcivt:I

    return-void
.end method

.method public final setRcl(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/miui/packageInstaller/model/RiskControlConfig;->rcl:Z

    return-void
.end method

.method public final setRclbl(Ljava/lang/String;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/model/RiskControlConfig;->rclbl:Ljava/lang/String;

    return-void
.end method

.method public final setRclbr(Ljava/lang/String;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/model/RiskControlConfig;->rclbr:Ljava/lang/String;

    return-void
.end method

.method public final setRclc(Ljava/lang/String;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/model/RiskControlConfig;->rclc:Ljava/lang/String;

    return-void
.end method

.method public final setRclhct(I)V
    .locals 0

    iput p1, p0, Lcom/miui/packageInstaller/model/RiskControlConfig;->rclhct:I

    return-void
.end method

.method public final setRcllct(I)V
    .locals 0

    iput p1, p0, Lcom/miui/packageInstaller/model/RiskControlConfig;->rcllct:I

    return-void
.end method

.method public final setRclmct(I)V
    .locals 0

    iput p1, p0, Lcom/miui/packageInstaller/model/RiskControlConfig;->rclmct:I

    return-void
.end method

.method public final setRclt(Ljava/lang/String;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/model/RiskControlConfig;->rclt:Ljava/lang/String;

    return-void
.end method
