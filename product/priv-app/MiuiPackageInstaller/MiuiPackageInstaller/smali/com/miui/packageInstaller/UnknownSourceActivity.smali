.class public final Lcom/miui/packageInstaller/UnknownSourceActivity;
.super Lq2/b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/packageInstaller/UnknownSourceActivity$a;
    }
.end annotation


# static fields
.field public static final C:Lcom/miui/packageInstaller/UnknownSourceActivity$a;


# instance fields
.field private A:Landroidx/core/widget/NestedScrollView;

.field private B:Landroidx/appcompat/widget/LinearLayoutCompat;

.field private u:Landroid/widget/CheckBox;

.field private v:Lcom/miui/packageInstaller/model/ApkInfo;

.field private w:Lm5/e;

.field private x:Lcom/miui/packageInstaller/model/InstallSourceTips;

.field private y:Landroidx/appcompat/widget/AppCompatImageView;

.field private z:Landroidx/appcompat/widget/AppCompatTextView;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/miui/packageInstaller/UnknownSourceActivity$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/miui/packageInstaller/UnknownSourceActivity$a;-><init>(Lm8/g;)V

    sput-object v0, Lcom/miui/packageInstaller/UnknownSourceActivity;->C:Lcom/miui/packageInstaller/UnknownSourceActivity$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lq2/b;-><init>()V

    return-void
.end method

.method public static synthetic J0(Lcom/miui/packageInstaller/UnknownSourceActivity;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/miui/packageInstaller/UnknownSourceActivity;->T0(Lcom/miui/packageInstaller/UnknownSourceActivity;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic K0(Lcom/miui/packageInstaller/UnknownSourceActivity;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/miui/packageInstaller/UnknownSourceActivity;->Q0(Lcom/miui/packageInstaller/UnknownSourceActivity;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic L0(Lcom/miui/packageInstaller/UnknownSourceActivity;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/miui/packageInstaller/UnknownSourceActivity;->P0(Lcom/miui/packageInstaller/UnknownSourceActivity;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic M0(Lcom/miui/packageInstaller/UnknownSourceActivity;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/miui/packageInstaller/UnknownSourceActivity;->S0(Lcom/miui/packageInstaller/UnknownSourceActivity;Landroid/view/View;)V

    return-void
.end method

.method public static final synthetic N0(Lcom/miui/packageInstaller/UnknownSourceActivity;ZZ)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/miui/packageInstaller/UnknownSourceActivity;->Y0(ZZ)V

    return-void
.end method

.method private final O0()V
    .locals 10

    const v0, 0x7f0a0380

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/AppCompatTextView;

    const v1, 0x7f0a0282

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroidx/core/widget/NestedScrollView;

    iput-object v1, p0, Lcom/miui/packageInstaller/UnknownSourceActivity;->A:Landroidx/core/widget/NestedScrollView;

    const v1, 0x7f0a0383

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroidx/appcompat/widget/AppCompatTextView;

    iput-object v1, p0, Lcom/miui/packageInstaller/UnknownSourceActivity;->z:Landroidx/appcompat/widget/AppCompatTextView;

    const v1, 0x7f0a00ea

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    const v2, 0x7f0a03e6

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const v3, 0x7f0a01a2

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroidx/appcompat/widget/AppCompatImageView;

    iput-object v3, p0, Lcom/miui/packageInstaller/UnknownSourceActivity;->y:Landroidx/appcompat/widget/AppCompatImageView;

    const v3, 0x7f0a00aa

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const-string v4, "findViewById<LinearLayoutCompat>(R.id.btn_layout)"

    invoke-static {v3, v4}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Landroidx/appcompat/widget/LinearLayoutCompat;

    iput-object v3, p0, Lcom/miui/packageInstaller/UnknownSourceActivity;->B:Landroidx/appcompat/widget/LinearLayoutCompat;

    const-string v3, "contentLayout"

    invoke-static {v1, v3}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/miui/packageInstaller/UnknownSourceActivity;->W0(Landroid/widget/FrameLayout;)V

    iget-object v1, p0, Lcom/miui/packageInstaller/UnknownSourceActivity;->x:Lcom/miui/packageInstaller/model/InstallSourceTips;

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/InstallSourceTips;->getType()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_0

    :cond_0
    move-object v1, v3

    :goto_0
    const/4 v4, 0x3

    const-string v5, "btnLayout"

    const/4 v6, 0x0

    const/4 v7, 0x1

    if-nez v1, :cond_1

    goto :goto_2

    :cond_1
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v8

    if-ne v8, v7, :cond_4

    iget-object v1, p0, Lcom/miui/packageInstaller/UnknownSourceActivity;->z:Landroidx/appcompat/widget/AppCompatTextView;

    invoke-direct {p0, v1}, Lcom/miui/packageInstaller/UnknownSourceActivity;->X0(Landroidx/appcompat/widget/AppCompatTextView;)V

    iget-object v1, p0, Lcom/miui/packageInstaller/UnknownSourceActivity;->y:Landroidx/appcompat/widget/AppCompatImageView;

    if-eqz v1, :cond_2

    const v8, 0x7f08010f

    invoke-virtual {v1, v8}, Landroidx/appcompat/widget/AppCompatImageView;->setImageResource(I)V

    :cond_2
    const v1, 0x7f1103d6

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f080110

    invoke-virtual {v2, v0}, Landroid/view/View;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/miui/packageInstaller/UnknownSourceActivity;->B:Landroidx/appcompat/widget/LinearLayoutCompat;

    if-nez v0, :cond_3

    :goto_1
    invoke-static {v5}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v3

    :cond_3
    invoke-direct {p0, v0, v7}, Lcom/miui/packageInstaller/UnknownSourceActivity;->V0(Landroidx/appcompat/widget/LinearLayoutCompat;Z)V

    goto/16 :goto_c

    :cond_4
    :goto_2
    const/4 v8, 0x2

    if-nez v1, :cond_5

    goto :goto_3

    :cond_5
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v9

    if-ne v9, v8, :cond_7

    iget-object v1, p0, Lcom/miui/packageInstaller/UnknownSourceActivity;->z:Landroidx/appcompat/widget/AppCompatTextView;

    invoke-direct {p0, v1}, Lcom/miui/packageInstaller/UnknownSourceActivity;->X0(Landroidx/appcompat/widget/AppCompatTextView;)V

    iget-object v1, p0, Lcom/miui/packageInstaller/UnknownSourceActivity;->y:Landroidx/appcompat/widget/AppCompatImageView;

    if-eqz v1, :cond_6

    const v8, 0x7f08010e

    invoke-virtual {v1, v8}, Landroidx/appcompat/widget/AppCompatImageView;->setImageResource(I)V

    :cond_6
    const v1, 0x7f11017b

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f08010d

    invoke-virtual {v2, v0}, Landroid/view/View;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/miui/packageInstaller/UnknownSourceActivity;->B:Landroidx/appcompat/widget/LinearLayoutCompat;

    if-nez v0, :cond_3

    goto :goto_1

    :cond_7
    :goto_3
    if-nez v1, :cond_8

    goto :goto_5

    :cond_8
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v8

    if-ne v8, v4, :cond_9

    :goto_4
    move v1, v7

    goto :goto_7

    :cond_9
    :goto_5
    if-nez v1, :cond_a

    goto :goto_6

    :cond_a
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-nez v1, :cond_b

    goto :goto_4

    :cond_b
    :goto_6
    move v1, v6

    :goto_7
    if-eqz v1, :cond_14

    iget-object v1, p0, Lcom/miui/packageInstaller/UnknownSourceActivity;->B:Landroidx/appcompat/widget/LinearLayoutCompat;

    if-nez v1, :cond_c

    invoke-static {v5}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v1, v3

    :cond_c
    invoke-direct {p0, v1, v6}, Lcom/miui/packageInstaller/UnknownSourceActivity;->V0(Landroidx/appcompat/widget/LinearLayoutCompat;Z)V

    sget-object v1, Lm8/w;->a:Lm8/w;

    const v1, 0x7f110178

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v8, "getString(R.string.insta\u2026source_dialog_safe_title)"

    invoke-static {v1, v8}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    new-array v8, v7, [Ljava/lang/Object;

    iget-object v9, p0, Lcom/miui/packageInstaller/UnknownSourceActivity;->w:Lm5/e;

    if-eqz v9, :cond_d

    iget-object v9, v9, Lm5/e;->e:Ljava/lang/String;

    goto :goto_8

    :cond_d
    move-object v9, v3

    :goto_8
    aput-object v9, v8, v6

    invoke-static {v8, v7}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v8

    invoke-static {v1, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v8, "format(format, *args)"

    invoke-static {v1, v8}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/UnknownSourceActivity;->x:Lcom/miui/packageInstaller/model/InstallSourceTips;

    if-eqz v0, :cond_e

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/InstallSourceTips;->getWarningText()Ljava/lang/String;

    move-result-object v0

    goto :goto_9

    :cond_e
    move-object v0, v3

    :goto_9
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/miui/packageInstaller/UnknownSourceActivity;->z:Landroidx/appcompat/widget/AppCompatTextView;

    if-nez v0, :cond_f

    goto :goto_b

    :cond_f
    const v1, 0x7f110179

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_a
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_b

    :cond_10
    iget-object v0, p0, Lcom/miui/packageInstaller/UnknownSourceActivity;->z:Landroidx/appcompat/widget/AppCompatTextView;

    if-nez v0, :cond_11

    goto :goto_b

    :cond_11
    iget-object v1, p0, Lcom/miui/packageInstaller/UnknownSourceActivity;->x:Lcom/miui/packageInstaller/model/InstallSourceTips;

    if-eqz v1, :cond_12

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/InstallSourceTips;->getWarningText()Ljava/lang/String;

    move-result-object v1

    goto :goto_a

    :cond_12
    move-object v1, v3

    goto :goto_a

    :goto_b
    iget-object v0, p0, Lcom/miui/packageInstaller/UnknownSourceActivity;->y:Landroidx/appcompat/widget/AppCompatImageView;

    if-eqz v0, :cond_13

    const v1, 0x7f080112

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/AppCompatImageView;->setImageResource(I)V

    :cond_13
    const v0, 0x7f080111

    invoke-virtual {v2, v0}, Landroid/view/View;->setBackgroundResource(I)V

    :cond_14
    :goto_c
    iget-object v0, p0, Lcom/miui/packageInstaller/UnknownSourceActivity;->B:Landroidx/appcompat/widget/LinearLayoutCompat;

    if-nez v0, :cond_15

    invoke-static {v5}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v3

    :cond_15
    const v1, 0x7f0a00bb

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/miui/packageInstaller/UnknownSourceActivity;->u:Landroid/widget/CheckBox;

    iget-object v0, p0, Lcom/miui/packageInstaller/UnknownSourceActivity;->B:Landroidx/appcompat/widget/LinearLayoutCompat;

    if-nez v0, :cond_16

    invoke-static {v5}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v3

    :cond_16
    const v1, 0x7f0a009f

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iget-object v1, p0, Lcom/miui/packageInstaller/UnknownSourceActivity;->B:Landroidx/appcompat/widget/LinearLayoutCompat;

    if-nez v1, :cond_17

    invoke-static {v5}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v1, v3

    :cond_17
    const v2, 0x7f0a00a0

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iget-object v2, p0, Lcom/miui/packageInstaller/UnknownSourceActivity;->x:Lcom/miui/packageInstaller/model/InstallSourceTips;

    if-eqz v2, :cond_18

    invoke-virtual {v2}, Lcom/miui/packageInstaller/model/InstallSourceTips;->getType()I

    move-result v2

    if-ne v2, v4, :cond_18

    move v2, v7

    goto :goto_d

    :cond_18
    move v2, v6

    :goto_d
    if-nez v2, :cond_1a

    iget-object v2, p0, Lcom/miui/packageInstaller/UnknownSourceActivity;->x:Lcom/miui/packageInstaller/model/InstallSourceTips;

    if-eqz v2, :cond_19

    invoke-virtual {v2}, Lcom/miui/packageInstaller/model/InstallSourceTips;->getType()I

    move-result v2

    if-nez v2, :cond_19

    move v6, v7

    :cond_19
    if-eqz v6, :cond_1b

    :cond_1a
    const v2, 0x7f1103d4

    invoke-virtual {p0, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    const v2, 0x7f1103d5

    invoke-virtual {p0, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :cond_1b
    iget-object v2, p0, Lcom/miui/packageInstaller/UnknownSourceActivity;->x:Lcom/miui/packageInstaller/model/InstallSourceTips;

    if-eqz v2, :cond_1c

    invoke-virtual {v2}, Lcom/miui/packageInstaller/model/InstallSourceTips;->getButton()Ljava/lang/String;

    move-result-object v2

    goto :goto_e

    :cond_1c
    move-object v2, v3

    :goto_e
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1e

    iget-object v2, p0, Lcom/miui/packageInstaller/UnknownSourceActivity;->x:Lcom/miui/packageInstaller/model/InstallSourceTips;

    if-eqz v2, :cond_1d

    invoke-virtual {v2}, Lcom/miui/packageInstaller/model/InstallSourceTips;->getButton()Ljava/lang/String;

    move-result-object v3

    :cond_1d
    invoke-virtual {v1, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :cond_1e
    if-eqz v1, :cond_1f

    new-instance v2, Lm5/s1;

    invoke-direct {v2, p0}, Lm5/s1;-><init>(Lcom/miui/packageInstaller/UnknownSourceActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1f
    if-eqz v0, :cond_20

    new-instance v1, Lm5/r1;

    invoke-direct {v1, p0}, Lm5/r1;-><init>(Lcom/miui/packageInstaller/UnknownSourceActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_20
    return-void
.end method

.method private static final P0(Lcom/miui/packageInstaller/UnknownSourceActivity;Landroid/view/View;)V
    .locals 4

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/miui/packageInstaller/UnknownSourceActivity;->x:Lcom/miui/packageInstaller/model/InstallSourceTips;

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/miui/packageInstaller/model/InstallSourceTips;->getType()I

    move-result p1

    if-ne p1, v0, :cond_0

    move p1, v0

    goto :goto_0

    :cond_0
    move p1, v1

    :goto_0
    const-string v2, "button"

    if-nez p1, :cond_6

    iget-object p1, p0, Lcom/miui/packageInstaller/UnknownSourceActivity;->x:Lcom/miui/packageInstaller/model/InstallSourceTips;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/miui/packageInstaller/model/InstallSourceTips;->getType()I

    move-result p1

    const/4 v3, 0x2

    if-ne p1, v3, :cond_1

    move p1, v0

    goto :goto_1

    :cond_1
    move p1, v1

    :goto_1
    if-eqz p1, :cond_2

    goto :goto_4

    :cond_2
    new-instance p1, Lp5/b;

    const-string v3, "forbid_btn"

    invoke-direct {p1, v3, v2, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    iget-object v2, p0, Lcom/miui/packageInstaller/UnknownSourceActivity;->u:Landroid/widget/CheckBox;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    if-ne v2, v0, :cond_3

    move v2, v0

    goto :goto_2

    :cond_3
    move v2, v1

    :goto_2
    if-eqz v2, :cond_4

    const-string v2, "true"

    goto :goto_3

    :cond_4
    const-string v2, "false"

    :goto_3
    const-string v3, "is_remember"

    invoke-virtual {p1, v3, v2}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p1

    invoke-virtual {p1}, Lp5/f;->c()Z

    iget-object p1, p0, Lcom/miui/packageInstaller/UnknownSourceActivity;->u:Landroid/widget/CheckBox;

    if-eqz p1, :cond_5

    invoke-virtual {p1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result p1

    if-ne p1, v0, :cond_5

    goto :goto_5

    :cond_5
    move v0, v1

    goto :goto_5

    :cond_6
    :goto_4
    new-instance p1, Lp5/b;

    const-string v3, "forbid_install_btn"

    invoke-direct {p1, v3, v2, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p1}, Lp5/f;->c()Z

    :goto_5
    invoke-direct {p0, v1, v0}, Lcom/miui/packageInstaller/UnknownSourceActivity;->Y0(ZZ)V

    invoke-virtual {p0}, Lmiuix/appcompat/app/j;->finish()V

    return-void
.end method

.method private static final Q0(Lcom/miui/packageInstaller/UnknownSourceActivity;Landroid/view/View;)V
    .locals 4

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/miui/packageInstaller/UnknownSourceActivity;->x:Lcom/miui/packageInstaller/model/InstallSourceTips;

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/miui/packageInstaller/model/InstallSourceTips;->getType()I

    move-result p1

    if-ne p1, v0, :cond_0

    move p1, v0

    goto :goto_0

    :cond_0
    move p1, v1

    :goto_0
    const-string v2, "button"

    if-nez p1, :cond_6

    iget-object p1, p0, Lcom/miui/packageInstaller/UnknownSourceActivity;->x:Lcom/miui/packageInstaller/model/InstallSourceTips;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/miui/packageInstaller/model/InstallSourceTips;->getType()I

    move-result p1

    const/4 v3, 0x2

    if-ne p1, v3, :cond_1

    move p1, v0

    goto :goto_1

    :cond_1
    move p1, v1

    :goto_1
    if-eqz p1, :cond_2

    goto :goto_4

    :cond_2
    new-instance p1, Lp5/b;

    const-string v3, "permit_btn"

    invoke-direct {p1, v3, v2, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    iget-object v2, p0, Lcom/miui/packageInstaller/UnknownSourceActivity;->u:Landroid/widget/CheckBox;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    if-ne v2, v0, :cond_3

    move v2, v0

    goto :goto_2

    :cond_3
    move v2, v1

    :goto_2
    if-eqz v2, :cond_4

    const-string v2, "true"

    goto :goto_3

    :cond_4
    const-string v2, "false"

    :goto_3
    const-string v3, "is_remember"

    invoke-virtual {p1, v3, v2}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p1

    invoke-virtual {p1}, Lp5/f;->c()Z

    iget-object p1, p0, Lcom/miui/packageInstaller/UnknownSourceActivity;->u:Landroid/widget/CheckBox;

    if-eqz p1, :cond_5

    invoke-virtual {p1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result p1

    if-ne p1, v0, :cond_5

    move v1, v0

    :cond_5
    invoke-direct {p0, v0, v1}, Lcom/miui/packageInstaller/UnknownSourceActivity;->Y0(ZZ)V

    goto :goto_5

    :cond_6
    :goto_4
    new-instance p1, Lp5/b;

    const-string v0, "cancel_instal_btn"

    invoke-direct {p1, v0, v2, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p1}, Lp5/f;->c()Z

    invoke-direct {p0, v1, v1}, Lcom/miui/packageInstaller/UnknownSourceActivity;->Y0(ZZ)V

    :goto_5
    invoke-virtual {p0}, Lmiuix/appcompat/app/j;->finish()V

    return-void
.end method

.method private final R0()V
    .locals 2

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "currentApkInfo"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/miui/packageInstaller/model/ApkInfo;

    iput-object v0, p0, Lcom/miui/packageInstaller/UnknownSourceActivity;->v:Lcom/miui/packageInstaller/model/ApkInfo;

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "callerPackage"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lm5/e;

    iput-object v0, p0, Lcom/miui/packageInstaller/UnknownSourceActivity;->w:Lm5/e;

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "installTips"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/miui/packageInstaller/model/InstallSourceTips;

    :goto_0
    iput-object v0, p0, Lcom/miui/packageInstaller/UnknownSourceActivity;->x:Lcom/miui/packageInstaller/model/InstallSourceTips;

    return-void
.end method

.method private static final S0(Lcom/miui/packageInstaller/UnknownSourceActivity;Landroid/view/View;)V
    .locals 0

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p1, 0x0

    invoke-direct {p0, p1, p1}, Lcom/miui/packageInstaller/UnknownSourceActivity;->Y0(ZZ)V

    invoke-virtual {p0}, Lmiuix/appcompat/app/j;->finish()V

    return-void
.end method

.method private static final T0(Lcom/miui/packageInstaller/UnknownSourceActivity;Landroid/view/View;)V
    .locals 2

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Landroid/content/Intent;

    const-class v0, Lcom/android/packageinstaller/SettingsActivity;

    invoke-direct {p1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/UnknownSourceActivity;->v:Lcom/miui/packageInstaller/model/ApkInfo;

    const-string v1, "apk_info"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/miui/packageInstaller/UnknownSourceActivity;->w:Lm5/e;

    const-string v1, "caller"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    iget-object v0, p0, Lq2/b;->q:Lo5/b;

    const-string v1, "fromPage"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    invoke-virtual {p0, p1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private final U0(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    if-eqz p1, :cond_2

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const p2, 0x7f1103be

    invoke-virtual {p0, p2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object p2

    :cond_0
    move-object v2, p2

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p2

    if-eqz p2, :cond_1

    const p2, 0x7f110048

    invoke-virtual {p0, p2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object p3

    :cond_1
    move-object v3, p3

    const p2, 0x7f060032

    invoke-virtual {p0, p2}, Landroid/app/Activity;->getColor(I)I

    move-result v4

    invoke-virtual {p0, p2}, Landroid/app/Activity;->getColor(I)I

    move-result v5

    sget-object v0, Lf6/b0;->a:Lf6/b0$a;

    invoke-static {v2}, Lm8/i;->c(Ljava/lang/Object;)V

    invoke-static {v3}, Lm8/i;->c(Ljava/lang/Object;)V

    new-instance v6, Lcom/miui/packageInstaller/UnknownSourceActivity$b;

    invoke-direct {v6, p0}, Lcom/miui/packageInstaller/UnknownSourceActivity$b;-><init>(Lcom/miui/packageInstaller/UnknownSourceActivity;)V

    move-object v1, p1

    invoke-virtual/range {v0 .. v6}, Lf6/b0$a;->a(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;IILf6/b0$a$a;)V

    :cond_2
    return-void
.end method

.method private final V0(Landroidx/appcompat/widget/LinearLayoutCompat;Z)V
    .locals 3

    const/4 v0, 0x1

    const-string v1, "button"

    if-eqz p2, :cond_1

    invoke-virtual {p1, v0}, Landroidx/appcompat/widget/LinearLayoutCompat;->setOrientation(I)V

    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p2

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result v2

    if-eqz v2, :cond_0

    const v2, 0x7f0d01c9

    goto :goto_0

    :cond_0
    const v2, 0x7f0d01c8

    :goto_0
    invoke-virtual {p2, v2, p1, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    new-instance p1, Lp5/g;

    const-string p2, "forbid_install_btn"

    invoke-direct {p1, p2, v1, p0}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p1}, Lp5/f;->c()Z

    new-instance p1, Lp5/g;

    const-string p2, "cancel_instal_btn"

    invoke-direct {p1, p2, v1, p0}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p1}, Lp5/f;->c()Z

    new-instance p1, Lp5/g;

    const-string p2, "authorize_install_btn"

    invoke-direct {p1, p2, v1, p0}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    goto :goto_2

    :cond_1
    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroidx/appcompat/widget/LinearLayoutCompat;->setOrientation(I)V

    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p2

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result v2

    if-eqz v2, :cond_2

    const v2, 0x7f0d01c7

    goto :goto_1

    :cond_2
    const v2, 0x7f0d01c6

    :goto_1
    invoke-virtual {p2, v2, p1, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    new-instance p1, Lp5/g;

    const-string p2, "forbid_btn"

    invoke-direct {p1, p2, v1, p0}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p1}, Lp5/f;->c()Z

    new-instance p1, Lp5/g;

    const-string p2, "permit_btn"

    invoke-direct {p1, p2, v1, p0}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    :goto_2
    invoke-virtual {p1}, Lp5/f;->c()Z

    return-void
.end method

.method private final W0(Landroid/widget/FrameLayout;)V
    .locals 10

    iget-object v0, p0, Lcom/miui/packageInstaller/UnknownSourceActivity;->x:Lcom/miui/packageInstaller/model/InstallSourceTips;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/InstallSourceTips;->getType()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    const/4 v2, 0x2

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-nez v0, :cond_1

    goto :goto_2

    :cond_1
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-ne v5, v4, :cond_2

    :goto_1
    move v5, v4

    goto :goto_4

    :cond_2
    :goto_2
    if-nez v0, :cond_3

    goto :goto_3

    :cond_3
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-ne v5, v2, :cond_4

    goto :goto_1

    :cond_4
    :goto_3
    move v5, v3

    :goto_4
    const/16 v6, 0x8

    const v7, 0x7f0a007b

    const v8, 0x7f0a0073

    if-eqz v5, :cond_d

    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    const v5, 0x7f0d017f

    invoke-virtual {v0, v5, p1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    const v0, 0x7f0a03c2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/AppCompatTextView;

    const v5, 0x7f0a02cf

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroidx/appcompat/widget/AppCompatTextView;

    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroidx/appcompat/widget/AppCompatImageView;

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroidx/appcompat/widget/AppCompatTextView;

    iget-object v9, p0, Lcom/miui/packageInstaller/UnknownSourceActivity;->w:Lm5/e;

    if-eqz v9, :cond_5

    iget-object v9, v9, Lm5/e;->e:Ljava/lang/String;

    goto :goto_5

    :cond_5
    move-object v9, v1

    :goto_5
    invoke-virtual {v7, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v7, p0, Lcom/miui/packageInstaller/UnknownSourceActivity;->w:Lm5/e;

    if-eqz v7, :cond_6

    invoke-virtual {v7}, Lm5/e;->j()Ljava/lang/String;

    move-result-object v7

    goto :goto_6

    :cond_6
    move-object v7, v1

    :goto_6
    invoke-static {p0, v7}, Lj2/f;->b(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-virtual {v8, v7}, Landroidx/appcompat/widget/AppCompatImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v7, p0, Lcom/miui/packageInstaller/UnknownSourceActivity;->x:Lcom/miui/packageInstaller/model/InstallSourceTips;

    if-eqz v7, :cond_7

    invoke-virtual {v7}, Lcom/miui/packageInstaller/model/InstallSourceTips;->getWarningText()Ljava/lang/String;

    move-result-object v7

    goto :goto_7

    :cond_7
    move-object v7, v1

    :goto_7
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_9

    iget-object v7, p0, Lcom/miui/packageInstaller/UnknownSourceActivity;->x:Lcom/miui/packageInstaller/model/InstallSourceTips;

    if-eqz v7, :cond_8

    invoke-virtual {v7}, Lcom/miui/packageInstaller/model/InstallSourceTips;->getWarningText()Ljava/lang/String;

    move-result-object v1

    :cond_8
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_9
    iget-object v1, p0, Lcom/miui/packageInstaller/UnknownSourceActivity;->x:Lcom/miui/packageInstaller/model/InstallSourceTips;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/InstallSourceTips;->getType()I

    move-result v1

    if-ne v1, v4, :cond_a

    move v1, v4

    goto :goto_8

    :cond_a
    move v1, v3

    :goto_8
    if-eqz v1, :cond_b

    const v1, 0x7f060069

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    const v0, 0x7f08056b

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_e

    :cond_b
    iget-object v1, p0, Lcom/miui/packageInstaller/UnknownSourceActivity;->x:Lcom/miui/packageInstaller/model/InstallSourceTips;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/InstallSourceTips;->getType()I

    move-result v1

    if-ne v1, v2, :cond_c

    move v3, v4

    :cond_c
    if-eqz v3, :cond_15

    const v1, 0x7f080543

    invoke-virtual {p1, v1}, Landroid/view/View;->setBackgroundResource(I)V

    const p1, 0x7f060070

    invoke-virtual {p0, p1}, Landroid/app/Activity;->getColor(I)I

    move-result p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_e

    :cond_d
    const/4 v2, 0x3

    if-nez v0, :cond_e

    goto :goto_a

    :cond_e
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-ne v5, v2, :cond_f

    :goto_9
    move v3, v4

    goto :goto_b

    :cond_f
    :goto_a
    if-nez v0, :cond_10

    goto :goto_b

    :cond_10
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v0, :cond_11

    goto :goto_9

    :cond_11
    :goto_b
    if-eqz v3, :cond_15

    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result v2

    if-eqz v2, :cond_12

    const v2, 0x7f0d0067

    goto :goto_c

    :cond_12
    const v2, 0x7f0d0066

    :goto_c
    invoke-virtual {v0, v2, p1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/AppCompatImageView;

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroidx/appcompat/widget/AppCompatTextView;

    const v3, 0x7f0a00bb

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/CheckBox;

    invoke-virtual {p1, v6}, Landroid/widget/CheckBox;->setVisibility(I)V

    iget-object p1, p0, Lcom/miui/packageInstaller/UnknownSourceActivity;->w:Lm5/e;

    if-eqz p1, :cond_13

    invoke-virtual {p1}, Lm5/e;->j()Ljava/lang/String;

    move-result-object p1

    goto :goto_d

    :cond_13
    move-object p1, v1

    :goto_d
    invoke-static {p0, p1}, Lj2/f;->b(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroidx/appcompat/widget/AppCompatImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object p1, p0, Lcom/miui/packageInstaller/UnknownSourceActivity;->w:Lm5/e;

    if-eqz p1, :cond_14

    iget-object v1, p1, Lm5/e;->e:Ljava/lang/String;

    :cond_14
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_15
    :goto_e
    return-void
.end method

.method private final X0(Landroidx/appcompat/widget/AppCompatTextView;)V
    .locals 9

    iget-object v0, p0, Lcom/miui/packageInstaller/UnknownSourceActivity;->x:Lcom/miui/packageInstaller/model/InstallSourceTips;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/InstallSourceTips;->getInstallSourceAuthText()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f1103d7

    if-nez p1, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {p0, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    invoke-virtual {p0, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f110047

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcom/miui/packageInstaller/UnknownSourceActivity;->U0(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_8

    :cond_2
    iget-object v0, p0, Lcom/miui/packageInstaller/UnknownSourceActivity;->x:Lcom/miui/packageInstaller/model/InstallSourceTips;

    if-eqz v0, :cond_a

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/InstallSourceTips;->getInstallSourceAuthText()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_a

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/4 v2, 0x0

    move v3, v2

    :goto_2
    const/16 v4, 0x23

    const/4 v5, 0x1

    const/4 v6, -0x1

    if-ge v3, v0, :cond_5

    invoke-interface {v1, v3}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v7

    if-ne v7, v4, :cond_3

    move v7, v5

    goto :goto_3

    :cond_3
    move v7, v2

    :goto_3
    if-eqz v7, :cond_4

    goto :goto_4

    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_5
    move v3, v6

    :goto_4
    add-int/2addr v3, v5

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    add-int/2addr v0, v6

    if-ltz v0, :cond_9

    :goto_5
    add-int/lit8 v7, v0, -0x1

    invoke-interface {v1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v8

    if-ne v8, v4, :cond_6

    move v8, v5

    goto :goto_6

    :cond_6
    move v8, v2

    :goto_6
    if-eqz v8, :cond_7

    move v6, v0

    goto :goto_7

    :cond_7
    if-gez v7, :cond_8

    goto :goto_7

    :cond_8
    move v0, v7

    goto :goto_5

    :cond_9
    :goto_7
    invoke-virtual {v1, v3, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    const-string v2, "this as java.lang.String\u2026ing(startIndex, endIndex)"

    invoke-static {v0, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    const-string v2, "#"

    const-string v3, ""

    invoke-static/range {v1 .. v6}, Lu8/g;->r(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1, v1, v0}, Lcom/miui/packageInstaller/UnknownSourceActivity;->U0(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;)V

    :cond_a
    :goto_8
    return-void
.end method

.method private final Y0(ZZ)V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "allowButton"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string p1, "doNotShowAgain"

    invoke-virtual {v0, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/16 p1, 0x232c

    invoke-virtual {p0, p1, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    return-void
.end method

.method private final Z0()V
    .locals 6

    iget-object v0, p0, Lcom/miui/packageInstaller/UnknownSourceActivity;->y:Landroidx/appcompat/widget/AppCompatImageView;

    const-string v1, "null cannot be cast to non-null type androidx.constraintlayout.widget.ConstraintLayout.LayoutParams"

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-static {v0, v1}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroidx/constraintlayout/widget/ConstraintLayout$b;

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070145

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    :cond_0
    iget-object v0, p0, Lcom/miui/packageInstaller/UnknownSourceActivity;->z:Landroidx/appcompat/widget/AppCompatTextView;

    const v2, 0x7f07010e

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    invoke-static {v3, v1}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Landroidx/constraintlayout/widget/ConstraintLayout$b;

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    iput v4, v3, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-static {v0, v1}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroidx/constraintlayout/widget/ConstraintLayout$b;

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    iput v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    :cond_1
    iget-object v0, p0, Lcom/miui/packageInstaller/UnknownSourceActivity;->A:Landroidx/core/widget/NestedScrollView;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    invoke-static {v3, v1}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Landroidx/constraintlayout/widget/ConstraintLayout$b;

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f070109

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    iput v4, v3, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-static {v0, v1}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroidx/constraintlayout/widget/ConstraintLayout$b;

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    iput v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    :cond_2
    iget-object v0, p0, Lcom/miui/packageInstaller/UnknownSourceActivity;->B:Landroidx/appcompat/widget/LinearLayoutCompat;

    const/4 v3, 0x0

    const-string v4, "btnLayout"

    if-nez v0, :cond_3

    invoke-static {v4}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v3

    :cond_3
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-static {v0, v1}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroidx/constraintlayout/widget/ConstraintLayout$b;

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v5

    iput v5, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget-object v0, p0, Lcom/miui/packageInstaller/UnknownSourceActivity;->B:Landroidx/appcompat/widget/LinearLayoutCompat;

    if-nez v0, :cond_4

    invoke-static {v4}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    move-object v3, v0

    :goto_0
    invoke-virtual {v3}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-static {v0, v1}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroidx/constraintlayout/widget/ConstraintLayout$b;

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    return-void
.end method

.method private final a1()V
    .locals 6

    iget-object v0, p0, Lcom/miui/packageInstaller/UnknownSourceActivity;->y:Landroidx/appcompat/widget/AppCompatImageView;

    const-string v1, "null cannot be cast to non-null type androidx.constraintlayout.widget.ConstraintLayout.LayoutParams"

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-static {v0, v1}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroidx/constraintlayout/widget/ConstraintLayout$b;

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f07014b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    :cond_0
    iget-object v0, p0, Lcom/miui/packageInstaller/UnknownSourceActivity;->z:Landroidx/appcompat/widget/AppCompatTextView;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    invoke-static {v2, v1}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Landroidx/constraintlayout/widget/ConstraintLayout$b;

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0700be

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    iput v3, v2, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-static {v0, v1}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroidx/constraintlayout/widget/ConstraintLayout$b;

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    :cond_1
    iget-object v0, p0, Lcom/miui/packageInstaller/UnknownSourceActivity;->A:Landroidx/core/widget/NestedScrollView;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    invoke-static {v2, v1}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Landroidx/constraintlayout/widget/ConstraintLayout$b;

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0700b4

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    iput v3, v2, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-static {v0, v1}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroidx/constraintlayout/widget/ConstraintLayout$b;

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    :cond_2
    iget-object v0, p0, Lcom/miui/packageInstaller/UnknownSourceActivity;->B:Landroidx/appcompat/widget/LinearLayoutCompat;

    const/4 v2, 0x0

    const-string v3, "btnLayout"

    if-nez v0, :cond_3

    invoke-static {v3}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v2

    :cond_3
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-static {v0, v1}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroidx/constraintlayout/widget/ConstraintLayout$b;

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0700bc

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    iput v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget-object v0, p0, Lcom/miui/packageInstaller/UnknownSourceActivity;->B:Landroidx/appcompat/widget/LinearLayoutCompat;

    if-nez v0, :cond_4

    invoke-static {v3}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    move-object v2, v0

    :goto_0
    invoke-virtual {v2}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-static {v0, v1}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroidx/constraintlayout/widget/ConstraintLayout$b;

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    return-void
.end method


# virtual methods
.method public E0()Ljava/lang/String;
    .locals 1

    const-string v0, "safe_mode_install_warning"

    return-object v0
.end method

.method public onBackPressed()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0, v0}, Lcom/miui/packageInstaller/UnknownSourceActivity;->Y0(ZZ)V

    invoke-super {p0}, Lmiuix/appcompat/app/j;->onBackPressed()V

    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    const-string v0, "newConfig"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lq2/b;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    iget p1, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    invoke-direct {p0}, Lcom/miui/packageInstaller/UnknownSourceActivity;->Z0()V

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    invoke-direct {p0}, Lcom/miui/packageInstaller/UnknownSourceActivity;->a1()V

    :cond_1
    :goto_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lq2/b;->onCreate(Landroid/os/Bundle;)V

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result p1

    if-eqz p1, :cond_0

    const p1, 0x7f0d01c5

    goto :goto_0

    :cond_0
    const p1, 0x7f0d01c4

    :goto_0
    invoke-virtual {p0, p1}, Lmiuix/appcompat/app/j;->setContentView(I)V

    const p1, 0x7f0a008a

    invoke-virtual {p0, p1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    new-instance v0, Lm5/t1;

    invoke-direct {v0, p0}, Lm5/t1;-><init>(Lcom/miui/packageInstaller/UnknownSourceActivity;)V

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p1, 0x7f0a030c

    invoke-virtual {p0, p1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    new-instance v0, Lm5/q1;

    invoke-direct {v0, p0}, Lm5/q1;-><init>(Lcom/miui/packageInstaller/UnknownSourceActivity;)V

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/miui/packageInstaller/UnknownSourceActivity;->R0()V

    invoke-direct {p0}, Lcom/miui/packageInstaller/UnknownSourceActivity;->O0()V

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result p1

    if-eqz p1, :cond_2

    invoke-virtual {p0}, Landroid/app/Activity;->getBaseContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object p1

    iget p1, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    invoke-direct {p0}, Lcom/miui/packageInstaller/UnknownSourceActivity;->Z0()V

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Landroid/app/Activity;->getBaseContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object p1

    iget p1, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v0, 0x1

    if-ne p1, v0, :cond_2

    invoke-direct {p0}, Lcom/miui/packageInstaller/UnknownSourceActivity;->a1()V

    :cond_2
    :goto_1
    return-void
.end method
