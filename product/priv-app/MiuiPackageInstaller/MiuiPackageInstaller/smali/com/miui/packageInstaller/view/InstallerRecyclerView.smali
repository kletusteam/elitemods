.class public Lcom/miui/packageInstaller/view/InstallerRecyclerView;
.super Landroidx/recyclerview/widget/RecyclerView;


# instance fields
.field private L0:Lt5/b0;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroidx/recyclerview/widget/RecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0, p2}, Lcom/miui/packageInstaller/view/InstallerRecyclerView;->y1(Landroid/util/AttributeSet;)V

    return-void
.end method

.method private y1(Landroid/util/AttributeSet;)V
    .locals 1

    new-instance v0, Lt5/b0;

    invoke-direct {v0, p0}, Lt5/b0;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/miui/packageInstaller/view/InstallerRecyclerView;->L0:Lt5/b0;

    if-eqz p1, :cond_0

    invoke-virtual {v0, p1}, Lt5/b0;->a(Landroid/util/AttributeSet;)V

    iget-object p1, p0, Lcom/miui/packageInstaller/view/InstallerRecyclerView;->L0:Lt5/b0;

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    invoke-virtual {p1, v0}, Lt5/b0;->b(I)V

    :cond_0
    return-void
.end method


# virtual methods
.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/view/InstallerRecyclerView;->L0:Lt5/b0;

    iget p1, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-virtual {v0, p1}, Lt5/b0;->b(I)V

    return-void
.end method
