.class public final Lcom/miui/packageInstaller/ui/listcomponets/PermissionInfoHeaderViewObject$ViewHolder;
.super Landroidx/recyclerview/widget/RecyclerView$d0;


# instance fields
.field private tvPermissionCount:Landroid/widget/TextView;

.field private tvPermissionCountPrivacy:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    const-string v0, "itemView"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$d0;-><init>(Landroid/view/View;)V

    const v0, 0x7f0a0077

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "itemView.findViewById(R.id.app_permission_count)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/PermissionInfoHeaderViewObject$ViewHolder;->tvPermissionCount:Landroid/widget/TextView;

    const v0, 0x7f0a0078

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v0, "itemView.findViewById(R.\u2026permission_count_privacy)"

    invoke-static {p1, v0}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/PermissionInfoHeaderViewObject$ViewHolder;->tvPermissionCountPrivacy:Landroid/widget/TextView;

    return-void
.end method


# virtual methods
.method public final getTvPermissionCount()Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/PermissionInfoHeaderViewObject$ViewHolder;->tvPermissionCount:Landroid/widget/TextView;

    return-object v0
.end method

.method public final getTvPermissionCountPrivacy()Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/PermissionInfoHeaderViewObject$ViewHolder;->tvPermissionCountPrivacy:Landroid/widget/TextView;

    return-object v0
.end method

.method public final setTvPermissionCount(Landroid/widget/TextView;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/PermissionInfoHeaderViewObject$ViewHolder;->tvPermissionCount:Landroid/widget/TextView;

    return-void
.end method

.method public final setTvPermissionCountPrivacy(Landroid/widget/TextView;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/PermissionInfoHeaderViewObject$ViewHolder;->tvPermissionCountPrivacy:Landroid/widget/TextView;

    return-void
.end method
