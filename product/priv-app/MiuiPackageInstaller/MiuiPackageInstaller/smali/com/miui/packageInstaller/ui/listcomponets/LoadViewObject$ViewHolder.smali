.class public final Lcom/miui/packageInstaller/ui/listcomponets/LoadViewObject$ViewHolder;
.super Landroidx/recyclerview/widget/RecyclerView$d0;


# instance fields
.field private lottieImage:Lcom/airbnb/lottie/LottieAnimationView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    const-string v0, "itemView"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$d0;-><init>(Landroid/view/View;)V

    const v0, 0x7f0a020d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/airbnb/lottie/LottieAnimationView;

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/LoadViewObject$ViewHolder;->lottieImage:Lcom/airbnb/lottie/LottieAnimationView;

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/airbnb/lottie/LottieAnimationView;->setRepeatMode(I)V

    :goto_0
    iget-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/LoadViewObject$ViewHolder;->lottieImage:Lcom/airbnb/lottie/LottieAnimationView;

    if-nez p1, :cond_1

    goto :goto_1

    :cond_1
    const/4 v0, -0x1

    invoke-virtual {p1, v0}, Lcom/airbnb/lottie/LottieAnimationView;->setRepeatCount(I)V

    :goto_1
    iget-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/LoadViewObject$ViewHolder;->lottieImage:Lcom/airbnb/lottie/LottieAnimationView;

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/airbnb/lottie/LottieAnimationView;->p()V

    :cond_2
    return-void
.end method


# virtual methods
.method public final getLottieImage()Lcom/airbnb/lottie/LottieAnimationView;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/LoadViewObject$ViewHolder;->lottieImage:Lcom/airbnb/lottie/LottieAnimationView;

    return-object v0
.end method

.method public final setLottieImage(Lcom/airbnb/lottie/LottieAnimationView;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/LoadViewObject$ViewHolder;->lottieImage:Lcom/airbnb/lottie/LottieAnimationView;

    return-void
.end method
