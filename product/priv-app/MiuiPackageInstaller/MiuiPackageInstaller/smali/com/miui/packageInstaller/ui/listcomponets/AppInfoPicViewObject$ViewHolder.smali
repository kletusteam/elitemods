.class public final Lcom/miui/packageInstaller/ui/listcomponets/AppInfoPicViewObject$ViewHolder;
.super Landroidx/recyclerview/widget/RecyclerView$d0;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/packageInstaller/ui/listcomponets/AppInfoPicViewObject;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ViewHolder"
.end annotation


# instance fields
.field private appInfoPic:Landroidx/recyclerview/widget/RecyclerView;

.field private mAdapter:Lj6/b;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    const-string v0, "itemView"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$d0;-><init>(Landroid/view/View;)V

    const v0, 0x7f0a02be

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoPicViewObject$ViewHolder;->appInfoPic:Landroidx/recyclerview/widget/RecyclerView;

    new-instance v0, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-direct {v0, p1}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    const/4 p1, 0x0

    invoke-virtual {v0, p1}, Landroidx/recyclerview/widget/LinearLayoutManager;->F2(I)V

    iget-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoPicViewObject$ViewHolder;->appInfoPic:Landroidx/recyclerview/widget/RecyclerView;

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$o;)V

    :goto_0
    new-instance p1, Lj6/b;

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoPicViewObject$ViewHolder;->appInfoPic:Landroidx/recyclerview/widget/RecyclerView;

    invoke-direct {p1, v0}, Lj6/b;-><init>(Landroidx/recyclerview/widget/RecyclerView;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoPicViewObject$ViewHolder;->mAdapter:Lj6/b;

    return-void
.end method


# virtual methods
.method public final getAppInfoPic()Landroidx/recyclerview/widget/RecyclerView;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoPicViewObject$ViewHolder;->appInfoPic:Landroidx/recyclerview/widget/RecyclerView;

    return-object v0
.end method

.method public final getMAdapter()Lj6/b;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoPicViewObject$ViewHolder;->mAdapter:Lj6/b;

    return-object v0
.end method

.method public final setAppInfoPic(Landroidx/recyclerview/widget/RecyclerView;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoPicViewObject$ViewHolder;->appInfoPic:Landroidx/recyclerview/widget/RecyclerView;

    return-void
.end method

.method public final setMAdapter(Lj6/b;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoPicViewObject$ViewHolder;->mAdapter:Lj6/b;

    return-void
.end method
