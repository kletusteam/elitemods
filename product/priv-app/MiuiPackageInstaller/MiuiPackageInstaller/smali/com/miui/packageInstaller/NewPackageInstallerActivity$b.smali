.class final Lcom/miui/packageInstaller/NewPackageInstallerActivity$b;
.super Lf8/k;

# interfaces
.implements Ll8/p;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/packageInstaller/NewPackageInstallerActivity;->F1()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf8/k;",
        "Ll8/p<",
        "Lv8/e0;",
        "Ld8/d<",
        "-",
        "La8/v;",
        ">;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation runtime Lf8/f;
    c = "com.miui.packageInstaller.NewPackageInstallerActivity$initView$1"
    f = "NewPackageInstallerActivity.kt"
    l = {
        0x7b,
        0x7c
    }
    m = "invokeSuspend"
.end annotation


# instance fields
.field e:Z

.field f:I

.field private synthetic g:Ljava/lang/Object;

.field final synthetic h:Lcom/miui/packageInstaller/NewPackageInstallerActivity;


# direct methods
.method constructor <init>(Lcom/miui/packageInstaller/NewPackageInstallerActivity;Ld8/d;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/miui/packageInstaller/NewPackageInstallerActivity;",
            "Ld8/d<",
            "-",
            "Lcom/miui/packageInstaller/NewPackageInstallerActivity$b;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/miui/packageInstaller/NewPackageInstallerActivity$b;->h:Lcom/miui/packageInstaller/NewPackageInstallerActivity;

    const/4 p1, 0x2

    invoke-direct {p0, p1, p2}, Lf8/k;-><init>(ILd8/d;)V

    return-void
.end method


# virtual methods
.method public final b(Ljava/lang/Object;Ld8/d;)Ld8/d;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ld8/d<",
            "*>;)",
            "Ld8/d<",
            "La8/v;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/miui/packageInstaller/NewPackageInstallerActivity$b;

    iget-object v1, p0, Lcom/miui/packageInstaller/NewPackageInstallerActivity$b;->h:Lcom/miui/packageInstaller/NewPackageInstallerActivity;

    invoke-direct {v0, v1, p2}, Lcom/miui/packageInstaller/NewPackageInstallerActivity$b;-><init>(Lcom/miui/packageInstaller/NewPackageInstallerActivity;Ld8/d;)V

    iput-object p1, v0, Lcom/miui/packageInstaller/NewPackageInstallerActivity$b;->g:Ljava/lang/Object;

    return-object v0
.end method

.method public bridge synthetic g(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lv8/e0;

    check-cast p2, Ld8/d;

    invoke-virtual {p0, p1, p2}, Lcom/miui/packageInstaller/NewPackageInstallerActivity$b;->q(Lv8/e0;Ld8/d;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final n(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 12

    invoke-static {}, Le8/b;->c()Ljava/lang/Object;

    move-result-object v0

    iget v1, p0, Lcom/miui/packageInstaller/NewPackageInstallerActivity$b;->f:I

    const/4 v2, 0x2

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-eqz v1, :cond_2

    if-eq v1, v3, :cond_1

    if-ne v1, v2, :cond_0

    iget-boolean v0, p0, Lcom/miui/packageInstaller/NewPackageInstallerActivity$b;->e:Z

    invoke-static {p1}, La8/n;->b(Ljava/lang/Object;)V

    goto :goto_1

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    iget-object v1, p0, Lcom/miui/packageInstaller/NewPackageInstallerActivity$b;->g:Ljava/lang/Object;

    check-cast v1, Lv8/l0;

    invoke-static {p1}, La8/n;->b(Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    invoke-static {p1}, La8/n;->b(Ljava/lang/Object;)V

    iget-object p1, p0, Lcom/miui/packageInstaller/NewPackageInstallerActivity$b;->g:Ljava/lang/Object;

    check-cast p1, Lv8/e0;

    invoke-static {}, Lv8/t0;->b()Lv8/a0;

    move-result-object v6

    const/4 v7, 0x0

    new-instance v8, Lcom/miui/packageInstaller/NewPackageInstallerActivity$b$b;

    invoke-direct {v8, v4}, Lcom/miui/packageInstaller/NewPackageInstallerActivity$b$b;-><init>(Ld8/d;)V

    const/4 v9, 0x2

    const/4 v10, 0x0

    move-object v5, p1

    invoke-static/range {v5 .. v10}, Lv8/f;->b(Lv8/e0;Ld8/g;Lv8/g0;Ll8/p;ILjava/lang/Object;)Lv8/l0;

    move-result-object v1

    invoke-static {}, Lv8/t0;->b()Lv8/a0;

    move-result-object v6

    new-instance v8, Lcom/miui/packageInstaller/NewPackageInstallerActivity$b$a;

    invoke-direct {v8, v4}, Lcom/miui/packageInstaller/NewPackageInstallerActivity$b$a;-><init>(Ld8/d;)V

    invoke-static/range {v5 .. v10}, Lv8/f;->b(Lv8/e0;Ld8/g;Lv8/g0;Ll8/p;ILjava/lang/Object;)Lv8/l0;

    move-result-object p1

    iput-object p1, p0, Lcom/miui/packageInstaller/NewPackageInstallerActivity$b;->g:Ljava/lang/Object;

    iput v3, p0, Lcom/miui/packageInstaller/NewPackageInstallerActivity$b;->f:I

    invoke-interface {v1, p0}, Lv8/l0;->z(Ld8/d;)Ljava/lang/Object;

    move-result-object v1

    if-ne v1, v0, :cond_3

    return-object v0

    :cond_3
    move-object v11, v1

    move-object v1, p1

    move-object p1, v11

    :goto_0
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    iput-object v4, p0, Lcom/miui/packageInstaller/NewPackageInstallerActivity$b;->g:Ljava/lang/Object;

    iput-boolean p1, p0, Lcom/miui/packageInstaller/NewPackageInstallerActivity$b;->e:Z

    iput v2, p0, Lcom/miui/packageInstaller/NewPackageInstallerActivity$b;->f:I

    invoke-interface {v1, p0}, Lv8/l0;->z(Ld8/d;)Ljava/lang/Object;

    move-result-object v1

    if-ne v1, v0, :cond_4

    return-object v0

    :cond_4
    move v0, p1

    move-object p1, v1

    :goto_1
    check-cast p1, Lcom/miui/packageInstaller/model/GuideOpenSafeModePopTips;

    if-eqz v0, :cond_5

    if-eqz p1, :cond_5

    iget-object p1, p0, Lcom/miui/packageInstaller/NewPackageInstallerActivity$b;->h:Lcom/miui/packageInstaller/NewPackageInstallerActivity;

    invoke-static {p1}, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->y2(Lcom/miui/packageInstaller/NewPackageInstallerActivity;)V

    goto :goto_2

    :cond_5
    iget-object p1, p0, Lcom/miui/packageInstaller/NewPackageInstallerActivity$b;->h:Lcom/miui/packageInstaller/NewPackageInstallerActivity;

    invoke-virtual {p1}, Lm5/z0;->P1()V

    :goto_2
    sget-object p1, La8/v;->a:La8/v;

    return-object p1
.end method

.method public final q(Lv8/e0;Ld8/d;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lv8/e0;",
            "Ld8/d<",
            "-",
            "La8/v;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/miui/packageInstaller/NewPackageInstallerActivity$b;->b(Ljava/lang/Object;Ld8/d;)Ld8/d;

    move-result-object p1

    check-cast p1, Lcom/miui/packageInstaller/NewPackageInstallerActivity$b;

    sget-object p2, La8/v;->a:La8/v;

    invoke-virtual {p1, p2}, Lcom/miui/packageInstaller/NewPackageInstallerActivity$b;->n(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method
