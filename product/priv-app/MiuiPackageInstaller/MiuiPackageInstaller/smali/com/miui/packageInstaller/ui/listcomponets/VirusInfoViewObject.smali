.class public final Lcom/miui/packageInstaller/ui/listcomponets/VirusInfoViewObject;
.super Lm6/a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/packageInstaller/ui/listcomponets/VirusInfoViewObject$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lm6/a<",
        "Lcom/miui/packageInstaller/ui/listcomponets/VirusInfoViewObject$ViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private l:Lcom/miui/packageInstaller/model/Virus;

.field private m:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/Virus;Ll6/c;Lm6/b;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/miui/packageInstaller/model/WarningCardInfo;

    invoke-direct {v0}, Lcom/miui/packageInstaller/model/WarningCardInfo;-><init>()V

    invoke-direct {p0, p1, v0, p3, p4}, Lm6/a;-><init>(Landroid/content/Context;Ljava/lang/Object;Ll6/c;Lm6/b;)V

    iput-object p2, p0, Lcom/miui/packageInstaller/ui/listcomponets/VirusInfoViewObject;->l:Lcom/miui/packageInstaller/model/Virus;

    invoke-static {p1}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->isSafeModelEnable(Landroid/content/Context;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/VirusInfoViewObject;->m:Z

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/Virus;Ll6/c;Lm6/b;ILm8/g;)V
    .locals 1

    and-int/lit8 p6, p5, 0x2

    const/4 v0, 0x0

    if-eqz p6, :cond_0

    move-object p2, v0

    :cond_0
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_1

    move-object p3, v0

    :cond_1
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_2

    move-object p4, v0

    :cond_2
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/miui/packageInstaller/ui/listcomponets/VirusInfoViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/Virus;Ll6/c;Lm6/b;)V

    return-void
.end method


# virtual methods
.method public final A(Lcom/miui/packageInstaller/model/Virus;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/VirusInfoViewObject;->l:Lcom/miui/packageInstaller/model/Virus;

    return-void
.end method

.method public k()I
    .locals 1

    const v0, 0x7f0d00a0

    return v0
.end method

.method public bridge synthetic o(Landroidx/recyclerview/widget/RecyclerView$d0;)V
    .locals 0

    check-cast p1, Lcom/miui/packageInstaller/ui/listcomponets/VirusInfoViewObject$ViewHolder;

    invoke-virtual {p0, p1}, Lcom/miui/packageInstaller/ui/listcomponets/VirusInfoViewObject;->z(Lcom/miui/packageInstaller/ui/listcomponets/VirusInfoViewObject$ViewHolder;)V

    return-void
.end method

.method public z(Lcom/miui/packageInstaller/ui/listcomponets/VirusInfoViewObject$ViewHolder;)V
    .locals 8

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/VirusInfoViewObject;->l:Lcom/miui/packageInstaller/model/Virus;

    const/4 v1, 0x0

    const/4 v2, 0x0

    if-nez v0, :cond_4

    if-eqz p1, :cond_0

    iget-object v0, p1, Landroidx/recyclerview/widget/RecyclerView$d0;->itemView:Landroid/view/View;

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    if-nez v0, :cond_1

    goto :goto_1

    :cond_1
    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    :goto_1
    if-eqz p1, :cond_2

    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView$d0;->itemView:Landroid/view/View;

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    :cond_2
    if-nez v1, :cond_3

    goto/16 :goto_e

    :cond_3
    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto/16 :goto_e

    :cond_4
    if-eqz p1, :cond_5

    iget-object v0, p1, Landroidx/recyclerview/widget/RecyclerView$d0;->itemView:Landroid/view/View;

    if-eqz v0, :cond_5

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    const/4 v3, -0x2

    iput v3, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    :cond_5
    if-eqz p1, :cond_6

    iget-object v0, p1, Landroidx/recyclerview/widget/RecyclerView$d0;->itemView:Landroid/view/View;

    goto :goto_2

    :cond_6
    move-object v0, v1

    :goto_2
    if-nez v0, :cond_7

    goto :goto_3

    :cond_7
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :goto_3
    if-eqz p1, :cond_8

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/VirusInfoViewObject$ViewHolder;->getIcon()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_8

    const v3, 0x7f0801b4

    invoke-virtual {v0, v3}, Landroid/view/View;->setBackgroundResource(I)V

    :cond_8
    if-eqz p1, :cond_9

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/VirusInfoViewObject$ViewHolder;->getTvVirusName()Landroid/widget/TextView;

    move-result-object v0

    goto :goto_4

    :cond_9
    move-object v0, v1

    :goto_4
    const/4 v3, 0x1

    if-nez v0, :cond_a

    goto :goto_6

    :cond_a
    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f1103f1

    new-array v6, v3, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/miui/packageInstaller/ui/listcomponets/VirusInfoViewObject;->l:Lcom/miui/packageInstaller/model/Virus;

    if-eqz v7, :cond_b

    iget-object v7, v7, Lcom/miui/packageInstaller/model/Virus;->name:Ljava/lang/String;

    goto :goto_5

    :cond_b
    move-object v7, v1

    :goto_5
    aput-object v7, v6, v2

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_6
    iget-boolean v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/VirusInfoViewObject;->m:Z

    if-eqz v0, :cond_10

    if-eqz p1, :cond_c

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/VirusInfoViewObject$ViewHolder;->getTvVirusInfo()Landroid/widget/TextView;

    move-result-object v0

    goto :goto_7

    :cond_c
    move-object v0, v1

    :goto_7
    if-nez v0, :cond_d

    goto :goto_b

    :cond_d
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/miui/packageInstaller/ui/listcomponets/VirusInfoViewObject;->l:Lcom/miui/packageInstaller/model/Virus;

    if-eqz v5, :cond_e

    iget-object v1, v5, Lcom/miui/packageInstaller/model/Virus;->virusInfo:Ljava/lang/String;

    :cond_e
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v1

    invoke-static {}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->getSecureSettingLocation()Ljava/lang/String;

    move-result-object v5

    const-string v6, "settings"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_f

    const v5, 0x7f1103ea

    goto :goto_8

    :cond_f
    const v5, 0x7f1103eb

    :goto_8
    invoke-virtual {v1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_a

    :cond_10
    if-eqz p1, :cond_11

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/VirusInfoViewObject$ViewHolder;->getTvVirusInfo()Landroid/widget/TextView;

    move-result-object v0

    goto :goto_9

    :cond_11
    move-object v0, v1

    :goto_9
    if-nez v0, :cond_12

    goto :goto_b

    :cond_12
    iget-object v4, p0, Lcom/miui/packageInstaller/ui/listcomponets/VirusInfoViewObject;->l:Lcom/miui/packageInstaller/model/Virus;

    if-eqz v4, :cond_13

    iget-object v1, v4, Lcom/miui/packageInstaller/model/Virus;->virusInfo:Ljava/lang/String;

    :cond_13
    :goto_a
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_b
    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result v0

    const v1, 0x7f070145

    if-eqz v0, :cond_15

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v3, :cond_14

    if-eqz p1, :cond_17

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/VirusInfoViewObject$ViewHolder;->getLayoutRoot()Landroid/widget/FrameLayout;

    move-result-object p1

    if-eqz p1, :cond_17

    :goto_c
    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    goto :goto_d

    :cond_14
    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x2

    if-ne v0, v3, :cond_17

    if-eqz p1, :cond_17

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/VirusInfoViewObject$ViewHolder;->getLayoutRoot()Landroid/widget/FrameLayout;

    move-result-object p1

    if-eqz p1, :cond_17

    goto :goto_c

    :cond_15
    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/packageinstaller/compat/SettingsCompat;->isIntentSafe(Landroid/content/Context;)Z

    move-result v0

    const v3, 0x7f0700ee

    if-eqz v0, :cond_16

    if-eqz p1, :cond_17

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/VirusInfoViewObject$ViewHolder;->getLayoutRoot()Landroid/widget/FrameLayout;

    move-result-object p1

    if-eqz p1, :cond_17

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    :goto_d
    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    invoke-virtual {p1, v2, v0, v2, v1}, Landroid/widget/FrameLayout;->setPadding(IIII)V

    goto :goto_e

    :cond_16
    if-eqz p1, :cond_17

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/VirusInfoViewObject$ViewHolder;->getLayoutRoot()Landroid/widget/FrameLayout;

    move-result-object p1

    if-eqz p1, :cond_17

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0700ce

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    invoke-virtual {p1, v0, v2, v3, v1}, Landroid/widget/FrameLayout;->setPadding(IIII)V

    :cond_17
    :goto_e
    return-void
.end method
