.class public Lcom/miui/packageInstaller/NewPackageInstallerActivity;
.super Lm5/z0;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/packageInstaller/NewPackageInstallerActivity$a;
    }
.end annotation


# instance fields
.field private final V:Ljava/lang/String;

.field private W:Landroidx/recyclerview/widget/RecyclerView;

.field private X:Landroidx/recyclerview/widget/RecyclerView;

.field private Y:Lj6/b;

.field private Z:Lj6/b;

.field private e0:La6/g;

.field private f0:Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;

.field private g0:Landroid/view/View;

.field private h0:Lcom/airbnb/lottie/LottieAnimationView;

.field private i0:Landroid/widget/LinearLayout;

.field private j0:Landroid/widget/TextView;

.field private k0:Lcom/miui/packageInstaller/NewPackageInstallerActivity$a;

.field private l0:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lm5/z0;-><init>()V

    const-string v0, "NewPI"

    iput-object v0, p0, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->V:Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->l0:Z

    return-void
.end method

.method private static final C2(Lcom/miui/packageInstaller/NewPackageInstallerActivity;Landroid/content/Context;ILjava/lang/Object;Lm6/a;)V
    .locals 0

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->G2()V

    return-void
.end method

.method private static final D2(Lcom/miui/packageInstaller/NewPackageInstallerActivity;Landroid/content/Context;ILjava/lang/Object;Lm6/a;)V
    .locals 0

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    instance-of p1, p3, Ljava/lang/Integer;

    if-eqz p1, :cond_0

    check-cast p3, Ljava/lang/Number;

    invoke-virtual {p3}, Ljava/lang/Number;->intValue()I

    move-result p1

    invoke-virtual {p0, p1}, Lm5/z0;->N1(I)V

    :cond_0
    return-void
.end method

.method private final E2()Z
    .locals 4

    invoke-virtual {p0}, Lm5/z0;->x1()Lcom/miui/packageInstaller/model/Virus;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-boolean v0, v0, Lcom/miui/packageInstaller/model/CloudParams;->riskApp:Z

    if-ne v0, v2, :cond_0

    move v0, v2

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    move v0, v1

    goto :goto_2

    :cond_2
    :goto_1
    move v0, v2

    :goto_2
    invoke-static {p0}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->isSafeModelEnable(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_4

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, v0, Lcom/miui/packageInstaller/model/CloudParams;->riskAppProtectionGuardTip:Lcom/miui/packageInstaller/model/RiskAppProtectionGuardTip;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/RiskAppProtectionGuardTip;->getRiskAppProtectionGuard()Z

    move-result v0

    if-ne v0, v2, :cond_3

    move v0, v2

    goto :goto_3

    :cond_3
    move v0, v1

    :goto_3
    if-eqz v0, :cond_4

    move v1, v2

    :cond_4
    return v1
.end method

.method private final F2()V
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ObjectAnimatorBinding"
        }
    .end annotation

    iget-object v0, p0, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->g0:Landroid/view/View;

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    const-string v2, "alpha"

    invoke-static {v0, v2, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    const-wide/16 v1, 0x12c

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    return-void

    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method private final G2()V
    .locals 3

    iget-object v0, p0, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->V:Ljava/lang/String;

    const-string v1, "reload"

    invoke-static {v0, v1}, Lf6/o;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    iget-boolean v0, p0, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->l0:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->l0:Z

    invoke-virtual {p0, v0}, Lm5/z0;->e2(Z)V

    invoke-direct {p0}, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->H2()V

    invoke-direct {p0}, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->J2()V

    iget-object v0, p0, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->Z:Lj6/b;

    const/4 v1, 0x0

    if-nez v0, :cond_1

    const-string v0, "mWarningAdapter"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_1
    invoke-virtual {v0, v1}, Lj6/b;->j0(Ljava/util/List;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->f0:Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;

    if-nez v0, :cond_2

    goto :goto_0

    :cond_2
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :goto_0
    invoke-virtual {p0}, Lm5/z0;->q1()Lcom/miui/packageInstaller/model/ApkInfo;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lm5/z0;->w1()Lw5/a;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lm5/z0;->z1()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {p0, v2}, Lm5/z0;->h2(I)V

    invoke-virtual {p0}, Lm5/z0;->z1()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Lw5/a;->l(Lcom/miui/packageInstaller/model/ApkInfo;I)V

    :cond_3
    return-void
.end method

.method private final H2()V
    .locals 3

    iget-object v0, p0, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->Y:Lj6/b;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const-string v0, "mAdapter"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_0
    invoke-virtual {v0}, Lj6/b;->R()Ljava/util/List;

    move-result-object v0

    const-string v2, "mAdapter.list"

    invoke-static {v0, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->Z:Lj6/b;

    if-nez v2, :cond_1

    const-string v2, "mWarningAdapter"

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move-object v1, v2

    :goto_0
    invoke-virtual {v1}, Lj6/b;->R()Ljava/util/List;

    move-result-object v1

    const-string v2, "mWarningAdapter.list"

    invoke-static {v1, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, v1}, Lb8/j;->y(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lm6/a;

    instance-of v2, v1, Lcom/miui/packageInstaller/ui/listcomponets/NetworkErrorObject;

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Lm6/a;->t()V

    :cond_3
    return-void
.end method

.method private final J2()V
    .locals 2

    iget-object v0, p0, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->g0:Landroid/view/View;

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    :goto_0
    iget-object v0, p0, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->g0:Landroid/view/View;

    const/4 v1, 0x0

    if-nez v0, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_1
    iget-object v0, p0, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->i0:Landroid/widget/LinearLayout;

    if-nez v0, :cond_2

    goto :goto_2

    :cond_2
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :goto_2
    iget-object v0, p0, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->h0:Lcom/airbnb/lottie/LottieAnimationView;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/airbnb/lottie/LottieAnimationView;->p()V

    :cond_3
    return-void
.end method

.method private final K2()V
    .locals 3

    invoke-virtual {p0}, Landroid/app/Activity;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    sget-object v0, La6/l;->n:La6/l$a;

    const/4 v1, 0x0

    new-instance v2, Lcom/miui/packageInstaller/NewPackageInstallerActivity$d;

    invoke-direct {v2, p0}, Lcom/miui/packageInstaller/NewPackageInstallerActivity$d;-><init>(Lcom/miui/packageInstaller/NewPackageInstallerActivity;)V

    invoke-virtual {v0, p0, v1, v2}, La6/l$a;->a(Landroid/content/Context;ZLl8/a;)V

    return-void
.end method

.method public static synthetic v2(Lcom/miui/packageInstaller/NewPackageInstallerActivity;Landroid/content/Context;ILjava/lang/Object;Lm6/a;)V
    .locals 0

    invoke-static {p0, p1, p2, p3, p4}, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->C2(Lcom/miui/packageInstaller/NewPackageInstallerActivity;Landroid/content/Context;ILjava/lang/Object;Lm6/a;)V

    return-void
.end method

.method public static synthetic w2(Lcom/miui/packageInstaller/NewPackageInstallerActivity;Landroid/content/Context;ILjava/lang/Object;Lm6/a;)V
    .locals 0

    invoke-static {p0, p1, p2, p3, p4}, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->D2(Lcom/miui/packageInstaller/NewPackageInstallerActivity;Landroid/content/Context;ILjava/lang/Object;Lm6/a;)V

    return-void
.end method

.method public static final synthetic x2(Lcom/miui/packageInstaller/NewPackageInstallerActivity;)Lo5/b;
    .locals 0

    iget-object p0, p0, Lq2/b;->q:Lo5/b;

    return-object p0
.end method

.method public static final synthetic y2(Lcom/miui/packageInstaller/NewPackageInstallerActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->K2()V

    return-void
.end method

.method private final z2()V
    .locals 2

    iget-object v0, p0, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->h0:Lcom/airbnb/lottie/LottieAnimationView;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/airbnb/lottie/LottieAnimationView;->h()V

    :cond_0
    iget-object v0, p0, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->i0:Landroid/widget/LinearLayout;

    if-nez v0, :cond_1

    goto :goto_0

    :cond_1
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :goto_0
    return-void
.end method


# virtual methods
.method public final A2()Z
    .locals 2

    iget-object v0, p0, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->Y:Lj6/b;

    if-nez v0, :cond_0

    const-string v0, "mAdapter"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v0, 0x0

    :cond_0
    invoke-virtual {v0}, Lj6/b;->R()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lm6/a;

    instance-of v1, v1, Lcom/miui/packageInstaller/ui/listcomponets/AdTitleViewObject;

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    return v0

    :cond_2
    const/4 v0, 0x0

    return v0
.end method

.method public final B2()V
    .locals 5

    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-boolean v0, v0, Lcom/miui/packageInstaller/model/CloudParams;->allowHighLight:Z

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_0
    invoke-virtual {p0, v0}, Lm5/z0;->c2(Z)V

    iget-object v0, p0, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->f0:Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;

    if-eqz v0, :cond_7

    invoke-virtual {p0, v0}, Lm5/z0;->O1(Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;)V

    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v2

    const/4 v3, 0x0

    if-eqz v2, :cond_1

    iget-object v2, v2, Lcom/miui/packageInstaller/model/CloudParams;->categoryAbbreviation:Ljava/lang/String;

    goto :goto_1

    :cond_1
    move-object v2, v3

    :goto_1
    const-string v4, "500_error"

    invoke-static {v4, v2}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    return-void

    :cond_2
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v2

    const/4 v4, 0x1

    if-eqz v2, :cond_3

    iget-boolean v2, v2, Lcom/miui/packageInstaller/model/CloudParams;->useSystemAppRules:Z

    if-ne v2, v4, :cond_3

    move v2, v4

    goto :goto_2

    :cond_3
    move v2, v1

    :goto_2
    if-eqz v2, :cond_4

    invoke-virtual {p0, v0}, Lm5/z0;->u2(Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;)V

    goto :goto_4

    :cond_4
    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v2

    if-eqz v2, :cond_5

    iget-boolean v2, v2, Lcom/miui/packageInstaller/model/CloudParams;->installNotAllow:Z

    if-ne v2, v4, :cond_5

    goto :goto_3

    :cond_5
    move v4, v1

    :goto_3
    if-eqz v4, :cond_6

    invoke-virtual {p0, v0, v3}, Lm5/z0;->J1(Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;)V

    goto :goto_4

    :cond_6
    invoke-virtual {p0, v0, v3, v1}, Lm5/z0;->B1(Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;Z)Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;

    :cond_7
    :goto_4
    return-void
.end method

.method public D1()V
    .locals 3

    invoke-super {p0}, Lm5/z0;->D1()V

    invoke-virtual {p0}, Lm5/z0;->k1()Ll6/d;

    move-result-object v0

    new-instance v1, Lm5/c1;

    invoke-direct {v1, p0}, Lm5/c1;-><init>(Lcom/miui/packageInstaller/NewPackageInstallerActivity;)V

    const v2, 0x7f0a02c5

    invoke-virtual {v0, v2, v1}, Ll6/d;->b(ILl6/e;)V

    invoke-virtual {p0}, Lm5/z0;->k1()Ll6/d;

    move-result-object v0

    new-instance v1, Lm5/d1;

    invoke-direct {v1, p0}, Lm5/d1;-><init>(Lcom/miui/packageInstaller/NewPackageInstallerActivity;)V

    const v2, 0x7f0a02a6

    invoke-virtual {v0, v2, v1}, Ll6/d;->b(ILl6/e;)V

    return-void
.end method

.method public F1()V
    .locals 9

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0d0025

    goto :goto_0

    :cond_0
    const v0, 0x7f0d0024

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/android/packageinstaller/utils/g;->v()Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f0d0023

    goto :goto_0

    :cond_2
    const v0, 0x7f0d0022

    :goto_0
    invoke-virtual {p0, v0}, Lmiuix/appcompat/app/j;->setContentView(I)V

    invoke-super {p0}, Lm5/z0;->F1()V

    const v0, 0x7f0a02d1

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-static {p0}, Lcom/android/packageinstaller/utils/u;->b(Landroid/app/Activity;)I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1, v2, v2}, Landroid/view/ViewGroup;->setPadding(IIII)V

    const v0, 0x7f0a0204

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->i0:Landroid/widget/LinearLayout;

    const v0, 0x7f0a0205

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->j0:Landroid/widget/TextView;

    const v0, 0x7f0a0216

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.main_content)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    iput-object v0, p0, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->W:Landroidx/recyclerview/widget/RecyclerView;

    const v0, 0x7f0a0206

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->g0:Landroid/view/View;

    const/4 v1, 0x0

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_1

    :cond_3
    move-object v0, v1

    :goto_1
    const/4 v3, 0x1

    if-nez v0, :cond_4

    goto :goto_2

    :cond_4
    invoke-virtual {v0, v3}, Landroid/graphics/drawable/Drawable;->setAutoMirrored(Z)V

    :goto_2
    invoke-static {}, Lcom/android/packageinstaller/utils/g;->v()Z

    move-result v0

    if-nez v0, :cond_5

    const v0, 0x7f0a020d

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/airbnb/lottie/LottieAnimationView;

    iput-object v0, p0, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->h0:Lcom/airbnb/lottie/LottieAnimationView;

    :cond_5
    invoke-static {p0}, Lcom/android/packageinstaller/utils/g;->t(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->h0:Lcom/airbnb/lottie/LottieAnimationView;

    if-eqz v0, :cond_7

    const-string v4, "dark_loading.json"

    goto :goto_3

    :cond_6
    iget-object v0, p0, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->h0:Lcom/airbnb/lottie/LottieAnimationView;

    if-eqz v0, :cond_7

    const-string v4, "loading.json"

    :goto_3
    invoke-virtual {v0, v4}, Lcom/airbnb/lottie/LottieAnimationView;->setAnimation(Ljava/lang/String;)V

    :cond_7
    iget-object v0, p0, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->h0:Lcom/airbnb/lottie/LottieAnimationView;

    if-nez v0, :cond_8

    goto :goto_4

    :cond_8
    invoke-virtual {v0, v3}, Lcom/airbnb/lottie/LottieAnimationView;->setRepeatMode(I)V

    :goto_4
    iget-object v0, p0, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->h0:Lcom/airbnb/lottie/LottieAnimationView;

    if-nez v0, :cond_9

    goto :goto_5

    :cond_9
    const/4 v4, -0x1

    invoke-virtual {v0, v4}, Lcom/airbnb/lottie/LottieAnimationView;->setRepeatCount(I)V

    :goto_5
    iget-object v0, p0, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->W:Landroidx/recyclerview/widget/RecyclerView;

    const-string v4, "mMainRecyclerView"

    if-nez v0, :cond_a

    invoke-static {v4}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_a
    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setItemAnimator(Landroidx/recyclerview/widget/RecyclerView$l;)V

    const v0, 0x7f0a01b0

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;

    iput-object v0, p0, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->f0:Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;

    iget-object v0, p0, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->W:Landroidx/recyclerview/widget/RecyclerView;

    if-nez v0, :cond_b

    invoke-static {v4}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_b
    const/4 v5, 0x2

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->setOverScrollMode(I)V

    iget-object v0, p0, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->W:Landroidx/recyclerview/widget/RecyclerView;

    if-nez v0, :cond_c

    invoke-static {v4}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_c
    new-instance v6, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-direct {v6, p0, v3, v2}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    invoke-virtual {v0, v6}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$o;)V

    new-instance v0, Lj6/b;

    iget-object v6, p0, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->W:Landroidx/recyclerview/widget/RecyclerView;

    if-nez v6, :cond_d

    invoke-static {v4}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v6, v1

    :cond_d
    invoke-direct {v0, v6}, Lj6/b;-><init>(Landroidx/recyclerview/widget/RecyclerView;)V

    iput-object v0, p0, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->Y:Lj6/b;

    const v0, 0x7f0a03f2

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    iput-object v0, p0, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->X:Landroidx/recyclerview/widget/RecyclerView;

    if-nez v0, :cond_e

    goto :goto_6

    :cond_e
    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->setOverScrollMode(I)V

    :goto_6
    iget-object v0, p0, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->X:Landroidx/recyclerview/widget/RecyclerView;

    if-nez v0, :cond_f

    goto :goto_7

    :cond_f
    new-instance v4, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-direct {v4, p0, v3, v2}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    invoke-virtual {v0, v4}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$o;)V

    :goto_7
    iget-object v0, p0, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->X:Landroidx/recyclerview/widget/RecyclerView;

    if-nez v0, :cond_10

    goto :goto_8

    :cond_10
    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setItemAnimator(Landroidx/recyclerview/widget/RecyclerView$l;)V

    :goto_8
    new-instance v0, Lj6/b;

    iget-object v2, p0, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->X:Landroidx/recyclerview/widget/RecyclerView;

    invoke-direct {v0, v2}, Lj6/b;-><init>(Landroidx/recyclerview/widget/RecyclerView;)V

    iput-object v0, p0, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->Z:Lj6/b;

    new-instance v0, Landroid/content/IntentFilter;

    const-string v2, "com.miui.packageinstaller.INSTALL_PROGRESS_START_SUCCESS"

    invoke-direct {v0, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const-string v2, "com.action.safe_mode_open"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    new-instance v2, Lcom/miui/packageInstaller/NewPackageInstallerActivity$a;

    invoke-direct {v2, p0}, Lcom/miui/packageInstaller/NewPackageInstallerActivity$a;-><init>(Lcom/miui/packageInstaller/NewPackageInstallerActivity;)V

    iput-object v2, p0, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->k0:Lcom/miui/packageInstaller/NewPackageInstallerActivity$a;

    invoke-virtual {p0, v2, v0}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-direct {p0}, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->J2()V

    invoke-static {p0}, Landroidx/lifecycle/k;->a(Landroidx/lifecycle/j;)Landroidx/lifecycle/e;

    move-result-object v3

    invoke-static {}, Lv8/t0;->c()Lv8/v1;

    move-result-object v4

    const/4 v5, 0x0

    new-instance v6, Lcom/miui/packageInstaller/NewPackageInstallerActivity$b;

    invoke-direct {v6, p0, v1}, Lcom/miui/packageInstaller/NewPackageInstallerActivity$b;-><init>(Lcom/miui/packageInstaller/NewPackageInstallerActivity;Ld8/d;)V

    const/4 v7, 0x2

    const/4 v8, 0x0

    invoke-static/range {v3 .. v8}, Lv8/f;->d(Lv8/e0;Ld8/g;Lv8/g0;Ll8/p;ILjava/lang/Object;)Lv8/k1;

    return-void
.end method

.method public final I2()V
    .locals 3

    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    iget-boolean v0, v0, Lcom/miui/packageInstaller/model/CloudParams;->useSystemAppRules:Z

    if-ne v0, v1, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    move v0, v2

    :goto_0
    if-nez v0, :cond_2

    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-boolean v0, v0, Lcom/miui/packageInstaller/model/CloudParams;->storeListed:Z

    if-ne v0, v1, :cond_1

    goto :goto_1

    :cond_1
    move v1, v2

    :goto_1
    if-nez v1, :cond_2

    invoke-virtual {p0}, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->A2()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lm5/z0;->M1()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    invoke-direct {p0}, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->z2()V

    :cond_3
    return-void
.end method

.method public O()V
    .locals 2

    iget-object v0, p0, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->f0:Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->getMFirstButton()Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;->setProgressVisibility(Z)V

    :cond_0
    return-void
.end method

.method public X1()Ljava/lang/String;
    .locals 4

    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    iget-object v2, v0, Lcom/miui/packageInstaller/model/CloudParams;->secureInstallTip:Lcom/miui/packageInstaller/model/Tips;

    if-eqz v2, :cond_1

    iget-object v2, v2, Lcom/miui/packageInstaller/model/Tips;->text:Ljava/lang/String;

    goto :goto_0

    :cond_1
    move-object v2, v1

    :goto_0
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    return-object v1

    :cond_2
    iget-boolean v3, v0, Lcom/miui/packageInstaller/model/CloudParams;->backgroundInstall:Z

    if-eqz v3, :cond_3

    iget-boolean v3, v0, Lcom/miui/packageInstaller/model/CloudParams;->bundleApp:Z

    if-nez v3, :cond_3

    return-object v2

    :cond_3
    invoke-virtual {p0}, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->A2()Z

    move-result v3

    if-nez v3, :cond_4

    iget-boolean v3, v0, Lcom/miui/packageInstaller/model/CloudParams;->storeListed:Z

    if-nez v3, :cond_4

    iget-boolean v0, v0, Lcom/miui/packageInstaller/model/CloudParams;->bundleApp:Z

    if-nez v0, :cond_4

    return-object v2

    :cond_4
    return-object v1
.end method

.method public a2()V
    .locals 5

    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/miui/packageInstaller/model/CloudParams;->riskAppProtectionGuardTip:Lcom/miui/packageInstaller/model/RiskAppProtectionGuardTip;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->E2()Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Lt5/a0;

    invoke-direct {v1, p0}, Lt5/a0;-><init>(Landroid/content/Context;)V

    new-instance v2, Lt5/c0;

    invoke-virtual {p0}, Lm5/z0;->m1()Lcom/miui/packageInstaller/model/ApkInfo;

    move-result-object v3

    invoke-virtual {p0}, Lm5/z0;->r1()Lm5/e;

    move-result-object v4

    invoke-direct {v2, p0, v0, v3, v4}, Lt5/c0;-><init>(Landroid/app/Activity;Lcom/miui/packageInstaller/model/RiskAppProtectionGuardTip;Lcom/miui/packageInstaller/model/ApkInfo;Lm5/e;)V

    invoke-virtual {v1, v2}, Lt5/a0;->c(Lt5/g0;)V

    iget-object v0, p0, Lq2/b;->q:Lo5/b;

    invoke-virtual {v0}, Lo5/b;->d()V

    new-instance v0, Lp5/g;

    const-string v2, "virus_cue_popup"

    const-string v3, "popup"

    invoke-direct {v0, v2, v3, p0}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    new-instance v0, Lcom/miui/packageInstaller/NewPackageInstallerActivity$c;

    invoke-direct {v0, p0}, Lcom/miui/packageInstaller/NewPackageInstallerActivity$c;-><init>(Lcom/miui/packageInstaller/NewPackageInstallerActivity;)V

    invoke-virtual {v1, v0}, Lt5/a0;->a(Lt5/g0$a;)V

    goto :goto_1

    :cond_1
    invoke-super {p0}, Lm5/z0;->a2()V

    :goto_1
    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 0

    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1
.end method

.method public e1()V
    .locals 4

    invoke-virtual {p0}, Lm5/z0;->l1()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lm5/z0;->x1()Lcom/miui/packageInstaller/model/Virus;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->f0:Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->getMFirstButton()Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->b(Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;)V

    :cond_0
    iget-object v0, p0, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->Y:Lj6/b;

    const/4 v1, 0x0

    if-nez v0, :cond_1

    const-string v0, "mAdapter"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_1
    invoke-virtual {v0}, Lj6/b;->R()Ljava/util/List;

    move-result-object v0

    const-string v2, "mAdapter.list"

    invoke-static {v0, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->Z:Lj6/b;

    if-nez v2, :cond_2

    const-string v2, "mWarningAdapter"

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    move-object v1, v2

    :goto_0
    invoke-virtual {v1}, Lj6/b;->R()Ljava/util/List;

    move-result-object v1

    const-string v2, "mWarningAdapter.list"

    invoke-static {v1, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, v1}, Lb8/j;->y(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_3
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lm6/a;

    invoke-virtual {p0}, Lm5/z0;->x1()Lcom/miui/packageInstaller/model/Virus;

    move-result-object v2

    if-eqz v2, :cond_4

    instance-of v2, v1, Lcom/miui/packageInstaller/ui/listcomponets/u;

    if-eqz v2, :cond_4

    invoke-virtual {v1}, Lm6/a;->t()V

    :cond_4
    instance-of v2, v1, Lcom/miui/packageInstaller/ui/listcomponets/VirusInfoViewObject;

    if-eqz v2, :cond_6

    invoke-virtual {p0}, Lm5/z0;->x1()Lcom/miui/packageInstaller/model/Virus;

    move-result-object v2

    if-eqz v2, :cond_5

    move-object v2, v1

    check-cast v2, Lcom/miui/packageInstaller/ui/listcomponets/VirusInfoViewObject;

    invoke-virtual {p0}, Lm5/z0;->x1()Lcom/miui/packageInstaller/model/Virus;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/miui/packageInstaller/ui/listcomponets/VirusInfoViewObject;->A(Lcom/miui/packageInstaller/model/Virus;)V

    invoke-virtual {v1}, Lm6/a;->m()V

    goto :goto_2

    :cond_5
    invoke-virtual {v1}, Lm6/a;->t()V

    :cond_6
    :goto_2
    instance-of v2, v1, Lcom/miui/packageInstaller/ui/listcomponets/e0;

    if-eqz v2, :cond_3

    check-cast v1, Lcom/miui/packageInstaller/ui/listcomponets/e0;

    invoke-interface {v1}, Lcom/miui/packageInstaller/ui/listcomponets/e0;->a()V

    goto :goto_1

    :cond_7
    invoke-direct {p0}, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->z2()V

    iget-object v0, p0, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->f0:Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;

    if-eqz v0, :cond_8

    invoke-virtual {v0}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->getMFirstButton()Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;

    move-result-object v0

    if-eqz v0, :cond_8

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;->setProgressVisibility(Z)V

    :cond_8
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    invoke-super {p0}, Lm5/z0;->onDestroy()V

    iget-object v0, p0, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->k0:Lcom/miui/packageInstaller/NewPackageInstallerActivity$a;

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_0
    iget-object v0, p0, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->e0:La6/g;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    :cond_1
    iget-object v0, p0, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->h0:Lcom/airbnb/lottie/LottieAnimationView;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/airbnb/lottie/LottieAnimationView;->h()V

    :cond_2
    return-void
.end method

.method public y(Ljava/util/List;Ljava/util/List;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lm6/a<",
            "*>;>;",
            "Ljava/util/List<",
            "Lm6/a<",
            "*>;>;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->V:Ljava/lang/String;

    const-string v1, "on on layout created"

    invoke-static {v0, v1}, Lf6/o;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->l0:Z

    iget-object v1, p0, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->Y:Lj6/b;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    const-string v1, "mAdapter"

    invoke-static {v1}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v1, v2

    :cond_0
    invoke-virtual {v1, p1}, Lj6/b;->j0(Ljava/util/List;)V

    iget-object p1, p0, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->Z:Lj6/b;

    if-nez p1, :cond_1

    const-string p1, "mWarningAdapter"

    invoke-static {p1}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move-object v2, p1

    :goto_0
    invoke-virtual {v2, p2}, Lj6/b;->j0(Ljava/util/List;)V

    invoke-virtual {p0}, Lm5/z0;->L1()Z

    move-result p1

    if-nez p1, :cond_6

    invoke-virtual {p0, v0}, Lm5/z0;->e2(Z)V

    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object p1

    if-eqz p1, :cond_2

    invoke-direct {p0}, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->F2()V

    goto :goto_2

    :cond_2
    iget-object p1, p0, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->g0:Landroid/view/View;

    if-nez p1, :cond_3

    goto :goto_1

    :cond_3
    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    :goto_1
    iget-object p1, p0, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->h0:Lcom/airbnb/lottie/LottieAnimationView;

    if-eqz p1, :cond_4

    invoke-virtual {p1}, Lcom/airbnb/lottie/LottieAnimationView;->h()V

    :cond_4
    :goto_2
    invoke-virtual {p0}, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->I2()V

    invoke-virtual {p0}, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->B2()V

    iget-object p1, p0, Lq2/b;->q:Lo5/b;

    invoke-virtual {p1}, Lo5/b;->v()Ljava/lang/String;

    move-result-object p1

    const-string p2, "install_start"

    invoke-static {p2, p1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_5

    invoke-virtual {p0, p2}, Lq2/b;->I0(Ljava/lang/String;)V

    sget-object v1, Ll2/a;->a:Ll2/a;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {p0}, Lm5/z0;->r1()Lm5/e;

    move-result-object v6

    const/16 v7, 0xc

    const/4 v8, 0x0

    const-string v3, "install_start"

    move-object v2, p0

    invoke-static/range {v1 .. v8}, Ll2/a;->c(Ll2/a;Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lm5/e;ILjava/lang/Object;)V

    :cond_5
    invoke-virtual {p0}, Lm5/z0;->u1()Z

    move-result p1

    if-eqz p1, :cond_6

    invoke-virtual {p0, v0}, Lm5/z0;->i2(Z)V

    :cond_6
    invoke-virtual {p0}, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->X1()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_7

    iget-object p2, p0, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->f0:Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;

    if-eqz p2, :cond_7

    invoke-virtual {p2, p1}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->setSecondTips(Ljava/lang/String;)V

    :cond_7
    invoke-virtual {p0}, Lm5/z0;->M1()Z

    move-result p1

    if-eqz p1, :cond_8

    invoke-virtual {p0}, Lcom/miui/packageInstaller/NewPackageInstallerActivity;->e1()V

    :cond_8
    return-void
.end method

.method public y1()Landroid/content/Intent;
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/miui/packageInstaller/InstallProgressActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method
