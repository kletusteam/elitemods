.class final Lcom/miui/packageInstaller/InstallProgressActivity$b;
.super Lf8/k;

# interfaces
.implements Ll8/p;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/packageInstaller/InstallProgressActivity;->m1()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf8/k;",
        "Ll8/p<",
        "Lv8/e0;",
        "Ld8/d<",
        "-",
        "La8/v;",
        ">;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation runtime Lf8/f;
    c = "com.miui.packageInstaller.InstallProgressActivity$initView$1"
    f = "InstallProgressActivity.kt"
    l = {
        0x3a
    }
    m = "invokeSuspend"
.end annotation


# instance fields
.field e:I

.field private synthetic f:Ljava/lang/Object;

.field final synthetic g:Lcom/miui/packageInstaller/InstallProgressActivity;


# direct methods
.method constructor <init>(Lcom/miui/packageInstaller/InstallProgressActivity;Ld8/d;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/miui/packageInstaller/InstallProgressActivity;",
            "Ld8/d<",
            "-",
            "Lcom/miui/packageInstaller/InstallProgressActivity$b;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/miui/packageInstaller/InstallProgressActivity$b;->g:Lcom/miui/packageInstaller/InstallProgressActivity;

    const/4 p1, 0x2

    invoke-direct {p0, p1, p2}, Lf8/k;-><init>(ILd8/d;)V

    return-void
.end method


# virtual methods
.method public final b(Ljava/lang/Object;Ld8/d;)Ld8/d;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ld8/d<",
            "*>;)",
            "Ld8/d<",
            "La8/v;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/miui/packageInstaller/InstallProgressActivity$b;

    iget-object v1, p0, Lcom/miui/packageInstaller/InstallProgressActivity$b;->g:Lcom/miui/packageInstaller/InstallProgressActivity;

    invoke-direct {v0, v1, p2}, Lcom/miui/packageInstaller/InstallProgressActivity$b;-><init>(Lcom/miui/packageInstaller/InstallProgressActivity;Ld8/d;)V

    iput-object p1, v0, Lcom/miui/packageInstaller/InstallProgressActivity$b;->f:Ljava/lang/Object;

    return-object v0
.end method

.method public bridge synthetic g(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lv8/e0;

    check-cast p2, Ld8/d;

    invoke-virtual {p0, p1, p2}, Lcom/miui/packageInstaller/InstallProgressActivity$b;->q(Lv8/e0;Ld8/d;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final n(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 9

    invoke-static {}, Le8/b;->c()Ljava/lang/Object;

    move-result-object v0

    iget v1, p0, Lcom/miui/packageInstaller/InstallProgressActivity$b;->e:I

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    if-ne v1, v2, :cond_0

    iget-object v0, p0, Lcom/miui/packageInstaller/InstallProgressActivity$b;->f:Ljava/lang/Object;

    check-cast v0, Lcom/miui/packageInstaller/InstallProgressActivity;

    invoke-static {p1}, La8/n;->b(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    invoke-static {p1}, La8/n;->b(Ljava/lang/Object;)V

    iget-object p1, p0, Lcom/miui/packageInstaller/InstallProgressActivity$b;->f:Ljava/lang/Object;

    move-object v3, p1

    check-cast v3, Lv8/e0;

    invoke-static {}, Lv8/t0;->b()Lv8/a0;

    move-result-object v4

    const/4 v5, 0x0

    new-instance v6, Lcom/miui/packageInstaller/InstallProgressActivity$b$a;

    const/4 p1, 0x0

    invoke-direct {v6, p1}, Lcom/miui/packageInstaller/InstallProgressActivity$b$a;-><init>(Ld8/d;)V

    const/4 v7, 0x2

    const/4 v8, 0x0

    invoke-static/range {v3 .. v8}, Lv8/f;->b(Lv8/e0;Ld8/g;Lv8/g0;Ll8/p;ILjava/lang/Object;)Lv8/l0;

    move-result-object p1

    iget-object v1, p0, Lcom/miui/packageInstaller/InstallProgressActivity$b;->g:Lcom/miui/packageInstaller/InstallProgressActivity;

    iput-object v1, p0, Lcom/miui/packageInstaller/InstallProgressActivity$b;->f:Ljava/lang/Object;

    iput v2, p0, Lcom/miui/packageInstaller/InstallProgressActivity$b;->e:I

    invoke-interface {p1, p0}, Lv8/l0;->z(Ld8/d;)Ljava/lang/Object;

    move-result-object p1

    if-ne p1, v0, :cond_2

    return-object v0

    :cond_2
    move-object v0, v1

    :goto_0
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-virtual {v0, p1}, Lcom/miui/packageInstaller/InstallProgressActivity;->V1(Z)V

    sget-object p1, La8/v;->a:La8/v;

    return-object p1
.end method

.method public final q(Lv8/e0;Ld8/d;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lv8/e0;",
            "Ld8/d<",
            "-",
            "La8/v;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/miui/packageInstaller/InstallProgressActivity$b;->b(Ljava/lang/Object;Ld8/d;)Ld8/d;

    move-result-object p1

    check-cast p1, Lcom/miui/packageInstaller/InstallProgressActivity$b;

    sget-object p2, La8/v;->a:La8/v;

    invoke-virtual {p1, p2}, Lcom/miui/packageInstaller/InstallProgressActivity$b;->n(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method
