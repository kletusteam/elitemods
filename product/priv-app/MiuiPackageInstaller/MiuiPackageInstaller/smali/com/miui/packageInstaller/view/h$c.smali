.class public final Lcom/miui/packageInstaller/view/h$c;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/packageInstaller/view/h;->Z1()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/miui/packageInstaller/view/h;

.field final synthetic b:Lm8/q;


# direct methods
.method constructor <init>(Lcom/miui/packageInstaller/view/h;Lm8/q;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/view/h$c;->a:Lcom/miui/packageInstaller/view/h;

    iput-object p2, p0, Lcom/miui/packageInstaller/view/h$c;->b:Lm8/q;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 4

    const-string v0, "s"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/view/h$c;->a:Lcom/miui/packageInstaller/view/h;

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/miui/packageInstaller/view/h;->j2(I)V

    iget-object v0, p0, Lcom/miui/packageInstaller/view/h$c;->b:Lm8/q;

    iget-boolean v0, v0, Lm8/q;->a:Z

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/miui/packageInstaller/view/h$c;->a:Lcom/miui/packageInstaller/view/h;

    invoke-static {v0}, Lcom/miui/packageInstaller/view/h;->P1(Lcom/miui/packageInstaller/view/h;)Landroid/widget/CheckBox;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "mCbReasonOther"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v0, 0x0

    :cond_0
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-lez v3, :cond_1

    move v3, v1

    goto :goto_0

    :cond_1
    move v3, v2

    :goto_0
    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    :cond_2
    iget-object v0, p0, Lcom/miui/packageInstaller/view/h$c;->b:Lm8/q;

    iput-boolean v2, v0, Lm8/q;->a:Z

    iget-object v0, p0, Lcom/miui/packageInstaller/view/h$c;->a:Lcom/miui/packageInstaller/view/h;

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result p1

    if-lez p1, :cond_3

    goto :goto_1

    :cond_3
    move v1, v2

    :goto_1
    invoke-static {v0, v1}, Lcom/miui/packageInstaller/view/h;->M1(Lcom/miui/packageInstaller/view/h;Z)V

    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    const-string p2, "s"

    invoke-static {p1, p2}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    const-string p2, "s"

    invoke-static {p1, p2}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method
