.class public final Lcom/miui/packageInstaller/ui/listcomponets/RiskAppInstallAuthHistoryViewObject;
.super Lm6/a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/packageInstaller/ui/listcomponets/RiskAppInstallAuthHistoryViewObject$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lm6/a<",
        "Lcom/miui/packageInstaller/ui/listcomponets/RiskAppInstallAuthHistoryViewObject$ViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private final l:Lcom/miui/packageInstaller/model/AuthInstallInfo;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/AuthInstallInfo;Ll6/c;Lm6/b;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mData"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2, p3, p4}, Lm6/a;-><init>(Landroid/content/Context;Ljava/lang/Object;Ll6/c;Lm6/b;)V

    iput-object p2, p0, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppInstallAuthHistoryViewObject;->l:Lcom/miui/packageInstaller/model/AuthInstallInfo;

    return-void
.end method


# virtual methods
.method public k()I
    .locals 1

    const v0, 0x7f0d014a

    return v0
.end method

.method public bridge synthetic o(Landroidx/recyclerview/widget/RecyclerView$d0;)V
    .locals 0

    check-cast p1, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppInstallAuthHistoryViewObject$ViewHolder;

    invoke-virtual {p0, p1}, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppInstallAuthHistoryViewObject;->z(Lcom/miui/packageInstaller/ui/listcomponets/RiskAppInstallAuthHistoryViewObject$ViewHolder;)V

    return-void
.end method

.method public z(Lcom/miui/packageInstaller/ui/listcomponets/RiskAppInstallAuthHistoryViewObject$ViewHolder;)V
    .locals 6

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppInstallAuthHistoryViewObject$ViewHolder;->getTvTittle()Landroid/widget/TextView;

    move-result-object v1

    goto :goto_0

    :cond_0
    move-object v1, v0

    :goto_0
    if-nez v1, :cond_1

    goto :goto_1

    :cond_1
    iget-object v2, p0, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppInstallAuthHistoryViewObject;->l:Lcom/miui/packageInstaller/model/AuthInstallInfo;

    invoke-virtual {v2}, Lcom/miui/packageInstaller/model/AuthInstallInfo;->getAppName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "yyyy-MM-dd HH:mm:ss"

    invoke-direct {v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppInstallAuthHistoryViewObject;->l:Lcom/miui/packageInstaller/model/AuthInstallInfo;

    invoke-virtual {v3}, Lcom/miui/packageInstaller/model/AuthInstallInfo;->getVersion()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v3, 0x7c

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    new-instance v3, Ljava/util/Date;

    iget-object v4, p0, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppInstallAuthHistoryViewObject;->l:Lcom/miui/packageInstaller/model/AuthInstallInfo;

    invoke-virtual {v4}, Lcom/miui/packageInstaller/model/AuthInstallInfo;->getAuthTime()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppInstallAuthHistoryViewObject$ViewHolder;->getTvDes()Landroid/widget/TextView;

    move-result-object v0

    :cond_2
    if-nez v0, :cond_3

    goto :goto_2

    :cond_3
    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const v2, 0x7f1102d7

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    invoke-virtual {p1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_2
    return-void
.end method
