.class public final Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceAnimViewObject;
.super Lm6/a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceAnimViewObject$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lm6/a<",
        "Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceAnimViewObject$ViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private final l:I

.field private m:Z

.field private n:J


# direct methods
.method public constructor <init>(Landroid/content/Context;ILl6/c;Lm6/b;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p3, p4}, Lm6/a;-><init>(Landroid/content/Context;Ljava/lang/Object;Ll6/c;Lm6/b;)V

    iput p2, p0, Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceAnimViewObject;->l:I

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;ILl6/c;Lm6/b;ILm8/g;)V
    .locals 1

    and-int/lit8 p6, p5, 0x4

    const/4 v0, 0x0

    if-eqz p6, :cond_0

    move-object p3, v0

    :cond_0
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_1

    move-object p4, v0

    :cond_1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceAnimViewObject;-><init>(Landroid/content/Context;ILl6/c;Lm6/b;)V

    return-void
.end method


# virtual methods
.method public A(Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceAnimViewObject$ViewHolder;)V
    .locals 4

    const-string v0, "viewHolder"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f07011f

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f070114

    :goto_0
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceAnimViewObject$ViewHolder;->getLlItem()Landroidx/appcompat/widget/LinearLayoutCompat;

    move-result-object v2

    invoke-virtual {v2, v1, v1, v1, v0}, Landroid/view/ViewGroup;->setPadding(IIII)V

    :cond_1
    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceAnimViewObject$ViewHolder;->getMTitle()Landroid/widget/TextView;

    move-result-object v2

    iget v3, p0, Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceAnimViewObject;->l:I

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceAnimViewObject$ViewHolder;->getMTitle()Landroid/widget/TextView;

    move-result-object v2

    const v3, 0x7f0700c7

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    const v3, 0x3e99999a    # 0.3f

    invoke-static {v2, v0, v3}, Lf6/y;->a(Landroid/widget/TextView;FF)V

    iget-boolean v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceAnimViewObject;->m:Z

    const/16 v2, 0x8

    if-nez v0, :cond_2

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceAnimViewObject$ViewHolder;->getMStatusDoneIndicator()Lcom/airbnb/lottie/LottieAnimationView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceAnimViewObject$ViewHolder;->getMStatusProgressIndicator()Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_2
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceAnimViewObject$ViewHolder;->getMStatusDoneIndicator()Lcom/airbnb/lottie/LottieAnimationView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-ne v0, v2, :cond_3

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceAnimViewObject$ViewHolder;->getMStatusDoneIndicator()Lcom/airbnb/lottie/LottieAnimationView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceAnimViewObject$ViewHolder;->playDoneAnim()V

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "play done anim"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceAnimViewObject;->n:J

    sub-long/2addr v0, v2

    invoke-virtual {p1, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "HZC"

    invoke-static {v0, p1}, Lf6/o;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    :cond_3
    return-void
.end method

.method public k()I
    .locals 1

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0d007f

    goto :goto_0

    :cond_0
    const v0, 0x7f0d007d

    :goto_0
    return v0
.end method

.method public bridge synthetic o(Landroidx/recyclerview/widget/RecyclerView$d0;)V
    .locals 0

    check-cast p1, Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceAnimViewObject$ViewHolder;

    invoke-virtual {p0, p1}, Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceAnimViewObject;->A(Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceAnimViewObject$ViewHolder;)V

    return-void
.end method

.method public final z()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceAnimViewObject;->m:Z

    invoke-virtual {p0}, Lm6/a;->m()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceAnimViewObject;->n:J

    return-void
.end method
