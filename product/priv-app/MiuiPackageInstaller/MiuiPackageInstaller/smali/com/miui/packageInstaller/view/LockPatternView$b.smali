.class public final enum Lcom/miui/packageInstaller/view/LockPatternView$b;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/packageInstaller/view/LockPatternView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/miui/packageInstaller/view/LockPatternView$b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/miui/packageInstaller/view/LockPatternView$b;

.field public static final enum b:Lcom/miui/packageInstaller/view/LockPatternView$b;

.field public static final enum c:Lcom/miui/packageInstaller/view/LockPatternView$b;

.field private static final synthetic d:[Lcom/miui/packageInstaller/view/LockPatternView$b;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    new-instance v0, Lcom/miui/packageInstaller/view/LockPatternView$b;

    const-string v1, "Correct"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/miui/packageInstaller/view/LockPatternView$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/miui/packageInstaller/view/LockPatternView$b;->a:Lcom/miui/packageInstaller/view/LockPatternView$b;

    new-instance v1, Lcom/miui/packageInstaller/view/LockPatternView$b;

    const-string v3, "Animate"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lcom/miui/packageInstaller/view/LockPatternView$b;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/miui/packageInstaller/view/LockPatternView$b;->b:Lcom/miui/packageInstaller/view/LockPatternView$b;

    new-instance v3, Lcom/miui/packageInstaller/view/LockPatternView$b;

    const-string v5, "Wrong"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lcom/miui/packageInstaller/view/LockPatternView$b;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lcom/miui/packageInstaller/view/LockPatternView$b;->c:Lcom/miui/packageInstaller/view/LockPatternView$b;

    const/4 v5, 0x3

    new-array v5, v5, [Lcom/miui/packageInstaller/view/LockPatternView$b;

    aput-object v0, v5, v2

    aput-object v1, v5, v4

    aput-object v3, v5, v6

    sput-object v5, Lcom/miui/packageInstaller/view/LockPatternView$b;->d:[Lcom/miui/packageInstaller/view/LockPatternView$b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/miui/packageInstaller/view/LockPatternView$b;
    .locals 1

    const-class v0, Lcom/miui/packageInstaller/view/LockPatternView$b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/miui/packageInstaller/view/LockPatternView$b;

    return-object p0
.end method

.method public static values()[Lcom/miui/packageInstaller/view/LockPatternView$b;
    .locals 1

    sget-object v0, Lcom/miui/packageInstaller/view/LockPatternView$b;->d:[Lcom/miui/packageInstaller/view/LockPatternView$b;

    invoke-virtual {v0}, [Lcom/miui/packageInstaller/view/LockPatternView$b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/miui/packageInstaller/view/LockPatternView$b;

    return-object v0
.end method
