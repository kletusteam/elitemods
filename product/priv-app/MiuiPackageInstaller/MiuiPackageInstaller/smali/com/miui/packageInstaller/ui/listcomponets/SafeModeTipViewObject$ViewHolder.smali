.class public final Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;
.super Landroidx/recyclerview/widget/RecyclerView$d0;

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ViewHolder"
.end annotation


# instance fields
.field private btnOpen:Lmiuix/slidingwidget/widget/SlidingButton;

.field private btnOpenStyleBtn:Landroid/widget/Button;

.field private clContentView:Landroidx/constraintlayout/widget/ConstraintLayout;

.field private desView:Landroid/widget/TextView;

.field private icArrow:Landroid/view/View;

.field private ivIcon:Landroid/widget/ImageView;

.field private learMore:Landroid/widget/TextView;

.field private rootView:Landroid/view/ViewGroup;

.field private titleView:Landroid/widget/TextView;

.field private tvSuggestion:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    const-string v0, "itemView"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$d0;-><init>(Landroid/view/View;)V

    const v0, 0x7f0a0198

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->ivIcon:Landroid/widget/ImageView;

    const v0, 0x7f0a00d5

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/constraintlayout/widget/ConstraintLayout;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->clContentView:Landroidx/constraintlayout/widget/ConstraintLayout;

    const v0, 0x7f0a00a5

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->btnOpenStyleBtn:Landroid/widget/Button;

    const v0, 0x7f0a00a4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiuix/slidingwidget/widget/SlidingButton;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->btnOpen:Lmiuix/slidingwidget/widget/SlidingButton;

    const v0, 0x7f0a03b8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->learMore:Landroid/widget/TextView;

    const v0, 0x7f0a02d1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->rootView:Landroid/view/ViewGroup;

    const v0, 0x7f0a03d3

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->titleView:Landroid/widget/TextView;

    const v0, 0x7f0a03af

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->desView:Landroid/widget/TextView;

    const v0, 0x7f0a03ce

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->tvSuggestion:Landroid/widget/TextView;

    const v0, 0x7f0a007e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->icArrow:Landroid/view/View;

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->learMore:Landroid/widget/TextView;

    new-instance v1, Lcom/miui/packageInstaller/ui/listcomponets/z;

    invoke-direct {v1, p1}, Lcom/miui/packageInstaller/ui/listcomponets/z;-><init>(Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->btnOpen:Lmiuix/slidingwidget/widget/SlidingButton;

    invoke-virtual {v0, p0}, Lmiuix/slidingwidget/widget/SlidingButton;->setOnPerformCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->icArrow:Landroid/view/View;

    new-instance v1, Lcom/miui/packageInstaller/ui/listcomponets/a0;

    invoke-direct {v1, p1}, Lcom/miui/packageInstaller/ui/listcomponets/a0;-><init>(Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->btnOpenStyleBtn:Landroid/widget/Button;

    new-instance v1, Lcom/miui/packageInstaller/ui/listcomponets/b0;

    invoke-direct {v1, p1, p0}, Lcom/miui/packageInstaller/ui/listcomponets/b0;-><init>(Landroid/view/View;Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private static final _init_$lambda-0(Landroid/view/View;Landroid/view/View;)V
    .locals 4

    const-string p1, "$itemView"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    instance-of p1, p1, Lo5/a;

    if-eqz p1, :cond_0

    new-instance p1, Lp5/b;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "null cannot be cast to non-null type com.miui.packageInstaller.analytics.IPage"

    invoke-static {v0, v1}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lo5/a;

    const-string v1, "safe_mode_know_btn"

    const-string v2, "button"

    invoke-direct {p1, v1, v2, v0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p1}, Lp5/f;->c()Z

    :cond_0
    new-instance p1, Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lcom/miui/packageInstaller/ui/SecureModeAdvantageActivity;

    invoke-direct {p1, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    instance-of v0, v0, Lq2/b;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "null cannot be cast to non-null type com.android.packageinstaller.miui.BaseActivity"

    invoke-static {v0, v1}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lq2/b;

    invoke-virtual {v0}, Lq2/b;->F0()Lo5/b;

    move-result-object v0

    goto :goto_0

    :cond_1
    new-instance v0, Lo5/b;

    const/4 v1, 0x2

    const-string v2, ""

    const/4 v3, 0x0

    invoke-direct {v0, v2, v3, v1, v3}, Lo5/b;-><init>(Ljava/lang/String;Ljava/lang/String;ILm8/g;)V

    :goto_0
    const-string v1, "fromPage"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-virtual {p0, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private static final _init_$lambda-1(Landroid/view/View;Landroid/view/View;)V
    .locals 3

    const-string p1, "$itemView"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    instance-of p1, p1, Lo5/a;

    if-eqz p1, :cond_0

    new-instance p1, Lp5/b;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "null cannot be cast to non-null type com.miui.packageInstaller.analytics.IPage"

    invoke-static {v0, v1}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lo5/a;

    const-string v1, "safe_mode_arrow_btn"

    const-string v2, "button"

    invoke-direct {p1, v1, v2, v0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p1}, Lp5/f;->c()Z

    :cond_0
    new-instance p1, Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lcom/miui/packageInstaller/ui/SecureModeAdvantageActivity;

    invoke-direct {p1, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-virtual {p0, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private static final _init_$lambda-2(Landroid/view/View;Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;Landroid/view/View;)V
    .locals 1

    const-string p2, "$itemView"

    invoke-static {p0, p2}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "this$0"

    invoke-static {p1, p2}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p2

    const/4 v0, 0x1

    invoke-static {p2, v0}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->setSafeModelEnabled(Landroid/content/Context;Z)V

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p0

    const-string p2, "itemView.context"

    invoke-static {p0, p2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, p0, v0}, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->updateSuggestionMsgState(Landroid/content/Context;Z)V

    invoke-direct {p1}, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->showOpenSecurityModeDialogAndTrack()V

    iget-object p0, p1, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->btnOpenStyleBtn:Landroid/widget/Button;

    const/16 p2, 0x8

    invoke-virtual {p0, p2}, Landroid/widget/Button;->setVisibility(I)V

    invoke-direct {p1}, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->showHasOpenedStyle()V

    return-void
.end method

.method public static synthetic a(Landroid/view/View;Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->_init_$lambda-2(Landroid/view/View;Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic b(Landroid/view/View;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->_init_$lambda-0(Landroid/view/View;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic c(Landroid/view/View;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->_init_$lambda-1(Landroid/view/View;Landroid/view/View;)V

    return-void
.end method

.method private final showHasOpenedStyle()V
    .locals 4

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->icArrow:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Landroidx/recyclerview/widget/RecyclerView$d0;->itemView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    instance-of v0, v0, Lo5/a;

    if-eqz v0, :cond_0

    new-instance v0, Lp5/g;

    iget-object v1, p0, Landroidx/recyclerview/widget/RecyclerView$d0;->itemView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "null cannot be cast to non-null type com.miui.packageInstaller.analytics.IPage"

    invoke-static {v1, v2}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lo5/a;

    const-string v2, "safe_mode_arrow_btn"

    const-string v3, "button"

    invoke-direct {v0, v2, v3, v1}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    :cond_0
    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->ivIcon:Landroid/widget/ImageView;

    const v1, 0x7f080588

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->titleView:Landroid/widget/TextView;

    iget-object v1, p0, Landroidx/recyclerview/widget/RecyclerView$d0;->itemView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f060023

    invoke-virtual {v1, v2}, Landroid/content/Context;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->clContentView:Landroidx/constraintlayout/widget/ConstraintLayout;

    const v1, 0x7f08012f

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setBackgroundResource(I)V

    return-void
.end method

.method private final showOpenSecurityModeDialog()V
    .locals 4

    iget-object v0, p0, Landroidx/recyclerview/widget/RecyclerView$d0;->itemView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "null cannot be cast to non-null type miuix.appcompat.app.AppCompatActivity"

    invoke-static {v0, v1}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lmiuix/appcompat/app/j;

    invoke-virtual {v0}, Landroidx/fragment/app/e;->X()Landroidx/fragment/app/m;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/fragment/app/m;->l()Landroidx/fragment/app/v;

    move-result-object v0

    const-string v1, "activity.supportFragmentManager.beginTransaction()"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v1, Ld6/f;

    invoke-direct {v1}, Ld6/f;-><init>()V

    const v2, 0x7f0a017f

    const-string v3, "openSecurityModeDialog"

    invoke-virtual {v0, v2, v1, v3}, Landroidx/fragment/app/v;->o(ILandroidx/fragment/app/Fragment;Ljava/lang/String;)Landroidx/fragment/app/v;

    invoke-virtual {v0}, Landroidx/fragment/app/v;->h()I

    return-void
.end method

.method private final showOpenSecurityModeDialogAndTrack()V
    .locals 2

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroidx/recyclerview/widget/RecyclerView$d0;->itemView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "itemView.context"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->showOpenSecurityModeToastLiteMode(Landroid/content/Context;)V

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->showOpenSecurityModeDialog()V

    :goto_0
    return-void
.end method

.method private final showOpenSecurityModeToastLiteMode(Landroid/content/Context;)V
    .locals 2

    const v0, 0x7f1102c3

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    return-void
.end method


# virtual methods
.method public final getBtnOpen()Lmiuix/slidingwidget/widget/SlidingButton;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->btnOpen:Lmiuix/slidingwidget/widget/SlidingButton;

    return-object v0
.end method

.method public final getBtnOpenStyleBtn()Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->btnOpenStyleBtn:Landroid/widget/Button;

    return-object v0
.end method

.method public final getClContentView()Landroidx/constraintlayout/widget/ConstraintLayout;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->clContentView:Landroidx/constraintlayout/widget/ConstraintLayout;

    return-object v0
.end method

.method public final getDesView()Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->desView:Landroid/widget/TextView;

    return-object v0
.end method

.method public final getIcArrow()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->icArrow:Landroid/view/View;

    return-object v0
.end method

.method public final getIvIcon()Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->ivIcon:Landroid/widget/ImageView;

    return-object v0
.end method

.method public final getLearMore()Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->learMore:Landroid/widget/TextView;

    return-object v0
.end method

.method public final getRootView()Landroid/view/ViewGroup;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->rootView:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public final getTitleView()Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->titleView:Landroid/widget/TextView;

    return-object v0
.end method

.method public final getTvSuggestion()Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->tvSuggestion:Landroid/widget/TextView;

    return-object v0
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 3

    iget-object p1, p0, Landroidx/recyclerview/widget/RecyclerView$d0;->itemView:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1, p2}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->setSafeModelEnabled(Landroid/content/Context;Z)V

    iget-object p1, p0, Landroidx/recyclerview/widget/RecyclerView$d0;->itemView:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    const-string v0, "itemView.context"

    invoke-static {p1, v0}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, p1, p2}, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->updateSuggestionMsgState(Landroid/content/Context;Z)V

    if-eqz p2, :cond_0

    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->showOpenSecurityModeDialogAndTrack()V

    :cond_0
    iget-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->icArrow:Landroid/view/View;

    const/4 v0, 0x0

    const/16 v1, 0x8

    if-eqz p2, :cond_1

    move v2, v0

    goto :goto_0

    :cond_1
    move v2, v1

    :goto_0
    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->btnOpen:Lmiuix/slidingwidget/widget/SlidingButton;

    if-eqz p2, :cond_2

    move v0, v1

    :cond_2
    invoke-virtual {p1, v0}, Landroid/widget/CheckBox;->setVisibility(I)V

    iget-object p1, p0, Landroidx/recyclerview/widget/RecyclerView$d0;->itemView:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    instance-of p1, p1, Lo5/a;

    if-eqz p1, :cond_4

    new-instance p1, Lp5/b;

    iget-object v0, p0, Landroidx/recyclerview/widget/RecyclerView$d0;->itemView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "null cannot be cast to non-null type com.miui.packageInstaller.analytics.IPage"

    invoke-static {v0, v1}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lo5/a;

    const-string v1, "safe_mode_switch"

    const-string v2, "switch"

    invoke-direct {p1, v1, v2, v0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    if-eqz p2, :cond_3

    const-string v0, "on"

    goto :goto_1

    :cond_3
    const-string v0, "off"

    :goto_1
    const-string v1, "switch_action"

    invoke-virtual {p1, v1, v0}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p1

    invoke-virtual {p1}, Lp5/f;->c()Z

    :cond_4
    if-eqz p2, :cond_5

    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->showHasOpenedStyle()V

    :cond_5
    return-void
.end method

.method public final setBtnOpen(Lmiuix/slidingwidget/widget/SlidingButton;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->btnOpen:Lmiuix/slidingwidget/widget/SlidingButton;

    return-void
.end method

.method public final setBtnOpenStyleBtn(Landroid/widget/Button;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->btnOpenStyleBtn:Landroid/widget/Button;

    return-void
.end method

.method public final setClContentView(Landroidx/constraintlayout/widget/ConstraintLayout;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->clContentView:Landroidx/constraintlayout/widget/ConstraintLayout;

    return-void
.end method

.method public final setDesView(Landroid/widget/TextView;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->desView:Landroid/widget/TextView;

    return-void
.end method

.method public final setIcArrow(Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->icArrow:Landroid/view/View;

    return-void
.end method

.method public final setIvIcon(Landroid/widget/ImageView;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->ivIcon:Landroid/widget/ImageView;

    return-void
.end method

.method public final setLearMore(Landroid/widget/TextView;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->learMore:Landroid/widget/TextView;

    return-void
.end method

.method public final setRootView(Landroid/view/ViewGroup;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->rootView:Landroid/view/ViewGroup;

    return-void
.end method

.method public final setTitleView(Landroid/widget/TextView;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->titleView:Landroid/widget/TextView;

    return-void
.end method

.method public final setTvSuggestion(Landroid/widget/TextView;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->tvSuggestion:Landroid/widget/TextView;

    return-void
.end method

.method public final updateSuggestionMsgState(Landroid/content/Context;Z)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_0

    iget-object p2, p0, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->tvSuggestion:Landroid/widget/TextView;

    const v0, 0x7f110323

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(I)V

    iget-object p2, p0, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->tvSuggestion:Landroid/widget/TextView;

    const v0, 0x7f08058e

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setBackgroundResource(I)V

    iget-object p2, p0, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->tvSuggestion:Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const v0, 0x7f06004a

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->titleView:Landroid/widget/TextView;

    const p2, 0x7f110300

    goto :goto_0

    :cond_0
    iget-object p2, p0, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->tvSuggestion:Landroid/widget/TextView;

    const v0, 0x7f08058d

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setBackgroundResource(I)V

    iget-object p2, p0, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->tvSuggestion:Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const v0, 0x7f060067

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->tvSuggestion:Landroid/widget/TextView;

    const p2, 0x7f110322

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(I)V

    iget-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->titleView:Landroid/widget/TextView;

    const p2, 0x7f1102ff

    :goto_0
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(I)V

    return-void
.end method
