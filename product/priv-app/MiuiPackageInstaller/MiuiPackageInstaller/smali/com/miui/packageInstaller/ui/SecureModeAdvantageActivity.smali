.class public final Lcom/miui/packageInstaller/ui/SecureModeAdvantageActivity;
.super Lq2/b;


# instance fields
.field private A:Landroid/view/View;

.field private B:Landroidx/core/widget/NestedScrollView;

.field private C:I

.field private D:I

.field private E:Ljava/lang/String;

.field private F:Ljava/lang/String;

.field private u:Landroid/view/View;

.field private v:Landroid/view/View;

.field private w:Landroid/widget/TextView;

.field private x:Landroid/view/View;

.field private y:Landroid/view/View;

.field private z:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lq2/b;-><init>()V

    const/16 v0, 0xff

    iput v0, p0, Lcom/miui/packageInstaller/ui/SecureModeAdvantageActivity;->D:I

    const-string v0, "packageinstaller"

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/SecureModeAdvantageActivity;->F:Ljava/lang/String;

    return-void
.end method

.method public static synthetic J0(Lcom/miui/packageInstaller/ui/SecureModeAdvantageActivity;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/miui/packageInstaller/ui/SecureModeAdvantageActivity;->Q0(Lcom/miui/packageInstaller/ui/SecureModeAdvantageActivity;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic K0(Lcom/miui/packageInstaller/ui/SecureModeAdvantageActivity;Landroid/view/View;IIII)V
    .locals 0

    invoke-static/range {p0 .. p5}, Lcom/miui/packageInstaller/ui/SecureModeAdvantageActivity;->P0(Lcom/miui/packageInstaller/ui/SecureModeAdvantageActivity;Landroid/view/View;IIII)V

    return-void
.end method

.method public static synthetic L0(Lcom/miui/packageInstaller/ui/SecureModeAdvantageActivity;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/miui/packageInstaller/ui/SecureModeAdvantageActivity;->O0(Lcom/miui/packageInstaller/ui/SecureModeAdvantageActivity;Landroid/view/View;)V

    return-void
.end method

.method private final M0()V
    .locals 5

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    const-string v1, "safe_mode_ref"

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :cond_0
    move-object v3, v2

    :goto_0
    iput-object v3, p0, Lcom/miui/packageInstaller/ui/SecureModeAdvantageActivity;->E:Ljava/lang/String;

    if-eqz v0, :cond_1

    const-string v3, "safe_mode_type"

    invoke-virtual {v0, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    :cond_1
    move-object v3, v2

    :goto_1
    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/miui/packageInstaller/ui/SecureModeAdvantageActivity;->F:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v3, p0, Lcom/miui/packageInstaller/ui/SecureModeAdvantageActivity;->E:Ljava/lang/String;

    const-string v4, "null"

    invoke-static {v3, v4}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_2
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/miui/packageInstaller/ui/SecureModeAdvantageActivity;->E:Ljava/lang/String;

    :cond_3
    iget-object v1, p0, Lcom/miui/packageInstaller/ui/SecureModeAdvantageActivity;->E:Ljava/lang/String;

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-eqz v1, :cond_5

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-lez v1, :cond_4

    move v1, v3

    goto :goto_2

    :cond_4
    move v1, v4

    :goto_2
    if-ne v1, v3, :cond_5

    goto :goto_3

    :cond_5
    move v3, v4

    :goto_3
    if-eqz v3, :cond_7

    iget-object v1, p0, Lq2/b;->q:Lo5/b;

    iget-object v3, p0, Lcom/miui/packageInstaller/ui/SecureModeAdvantageActivity;->E:Ljava/lang/String;

    if-nez v3, :cond_6

    const-string v3, ""

    :cond_6
    invoke-virtual {v1, v3}, Lo5/b;->s(Ljava/lang/String;)V

    :cond_7
    iget-object v1, p0, Lcom/miui/packageInstaller/ui/SecureModeAdvantageActivity;->E:Ljava/lang/String;

    const-string v3, "notification_from"

    invoke-static {v1, v3}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    if-eqz v0, :cond_8

    const-string v1, "style"

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :cond_8
    invoke-direct {p0, v2}, Lcom/miui/packageInstaller/ui/SecureModeAdvantageActivity;->S0(Ljava/lang/String;)V

    :cond_9
    return-void
.end method

.method private static final O0(Lcom/miui/packageInstaller/ui/SecureModeAdvantageActivity;Landroid/view/View;)V
    .locals 2

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Lp5/b;

    const-string v0, "page_back_btn"

    const-string v1, "button"

    invoke-direct {p1, v0, v1, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    const-string v0, "back_type"

    const-string v1, "click_icon"

    invoke-virtual {p1, v0, v1}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p1

    invoke-virtual {p1}, Lp5/f;->c()Z

    invoke-virtual {p0}, Lmiuix/appcompat/app/j;->finish()V

    return-void
.end method

.method private static final P0(Lcom/miui/packageInstaller/ui/SecureModeAdvantageActivity;Landroid/view/View;IIII)V
    .locals 3

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/miui/packageInstaller/ui/SecureModeAdvantageActivity;->v:Landroid/view/View;

    const-string p2, "backIcon"

    const/4 p4, 0x0

    if-nez p1, :cond_0

    invoke-static {p2}, Lm8/i;->s(Ljava/lang/String;)V

    move-object p1, p4

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result p1

    const-string p5, "tvTitle"

    const-string v0, "titleView"

    const/16 v1, 0xff

    const/4 v2, 0x0

    if-le p3, p1, :cond_2

    iput v1, p0, Lcom/miui/packageInstaller/ui/SecureModeAdvantageActivity;->D:I

    iget-object p1, p0, Lcom/miui/packageInstaller/ui/SecureModeAdvantageActivity;->w:Landroid/widget/TextView;

    if-nez p1, :cond_1

    invoke-static {p5}, Lm8/i;->s(Ljava/lang/String;)V

    move-object p1, p4

    :cond_1
    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_2
    iget-object p1, p0, Lcom/miui/packageInstaller/ui/SecureModeAdvantageActivity;->u:Landroid/view/View;

    if-nez p1, :cond_3

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object p1, p4

    :cond_3
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result p1

    mul-int/2addr p3, p1

    iget-object p1, p0, Lcom/miui/packageInstaller/ui/SecureModeAdvantageActivity;->v:Landroid/view/View;

    if-nez p1, :cond_4

    invoke-static {p2}, Lm8/i;->s(Ljava/lang/String;)V

    move-object p1, p4

    :cond_4
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result p1

    div-int/2addr p3, p1

    add-int/2addr p3, v2

    iput p3, p0, Lcom/miui/packageInstaller/ui/SecureModeAdvantageActivity;->D:I

    iget-object p1, p0, Lcom/miui/packageInstaller/ui/SecureModeAdvantageActivity;->w:Landroid/widget/TextView;

    if-nez p1, :cond_5

    invoke-static {p5}, Lm8/i;->s(Ljava/lang/String;)V

    move-object p1, p4

    :cond_5
    const/4 p2, 0x4

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    invoke-virtual {p0, p0}, Lcom/miui/packageInstaller/ui/SecureModeAdvantageActivity;->R0(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_7

    iget-object p1, p0, Lcom/miui/packageInstaller/ui/SecureModeAdvantageActivity;->u:Landroid/view/View;

    if-nez p1, :cond_6

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_1

    :cond_6
    move-object p4, p1

    :goto_1
    iget p0, p0, Lcom/miui/packageInstaller/ui/SecureModeAdvantageActivity;->D:I

    invoke-static {p0, v2, v2, v2}, Landroid/graphics/Color;->argb(IIII)I

    move-result p0

    goto :goto_3

    :cond_7
    iget-object p1, p0, Lcom/miui/packageInstaller/ui/SecureModeAdvantageActivity;->u:Landroid/view/View;

    if-nez p1, :cond_8

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_2

    :cond_8
    move-object p4, p1

    :goto_2
    iget p0, p0, Lcom/miui/packageInstaller/ui/SecureModeAdvantageActivity;->D:I

    invoke-static {p0, v1, v1, v1}, Landroid/graphics/Color;->argb(IIII)I

    move-result p0

    :goto_3
    invoke-virtual {p4, p0}, Landroid/view/View;->setBackgroundColor(I)V

    return-void
.end method

.method private static final Q0(Lcom/miui/packageInstaller/ui/SecureModeAdvantageActivity;Landroid/view/View;)V
    .locals 2

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Landroid/content/Intent;

    const-class v0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;

    invoke-direct {p1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v0, 0x4000000

    invoke-virtual {p1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const-string v0, "auto_open"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0, p1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    new-instance p1, Lp5/b;

    const-string v0, "safe_mode_open"

    const-string v1, "button"

    invoke-direct {p1, v0, v1, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p1}, Lp5/f;->c()Z

    invoke-virtual {p0}, Lmiuix/appcompat/app/j;->finish()V

    return-void
.end method

.method private final S0(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lq2/b;->q:Lo5/b;

    invoke-virtual {v0}, Lo5/b;->d()V

    const-string v0, "child_mode"

    invoke-static {p1, v0}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    const-string v0, "button"

    if-eqz p1, :cond_0

    iget-object p1, p0, Lq2/b;->q:Lo5/b;

    const-string v1, "appstore_for_child"

    invoke-virtual {p1, v1}, Lo5/b;->B(Ljava/lang/String;)V

    new-instance p1, Lp5/b;

    const-string v1, "safe_mode_opened_toast_know_btn"

    invoke-direct {p1, v1, v0, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lq2/b;->q:Lo5/b;

    const-string v1, "appstore_for_old"

    invoke-virtual {p1, v1}, Lo5/b;->B(Ljava/lang/String;)V

    new-instance p1, Lp5/b;

    const-string v1, "protect_mode_opened_toast_know_btn"

    invoke-direct {p1, v1, v0, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    :goto_0
    invoke-virtual {p1}, Lp5/f;->c()Z

    iget-object p1, p0, Lq2/b;->q:Lo5/b;

    invoke-virtual {p1}, Lo5/b;->c()V

    return-void
.end method


# virtual methods
.method public E0()Ljava/lang/String;
    .locals 1

    const-string v0, "safe_mode_benefit"

    return-object v0
.end method

.method public final N0()V
    .locals 6

    sget-object v0, Lf6/v;->a:Lf6/v$a;

    invoke-virtual {v0, p0}, Lf6/v$a;->a(Landroid/app/Activity;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x2000

    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    invoke-static {p0}, Lcom/android/packageinstaller/utils/u;->b(Landroid/app/Activity;)I

    move-result v0

    iput v0, p0, Lcom/miui/packageInstaller/ui/SecureModeAdvantageActivity;->C:I

    const v0, 0x7f0a0089

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.back_icon)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/SecureModeAdvantageActivity;->v:Landroid/view/View;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const-string v0, "backIcon"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_0
    new-instance v2, Lz5/n;

    invoke-direct {v2, p0}, Lz5/n;-><init>(Lcom/miui/packageInstaller/ui/SecureModeAdvantageActivity;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0a0387

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v2, "findViewById(R.id.title_view)"

    invoke-static {v0, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/SecureModeAdvantageActivity;->u:Landroid/view/View;

    const v0, 0x7f0a03d3

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v2, "findViewById(R.id.tv_title)"

    invoke-static {v0, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/SecureModeAdvantageActivity;->w:Landroid/widget/TextView;

    const v0, 0x7f0a034b

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v2, "findViewById(R.id.sv_advantage)"

    invoke-static {v0, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroidx/core/widget/NestedScrollView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/SecureModeAdvantageActivity;->B:Landroidx/core/widget/NestedScrollView;

    const v0, 0x7f0a01eb

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v2, "findViewById(R.id.line)"

    invoke-static {v0, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/SecureModeAdvantageActivity;->x:Landroid/view/View;

    const v0, 0x7f0a0095

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v2, "findViewById(R.id.bottom_layout)"

    invoke-static {v0, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/SecureModeAdvantageActivity;->y:Landroid/view/View;

    const v0, 0x7f0a0046

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v2, "findViewById(R.id.action_button)"

    invoke-static {v0, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/SecureModeAdvantageActivity;->z:Landroid/view/View;

    const v0, 0x7f0a034c

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v2, "findViewById(R.id.sv_content)"

    invoke-static {v0, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/SecureModeAdvantageActivity;->A:Landroid/view/View;

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/SecureModeAdvantageActivity;->u:Landroid/view/View;

    if-nez v0, :cond_1

    const-string v0, "titleView"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_1
    iget v2, p0, Lcom/miui/packageInstaller/ui/SecureModeAdvantageActivity;->C:I

    const/4 v3, 0x0

    invoke-virtual {v0, v3, v2, v3, v3}, Landroid/view/View;->setPadding(IIII)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/SecureModeAdvantageActivity;->x:Landroid/view/View;

    const-string v2, "scrollviewTop"

    if-nez v0, :cond_2

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_2
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    const-string v3, "null cannot be cast to non-null type androidx.constraintlayout.widget.ConstraintLayout.LayoutParams"

    invoke-static {v0, v3}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroidx/constraintlayout/widget/ConstraintLayout$b;

    iget v3, p0, Lcom/miui/packageInstaller/ui/SecureModeAdvantageActivity;->C:I

    iput v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget-object v3, p0, Lcom/miui/packageInstaller/ui/SecureModeAdvantageActivity;->x:Landroid/view/View;

    if-nez v3, :cond_3

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v3, v1

    :cond_3
    invoke-virtual {v3, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/SecureModeAdvantageActivity;->B:Landroidx/core/widget/NestedScrollView;

    if-nez v0, :cond_4

    const-string v0, "mScrollView"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_4
    new-instance v2, Lz5/o;

    invoke-direct {v2, p0}, Lz5/o;-><init>(Lcom/miui/packageInstaller/ui/SecureModeAdvantageActivity;)V

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setOnScrollChangeListener(Landroid/view/View$OnScrollChangeListener;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/SecureModeAdvantageActivity;->A:Landroid/view/View;

    const-string v2, "contentLayout"

    if-nez v0, :cond_5

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_5
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    const-string v3, "null cannot be cast to non-null type android.widget.FrameLayout.LayoutParams"

    invoke-static {v0, v3}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-static {p0}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->isSafeModelEnable(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/miui/packageInstaller/ui/SecureModeAdvantageActivity;->y:Landroid/view/View;

    if-nez v3, :cond_6

    const-string v3, "bottomLayout"

    invoke-static {v3}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v3, v1

    :cond_6
    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f07011c

    goto :goto_0

    :cond_7
    new-instance v3, Lp5/g;

    const-string v4, "safe_mode_open"

    const-string v5, "button"

    invoke-direct {v3, v4, v5, p0}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v3}, Lp5/f;->c()Z

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0700b4

    :goto_0
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    iput v3, v0, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    iget-object v3, p0, Lcom/miui/packageInstaller/ui/SecureModeAdvantageActivity;->A:Landroid/view/View;

    if-nez v3, :cond_8

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v3, v1

    :cond_8
    invoke-virtual {v3, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/SecureModeAdvantageActivity;->z:Landroid/view/View;

    if-nez v0, :cond_9

    const-string v0, "actionButton"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_1

    :cond_9
    move-object v1, v0

    :goto_1
    new-instance v0, Lz5/m;

    invoke-direct {v0, p0}, Lz5/m;-><init>(Lcom/miui/packageInstaller/ui/SecureModeAdvantageActivity;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final R0(Landroid/content/Context;)Z
    .locals 2

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object p1

    if-nez p1, :cond_1

    return v0

    :cond_1
    iget p1, p1, Landroid/content/res/Configuration;->uiMode:I

    and-int/lit8 p1, p1, 0x30

    const/16 v1, 0x20

    if-ne p1, v1, :cond_2

    const/4 v0, 0x1

    :cond_2
    return v0
.end method

.method public onBackPressed()V
    .locals 3

    invoke-super {p0}, Lmiuix/appcompat/app/j;->onBackPressed()V

    new-instance v0, Lp5/b;

    const-string v1, "page_back_btn"

    const-string v2, "button"

    invoke-direct {v0, v1, v2, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    const-string v1, "back_type"

    const-string v2, "system"

    invoke-virtual {v0, v1, v2}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object v0

    invoke-virtual {v0}, Lp5/f;->c()Z

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Lq2/b;->onCreate(Landroid/os/Bundle;)V

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->v()Z

    move-result p1

    if-eqz p1, :cond_0

    const p1, 0x7f0d019f

    goto :goto_0

    :cond_0
    const p1, 0x7f0d019d

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/android/packageinstaller/utils/g;->v()Z

    move-result p1

    if-eqz p1, :cond_2

    const p1, 0x7f0d019e

    goto :goto_0

    :cond_2
    const p1, 0x7f0d019c

    :goto_0
    invoke-virtual {p0, p1}, Lmiuix/appcompat/app/j;->setContentView(I)V

    invoke-virtual {p0}, Lcom/miui/packageInstaller/ui/SecureModeAdvantageActivity;->N0()V

    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/SecureModeAdvantageActivity;->M0()V

    return-void
.end method
