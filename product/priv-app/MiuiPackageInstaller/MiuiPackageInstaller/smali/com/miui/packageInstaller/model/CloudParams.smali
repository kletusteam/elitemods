.class public Lcom/miui/packageInstaller/model/CloudParams;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field public adaptiveApp:Z

.field public allowHighLight:Z

.field public appInfo:Lcom/miui/packageInstaller/model/MarketAppInfo;

.field public appManageScene:Ljava/lang/String;

.field public appType:Ljava/lang/String;

.field public authFunction:Lcom/miui/packageInstaller/model/AuthFunction;

.field public backButtonUri:Ljava/lang/String;

.field public backIconUri:Ljava/lang/String;

.field public backgroundInstall:Z

.field public bit64Tip:Lcom/miui/packageInstaller/model/BitTip;

.field public bundleApp:Z

.field public cInfo:Lcom/miui/packageInstaller/model/CInfo;

.field public categoryAbbreviation:Ljava/lang/String;

.field public channel:Ljava/lang/String;

.field public doneButtonTip:Lcom/miui/packageInstaller/model/DoneButtonTip;

.field public expId:Ljava/lang/String;

.field public guideOpenSafeModePopTips:Lcom/miui/packageInstaller/model/GuideOpenSafeModePopTips;

.field public incrementPackageInfo:Lcom/miui/packageInstaller/model/HasIncrement;

.field public installNotAllow:Z

.field public installSourceTips:Lcom/miui/packageInstaller/model/InstallSourceTips;

.field public isSingletonOnce:Z

.field public miPackageName:Ljava/lang/String;

.field public onlineFailButtonTip:Lcom/miui/packageInstaller/model/OnlineFailButtonTip;

.field public openButton:Z

.field public originalBundleAppInfo:Lcom/miui/packageInstaller/model/OriginalBundleAppInfo;

.field public positiveButtonTip:Lcom/miui/packageInstaller/model/PositiveButtonRules;

.field public rc:Lcom/miui/packageInstaller/model/RiskControlConfig;

.field public riskApp:Z

.field public riskAppProtectionGuardTip:Lcom/miui/packageInstaller/model/RiskAppProtectionGuardTip;

.field public safeModeTip:Lcom/miui/packageInstaller/model/SafeModeTip;

.field public safeType:Ljava/lang/String;

.field public secureInstallTip:Lcom/miui/packageInstaller/model/Tips;

.field public secureWarningTip:Lcom/miui/packageInstaller/model/WarningCardInfo;

.field public seniorsApk:Z

.field public showAdsAfter:Z

.field public showAdsBefore:Z

.field public showSafeModeTip:Z

.field public singletonAuthShowAdsAfter:Z

.field public storeListed:Z

.field public testConfig:Lcom/miui/packageInstaller/model/InterceptAbTestConfig;

.field public uiConfig:Lcom/miui/packageInstaller/model/UiConfig;

.field public unOpenSafeModeText:Ljava/lang/String;

.field public useSystemAppRules:Z

.field public verifyAccount:I


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/packageInstaller/model/CloudParams;->showAdsBefore:Z

    iput-boolean v0, p0, Lcom/miui/packageInstaller/model/CloudParams;->showAdsAfter:Z

    iput-boolean v0, p0, Lcom/miui/packageInstaller/model/CloudParams;->singletonAuthShowAdsAfter:Z

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/miui/packageInstaller/model/CloudParams;->useSystemAppRules:Z

    iput-boolean v1, p0, Lcom/miui/packageInstaller/model/CloudParams;->allowHighLight:Z

    iput-boolean v1, p0, Lcom/miui/packageInstaller/model/CloudParams;->backgroundInstall:Z

    iput-boolean v1, p0, Lcom/miui/packageInstaller/model/CloudParams;->adaptiveApp:Z

    iput-boolean v1, p0, Lcom/miui/packageInstaller/model/CloudParams;->storeListed:Z

    iput-boolean v1, p0, Lcom/miui/packageInstaller/model/CloudParams;->bundleApp:Z

    iput-boolean v1, p0, Lcom/miui/packageInstaller/model/CloudParams;->isSingletonOnce:Z

    iput-boolean v0, p0, Lcom/miui/packageInstaller/model/CloudParams;->openButton:Z

    return-void
.end method
