.class public final Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;
.super Lq2/b;

# interfaces
.implements Lcom/miui/packageInstaller/view/ServiceModeItemView$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity$a;,
        Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity$b;
    }
.end annotation


# static fields
.field public static final M:Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity$a;


# instance fields
.field private A:Landroid/widget/TextView;

.field private B:Landroid/widget/ImageView;

.field private C:Landroid/widget/LinearLayout;

.field private D:Landroidx/core/widget/NestedScrollView;

.field private E:Lcom/miui/packageInstaller/view/ServiceModeItemView;

.field private F:Lcom/miui/packageInstaller/view/ServiceModeItemView;

.field private G:Lcom/miui/packageInstaller/view/ServiceModeItemView;

.field private H:Lcom/miui/packageInstaller/view/ServiceModeItemView;

.field private I:Lcom/miui/packageInstaller/view/ServiceModeItemView;

.field private J:Lcom/miui/packageInstaller/view/ServiceModeItemView;

.field private K:I

.field private L:I

.field private u:Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity$b;

.field private v:Landroid/view/ViewGroup;

.field private w:Landroid/view/View;

.field private x:Landroid/view/View;

.field private y:Landroid/widget/TextView;

.field private z:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity$a;-><init>(Lm8/g;)V

    sput-object v0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->M:Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lq2/b;-><init>()V

    const/16 v0, 0xff

    iput v0, p0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->L:I

    return-void
.end method

.method public static synthetic J0(Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->N0(Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic K0(Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;Landroid/view/View;IIII)V
    .locals 0

    invoke-static/range {p0 .. p5}, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->O0(Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;Landroid/view/View;IIII)V

    return-void
.end method

.method private static final N0(Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;Landroid/view/View;)V
    .locals 2

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Lp5/b;

    const-string v0, "page_back_btn"

    const-string v1, "button"

    invoke-direct {p1, v0, v1, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    const-string v0, "back_type"

    const-string v1, "click_icon"

    invoke-virtual {p1, v0, v1}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p1

    invoke-virtual {p1}, Lp5/f;->c()Z

    invoke-virtual {p0}, Lmiuix/appcompat/app/j;->finish()V

    return-void
.end method

.method private static final O0(Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;Landroid/view/View;IIII)V
    .locals 3

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->x:Landroid/view/View;

    const-string p2, "backIcon"

    const/4 p4, 0x0

    if-nez p1, :cond_0

    invoke-static {p2}, Lm8/i;->s(Ljava/lang/String;)V

    move-object p1, p4

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result p1

    const-string p5, "tvTitle"

    const-string v0, "titleView"

    const/16 v1, 0xff

    const/4 v2, 0x0

    if-le p3, p1, :cond_2

    iput v1, p0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->L:I

    iget-object p1, p0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->y:Landroid/widget/TextView;

    if-nez p1, :cond_1

    invoke-static {p5}, Lm8/i;->s(Ljava/lang/String;)V

    move-object p1, p4

    :cond_1
    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_2
    iget-object p1, p0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->w:Landroid/view/View;

    if-nez p1, :cond_3

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object p1, p4

    :cond_3
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result p1

    mul-int/2addr p3, p1

    iget-object p1, p0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->x:Landroid/view/View;

    if-nez p1, :cond_4

    invoke-static {p2}, Lm8/i;->s(Ljava/lang/String;)V

    move-object p1, p4

    :cond_4
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result p1

    div-int/2addr p3, p1

    add-int/2addr p3, v2

    iput p3, p0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->L:I

    iget-object p1, p0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->y:Landroid/widget/TextView;

    if-nez p1, :cond_5

    invoke-static {p5}, Lm8/i;->s(Ljava/lang/String;)V

    move-object p1, p4

    :cond_5
    const/4 p2, 0x4

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    invoke-direct {p0, p0}, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->P0(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_7

    iget-object p1, p0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->w:Landroid/view/View;

    if-nez p1, :cond_6

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_1

    :cond_6
    move-object p4, p1

    :goto_1
    iget p0, p0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->L:I

    invoke-static {p0, v2, v2, v2}, Landroid/graphics/Color;->argb(IIII)I

    move-result p0

    goto :goto_3

    :cond_7
    iget-object p1, p0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->w:Landroid/view/View;

    if-nez p1, :cond_8

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_2

    :cond_8
    move-object p4, p1

    :goto_2
    iget p0, p0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->L:I

    invoke-static {p0, v1, v1, v1}, Landroid/graphics/Color;->argb(IIII)I

    move-result p0

    :goto_3
    invoke-virtual {p4, p0}, Landroid/view/View;->setBackgroundColor(I)V

    return-void
.end method

.method private final P0(Landroid/content/Context;)Z
    .locals 2

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object p1

    if-nez p1, :cond_1

    return v0

    :cond_1
    iget p1, p1, Landroid/content/res/Configuration;->uiMode:I

    and-int/lit8 p1, p1, 0x30

    const/16 v1, 0x20

    if-ne p1, v1, :cond_2

    const/4 v0, 0x1

    :cond_2
    return v0
.end method

.method private final Q0()V
    .locals 13

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->u:Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity$b;

    sget-object v1, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity$b;->a:Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity$b;

    const-string v2, "mAppErrorInstallView"

    const-string v3, "resources.getString(R.st\u2026pp_error_install_summary)"

    const-string v4, "resources.getString(R.st\u2026e_mode_app_error_install)"

    const v5, 0x7f11033f

    const-string v6, "mRiskAppDownloadView"

    const-string v7, "resources.getString(R.st\u2026isk_app_download_summary)"

    const-string v8, "modeSubTitle"

    const-string v9, "tvTitle"

    const-string v10, "modeTitle"

    const/4 v11, 0x0

    if-ne v0, v1, :cond_5

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->z:Landroid/widget/TextView;

    if-nez v0, :cond_0

    invoke-static {v10}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v11

    :cond_0
    const v10, 0x7f110158

    invoke-virtual {p0, v10}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v0, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->y:Landroid/widget/TextView;

    if-nez v0, :cond_1

    invoke-static {v9}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v11

    :cond_1
    invoke-virtual {p0, v10}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->A:Landroid/widget/TextView;

    if-nez v0, :cond_2

    invoke-static {v8}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v11

    :cond_2
    const v8, 0x7f110159

    invoke-virtual {p0, v8}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->E:Lcom/miui/packageInstaller/view/ServiceModeItemView;

    if-nez v0, :cond_3

    invoke-static {v6}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v11

    :cond_3
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v8, 0x7f110348

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v8, "resources.getString(R.st\u2026e_mode_risk_app_download)"

    invoke-static {v6, v8}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f110349

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8, v7}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v6, v8}, Lcom/miui/packageInstaller/view/ServiceModeItemView;->E(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->F:Lcom/miui/packageInstaller/view/ServiceModeItemView;

    if-nez v0, :cond_4

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    move-object v11, v0

    :goto_0
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v4}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f110340

    goto/16 :goto_2

    :cond_5
    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->z:Landroid/widget/TextView;

    if-nez v0, :cond_6

    invoke-static {v10}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v11

    :cond_6
    const v10, 0x7f1100f7

    invoke-virtual {p0, v10}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v0, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->y:Landroid/widget/TextView;

    if-nez v0, :cond_7

    invoke-static {v9}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v11

    :cond_7
    invoke-virtual {p0, v10}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->A:Landroid/widget/TextView;

    if-nez v0, :cond_8

    invoke-static {v8}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v11

    :cond_8
    const v8, 0x7f1100f8

    invoke-virtual {p0, v8}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->E:Lcom/miui/packageInstaller/view/ServiceModeItemView;

    if-nez v0, :cond_9

    invoke-static {v6}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v11

    :cond_9
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v8, 0x7f110344

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v8, "resources.getString(R.st\u2026_elder_risk_app_download)"

    invoke-static {v6, v8}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f110345

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8, v7}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v6, v8}, Lcom/miui/packageInstaller/view/ServiceModeItemView;->E(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->F:Lcom/miui/packageInstaller/view/ServiceModeItemView;

    if-nez v0, :cond_a

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_1

    :cond_a
    move-object v11, v0

    :goto_1
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v4}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f110343

    :goto_2
    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v3}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v11, v0, v2}, Lcom/miui/packageInstaller/view/ServiceModeItemView;->E(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->B:Landroid/widget/ImageView;

    if-eqz v0, :cond_d

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    const-string v3, "null cannot be cast to non-null type androidx.constraintlayout.widget.ConstraintLayout.LayoutParams"

    invoke-static {v2, v3}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Landroidx/constraintlayout/widget/ConstraintLayout$b;

    iget-object v3, p0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->u:Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity$b;

    if-ne v3, v1, :cond_c

    const v1, 0x7f080594

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result v1

    if-eqz v1, :cond_b

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0700b4

    goto :goto_3

    :cond_b
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0700c8

    :goto_3
    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, v2, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, v2, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f07011c

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f070121

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, v2, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    goto :goto_4

    :cond_c
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0700f6

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0700d3

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, v2, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0700d2

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, v2, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, v2, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    invoke-static {p0}, Lcom/bumptech/glide/b;->u(Landroidx/fragment/app/e;)Lcom/bumptech/glide/k;

    move-result-object v1

    const v3, 0x7f08014f

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/bumptech/glide/k;->s(Ljava/lang/Integer;)Lcom/bumptech/glide/j;

    move-result-object v1

    invoke-virtual {v1}, Lm3/a;->e()Lm3/a;

    move-result-object v1

    check-cast v1, Lcom/bumptech/glide/j;

    invoke-virtual {v1, v0}, Lcom/bumptech/glide/j;->w0(Landroid/widget/ImageView;)Ln3/i;

    :goto_4
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_d
    return-void
.end method


# virtual methods
.method public E0()Ljava/lang/String;
    .locals 1

    const-string v0, "submode_setting"

    return-object v0
.end method

.method public final L0()V
    .locals 2

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "mode_type"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity$b;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->u:Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity$b;

    return-void
.end method

.method public final M0()V
    .locals 9

    sget-object v0, Lf6/v;->a:Lf6/v$a;

    invoke-virtual {v0, p0}, Lf6/v$a;->a(Landroid/app/Activity;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x2000

    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    invoke-static {p0}, Lcom/android/packageinstaller/utils/u;->b(Landroid/app/Activity;)I

    move-result v0

    iput v0, p0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->K:I

    const v0, 0x7f0a02d2

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.root_layout)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->v:Landroid/view/ViewGroup;

    const v0, 0x7f0a0089

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.back_icon)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->x:Landroid/view/View;

    const v0, 0x7f0a0387

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.title_view)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->w:Landroid/view/View;

    const v0, 0x7f0a03d3

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.tv_title)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->y:Landroid/widget/TextView;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const-string v0, "tvTitle"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_0
    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->x:Landroid/view/View;

    if-nez v0, :cond_1

    const-string v0, "backIcon"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_1
    new-instance v2, Ld6/q0;

    invoke-direct {v2, p0}, Ld6/q0;-><init>(Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0a03cc

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v2, "findViewById(R.id.tv_service_mode_title)"

    invoke-static {v0, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->z:Landroid/widget/TextView;

    const v0, 0x7f0a03cb

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v2, "findViewById(R.id.tv_service_mode_subtitle)"

    invoke-static {v0, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->A:Landroid/widget/TextView;

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->v()Z

    move-result v0

    if-nez v0, :cond_3

    const v0, 0x7f0a01d7

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->B:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->v:Landroid/view/ViewGroup;

    if-nez v0, :cond_2

    const-string v0, "rootLayout"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_2
    new-instance v2, Lcom/miui/packageInstaller/view/i;

    const v3, 0x7f080182

    invoke-direct {v2, v3}, Lcom/miui/packageInstaller/view/i;-><init>(I)V

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setBackground(Landroid/graphics/drawable/Drawable;)V

    :cond_3
    const v0, 0x7f0a0308

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v2, "findViewById(R.id.service_mode_content)"

    invoke-static {v0, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->C:Landroid/widget/LinearLayout;

    const v0, 0x7f0a034d

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v2, "findViewById(R.id.sv_service_mode)"

    invoke-static {v0, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroidx/core/widget/NestedScrollView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->D:Landroidx/core/widget/NestedScrollView;

    const v0, 0x7f0a01c2

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v2, "findViewById(R.id.item_risk_app_download)"

    invoke-static {v0, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/miui/packageInstaller/view/ServiceModeItemView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->E:Lcom/miui/packageInstaller/view/ServiceModeItemView;

    const v0, 0x7f0a01bb

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v2, "findViewById(R.id.item_app_error_install)"

    invoke-static {v0, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/miui/packageInstaller/view/ServiceModeItemView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->F:Lcom/miui/packageInstaller/view/ServiceModeItemView;

    const v0, 0x7f0a01bc

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v2, "findViewById(R.id.item_app_security_detection)"

    invoke-static {v0, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/miui/packageInstaller/view/ServiceModeItemView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->G:Lcom/miui/packageInstaller/view/ServiceModeItemView;

    const v0, 0x7f0a01c4

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v2, "findViewById(R.id.item_risk_app_remind)"

    invoke-static {v0, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/miui/packageInstaller/view/ServiceModeItemView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->H:Lcom/miui/packageInstaller/view/ServiceModeItemView;

    const v0, 0x7f0a01c1

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v2, "findViewById(R.id.item_pay_protection)"

    invoke-static {v0, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/miui/packageInstaller/view/ServiceModeItemView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->I:Lcom/miui/packageInstaller/view/ServiceModeItemView;

    const v0, 0x7f0a01c5

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v2, "findViewById(R.id.item_telecom_fraud_protection)"

    invoke-static {v0, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/miui/packageInstaller/view/ServiceModeItemView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->J:Lcom/miui/packageInstaller/view/ServiceModeItemView;

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->E:Lcom/miui/packageInstaller/view/ServiceModeItemView;

    if-nez v0, :cond_4

    const-string v0, "mRiskAppDownloadView"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_4
    invoke-virtual {v0, p0}, Lcom/miui/packageInstaller/view/ServiceModeItemView;->setOnCheckedChangeListener(Lcom/miui/packageInstaller/view/ServiceModeItemView$a;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->F:Lcom/miui/packageInstaller/view/ServiceModeItemView;

    if-nez v0, :cond_5

    const-string v0, "mAppErrorInstallView"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_5
    invoke-virtual {v0, p0}, Lcom/miui/packageInstaller/view/ServiceModeItemView;->setOnCheckedChangeListener(Lcom/miui/packageInstaller/view/ServiceModeItemView$a;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->G:Lcom/miui/packageInstaller/view/ServiceModeItemView;

    if-nez v0, :cond_6

    const-string v0, "mAppSecurityDetectionView"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_6
    invoke-virtual {v0, p0}, Lcom/miui/packageInstaller/view/ServiceModeItemView;->setOnCheckedChangeListener(Lcom/miui/packageInstaller/view/ServiceModeItemView$a;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->H:Lcom/miui/packageInstaller/view/ServiceModeItemView;

    if-nez v0, :cond_7

    const-string v0, "mRiskAppRemindView"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_7
    invoke-virtual {v0, p0}, Lcom/miui/packageInstaller/view/ServiceModeItemView;->setOnCheckedChangeListener(Lcom/miui/packageInstaller/view/ServiceModeItemView$a;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->I:Lcom/miui/packageInstaller/view/ServiceModeItemView;

    if-nez v0, :cond_8

    const-string v0, "mPayProtectionView"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_8
    invoke-virtual {v0, p0}, Lcom/miui/packageInstaller/view/ServiceModeItemView;->setOnCheckedChangeListener(Lcom/miui/packageInstaller/view/ServiceModeItemView$a;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->J:Lcom/miui/packageInstaller/view/ServiceModeItemView;

    if-nez v0, :cond_9

    const-string v0, "mTelecomFraudProtectionView"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_9
    invoke-virtual {v0, p0}, Lcom/miui/packageInstaller/view/ServiceModeItemView;->setOnCheckedChangeListener(Lcom/miui/packageInstaller/view/ServiceModeItemView$a;)V

    new-instance v0, Lp5/g;

    const-string v2, "submode_setting_switch"

    const-string v3, "switch"

    invoke-direct {v0, v2, v3, p0}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    const-string v4, "switch_name"

    const-string v5, "risk_app_warning"

    invoke-virtual {v0, v4, v5}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object v0

    invoke-static {p0}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->getSafeModelRiskAppDownloadWarn(Landroid/content/Context;)Z

    move-result v5

    const-string v6, "off"

    const-string v7, "on"

    if-eqz v5, :cond_a

    move-object v5, v7

    goto :goto_0

    :cond_a
    move-object v5, v6

    :goto_0
    const-string v8, "switch_action"

    invoke-virtual {v0, v8, v5}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object v0

    invoke-virtual {v0}, Lp5/f;->c()Z

    new-instance v0, Lp5/g;

    invoke-direct {v0, v2, v3, p0}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    const-string v5, "ad_app_warning"

    invoke-virtual {v0, v4, v5}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object v0

    invoke-static {p0}, Lm2/b;->g(Landroid/content/Context;)Lm2/b;

    move-result-object v5

    invoke-virtual {v5}, Lm2/b;->l()Z

    move-result v5

    if-eqz v5, :cond_b

    move-object v5, v7

    goto :goto_1

    :cond_b
    move-object v5, v6

    :goto_1
    invoke-virtual {v0, v8, v5}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object v0

    invoke-virtual {v0}, Lp5/f;->c()Z

    new-instance v0, Lp5/g;

    invoke-direct {v0, v2, v3, p0}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    const-string v5, "app_check"

    invoke-virtual {v0, v4, v5}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object v0

    invoke-virtual {v0, v8, v7}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object v0

    invoke-virtual {v0}, Lp5/f;->c()Z

    new-instance v0, Lp5/g;

    invoke-direct {v0, v2, v3, p0}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    const-string v5, "use_risk_app_warning"

    invoke-virtual {v0, v4, v5}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object v0

    invoke-static {p0}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->getSafeModelRiskAppRemindWarn(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_c

    move-object v6, v7

    :cond_c
    invoke-virtual {v0, v8, v6}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object v0

    invoke-virtual {v0}, Lp5/f;->c()Z

    new-instance v0, Lp5/g;

    invoke-direct {v0, v2, v3, p0}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    const-string v5, "pay_protect"

    invoke-virtual {v0, v4, v5}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object v0

    invoke-virtual {v0, v8, v7}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object v0

    invoke-virtual {v0}, Lp5/f;->c()Z

    new-instance v0, Lp5/g;

    invoke-direct {v0, v2, v3, p0}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    const-string v2, "tel_fraud_protect"

    invoke-virtual {v0, v4, v2}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object v0

    invoke-virtual {v0, v8, v7}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object v0

    invoke-virtual {v0}, Lp5/f;->c()Z

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->D:Landroidx/core/widget/NestedScrollView;

    const-string v2, "mScrollView"

    if-nez v0, :cond_d

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_d
    iget v3, p0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->K:I

    const/4 v4, 0x0

    invoke-virtual {v0, v4, v3, v4, v4}, Landroid/widget/FrameLayout;->setPadding(IIII)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->w:Landroid/view/View;

    if-nez v0, :cond_e

    const-string v0, "titleView"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_e
    iget v3, p0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->K:I

    invoke-virtual {v0, v4, v3, v4, v4}, Landroid/view/View;->setPadding(IIII)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->D:Landroidx/core/widget/NestedScrollView;

    if-nez v0, :cond_f

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_2

    :cond_f
    move-object v1, v0

    :goto_2
    new-instance v0, Ld6/r0;

    invoke-direct {v0, p0}, Ld6/r0;-><init>(Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;)V

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setOnScrollChangeListener(Landroid/view/View$OnScrollChangeListener;)V

    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->Q0()V

    invoke-virtual {p0}, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->R0()V

    return-void
.end method

.method public final R0()V
    .locals 5

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->C:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    const-string v2, "itemLayout"

    if-nez v0, :cond_0

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_0
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    const-string v3, "null cannot be cast to non-null type androidx.constraintlayout.widget.ConstraintLayout.LayoutParams"

    invoke-static {v0, v3}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroidx/constraintlayout/widget/ConstraintLayout$b;

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0700fe

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f070110

    :goto_0
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    iput v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    iget-object v3, p0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->C:Landroid/widget/LinearLayout;

    if-nez v3, :cond_2

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    move-object v1, v3

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public onBackPressed()V
    .locals 3

    invoke-super {p0}, Lmiuix/appcompat/app/j;->onBackPressed()V

    new-instance v0, Lp5/b;

    const-string v1, "page_back_btn"

    const-string v2, "button"

    invoke-direct {v0, v1, v2, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    const-string v1, "back_type"

    const-string v2, "system"

    invoke-virtual {v0, v1, v2}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object v0

    invoke-virtual {v0}, Lp5/f;->c()Z

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Lq2/b;->onCreate(Landroid/os/Bundle;)V

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->v()Z

    move-result p1

    if-eqz p1, :cond_0

    const p1, 0x7f0d01af

    goto :goto_0

    :cond_0
    const p1, 0x7f0d01ad

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/android/packageinstaller/utils/g;->v()Z

    move-result p1

    if-eqz p1, :cond_2

    const p1, 0x7f0d01ae

    goto :goto_0

    :cond_2
    const p1, 0x7f0d01ac

    :goto_0
    invoke-virtual {p0, p1}, Lmiuix/appcompat/app/j;->setContentView(I)V

    invoke-virtual {p0}, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->L0()V

    invoke-virtual {p0}, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->M0()V

    return-void
.end method

.method protected onResume()V
    .locals 3

    invoke-super {p0}, Landroidx/fragment/app/e;->onResume()V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->E:Lcom/miui/packageInstaller/view/ServiceModeItemView;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const-string v0, "mRiskAppDownloadView"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_0
    invoke-static {p0}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->getSafeModelRiskAppDownloadWarn(Landroid/content/Context;)Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/miui/packageInstaller/view/ServiceModeItemView;->setChecked(Z)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->F:Lcom/miui/packageInstaller/view/ServiceModeItemView;

    if-nez v0, :cond_1

    const-string v0, "mAppErrorInstallView"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_1
    invoke-static {p0}, Lm2/b;->g(Landroid/content/Context;)Lm2/b;

    move-result-object v2

    invoke-virtual {v2}, Lm2/b;->l()Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/miui/packageInstaller/view/ServiceModeItemView;->setChecked(Z)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->H:Lcom/miui/packageInstaller/view/ServiceModeItemView;

    if-nez v0, :cond_2

    const-string v0, "mRiskAppRemindView"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_2
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/miui/packageInstaller/view/ServiceModeItemView;->setCheckEnabled(Z)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->G:Lcom/miui/packageInstaller/view/ServiceModeItemView;

    if-nez v0, :cond_3

    const-string v0, "mAppSecurityDetectionView"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_3
    invoke-virtual {v0, v2}, Lcom/miui/packageInstaller/view/ServiceModeItemView;->setCheckEnabled(Z)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->I:Lcom/miui/packageInstaller/view/ServiceModeItemView;

    if-nez v0, :cond_4

    const-string v0, "mPayProtectionView"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_4
    invoke-virtual {v0, v2}, Lcom/miui/packageInstaller/view/ServiceModeItemView;->setCheckEnabled(Z)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->J:Lcom/miui/packageInstaller/view/ServiceModeItemView;

    if-nez v0, :cond_5

    const-string v0, "mTelecomFraudProtectionView"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    move-object v1, v0

    :goto_0
    invoke-virtual {v1, v2}, Lcom/miui/packageInstaller/view/ServiceModeItemView;->setCheckEnabled(Z)V

    return-void
.end method

.method public w(Lcom/miui/packageInstaller/view/ServiceModeItemView;Z)V
    .locals 8

    const-string v0, "view"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->E:Lcom/miui/packageInstaller/view/ServiceModeItemView;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const-string v0, "mRiskAppDownloadView"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_0
    invoke-static {p1, v0}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const-string v2, "on"

    const-string v3, "off"

    const-string v4, "switch_action"

    const-string v5, "switch_name"

    const-string v6, "switch"

    const-string v7, "submode_setting_switch"

    if-eqz v0, :cond_2

    new-instance p1, Lp5/b;

    invoke-direct {p1, v7, v6, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    const-string v0, "risk_app_warning"

    invoke-virtual {p1, v5, v0}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p1

    if-eqz p2, :cond_1

    goto :goto_0

    :cond_1
    move-object v2, v3

    :goto_0
    invoke-virtual {p1, v4, v2}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p1

    invoke-virtual {p1}, Lp5/f;->c()Z

    invoke-static {p0, p2}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->setSafeModelRiskAppDownloadWarn(Landroid/content/Context;Z)V

    goto :goto_4

    :cond_2
    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->F:Lcom/miui/packageInstaller/view/ServiceModeItemView;

    if-nez v0, :cond_3

    const-string v0, "mAppErrorInstallView"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_3
    invoke-static {p1, v0}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    new-instance p1, Lp5/b;

    invoke-direct {p1, v7, v6, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    const-string v0, "ad_app_warning"

    invoke-virtual {p1, v5, v0}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p1

    if-eqz p2, :cond_4

    goto :goto_1

    :cond_4
    move-object v2, v3

    :goto_1
    invoke-virtual {p1, v4, v2}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p1

    invoke-virtual {p1}, Lp5/f;->c()Z

    invoke-static {p0}, Lm2/b;->g(Landroid/content/Context;)Lm2/b;

    move-result-object p1

    invoke-virtual {p1, p2}, Lm2/b;->K(Z)V

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/ServiceModeActivity;->H:Lcom/miui/packageInstaller/view/ServiceModeItemView;

    if-nez v0, :cond_6

    const-string v0, "mRiskAppRemindView"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_2

    :cond_6
    move-object v1, v0

    :goto_2
    invoke-static {p1, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_8

    invoke-static {p0, p2}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->setSafeModelRiskAppRemindWarn(Landroid/content/Context;Z)V

    new-instance p1, Lp5/b;

    invoke-direct {p1, v7, v6, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    const-string v0, "use_risk_app_warning"

    invoke-virtual {p1, v5, v0}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p1

    if-eqz p2, :cond_7

    goto :goto_3

    :cond_7
    move-object v2, v3

    :goto_3
    invoke-virtual {p1, v4, v2}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p1

    invoke-virtual {p1}, Lp5/f;->c()Z

    :cond_8
    :goto_4
    return-void
.end method
