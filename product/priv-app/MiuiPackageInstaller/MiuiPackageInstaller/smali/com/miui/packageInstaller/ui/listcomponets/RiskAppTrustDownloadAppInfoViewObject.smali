.class public final Lcom/miui/packageInstaller/ui/listcomponets/RiskAppTrustDownloadAppInfoViewObject;
.super Lm6/a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/packageInstaller/ui/listcomponets/RiskAppTrustDownloadAppInfoViewObject$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lm6/a<",
        "Lcom/miui/packageInstaller/ui/listcomponets/RiskAppTrustDownloadAppInfoViewObject$ViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private final l:Lr6/c;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lr6/c;Ll6/c;Lm6/b;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "riskAppInfo"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2, p3, p4}, Lm6/a;-><init>(Landroid/content/Context;Ljava/lang/Object;Ll6/c;Lm6/b;)V

    iput-object p2, p0, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppTrustDownloadAppInfoViewObject;->l:Lr6/c;

    return-void
.end method


# virtual methods
.method public k()I
    .locals 1

    const v0, 0x7f0d014b

    return v0
.end method

.method public bridge synthetic o(Landroidx/recyclerview/widget/RecyclerView$d0;)V
    .locals 0

    check-cast p1, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppTrustDownloadAppInfoViewObject$ViewHolder;

    invoke-virtual {p0, p1}, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppTrustDownloadAppInfoViewObject;->z(Lcom/miui/packageInstaller/ui/listcomponets/RiskAppTrustDownloadAppInfoViewObject$ViewHolder;)V

    return-void
.end method

.method public z(Lcom/miui/packageInstaller/ui/listcomponets/RiskAppTrustDownloadAppInfoViewObject$ViewHolder;)V
    .locals 9

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppTrustDownloadAppInfoViewObject$ViewHolder;->getTvAppName()Landroid/widget/TextView;

    move-result-object v1

    goto :goto_0

    :cond_0
    move-object v1, v0

    :goto_0
    if-nez v1, :cond_1

    goto :goto_1

    :cond_1
    iget-object v2, p0, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppTrustDownloadAppInfoViewObject;->l:Lr6/c;

    invoke-virtual {v2}, Lr6/c;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz p1, :cond_5

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppTrustDownloadAppInfoViewObject$ViewHolder;->getIvAppIcon()Landroid/widget/ImageView;

    move-result-object v3

    if-eqz v3, :cond_5

    iget-object v4, p0, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppTrustDownloadAppInfoViewObject;->l:Lr6/c;

    invoke-virtual {v4}, Lr6/c;->b()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-static {v4}, Lu8/g;->l(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    goto :goto_2

    :cond_2
    move v4, v2

    goto :goto_3

    :cond_3
    :goto_2
    move v4, v1

    :goto_3
    if-nez v4, :cond_4

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/bumptech/glide/b;->t(Landroid/content/Context;)Lcom/bumptech/glide/k;

    move-result-object v4

    iget-object v5, p0, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppTrustDownloadAppInfoViewObject;->l:Lr6/c;

    invoke-virtual {v5}, Lr6/c;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/bumptech/glide/k;->t(Ljava/lang/String;)Lcom/bumptech/glide/j;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/bumptech/glide/j;->w0(Landroid/widget/ImageView;)Ln3/i;

    move-result-object v3

    const-string v4, "{\n                Glide.\u2026l).into(it)\n            }"

    invoke-static {v3, v4}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_4

    :cond_4
    const v4, 0x7f080146

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    sget-object v3, La8/v;->a:La8/v;

    :cond_5
    :goto_4
    iget-object v3, p0, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppTrustDownloadAppInfoViewObject;->l:Lr6/c;

    invoke-virtual {v3}, Lr6/c;->k()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_7

    invoke-static {v3}, Lu8/g;->l(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_6

    goto :goto_5

    :cond_6
    move v3, v2

    goto :goto_6

    :cond_7
    :goto_5
    move v3, v1

    :goto_6
    const/16 v4, 0x8

    if-nez v3, :cond_c

    if-eqz p1, :cond_8

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppTrustDownloadAppInfoViewObject$ViewHolder;->getTvAppVersion()Landroid/widget/TextView;

    move-result-object v3

    goto :goto_7

    :cond_8
    move-object v3, v0

    :goto_7
    if-nez v3, :cond_9

    goto :goto_8

    :cond_9
    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f110043

    new-array v7, v1, [Ljava/lang/Object;

    iget-object v8, p0, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppTrustDownloadAppInfoViewObject;->l:Lr6/c;

    invoke-virtual {v8}, Lr6/c;->k()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v2

    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_8
    if-eqz p1, :cond_a

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppTrustDownloadAppInfoViewObject$ViewHolder;->getTvAppVersion()Landroid/widget/TextView;

    move-result-object v3

    goto :goto_9

    :cond_a
    move-object v3, v0

    :goto_9
    if-nez v3, :cond_b

    goto :goto_c

    :cond_b
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_c

    :cond_c
    if-eqz p1, :cond_d

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppTrustDownloadAppInfoViewObject$ViewHolder;->getTvAppVersion()Landroid/widget/TextView;

    move-result-object v3

    goto :goto_a

    :cond_d
    move-object v3, v0

    :goto_a
    if-nez v3, :cond_e

    goto :goto_b

    :cond_e
    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_b
    if-eqz p1, :cond_f

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppTrustDownloadAppInfoViewObject$ViewHolder;->getTvInstallSource()Landroid/widget/TextView;

    move-result-object v3

    if-eqz v3, :cond_f

    invoke-virtual {v3}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    const-string v6, "null cannot be cast to non-null type androidx.appcompat.widget.LinearLayoutCompat.LayoutParams"

    invoke-static {v5, v6}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v5, Landroidx/appcompat/widget/LinearLayoutCompat$a;

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0700a8

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v6

    iput v6, v5, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_f
    :goto_c
    if-eqz p1, :cond_10

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppTrustDownloadAppInfoViewObject$ViewHolder;->getTvInstallSource()Landroid/widget/TextView;

    move-result-object v3

    goto :goto_d

    :cond_10
    move-object v3, v0

    :goto_d
    if-nez v3, :cond_11

    goto :goto_e

    :cond_11
    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f1102d9

    new-array v7, v1, [Ljava/lang/Object;

    iget-object v8, p0, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppTrustDownloadAppInfoViewObject;->l:Lr6/c;

    invoke-virtual {v8}, Lr6/c;->c()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v2

    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_e
    if-eqz p1, :cond_12

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppTrustDownloadAppInfoViewObject$ViewHolder;->getTvVirusInfo()Landroid/widget/TextView;

    move-result-object v0

    :cond_12
    if-nez v0, :cond_13

    goto :goto_12

    :cond_13
    iget-object v3, p0, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppTrustDownloadAppInfoViewObject;->l:Lr6/c;

    invoke-virtual {v3}, Lr6/c;->d()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_15

    invoke-static {v3}, Lu8/g;->l(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_14

    goto :goto_f

    :cond_14
    move v3, v2

    goto :goto_10

    :cond_15
    :goto_f
    move v3, v1

    :goto_10
    if-nez v3, :cond_16

    iget-object v3, p0, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppTrustDownloadAppInfoViewObject;->l:Lr6/c;

    invoke-virtual {v3}, Lr6/c;->d()Ljava/lang/String;

    move-result-object v3

    goto :goto_11

    :cond_16
    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v3

    const v5, 0x7f1102e4

    invoke-virtual {v3, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    :goto_11
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_12
    if-eqz p1, :cond_1b

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppTrustDownloadAppInfoViewObject$ViewHolder;->getTvTrustStatus()Landroid/widget/TextView;

    move-result-object p1

    if-eqz p1, :cond_1b

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppTrustDownloadAppInfoViewObject;->l:Lr6/c;

    invoke-virtual {v0}, Lr6/c;->i()Ljava/lang/Integer;

    move-result-object v0

    if-nez v0, :cond_17

    goto :goto_14

    :cond_17
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-ne v3, v1, :cond_18

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f1102cf

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f060049

    :goto_13
    invoke-virtual {v0, v1}, Landroid/content/Context;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_16

    :cond_18
    :goto_14
    const/4 v1, 0x2

    if-nez v0, :cond_19

    goto :goto_15

    :cond_19
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v1, :cond_1a

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f1102e2

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f060069

    goto :goto_13

    :cond_1a
    :goto_15
    invoke-virtual {p1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_1b
    :goto_16
    return-void
.end method
