.class public interface abstract Lcom/miui/packageInstaller/model/AdInterface;
.super Ljava/lang/Object;


# virtual methods
.method public abstract getAppChannel()Ljava/lang/String;
.end method

.method public abstract getAppClientId()Ljava/lang/String;
.end method

.method public abstract getAppRef()Ljava/lang/String;
.end method

.method public abstract getAppSignature()Ljava/lang/String;
.end method

.method public abstract getClickMonitorUrls()[Ljava/lang/String;
.end method

.method public abstract getEx()Ljava/lang/String;
.end method

.method public abstract getFloatCardData()Ljava/lang/String;
.end method

.method public abstract getId()J
.end method

.method public abstract getNonce()Ljava/lang/String;
.end method

.method public abstract getPackageName()Ljava/lang/String;
.end method

.method public abstract getRef()Ljava/lang/String;
.end method

.method public abstract getSourcePackage()Ljava/lang/String;
.end method

.method public abstract getViewMonitorUrls()[Ljava/lang/String;
.end method
