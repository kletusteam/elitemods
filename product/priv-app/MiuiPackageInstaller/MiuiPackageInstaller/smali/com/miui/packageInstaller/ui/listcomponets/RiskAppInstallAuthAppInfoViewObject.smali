.class public final Lcom/miui/packageInstaller/ui/listcomponets/RiskAppInstallAuthAppInfoViewObject;
.super Lm6/a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/packageInstaller/ui/listcomponets/RiskAppInstallAuthAppInfoViewObject$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lm6/a<",
        "Lcom/miui/packageInstaller/ui/listcomponets/RiskAppInstallAuthAppInfoViewObject$ViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private final l:Lcom/miui/packageInstaller/model/ApkInfo;

.field private m:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/ApkInfo;Ll6/c;Lm6/b;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mData"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2, p3, p4}, Lm6/a;-><init>(Landroid/content/Context;Ljava/lang/Object;Ll6/c;Lm6/b;)V

    iput-object p2, p0, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppInstallAuthAppInfoViewObject;->l:Lcom/miui/packageInstaller/model/ApkInfo;

    return-void
.end method


# virtual methods
.method public A(Lcom/miui/packageInstaller/ui/listcomponets/RiskAppInstallAuthAppInfoViewObject$ViewHolder;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/miui/packageInstaller/ui/listcomponets/RiskAppInstallAuthAppInfoViewObject$ViewHolder;",
            "Ljava/util/List<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppInstallAuthAppInfoViewObject;->z(Lcom/miui/packageInstaller/ui/listcomponets/RiskAppInstallAuthAppInfoViewObject$ViewHolder;)V

    return-void
.end method

.method public final B(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppInstallAuthAppInfoViewObject;->m:Z

    const p1, 0x7f0a01c3

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p0, p1}, Lm6/a;->n(Ljava/lang/Object;)V

    return-void
.end method

.method public k()I
    .locals 1

    const v0, 0x7f0d0090

    return v0
.end method

.method public bridge synthetic o(Landroidx/recyclerview/widget/RecyclerView$d0;)V
    .locals 0

    check-cast p1, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppInstallAuthAppInfoViewObject$ViewHolder;

    invoke-virtual {p0, p1}, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppInstallAuthAppInfoViewObject;->z(Lcom/miui/packageInstaller/ui/listcomponets/RiskAppInstallAuthAppInfoViewObject$ViewHolder;)V

    return-void
.end method

.method public bridge synthetic p(Landroidx/recyclerview/widget/RecyclerView$d0;Ljava/util/List;)V
    .locals 0

    check-cast p1, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppInstallAuthAppInfoViewObject$ViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppInstallAuthAppInfoViewObject;->A(Lcom/miui/packageInstaller/ui/listcomponets/RiskAppInstallAuthAppInfoViewObject$ViewHolder;Ljava/util/List;)V

    return-void
.end method

.method public z(Lcom/miui/packageInstaller/ui/listcomponets/RiskAppInstallAuthAppInfoViewObject$ViewHolder;)V
    .locals 6

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppInstallAuthAppInfoViewObject$ViewHolder;->getTvAppName()Landroid/widget/TextView;

    move-result-object v1

    goto :goto_0

    :cond_0
    move-object v1, v0

    :goto_0
    if-nez v1, :cond_1

    goto :goto_1

    :cond_1
    iget-object v2, p0, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppInstallAuthAppInfoViewObject;->l:Lcom/miui/packageInstaller/model/ApkInfo;

    invoke-virtual {v2}, Lcom/miui/packageInstaller/model/ApkInfo;->getLabel()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppInstallAuthAppInfoViewObject$ViewHolder;->getTvAppVersion()Landroid/widget/TextView;

    move-result-object v1

    goto :goto_2

    :cond_2
    move-object v1, v0

    :goto_2
    if-nez v1, :cond_3

    goto :goto_3

    :cond_3
    iget-object v2, p0, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppInstallAuthAppInfoViewObject;->l:Lcom/miui/packageInstaller/model/ApkInfo;

    invoke-virtual {v2}, Lcom/miui/packageInstaller/model/ApkInfo;->getVersionName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_3
    if-eqz p1, :cond_4

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppInstallAuthAppInfoViewObject$ViewHolder;->getIvIcon()Landroid/widget/ImageView;

    move-result-object v1

    if-eqz v1, :cond_4

    iget-object v2, p0, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppInstallAuthAppInfoViewObject;->l:Lcom/miui/packageInstaller/model/ApkInfo;

    invoke-virtual {v2}, Lcom/miui/packageInstaller/model/ApkInfo;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_4
    iget-boolean v1, p0, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppInstallAuthAppInfoViewObject;->m:Z

    const/16 v2, 0x8

    if-nez v1, :cond_9

    if-eqz p1, :cond_5

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppInstallAuthAppInfoViewObject$ViewHolder;->getMRiskContentDes()Landroid/view/View;

    move-result-object v1

    goto :goto_4

    :cond_5
    move-object v1, v0

    :goto_4
    if-nez v1, :cond_6

    goto :goto_5

    :cond_6
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    :goto_5
    if-eqz p1, :cond_7

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppInstallAuthAppInfoViewObject$ViewHolder;->getMAuthResultText()Landroid/widget/TextView;

    move-result-object v0

    :cond_7
    if-nez v0, :cond_8

    goto :goto_6

    :cond_8
    const-string p1, ""

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_6

    :cond_9
    if-eqz p1, :cond_a

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppInstallAuthAppInfoViewObject$ViewHolder;->getMContentLayout()Landroid/widget/LinearLayout;

    move-result-object v1

    if-eqz v1, :cond_a

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :cond_a
    const/4 v1, 0x0

    if-eqz p1, :cond_b

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppInstallAuthAppInfoViewObject$ViewHolder;->getMRiskContentDes()Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_b

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_b
    if-eqz p1, :cond_d

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppInstallAuthAppInfoViewObject$ViewHolder;->getMAuthResultText()Landroid/widget/TextView;

    move-result-object p1

    if-eqz p1, :cond_d

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f1102d4

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppInstallAuthAppInfoViewObject;->l:Lcom/miui/packageInstaller/model/ApkInfo;

    if-eqz v5, :cond_c

    invoke-virtual {v5}, Lcom/miui/packageInstaller/model/ApkInfo;->getLabel()Ljava/lang/String;

    move-result-object v0

    :cond_c
    aput-object v0, v4, v1

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_d
    :goto_6
    return-void
.end method
