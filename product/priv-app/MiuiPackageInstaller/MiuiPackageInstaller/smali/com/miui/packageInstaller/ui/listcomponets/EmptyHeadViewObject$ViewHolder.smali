.class public final Lcom/miui/packageInstaller/ui/listcomponets/EmptyHeadViewObject$ViewHolder;
.super Landroidx/recyclerview/widget/RecyclerView$d0;


# instance fields
.field private rootView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    const-string v0, "itemView"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$d0;-><init>(Landroid/view/View;)V

    const v0, 0x7f0a014f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v0, "itemView.findViewById(R.id.empty_root_view)"

    invoke-static {p1, v0}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/EmptyHeadViewObject$ViewHolder;->rootView:Landroid/view/View;

    return-void
.end method


# virtual methods
.method public final getRootView()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/EmptyHeadViewObject$ViewHolder;->rootView:Landroid/view/View;

    return-object v0
.end method

.method public final setRootView(Landroid/view/View;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/EmptyHeadViewObject$ViewHolder;->rootView:Landroid/view/View;

    return-void
.end method
