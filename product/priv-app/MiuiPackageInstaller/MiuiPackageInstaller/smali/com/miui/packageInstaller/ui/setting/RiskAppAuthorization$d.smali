.class public final Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization$d;
.super Ljava/lang/Object;

# interfaces
.implements Lh6/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;->R0(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;

.field final synthetic b:La6/i;

.field final synthetic c:Z


# direct methods
.method constructor <init>(Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;La6/i;Z)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization$d;->a:Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;

    iput-object p2, p0, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization$d;->b:La6/i;

    iput-boolean p3, p0, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization$d;->c:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ILjava/lang/String;)V
    .locals 1

    sget-object p2, Lt5/a;->e:Lt5/a$a;

    invoke-virtual {p2}, Lt5/a$a;->a()I

    move-result p2

    const/4 v0, 0x0

    if-ne p2, p1, :cond_1

    iget-object p1, p0, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization$d;->a:Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;

    invoke-static {p1}, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;->N0(Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;)Lh6/a;

    move-result-object p1

    if-nez p1, :cond_0

    const-string p1, "fingerPrintHelper"

    invoke-static {p1}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 p1, 0x0

    :cond_0
    invoke-virtual {p1}, Lh6/a;->a()V

    iget-object p1, p0, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization$d;->a:Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;

    const p2, 0x7f110120

    invoke-static {p1, p2, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    iget-object p1, p0, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization$d;->a:Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;

    invoke-static {}, Lh6/c;->a()Landroid/content/Intent;

    move-result-object p2

    const/16 v0, 0x3f2

    invoke-virtual {p1, p2, v0}, Landroidx/activity/ComponentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    iget-object p1, p0, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization$d;->b:La6/i;

    invoke-virtual {p1}, Landroid/app/Dialog;->dismiss()V

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization$d;->a:Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;

    const p2, 0x7f11011f

    invoke-static {p1, p2, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    :goto_0
    return-void
.end method

.method public b()V
    .locals 2

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization$d;->b:La6/i;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization$d;->a:Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;

    iget-boolean v1, p0, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization$d;->c:Z

    invoke-static {v0, v1}, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;->L0(Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;Z)V

    return-void
.end method
