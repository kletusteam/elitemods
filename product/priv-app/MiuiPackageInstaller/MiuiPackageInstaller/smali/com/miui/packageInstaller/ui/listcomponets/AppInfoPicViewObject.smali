.class public final Lcom/miui/packageInstaller/ui/listcomponets/AppInfoPicViewObject;
.super Lm6/a;

# interfaces
.implements Lcom/miui/packageInstaller/ui/listcomponets/v;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/packageInstaller/ui/listcomponets/AppInfoPicViewObject$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lm6/a<",
        "Lcom/miui/packageInstaller/ui/listcomponets/AppInfoPicViewObject$ViewHolder;",
        ">;",
        "Lcom/miui/packageInstaller/ui/listcomponets/v;"
    }
.end annotation


# instance fields
.field private final l:Lcom/miui/packageInstaller/model/CloudParams;

.field private final m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lm6/a<",
            "*>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/CloudParams;Ll6/c;Lm6/b;)V
    .locals 4

    const-string v0, "context"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mData"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2, p3, p4}, Lm6/a;-><init>(Landroid/content/Context;Ljava/lang/Object;Ll6/c;Lm6/b;)V

    iput-object p2, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoPicViewObject;->l:Lcom/miui/packageInstaller/model/CloudParams;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoPicViewObject;->m:Ljava/util/List;

    iget-object v0, p2, Lcom/miui/packageInstaller/model/CloudParams;->appInfo:Lcom/miui/packageInstaller/model/MarketAppInfo;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v2, v0, Lcom/miui/packageInstaller/model/MarketAppInfo;->detailVideoAndScreenshotList:Ljava/util/List;

    goto :goto_0

    :cond_0
    move-object v2, v1

    :goto_0
    if-eqz v2, :cond_2

    if-eqz v0, :cond_1

    iget-object v0, v0, Lcom/miui/packageInstaller/model/MarketAppInfo;->detailVideoAndScreenshotList:Ljava/util/List;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    :cond_1
    invoke-static {v1}, Lm8/i;->c(Ljava/lang/Object;)V

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object p2, p2, Lcom/miui/packageInstaller/model/CloudParams;->appInfo:Lcom/miui/packageInstaller/model/MarketAppInfo;

    iget-object p2, p2, Lcom/miui/packageInstaller/model/MarketAppInfo;->detailVideoAndScreenshotList:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/packageInstaller/model/MarketAppInfo$DetailVideoAndScreenshot;

    iget-object v1, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoPicViewObject;->m:Ljava/util/List;

    new-instance v2, Lcom/miui/packageInstaller/ui/listcomponets/AppPIcViewObject;

    const-string v3, "image"

    invoke-static {v0, v3}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v2, p1, v0, p3, p4}, Lcom/miui/packageInstaller/ui/listcomponets/AppPIcViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/MarketAppInfo$DetailVideoAndScreenshot;Ll6/c;Lm6/b;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/CloudParams;Ll6/c;Lm6/b;ILm8/g;)V
    .locals 0

    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_0

    const/4 p4, 0x0

    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoPicViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/CloudParams;Ll6/c;Lm6/b;)V

    return-void
.end method


# virtual methods
.method public k()I
    .locals 1

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0d0035

    goto :goto_0

    :cond_0
    const v0, 0x7f0d0034

    :goto_0
    return v0
.end method

.method public bridge synthetic o(Landroidx/recyclerview/widget/RecyclerView$d0;)V
    .locals 0

    check-cast p1, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoPicViewObject$ViewHolder;

    invoke-virtual {p0, p1}, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoPicViewObject;->z(Lcom/miui/packageInstaller/ui/listcomponets/AppInfoPicViewObject$ViewHolder;)V

    return-void
.end method

.method public z(Lcom/miui/packageInstaller/ui/listcomponets/AppInfoPicViewObject$ViewHolder;)V
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoPicViewObject$ViewHolder;->getMAdapter()Lj6/b;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lj6/b;->Q()I

    move-result v2

    if-nez v2, :cond_0

    move v1, v0

    :cond_0
    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoPicViewObject;->m:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    xor-int/2addr v0, v1

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoPicViewObject$ViewHolder;->getMAdapter()Lj6/b;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoPicViewObject;->m:Ljava/util/List;

    invoke-virtual {v0, v1}, Lj6/b;->M(Ljava/util/List;)I

    :cond_1
    const/4 v0, 0x0

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoPicViewObject$ViewHolder;->getAppInfoPic()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v1

    goto :goto_0

    :cond_2
    move-object v1, v0

    :goto_0
    if-nez v1, :cond_3

    goto :goto_1

    :cond_3
    if-eqz p1, :cond_4

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoPicViewObject$ViewHolder;->getMAdapter()Lj6/b;

    move-result-object v0

    :cond_4
    invoke-virtual {v1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$g;)V

    :goto_1
    return-void
.end method
