.class public Lcom/miui/packageInstaller/ui/listcomponets/BundleAppViewObject;
.super Lcom/miui/packageInstaller/ui/listcomponets/f0;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/packageInstaller/ui/listcomponets/BundleAppViewObject$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/miui/packageInstaller/ui/listcomponets/f0<",
        "Lcom/miui/packageInstaller/ui/listcomponets/BundleAppViewObject$ViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private final m:Lcom/miui/packageInstaller/model/ApkInfo;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/ApkInfo;Ll6/c;Lm6/b;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "apkInfo"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/miui/packageInstaller/ui/listcomponets/f0;-><init>(Landroid/content/Context;Ljava/lang/Object;Ll6/c;Lm6/b;)V

    iput-object p2, p0, Lcom/miui/packageInstaller/ui/listcomponets/BundleAppViewObject;->m:Lcom/miui/packageInstaller/model/ApkInfo;

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/ApkInfo;Ll6/c;Lm6/b;ILm8/g;)V
    .locals 1

    and-int/lit8 p6, p5, 0x4

    const/4 v0, 0x0

    if-eqz p6, :cond_0

    move-object p3, v0

    :cond_0
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_1

    move-object p4, v0

    :cond_1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/miui/packageInstaller/ui/listcomponets/BundleAppViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/ApkInfo;Ll6/c;Lm6/b;)V

    return-void
.end method

.method private final A()V
    .locals 2

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v0

    instance-of v0, v0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v0

    const-string v1, "null cannot be cast to non-null type com.miui.packageInstaller.PurePackageInstallerActivity"

    invoke-static {v0, v1}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;

    invoke-virtual {v0}, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->b3()V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v0

    instance-of v0, v0, Lm5/z0;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v0

    const-string v1, "null cannot be cast to non-null type com.miui.packageInstaller.InstallerPrepareActivity"

    invoke-static {v0, v1}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lm5/z0;

    invoke-virtual {v0}, Lm5/z0;->a2()V

    :cond_1
    :goto_0
    return-void
.end method

.method public static final synthetic z(Lcom/miui/packageInstaller/ui/listcomponets/BundleAppViewObject;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/listcomponets/BundleAppViewObject;->A()V

    return-void
.end method


# virtual methods
.method public final B()V
    .locals 4

    new-instance v0, Lp5/b;

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v1

    const-string v2, "null cannot be cast to non-null type com.miui.packageInstaller.analytics.IPage"

    invoke-static {v1, v2}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lo5/a;

    const-string v2, "click_to_continue_btn"

    const-string v3, "button"

    invoke-direct {v0, v2, v3, v1}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    return-void
.end method

.method public final C()V
    .locals 4

    new-instance v0, Lp5/g;

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v1

    const-string v2, "null cannot be cast to non-null type com.miui.packageInstaller.analytics.IPage"

    invoke-static {v1, v2}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lo5/a;

    const-string v2, "click_to_continue_btn"

    const-string v3, "button"

    invoke-direct {v0, v2, v3, v1}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    return-void
.end method

.method public D(Lcom/miui/packageInstaller/ui/listcomponets/BundleAppViewObject$ViewHolder;)V
    .locals 18

    move-object/from16 v0, p0

    const-string v1, "viewHolder"

    move-object/from16 v2, p1

    invoke-static {v2, v1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, v0, Lcom/miui/packageInstaller/ui/listcomponets/BundleAppViewObject;->m:Lcom/miui/packageInstaller/model/ApkInfo;

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/ApkInfo;->getCloudParams()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual/range {p0 .. p0}, Lcom/miui/packageInstaller/ui/listcomponets/BundleAppViewObject;->C()V

    iget-boolean v1, v1, Lcom/miui/packageInstaller/model/CloudParams;->storeListed:Z

    const-string v3, "context.getString(R.stri\u2026_on_self_desc_text_click)"

    const v4, 0x7f11004f

    const-string v5, "viewHolder.desText"

    const v6, 0x7f060030

    const-string v7, "format(format, *args)"

    const/4 v8, 0x0

    const/4 v9, 0x1

    if-eqz v1, :cond_0

    invoke-virtual/range {p1 .. p1}, Lcom/miui/packageInstaller/ui/listcomponets/BundleAppViewObject$ViewHolder;->getTitleText()Landroidx/appcompat/widget/AppCompatTextView;

    move-result-object v1

    sget-object v10, Lm8/w;->a:Lm8/w;

    invoke-virtual/range {p0 .. p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v10

    const v11, 0x7f110052

    invoke-virtual {v10, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    const-string v11, "context.getString(R.stri\u2026e_app_on_self_title_text)"

    invoke-static {v10, v11}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    new-array v11, v9, [Ljava/lang/Object;

    iget-object v12, v0, Lcom/miui/packageInstaller/ui/listcomponets/BundleAppViewObject;->m:Lcom/miui/packageInstaller/model/ApkInfo;

    invoke-virtual {v12}, Lcom/miui/packageInstaller/model/ApkInfo;->getLabel()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v11, v8

    invoke-static {v11, v9}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v11

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10, v7}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    sget-object v11, Lf6/b0;->a:Lf6/b0$a;

    invoke-virtual/range {p1 .. p1}, Lcom/miui/packageInstaller/ui/listcomponets/BundleAppViewObject$ViewHolder;->getDesText()Landroidx/appcompat/widget/AppCompatTextView;

    move-result-object v12

    invoke-static {v12, v5}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f11004e

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "context.getString(R.stri\u2026le_app_on_self_desc_text)"

    invoke-static {v1, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    new-array v2, v9, [Ljava/lang/Object;

    iget-object v5, v0, Lcom/miui/packageInstaller/ui/listcomponets/BundleAppViewObject;->m:Lcom/miui/packageInstaller/model/ApkInfo;

    invoke-virtual {v5}, Lcom/miui/packageInstaller/model/ApkInfo;->getLabel()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v8

    invoke-static {v2, v9}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v2

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-static {v13, v7}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-static {v14, v3}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/Context;->getColor(I)I

    move-result v15

    invoke-virtual/range {p0 .. p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/Context;->getColor(I)I

    move-result v16

    new-instance v1, Lcom/miui/packageInstaller/ui/listcomponets/BundleAppViewObject$a;

    invoke-direct {v1, v0}, Lcom/miui/packageInstaller/ui/listcomponets/BundleAppViewObject$a;-><init>(Lcom/miui/packageInstaller/ui/listcomponets/BundleAppViewObject;)V

    goto/16 :goto_0

    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/miui/packageInstaller/ui/listcomponets/BundleAppViewObject$ViewHolder;->getTitleText()Landroidx/appcompat/widget/AppCompatTextView;

    move-result-object v1

    sget-object v10, Lm8/w;->a:Lm8/w;

    invoke-virtual/range {p0 .. p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v10

    const v11, 0x7f110051

    invoke-virtual {v10, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    const-string v11, "context.getString(R.stri\u2026pp_on_self_tips_no_store)"

    invoke-static {v10, v11}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    new-array v11, v9, [Ljava/lang/Object;

    iget-object v12, v0, Lcom/miui/packageInstaller/ui/listcomponets/BundleAppViewObject;->m:Lcom/miui/packageInstaller/model/ApkInfo;

    invoke-virtual {v12}, Lcom/miui/packageInstaller/model/ApkInfo;->getLabel()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v11, v8

    invoke-static {v11, v9}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v11

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10, v7}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    sget-object v11, Lf6/b0;->a:Lf6/b0$a;

    invoke-virtual/range {p1 .. p1}, Lcom/miui/packageInstaller/ui/listcomponets/BundleAppViewObject$ViewHolder;->getDesText()Landroidx/appcompat/widget/AppCompatTextView;

    move-result-object v12

    invoke-static {v12, v5}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f110053

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "context.getString(R.stri\u2026re_app_on_self_desc_text)"

    invoke-static {v1, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x2

    new-array v5, v2, [Ljava/lang/Object;

    iget-object v10, v0, Lcom/miui/packageInstaller/ui/listcomponets/BundleAppViewObject;->m:Lcom/miui/packageInstaller/model/ApkInfo;

    invoke-virtual {v10}, Lcom/miui/packageInstaller/model/ApkInfo;->getLabel()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v5, v8

    iget-object v8, v0, Lcom/miui/packageInstaller/ui/listcomponets/BundleAppViewObject;->m:Lcom/miui/packageInstaller/model/ApkInfo;

    invoke-virtual {v8}, Lcom/miui/packageInstaller/model/ApkInfo;->getLabel()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v5, v9

    invoke-static {v5, v2}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v2

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-static {v13, v7}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-static {v14, v3}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/Context;->getColor(I)I

    move-result v15

    invoke-virtual/range {p0 .. p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/Context;->getColor(I)I

    move-result v16

    new-instance v1, Lcom/miui/packageInstaller/ui/listcomponets/BundleAppViewObject$b;

    invoke-direct {v1, v0}, Lcom/miui/packageInstaller/ui/listcomponets/BundleAppViewObject$b;-><init>(Lcom/miui/packageInstaller/ui/listcomponets/BundleAppViewObject;)V

    :goto_0
    move-object/from16 v17, v1

    invoke-virtual/range {v11 .. v17}, Lf6/b0$a;->a(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;IILf6/b0$a$a;)V

    :cond_1
    return-void
.end method

.method public k()I
    .locals 1

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->isSafeModelEnable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0d003b

    goto :goto_0

    :cond_0
    const v0, 0x7f0d003c

    goto :goto_0

    :cond_1
    const v0, 0x7f0d003a

    :goto_0
    return v0
.end method

.method public bridge synthetic o(Landroidx/recyclerview/widget/RecyclerView$d0;)V
    .locals 0

    check-cast p1, Lcom/miui/packageInstaller/ui/listcomponets/BundleAppViewObject$ViewHolder;

    invoke-virtual {p0, p1}, Lcom/miui/packageInstaller/ui/listcomponets/BundleAppViewObject;->D(Lcom/miui/packageInstaller/ui/listcomponets/BundleAppViewObject$ViewHolder;)V

    return-void
.end method
