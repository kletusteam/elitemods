.class public final Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity;
.super Lmiuix/appcompat/app/j;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity$a;,
        Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity$b;
    }
.end annotation


# static fields
.field public static final v:Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity$a;


# instance fields
.field private q:Ljava/lang/String;

.field private r:Ljava/lang/String;

.field private s:Ljava/lang/String;

.field private t:I

.field private u:Lo5/b;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity$a;-><init>(Lm8/g;)V

    sput-object v0, Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity;->v:Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    invoke-direct {p0}, Lmiuix/appcompat/app/j;-><init>()V

    const/4 v0, 0x2

    iput v0, p0, Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity;->t:I

    new-instance v1, Lo5/b;

    const-string v2, "browser_install"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3, v0, v3}, Lo5/b;-><init>(Ljava/lang/String;Ljava/lang/String;ILm8/g;)V

    iput-object v1, p0, Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity;->u:Lo5/b;

    return-void
.end method

.method public static final synthetic D0(Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity;)Lo5/b;
    .locals 0

    iget-object p0, p0, Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity;->u:Lo5/b;

    return-object p0
.end method

.method public static final synthetic E0(Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity;Lx5/a;)Ljava/lang/String;
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity;->F0(Lx5/a;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private final F0(Lx5/a;)Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity$b;->a:[I

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_3

    const/4 v0, 0x2

    if-eq p1, v0, :cond_2

    const/4 v0, 0x3

    if-eq p1, v0, :cond_1

    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    const-string p1, "none"

    goto :goto_0

    :cond_0
    const-string p1, "face_password"

    goto :goto_0

    :cond_1
    const-string p1, "fingerprint_password"

    goto :goto_0

    :cond_2
    const-string p1, "screen_password"

    goto :goto_0

    :cond_3
    const-string p1, "mi_account"

    :goto_0
    return-object p1
.end method

.method private final G0()V
    .locals 3

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "title"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    const v1, 0x7f11030e

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    :cond_0
    iput-object v1, p0, Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity;->q:Ljava/lang/String;

    const-string v1, "msg"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    const v1, 0x7f1103e3

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    :cond_1
    iput-object v1, p0, Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity;->r:Ljava/lang/String;

    const-string v1, "sub_msg"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity;->s:Ljava/lang/String;

    const/4 v1, 0x2

    const-string v2, "auth_type"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity;->t:I

    return-void
.end method

.method private final H0()V
    .locals 3

    iget v0, p0, Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity;->t:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v2, 0x2

    if-eq v0, v2, :cond_1

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lmiuix/appcompat/app/j;->finish()V

    goto :goto_0

    :cond_0
    sget-object v0, Lx5/a;->f:Lx5/a;

    invoke-direct {p0, v0}, Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity;->J0(Lx5/a;)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    invoke-static {p0, v0, v1, v0}, Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity;->K0(Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity;Lx5/a;ILjava/lang/Object;)V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity;->I0()V

    :goto_0
    return-void
.end method

.method private final I0()V
    .locals 4

    new-instance v0, Lx5/b;

    invoke-direct {v0, p0}, Lx5/b;-><init>(Landroid/app/Activity;)V

    new-instance v1, Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity$c;

    invoke-direct {v1, p0}, Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity$c;-><init>(Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity;)V

    invoke-virtual {v0, v1}, Lx5/b;->b(Ll8/p;)V

    new-instance v0, Lp5/g;

    iget-object v1, p0, Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity;->u:Lo5/b;

    const-string v2, "risk_verifying_popup"

    const-string v3, "popup"

    invoke-direct {v0, v2, v3, v1}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    const-string v1, "verify_method"

    const-string v2, "risk_verify"

    invoke-virtual {v0, v1, v2}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object v0

    invoke-virtual {v0}, Lp5/f;->c()Z

    return-void
.end method

.method private final J0(Lx5/a;)V
    .locals 3

    sget-object v0, Lx5/d;->b:Lx5/d$a;

    invoke-virtual {v0}, Lx5/d$a;->c()Lx5/a;

    move-result-object v1

    sget-object v2, Lx5/a;->g:Lx5/a;

    if-ne v1, v2, :cond_0

    if-nez p1, :cond_0

    const/4 p1, -0x1

    invoke-virtual {p0, p1}, Landroid/app/Activity;->setResult(I)V

    return-void

    :cond_0
    invoke-virtual {v0}, Lx5/d$a;->b()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz p1, :cond_1

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    :cond_1
    new-instance p1, Lx5/j$a;

    invoke-direct {p1, p0}, Lx5/j$a;-><init>(Landroid/app/Activity;)V

    const v1, 0x7f1100ae

    invoke-virtual {p1, v1}, Lx5/j$a;->f(I)Lx5/j$a;

    move-result-object p1

    const v1, 0x7f1100ad

    invoke-virtual {p1, v1}, Lx5/j$a;->e(I)Lx5/j$a;

    move-result-object p1

    new-instance v1, Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity$e;

    invoke-direct {v1, p0}, Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity$e;-><init>(Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity;)V

    invoke-virtual {p1, v1}, Lx5/j$a;->b(Ll8/l;)Lx5/j$a;

    move-result-object p1

    invoke-virtual {p1, v0}, Lx5/j$a;->c(Ljava/util/ArrayList;)Lx5/j$a;

    move-result-object p1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity;->q:Ljava/lang/String;

    if-eqz v0, :cond_2

    invoke-virtual {p1, v0}, Lx5/j$a;->m(Ljava/lang/CharSequence;)Lx5/j$a;

    :cond_2
    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity;->r:Ljava/lang/String;

    if-eqz v0, :cond_3

    invoke-virtual {p1, v0}, Lx5/j$a;->i(Ljava/lang/CharSequence;)Lx5/j$a;

    :cond_3
    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity;->s:Ljava/lang/String;

    if-eqz v0, :cond_4

    invoke-virtual {p1, v0}, Lx5/j$a;->k(Ljava/lang/CharSequence;)Lx5/j$a;

    :cond_4
    invoke-virtual {p1}, Lx5/j$a;->a()Lx5/j;

    move-result-object p1

    new-instance v0, Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity$d;

    invoke-direct {v0, p0}, Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity$d;-><init>(Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity;)V

    invoke-virtual {p1, v0}, Lx5/j;->p(Ll8/p;)V

    return-void
.end method

.method static synthetic K0(Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity;Lx5/a;ILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    const/4 p1, 0x0

    :cond_0
    invoke-direct {p0, p1}, Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity;->J0(Lx5/a;)V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Lmiuix/appcompat/app/j;->onCreate(Landroid/os/Bundle;)V

    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity;->G0()V

    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity;->H0()V

    return-void
.end method
