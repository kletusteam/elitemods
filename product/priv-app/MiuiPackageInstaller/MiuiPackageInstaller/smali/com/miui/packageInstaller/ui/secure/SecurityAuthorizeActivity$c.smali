.class final Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity$c;
.super Lm8/j;

# interfaces
.implements Ll8/p;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity;->I0()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lm8/j;",
        "Ll8/p<",
        "Ljava/lang/Integer;",
        "Ljava/lang/String;",
        "La8/v;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic b:Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity;


# direct methods
.method constructor <init>(Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity$c;->b:Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lm8/j;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final b(ILjava/lang/String;)V
    .locals 2

    const-string v0, "msg"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    iget-object p1, p0, Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity$c;->b:Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity;

    const/4 p2, -0x1

    invoke-virtual {p1, p2}, Landroid/app/Activity;->setResult(I)V

    goto :goto_1

    :cond_0
    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result p1

    const/4 v1, 0x0

    if-lez p1, :cond_1

    goto :goto_0

    :cond_1
    move v0, v1

    :goto_0
    if-eqz v0, :cond_2

    iget-object p1, p0, Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity$c;->b:Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity;

    invoke-static {p1, p2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    :cond_2
    iget-object p1, p0, Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity$c;->b:Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity;

    invoke-virtual {p1, v1}, Landroid/app/Activity;->setResult(I)V

    new-instance p1, Lp5/b;

    iget-object p2, p0, Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity$c;->b:Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity;

    invoke-static {p2}, Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity;->D0(Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity;)Lo5/b;

    move-result-object p2

    const-string v0, "risk_verifying_popup_close_btn"

    const-string v1, "button"

    invoke-direct {p1, v0, v1, p2}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    const-string p2, "verify_method"

    const-string v0, "risk_verify"

    invoke-virtual {p1, p2, v0}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p1

    invoke-virtual {p1}, Lp5/f;->c()Z

    :goto_1
    iget-object p1, p0, Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity$c;->b:Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity;

    invoke-virtual {p1}, Lmiuix/appcompat/app/j;->finish()V

    return-void
.end method

.method public bridge synthetic g(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    check-cast p2, Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Lcom/miui/packageInstaller/ui/secure/SecurityAuthorizeActivity$c;->b(ILjava/lang/String;)V

    sget-object p1, La8/v;->a:La8/v;

    return-object p1
.end method
