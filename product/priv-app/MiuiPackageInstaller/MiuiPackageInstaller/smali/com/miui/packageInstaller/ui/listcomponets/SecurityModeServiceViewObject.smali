.class public final Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceViewObject;
.super Lm6/a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceViewObject$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lm6/a<",
        "Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceViewObject$ViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private final l:I

.field private final m:I

.field private final n:I


# direct methods
.method public constructor <init>(Landroid/content/Context;IIILl6/c;Lm6/b;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p5, p6}, Lm6/a;-><init>(Landroid/content/Context;Ljava/lang/Object;Ll6/c;Lm6/b;)V

    iput p2, p0, Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceViewObject;->l:I

    iput p3, p0, Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceViewObject;->m:I

    iput p4, p0, Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceViewObject;->n:I

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;IIILl6/c;Lm6/b;ILm8/g;)V
    .locals 9

    and-int/lit8 v0, p7, 0x10

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    move-object v7, v1

    goto :goto_0

    :cond_0
    move-object v7, p5

    :goto_0
    and-int/lit8 v0, p7, 0x20

    if-eqz v0, :cond_1

    move-object v8, v1

    goto :goto_1

    :cond_1
    move-object v8, p6

    :goto_1
    move-object v2, p0

    move-object v3, p1

    move v4, p2

    move v5, p3

    move v6, p4

    invoke-direct/range {v2 .. v8}, Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceViewObject;-><init>(Landroid/content/Context;IIILl6/c;Lm6/b;)V

    return-void
.end method


# virtual methods
.method public k()I
    .locals 1

    const v0, 0x7f0d007c

    return v0
.end method

.method public bridge synthetic o(Landroidx/recyclerview/widget/RecyclerView$d0;)V
    .locals 0

    check-cast p1, Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceViewObject$ViewHolder;

    invoke-virtual {p0, p1}, Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceViewObject;->z(Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceViewObject$ViewHolder;)V

    return-void
.end method

.method public z(Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceViewObject$ViewHolder;)V
    .locals 3

    const-string v0, "viewHolder"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceViewObject$ViewHolder;->getMIcon()Landroid/widget/ImageView;

    move-result-object v0

    iget v1, p0, Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceViewObject;->l:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceViewObject$ViewHolder;->getMTitle()Landroid/widget/TextView;

    move-result-object v0

    iget v1, p0, Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceViewObject;->m:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceViewObject$ViewHolder;->getMDes()Landroid/widget/TextView;

    move-result-object v0

    iget v1, p0, Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceViewObject;->n:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceViewObject$ViewHolder;->getMTitle()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0700c5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    const/high16 v2, 0x3f000000    # 0.5f

    invoke-static {v0, v1, v2}, Lf6/y;->a(Landroid/widget/TextView;FF)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceViewObject$ViewHolder;->getMDes()Landroid/widget/TextView;

    move-result-object p1

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0700ae

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    const v1, 0x3ecccccd    # 0.4f

    invoke-static {p1, v0, v1}, Lf6/y;->a(Landroid/widget/TextView;FF)V

    return-void
.end method
