.class public final Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$a;
.super Ljava/lang/Object;

# interfaces
.implements Ln5/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->R()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;


# direct methods
.method constructor <init>(Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$a;->a:Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static synthetic b(Lm8/t;)V
    .locals 0

    invoke-static {p0}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$a;->c(Lm8/t;)V

    return-void
.end method

.method private static final c(Lm8/t;)V
    .locals 1

    const-string v0, "$currentAdapter"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p0, p0, Lm8/t;->a:Ljava/lang/Object;

    check-cast p0, Lj6/b;

    invoke-virtual {p0}, Lj6/b;->S()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object p0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroidx/recyclerview/widget/RecyclerView;->setItemAnimator(Landroidx/recyclerview/widget/RecyclerView$l;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/miui/packageInstaller/model/AdModel;)V
    .locals 10

    if-eqz p1, :cond_4

    invoke-virtual {p1}, Lcom/miui/packageInstaller/model/AdModel;->getData()Ljava/util/List;

    move-result-object p1

    if-eqz p1, :cond_4

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$a;->a:Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    const/4 v2, 0x1

    xor-int/2addr v1, v2

    if-ne v1, v2, :cond_4

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0, v1}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->P(Ljava/util/List;)V

    invoke-virtual {v0}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->G()Ljava/util/List;

    move-result-object v1

    const-string v3, "context"

    if-eqz v1, :cond_1

    new-instance v4, Lcom/miui/packageInstaller/ui/listcomponets/SecondAdTitleViewObject;

    invoke-virtual {v0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, v3}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->C(Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;)Lcom/miui/packageInstaller/model/AdModel$DesData;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-virtual {v6}, Lcom/miui/packageInstaller/model/AdModel$DesData;->getData()Lcom/miui/packageInstaller/model/AdData;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-virtual {v6}, Lcom/miui/packageInstaller/model/AdData;->getTitle()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-static {v6}, Lu8/g;->u0(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    goto :goto_0

    :cond_0
    const/4 v6, 0x0

    :goto_0
    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Lm6/a;->e()Ll6/c;

    move-result-object v7

    invoke-static {v0}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->E(Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;)Lm6/b;

    move-result-object v8

    invoke-direct {v4, v5, v6, v7, v8}, Lcom/miui/packageInstaller/ui/listcomponets/SecondAdTitleViewObject;-><init>(Landroid/content/Context;Ljava/lang/String;Ll6/c;Lm6/b;)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_2
    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/miui/packageInstaller/model/AdModel$DesData;

    new-instance v1, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;

    invoke-virtual {v0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, v3}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->D(Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;)Lcom/miui/packageInstaller/model/ApkInfo;

    move-result-object v6

    invoke-virtual {v0}, Lm6/a;->e()Ll6/c;

    move-result-object v8

    invoke-static {v0}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->E(Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;)Lm6/b;

    move-result-object v9

    move-object v4, v1

    invoke-direct/range {v4 .. v9}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/ApkInfo;Lcom/miui/packageInstaller/model/AdModel$DesData;Ll6/c;Lm6/b;)V

    invoke-virtual {v1, v2}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->Q(Z)V

    invoke-virtual {v0}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->G()Ljava/util/List;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    new-instance p1, Lm8/t;

    invoke-direct {p1}, Lm8/t;-><init>()V

    invoke-virtual {v0}, Lm6/a;->f()Lj6/b;

    move-result-object v1

    iput-object v1, p1, Lm8/t;->a:Ljava/lang/Object;

    if-eqz v1, :cond_4

    invoke-virtual {v1, v0}, Lj6/b;->U(Lm6/a;)I

    move-result v3

    add-int/2addr v3, v2

    invoke-virtual {v0}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->G()Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Lj6/b;->K(ILjava/util/List;)I

    iget-object v0, p1, Lm8/t;->a:Ljava/lang/Object;

    check-cast v0, Lj6/b;

    invoke-virtual {v0}, Lj6/b;->S()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    new-instance v1, Lbb/b;

    invoke-direct {v1}, Lbb/b;-><init>()V

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setItemAnimator(Landroidx/recyclerview/widget/RecyclerView$l;)V

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object v0

    new-instance v1, Lcom/miui/packageInstaller/ui/listcomponets/t;

    invoke-direct {v1, p1}, Lcom/miui/packageInstaller/ui/listcomponets/t;-><init>(Lm8/t;)V

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Lf6/z;->d(Ljava/lang/Runnable;J)V

    :cond_4
    return-void
.end method
