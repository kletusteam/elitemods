.class public Lcom/miui/packageInstaller/view/RoundImageView;
.super Landroidx/appcompat/widget/AppCompatImageView;


# instance fields
.field private c:F

.field private d:F

.field private e:F

.field private f:F

.field private g:F

.field private h:I

.field private i:I

.field private j:Landroid/graphics/Path;

.field private k:Landroid/graphics/RectF;

.field private l:[F

.field private m:I

.field n:Landroid/graphics/Paint;

.field o:Landroid/graphics/RectF;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/miui/packageInstaller/view/RoundImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    invoke-direct {p0, p1, p2, p3}, Landroidx/appcompat/widget/AppCompatImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance p3, Landroid/graphics/Paint;

    invoke-direct {p3}, Landroid/graphics/Paint;-><init>()V

    iput-object p3, p0, Lcom/miui/packageInstaller/view/RoundImageView;->n:Landroid/graphics/Paint;

    new-instance p3, Landroid/graphics/RectF;

    invoke-direct {p3}, Landroid/graphics/RectF;-><init>()V

    iput-object p3, p0, Lcom/miui/packageInstaller/view/RoundImageView;->o:Landroid/graphics/RectF;

    sget-object p3, Ln6/a;->f2:[I

    invoke-virtual {p1, p2, p3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p2

    const/4 p3, 0x0

    const/4 v0, 0x0

    invoke-virtual {p2, p3, v0}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Lcom/miui/packageInstaller/view/RoundImageView;->c:F

    const/4 v1, 0x4

    invoke-virtual {p2, v1, v0}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v2

    iput v2, p0, Lcom/miui/packageInstaller/view/RoundImageView;->d:F

    const/4 v2, 0x5

    invoke-virtual {p2, v2, v0}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v3

    iput v3, p0, Lcom/miui/packageInstaller/view/RoundImageView;->e:F

    const/4 v3, 0x2

    invoke-virtual {p2, v3, v0}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v4

    iput v4, p0, Lcom/miui/packageInstaller/view/RoundImageView;->g:F

    const/4 v4, 0x1

    invoke-virtual {p2, v4, v0}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    iput v0, p0, Lcom/miui/packageInstaller/view/RoundImageView;->f:F

    const v0, 0x7f06002a

    invoke-virtual {p1, v0}, Landroid/content/Context;->getColor(I)I

    move-result p1

    const/4 v0, 0x3

    invoke-virtual {p2, v0, p1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result p1

    iput p1, p0, Lcom/miui/packageInstaller/view/RoundImageView;->m:I

    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    new-instance p1, Landroid/graphics/Path;

    invoke-direct {p1}, Landroid/graphics/Path;-><init>()V

    iput-object p1, p0, Lcom/miui/packageInstaller/view/RoundImageView;->j:Landroid/graphics/Path;

    new-instance p1, Landroid/graphics/RectF;

    invoke-direct {p1}, Landroid/graphics/RectF;-><init>()V

    iput-object p1, p0, Lcom/miui/packageInstaller/view/RoundImageView;->k:Landroid/graphics/RectF;

    const/16 p1, 0x8

    new-array p1, p1, [F

    iget p2, p0, Lcom/miui/packageInstaller/view/RoundImageView;->d:F

    aput p2, p1, p3

    aput p2, p1, v4

    iget p2, p0, Lcom/miui/packageInstaller/view/RoundImageView;->e:F

    aput p2, p1, v3

    aput p2, p1, v0

    iget p2, p0, Lcom/miui/packageInstaller/view/RoundImageView;->g:F

    aput p2, p1, v1

    aput p2, p1, v2

    iget p2, p0, Lcom/miui/packageInstaller/view/RoundImageView;->f:F

    const/4 p3, 0x6

    aput p2, p1, p3

    const/4 p3, 0x7

    aput p2, p1, p3

    iput-object p1, p0, Lcom/miui/packageInstaller/view/RoundImageView;->l:[F

    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 4

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    iget-object v0, p0, Lcom/miui/packageInstaller/view/RoundImageView;->j:Landroid/graphics/Path;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;)Z

    invoke-super {p0, p1}, Landroid/widget/ImageView;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    iget-object v0, p0, Lcom/miui/packageInstaller/view/RoundImageView;->n:Landroid/graphics/Paint;

    iget v1, p0, Lcom/miui/packageInstaller/view/RoundImageView;->m:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/miui/packageInstaller/view/RoundImageView;->n:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/view/RoundImageView;->n:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lcom/miui/packageInstaller/view/RoundImageView;->o:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/miui/packageInstaller/view/RoundImageView;->k:Landroid/graphics/RectF;

    iget v2, v1, Landroid/graphics/RectF;->left:F

    const/high16 v3, 0x40000000    # 2.0f

    add-float/2addr v2, v3

    iput v2, v0, Landroid/graphics/RectF;->left:F

    iget v2, v1, Landroid/graphics/RectF;->top:F

    add-float/2addr v2, v3

    iput v2, v0, Landroid/graphics/RectF;->top:F

    iget v2, v1, Landroid/graphics/RectF;->right:F

    sub-float/2addr v2, v3

    iput v2, v0, Landroid/graphics/RectF;->right:F

    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v1, v3

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    iget v1, p0, Lcom/miui/packageInstaller/view/RoundImageView;->c:F

    iget-object v2, p0, Lcom/miui/packageInstaller/view/RoundImageView;->n:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v1, v2}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 2

    invoke-super {p0, p1, p2}, Landroid/widget/ImageView;->onMeasure(II)V

    invoke-virtual {p0}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result p1

    iput p1, p0, Lcom/miui/packageInstaller/view/RoundImageView;->h:I

    invoke-virtual {p0}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result p1

    iput p1, p0, Lcom/miui/packageInstaller/view/RoundImageView;->i:I

    iget-object p2, p0, Lcom/miui/packageInstaller/view/RoundImageView;->k:Landroid/graphics/RectF;

    const/4 v0, 0x0

    iput v0, p2, Landroid/graphics/RectF;->left:F

    iput v0, p2, Landroid/graphics/RectF;->top:F

    iget v1, p0, Lcom/miui/packageInstaller/view/RoundImageView;->h:I

    int-to-float v1, v1

    iput v1, p2, Landroid/graphics/RectF;->right:F

    int-to-float p1, p1

    iput p1, p2, Landroid/graphics/RectF;->bottom:F

    iget-object p1, p0, Lcom/miui/packageInstaller/view/RoundImageView;->j:Landroid/graphics/Path;

    invoke-virtual {p1}, Landroid/graphics/Path;->reset()V

    iget p1, p0, Lcom/miui/packageInstaller/view/RoundImageView;->c:F

    cmpl-float p2, p1, v0

    if-lez p2, :cond_0

    iget-object p2, p0, Lcom/miui/packageInstaller/view/RoundImageView;->j:Landroid/graphics/Path;

    iget-object v0, p0, Lcom/miui/packageInstaller/view/RoundImageView;->k:Landroid/graphics/RectF;

    sget-object v1, Landroid/graphics/Path$Direction;->CCW:Landroid/graphics/Path$Direction;

    invoke-virtual {p2, v0, p1, p1, v1}, Landroid/graphics/Path;->addRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Path$Direction;)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/miui/packageInstaller/view/RoundImageView;->j:Landroid/graphics/Path;

    iget-object p2, p0, Lcom/miui/packageInstaller/view/RoundImageView;->k:Landroid/graphics/RectF;

    iget-object v0, p0, Lcom/miui/packageInstaller/view/RoundImageView;->l:[F

    sget-object v1, Landroid/graphics/Path$Direction;->CCW:Landroid/graphics/Path$Direction;

    invoke-virtual {p1, p2, v0, v1}, Landroid/graphics/Path;->addRoundRect(Landroid/graphics/RectF;[FLandroid/graphics/Path$Direction;)V

    :goto_0
    iget p1, p0, Lcom/miui/packageInstaller/view/RoundImageView;->h:I

    iget p2, p0, Lcom/miui/packageInstaller/view/RoundImageView;->i:I

    invoke-virtual {p0, p1, p2}, Landroid/widget/ImageView;->setMeasuredDimension(II)V

    return-void
.end method
