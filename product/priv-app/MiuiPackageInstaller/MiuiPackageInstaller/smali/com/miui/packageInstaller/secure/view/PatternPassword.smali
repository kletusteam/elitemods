.class public final Lcom/miui/packageInstaller/secure/view/PatternPassword;
.super Landroid/widget/LinearLayout;

# interfaces
.implements Lx5/c;
.implements Lcom/miui/packageInstaller/view/LockPatternView$c;


# instance fields
.field private a:Lcom/miui/packageInstaller/view/LockPatternView;

.field private b:Ljava/lang/Runnable;

.field private c:Lx5/h;

.field private d:I

.field private e:Landroid/widget/TextView;

.field private f:Ll8/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ll8/l<",
            "-",
            "Ljava/lang/Integer;",
            "La8/v;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lf6/m;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance p1, Ly5/i;

    invoke-direct {p1, p0}, Ly5/i;-><init>(Lcom/miui/packageInstaller/secure/view/PatternPassword;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/secure/view/PatternPassword;->b:Ljava/lang/Runnable;

    new-instance p1, Lf6/m;

    invoke-static {}, Lcom/android/packageinstaller/InstallerApplication;->g()Lcom/android/packageinstaller/InstallerApplication;

    move-result-object p2

    invoke-direct {p1, p2}, Lf6/m;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/secure/view/PatternPassword;->g:Lf6/m;

    return-void
.end method

.method public static synthetic g(Ljava/util/List;Lcom/miui/packageInstaller/secure/view/PatternPassword;Lf6/z;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/miui/packageInstaller/secure/view/PatternPassword;->r(Ljava/util/List;Lcom/miui/packageInstaller/secure/view/PatternPassword;Lf6/z;)V

    return-void
.end method

.method public static synthetic h(Lcom/miui/packageInstaller/secure/view/PatternPassword;Lf6/z;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/miui/packageInstaller/secure/view/PatternPassword;->t(Lcom/miui/packageInstaller/secure/view/PatternPassword;Lf6/z;)V

    return-void
.end method

.method public static synthetic i(Lcom/miui/packageInstaller/secure/view/PatternPassword;)V
    .locals 0

    invoke-static {p0}, Lcom/miui/packageInstaller/secure/view/PatternPassword;->p(Lcom/miui/packageInstaller/secure/view/PatternPassword;)V

    return-void
.end method

.method public static synthetic j(Lcom/miui/packageInstaller/secure/view/PatternPassword;)V
    .locals 0

    invoke-static {p0}, Lcom/miui/packageInstaller/secure/view/PatternPassword;->s(Lcom/miui/packageInstaller/secure/view/PatternPassword;)V

    return-void
.end method

.method public static final synthetic k(Lcom/miui/packageInstaller/secure/view/PatternPassword;)Lx5/h;
    .locals 0

    iget-object p0, p0, Lcom/miui/packageInstaller/secure/view/PatternPassword;->c:Lx5/h;

    return-object p0
.end method

.method public static final synthetic l(Lcom/miui/packageInstaller/secure/view/PatternPassword;)Landroid/widget/TextView;
    .locals 0

    iget-object p0, p0, Lcom/miui/packageInstaller/secure/view/PatternPassword;->e:Landroid/widget/TextView;

    return-object p0
.end method

.method public static final synthetic m(Lcom/miui/packageInstaller/secure/view/PatternPassword;)Lcom/miui/packageInstaller/view/LockPatternView;
    .locals 0

    iget-object p0, p0, Lcom/miui/packageInstaller/secure/view/PatternPassword;->a:Lcom/miui/packageInstaller/view/LockPatternView;

    return-object p0
.end method

.method public static final synthetic n(Lcom/miui/packageInstaller/secure/view/PatternPassword;)I
    .locals 0

    iget p0, p0, Lcom/miui/packageInstaller/secure/view/PatternPassword;->d:I

    return p0
.end method

.method public static final synthetic o(Lcom/miui/packageInstaller/secure/view/PatternPassword;I)V
    .locals 0

    iput p1, p0, Lcom/miui/packageInstaller/secure/view/PatternPassword;->d:I

    return-void
.end method

.method private static final p(Lcom/miui/packageInstaller/secure/view/PatternPassword;)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p0, p0, Lcom/miui/packageInstaller/secure/view/PatternPassword;->a:Lcom/miui/packageInstaller/view/LockPatternView;

    if-nez p0, :cond_0

    const-string p0, "patternsView"

    invoke-static {p0}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 p0, 0x0

    :cond_0
    invoke-virtual {p0}, Lcom/miui/packageInstaller/view/LockPatternView;->c()V

    return-void
.end method

.method private static final r(Ljava/util/List;Lcom/miui/packageInstaller/secure/view/PatternPassword;Lf6/z;)V
    .locals 6

    const-string v0, "this$0"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p0, :cond_1

    :try_start_0
    invoke-virtual {p1, p0}, Lcom/miui/packageInstaller/secure/view/PatternPassword;->q(Ljava/util/List;)Z

    move-result p0

    if-eqz p0, :cond_0

    new-instance p0, Ly5/j;

    invoke-direct {p0, p1}, Ly5/j;-><init>(Lcom/miui/packageInstaller/secure/view/PatternPassword;)V

    :goto_0
    invoke-virtual {p2, p0}, Lf6/z;->e(Ljava/lang/Runnable;)V

    goto :goto_1

    :cond_0
    new-instance p0, Ly5/k;

    invoke-direct {p0, p1, p2}, Ly5/k;-><init>(Lcom/miui/packageInstaller/secure/view/PatternPassword;Lf6/z;)V
    :try_end_0
    .catch Lf6/m$b; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    invoke-static {}, Lv8/t0;->c()Lv8/v1;

    move-result-object p2

    invoke-static {p2}, Lv8/f0;->a(Ld8/g;)Lv8/e0;

    move-result-object v0

    invoke-static {}, Lv8/t0;->c()Lv8/v1;

    move-result-object v1

    const/4 v2, 0x0

    new-instance v3, Lcom/miui/packageInstaller/secure/view/PatternPassword$a;

    const/4 p2, 0x0

    invoke-direct {v3, p1, p0, p2}, Lcom/miui/packageInstaller/secure/view/PatternPassword$a;-><init>(Lcom/miui/packageInstaller/secure/view/PatternPassword;Lf6/m$b;Ld8/d;)V

    const/4 v4, 0x2

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lv8/f;->d(Lv8/e0;Ld8/g;Lv8/g0;Ll8/p;ILjava/lang/Object;)Lv8/k1;

    :cond_1
    :goto_1
    return-void
.end method

.method private static final s(Lcom/miui/packageInstaller/secure/view/PatternPassword;)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p0, p0, Lcom/miui/packageInstaller/secure/view/PatternPassword;->f:Ll8/l;

    if-eqz p0, :cond_0

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p0, v0}, Ll8/l;->j(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method private static final t(Lcom/miui/packageInstaller/secure/view/PatternPassword;Lf6/z;)V
    .locals 3

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/secure/view/PatternPassword;->a:Lcom/miui/packageInstaller/view/LockPatternView;

    if-nez v0, :cond_0

    const-string v0, "patternsView"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v0, 0x0

    :cond_0
    sget-object v1, Lcom/miui/packageInstaller/view/LockPatternView$b;->c:Lcom/miui/packageInstaller/view/LockPatternView$b;

    invoke-virtual {v0, v1}, Lcom/miui/packageInstaller/view/LockPatternView;->setDisplayMode(Lcom/miui/packageInstaller/view/LockPatternView$b;)V

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "vibrator"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    const-string v1, "null cannot be cast to non-null type android.os.Vibrator"

    invoke-static {v0, v1}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/os/Vibrator;

    const-wide/16 v1, 0xc8

    invoke-virtual {v0, v1, v2}, Landroid/os/Vibrator;->vibrate(J)V

    iget-object p0, p0, Lcom/miui/packageInstaller/secure/view/PatternPassword;->b:Ljava/lang/Runnable;

    const-wide/16 v0, 0x320

    invoke-virtual {p1, p0, v0, v1}, Lf6/z;->d(Ljava/lang/Runnable;J)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    iget-object v0, p0, Lcom/miui/packageInstaller/secure/view/PatternPassword;->f:Ll8/l;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ll8/l;->j(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public b()V
    .locals 0

    return-void
.end method

.method public c(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/miui/packageInstaller/view/LockPatternView$a;",
            ">;)V"
        }
    .end annotation

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object v0

    new-instance v1, Ly5/l;

    invoke-direct {v1, p1, p0, v0}, Ly5/l;-><init>(Ljava/util/List;Lcom/miui/packageInstaller/secure/view/PatternPassword;Lf6/z;)V

    invoke-virtual {v0, v1}, Lf6/z;->g(Ljava/lang/Runnable;)V

    return-void
.end method

.method public d(Lx5/h;Ll8/l;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lx5/h;",
            "Ll8/l<",
            "-",
            "Ljava/lang/Integer;",
            "La8/v;",
            ">;)V"
        }
    .end annotation

    const-string v0, "dialog"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "callback"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/secure/view/PatternPassword;->c:Lx5/h;

    iput-object p2, p0, Lcom/miui/packageInstaller/secure/view/PatternPassword;->f:Ll8/l;

    return-void
.end method

.method public e(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/miui/packageInstaller/view/LockPatternView$a;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public f()V
    .locals 2

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/packageInstaller/secure/view/PatternPassword;->b:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lf6/z;->f(Ljava/lang/Runnable;)V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 4

    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    const v0, 0x7f0a020f

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.lpv)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/miui/packageInstaller/view/LockPatternView;

    iput-object v0, p0, Lcom/miui/packageInstaller/secure/view/PatternPassword;->a:Lcom/miui/packageInstaller/view/LockPatternView;

    const/4 v1, 0x0

    const-string v2, "patternsView"

    if-nez v0, :cond_0

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_0
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/view/View;->setNestedScrollingEnabled(Z)V

    iget-object v0, p0, Lcom/miui/packageInstaller/secure/view/PatternPassword;->a:Lcom/miui/packageInstaller/view/LockPatternView;

    if-nez v0, :cond_1

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move-object v1, v0

    :goto_0
    invoke-virtual {v1, p0}, Lcom/miui/packageInstaller/view/LockPatternView;->setOnPatternListener(Lcom/miui/packageInstaller/view/LockPatternView$c;)V

    const v0, 0x7f0a0299

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.password_error_tip)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/secure/view/PatternPassword;->e:Landroid/widget/TextView;

    return-void
.end method

.method public final q(Ljava/util/List;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/miui/packageInstaller/view/LockPatternView$a;",
            ">;)Z"
        }
    .end annotation

    const-string v0, "pattern"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/secure/view/PatternPassword;->g:Lf6/m;

    invoke-static {}, Lcom/android/packageinstaller/compat/UserHandleCompat;->myUserId()I

    move-result v1

    invoke-virtual {v0, p1, v1}, Lf6/m;->f(Ljava/util/List;I)Z

    move-result p1

    return p1
.end method

.method public release()V
    .locals 0

    invoke-static {p0}, Lx5/c$a;->a(Lx5/c;)V

    return-void
.end method

.method public setCancelButtonText(Ljava/lang/CharSequence;)V
    .locals 0

    invoke-static {p0, p1}, Lx5/c$a;->b(Lx5/c;Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setConfirmButtonText(Ljava/lang/CharSequence;)V
    .locals 0

    invoke-static {p0, p1}, Lx5/c$a;->c(Lx5/c;Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setTipMsgText(Ljava/lang/CharSequence;)V
    .locals 0

    invoke-static {p0, p1}, Lx5/c$a;->d(Lx5/c;Ljava/lang/CharSequence;)V

    return-void
.end method
