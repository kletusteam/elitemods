.class public final Lcom/miui/packageInstaller/ui/listcomponets/PureModeElderTipViewObject;
.super Lcom/miui/packageInstaller/ui/listcomponets/f0;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/packageInstaller/ui/listcomponets/PureModeElderTipViewObject$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/miui/packageInstaller/ui/listcomponets/f0<",
        "Lcom/miui/packageInstaller/ui/listcomponets/PureModeElderTipViewObject$ViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private m:Lcom/miui/packageInstaller/model/CloudParams;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/CloudParams;Ll6/c;Lm6/b;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mData"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/miui/packageInstaller/ui/listcomponets/f0;-><init>(Landroid/content/Context;Ljava/lang/Object;Ll6/c;Lm6/b;)V

    iput-object p2, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeElderTipViewObject;->m:Lcom/miui/packageInstaller/model/CloudParams;

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/CloudParams;Ll6/c;Lm6/b;ILm8/g;)V
    .locals 1

    and-int/lit8 p6, p5, 0x4

    const/4 v0, 0x0

    if-eqz p6, :cond_0

    move-object p3, v0

    :cond_0
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_1

    move-object p4, v0

    :cond_1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/miui/packageInstaller/ui/listcomponets/PureModeElderTipViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/CloudParams;Ll6/c;Lm6/b;)V

    return-void
.end method

.method public static synthetic A(Lcom/miui/packageInstaller/ui/listcomponets/PureModeElderTipViewObject;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/miui/packageInstaller/ui/listcomponets/PureModeElderTipViewObject;->E(Lcom/miui/packageInstaller/ui/listcomponets/PureModeElderTipViewObject;Landroid/view/View;)V

    return-void
.end method

.method private final B()V
    .locals 5

    new-instance v0, Lp5/g;

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v1

    const-string v2, "null cannot be cast to non-null type com.miui.packageInstaller.analytics.IPage"

    invoke-static {v1, v2}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lo5/a;

    const-string v3, "protect_mode_know_btn"

    const-string v4, "button"

    invoke-direct {v0, v3, v4, v1}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    new-instance v0, Lp5/g;

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v2}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lo5/a;

    const-string v2, "protect_mode_open_btn"

    invoke-direct {v0, v2, v4, v1}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    return-void
.end method

.method private static final D(Lcom/miui/packageInstaller/ui/listcomponets/PureModeElderTipViewObject;Landroid/view/View;)V
    .locals 3

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Lp5/b;

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v0

    const-string v1, "null cannot be cast to non-null type com.miui.packageInstaller.analytics.IPage"

    invoke-static {v0, v1}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lo5/a;

    const-string v1, "protect_mode_know_btn"

    const-string v2, "button"

    invoke-direct {p1, v1, v2, v0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p1}, Lp5/f;->c()Z

    new-instance p1, Landroid/content/Intent;

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lcom/miui/packageInstaller/ui/secure/InstallerWebViewActivity;

    invoke-direct {p1, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v0, "hasTitle"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    sget-boolean v0, Lq2/c;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "http://fe.market.pt.xiaomi.com/hd/apm-h5-cdn/cdn-risk-intro.html"

    goto :goto_0

    :cond_0
    sget-object v0, Lf6/d0;->a:Lf6/d0;

    invoke-virtual {v0}, Lf6/d0;->b()Ljava/lang/String;

    move-result-object v0

    :goto_0
    const-string v1, "jump_url"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object p0

    invoke-virtual {p0, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private static final E(Lcom/miui/packageInstaller/ui/listcomponets/PureModeElderTipViewObject;Landroid/view/View;)V
    .locals 9

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object p1

    const/4 v0, 0x1

    const-string v1, "elder"

    invoke-static {p1, v0, v1}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->setSafeModelEnabled(Landroid/content/Context;ZLjava/lang/String;)V

    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/listcomponets/PureModeElderTipViewObject;->G()V

    new-instance p1, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject;

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v3

    const-string v0, "context"

    invoke-static {v3, v0}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeElderTipViewObject;->m:Lcom/miui/packageInstaller/model/CloudParams;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0xc

    const/4 v8, 0x0

    move-object v2, p1

    invoke-direct/range {v2 .. v8}, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/CloudParams;Ll6/c;Lm6/b;ILm8/g;)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/f0;->a()V

    invoke-virtual {p0, p1}, Lm6/a;->u(Lm6/a;)V

    return-void
.end method

.method private final F()V
    .locals 4

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v0

    const-string v1, "null cannot be cast to non-null type miuix.appcompat.app.AppCompatActivity"

    invoke-static {v0, v1}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lmiuix/appcompat/app/j;

    invoke-virtual {v0}, Landroidx/fragment/app/e;->X()Landroidx/fragment/app/m;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/fragment/app/m;->l()Landroidx/fragment/app/v;

    move-result-object v0

    const-string v1, "activity.supportFragmentManager.beginTransaction()"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v1, Ld6/f;

    invoke-direct {v1}, Ld6/f;-><init>()V

    const v2, 0x7f0a017f

    const-string v3, "openSecurityModeDialog"

    invoke-virtual {v0, v2, v1, v3}, Landroidx/fragment/app/v;->o(ILandroidx/fragment/app/Fragment;Ljava/lang/String;)Landroidx/fragment/app/v;

    invoke-virtual {v0}, Landroidx/fragment/app/v;->h()I

    return-void
.end method

.method private final G()V
    .locals 4

    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/listcomponets/PureModeElderTipViewObject;->F()V

    new-instance v0, Lp5/b;

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v1

    const-string v2, "null cannot be cast to non-null type com.miui.packageInstaller.analytics.IPage"

    invoke-static {v1, v2}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lo5/a;

    const-string v2, "protect_mode_open_btn"

    const-string v3, "button"

    invoke-direct {v0, v2, v3, v1}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    return-void
.end method

.method public static synthetic z(Lcom/miui/packageInstaller/ui/listcomponets/PureModeElderTipViewObject;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/miui/packageInstaller/ui/listcomponets/PureModeElderTipViewObject;->D(Lcom/miui/packageInstaller/ui/listcomponets/PureModeElderTipViewObject;Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public C(Lcom/miui/packageInstaller/ui/listcomponets/PureModeElderTipViewObject$ViewHolder;)V
    .locals 2

    const-string v0, "viewHolder"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/miui/packageInstaller/ui/listcomponets/f0;->o(Landroidx/recyclerview/widget/RecyclerView$d0;)V

    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/listcomponets/PureModeElderTipViewObject;->B()V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/PureModeElderTipViewObject$ViewHolder;->getSecurity()Landroidx/appcompat/widget/LinearLayoutCompat;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/miui/packageInstaller/ui/listcomponets/m;

    invoke-direct {v1, p0}, Lcom/miui/packageInstaller/ui/listcomponets/m;-><init>(Lcom/miui/packageInstaller/ui/listcomponets/PureModeElderTipViewObject;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/PureModeElderTipViewObject$ViewHolder;->getOpenNow()Landroidx/appcompat/widget/AppCompatButton;

    move-result-object p1

    if-eqz p1, :cond_1

    new-instance v0, Lcom/miui/packageInstaller/ui/listcomponets/n;

    invoke-direct {v0, p0}, Lcom/miui/packageInstaller/ui/listcomponets/n;-><init>(Lcom/miui/packageInstaller/ui/listcomponets/PureModeElderTipViewObject;)V

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    return-void
.end method

.method public k()I
    .locals 1

    const v0, 0x7f0d016b

    return v0
.end method

.method public bridge synthetic o(Landroidx/recyclerview/widget/RecyclerView$d0;)V
    .locals 0

    check-cast p1, Lcom/miui/packageInstaller/ui/listcomponets/PureModeElderTipViewObject$ViewHolder;

    invoke-virtual {p0, p1}, Lcom/miui/packageInstaller/ui/listcomponets/PureModeElderTipViewObject;->C(Lcom/miui/packageInstaller/ui/listcomponets/PureModeElderTipViewObject$ViewHolder;)V

    return-void
.end method
