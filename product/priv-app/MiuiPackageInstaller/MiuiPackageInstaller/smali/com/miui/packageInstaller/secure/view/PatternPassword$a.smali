.class final Lcom/miui/packageInstaller/secure/view/PatternPassword$a;
.super Lf8/k;

# interfaces
.implements Ll8/p;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/packageInstaller/secure/view/PatternPassword;->r(Ljava/util/List;Lcom/miui/packageInstaller/secure/view/PatternPassword;Lf6/z;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf8/k;",
        "Ll8/p<",
        "Lv8/e0;",
        "Ld8/d<",
        "-",
        "La8/v;",
        ">;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation runtime Lf8/f;
    c = "com.miui.packageInstaller.secure.view.PatternPassword$onPatternDetected$1$1$1$3"
    f = "PatternPassword.kt"
    l = {
        0x68
    }
    m = "invokeSuspend"
.end annotation


# instance fields
.field e:I

.field f:I

.field private synthetic g:Ljava/lang/Object;

.field final synthetic h:Lcom/miui/packageInstaller/secure/view/PatternPassword;

.field final synthetic i:Lf6/m$b;


# direct methods
.method constructor <init>(Lcom/miui/packageInstaller/secure/view/PatternPassword;Lf6/m$b;Ld8/d;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/miui/packageInstaller/secure/view/PatternPassword;",
            "Lf6/m$b;",
            "Ld8/d<",
            "-",
            "Lcom/miui/packageInstaller/secure/view/PatternPassword$a;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/miui/packageInstaller/secure/view/PatternPassword$a;->h:Lcom/miui/packageInstaller/secure/view/PatternPassword;

    iput-object p2, p0, Lcom/miui/packageInstaller/secure/view/PatternPassword$a;->i:Lf6/m$b;

    const/4 p1, 0x2

    invoke-direct {p0, p1, p3}, Lf8/k;-><init>(ILd8/d;)V

    return-void
.end method


# virtual methods
.method public final b(Ljava/lang/Object;Ld8/d;)Ld8/d;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ld8/d<",
            "*>;)",
            "Ld8/d<",
            "La8/v;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/miui/packageInstaller/secure/view/PatternPassword$a;

    iget-object v1, p0, Lcom/miui/packageInstaller/secure/view/PatternPassword$a;->h:Lcom/miui/packageInstaller/secure/view/PatternPassword;

    iget-object v2, p0, Lcom/miui/packageInstaller/secure/view/PatternPassword$a;->i:Lf6/m$b;

    invoke-direct {v0, v1, v2, p2}, Lcom/miui/packageInstaller/secure/view/PatternPassword$a;-><init>(Lcom/miui/packageInstaller/secure/view/PatternPassword;Lf6/m$b;Ld8/d;)V

    iput-object p1, v0, Lcom/miui/packageInstaller/secure/view/PatternPassword$a;->g:Ljava/lang/Object;

    return-object v0
.end method

.method public bridge synthetic g(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lv8/e0;

    check-cast p2, Ld8/d;

    invoke-virtual {p0, p1, p2}, Lcom/miui/packageInstaller/secure/view/PatternPassword$a;->q(Lv8/e0;Ld8/d;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final n(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 18

    move-object/from16 v0, p0

    invoke-static {}, Le8/b;->c()Ljava/lang/Object;

    move-result-object v1

    iget v2, v0, Lcom/miui/packageInstaller/secure/view/PatternPassword$a;->f:I

    const-string v3, "patternsView"

    const/4 v4, 0x1

    if-eqz v2, :cond_1

    if-ne v2, v4, :cond_0

    iget v2, v0, Lcom/miui/packageInstaller/secure/view/PatternPassword$a;->e:I

    iget-object v6, v0, Lcom/miui/packageInstaller/secure/view/PatternPassword$a;->g:Ljava/lang/Object;

    check-cast v6, Lv8/e0;

    invoke-static/range {p1 .. p1}, La8/n;->b(Ljava/lang/Object;)V

    move-object v7, v0

    move v5, v4

    goto/16 :goto_2

    :cond_0
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static/range {p1 .. p1}, La8/n;->b(Ljava/lang/Object;)V

    iget-object v2, v0, Lcom/miui/packageInstaller/secure/view/PatternPassword$a;->g:Ljava/lang/Object;

    check-cast v2, Lv8/e0;

    iget-object v6, v0, Lcom/miui/packageInstaller/secure/view/PatternPassword$a;->h:Lcom/miui/packageInstaller/secure/view/PatternPassword;

    iget-object v7, v0, Lcom/miui/packageInstaller/secure/view/PatternPassword$a;->i:Lf6/m$b;

    invoke-virtual {v7}, Lf6/m$b;->a()I

    move-result v7

    invoke-static {v6, v7}, Lcom/miui/packageInstaller/secure/view/PatternPassword;->o(Lcom/miui/packageInstaller/secure/view/PatternPassword;I)V

    iget-object v6, v0, Lcom/miui/packageInstaller/secure/view/PatternPassword$a;->h:Lcom/miui/packageInstaller/secure/view/PatternPassword;

    invoke-static {v6}, Lcom/miui/packageInstaller/secure/view/PatternPassword;->m(Lcom/miui/packageInstaller/secure/view/PatternPassword;)Lcom/miui/packageInstaller/view/LockPatternView;

    move-result-object v6

    if-nez v6, :cond_2

    invoke-static {v3}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v6, 0x0

    :cond_2
    invoke-virtual {v6}, Lcom/miui/packageInstaller/view/LockPatternView;->c()V

    const/16 v6, 0xc8

    move-object v7, v0

    move/from16 v17, v6

    move-object v6, v2

    move/from16 v2, v17

    :goto_0
    iget-object v8, v7, Lcom/miui/packageInstaller/secure/view/PatternPassword$a;->h:Lcom/miui/packageInstaller/secure/view/PatternPassword;

    invoke-static {v8}, Lcom/miui/packageInstaller/secure/view/PatternPassword;->n(Lcom/miui/packageInstaller/secure/view/PatternPassword;)I

    move-result v8

    const-string v9, "errorTipsTextView"

    if-lez v8, :cond_a

    iget-object v8, v7, Lcom/miui/packageInstaller/secure/view/PatternPassword$a;->h:Lcom/miui/packageInstaller/secure/view/PatternPassword;

    invoke-static {v8}, Lcom/miui/packageInstaller/secure/view/PatternPassword;->l(Lcom/miui/packageInstaller/secure/view/PatternPassword;)Landroid/widget/TextView;

    move-result-object v8

    if-nez v8, :cond_3

    invoke-static {v9}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v8, 0x0

    :cond_3
    const/4 v10, 0x0

    invoke-virtual {v8, v10}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v8, v7, Lcom/miui/packageInstaller/secure/view/PatternPassword$a;->h:Lcom/miui/packageInstaller/secure/view/PatternPassword;

    invoke-static {v8}, Lcom/miui/packageInstaller/secure/view/PatternPassword;->l(Lcom/miui/packageInstaller/secure/view/PatternPassword;)Landroid/widget/TextView;

    move-result-object v8

    if-nez v8, :cond_4

    invoke-static {v9}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v8, 0x0

    :cond_4
    iget-object v11, v7, Lcom/miui/packageInstaller/secure/view/PatternPassword$a;->h:Lcom/miui/packageInstaller/secure/view/PatternPassword;

    invoke-virtual {v11}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f0f0016

    iget-object v13, v7, Lcom/miui/packageInstaller/secure/view/PatternPassword$a;->h:Lcom/miui/packageInstaller/secure/view/PatternPassword;

    invoke-static {v13}, Lcom/miui/packageInstaller/secure/view/PatternPassword;->n(Lcom/miui/packageInstaller/secure/view/PatternPassword;)I

    move-result v13

    add-int/lit16 v13, v13, 0x1f4

    int-to-double v13, v13

    const-wide v15, 0x408f400000000000L    # 1000.0

    div-double/2addr v13, v15

    invoke-static {v13, v14}, Lo8/a;->a(D)I

    move-result v13

    new-array v14, v4, [Ljava/lang/Object;

    iget-object v5, v7, Lcom/miui/packageInstaller/secure/view/PatternPassword$a;->h:Lcom/miui/packageInstaller/secure/view/PatternPassword;

    invoke-static {v5}, Lcom/miui/packageInstaller/secure/view/PatternPassword;->n(Lcom/miui/packageInstaller/secure/view/PatternPassword;)I

    move-result v5

    add-int/lit16 v5, v5, 0x1f4

    int-to-double v4, v5

    div-double/2addr v4, v15

    invoke-static {v4, v5}, Lo8/a;->a(D)I

    move-result v4

    invoke-static {v4}, Lf8/b;->b(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v14, v10

    invoke-virtual {v11, v12, v13, v14}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v8, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, v7, Lcom/miui/packageInstaller/secure/view/PatternPassword$a;->h:Lcom/miui/packageInstaller/secure/view/PatternPassword;

    invoke-static {v4}, Lcom/miui/packageInstaller/secure/view/PatternPassword;->m(Lcom/miui/packageInstaller/secure/view/PatternPassword;)Lcom/miui/packageInstaller/view/LockPatternView;

    move-result-object v4

    if-nez v4, :cond_5

    invoke-static {v3}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v4, 0x0

    :cond_5
    invoke-virtual {v4, v10}, Landroid/view/View;->setEnabled(Z)V

    iget-object v4, v7, Lcom/miui/packageInstaller/secure/view/PatternPassword$a;->h:Lcom/miui/packageInstaller/secure/view/PatternPassword;

    invoke-static {v4}, Lcom/miui/packageInstaller/secure/view/PatternPassword;->n(Lcom/miui/packageInstaller/secure/view/PatternPassword;)I

    move-result v5

    sub-int/2addr v5, v2

    invoke-static {v4, v5}, Lcom/miui/packageInstaller/secure/view/PatternPassword;->o(Lcom/miui/packageInstaller/secure/view/PatternPassword;I)V

    iget-object v4, v7, Lcom/miui/packageInstaller/secure/view/PatternPassword$a;->h:Lcom/miui/packageInstaller/secure/view/PatternPassword;

    invoke-static {v4}, Lcom/miui/packageInstaller/secure/view/PatternPassword;->k(Lcom/miui/packageInstaller/secure/view/PatternPassword;)Lx5/h;

    move-result-object v4

    if-eqz v4, :cond_6

    invoke-virtual {v4}, Lx5/h;->g()Z

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_7

    move v10, v5

    goto :goto_1

    :cond_6
    const/4 v5, 0x1

    :cond_7
    :goto_1
    if-eqz v10, :cond_9

    int-to-long v8, v2

    iput-object v6, v7, Lcom/miui/packageInstaller/secure/view/PatternPassword$a;->g:Ljava/lang/Object;

    iput v2, v7, Lcom/miui/packageInstaller/secure/view/PatternPassword$a;->e:I

    iput v5, v7, Lcom/miui/packageInstaller/secure/view/PatternPassword$a;->f:I

    invoke-static {v8, v9, v7}, Lv8/o0;->a(JLd8/d;)Ljava/lang/Object;

    move-result-object v4

    if-ne v4, v1, :cond_8

    return-object v1

    :cond_8
    :goto_2
    move v4, v5

    goto/16 :goto_0

    :cond_9
    const/4 v1, 0x0

    invoke-static {v6, v1, v5, v1}, Lv8/f0;->c(Lv8/e0;Ljava/util/concurrent/CancellationException;ILjava/lang/Object;)V

    goto :goto_3

    :cond_a
    move v5, v4

    const/4 v1, 0x0

    :goto_3
    iget-object v2, v7, Lcom/miui/packageInstaller/secure/view/PatternPassword$a;->h:Lcom/miui/packageInstaller/secure/view/PatternPassword;

    invoke-static {v2}, Lcom/miui/packageInstaller/secure/view/PatternPassword;->m(Lcom/miui/packageInstaller/secure/view/PatternPassword;)Lcom/miui/packageInstaller/view/LockPatternView;

    move-result-object v2

    if-nez v2, :cond_b

    invoke-static {v3}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v2, v1

    :cond_b
    invoke-virtual {v2, v5}, Landroid/view/View;->setEnabled(Z)V

    iget-object v2, v7, Lcom/miui/packageInstaller/secure/view/PatternPassword$a;->h:Lcom/miui/packageInstaller/secure/view/PatternPassword;

    invoke-static {v2}, Lcom/miui/packageInstaller/secure/view/PatternPassword;->l(Lcom/miui/packageInstaller/secure/view/PatternPassword;)Landroid/widget/TextView;

    move-result-object v2

    if-nez v2, :cond_c

    invoke-static {v9}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v2, v1

    :cond_c
    const-string v3, " "

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, v7, Lcom/miui/packageInstaller/secure/view/PatternPassword$a;->h:Lcom/miui/packageInstaller/secure/view/PatternPassword;

    invoke-static {v2}, Lcom/miui/packageInstaller/secure/view/PatternPassword;->l(Lcom/miui/packageInstaller/secure/view/PatternPassword;)Landroid/widget/TextView;

    move-result-object v2

    if-nez v2, :cond_d

    invoke-static {v9}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v5, v1

    goto :goto_4

    :cond_d
    move-object v5, v2

    :goto_4
    const/4 v1, 0x4

    invoke-virtual {v5, v1}, Landroid/widget/TextView;->setVisibility(I)V

    sget-object v1, La8/v;->a:La8/v;

    return-object v1
.end method

.method public final q(Lv8/e0;Ld8/d;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lv8/e0;",
            "Ld8/d<",
            "-",
            "La8/v;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/miui/packageInstaller/secure/view/PatternPassword$a;->b(Ljava/lang/Object;Ld8/d;)Ld8/d;

    move-result-object p1

    check-cast p1, Lcom/miui/packageInstaller/secure/view/PatternPassword$a;

    sget-object p2, La8/v;->a:La8/v;

    invoke-virtual {p1, p2}, Lcom/miui/packageInstaller/secure/view/PatternPassword$a;->n(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method
