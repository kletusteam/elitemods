.class public final Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject;
.super Lcom/miui/packageInstaller/ui/listcomponets/f0;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/miui/packageInstaller/ui/listcomponets/f0<",
        "Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private m:Lcom/miui/packageInstaller/model/CloudParams;

.field private n:Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/CloudParams;Ll6/c;Lm6/b;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mData"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/miui/packageInstaller/ui/listcomponets/f0;-><init>(Landroid/content/Context;Ljava/lang/Object;Ll6/c;Lm6/b;)V

    iput-object p2, p0, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject;->m:Lcom/miui/packageInstaller/model/CloudParams;

    instance-of p2, p1, Lo5/a;

    if-eqz p2, :cond_1

    invoke-static {p1}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->isSafeModelEnable(Landroid/content/Context;)Z

    move-result p2

    new-instance p3, Lp5/g;

    check-cast p1, Lo5/a;

    const-string p4, "safe_mode_switch"

    const-string v0, "switch"

    invoke-direct {p3, p4, v0, p1}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    if-eqz p2, :cond_0

    const-string p2, "on"

    goto :goto_0

    :cond_0
    const-string p2, "off"

    :goto_0
    const-string p4, "switch_action"

    invoke-virtual {p3, p4, p2}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p2

    invoke-virtual {p2}, Lp5/f;->c()Z

    new-instance p2, Lp5/g;

    const-string p3, "safe_mode_know_btn"

    const-string p4, "button"

    invoke-direct {p2, p3, p4, p1}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p2}, Lp5/f;->c()Z

    :cond_1
    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/CloudParams;Ll6/c;Lm6/b;ILm8/g;)V
    .locals 1

    and-int/lit8 p6, p5, 0x4

    const/4 v0, 0x0

    if-eqz p6, :cond_0

    move-object p3, v0

    :cond_0
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_1

    move-object p4, v0

    :cond_1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/CloudParams;Ll6/c;Lm6/b;)V

    return-void
.end method

.method private final A(Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;)V
    .locals 7

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject;->m:Lcom/miui/packageInstaller/model/CloudParams;

    iget-object v0, v0, Lcom/miui/packageInstaller/model/CloudParams;->uiConfig:Lcom/miui/packageInstaller/model/UiConfig;

    iget v0, v0, Lcom/miui/packageInstaller/model/UiConfig;->openSafeModeStyle:I

    const/4 v1, 0x1

    const v2, 0x7f080130

    const v3, 0x7f060067

    const v4, 0x7f080589

    const/16 v5, 0x8

    const/4 v6, 0x0

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->getBtnOpen()Lmiuix/slidingwidget/widget/SlidingButton;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setVisibility(I)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->getBtnOpenStyleBtn()Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->getIvIcon()Landroid/widget/ImageView;

    move-result-object v0

    const v1, 0x7f080588

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->getTitleView()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f060023

    invoke-virtual {v1, v2}, Landroid/content/Context;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->getClContentView()Landroidx/constraintlayout/widget/ConstraintLayout;

    move-result-object p1

    const v0, 0x7f08012f

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->setBackgroundResource(I)V

    goto :goto_1

    :cond_0
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->getBtnOpen()Lmiuix/slidingwidget/widget/SlidingButton;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/CheckBox;->setVisibility(I)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->getBtnOpenStyleBtn()Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->getBtnOpen()Lmiuix/slidingwidget/widget/SlidingButton;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setVisibility(I)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->getBtnOpenStyleBtn()Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    :goto_0
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->getIvIcon()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->getTitleView()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/Context;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->getClContentView()Landroidx/constraintlayout/widget/ConstraintLayout;

    move-result-object p1

    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->setBackgroundResource(I)V

    :goto_1
    return-void
.end method

.method private final B(Landroid/content/res/Configuration;)V
    .locals 3

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result v0

    if-eqz v0, :cond_1

    iget p1, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, v0, :cond_0

    iget-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject;->n:Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->getRootView()Landroid/view/ViewGroup;

    move-result-object p1

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f070145

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    invoke-virtual {p1, v1, v1, v1, v0}, Landroid/view/ViewGroup;->setPadding(IIII)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    iget-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject;->n:Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->getRootView()Landroid/view/ViewGroup;

    move-result-object p1

    if-eqz p1, :cond_1

    invoke-virtual {p1, v1, v1, v1, v1}, Landroid/view/ViewGroup;->setPadding(IIII)V

    :cond_1
    :goto_0
    return-void
.end method


# virtual methods
.method public k()I
    .locals 1

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0d0199

    goto :goto_0

    :cond_0
    const v0, 0x7f0d0198

    :goto_0
    return v0
.end method

.method public bridge synthetic o(Landroidx/recyclerview/widget/RecyclerView$d0;)V
    .locals 0

    check-cast p1, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;

    invoke-virtual {p0, p1}, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject;->z(Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;)V

    return-void
.end method

.method public z(Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;)V
    .locals 4

    const-string v0, "viewHolder"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/miui/packageInstaller/ui/listcomponets/f0;->o(Landroidx/recyclerview/widget/RecyclerView$d0;)V

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->isSafeModelEnable(Landroid/content/Context;)Z

    move-result v0

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->getBtnOpen()Lmiuix/slidingwidget/widget/SlidingButton;

    move-result-object v1

    invoke-virtual {v1, v0}, Lmiuix/slidingwidget/widget/SlidingButton;->setChecked(Z)V

    iget-object v1, p1, Landroidx/recyclerview/widget/RecyclerView$d0;->itemView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "viewHolder.itemView.context"

    invoke-static {v1, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, v1, v0}, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->updateSuggestionMsgState(Landroid/content/Context;Z)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->getTitleView()Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0700bf

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    const/high16 v3, 0x3f000000    # 0.5f

    invoke-static {v1, v2, v3}, Lf6/y;->a(Landroid/widget/TextView;FF)V

    const/16 v1, 0x8

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->getTitleView()Landroid/widget/TextView;

    move-result-object v0

    const v2, 0x7f110300

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->getTvSuggestion()Landroid/widget/TextView;

    move-result-object v0

    const v2, 0x7f110323

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->getIcArrow()Landroid/view/View;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->getBtnOpen()Lmiuix/slidingwidget/widget/SlidingButton;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->getBtnOpenStyleBtn()Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->getTitleView()Landroid/widget/TextView;

    move-result-object v0

    const v2, 0x7f1102ff

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->getTvSuggestion()Landroid/widget/TextView;

    move-result-object v0

    const v2, 0x7f110322

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->getIcArrow()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0, p1}, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject;->A(Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;)V

    :goto_0
    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject;->m:Lcom/miui/packageInstaller/model/CloudParams;

    iget-object v0, v0, Lcom/miui/packageInstaller/model/CloudParams;->unOpenSafeModeText:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;->getDesView()Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject;->m:Lcom/miui/packageInstaller/model/CloudParams;

    iget-object v1, v1, Lcom/miui/packageInstaller/model/CloudParams;->unOpenSafeModeText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject;->n:Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject$ViewHolder;

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object p1

    const-string v0, "context.resources.configuration"

    invoke-static {p1, v0}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject;->B(Landroid/content/res/Configuration;)V

    return-void
.end method
