.class public Lcom/miui/packageInstaller/view/recyclerview/adapter/AdapterDelegate;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/packageInstaller/view/recyclerview/adapter/AdapterDelegate$DefaultViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Landroidx/recyclerview/widget/RecyclerView$d0;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private a:Lcom/miui/packageInstaller/view/recyclerview/adapter/b;


# direct methods
.method public constructor <init>(Lm6/a;Lcom/miui/packageInstaller/view/recyclerview/adapter/b;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/miui/packageInstaller/view/recyclerview/adapter/AdapterDelegate;->a:Lcom/miui/packageInstaller/view/recyclerview/adapter/b;

    return-void
.end method


# virtual methods
.method public a(Ljava/util/List;ILandroidx/recyclerview/widget/RecyclerView$d0;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lm6/a;",
            ">;I",
            "Landroidx/recyclerview/widget/RecyclerView$d0;",
            ")V"
        }
    .end annotation

    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lm6/a;

    invoke-virtual {p0, p1, p3}, Lcom/miui/packageInstaller/view/recyclerview/adapter/AdapterDelegate;->c(Lm6/a;Landroidx/recyclerview/widget/RecyclerView$d0;)V

    return-void
.end method

.method public b(Ljava/util/List;ILandroidx/recyclerview/widget/RecyclerView$d0;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lm6/a;",
            ">;I",
            "Landroidx/recyclerview/widget/RecyclerView$d0;",
            "Ljava/util/List<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lm6/a;

    invoke-virtual {p0, p1, p3, p4}, Lcom/miui/packageInstaller/view/recyclerview/adapter/AdapterDelegate;->d(Lm6/a;Landroidx/recyclerview/widget/RecyclerView$d0;Ljava/util/List;)V

    return-void
.end method

.method protected c(Lm6/a;Landroidx/recyclerview/widget/RecyclerView$d0;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lm6/a;",
            "TT;)V"
        }
    .end annotation

    :try_start_0
    invoke-virtual {p1, p2}, Lm6/a;->o(Landroidx/recyclerview/widget/RecyclerView$d0;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    :goto_0
    return-void
.end method

.method protected d(Lm6/a;Landroidx/recyclerview/widget/RecyclerView$d0;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lm6/a;",
            "TT;",
            "Ljava/util/List<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    :try_start_0
    invoke-virtual {p1, p2, p3}, Lm6/a;->p(Landroidx/recyclerview/widget/RecyclerView$d0;Ljava/util/List;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    :goto_0
    return-void
.end method

.method public e(Landroid/view/ViewGroup;)Landroidx/recyclerview/widget/RecyclerView$d0;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/view/recyclerview/adapter/AdapterDelegate;->a:Lcom/miui/packageInstaller/view/recyclerview/adapter/b;

    invoke-interface {v0, p1}, Lcom/miui/packageInstaller/view/recyclerview/adapter/b;->a(Landroid/view/ViewGroup;)Landroidx/recyclerview/widget/RecyclerView$d0;

    move-result-object p1

    return-object p1
.end method
