.class public final Lcom/miui/packageInstaller/ui/listcomponets/RiskAppInstallAuthAppInfoViewObject$ViewHolder;
.super Landroidx/recyclerview/widget/RecyclerView$d0;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/packageInstaller/ui/listcomponets/RiskAppInstallAuthAppInfoViewObject;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ViewHolder"
.end annotation


# instance fields
.field private ivIcon:Landroid/widget/ImageView;

.field private mAuthResultText:Landroid/widget/TextView;

.field private final mContentLayout:Landroid/widget/LinearLayout;

.field private mRiskContentDes:Landroid/view/View;

.field private tvAppName:Landroid/widget/TextView;

.field private tvAppVersion:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    const-string v0, "itemView"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$d0;-><init>(Landroid/view/View;)V

    const v0, 0x7f0a02a1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "itemView.findViewById(R.id.perm_install_app_name)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppInstallAuthAppInfoViewObject$ViewHolder;->tvAppName:Landroid/widget/TextView;

    const v0, 0x7f0a02a2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "itemView.findViewById(R.\u2026perm_install_app_version)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppInstallAuthAppInfoViewObject$ViewHolder;->tvAppVersion:Landroid/widget/TextView;

    const v0, 0x7f0a02a0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "itemView.findViewById(R.id.perm_app_install_icon)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppInstallAuthAppInfoViewObject$ViewHolder;->ivIcon:Landroid/widget/ImageView;

    const v0, 0x7f0a01c3

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "itemView.findViewById(R.\u2026tem_risk_app_install_des)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppInstallAuthAppInfoViewObject$ViewHolder;->mRiskContentDes:Landroid/view/View;

    const v0, 0x7f0a01ac

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "itemView.findViewById(R.\u2026install_confirm_question)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppInstallAuthAppInfoViewObject$ViewHolder;->mAuthResultText:Landroid/widget/TextView;

    const v0, 0x7f0a01be

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v0, "itemView.findViewById(R.\u2026m_content_install_layout)"

    invoke-static {p1, v0}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppInstallAuthAppInfoViewObject$ViewHolder;->mContentLayout:Landroid/widget/LinearLayout;

    return-void
.end method


# virtual methods
.method public final getIvIcon()Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppInstallAuthAppInfoViewObject$ViewHolder;->ivIcon:Landroid/widget/ImageView;

    return-object v0
.end method

.method public final getMAuthResultText()Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppInstallAuthAppInfoViewObject$ViewHolder;->mAuthResultText:Landroid/widget/TextView;

    return-object v0
.end method

.method public final getMContentLayout()Landroid/widget/LinearLayout;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppInstallAuthAppInfoViewObject$ViewHolder;->mContentLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method public final getMRiskContentDes()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppInstallAuthAppInfoViewObject$ViewHolder;->mRiskContentDes:Landroid/view/View;

    return-object v0
.end method

.method public final getTvAppName()Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppInstallAuthAppInfoViewObject$ViewHolder;->tvAppName:Landroid/widget/TextView;

    return-object v0
.end method

.method public final getTvAppVersion()Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppInstallAuthAppInfoViewObject$ViewHolder;->tvAppVersion:Landroid/widget/TextView;

    return-object v0
.end method

.method public final setIvIcon(Landroid/widget/ImageView;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppInstallAuthAppInfoViewObject$ViewHolder;->ivIcon:Landroid/widget/ImageView;

    return-void
.end method

.method public final setMAuthResultText(Landroid/widget/TextView;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppInstallAuthAppInfoViewObject$ViewHolder;->mAuthResultText:Landroid/widget/TextView;

    return-void
.end method

.method public final setMRiskContentDes(Landroid/view/View;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppInstallAuthAppInfoViewObject$ViewHolder;->mRiskContentDes:Landroid/view/View;

    return-void
.end method

.method public final setTvAppName(Landroid/widget/TextView;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppInstallAuthAppInfoViewObject$ViewHolder;->tvAppName:Landroid/widget/TextView;

    return-void
.end method

.method public final setTvAppVersion(Landroid/widget/TextView;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppInstallAuthAppInfoViewObject$ViewHolder;->tvAppVersion:Landroid/widget/TextView;

    return-void
.end method
