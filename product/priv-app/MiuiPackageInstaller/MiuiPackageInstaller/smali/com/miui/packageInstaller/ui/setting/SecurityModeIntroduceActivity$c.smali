.class public final Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$c;
.super Ljava/lang/Object;

# interfaces
.implements La6/d$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;->c1()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;


# direct methods
.method constructor <init>(Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$c;->a:Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    new-instance v0, Lx5/j$a;

    iget-object v1, p0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$c;->a:Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;

    invoke-direct {v0, v1}, Lx5/j$a;-><init>(Landroid/app/Activity;)V

    const v1, 0x7f11030e

    invoke-virtual {v0, v1}, Lx5/j$a;->l(I)Lx5/j$a;

    move-result-object v0

    const v1, 0x7f1103e3

    invoke-virtual {v0, v1}, Lx5/j$a;->h(I)Lx5/j$a;

    move-result-object v0

    const v1, 0x7f1100ae

    invoke-virtual {v0, v1}, Lx5/j$a;->f(I)Lx5/j$a;

    move-result-object v0

    const v1, 0x7f1100ad

    invoke-virtual {v0, v1}, Lx5/j$a;->e(I)Lx5/j$a;

    move-result-object v0

    sget-object v1, Lx5/d;->b:Lx5/d$a;

    invoke-virtual {v1}, Lx5/d$a;->a()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lx5/j$a;->c(Ljava/util/ArrayList;)Lx5/j$a;

    move-result-object v0

    invoke-virtual {v0}, Lx5/j$a;->a()Lx5/j;

    move-result-object v0

    new-instance v1, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$c$a;

    iget-object v2, p0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$c;->a:Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;

    invoke-direct {v1, v2}, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$c$a;-><init>(Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;)V

    invoke-virtual {v0, v1}, Lx5/j;->p(Ll8/p;)V

    new-instance v0, Lp5/b;

    iget-object v1, p0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$c;->a:Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;

    const-string v2, "safe_mode_close_warning_popup_continue_btn"

    const-string v3, "button"

    invoke-direct {v0, v2, v3, v1}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    return-void
.end method

.method public cancel()V
    .locals 4

    new-instance v0, Lp5/b;

    iget-object v1, p0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$c;->a:Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;

    const-string v2, "safe_mode_close_warning_popup_cancel_btn"

    const-string v3, "button"

    invoke-direct {v0, v2, v3, v1}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    return-void
.end method
