.class public final Lcom/miui/packageInstaller/model/CInfo;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private cFlag:Ljava/lang/Boolean;

.field private cOffset:J

.field private cSize:J


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getCFlag()Ljava/lang/Boolean;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/model/CInfo;->cFlag:Ljava/lang/Boolean;

    return-object v0
.end method

.method public final getCOffset()J
    .locals 2

    iget-wide v0, p0, Lcom/miui/packageInstaller/model/CInfo;->cOffset:J

    return-wide v0
.end method

.method public final getCSize()J
    .locals 2

    iget-wide v0, p0, Lcom/miui/packageInstaller/model/CInfo;->cSize:J

    return-wide v0
.end method

.method public final setCFlag(Ljava/lang/Boolean;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/model/CInfo;->cFlag:Ljava/lang/Boolean;

    return-void
.end method

.method public final setCOffset(J)V
    .locals 0

    iput-wide p1, p0, Lcom/miui/packageInstaller/model/CInfo;->cOffset:J

    return-void
.end method

.method public final setCSize(J)V
    .locals 0

    iput-wide p1, p0, Lcom/miui/packageInstaller/model/CInfo;->cSize:J

    return-void
.end method
