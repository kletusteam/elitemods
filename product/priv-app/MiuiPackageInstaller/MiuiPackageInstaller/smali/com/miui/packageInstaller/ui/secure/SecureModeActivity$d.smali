.class final Lcom/miui/packageInstaller/ui/secure/SecureModeActivity$d;
.super Lf8/k;

# interfaces
.implements Ll8/p;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->onResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf8/k;",
        "Ll8/p<",
        "Lv8/e0;",
        "Ld8/d<",
        "-",
        "La8/v;",
        ">;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation runtime Lf8/f;
    c = "com.miui.packageInstaller.ui.secure.SecureModeActivity$onResume$1"
    f = "SecureModeActivity.kt"
    l = {
        0xe6
    }
    m = "invokeSuspend"
.end annotation


# instance fields
.field e:I

.field private synthetic f:Ljava/lang/Object;

.field final synthetic g:Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;


# direct methods
.method constructor <init>(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;Ld8/d;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;",
            "Ld8/d<",
            "-",
            "Lcom/miui/packageInstaller/ui/secure/SecureModeActivity$d;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity$d;->g:Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;

    const/4 p1, 0x2

    invoke-direct {p0, p1, p2}, Lf8/k;-><init>(ILd8/d;)V

    return-void
.end method


# virtual methods
.method public final b(Ljava/lang/Object;Ld8/d;)Ld8/d;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ld8/d<",
            "*>;)",
            "Ld8/d<",
            "La8/v;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity$d;

    iget-object v1, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity$d;->g:Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;

    invoke-direct {v0, v1, p2}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity$d;-><init>(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;Ld8/d;)V

    iput-object p1, v0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity$d;->f:Ljava/lang/Object;

    return-object v0
.end method

.method public bridge synthetic g(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lv8/e0;

    check-cast p2, Ld8/d;

    invoke-virtual {p0, p1, p2}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity$d;->q(Lv8/e0;Ld8/d;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final n(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 9

    invoke-static {}, Le8/b;->c()Ljava/lang/Object;

    move-result-object v0

    iget v1, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity$d;->e:I

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    if-ne v1, v2, :cond_0

    invoke-static {p1}, La8/n;->b(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    invoke-static {p1}, La8/n;->b(Ljava/lang/Object;)V

    iget-object p1, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity$d;->f:Ljava/lang/Object;

    move-object v3, p1

    check-cast v3, Lv8/e0;

    invoke-static {}, Lv8/t0;->a()Lv8/a0;

    move-result-object v4

    const/4 v5, 0x0

    new-instance v6, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity$d$a;

    iget-object p1, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity$d;->g:Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;

    const/4 v1, 0x0

    invoke-direct {v6, p1, v1}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity$d$a;-><init>(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;Ld8/d;)V

    const/4 v7, 0x2

    const/4 v8, 0x0

    invoke-static/range {v3 .. v8}, Lv8/f;->b(Lv8/e0;Ld8/g;Lv8/g0;Ll8/p;ILjava/lang/Object;)Lv8/l0;

    move-result-object p1

    iput v2, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity$d;->e:I

    invoke-interface {p1, p0}, Lv8/l0;->z(Ld8/d;)Ljava/lang/Object;

    move-result-object p1

    if-ne p1, v0, :cond_2

    return-object v0

    :cond_2
    :goto_0
    iget-object p1, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity$d;->g:Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->i1()V

    iget-object p1, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity$d;->g:Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;

    invoke-static {p1}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->c1(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;)Z

    move-result p1

    const-string v0, "safe_mode_homepage_status"

    const-string v1, "button"

    if-eqz p1, :cond_3

    new-instance p1, Lp5/g;

    iget-object v2, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity$d;->g:Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;

    const-string v3, "safe_mode_started_services"

    invoke-direct {p1, v3, v1, v2}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    iget-object v2, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity$d;->g:Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;

    invoke-static {v2}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->a1(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p1

    invoke-virtual {p1}, Lp5/f;->c()Z

    iget-object p1, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity$d;->g:Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;

    invoke-static {p1}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->X0(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;)Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;

    iget-object p1, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity$d;->g:Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;

    new-instance v2, Lp5/g;

    const-string v3, "safe_mode_authorize_btn"

    invoke-direct {v2, v3, v1, p1}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-static {p1}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->a1(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, v0, p1}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p1

    goto :goto_1

    :cond_3
    new-instance p1, Lp5/g;

    iget-object v2, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity$d;->g:Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;

    const-string v3, "safe_mode_start_protect_open_btn"

    invoke-direct {p1, v3, v1, v2}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    iget-object v1, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity$d;->g:Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;

    invoke-static {v1}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->a1(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p1

    :goto_1
    invoke-virtual {p1}, Lp5/f;->c()Z

    sget-object p1, La8/v;->a:La8/v;

    return-object p1
.end method

.method public final q(Lv8/e0;Ld8/d;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lv8/e0;",
            "Ld8/d<",
            "-",
            "La8/v;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity$d;->b(Ljava/lang/Object;Ld8/d;)Ld8/d;

    move-result-object p1

    check-cast p1, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity$d;

    sget-object p2, La8/v;->a:La8/v;

    invoke-virtual {p1, p2}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity$d;->n(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method
