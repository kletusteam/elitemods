.class public final Lcom/miui/packageInstaller/ui/listcomponets/AdTitleViewObject;
.super Lm6/a;

# interfaces
.implements Lcom/miui/packageInstaller/ui/listcomponets/v;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/packageInstaller/ui/listcomponets/AdTitleViewObject$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lm6/a<",
        "Lcom/miui/packageInstaller/ui/listcomponets/AdTitleViewObject$ViewHolder;",
        ">;",
        "Lcom/miui/packageInstaller/ui/listcomponets/v;"
    }
.end annotation


# instance fields
.field private final l:Lcom/miui/packageInstaller/model/AdTitleModel;

.field private m:Lcom/miui/packageInstaller/model/AdTitleModel;

.field private final n:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/AdTitleModel;Ll6/c;Lm6/b;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mData"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2, p3, p4}, Lm6/a;-><init>(Landroid/content/Context;Ljava/lang/Object;Ll6/c;Lm6/b;)V

    iput-object p2, p0, Lcom/miui/packageInstaller/ui/listcomponets/AdTitleViewObject;->l:Lcom/miui/packageInstaller/model/AdTitleModel;

    iput-object p2, p0, Lcom/miui/packageInstaller/ui/listcomponets/AdTitleViewObject;->m:Lcom/miui/packageInstaller/model/AdTitleModel;

    new-instance p2, Lp5/g;

    move-object p3, p1

    check-cast p3, Lo5/a;

    const-string p4, "advertise_close_btn"

    const-string v0, "button"

    invoke-direct {p2, p4, v0, p3}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p2}, Lp5/f;->c()Z

    new-instance p2, Lcom/miui/packageInstaller/ui/listcomponets/a;

    invoke-direct {p2, p0, p1}, Lcom/miui/packageInstaller/ui/listcomponets/a;-><init>(Lcom/miui/packageInstaller/ui/listcomponets/AdTitleViewObject;Landroid/content/Context;)V

    iput-object p2, p0, Lcom/miui/packageInstaller/ui/listcomponets/AdTitleViewObject;->n:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/AdTitleModel;Ll6/c;Lm6/b;ILm8/g;)V
    .locals 1

    and-int/lit8 p6, p5, 0x4

    const/4 v0, 0x0

    if-eqz p6, :cond_0

    move-object p3, v0

    :cond_0
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_1

    move-object p4, v0

    :cond_1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/miui/packageInstaller/ui/listcomponets/AdTitleViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/AdTitleModel;Ll6/c;Lm6/b;)V

    return-void
.end method

.method private static final A(Lcom/miui/packageInstaller/ui/listcomponets/AdTitleViewObject;Landroid/content/Context;Landroid/view/View;)V
    .locals 2

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$context"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/view/View;->getId()I

    move-result p2

    const v0, 0x7f0a01f1

    if-ne p2, v0, :cond_2

    invoke-virtual {p0}, Lm6/a;->f()Lj6/b;

    move-result-object p2

    if-eqz p2, :cond_2

    invoke-virtual {p0}, Lm6/a;->f()Lj6/b;

    move-result-object p2

    invoke-virtual {p2}, Lj6/b;->R()Ljava/util/List;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_0
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lm6/a;

    instance-of v1, v0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lm6/a;->t()V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lm6/a;->t()V

    new-instance p0, Lp5/b;

    check-cast p1, Lo5/a;

    const-string p2, "advertise_close_btn"

    const-string v0, "button"

    invoke-direct {p0, p2, v0, p1}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p0}, Lp5/f;->c()Z

    :cond_2
    return-void
.end method

.method public static synthetic z(Lcom/miui/packageInstaller/ui/listcomponets/AdTitleViewObject;Landroid/content/Context;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/miui/packageInstaller/ui/listcomponets/AdTitleViewObject;->A(Lcom/miui/packageInstaller/ui/listcomponets/AdTitleViewObject;Landroid/content/Context;Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public B(Lcom/miui/packageInstaller/ui/listcomponets/AdTitleViewObject$ViewHolder;)V
    .locals 3

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/AdTitleViewObject$ViewHolder;->getAdDes()Landroid/widget/TextView;

    move-result-object v1

    goto :goto_0

    :cond_0
    move-object v1, v0

    :goto_0
    if-nez v1, :cond_1

    goto :goto_1

    :cond_1
    iget-object v2, p0, Lcom/miui/packageInstaller/ui/listcomponets/AdTitleViewObject;->m:Lcom/miui/packageInstaller/model/AdTitleModel;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/miui/packageInstaller/model/AdTitleModel;->getTitle()Ljava/lang/String;

    move-result-object v0

    :cond_2
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/AdTitleViewObject$ViewHolder;->getAdClose()Landroid/widget/LinearLayout;

    move-result-object p1

    if-eqz p1, :cond_3

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/AdTitleViewObject;->n:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_3
    return-void
.end method

.method public k()I
    .locals 1

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0d002d

    goto :goto_0

    :cond_0
    const v0, 0x7f0d002c

    :goto_0
    return v0
.end method

.method public bridge synthetic o(Landroidx/recyclerview/widget/RecyclerView$d0;)V
    .locals 0

    check-cast p1, Lcom/miui/packageInstaller/ui/listcomponets/AdTitleViewObject$ViewHolder;

    invoke-virtual {p0, p1}, Lcom/miui/packageInstaller/ui/listcomponets/AdTitleViewObject;->B(Lcom/miui/packageInstaller/ui/listcomponets/AdTitleViewObject$ViewHolder;)V

    return-void
.end method
