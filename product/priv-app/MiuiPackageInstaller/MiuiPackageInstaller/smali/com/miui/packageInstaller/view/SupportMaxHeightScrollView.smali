.class public Lcom/miui/packageInstaller/view/SupportMaxHeightScrollView;
.super Landroidx/core/widget/NestedScrollView;


# instance fields
.field private C:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroidx/core/widget/NestedScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 1

    iget p2, p0, Lcom/miui/packageInstaller/view/SupportMaxHeightScrollView;->C:I

    const/high16 v0, -0x80000000

    invoke-static {p2, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    invoke-super {p0, p1, p2}, Landroidx/core/widget/NestedScrollView;->onMeasure(II)V

    return-void
.end method

.method public setMaxHeight(I)V
    .locals 0

    iput p1, p0, Lcom/miui/packageInstaller/view/SupportMaxHeightScrollView;->C:I

    return-void
.end method
