.class public final Lcom/miui/packageInstaller/ui/listcomponets/CloseReasonViewObject;
.super Lm6/a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/packageInstaller/ui/listcomponets/CloseReasonViewObject$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lm6/a<",
        "Lcom/miui/packageInstaller/ui/listcomponets/CloseReasonViewObject$ViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private l:Lcom/miui/packageInstaller/model/CloseReasonItemData;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/CloseReasonItemData;Ll6/c;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "reasonItemData"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lm6/a;-><init>(Landroid/content/Context;Ljava/lang/Object;Ll6/c;Lm6/b;)V

    iput-object p2, p0, Lcom/miui/packageInstaller/ui/listcomponets/CloseReasonViewObject;->l:Lcom/miui/packageInstaller/model/CloseReasonItemData;

    return-void
.end method

.method private static final B(Lcom/miui/packageInstaller/ui/listcomponets/CloseReasonViewObject;Landroid/widget/CompoundButton;Z)V
    .locals 0

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/CloseReasonViewObject;->l:Lcom/miui/packageInstaller/model/CloseReasonItemData;

    invoke-virtual {p1, p2}, Lcom/miui/packageInstaller/model/CloseReasonItemData;->setChecked(Z)V

    const p1, 0x7f0a00bc

    invoke-virtual {p0, p1}, Lm6/a;->r(I)V

    return-void
.end method

.method public static synthetic z(Lcom/miui/packageInstaller/ui/listcomponets/CloseReasonViewObject;Landroid/widget/CompoundButton;Z)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/miui/packageInstaller/ui/listcomponets/CloseReasonViewObject;->B(Lcom/miui/packageInstaller/ui/listcomponets/CloseReasonViewObject;Landroid/widget/CompoundButton;Z)V

    return-void
.end method


# virtual methods
.method public A(Lcom/miui/packageInstaller/ui/listcomponets/CloseReasonViewObject$ViewHolder;)V
    .locals 2

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/CloseReasonViewObject$ViewHolder;->getMCbReason()Landroid/widget/CheckBox;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_1

    goto :goto_1

    :cond_1
    iget-object v1, p0, Lcom/miui/packageInstaller/ui/listcomponets/CloseReasonViewObject;->l:Lcom/miui/packageInstaller/model/CloseReasonItemData;

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/CloseReasonItemData;->getReason()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/CloseReasonViewObject$ViewHolder;->getMCbReason()Landroid/widget/CheckBox;

    move-result-object p1

    if-eqz p1, :cond_2

    new-instance v0, Lcom/miui/packageInstaller/ui/listcomponets/f;

    invoke-direct {v0, p0}, Lcom/miui/packageInstaller/ui/listcomponets/f;-><init>(Lcom/miui/packageInstaller/ui/listcomponets/CloseReasonViewObject;)V

    invoke-virtual {p1, v0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    :cond_2
    return-void
.end method

.method public k()I
    .locals 1

    const v0, 0x7f0d0196

    return v0
.end method

.method public bridge synthetic o(Landroidx/recyclerview/widget/RecyclerView$d0;)V
    .locals 0

    check-cast p1, Lcom/miui/packageInstaller/ui/listcomponets/CloseReasonViewObject$ViewHolder;

    invoke-virtual {p0, p1}, Lcom/miui/packageInstaller/ui/listcomponets/CloseReasonViewObject;->A(Lcom/miui/packageInstaller/ui/listcomponets/CloseReasonViewObject$ViewHolder;)V

    return-void
.end method
