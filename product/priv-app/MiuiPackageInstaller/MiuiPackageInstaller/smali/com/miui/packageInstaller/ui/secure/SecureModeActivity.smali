.class public Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;
.super Lq2/b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/packageInstaller/ui/secure/SecureModeActivity$a;
    }
.end annotation


# static fields
.field public static final Z:Lcom/miui/packageInstaller/ui/secure/SecureModeActivity$a;


# instance fields
.field private A:Landroid/widget/TextView;

.field private B:Landroidx/appcompat/widget/LinearLayoutCompat;

.field private C:Landroidx/appcompat/widget/LinearLayoutCompat;

.field private D:Landroid/widget/TextView;

.field private E:Landroid/view/ViewGroup;

.field private F:Landroid/view/ViewGroup;

.field private G:Lj6/b;

.field private H:Landroid/widget/FrameLayout;

.field private I:Landroid/view/View;

.field private J:Landroid/view/View;

.field private K:Landroid/widget/FrameLayout;

.field private L:Landroid/view/View;

.field private M:Landroid/view/View;

.field private N:Landroid/widget/TextView;

.field private O:Z

.field private P:J

.field private Q:I

.field private R:Lm5/e;

.field private S:Lcom/miui/packageInstaller/model/ApkInfo;

.field private T:Ljava/lang/String;

.field private U:Ljava/lang/String;

.field private V:J

.field private W:Ljava/lang/String;

.field private X:Z

.field private Y:Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;

.field private u:Landroid/widget/TextView;

.field private v:Landroid/widget/TextView;

.field private w:Landroid/widget/TextView;

.field private x:Landroid/widget/ImageView;

.field private y:Landroidx/recyclerview/widget/RecyclerView;

.field private z:Landroid/view/ViewGroup;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity$a;-><init>(Lm8/g;)V

    sput-object v0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->Z:Lcom/miui/packageInstaller/ui/secure/SecureModeActivity$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lq2/b;-><init>()V

    const-string v0, "packageinstaller"

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->T:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->W:Ljava/lang/String;

    return-void
.end method

.method private final A1(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lq2/b;->q:Lo5/b;

    invoke-virtual {v0}, Lo5/b;->d()V

    const-string v0, "child_mode"

    invoke-static {p1, v0}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    const-string v0, "button"

    if-eqz p1, :cond_0

    iget-object p1, p0, Lq2/b;->q:Lo5/b;

    const-string v1, "appstore_for_child"

    invoke-virtual {p1, v1}, Lo5/b;->B(Ljava/lang/String;)V

    new-instance p1, Lp5/b;

    const-string v1, "safe_mode_opened_toast_know_btn"

    invoke-direct {p1, v1, v0, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lq2/b;->q:Lo5/b;

    const-string v1, "appstore_for_old"

    invoke-virtual {p1, v1}, Lo5/b;->B(Ljava/lang/String;)V

    new-instance p1, Lp5/b;

    const-string v1, "protect_mode_opened_toast_know_btn"

    invoke-direct {p1, v1, v0, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    :goto_0
    invoke-virtual {p1}, Lp5/f;->c()Z

    iget-object p1, p0, Lq2/b;->q:Lo5/b;

    invoke-virtual {p1}, Lo5/b;->c()V

    return-void
.end method

.method private final C1()V
    .locals 4

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->z:Landroid/view/ViewGroup;

    if-nez v0, :cond_0

    const-string v0, "llStatusLayout"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v0, 0x0

    :cond_0
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    const-string v2, "null cannot be cast to non-null type android.widget.FrameLayout.LayoutParams"

    invoke-static {v1, v2}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Landroid/widget/FrameLayout$LayoutParams;

    iget-boolean v2, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->O:Z

    if-eqz v2, :cond_1

    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070155

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    :goto_0
    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_2
    return-void
.end method

.method private static final E1(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;Landroid/content/DialogInterface;I)V
    .locals 1

    const-string p2, "this$0"

    invoke-static {p0, p2}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "dialog"

    invoke-static {p1, p2}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    new-instance p1, Lp5/b;

    const-string p2, "single_authorize_popup_cancel_btn"

    const-string v0, "button"

    invoke-direct {p1, p2, v0, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p1}, Lp5/f;->c()Z

    return-void
.end method

.method private static final F1(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;Landroid/content/DialogInterface;I)V
    .locals 1

    const-string p3, "this$0"

    invoke-static {p0, p3}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p3, "$authInfo"

    invoke-static {p1, p3}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p3, "dialog"

    invoke-static {p2, p3}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2}, Landroid/content/DialogInterface;->dismiss()V

    new-instance p2, Ld6/l0;

    iget-object p3, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->T:Ljava/lang/String;

    invoke-direct {p2, p0, p3}, Ld6/l0;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iget-object p3, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->S:Lcom/miui/packageInstaller/model/ApkInfo;

    new-instance v0, Ld6/a0;

    invoke-direct {v0, p0}, Ld6/a0;-><init>(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;)V

    invoke-virtual {p2, p3, p1, v0}, Ld6/l0;->e(Lcom/miui/packageInstaller/model/ApkInfo;Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;Ljava/lang/Runnable;)V

    new-instance p1, Lp5/b;

    const-string p2, "single_authorize_popup_authorize_btn"

    const-string p3, "button"

    invoke-direct {p1, p2, p3, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p1}, Lp5/f;->c()Z

    return-void
.end method

.method private static final G1(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;)V
    .locals 4

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->D:Landroid/widget/TextView;

    const/4 v1, 0x0

    const-string v2, "btnAction"

    if-nez v0, :cond_0

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_0
    const v3, 0x7f1102f6

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->D:Landroid/widget/TextView;

    if-nez v0, :cond_1

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move-object v1, v0

    :goto_0
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->T:Ljava/lang/String;

    const-string v1, "packageinstaller"

    invoke-static {v0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lmiuix/appcompat/app/j;->finish()V

    :cond_2
    return-void
.end method

.method private static final H1(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;Landroid/content/DialogInterface;)V
    .locals 2

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Lp5/b;

    const-string v0, "single_authorize_popup_back_btn"

    const-string v1, "button"

    invoke-direct {p1, v0, v1, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p1}, Lp5/f;->c()Z

    return-void
.end method

.method private final I1()V
    .locals 2

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object v0

    new-instance v1, Ld6/z;

    invoke-direct {v1, p0}, Ld6/z;-><init>(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;)V

    invoke-virtual {v0, v1}, Lf6/z;->g(Ljava/lang/Runnable;)V

    return-void
.end method

.method public static synthetic J0(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;Landroid/content/DialogInterface;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->H1(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;Landroid/content/DialogInterface;)V

    return-void
.end method

.method private static final J1(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;)V
    .locals 6

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "renamed.config"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/app/Activity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v2, "isShowed"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    return-void

    :cond_0
    const-string v1, "com.android.settings"

    :try_start_0
    invoke-virtual {p0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v3

    const-string v4, "packageManager.getResour\u2026ation(settingPackageName)"

    invoke-static {v3, v4}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "safe_install_mode_settings"

    const-string v5, "string"

    invoke-virtual {v3, v4, v5, v1}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    const/4 v4, 0x1

    if-nez v1, :cond_1

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p0

    invoke-interface {p0, v2, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object p0

    invoke-interface {p0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void

    :cond_1
    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v3, "settingsResource.getString(securitySettingId)"

    invoke-static {v1, v3}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f1102f4

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object v1

    new-instance v3, Ld6/k0;

    invoke-direct {v3, p0}, Ld6/k0;-><init>(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;)V

    invoke-virtual {v1, v3}, Lf6/z;->e(Ljava/lang/Runnable;)V

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p0

    invoke-interface {p0, v2, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object p0

    invoke-interface {p0}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_2
    return-void
.end method

.method public static synthetic K0(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->x1(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;Landroid/view/View;)V

    return-void
.end method

.method private static final K1(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    :try_start_0
    new-instance v0, La6/y;

    invoke-direct {v0, p0}, La6/y;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v0}, La6/y;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method public static synthetic L0(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;Landroid/content/DialogInterface;I)V
    .locals 0

    invoke-static {p0, p1, p2, p3}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->F1(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;Landroid/content/DialogInterface;I)V

    return-void
.end method

.method private final L1()V
    .locals 2

    invoke-virtual {p0}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->m1()V

    const v0, 0x7f1102c3

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method public static synthetic M0(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;)V
    .locals 0

    invoke-static {p0}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->K1(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;)V

    return-void
.end method

.method private final M1()V
    .locals 10

    const v0, 0x7f0a005f

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result v2

    if-eqz v2, :cond_0

    const v2, 0x7f0d009a

    goto :goto_0

    :cond_0
    const v2, 0x7f0d0099

    :goto_0
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0a0063

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f07010f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    const/high16 v4, 0x3f000000    # 0.5f

    invoke-static {v2, v3, v4}, Lf6/y;->a(Landroid/widget/TextView;FF)V

    const v2, 0x7f0a0060

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const-string v3, "animViewGroup.findViewBy\u2026d.anim_group_root_layout)"

    invoke-static {v2, v3}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Landroidx/appcompat/widget/LinearLayoutCompat;

    iput-object v2, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->C:Landroidx/appcompat/widget/LinearLayoutCompat;

    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->s1()V

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;II)V

    const v0, 0x7f0a0307

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "animViewGroup.findViewById(R.id.service_list)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v7, v0

    check-cast v7, Landroidx/recyclerview/widget/RecyclerView;

    new-instance v0, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-direct {v0, p0}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    invoke-virtual {v7, v0}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$o;)V

    new-instance v8, Ljava/util/ArrayList;

    const/4 v0, 0x4

    invoke-direct {v8, v0}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v9, Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceAnimViewObject;

    const v2, 0x7f110356

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xc

    const/4 v6, 0x0

    move-object v0, v9

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceAnimViewObject;-><init>(Landroid/content/Context;ILl6/c;Lm6/b;ILm8/g;)V

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v9, Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceAnimViewObject;

    const v2, 0x7f110357

    move-object v0, v9

    invoke-direct/range {v0 .. v6}, Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceAnimViewObject;-><init>(Landroid/content/Context;ILl6/c;Lm6/b;ILm8/g;)V

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v9, Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceAnimViewObject;

    const v2, 0x7f110358

    move-object v0, v9

    invoke-direct/range {v0 .. v6}, Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceAnimViewObject;-><init>(Landroid/content/Context;ILl6/c;Lm6/b;ILm8/g;)V

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v9, Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceAnimViewObject;

    const v2, 0x7f110359

    move-object v0, v9

    invoke-direct/range {v0 .. v6}, Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceAnimViewObject;-><init>(Landroid/content/Context;ILl6/c;Lm6/b;ILm8/g;)V

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v9, Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceAnimViewObject;

    const v2, 0x7f11035a

    move-object v0, v9

    invoke-direct/range {v0 .. v6}, Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceAnimViewObject;-><init>(Landroid/content/Context;ILl6/c;Lm6/b;ILm8/g;)V

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lj6/b;

    invoke-direct {v0, v7}, Lj6/b;-><init>(Landroidx/recyclerview/widget/RecyclerView;)V

    invoke-virtual {v0, v8}, Lj6/b;->j0(Ljava/util/List;)V

    invoke-static {p0}, Landroidx/lifecycle/k;->a(Landroidx/lifecycle/j;)Landroidx/lifecycle/e;

    move-result-object v1

    invoke-static {}, Lv8/t0;->c()Lv8/v1;

    move-result-object v2

    new-instance v4, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity$e;

    const/4 v0, 0x0

    invoke-direct {v4, p0, v0}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity$e;-><init>(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;Ld8/d;)V

    const/4 v5, 0x2

    invoke-static/range {v1 .. v6}, Lv8/f;->d(Lv8/e0;Ld8/g;Lv8/g0;Ll8/p;ILjava/lang/Object;)Lv8/k1;

    return-void
.end method

.method public static synthetic N0(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->k1(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic O0(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->l1(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic P0(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;)V
    .locals 0

    invoke-static {p0}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->J1(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;)V

    return-void
.end method

.method public static synthetic Q0(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->y1(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic R0(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->j1(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic S0(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;Landroid/content/DialogInterface;I)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->E1(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;Landroid/content/DialogInterface;I)V

    return-void
.end method

.method public static synthetic T0(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->w1(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic U0(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;)V
    .locals 0

    invoke-static {p0}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->G1(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;)V

    return-void
.end method

.method public static synthetic V0(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;)V
    .locals 0

    invoke-static {p0}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->q1(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;)V

    return-void
.end method

.method public static final synthetic W0(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;)Landroid/view/ViewGroup;
    .locals 0

    iget-object p0, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->F:Landroid/view/ViewGroup;

    return-object p0
.end method

.method public static final synthetic X0(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;)Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;
    .locals 0

    iget-object p0, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->Y:Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;

    return-object p0
.end method

.method public static final synthetic Y0(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;)J
    .locals 2

    iget-wide v0, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->V:J

    return-wide v0
.end method

.method public static final synthetic Z0(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;)J
    .locals 2

    iget-wide v0, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->P:J

    return-wide v0
.end method

.method public static final synthetic a1(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;)Ljava/lang/String;
    .locals 0

    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->o1()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic b1(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->t1()V

    return-void
.end method

.method public static final synthetic c1(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->O:Z

    return p0
.end method

.method public static final synthetic d1(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->W:Ljava/lang/String;

    return-void
.end method

.method public static final synthetic e1(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;J)V
    .locals 0

    iput-wide p1, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->V:J

    return-void
.end method

.method public static final synthetic f1(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;J)V
    .locals 0

    iput-wide p1, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->P:J

    return-void
.end method

.method public static final synthetic g1(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->X:Z

    return-void
.end method

.method private final h1()V
    .locals 10

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->U:Ljava/lang/String;

    const-string v1, "packageinstaller_elderly_type"

    invoke-static {v0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Lp5/g;

    const-string v1, "current_install_button"

    const-string v2, "button"

    invoke-direct {v0, v1, v2, p0}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    iget-object v1, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->R:Lm5/e;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lm5/e;->j()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_0
    move-object v1, v2

    :goto_0
    const-string v3, "related_package_name"

    invoke-virtual {v0, v3, v1}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object v0

    invoke-virtual {v0}, Lp5/f;->c()Z

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->G:Lj6/b;

    if-nez v0, :cond_1

    const-string v0, "mServiceListAdapter"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    move-object v2, v0

    :goto_1
    new-instance v0, Lcom/miui/packageInstaller/ui/listcomponets/WaitInstallAppViewObject;

    iget-object v5, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->R:Lm5/e;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0xc

    const/4 v9, 0x0

    move-object v3, v0

    move-object v4, p0

    invoke-direct/range {v3 .. v9}, Lcom/miui/packageInstaller/ui/listcomponets/WaitInstallAppViewObject;-><init>(Landroid/content/Context;Lm5/e;Ll6/c;Lm6/b;ILm8/g;)V

    invoke-virtual {v2, v0}, Lj6/b;->J(Lm6/a;)I

    :cond_2
    return-void
.end method

.method private static final j1(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;Landroid/view/View;)V
    .locals 2

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Lp5/b;

    const-string v0, "safe_mode_authorize_btn"

    const-string v1, "button"

    invoke-direct {p1, v0, v1, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->o1()Ljava/lang/String;

    move-result-object v0

    const-string v1, "safe_mode_homepage_status"

    invoke-virtual {p1, v1, v0}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p1

    invoke-virtual {p1}, Lp5/f;->c()Z

    invoke-virtual {p0}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->D1()V

    return-void
.end method

.method private static final k1(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;Landroid/view/View;)V
    .locals 2

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Lp5/b;

    const-string v0, "safe_mode_started_services"

    const-string v1, "button"

    invoke-direct {p1, v0, v1, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->o1()Ljava/lang/String;

    move-result-object v0

    const-string v1, "safe_mode_homepage_status"

    invoke-virtual {p1, v1, v0}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p1

    invoke-virtual {p1}, Lp5/f;->c()Z

    new-instance p1, Landroid/content/Intent;

    const-class v0, Lcom/miui/packageInstaller/ui/SecureModeAdvantageActivity;

    invoke-direct {p1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, p1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private static final l1(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;Landroid/view/View;)V
    .locals 2

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p1, 0x1

    invoke-static {p0, p1}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->setSafeModelEnabled(Landroid/content/Context;Z)V

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->v()Z

    move-result p1

    if-nez p1, :cond_0

    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->M1()V

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->L1()V

    :goto_0
    new-instance p1, Lp5/b;

    const-string v0, "safe_mode_start_protect_open_btn"

    const-string v1, "button"

    invoke-direct {p1, v0, v1, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->o1()Ljava/lang/String;

    move-result-object p0

    const-string v0, "safe_mode_homepage_status"

    invoke-virtual {p1, v0, p0}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p0

    invoke-virtual {p0}, Lp5/f;->c()Z

    return-void
.end method

.method private final o1()Ljava/lang/String;
    .locals 4

    iget-boolean v0, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->O:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->Y:Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;

    const-string v1, "started"

    if-eqz v0, :cond_2

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;->getAuthorized()Z

    move-result v0

    if-ne v0, v2, :cond_0

    goto :goto_0

    :cond_0
    move v2, v3

    :goto_0
    if-eqz v2, :cond_1

    goto :goto_1

    :cond_1
    const-string v1, "authorize"

    :cond_2
    :goto_1
    return-object v1

    :cond_3
    const-string v0, "start_protect"

    return-object v0
.end method

.method private final p1(Landroid/content/Intent;)V
    .locals 3

    const-string v0, "auto_open"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object p1

    new-instance v0, Ld6/b0;

    invoke-direct {v0, p0}, Ld6/b0;-><init>(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;)V

    const-wide/16 v1, 0x12c

    invoke-virtual {p1, v0, v1, v2}, Lf6/z;->d(Ljava/lang/Runnable;J)V

    :cond_0
    return-void
.end method

.method private static final q1(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/app/Activity;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->setSafeModelEnabled(Landroid/content/Context;Z)V

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->v()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->M1()V

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->L1()V

    :goto_0
    return-void
.end method

.method private final s1()V
    .locals 4

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->C:Landroidx/appcompat/widget/LinearLayoutCompat;

    if-eqz v0, :cond_7

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    iget-boolean v0, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->X:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f070146

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0700d0

    goto :goto_0

    :cond_1
    iget-boolean v0, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->X:Z

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0700e4

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f070118

    :goto_0
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    if-ne v2, v1, :cond_4

    iget-boolean v1, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->X:Z

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07013a

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0700cb

    goto :goto_1

    :cond_4
    iget-boolean v1, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->X:Z

    if-eqz v1, :cond_5

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0700de

    goto :goto_1

    :cond_5
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070116

    :goto_1
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iget-object v2, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->C:Landroidx/appcompat/widget/LinearLayoutCompat;

    if-nez v2, :cond_6

    const-string v2, "llAnimGroupRootLayout"

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v2, 0x0

    :cond_6
    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3, v1, v3}, Landroid/view/ViewGroup;->setPadding(IIII)V

    :cond_7
    return-void
.end method

.method private final t1()V
    .locals 7

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result v0

    if-eqz v0, :cond_e

    iget-boolean v0, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->X:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->v:Landroid/widget/TextView;

    if-nez v0, :cond_0

    const-string v0, "tvSecurityModeTitle"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070546

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextSize(F)V

    :cond_1
    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->z:Landroid/view/ViewGroup;

    if-nez v0, :cond_2

    const-string v0, "llStatusLayout"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_2
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    const-string v3, "null cannot be cast to non-null type android.widget.FrameLayout.LayoutParams"

    invoke-static {v2, v3}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget v4, v4, Landroid/content/res/Configuration;->orientation:I

    const/4 v5, 0x1

    if-ne v4, v5, :cond_4

    iget-boolean v4, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->X:Z

    if-eqz v4, :cond_3

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f07012c

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f07014a

    goto :goto_0

    :cond_4
    iget-boolean v4, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->X:Z

    if-eqz v4, :cond_5

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f0700b9

    goto :goto_0

    :cond_5
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f070100

    :goto_0
    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    invoke-virtual {v2, v4}, Landroid/widget/FrameLayout$LayoutParams;->setMarginStart(I)V

    invoke-virtual {v2, v4}, Landroid/widget/FrameLayout$LayoutParams;->setMarginEnd(I)V

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->u:Landroid/widget/TextView;

    if-nez v0, :cond_6

    const-string v0, "tvSecurityModeStatus"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_1

    :cond_6
    move-object v1, v0

    :goto_1
    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    const-string v2, "null cannot be cast to non-null type androidx.appcompat.widget.LinearLayoutCompat.LayoutParams"

    invoke-static {v0, v2}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroidx/appcompat/widget/LinearLayoutCompat$a;

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    if-ne v2, v5, :cond_8

    iget-boolean v2, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->X:Z

    if-eqz v2, :cond_7

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0700d7

    goto :goto_2

    :cond_7
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f070107

    goto :goto_2

    :cond_8
    iget-boolean v2, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->X:Z

    if-eqz v2, :cond_9

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f070148

    goto :goto_2

    :cond_9
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0700f7

    :goto_2
    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->x:Landroid/widget/ImageView;

    if-eqz v0, :cond_d

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-static {v1, v3}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    const v3, 0x7f0700c9

    const v4, 0x7f0700e2

    if-ne v2, v5, :cond_b

    iget-boolean v2, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->X:Z

    if-eqz v2, :cond_a

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v5, 0x7f0700cb

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout$LayoutParams;->setMarginStart(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v5, 0x7f0700d0

    goto :goto_3

    :cond_a
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0700de

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout$LayoutParams;->setMarginStart(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f07013e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070128

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->width:I

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070116

    goto :goto_4

    :cond_b
    iget-boolean v2, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->X:Z

    if-eqz v2, :cond_c

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v5, 0x7f070118

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout$LayoutParams;->setMarginStart(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v5, 0x7f07013b

    :goto_3
    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->width:I

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    goto :goto_4

    :cond_c
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070132

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout$LayoutParams;->setMarginStart(I)V

    const/4 v2, 0x0

    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f07011b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->width:I

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070104

    :goto_4
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->height:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_d
    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->C1()V

    :cond_e
    return-void
.end method

.method private final u1()V
    .locals 9

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->w:Landroid/widget/TextView;

    const/4 v1, 0x0

    const-string v2, "tvSecurityModeServiceListTitle"

    if-nez v0, :cond_0

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v3, v1

    goto :goto_0

    :cond_0
    move-object v3, v0

    :goto_0
    const v0, 0x7f110351

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v0, "getString(R.string.setti\u2026ervice_list_title_on_msg)"

    invoke-static {v4, v0}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const v0, 0x7f1102b0

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v0, "getString(R.string.pure_guarding)"

    invoke-static {v5, v0}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const v0, 0x7f06004a

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getColor(I)I

    move-result v6

    const/high16 v7, 0x41200000    # 10.0f

    const v8, 0x7f08058f

    invoke-static/range {v3 .. v8}, Lcom/android/packageinstaller/utils/v;->a(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;IFI)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->w:Landroid/widget/TextView;

    if-nez v3, :cond_1

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    move-object v1, v3

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private static final w1(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;Landroid/view/View;)V
    .locals 2

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Lp5/b;

    const-string v0, "page_back_btn"

    const-string v1, "button"

    invoke-direct {p1, v0, v1, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    const-string v0, "back_type"

    const-string v1, "click_icon"

    invoke-virtual {p1, v0, v1}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p1

    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->o1()Ljava/lang/String;

    move-result-object v0

    const-string v1, "safe_mode_homepage_status"

    invoke-virtual {p1, v1, v0}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p1

    invoke-virtual {p1}, Lp5/f;->c()Z

    invoke-virtual {p0}, Lmiuix/appcompat/app/j;->finish()V

    return-void
.end method

.method private static final x1(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;Landroid/view/View;)V
    .locals 2

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Lp5/b;

    const-string v0, "safe_mode_start_protect_others_btn"

    const-string v1, "button"

    invoke-direct {p1, v0, v1, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->o1()Ljava/lang/String;

    move-result-object v0

    const-string v1, "safe_mode_homepage_status"

    invoke-virtual {p1, v1, v0}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p1

    invoke-virtual {p1}, Lp5/f;->c()Z

    new-instance p1, Landroid/content/Intent;

    invoke-direct {p1}, Landroid/content/Intent;-><init>()V

    const-class v0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;

    invoke-virtual {p1, p0, v0}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->R:Lm5/e;

    const-string v1, "caller"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->S:Lcom/miui/packageInstaller/model/ApkInfo;

    const-string v1, "apk_info"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    iget-object v0, p0, Lq2/b;->q:Lo5/b;

    const-string v1, "fromPage"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    invoke-virtual {p0, p1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private static final y1(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;Landroid/view/View;)V
    .locals 1

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Landroid/content/Intent;

    const-class v0, Lcom/miui/packageInstaller/ui/SecureModeAdvantageActivity;

    invoke-direct {p1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, p1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private final z1()V
    .locals 6

    invoke-static {p0}, Landroidx/lifecycle/k;->a(Landroidx/lifecycle/j;)Landroidx/lifecycle/e;

    move-result-object v0

    invoke-static {}, Lv8/t0;->c()Lv8/v1;

    move-result-object v1

    new-instance v3, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity$c;

    const/4 v2, 0x0

    invoke-direct {v3, p0, v2}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity$c;-><init>(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;Ld8/d;)V

    const/4 v4, 0x2

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lv8/f;->d(Lv8/e0;Ld8/g;Lv8/g0;Ll8/p;ILjava/lang/Object;)Lv8/k1;

    return-void
.end method


# virtual methods
.method public final B1()V
    .locals 5

    invoke-static {p0}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->isSafeModelEnable(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->O:Z

    invoke-static {}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->calculateSecurityModeProtectDays()I

    move-result v0

    iput v0, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->Q:I

    iget-boolean v0, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->O:Z

    if-eqz v0, :cond_5

    sget-object v0, Ld6/l0;->c:Ld6/l0$a;

    invoke-virtual {v0}, Ld6/l0$a;->c()Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;

    move-result-object v1

    iput-object v1, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->Y:Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;->getPackageMd5()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->S:Lcom/miui/packageInstaller/model/ApkInfo;

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Lcom/miui/packageInstaller/model/ApkInfo;->getApkMd5()Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    :cond_0
    move-object v4, v2

    :goto_0
    invoke-static {v3, v4}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;->isValid()Z

    move-result v1

    if-nez v1, :cond_1

    iput-object v2, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->Y:Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;

    invoke-virtual {v0}, Ld6/l0$a;->a()V

    :cond_1
    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->R:Lm5/e;

    if-nez v0, :cond_3

    new-instance v0, Lm5/e;

    invoke-direct {v0}, Lm5/e;-><init>()V

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->R:Lm5/e;

    iget-object v1, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->Y:Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;->getCallerPackageName()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_2
    move-object v1, v2

    :goto_1
    iput-object v1, v0, Lm5/e;->e:Ljava/lang/String;

    :cond_3
    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->S:Lcom/miui/packageInstaller/model/ApkInfo;

    if-nez v0, :cond_5

    new-instance v0, Lcom/miui/packageInstaller/model/ApkInfo;

    invoke-direct {v0}, Lcom/miui/packageInstaller/model/ApkInfo;-><init>()V

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->S:Lcom/miui/packageInstaller/model/ApkInfo;

    iget-object v1, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->Y:Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;->getAppName()Ljava/lang/String;

    move-result-object v2

    :cond_4
    invoke-virtual {v0, v2}, Lcom/miui/packageInstaller/model/ApkInfo;->setLabel(Ljava/lang/String;)V

    :cond_5
    return-void
.end method

.method public final D1()V
    .locals 6

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->Y:Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;

    if-nez v0, :cond_0

    return-void

    :cond_0
    new-instance v1, Lmiuix/appcompat/app/i$b;

    invoke-direct {v1, p0}, Lmiuix/appcompat/app/i$b;-><init>(Landroid/content/Context;)V

    const v2, 0x7f11025b

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;->getAppName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {p0, v2, v3}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmiuix/appcompat/app/i$b;->t(Ljava/lang/CharSequence;)Lmiuix/appcompat/app/i$b;

    move-result-object v1

    const v2, 0x7f11025a

    invoke-virtual {v1, v2}, Lmiuix/appcompat/app/i$b;->e(I)Lmiuix/appcompat/app/i$b;

    move-result-object v1

    const v2, 0x7f1100b5

    invoke-virtual {p0, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ld6/y;

    invoke-direct {v3, p0}, Ld6/y;-><init>(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;)V

    invoke-virtual {v1, v2, v3}, Lmiuix/appcompat/app/i$b;->i(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/i$b;

    move-result-object v1

    const v2, 0x7f1103bd

    invoke-virtual {p0, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ld6/c0;

    invoke-direct {v3, p0, v0}, Ld6/c0;-><init>(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;)V

    invoke-virtual {v1, v2, v3}, Lmiuix/appcompat/app/i$b;->p(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/i$b;

    move-result-object v0

    new-instance v1, Ld6/d0;

    invoke-direct {v1, p0}, Ld6/d0;-><init>(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;)V

    invoke-virtual {v0, v1}, Lmiuix/appcompat/app/i$b;->m(Landroid/content/DialogInterface$OnDismissListener;)Lmiuix/appcompat/app/i$b;

    move-result-object v0

    invoke-virtual {v0}, Lmiuix/appcompat/app/i$b;->w()Lmiuix/appcompat/app/i;

    new-instance v0, Lp5/g;

    const-string v1, "single_authorize_popup"

    const-string v2, "popup"

    invoke-direct {v0, v1, v2, p0}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    new-instance v0, Lp5/g;

    const-string v1, "single_authorize_popup_cancel_btn"

    const-string v2, "button"

    invoke-direct {v0, v1, v2, p0}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    new-instance v0, Lp5/g;

    const-string v1, "single_authorize_popup_authorize_btn"

    invoke-direct {v0, v1, v2, p0}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    return-void
.end method

.method public E0()Ljava/lang/String;
    .locals 1

    const-string v0, "safe_mode_homepage"

    return-object v0
.end method

.method public G()Lcom/miui/packageInstaller/model/ApkInfo;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->S:Lcom/miui/packageInstaller/model/ApkInfo;

    return-object v0
.end method

.method public G0()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->o1()Ljava/lang/String;

    move-result-object v1

    const-string v2, "safe_mode_homepage_status"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0
.end method

.method public final i1()V
    .locals 17

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->O:Z

    const-string v2, "securityModeMoreInfoViewGroup"

    const-string v3, "securityModeMoreInfoIndicator"

    const-string v4, "tvSecurityModeTitle"

    const-string v5, "tvSecurityModeStatus"

    const-string v6, "mLearnMore"

    const-string v7, "bottomLayout"

    const/4 v8, 0x1

    const/16 v9, 0x8

    const-string v10, "tvActionButtonAboveMsg"

    const-string v11, "btnAction"

    const/4 v12, 0x0

    if-eqz v1, :cond_16

    iget-object v1, v0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->u:Landroid/widget/TextView;

    if-nez v1, :cond_0

    invoke-static {v5}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v1, 0x0

    :cond_0
    const v5, 0x7f110360

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(I)V

    invoke-direct/range {p0 .. p0}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->u1()V

    iget-object v1, v0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->v:Landroid/widget/TextView;

    if-nez v1, :cond_1

    invoke-static {v4}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v1, 0x0

    :cond_1
    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0f001b

    iget v14, v0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->Q:I

    new-array v15, v8, [Ljava/lang/Object;

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v15, v12

    invoke-virtual {v4, v5, v14, v15}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, v0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->Y:Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;

    if-eqz v1, :cond_e

    iget-object v4, v0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->A:Landroid/widget/TextView;

    if-nez v4, :cond_2

    invoke-static {v10}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v4, 0x0

    :cond_2
    invoke-virtual {v4, v12}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v4, v0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->D:Landroid/widget/TextView;

    if-nez v4, :cond_3

    invoke-static {v11}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v4, 0x0

    :cond_3
    invoke-virtual {v4, v12}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v4, v0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->B:Landroidx/appcompat/widget/LinearLayoutCompat;

    if-nez v4, :cond_4

    invoke-static {v7}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v4, 0x0

    :cond_4
    invoke-virtual {v4, v12}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v4, v0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->A:Landroid/widget/TextView;

    if-nez v4, :cond_5

    invoke-static {v10}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v4, 0x0

    :cond_5
    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v14, 0x7f11034f

    new-array v8, v8, [Ljava/lang/Object;

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;->getAppName()Ljava/lang/String;

    move-result-object v15

    aput-object v15, v8, v12

    invoke-virtual {v5, v14, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;->getAuthorized()Z

    move-result v1

    if-eqz v1, :cond_8

    iget-object v1, v0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->D:Landroid/widget/TextView;

    if-nez v1, :cond_6

    invoke-static {v11}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v1, 0x0

    :cond_6
    const v4, 0x7f1102f6

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(I)V

    iget-object v1, v0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->D:Landroid/widget/TextView;

    if-nez v1, :cond_7

    invoke-static {v11}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v1, 0x0

    :cond_7
    invoke-virtual {v1, v12}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_0

    :cond_8
    iget-object v1, v0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->D:Landroid/widget/TextView;

    if-nez v1, :cond_9

    invoke-static {v11}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v1, 0x0

    :cond_9
    const v4, 0x7f1102f8

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(I)V

    :goto_0
    iget-object v1, v0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->D:Landroid/widget/TextView;

    if-nez v1, :cond_a

    invoke-static {v11}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v1, 0x0

    :cond_a
    new-instance v4, Ld6/i0;

    invoke-direct {v4, v0}, Ld6/i0;-><init>(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;)V

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, v0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->A:Landroid/widget/TextView;

    if-nez v1, :cond_b

    invoke-static {v10}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v1, 0x0

    :cond_b
    invoke-virtual {v1, v12}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, v0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->D:Landroid/widget/TextView;

    if-nez v1, :cond_c

    invoke-static {v11}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v1, 0x0

    :cond_c
    invoke-virtual {v1, v12}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, v0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->B:Landroidx/appcompat/widget/LinearLayoutCompat;

    if-nez v1, :cond_d

    invoke-static {v7}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v1, 0x0

    :cond_d
    invoke-virtual {v1, v12}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_1

    :cond_e
    iget-object v1, v0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->A:Landroid/widget/TextView;

    if-nez v1, :cond_f

    invoke-static {v10}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v1, 0x0

    :cond_f
    invoke-virtual {v1, v9}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, v0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->D:Landroid/widget/TextView;

    if-nez v1, :cond_10

    invoke-static {v11}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v1, 0x0

    :cond_10
    invoke-virtual {v1, v9}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, v0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->B:Landroidx/appcompat/widget/LinearLayoutCompat;

    if-nez v1, :cond_11

    invoke-static {v7}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v1, 0x0

    :cond_11
    invoke-virtual {v1, v9}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v1, v0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->N:Landroid/widget/TextView;

    if-nez v1, :cond_12

    invoke-static {v6}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v1, 0x0

    :cond_12
    invoke-virtual {v1, v9}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_1
    const-string v1, "safe_mode_openstate"

    iput-object v1, v0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->W:Ljava/lang/String;

    iget-object v1, v0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->L:Landroid/view/View;

    if-nez v1, :cond_13

    invoke-static {v3}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v1, 0x0

    :cond_13
    invoke-virtual {v1, v12}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, v0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->M:Landroid/view/View;

    if-nez v1, :cond_14

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v1, 0x0

    :cond_14
    new-instance v2, Ld6/f0;

    invoke-direct {v2, v0}, Ld6/f0;-><init>(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, v0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->N:Landroid/widget/TextView;

    if-nez v1, :cond_15

    invoke-static {v6}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v1, 0x0

    :cond_15
    invoke-virtual {v1, v9}, Landroid/widget/TextView;->setVisibility(I)V

    const/4 v2, 0x0

    goto/16 :goto_2

    :cond_16
    iget-object v1, v0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->w:Landroid/widget/TextView;

    if-nez v1, :cond_17

    const-string v1, "tvSecurityModeServiceListTitle"

    invoke-static {v1}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v1, 0x0

    :cond_17
    const v14, 0x7f110350

    invoke-virtual {v1, v14}, Landroid/widget/TextView;->setText(I)V

    iget-object v1, v0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->u:Landroid/widget/TextView;

    if-nez v1, :cond_18

    invoke-static {v5}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v1, 0x0

    :cond_18
    const v5, 0x7f11035f

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(I)V

    iget-object v1, v0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->v:Landroid/widget/TextView;

    if-nez v1, :cond_19

    invoke-static {v4}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v1, 0x0

    :cond_19
    const v4, 0x7f110361

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(I)V

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f0f0018

    iget-wide v14, v0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->P:J

    long-to-int v5, v14

    new-array v13, v8, [Ljava/lang/Object;

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    aput-object v14, v13, v12

    invoke-virtual {v1, v4, v5, v13}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v12}, Landroid/text/Html;->fromHtml(Ljava/lang/String;I)Landroid/text/Spanned;

    move-result-object v1

    iget-object v4, v0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->A:Landroid/widget/TextView;

    if-nez v4, :cond_1a

    invoke-static {v10}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v4, 0x0

    :cond_1a
    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, v0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->D:Landroid/widget/TextView;

    if-nez v1, :cond_1b

    invoke-static {v11}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v1, 0x0

    :cond_1b
    const v4, 0x7f110100

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(I)V

    iget-object v1, v0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->D:Landroid/widget/TextView;

    if-nez v1, :cond_1c

    invoke-static {v11}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v1, 0x0

    :cond_1c
    new-instance v4, Ld6/g0;

    invoke-direct {v4, v0}, Ld6/g0;-><init>(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;)V

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, v0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->D:Landroid/widget/TextView;

    if-nez v1, :cond_1d

    invoke-static {v11}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v1, 0x0

    :cond_1d
    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v1, v0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->A:Landroid/widget/TextView;

    if-nez v1, :cond_1e

    invoke-static {v10}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v1, 0x0

    :cond_1e
    invoke-virtual {v1, v12}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, v0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->D:Landroid/widget/TextView;

    if-nez v1, :cond_1f

    invoke-static {v11}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v1, 0x0

    :cond_1f
    invoke-virtual {v1, v12}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, v0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->B:Landroidx/appcompat/widget/LinearLayoutCompat;

    if-nez v1, :cond_20

    invoke-static {v7}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v1, 0x0

    :cond_20
    invoke-virtual {v1, v12}, Landroid/view/ViewGroup;->setVisibility(I)V

    const-string v1, "safe_mode_initial"

    iput-object v1, v0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->W:Ljava/lang/String;

    iget-object v1, v0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->L:Landroid/view/View;

    if-nez v1, :cond_21

    invoke-static {v3}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v1, 0x0

    :cond_21
    invoke-virtual {v1, v9}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, v0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->M:Landroid/view/View;

    if-nez v1, :cond_22

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v1, 0x0

    :cond_22
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, v0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->N:Landroid/widget/TextView;

    if-nez v1, :cond_23

    invoke-static {v6}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v1, v2

    :cond_23
    invoke-virtual {v1, v12}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_2
    invoke-direct/range {p0 .. p0}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->C1()V

    iget-object v1, v0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->G:Lj6/b;

    if-nez v1, :cond_24

    const-string v1, "mServiceListAdapter"

    invoke-static {v1}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v13, v2

    goto :goto_3

    :cond_24
    move-object v13, v1

    :goto_3
    invoke-virtual {v13}, Landroidx/recyclerview/widget/RecyclerView$g;->k()V

    return-void
.end method

.method public final m1()V
    .locals 1

    const-string v0, "safe_mode_openAnimation"

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->W:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->B1()V

    invoke-virtual {p0}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->i1()V

    return-void
.end method

.method public final n1()V
    .locals 2

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->X:Z

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->F:Landroid/view/ViewGroup;

    if-nez v0, :cond_0

    const-string v0, "contentLayout"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v0, 0x0

    :cond_0
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity$b;

    invoke-direct {v1, p0}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity$b;-><init>(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    :cond_1
    return-void
.end method

.method public onBackPressed()V
    .locals 3

    invoke-super {p0}, Lmiuix/appcompat/app/j;->onBackPressed()V

    new-instance v0, Lp5/b;

    const-string v1, "page_back_btn"

    const-string v2, "button"

    invoke-direct {v0, v1, v2, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    const-string v1, "back_type"

    const-string v2, "system"

    invoke-virtual {v0, v1, v2}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object v0

    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->o1()Ljava/lang/String;

    move-result-object v1

    const-string v2, "safe_mode_homepage_status"

    invoke-virtual {v0, v2, v1}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object v0

    invoke-virtual {v0}, Lp5/f;->c()Z

    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    const-string v0, "newConfig"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lq2/b;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->t1()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lq2/b;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->r1()V

    invoke-virtual {p0}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->B1()V

    invoke-virtual {p0}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->v1()V

    invoke-virtual {p0}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->n1()V

    invoke-virtual {p0}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->i1()V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "intent"

    invoke-static {p1, v0}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->p1(Landroid/content/Intent;)V

    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0

    invoke-super {p0, p1}, Landroidx/fragment/app/e;->onNewIntent(Landroid/content/Intent;)V

    if-eqz p1, :cond_0

    invoke-direct {p0, p1}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->p1(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 6

    invoke-super {p0}, Landroidx/fragment/app/e;->onResume()V

    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->z1()V

    invoke-static {p0}, Landroidx/lifecycle/k;->a(Landroidx/lifecycle/j;)Landroidx/lifecycle/e;

    move-result-object v0

    invoke-static {}, Lv8/t0;->c()Lv8/v1;

    move-result-object v1

    new-instance v3, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity$d;

    const/4 v2, 0x0

    invoke-direct {v3, p0, v2}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity$d;-><init>(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;Ld8/d;)V

    const/4 v4, 0x2

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lv8/f;->d(Lv8/e0;Ld8/g;Lv8/g0;Ll8/p;ILjava/lang/Object;)Lv8/k1;

    return-void
.end method

.method public p()Lm5/e;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->R:Lm5/e;

    return-object v0
.end method

.method public final r1()V
    .locals 7

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "apk_info"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/miui/packageInstaller/model/ApkInfo;

    iput-object v1, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->S:Lcom/miui/packageInstaller/model/ApkInfo;

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "caller"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lm5/e;

    iput-object v1, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->R:Lm5/e;

    const-string v1, "safe_mode_type"

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :cond_0
    move-object v3, v2

    :goto_0
    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->T:Ljava/lang/String;

    const-string v3, "safe_mode_ref"

    if-eqz v0, :cond_1

    invoke-virtual {v0, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    :cond_1
    move-object v4, v2

    :goto_1
    iput-object v4, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->U:Ljava/lang/String;

    iget-object v4, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->S:Lcom/miui/packageInstaller/model/ApkInfo;

    if-eqz v4, :cond_3

    invoke-virtual {v4}, Lcom/miui/packageInstaller/model/ApkInfo;->getPackageInfo()Landroid/content/pm/PackageInfo;

    move-result-object v4

    if-eqz v4, :cond_3

    iget-object v4, v4, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    if-eqz v4, :cond_3

    iget-object v5, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->S:Lcom/miui/packageInstaller/model/ApkInfo;

    if-nez v5, :cond_2

    goto :goto_2

    :cond_2
    invoke-virtual {p0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    invoke-virtual {v6, v4}, Landroid/content/pm/PackageManager;->getApplicationIcon(Landroid/content/pm/ApplicationInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v5, v4}, Lcom/miui/packageInstaller/model/ApkInfo;->setIcon(Landroid/graphics/drawable/Drawable;)V

    :cond_3
    :goto_2
    if-eqz v0, :cond_4

    iget-object v4, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->T:Ljava/lang/String;

    const-string v5, "null"

    invoke-static {v4, v5}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    :cond_4
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->T:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->U:Ljava/lang/String;

    :cond_5
    iget-object v1, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->U:Ljava/lang/String;

    const-string v3, "notification_from"

    invoke-static {v1, v3}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    if-eqz v0, :cond_6

    const-string v1, "style"

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :cond_6
    invoke-direct {p0, v2}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->A1(Ljava/lang/String;)V

    :cond_7
    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->U:Ljava/lang/String;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_9

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_8

    move v0, v1

    goto :goto_3

    :cond_8
    move v0, v2

    :goto_3
    if-ne v0, v1, :cond_9

    goto :goto_4

    :cond_9
    move v1, v2

    :goto_4
    if-eqz v1, :cond_b

    iget-object v0, p0, Lq2/b;->q:Lo5/b;

    iget-object v1, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->U:Ljava/lang/String;

    if-nez v1, :cond_a

    const-string v1, ""

    :cond_a
    invoke-virtual {v0, v1}, Lo5/b;->s(Ljava/lang/String;)V

    :cond_b
    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->T:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_c

    const-string v0, "packageinstaller"

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->T:Ljava/lang/String;

    :cond_c
    return-void
.end method

.method public final v1()V
    .locals 13

    const-string v0, "security_mode_config"

    const/4 v9, 0x0

    invoke-virtual {p0, v0, v9}, Landroid/app/Activity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "saved_security_mode_enable_count"

    const-wide/32 v2, 0x737a80

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->P:J

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0d01a4

    goto :goto_0

    :cond_0
    const v0, 0x7f0d01a2

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/android/packageinstaller/utils/g;->v()Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f0d01a3

    goto :goto_0

    :cond_2
    const v0, 0x7f0d01a1

    :goto_0
    invoke-virtual {p0, v0}, Lmiuix/appcompat/app/j;->setContentView(I)V

    const v0, 0x7f0a02d2

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const-string v2, "findViewById<FrameLayout>(R.id.root_layout)"

    invoke-static {v1, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->E:Landroid/view/ViewGroup;

    const-string v2, "rootLayout"

    const/4 v10, 0x0

    if-nez v1, :cond_3

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v1, v10

    :cond_3
    invoke-static {p0}, Lcom/android/packageinstaller/utils/u;->b(Landroid/app/Activity;)I

    move-result v3

    invoke-virtual {v1, v9, v3, v9, v9}, Landroid/view/ViewGroup;->setPadding(IIII)V

    const v1, 0x7f0a00ea

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const-string v3, "findViewById(R.id.content_layout)"

    invoke-static {v1, v3}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->F:Landroid/view/ViewGroup;

    const v1, 0x7f0a02fc

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const-string v3, "findViewById(R.id.security_mode_status)"

    invoke-static {v1, v3}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->u:Landroid/widget/TextView;

    if-nez v1, :cond_4

    const-string v1, "tvSecurityModeStatus"

    invoke-static {v1}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v1, v10

    :cond_4
    invoke-static {}, Lcom/android/packageinstaller/utils/g;->B()Z

    move-result v3

    if-eqz v3, :cond_5

    const/high16 v3, 0x3e800000    # 0.25f

    goto :goto_1

    :cond_5
    const/4 v3, 0x0

    :goto_1
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setLetterSpacing(F)V

    const v1, 0x7f0a02fd

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const-string v3, "findViewById(R.id.security_mode_title)"

    invoke-static {v1, v3}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->v:Landroid/widget/TextView;

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->v()Z

    move-result v1

    if-nez v1, :cond_7

    const v1, 0x7f0a020b

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->x:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->E:Landroid/view/ViewGroup;

    if-nez v1, :cond_6

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v1, v10

    :cond_6
    new-instance v2, Lcom/miui/packageInstaller/view/i;

    const v3, 0x7f080182

    invoke-direct {v2, v3}, Lcom/miui/packageInstaller/view/i;-><init>(I)V

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setBackground(Landroid/graphics/drawable/Drawable;)V

    :cond_7
    const v1, 0x7f0a02f8

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const-string v2, "findViewById(R.id.security_mode_service_list)"

    invoke-static {v1, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Landroidx/recyclerview/widget/RecyclerView;

    iput-object v1, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->y:Landroidx/recyclerview/widget/RecyclerView;

    const v1, 0x7f0a01ff

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const-string v2, "findViewById(R.id.ll_status_layout)"

    invoke-static {v1, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->z:Landroid/view/ViewGroup;

    const v1, 0x7f0a0253

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const-string v3, "findViewById(R.id.more_indicator)"

    invoke-static {v2, v3}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->L:Landroid/view/View;

    const v2, 0x7f0a02fa

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const-string v3, "findViewById(R.id.securi\u2026service_list_title_group)"

    invoke-static {v2, v3}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->M:Landroid/view/View;

    const/4 v3, 0x1

    new-array v4, v3, [Landroid/view/View;

    const-string v5, "securityModeMoreInfoViewGroup"

    if-nez v2, :cond_8

    invoke-static {v5}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v2, v10

    :cond_8
    aput-object v2, v4, v9

    invoke-static {v4}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object v2

    invoke-interface {v2}, Lmiuix/animation/e;->c()Lmiuix/animation/j;

    move-result-object v2

    new-array v4, v9, [Lmiuix/animation/j$b;

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-interface {v2, v6, v4}, Lmiuix/animation/j;->G(F[Lmiuix/animation/j$b;)Lmiuix/animation/j;

    move-result-object v2

    const v4, 0x7f060025

    invoke-virtual {p0, v4}, Landroid/app/Activity;->getColor(I)I

    move-result v4

    invoke-interface {v2, v4}, Lmiuix/animation/j;->i(I)Lmiuix/animation/j;

    move-result-object v2

    iget-object v4, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->M:Landroid/view/View;

    if-nez v4, :cond_9

    invoke-static {v5}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v4, v10

    :cond_9
    new-array v5, v9, [Lc9/a;

    invoke-interface {v2, v4, v5}, Lmiuix/animation/j;->z(Landroid/view/View;[Lc9/a;)V

    const v2, 0x7f0a036f

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const-string v4, "findViewById(R.id.text_above_button)"

    invoke-static {v2, v4}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->A:Landroid/widget/TextView;

    const v2, 0x7f0a0046

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const-string v4, "findViewById(R.id.action_button)"

    invoke-static {v2, v4}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->D:Landroid/widget/TextView;

    const v2, 0x7f0a0095

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const-string v4, "findViewById(R.id.bottom_layout)"

    invoke-static {v2, v4}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Landroidx/appcompat/widget/LinearLayoutCompat;

    iput-object v2, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->B:Landroidx/appcompat/widget/LinearLayoutCompat;

    const v2, 0x7f0a0089

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const-string v4, "findViewById(R.id.back_icon)"

    invoke-static {v2, v4}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Landroid/widget/FrameLayout;

    iput-object v2, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->H:Landroid/widget/FrameLayout;

    if-nez v2, :cond_a

    const-string v2, "flBackIcon"

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v2, v10

    :cond_a
    new-instance v4, Ld6/j0;

    invoke-direct {v4, p0}, Ld6/j0;-><init>(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;)V

    invoke-virtual {v2, v4}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v2, 0x7f0a008a

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->I:Landroid/view/View;

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->J:Landroid/view/View;

    iget-object v1, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->I:Landroid/view/View;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    goto :goto_2

    :cond_b
    move-object v1, v10

    :goto_2
    if-nez v1, :cond_c

    goto :goto_3

    :cond_c
    invoke-virtual {v1, v3}, Landroid/graphics/drawable/Drawable;->setAutoMirrored(Z)V

    :goto_3
    iget-object v1, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->J:Landroid/view/View;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    goto :goto_4

    :cond_d
    move-object v1, v10

    :goto_4
    if-nez v1, :cond_e

    goto :goto_5

    :cond_e
    invoke-virtual {v1, v3}, Landroid/graphics/drawable/Drawable;->setAutoMirrored(Z)V

    :goto_5
    const v1, 0x7f0a02fb

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const-string v2, "findViewById(R.id.security_mode_setting_icon)"

    invoke-static {v1, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Landroid/widget/FrameLayout;

    iput-object v1, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->K:Landroid/widget/FrameLayout;

    if-nez v1, :cond_f

    const-string v1, "flSecurityModeSetting"

    invoke-static {v1}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v1, v10

    :cond_f
    new-instance v2, Ld6/e0;

    invoke-direct {v2, p0}, Ld6/e0;-><init>(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-array v1, v3, [Landroid/view/View;

    iget-object v2, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->D:Landroid/widget/TextView;

    const-string v3, "btnAction"

    if-nez v2, :cond_10

    invoke-static {v3}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v2, v10

    :cond_10
    aput-object v2, v1, v9

    invoke-static {v1}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object v1

    invoke-interface {v1}, Lmiuix/animation/e;->c()Lmiuix/animation/j;

    move-result-object v1

    new-array v2, v9, [Lmiuix/animation/j$b;

    invoke-interface {v1, v6, v2}, Lmiuix/animation/j;->G(F[Lmiuix/animation/j$b;)Lmiuix/animation/j;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->D:Landroid/widget/TextView;

    if-nez v2, :cond_11

    invoke-static {v3}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v2, v10

    :cond_11
    new-array v3, v9, [Lc9/a;

    invoke-interface {v1, v2, v3}, Lmiuix/animation/j;->z(Landroid/view/View;[Lc9/a;)V

    const v1, 0x7f0a02f9

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const-string v2, "findViewById(R.id.securi\u2026_mode_service_list_title)"

    invoke-static {v1, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->w:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.root_layout)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->E:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->y:Landroidx/recyclerview/widget/RecyclerView;

    const-string v1, "rvSecurityModeList"

    if-nez v0, :cond_12

    invoke-static {v1}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v10

    :cond_12
    new-instance v2, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-direct {v2, p0}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v2}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$o;)V

    new-instance v0, Lj6/b;

    iget-object v2, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->y:Landroidx/recyclerview/widget/RecyclerView;

    if-nez v2, :cond_13

    invoke-static {v1}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v2, v10

    :cond_13
    invoke-direct {v0, v2}, Lj6/b;-><init>(Landroidx/recyclerview/widget/RecyclerView;)V

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->G:Lj6/b;

    new-instance v11, Ljava/util/ArrayList;

    const/4 v0, 0x4

    invoke-direct {v11, v0}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v12, Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceViewObject;

    const v2, 0x7f0801ab

    const v3, 0x7f11035b

    const v4, 0x7f110352

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x30

    const/4 v8, 0x0

    move-object v0, v12

    move-object v1, p0

    invoke-direct/range {v0 .. v8}, Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceViewObject;-><init>(Landroid/content/Context;IIILl6/c;Lm6/b;ILm8/g;)V

    invoke-virtual {v11, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v12, Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceViewObject;

    const v2, 0x7f0801ac

    const v3, 0x7f11035c

    const v4, 0x7f110353

    move-object v0, v12

    invoke-direct/range {v0 .. v8}, Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceViewObject;-><init>(Landroid/content/Context;IIILl6/c;Lm6/b;ILm8/g;)V

    invoke-virtual {v11, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v12, Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceViewObject;

    const v2, 0x7f0801ad

    const v3, 0x7f11035d

    const v4, 0x7f110354

    move-object v0, v12

    invoke-direct/range {v0 .. v8}, Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceViewObject;-><init>(Landroid/content/Context;IIILl6/c;Lm6/b;ILm8/g;)V

    invoke-virtual {v11, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v12, Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceViewObject;

    const v2, 0x7f0801ae

    const v3, 0x7f11035e

    const v4, 0x7f110355

    move-object v0, v12

    invoke-direct/range {v0 .. v8}, Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceViewObject;-><init>(Landroid/content/Context;IIILl6/c;Lm6/b;ILm8/g;)V

    invoke-virtual {v11, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->G:Lj6/b;

    if-nez v0, :cond_14

    const-string v0, "mServiceListAdapter"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v10

    :cond_14
    invoke-virtual {v0, v11}, Lj6/b;->j0(Ljava/util/List;)V

    const v0, 0x7f0a02f6

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.security_learn_more)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->N:Landroid/widget/TextView;

    iget-boolean v1, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->O:Z

    const-string v2, "mLearnMore"

    if-eqz v1, :cond_16

    if-nez v0, :cond_15

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v10

    :cond_15
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_6

    :cond_16
    if-nez v0, :cond_17

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v10

    :cond_17
    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_6
    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->N:Landroid/widget/TextView;

    if-nez v0, :cond_18

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_7

    :cond_18
    move-object v10, v0

    :goto_7
    new-instance v0, Ld6/h0;

    invoke-direct {v0, p0}, Ld6/h0;-><init>(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;)V

    invoke-virtual {v10, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->h1()V

    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->t1()V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->U:Ljava/lang/String;

    const-string v1, "setting_entry"

    invoke-static {v0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_19

    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->I1()V

    :cond_19
    return-void
.end method
