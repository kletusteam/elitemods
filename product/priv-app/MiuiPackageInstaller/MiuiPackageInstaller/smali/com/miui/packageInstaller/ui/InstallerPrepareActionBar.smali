.class public Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;
.super Landroid/widget/LinearLayout;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$a;,
        Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;
    }
.end annotation


# static fields
.field public static final h:Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$a;


# instance fields
.field private a:Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;

.field private b:Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;

.field private c:Lcom/miui/packageInstaller/view/InstallerActionButton;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$a;-><init>(Lm8/g;)V

    sput-object v0, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->h:Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$a;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p1, 0x1

    iput p1, p0, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->g:I

    return-void
.end method


# virtual methods
.method public a(I)I
    .locals 3

    const v0, 0x7f0d00a2

    const v1, 0x7f0d00a1

    if-eqz p1, :cond_3

    const/4 v2, 0x1

    if-eq p1, v2, :cond_3

    const/4 v2, 0x2

    if-eq p1, v2, :cond_1

    const/4 v2, 0x3

    if-eq p1, v2, :cond_1

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_0
    return v0

    :cond_1
    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result p1

    if-eqz p1, :cond_2

    const p1, 0x7f0d01ce

    goto :goto_1

    :cond_2
    const p1, 0x7f0d01cd

    :goto_1
    return p1

    :cond_3
    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result p1

    if-eqz p1, :cond_4

    goto :goto_2

    :cond_4
    move v0, v1

    :goto_2
    return v0
.end method

.method public final b(Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;)V
    .locals 2

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f060513

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-interface {p1, v0}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;->setButtonTextColor(I)V

    :cond_0
    if-eqz p1, :cond_1

    const v0, 0x7f08011b

    invoke-interface {p1, v0}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;->setButtonBackgroundResource(I)V

    :cond_1
    return-void
.end method

.method protected final c()V
    .locals 6

    const v0, 0x7f0a016c

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->a:Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;

    const v0, 0x7f0a02f4

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->b:Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;

    const v0, 0x7f0a037b

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/miui/packageInstaller/view/InstallerActionButton;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->c:Lcom/miui/packageInstaller/view/InstallerActionButton;

    const v0, 0x7f0a0289

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->d:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->a:Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v0, :cond_0

    new-array v4, v2, [Landroid/view/View;

    invoke-interface {v0}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;->a()Landroid/view/View;

    move-result-object v5

    aput-object v5, v4, v3

    invoke-static {v4}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object v4

    invoke-interface {v4}, Lmiuix/animation/e;->c()Lmiuix/animation/j;

    move-result-object v4

    new-array v5, v3, [Lmiuix/animation/j$b;

    invoke-interface {v4, v1, v5}, Lmiuix/animation/j;->G(F[Lmiuix/animation/j$b;)Lmiuix/animation/j;

    move-result-object v4

    invoke-interface {v0}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;->a()Landroid/view/View;

    move-result-object v0

    new-array v5, v3, [Lc9/a;

    invoke-interface {v4, v0, v5}, Lmiuix/animation/j;->z(Landroid/view/View;[Lc9/a;)V

    :cond_0
    iget-object v0, p0, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->b:Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;

    if-eqz v0, :cond_1

    new-array v4, v2, [Landroid/view/View;

    invoke-interface {v0}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;->a()Landroid/view/View;

    move-result-object v5

    aput-object v5, v4, v3

    invoke-static {v4}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object v4

    invoke-interface {v4}, Lmiuix/animation/e;->c()Lmiuix/animation/j;

    move-result-object v4

    new-array v5, v3, [Lmiuix/animation/j$b;

    invoke-interface {v4, v1, v5}, Lmiuix/animation/j;->G(F[Lmiuix/animation/j$b;)Lmiuix/animation/j;

    move-result-object v4

    invoke-interface {v0}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;->a()Landroid/view/View;

    move-result-object v0

    new-array v5, v3, [Lc9/a;

    invoke-interface {v4, v0, v5}, Lmiuix/animation/j;->z(Landroid/view/View;[Lc9/a;)V

    :cond_1
    iget-object v0, p0, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->c:Lcom/miui/packageInstaller/view/InstallerActionButton;

    if-eqz v0, :cond_2

    new-array v2, v2, [Landroid/view/View;

    aput-object v0, v2, v3

    invoke-static {v2}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object v2

    invoke-interface {v2}, Lmiuix/animation/e;->c()Lmiuix/animation/j;

    move-result-object v2

    new-array v4, v3, [Lmiuix/animation/j$b;

    invoke-interface {v2, v1, v4}, Lmiuix/animation/j;->G(F[Lmiuix/animation/j$b;)Lmiuix/animation/j;

    move-result-object v1

    invoke-virtual {v0}, Lcom/miui/packageInstaller/view/InstallerActionButton;->a()Landroid/view/View;

    move-result-object v0

    new-array v2, v3, [Lc9/a;

    invoke-interface {v1, v0, v2}, Lmiuix/animation/j;->z(Landroid/view/View;[Lc9/a;)V

    :cond_2
    const v0, 0x7f0a037f

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->e:Landroid/widget/TextView;

    const v0, 0x7f0a02f5

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->f:Landroid/widget/TextView;

    return-void
.end method

.method public d(I)V
    .locals 1

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->removeAllViews()V

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-virtual {p0, p1}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->a(I)I

    move-result p1

    invoke-virtual {v0, p1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    invoke-virtual {p0}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->c()V

    return-void
.end method

.method public e(Landroid/view/View;)V
    .locals 1

    const-string v0, "view"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->removeAllViews()V

    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->c()V

    return-void
.end method

.method public final getMFirstButton()Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->a:Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;

    return-object v0
.end method

.method public final getMOnceAuthorizeBtn()Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->d:Landroid/widget/TextView;

    return-object v0
.end method

.method public final getMSecondButton()Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->b:Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;

    return-object v0
.end method

.method public final getMThirdButton()Lcom/miui/packageInstaller/view/InstallerActionButton;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->c:Lcom/miui/packageInstaller/view/InstallerActionButton;

    return-object v0
.end method

.method public final getTvTips()Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->e:Landroid/widget/TextView;

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 0

    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    return-void
.end method

.method public final setMFirstButton(Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->a:Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;

    return-void
.end method

.method public final setMOnceAuthorizeBtn(Landroid/widget/TextView;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->d:Landroid/widget/TextView;

    return-void
.end method

.method public final setMSecondButton(Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->b:Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;

    return-void
.end method

.method public final setMThirdButton(Lcom/miui/packageInstaller/view/InstallerActionButton;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->c:Lcom/miui/packageInstaller/view/InstallerActionButton;

    return-void
.end method

.method public final setSecondTips(Ljava/lang/String;)V
    .locals 2

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->f:Landroid/widget/TextView;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {p1, v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;I)Landroid/text/Spanned;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    iget-object p1, p0, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->f:Landroid/widget/TextView;

    if-nez p1, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    :cond_2
    iget-object p1, p0, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->f:Landroid/widget/TextView;

    if-nez p1, :cond_3

    goto :goto_1

    :cond_3
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_1
    return-void
.end method

.method public final setTvTips(Landroid/widget/TextView;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->e:Landroid/widget/TextView;

    return-void
.end method
