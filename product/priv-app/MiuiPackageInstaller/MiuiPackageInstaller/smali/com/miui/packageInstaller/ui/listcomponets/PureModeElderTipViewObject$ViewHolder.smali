.class public final Lcom/miui/packageInstaller/ui/listcomponets/PureModeElderTipViewObject$ViewHolder;
.super Landroidx/recyclerview/widget/RecyclerView$d0;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/packageInstaller/ui/listcomponets/PureModeElderTipViewObject;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ViewHolder"
.end annotation


# instance fields
.field private openNow:Landroidx/appcompat/widget/AppCompatButton;

.field private security:Landroidx/appcompat/widget/LinearLayoutCompat;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    const-string v0, "itemView"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$d0;-><init>(Landroid/view/View;)V

    const v0, 0x7f0a02f7

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/LinearLayoutCompat;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeElderTipViewObject$ViewHolder;->security:Landroidx/appcompat/widget/LinearLayoutCompat;

    const v0, 0x7f0a028b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroidx/appcompat/widget/AppCompatButton;

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeElderTipViewObject$ViewHolder;->openNow:Landroidx/appcompat/widget/AppCompatButton;

    return-void
.end method


# virtual methods
.method public final getOpenNow()Landroidx/appcompat/widget/AppCompatButton;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeElderTipViewObject$ViewHolder;->openNow:Landroidx/appcompat/widget/AppCompatButton;

    return-object v0
.end method

.method public final getSecurity()Landroidx/appcompat/widget/LinearLayoutCompat;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeElderTipViewObject$ViewHolder;->security:Landroidx/appcompat/widget/LinearLayoutCompat;

    return-object v0
.end method

.method public final setOpenNow(Landroidx/appcompat/widget/AppCompatButton;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeElderTipViewObject$ViewHolder;->openNow:Landroidx/appcompat/widget/AppCompatButton;

    return-void
.end method

.method public final setSecurity(Landroidx/appcompat/widget/LinearLayoutCompat;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeElderTipViewObject$ViewHolder;->security:Landroidx/appcompat/widget/LinearLayoutCompat;

    return-void
.end method
