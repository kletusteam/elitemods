.class public final Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;
.super Landroid/widget/LinearLayout;

# interfaces
.implements Lx5/c;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/packageInstaller/secure/view/ScreenLockPassword$a;
    }
.end annotation


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/EditText;

.field private d:Landroid/widget/Button;

.field private e:Landroid/widget/Button;

.field private f:Landroid/widget/ImageView;

.field private g:Z

.field private h:Lx5/h;

.field private i:I

.field private j:Lf6/m;

.field private k:Ll8/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ll8/l<",
            "-",
            "Ljava/lang/Integer;",
            "La8/v;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance p1, Lf6/m;

    invoke-static {}, Lcom/android/packageinstaller/InstallerApplication;->g()Lcom/android/packageinstaller/InstallerApplication;

    move-result-object p2

    invoke-direct {p1, p2}, Lf6/m;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->j:Lf6/m;

    return-void
.end method

.method public static synthetic b(Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->s(Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic c(Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;)V
    .locals 0

    invoke-static {p0}, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->p(Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;)V

    return-void
.end method

.method public static synthetic e(Landroid/view/View;Landroid/view/View;Z)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->r(Landroid/view/View;Landroid/view/View;Z)V

    return-void
.end method

.method public static synthetic f(Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;)V
    .locals 0

    invoke-static {p0}, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->v(Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;)V

    return-void
.end method

.method public static synthetic g(Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->u(Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic h(Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->t(Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic i(Lm8/q;Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->w(Lm8/q;Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;)V

    return-void
.end method

.method public static final synthetic j(Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;)Landroid/widget/Button;
    .locals 0

    iget-object p0, p0, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->d:Landroid/widget/Button;

    return-object p0
.end method

.method public static final synthetic k(Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;)Lx5/h;
    .locals 0

    iget-object p0, p0, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->h:Lx5/h;

    return-object p0
.end method

.method public static final synthetic l(Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;)Landroid/widget/TextView;
    .locals 0

    iget-object p0, p0, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->b:Landroid/widget/TextView;

    return-object p0
.end method

.method public static final synthetic m(Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;)Landroid/widget/EditText;
    .locals 0

    iget-object p0, p0, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->c:Landroid/widget/EditText;

    return-object p0
.end method

.method public static final synthetic n(Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;)I
    .locals 0

    iget p0, p0, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->i:I

    return p0
.end method

.method public static final synthetic o(Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;I)V
    .locals 0

    iput p1, p0, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->i:I

    return-void
.end method

.method private static final p(Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;)V
    .locals 4

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->c:Landroid/widget/EditText;

    const/4 v1, 0x0

    const-string v2, "passwordEditText"

    if-nez v0, :cond_0

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_0
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setEnabled(Z)V

    iget-object v0, p0, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->c:Landroid/widget/EditText;

    if-nez v0, :cond_1

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_1
    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setFocusableInTouchMode(Z)V

    iget-object v0, p0, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->c:Landroid/widget/EditText;

    if-nez v0, :cond_2

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_2
    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    iget-object v0, p0, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->c:Landroid/widget/EditText;

    if-nez v0, :cond_3

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_3
    invoke-virtual {v0}, Landroid/widget/EditText;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-class v3, Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iget-object p0, p0, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->c:Landroid/widget/EditText;

    if-nez p0, :cond_4

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    move-object v1, p0

    :goto_0
    const/4 p0, 0x0

    invoke-virtual {v0, v1, p0}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    return-void
.end method

.method private final q()Z
    .locals 2

    iget-object v0, p0, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->j:Lf6/m;

    invoke-static {}, Lcom/android/packageinstaller/compat/UserHandleCompat;->myUserId()I

    move-result v1

    invoke-virtual {v0, v1}, Lf6/m;->g(I)I

    move-result v0

    const/high16 v1, 0x20000

    if-eq v0, v1, :cond_0

    const/high16 v1, 0x30000

    if-eq v0, v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0
.end method

.method private static final r(Landroid/view/View;Landroid/view/View;Z)V
    .locals 0

    const-string p1, "$editTextContainer"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_0

    const p1, 0x7f080120

    goto :goto_0

    :cond_0
    const p1, 0x7f08011f

    :goto_0
    invoke-virtual {p0, p1}, Landroid/view/View;->setBackgroundResource(I)V

    return-void
.end method

.method private static final s(Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;Landroid/view/View;)V
    .locals 4

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-boolean p1, p0, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->g:Z

    const-string v0, "passwordEyes"

    const-string v1, "passwordEditText"

    const/4 v2, 0x0

    if-eqz p1, :cond_4

    iget-object p1, p0, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->c:Landroid/widget/EditText;

    if-nez p1, :cond_0

    invoke-static {v1}, Lm8/i;->s(Ljava/lang/String;)V

    move-object p1, v2

    :cond_0
    invoke-static {}, Landroid/text/method/PasswordTransformationMethod;->getInstance()Landroid/text/method/PasswordTransformationMethod;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/widget/EditText;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->g:Z

    iget-object p1, p0, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->c:Landroid/widget/EditText;

    if-nez p1, :cond_1

    invoke-static {v1}, Lm8/i;->s(Ljava/lang/String;)V

    move-object p1, v2

    :cond_1
    iget-object v3, p0, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->c:Landroid/widget/EditText;

    if-nez v3, :cond_2

    invoke-static {v1}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v3, v2

    :cond_2
    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/widget/EditText;->setSelection(I)V

    iget-object p0, p0, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->f:Landroid/widget/ImageView;

    if-nez p0, :cond_3

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    move-object v2, p0

    :goto_0
    const p0, 0x7f080186

    goto :goto_2

    :cond_4
    iget-object p1, p0, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->c:Landroid/widget/EditText;

    if-nez p1, :cond_5

    invoke-static {v1}, Lm8/i;->s(Ljava/lang/String;)V

    move-object p1, v2

    :cond_5
    invoke-static {}, Landroid/text/method/HideReturnsTransformationMethod;->getInstance()Landroid/text/method/HideReturnsTransformationMethod;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/widget/EditText;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->g:Z

    iget-object p1, p0, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->c:Landroid/widget/EditText;

    if-nez p1, :cond_6

    invoke-static {v1}, Lm8/i;->s(Ljava/lang/String;)V

    move-object p1, v2

    :cond_6
    iget-object v3, p0, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->c:Landroid/widget/EditText;

    if-nez v3, :cond_7

    invoke-static {v1}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v3, v2

    :cond_7
    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/widget/EditText;->setSelection(I)V

    iget-object p0, p0, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->f:Landroid/widget/ImageView;

    if-nez p0, :cond_8

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_1

    :cond_8
    move-object v2, p0

    :goto_1
    const p0, 0x7f080187

    :goto_2
    invoke-virtual {v2, p0}, Landroid/widget/ImageView;->setImageResource(I)V

    return-void
.end method

.method private static final t(Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;Landroid/view/View;)V
    .locals 0

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p0, p0, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->k:Ll8/l;

    if-eqz p0, :cond_0

    const/4 p1, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {p0, p1}, Ll8/l;->j(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method private static final u(Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;Landroid/view/View;)V
    .locals 1

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object p1

    new-instance v0, Ly5/r;

    invoke-direct {v0, p0}, Ly5/r;-><init>(Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;)V

    invoke-virtual {p1, v0}, Lf6/z;->g(Ljava/lang/Runnable;)V

    return-void
.end method

.method private static final v(Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;)V
    .locals 9

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lx5/h$d;->a:Lx5/h$d;

    goto :goto_0

    :cond_0
    sget-object v0, Lx5/h$d;->b:Lx5/h$d;

    :goto_0
    const/4 v1, 0x0

    :try_start_0
    new-instance v2, Lm8/q;

    invoke-direct {v2}, Lm8/q;-><init>()V

    iget-object v3, p0, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->j:Lf6/m;

    iget-object v4, p0, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->c:Landroid/widget/EditText;

    if-nez v4, :cond_1

    const-string v4, "passwordEditText"

    invoke-static {v4}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v4, v1

    :cond_1
    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Lcom/android/packageinstaller/compat/UserHandleCompat;->myUserId()I

    move-result v5

    invoke-virtual {v3, v0, v4, v5}, Lf6/m;->e(Lx5/h$d;Ljava/lang/CharSequence;I)Z

    move-result v0

    iput-boolean v0, v2, Lm8/q;->a:Z

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object v0

    new-instance v3, Ly5/s;

    invoke-direct {v3, v2, p0}, Ly5/s;-><init>(Lm8/q;Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;)V

    invoke-virtual {v0, v3}, Lf6/z;->e(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Lf6/m$b; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-static {}, Lv8/t0;->c()Lv8/v1;

    move-result-object v2

    invoke-static {v2}, Lv8/f0;->a(Ld8/g;)Lv8/e0;

    move-result-object v3

    invoke-static {}, Lv8/t0;->c()Lv8/v1;

    move-result-object v4

    const/4 v5, 0x0

    new-instance v6, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword$b;

    invoke-direct {v6, p0, v0, v1}, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword$b;-><init>(Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;Lf6/m$b;Ld8/d;)V

    const/4 v7, 0x2

    const/4 v8, 0x0

    invoke-static/range {v3 .. v8}, Lv8/f;->d(Lv8/e0;Ld8/g;Lv8/g0;Ll8/p;ILjava/lang/Object;)Lv8/k1;

    :goto_1
    return-void
.end method

.method private static final w(Lm8/q;Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;)V
    .locals 3

    const-string v0, "$success"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "this$0"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-boolean p0, p0, Lm8/q;->a:Z

    if-eqz p0, :cond_0

    iget-object p0, p1, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->k:Ll8/l;

    if-eqz p0, :cond_4

    const/4 p1, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {p0, p1}, Ll8/l;->j(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_0
    iget-object p0, p1, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->c:Landroid/widget/EditText;

    const/4 v0, 0x0

    if-nez p0, :cond_1

    const-string p0, "passwordEditText"

    invoke-static {p0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object p0, v0

    :cond_1
    const-string v1, ""

    invoke-virtual {p0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object p0, p1, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->b:Landroid/widget/TextView;

    const-string v1, "errorTipsTextView"

    if-nez p0, :cond_2

    invoke-static {v1}, Lm8/i;->s(Ljava/lang/String;)V

    move-object p0, v0

    :cond_2
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object p0, p1, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->b:Landroid/widget/TextView;

    if-nez p0, :cond_3

    invoke-static {v1}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    move-object v0, p0

    :goto_0
    invoke-virtual {p1}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object p0

    const p1, 0x7f110265

    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_4
    :goto_1
    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    return-void
.end method

.method public d(Lx5/h;Ll8/l;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lx5/h;",
            "Ll8/l<",
            "-",
            "Ljava/lang/Integer;",
            "La8/v;",
            ">;)V"
        }
    .end annotation

    const-string v0, "dialog"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "callback"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->h:Lx5/h;

    iput-object p2, p0, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->k:Ll8/l;

    invoke-direct {p0}, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->q()Z

    move-result p1

    const/4 p2, 0x0

    const-string v0, "passwordEditText"

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->c:Landroid/widget/EditText;

    if-nez p1, :cond_0

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    move-object p2, p1

    :goto_0
    const/16 p1, 0x12

    goto :goto_2

    :cond_1
    iget-object p1, p0, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->c:Landroid/widget/EditText;

    if-nez p1, :cond_2

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    move-object p2, p1

    :goto_1
    const/16 p1, 0x81

    :goto_2
    invoke-virtual {p2, p1}, Landroid/widget/EditText;->setInputType(I)V

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object p1

    new-instance p2, Ly5/q;

    invoke-direct {p2, p0}, Ly5/q;-><init>(Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;)V

    const-wide/16 v0, 0x12c

    invoke-virtual {p1, p2, v0, v1}, Lf6/z;->d(Ljava/lang/Runnable;J)V

    return-void
.end method

.method public final getMinPasswordLength()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected onFinishInflate()V
    .locals 10

    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    const v0, 0x7f0a037f

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.tips)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->a:Landroid/widget/TextView;

    const v0, 0x7f0a0298

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.password_edit_text)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->c:Landroid/widget/EditText;

    const-string v1, "passwordEditText"

    const/4 v2, 0x0

    if-nez v0, :cond_0

    invoke-static {v1}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v2

    :cond_0
    new-instance v3, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword$a;

    invoke-direct {v3, p0}, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword$a;-><init>(Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;)V

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    const v0, 0x7f0a00a1

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v3, "findViewById(R.id.bt_confirm)"

    invoke-static {v0, v3}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->d:Landroid/widget/Button;

    const v0, 0x7f0a00a0

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v3, "findViewById(R.id.bt_cancel)"

    invoke-static {v0, v3}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->e:Landroid/widget/Button;

    const v0, 0x7f0a029a

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v3, "findViewById(R.id.password_eyes)"

    invoke-static {v0, v3}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->f:Landroid/widget/ImageView;

    const v0, 0x7f0a0299

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v3, "findViewById(R.id.password_error_tip)"

    invoke-static {v0, v3}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->b:Landroid/widget/TextView;

    const/4 v0, 0x1

    new-array v3, v0, [Landroid/view/View;

    iget-object v4, p0, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->d:Landroid/widget/Button;

    const-string v5, "confirmButton"

    if-nez v4, :cond_1

    invoke-static {v5}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v4, v2

    :cond_1
    const/4 v6, 0x0

    aput-object v4, v3, v6

    invoke-static {v3}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object v3

    invoke-interface {v3}, Lmiuix/animation/e;->c()Lmiuix/animation/j;

    move-result-object v3

    new-array v4, v6, [Lmiuix/animation/j$b;

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-interface {v3, v7, v4}, Lmiuix/animation/j;->G(F[Lmiuix/animation/j$b;)Lmiuix/animation/j;

    move-result-object v3

    iget-object v4, p0, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->d:Landroid/widget/Button;

    if-nez v4, :cond_2

    invoke-static {v5}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v4, v2

    :cond_2
    new-array v8, v6, [Lc9/a;

    invoke-interface {v3, v4, v8}, Lmiuix/animation/j;->z(Landroid/view/View;[Lc9/a;)V

    new-array v3, v0, [Landroid/view/View;

    iget-object v4, p0, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->e:Landroid/widget/Button;

    const-string v8, "cancelButton"

    if-nez v4, :cond_3

    invoke-static {v8}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v4, v2

    :cond_3
    aput-object v4, v3, v6

    invoke-static {v3}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object v3

    invoke-interface {v3}, Lmiuix/animation/e;->c()Lmiuix/animation/j;

    move-result-object v3

    new-array v4, v6, [Lmiuix/animation/j$b;

    invoke-interface {v3, v7, v4}, Lmiuix/animation/j;->G(F[Lmiuix/animation/j$b;)Lmiuix/animation/j;

    move-result-object v3

    iget-object v4, p0, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->e:Landroid/widget/Button;

    if-nez v4, :cond_4

    invoke-static {v8}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v4, v2

    :cond_4
    new-array v9, v6, [Lc9/a;

    invoke-interface {v3, v4, v9}, Lmiuix/animation/j;->z(Landroid/view/View;[Lc9/a;)V

    new-array v0, v0, [Landroid/view/View;

    iget-object v3, p0, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->f:Landroid/widget/ImageView;

    const-string v4, "passwordEyes"

    if-nez v3, :cond_5

    invoke-static {v4}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v3, v2

    :cond_5
    aput-object v3, v0, v6

    invoke-static {v0}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object v0

    invoke-interface {v0}, Lmiuix/animation/e;->c()Lmiuix/animation/j;

    move-result-object v0

    new-array v3, v6, [Lmiuix/animation/j$b;

    invoke-interface {v0, v7, v3}, Lmiuix/animation/j;->G(F[Lmiuix/animation/j$b;)Lmiuix/animation/j;

    move-result-object v0

    iget-object v3, p0, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->f:Landroid/widget/ImageView;

    if-nez v3, :cond_6

    invoke-static {v4}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v3, v2

    :cond_6
    new-array v6, v6, [Lc9/a;

    invoke-interface {v0, v3, v6}, Lmiuix/animation/j;->z(Landroid/view/View;[Lc9/a;)V

    const v0, 0x7f0a0156

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v3, "findViewById(R.id.exit_text_container)"

    invoke-static {v0, v3}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->c:Landroid/widget/EditText;

    if-nez v3, :cond_7

    invoke-static {v1}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v3, v2

    :cond_7
    new-instance v6, Ly5/p;

    invoke-direct {v6, v0}, Ly5/p;-><init>(Landroid/view/View;)V

    invoke-virtual {v3, v6}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v3, p0, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->c:Landroid/widget/EditText;

    if-nez v3, :cond_8

    invoke-static {v1}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v3, v2

    :cond_8
    invoke-virtual {v3}, Landroid/widget/EditText;->hasFocus()Z

    move-result v1

    if-eqz v1, :cond_9

    const v1, 0x7f080120

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    :cond_9
    iget-object v0, p0, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->f:Landroid/widget/ImageView;

    if-nez v0, :cond_a

    invoke-static {v4}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v2

    :cond_a
    new-instance v1, Ly5/m;

    invoke-direct {v1, p0}, Ly5/m;-><init>(Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->e:Landroid/widget/Button;

    if-nez v0, :cond_b

    invoke-static {v8}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v2

    :cond_b
    new-instance v1, Ly5/o;

    invoke-direct {v1, p0}, Ly5/o;-><init>(Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->d:Landroid/widget/Button;

    if-nez v0, :cond_c

    invoke-static {v5}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_0

    :cond_c
    move-object v2, v0

    :goto_0
    new-instance v0, Ly5/n;

    invoke-direct {v0, p0}, Ly5/n;-><init>(Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;)V

    invoke-virtual {v2, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public release()V
    .locals 0

    invoke-static {p0}, Lx5/c$a;->a(Lx5/c;)V

    return-void
.end method

.method public setCancelButtonText(Ljava/lang/CharSequence;)V
    .locals 1

    const-string v0, "text"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->e:Landroid/widget/Button;

    if-nez v0, :cond_0

    const-string v0, "cancelButton"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v0, 0x0

    :cond_0
    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setConfirmButtonText(Ljava/lang/CharSequence;)V
    .locals 1

    const-string v0, "text"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->d:Landroid/widget/Button;

    if-nez v0, :cond_0

    const-string v0, "confirmButton"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v0, 0x0

    :cond_0
    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setTipMsgText(Ljava/lang/CharSequence;)V
    .locals 1

    const-string v0, "text"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->a:Landroid/widget/TextView;

    if-nez v0, :cond_0

    const-string v0, "tipsTextView"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v0, 0x0

    :cond_0
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
