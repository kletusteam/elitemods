.class public Lcom/miui/packageInstaller/PurePackageInstallerActivity;
.super Lm5/z0;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/packageInstaller/PurePackageInstallerActivity$b;,
        Lcom/miui/packageInstaller/PurePackageInstallerActivity$a;
    }
.end annotation


# instance fields
.field private final V:Ljava/lang/String;

.field private W:Landroidx/recyclerview/widget/RecyclerView;

.field private X:Landroidx/recyclerview/widget/RecyclerView;

.field private Y:Lj6/b;

.field private Z:Lj6/b;

.field private e0:La6/g;

.field private f0:Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;

.field private g0:Landroid/widget/FrameLayout;

.field private h0:Landroid/widget/TextView;

.field private i0:Lcom/airbnb/lottie/LottieAnimationView;

.field private j0:Landroid/widget/LinearLayout;

.field private k0:Landroid/widget/TextView;

.field private l0:Landroid/view/ViewGroup;

.field private m0:Landroid/view/ViewGroup;

.field private n0:Landroid/widget/TextView;

.field private o0:Landroid/widget/TextView;

.field private p0:Landroid/widget/FrameLayout;

.field private q0:Landroid/widget/ImageView;

.field private r0:Lcom/miui/packageInstaller/PurePackageInstallerActivity$a;

.field private s0:Z

.field private t0:Lcom/miui/packageInstaller/PurePackageInstallerActivity$b;

.field private u0:I

.field private v0:I

.field private w0:J

.field private x0:Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;

.field private y0:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lm5/z0;-><init>()V

    const-string v0, "NewPI"

    iput-object v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->V:Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->s0:Z

    iput v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->u0:I

    iput v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->v0:I

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->w0:J

    return-void
.end method

.method public static synthetic A2(Lcom/miui/packageInstaller/PurePackageInstallerActivity;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->n3(Lcom/miui/packageInstaller/PurePackageInstallerActivity;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic B2(Lcom/miui/packageInstaller/PurePackageInstallerActivity;Landroid/content/DialogInterface;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->l3(Lcom/miui/packageInstaller/PurePackageInstallerActivity;Landroid/content/DialogInterface;)V

    return-void
.end method

.method public static synthetic C2(Lcom/miui/packageInstaller/PurePackageInstallerActivity;Landroid/content/Context;ILjava/lang/Object;Lm6/a;)V
    .locals 0

    invoke-static {p0, p1, p2, p3, p4}, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->O2(Lcom/miui/packageInstaller/PurePackageInstallerActivity;Landroid/content/Context;ILjava/lang/Object;Lm6/a;)V

    return-void
.end method

.method public static synthetic D2(Lcom/miui/packageInstaller/PurePackageInstallerActivity;Landroid/content/DialogInterface;I)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->j3(Lcom/miui/packageInstaller/PurePackageInstallerActivity;Landroid/content/DialogInterface;I)V

    return-void
.end method

.method public static final synthetic E2(Lcom/miui/packageInstaller/PurePackageInstallerActivity;)Lo5/b;
    .locals 0

    iget-object p0, p0, Lq2/b;->q:Lo5/b;

    return-object p0
.end method

.method private final F2(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lq2/b;->q:Lo5/b;

    invoke-virtual {v0}, Lo5/b;->d()V

    new-instance v0, Lp5/b;

    const-string v1, "button"

    invoke-direct {v0, p1, v1, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    iget-object p1, p0, Lq2/b;->q:Lo5/b;

    invoke-virtual {p1}, Lo5/b;->c()V

    return-void
.end method

.method private final G2()V
    .locals 2

    iget-boolean v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->y0:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->y0:Z

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object v0

    sget-object v1, Lm5/m1;->a:Lm5/m1;

    invoke-virtual {v0, v1}, Lf6/z;->g(Ljava/lang/Runnable;)V

    return-void
.end method

.method private static final H2()V
    .locals 0

    invoke-static {}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->countSecurityModeProtectTimes()V

    return-void
.end method

.method private final I2()V
    .locals 1

    iget-object v0, p0, Lq2/b;->q:Lo5/b;

    invoke-virtual {v0}, Lo5/b;->d()V

    iget-object v0, p0, Lq2/b;->q:Lo5/b;

    invoke-virtual {v0}, Lo5/b;->c()V

    return-void
.end method

.method private final K2(II)I
    .locals 1

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->u()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move p1, p2

    :cond_1
    return p1
.end method

.method private final L2()V
    .locals 2

    iget-object v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->i0:Lcom/airbnb/lottie/LottieAnimationView;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/airbnb/lottie/LottieAnimationView;->h()V

    :cond_0
    iget-object v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->j0:Landroid/widget/LinearLayout;

    if-nez v0, :cond_1

    goto :goto_0

    :cond_1
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method private final N2()V
    .locals 5

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    const-string v2, "null cannot be cast to non-null type androidx.constraintlayout.widget.ConstraintLayout.LayoutParams"

    const-string v3, "null cannot be cast to non-null type android.widget.FrameLayout.LayoutParams"

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->q0:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-static {v1, v3}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0700e0

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/widget/FrameLayout$LayoutParams;->setMarginEnd(I)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_0
    iget-object v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->l0:Landroid/view/ViewGroup;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-static {v1, v2}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Landroidx/constraintlayout/widget/ConstraintLayout$b;

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070146

    :goto_0
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMarginStart(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMarginEnd(I)V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->q0:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-static {v1, v3}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0700c1

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/widget/FrameLayout$LayoutParams;->setMarginEnd(I)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_2
    iget-object v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->l0:Landroid/view/ViewGroup;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-static {v1, v2}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Landroidx/constraintlayout/widget/ConstraintLayout$b;

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0700dc

    goto :goto_0

    :cond_3
    :goto_1
    return-void
.end method

.method private static final O2(Lcom/miui/packageInstaller/PurePackageInstallerActivity;Landroid/content/Context;ILjava/lang/Object;Lm6/a;)V
    .locals 0

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->d3()V

    return-void
.end method

.method private static final P2(Lcom/miui/packageInstaller/PurePackageInstallerActivity;Landroid/content/Context;ILjava/lang/Object;Lm6/a;)V
    .locals 0

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    instance-of p1, p3, Ljava/lang/Integer;

    if-eqz p1, :cond_0

    check-cast p3, Ljava/lang/Number;

    invoke-virtual {p3}, Ljava/lang/Number;->intValue()I

    move-result p1

    invoke-virtual {p0, p1}, Lm5/z0;->N1(I)V

    :cond_0
    return-void
.end method

.method private final Q2()Z
    .locals 4

    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/miui/packageInstaller/model/CloudParams;->testConfig:Lcom/miui/packageInstaller/model/InterceptAbTestConfig;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/InterceptAbTestConfig;->getSingleAuthStyle()I

    move-result v0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    move v0, v2

    :goto_0
    if-nez v0, :cond_3

    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, v0, Lcom/miui/packageInstaller/model/CloudParams;->testConfig:Lcom/miui/packageInstaller/model/InterceptAbTestConfig;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/InterceptAbTestConfig;->getSingleAuthStyle()I

    move-result v0

    const/4 v3, 0x3

    if-ne v0, v3, :cond_1

    move v0, v1

    goto :goto_1

    :cond_1
    move v0, v2

    :goto_1
    if-eqz v0, :cond_2

    goto :goto_2

    :cond_2
    move v1, v2

    :cond_3
    :goto_2
    return v1
.end method

.method private final R2()Z
    .locals 5

    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    iget-boolean v0, v0, Lcom/miui/packageInstaller/model/CloudParams;->storeListed:Z

    if-nez v0, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    move v0, v2

    :goto_0
    const/4 v3, 0x0

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lm5/z0;->x1()Lcom/miui/packageInstaller/model/Virus;

    move-result-object v0

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, v0, Lcom/miui/packageInstaller/model/CloudParams;->safeType:Ljava/lang/String;

    goto :goto_1

    :cond_1
    move-object v0, v3

    :goto_1
    const-string v4, "unknown"

    invoke-static {v0, v4}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, v0, Lcom/miui/packageInstaller/model/CloudParams;->safeType:Ljava/lang/String;

    goto :goto_2

    :cond_2
    move-object v0, v3

    :goto_2
    const-string v4, "risk_c_level"

    invoke-static {v0, v4}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    :cond_3
    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, v0, Lcom/miui/packageInstaller/model/CloudParams;->safeType:Ljava/lang/String;

    goto :goto_3

    :cond_4
    move-object v0, v3

    :goto_3
    const-string v4, "risk_vpn"

    invoke-static {v0, v4}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v3, v0, Lcom/miui/packageInstaller/model/CloudParams;->safeType:Ljava/lang/String;

    :cond_5
    const-string v0, "risk_anti_spoofing"

    invoke-static {v3, v0}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v0

    if-eqz v0, :cond_6

    iget-boolean v0, v0, Lcom/miui/packageInstaller/model/CloudParams;->useSystemAppRules:Z

    if-nez v0, :cond_6

    move v0, v1

    goto :goto_4

    :cond_6
    move v0, v2

    :goto_4
    if-eqz v0, :cond_9

    invoke-virtual {p0}, Lm5/z0;->q1()Lcom/miui/packageInstaller/model/ApkInfo;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/ApkInfo;->isOtherVersionInstalled()Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v1

    goto :goto_5

    :cond_7
    move v0, v2

    :goto_5
    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->x0:Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;

    if-eqz v0, :cond_8

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;->getAuthorized()Z

    move-result v0

    if-ne v0, v1, :cond_8

    move v0, v1

    goto :goto_6

    :cond_8
    move v0, v2

    :goto_6
    if-nez v0, :cond_9

    goto :goto_7

    :cond_9
    move v1, v2

    :goto_7
    return v1
.end method

.method private final S2()V
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ObjectAnimatorBinding"
        }
    .end annotation

    iget-object v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->g0:Landroid/widget/FrameLayout;

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    const-string v2, "alpha"

    invoke-static {v0, v2, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    const-wide/16 v1, 0x12c

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    return-void

    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method private final T2(Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;)V
    .locals 9

    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    iget-boolean v0, v0, Lcom/miui/packageInstaller/model/CloudParams;->bundleApp:Z

    if-ne v0, v2, :cond_0

    move v0, v2

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {p0, p1, p2, v2}, Lm5/z0;->a1(Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;Z)Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;

    goto/16 :goto_c

    :cond_1
    invoke-virtual {p0}, Lm5/z0;->q1()Lcom/miui/packageInstaller/model/ApkInfo;

    move-result-object p2

    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lcom/miui/packageInstaller/model/ApkInfo;->isOtherVersionInstalled()Z

    move-result p2

    if-ne p2, v2, :cond_2

    move v1, v2

    :cond_2
    const/4 p2, 0x2

    const/16 v0, 0x8

    const/4 v2, 0x0

    if-eqz v1, :cond_6

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->getMFirstButton()Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;

    move-result-object v4

    if-eqz v4, :cond_3

    const v1, 0x7f1103dc

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v1, "getString(R.string.update_start)"

    invoke-static {v5, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v6, 0x0

    const/4 v7, 0x4

    const/4 v8, 0x0

    move-object v3, p0

    invoke-static/range {v3 .. v8}, Lm5/z0;->p2(Lm5/z0;Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;Ljava/lang/String;ZILjava/lang/Object;)V

    :cond_3
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->getMSecondButton()Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-static {p0, v1, v2, p2, v2}, Lm5/z0;->k2(Lm5/z0;Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;Ljava/lang/CharSequence;ILjava/lang/Object;)V

    :cond_4
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->getMThirdButton()Lcom/miui/packageInstaller/view/InstallerActionButton;

    move-result-object p1

    if-nez p1, :cond_5

    goto/16 :goto_c

    :cond_5
    invoke-virtual {p1, v0}, Landroid/widget/Button;->setVisibility(I)V

    goto/16 :goto_c

    :cond_6
    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result v1

    if-eqz v1, :cond_f

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->getMThirdButton()Lcom/miui/packageInstaller/view/InstallerActionButton;

    move-result-object v1

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/miui/packageInstaller/view/InstallerActionButton;->a()Landroid/view/View;

    move-result-object v1

    goto :goto_1

    :cond_7
    move-object v1, v2

    :goto_1
    if-nez v1, :cond_8

    goto :goto_2

    :cond_8
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    :goto_2
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->getMSecondButton()Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;

    move-result-object v1

    if-eqz v1, :cond_9

    invoke-static {p0, v1, v2, p2, v2}, Lm5/z0;->k2(Lm5/z0;Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;Ljava/lang/CharSequence;ILjava/lang/Object;)V

    :cond_9
    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object p2

    if-eqz p2, :cond_a

    iget-object p2, p2, Lcom/miui/packageInstaller/model/CloudParams;->positiveButtonTip:Lcom/miui/packageInstaller/model/PositiveButtonRules;

    if-eqz p2, :cond_a

    iget-object p2, p2, Lcom/miui/packageInstaller/model/PositiveButtonRules;->method:Ljava/lang/String;

    goto :goto_3

    :cond_a
    move-object p2, v2

    :goto_3
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p2

    if-nez p2, :cond_c

    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object p2

    if-eqz p2, :cond_b

    iget-object p2, p2, Lcom/miui/packageInstaller/model/CloudParams;->positiveButtonTip:Lcom/miui/packageInstaller/model/PositiveButtonRules;

    if-eqz p2, :cond_b

    iget-object p2, p2, Lcom/miui/packageInstaller/model/PositiveButtonRules;->actionUrl:Ljava/lang/String;

    goto :goto_4

    :cond_b
    move-object p2, v2

    :goto_4
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p2

    if-nez p2, :cond_c

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->getMFirstButton()Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;

    move-result-object p1

    if-eqz p1, :cond_19

    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object p2

    if-eqz p2, :cond_15

    goto/16 :goto_a

    :cond_c
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->getMFirstButton()Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;

    move-result-object p2

    if-eqz p2, :cond_d

    invoke-interface {p2}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;->a()Landroid/view/View;

    move-result-object v2

    :cond_d
    if-nez v2, :cond_e

    goto :goto_5

    :cond_e
    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    :goto_5
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->getMSecondButton()Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;

    goto/16 :goto_c

    :cond_f
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->getMFirstButton()Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;

    move-result-object v1

    if-eqz v1, :cond_10

    invoke-interface {v1}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;->a()Landroid/view/View;

    move-result-object v1

    goto :goto_6

    :cond_10
    move-object v1, v2

    :goto_6
    if-nez v1, :cond_11

    goto :goto_7

    :cond_11
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    :goto_7
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->getMThirdButton()Lcom/miui/packageInstaller/view/InstallerActionButton;

    move-result-object v1

    if-eqz v1, :cond_12

    invoke-static {p0, v1, v2, p2, v2}, Lm5/z0;->k2(Lm5/z0;Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;Ljava/lang/CharSequence;ILjava/lang/Object;)V

    :cond_12
    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object p2

    if-eqz p2, :cond_13

    iget-object p2, p2, Lcom/miui/packageInstaller/model/CloudParams;->positiveButtonTip:Lcom/miui/packageInstaller/model/PositiveButtonRules;

    if-eqz p2, :cond_13

    iget-object p2, p2, Lcom/miui/packageInstaller/model/PositiveButtonRules;->method:Ljava/lang/String;

    goto :goto_8

    :cond_13
    move-object p2, v2

    :goto_8
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p2

    if-nez p2, :cond_16

    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object p2

    if-eqz p2, :cond_14

    iget-object p2, p2, Lcom/miui/packageInstaller/model/CloudParams;->positiveButtonTip:Lcom/miui/packageInstaller/model/PositiveButtonRules;

    if-eqz p2, :cond_14

    iget-object p2, p2, Lcom/miui/packageInstaller/model/PositiveButtonRules;->actionUrl:Ljava/lang/String;

    goto :goto_9

    :cond_14
    move-object p2, v2

    :goto_9
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p2

    if-nez p2, :cond_16

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->getMSecondButton()Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;

    move-result-object p1

    if-eqz p1, :cond_19

    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object p2

    if-eqz p2, :cond_15

    :goto_a
    iget-object v2, p2, Lcom/miui/packageInstaller/model/CloudParams;->positiveButtonTip:Lcom/miui/packageInstaller/model/PositiveButtonRules;

    :cond_15
    invoke-static {v2}, Lm8/i;->c(Ljava/lang/Object;)V

    invoke-virtual {p0, p1, v2}, Lm5/z0;->m2(Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;Lcom/miui/packageInstaller/model/PositiveButtonRules;)V

    goto :goto_c

    :cond_16
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->getMSecondButton()Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;

    move-result-object p2

    if-eqz p2, :cond_17

    invoke-interface {p2}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;->a()Landroid/view/View;

    move-result-object v2

    :cond_17
    if-nez v2, :cond_18

    goto :goto_b

    :cond_18
    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    :goto_b
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->getMThirdButton()Lcom/miui/packageInstaller/view/InstallerActionButton;

    :cond_19
    :goto_c
    return-void
.end method

.method private final U2()V
    .locals 0

    invoke-virtual {p0}, Lm5/z0;->h1()V

    invoke-direct {p0}, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->e3()V

    return-void
.end method

.method private final V2(Landroid/widget/TextView;IILjava/lang/String;Ljava/lang/String;Ln2/b$a;)V
    .locals 7

    new-instance v0, Lp5/g;

    const-string v1, "single_authorize_btn"

    const-string v2, "button"

    invoke-direct {v0, v1, v2, p0}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    if-eqz p1, :cond_2

    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const p4, 0x7f1103be

    invoke-virtual {p0, p4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object p4

    :cond_0
    move-object v2, p4

    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p4

    if-eqz p4, :cond_1

    const p4, 0x7f110048

    invoke-virtual {p0, p4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object p5

    :cond_1
    move-object v3, p5

    sget-object v0, Lf6/b0;->a:Lf6/b0$a;

    invoke-static {v2}, Lm8/i;->c(Ljava/lang/Object;)V

    invoke-static {v3}, Lm8/i;->c(Ljava/lang/Object;)V

    new-instance v6, Lcom/miui/packageInstaller/PurePackageInstallerActivity$d;

    invoke-direct {v6, p0, p6}, Lcom/miui/packageInstaller/PurePackageInstallerActivity$d;-><init>(Lcom/miui/packageInstaller/PurePackageInstallerActivity;Ln2/b$a;)V

    move-object v1, p1

    move v4, p2

    move v5, p3

    invoke-virtual/range {v0 .. v6}, Lf6/b0$a;->a(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;IILf6/b0$a$a;)V

    :cond_2
    return-void
.end method

.method private final W2(Landroid/widget/TextView;)V
    .locals 8

    const v0, 0x7f060501

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getColor(I)I

    move-result v3

    const v0, 0x7f060502

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getColor(I)I

    move-result v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v1 .. v7}, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->V2(Landroid/widget/TextView;IILjava/lang/String;Ljava/lang/String;Ln2/b$a;)V

    return-void
.end method

.method private final X2(Landroid/widget/TextView;)V
    .locals 8

    const v0, 0x7f06002f

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getColor(I)I

    move-result v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v1, p0

    move-object v2, p1

    move v3, v4

    invoke-direct/range {v1 .. v7}, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->V2(Landroid/widget/TextView;IILjava/lang/String;Ljava/lang/String;Ln2/b$a;)V

    return-void
.end method

.method private final Y2(Landroid/widget/TextView;)V
    .locals 8

    const v0, 0x7f060501

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getColor(I)I

    move-result v3

    const v0, 0x7f060502

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getColor(I)I

    move-result v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v1 .. v7}, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->V2(Landroid/widget/TextView;IILjava/lang/String;Ljava/lang/String;Ln2/b$a;)V

    return-void
.end method

.method private final Z2(Landroid/widget/TextView;)V
    .locals 8

    const v0, 0x7f06002f

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getColor(I)I

    move-result v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v1, p0

    move-object v2, p1

    move v3, v4

    invoke-direct/range {v1 .. v7}, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->V2(Landroid/widget/TextView;IILjava/lang/String;Ljava/lang/String;Ln2/b$a;)V

    return-void
.end method

.method private final a3()V
    .locals 2

    iget-object v0, p0, Lq2/b;->q:Lo5/b;

    invoke-virtual {v0}, Lo5/b;->d()V

    iget-object v0, p0, Lq2/b;->q:Lo5/b;

    const-string v1, "popup_virus"

    invoke-virtual {v0, v1}, Lo5/b;->B(Ljava/lang/String;)V

    iget-object v0, p0, Lq2/b;->q:Lo5/b;

    invoke-virtual {v0}, Lo5/b;->c()V

    return-void
.end method

.method private final c3()V
    .locals 8

    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    iget-boolean v0, v0, Lcom/miui/packageInstaller/model/CloudParams;->storeListed:Z

    if-ne v0, v1, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    move v0, v2

    :goto_0
    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-boolean v0, v0, Lcom/miui/packageInstaller/model/CloudParams;->bundleApp:Z

    if-ne v0, v1, :cond_1

    goto :goto_1

    :cond_1
    move v1, v2

    :goto_1
    if-eqz v1, :cond_a

    :cond_2
    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_3

    iget-object v0, v0, Lcom/miui/packageInstaller/model/CloudParams;->safeType:Ljava/lang/String;

    goto :goto_2

    :cond_3
    move-object v0, v1

    :goto_2
    const-string v2, "risk_vpn"

    invoke-static {v0, v2}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v1, v0, Lcom/miui/packageInstaller/model/CloudParams;->safeType:Ljava/lang/String;

    :cond_4
    const-string v0, "risk_anti_spoofing"

    invoke-static {v1, v0}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    invoke-virtual {p0}, Lm5/z0;->q1()Lcom/miui/packageInstaller/model/ApkInfo;

    move-result-object v0

    if-eqz v0, :cond_a

    sget-object v1, Ld6/l0;->c:Ld6/l0$a;

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/ApkInfo;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    if-nez v2, :cond_5

    move-object v2, v3

    :cond_5
    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/ApkInfo;->getLabel()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_6

    move-object v4, v3

    :cond_6
    invoke-virtual {p0}, Lm5/z0;->r1()Lm5/e;

    move-result-object v5

    iget-object v5, v5, Lm5/e;->e:Ljava/lang/String;

    const-string v6, "mCallingPackage.name ?: \"\""

    if-nez v5, :cond_7

    move-object v5, v3

    goto :goto_3

    :cond_7
    invoke-static {v5, v6}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_3
    invoke-virtual {p0}, Lm5/z0;->r1()Lm5/e;

    move-result-object v7

    iget-object v7, v7, Lm5/e;->e:Ljava/lang/String;

    if-nez v7, :cond_8

    move-object v7, v3

    goto :goto_4

    :cond_8
    invoke-static {v7, v6}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_4
    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/ApkInfo;->getApkMd5()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_9

    move-object v6, v3

    goto :goto_5

    :cond_9
    move-object v6, v0

    :goto_5
    move-object v3, v4

    move-object v4, v5

    move-object v5, v7

    invoke-virtual/range {v1 .. v6}, Ld6/l0$a;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->x0:Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;

    :cond_a
    return-void
.end method

.method private final d3()V
    .locals 3

    iget-object v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->V:Ljava/lang/String;

    const-string v1, "reload"

    invoke-static {v0, v1}, Lf6/o;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    iget-boolean v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->s0:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->s0:Z

    invoke-virtual {p0, v0}, Lm5/z0;->e2(Z)V

    invoke-direct {p0}, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->h3()V

    iget-object v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->Z:Lj6/b;

    const/4 v1, 0x0

    if-nez v0, :cond_1

    const-string v0, "mWarningAdapter"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_1
    invoke-virtual {v0, v1}, Lj6/b;->j0(Ljava/util/List;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->f0:Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;

    if-nez v0, :cond_2

    goto :goto_0

    :cond_2
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :goto_0
    invoke-virtual {p0}, Lm5/z0;->q1()Lcom/miui/packageInstaller/model/ApkInfo;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lm5/z0;->w1()Lw5/a;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lm5/z0;->z1()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {p0, v2}, Lm5/z0;->h2(I)V

    invoke-virtual {p0}, Lm5/z0;->z1()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Lw5/a;->l(Lcom/miui/packageInstaller/model/ApkInfo;I)V

    :cond_3
    return-void
.end method

.method private final e3()V
    .locals 1

    sget-object v0, Ld6/l0;->c:Ld6/l0$a;

    invoke-virtual {v0}, Ld6/l0$a;->a()V

    return-void
.end method

.method private final f3(Landroid/widget/TextView;)V
    .locals 3

    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/miui/packageInstaller/model/CloudParams;->testConfig:Lcom/miui/packageInstaller/model/InterceptAbTestConfig;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/InterceptAbTestConfig;->getSingleAuthStyle()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x1

    if-nez v0, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v2, v1, :cond_2

    invoke-direct {p0, p1}, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->X2(Landroid/widget/TextView;)V

    goto :goto_4

    :cond_2
    :goto_1
    const/4 v1, 0x2

    if-nez v0, :cond_3

    goto :goto_2

    :cond_3
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v2, v1, :cond_4

    invoke-direct {p0, p1}, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->Y2(Landroid/widget/TextView;)V

    goto :goto_4

    :cond_4
    :goto_2
    const/4 v1, 0x3

    if-nez v0, :cond_5

    goto :goto_3

    :cond_5
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v1, :cond_6

    invoke-direct {p0, p1}, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->Z2(Landroid/widget/TextView;)V

    goto :goto_4

    :cond_6
    :goto_3
    invoke-direct {p0, p1}, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->W2(Landroid/widget/TextView;)V

    :goto_4
    return-void
.end method

.method private final h3()V
    .locals 2

    iget-object v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->g0:Landroid/widget/FrameLayout;

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setAlpha(F)V

    :goto_0
    iget-object v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->g0:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    if-nez v0, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    :goto_1
    iget-object v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->j0:Landroid/widget/LinearLayout;

    if-nez v0, :cond_2

    goto :goto_2

    :cond_2
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :goto_2
    iget-object v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->i0:Lcom/airbnb/lottie/LottieAnimationView;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/airbnb/lottie/LottieAnimationView;->p()V

    :cond_3
    return-void
.end method

.method private final i3()V
    .locals 4

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->w0:J

    new-instance v0, Lmiuix/appcompat/app/i$b;

    invoke-direct {v0, p0}, Lmiuix/appcompat/app/i$b;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0}, Lm5/z0;->q1()Lcom/miui/packageInstaller/model/ApkInfo;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/miui/packageInstaller/model/ApkInfo;->getLabel()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    const/4 v3, 0x0

    aput-object v2, v1, v3

    const v2, 0x7f11025b

    invoke-virtual {p0, v2, v1}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiuix/appcompat/app/i$b;->t(Ljava/lang/CharSequence;)Lmiuix/appcompat/app/i$b;

    move-result-object v0

    const v1, 0x7f11025a

    invoke-virtual {v0, v1}, Lmiuix/appcompat/app/i$b;->e(I)Lmiuix/appcompat/app/i$b;

    move-result-object v0

    const v1, 0x7f1100b5

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lm5/i1;

    invoke-direct {v2, p0}, Lm5/i1;-><init>(Lcom/miui/packageInstaller/PurePackageInstallerActivity;)V

    invoke-virtual {v0, v1, v2}, Lmiuix/appcompat/app/i$b;->i(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/i$b;

    move-result-object v0

    const v1, 0x7f1103bd

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lm5/h1;

    invoke-direct {v2, p0}, Lm5/h1;-><init>(Lcom/miui/packageInstaller/PurePackageInstallerActivity;)V

    invoke-virtual {v0, v1, v2}, Lmiuix/appcompat/app/i$b;->p(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/i$b;

    move-result-object v0

    invoke-virtual {v0}, Lmiuix/appcompat/app/i$b;->w()Lmiuix/appcompat/app/i;

    move-result-object v0

    new-instance v1, Lm5/j1;

    invoke-direct {v1, p0}, Lm5/j1;-><init>(Lcom/miui/packageInstaller/PurePackageInstallerActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    new-instance v0, Lp5/g;

    const-string v1, "single_authorize_popup"

    const-string v2, "popup"

    invoke-direct {v0, v1, v2, p0}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    new-instance v0, Lp5/g;

    const-string v1, "single_authorize_popup_cancel_btn"

    const-string v2, "button"

    invoke-direct {v0, v1, v2, p0}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    new-instance v0, Lp5/g;

    const-string v1, "single_authorize_popup_authorize_btn"

    invoke-direct {v0, v1, v2, p0}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    return-void
.end method

.method private static final j3(Lcom/miui/packageInstaller/PurePackageInstallerActivity;Landroid/content/DialogInterface;I)V
    .locals 0

    const-string p2, "this$0"

    invoke-static {p0, p2}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "dialog"

    invoke-static {p1, p2}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    const-string p1, "single_authorize_popup_cancel_btn"

    invoke-direct {p0, p1}, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->F2(Ljava/lang/String;)V

    return-void
.end method

.method private static final k3(Lcom/miui/packageInstaller/PurePackageInstallerActivity;Landroid/content/DialogInterface;I)V
    .locals 0

    const-string p2, "this$0"

    invoke-static {p0, p2}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "dialog"

    invoke-static {p1, p2}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "single_authorize_popup_authorize_btn"

    invoke-direct {p0, p2}, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->F2(Ljava/lang/String;)V

    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    invoke-direct {p0}, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->p3()V

    return-void
.end method

.method private static final l3(Lcom/miui/packageInstaller/PurePackageInstallerActivity;Landroid/content/DialogInterface;)V
    .locals 2

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Lp5/b;

    const-string v0, "single_authorize_popup_back_btn"

    const-string v1, "button"

    invoke-direct {p1, v0, v1, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p1}, Lp5/f;->c()Z

    invoke-direct {p0}, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->I2()V

    return-void
.end method

.method private final m3(Ljava/lang/String;)V
    .locals 4

    new-instance v0, Lp5/g;

    const-string v1, "virus_cue_popup"

    const-string v2, "popup"

    invoke-direct {v0, v1, v2, p0}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    new-instance v0, Lp5/g;

    const-string v1, "virus_cue_popup_know_btn"

    const-string v2, "button"

    invoke-direct {v0, v1, v2, p0}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    sget-object v0, La6/d;->a:La6/d;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0}, Lm5/z0;->q1()Lcom/miui/packageInstaller/model/ApkInfo;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/miui/packageInstaller/model/ApkInfo;->getLabel()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    const/4 v3, 0x0

    aput-object v2, v1, v3

    const v2, 0x7f11025b

    invoke-virtual {p0, v2, v1}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "getString(R.string.once_\u2026g_title, mApkInfo?.label)"

    invoke-static {v1, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v2, Lm5/k1;

    invoke-direct {v2, p0}, Lm5/k1;-><init>(Lcom/miui/packageInstaller/PurePackageInstallerActivity;)V

    invoke-virtual {v0, p0, v1, p1, v2}, La6/d;->g(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)Lmiuix/appcompat/app/i;

    move-result-object p1

    new-instance v0, Lm5/g1;

    invoke-direct {v0, p0}, Lm5/g1;-><init>(Lcom/miui/packageInstaller/PurePackageInstallerActivity;)V

    invoke-virtual {p1, v0}, Landroid/app/Dialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    return-void
.end method

.method private static final n3(Lcom/miui/packageInstaller/PurePackageInstallerActivity;Landroid/view/View;)V
    .locals 2

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->a3()V

    new-instance p1, Lp5/b;

    const-string v0, "virus_cue_popup_know_btn"

    const-string v1, "button"

    invoke-direct {p1, v0, v1, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p1}, Lp5/f;->c()Z

    return-void
.end method

.method private static final o3(Lcom/miui/packageInstaller/PurePackageInstallerActivity;Landroid/content/DialogInterface;)V
    .locals 2

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Lp5/b;

    const-string v0, "virus_cue_popup_back_btn"

    const-string v1, "button"

    invoke-direct {p1, v0, v1, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p1}, Lp5/f;->c()Z

    invoke-direct {p0}, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->I2()V

    return-void
.end method

.method private final p3()V
    .locals 4

    iget-object v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->x0:Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;

    if-eqz v0, :cond_0

    new-instance v1, Ld6/l0;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Ld6/l0;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {p0}, Lm5/z0;->q1()Lcom/miui/packageInstaller/model/ApkInfo;

    move-result-object v2

    new-instance v3, Lm5/l1;

    invoke-direct {v3, p0}, Lm5/l1;-><init>(Lcom/miui/packageInstaller/PurePackageInstallerActivity;)V

    invoke-virtual {v1, v2, v0, v3}, Ld6/l0;->e(Lcom/miui/packageInstaller/model/ApkInfo;Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.method private static final q3(Lcom/miui/packageInstaller/PurePackageInstallerActivity;)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->U2()V

    return-void
.end method

.method public static synthetic v2(Lcom/miui/packageInstaller/PurePackageInstallerActivity;)V
    .locals 0

    invoke-static {p0}, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->q3(Lcom/miui/packageInstaller/PurePackageInstallerActivity;)V

    return-void
.end method

.method public static synthetic w2(Lcom/miui/packageInstaller/PurePackageInstallerActivity;Landroid/content/DialogInterface;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->o3(Lcom/miui/packageInstaller/PurePackageInstallerActivity;Landroid/content/DialogInterface;)V

    return-void
.end method

.method public static synthetic x2(Lcom/miui/packageInstaller/PurePackageInstallerActivity;Landroid/content/DialogInterface;I)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->k3(Lcom/miui/packageInstaller/PurePackageInstallerActivity;Landroid/content/DialogInterface;I)V

    return-void
.end method

.method public static synthetic y2(Lcom/miui/packageInstaller/PurePackageInstallerActivity;Landroid/content/Context;ILjava/lang/Object;Lm6/a;)V
    .locals 0

    invoke-static {p0, p1, p2, p3, p4}, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->P2(Lcom/miui/packageInstaller/PurePackageInstallerActivity;Landroid/content/Context;ILjava/lang/Object;Lm6/a;)V

    return-void
.end method

.method public static synthetic z2()V
    .locals 0

    invoke-static {}, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->H2()V

    return-void
.end method


# virtual methods
.method public D1()V
    .locals 3

    invoke-super {p0}, Lm5/z0;->D1()V

    invoke-virtual {p0}, Lm5/z0;->k1()Ll6/d;

    move-result-object v0

    new-instance v1, Lm5/o1;

    invoke-direct {v1, p0}, Lm5/o1;-><init>(Lcom/miui/packageInstaller/PurePackageInstallerActivity;)V

    const v2, 0x7f0a02c5

    invoke-virtual {v0, v2, v1}, Ll6/d;->b(ILl6/e;)V

    invoke-virtual {p0}, Lm5/z0;->k1()Ll6/d;

    move-result-object v0

    new-instance v1, Lm5/n1;

    invoke-direct {v1, p0}, Lm5/n1;-><init>(Lcom/miui/packageInstaller/PurePackageInstallerActivity;)V

    const v2, 0x7f0a02a6

    invoke-virtual {v0, v2, v1}, Ll6/d;->b(ILl6/e;)V

    return-void
.end method

.method public F1()V
    .locals 7

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0d0029

    goto :goto_0

    :cond_0
    const v0, 0x7f0d0028

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/android/packageinstaller/utils/g;->v()Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f0d0027

    goto :goto_0

    :cond_2
    const v0, 0x7f0d0026

    :goto_0
    invoke-virtual {p0, v0}, Lmiuix/appcompat/app/j;->setContentView(I)V

    invoke-super {p0}, Lm5/z0;->F1()V

    invoke-static {}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->calculateSecurityModeProtectDays()I

    move-result v0

    iput v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->u0:I

    const v0, 0x7f0a0384

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    const-string v1, "null cannot be cast to non-null type androidx.constraintlayout.widget.ConstraintLayout.LayoutParams"

    invoke-static {v0, v1}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroidx/constraintlayout/widget/ConstraintLayout$b;

    invoke-static {p0}, Lcom/android/packageinstaller/utils/u;->b(Landroid/app/Activity;)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    const v0, 0x7f0a0200

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->l0:Landroid/view/ViewGroup;

    const v0, 0x7f0a0217

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->m0:Landroid/view/ViewGroup;

    const v0, 0x7f0a03c5

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->n0:Landroid/widget/TextView;

    const v0, 0x7f0a03c6

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->o0:Landroid/widget/TextView;

    const v0, 0x7f0a0173

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->p0:Landroid/widget/FrameLayout;

    const v0, 0x7f0a0204

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->j0:Landroid/widget/LinearLayout;

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->v()Z

    move-result v0

    if-nez v0, :cond_3

    const v0, 0x7f0a01c9

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->q0:Landroid/widget/ImageView;

    :cond_3
    const v0, 0x7f0a0205

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->k0:Landroid/widget/TextView;

    const v0, 0x7f0a0216

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.main_content)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    iput-object v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->W:Landroidx/recyclerview/widget/RecyclerView;

    const v0, 0x7f0a0206

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->g0:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    if-eqz v0, :cond_4

    const v2, 0x7f0a0207

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_1

    :cond_4
    move-object v0, v1

    :goto_1
    const/4 v2, 0x1

    if-nez v0, :cond_5

    goto :goto_2

    :cond_5
    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->setAutoMirrored(Z)V

    :goto_2
    invoke-static {}, Lcom/android/packageinstaller/utils/g;->v()Z

    move-result v0

    if-nez v0, :cond_6

    const v0, 0x7f0a020d

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/airbnb/lottie/LottieAnimationView;

    iput-object v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->i0:Lcom/airbnb/lottie/LottieAnimationView;

    :cond_6
    const v0, 0x7f0a037f

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v3, "findViewById(R.id.tips)"

    invoke-static {v0, v3}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->h0:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->N2()V

    invoke-static {p0}, Lcom/android/packageinstaller/utils/g;->t(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->i0:Lcom/airbnb/lottie/LottieAnimationView;

    if-eqz v0, :cond_8

    const-string v3, "dark_safe_mode_loading.json"

    goto :goto_3

    :cond_7
    iget-object v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->i0:Lcom/airbnb/lottie/LottieAnimationView;

    if-eqz v0, :cond_8

    const-string v3, "safe_loading.json"

    :goto_3
    invoke-virtual {v0, v3}, Lcom/airbnb/lottie/LottieAnimationView;->setAnimation(Ljava/lang/String;)V

    :cond_8
    iget-object v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->i0:Lcom/airbnb/lottie/LottieAnimationView;

    if-nez v0, :cond_9

    goto :goto_4

    :cond_9
    invoke-virtual {v0, v2}, Lcom/airbnb/lottie/LottieAnimationView;->setRepeatMode(I)V

    :goto_4
    iget-object v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->i0:Lcom/airbnb/lottie/LottieAnimationView;

    if-nez v0, :cond_a

    goto :goto_5

    :cond_a
    const/4 v3, -0x1

    invoke-virtual {v0, v3}, Lcom/airbnb/lottie/LottieAnimationView;->setRepeatCount(I)V

    :goto_5
    iget-object v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->W:Landroidx/recyclerview/widget/RecyclerView;

    const-string v3, "mMainRecyclerView"

    if-nez v0, :cond_b

    invoke-static {v3}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_b
    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setItemAnimator(Landroidx/recyclerview/widget/RecyclerView$l;)V

    const v0, 0x7f0a01b0

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;

    iput-object v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->f0:Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;

    iget-object v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->W:Landroidx/recyclerview/widget/RecyclerView;

    if-nez v0, :cond_c

    invoke-static {v3}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_c
    const/4 v4, 0x2

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setOverScrollMode(I)V

    iget-object v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->W:Landroidx/recyclerview/widget/RecyclerView;

    if-nez v0, :cond_d

    invoke-static {v3}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_d
    new-instance v5, Landroidx/recyclerview/widget/LinearLayoutManager;

    const/4 v6, 0x0

    invoke-direct {v5, p0, v2, v6}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    invoke-virtual {v0, v5}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$o;)V

    new-instance v0, Lj6/b;

    iget-object v5, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->W:Landroidx/recyclerview/widget/RecyclerView;

    if-nez v5, :cond_e

    invoke-static {v3}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v5, v1

    :cond_e
    invoke-direct {v0, v5}, Lj6/b;-><init>(Landroidx/recyclerview/widget/RecyclerView;)V

    iput-object v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->Y:Lj6/b;

    const v0, 0x7f0a03f2

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    iput-object v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->X:Landroidx/recyclerview/widget/RecyclerView;

    if-nez v0, :cond_f

    goto :goto_6

    :cond_f
    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setOverScrollMode(I)V

    :goto_6
    iget-object v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->X:Landroidx/recyclerview/widget/RecyclerView;

    if-nez v0, :cond_10

    goto :goto_7

    :cond_10
    new-instance v3, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-direct {v3, p0, v2, v6}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    invoke-virtual {v0, v3}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$o;)V

    :goto_7
    iget-object v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->X:Landroidx/recyclerview/widget/RecyclerView;

    if-nez v0, :cond_11

    goto :goto_8

    :cond_11
    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setItemAnimator(Landroidx/recyclerview/widget/RecyclerView$l;)V

    :goto_8
    new-instance v0, Lj6/b;

    iget-object v1, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->X:Landroidx/recyclerview/widget/RecyclerView;

    invoke-direct {v0, v1}, Lj6/b;-><init>(Landroidx/recyclerview/widget/RecyclerView;)V

    iput-object v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->Z:Lj6/b;

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.miui.packageinstaller.INSTALL_PROGRESS_START_SUCCESS"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const-string v1, "com.action.safe_mode_open"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    new-instance v1, Lcom/miui/packageInstaller/PurePackageInstallerActivity$a;

    invoke-direct {v1, p0}, Lcom/miui/packageInstaller/PurePackageInstallerActivity$a;-><init>(Lcom/miui/packageInstaller/PurePackageInstallerActivity;)V

    iput-object v1, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->r0:Lcom/miui/packageInstaller/PurePackageInstallerActivity$a;

    invoke-virtual {p0, v1, v0}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.action.once_Authorize"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    new-instance v1, Lcom/miui/packageInstaller/PurePackageInstallerActivity$b;

    invoke-direct {v1, p0}, Lcom/miui/packageInstaller/PurePackageInstallerActivity$b;-><init>(Lcom/miui/packageInstaller/PurePackageInstallerActivity;)V

    iput-object v1, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->t0:Lcom/miui/packageInstaller/PurePackageInstallerActivity$b;

    invoke-virtual {p0, v1, v0}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-direct {p0}, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->h3()V

    invoke-virtual {p0}, Lm5/z0;->P1()V

    return-void
.end method

.method public final J2(Landroid/widget/TextView;)V
    .locals 1

    invoke-direct {p0}, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->R2()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->f3(Landroid/widget/TextView;)V

    :cond_0
    return-void
.end method

.method public final M2()V
    .locals 12

    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-boolean v0, v0, Lcom/miui/packageInstaller/model/CloudParams;->allowHighLight:Z

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_0
    invoke-virtual {p0, v0}, Lm5/z0;->c2(Z)V

    iget-object v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->f0:Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;

    if-eqz v0, :cond_f

    invoke-virtual {p0, v0}, Lm5/z0;->O1(Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;)V

    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v2

    const/4 v3, 0x0

    if-eqz v2, :cond_1

    iget-object v2, v2, Lcom/miui/packageInstaller/model/CloudParams;->categoryAbbreviation:Ljava/lang/String;

    goto :goto_1

    :cond_1
    move-object v2, v3

    :goto_1
    const-string v4, "500_error"

    invoke-static {v2, v4}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    const/16 v4, 0x8

    if-eqz v2, :cond_2

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    return-void

    :cond_2
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v2

    const/4 v5, 0x1

    if-eqz v2, :cond_3

    iget-boolean v2, v2, Lcom/miui/packageInstaller/model/CloudParams;->useSystemAppRules:Z

    if-ne v2, v5, :cond_3

    move v2, v5

    goto :goto_2

    :cond_3
    move v2, v1

    :goto_2
    if-eqz v2, :cond_5

    invoke-virtual {p0, v0}, Lm5/z0;->u2(Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;)V

    invoke-virtual {v0}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->getMOnceAuthorizeBtn()Landroid/widget/TextView;

    move-result-object v0

    if-nez v0, :cond_4

    goto/16 :goto_5

    :cond_4
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_5

    :cond_5
    iget-object v2, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->x0:Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;->getAuthorized()Z

    move-result v2

    if-ne v2, v5, :cond_6

    move v2, v5

    goto :goto_3

    :cond_6
    move v2, v1

    :goto_3
    if-eqz v2, :cond_a

    invoke-virtual {v0}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->getMFirstButton()Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;

    move-result-object v7

    if-eqz v7, :cond_7

    const v1, 0x7f110381

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v8

    const-string v1, "getString(R.string.start_install)"

    invoke-static {v8, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v9, 0x0

    const/4 v10, 0x4

    const/4 v11, 0x0

    move-object v6, p0

    invoke-static/range {v6 .. v11}, Lm5/z0;->p2(Lm5/z0;Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;Ljava/lang/String;ZILjava/lang/Object;)V

    :cond_7
    invoke-virtual {v0}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->getMSecondButton()Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;

    move-result-object v1

    if-eqz v1, :cond_8

    const/4 v2, 0x2

    invoke-static {p0, v1, v3, v2, v3}, Lm5/z0;->k2(Lm5/z0;Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;Ljava/lang/CharSequence;ILjava/lang/Object;)V

    :cond_8
    invoke-virtual {v0}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->getMThirdButton()Lcom/miui/packageInstaller/view/InstallerActionButton;

    move-result-object v0

    if-nez v0, :cond_9

    goto :goto_5

    :cond_9
    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_5

    :cond_a
    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v2

    if-eqz v2, :cond_b

    iget-boolean v2, v2, Lcom/miui/packageInstaller/model/CloudParams;->installNotAllow:Z

    if-ne v2, v5, :cond_b

    move v2, v5

    goto :goto_4

    :cond_b
    move v2, v1

    :goto_4
    if-eqz v2, :cond_c

    invoke-virtual {p0, v0, v3}, Lm5/z0;->J1(Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;)V

    goto :goto_5

    :cond_c
    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v2

    if-eqz v2, :cond_d

    iget-boolean v2, v2, Lcom/miui/packageInstaller/model/CloudParams;->storeListed:Z

    if-ne v2, v5, :cond_d

    move v1, v5

    :cond_d
    if-nez v1, :cond_e

    invoke-direct {p0, v0, v3}, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->T2(Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;)V

    goto :goto_5

    :cond_e
    invoke-virtual {p0, v0, v3, v5}, Lm5/z0;->B1(Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;Z)Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;

    :cond_f
    :goto_5
    return-void
.end method

.method public Y0(Landroid/content/Intent;)V
    .locals 3

    const-string v0, "intent"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    iget-boolean v0, v0, Lcom/miui/packageInstaller/model/CloudParams;->bundleApp:Z

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    move v1, v2

    :goto_0
    if-eqz v1, :cond_1

    const/4 v0, 0x2

    iput v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->v0:I

    :cond_1
    iget v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->v0:I

    const-string v1, "app_type_level"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-super {p0, p1}, Lm5/z0;->Y0(Landroid/content/Intent;)V

    return-void
.end method

.method public Y1(Lcom/miui/packageInstaller/model/ApkInfo;I)V
    .locals 2

    invoke-super {p0, p1, p2}, Lm5/z0;->Y1(Lcom/miui/packageInstaller/model/ApkInfo;I)V

    sget-object p1, Ld6/l0;->c:Ld6/l0$a;

    invoke-virtual {p1}, Ld6/l0$a;->c()Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;

    move-result-object p2

    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;->isValid()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p2}, Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;->getPackageMd5()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lm5/z0;->q1()Lcom/miui/packageInstaller/model/ApkInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/ApkInfo;->getApkMd5()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-static {v0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object p2, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->x0:Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;

    goto :goto_1

    :cond_1
    invoke-virtual {p1}, Ld6/l0$a;->a()V

    :cond_2
    :goto_1
    return-void
.end method

.method public a2()V
    .locals 5

    new-instance v0, Lt5/a0;

    invoke-direct {v0, p0}, Lt5/a0;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lq2/b;->q:Lo5/b;

    invoke-virtual {v1}, Lo5/b;->d()V

    invoke-static {p0}, Lm2/b;->g(Landroid/content/Context;)Lm2/b;

    move-result-object v1

    invoke-virtual {v1}, Lm2/b;->h()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    iget-object v1, v1, Lcom/miui/packageInstaller/model/CloudParams;->rc:Lcom/miui/packageInstaller/model/RiskControlConfig;

    goto :goto_0

    :cond_0
    move-object v1, v2

    :goto_0
    if-eqz v1, :cond_2

    new-instance v1, Lt5/j0;

    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v3

    if-eqz v3, :cond_1

    iget-object v2, v3, Lcom/miui/packageInstaller/model/CloudParams;->rc:Lcom/miui/packageInstaller/model/RiskControlConfig;

    :cond_1
    invoke-static {v2}, Lm8/i;->c(Ljava/lang/Object;)V

    invoke-virtual {p0}, Lm5/z0;->q1()Lcom/miui/packageInstaller/model/ApkInfo;

    move-result-object v3

    invoke-virtual {p0}, Lm5/z0;->r1()Lm5/e;

    move-result-object v4

    invoke-direct {v1, p0, v2, v3, v4}, Lt5/j0;-><init>(Lq2/b;Lcom/miui/packageInstaller/model/RiskControlConfig;Lcom/miui/packageInstaller/model/ApkInfo;Lm5/e;)V

    invoke-virtual {v0, v1}, Lt5/a0;->c(Lt5/g0;)V

    :cond_2
    new-instance v1, Lcom/miui/packageInstaller/PurePackageInstallerActivity$c;

    invoke-direct {v1, p0}, Lcom/miui/packageInstaller/PurePackageInstallerActivity$c;-><init>(Lcom/miui/packageInstaller/PurePackageInstallerActivity;)V

    invoke-virtual {v0, v1}, Lt5/a0;->a(Lt5/g0$a;)V

    return-void
.end method

.method public final b3()V
    .locals 5

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->w0:J

    invoke-virtual {p0}, Lm5/z0;->x1()Lcom/miui/packageInstaller/model/Virus;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f1102ca

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {p0}, Lm5/z0;->x1()Lcom/miui/packageInstaller/model/Virus;

    move-result-object v4

    invoke-static {v4}, Lm8/i;->c(Ljava/lang/Object;)V

    iget-object v4, v4, Lcom/miui/packageInstaller/model/Virus;->name:Ljava/lang/String;

    aput-object v4, v3, v1

    invoke-virtual {p0, v2, v3}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lm5/z0;->x1()Lcom/miui/packageInstaller/model/Virus;

    move-result-object v1

    invoke-static {v1}, Lm8/i;->c(Ljava/lang/Object;)V

    iget-object v1, v1, Lcom/miui/packageInstaller/model/Virus;->virusInfo:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->m3(Ljava/lang/String;)V

    goto :goto_4

    :cond_0
    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v0

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    iget-object v0, v0, Lcom/miui/packageInstaller/model/CloudParams;->safeType:Ljava/lang/String;

    goto :goto_1

    :cond_1
    move-object v0, v2

    :goto_1
    const-string v3, "unknown"

    invoke-static {v0, v3}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    invoke-direct {p0}, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->i3()V

    goto :goto_4

    :cond_3
    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, v0, Lcom/miui/packageInstaller/model/CloudParams;->secureWarningTip:Lcom/miui/packageInstaller/model/WarningCardInfo;

    if-eqz v0, :cond_4

    iget-object v0, v0, Lcom/miui/packageInstaller/model/WarningCardInfo;->title:Ljava/lang/String;

    goto :goto_2

    :cond_4
    move-object v0, v2

    :goto_2
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v3

    if-eqz v3, :cond_5

    iget-object v3, v3, Lcom/miui/packageInstaller/model/CloudParams;->secureWarningTip:Lcom/miui/packageInstaller/model/WarningCardInfo;

    if-eqz v3, :cond_5

    iget-object v3, v3, Lcom/miui/packageInstaller/model/WarningCardInfo;->title:Ljava/lang/String;

    goto :goto_3

    :cond_5
    move-object v3, v2

    :goto_3
    invoke-static {v3, v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;I)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const v3, 0xff1a

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v3

    if-eqz v3, :cond_6

    iget-object v3, v3, Lcom/miui/packageInstaller/model/CloudParams;->secureWarningTip:Lcom/miui/packageInstaller/model/WarningCardInfo;

    if-eqz v3, :cond_6

    iget-object v2, v3, Lcom/miui/packageInstaller/model/WarningCardInfo;->text:Ljava/lang/String;

    :cond_6
    invoke-static {v2, v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;I)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_0

    :goto_4
    return-void
.end method

.method public e1()V
    .locals 8

    invoke-virtual {p0}, Lm5/z0;->l1()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lm5/z0;->x1()Lcom/miui/packageInstaller/model/Virus;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->f0:Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->getMFirstButton()Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->b(Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;)V

    :cond_0
    iget-object v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->Y:Lj6/b;

    const/4 v1, 0x0

    if-nez v0, :cond_1

    const-string v0, "mAdapter"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_1
    invoke-virtual {v0}, Lj6/b;->R()Ljava/util/List;

    move-result-object v0

    const-string v2, "mAdapter.list"

    invoke-static {v0, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->Z:Lj6/b;

    if-nez v2, :cond_2

    const-string v2, "mWarningAdapter"

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v2, v1

    :cond_2
    invoke-virtual {v2}, Lj6/b;->R()Ljava/util/List;

    move-result-object v2

    const-string v3, "mWarningAdapter.list"

    invoke-static {v2, v3}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, v2}, Lb8/j;->y(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_3
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-eqz v2, :cond_11

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lm6/a;

    instance-of v5, v2, Lcom/miui/packageInstaller/ui/listcomponets/e0;

    if-eqz v5, :cond_5

    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v5

    if-eqz v5, :cond_4

    iget-object v5, v5, Lcom/miui/packageInstaller/model/CloudParams;->secureWarningTip:Lcom/miui/packageInstaller/model/WarningCardInfo;

    if-eqz v5, :cond_4

    iget-object v5, v5, Lcom/miui/packageInstaller/model/WarningCardInfo;->title:Ljava/lang/String;

    goto :goto_1

    :cond_4
    move-object v5, v1

    :goto_1
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_5

    invoke-virtual {p0}, Lm5/z0;->x1()Lcom/miui/packageInstaller/model/Virus;

    move-result-object v5

    if-nez v5, :cond_5

    move-object v5, v2

    check-cast v5, Lcom/miui/packageInstaller/ui/listcomponets/e0;

    invoke-interface {v5}, Lcom/miui/packageInstaller/ui/listcomponets/e0;->a()V

    :cond_5
    instance-of v5, v2, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject;

    if-eqz v5, :cond_8

    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v6

    if-eqz v6, :cond_6

    iget-object v6, v6, Lcom/miui/packageInstaller/model/CloudParams;->secureWarningTip:Lcom/miui/packageInstaller/model/WarningCardInfo;

    if-eqz v6, :cond_6

    iget-object v6, v6, Lcom/miui/packageInstaller/model/WarningCardInfo;->title:Ljava/lang/String;

    goto :goto_2

    :cond_6
    move-object v6, v1

    :goto_2
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_8

    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v6

    if-eqz v6, :cond_7

    iget-boolean v6, v6, Lcom/miui/packageInstaller/model/CloudParams;->storeListed:Z

    if-ne v6, v3, :cond_7

    move v4, v3

    :cond_7
    if-eqz v4, :cond_8

    move-object v4, v2

    check-cast v4, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject;

    invoke-virtual {v4}, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject;->A()Lcom/miui/packageInstaller/model/PureModeTip;

    move-result-object v6

    const/4 v7, 0x3

    invoke-virtual {v6, v7}, Lcom/miui/packageInstaller/model/PureModeTip;->setLevel(I)V

    invoke-virtual {v4}, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject;->a()V

    :cond_8
    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v4

    if-eqz v4, :cond_9

    iget-object v4, v4, Lcom/miui/packageInstaller/model/CloudParams;->safeType:Ljava/lang/String;

    goto :goto_3

    :cond_9
    move-object v4, v1

    :goto_3
    const-string v6, "risk_vpn"

    invoke-static {v4, v6}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_c

    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v4

    if-eqz v4, :cond_a

    iget-object v4, v4, Lcom/miui/packageInstaller/model/CloudParams;->safeType:Ljava/lang/String;

    goto :goto_4

    :cond_a
    move-object v4, v1

    :goto_4
    const-string v6, "risk_anti_spoofing"

    invoke-static {v4, v6}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_c

    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v4

    if-eqz v4, :cond_b

    iget-object v4, v4, Lcom/miui/packageInstaller/model/CloudParams;->safeType:Ljava/lang/String;

    goto :goto_5

    :cond_b
    move-object v4, v1

    :goto_5
    const-string v6, "risk_c_level"

    invoke-static {v4, v6}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    :cond_c
    if-eqz v5, :cond_d

    move-object v4, v2

    check-cast v4, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject;

    invoke-virtual {v4}, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject;->A()Lcom/miui/packageInstaller/model/PureModeTip;

    move-result-object v6

    const/4 v7, 0x4

    invoke-virtual {v6, v7}, Lcom/miui/packageInstaller/model/PureModeTip;->setLevel(I)V

    invoke-virtual {v4}, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject;->a()V

    :cond_d
    invoke-virtual {p0}, Lm5/z0;->x1()Lcom/miui/packageInstaller/model/Virus;

    move-result-object v4

    if-eqz v4, :cond_10

    if-eqz v5, :cond_10

    new-instance v4, Lcom/miui/packageInstaller/model/PureModeTip;

    invoke-direct {v4}, Lcom/miui/packageInstaller/model/PureModeTip;-><init>()V

    invoke-virtual {p0}, Lm5/z0;->x1()Lcom/miui/packageInstaller/model/Virus;

    move-result-object v6

    invoke-static {v6}, Lm8/i;->c(Ljava/lang/Object;)V

    iget-object v6, v6, Lcom/miui/packageInstaller/model/Virus;->name:Ljava/lang/String;

    const-string v7, ""

    if-nez v6, :cond_e

    move-object v6, v7

    :cond_e
    invoke-virtual {v4, v6}, Lcom/miui/packageInstaller/model/PureModeTip;->setTitle(Ljava/lang/String;)V

    invoke-virtual {p0}, Lm5/z0;->x1()Lcom/miui/packageInstaller/model/Virus;

    move-result-object v6

    invoke-static {v6}, Lm8/i;->c(Ljava/lang/Object;)V

    iget-object v6, v6, Lcom/miui/packageInstaller/model/Virus;->virusInfo:Ljava/lang/String;

    if-nez v6, :cond_f

    goto :goto_6

    :cond_f
    move-object v7, v6

    :goto_6
    invoke-virtual {v4, v7}, Lcom/miui/packageInstaller/model/PureModeTip;->setMessage(Ljava/lang/String;)V

    const/4 v6, 0x2

    invoke-virtual {v4, v6}, Lcom/miui/packageInstaller/model/PureModeTip;->setLevel(I)V

    move-object v6, v2

    check-cast v6, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject;

    invoke-virtual {v6, v4}, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject;->E(Lcom/miui/packageInstaller/model/PureModeTip;)V

    invoke-virtual {v6}, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject;->a()V

    :cond_10
    invoke-direct {p0}, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->Q2()Z

    move-result v4

    if-eqz v4, :cond_3

    if-eqz v5, :cond_3

    check-cast v2, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject;

    invoke-virtual {v2, v3}, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject;->B(Z)V

    invoke-virtual {v2}, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject;->a()V

    goto/16 :goto_0

    :cond_11
    invoke-direct {p0}, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->L2()V

    invoke-virtual {p0}, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->g3()V

    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v0

    if-eqz v0, :cond_12

    iget-object v0, v0, Lcom/miui/packageInstaller/model/CloudParams;->testConfig:Lcom/miui/packageInstaller/model/InterceptAbTestConfig;

    goto :goto_7

    :cond_12
    move-object v0, v1

    :goto_7
    if-eqz v0, :cond_15

    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v0

    if-eqz v0, :cond_13

    iget-object v0, v0, Lcom/miui/packageInstaller/model/CloudParams;->testConfig:Lcom/miui/packageInstaller/model/InterceptAbTestConfig;

    if-eqz v0, :cond_13

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/InterceptAbTestConfig;->getSingleAuthStyle()I

    move-result v0

    if-nez v0, :cond_13

    move v0, v3

    goto :goto_8

    :cond_13
    move v0, v4

    :goto_8
    if-nez v0, :cond_15

    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v0

    if-eqz v0, :cond_14

    iget-object v0, v0, Lcom/miui/packageInstaller/model/CloudParams;->testConfig:Lcom/miui/packageInstaller/model/InterceptAbTestConfig;

    if-eqz v0, :cond_14

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/InterceptAbTestConfig;->getSingleAuthStyle()I

    move-result v0

    if-ne v0, v3, :cond_14

    goto :goto_9

    :cond_14
    move v3, v4

    :goto_9
    if-eqz v3, :cond_17

    :cond_15
    iget-object v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->f0:Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;

    if-eqz v0, :cond_16

    invoke-virtual {v0}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->getMOnceAuthorizeBtn()Landroid/widget/TextView;

    move-result-object v1

    :cond_16
    invoke-virtual {p0, v1}, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->J2(Landroid/widget/TextView;)V

    :cond_17
    iget-object v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->f0:Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;

    if-eqz v0, :cond_18

    invoke-virtual {v0}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->getMFirstButton()Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;

    move-result-object v0

    if-eqz v0, :cond_18

    invoke-interface {v0, v4}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;->setProgressVisibility(Z)V

    :cond_18
    iget-object v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->f0:Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;

    if-eqz v0, :cond_19

    invoke-virtual {v0}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar;->getMSecondButton()Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;

    move-result-object v0

    if-eqz v0, :cond_19

    invoke-interface {v0, v4}, Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;->setProgressVisibility(Z)V

    :cond_19
    return-void
.end method

.method public final g3()V
    .locals 9

    iget-object v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->q0:Landroid/widget/ImageView;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_0
    iget-object v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->o0:Landroid/widget/TextView;

    const/4 v2, 0x1

    if-nez v0, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f0017

    iget v5, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->u0:I

    new-array v6, v2, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v1

    invoke-virtual {v3, v4, v5, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-boolean v0, v0, Lcom/miui/packageInstaller/model/CloudParams;->storeListed:Z

    if-ne v0, v2, :cond_2

    move v0, v2

    goto :goto_2

    :cond_2
    move v0, v1

    :goto_2
    const v3, 0x7f0805a4

    const v4, 0x7f0805a3

    const v5, 0x7f0805a2

    const v6, 0x7f0805a1

    const/4 v7, 0x0

    if-nez v0, :cond_1b

    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-boolean v0, v0, Lcom/miui/packageInstaller/model/CloudParams;->useSystemAppRules:Z

    if-ne v0, v2, :cond_3

    move v0, v2

    goto :goto_3

    :cond_3
    move v0, v1

    :goto_3
    if-eqz v0, :cond_4

    goto/16 :goto_13

    :cond_4
    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-boolean v0, v0, Lcom/miui/packageInstaller/model/CloudParams;->storeListed:Z

    if-nez v0, :cond_5

    move v1, v2

    :cond_5
    const-string v0, "risk_c_level"

    const-string v2, "risk_anti_spoofing"

    const-string v8, "risk_vpn"

    if-eqz v1, :cond_c

    invoke-virtual {p0}, Lm5/z0;->x1()Lcom/miui/packageInstaller/model/Virus;

    move-result-object v1

    if-nez v1, :cond_c

    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v1

    if-eqz v1, :cond_6

    iget-object v1, v1, Lcom/miui/packageInstaller/model/CloudParams;->safeType:Ljava/lang/String;

    goto :goto_4

    :cond_6
    move-object v1, v7

    :goto_4
    invoke-static {v1, v8}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v1

    if-eqz v1, :cond_7

    iget-object v1, v1, Lcom/miui/packageInstaller/model/CloudParams;->safeType:Ljava/lang/String;

    goto :goto_5

    :cond_7
    move-object v1, v7

    :goto_5
    invoke-static {v1, v2}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v1

    if-eqz v1, :cond_8

    iget-object v1, v1, Lcom/miui/packageInstaller/model/CloudParams;->safeType:Ljava/lang/String;

    goto :goto_6

    :cond_8
    move-object v1, v7

    :goto_6
    invoke-static {v1, v0}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    iget-object v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->n0:Landroid/widget/TextView;

    if-nez v0, :cond_9

    goto :goto_7

    :cond_9
    const v1, 0x7f11005d

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_7
    iget-object v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->p0:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_a

    const v1, 0x7f080533

    const v2, 0x7f080534

    invoke-direct {p0, v1, v2}, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->K2(II)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    :cond_a
    iget-object v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->q0:Landroid/widget/ImageView;

    if-eqz v0, :cond_b

    const v1, 0x7f080535

    const v2, 0x7f080536

    invoke-direct {p0, v1, v2}, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->K2(II)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_b
    const/4 v0, 0x2

    iput v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->v0:I

    goto/16 :goto_12

    :cond_c
    invoke-virtual {p0}, Lm5/z0;->x1()Lcom/miui/packageInstaller/model/Virus;

    move-result-object v1

    if-nez v1, :cond_10

    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v1

    if-eqz v1, :cond_d

    iget-object v1, v1, Lcom/miui/packageInstaller/model/CloudParams;->safeType:Ljava/lang/String;

    goto :goto_8

    :cond_d
    move-object v1, v7

    :goto_8
    invoke-static {v1, v8}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_10

    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v1

    if-eqz v1, :cond_e

    iget-object v1, v1, Lcom/miui/packageInstaller/model/CloudParams;->safeType:Ljava/lang/String;

    goto :goto_9

    :cond_e
    move-object v1, v7

    :goto_9
    invoke-static {v1, v2}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_10

    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v1

    if-eqz v1, :cond_f

    iget-object v1, v1, Lcom/miui/packageInstaller/model/CloudParams;->safeType:Ljava/lang/String;

    goto :goto_a

    :cond_f
    move-object v1, v7

    :goto_a
    invoke-static {v1, v0}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_29

    :cond_10
    invoke-virtual {p0}, Lm5/z0;->x1()Lcom/miui/packageInstaller/model/Virus;

    move-result-object v0

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->n0:Landroid/widget/TextView;

    if-nez v0, :cond_11

    goto :goto_b

    :cond_11
    const v1, 0x7f11005c

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_b
    const/4 v0, 0x6

    :goto_c
    iput v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->v0:I

    goto :goto_11

    :cond_12
    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v0

    if-eqz v0, :cond_13

    iget-object v0, v0, Lcom/miui/packageInstaller/model/CloudParams;->safeType:Ljava/lang/String;

    goto :goto_d

    :cond_13
    move-object v0, v7

    :goto_d
    invoke-static {v0, v8}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_17

    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v0

    if-eqz v0, :cond_14

    iget-object v7, v0, Lcom/miui/packageInstaller/model/CloudParams;->safeType:Ljava/lang/String;

    :cond_14
    invoke-static {v7, v2}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    goto :goto_f

    :cond_15
    iget-object v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->n0:Landroid/widget/TextView;

    if-nez v0, :cond_16

    goto :goto_e

    :cond_16
    const v1, 0x7f1100c2

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_e
    const/4 v0, 0x3

    goto :goto_c

    :cond_17
    :goto_f
    iget-object v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->n0:Landroid/widget/TextView;

    if-nez v0, :cond_18

    goto :goto_10

    :cond_18
    const v1, 0x7f11005b

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_10
    const/4 v0, 0x5

    goto :goto_c

    :goto_11
    iget-object v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->p0:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_19

    invoke-direct {p0, v6, v5}, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->K2(II)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    :cond_19
    iget-object v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->q0:Landroid/widget/ImageView;

    if-eqz v0, :cond_1a

    invoke-direct {p0, v4, v3}, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->K2(II)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_1a
    :goto_12
    invoke-direct {p0}, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->G2()V

    goto/16 :goto_18

    :cond_1b
    :goto_13
    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v0

    if-eqz v0, :cond_1c

    iget-boolean v0, v0, Lcom/miui/packageInstaller/model/CloudParams;->useSystemAppRules:Z

    if-nez v0, :cond_1c

    move v0, v2

    goto :goto_14

    :cond_1c
    move v0, v1

    :goto_14
    if-eqz v0, :cond_22

    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v0

    if-eqz v0, :cond_1d

    iget-object v0, v0, Lcom/miui/packageInstaller/model/CloudParams;->secureWarningTip:Lcom/miui/packageInstaller/model/WarningCardInfo;

    if-eqz v0, :cond_1d

    iget-object v7, v0, Lcom/miui/packageInstaller/model/WarningCardInfo;->title:Ljava/lang/String;

    :cond_1d
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1e

    invoke-virtual {p0}, Lm5/z0;->x1()Lcom/miui/packageInstaller/model/Virus;

    move-result-object v0

    if-eqz v0, :cond_22

    :cond_1e
    iget-object v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->n0:Landroid/widget/TextView;

    if-nez v0, :cond_1f

    goto :goto_15

    :cond_1f
    const v1, 0x7f110042

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_15
    iget-object v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->p0:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_20

    invoke-direct {p0, v6, v5}, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->K2(II)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    :cond_20
    iget-object v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->q0:Landroid/widget/ImageView;

    if-eqz v0, :cond_21

    invoke-direct {p0, v4, v3}, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->K2(II)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_21
    const/4 v0, 0x4

    iput v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->v0:I

    goto :goto_18

    :cond_22
    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v0

    if-eqz v0, :cond_23

    iget-boolean v0, v0, Lcom/miui/packageInstaller/model/CloudParams;->useSystemAppRules:Z

    if-ne v0, v2, :cond_23

    move v1, v2

    :cond_23
    iget-object v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->n0:Landroid/widget/TextView;

    if-eqz v1, :cond_25

    if-nez v0, :cond_24

    goto :goto_17

    :cond_24
    const v1, 0x7f110263

    goto :goto_16

    :cond_25
    if-nez v0, :cond_26

    goto :goto_17

    :cond_26
    const v1, 0x7f110254

    :goto_16
    invoke-virtual {p0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_17
    iget-object v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->p0:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_27

    const v1, 0x7f080574

    const v3, 0x7f080577

    invoke-direct {p0, v1, v3}, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->K2(II)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    :cond_27
    iget-object v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->q0:Landroid/widget/ImageView;

    if-eqz v0, :cond_28

    const v1, 0x7f080575

    const v3, 0x7f080576

    invoke-direct {p0, v1, v3}, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->K2(II)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_28
    iput v2, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->v0:I

    :cond_29
    :goto_18
    return-void
.end method

.method public i1()V
    .locals 1

    const/4 v0, 0x1

    iput v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->v0:I

    invoke-super {p0}, Lm5/z0;->i1()V

    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    const-string v0, "newConfig"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lm5/z0;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    invoke-direct {p0}, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->N2()V

    return-void
.end method

.method protected onDestroy()V
    .locals 1

    invoke-super {p0}, Lm5/z0;->onDestroy()V

    iget-object v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->r0:Lcom/miui/packageInstaller/PurePackageInstallerActivity$a;

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_0
    iget-object v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->t0:Lcom/miui/packageInstaller/PurePackageInstallerActivity$b;

    if-eqz v0, :cond_1

    invoke-virtual {p0, v0}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_1
    iget-object v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->e0:La6/g;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    :cond_2
    iget-object v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->i0:Lcom/airbnb/lottie/LottieAnimationView;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/airbnb/lottie/LottieAnimationView;->h()V

    :cond_3
    return-void
.end method

.method public y(Ljava/util/List;Ljava/util/List;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lm6/a<",
            "*>;>;",
            "Ljava/util/List<",
            "Lm6/a<",
            "*>;>;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->V:Ljava/lang/String;

    const-string v1, "on on layout created"

    invoke-static {v0, v1}, Lf6/o;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->s0:Z

    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->L2()V

    :cond_0
    iget-object v1, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->x0:Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;->getAuthorized()Z

    move-result v1

    if-ne v1, v0, :cond_1

    move v1, v0

    goto :goto_0

    :cond_1
    move v1, v2

    :goto_0
    if-nez v1, :cond_2

    invoke-direct {p0}, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->c3()V

    goto :goto_1

    :cond_2
    invoke-direct {p0}, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->e3()V

    :goto_1
    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v3

    if-nez v3, :cond_3

    goto :goto_2

    :cond_3
    iput-boolean v1, v3, Lcom/miui/packageInstaller/model/CloudParams;->isSingletonOnce:Z

    :goto_2
    iget-object v1, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->Y:Lj6/b;

    const/4 v3, 0x0

    if-nez v1, :cond_4

    const-string v1, "mAdapter"

    invoke-static {v1}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v1, v3

    :cond_4
    invoke-virtual {v1, p1}, Lj6/b;->j0(Ljava/util/List;)V

    iget-object p1, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->Z:Lj6/b;

    if-nez p1, :cond_5

    const-string p1, "mWarningAdapter"

    invoke-static {p1}, Lm8/i;->s(Ljava/lang/String;)V

    move-object p1, v3

    :cond_5
    invoke-virtual {p1, p2}, Lj6/b;->j0(Ljava/util/List;)V

    invoke-virtual {p0}, Lm5/z0;->L1()Z

    move-result p1

    if-nez p1, :cond_e

    invoke-virtual {p0, v0}, Lm5/z0;->e2(Z)V

    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object p1

    if-eqz p1, :cond_6

    invoke-direct {p0}, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->S2()V

    goto :goto_4

    :cond_6
    iget-object p1, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->g0:Landroid/widget/FrameLayout;

    if-nez p1, :cond_7

    goto :goto_3

    :cond_7
    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    :goto_3
    iget-object p1, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->i0:Lcom/airbnb/lottie/LottieAnimationView;

    if-eqz p1, :cond_8

    invoke-virtual {p1}, Lcom/airbnb/lottie/LottieAnimationView;->h()V

    :cond_8
    :goto_4
    invoke-virtual {p0}, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->M2()V

    invoke-virtual {p0}, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->g3()V

    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object p1

    if-eqz p1, :cond_9

    iget-boolean p1, p1, Lcom/miui/packageInstaller/model/CloudParams;->storeListed:Z

    if-ne p1, v0, :cond_9

    move p1, v0

    goto :goto_5

    :cond_9
    move p1, v2

    :goto_5
    if-nez p1, :cond_b

    invoke-virtual {p0}, Lm5/z0;->s1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object p1

    if-eqz p1, :cond_a

    iget-boolean p1, p1, Lcom/miui/packageInstaller/model/CloudParams;->bundleApp:Z

    if-ne p1, v0, :cond_a

    move p1, v0

    goto :goto_6

    :cond_a
    move p1, v2

    :goto_6
    if-eqz p1, :cond_c

    :cond_b
    invoke-direct {p0}, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->L2()V

    :cond_c
    iget-object p1, p0, Lq2/b;->q:Lo5/b;

    invoke-virtual {p1}, Lo5/b;->v()Ljava/lang/String;

    move-result-object p1

    const-string p2, "install_start"

    invoke-static {p2, p1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_d

    invoke-virtual {p0, p2}, Lq2/b;->I0(Ljava/lang/String;)V

    sget-object v4, Ll2/a;->a:Ll2/a;

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {p0}, Lm5/z0;->r1()Lm5/e;

    move-result-object v9

    const/16 v10, 0xc

    const/4 v11, 0x0

    const-string v6, "install_start"

    move-object v5, p0

    invoke-static/range {v4 .. v11}, Ll2/a;->c(Ll2/a;Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lm5/e;ILjava/lang/Object;)V

    :cond_d
    invoke-virtual {p0}, Lm5/z0;->u1()Z

    move-result p1

    if-eqz p1, :cond_e

    invoke-virtual {p0, v0}, Lm5/z0;->i2(Z)V

    :cond_e
    invoke-virtual {p0}, Lm5/z0;->X1()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_11

    iget-object p2, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->h0:Landroid/widget/TextView;

    const-string v0, "securityTipsTextView"

    if-nez p2, :cond_f

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object p2, v3

    :cond_f
    invoke-virtual {p2, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object p2, p0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->h0:Landroid/widget/TextView;

    if-nez p2, :cond_10

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_7

    :cond_10
    move-object v3, p2

    :goto_7
    invoke-virtual {v3, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_11
    invoke-virtual {p0}, Lm5/z0;->M1()Z

    move-result p1

    if-eqz p1, :cond_12

    invoke-virtual {p0}, Lcom/miui/packageInstaller/PurePackageInstallerActivity;->e1()V

    :cond_12
    return-void
.end method

.method public y1()Landroid/content/Intent;
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/miui/packageInstaller/PureInstallProgressActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method
