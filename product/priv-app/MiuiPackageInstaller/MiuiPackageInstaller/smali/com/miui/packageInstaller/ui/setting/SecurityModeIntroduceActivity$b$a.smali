.class public final Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$b$a;
.super Landroid/content/BroadcastReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$b;)V
    .locals 1

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$b$a;->a:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    const-string v0, "context"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "intent"

    invoke-static {p2, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$b$a;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$b$a;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    invoke-static {p1}, Lm8/i;->c(Ljava/lang/Object;)V

    check-cast p1, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$b;

    const/4 v0, 0x0

    const-string v1, "secure_mode_verification"

    invoke-virtual {p2, v1, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result p2

    invoke-static {p1, p2}, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$b;->k2(Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$b;Z)V

    iget-object p1, p0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$b$a;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    invoke-static {p1}, Lm8/i;->c(Ljava/lang/Object;)V

    check-cast p1, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$b;

    invoke-static {p1}, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$b;->j2(Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$b;)V

    :cond_0
    return-void
.end method
