.class public final Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;
.super Lm6/a;

# interfaces
.implements Lcom/miui/packageInstaller/ui/listcomponets/v;
.implements Lk2/h;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lm6/a<",
        "Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;",
        ">;",
        "Lcom/miui/packageInstaller/ui/listcomponets/v;",
        "Lk2/h;"
    }
.end annotation


# instance fields
.field private final l:Lcom/miui/packageInstaller/model/ApkInfo;

.field private final m:Lcom/miui/packageInstaller/model/AdModel$DesData;

.field private n:Lcom/miui/packageInstaller/model/AdModel$DesData;

.field private o:Landroid/graphics/drawable/GradientDrawable;

.field private p:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lm6/a<",
            "*>;>;"
        }
    .end annotation
.end field

.field private q:Z

.field private r:Z

.field private final s:I

.field private t:Ljava/lang/String;

.field private final u:Landroid/view/View$OnClickListener;

.field private v:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/ApkInfo;Lcom/miui/packageInstaller/model/AdModel$DesData;Ll6/c;Lm6/b;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mData"

    invoke-static {p3, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p3, p4, p5}, Lm6/a;-><init>(Landroid/content/Context;Ljava/lang/Object;Ll6/c;Lm6/b;)V

    iput-object p2, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->l:Lcom/miui/packageInstaller/model/ApkInfo;

    iput-object p3, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->m:Lcom/miui/packageInstaller/model/AdModel$DesData;

    new-instance p2, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {p2}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    iput-object p2, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->o:Landroid/graphics/drawable/GradientDrawable;

    const-string p2, ""

    iput-object p2, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->t:Ljava/lang/String;

    const p2, 0x7f0604f2

    invoke-virtual {p1, p2}, Landroid/content/Context;->getColor(I)I

    move-result p2

    iput p2, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->s:I

    iput-object p3, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->n:Lcom/miui/packageInstaller/model/AdModel$DesData;

    move-object p2, p1

    check-cast p2, Lm5/s;

    invoke-interface {p2}, Lm5/s;->p()Lm5/e;

    move-result-object p2

    const/4 p3, 0x0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lm5/e;->j()Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    :cond_0
    move-object p2, p3

    :goto_0
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    iput-object p2, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->t:Ljava/lang/String;

    invoke-static {}, Lk2/e;->h()Lk2/e;

    move-result-object p2

    new-instance p4, Lk2/j;

    iget-object p5, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->n:Lcom/miui/packageInstaller/model/AdModel$DesData;

    if-eqz p5, :cond_1

    invoke-virtual {p5}, Lcom/miui/packageInstaller/model/AdModel$DesData;->getData()Lcom/miui/packageInstaller/model/AdData;

    move-result-object p5

    if-eqz p5, :cond_1

    invoke-virtual {p5}, Lcom/miui/packageInstaller/model/AdData;->getPackageName()Ljava/lang/String;

    move-result-object p3

    :cond_1
    invoke-direct {p4, p3, p0}, Lk2/j;-><init>(Ljava/lang/String;Lk2/h;)V

    invoke-virtual {p2, p4}, Lk2/e;->m(Lk2/h;)V

    new-instance p2, Lcom/miui/packageInstaller/ui/listcomponets/q;

    invoke-direct {p2, p1, p0}, Lcom/miui/packageInstaller/ui/listcomponets/q;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;)V

    iput-object p2, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->u:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/ApkInfo;Lcom/miui/packageInstaller/model/AdModel$DesData;Ll6/c;Lm6/b;ILm8/g;)V
    .locals 6

    and-int/lit8 p6, p6, 0x10

    if-eqz p6, :cond_0

    const/4 p5, 0x0

    :cond_0
    move-object v5, p5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/ApkInfo;Lcom/miui/packageInstaller/model/AdModel$DesData;Ll6/c;Lm6/b;)V

    return-void
.end method

.method public static synthetic A(Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->O(Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic B(Landroid/content/Context;Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->F(Landroid/content/Context;Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;Landroid/view/View;)V

    return-void
.end method

.method public static final synthetic C(Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;)Lcom/miui/packageInstaller/model/AdModel$DesData;
    .locals 0

    iget-object p0, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->n:Lcom/miui/packageInstaller/model/AdModel$DesData;

    return-object p0
.end method

.method public static final synthetic D(Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;)Lcom/miui/packageInstaller/model/ApkInfo;
    .locals 0

    iget-object p0, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->l:Lcom/miui/packageInstaller/model/ApkInfo;

    return-object p0
.end method

.method public static final synthetic E(Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;)Lm6/b;
    .locals 0

    iget-object p0, p0, Lm6/a;->c:Lm6/b;

    return-object p0
.end method

.method private static final F(Landroid/content/Context;Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;Landroid/view/View;)V
    .locals 8

    const-string v0, "$context"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "this$0"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/view/View;->getId()I

    move-result p2

    const v0, 0x7f0a01aa

    const/4 v1, 0x2

    const-string v2, "migamecenter:"

    const-string v3, "private"

    const-string v4, ""

    const-string v5, "CLICK"

    const/4 v6, 0x0

    const/4 v7, 0x0

    if-eq p2, v0, :cond_a

    const v0, 0x7f0a01c8

    if-eq p2, v0, :cond_0

    const v0, 0x7f0a01f2

    if-eq p2, v0, :cond_0

    goto/16 :goto_f

    :cond_0
    iget-object p2, p1, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->n:Lcom/miui/packageInstaller/model/AdModel$DesData;

    if-eqz p2, :cond_1

    invoke-virtual {p2}, Lcom/miui/packageInstaller/model/AdModel$DesData;->getData()Lcom/miui/packageInstaller/model/AdData;

    move-result-object p2

    if-eqz p2, :cond_1

    invoke-virtual {p2}, Lcom/miui/packageInstaller/model/AdData;->getPackageName()Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    :cond_1
    move-object p2, v7

    :goto_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p2

    if-nez p2, :cond_7

    :try_start_0
    iget-object p2, p1, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->n:Lcom/miui/packageInstaller/model/AdModel$DesData;

    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lcom/miui/packageInstaller/model/AdModel$DesData;->getData()Lcom/miui/packageInstaller/model/AdData;

    move-result-object p2

    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lcom/miui/packageInstaller/model/AdData;->getLandingPageUrl()Ljava/lang/String;

    move-result-object p2

    goto :goto_1

    :cond_2
    move-object p2, v7

    :goto_1
    if-eqz p2, :cond_5

    iget-object p2, p1, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->n:Lcom/miui/packageInstaller/model/AdModel$DesData;

    if-eqz p2, :cond_3

    invoke-virtual {p2}, Lcom/miui/packageInstaller/model/AdModel$DesData;->getData()Lcom/miui/packageInstaller/model/AdData;

    move-result-object p2

    if-eqz p2, :cond_3

    invoke-virtual {p2}, Lcom/miui/packageInstaller/model/AdData;->getLandingPageUrl()Ljava/lang/String;

    move-result-object p2

    goto :goto_2

    :cond_3
    move-object p2, v7

    :goto_2
    invoke-static {p2}, Lm8/i;->c(Ljava/lang/Object;)V

    invoke-static {p2, v2, v6, v1, v7}, Lu8/g;->v(Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_5

    iget-object p2, p1, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->n:Lcom/miui/packageInstaller/model/AdModel$DesData;

    if-eqz p2, :cond_4

    invoke-virtual {p2}, Lcom/miui/packageInstaller/model/AdModel$DesData;->getData()Lcom/miui/packageInstaller/model/AdData;

    move-result-object p2

    if-eqz p2, :cond_4

    invoke-virtual {p2}, Lcom/miui/packageInstaller/model/AdData;->getLandingPageUrl()Ljava/lang/String;

    move-result-object p2

    goto :goto_3

    :cond_4
    move-object p2, v7

    :goto_3
    invoke-static {p0, p2}, Lq2/d;->d(Landroid/content/Context;Ljava/lang/String;)Z

    goto :goto_6

    :cond_5
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p1, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->n:Lcom/miui/packageInstaller/model/AdModel$DesData;

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/AdModel$DesData;->getData()Lcom/miui/packageInstaller/model/AdData;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/AdData;->getLandingPageUrl()Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    :cond_6
    move-object v0, v7

    :goto_4
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "&sourcePackageChain="

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p1, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->t:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p0, p2}, Lcom/android/packageinstaller/utils/l;->a(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_6

    :catch_0
    move-exception p2

    invoke-virtual {p2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_6

    :cond_7
    iget-object p2, p1, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->n:Lcom/miui/packageInstaller/model/AdModel$DesData;

    if-eqz p2, :cond_8

    invoke-virtual {p2}, Lcom/miui/packageInstaller/model/AdModel$DesData;->getData()Lcom/miui/packageInstaller/model/AdData;

    move-result-object p2

    if-eqz p2, :cond_8

    invoke-virtual {p2}, Lcom/miui/packageInstaller/model/AdData;->getLandingPageUrl()Ljava/lang/String;

    move-result-object p2

    goto :goto_5

    :cond_8
    move-object p2, v7

    :goto_5
    invoke-static {p0, p2}, Lq2/d;->d(Landroid/content/Context;Ljava/lang/String;)Z

    :goto_6
    iget-object p1, p1, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->m:Lcom/miui/packageInstaller/model/AdModel$DesData;

    invoke-virtual {p1}, Lcom/miui/packageInstaller/model/AdModel$DesData;->getData()Lcom/miui/packageInstaller/model/AdData;

    move-result-object p1

    check-cast p0, Lm5/s;

    invoke-interface {p0, v3}, Lm5/s;->N(Ljava/lang/String;)Lo5/c;

    move-result-object p0

    if-eqz p0, :cond_9

    invoke-virtual {p0}, Lo5/c;->p()Ljava/lang/String;

    move-result-object v7

    :cond_9
    const-string p0, "layout"

    invoke-static {v5, p1, p0, v4, v7}, Lk2/a;->d(Ljava/lang/String;Lcom/miui/packageInstaller/model/AdInterface;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_f

    :cond_a
    iget-object p2, p1, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->n:Lcom/miui/packageInstaller/model/AdModel$DesData;

    if-eqz p2, :cond_b

    invoke-virtual {p2}, Lcom/miui/packageInstaller/model/AdModel$DesData;->getData()Lcom/miui/packageInstaller/model/AdData;

    move-result-object p2

    if-eqz p2, :cond_b

    invoke-virtual {p2}, Lcom/miui/packageInstaller/model/AdData;->getPackageName()Ljava/lang/String;

    move-result-object p2

    goto :goto_7

    :cond_b
    move-object p2, v7

    :goto_7
    invoke-static {p0, p2}, Lj2/f;->o(Landroid/content/Context;Ljava/lang/String;)Z

    move-result p2

    if-eqz p2, :cond_d

    iget-object p1, p1, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->n:Lcom/miui/packageInstaller/model/AdModel$DesData;

    if-eqz p1, :cond_c

    invoke-virtual {p1}, Lcom/miui/packageInstaller/model/AdModel$DesData;->getData()Lcom/miui/packageInstaller/model/AdData;

    move-result-object p1

    if-eqz p1, :cond_c

    invoke-virtual {p1}, Lcom/miui/packageInstaller/model/AdData;->getPackageName()Ljava/lang/String;

    move-result-object v7

    :cond_c
    invoke-static {p0, v7}, Lj2/f;->r(Landroid/content/Context;Ljava/lang/String;)V

    new-instance p1, Lp5/a;

    const/4 v1, 0x0

    const/4 v2, 0x0

    move-object v3, p0

    check-cast v3, Lo5/a;

    const/4 v4, 0x3

    const/4 v5, 0x0

    move-object v0, p1

    invoke-direct/range {v0 .. v5}, Lp5/a;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;ILm8/g;)V

    invoke-virtual {p1}, Lp5/f;->c()Z

    goto/16 :goto_f

    :cond_d
    invoke-static {p0}, Lcom/android/packageinstaller/utils/g;->w(Landroid/content/Context;)Z

    move-result p2

    if-nez p2, :cond_e

    const p1, 0x7f1103ba

    invoke-static {p0, p1, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object p0

    invoke-virtual {p0}, Landroid/widget/Toast;->show()V

    return-void

    :cond_e
    iget-object p2, p1, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->n:Lcom/miui/packageInstaller/model/AdModel$DesData;

    if-eqz p2, :cond_f

    invoke-virtual {p2}, Lcom/miui/packageInstaller/model/AdModel$DesData;->getData()Lcom/miui/packageInstaller/model/AdData;

    move-result-object p2

    if-eqz p2, :cond_f

    invoke-virtual {p2}, Lcom/miui/packageInstaller/model/AdData;->getActionUrl()Ljava/lang/String;

    move-result-object p2

    goto :goto_8

    :cond_f
    move-object p2, v7

    :goto_8
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p2

    if-nez p2, :cond_12

    iget-object p2, p1, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->n:Lcom/miui/packageInstaller/model/AdModel$DesData;

    if-eqz p2, :cond_10

    invoke-virtual {p2}, Lcom/miui/packageInstaller/model/AdModel$DesData;->getData()Lcom/miui/packageInstaller/model/AdData;

    move-result-object p2

    if-eqz p2, :cond_10

    invoke-virtual {p2}, Lcom/miui/packageInstaller/model/AdData;->getActionUrl()Ljava/lang/String;

    move-result-object p2

    goto :goto_9

    :cond_10
    move-object p2, v7

    :goto_9
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-static {p2, v2, v6, v1, v7}, Lu8/g;->v(Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_12

    iget-object p1, p1, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->n:Lcom/miui/packageInstaller/model/AdModel$DesData;

    if-eqz p1, :cond_11

    invoke-virtual {p1}, Lcom/miui/packageInstaller/model/AdModel$DesData;->getData()Lcom/miui/packageInstaller/model/AdData;

    move-result-object p1

    if-eqz p1, :cond_11

    invoke-virtual {p1}, Lcom/miui/packageInstaller/model/AdData;->getActionUrl()Ljava/lang/String;

    move-result-object v7

    :cond_11
    invoke-static {p0, v7}, Lq2/d;->d(Landroid/content/Context;Ljava/lang/String;)Z

    goto/16 :goto_f

    :cond_12
    iget-object p2, p1, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->n:Lcom/miui/packageInstaller/model/AdModel$DesData;

    if-eqz p2, :cond_13

    invoke-virtual {p2}, Lcom/miui/packageInstaller/model/AdModel$DesData;->getData()Lcom/miui/packageInstaller/model/AdData;

    move-result-object p2

    goto :goto_a

    :cond_13
    move-object p2, v7

    :goto_a
    if-nez p2, :cond_14

    goto :goto_b

    :cond_14
    iget-object v0, p1, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->t:Ljava/lang/String;

    invoke-virtual {p2, v0}, Lcom/miui/packageInstaller/model/AdData;->setSourcePackageName(Ljava/lang/String;)V

    :goto_b
    invoke-static {}, Lk2/e;->h()Lk2/e;

    move-result-object p2

    iget-object v0, p1, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->n:Lcom/miui/packageInstaller/model/AdModel$DesData;

    if-eqz v0, :cond_15

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/AdModel$DesData;->getData()Lcom/miui/packageInstaller/model/AdData;

    move-result-object v0

    goto :goto_c

    :cond_15
    move-object v0, v7

    :goto_c
    invoke-virtual {p2, p0, v0}, Lk2/e;->p(Landroid/content/Context;Lcom/miui/packageInstaller/model/AdInterface;)V

    const/4 p2, 0x1

    iput-boolean p2, p1, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->q:Z

    new-instance p2, Landroid/util/ArrayMap;

    invoke-direct {p2}, Landroid/util/ArrayMap;-><init>()V

    move-object v0, p0

    check-cast v0, Lm5/s;

    invoke-interface {v0, v3}, Lm5/s;->N(Ljava/lang/String;)Lo5/c;

    move-result-object v0

    if-eqz v0, :cond_16

    invoke-virtual {v0, p2}, Lo5/c;->j(Ljava/util/Map;)V

    :cond_16
    iget-object v0, p1, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->m:Lcom/miui/packageInstaller/model/AdModel$DesData;

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/AdModel$DesData;->getData()Lcom/miui/packageInstaller/model/AdData;

    move-result-object v0

    new-instance v1, Lp4/e;

    invoke-direct {v1}, Lp4/e;-><init>()V

    invoke-virtual {v1, p2}, Lp4/e;->q(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    const-string v1, "button"

    invoke-static {v5, v0, v1, v4, p2}, Lk2/a;->d(Ljava/lang/String;Lcom/miui/packageInstaller/model/AdInterface;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance p2, Lp5/b;

    check-cast p0, Lo5/a;

    const-string v0, "advertise_app_install_btn"

    invoke-direct {p2, v0, v1, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    iget-object p0, p1, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->n:Lcom/miui/packageInstaller/model/AdModel$DesData;

    if-eqz p0, :cond_17

    invoke-virtual {p0}, Lcom/miui/packageInstaller/model/AdModel$DesData;->getData()Lcom/miui/packageInstaller/model/AdData;

    move-result-object p0

    if-eqz p0, :cond_17

    invoke-virtual {p0}, Lcom/miui/packageInstaller/model/AdData;->getAppId()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p0

    goto :goto_d

    :cond_17
    move-object p0, v7

    :goto_d
    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    const-string v0, "related_app_id"

    invoke-virtual {p2, v0, p0}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p0

    iget-object p1, p1, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->n:Lcom/miui/packageInstaller/model/AdModel$DesData;

    if-eqz p1, :cond_18

    invoke-virtual {p1}, Lcom/miui/packageInstaller/model/AdModel$DesData;->getData()Lcom/miui/packageInstaller/model/AdData;

    move-result-object p1

    if-eqz p1, :cond_18

    invoke-virtual {p1}, Lcom/miui/packageInstaller/model/AdData;->getAppInfo()Lcom/miui/packageInstaller/model/MarketAppInfo;

    move-result-object p1

    if-eqz p1, :cond_18

    iget-object v7, p1, Lcom/miui/packageInstaller/model/MarketAppInfo;->level1Category:Ljava/lang/String;

    :cond_18
    const-string p1, "15"

    invoke-static {v7, p1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_19

    const-string p1, "game"

    goto :goto_e

    :cond_19
    const-string p1, "app"

    :goto_e
    const-string p2, "related_app_type"

    invoke-virtual {p0, p2, p1}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p0

    invoke-virtual {p0}, Lp5/f;->c()Z

    :goto_f
    return-void
.end method

.method private final J()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->n:Lcom/miui/packageInstaller/model/AdModel$DesData;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/AdModel$DesData;->getData()Lcom/miui/packageInstaller/model/AdData;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/AdData;->getChannel()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    const-string v2, "08-1"

    invoke-static {v2, v0}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const-string v2, "08-3"

    if-eqz v0, :cond_1

    return-object v2

    :cond_1
    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->n:Lcom/miui/packageInstaller/model/AdModel$DesData;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/AdModel$DesData;->getData()Lcom/miui/packageInstaller/model/AdData;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/AdData;->getChannel()Ljava/lang/String;

    move-result-object v1

    :cond_2
    const-string v0, "08-0"

    invoke-static {v0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "08-4"

    return-object v0

    :cond_3
    return-object v2
.end method

.method private final K()Z
    .locals 3

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->n:Lcom/miui/packageInstaller/model/AdModel$DesData;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/AdModel$DesData;->getData()Lcom/miui/packageInstaller/model/AdData;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/AdData;->getUiConfig()Lcom/miui/packageInstaller/model/UiConfig;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v0, v0, Lcom/miui/packageInstaller/model/UiConfig;->displayType:I

    if-ne v0, v1, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->q:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->v:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->r:Z

    if-nez v0, :cond_1

    goto :goto_1

    :cond_1
    move v1, v2

    :goto_1
    return v1
.end method

.method private static final N(Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;Landroid/view/View;)V
    .locals 2

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object p1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->n:Lcom/miui/packageInstaller/model/AdModel$DesData;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/AdModel$DesData;->getData()Lcom/miui/packageInstaller/model/AdData;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/AdData;->getAppPermission()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {p1, v0}, Lq2/d;->d(Landroid/content/Context;Ljava/lang/String;)Z

    new-instance p1, Lp5/b;

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object p0

    const-string v0, "null cannot be cast to non-null type com.miui.packageInstaller.analytics.IPage"

    invoke-static {p0, v0}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p0, Lo5/a;

    const-string v0, "advertise_app_right_btn"

    const-string v1, "button"

    invoke-direct {p1, v0, v1, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p1}, Lp5/f;->c()Z

    return-void
.end method

.method private static final O(Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;Landroid/view/View;)V
    .locals 2

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object p1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->n:Lcom/miui/packageInstaller/model/AdModel$DesData;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/AdModel$DesData;->getData()Lcom/miui/packageInstaller/model/AdData;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/AdData;->getAppPrivacy()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {p1, v0}, Lq2/d;->d(Landroid/content/Context;Ljava/lang/String;)Z

    new-instance p1, Lp5/b;

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object p0

    const-string v0, "null cannot be cast to non-null type com.miui.packageInstaller.analytics.IPage"

    invoke-static {p0, v0}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p0, Lo5/a;

    const-string v0, "advertise_privacy_statment_btn"

    const-string v1, "button"

    invoke-direct {p1, v0, v1, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p1}, Lp5/f;->c()Z

    return-void
.end method

.method private final R()V
    .locals 11

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->v:Z

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v0

    const-string v1, "null cannot be cast to non-null type com.miui.packageInstaller.IInstallerContext"

    invoke-static {v0, v1}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lm5/s;

    invoke-interface {v0}, Lm5/s;->G()Lcom/miui/packageInstaller/model/ApkInfo;

    move-result-object v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/ApkInfo;->getLabel()Ljava/lang/String;

    move-result-object v1

    move-object v4, v1

    goto :goto_0

    :cond_0
    move-object v4, v2

    :goto_0
    invoke-interface {v0}, Lm5/s;->G()Lcom/miui/packageInstaller/model/ApkInfo;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/ApkInfo;->getPackageName()Ljava/lang/String;

    move-result-object v1

    move-object v5, v1

    goto :goto_1

    :cond_1
    move-object v5, v2

    :goto_1
    invoke-interface {v0}, Lm5/s;->G()Lcom/miui/packageInstaller/model/ApkInfo;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/ApkInfo;->getNewInstall()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    move-object v7, v0

    goto :goto_2

    :cond_2
    move-object v7, v2

    :goto_2
    sget-object v3, Ln5/e;->a:Ln5/e;

    iget-object v6, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->t:Ljava/lang/String;

    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->J()Ljava/lang/String;

    move-result-object v8

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->m:Lcom/miui/packageInstaller/model/AdModel$DesData;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/AdModel$DesData;->getData()Lcom/miui/packageInstaller/model/AdData;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/AdData;->getAppId()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    :cond_3
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    new-instance v10, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$a;

    invoke-direct {v10, p0}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$a;-><init>(Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;)V

    invoke-virtual/range {v3 .. v10}, Ln5/e;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ln5/a;)V

    return-void
.end method

.method private final S(Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;Lk2/g;)V
    .locals 7

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    iget v1, p2, Lk2/g;->d:I

    goto :goto_0

    :cond_0
    move v1, v0

    :goto_0
    const/4 v2, 0x0

    const/4 v3, 0x0

    const v4, 0x660bae73

    const v5, -0xf4518d

    if-eqz v1, :cond_14

    const/4 v6, 0x5

    if-eq v1, v6, :cond_e

    const/4 v6, 0x2

    if-eq v1, v6, :cond_8

    const/4 v2, 0x3

    if-eq v1, v2, :cond_1

    goto/16 :goto_6

    :cond_1
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->getInstallBtn()Lcom/miui/packageInstaller/view/AdActionButton;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setEnabled(Z)V

    :cond_2
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->getInstallBtn()Lcom/miui/packageInstaller/view/AdActionButton;

    move-result-object v1

    if-eqz v1, :cond_4

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    if-eqz p2, :cond_3

    iget v3, p2, Lk2/g;->e:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    :cond_3
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v3, 0x25

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_4
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->getInstallBtn()Lcom/miui/packageInstaller/view/AdActionButton;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_5
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->getInstallBtn()Lcom/miui/packageInstaller/view/AdActionButton;

    move-result-object v1

    if-eqz v1, :cond_6

    invoke-virtual {v1, v4}, Lcom/miui/packageInstaller/view/AdActionButton;->setBorderColor(I)V

    :cond_6
    if-eqz p2, :cond_7

    iget p2, p2, Lk2/g;->e:I

    int-to-float p2, p2

    const/high16 v1, 0x42c80000    # 100.0f

    div-float/2addr p2, v1

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->getInstallBtn()Lcom/miui/packageInstaller/view/AdActionButton;

    move-result-object v1

    if-eqz v1, :cond_7

    invoke-virtual {v1, p2}, Lcom/miui/packageInstaller/view/AdActionButton;->setProgress(F)V

    :cond_7
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->getInstallBtn()Lcom/miui/packageInstaller/view/AdActionButton;

    move-result-object p1

    if-eqz p1, :cond_24

    invoke-virtual {p1, v0}, Lcom/miui/packageInstaller/view/AdActionButton;->setBgColor(I)V

    goto/16 :goto_6

    :cond_8
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->getInstallBtn()Lcom/miui/packageInstaller/view/AdActionButton;

    move-result-object p2

    if-eqz p2, :cond_9

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setEnabled(Z)V

    :cond_9
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->getInstallBtn()Lcom/miui/packageInstaller/view/AdActionButton;

    move-result-object p2

    if-eqz p2, :cond_a

    const v1, 0x7f110026

    invoke-virtual {p2, v1}, Landroid/widget/TextView;->setText(I)V

    :cond_a
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->getInstallBtn()Lcom/miui/packageInstaller/view/AdActionButton;

    move-result-object p2

    if-eqz p2, :cond_b

    invoke-virtual {p2, v5}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_b
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->getInstallBtn()Lcom/miui/packageInstaller/view/AdActionButton;

    move-result-object p2

    if-eqz p2, :cond_c

    invoke-virtual {p2, v4}, Lcom/miui/packageInstaller/view/AdActionButton;->setBorderColor(I)V

    :cond_c
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->getInstallBtn()Lcom/miui/packageInstaller/view/AdActionButton;

    move-result-object p2

    if-eqz p2, :cond_d

    invoke-virtual {p2, v0}, Lcom/miui/packageInstaller/view/AdActionButton;->setBgColor(I)V

    :cond_d
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->getInstallBtn()Lcom/miui/packageInstaller/view/AdActionButton;

    move-result-object p1

    if-eqz p1, :cond_24

    invoke-virtual {p1, v2}, Lcom/miui/packageInstaller/view/AdActionButton;->setProgress(F)V

    goto/16 :goto_6

    :cond_e
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->getInstallBtn()Lcom/miui/packageInstaller/view/AdActionButton;

    move-result-object p2

    if-eqz p2, :cond_f

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setEnabled(Z)V

    :cond_f
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->getInstallBtn()Lcom/miui/packageInstaller/view/AdActionButton;

    move-result-object p2

    if-eqz p2, :cond_10

    const v1, 0x7f110025

    invoke-virtual {p2, v1}, Landroid/widget/TextView;->setText(I)V

    :cond_10
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->getInstallBtn()Lcom/miui/packageInstaller/view/AdActionButton;

    move-result-object p2

    if-eqz p2, :cond_11

    invoke-virtual {p2, v5}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_11
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->getInstallBtn()Lcom/miui/packageInstaller/view/AdActionButton;

    move-result-object p2

    if-eqz p2, :cond_12

    invoke-virtual {p2, v4}, Lcom/miui/packageInstaller/view/AdActionButton;->setBorderColor(I)V

    :cond_12
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->getInstallBtn()Lcom/miui/packageInstaller/view/AdActionButton;

    move-result-object p2

    if-eqz p2, :cond_13

    invoke-virtual {p2, v0}, Lcom/miui/packageInstaller/view/AdActionButton;->setBgColor(I)V

    :cond_13
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->getInstallBtn()Lcom/miui/packageInstaller/view/AdActionButton;

    move-result-object p1

    if-eqz p1, :cond_24

    const/high16 p2, 0x3f800000    # 1.0f

    invoke-virtual {p1, p2}, Lcom/miui/packageInstaller/view/AdActionButton;->setProgress(F)V

    goto/16 :goto_6

    :cond_14
    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object p2

    iget-object v1, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->n:Lcom/miui/packageInstaller/model/AdModel$DesData;

    if-eqz v1, :cond_15

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/AdModel$DesData;->getData()Lcom/miui/packageInstaller/model/AdData;

    move-result-object v1

    if-eqz v1, :cond_15

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/AdData;->getPackageName()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_15
    move-object v1, v3

    :goto_1
    invoke-static {p2, v1}, Lj2/f;->o(Landroid/content/Context;Ljava/lang/String;)Z

    move-result p2

    if-eqz p2, :cond_1a

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->getInstallBtn()Lcom/miui/packageInstaller/view/AdActionButton;

    move-result-object p2

    if-eqz p2, :cond_16

    const v1, 0x7f110024

    invoke-virtual {p2, v1}, Landroid/widget/TextView;->setText(I)V

    :cond_16
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->getInstallBtn()Lcom/miui/packageInstaller/view/AdActionButton;

    move-result-object p2

    if-eqz p2, :cond_17

    invoke-virtual {p2, v5}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_17
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->getInstallBtn()Lcom/miui/packageInstaller/view/AdActionButton;

    move-result-object p2

    if-eqz p2, :cond_18

    invoke-virtual {p2, v4}, Lcom/miui/packageInstaller/view/AdActionButton;->setBorderColor(I)V

    :cond_18
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->getInstallBtn()Lcom/miui/packageInstaller/view/AdActionButton;

    move-result-object p2

    if-eqz p2, :cond_19

    invoke-virtual {p2, v0}, Lcom/miui/packageInstaller/view/AdActionButton;->setBgColor(I)V

    :cond_19
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->getInstallBtn()Lcom/miui/packageInstaller/view/AdActionButton;

    move-result-object p2

    if-eqz p2, :cond_23

    :goto_2
    invoke-virtual {p2, v2}, Lcom/miui/packageInstaller/view/AdActionButton;->setProgress(F)V

    goto :goto_5

    :cond_1a
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->getInstallBtn()Lcom/miui/packageInstaller/view/AdActionButton;

    move-result-object p2

    if-nez p2, :cond_1b

    goto :goto_3

    :cond_1b
    iget-object v1, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->n:Lcom/miui/packageInstaller/model/AdModel$DesData;

    if-eqz v1, :cond_1c

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/AdModel$DesData;->getData()Lcom/miui/packageInstaller/model/AdData;

    move-result-object v1

    if-eqz v1, :cond_1c

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/AdData;->getExtra()Lcom/miui/packageInstaller/model/AdModel$ExtraData;

    move-result-object v1

    if-eqz v1, :cond_1c

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/AdModel$ExtraData;->getButton()Ljava/lang/String;

    move-result-object v3

    :cond_1c
    invoke-virtual {p2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_3
    iget-boolean p2, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->r:Z

    if-eqz p2, :cond_1f

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->getInstallBtn()Lcom/miui/packageInstaller/view/AdActionButton;

    move-result-object p2

    if-eqz p2, :cond_1d

    invoke-virtual {p2, v5}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_1d
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->getInstallBtn()Lcom/miui/packageInstaller/view/AdActionButton;

    move-result-object p2

    if-eqz p2, :cond_1e

    invoke-virtual {p2, v4}, Lcom/miui/packageInstaller/view/AdActionButton;->setBorderColor(I)V

    :cond_1e
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->getInstallBtn()Lcom/miui/packageInstaller/view/AdActionButton;

    move-result-object p2

    if-eqz p2, :cond_22

    invoke-virtual {p2, v0}, Lcom/miui/packageInstaller/view/AdActionButton;->setBgColor(I)V

    goto :goto_4

    :cond_1f
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->getInstallBtn()Lcom/miui/packageInstaller/view/AdActionButton;

    move-result-object p2

    if-eqz p2, :cond_20

    const/4 v0, -0x1

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_20
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->getInstallBtn()Lcom/miui/packageInstaller/view/AdActionButton;

    move-result-object p2

    if-eqz p2, :cond_21

    iget v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->s:I

    invoke-virtual {p2, v0}, Lcom/miui/packageInstaller/view/AdActionButton;->setBgColor(I)V

    :cond_21
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->getInstallBtn()Lcom/miui/packageInstaller/view/AdActionButton;

    move-result-object p2

    if-eqz p2, :cond_22

    invoke-virtual {p2, v5}, Lcom/miui/packageInstaller/view/AdActionButton;->setBorderColor(I)V

    :cond_22
    :goto_4
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->getInstallBtn()Lcom/miui/packageInstaller/view/AdActionButton;

    move-result-object p2

    if-eqz p2, :cond_23

    goto :goto_2

    :cond_23
    :goto_5
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->getInstallBtn()Lcom/miui/packageInstaller/view/AdActionButton;

    move-result-object p1

    if-eqz p1, :cond_24

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setEnabled(Z)V

    :cond_24
    :goto_6
    return-void
.end method

.method public static synthetic z(Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->N(Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public final G()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lm6/a<",
            "*>;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->p:Ljava/util/List;

    return-object v0
.end method

.method public final H()I
    .locals 4

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->m:Lcom/miui/packageInstaller/model/AdModel$DesData;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/AdModel$DesData;->getData()Lcom/miui/packageInstaller/model/AdData;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/AdData;->getAppInfo()Lcom/miui/packageInstaller/model/MarketAppInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/miui/packageInstaller/model/MarketAppInfo;->appScreenshotInfo:Lcom/miui/packageInstaller/model/AppScreenshotInfo;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/AppScreenshotInfo;->getImgExloc()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    const/4 v2, 0x1

    if-eqz v0, :cond_1

    return v2

    :cond_1
    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->m:Lcom/miui/packageInstaller/model/AdModel$DesData;

    const/4 v3, 0x0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/AdModel$DesData;->getData()Lcom/miui/packageInstaller/model/AdData;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/AdData;->getAppInfo()Lcom/miui/packageInstaller/model/MarketAppInfo;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, v0, Lcom/miui/packageInstaller/model/MarketAppInfo;->detailVideoAndScreenshotList:Ljava/util/List;

    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    xor-int/2addr v0, v2

    if-ne v0, v2, :cond_2

    goto :goto_1

    :cond_2
    move v2, v3

    :goto_1
    if-eqz v2, :cond_4

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->m:Lcom/miui/packageInstaller/model/AdModel$DesData;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/AdModel$DesData;->getData()Lcom/miui/packageInstaller/model/AdData;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/AdData;->getAppInfo()Lcom/miui/packageInstaller/model/MarketAppInfo;

    move-result-object v1

    :cond_3
    invoke-static {v1}, Lm8/i;->c(Ljava/lang/Object;)V

    iget-object v0, v1, Lcom/miui/packageInstaller/model/MarketAppInfo;->detailVideoAndScreenshotList:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/packageInstaller/model/MarketAppInfo$DetailVideoAndScreenshot;

    iget v0, v0, Lcom/miui/packageInstaller/model/MarketAppInfo$DetailVideoAndScreenshot;->orientation:I

    return v0

    :cond_4
    const/4 v0, -0x1

    return v0
.end method

.method public final I()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->m:Lcom/miui/packageInstaller/model/AdModel$DesData;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/AdModel$DesData;->getData()Lcom/miui/packageInstaller/model/AdData;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/AdData;->getAppInfo()Lcom/miui/packageInstaller/model/MarketAppInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, v1, Lcom/miui/packageInstaller/model/MarketAppInfo;->appScreenshotInfo:Lcom/miui/packageInstaller/model/AppScreenshotInfo;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/AppScreenshotInfo;->getImgExloc()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_0
    move-object v1, v2

    :goto_0
    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->m:Lcom/miui/packageInstaller/model/AdModel$DesData;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/AdModel$DesData;->getData()Lcom/miui/packageInstaller/model/AdData;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/AdData;->getAppInfo()Lcom/miui/packageInstaller/model/MarketAppInfo;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, v1, Lcom/miui/packageInstaller/model/MarketAppInfo;->appScreenshotInfo:Lcom/miui/packageInstaller/model/AppScreenshotInfo;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/AppScreenshotInfo;->getImgExloc()Ljava/lang/String;

    move-result-object v2

    :cond_1
    invoke-static {v2}, Lm8/i;->c(Ljava/lang/Object;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_2
    iget-object v1, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->m:Lcom/miui/packageInstaller/model/AdModel$DesData;

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/AdModel$DesData;->getData()Lcom/miui/packageInstaller/model/AdData;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/AdData;->getAppInfo()Lcom/miui/packageInstaller/model/MarketAppInfo;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, v1, Lcom/miui/packageInstaller/model/MarketAppInfo;->detailVideoAndScreenshotList:Ljava/util/List;

    if-eqz v1, :cond_3

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    xor-int/2addr v1, v4

    if-ne v1, v4, :cond_3

    move v3, v4

    :cond_3
    if-eqz v3, :cond_5

    iget-object v1, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->m:Lcom/miui/packageInstaller/model/AdModel$DesData;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/AdModel$DesData;->getData()Lcom/miui/packageInstaller/model/AdData;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/AdData;->getAppInfo()Lcom/miui/packageInstaller/model/MarketAppInfo;

    move-result-object v2

    :cond_4
    invoke-static {v2}, Lm8/i;->c(Ljava/lang/Object;)V

    iget-object v1, v2, Lcom/miui/packageInstaller/model/MarketAppInfo;->detailVideoAndScreenshotList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/packageInstaller/model/MarketAppInfo$DetailVideoAndScreenshot;

    iget-object v2, v2, Lcom/miui/packageInstaller/model/MarketAppInfo$DetailVideoAndScreenshot;->screenshot:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_5
    :goto_2
    return-object v0
.end method

.method public L(Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;)V
    .locals 10

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->getAppIcon()Landroid/widget/ImageView;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/bumptech/glide/b;->t(Landroid/content/Context;)Lcom/bumptech/glide/k;

    move-result-object v2

    iget-object v3, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->n:Lcom/miui/packageInstaller/model/AdModel$DesData;

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/miui/packageInstaller/model/AdModel$DesData;->getData()Lcom/miui/packageInstaller/model/AdData;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/miui/packageInstaller/model/AdData;->getIconUrl()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :cond_0
    move-object v3, v0

    :goto_0
    invoke-virtual {v2, v3}, Lcom/bumptech/glide/k;->t(Ljava/lang/String;)Lcom/bumptech/glide/j;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/bumptech/glide/j;->w0(Landroid/widget/ImageView;)Ln3/i;

    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->getAppName()Landroid/widget/TextView;

    move-result-object v1

    goto :goto_1

    :cond_2
    move-object v1, v0

    :goto_1
    if-nez v1, :cond_3

    goto :goto_4

    :cond_3
    iget-object v2, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->n:Lcom/miui/packageInstaller/model/AdModel$DesData;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Lcom/miui/packageInstaller/model/AdModel$DesData;->getData()Lcom/miui/packageInstaller/model/AdData;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Lcom/miui/packageInstaller/model/AdData;->getAppName()Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    :cond_4
    move-object v2, v0

    :goto_2
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    iget-object v2, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->n:Lcom/miui/packageInstaller/model/AdModel$DesData;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Lcom/miui/packageInstaller/model/AdModel$DesData;->getData()Lcom/miui/packageInstaller/model/AdData;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Lcom/miui/packageInstaller/model/AdData;->getAppName()Ljava/lang/String;

    move-result-object v2

    goto :goto_3

    :cond_5
    move-object v2, v0

    goto :goto_3

    :cond_6
    iget-object v2, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->n:Lcom/miui/packageInstaller/model/AdModel$DesData;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Lcom/miui/packageInstaller/model/AdModel$DesData;->getData()Lcom/miui/packageInstaller/model/AdData;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Lcom/miui/packageInstaller/model/AdData;->getTitle()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-static {v2}, Lu8/g;->u0(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_3
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_4
    if-eqz p1, :cond_7

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->getAppDes()Landroid/widget/TextView;

    move-result-object v1

    goto :goto_5

    :cond_7
    move-object v1, v0

    :goto_5
    if-nez v1, :cond_8

    goto :goto_7

    :cond_8
    iget-object v2, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->n:Lcom/miui/packageInstaller/model/AdModel$DesData;

    if-eqz v2, :cond_9

    invoke-virtual {v2}, Lcom/miui/packageInstaller/model/AdModel$DesData;->getData()Lcom/miui/packageInstaller/model/AdData;

    move-result-object v2

    if-eqz v2, :cond_9

    invoke-virtual {v2}, Lcom/miui/packageInstaller/model/AdData;->getSummary()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_9

    invoke-static {v2}, Lu8/g;->u0(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_6

    :cond_9
    move-object v2, v0

    :goto_6
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_7
    if-eqz p1, :cond_a

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->getAppIcon()Landroid/widget/ImageView;

    move-result-object v1

    goto :goto_8

    :cond_a
    move-object v1, v0

    :goto_8
    if-nez v1, :cond_b

    goto :goto_a

    :cond_b
    if-eqz p1, :cond_c

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->getAppName()Landroid/widget/TextView;

    move-result-object v2

    if-eqz v2, :cond_c

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    goto :goto_9

    :cond_c
    move-object v2, v0

    :goto_9
    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    :goto_a
    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060029

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getColor(ILandroid/content/res/Resources$Theme;)I

    move-result v1

    const/4 v2, 0x1

    new-array v3, v2, [Landroid/view/View;

    if-eqz p1, :cond_d

    iget-object v4, p1, Landroidx/recyclerview/widget/RecyclerView$d0;->itemView:Landroid/view/View;

    goto :goto_b

    :cond_d
    move-object v4, v0

    :goto_b
    const/4 v5, 0x0

    aput-object v4, v3, v5

    invoke-static {v3}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object v3

    invoke-interface {v3}, Lmiuix/animation/e;->c()Lmiuix/animation/j;

    move-result-object v3

    new-array v4, v5, [Lmiuix/animation/j$b;

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-interface {v3, v6, v4}, Lmiuix/animation/j;->G(F[Lmiuix/animation/j$b;)Lmiuix/animation/j;

    move-result-object v3

    invoke-interface {v3, v1}, Lmiuix/animation/j;->i(I)Lmiuix/animation/j;

    move-result-object v3

    if-eqz p1, :cond_e

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->getAppDesLayout()Landroid/widget/LinearLayout;

    move-result-object v4

    goto :goto_c

    :cond_e
    move-object v4, v0

    :goto_c
    new-array v7, v5, [Lc9/a;

    invoke-interface {v3, v4, v7}, Lmiuix/animation/j;->z(Landroid/view/View;[Lc9/a;)V

    new-array v3, v2, [Landroid/view/View;

    if-eqz p1, :cond_f

    iget-object v4, p1, Landroidx/recyclerview/widget/RecyclerView$d0;->itemView:Landroid/view/View;

    goto :goto_d

    :cond_f
    move-object v4, v0

    :goto_d
    aput-object v4, v3, v5

    invoke-static {v3}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object v3

    invoke-interface {v3}, Lmiuix/animation/e;->c()Lmiuix/animation/j;

    move-result-object v3

    new-array v4, v5, [Lmiuix/animation/j$b;

    invoke-interface {v3, v6, v4}, Lmiuix/animation/j;->G(F[Lmiuix/animation/j$b;)Lmiuix/animation/j;

    move-result-object v3

    invoke-interface {v3, v1}, Lmiuix/animation/j;->i(I)Lmiuix/animation/j;

    move-result-object v1

    if-eqz p1, :cond_10

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->getAppIcon()Landroid/widget/ImageView;

    move-result-object v3

    goto :goto_e

    :cond_10
    move-object v3, v0

    :goto_e
    new-array v4, v5, [Lc9/a;

    invoke-interface {v1, v3, v4}, Lmiuix/animation/j;->z(Landroid/view/View;[Lc9/a;)V

    new-array v1, v2, [Landroid/view/View;

    if-eqz p1, :cond_11

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->getInstallBtn()Lcom/miui/packageInstaller/view/AdActionButton;

    move-result-object v3

    goto :goto_f

    :cond_11
    move-object v3, v0

    :goto_f
    aput-object v3, v1, v5

    invoke-static {v1}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object v1

    invoke-interface {v1}, Lmiuix/animation/e;->c()Lmiuix/animation/j;

    move-result-object v1

    new-array v3, v5, [Lmiuix/animation/j$b;

    invoke-interface {v1, v6, v3}, Lmiuix/animation/j;->G(F[Lmiuix/animation/j$b;)Lmiuix/animation/j;

    move-result-object v1

    if-eqz p1, :cond_12

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->getInstallBtn()Lcom/miui/packageInstaller/view/AdActionButton;

    move-result-object v3

    goto :goto_10

    :cond_12
    move-object v3, v0

    :goto_10
    new-array v4, v5, [Lc9/a;

    invoke-interface {v1, v3, v4}, Lmiuix/animation/j;->z(Landroid/view/View;[Lc9/a;)V

    if-eqz p1, :cond_13

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->getAppDesLayout()Landroid/widget/LinearLayout;

    move-result-object v1

    if-eqz v1, :cond_13

    iget-object v3, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->u:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_13
    if-eqz p1, :cond_14

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->getAppIcon()Landroid/widget/ImageView;

    move-result-object v1

    if-eqz v1, :cond_14

    iget-object v3, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->u:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_14
    if-eqz p1, :cond_15

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->getInstallBtn()Lcom/miui/packageInstaller/view/AdActionButton;

    move-result-object v1

    if-eqz v1, :cond_15

    iget-object v3, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->u:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_15
    new-instance v1, Lp5/g;

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v3

    const-string v4, "null cannot be cast to non-null type com.miui.packageInstaller.analytics.IPage"

    invoke-static {v3, v4}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Lo5/a;

    const-string v6, "advertise_app_install_btn"

    const-string v7, "button"

    invoke-direct {v1, v6, v7, v3}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    iget-object v3, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->n:Lcom/miui/packageInstaller/model/AdModel$DesData;

    if-eqz v3, :cond_16

    invoke-virtual {v3}, Lcom/miui/packageInstaller/model/AdModel$DesData;->getData()Lcom/miui/packageInstaller/model/AdData;

    move-result-object v3

    if-eqz v3, :cond_16

    invoke-virtual {v3}, Lcom/miui/packageInstaller/model/AdData;->getAppId()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    goto :goto_11

    :cond_16
    move-object v3, v0

    :goto_11
    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v6, "related_app_id"

    invoke-virtual {v1, v6, v3}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object v1

    iget-object v3, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->n:Lcom/miui/packageInstaller/model/AdModel$DesData;

    if-eqz v3, :cond_17

    invoke-virtual {v3}, Lcom/miui/packageInstaller/model/AdModel$DesData;->getData()Lcom/miui/packageInstaller/model/AdData;

    move-result-object v3

    if-eqz v3, :cond_17

    invoke-virtual {v3}, Lcom/miui/packageInstaller/model/AdData;->getAppInfo()Lcom/miui/packageInstaller/model/MarketAppInfo;

    move-result-object v3

    if-eqz v3, :cond_17

    iget-object v3, v3, Lcom/miui/packageInstaller/model/MarketAppInfo;->level1Category:Ljava/lang/String;

    goto :goto_12

    :cond_17
    move-object v3, v0

    :goto_12
    const-string v6, "15"

    invoke-static {v3, v6}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_18

    const-string v3, "game"

    goto :goto_13

    :cond_18
    const-string v3, "app"

    :goto_13
    const-string v6, "related_app_type"

    invoke-virtual {v1, v6, v3}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object v1

    invoke-virtual {v1}, Lp5/f;->c()Z

    new-instance v1, Lp5/g;

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v4}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Lo5/a;

    const-string v6, "advertise_privacy_statment_btn"

    invoke-direct {v1, v6, v7, v3}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v1}, Lp5/f;->c()Z

    new-instance v1, Lp5/g;

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v4}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Lo5/a;

    const-string v6, "advertise_app_right_btn"

    invoke-direct {v1, v6, v7, v3}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v1}, Lp5/f;->c()Z

    new-instance v1, Landroid/util/ArrayMap;

    invoke-direct {v1}, Landroid/util/ArrayMap;-><init>()V

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v4}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Lo5/a;

    invoke-static {v3, v1}, Lq2/a;->c(Lo5/a;Ljava/util/Map;)V

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v3

    const-string v4, "null cannot be cast to non-null type com.miui.packageInstaller.IInstallerContext"

    invoke-static {v3, v4}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Lm5/s;

    const-string v4, "private"

    invoke-interface {v3, v4}, Lm5/s;->N(Ljava/lang/String;)Lo5/c;

    move-result-object v3

    if-eqz v3, :cond_19

    invoke-virtual {v3, v1}, Lo5/c;->j(Ljava/util/Map;)V

    :cond_19
    iget-object v3, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->m:Lcom/miui/packageInstaller/model/AdModel$DesData;

    invoke-virtual {v3}, Lcom/miui/packageInstaller/model/AdModel$DesData;->getData()Lcom/miui/packageInstaller/model/AdData;

    move-result-object v3

    new-instance v4, Lp4/e;

    invoke-direct {v4}, Lp4/e;-><init>()V

    invoke-virtual {v4, v1}, Lp4/e;->q(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v4, "VIEW"

    const-string v6, ""

    invoke-static {v4, v3, v6, v6, v1}, Lk2/a;->d(Ljava/lang/String;Lcom/miui/packageInstaller/model/AdInterface;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p1, :cond_1a

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->getTvVersion()Landroid/widget/TextView;

    move-result-object v1

    goto :goto_14

    :cond_1a
    move-object v1, v0

    :goto_14
    if-nez v1, :cond_1b

    goto :goto_16

    :cond_1b
    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f110043

    new-array v6, v2, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->n:Lcom/miui/packageInstaller/model/AdModel$DesData;

    if-eqz v7, :cond_1c

    invoke-virtual {v7}, Lcom/miui/packageInstaller/model/AdModel$DesData;->getData()Lcom/miui/packageInstaller/model/AdData;

    move-result-object v7

    if-eqz v7, :cond_1c

    invoke-virtual {v7}, Lcom/miui/packageInstaller/model/AdData;->getAppVersion()Ljava/lang/String;

    move-result-object v7

    goto :goto_15

    :cond_1c
    move-object v7, v0

    :goto_15
    aput-object v7, v6, v5

    invoke-virtual {v3, v4, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_16
    if-eqz p1, :cond_1d

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->getAppSize()Landroid/widget/TextView;

    move-result-object v1

    goto :goto_17

    :cond_1d
    move-object v1, v0

    :goto_17
    if-nez v1, :cond_1e

    goto :goto_19

    :cond_1e
    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f110038

    new-array v6, v2, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->n:Lcom/miui/packageInstaller/model/AdModel$DesData;

    if-eqz v7, :cond_1f

    invoke-virtual {v7}, Lcom/miui/packageInstaller/model/AdModel$DesData;->getData()Lcom/miui/packageInstaller/model/AdData;

    move-result-object v7

    if-eqz v7, :cond_1f

    invoke-virtual {v7}, Lcom/miui/packageInstaller/model/AdData;->getAppInfo()Lcom/miui/packageInstaller/model/MarketAppInfo;

    move-result-object v7

    if-eqz v7, :cond_1f

    iget-wide v7, v7, Lcom/miui/packageInstaller/model/MarketAppInfo;->apkSize:J

    invoke-static {v7, v8}, Lcom/android/packageinstaller/utils/h;->e(J)Ljava/lang/String;

    move-result-object v7

    goto :goto_18

    :cond_1f
    move-object v7, v0

    :goto_18
    aput-object v7, v6, v5

    invoke-virtual {v3, v4, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_19
    if-eqz p1, :cond_20

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->getTvPermission()Landroid/widget/TextView;

    move-result-object v1

    if-eqz v1, :cond_20

    new-instance v3, Lcom/miui/packageInstaller/ui/listcomponets/r;

    invoke-direct {v3, p0}, Lcom/miui/packageInstaller/ui/listcomponets/r;-><init>(Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;)V

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_20
    if-eqz p1, :cond_21

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->getTvPrivacy()Landroid/widget/TextView;

    move-result-object v1

    if-eqz v1, :cond_21

    new-instance v3, Lcom/miui/packageInstaller/ui/listcomponets/s;

    invoke-direct {v3, p0}, Lcom/miui/packageInstaller/ui/listcomponets/s;-><init>(Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;)V

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_21
    if-eqz p1, :cond_22

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->getTvDeveloper()Landroid/widget/TextView;

    move-result-object v1

    goto :goto_1a

    :cond_22
    move-object v1, v0

    :goto_1a
    if-nez v1, :cond_23

    goto :goto_1c

    :cond_23
    iget-object v3, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->n:Lcom/miui/packageInstaller/model/AdModel$DesData;

    if-eqz v3, :cond_24

    invoke-virtual {v3}, Lcom/miui/packageInstaller/model/AdModel$DesData;->getData()Lcom/miui/packageInstaller/model/AdData;

    move-result-object v3

    if-eqz v3, :cond_24

    invoke-virtual {v3}, Lcom/miui/packageInstaller/model/AdData;->getAppDeveloper()Ljava/lang/String;

    move-result-object v3

    goto :goto_1b

    :cond_24
    move-object v3, v0

    :goto_1b
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1c
    invoke-static {p1}, Lm8/i;->c(Ljava/lang/Object;)V

    invoke-static {}, Lk2/e;->h()Lk2/e;

    move-result-object v1

    iget-object v3, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->n:Lcom/miui/packageInstaller/model/AdModel$DesData;

    if-eqz v3, :cond_25

    invoke-virtual {v3}, Lcom/miui/packageInstaller/model/AdModel$DesData;->getData()Lcom/miui/packageInstaller/model/AdData;

    move-result-object v3

    if-eqz v3, :cond_25

    invoke-virtual {v3}, Lcom/miui/packageInstaller/model/AdData;->getPackageName()Ljava/lang/String;

    move-result-object v3

    goto :goto_1d

    :cond_25
    move-object v3, v0

    :goto_1d
    invoke-virtual {v1, v3}, Lk2/e;->g(Ljava/lang/String;)Lk2/g;

    move-result-object v1

    invoke-direct {p0, p1, v1}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->S(Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;Lk2/g;)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->getMediaContainer()Landroid/widget/FrameLayout;

    move-result-object v1

    const/16 v3, 0x8

    if-eqz v1, :cond_37

    iget-object v1, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->n:Lcom/miui/packageInstaller/model/AdModel$DesData;

    if-eqz v1, :cond_26

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/AdModel$DesData;->getData()Lcom/miui/packageInstaller/model/AdData;

    move-result-object v1

    if-eqz v1, :cond_26

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/AdData;->getUiConfig()Lcom/miui/packageInstaller/model/UiConfig;

    move-result-object v1

    if-eqz v1, :cond_26

    iget v1, v1, Lcom/miui/packageInstaller/model/UiConfig;->displayType:I

    const/4 v4, 0x2

    if-ne v1, v4, :cond_26

    move v1, v2

    goto :goto_1e

    :cond_26
    move v1, v5

    :goto_1e
    if-eqz v1, :cond_37

    invoke-virtual {p0}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->H()I

    move-result v1

    invoke-virtual {p0}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->I()Ljava/util/List;

    move-result-object v4

    if-eqz v1, :cond_2e

    if-eq v1, v2, :cond_27

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->getMediaContainer()Landroid/widget/FrameLayout;

    move-result-object p1

    if-nez p1, :cond_38

    goto/16 :goto_26

    :cond_27
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->getMediaContainer()Landroid/widget/FrameLayout;

    move-result-object v1

    if-eqz v1, :cond_28

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getChildCount()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_1f

    :cond_28
    move-object v1, v0

    :goto_1f
    invoke-static {v1}, Lm8/i;->c(Ljava/lang/Object;)V

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-lez v1, :cond_2b

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->getMediaContainer()Landroid/widget/FrameLayout;

    move-result-object v1

    if-eqz v1, :cond_29

    invoke-virtual {v1, v5}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    goto :goto_20

    :cond_29
    move-object v1, v0

    :goto_20
    instance-of v2, v1, Landroid/widget/ImageView;

    if-eqz v2, :cond_2a

    move-object v0, v1

    check-cast v0, Landroid/widget/ImageView;

    goto :goto_21

    :cond_2a
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->getMediaContainer()Landroid/widget/FrameLayout;

    move-result-object v1

    if-eqz v1, :cond_2b

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->removeAllViews()V

    :cond_2b
    :goto_21
    if-nez v0, :cond_2d

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0d002f

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->getMediaContainer()Landroid/widget/FrameLayout;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->getMediaContainer()Landroid/widget/FrameLayout;

    move-result-object v1

    if-eqz v1, :cond_2c

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    :cond_2c
    if-eqz v0, :cond_2d

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/bumptech/glide/b;->t(Landroid/content/Context;)Lcom/bumptech/glide/k;

    move-result-object v1

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/bumptech/glide/k;->t(Ljava/lang/String;)Lcom/bumptech/glide/j;

    move-result-object v1

    invoke-virtual {v1}, Lm3/a;->d()Lm3/a;

    move-result-object v1

    check-cast v1, Lcom/bumptech/glide/j;

    invoke-virtual {v1, v0}, Lcom/bumptech/glide/j;->w0(Landroid/widget/ImageView;)Ln3/i;

    :cond_2d
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->getMediaContainer()Landroid/widget/FrameLayout;

    move-result-object p1

    if-nez p1, :cond_36

    goto/16 :goto_26

    :cond_2e
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->getMediaContainer()Landroid/widget/FrameLayout;

    move-result-object v1

    if-eqz v1, :cond_2f

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getChildCount()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_22

    :cond_2f
    move-object v1, v0

    :goto_22
    invoke-static {v1}, Lm8/i;->c(Ljava/lang/Object;)V

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-lez v1, :cond_32

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->getMediaContainer()Landroid/widget/FrameLayout;

    move-result-object v1

    if-eqz v1, :cond_30

    invoke-virtual {v1, v5}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    goto :goto_23

    :cond_30
    move-object v1, v0

    :goto_23
    instance-of v2, v1, Landroid/view/ViewGroup;

    if-eqz v2, :cond_31

    move-object v0, v1

    check-cast v0, Landroid/view/ViewGroup;

    goto :goto_24

    :cond_31
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->getMediaContainer()Landroid/widget/FrameLayout;

    move-result-object v1

    if-eqz v1, :cond_32

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->removeAllViews()V

    :cond_32
    :goto_24
    if-nez v0, :cond_35

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0d002e

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->getMediaContainer()Landroid/widget/FrameLayout;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->getMediaContainer()Landroid/widget/FrameLayout;

    move-result-object v1

    if-eqz v1, :cond_33

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    :cond_33
    if-eqz v0, :cond_35

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    move v2, v5

    :goto_25
    if-ge v2, v1, :cond_35

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    const-string v6, "null cannot be cast to non-null type android.widget.ImageView"

    invoke-static {v3, v6}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Landroid/widget/ImageView;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    if-ge v2, v6, :cond_34

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/bumptech/glide/b;->t(Landroid/content/Context;)Lcom/bumptech/glide/k;

    move-result-object v6

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/bumptech/glide/k;->t(Ljava/lang/String;)Lcom/bumptech/glide/j;

    move-result-object v6

    invoke-virtual {v6}, Lm3/a;->d()Lm3/a;

    move-result-object v6

    check-cast v6, Lcom/bumptech/glide/j;

    invoke-virtual {v6, v3}, Lcom/bumptech/glide/j;->w0(Landroid/widget/ImageView;)Ln3/i;

    :cond_34
    add-int/lit8 v2, v2, 0x1

    goto :goto_25

    :cond_35
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->getMediaContainer()Landroid/widget/FrameLayout;

    move-result-object p1

    if-nez p1, :cond_36

    goto :goto_26

    :cond_36
    invoke-virtual {p1, v5}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_26

    :cond_37
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->getMediaContainer()Landroid/widget/FrameLayout;

    move-result-object p1

    if-nez p1, :cond_38

    goto :goto_26

    :cond_38
    invoke-virtual {p1, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    :goto_26
    return-void
.end method

.method public M(Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;",
            "Ljava/util/List<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    invoke-super {p0, p1, p2}, Lm6/a;->p(Landroidx/recyclerview/widget/RecyclerView$d0;Ljava/util/List;)V

    if-eqz p2, :cond_2

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_0
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lk2/e;

    if-eqz v1, :cond_0

    invoke-static {p1}, Lm8/i;->c(Ljava/lang/Object;)V

    check-cast v0, Lk2/e;

    iget-object v1, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->n:Lcom/miui/packageInstaller/model/AdModel$DesData;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/AdModel$DesData;->getData()Lcom/miui/packageInstaller/model/AdData;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/AdData;->getPackageName()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v0, v1}, Lk2/e;->g(Ljava/lang/String;)Lk2/g;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->S(Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;Lk2/g;)V

    goto :goto_0

    :cond_2
    return-void
.end method

.method public final P(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lm6/a<",
            "*>;>;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->p:Ljava/util/List;

    return-void
.end method

.method public final Q(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->r:Z

    return-void
.end method

.method public b(Ljava/lang/String;Lk2/g;)V
    .locals 11

    invoke-virtual {p0}, Lm6/a;->f()Lj6/b;

    move-result-object p1

    if-nez p1, :cond_0

    return-void

    :cond_0
    const/4 p1, 0x1

    const/4 v0, 0x0

    if-eqz p2, :cond_1

    iget v1, p2, Lk2/g;->d:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    move v0, p1

    :cond_1
    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->K()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->R()V

    :cond_2
    if-eqz p2, :cond_3

    iget p2, p2, Lk2/g;->d:I

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    goto :goto_0

    :cond_3
    const/4 p2, 0x0

    :goto_0
    const-string v0, "advertise"

    const-string v1, "download_source"

    const-string v2, "download_process"

    const-string v3, "null cannot be cast to non-null type com.miui.packageInstaller.analytics.IPage"

    if-nez p2, :cond_4

    goto :goto_2

    :cond_4
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-ne v4, p1, :cond_5

    new-instance p1, Lp5/c;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object p2

    invoke-static {p2, v3}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v8, p2

    check-cast v8, Lo5/a;

    const/4 v9, 0x3

    const/4 v10, 0x0

    move-object v5, p1

    invoke-direct/range {v5 .. v10}, Lp5/c;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;ILm8/g;)V

    const-string p2, "download_start"

    invoke-virtual {p1, v2, p2}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p1

    invoke-virtual {p1, v1, v0}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p1

    :goto_1
    invoke-virtual {p1}, Lp5/f;->c()Z

    goto :goto_4

    :cond_5
    :goto_2
    const/16 p1, 0x8

    if-nez p2, :cond_6

    goto :goto_3

    :cond_6
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-ne v4, p1, :cond_7

    new-instance p1, Lp5/c;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object p2

    invoke-static {p2, v3}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v8, p2

    check-cast v8, Lo5/a;

    const/4 v9, 0x3

    const/4 v10, 0x0

    move-object v5, p1

    invoke-direct/range {v5 .. v10}, Lp5/c;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;ILm8/g;)V

    const-string p2, "download_finish"

    invoke-virtual {p1, v2, p2}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p1

    invoke-virtual {p1, v1, v0}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p1

    const-string p2, "download_finish_status"

    const-string v0, "success"

    invoke-virtual {p1, p2, v0}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p1

    goto :goto_1

    :cond_7
    :goto_3
    if-nez p2, :cond_8

    goto :goto_4

    :cond_8
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    :goto_4
    invoke-static {}, Lk2/e;->h()Lk2/e;

    move-result-object p1

    invoke-virtual {p0, p1}, Lm6/a;->n(Ljava/lang/Object;)V

    return-void
.end method

.method public k()I
    .locals 1

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0d0177

    goto :goto_0

    :cond_0
    iget-boolean v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->r:Z

    if-eqz v0, :cond_1

    const v0, 0x7f0d019b

    goto :goto_0

    :cond_1
    const v0, 0x7f0d0179

    :goto_0
    return v0
.end method

.method public bridge synthetic o(Landroidx/recyclerview/widget/RecyclerView$d0;)V
    .locals 0

    check-cast p1, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;

    invoke-virtual {p0, p1}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->L(Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;)V

    return-void
.end method

.method public bridge synthetic p(Landroidx/recyclerview/widget/RecyclerView$d0;Ljava/util/List;)V
    .locals 0

    check-cast p1, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;->M(Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;Ljava/util/List;)V

    return-void
.end method
