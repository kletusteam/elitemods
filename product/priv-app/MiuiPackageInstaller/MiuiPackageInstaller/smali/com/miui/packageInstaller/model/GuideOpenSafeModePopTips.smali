.class public final Lcom/miui/packageInstaller/model/GuideOpenSafeModePopTips;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private bl:Ljava/lang/String;

.field private br:Ljava/lang/String;

.field private guideOpenSafeModeSwitch:Z

.field private text:Ljava/lang/String;

.field private title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/miui/packageInstaller/model/GuideOpenSafeModePopTips;->title:Ljava/lang/String;

    iput-object v0, p0, Lcom/miui/packageInstaller/model/GuideOpenSafeModePopTips;->text:Ljava/lang/String;

    iput-object v0, p0, Lcom/miui/packageInstaller/model/GuideOpenSafeModePopTips;->bl:Ljava/lang/String;

    iput-object v0, p0, Lcom/miui/packageInstaller/model/GuideOpenSafeModePopTips;->br:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final getBl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/model/GuideOpenSafeModePopTips;->bl:Ljava/lang/String;

    return-object v0
.end method

.method public final getBr()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/model/GuideOpenSafeModePopTips;->br:Ljava/lang/String;

    return-object v0
.end method

.method public final getGuideOpenSafeModeSwitch()Z
    .locals 1

    iget-boolean v0, p0, Lcom/miui/packageInstaller/model/GuideOpenSafeModePopTips;->guideOpenSafeModeSwitch:Z

    return v0
.end method

.method public final getText()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/model/GuideOpenSafeModePopTips;->text:Ljava/lang/String;

    return-object v0
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/model/GuideOpenSafeModePopTips;->title:Ljava/lang/String;

    return-object v0
.end method

.method public final setBl(Ljava/lang/String;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/model/GuideOpenSafeModePopTips;->bl:Ljava/lang/String;

    return-void
.end method

.method public final setBr(Ljava/lang/String;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/model/GuideOpenSafeModePopTips;->br:Ljava/lang/String;

    return-void
.end method

.method public final setGuideOpenSafeModeSwitch(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/miui/packageInstaller/model/GuideOpenSafeModePopTips;->guideOpenSafeModeSwitch:Z

    return-void
.end method

.method public final setText(Ljava/lang/String;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/model/GuideOpenSafeModePopTips;->text:Ljava/lang/String;

    return-void
.end method

.method public final setTitle(Ljava/lang/String;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/model/GuideOpenSafeModePopTips;->title:Ljava/lang/String;

    return-void
.end method
