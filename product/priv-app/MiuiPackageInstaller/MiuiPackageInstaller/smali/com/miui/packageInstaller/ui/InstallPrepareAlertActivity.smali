.class public final Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;
.super Lq2/b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity$b;,
        Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity$a;
    }
.end annotation


# instance fields
.field private final u:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity$b;",
            ">;"
        }
    .end annotation
.end field

.field private v:Lm5/e;

.field private w:Ljava/lang/String;

.field private x:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lq2/b;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;->u:Ljava/util/ArrayList;

    new-instance v0, Lm5/e;

    invoke-direct {v0}, Lm5/e;-><init>()V

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;->v:Lm5/e;

    const-string v0, ""

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;->w:Ljava/lang/String;

    const-class v0, Lcom/miui/packageInstaller/NewPackageInstallerActivity;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;->x:Ljava/lang/Class;

    return-void
.end method

.method public static synthetic J0(Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;)V
    .locals 0

    invoke-static {p0}, Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;->R0(Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;)V

    return-void
.end method

.method public static synthetic K0(Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;)V
    .locals 0

    invoke-static {p0}, Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;->S0(Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;)V

    return-void
.end method

.method public static final synthetic L0(Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;Ljava/util/Iterator;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;->U0(Ljava/util/Iterator;)V

    return-void
.end method

.method private final N0()V
    .locals 6

    sget-object v0, Lt5/f0;->a:Lt5/f0;

    iget-object v1, p0, Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;->v:Lm5/e;

    invoke-virtual {v1}, Lm5/e;->j()Ljava/lang/String;

    move-result-object v1

    const-string v2, "mCaller.callingPackage"

    invoke-static {v1, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lt5/f0;->g(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {p0}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->getSecurityModeStyle(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "normal"

    invoke-static {v1, v3}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;->u:Ljava/util/ArrayList;

    new-instance v3, Lc6/d;

    invoke-direct {v3}, Lc6/d;-><init>()V

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;->u:Ljava/util/ArrayList;

    new-instance v3, Lc6/m;

    invoke-direct {v3}, Lc6/m;-><init>()V

    :goto_0
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    iget-object v1, p0, Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;->v:Lm5/e;

    invoke-virtual {v1}, Lm5/e;->m()Ljava/lang/Boolean;

    move-result-object v1

    const-string v3, "mCaller.isSystemApp"

    invoke-static {v1, v3}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_2

    return-void

    :cond_2
    iget-object v1, p0, Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;->v:Lm5/e;

    invoke-virtual {v1}, Lm5/e;->j()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lt5/f0;->e(Ljava/lang/String;)Lcom/miui/packageInstaller/model/RiskControlRules;

    move-result-object v0

    if-eqz v0, :cond_8

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/RiskControlRules;->getCurrentLevel()I

    move-result v1

    const/4 v2, 0x1

    if-lez v1, :cond_4

    if-ne v1, v2, :cond_3

    const v3, 0x7f110045

    goto :goto_1

    :cond_3
    const v3, 0x7f110046

    :goto_1
    invoke-virtual {p0, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "if (currentLevel == Risk\u2026uth_type_security_verify)"

    invoke-static {v3, v4}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;->u:Ljava/util/ArrayList;

    new-instance v5, Lc6/h;

    invoke-direct {v5, v0, v3}, Lc6/h;-><init>(Lcom/miui/packageInstaller/model/RiskControlRules;Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    if-eq v1, v2, :cond_7

    const/4 v0, 0x2

    if-eq v1, v0, :cond_6

    const/4 v0, 0x3

    if-eq v1, v0, :cond_5

    goto :goto_3

    :cond_5
    iget-object v0, p0, Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;->u:Ljava/util/ArrayList;

    new-instance v1, Lc6/i;

    sget-object v2, Lx5/a;->f:Lx5/a;

    invoke-direct {v1, v2}, Lc6/i;-><init>(Lx5/a;)V

    goto :goto_2

    :cond_6
    iget-object v0, p0, Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;->u:Ljava/util/ArrayList;

    new-instance v1, Lc6/i;

    invoke-direct {v1}, Lc6/i;-><init>()V

    goto :goto_2

    :cond_7
    iget-object v0, p0, Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;->u:Ljava/util/ArrayList;

    new-instance v1, Lc6/a;

    invoke-direct {v1}, Lc6/a;-><init>()V

    :goto_2
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_8
    :goto_3
    return-void
.end method

.method private final O0()V
    .locals 2

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_CALLING_PACKAGE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lm5/e;

    if-eqz v0, :cond_0

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;->v:Lm5/e;

    :cond_0
    invoke-static {}, Lcom/android/packageinstaller/utils/x;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    const-class v0, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;

    :goto_0
    iput-object v0, p0, Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;->x:Ljava/lang/Class;

    goto :goto_1

    :cond_1
    invoke-static {p0}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->isSafeModelEnable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-class v0, Lcom/miui/packageInstaller/PurePackageInstallerActivity;

    goto :goto_0

    :cond_2
    :goto_1
    return-void
.end method

.method private final P0()V
    .locals 5

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    const/high16 v1, 0x2000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v2, 0x1

    const/16 v3, 0x1d

    if-le v1, v3, :cond_0

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    const-string v3, "content"

    invoke-static {v3, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :try_start_0
    invoke-virtual {p0}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {p0, v1, v3, v2}, Landroid/app/Activity;->grantUriPermission(Ljava/lang/String;Landroid/net/Uri;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;->v:Lm5/e;

    const-string v2, "EXTRA_CALLING_PACKAGE"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;->x:Ljava/lang/Class;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    :try_start_1
    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    const-string v2, "getDefault()"

    invoke-static {v1, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "this as java.lang.String).toLowerCase(locale)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x2

    const/4 v2, 0x0

    const-string v3, "not have permission"

    const/4 v4, 0x0

    invoke-static {v0, v3, v4, v1, v2}, Lu8/g;->A(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZILjava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f1103dd

    invoke-static {p0, v0, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_2
    :goto_1
    invoke-virtual {p0}, Lmiuix/appcompat/app/j;->finish()V

    return-void
.end method

.method private static final R0(Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;)V
    .locals 2

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;->N0()V

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object v0

    new-instance v1, Lz5/l;

    invoke-direct {v1, p0}, Lz5/l;-><init>(Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;)V

    invoke-virtual {v0, v1}, Lf6/z;->e(Ljava/lang/Runnable;)V

    return-void
.end method

.method private static final S0(Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;)V
    .locals 2

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;->u:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const-string v1, "alerts.iterator()"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;->U0(Ljava/util/Iterator;)V

    return-void
.end method

.method private final U0(Ljava/util/Iterator;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Iterator<",
            "+",
            "Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity$b;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity$b;

    new-instance v1, Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity$c;

    invoke-direct {v1, p0, p1}, Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity$c;-><init>(Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;Ljava/util/Iterator;)V

    invoke-interface {v0, p0, v1}, Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity$b;->a(Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity$a;)V

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;->P0()V

    :goto_0
    return-void
.end method


# virtual methods
.method public final M0()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setResult(I)V

    invoke-virtual {p0}, Lmiuix/appcompat/app/j;->finish()V

    return-void
.end method

.method public final Q0()Lm5/e;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;->v:Lm5/e;

    return-object v0
.end method

.method public final T0(Ljava/lang/String;)V
    .locals 1

    const-string v0, "ref"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;->w:Ljava/lang/String;

    return-void
.end method

.method public getRef()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;->w:Ljava/lang/String;

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lq2/b;->onCreate(Landroid/os/Bundle;)V

    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;->O0()V

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object p1

    new-instance v0, Lz5/k;

    invoke-direct {v0, p0}, Lz5/k;-><init>(Lcom/miui/packageInstaller/ui/InstallPrepareAlertActivity;)V

    invoke-virtual {p1, v0}, Lf6/z;->g(Ljava/lang/Runnable;)V

    return-void
.end method
