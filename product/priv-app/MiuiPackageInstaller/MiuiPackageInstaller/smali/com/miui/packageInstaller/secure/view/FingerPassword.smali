.class public final Lcom/miui/packageInstaller/secure/view/FingerPassword;
.super Landroid/widget/FrameLayout;

# interfaces
.implements Lx5/c;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/packageInstaller/secure/view/FingerPassword$a;
    }
.end annotation


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Lcom/airbnb/lottie/LottieAnimationView;

.field private c:Lh6/a;

.field private d:Lx5/h;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/miui/packageInstaller/secure/view/FingerPassword;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance p1, Lh6/a;

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-direct {p1, p2}, Lh6/a;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/secure/view/FingerPassword;->c:Lh6/a;

    return-void
.end method

.method public static final synthetic b(Lcom/miui/packageInstaller/secure/view/FingerPassword;)Lh6/a;
    .locals 0

    iget-object p0, p0, Lcom/miui/packageInstaller/secure/view/FingerPassword;->c:Lh6/a;

    return-object p0
.end method

.method public static final synthetic c(Lcom/miui/packageInstaller/secure/view/FingerPassword;)Landroid/widget/TextView;
    .locals 0

    iget-object p0, p0, Lcom/miui/packageInstaller/secure/view/FingerPassword;->a:Landroid/widget/TextView;

    return-object p0
.end method

.method public static synthetic f(Lcom/miui/packageInstaller/secure/view/FingerPassword;Ll8/a;ILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    const/4 p1, 0x0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/miui/packageInstaller/secure/view/FingerPassword;->e(Ll8/a;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/secure/view/FingerPassword;->c:Lh6/a;

    invoke-virtual {v0}, Lh6/a;->a()V

    return-void
.end method

.method public d(Lx5/h;Ll8/l;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lx5/h;",
            "Ll8/l<",
            "-",
            "Ljava/lang/Integer;",
            "La8/v;",
            ">;)V"
        }
    .end annotation

    const-string v0, "dialog"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "callback"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/secure/view/FingerPassword;->d:Lx5/h;

    iget-object v0, p0, Lcom/miui/packageInstaller/secure/view/FingerPassword;->c:Lh6/a;

    new-instance v1, Lcom/miui/packageInstaller/secure/view/FingerPassword$b;

    invoke-direct {v1, p0, p2, p1}, Lcom/miui/packageInstaller/secure/view/FingerPassword$b;-><init>(Lcom/miui/packageInstaller/secure/view/FingerPassword;Ll8/l;Lx5/h;)V

    invoke-virtual {v0, v1}, Lh6/a;->c(Lh6/b;)V

    return-void
.end method

.method public final e(Ll8/a;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ll8/a<",
            "La8/v;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/miui/packageInstaller/secure/view/FingerPassword;->a:Landroid/widget/TextView;

    const/4 v1, 0x0

    const-string v2, "tipsTextView"

    if-nez v0, :cond_0

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_0
    invoke-virtual {v0}, Landroid/widget/TextView;->getTranslationX()F

    iget-object v0, p0, Lcom/miui/packageInstaller/secure/view/FingerPassword;->a:Landroid/widget/TextView;

    if-nez v0, :cond_1

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move-object v1, v0

    :goto_0
    const/4 v0, 0x2

    new-array v0, v0, [F

    const/4 v2, 0x0

    const/4 v3, 0x0

    aput v3, v0, v2

    const/4 v2, 0x1

    const/high16 v3, 0x41600000    # 14.0f

    invoke-static {v3}, Lf6/d;->a(F)I

    move-result v3

    int-to-float v3, v3

    aput v3, v0, v2

    const-string v2, "translationX"

    invoke-static {v1, v2, v0}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    new-instance v1, Lcom/miui/packageInstaller/secure/view/FingerPassword$a;

    const/high16 v2, 0x40200000    # 2.5f

    invoke-direct {v1, v2}, Lcom/miui/packageInstaller/secure/view/FingerPassword$a;-><init>(F)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    const-wide/16 v1, 0x3d4

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    new-instance v1, Lcom/miui/packageInstaller/secure/view/FingerPassword$c;

    invoke-direct {v1, p1}, Lcom/miui/packageInstaller/secure/view/FingerPassword$c;-><init>(Ll8/a;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    return-void
.end method

.method public final g(Ll8/a;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ll8/a<",
            "La8/v;",
            ">;)V"
        }
    .end annotation

    const-string v0, "onAnimDone"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/secure/view/FingerPassword;->a:Landroid/widget/TextView;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const-string v0, "tipsTextView"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_0
    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f110118

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/secure/view/FingerPassword;->b:Lcom/airbnb/lottie/LottieAnimationView;

    const-string v2, "fingerIconView"

    if-nez v0, :cond_1

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_1
    const-string v3, "finger_authorization_success.json"

    invoke-virtual {v0, v3}, Lcom/airbnb/lottie/LottieAnimationView;->setAnimation(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/secure/view/FingerPassword;->b:Lcom/airbnb/lottie/LottieAnimationView;

    if-nez v0, :cond_2

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_2
    invoke-virtual {v0}, Lcom/airbnb/lottie/LottieAnimationView;->p()V

    iget-object v0, p0, Lcom/miui/packageInstaller/secure/view/FingerPassword;->b:Lcom/airbnb/lottie/LottieAnimationView;

    if-nez v0, :cond_3

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_3
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/miui/packageInstaller/secure/view/FingerPassword;->b:Lcom/airbnb/lottie/LottieAnimationView;

    if-nez v0, :cond_4

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    move-object v1, v0

    :goto_0
    new-instance v0, Lcom/miui/packageInstaller/secure/view/FingerPassword$d;

    invoke-direct {v0, p1}, Lcom/miui/packageInstaller/secure/view/FingerPassword$d;-><init>(Ll8/a;)V

    invoke-virtual {v1, v0}, Lcom/airbnb/lottie/LottieAnimationView;->f(Landroid/animation/Animator$AnimatorListener;)V

    return-void
.end method

.method public final getDialog()Lx5/h;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/secure/view/FingerPassword;->d:Lx5/h;

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 4

    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    const v0, 0x7f0a016b

    invoke-virtual {p0, v0}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.finger_tips)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/secure/view/FingerPassword;->a:Landroid/widget/TextView;

    const v0, 0x7f0a0169

    invoke-virtual {p0, v0}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.finger_icon)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/airbnb/lottie/LottieAnimationView;

    iput-object v0, p0, Lcom/miui/packageInstaller/secure/view/FingerPassword;->b:Lcom/airbnb/lottie/LottieAnimationView;

    const-string v1, "fingerIconView"

    const/4 v2, 0x0

    if-nez v0, :cond_0

    invoke-static {v1}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v2

    :cond_0
    const v3, 0x7f080191

    invoke-virtual {v0, v3}, Lcom/airbnb/lottie/LottieAnimationView;->setImageResource(I)V

    sget-boolean v0, Lcom/android/packageinstaller/utils/g;->h:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v0, v0, v0}, Landroid/widget/FrameLayout;->setPadding(IIII)V

    iget-object v0, p0, Lcom/miui/packageInstaller/secure/view/FingerPassword;->b:Lcom/airbnb/lottie/LottieAnimationView;

    if-nez v0, :cond_1

    invoke-static {v1}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v2

    :cond_1
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_2
    iget-object v0, p0, Lcom/miui/packageInstaller/secure/view/FingerPassword;->a:Landroid/widget/TextView;

    if-nez v0, :cond_3

    const-string v0, "tipsTextView"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    move-object v2, v0

    :goto_0
    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f11011a

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public release()V
    .locals 1

    invoke-static {p0}, Lx5/c$a;->a(Lx5/c;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/secure/view/FingerPassword;->c:Lh6/a;

    invoke-virtual {v0}, Lh6/a;->a()V

    return-void
.end method

.method public setCancelButtonText(Ljava/lang/CharSequence;)V
    .locals 0

    invoke-static {p0, p1}, Lx5/c$a;->b(Lx5/c;Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setConfirmButtonText(Ljava/lang/CharSequence;)V
    .locals 0

    invoke-static {p0, p1}, Lx5/c$a;->c(Lx5/c;Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final setDialog(Lx5/h;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/secure/view/FingerPassword;->d:Lx5/h;

    return-void
.end method

.method public setTipMsgText(Ljava/lang/CharSequence;)V
    .locals 0

    invoke-static {p0, p1}, Lx5/c$a;->d(Lx5/c;Ljava/lang/CharSequence;)V

    return-void
.end method
