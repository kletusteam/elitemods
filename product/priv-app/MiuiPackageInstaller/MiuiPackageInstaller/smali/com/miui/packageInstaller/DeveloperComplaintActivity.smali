.class public final Lcom/miui/packageInstaller/DeveloperComplaintActivity;
.super Lm5/p1;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/packageInstaller/DeveloperComplaintActivity$a;
    }
.end annotation


# static fields
.field public static final J:Lcom/miui/packageInstaller/DeveloperComplaintActivity$a;


# instance fields
.field private A:Lcom/miui/packageInstaller/view/DevEditText;

.field private B:Lcom/miui/packageInstaller/view/DevEditText;

.field private C:Landroid/widget/Spinner;

.field private D:Landroid/widget/Button;

.field private E:Lcom/miui/packageInstaller/view/ComplaintContentView;

.field private F:Li6/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Li6/a<",
            "*>;"
        }
    .end annotation
.end field

.field private G:I

.field private final H:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private I:[Ljava/lang/String;

.field private w:Lcom/miui/packageInstaller/view/DevEditText;

.field private x:Lcom/miui/packageInstaller/view/DevEditText;

.field private y:Lcom/miui/packageInstaller/view/DevEditText;

.field private z:Lcom/miui/packageInstaller/view/DevEditText;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/miui/packageInstaller/DeveloperComplaintActivity$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/miui/packageInstaller/DeveloperComplaintActivity$a;-><init>(Lm8/g;)V

    sput-object v0, Lcom/miui/packageInstaller/DeveloperComplaintActivity;->J:Lcom/miui/packageInstaller/DeveloperComplaintActivity$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 9

    invoke-direct {p0}, Lm5/p1;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/packageInstaller/DeveloperComplaintActivity;->H:Ljava/util/List;

    const-string v1, ""

    const-string v2, ""

    const-string v3, ""

    const-string v4, ""

    const-string v5, ""

    const-string v6, ""

    const-string v7, ""

    const-string v8, ""

    filled-new-array/range {v1 .. v8}, [Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/packageInstaller/DeveloperComplaintActivity;->I:[Ljava/lang/String;

    return-void
.end method

.method public static synthetic L0(Lcom/miui/packageInstaller/DeveloperComplaintActivity;)V
    .locals 0

    invoke-static {p0}, Lcom/miui/packageInstaller/DeveloperComplaintActivity;->Y0(Lcom/miui/packageInstaller/DeveloperComplaintActivity;)V

    return-void
.end method

.method public static synthetic M0()V
    .locals 0

    invoke-static {}, Lcom/miui/packageInstaller/DeveloperComplaintActivity;->f1()V

    return-void
.end method

.method public static synthetic N0(Lcom/miui/packageInstaller/DeveloperComplaintActivity;)V
    .locals 0

    invoke-static {p0}, Lcom/miui/packageInstaller/DeveloperComplaintActivity;->X0(Lcom/miui/packageInstaller/DeveloperComplaintActivity;)V

    return-void
.end method

.method public static synthetic O0(Ljava/util/HashMap;Lcom/miui/packageInstaller/DeveloperComplaintActivity;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/miui/packageInstaller/DeveloperComplaintActivity;->e1(Ljava/util/HashMap;Lcom/miui/packageInstaller/DeveloperComplaintActivity;)V

    return-void
.end method

.method public static synthetic P0(Lcom/miui/packageInstaller/DeveloperComplaintActivity;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/miui/packageInstaller/DeveloperComplaintActivity;->a1(Lcom/miui/packageInstaller/DeveloperComplaintActivity;Landroid/view/View;)V

    return-void
.end method

.method public static final synthetic Q0(Lcom/miui/packageInstaller/DeveloperComplaintActivity;)I
    .locals 0

    iget p0, p0, Lcom/miui/packageInstaller/DeveloperComplaintActivity;->G:I

    return p0
.end method

.method public static final synthetic R0(Lcom/miui/packageInstaller/DeveloperComplaintActivity;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lcom/miui/packageInstaller/DeveloperComplaintActivity;->H:Ljava/util/List;

    return-object p0
.end method

.method public static final synthetic S0(Lcom/miui/packageInstaller/DeveloperComplaintActivity;)[Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/miui/packageInstaller/DeveloperComplaintActivity;->I:[Ljava/lang/String;

    return-object p0
.end method

.method public static final synthetic T0(Lcom/miui/packageInstaller/DeveloperComplaintActivity;I)V
    .locals 0

    iput p1, p0, Lcom/miui/packageInstaller/DeveloperComplaintActivity;->G:I

    return-void
.end method

.method private final U0()Z
    .locals 9

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sget-object v2, Lf6/s;->a:Lf6/s$a;

    invoke-virtual {v2}, Lf6/s$a;->a()Lf6/s;

    move-result-object v3

    const-string v4, "dev_com_submit_time"

    invoke-virtual {v3, v4}, Lf6/s;->f(Ljava/lang/String;)J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v7, v3, v5

    const/4 v8, 0x1

    if-nez v7, :cond_0

    return v8

    :cond_0
    invoke-direct {p0, v0, v1, v3, v4}, Lcom/miui/packageInstaller/DeveloperComplaintActivity;->b1(JJ)Z

    move-result v0

    const-string v1, "dev_com_submit_num"

    if-eqz v0, :cond_2

    invoke-virtual {v2}, Lf6/s$a;->a()Lf6/s;

    move-result-object v0

    invoke-virtual {v0, v1}, Lf6/s;->f(Ljava/lang/String;)J

    move-result-wide v0

    const-wide/16 v2, 0x2

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v8, 0x0

    :goto_0
    return v8

    :cond_2
    invoke-virtual {v2}, Lf6/s$a;->a()Lf6/s;

    move-result-object v0

    invoke-virtual {v0, v1, v5, v6}, Lf6/s;->j(Ljava/lang/String;J)V

    return v8
.end method

.method private final W0()V
    .locals 2

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object v0

    new-instance v1, Lm5/l;

    invoke-direct {v1, p0}, Lm5/l;-><init>(Lcom/miui/packageInstaller/DeveloperComplaintActivity;)V

    invoke-virtual {v0, v1}, Lf6/z;->g(Ljava/lang/Runnable;)V

    return-void
.end method

.method private static final X0(Lcom/miui/packageInstaller/DeveloperComplaintActivity;)V
    .locals 4

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    :try_start_0
    sget-object v0, Lj2/e;->g:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/packageinstaller/utils/n;->f(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/android/packageinstaller/utils/j;->b()Lp4/e;

    move-result-object v2

    const-class v3, Lcom/miui/packageInstaller/model/RiskTypeResult;

    invoke-virtual {v2, v0, v3}, Lp4/e;->h(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/packageInstaller/model/RiskTypeResult;

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/RiskTypeResult;->getCode()I

    move-result v2

    const/16 v3, 0xc8

    if-ne v2, v3, :cond_0

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/RiskTypeResult;->getData()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    xor-int/2addr v1, v2

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/miui/packageInstaller/DeveloperComplaintActivity;->H:Ljava/util/List;

    const v2, 0x7f1100d3

    invoke-virtual {p0, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "getString(R.string.dev_com_risk_choose)"

    invoke-static {v2, v3}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/miui/packageInstaller/DeveloperComplaintActivity;->H:Ljava/util/List;

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/RiskTypeResult;->getData()Ljava/util/List;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object v0

    new-instance v1, Lm5/k;

    invoke-direct {v1, p0}, Lm5/k;-><init>(Lcom/miui/packageInstaller/DeveloperComplaintActivity;)V

    invoke-virtual {v0, v1}, Lf6/z;->e(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    invoke-virtual {p0}, Ljava/lang/Exception;->printStackTrace()V

    :cond_0
    :goto_0
    return-void
.end method

.method private static final Y0(Lcom/miui/packageInstaller/DeveloperComplaintActivity;)V
    .locals 5

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Li6/a;->a:Li6/a$a;

    iget-object v1, p0, Lcom/miui/packageInstaller/DeveloperComplaintActivity;->H:Ljava/util/List;

    const/4 v2, 0x0

    new-array v3, v2, [Ljava/lang/String;

    invoke-interface {v1, v3}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    const-string v3, "null cannot be cast to non-null type kotlin.Array<T of kotlin.collections.ArraysKt__ArraysJVMKt.toTypedArray>"

    invoke-static {v1, v3}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, [Ljava/lang/String;

    const v3, 0x7f0d01b0

    invoke-virtual {v0, p0, v3, v1}, Li6/a$a;->a(Landroid/content/Context;I[Ljava/lang/String;)Li6/a;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/packageInstaller/DeveloperComplaintActivity;->F:Li6/a;

    iget-object v0, p0, Lcom/miui/packageInstaller/DeveloperComplaintActivity;->C:Landroid/widget/Spinner;

    const-string v1, "mSpinner"

    const/4 v3, 0x0

    if-nez v0, :cond_0

    invoke-static {v1}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v3

    :cond_0
    iget-object v4, p0, Lcom/miui/packageInstaller/DeveloperComplaintActivity;->F:Li6/a;

    if-nez v4, :cond_1

    const-string v4, "mAdapter"

    invoke-static {v4}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v4, v3

    :cond_1
    invoke-virtual {v0, v4}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object p0, p0, Lcom/miui/packageInstaller/DeveloperComplaintActivity;->C:Landroid/widget/Spinner;

    if-nez p0, :cond_2

    invoke-static {v1}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    move-object v3, p0

    :goto_0
    invoke-virtual {v3, v2}, Landroid/widget/Spinner;->setSelection(I)V

    return-void
.end method

.method private static final a1(Lcom/miui/packageInstaller/DeveloperComplaintActivity;Landroid/view/View;)V
    .locals 2

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Lp5/b;

    const-string v0, "submit_appeal_btn"

    const-string v1, "button"

    invoke-direct {p1, v0, v1, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p1}, Lp5/f;->c()Z

    invoke-virtual {p0}, Lcom/miui/packageInstaller/DeveloperComplaintActivity;->d1()V

    return-void
.end method

.method private final b1(JJ)Z
    .locals 5

    const-string v0, "yyyy-MM-dd  HH:mm:ss"

    :try_start_0
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    new-instance v3, Ljava/text/SimpleDateFormat;

    invoke-direct {v3, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/text/SimpleDateFormat;

    invoke-direct {v4, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {v3, p1}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-virtual {v4, p2}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v3, p1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p1

    invoke-virtual {v4, p2}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p2

    invoke-virtual {v1, p1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    invoke-virtual {v2, p2}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    invoke-direct {p0, v1, v2}, Lcom/miui/packageInstaller/DeveloperComplaintActivity;->c1(Ljava/util/Calendar;Ljava/util/Calendar;)Z

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    const/4 p1, 0x0

    return p1
.end method

.method private final c1(Ljava/util/Calendar;Ljava/util/Calendar;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p1, v1}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-virtual {p2, v1}, Ljava/util/Calendar;->get(I)I

    move-result v3

    if-ne v2, v3, :cond_0

    invoke-virtual {p1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-virtual {p2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v3

    if-ne v2, v3, :cond_0

    const/4 v2, 0x6

    invoke-virtual {p1, v2}, Ljava/util/Calendar;->get(I)I

    move-result p1

    invoke-virtual {p2, v2}, Ljava/util/Calendar;->get(I)I

    move-result p2

    if-ne p1, p2, :cond_0

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_0
    return v0
.end method

.method private static final e1(Ljava/util/HashMap;Lcom/miui/packageInstaller/DeveloperComplaintActivity;)V
    .locals 1

    const-string v0, "$map"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "this$0"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    :try_start_0
    const-class v0, Lu5/j;

    invoke-static {v0}, Lu5/k;->f(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lu5/j;

    invoke-interface {v0, p0}, Lu5/j;->d(Ljava/util/Map;)Lgc/b;

    move-result-object p0

    new-instance v0, Lcom/miui/packageInstaller/DeveloperComplaintActivity$j;

    invoke-direct {v0, p1}, Lcom/miui/packageInstaller/DeveloperComplaintActivity$j;-><init>(Lcom/miui/packageInstaller/DeveloperComplaintActivity;)V

    invoke-interface {p0, v0}, Lgc/b;->M(Lgc/d;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    invoke-virtual {p0}, Ljava/lang/Exception;->printStackTrace()V

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object p0

    sget-object p1, Lm5/n;->a:Lm5/n;

    invoke-virtual {p0, p1}, Lf6/z;->e(Ljava/lang/Runnable;)V

    :goto_0
    return-void
.end method

.method private static final f1()V
    .locals 3

    invoke-static {}, Lcom/android/packageinstaller/InstallerApplication;->g()Lcom/android/packageinstaller/InstallerApplication;

    move-result-object v0

    const v1, 0x7f110119

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method


# virtual methods
.method public E0()Ljava/lang/String;
    .locals 1

    const-string v0, "pioneer_appeal"

    return-object v0
.end method

.method public final V0()V
    .locals 7

    iget-object v0, p0, Lcom/miui/packageInstaller/DeveloperComplaintActivity;->I:[Ljava/lang/String;

    array-length v1, v0

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v1, :cond_3

    aget-object v4, v0, v3

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    const/4 v5, 0x0

    const-string v6, "mSubmitBtn"

    if-eqz v4, :cond_1

    iget-object v0, p0, Lcom/miui/packageInstaller/DeveloperComplaintActivity;->D:Landroid/widget/Button;

    if-nez v0, :cond_0

    invoke-static {v6}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_1

    :cond_0
    move-object v5, v0

    :goto_1
    invoke-virtual {v5, v2}, Landroid/widget/Button;->setEnabled(Z)V

    return-void

    :cond_1
    iget-object v4, p0, Lcom/miui/packageInstaller/DeveloperComplaintActivity;->D:Landroid/widget/Button;

    if-nez v4, :cond_2

    invoke-static {v6}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_2

    :cond_2
    move-object v5, v4

    :goto_2
    const/4 v4, 0x1

    invoke-virtual {v5, v4}, Landroid/widget/Button;->setEnabled(Z)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_3
    return-void
.end method

.method public final Z0()V
    .locals 7

    const v0, 0x7f0a0118

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.dev_com_edit_appname)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/miui/packageInstaller/view/DevEditText;

    iput-object v0, p0, Lcom/miui/packageInstaller/DeveloperComplaintActivity;->w:Lcom/miui/packageInstaller/view/DevEditText;

    const v0, 0x7f0a011f

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.dev_com_edit_pkgname)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/miui/packageInstaller/view/DevEditText;

    iput-object v0, p0, Lcom/miui/packageInstaller/DeveloperComplaintActivity;->x:Lcom/miui/packageInstaller/view/DevEditText;

    const v0, 0x7f0a011a

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.dev_com_edit_devname)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/miui/packageInstaller/view/DevEditText;

    iput-object v0, p0, Lcom/miui/packageInstaller/DeveloperComplaintActivity;->y:Lcom/miui/packageInstaller/view/DevEditText;

    const v0, 0x7f0a0119

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.dev_com_edit_contact)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/miui/packageInstaller/view/DevEditText;

    iput-object v0, p0, Lcom/miui/packageInstaller/DeveloperComplaintActivity;->z:Lcom/miui/packageInstaller/view/DevEditText;

    const v0, 0x7f0a011c

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.dev_com_edit_mail)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/miui/packageInstaller/view/DevEditText;

    iput-object v0, p0, Lcom/miui/packageInstaller/DeveloperComplaintActivity;->A:Lcom/miui/packageInstaller/view/DevEditText;

    const v0, 0x7f0a011e

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.dev_com_edit_phone)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/miui/packageInstaller/view/DevEditText;

    iput-object v0, p0, Lcom/miui/packageInstaller/DeveloperComplaintActivity;->B:Lcom/miui/packageInstaller/view/DevEditText;

    const v0, 0x7f0a0116

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.dev_com_content_view)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/miui/packageInstaller/view/ComplaintContentView;

    iput-object v0, p0, Lcom/miui/packageInstaller/DeveloperComplaintActivity;->E:Lcom/miui/packageInstaller/view/ComplaintContentView;

    const v0, 0x7f0a0112

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.dev_com_button)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/miui/packageInstaller/DeveloperComplaintActivity;->D:Landroid/widget/Button;

    const-string v1, "mSubmitBtn"

    const/4 v2, 0x0

    if-nez v0, :cond_0

    invoke-static {v1}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v2

    :cond_0
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    const v0, 0x7f0a0122

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v4, "findViewById(R.id.dev_com_spinner)"

    invoke-static {v0, v4}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/miui/packageInstaller/DeveloperComplaintActivity;->C:Landroid/widget/Spinner;

    new-instance v0, Lp5/g;

    const-string v4, "submit_appeal_btn"

    const-string v5, "button"

    invoke-direct {v0, v4, v5, p0}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    iget-object v0, p0, Lcom/miui/packageInstaller/DeveloperComplaintActivity;->D:Landroid/widget/Button;

    if-nez v0, :cond_1

    invoke-static {v1}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v2

    :cond_1
    new-instance v1, Lm5/j;

    invoke-direct {v1, p0}, Lm5/j;-><init>(Lcom/miui/packageInstaller/DeveloperComplaintActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/DeveloperComplaintActivity;->C:Landroid/widget/Spinner;

    const-string v1, "mSpinner"

    if-nez v0, :cond_2

    invoke-static {v1}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v2

    :cond_2
    new-instance v4, Lcom/miui/packageInstaller/DeveloperComplaintActivity$b;

    invoke-direct {v4, p0}, Lcom/miui/packageInstaller/DeveloperComplaintActivity$b;-><init>(Lcom/miui/packageInstaller/DeveloperComplaintActivity;)V

    invoke-virtual {v0, v4}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    sget-object v0, Li6/a;->a:Li6/a$a;

    const v4, 0x7f0d01b0

    iget-object v5, p0, Lcom/miui/packageInstaller/DeveloperComplaintActivity;->H:Ljava/util/List;

    new-array v6, v3, [Ljava/lang/String;

    invoke-interface {v5, v6}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    const-string v6, "null cannot be cast to non-null type kotlin.Array<T of kotlin.collections.ArraysKt__ArraysJVMKt.toTypedArray>"

    invoke-static {v5, v6}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v5, [Ljava/lang/String;

    invoke-virtual {v0, p0, v4, v5}, Li6/a$a;->a(Landroid/content/Context;I[Ljava/lang/String;)Li6/a;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/packageInstaller/DeveloperComplaintActivity;->F:Li6/a;

    iget-object v0, p0, Lcom/miui/packageInstaller/DeveloperComplaintActivity;->C:Landroid/widget/Spinner;

    if-nez v0, :cond_3

    invoke-static {v1}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v2

    :cond_3
    iget-object v4, p0, Lcom/miui/packageInstaller/DeveloperComplaintActivity;->F:Li6/a;

    if-nez v4, :cond_4

    const-string v4, "mAdapter"

    invoke-static {v4}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v4, v2

    :cond_4
    invoke-virtual {v0, v4}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/DeveloperComplaintActivity;->C:Landroid/widget/Spinner;

    if-nez v0, :cond_5

    invoke-static {v1}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v2

    :cond_5
    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setSelection(I)V

    iget-object v0, p0, Lcom/miui/packageInstaller/DeveloperComplaintActivity;->w:Lcom/miui/packageInstaller/view/DevEditText;

    if-nez v0, :cond_6

    const-string v0, "mAppNameEt"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v2

    :cond_6
    new-instance v1, Lcom/miui/packageInstaller/DeveloperComplaintActivity$c;

    invoke-direct {v1, p0}, Lcom/miui/packageInstaller/DeveloperComplaintActivity$c;-><init>(Lcom/miui/packageInstaller/DeveloperComplaintActivity;)V

    invoke-virtual {v0, v1}, Lcom/miui/packageInstaller/view/DevEditText;->setTextChangedListener(Lcom/miui/packageInstaller/view/DevEditText$b;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/DeveloperComplaintActivity;->x:Lcom/miui/packageInstaller/view/DevEditText;

    if-nez v0, :cond_7

    const-string v0, "mPkgNameEt"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v2

    :cond_7
    new-instance v1, Lcom/miui/packageInstaller/DeveloperComplaintActivity$d;

    invoke-direct {v1, p0}, Lcom/miui/packageInstaller/DeveloperComplaintActivity$d;-><init>(Lcom/miui/packageInstaller/DeveloperComplaintActivity;)V

    invoke-virtual {v0, v1}, Lcom/miui/packageInstaller/view/DevEditText;->setTextChangedListener(Lcom/miui/packageInstaller/view/DevEditText$b;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/DeveloperComplaintActivity;->y:Lcom/miui/packageInstaller/view/DevEditText;

    if-nez v0, :cond_8

    const-string v0, "mDevNameEt"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v2

    :cond_8
    new-instance v1, Lcom/miui/packageInstaller/DeveloperComplaintActivity$e;

    invoke-direct {v1, p0}, Lcom/miui/packageInstaller/DeveloperComplaintActivity$e;-><init>(Lcom/miui/packageInstaller/DeveloperComplaintActivity;)V

    invoke-virtual {v0, v1}, Lcom/miui/packageInstaller/view/DevEditText;->setTextChangedListener(Lcom/miui/packageInstaller/view/DevEditText$b;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/DeveloperComplaintActivity;->z:Lcom/miui/packageInstaller/view/DevEditText;

    if-nez v0, :cond_9

    const-string v0, "mContactEt"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v2

    :cond_9
    new-instance v1, Lcom/miui/packageInstaller/DeveloperComplaintActivity$f;

    invoke-direct {v1, p0}, Lcom/miui/packageInstaller/DeveloperComplaintActivity$f;-><init>(Lcom/miui/packageInstaller/DeveloperComplaintActivity;)V

    invoke-virtual {v0, v1}, Lcom/miui/packageInstaller/view/DevEditText;->setTextChangedListener(Lcom/miui/packageInstaller/view/DevEditText$b;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/DeveloperComplaintActivity;->A:Lcom/miui/packageInstaller/view/DevEditText;

    if-nez v0, :cond_a

    const-string v0, "mMailTv"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v2

    :cond_a
    new-instance v1, Lcom/miui/packageInstaller/DeveloperComplaintActivity$g;

    invoke-direct {v1, p0}, Lcom/miui/packageInstaller/DeveloperComplaintActivity$g;-><init>(Lcom/miui/packageInstaller/DeveloperComplaintActivity;)V

    invoke-virtual {v0, v1}, Lcom/miui/packageInstaller/view/DevEditText;->setTextChangedListener(Lcom/miui/packageInstaller/view/DevEditText$b;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/DeveloperComplaintActivity;->B:Lcom/miui/packageInstaller/view/DevEditText;

    if-nez v0, :cond_b

    const-string v0, "mPhoneTv"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v2

    :cond_b
    new-instance v1, Lcom/miui/packageInstaller/DeveloperComplaintActivity$h;

    invoke-direct {v1, p0}, Lcom/miui/packageInstaller/DeveloperComplaintActivity$h;-><init>(Lcom/miui/packageInstaller/DeveloperComplaintActivity;)V

    invoke-virtual {v0, v1}, Lcom/miui/packageInstaller/view/DevEditText;->setTextChangedListener(Lcom/miui/packageInstaller/view/DevEditText$b;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/DeveloperComplaintActivity;->E:Lcom/miui/packageInstaller/view/ComplaintContentView;

    if-nez v0, :cond_c

    const-string v0, "mContentView"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_0

    :cond_c
    move-object v2, v0

    :goto_0
    new-instance v0, Lcom/miui/packageInstaller/DeveloperComplaintActivity$i;

    invoke-direct {v0, p0}, Lcom/miui/packageInstaller/DeveloperComplaintActivity$i;-><init>(Lcom/miui/packageInstaller/DeveloperComplaintActivity;)V

    invoke-virtual {v2, v0}, Lcom/miui/packageInstaller/view/ComplaintContentView;->setTextChangedListener(Lcom/miui/packageInstaller/view/DevEditText$b;)V

    return-void
.end method

.method public final d1()V
    .locals 11

    invoke-direct {p0}, Lcom/miui/packageInstaller/DeveloperComplaintActivity;->U0()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const v0, 0x7f1100d5

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/miui/packageInstaller/DeveloperComplaintActivity;->w:Lcom/miui/packageInstaller/view/DevEditText;

    const/4 v2, 0x0

    if-nez v0, :cond_1

    const-string v0, "mAppNameEt"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v2

    :cond_1
    invoke-virtual {v0}, Lcom/miui/packageInstaller/view/DevEditText;->getEditText()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/miui/packageInstaller/DeveloperComplaintActivity;->x:Lcom/miui/packageInstaller/view/DevEditText;

    if-nez v3, :cond_2

    const-string v3, "mPkgNameEt"

    invoke-static {v3}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v3, v2

    :cond_2
    invoke-virtual {v3}, Lcom/miui/packageInstaller/view/DevEditText;->getEditText()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/miui/packageInstaller/DeveloperComplaintActivity;->y:Lcom/miui/packageInstaller/view/DevEditText;

    if-nez v4, :cond_3

    const-string v4, "mDevNameEt"

    invoke-static {v4}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v4, v2

    :cond_3
    invoke-virtual {v4}, Lcom/miui/packageInstaller/view/DevEditText;->getEditText()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/miui/packageInstaller/DeveloperComplaintActivity;->z:Lcom/miui/packageInstaller/view/DevEditText;

    if-nez v5, :cond_4

    const-string v5, "mContactEt"

    invoke-static {v5}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v5, v2

    :cond_4
    invoke-virtual {v5}, Lcom/miui/packageInstaller/view/DevEditText;->getEditText()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/miui/packageInstaller/DeveloperComplaintActivity;->A:Lcom/miui/packageInstaller/view/DevEditText;

    if-nez v6, :cond_5

    const-string v6, "mMailTv"

    invoke-static {v6}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v6, v2

    :cond_5
    invoke-virtual {v6}, Lcom/miui/packageInstaller/view/DevEditText;->getEditText()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/miui/packageInstaller/DeveloperComplaintActivity;->B:Lcom/miui/packageInstaller/view/DevEditText;

    if-nez v7, :cond_6

    const-string v7, "mPhoneTv"

    invoke-static {v7}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v7, v2

    :cond_6
    invoke-virtual {v7}, Lcom/miui/packageInstaller/view/DevEditText;->getEditText()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/miui/packageInstaller/DeveloperComplaintActivity;->H:Ljava/util/List;

    iget v9, p0, Lcom/miui/packageInstaller/DeveloperComplaintActivity;->G:I

    invoke-interface {v8, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    iget-object v9, p0, Lcom/miui/packageInstaller/DeveloperComplaintActivity;->E:Lcom/miui/packageInstaller/view/ComplaintContentView;

    if-nez v9, :cond_7

    const-string v9, "mContentView"

    invoke-static {v9}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_0

    :cond_7
    move-object v2, v9

    :goto_0
    invoke-virtual {v2}, Lcom/miui/packageInstaller/view/ComplaintContentView;->getEditText()Ljava/lang/String;

    move-result-object v2

    sget-object v9, Lcom/miui/packageInstaller/view/DevEditText;->i:Lcom/miui/packageInstaller/view/DevEditText$a;

    invoke-virtual {v9, v6}, Lcom/miui/packageInstaller/view/DevEditText$a;->a(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_8

    const v0, 0x7f1100cc

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void

    :cond_8
    invoke-virtual {v9, v7}, Lcom/miui/packageInstaller/view/DevEditText$a;->b(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_9

    const v0, 0x7f1100cf

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void

    :cond_9
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    const-string v9, "appName"

    invoke-interface {v1, v9, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "packageName"

    invoke-interface {v1, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "devName"

    invoke-interface {v1, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "contactName"

    invoke-interface {v1, v0, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "email"

    invoke-interface {v1, v0, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "phone"

    invoke-interface {v1, v0, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "riskType"

    invoke-interface {v1, v0, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v2}, Lm8/i;->c(Ljava/lang/Object;)V

    const-string v0, "appealContent"

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    const-string v2, "timestamp"

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object v0

    new-instance v2, Lm5/m;

    invoke-direct {v2, v1, p0}, Lm5/m;-><init>(Ljava/util/HashMap;Lcom/miui/packageInstaller/DeveloperComplaintActivity;)V

    invoke-virtual {v0, v2}, Lf6/z;->g(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onBackPressed()V
    .locals 3

    invoke-super {p0}, Lmiuix/appcompat/app/j;->onBackPressed()V

    new-instance v0, Lp5/b;

    const-string v1, "page_back_btn"

    const-string v2, "button"

    invoke-direct {v0, v1, v2, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    const-string v1, "back_type"

    const-string v2, "system"

    invoke-virtual {v0, v1, v2}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object v0

    invoke-virtual {v0}, Lp5/f;->c()Z

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Lm5/p1;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0d001e

    invoke-virtual {p0, p1}, Lmiuix/appcompat/app/j;->setContentView(I)V

    const p1, 0x7f1100d6

    invoke-virtual {p0, p1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lm5/p1;->J0(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/miui/packageInstaller/DeveloperComplaintActivity;->Z0()V

    invoke-direct {p0}, Lcom/miui/packageInstaller/DeveloperComplaintActivity;->W0()V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    const-string v0, "item"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    new-instance v0, Lp5/b;

    const-string v1, "page_back_btn"

    const-string v2, "button"

    invoke-direct {v0, v1, v2, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    const-string v1, "back_type"

    const-string v2, "click_icon"

    invoke-virtual {v0, v1, v2}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object v0

    invoke-virtual {v0}, Lp5/f;->c()Z

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result p1

    return p1
.end method
