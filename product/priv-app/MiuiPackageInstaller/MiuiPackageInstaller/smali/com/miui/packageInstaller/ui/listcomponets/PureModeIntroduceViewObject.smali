.class public final Lcom/miui/packageInstaller/ui/listcomponets/PureModeIntroduceViewObject;
.super Lm6/a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/packageInstaller/ui/listcomponets/PureModeIntroduceViewObject$a;,
        Lcom/miui/packageInstaller/ui/listcomponets/PureModeIntroduceViewObject$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lm6/a<",
        "Lcom/miui/packageInstaller/ui/listcomponets/PureModeIntroduceViewObject$ViewHolder;",
        ">;"
    }
.end annotation


# static fields
.field public static final m:Lcom/miui/packageInstaller/ui/listcomponets/PureModeIntroduceViewObject$a;


# instance fields
.field private final l:Lcom/miui/packageInstaller/model/IntroductionModeBean;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeIntroduceViewObject$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/miui/packageInstaller/ui/listcomponets/PureModeIntroduceViewObject$a;-><init>(Lm8/g;)V

    sput-object v0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeIntroduceViewObject;->m:Lcom/miui/packageInstaller/ui/listcomponets/PureModeIntroduceViewObject$a;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/IntroductionModeBean;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "introductionMode"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0, v0}, Lm6/a;-><init>(Landroid/content/Context;Ljava/lang/Object;Ll6/c;Lm6/b;)V

    iput-object p2, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeIntroduceViewObject;->l:Lcom/miui/packageInstaller/model/IntroductionModeBean;

    return-void
.end method

.method private static final B(Lcom/miui/packageInstaller/ui/listcomponets/PureModeIntroduceViewObject;Landroid/view/View;)V
    .locals 4

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeIntroduceViewObject;->l:Lcom/miui/packageInstaller/model/IntroductionModeBean;

    invoke-virtual {p1}, Lcom/miui/packageInstaller/model/IntroductionModeBean;->getLabel()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f1102b8

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p1

    const-string v0, "null cannot be cast to non-null type com.miui.packageInstaller.analytics.IPage"

    const-string v1, "button"

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object p1

    new-instance v2, Landroid/content/Intent;

    const-string v3, "miui.intent.action.SAFE_PAY_MONITOR_SETTINGS"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    new-instance p1, Lp5/b;

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object p0

    invoke-static {p0, v0}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p0, Lo5/a;

    const-string v0, "safe_mode_function_pay_protect_btn"

    invoke-direct {p1, v0, v1, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object p1

    new-instance v2, Landroid/content/Intent;

    const-string v3, "miui.intent.action.PRIVACY_SETTINGS"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    new-instance p1, Lp5/b;

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object p0

    invoke-static {p0, v0}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p0, Lo5/a;

    const-string v0, "safe_mode_function_privacy_protect_btn"

    invoke-direct {p1, v0, v1, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    :goto_0
    invoke-virtual {p1}, Lp5/f;->c()Z

    return-void
.end method

.method public static synthetic z(Lcom/miui/packageInstaller/ui/listcomponets/PureModeIntroduceViewObject;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/miui/packageInstaller/ui/listcomponets/PureModeIntroduceViewObject;->B(Lcom/miui/packageInstaller/ui/listcomponets/PureModeIntroduceViewObject;Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public A(Lcom/miui/packageInstaller/ui/listcomponets/PureModeIntroduceViewObject$ViewHolder;)V
    .locals 6

    const-string v0, "viewHolder"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/PureModeIntroduceViewObject$ViewHolder;->getImage()Landroidx/appcompat/widget/AppCompatImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeIntroduceViewObject;->l:Lcom/miui/packageInstaller/model/IntroductionModeBean;

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/IntroductionModeBean;->getResourceId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/AppCompatImageView;->setImageResource(I)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/PureModeIntroduceViewObject$ViewHolder;->getTitle()Landroidx/appcompat/widget/AppCompatTextView;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeIntroduceViewObject;->l:Lcom/miui/packageInstaller/model/IntroductionModeBean;

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/IntroductionModeBean;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/PureModeIntroduceViewObject$ViewHolder;->getLabel()Landroidx/appcompat/widget/AppCompatTextView;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeIntroduceViewObject;->l:Lcom/miui/packageInstaller/model/IntroductionModeBean;

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/IntroductionModeBean;->getLabel()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/PureModeIntroduceViewObject$ViewHolder;->getIntroductionDes()Landroidx/appcompat/widget/AppCompatTextView;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeIntroduceViewObject;->l:Lcom/miui/packageInstaller/model/IntroductionModeBean;

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/IntroductionModeBean;->getIntroductionDes()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeIntroduceViewObject;->l:Lcom/miui/packageInstaller/model/IntroductionModeBean;

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/IntroductionModeBean;->getLabel()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f1102b8

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const-string v1, "null cannot be cast to non-null type com.miui.packageInstaller.analytics.IPage"

    const-string v3, "button"

    if-eqz v0, :cond_0

    new-instance v0, Lp5/g;

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v1}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v4, Lo5/a;

    const-string v5, "safe_mode_function_pay_protect_btn"

    invoke-direct {v0, v5, v3, v4}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    :cond_0
    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeIntroduceViewObject;->l:Lcom/miui/packageInstaller/model/IntroductionModeBean;

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/IntroductionModeBean;->getLabel()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f1102b9

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lp5/g;

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v1}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v4, Lo5/a;

    const-string v1, "safe_mode_function_privacy_protect_btn"

    invoke-direct {v0, v1, v3, v4}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    :cond_1
    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeIntroduceViewObject;->l:Lcom/miui/packageInstaller/model/IntroductionModeBean;

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/IntroductionModeBean;->getLabel()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeIntroduceViewObject;->l:Lcom/miui/packageInstaller/model/IntroductionModeBean;

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/IntroductionModeBean;->getLabel()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/PureModeIntroduceViewObject$ViewHolder;->getArrowRight()Landroidx/appcompat/widget/AppCompatImageView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/PureModeIntroduceViewObject$ViewHolder;->getLlLayout()Landroidx/appcompat/widget/LinearLayoutCompat;

    move-result-object p1

    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    :goto_0
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/PureModeIntroduceViewObject$ViewHolder;->getArrowRight()Landroidx/appcompat/widget/AppCompatImageView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/PureModeIntroduceViewObject$ViewHolder;->getLlLayout()Landroidx/appcompat/widget/LinearLayoutCompat;

    move-result-object p1

    new-instance v0, Lcom/miui/packageInstaller/ui/listcomponets/o;

    invoke-direct {v0, p0}, Lcom/miui/packageInstaller/ui/listcomponets/o;-><init>(Lcom/miui/packageInstaller/ui/listcomponets/PureModeIntroduceViewObject;)V

    :goto_1
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public k()I
    .locals 1

    const v0, 0x7f0d016e

    return v0
.end method

.method public bridge synthetic o(Landroidx/recyclerview/widget/RecyclerView$d0;)V
    .locals 0

    check-cast p1, Lcom/miui/packageInstaller/ui/listcomponets/PureModeIntroduceViewObject$ViewHolder;

    invoke-virtual {p0, p1}, Lcom/miui/packageInstaller/ui/listcomponets/PureModeIntroduceViewObject;->A(Lcom/miui/packageInstaller/ui/listcomponets/PureModeIntroduceViewObject$ViewHolder;)V

    return-void
.end method
