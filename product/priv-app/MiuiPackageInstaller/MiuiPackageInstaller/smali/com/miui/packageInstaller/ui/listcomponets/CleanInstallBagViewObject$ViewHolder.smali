.class public final Lcom/miui/packageInstaller/ui/listcomponets/CleanInstallBagViewObject$ViewHolder;
.super Landroidx/recyclerview/widget/RecyclerView$d0;


# instance fields
.field private apkSize:Landroid/widget/TextView;

.field private slidingButton:Lmiuix/slidingwidget/widget/SlidingButton;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    const-string v0, "itemView"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$d0;-><init>(Landroid/view/View;)V

    const v0, 0x7f0a0321

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/CleanInstallBagViewObject$ViewHolder;->apkSize:Landroid/widget/TextView;

    const v0, 0x7f0a02db

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lmiuix/slidingwidget/widget/SlidingButton;

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/CleanInstallBagViewObject$ViewHolder;->slidingButton:Lmiuix/slidingwidget/widget/SlidingButton;

    return-void
.end method


# virtual methods
.method public final getApkSize()Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/CleanInstallBagViewObject$ViewHolder;->apkSize:Landroid/widget/TextView;

    return-object v0
.end method

.method public final getSlidingButton()Lmiuix/slidingwidget/widget/SlidingButton;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/CleanInstallBagViewObject$ViewHolder;->slidingButton:Lmiuix/slidingwidget/widget/SlidingButton;

    return-object v0
.end method

.method public final setApkSize(Landroid/widget/TextView;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/CleanInstallBagViewObject$ViewHolder;->apkSize:Landroid/widget/TextView;

    return-void
.end method

.method public final setSlidingButton(Lmiuix/slidingwidget/widget/SlidingButton;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/CleanInstallBagViewObject$ViewHolder;->slidingButton:Lmiuix/slidingwidget/widget/SlidingButton;

    return-void
.end method
