.class public final Lcom/miui/packageInstaller/view/ComplaintContentView$a;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/packageInstaller/view/ComplaintContentView;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/miui/packageInstaller/view/ComplaintContentView;


# direct methods
.method constructor <init>(Lcom/miui/packageInstaller/view/ComplaintContentView;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/view/ComplaintContentView$a;->a:Lcom/miui/packageInstaller/view/ComplaintContentView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 1

    const-string v0, "editable"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    const-string p2, "charSequence"

    invoke-static {p1, p2}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 3

    const-string p2, "charSequence"

    invoke-static {p1, p2}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p2, p0, Lcom/miui/packageInstaller/view/ComplaintContentView$a;->a:Lcom/miui/packageInstaller/view/ComplaintContentView;

    invoke-static {p2}, Lcom/miui/packageInstaller/view/ComplaintContentView;->b(Lcom/miui/packageInstaller/view/ComplaintContentView;)Landroid/widget/TextView;

    move-result-object p2

    if-nez p2, :cond_0

    const-string p2, "mNumTv"

    invoke-static {p2}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 p2, 0x0

    :cond_0
    sget-object p3, Lm8/w;->a:Lm8/w;

    iget-object p3, p0, Lcom/miui/packageInstaller/view/ComplaintContentView$a;->a:Lcom/miui/packageInstaller/view/ComplaintContentView;

    invoke-virtual {p3}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object p3

    const p4, 0x7f1100c8

    invoke-virtual {p3, p4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p3

    const-string p4, "context.getString(R.string.dev_com_content_length)"

    invoke-static {p3, p4}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p4, 0x1

    new-array v0, p4, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0, p4}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object p4

    invoke-static {p3, p4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p3

    const-string p4, "format(format, *args)"

    invoke-static {p3, p4}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object p2, p0, Lcom/miui/packageInstaller/view/ComplaintContentView$a;->a:Lcom/miui/packageInstaller/view/ComplaintContentView;

    invoke-static {p2}, Lcom/miui/packageInstaller/view/ComplaintContentView;->a(Lcom/miui/packageInstaller/view/ComplaintContentView;)Lcom/miui/packageInstaller/view/DevEditText$b;

    move-result-object p2

    if-eqz p2, :cond_1

    invoke-interface {p2, p1}, Lcom/miui/packageInstaller/view/DevEditText$b;->a(Ljava/lang/CharSequence;)V

    :cond_1
    return-void
.end method
