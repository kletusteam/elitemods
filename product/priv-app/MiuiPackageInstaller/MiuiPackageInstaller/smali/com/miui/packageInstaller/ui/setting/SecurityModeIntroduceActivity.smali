.class public final Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;
.super Lq2/b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$a;,
        Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$b;
    }
.end annotation


# static fields
.field public static final D:Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$a;


# instance fields
.field private A:Lm5/e;

.field private B:Lcom/miui/packageInstaller/model/ApkInfo;

.field public C:Landroidx/fragment/app/m;

.field private u:Landroid/widget/TextView;

.field private v:Landroid/widget/LinearLayout;

.field private w:Landroid/widget/Button;

.field private x:Landroid/widget/FrameLayout;

.field private y:Z

.field private z:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$a;-><init>(Lm8/g;)V

    sput-object v0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;->D:Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lq2/b;-><init>()V

    return-void
.end method

.method public static synthetic J0(Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;->U0(Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic K0(Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;)V
    .locals 0

    invoke-static {p0}, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;->a1(Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;)V

    return-void
.end method

.method public static final synthetic L0(Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;)Lcom/miui/packageInstaller/model/ApkInfo;
    .locals 0

    iget-object p0, p0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;->B:Lcom/miui/packageInstaller/model/ApkInfo;

    return-object p0
.end method

.method public static final synthetic M0(Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;)Lm5/e;
    .locals 0

    iget-object p0, p0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;->A:Lm5/e;

    return-object p0
.end method

.method public static final synthetic N0(Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;)Lo5/b;
    .locals 0

    iget-object p0, p0, Lq2/b;->q:Lo5/b;

    return-object p0
.end method

.method public static final synthetic O0(Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;->V0()V

    return-void
.end method

.method private final Q0()V
    .locals 4

    invoke-virtual {p0}, Landroidx/fragment/app/e;->X()Landroidx/fragment/app/m;

    move-result-object v0

    const-string v1, "supportFragmentManager"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;->b1(Landroidx/fragment/app/m;)V

    invoke-virtual {p0}, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;->P0()Landroidx/fragment/app/m;

    move-result-object v0

    const-string v1, "tag_fragment"

    invoke-virtual {v0, v1}, Landroidx/fragment/app/m;->h0(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;->P0()Landroidx/fragment/app/m;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/fragment/app/m;->l()Landroidx/fragment/app/v;

    move-result-object v0

    const-string v2, "fragmentManager.beginTransaction()"

    invoke-static {v0, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v2, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$b;

    invoke-direct {v2}, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$b;-><init>()V

    const v3, 0x7f0a017a

    invoke-virtual {v0, v3, v2, v1}, Landroidx/fragment/app/v;->b(ILandroidx/fragment/app/Fragment;Ljava/lang/String;)Landroidx/fragment/app/v;

    invoke-virtual {v0}, Landroidx/fragment/app/v;->h()I

    :cond_0
    return-void
.end method

.method private final R0()V
    .locals 3

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "apk_info"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/miui/packageInstaller/model/ApkInfo;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;->B:Lcom/miui/packageInstaller/model/ApkInfo;

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "caller"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lm5/e;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;->A:Lm5/e;

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;->B:Lcom/miui/packageInstaller/model/ApkInfo;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/ApkInfo;->getPackageInfo()Landroid/content/pm/PackageInfo;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;->B:Lcom/miui/packageInstaller/model/ApkInfo;

    if-nez v1, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/pm/PackageManager;->getApplicationIcon(Landroid/content/pm/ApplicationInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/miui/packageInstaller/model/ApkInfo;->setIcon(Landroid/graphics/drawable/Drawable;)V

    :cond_1
    :goto_0
    return-void
.end method

.method private final S0()V
    .locals 7

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;->x:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const-string v0, "flPreference"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_0
    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    const-string v3, "null cannot be cast to non-null type android.widget.RelativeLayout.LayoutParams"

    invoke-static {v2, v3}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    const v4, 0x7f070146

    const v5, 0x7f0700b9

    const/4 v6, 0x1

    if-ne v3, v6, :cond_1

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    :goto_0
    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMarginStart(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    if-ne v3, v6, :cond_2

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    :goto_1
    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMarginEnd(I)V

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;->w:Landroid/widget/Button;

    if-nez v0, :cond_3

    const-string v0, "btnCloseSafeMode"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_2

    :cond_3
    move-object v1, v0

    :goto_2
    invoke-virtual {v1}, Landroid/widget/Button;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    const-string v2, "null cannot be cast to non-null type android.widget.LinearLayout.LayoutParams"

    invoke-static {v0, v2}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f07010d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070124

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_4
    return-void
.end method

.method private final T0()V
    .locals 4

    const v0, 0x7f0a01f7

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.ll_bottom_view)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;->v:Landroid/widget/LinearLayout;

    const v0, 0x7f0a03c4

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.tv_safe_protect_count)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;->u:Landroid/widget/TextView;

    const v0, 0x7f0a00a8

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.btn_close_safe_mode)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;->w:Landroid/widget/Button;

    const v0, 0x7f0a017a

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.fl_saf\u2026ode_introduce_preference)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;->x:Landroid/widget/FrameLayout;

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;->v:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const-string v0, "llBottomView"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_0
    iget-boolean v2, p0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;->y:Z

    if-eqz v2, :cond_1

    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    const/4 v2, 0x4

    :goto_0
    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-boolean v0, p0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;->y:Z

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;->Y0()V

    new-instance v0, Lp5/g;

    const-string v2, "safe_mode_close_btn"

    const-string v3, "button"

    invoke-direct {v0, v2, v3, p0}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    :cond_2
    iget-object v0, p0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;->w:Landroid/widget/Button;

    if-nez v0, :cond_3

    const-string v0, "btnCloseSafeMode"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    move-object v1, v0

    :goto_1
    new-instance v0, Le6/e;

    invoke-direct {v0, p0}, Le6/e;-><init>(Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;)V

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private static final U0(Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;Landroid/view/View;)V
    .locals 2

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;->c1()V

    new-instance p1, Lp5/b;

    const-string v0, "safe_mode_close_btn"

    const-string v1, "button"

    invoke-direct {p1, v0, v1, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p1}, Lp5/f;->c()Z

    return-void
.end method

.method private final V0()V
    .locals 2

    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;->Z0()V

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/miui/packageInstaller/ui/secure/SecurityModeFeedBackActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lmiuix/appcompat/app/j;->finish()V

    return-void
.end method

.method private final W0()V
    .locals 3

    new-instance v0, Lp5/g;

    const-string v1, "safe_mode_define_btn"

    const-string v2, "button"

    invoke-direct {v0, v1, v2, p0}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    new-instance v0, Lp5/g;

    const-string v1, "safe_mode_intro_btn"

    invoke-direct {v0, v1, v2, p0}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    return-void
.end method

.method private final X0()V
    .locals 1

    invoke-static {p0}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->isSafeModelEnable(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;->y:Z

    return-void
.end method

.method private final Y0()V
    .locals 7

    invoke-static {}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->getSecurityModeProtectTimes()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;->z:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-lez v0, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0f0019

    iget-wide v4, p0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;->z:J

    long-to-int v6, v4

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-virtual {v0, v3, v6, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->calculateSecurityModeProtectDays()I

    move-result v0

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f001a

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v2

    invoke-virtual {v3, v4, v0, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v0, v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;I)Landroid/text/Spanned;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;->u:Landroid/widget/TextView;

    if-nez v1, :cond_1

    const-string v1, "tvProtectCount"

    invoke-static {v1}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v1, 0x0

    :cond_1
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private final Z0()V
    .locals 2

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object v0

    new-instance v1, Le6/f;

    invoke-direct {v1, p0}, Le6/f;-><init>(Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;)V

    invoke-virtual {v0, v1}, Lf6/z;->g(Ljava/lang/Runnable;)V

    return-void
.end method

.method private static final a1(Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Ld6/l0;->c:Ld6/l0$a;

    invoke-virtual {v0}, Ld6/l0$a;->a()V

    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->setSafeModelEnabled(Landroid/content/Context;Z)V

    return-void
.end method

.method private final c1()V
    .locals 3

    new-instance v0, Lp5/g;

    const-string v1, "safe_mode_close_warning_popup"

    const-string v2, "popup"

    invoke-direct {v0, v1, v2, p0}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    new-instance v0, Lp5/g;

    const-string v1, "safe_mode_close_warning_popup_continue_btn"

    const-string v2, "button"

    invoke-direct {v0, v1, v2, p0}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    new-instance v0, Lp5/g;

    const-string v1, "safe_mode_close_warning_popup_cancel_btn"

    invoke-direct {v0, v1, v2, p0}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    sget-object v0, La6/d;->a:La6/d;

    new-instance v1, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$c;

    invoke-direct {v1, p0}, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity$c;-><init>(Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;)V

    invoke-virtual {v0, p0, v1}, La6/d;->d(Landroid/content/Context;La6/d$a;)Lmiuix/appcompat/app/i;

    return-void
.end method


# virtual methods
.method public E0()Ljava/lang/String;
    .locals 1

    const-string v0, "safe_mode_settings"

    return-object v0
.end method

.method public G()Lcom/miui/packageInstaller/model/ApkInfo;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;->B:Lcom/miui/packageInstaller/model/ApkInfo;

    return-object v0
.end method

.method public final P0()Landroidx/fragment/app/m;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;->C:Landroidx/fragment/app/m;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    const-string v0, "fragmentManager"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v0, 0x0

    return-object v0
.end method

.method public final b1(Landroidx/fragment/app/m;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;->C:Landroidx/fragment/app/m;

    return-void
.end method

.method public onBackPressed()V
    .locals 3

    invoke-super {p0}, Lmiuix/appcompat/app/j;->onBackPressed()V

    new-instance v0, Lp5/b;

    const-string v1, "page_back_btn"

    const-string v2, "button"

    invoke-direct {v0, v1, v2, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    const-string v1, "back_type"

    const-string v2, "system"

    invoke-virtual {v0, v1, v2}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object v0

    invoke-virtual {v0}, Lp5/f;->c()Z

    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    const-string v0, "newConfig"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lq2/b;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;->S0()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lq2/b;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0d002b

    invoke-virtual {p0, p1}, Lmiuix/appcompat/app/j;->setContentView(I)V

    invoke-virtual {p0}, Lmiuix/appcompat/app/j;->r0()Lmiuix/appcompat/app/a;

    move-result-object p1

    if-eqz p1, :cond_2

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    invoke-static {p1}, Lcom/android/packageinstaller/utils/g;->y(Landroid/content/Intent;)Z

    move-result p1

    if-eqz p1, :cond_2

    invoke-virtual {p0}, Lmiuix/appcompat/app/j;->r0()Lmiuix/appcompat/app/a;

    move-result-object p1

    const/4 v0, 0x0

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p1, v0}, Lmiuix/appcompat/app/a;->z(I)V

    :goto_0
    invoke-virtual {p0}, Lmiuix/appcompat/app/j;->r0()Lmiuix/appcompat/app/a;

    move-result-object p1

    if-nez p1, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {p1, v0}, Lmiuix/appcompat/app/a;->A(Z)V

    :cond_2
    :goto_1
    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;->W0()V

    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;->R0()V

    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;->X0()V

    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;->Q0()V

    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;->T0()V

    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;->S0()V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    const-string v0, "item"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    new-instance v0, Lp5/b;

    const-string v1, "page_back_btn"

    const-string v2, "button"

    invoke-direct {v0, v1, v2, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    const-string v1, "back_type"

    const-string v2, "click_icon"

    invoke-virtual {v0, v1, v2}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object v0

    invoke-virtual {v0}, Lp5/f;->c()Z

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result p1

    return p1
.end method

.method public p()Lm5/e;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/setting/SecurityModeIntroduceActivity;->A:Lm5/e;

    return-object v0
.end method
