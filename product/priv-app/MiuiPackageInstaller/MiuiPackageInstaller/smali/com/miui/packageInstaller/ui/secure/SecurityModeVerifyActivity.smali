.class public Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity;
.super Lq2/b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity$b;,
        Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity$a;,
        Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity$c;
    }
.end annotation


# static fields
.field public static final G:Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity$b;


# instance fields
.field private A:Landroid/widget/TextView;

.field private B:Z

.field private C:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity$a;",
            ">;"
        }
    .end annotation
.end field

.field private D:Lh6/a;

.field private E:Lg6/c;

.field private F:La6/w;

.field private u:Lmiuix/slidingwidget/widget/SlidingButton;

.field private v:Landroid/widget/TextView;

.field private w:Landroid/widget/TextView;

.field private x:Landroid/widget/TextView;

.field private y:Landroid/widget/TextView;

.field private z:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity$b;-><init>(Lm8/g;)V

    sput-object v0, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity;->G:Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity$b;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lq2/b;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity;->B:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity;->C:Ljava/util/ArrayList;

    return-void
.end method

.method public static synthetic J0(Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity;Landroid/widget/CompoundButton;Z)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity;->R0(Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity;Landroid/widget/CompoundButton;Z)V

    return-void
.end method

.method public static final synthetic K0(Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity;->N0(Z)V

    return-void
.end method

.method public static final synthetic L0(Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity;)La6/w;
    .locals 0

    iget-object p0, p0, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity;->F:La6/w;

    return-object p0
.end method

.method private final M0()V
    .locals 3

    sget-object v0, Lf6/r;->a:Lf6/r$a;

    invoke-static {p0}, Lm2/b;->g(Landroid/content/Context;)Lm2/b;

    move-result-object v1

    invoke-virtual {v1}, Lm2/b;->k()Lx5/a;

    move-result-object v1

    const-string v2, "getInstance(this).secureVerifyType"

    invoke-static {v1, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p0, v1}, Lf6/r$a;->c(Landroid/content/Context;Lx5/a;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0, p0}, Lf6/r$a;->e(Landroid/content/Context;)V

    :cond_0
    return-void
.end method

.method private final N0(Z)V
    .locals 2

    invoke-static {p0}, Lm2/b;->g(Landroid/content/Context;)Lm2/b;

    move-result-object v0

    invoke-virtual {v0, p1}, Lm2/b;->I(Z)V

    iput-boolean p1, p0, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity;->B:Z

    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity;->S0()V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "secure_mode_verification_broadcast.Action"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "secure_mode_verification"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity;->u:Lmiuix/slidingwidget/widget/SlidingButton;

    if-nez v0, :cond_0

    const-string v0, "mSlidingButton"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v0, 0x0

    :cond_0
    invoke-virtual {v0, p1}, Lmiuix/slidingwidget/widget/SlidingButton;->setChecked(Z)V

    return-void
.end method

.method private final O0(Lx5/a;)Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity$c;->a:[I

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_3

    const/4 v0, 0x2

    if-eq p1, v0, :cond_2

    const/4 v0, 0x3

    if-eq p1, v0, :cond_1

    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    const-string p1, ""

    goto :goto_0

    :cond_0
    const-string p1, "face_password"

    goto :goto_0

    :cond_1
    const-string p1, "fingerprint_password"

    goto :goto_0

    :cond_2
    const-string p1, "screen_password"

    goto :goto_0

    :cond_3
    const-string p1, "mi_account"

    :goto_0
    return-object p1
.end method

.method private final P0()V
    .locals 1

    invoke-static {p0}, Lm2/b;->g(Landroid/content/Context;)Lm2/b;

    move-result-object v0

    invoke-virtual {v0}, Lm2/b;->s()Z

    move-result v0

    iput-boolean v0, p0, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity;->B:Z

    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity;->S0()V

    return-void
.end method

.method private final Q0()V
    .locals 12

    const v0, 0x7f0a028c

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.open_risk)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lmiuix/slidingwidget/widget/SlidingButton;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity;->u:Lmiuix/slidingwidget/widget/SlidingButton;

    const v0, 0x7f0a03d4

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.tv_xiaomi_account_text)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity;->v:Landroid/widget/TextView;

    const v0, 0x7f0a03b9

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.tv_lock_screen_text)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity;->w:Landroid/widget/TextView;

    const v0, 0x7f0a03b3

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.tv_finger_text)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity;->x:Landroid/widget/TextView;

    const v0, 0x7f0a03b2

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.tv_face_text)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity;->y:Landroid/widget/TextView;

    const v0, 0x7f0a03ba

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.tv_lock_screen_tip)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity;->z:Landroid/widget/TextView;

    const v0, 0x7f0a03aa

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.tv_app_check_tip)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity;->A:Landroid/widget/TextView;

    const/4 v0, 0x4

    new-array v1, v0, [Lx5/a;

    sget-object v2, Lx5/a;->f:Lx5/a;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    sget-object v2, Lx5/a;->c:Lx5/a;

    const/4 v4, 0x1

    aput-object v2, v1, v4

    sget-object v2, Lx5/a;->d:Lx5/a;

    const/4 v5, 0x2

    aput-object v2, v1, v5

    sget-object v2, Lx5/a;->e:Lx5/a;

    const/4 v6, 0x3

    aput-object v2, v1, v6

    invoke-static {v1}, Lb8/d;->j([Ljava/lang/Object;)Lt8/f;

    move-result-object v1

    new-instance v2, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity$d;

    invoke-direct {v2, p0}, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity$d;-><init>(Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity;)V

    invoke-static {v1, v2}, Lt8/g;->f(Lt8/f;Ll8/l;)Lt8/f;

    move-result-object v1

    invoke-interface {v1}, Lt8/f;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lx5/a;

    sget-object v7, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity$c;->a:[I

    invoke-virtual {v2}, Ljava/lang/Enum;->ordinal()I

    move-result v8

    aget v7, v7, v8

    if-eq v7, v4, :cond_3

    if-eq v7, v5, :cond_2

    if-eq v7, v6, :cond_1

    if-eq v7, v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v7, p0, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity;->C:Ljava/util/ArrayList;

    new-instance v8, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity$a;

    const v9, 0x7f0a015b

    invoke-virtual {p0, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    const-string v10, "findViewById(R.id.face_risk)"

    invoke-static {v9, v10}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const v10, 0x7f0a01cf

    invoke-virtual {p0, v10}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v10

    const-string v11, "findViewById(R.id.iv_face_arrow)"

    invoke-static {v10, v11}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v10, Landroid/widget/ImageView;

    invoke-direct {v8, p0, v2, v9, v10}, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity$a;-><init>(Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity;Lx5/a;Landroid/view/View;Landroid/widget/ImageView;)V

    goto :goto_1

    :cond_1
    iget-object v7, p0, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity;->C:Ljava/util/ArrayList;

    new-instance v8, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity$a;

    const v9, 0x7f0a016a

    invoke-virtual {p0, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    const-string v10, "findViewById(R.id.finger_risk)"

    invoke-static {v9, v10}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const v10, 0x7f0a01d0

    invoke-virtual {p0, v10}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v10

    const-string v11, "findViewById(R.id.iv_finger_arrow)"

    invoke-static {v10, v11}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v10, Landroid/widget/ImageView;

    invoke-direct {v8, p0, v2, v9, v10}, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity$a;-><init>(Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity;Lx5/a;Landroid/view/View;Landroid/widget/ImageView;)V

    goto :goto_1

    :cond_2
    iget-object v7, p0, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity;->C:Ljava/util/ArrayList;

    new-instance v8, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity$a;

    const v9, 0x7f0a020a

    invoke-virtual {p0, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    const-string v10, "findViewById(R.id.lock_screen_risk)"

    invoke-static {v9, v10}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const v10, 0x7f0a01d4

    invoke-virtual {p0, v10}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v10

    const-string v11, "findViewById(R.id.iv_lock_screen_arrow)"

    invoke-static {v10, v11}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v10, Landroid/widget/ImageView;

    invoke-direct {v8, p0, v2, v9, v10}, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity$a;-><init>(Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity;Lx5/a;Landroid/view/View;Landroid/widget/ImageView;)V

    goto :goto_1

    :cond_3
    iget-object v7, p0, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity;->C:Ljava/util/ArrayList;

    new-instance v8, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity$a;

    const v9, 0x7f0a03fc

    invoke-virtual {p0, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    const-string v10, "findViewById(R.id.xiaomi_account_risk)"

    invoke-static {v9, v10}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const v10, 0x7f0a01d9

    invoke-virtual {p0, v10}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v10

    const-string v11, "findViewById(R.id.iv_xiaomi_account_arrow)"

    invoke-static {v10, v11}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v10, Landroid/widget/ImageView;

    invoke-direct {v8, p0, v2, v9, v10}, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity$a;-><init>(Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity;Lx5/a;Landroid/view/View;Landroid/widget/ImageView;)V

    :goto_1
    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_4
    invoke-static {p0}, Lm2/b;->g(Landroid/content/Context;)Lm2/b;

    move-result-object v0

    invoke-virtual {v0}, Lm2/b;->k()Lx5/a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity;->C:Ljava/util/ArrayList;

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity$a;

    invoke-virtual {v2}, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity$a;->c()Lx5/a;

    move-result-object v5

    if-ne v5, v0, :cond_5

    move v5, v4

    goto :goto_3

    :cond_5
    move v5, v3

    :goto_3
    invoke-virtual {v2, v5}, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity$a;->e(Z)V

    goto :goto_2

    :cond_6
    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity;->u:Lmiuix/slidingwidget/widget/SlidingButton;

    if-nez v0, :cond_7

    const-string v0, "mSlidingButton"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v0, 0x0

    :cond_7
    new-instance v1, Ld6/o0;

    invoke-direct {v1, p0}, Ld6/o0;-><init>(Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity;)V

    invoke-virtual {v0, v1}, Lmiuix/slidingwidget/widget/SlidingButton;->setOnPerformCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    new-instance v0, Lp5/g;

    const-string v1, "app_safe_verify_switch"

    const-string v2, "switch"

    invoke-direct {v0, v1, v2, p0}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    return-void
.end method

.method private static final R0(Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity;Landroid/widget/CompoundButton;Z)V
    .locals 1

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_0

    invoke-direct {p0, p2}, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity;->N0(Z)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity;->u:Lmiuix/slidingwidget/widget/SlidingButton;

    if-nez p1, :cond_1

    const-string p1, "mSlidingButton"

    invoke-static {p1}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 p1, 0x0

    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lmiuix/slidingwidget/widget/SlidingButton;->setChecked(Z)V

    invoke-direct {p0, p2, p0}, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity;->U0(ZLandroid/content/Context;)V

    :goto_0
    return-void
.end method

.method private final S0()V
    .locals 13

    iget-boolean v0, p0, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity;->B:Z

    const-string v1, "mSlidingButton"

    const-string v2, "noLockTipText"

    const-string v3, "desTip"

    const/16 v4, 0x8

    const/4 v5, 0x0

    const/4 v6, 0x0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity;->A:Landroid/widget/TextView;

    if-nez v0, :cond_0

    invoke-static {v3}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v5

    :cond_0
    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity;->C:Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    move v3, v6

    move v7, v3

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    const/4 v9, 0x1

    if-eqz v8, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity$a;

    invoke-virtual {v8}, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity$a;->d()Landroid/view/View;

    move-result-object v10

    invoke-virtual {v10, v6}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v8}, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity$a;->d()Landroid/view/View;

    move-result-object v10

    sget-object v11, Lf6/r;->a:Lf6/r$a;

    invoke-virtual {v8}, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity$a;->c()Lx5/a;

    move-result-object v12

    invoke-virtual {v11, p0, v12}, Lf6/r$a;->c(Landroid/content/Context;Lx5/a;)Z

    move-result v11

    invoke-virtual {v10, v11}, Landroid/view/View;->setEnabled(Z)V

    invoke-static {p0}, Lm2/b;->g(Landroid/content/Context;)Lm2/b;

    move-result-object v10

    invoke-virtual {v10}, Lm2/b;->k()Lx5/a;

    move-result-object v10

    invoke-virtual {v8}, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity$a;->c()Lx5/a;

    move-result-object v11

    if-ne v10, v11, :cond_2

    invoke-virtual {v8}, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity$a;->d()Landroid/view/View;

    move-result-object v10

    invoke-virtual {v10}, Landroid/view/View;->isEnabled()Z

    move-result v10

    if-nez v10, :cond_2

    move v3, v9

    :cond_2
    if-eqz v3, :cond_3

    invoke-virtual {v8}, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity$a;->d()Landroid/view/View;

    move-result-object v10

    invoke-virtual {v10}, Landroid/view/View;->isEnabled()Z

    move-result v10

    if-eqz v10, :cond_3

    invoke-virtual {v8}, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity$a;->c()Lx5/a;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity;->T0(Lx5/a;)V

    move v3, v6

    :cond_3
    invoke-virtual {v8}, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity$a;->d()Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/View;->isEnabled()Z

    move-result v8

    if-nez v8, :cond_1

    move v7, v9

    goto :goto_0

    :cond_4
    if-eqz v3, :cond_5

    sget-object v0, Lx5/a;->f:Lx5/a;

    invoke-virtual {p0, v0}, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity;->T0(Lx5/a;)V

    :cond_5
    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity;->z:Landroid/widget/TextView;

    if-nez v0, :cond_6

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v5

    :cond_6
    if-eqz v7, :cond_7

    move v4, v6

    :cond_7
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity;->u:Lmiuix/slidingwidget/widget/SlidingButton;

    if-nez v0, :cond_8

    invoke-static {v1}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_1

    :cond_8
    move-object v5, v0

    :goto_1
    invoke-virtual {v5, v9}, Lmiuix/slidingwidget/widget/SlidingButton;->setChecked(Z)V

    goto :goto_4

    :cond_9
    invoke-static {p0}, Lm2/b;->g(Landroid/content/Context;)Lm2/b;

    move-result-object v0

    sget-object v7, Lx5/a;->f:Lx5/a;

    invoke-virtual {v0, v7}, Lm2/b;->J(Lx5/a;)V

    invoke-virtual {p0, v7}, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity;->T0(Lx5/a;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity;->A:Landroid/widget/TextView;

    if-nez v0, :cond_a

    invoke-static {v3}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v5

    :cond_a
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity;->z:Landroid/widget/TextView;

    if-nez v0, :cond_b

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v5

    :cond_b
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity;->C:Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_c

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity$a;

    invoke-virtual {v2}, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity$a;->d()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    :cond_c
    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity;->u:Lmiuix/slidingwidget/widget/SlidingButton;

    if-nez v0, :cond_d

    invoke-static {v1}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_3

    :cond_d
    move-object v5, v0

    :goto_3
    invoke-virtual {v5, v6}, Lmiuix/slidingwidget/widget/SlidingButton;->setChecked(Z)V

    :goto_4
    return-void
.end method

.method private final U0(ZLandroid/content/Context;)V
    .locals 1

    iget-object p2, p0, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity;->F:La6/w;

    if-nez p2, :cond_0

    new-instance p2, La6/w;

    invoke-direct {p2, p0}, La6/w;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity;->F:La6/w;

    new-instance v0, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity$e;

    invoke-direct {v0, p0, p1}, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity$e;-><init>(Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity;Z)V

    invoke-virtual {p2, v0}, La6/v;->k(La6/v$a;)V

    :cond_0
    iget-object p1, p0, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity;->F:La6/w;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Landroid/app/Dialog;->show()V

    :cond_1
    return-void
.end method


# virtual methods
.method public final T0(Lx5/a;)V
    .locals 3

    const-string v0, "type"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity;->C:Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity$a;

    invoke-virtual {v1}, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity$a;->c()Lx5/a;

    move-result-object v2

    if-ne v2, p1, :cond_0

    const/4 v2, 0x1

    goto :goto_1

    :cond_0
    const/4 v2, 0x0

    :goto_1
    invoke-virtual {v1, v2}, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity$a;->e(Z)V

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "type = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lx5/a;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "setCurrentSelectedAuthorizationType"

    invoke-static {v1, v0}, Lf6/o;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    invoke-static {p0}, Lm2/b;->g(Landroid/content/Context;)Lm2/b;

    move-result-object v0

    invoke-virtual {v0, p1}, Lm2/b;->J(Lx5/a;)V

    new-instance v0, Lp5/b;

    const-string v1, "app_safe_verify_switch"

    const-string v2, "switch"

    invoke-direct {v0, v1, v2, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-direct {p0, p1}, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity;->O0(Lx5/a;)Ljava/lang/String;

    move-result-object p1

    const-string v1, "verify_method"

    invoke-virtual {v0, v1, p1}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p1

    invoke-virtual {p1}, Lp5/f;->c()Z

    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    invoke-super {p0, p1, p2, p3}, Landroidx/fragment/app/e;->onActivityResult(IILandroid/content/Intent;)V

    const/16 p3, 0x3f2

    if-ne p1, p3, :cond_0

    const/4 p1, -0x1

    if-ne p1, p2, :cond_0

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity;->N0(Z)V

    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lq2/b;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lmiuix/appcompat/app/j;->r0()Lmiuix/appcompat/app/a;

    move-result-object p1

    const/4 v0, 0x0

    if-eqz p1, :cond_2

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    invoke-static {p1}, Lcom/android/packageinstaller/utils/g;->y(Landroid/content/Intent;)Z

    move-result p1

    if-eqz p1, :cond_2

    invoke-virtual {p0}, Lmiuix/appcompat/app/j;->r0()Lmiuix/appcompat/app/a;

    move-result-object p1

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p1, v0}, Lmiuix/appcompat/app/a;->z(I)V

    :goto_0
    invoke-virtual {p0}, Lmiuix/appcompat/app/j;->r0()Lmiuix/appcompat/app/a;

    move-result-object p1

    if-nez p1, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {p1, v0}, Lmiuix/appcompat/app/a;->A(Z)V

    :cond_2
    :goto_1
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v1, "secure_mode_verification"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result p1

    iput-boolean p1, p0, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity;->B:Z

    const p1, 0x7f0d01a0

    invoke-virtual {p0, p1}, Lmiuix/appcompat/app/j;->setContentView(I)V

    new-instance p1, Lh6/a;

    invoke-direct {p1, p0}, Lh6/a;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity;->D:Lh6/a;

    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity;->M0()V

    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity;->Q0()V

    return-void
.end method

.method protected onDestroy()V
    .locals 1

    invoke-super {p0}, Landroidx/fragment/app/e;->onDestroy()V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity;->F:La6/w;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 2

    invoke-super {p0}, Landroidx/fragment/app/e;->onResume()V

    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lg6/c;->m(Landroid/content/Context;)Lg6/c;

    move-result-object v0

    const-string v1, "getInstance(applicationContext)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity;->E:Lg6/c;

    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/secure/SecurityModeVerifyActivity;->P0()V

    return-void
.end method
