.class public final Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/packageInstaller/model/InstallAuthorizeInfo$Companion;
    }
.end annotation


# static fields
.field public static final Companion:Lcom/miui/packageInstaller/model/InstallAuthorizeInfo$Companion;

.field public static final MAX_VALID_TIME:I = 0x1d4c0


# instance fields
.field private appName:Ljava/lang/String;

.field private authorized:Z

.field private callerLabel:Ljava/lang/String;

.field private callerPackageName:Ljava/lang/String;

.field private packageMd5:Ljava/lang/String;

.field private packageName:Ljava/lang/String;

.field private updateTime:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/miui/packageInstaller/model/InstallAuthorizeInfo$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/miui/packageInstaller/model/InstallAuthorizeInfo$Companion;-><init>(Lm8/g;)V

    sput-object v0, Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;->Companion:Lcom/miui/packageInstaller/model/InstallAuthorizeInfo$Companion;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJ)V
    .locals 1

    const-string v0, "packageName"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "appName"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "callerPackageName"

    invoke-static {p3, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "callerLabel"

    invoke-static {p4, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "packageMd5"

    invoke-static {p5, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;->packageName:Ljava/lang/String;

    iput-object p2, p0, Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;->appName:Ljava/lang/String;

    iput-object p3, p0, Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;->callerPackageName:Ljava/lang/String;

    iput-object p4, p0, Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;->callerLabel:Ljava/lang/String;

    iput-object p5, p0, Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;->packageMd5:Ljava/lang/String;

    iput-boolean p6, p0, Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;->authorized:Z

    iput-wide p7, p0, Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;->updateTime:J

    return-void
.end method


# virtual methods
.method public final getAppName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;->appName:Ljava/lang/String;

    return-object v0
.end method

.method public final getAuthorized()Z
    .locals 1

    iget-boolean v0, p0, Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;->authorized:Z

    return v0
.end method

.method public final getCallerLabel()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;->callerLabel:Ljava/lang/String;

    return-object v0
.end method

.method public final getCallerPackageName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;->callerPackageName:Ljava/lang/String;

    return-object v0
.end method

.method public final getPackageMd5()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;->packageMd5:Ljava/lang/String;

    return-object v0
.end method

.method public final getPackageName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;->packageName:Ljava/lang/String;

    return-object v0
.end method

.method public final getUpdateTime()J
    .locals 2

    iget-wide v0, p0, Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;->updateTime:J

    return-wide v0
.end method

.method public final isValid()Z
    .locals 4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;->updateTime:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x1d4c0

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final setAppName(Ljava/lang/String;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;->appName:Ljava/lang/String;

    return-void
.end method

.method public final setAuthorized(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;->authorized:Z

    return-void
.end method

.method public final setCallerLabel(Ljava/lang/String;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;->callerLabel:Ljava/lang/String;

    return-void
.end method

.method public final setCallerPackageName(Ljava/lang/String;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;->callerPackageName:Ljava/lang/String;

    return-void
.end method

.method public final setPackageMd5(Ljava/lang/String;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;->packageMd5:Ljava/lang/String;

    return-void
.end method

.method public final setPackageName(Ljava/lang/String;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;->packageName:Ljava/lang/String;

    return-void
.end method

.method public final setUpdateTime(J)V
    .locals 0

    iput-wide p1, p0, Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;->updateTime:J

    return-void
.end method
