.class public final Lcom/miui/packageInstaller/model/AppManageSceneMode;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/packageInstaller/model/AppManageSceneMode$Companion;
    }
.end annotation


# static fields
.field public static final COMPETITIVE_PRODUCT:Ljava/lang/String; = "competitive_product"

.field public static final Companion:Lcom/miui/packageInstaller/model/AppManageSceneMode$Companion;

.field public static final FORBIDDEN:Ljava/lang/String; = "forbidden"

.field public static final HIGH_RISK:Ljava/lang/String; = "high_risk"

.field public static final ILLEGAL_DISTRIBUTION:Ljava/lang/String; = "illegal_distribution"

.field public static final NO_BLOCK:Ljava/lang/String; = "no_block"

.field public static final OFF_SHELF:Ljava/lang/String; = "off_shelf"

.field public static final ON_SHELF:Ljava/lang/String; = "on_shelf"

.field public static final RISK:Ljava/lang/String; = "risk"

.field public static final RISK_ANTI_SPOOFING:Ljava/lang/String; = "risk_anti_spoofing"

.field public static final RISK_VPN:Ljava/lang/String; = "risk_vpn"

.field public static final VIRUS_ENGINE:Ljava/lang/String; = "virus_engine"

.field public static final WHITE:Ljava/lang/String; = "white"


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/miui/packageInstaller/model/AppManageSceneMode$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/miui/packageInstaller/model/AppManageSceneMode$Companion;-><init>(Lm8/g;)V

    sput-object v0, Lcom/miui/packageInstaller/model/AppManageSceneMode;->Companion:Lcom/miui/packageInstaller/model/AppManageSceneMode$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
