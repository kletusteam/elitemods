.class public final Lcom/miui/packageInstaller/view/ScanActionButton;
.super Landroid/widget/FrameLayout;

# interfaces
.implements Lcom/miui/packageInstaller/ui/InstallerPrepareActionBar$b;


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Landroid/view/View;

.field private c:Landroid/view/View;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-virtual {p0}, Lcom/miui/packageInstaller/view/ScanActionButton;->b()V

    return-void
.end method


# virtual methods
.method public a()Landroid/view/View;
    .locals 0

    return-object p0
.end method

.method public final b()V
    .locals 2

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result v1

    if-eqz v1, :cond_0

    const v1, 0x7f0d0040

    goto :goto_0

    :cond_0
    const v1, 0x7f0d003f

    :goto_0
    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    return-void
.end method

.method public c(Ljava/lang/CharSequence;ILjava/lang/CharSequence;I)V
    .locals 5

    const-string v0, "warningText"

    invoke-static {p3, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/view/ScanActionButton;->e:Landroid/widget/TextView;

    const-string v1, "leftButtonWarningMsg"

    const/4 v2, 0x0

    if-nez v0, :cond_0

    invoke-static {v1}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v2

    :cond_0
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    const/4 v4, 0x0

    if-eqz v3, :cond_1

    const/16 v3, 0x8

    goto :goto_0

    :cond_1
    move v3, v4

    :goto_0
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/miui/packageInstaller/view/ScanActionButton;->d:Landroid/widget/TextView;

    const-string v3, "leftButtonMsg"

    if-nez v0, :cond_2

    invoke-static {v3}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v2

    :cond_2
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object p1, p0, Lcom/miui/packageInstaller/view/ScanActionButton;->e:Landroid/widget/TextView;

    if-nez p1, :cond_3

    invoke-static {v1}, Lm8/i;->s(Ljava/lang/String;)V

    move-object p1, v2

    :cond_3
    invoke-virtual {p1, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    if-eqz p2, :cond_5

    iget-object p1, p0, Lcom/miui/packageInstaller/view/ScanActionButton;->d:Landroid/widget/TextView;

    if-nez p1, :cond_4

    invoke-static {v3}, Lm8/i;->s(Ljava/lang/String;)V

    move-object p1, v2

    :cond_4
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_5
    if-eqz p4, :cond_7

    iget-object p1, p0, Lcom/miui/packageInstaller/view/ScanActionButton;->e:Landroid/widget/TextView;

    if-nez p1, :cond_6

    invoke-static {v1}, Lm8/i;->s(Ljava/lang/String;)V

    move-object p1, v2

    :cond_6
    invoke-virtual {p1, p4}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_7
    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result p1

    if-eqz p1, :cond_a

    iget-object p1, p0, Lcom/miui/packageInstaller/view/ScanActionButton;->d:Landroid/widget/TextView;

    if-nez p1, :cond_8

    invoke-static {v3}, Lm8/i;->s(Ljava/lang/String;)V

    move-object p1, v2

    :cond_8
    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const p3, 0x7f070532

    invoke-virtual {p2, p3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result p2

    invoke-virtual {p1, v4, p2}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object p1, p0, Lcom/miui/packageInstaller/view/ScanActionButton;->e:Landroid/widget/TextView;

    if-nez p1, :cond_9

    invoke-static {v1}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_1

    :cond_9
    move-object v2, p1

    :goto_1
    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-virtual {p1, p3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result p1

    invoke-virtual {v2, v4, p1}, Landroid/widget/TextView;->setTextSize(IF)V

    :cond_a
    return-void
.end method

.method public getButtonText()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/view/ScanActionButton;->d:Landroid/widget/TextView;

    if-nez v0, :cond_0

    const-string v0, "leftButtonMsg"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v0, 0x0

    :cond_0
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 4

    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    const v0, 0x7f0a01e6

    invoke-virtual {p0, v0}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const-string v2, "findViewById(R.id.left_button_msg)"

    invoke-static {v1, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/miui/packageInstaller/view/ScanActionButton;->a:Landroid/widget/TextView;

    const v1, 0x7f0a01e7

    invoke-virtual {p0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const-string v3, "findViewById(R.id.left_button_scan_view)"

    invoke-static {v1, v3}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/miui/packageInstaller/view/ScanActionButton;->b:Landroid/view/View;

    const v1, 0x7f0a01e5

    invoke-virtual {p0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const-string v3, "findViewById(R.id.left_button_info_view)"

    invoke-static {v1, v3}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/miui/packageInstaller/view/ScanActionButton;->c:Landroid/view/View;

    invoke-virtual {p0, v0}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/view/ScanActionButton;->d:Landroid/widget/TextView;

    const v0, 0x7f0a01e8

    invoke-virtual {p0, v0}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.left_button_warring_msg)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/view/ScanActionButton;->e:Landroid/widget/TextView;

    return-void
.end method

.method public setButtonBackgroundResource(I)V
    .locals 0

    invoke-virtual {p0, p1}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    return-void
.end method

.method public setButtonText(Ljava/lang/CharSequence;)V
    .locals 2

    const/4 v0, 0x0

    const-string v1, ""

    invoke-virtual {p0, p1, v0, v1, v0}, Lcom/miui/packageInstaller/view/ScanActionButton;->c(Ljava/lang/CharSequence;ILjava/lang/CharSequence;I)V

    return-void
.end method

.method public setButtonTextColor(I)V
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/view/ScanActionButton;->a:Landroid/widget/TextView;

    if-nez v0, :cond_0

    const-string v0, "tvText"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v0, 0x0

    :cond_0
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method

.method public setClick(Landroid/view/View$OnClickListener;)V
    .locals 1

    const-string v0, "l"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setProgressVisibility(Z)V
    .locals 5

    iget-object v0, p0, Lcom/miui/packageInstaller/view/ScanActionButton;->c:Landroid/view/View;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const-string v0, "leftButtonInfoView"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_0
    const/16 v2, 0x8

    const/4 v3, 0x0

    if-eqz p1, :cond_1

    move v4, v2

    goto :goto_0

    :cond_1
    move v4, v3

    :goto_0
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/miui/packageInstaller/view/ScanActionButton;->b:Landroid/view/View;

    if-nez v0, :cond_2

    const-string v0, "leftButtonScanView"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    move-object v1, v0

    :goto_1
    if-eqz p1, :cond_3

    move v2, v3

    :cond_3
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    xor-int/lit8 p1, p1, 0x1

    invoke-virtual {p0, p1}, Landroid/widget/FrameLayout;->setEnabled(Z)V

    return-void
.end method
