.class public final Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;
.super Landroidx/recyclerview/widget/RecyclerView$d0;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ViewHolder"
.end annotation


# instance fields
.field private appDes:Landroid/widget/TextView;

.field private appDesLayout:Landroid/widget/LinearLayout;

.field private appIcon:Landroid/widget/ImageView;

.field private appName:Landroid/widget/TextView;

.field private appSize:Landroid/widget/TextView;

.field private installBtn:Lcom/miui/packageInstaller/view/AdActionButton;

.field private mediaContainer:Landroid/widget/FrameLayout;

.field private tvDeveloper:Landroid/widget/TextView;

.field private tvPermission:Landroid/widget/TextView;

.field private tvPrivacy:Landroid/widget/TextView;

.field private tvVersion:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 3

    const-string v0, "itemView"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$d0;-><init>(Landroid/view/View;)V

    const v0, 0x7f0a0072

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->tvVersion:Landroid/widget/TextView;

    const v0, 0x7f0a0079

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->appSize:Landroid/widget/TextView;

    const v0, 0x7f0a02b3

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->tvPrivacy:Landroid/widget/TextView;

    const v0, 0x7f0a02a3

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->tvPermission:Landroid/widget/TextView;

    const v0, 0x7f0a0126

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->tvDeveloper:Landroid/widget/TextView;

    const v0, 0x7f0a022f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->mediaContainer:Landroid/widget/FrameLayout;

    const v0, 0x7f0a01c8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->appIcon:Landroid/widget/ImageView;

    const v0, 0x7f0a03a4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->appName:Landroid/widget/TextView;

    const v0, 0x7f0a03a3

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->appDes:Landroid/widget/TextView;

    const v0, 0x7f0a01aa

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/miui/packageInstaller/view/AdActionButton;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->installBtn:Lcom/miui/packageInstaller/view/AdActionButton;

    const v0, 0x7f0a01f2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->appDesLayout:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->tvPrivacy:Landroid/widget/TextView;

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f110021

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->tvPermission:Landroid/widget/TextView;

    if-nez v0, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    const v1, 0x7f110020

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    const/4 p1, 0x1

    new-array p1, p1, [Landroid/view/View;

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->installBtn:Lcom/miui/packageInstaller/view/AdActionButton;

    const/4 v1, 0x0

    aput-object v0, p1, v1

    invoke-static {p1}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object p1

    invoke-interface {p1}, Lmiuix/animation/e;->c()Lmiuix/animation/j;

    move-result-object p1

    const/high16 v0, 0x3f800000    # 1.0f

    new-array v2, v1, [Lmiuix/animation/j$b;

    invoke-interface {p1, v0, v2}, Lmiuix/animation/j;->G(F[Lmiuix/animation/j$b;)Lmiuix/animation/j;

    move-result-object p1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->installBtn:Lcom/miui/packageInstaller/view/AdActionButton;

    new-array v1, v1, [Lc9/a;

    invoke-interface {p1, v0, v1}, Lmiuix/animation/j;->z(Landroid/view/View;[Lc9/a;)V

    return-void
.end method


# virtual methods
.method public final getAppDes()Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->appDes:Landroid/widget/TextView;

    return-object v0
.end method

.method public final getAppDesLayout()Landroid/widget/LinearLayout;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->appDesLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method public final getAppIcon()Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->appIcon:Landroid/widget/ImageView;

    return-object v0
.end method

.method public final getAppName()Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->appName:Landroid/widget/TextView;

    return-object v0
.end method

.method public final getAppSize()Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->appSize:Landroid/widget/TextView;

    return-object v0
.end method

.method public final getInstallBtn()Lcom/miui/packageInstaller/view/AdActionButton;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->installBtn:Lcom/miui/packageInstaller/view/AdActionButton;

    return-object v0
.end method

.method public final getMediaContainer()Landroid/widget/FrameLayout;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->mediaContainer:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method public final getTvDeveloper()Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->tvDeveloper:Landroid/widget/TextView;

    return-object v0
.end method

.method public final getTvPermission()Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->tvPermission:Landroid/widget/TextView;

    return-object v0
.end method

.method public final getTvPrivacy()Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->tvPrivacy:Landroid/widget/TextView;

    return-object v0
.end method

.method public final getTvVersion()Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->tvVersion:Landroid/widget/TextView;

    return-object v0
.end method

.method public final setAppDes(Landroid/widget/TextView;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->appDes:Landroid/widget/TextView;

    return-void
.end method

.method public final setAppDesLayout(Landroid/widget/LinearLayout;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->appDesLayout:Landroid/widget/LinearLayout;

    return-void
.end method

.method public final setAppIcon(Landroid/widget/ImageView;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->appIcon:Landroid/widget/ImageView;

    return-void
.end method

.method public final setAppName(Landroid/widget/TextView;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->appName:Landroid/widget/TextView;

    return-void
.end method

.method public final setAppSize(Landroid/widget/TextView;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->appSize:Landroid/widget/TextView;

    return-void
.end method

.method public final setInstallBtn(Lcom/miui/packageInstaller/view/AdActionButton;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->installBtn:Lcom/miui/packageInstaller/view/AdActionButton;

    return-void
.end method

.method public final setMediaContainer(Landroid/widget/FrameLayout;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->mediaContainer:Landroid/widget/FrameLayout;

    return-void
.end method

.method public final setTvDeveloper(Landroid/widget/TextView;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->tvDeveloper:Landroid/widget/TextView;

    return-void
.end method

.method public final setTvPermission(Landroid/widget/TextView;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->tvPermission:Landroid/widget/TextView;

    return-void
.end method

.method public final setTvPrivacy(Landroid/widget/TextView;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->tvPrivacy:Landroid/widget/TextView;

    return-void
.end method

.method public final setTvVersion(Landroid/widget/TextView;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject$ViewHolder;->tvVersion:Landroid/widget/TextView;

    return-void
.end method
