.class public final Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;
.super Landroidx/recyclerview/widget/RecyclerView$d0;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ViewHolder"
.end annotation


# instance fields
.field private appIcon:Landroid/widget/ImageView;

.field private appName:Landroid/widget/TextView;

.field private appPermissionLayout:Landroid/widget/LinearLayout;

.field private appPermissionNum:Landroid/widget/TextView;

.field private appRankType:Landroid/widget/TextView;

.field private appRanking:Landroid/widget/TextView;

.field private appStar:Landroid/widget/TextView;

.field private installDes:Landroid/widget/TextView;

.field private installNum:Landroid/widget/TextView;

.field private llAppDownloadLayout:Landroid/widget/LinearLayout;

.field private llAppScoreLayout:Landroid/widget/LinearLayout;

.field private llAppTags:Landroid/widget/LinearLayout;

.field private llRootLayout:Landroid/widget/LinearLayout;

.field private rankLayout:Landroid/widget/LinearLayout;

.field private scoreNum:Landroid/widget/TextView;

.field private version:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    const-string v0, "itemView"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$d0;-><init>(Landroid/view/View;)V

    const v0, 0x7f0a0068

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "itemView.findViewById(R.id.appIcon)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->appIcon:Landroid/widget/ImageView;

    const v0, 0x7f0a006b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "itemView.findViewById(R.id.appName)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->appName:Landroid/widget/TextView;

    const v0, 0x7f0a03e0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "itemView.findViewById(R.id.version)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->version:Landroid/widget/TextView;

    const v0, 0x7f0a0070

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "itemView.findViewById(R.id.appScore)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->appStar:Landroid/widget/TextView;

    const v0, 0x7f0a0071

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "itemView.findViewById(R.id.appScoreNum)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->scoreNum:Landroid/widget/TextView;

    const v0, 0x7f0a006c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "itemView.findViewById(R.id.appPermission)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->appPermissionNum:Landroid/widget/TextView;

    const v0, 0x7f0a006d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "itemView.findViewById(R.id.appPermissionLayout)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->appPermissionLayout:Landroid/widget/LinearLayout;

    const v0, 0x7f0a0069

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "itemView.findViewById(R.id.appInstallNum)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->installNum:Landroid/widget/TextView;

    const v0, 0x7f0a006a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "itemView.findViewById(R.id.appInstallNumDes)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->installDes:Landroid/widget/TextView;

    const v0, 0x7f0a006f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "itemView.findViewById(R.id.appRanking)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->appRanking:Landroid/widget/TextView;

    const v0, 0x7f0a006e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "itemView.findViewById(R.id.appRankType)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->appRankType:Landroid/widget/TextView;

    const v0, 0x7f0a01f6

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "itemView.findViewById(R.id.llRank)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->rankLayout:Landroid/widget/LinearLayout;

    const v0, 0x7f0a01f4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "itemView.findViewById(R.id.llAppScoreLayout)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->llAppScoreLayout:Landroid/widget/LinearLayout;

    const v0, 0x7f0a01f3

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "itemView.findViewById(R.id.llAppDownloadLayout)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->llAppDownloadLayout:Landroid/widget/LinearLayout;

    const v0, 0x7f0a01f5

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->llAppTags:Landroid/widget/LinearLayout;

    const v0, 0x7f0a01fd

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->llRootLayout:Landroid/widget/LinearLayout;

    return-void
.end method


# virtual methods
.method public final getAppIcon()Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->appIcon:Landroid/widget/ImageView;

    return-object v0
.end method

.method public final getAppName()Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->appName:Landroid/widget/TextView;

    return-object v0
.end method

.method public final getAppPermissionLayout()Landroid/widget/LinearLayout;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->appPermissionLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method public final getAppPermissionNum()Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->appPermissionNum:Landroid/widget/TextView;

    return-object v0
.end method

.method public final getAppRankType()Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->appRankType:Landroid/widget/TextView;

    return-object v0
.end method

.method public final getAppRanking()Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->appRanking:Landroid/widget/TextView;

    return-object v0
.end method

.method public final getAppStar()Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->appStar:Landroid/widget/TextView;

    return-object v0
.end method

.method public final getInstallDes()Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->installDes:Landroid/widget/TextView;

    return-object v0
.end method

.method public final getInstallNum()Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->installNum:Landroid/widget/TextView;

    return-object v0
.end method

.method public final getLlAppDownloadLayout()Landroid/widget/LinearLayout;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->llAppDownloadLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method public final getLlAppScoreLayout()Landroid/widget/LinearLayout;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->llAppScoreLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method public final getLlAppTags()Landroid/widget/LinearLayout;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->llAppTags:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method public final getLlRootLayout()Landroid/widget/LinearLayout;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->llRootLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method public final getRankLayout()Landroid/widget/LinearLayout;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->rankLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method public final getScoreNum()Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->scoreNum:Landroid/widget/TextView;

    return-object v0
.end method

.method public final getVersion()Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->version:Landroid/widget/TextView;

    return-object v0
.end method

.method public final setAppIcon(Landroid/widget/ImageView;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->appIcon:Landroid/widget/ImageView;

    return-void
.end method

.method public final setAppName(Landroid/widget/TextView;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->appName:Landroid/widget/TextView;

    return-void
.end method

.method public final setAppPermissionLayout(Landroid/widget/LinearLayout;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->appPermissionLayout:Landroid/widget/LinearLayout;

    return-void
.end method

.method public final setAppPermissionNum(Landroid/widget/TextView;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->appPermissionNum:Landroid/widget/TextView;

    return-void
.end method

.method public final setAppRankType(Landroid/widget/TextView;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->appRankType:Landroid/widget/TextView;

    return-void
.end method

.method public final setAppRanking(Landroid/widget/TextView;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->appRanking:Landroid/widget/TextView;

    return-void
.end method

.method public final setAppStar(Landroid/widget/TextView;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->appStar:Landroid/widget/TextView;

    return-void
.end method

.method public final setInstallDes(Landroid/widget/TextView;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->installDes:Landroid/widget/TextView;

    return-void
.end method

.method public final setInstallNum(Landroid/widget/TextView;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->installNum:Landroid/widget/TextView;

    return-void
.end method

.method public final setLlAppDownloadLayout(Landroid/widget/LinearLayout;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->llAppDownloadLayout:Landroid/widget/LinearLayout;

    return-void
.end method

.method public final setLlAppScoreLayout(Landroid/widget/LinearLayout;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->llAppScoreLayout:Landroid/widget/LinearLayout;

    return-void
.end method

.method public final setLlAppTags(Landroid/widget/LinearLayout;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->llAppTags:Landroid/widget/LinearLayout;

    return-void
.end method

.method public final setLlRootLayout(Landroid/widget/LinearLayout;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->llRootLayout:Landroid/widget/LinearLayout;

    return-void
.end method

.method public final setRankLayout(Landroid/widget/LinearLayout;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->rankLayout:Landroid/widget/LinearLayout;

    return-void
.end method

.method public final setScoreNum(Landroid/widget/TextView;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->scoreNum:Landroid/widget/TextView;

    return-void
.end method

.method public final setVersion(Landroid/widget/TextView;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->version:Landroid/widget/TextView;

    return-void
.end method
