.class public final Lcom/miui/packageInstaller/view/h$e;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/packageInstaller/view/h;->e2()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/miui/packageInstaller/view/h;


# direct methods
.method constructor <init>(Lcom/miui/packageInstaller/view/h;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/view/h$e;->a:Lcom/miui/packageInstaller/view/h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 8

    iget-object v0, p0, Lcom/miui/packageInstaller/view/h$e;->a:Lcom/miui/packageInstaller/view/h;

    invoke-static {v0}, Lcom/miui/packageInstaller/view/h;->Q1(Lcom/miui/packageInstaller/view/h;)Landroid/view/ViewGroup;

    move-result-object v0

    const-string v1, "rootLayout"

    const/4 v2, 0x0

    if-nez v0, :cond_0

    invoke-static {v1}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v2

    :cond_0
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/view/h$e;->a:Lcom/miui/packageInstaller/view/h;

    invoke-static {v0}, Lcom/miui/packageInstaller/view/h;->Q1(Lcom/miui/packageInstaller/view/h;)Landroid/view/ViewGroup;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-static {v1}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v2

    :cond_1
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getWidth()I

    move-result v0

    invoke-static {}, Lf6/d;->g()I

    move-result v1

    if-eq v0, v1, :cond_7

    iget-object v0, p0, Lcom/miui/packageInstaller/view/h$e;->a:Lcom/miui/packageInstaller/view/h;

    invoke-static {v0}, Lcom/miui/packageInstaller/view/h;->O1(Lcom/miui/packageInstaller/view/h;)Landroid/widget/LinearLayout;

    move-result-object v0

    if-nez v0, :cond_2

    const-string v0, "llReason"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    move-object v2, v0

    :goto_0
    iget-object v0, p0, Lcom/miui/packageInstaller/view/h$e;->a:Lcom/miui/packageInstaller/view/h;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    const-string v3, "null cannot be cast to non-null type android.widget.LinearLayout.LayoutParams"

    invoke-static {v1, v3}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->N()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget v4, v4, Landroid/content/res/Configuration;->orientation:I

    const v5, 0x7f070114

    const v6, 0x7f0700d0

    const/4 v7, 0x1

    if-ne v4, v7, :cond_3

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->N()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    goto :goto_1

    :cond_3
    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->N()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    :goto_1
    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout$LayoutParams;->setMarginStart(I)V

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->N()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget v4, v4, Landroid/content/res/Configuration;->orientation:I

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->N()Landroid/content/res/Resources;

    move-result-object v0

    if-ne v4, v7, :cond_4

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    goto :goto_2

    :cond_4
    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    :goto_2
    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout$LayoutParams;->setMarginEnd(I)V

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/view/h$e;->a:Lcom/miui/packageInstaller/view/h;

    invoke-static {v0}, Lcom/miui/packageInstaller/view/h;->N1(Lcom/miui/packageInstaller/view/h;)Landroid/widget/ImageView;

    move-result-object v0

    if-eqz v0, :cond_7

    iget-object v1, p0, Lcom/miui/packageInstaller/view/h$e;->a:Lcom/miui/packageInstaller/view/h;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    invoke-static {v2, v3}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v1}, Landroidx/fragment/app/Fragment;->N()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    if-ne v3, v7, :cond_5

    invoke-virtual {v1}, Landroidx/fragment/app/Fragment;->N()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0700f1

    goto :goto_3

    :cond_5
    invoke-virtual {v1}, Landroidx/fragment/app/Fragment;->N()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0700dc

    :goto_3
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    iput v3, v2, Landroid/widget/LinearLayout$LayoutParams;->width:I

    invoke-virtual {v1}, Landroidx/fragment/app/Fragment;->N()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    invoke-virtual {v1}, Landroidx/fragment/app/Fragment;->N()Landroid/content/res/Resources;

    move-result-object v1

    if-ne v3, v7, :cond_6

    const v3, 0x7f0700cc

    goto :goto_4

    :cond_6
    const v3, 0x7f0700ba

    :goto_4
    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, v2, Landroid/widget/LinearLayout$LayoutParams;->height:I

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_7
    return-void
.end method
