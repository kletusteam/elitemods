.class public Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;
.super Lq2/b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization$b;,
        Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization$a;,
        Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization$c;
    }
.end annotation


# static fields
.field public static final E:Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization$b;


# instance fields
.field private A:Z

.field private B:[Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization$a;

.field private C:Lh6/a;

.field private D:La6/v;

.field private u:Lmiuix/slidingwidget/widget/SlidingButton;

.field private v:Landroid/widget/TextView;

.field private w:Landroid/widget/TextView;

.field private x:Landroid/widget/TextView;

.field private y:Landroid/widget/TextView;

.field private z:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization$b;-><init>(Lm8/g;)V

    sput-object v0, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;->E:Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization$b;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lq2/b;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;->A:Z

    return-void
.end method

.method public static synthetic J0(Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;Landroid/widget/CompoundButton;Z)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;->V0(Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;Landroid/widget/CompoundButton;Z)V

    return-void
.end method

.method public static synthetic K0(Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;Landroid/content/DialogInterface;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;->S0(Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;Landroid/content/DialogInterface;)V

    return-void
.end method

.method public static final synthetic L0(Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;->Q0(Z)V

    return-void
.end method

.method public static final synthetic M0(Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;->R0(Z)V

    return-void
.end method

.method public static final synthetic N0(Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;)Lh6/a;
    .locals 0

    iget-object p0, p0, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;->C:Lh6/a;

    return-object p0
.end method

.method public static final synthetic O0(Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;)La6/v;
    .locals 0

    iget-object p0, p0, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;->D:La6/v;

    return-object p0
.end method

.method public static final synthetic P0(Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;->X0(Z)V

    return-void
.end method

.method private final Q0(Z)V
    .locals 2

    invoke-static {p0}, Lm2/b;->g(Landroid/content/Context;)Lm2/b;

    move-result-object v0

    invoke-virtual {v0, p1}, Lm2/b;->F(Z)V

    iput-boolean p1, p0, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;->A:Z

    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;->Y0()V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "risk_authorization_broadcast.Action"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "risk_authorization"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;->u:Lmiuix/slidingwidget/widget/SlidingButton;

    if-nez v0, :cond_0

    const-string v0, "mSlidingButton"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v0, 0x0

    :cond_0
    invoke-virtual {v0, p1}, Lmiuix/slidingwidget/widget/SlidingButton;->setChecked(Z)V

    return-void
.end method

.method private final R0(Z)V
    .locals 6

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;->C:Lh6/a;

    const/4 v1, 0x0

    const-string v2, "fingerPrintHelper"

    if-nez v0, :cond_0

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_0
    invoke-virtual {v0}, Lh6/a;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "ro.hardware.fp.fod"

    const/4 v3, 0x0

    invoke-static {v0, v3}, Lcom/android/packageinstaller/compat/SystemPropertiesCompat;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    new-instance v4, La6/i;

    invoke-direct {v4, p0}, La6/i;-><init>(Landroid/content/Context;)V

    new-instance v5, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization$d;

    invoke-direct {v5, p0, v4, p1}, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization$d;-><init>(Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;La6/i;Z)V

    new-instance p1, Le6/b;

    invoke-direct {p1, p0}, Le6/b;-><init>(Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;)V

    invoke-virtual {v4, p1}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    if-eqz v0, :cond_1

    const/4 p1, 0x4

    invoke-virtual {v4, p1}, La6/i;->g(I)V

    invoke-virtual {v4, p1}, La6/i;->h(I)V

    goto :goto_0

    :cond_1
    invoke-virtual {v4, v3}, La6/i;->g(I)V

    invoke-virtual {v4, v3}, La6/i;->h(I)V

    :goto_0
    iget-object p1, p0, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;->C:Lh6/a;

    if-nez p1, :cond_2

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    move-object v1, p1

    :goto_1
    invoke-virtual {v1, v5}, Lh6/a;->c(Lh6/b;)V

    invoke-virtual {v4}, Landroid/app/Dialog;->show()V

    goto :goto_2

    :cond_3
    invoke-static {p0}, Lh6/c;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {}, Lh6/c;->a()Landroid/content/Intent;

    move-result-object p1

    const/16 v0, 0x3f2

    invoke-virtual {p0, p1, v0}, Landroidx/activity/ComponentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_2

    :cond_4
    invoke-direct {p0, p1}, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;->Q0(Z)V

    :goto_2
    return-void
.end method

.method private static final S0(Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;Landroid/content/DialogInterface;)V
    .locals 0

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p0, p0, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;->C:Lh6/a;

    if-nez p0, :cond_0

    const-string p0, "fingerPrintHelper"

    invoke-static {p0}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 p0, 0x0

    :cond_0
    invoke-virtual {p0}, Lh6/a;->a()V

    return-void
.end method

.method private final T0()V
    .locals 1

    invoke-static {p0}, Lm2/b;->g(Landroid/content/Context;)Lm2/b;

    move-result-object v0

    invoke-virtual {v0}, Lm2/b;->h()Z

    move-result v0

    iput-boolean v0, p0, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;->A:Z

    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;->Y0()V

    return-void
.end method

.method private final U0()V
    .locals 9

    const v0, 0x7f0a028c

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.open_risk)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lmiuix/slidingwidget/widget/SlidingButton;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;->u:Lmiuix/slidingwidget/widget/SlidingButton;

    const v0, 0x7f0a03d4

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.tv_xiaomi_account_text)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;->v:Landroid/widget/TextView;

    const v0, 0x7f0a03b9

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.tv_lock_screen_text)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;->w:Landroid/widget/TextView;

    const v0, 0x7f0a03b3

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.tv_finger_text)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;->x:Landroid/widget/TextView;

    const v0, 0x7f0a03ba

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.tv_lock_screen_tip)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;->y:Landroid/widget/TextView;

    const v0, 0x7f0a03aa

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.tv_app_check_tip)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;->z:Landroid/widget/TextView;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization$a;

    new-instance v1, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization$a;

    sget-object v2, Lx5/a;->f:Lx5/a;

    const v3, 0x7f0a03fc

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const-string v4, "findViewById(R.id.xiaomi_account_risk)"

    invoke-static {v3, v4}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const v4, 0x7f0a01d9

    invoke-virtual {p0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    const-string v5, "findViewById(R.id.iv_xiaomi_account_arrow)"

    invoke-static {v4, v5}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v4, Landroid/widget/ImageView;

    invoke-direct {v1, p0, v2, v3, v4}, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization$a;-><init>(Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;Lx5/a;Landroid/view/View;Landroid/widget/ImageView;)V

    const/4 v2, 0x0

    aput-object v1, v0, v2

    new-instance v1, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization$a;

    sget-object v3, Lx5/a;->c:Lx5/a;

    const v4, 0x7f0a020a

    invoke-virtual {p0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    const-string v5, "findViewById(R.id.lock_screen_risk)"

    invoke-static {v4, v5}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const v5, 0x7f0a01d4

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    const-string v6, "findViewById(R.id.iv_lock_screen_arrow)"

    invoke-static {v5, v6}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v5, Landroid/widget/ImageView;

    invoke-direct {v1, p0, v3, v4, v5}, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization$a;-><init>(Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;Lx5/a;Landroid/view/View;Landroid/widget/ImageView;)V

    const/4 v3, 0x1

    aput-object v1, v0, v3

    new-instance v1, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization$a;

    sget-object v4, Lx5/a;->d:Lx5/a;

    const v5, 0x7f0a016a

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    const-string v6, "findViewById(R.id.finger_risk)"

    invoke-static {v5, v6}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const v6, 0x7f0a01d0

    invoke-virtual {p0, v6}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    const-string v7, "findViewById(R.id.iv_finger_arrow)"

    invoke-static {v6, v7}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v6, Landroid/widget/ImageView;

    invoke-direct {v1, p0, v4, v5, v6}, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization$a;-><init>(Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;Lx5/a;Landroid/view/View;Landroid/widget/ImageView;)V

    const/4 v4, 0x2

    aput-object v1, v0, v4

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;->B:[Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization$a;

    invoke-static {p0}, Lm2/b;->g(Landroid/content/Context;)Lm2/b;

    move-result-object v0

    invoke-virtual {v0}, Lm2/b;->i()Lx5/a;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;->B:[Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization$a;

    const/4 v4, 0x0

    if-nez v1, :cond_0

    const-string v1, "authorizationTypes"

    invoke-static {v1}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v1, v4

    :cond_0
    array-length v5, v1

    move v6, v2

    :goto_0
    if-ge v6, v5, :cond_2

    aget-object v7, v1, v6

    invoke-virtual {v7}, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization$a;->c()Lx5/a;

    move-result-object v8

    if-ne v8, v0, :cond_1

    move v8, v3

    goto :goto_1

    :cond_1
    move v8, v2

    :goto_1
    invoke-virtual {v7, v8}, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization$a;->e(Z)V

    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;->u:Lmiuix/slidingwidget/widget/SlidingButton;

    if-nez v0, :cond_3

    const-string v0, "mSlidingButton"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_2

    :cond_3
    move-object v4, v0

    :goto_2
    new-instance v0, Le6/c;

    invoke-direct {v0, p0}, Le6/c;-><init>(Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;)V

    invoke-virtual {v4, v0}, Lmiuix/slidingwidget/widget/SlidingButton;->setOnPerformCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    return-void
.end method

.method private static final V0(Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;Landroid/widget/CompoundButton;Z)V
    .locals 1

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_0

    invoke-direct {p0, p2}, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;->Q0(Z)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;->u:Lmiuix/slidingwidget/widget/SlidingButton;

    if-nez p1, :cond_1

    const-string p1, "mSlidingButton"

    invoke-static {p1}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 p1, 0x0

    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lmiuix/slidingwidget/widget/SlidingButton;->setChecked(Z)V

    invoke-direct {p0, p2}, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;->a1(Z)V

    :goto_0
    return-void
.end method

.method private final X0(Z)V
    .locals 3

    new-instance v0, Lt5/k;

    invoke-direct {v0}, Lt5/k;-><init>()V

    new-instance v1, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization$e;

    invoke-direct {v1, p0, p1}, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization$e;-><init>(Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;Z)V

    const p1, 0x7f1103e2

    invoke-virtual {p0, p1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string v2, "getString(R.string.verify_account_close_title)"

    invoke-static {p1, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p0, v1, p1}, Lt5/k;->i(Landroid/app/Activity;Lt5/k$a;Ljava/lang/String;)V

    return-void
.end method

.method private final Y0()V
    .locals 14

    iget-boolean v0, p0, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;->A:Z

    const-string v1, "mSlidingButton"

    const-string v2, "authorizationTypes"

    const-string v3, "noLockTipText"

    const-string v4, "desTip"

    const/16 v5, 0x8

    const/4 v6, 0x0

    const/4 v7, 0x0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;->z:Landroid/widget/TextView;

    if-nez v0, :cond_0

    invoke-static {v4}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v6

    :cond_0
    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;->B:[Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization$a;

    if-nez v0, :cond_1

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v6

    :cond_1
    array-length v2, v0

    move v4, v7

    move v8, v4

    move v9, v8

    :goto_0
    const/4 v10, 0x1

    if-ge v4, v2, :cond_5

    aget-object v11, v0, v4

    invoke-virtual {v11}, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization$a;->d()Landroid/view/View;

    move-result-object v12

    invoke-virtual {v12, v7}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v11}, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization$a;->d()Landroid/view/View;

    move-result-object v12

    invoke-virtual {v11}, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization$a;->c()Lx5/a;

    move-result-object v13

    invoke-virtual {p0, v13}, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;->W0(Lx5/a;)Z

    move-result v13

    invoke-virtual {v12, v13}, Landroid/view/View;->setEnabled(Z)V

    invoke-static {p0}, Lm2/b;->g(Landroid/content/Context;)Lm2/b;

    move-result-object v12

    invoke-virtual {v12}, Lm2/b;->i()Lx5/a;

    move-result-object v12

    invoke-virtual {v11}, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization$a;->c()Lx5/a;

    move-result-object v13

    if-ne v12, v13, :cond_2

    invoke-virtual {v11}, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization$a;->d()Landroid/view/View;

    move-result-object v12

    invoke-virtual {v12}, Landroid/view/View;->isEnabled()Z

    move-result v12

    if-nez v12, :cond_2

    move v8, v10

    :cond_2
    if-eqz v8, :cond_3

    invoke-virtual {v11}, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization$a;->d()Landroid/view/View;

    move-result-object v12

    invoke-virtual {v12}, Landroid/view/View;->isEnabled()Z

    move-result v12

    if-eqz v12, :cond_3

    invoke-virtual {v11}, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization$a;->c()Lx5/a;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;->Z0(Lx5/a;)V

    move v8, v7

    :cond_3
    invoke-virtual {v11}, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization$a;->d()Landroid/view/View;

    move-result-object v11

    invoke-virtual {v11}, Landroid/view/View;->isEnabled()Z

    move-result v11

    if-nez v11, :cond_4

    move v9, v10

    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_5
    if-eqz v8, :cond_6

    sget-object v0, Lx5/a;->f:Lx5/a;

    invoke-virtual {p0, v0}, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;->Z0(Lx5/a;)V

    :cond_6
    iget-object v0, p0, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;->y:Landroid/widget/TextView;

    if-nez v0, :cond_7

    invoke-static {v3}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v6

    :cond_7
    if-eqz v9, :cond_8

    move v5, v7

    :cond_8
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;->u:Lmiuix/slidingwidget/widget/SlidingButton;

    if-nez v0, :cond_9

    invoke-static {v1}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_1

    :cond_9
    move-object v6, v0

    :goto_1
    invoke-virtual {v6, v10}, Lmiuix/slidingwidget/widget/SlidingButton;->setChecked(Z)V

    goto :goto_4

    :cond_a
    iget-object v0, p0, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;->z:Landroid/widget/TextView;

    if-nez v0, :cond_b

    invoke-static {v4}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v6

    :cond_b
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;->y:Landroid/widget/TextView;

    if-nez v0, :cond_c

    invoke-static {v3}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v6

    :cond_c
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;->B:[Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization$a;

    if-nez v0, :cond_d

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v6

    :cond_d
    array-length v2, v0

    move v3, v7

    :goto_2
    if-ge v3, v2, :cond_e

    aget-object v4, v0, v3

    invoke-virtual {v4}, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization$a;->d()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_e
    iget-object v0, p0, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;->u:Lmiuix/slidingwidget/widget/SlidingButton;

    if-nez v0, :cond_f

    invoke-static {v1}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_3

    :cond_f
    move-object v6, v0

    :goto_3
    invoke-virtual {v6, v7}, Lmiuix/slidingwidget/widget/SlidingButton;->setChecked(Z)V

    :goto_4
    return-void
.end method

.method private final a1(Z)V
    .locals 2

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;->D:La6/v;

    if-nez v0, :cond_0

    new-instance v0, La6/v;

    invoke-direct {v0, p0}, La6/v;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;->D:La6/v;

    new-instance v1, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization$f;

    invoke-direct {v1, p0, p1}, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization$f;-><init>(Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;Z)V

    invoke-virtual {v0, v1}, La6/v;->k(La6/v$a;)V

    :cond_0
    iget-object p1, p0, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;->D:La6/v;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Landroid/app/Dialog;->show()V

    :cond_1
    return-void
.end method


# virtual methods
.method public final W0(Lx5/a;)Z
    .locals 1

    const-string v0, "type"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization$c;->a:[I

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_3

    const/4 v0, 0x2

    if-eq p1, v0, :cond_2

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;->C:Lh6/a;

    if-nez p1, :cond_1

    const-string p1, "fingerPrintHelper"

    invoke-static {p1}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 p1, 0x0

    :cond_1
    invoke-virtual {p1}, Lh6/a;->b()Z

    move-result v0

    goto :goto_0

    :cond_2
    invoke-static {p0}, Lh6/c;->b(Landroid/content/Context;)Z

    move-result v0

    :cond_3
    :goto_0
    return v0
.end method

.method public final Z0(Lx5/a;)V
    .locals 6

    const-string v0, "type"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;->B:[Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization$a;

    if-nez v0, :cond_0

    const-string v0, "authorizationTypes"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v0, 0x0

    :cond_0
    array-length v1, v0

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v1, :cond_2

    aget-object v4, v0, v3

    invoke-virtual {v4}, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization$a;->c()Lx5/a;

    move-result-object v5

    if-ne v5, p1, :cond_1

    const/4 v5, 0x1

    goto :goto_1

    :cond_1
    move v5, v2

    :goto_1
    invoke-virtual {v4, v5}, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization$a;->e(Z)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "type = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lx5/a;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "setCurrentSelectedAuthorizationType"

    invoke-static {v1, v0}, Lf6/o;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    invoke-static {p0}, Lm2/b;->g(Landroid/content/Context;)Lm2/b;

    move-result-object v0

    invoke-virtual {v0, p1}, Lm2/b;->G(Lx5/a;)V

    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    invoke-super {p0, p1, p2, p3}, Landroidx/fragment/app/e;->onActivityResult(IILandroid/content/Intent;)V

    const/16 p3, 0x3f2

    if-ne p1, p3, :cond_0

    const/4 p1, -0x1

    if-ne p1, p2, :cond_0

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;->Q0(Z)V

    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lq2/b;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "risk_authorization"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result p1

    iput-boolean p1, p0, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;->A:Z

    const p1, 0x7f0d017c

    invoke-virtual {p0, p1}, Lmiuix/appcompat/app/j;->setContentView(I)V

    new-instance p1, Lh6/a;

    invoke-direct {p1, p0}, Lh6/a;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;->C:Lh6/a;

    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;->U0()V

    return-void
.end method

.method protected onDestroy()V
    .locals 1

    invoke-super {p0}, Landroidx/fragment/app/e;->onDestroy()V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;->D:La6/v;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 0

    invoke-super {p0}, Landroidx/fragment/app/e;->onResume()V

    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;->T0()V

    return-void
.end method
