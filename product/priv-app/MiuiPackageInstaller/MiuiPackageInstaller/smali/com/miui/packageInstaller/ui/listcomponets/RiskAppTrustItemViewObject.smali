.class public final Lcom/miui/packageInstaller/ui/listcomponets/RiskAppTrustItemViewObject;
.super Lm6/a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/packageInstaller/ui/listcomponets/RiskAppTrustItemViewObject$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lm6/a<",
        "Lcom/miui/packageInstaller/ui/listcomponets/RiskAppTrustItemViewObject$ViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private final l:Lr6/c;

.field private final m:Lm5/e;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lr6/c;Lm5/e;Ll6/c;Lm6/b;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "riskAppInfo"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2, p4, p5}, Lm6/a;-><init>(Landroid/content/Context;Ljava/lang/Object;Ll6/c;Lm6/b;)V

    iput-object p2, p0, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppTrustItemViewObject;->l:Lr6/c;

    iput-object p3, p0, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppTrustItemViewObject;->m:Lm5/e;

    return-void
.end method

.method private static final B(Landroid/widget/TextView;Lcom/miui/packageInstaller/ui/listcomponets/RiskAppTrustItemViewObject;Landroid/view/View;)V
    .locals 3

    const-string p2, "$this_apply"

    invoke-static {p0, p2}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "this$0"

    invoke-static {p1, p2}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p2, Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lcom/miui/packageInstaller/ui/secure/RiskAppTrustDetailActivity;

    invoke-direct {p2, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v0, p1, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppTrustItemViewObject;->l:Lr6/c;

    const-string v1, "apk_info"

    invoke-virtual {p2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    iget-object v0, p1, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppTrustItemViewObject;->m:Lm5/e;

    const-string v1, "caller"

    invoke-virtual {p2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v0

    instance-of v0, v0, Lq2/b;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "null cannot be cast to non-null type com.android.packageinstaller.miui.BaseActivity"

    invoke-static {v0, v1}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lq2/b;

    const/16 v2, 0x64

    invoke-virtual {v0, p2, v2}, Landroidx/activity/ComponentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    new-instance p2, Lp5/b;

    invoke-virtual {p0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-static {p0, v1}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p0, Lq2/b;

    const-string v0, "trust_risk_app_setting"

    const-string v1, "button"

    invoke-direct {p2, v0, v1, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    iget-object p0, p1, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppTrustItemViewObject;->l:Lr6/c;

    invoke-virtual {p0}, Lr6/c;->f()Ljava/lang/String;

    move-result-object p0

    if-nez p0, :cond_0

    const-string p0, ""

    :cond_0
    const-string p1, "related_file_name"

    invoke-virtual {p2, p1, p0}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p0

    invoke-virtual {p0}, Lp5/f;->c()Z

    :cond_1
    return-void
.end method

.method public static synthetic z(Landroid/widget/TextView;Lcom/miui/packageInstaller/ui/listcomponets/RiskAppTrustItemViewObject;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppTrustItemViewObject;->B(Landroid/widget/TextView;Lcom/miui/packageInstaller/ui/listcomponets/RiskAppTrustItemViewObject;Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public A(Lcom/miui/packageInstaller/ui/listcomponets/RiskAppTrustItemViewObject$ViewHolder;)V
    .locals 3

    const/4 v0, 0x1

    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppTrustItemViewObject$ViewHolder;->getTvAppName()Landroid/widget/TextView;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppTrustItemViewObject;->l:Lr6/c;

    invoke-virtual {v2}, Lr6/c;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppTrustItemViewObject;->l:Lr6/c;

    invoke-virtual {v1}, Lr6/c;->b()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-static {v1}, Lu8/g;->l(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    move v1, v0

    :goto_1
    if-nez v1, :cond_2

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/bumptech/glide/b;->t(Landroid/content/Context;)Lcom/bumptech/glide/k;

    move-result-object v1

    iget-object v2, p0, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppTrustItemViewObject;->l:Lr6/c;

    invoke-virtual {v2}, Lr6/c;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/bumptech/glide/k;->t(Ljava/lang/String;)Lcom/bumptech/glide/j;

    move-result-object v1

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppTrustItemViewObject$ViewHolder;->getIvAppIcon()Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/bumptech/glide/j;->w0(Landroid/widget/ImageView;)Ln3/i;

    move-result-object v1

    const-string v2, "{\n                Glide.\u2026.ivAppIcon)\n            }"

    invoke-static {v1, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_2

    :cond_2
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppTrustItemViewObject$ViewHolder;->getIvAppIcon()Landroid/widget/ImageView;

    move-result-object v1

    const v2, 0x7f080146

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    sget-object v1, La8/v;->a:La8/v;

    :cond_3
    :goto_2
    if-eqz p1, :cond_8

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppTrustItemViewObject$ViewHolder;->getTvStatus()Landroid/widget/TextView;

    move-result-object p1

    if-eqz p1, :cond_8

    iget-object v1, p0, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppTrustItemViewObject;->l:Lr6/c;

    invoke-virtual {v1}, Lr6/c;->i()Ljava/lang/Integer;

    move-result-object v1

    if-nez v1, :cond_4

    goto :goto_4

    :cond_4
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v2, v0, :cond_5

    invoke-virtual {p1}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f1102cf

    :goto_3
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_6

    :cond_5
    :goto_4
    const/4 v0, 0x2

    if-nez v1, :cond_6

    goto :goto_5

    :cond_6
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v1, v0, :cond_7

    invoke-virtual {p1}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f1102e2

    goto :goto_3

    :cond_7
    :goto_5
    invoke-virtual {p1}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f1102db

    goto :goto_3

    :goto_6
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v0, Lcom/miui/packageInstaller/ui/listcomponets/w;

    invoke-direct {v0, p1, p0}, Lcom/miui/packageInstaller/ui/listcomponets/w;-><init>(Landroid/widget/TextView;Lcom/miui/packageInstaller/ui/listcomponets/RiskAppTrustItemViewObject;)V

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_8
    return-void
.end method

.method public k()I
    .locals 1

    const v0, 0x7f0d014d

    return v0
.end method

.method public bridge synthetic o(Landroidx/recyclerview/widget/RecyclerView$d0;)V
    .locals 0

    check-cast p1, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppTrustItemViewObject$ViewHolder;

    invoke-virtual {p0, p1}, Lcom/miui/packageInstaller/ui/listcomponets/RiskAppTrustItemViewObject;->A(Lcom/miui/packageInstaller/ui/listcomponets/RiskAppTrustItemViewObject$ViewHolder;)V

    return-void
.end method
