.class public final Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;
.super Lm5/z0;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity$a;
    }
.end annotation


# static fields
.field public static final n0:Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity$a;


# instance fields
.field private V:Landroid/widget/FrameLayout;

.field private W:Landroidx/constraintlayout/widget/ConstraintLayout;

.field private X:Landroid/widget/ImageView;

.field private Y:Landroid/widget/ImageView;

.field private Z:Landroid/widget/TextView;

.field private e0:Landroid/widget/TextView;

.field private f0:Landroid/widget/TextView;

.field private g0:Landroid/widget/TextView;

.field private h0:Landroid/widget/TextView;

.field private i0:Landroid/widget/Button;

.field private j0:Landroid/widget/Button;

.field private k0:Landroid/widget/LinearLayout;

.field private l0:Lcom/airbnb/lottie/LottieAnimationView;

.field private m0:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity$a;-><init>(Lm8/g;)V

    sput-object v0, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;->n0:Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lm5/z0;-><init>()V

    return-void
.end method

.method private final A2()V
    .locals 6

    invoke-static {p0}, Landroidx/lifecycle/k;->a(Landroidx/lifecycle/j;)Landroidx/lifecycle/e;

    move-result-object v0

    invoke-static {}, Lv8/t0;->c()Lv8/v1;

    move-result-object v1

    new-instance v3, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity$b;

    const/4 v2, 0x0

    invoke-direct {v3, p0, v2}, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity$b;-><init>(Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;Ld8/d;)V

    const/4 v4, 0x2

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lv8/f;->d(Lv8/e0;Ld8/g;Lv8/g0;Ll8/p;ILjava/lang/Object;)Lv8/k1;

    return-void
.end method

.method private final B2()V
    .locals 3

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;->l0:Lcom/airbnb/lottie/LottieAnimationView;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const-string v0, "lottieImage"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_0
    invoke-virtual {v0}, Lcom/airbnb/lottie/LottieAnimationView;->h()V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;->k0:Landroid/widget/LinearLayout;

    if-nez v0, :cond_1

    const-string v0, "loadingLayout"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_1
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;->W:Landroidx/constraintlayout/widget/ConstraintLayout;

    if-nez v0, :cond_2

    const-string v0, "clContentLayout"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    move-object v1, v0

    :goto_0
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    return-void
.end method

.method private final C2()V
    .locals 3

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;->j0:Landroid/widget/Button;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const-string v0, "btnExit"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_0
    new-instance v2, Lz5/d;

    invoke-direct {v2, p0}, Lz5/d;-><init>(Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;->i0:Landroid/widget/Button;

    if-nez v0, :cond_1

    const-string v0, "btnParentAuth"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move-object v1, v0

    :goto_0
    new-instance v0, Lz5/b;

    invoke-direct {v0, p0}, Lz5/b;-><init>(Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;)V

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0a0089

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lz5/c;

    invoke-direct {v1, p0}, Lz5/c;-><init>(Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private static final D2(Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;Landroid/view/View;)V
    .locals 2

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Lp5/b;

    const-string v0, "cancel_btn"

    const-string v1, "button"

    invoke-direct {p1, v0, v1, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p1}, Lp5/f;->c()Z

    invoke-virtual {p0}, Lmiuix/appcompat/app/j;->finish()V

    return-void
.end method

.method private static final E2(Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;Landroid/view/View;)V
    .locals 2

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0}, Lcom/android/packageinstaller/utils/g;->w(Landroid/content/Context;)Z

    move-result p1

    if-nez p1, :cond_0

    const p1, 0x7f1103ba

    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object p0

    invoke-virtual {p0}, Landroid/widget/Toast;->show()V

    return-void

    :cond_0
    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object p1

    new-instance v0, Lz5/e;

    invoke-direct {v0, p0}, Lz5/e;-><init>(Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;)V

    invoke-virtual {p1, v0}, Lf6/z;->g(Ljava/lang/Runnable;)V

    invoke-static {p0}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->isSafeModelEnable(Landroid/content/Context;)Z

    move-result p1

    if-nez p1, :cond_1

    const/4 p1, 0x1

    invoke-static {p0, p1}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->setSafeModelEnabled(Landroid/content/Context;Z)V

    :cond_1
    new-instance p1, Lp5/b;

    const-string v0, "parent_verify_btn"

    const-string v1, "button"

    invoke-direct {p1, v0, v1, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p1}, Lp5/f;->c()Z

    return-void
.end method

.method private static final F2(Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0}, Lcom/android/packageinstaller/utils/x;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;->A2()V

    :cond_0
    return-void
.end method

.method private static final G2(Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;Landroid/view/View;)V
    .locals 0

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lm5/z0;->onBackPressed()V

    return-void
.end method

.method private final H2()V
    .locals 15

    sget-object v0, Lf6/s;->a:Lf6/s$a;

    invoke-virtual {v0}, Lf6/s$a;->a()Lf6/s;

    move-result-object v0

    const-string v1, "minorLITips"

    invoke-virtual {v0, v1}, Lf6/s;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/miui/packageInstaller/model/MinorLaunchInstallerTips;

    invoke-static {v0, v1}, Lcom/android/packageinstaller/utils/j;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/miui/packageInstaller/model/MinorLaunchInstallerTips;

    invoke-static {p0}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->isSafeModelEnable(Landroid/content/Context;)Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v0, :cond_3

    iget-object v3, p0, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;->e0:Landroid/widget/TextView;

    if-nez v3, :cond_0

    const-string v3, "tvTitle"

    invoke-static {v3}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v3, v2

    :cond_0
    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/MinorLaunchInstallerTips;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;->f0:Landroid/widget/TextView;

    if-nez v3, :cond_1

    const-string v3, "tvContent"

    invoke-static {v3}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v3, v2

    :cond_1
    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/MinorLaunchInstallerTips;->getContent()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    if-nez v1, :cond_3

    iget-object v3, p0, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;->i0:Landroid/widget/Button;

    if-nez v3, :cond_2

    const-string v3, "btnParentAuth"

    invoke-static {v3}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v3, v2

    :cond_2
    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/MinorLaunchInstallerTips;->getButton()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    const-string v3, "tvButtonAbove"

    if-eqz v1, :cond_5

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;->g0:Landroid/widget/TextView;

    if-nez v0, :cond_4

    invoke-static {v3}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    move-object v2, v0

    :goto_0
    const/16 v0, 0x8

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_a

    :cond_5
    iget-object v1, p0, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;->g0:Landroid/widget/TextView;

    if-nez v1, :cond_6

    invoke-static {v3}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v1, v2

    :cond_6
    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    new-instance v1, Lp5/g;

    const-string v5, "safe_protect_btn"

    const-string v6, "button"

    invoke-direct {v1, v5, v6, p0}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v1}, Lp5/f;->c()Z

    const-string v1, ""

    const/4 v5, 0x1

    if-eqz v0, :cond_e

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/MinorLaunchInstallerTips;->getButtonAbove()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_e

    invoke-interface {v6}, Ljava/lang/CharSequence;->length()I

    move-result v0

    move v1, v4

    :goto_1
    const/16 v7, 0x23

    const/4 v8, -0x1

    if-ge v1, v0, :cond_9

    invoke-interface {v6, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v9

    if-ne v9, v7, :cond_7

    move v9, v5

    goto :goto_2

    :cond_7
    move v9, v4

    :goto_2
    if-eqz v9, :cond_8

    goto :goto_3

    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_9
    move v1, v8

    :goto_3
    add-int/2addr v1, v5

    invoke-interface {v6}, Ljava/lang/CharSequence;->length()I

    move-result v0

    add-int/2addr v0, v8

    if-ltz v0, :cond_d

    :goto_4
    add-int/lit8 v9, v0, -0x1

    invoke-interface {v6, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v10

    if-ne v10, v7, :cond_a

    move v10, v5

    goto :goto_5

    :cond_a
    move v10, v4

    :goto_5
    if-eqz v10, :cond_b

    move v8, v0

    goto :goto_6

    :cond_b
    if-gez v9, :cond_c

    goto :goto_6

    :cond_c
    move v0, v9

    goto :goto_4

    :cond_d
    :goto_6
    invoke-virtual {v6, v1, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    const-string v0, "this as java.lang.String\u2026ing(startIndex, endIndex)"

    invoke-static {v1, v0}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v9, 0x0

    const/4 v10, 0x4

    const/4 v11, 0x0

    const-string v7, "#"

    const-string v8, ""

    invoke-static/range {v6 .. v11}, Lu8/g;->r(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v14, v1

    move-object v1, v0

    move-object v0, v14

    goto :goto_7

    :cond_e
    move-object v0, v1

    :goto_7
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v6

    if-nez v6, :cond_f

    move v6, v5

    goto :goto_8

    :cond_f
    move v6, v4

    :goto_8
    if-eqz v6, :cond_10

    const v1, 0x7f110064

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v6, "getString(R.string.child_open_safe_mode_tips)"

    invoke-static {v1, v6}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_10
    move-object v9, v1

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-nez v1, :cond_11

    move v4, v5

    :cond_11
    if-eqz v4, :cond_12

    const v0, 0x7f110063

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "getString(R.string.child\u2026pen_safe_mode_click_tips)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_12
    move-object v10, v0

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;->g0:Landroid/widget/TextView;

    if-nez v0, :cond_13

    invoke-static {v3}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v8, v2

    goto :goto_9

    :cond_13
    move-object v8, v0

    :goto_9
    const v0, 0x7f060050

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getColor(I)I

    move-result v11

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getColor(I)I

    move-result v12

    sget-object v7, Lf6/b0;->a:Lf6/b0$a;

    new-instance v13, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity$c;

    invoke-direct {v13, p0}, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity$c;-><init>(Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;)V

    invoke-virtual/range {v7 .. v13}, Lf6/b0$a;->a(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;IILf6/b0$a$a;)V

    :goto_a
    return-void
.end method

.method private final I2()V
    .locals 4

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;->X:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    const-string v0, "ivTips"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v0, 0x0

    :cond_0
    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    const-string v2, "null cannot be cast to non-null type androidx.constraintlayout.widget.ConstraintLayout.LayoutParams"

    invoke-static {v1, v2}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Landroidx/constraintlayout/widget/ConstraintLayout$b;

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0700d0

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f07015b

    :goto_0
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    iput v2, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_2
    return-void
.end method

.method private final J2(Ljava/lang/String;)V
    .locals 4

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    const/high16 v1, 0x2000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v2, 0x1

    const/16 v3, 0x1d

    if-le v1, v3, :cond_0

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    const-string v3, "content"

    invoke-static {v3, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :try_start_0
    invoke-virtual {p0}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {p0, v1, v3, v2}, Landroid/app/Activity;->grantUriPermission(Ljava/lang/String;Landroid/net/Uri;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    :cond_1
    :goto_0
    const-class v1, Lcom/miui/packageInstaller/ui/ChildIdentifyWebViewActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p0}, Lm5/z0;->m1()Lcom/miui/packageInstaller/model/ApkInfo;

    move-result-object v1

    const-string v3, "apk_info"

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "from_type"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "jump_url"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private final K2()V
    .locals 3

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;->k0:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const-string v0, "loadingLayout"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_0
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;->W:Landroidx/constraintlayout/widget/ConstraintLayout;

    if-nez v0, :cond_1

    const-string v0, "clContentLayout"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_1
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;->l0:Lcom/airbnb/lottie/LottieAnimationView;

    if-nez v0, :cond_2

    const-string v0, "lottieImage"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    move-object v1, v0

    :goto_0
    invoke-virtual {v1}, Lcom/airbnb/lottie/LottieAnimationView;->p()V

    return-void
.end method

.method public static synthetic v2(Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;)V
    .locals 0

    invoke-static {p0}, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;->F2(Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;)V

    return-void
.end method

.method public static synthetic w2(Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;->E2(Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic x2(Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;->G2(Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic y2(Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;->D2(Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;Landroid/view/View;)V

    return-void
.end method

.method public static final synthetic z2(Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;->J2(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public E0()Ljava/lang/String;
    .locals 1

    const-string v0, "child_launch_install_before"

    return-object v0
.end method

.method public F1()V
    .locals 4

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0d001d

    goto :goto_0

    :cond_0
    const v0, 0x7f0d001c

    :goto_0
    invoke-virtual {p0, v0}, Lmiuix/appcompat/app/j;->setContentView(I)V

    invoke-super {p0}, Lm5/z0;->F1()V

    const v0, 0x7f0a02d1

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/miui/packageInstaller/view/i;

    const v2, 0x7f080182

    invoke-direct {v1, v2}, Lcom/miui/packageInstaller/view/i;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    const v0, 0x7f0a017b

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.fl_title_layout)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;->V:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    if-nez v0, :cond_1

    const-string v0, "flTitleLayout"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_1
    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    const-string v2, "null cannot be cast to non-null type androidx.constraintlayout.widget.ConstraintLayout.LayoutParams"

    invoke-static {v0, v2}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroidx/constraintlayout/widget/ConstraintLayout$b;

    invoke-static {p0}, Lcom/android/packageinstaller/utils/u;->b(Landroid/app/Activity;)I

    move-result v2

    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    const v0, 0x7f0a00d6

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v2, "findViewById(R.id.cl_content_view)"

    invoke-static {v0, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroidx/constraintlayout/widget/ConstraintLayout;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;->W:Landroidx/constraintlayout/widget/ConstraintLayout;

    const v0, 0x7f0a01d8

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v2, "findViewById(R.id.iv_tips)"

    invoke-static {v0, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;->X:Landroid/widget/ImageView;

    const v0, 0x7f0a03d2

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v2, "findViewById(R.id.tv_tips_title)"

    invoke-static {v0, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;->e0:Landroid/widget/TextView;

    const v0, 0x7f0a03d1

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v2, "findViewById(R.id.tv_tips_content)"

    invoke-static {v0, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;->f0:Landroid/widget/TextView;

    const v0, 0x7f0a03ab

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v2, "findViewById(R.id.tv_button_above)"

    invoke-static {v0, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;->g0:Landroid/widget/TextView;

    const v0, 0x7f0a0073

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v2, "findViewById(R.id.app_icon)"

    invoke-static {v0, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;->Y:Landroid/widget/ImageView;

    const v0, 0x7f0a0075

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v2, "findViewById(R.id.app_name)"

    invoke-static {v0, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;->Z:Landroid/widget/TextView;

    const v0, 0x7f0a01ad

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v2, "findViewById(R.id.install_source)"

    invoke-static {v0, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;->h0:Landroid/widget/TextView;

    const v0, 0x7f0a00ab

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v2, "findViewById(R.id.btn_parent_auth)"

    invoke-static {v0, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;->i0:Landroid/widget/Button;

    const v0, 0x7f0a00a9

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v2, "findViewById(R.id.btn_exit)"

    invoke-static {v0, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;->j0:Landroid/widget/Button;

    const v0, 0x7f0a01fa

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v2, "findViewById(R.id.ll_loading_layout)"

    invoke-static {v0, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;->k0:Landroid/widget/LinearLayout;

    const v0, 0x7f0a0205

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v2, "findViewById(R.id.loadingText)"

    invoke-static {v0, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;->m0:Landroid/widget/TextView;

    const v0, 0x7f0a020d

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v2, "findViewById(R.id.lottieImage)"

    invoke-static {v0, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/airbnb/lottie/LottieAnimationView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;->l0:Lcom/airbnb/lottie/LottieAnimationView;

    invoke-static {p0}, Lcom/android/packageinstaller/utils/g;->t(Landroid/content/Context;)Z

    move-result v0

    const-string v2, "lottieImage"

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;->l0:Lcom/airbnb/lottie/LottieAnimationView;

    if-nez v0, :cond_2

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_2
    const-string v3, "dark_loading.json"

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;->l0:Lcom/airbnb/lottie/LottieAnimationView;

    if-nez v0, :cond_4

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_4
    const-string v3, "loading.json"

    :goto_1
    invoke-virtual {v0, v3}, Lcom/airbnb/lottie/LottieAnimationView;->setAnimation(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;->l0:Lcom/airbnb/lottie/LottieAnimationView;

    if-nez v0, :cond_5

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_5
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/airbnb/lottie/LottieAnimationView;->setRepeatMode(I)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;->l0:Lcom/airbnb/lottie/LottieAnimationView;

    if-nez v0, :cond_6

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_2

    :cond_6
    move-object v1, v0

    :goto_2
    const/4 v0, -0x1

    invoke-virtual {v1, v0}, Lcom/airbnb/lottie/LottieAnimationView;->setRepeatCount(I)V

    new-instance v0, Lp5/g;

    const-string v1, "parent_verify_btn"

    const-string v2, "button"

    invoke-direct {v0, v1, v2, p0}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    new-instance v0, Lp5/g;

    const-string v1, "cancel_btn"

    invoke-direct {v0, v1, v2, p0}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;->H2()V

    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;->C2()V

    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;->K2()V

    invoke-virtual {p0}, Lm5/z0;->P1()V

    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;->I2()V

    return-void
.end method

.method public Y1(Lcom/miui/packageInstaller/model/ApkInfo;I)V
    .locals 4

    invoke-virtual {p0}, Lm5/z0;->m1()Lcom/miui/packageInstaller/model/ApkInfo;

    move-result-object p1

    if-eqz p1, :cond_4

    iget-object p2, p0, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;->Y:Landroid/widget/ImageView;

    const/4 v0, 0x0

    if-nez p2, :cond_0

    const-string p2, "ivAppIcon"

    invoke-static {p2}, Lm8/i;->s(Ljava/lang/String;)V

    move-object p2, v0

    :cond_0
    invoke-virtual {p1}, Lcom/miui/packageInstaller/model/ApkInfo;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object p2, p0, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;->Z:Landroid/widget/TextView;

    if-nez p2, :cond_1

    const-string p2, "tvAppName"

    invoke-static {p2}, Lm8/i;->s(Ljava/lang/String;)V

    move-object p2, v0

    :cond_1
    invoke-virtual {p1}, Lcom/miui/packageInstaller/model/ApkInfo;->getLabel()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object p1, p0, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;->h0:Landroid/widget/TextView;

    if-nez p1, :cond_2

    const-string p1, "tvInstallSource"

    invoke-static {p1}, Lm8/i;->s(Ljava/lang/String;)V

    move-object p1, v0

    :cond_2
    const p2, 0x7f1102d9

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lm5/z0;->c()Lm5/e;

    move-result-object v3

    if-eqz v3, :cond_3

    iget-object v0, v3, Lm5/e;->e:Ljava/lang/String;

    :cond_3
    aput-object v0, v1, v2

    invoke-virtual {p0, p2, v1}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_4
    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;->B2()V

    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    const-string v0, "newConfig"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lm5/z0;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;->I2()V

    return-void
.end method

.method protected onDestroy()V
    .locals 1

    invoke-super {p0}, Lm5/z0;->onDestroy()V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/ChildAccountInstallerActivity;->l0:Lcom/airbnb/lottie/LottieAnimationView;

    if-nez v0, :cond_0

    const-string v0, "lottieImage"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v0, 0x0

    :cond_0
    invoke-virtual {v0}, Lcom/airbnb/lottie/LottieAnimationView;->h()V

    return-void
.end method

.method public y1()Landroid/content/Intent;
    .locals 1

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    return-object v0
.end method
