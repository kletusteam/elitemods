.class public final Lcom/miui/packageInstaller/ui/listcomponets/PureModeIntroduceViewObject$ViewHolder;
.super Landroidx/recyclerview/widget/RecyclerView$d0;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/packageInstaller/ui/listcomponets/PureModeIntroduceViewObject;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ViewHolder"
.end annotation


# instance fields
.field private final arrowRight:Landroidx/appcompat/widget/AppCompatImageView;

.field private final image:Landroidx/appcompat/widget/AppCompatImageView;

.field private final introductionDes:Landroidx/appcompat/widget/AppCompatTextView;

.field private final label:Landroidx/appcompat/widget/AppCompatTextView;

.field private final llLayout:Landroidx/appcompat/widget/LinearLayoutCompat;

.field private final title:Landroidx/appcompat/widget/AppCompatTextView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    const-string v0, "itemView"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$d0;-><init>(Landroid/view/View;)V

    const v0, 0x7f0a01a2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "itemView.findViewById(R.id.image)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroidx/appcompat/widget/AppCompatImageView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeIntroduceViewObject$ViewHolder;->image:Landroidx/appcompat/widget/AppCompatImageView;

    const v0, 0x7f0a03d3

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "itemView.findViewById(R.id.tv_title)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroidx/appcompat/widget/AppCompatTextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeIntroduceViewObject$ViewHolder;->title:Landroidx/appcompat/widget/AppCompatTextView;

    const v0, 0x7f0a01dc

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "itemView.findViewById(R.id.label)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroidx/appcompat/widget/AppCompatTextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeIntroduceViewObject$ViewHolder;->label:Landroidx/appcompat/widget/AppCompatTextView;

    const v0, 0x7f0a01b2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "itemView.findViewById(R.id.introduction_des)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroidx/appcompat/widget/AppCompatTextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeIntroduceViewObject$ViewHolder;->introductionDes:Landroidx/appcompat/widget/AppCompatTextView;

    const v0, 0x7f0a007f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "itemView.findViewById(R.id.arrow_right)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroidx/appcompat/widget/AppCompatImageView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeIntroduceViewObject$ViewHolder;->arrowRight:Landroidx/appcompat/widget/AppCompatImageView;

    const v0, 0x7f0a01f9

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v0, "itemView.findViewById(R.id.ll_description)"

    invoke-static {p1, v0}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroidx/appcompat/widget/LinearLayoutCompat;

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeIntroduceViewObject$ViewHolder;->llLayout:Landroidx/appcompat/widget/LinearLayoutCompat;

    return-void
.end method


# virtual methods
.method public final getArrowRight()Landroidx/appcompat/widget/AppCompatImageView;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeIntroduceViewObject$ViewHolder;->arrowRight:Landroidx/appcompat/widget/AppCompatImageView;

    return-object v0
.end method

.method public final getImage()Landroidx/appcompat/widget/AppCompatImageView;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeIntroduceViewObject$ViewHolder;->image:Landroidx/appcompat/widget/AppCompatImageView;

    return-object v0
.end method

.method public final getIntroductionDes()Landroidx/appcompat/widget/AppCompatTextView;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeIntroduceViewObject$ViewHolder;->introductionDes:Landroidx/appcompat/widget/AppCompatTextView;

    return-object v0
.end method

.method public final getLabel()Landroidx/appcompat/widget/AppCompatTextView;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeIntroduceViewObject$ViewHolder;->label:Landroidx/appcompat/widget/AppCompatTextView;

    return-object v0
.end method

.method public final getLlLayout()Landroidx/appcompat/widget/LinearLayoutCompat;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeIntroduceViewObject$ViewHolder;->llLayout:Landroidx/appcompat/widget/LinearLayoutCompat;

    return-object v0
.end method

.method public final getTitle()Landroidx/appcompat/widget/AppCompatTextView;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeIntroduceViewObject$ViewHolder;->title:Landroidx/appcompat/widget/AppCompatTextView;

    return-object v0
.end method
