.class public final Lcom/miui/packageInstaller/view/DevEditText$c;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/packageInstaller/view/DevEditText;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/miui/packageInstaller/view/DevEditText;


# direct methods
.method constructor <init>(Lcom/miui/packageInstaller/view/DevEditText;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/view/DevEditText$c;->a:Lcom/miui/packageInstaller/view/DevEditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 1

    const-string v0, "editable"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    const-string p2, "charSequence"

    invoke-static {p1, p2}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    const-string p2, "charSequence"

    invoke-static {p1, p2}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p2, p0, Lcom/miui/packageInstaller/view/DevEditText$c;->a:Lcom/miui/packageInstaller/view/DevEditText;

    invoke-static {p2}, Lcom/miui/packageInstaller/view/DevEditText;->c(Lcom/miui/packageInstaller/view/DevEditText;)I

    iget-object p2, p0, Lcom/miui/packageInstaller/view/DevEditText$c;->a:Lcom/miui/packageInstaller/view/DevEditText;

    invoke-static {p2, p1}, Lcom/miui/packageInstaller/view/DevEditText;->b(Lcom/miui/packageInstaller/view/DevEditText;Ljava/lang/CharSequence;)V

    iget-object p2, p0, Lcom/miui/packageInstaller/view/DevEditText$c;->a:Lcom/miui/packageInstaller/view/DevEditText;

    invoke-virtual {p2}, Lcom/miui/packageInstaller/view/DevEditText;->getMListener()Lcom/miui/packageInstaller/view/DevEditText$b;

    move-result-object p2

    if-eqz p2, :cond_0

    invoke-interface {p2, p1}, Lcom/miui/packageInstaller/view/DevEditText$b;->a(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method
