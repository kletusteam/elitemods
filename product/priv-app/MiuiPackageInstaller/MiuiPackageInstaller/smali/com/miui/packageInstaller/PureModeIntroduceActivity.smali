.class public final Lcom/miui/packageInstaller/PureModeIntroduceActivity;
.super Lq2/b;


# instance fields
.field private u:Lmiuix/recyclerview/widget/RecyclerView;

.field private v:Lj6/b;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lq2/b;-><init>()V

    return-void
.end method

.method public static final synthetic J0(Lcom/miui/packageInstaller/PureModeIntroduceActivity;)Lj6/b;
    .locals 0

    iget-object p0, p0, Lcom/miui/packageInstaller/PureModeIntroduceActivity;->v:Lj6/b;

    return-object p0
.end method

.method private final K0()V
    .locals 14

    const v0, 0x7f0a0216

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.main_content)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lmiuix/recyclerview/widget/RecyclerView;

    iput-object v0, p0, Lcom/miui/packageInstaller/PureModeIntroduceActivity;->u:Lmiuix/recyclerview/widget/RecyclerView;

    const/4 v1, 0x0

    const-string v2, "mRecyclerView"

    if-nez v0, :cond_0

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_0
    new-instance v3, Landroidx/recyclerview/widget/LinearLayoutManager;

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-direct {v3, p0, v4, v5}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    invoke-virtual {v0, v3}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$o;)V

    new-instance v0, Lj6/b;

    iget-object v3, p0, Lcom/miui/packageInstaller/PureModeIntroduceActivity;->u:Lmiuix/recyclerview/widget/RecyclerView;

    if-nez v3, :cond_1

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move-object v1, v3

    :goto_0
    invoke-direct {v0, v1}, Lj6/b;-><init>(Landroidx/recyclerview/widget/RecyclerView;)V

    iput-object v0, p0, Lcom/miui/packageInstaller/PureModeIntroduceActivity;->v:Lj6/b;

    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f030015

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v8

    const-string v0, "resources.getStringArray\u2026.pure_introduction_title)"

    invoke-static {v8, v0}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f030014

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v9

    const-string v0, "resources.getStringArray\u2026.pure_introduction_label)"

    invoke-static {v9, v0}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f030013

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v10

    const-string v0, "resources.getStringArray\u2026ay.pure_introduction_des)"

    invoke-static {v10, v0}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x5

    new-array v11, v0, [Ljava/lang/Integer;

    const v0, 0x7f08055c

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v11, v5

    const v0, 0x7f08055d

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v11, v4

    const/4 v0, 0x2

    const v1, 0x7f08055e

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v11, v0

    const/4 v0, 0x3

    const v1, 0x7f08055f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v11, v0

    const/4 v0, 0x4

    const v1, 0x7f080560

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v11, v0

    invoke-static {p0}, Landroidx/lifecycle/k;->a(Landroidx/lifecycle/j;)Landroidx/lifecycle/e;

    move-result-object v2

    invoke-static {}, Lv8/t0;->c()Lv8/v1;

    move-result-object v3

    const/4 v4, 0x0

    new-instance v5, Lcom/miui/packageInstaller/PureModeIntroduceActivity$a;

    const/4 v13, 0x0

    move-object v6, v5

    move-object v7, p0

    invoke-direct/range {v6 .. v13}, Lcom/miui/packageInstaller/PureModeIntroduceActivity$a;-><init>(Lcom/miui/packageInstaller/PureModeIntroduceActivity;[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/Integer;Ljava/util/List;Ld8/d;)V

    const/4 v6, 0x2

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lv8/f;->d(Lv8/e0;Ld8/g;Lv8/g0;Ll8/p;ILjava/lang/Object;)Lv8/k1;

    return-void
.end method


# virtual methods
.method public E0()Ljava/lang/String;
    .locals 1

    const-string v0, "safe_mode_function"

    return-object v0
.end method

.method public onBackPressed()V
    .locals 3

    invoke-super {p0}, Lmiuix/appcompat/app/j;->onBackPressed()V

    new-instance v0, Lp5/b;

    const-string v1, "page_back_btn"

    const-string v2, "button"

    invoke-direct {v0, v1, v2, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    const-string v1, "back_type"

    const-string v2, "system"

    invoke-virtual {v0, v1, v2}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object v0

    invoke-virtual {v0}, Lp5/f;->c()Z

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Lq2/b;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0d016d

    invoke-virtual {p0, p1}, Lmiuix/appcompat/app/j;->setContentView(I)V

    invoke-direct {p0}, Lcom/miui/packageInstaller/PureModeIntroduceActivity;->K0()V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    const-string v0, "item"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    new-instance v0, Lp5/b;

    const-string v1, "page_back_btn"

    const-string v2, "button"

    invoke-direct {v0, v1, v2, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    const-string v1, "back_type"

    const-string v2, "click_icon"

    invoke-virtual {v0, v1, v2}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object v0

    invoke-virtual {v0}, Lp5/f;->c()Z

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result p1

    return p1
.end method
