.class public final Lcom/miui/packageInstaller/model/RiskControlRules$Companion;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/packageInstaller/model/RiskControlRules;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lm8/g;)V
    .locals 0

    invoke-direct {p0}, Lcom/miui/packageInstaller/model/RiskControlRules$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final createFromCloudConfig(Ljava/lang/String;Lcom/miui/packageInstaller/model/RiskControlConfig;)Lcom/miui/packageInstaller/model/RiskControlRules;
    .locals 4

    const-string v0, "installer"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "config"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/miui/packageInstaller/model/RiskControlRules;

    invoke-direct {v0, p1}, Lcom/miui/packageInstaller/model/RiskControlRules;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/miui/packageInstaller/model/RiskControlConfig;->getRcllct()I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/miui/packageInstaller/model/RiskControlRules;->setRiskControlHumanVerifyCount(I)V

    invoke-virtual {p2}, Lcom/miui/packageInstaller/model/RiskControlConfig;->getRclmct()I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/miui/packageInstaller/model/RiskControlRules;->setRiskControlSecurityVerifyCount(I)V

    invoke-virtual {p2}, Lcom/miui/packageInstaller/model/RiskControlConfig;->getRclhct()I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/miui/packageInstaller/model/RiskControlRules;->setRiskControlAccountFirstVerifyCount(I)V

    invoke-virtual {p2}, Lcom/miui/packageInstaller/model/RiskControlConfig;->getRclt()Ljava/lang/String;

    move-result-object p1

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz p1, :cond_1

    invoke-static {p1}, Lu8/g;->l(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    move p1, v1

    goto :goto_1

    :cond_1
    :goto_0
    move p1, v2

    :goto_1
    if-nez p1, :cond_2

    invoke-virtual {p2}, Lcom/miui/packageInstaller/model/RiskControlConfig;->getRclt()Ljava/lang/String;

    move-result-object p1

    goto :goto_2

    :cond_2
    invoke-static {}, Lcom/android/packageinstaller/InstallerApplication;->g()Lcom/android/packageinstaller/InstallerApplication;

    move-result-object p1

    const v3, 0x7f1102eb

    invoke-virtual {p1, v3}, Landroid/app/Application;->getString(I)Ljava/lang/String;

    move-result-object p1

    :goto_2
    invoke-virtual {v0, p1}, Lcom/miui/packageInstaller/model/RiskControlRules;->setRiskControlLaunchTitle(Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/miui/packageInstaller/model/RiskControlConfig;->getRclc()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_4

    invoke-static {p1}, Lu8/g;->l(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_3

    goto :goto_3

    :cond_3
    move p1, v1

    goto :goto_4

    :cond_4
    :goto_3
    move p1, v2

    :goto_4
    if-nez p1, :cond_5

    invoke-virtual {p2}, Lcom/miui/packageInstaller/model/RiskControlConfig;->getRclc()Ljava/lang/String;

    move-result-object p1

    goto :goto_5

    :cond_5
    invoke-static {}, Lcom/android/packageinstaller/InstallerApplication;->g()Lcom/android/packageinstaller/InstallerApplication;

    move-result-object p1

    const v3, 0x7f1102ec

    invoke-virtual {p1, v3}, Landroid/app/Application;->getString(I)Ljava/lang/String;

    move-result-object p1

    :goto_5
    invoke-virtual {v0, p1}, Lcom/miui/packageInstaller/model/RiskControlRules;->setRiskControlLaunchContent(Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/miui/packageInstaller/model/RiskControlConfig;->getRclbl()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_7

    invoke-static {p1}, Lu8/g;->l(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_6

    goto :goto_6

    :cond_6
    move p1, v1

    goto :goto_7

    :cond_7
    :goto_6
    move p1, v2

    :goto_7
    if-nez p1, :cond_8

    invoke-virtual {p2}, Lcom/miui/packageInstaller/model/RiskControlConfig;->getRclbl()Ljava/lang/String;

    move-result-object p1

    goto :goto_8

    :cond_8
    invoke-static {}, Lcom/android/packageinstaller/InstallerApplication;->g()Lcom/android/packageinstaller/InstallerApplication;

    move-result-object p1

    const v3, 0x7f1103e1

    invoke-virtual {p1, v3}, Landroid/app/Application;->getString(I)Ljava/lang/String;

    move-result-object p1

    :goto_8
    invoke-virtual {v0, p1}, Lcom/miui/packageInstaller/model/RiskControlRules;->setRiskControlLaunchLeftButton(Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/miui/packageInstaller/model/RiskControlConfig;->getRclbr()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_9

    invoke-static {p1}, Lu8/g;->l(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_a

    :cond_9
    move v1, v2

    :cond_a
    if-nez v1, :cond_b

    invoke-virtual {p2}, Lcom/miui/packageInstaller/model/RiskControlConfig;->getRclbr()Ljava/lang/String;

    move-result-object p1

    goto :goto_9

    :cond_b
    invoke-static {}, Lcom/android/packageinstaller/InstallerApplication;->g()Lcom/android/packageinstaller/InstallerApplication;

    move-result-object p1

    const p2, 0x7f1100ad

    invoke-virtual {p1, p2}, Landroid/app/Application;->getString(I)Ljava/lang/String;

    move-result-object p1

    :goto_9
    invoke-virtual {v0, p1}, Lcom/miui/packageInstaller/model/RiskControlRules;->setRiskControlLaunchRightButton(Ljava/lang/String;)V

    return-object v0
.end method
