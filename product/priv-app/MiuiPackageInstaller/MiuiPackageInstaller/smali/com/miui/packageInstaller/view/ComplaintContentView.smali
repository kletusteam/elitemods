.class public final Lcom/miui/packageInstaller/view/ComplaintContentView;
.super Landroid/widget/FrameLayout;


# instance fields
.field private a:Landroidx/appcompat/widget/AppCompatEditText;

.field private b:Landroid/widget/TextView;

.field private c:Lcom/miui/packageInstaller/view/DevEditText$b;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-virtual {p0}, Lcom/miui/packageInstaller/view/ComplaintContentView;->c()V

    return-void
.end method

.method public static final synthetic a(Lcom/miui/packageInstaller/view/ComplaintContentView;)Lcom/miui/packageInstaller/view/DevEditText$b;
    .locals 0

    iget-object p0, p0, Lcom/miui/packageInstaller/view/ComplaintContentView;->c:Lcom/miui/packageInstaller/view/DevEditText$b;

    return-object p0
.end method

.method public static final synthetic b(Lcom/miui/packageInstaller/view/ComplaintContentView;)Landroid/widget/TextView;
    .locals 0

    iget-object p0, p0, Lcom/miui/packageInstaller/view/ComplaintContentView;->b:Landroid/widget/TextView;

    return-object p0
.end method


# virtual methods
.method public final c()V
    .locals 2

    invoke-virtual {p0}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0d005c

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    return-void
.end method

.method public final getEditText()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/miui/packageInstaller/view/ComplaintContentView;->a:Landroidx/appcompat/widget/AppCompatEditText;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const-string v0, "mEditText"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_0
    invoke-virtual {v0}, Landroidx/appcompat/widget/AppCompatEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {v0}, Lu8/g;->u0(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    :cond_1
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 2

    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    const v0, 0x7f0a0113

    invoke-virtual {p0, v0}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.dev_com_content_edit)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroidx/appcompat/widget/AppCompatEditText;

    iput-object v0, p0, Lcom/miui/packageInstaller/view/ComplaintContentView;->a:Landroidx/appcompat/widget/AppCompatEditText;

    const v0, 0x7f0a0115

    invoke-virtual {p0, v0}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.dev_com_content_text_num)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/view/ComplaintContentView;->b:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/miui/packageInstaller/view/ComplaintContentView;->a:Landroidx/appcompat/widget/AppCompatEditText;

    if-nez v0, :cond_0

    const-string v0, "mEditText"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v0, 0x0

    :cond_0
    new-instance v1, Lcom/miui/packageInstaller/view/ComplaintContentView$a;

    invoke-direct {v1, p0}, Lcom/miui/packageInstaller/view/ComplaintContentView$a;-><init>(Lcom/miui/packageInstaller/view/ComplaintContentView;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method

.method public final setText(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/view/ComplaintContentView;->a:Landroidx/appcompat/widget/AppCompatEditText;

    if-nez v0, :cond_0

    const-string v0, "mEditText"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v0, 0x0

    :cond_0
    if-nez p1, :cond_1

    const-string p1, ""

    :cond_1
    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final setTextChangedListener(Lcom/miui/packageInstaller/view/DevEditText$b;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/view/ComplaintContentView;->c:Lcom/miui/packageInstaller/view/DevEditText$b;

    return-void
.end method
