.class final Lcom/miui/packageInstaller/a$b;
.super Lf8/k;

# interfaces
.implements Ll8/p;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/packageInstaller/a;->H1(Lm5/e;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf8/k;",
        "Ll8/p<",
        "Lv8/e0;",
        "Ld8/d<",
        "-",
        "La8/v;",
        ">;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation runtime Lf8/f;
    c = "com.miui.packageInstaller.InstallProgressParentActivity$reportErrorApk$1"
    f = "InstallProgressParentActivity.kt"
    l = {
        0x2a1
    }
    m = "invokeSuspend"
.end annotation


# instance fields
.field e:I

.field final synthetic f:Lcom/miui/packageInstaller/a;

.field final synthetic g:Lm5/e;

.field final synthetic h:Ljava/lang/String;

.field final synthetic i:Ljava/lang/Integer;

.field final synthetic j:Ljava/lang/Integer;


# direct methods
.method constructor <init>(Lcom/miui/packageInstaller/a;Lm5/e;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ld8/d;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/miui/packageInstaller/a;",
            "Lm5/e;",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Ld8/d<",
            "-",
            "Lcom/miui/packageInstaller/a$b;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/miui/packageInstaller/a$b;->f:Lcom/miui/packageInstaller/a;

    iput-object p2, p0, Lcom/miui/packageInstaller/a$b;->g:Lm5/e;

    iput-object p3, p0, Lcom/miui/packageInstaller/a$b;->h:Ljava/lang/String;

    iput-object p4, p0, Lcom/miui/packageInstaller/a$b;->i:Ljava/lang/Integer;

    iput-object p5, p0, Lcom/miui/packageInstaller/a$b;->j:Ljava/lang/Integer;

    const/4 p1, 0x2

    invoke-direct {p0, p1, p6}, Lf8/k;-><init>(ILd8/d;)V

    return-void
.end method


# virtual methods
.method public final b(Ljava/lang/Object;Ld8/d;)Ld8/d;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ld8/d<",
            "*>;)",
            "Ld8/d<",
            "La8/v;",
            ">;"
        }
    .end annotation

    new-instance p1, Lcom/miui/packageInstaller/a$b;

    iget-object v1, p0, Lcom/miui/packageInstaller/a$b;->f:Lcom/miui/packageInstaller/a;

    iget-object v2, p0, Lcom/miui/packageInstaller/a$b;->g:Lm5/e;

    iget-object v3, p0, Lcom/miui/packageInstaller/a$b;->h:Ljava/lang/String;

    iget-object v4, p0, Lcom/miui/packageInstaller/a$b;->i:Ljava/lang/Integer;

    iget-object v5, p0, Lcom/miui/packageInstaller/a$b;->j:Ljava/lang/Integer;

    move-object v0, p1

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/miui/packageInstaller/a$b;-><init>(Lcom/miui/packageInstaller/a;Lm5/e;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ld8/d;)V

    return-object p1
.end method

.method public bridge synthetic g(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lv8/e0;

    check-cast p2, Ld8/d;

    invoke-virtual {p0, p1, p2}, Lcom/miui/packageInstaller/a$b;->q(Lv8/e0;Ld8/d;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final n(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 10

    invoke-static {}, Le8/b;->c()Ljava/lang/Object;

    move-result-object v0

    iget v1, p0, Lcom/miui/packageInstaller/a$b;->e:I

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_1

    if-ne v1, v2, :cond_0

    :try_start_0
    invoke-static {p1}, La8/n;->b(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    goto/16 :goto_7

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    invoke-static {p1}, La8/n;->b(Ljava/lang/Object;)V

    iget-object p1, p0, Lcom/miui/packageInstaller/a$b;->f:Lcom/miui/packageInstaller/a;

    invoke-virtual {p1}, Lcom/miui/packageInstaller/a;->f1()Lcom/miui/packageInstaller/model/ApkInfo;

    move-result-object p1

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/miui/packageInstaller/model/ApkInfo;->getOriginalUri()Landroid/net/Uri;

    move-result-object p1

    goto :goto_0

    :cond_2
    move-object p1, v3

    :goto_0
    if-eqz p1, :cond_f

    :try_start_1
    sget-object p1, Lf6/a;->a:Lf6/a$a;

    iget-object v1, p0, Lcom/miui/packageInstaller/a$b;->g:Lm5/e;

    iget-object v4, p0, Lcom/miui/packageInstaller/a$b;->f:Lcom/miui/packageInstaller/a;

    invoke-virtual {v4}, Lcom/miui/packageInstaller/a;->f1()Lcom/miui/packageInstaller/model/ApkInfo;

    move-result-object v4

    iput v2, p0, Lcom/miui/packageInstaller/a$b;->e:I

    invoke-virtual {p1, v1, v4, p0}, Lf6/a$a;->a(Lm5/e;Lcom/miui/packageInstaller/model/ApkInfo;Ld8/d;)Ljava/lang/Object;

    move-result-object p1

    if-ne p1, v0, :cond_3

    return-object v0

    :cond_3
    :goto_1
    iget-object p1, p0, Lcom/miui/packageInstaller/a$b;->f:Lcom/miui/packageInstaller/a;

    invoke-static {p1}, Lcom/miui/packageInstaller/a;->W0(Lcom/miui/packageInstaller/a;)Lo5/b;

    move-result-object p1

    iget-object v0, p0, Lcom/miui/packageInstaller/a$b;->h:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    const-string v1, ""

    if-nez v0, :cond_4

    move-object v0, v1

    :cond_4
    :try_start_2
    invoke-virtual {p1, v0}, Lo5/b;->q(Ljava/lang/String;)V

    iget-object p1, p0, Lcom/miui/packageInstaller/a$b;->f:Lcom/miui/packageInstaller/a;

    invoke-static {p1}, Lcom/miui/packageInstaller/a;->W0(Lcom/miui/packageInstaller/a;)Lo5/b;

    move-result-object p1

    iget-object v0, p0, Lcom/miui/packageInstaller/a$b;->f:Lcom/miui/packageInstaller/a;

    invoke-virtual {v0}, Lcom/miui/packageInstaller/a;->f1()Lcom/miui/packageInstaller/model/ApkInfo;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/ApkInfo;->getLabel()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_6

    :cond_5
    move-object v0, v1

    :cond_6
    invoke-virtual {p1, v0}, Lo5/b;->g(Ljava/lang/String;)V

    iget-object p1, p0, Lcom/miui/packageInstaller/a$b;->f:Lcom/miui/packageInstaller/a;

    invoke-static {p1}, Lcom/miui/packageInstaller/a;->W0(Lcom/miui/packageInstaller/a;)Lo5/b;

    move-result-object p1

    iget-object v0, p0, Lcom/miui/packageInstaller/a$b;->f:Lcom/miui/packageInstaller/a;

    invoke-virtual {v0}, Lcom/miui/packageInstaller/a;->f1()Lcom/miui/packageInstaller/model/ApkInfo;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/ApkInfo;->getPackageInfo()Landroid/content/pm/PackageInfo;

    move-result-object v0

    if-eqz v0, :cond_7

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    goto :goto_2

    :cond_7
    move-object v0, v3

    :goto_2
    if-nez v0, :cond_8

    move-object v0, v1

    :cond_8
    invoke-virtual {p1, v0}, Lo5/b;->w(Ljava/lang/String;)V

    iget-object p1, p0, Lcom/miui/packageInstaller/a$b;->f:Lcom/miui/packageInstaller/a;

    invoke-static {p1}, Lcom/miui/packageInstaller/a;->W0(Lcom/miui/packageInstaller/a;)Lo5/b;

    move-result-object p1

    iget-object v0, p0, Lcom/miui/packageInstaller/a$b;->f:Lcom/miui/packageInstaller/a;

    invoke-virtual {v0}, Lcom/miui/packageInstaller/a;->f1()Lcom/miui/packageInstaller/model/ApkInfo;

    move-result-object v0

    if-eqz v0, :cond_9

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/ApkInfo;->getPackageInfo()Landroid/content/pm/PackageInfo;

    move-result-object v0

    if-eqz v0, :cond_9

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    goto :goto_3

    :cond_9
    move-object v0, v3

    :goto_3
    if-nez v0, :cond_a

    goto :goto_4

    :cond_a
    move-object v1, v0

    :goto_4
    invoke-virtual {p1, v1}, Lo5/b;->h(Ljava/lang/String;)V

    new-instance p1, Lp5/d;

    const/4 v5, 0x0

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/miui/packageInstaller/a$b;->f:Lcom/miui/packageInstaller/a;

    const/4 v8, 0x3

    const/4 v9, 0x0

    move-object v4, p1

    invoke-direct/range {v4 .. v9}, Lp5/d;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;ILm8/g;)V

    const-string v0, "error_type"

    const-string v1, "break_into_installing"

    invoke-virtual {p1, v0, v1}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p1

    const-string v0, "error_file_name"

    iget-object v1, p0, Lcom/miui/packageInstaller/a$b;->f:Lcom/miui/packageInstaller/a;

    invoke-virtual {v1}, Lcom/miui/packageInstaller/a;->f1()Lcom/miui/packageInstaller/model/ApkInfo;

    move-result-object v1

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/ApkInfo;->getRealFileName()Ljava/lang/String;

    move-result-object v1

    goto :goto_5

    :cond_b
    move-object v1, v3

    :goto_5
    invoke-virtual {p1, v0, v1}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p1

    const-string v0, "error_file_path"

    iget-object v1, p0, Lcom/miui/packageInstaller/a$b;->f:Lcom/miui/packageInstaller/a;

    invoke-virtual {v1}, Lcom/miui/packageInstaller/a;->f1()Lcom/miui/packageInstaller/model/ApkInfo;

    move-result-object v1

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/ApkInfo;->getOriginalFilePath()Ljava/lang/String;

    move-result-object v1

    goto :goto_6

    :cond_c
    move-object v1, v3

    :goto_6
    invoke-virtual {p1, v0, v1}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p1

    const-string v0, "error_pkg_size"

    iget-object v1, p0, Lcom/miui/packageInstaller/a$b;->f:Lcom/miui/packageInstaller/a;

    invoke-virtual {v1}, Lcom/miui/packageInstaller/a;->f1()Lcom/miui/packageInstaller/model/ApkInfo;

    move-result-object v1

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/ApkInfo;->getFileSizeString()Ljava/lang/String;

    move-result-object v3

    :cond_d
    invoke-virtual {p1, v0, v3}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p1

    const-string v0, "from_uid"

    iget-object v1, p0, Lcom/miui/packageInstaller/a$b;->i:Ljava/lang/Integer;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p1

    const-string v0, "bind_uid"

    iget-object v1, p0, Lcom/miui/packageInstaller/a$b;->j:Ljava/lang/Integer;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p1

    invoke-virtual {p1}, Lp5/f;->c()Z

    iget-object p1, p0, Lcom/miui/packageInstaller/a$b;->f:Lcom/miui/packageInstaller/a;

    invoke-virtual {p1}, Lcom/miui/packageInstaller/a;->i1()Ls5/l;

    move-result-object p1

    if-eqz p1, :cond_f

    invoke-virtual {p1}, Ls5/l;->d()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_8

    :goto_7
    iget-object v0, p0, Lcom/miui/packageInstaller/a$b;->f:Lcom/miui/packageInstaller/a;

    invoke-virtual {v0}, Lcom/miui/packageInstaller/a;->i1()Ls5/l;

    move-result-object v0

    if-eqz v0, :cond_e

    invoke-virtual {v0}, Ls5/l;->d()V

    :cond_e
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :cond_f
    :goto_8
    sget-object p1, La8/v;->a:La8/v;

    return-object p1
.end method

.method public final q(Lv8/e0;Ld8/d;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lv8/e0;",
            "Ld8/d<",
            "-",
            "La8/v;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/miui/packageInstaller/a$b;->b(Ljava/lang/Object;Ld8/d;)Ld8/d;

    move-result-object p1

    check-cast p1, Lcom/miui/packageInstaller/a$b;

    sget-object p2, La8/v;->a:La8/v;

    invoke-virtual {p1, p2}, Lcom/miui/packageInstaller/a$b;->n(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method
