.class public final Lcom/miui/packageInstaller/model/MinorLaunchInstallerTips;
.super Ljava/lang/Object;


# instance fields
.field private button:Ljava/lang/String;

.field private buttonAbove:Ljava/lang/String;

.field private content:Ljava/lang/String;

.field private title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getButton()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/model/MinorLaunchInstallerTips;->button:Ljava/lang/String;

    return-object v0
.end method

.method public final getButtonAbove()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/model/MinorLaunchInstallerTips;->buttonAbove:Ljava/lang/String;

    return-object v0
.end method

.method public final getContent()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/model/MinorLaunchInstallerTips;->content:Ljava/lang/String;

    return-object v0
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/model/MinorLaunchInstallerTips;->title:Ljava/lang/String;

    return-object v0
.end method

.method public final setButton(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/model/MinorLaunchInstallerTips;->button:Ljava/lang/String;

    return-void
.end method

.method public final setButtonAbove(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/model/MinorLaunchInstallerTips;->buttonAbove:Ljava/lang/String;

    return-void
.end method

.method public final setContent(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/model/MinorLaunchInstallerTips;->content:Ljava/lang/String;

    return-void
.end method

.method public final setTitle(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/model/MinorLaunchInstallerTips;->title:Ljava/lang/String;

    return-void
.end method
