.class public final Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject$ViewHolder;
.super Landroidx/recyclerview/widget/RecyclerView$d0;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ViewHolder"
.end annotation


# instance fields
.field private clRootLayout:Landroidx/constraintlayout/widget/ConstraintLayout;

.field private onceMessage:Landroid/widget/TextView;

.field private permissionIcon:Landroid/widget/ImageView;

.field private permissionLayout:Landroid/widget/LinearLayout;

.field private permissionLayoutClick:Landroidx/constraintlayout/widget/Group;

.field private perssionTitle:Landroid/widget/TextView;

.field private refIds:[I

.field private tipIcon:Landroid/widget/ImageView;

.field private tipMessage:Landroid/widget/TextView;

.field private tipTItle:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    const-string v0, "itemView"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$d0;-><init>(Landroid/view/View;)V

    const v0, 0x7f0a00d7

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/constraintlayout/widget/ConstraintLayout;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject$ViewHolder;->clRootLayout:Landroidx/constraintlayout/widget/ConstraintLayout;

    const v0, 0x7f0a01d5

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject$ViewHolder;->tipIcon:Landroid/widget/ImageView;

    const v0, 0x7f0a03be

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject$ViewHolder;->tipTItle:Landroid/widget/TextView;

    const v0, 0x7f0a03bd

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject$ViewHolder;->tipMessage:Landroid/widget/TextView;

    const v0, 0x7f0a03bb

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject$ViewHolder;->onceMessage:Landroid/widget/TextView;

    const v0, 0x7f0a01d6

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject$ViewHolder;->permissionIcon:Landroid/widget/ImageView;

    const v0, 0x7f0a03bc

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject$ViewHolder;->perssionTitle:Landroid/widget/TextView;

    const v0, 0x7f0a02a4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/constraintlayout/widget/Group;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject$ViewHolder;->permissionLayoutClick:Landroidx/constraintlayout/widget/Group;

    const v0, 0x7f0a01fb

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject$ViewHolder;->permissionLayout:Landroid/widget/LinearLayout;

    iget-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject$ViewHolder;->permissionLayoutClick:Landroidx/constraintlayout/widget/Group;

    invoke-virtual {p1}, Landroidx/constraintlayout/widget/b;->getReferencedIds()[I

    move-result-object p1

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject$ViewHolder;->refIds:[I

    iget-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject$ViewHolder;->tipTItle:Landroid/widget/TextView;

    new-instance v0, Landroid/text/method/LinkMovementMethod;

    invoke-direct {v0}, Landroid/text/method/LinkMovementMethod;-><init>()V

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    return-void
.end method


# virtual methods
.method public final getClRootLayout()Landroidx/constraintlayout/widget/ConstraintLayout;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject$ViewHolder;->clRootLayout:Landroidx/constraintlayout/widget/ConstraintLayout;

    return-object v0
.end method

.method public final getOnceMessage()Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject$ViewHolder;->onceMessage:Landroid/widget/TextView;

    return-object v0
.end method

.method public final getPermissionIcon()Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject$ViewHolder;->permissionIcon:Landroid/widget/ImageView;

    return-object v0
.end method

.method public final getPermissionLayout()Landroid/widget/LinearLayout;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject$ViewHolder;->permissionLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method public final getPermissionLayoutClick()Landroidx/constraintlayout/widget/Group;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject$ViewHolder;->permissionLayoutClick:Landroidx/constraintlayout/widget/Group;

    return-object v0
.end method

.method public final getPerssionTitle()Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject$ViewHolder;->perssionTitle:Landroid/widget/TextView;

    return-object v0
.end method

.method public final getRefIds()[I
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject$ViewHolder;->refIds:[I

    return-object v0
.end method

.method public final getTipIcon()Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject$ViewHolder;->tipIcon:Landroid/widget/ImageView;

    return-object v0
.end method

.method public final getTipMessage()Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject$ViewHolder;->tipMessage:Landroid/widget/TextView;

    return-object v0
.end method

.method public final getTipTItle()Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject$ViewHolder;->tipTItle:Landroid/widget/TextView;

    return-object v0
.end method

.method public final setAllOnclickListener(Landroid/view/View$OnClickListener;)V
    .locals 5

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject$ViewHolder;->refIds:[I

    const-string v1, "refIds"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget v3, v0, v2

    iget-object v4, p0, Landroidx/recyclerview/widget/RecyclerView$d0;->itemView:Landroid/view/View;

    invoke-virtual {v4, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final setClRootLayout(Landroidx/constraintlayout/widget/ConstraintLayout;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject$ViewHolder;->clRootLayout:Landroidx/constraintlayout/widget/ConstraintLayout;

    return-void
.end method

.method public final setOnceMessage(Landroid/widget/TextView;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject$ViewHolder;->onceMessage:Landroid/widget/TextView;

    return-void
.end method

.method public final setPermissionIcon(Landroid/widget/ImageView;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject$ViewHolder;->permissionIcon:Landroid/widget/ImageView;

    return-void
.end method

.method public final setPermissionLayout(Landroid/widget/LinearLayout;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject$ViewHolder;->permissionLayout:Landroid/widget/LinearLayout;

    return-void
.end method

.method public final setPermissionLayoutClick(Landroidx/constraintlayout/widget/Group;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject$ViewHolder;->permissionLayoutClick:Landroidx/constraintlayout/widget/Group;

    return-void
.end method

.method public final setPerssionTitle(Landroid/widget/TextView;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject$ViewHolder;->perssionTitle:Landroid/widget/TextView;

    return-void
.end method

.method public final setRefIds([I)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject$ViewHolder;->refIds:[I

    return-void
.end method

.method public final setTipIcon(Landroid/widget/ImageView;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject$ViewHolder;->tipIcon:Landroid/widget/ImageView;

    return-void
.end method

.method public final setTipMessage(Landroid/widget/TextView;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject$ViewHolder;->tipMessage:Landroid/widget/TextView;

    return-void
.end method

.method public final setTipTItle(Landroid/widget/TextView;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/PureModeTipViewObject$ViewHolder;->tipTItle:Landroid/widget/TextView;

    return-void
.end method
