.class final Lcom/miui/packageInstaller/secure/view/ScreenLockPassword$b;
.super Lf8/k;

# interfaces
.implements Ll8/p;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->v(Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf8/k;",
        "Ll8/p<",
        "Lv8/e0;",
        "Ld8/d<",
        "-",
        "La8/v;",
        ">;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation runtime Lf8/f;
    c = "com.miui.packageInstaller.secure.view.ScreenLockPassword$onFinishInflate$4$1$2"
    f = "ScreenLockPassword.kt"
    l = {
        0x72
    }
    m = "invokeSuspend"
.end annotation


# instance fields
.field e:I

.field f:I

.field private synthetic g:Ljava/lang/Object;

.field final synthetic h:Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;

.field final synthetic i:Lf6/m$b;


# direct methods
.method constructor <init>(Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;Lf6/m$b;Ld8/d;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;",
            "Lf6/m$b;",
            "Ld8/d<",
            "-",
            "Lcom/miui/packageInstaller/secure/view/ScreenLockPassword$b;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword$b;->h:Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;

    iput-object p2, p0, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword$b;->i:Lf6/m$b;

    const/4 p1, 0x2

    invoke-direct {p0, p1, p3}, Lf8/k;-><init>(ILd8/d;)V

    return-void
.end method


# virtual methods
.method public final b(Ljava/lang/Object;Ld8/d;)Ld8/d;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ld8/d<",
            "*>;)",
            "Ld8/d<",
            "La8/v;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword$b;

    iget-object v1, p0, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword$b;->h:Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;

    iget-object v2, p0, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword$b;->i:Lf6/m$b;

    invoke-direct {v0, v1, v2, p2}, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword$b;-><init>(Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;Lf6/m$b;Ld8/d;)V

    iput-object p1, v0, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword$b;->g:Ljava/lang/Object;

    return-object v0
.end method

.method public bridge synthetic g(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lv8/e0;

    check-cast p2, Ld8/d;

    invoke-virtual {p0, p1, p2}, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword$b;->q(Lv8/e0;Ld8/d;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final n(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 20

    move-object/from16 v0, p0

    invoke-static {}, Le8/b;->c()Ljava/lang/Object;

    move-result-object v1

    iget v2, v0, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword$b;->f:I

    const-string v3, "passwordEditText"

    const-string v4, ""

    const/4 v5, 0x1

    if-eqz v2, :cond_1

    if-ne v2, v5, :cond_0

    iget v2, v0, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword$b;->e:I

    iget-object v7, v0, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword$b;->g:Ljava/lang/Object;

    check-cast v7, Lv8/e0;

    invoke-static/range {p1 .. p1}, La8/n;->b(Ljava/lang/Object;)V

    move-object v8, v0

    move v6, v5

    goto/16 :goto_2

    :cond_0
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static/range {p1 .. p1}, La8/n;->b(Ljava/lang/Object;)V

    iget-object v2, v0, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword$b;->g:Ljava/lang/Object;

    check-cast v2, Lv8/e0;

    iget-object v7, v0, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword$b;->h:Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;

    invoke-static {v7}, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->m(Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;)Landroid/widget/EditText;

    move-result-object v7

    if-nez v7, :cond_2

    invoke-static {v3}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v7, 0x0

    :cond_2
    invoke-virtual {v7, v4}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v7, v0, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword$b;->h:Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;

    iget-object v8, v0, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword$b;->i:Lf6/m$b;

    invoke-virtual {v8}, Lf6/m$b;->a()I

    move-result v8

    invoke-static {v7, v8}, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->o(Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;I)V

    const/16 v7, 0xc8

    move-object v8, v0

    move/from16 v19, v7

    move-object v7, v2

    move/from16 v2, v19

    :goto_0
    iget-object v9, v8, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword$b;->h:Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;

    invoke-static {v9}, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->n(Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;)I

    move-result v9

    const-string v10, "confirmButton"

    const-string v11, "errorTipsTextView"

    const/4 v12, 0x0

    if-lez v9, :cond_a

    iget-object v9, v8, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword$b;->h:Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;

    invoke-static {v9}, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->l(Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;)Landroid/widget/TextView;

    move-result-object v9

    if-nez v9, :cond_3

    invoke-static {v11}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v9, 0x0

    :cond_3
    invoke-virtual {v9, v12}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v9, v8, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword$b;->h:Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;

    invoke-static {v9}, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->l(Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;)Landroid/widget/TextView;

    move-result-object v9

    if-nez v9, :cond_4

    invoke-static {v11}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v9, 0x0

    :cond_4
    iget-object v13, v8, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword$b;->h:Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;

    invoke-virtual {v13}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v13

    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f0f0016

    iget-object v15, v8, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword$b;->h:Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;

    invoke-static {v15}, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->n(Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;)I

    move-result v15

    add-int/lit16 v15, v15, 0x1f4

    move-object/from16 p1, v7

    int-to-double v6, v15

    const-wide v16, 0x408f400000000000L    # 1000.0

    div-double v6, v6, v16

    invoke-static {v6, v7}, Lo8/a;->a(D)I

    move-result v6

    new-array v7, v5, [Ljava/lang/Object;

    iget-object v15, v8, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword$b;->h:Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;

    invoke-static {v15}, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->n(Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;)I

    move-result v15

    add-int/lit16 v15, v15, 0x1f4

    move/from16 v18, v6

    int-to-double v5, v15

    div-double v5, v5, v16

    invoke-static {v5, v6}, Lo8/a;->a(D)I

    move-result v5

    invoke-static {v5}, Lf8/b;->b(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v7, v12

    move/from16 v5, v18

    invoke-virtual {v13, v14, v5, v7}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v9, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v5, v8, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword$b;->h:Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;

    invoke-static {v5}, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->j(Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;)Landroid/widget/Button;

    move-result-object v5

    if-nez v5, :cond_5

    invoke-static {v10}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v5, 0x0

    :cond_5
    invoke-virtual {v5, v12}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v5, v8, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword$b;->h:Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;

    invoke-static {v5}, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->n(Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;)I

    move-result v6

    sub-int/2addr v6, v2

    invoke-static {v5, v6}, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->o(Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;I)V

    iget-object v5, v8, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword$b;->h:Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;

    invoke-static {v5}, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->k(Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;)Lx5/h;

    move-result-object v5

    if-eqz v5, :cond_6

    invoke-virtual {v5}, Lx5/h;->g()Z

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_7

    move v5, v6

    goto :goto_1

    :cond_6
    const/4 v6, 0x1

    :cond_7
    move v5, v12

    :goto_1
    if-eqz v5, :cond_9

    int-to-long v9, v2

    move-object/from16 v5, p1

    iput-object v5, v8, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword$b;->g:Ljava/lang/Object;

    iput v2, v8, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword$b;->e:I

    iput v6, v8, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword$b;->f:I

    invoke-static {v9, v10, v8}, Lv8/o0;->a(JLd8/d;)Ljava/lang/Object;

    move-result-object v7

    if-ne v7, v1, :cond_8

    return-object v1

    :cond_8
    move-object v7, v5

    :goto_2
    move v5, v6

    goto/16 :goto_0

    :cond_9
    move-object/from16 v5, p1

    const/4 v1, 0x0

    invoke-static {v5, v1, v6, v1}, Lv8/f0;->c(Lv8/e0;Ljava/util/concurrent/CancellationException;ILjava/lang/Object;)V

    goto :goto_3

    :cond_a
    move v6, v5

    const/4 v1, 0x0

    :goto_3
    iget-object v2, v8, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword$b;->h:Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;

    invoke-static {v2}, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->j(Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;)Landroid/widget/Button;

    move-result-object v2

    if-nez v2, :cond_b

    invoke-static {v10}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v2, v1

    :cond_b
    iget-object v5, v8, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword$b;->h:Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;

    invoke-static {v5}, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->m(Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;)Landroid/widget/EditText;

    move-result-object v5

    if-nez v5, :cond_c

    invoke-static {v3}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v5, v1

    :cond_c
    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    const-string v5, "passwordEditText.text"

    invoke-static {v3, v5}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-lez v3, :cond_d

    move v5, v6

    goto :goto_4

    :cond_d
    move v5, v12

    :goto_4
    invoke-virtual {v2, v5}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v2, v8, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword$b;->h:Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;

    invoke-static {v2}, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->l(Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;)Landroid/widget/TextView;

    move-result-object v2

    if-nez v2, :cond_e

    invoke-static {v11}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v2, v1

    :cond_e
    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, v8, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword$b;->h:Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;

    invoke-static {v2}, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;->l(Lcom/miui/packageInstaller/secure/view/ScreenLockPassword;)Landroid/widget/TextView;

    move-result-object v2

    if-nez v2, :cond_f

    invoke-static {v11}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v6, v1

    goto :goto_5

    :cond_f
    move-object v6, v2

    :goto_5
    const/16 v1, 0x8

    invoke-virtual {v6, v1}, Landroid/widget/TextView;->setVisibility(I)V

    sget-object v1, La8/v;->a:La8/v;

    return-object v1
.end method

.method public final q(Lv8/e0;Ld8/d;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lv8/e0;",
            "Ld8/d<",
            "-",
            "La8/v;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword$b;->b(Ljava/lang/Object;Ld8/d;)Ld8/d;

    move-result-object p1

    check-cast p1, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword$b;

    sget-object p2, La8/v;->a:La8/v;

    invoke-virtual {p1, p2}, Lcom/miui/packageInstaller/secure/view/ScreenLockPassword$b;->n(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method
