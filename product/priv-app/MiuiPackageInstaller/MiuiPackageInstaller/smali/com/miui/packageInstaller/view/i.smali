.class public final Lcom/miui/packageInstaller/view/i;
.super Landroid/graphics/drawable/Drawable;


# instance fields
.field private final a:I

.field private b:Landroid/graphics/drawable/Drawable;

.field private final c:Landroid/graphics/Rect;

.field private d:F

.field private e:I


# direct methods
.method public constructor <init>(I)V
    .locals 2

    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    iput p1, p0, Lcom/miui/packageInstaller/view/i;->a:I

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/miui/packageInstaller/view/i;->c:Landroid/graphics/Rect;

    invoke-static {}, Lcom/android/packageinstaller/InstallerApplication;->g()Lcom/android/packageinstaller/InstallerApplication;

    move-result-object v0

    const v1, 0x7f060512

    invoke-virtual {v0, v1}, Landroid/app/Application;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/miui/packageInstaller/view/i;->e:I

    invoke-static {}, Lcom/android/packageinstaller/InstallerApplication;->g()Lcom/android/packageinstaller/InstallerApplication;

    move-result-object v0

    const v1, 0x7f080182

    invoke-virtual {v0, v1}, Landroid/app/Application;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-static {v0}, Lm8/i;->c(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/miui/packageInstaller/view/i;->b:Landroid/graphics/drawable/Drawable;

    invoke-static {}, Lcom/android/packageinstaller/InstallerApplication;->g()Lcom/android/packageinstaller/InstallerApplication;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/Application;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    invoke-static {p1}, Lm8/i;->c(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/view/i;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result p1

    int-to-float p1, p1

    iget-object v0, p0, Lcom/miui/packageInstaller/view/i;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr p1, v0

    iput p1, p0, Lcom/miui/packageInstaller/view/i;->d:F

    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 1

    const-string v0, "canvas"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget v0, p0, Lcom/miui/packageInstaller/view/i;->e:I

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    iget-object v0, p0, Lcom/miui/packageInstaller/view/i;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method public getOpacity()I
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/view/i;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getOpacity()I

    move-result v0

    return v0
.end method

.method protected onBoundsChange(Landroid/graphics/Rect;)V
    .locals 3

    const-string v0, "bounds"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Landroid/graphics/drawable/Drawable;->onBoundsChange(Landroid/graphics/Rect;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/view/i;->c:Landroid/graphics/Rect;

    iget v1, p1, Landroid/graphics/Rect;->left:I

    iput v1, v0, Landroid/graphics/Rect;->left:I

    iget v1, p1, Landroid/graphics/Rect;->top:I

    iput v1, v0, Landroid/graphics/Rect;->top:I

    iget v1, p1, Landroid/graphics/Rect;->right:I

    iput v1, v0, Landroid/graphics/Rect;->right:I

    iget v1, p1, Landroid/graphics/Rect;->top:I

    int-to-float v1, v1

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result p1

    int-to-float p1, p1

    iget v2, p0, Lcom/miui/packageInstaller/view/i;->d:F

    div-float/2addr p1, v2

    add-float/2addr v1, p1

    float-to-int p1, v1

    iput p1, v0, Landroid/graphics/Rect;->bottom:I

    iget-object p1, p0, Lcom/miui/packageInstaller/view/i;->b:Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/miui/packageInstaller/view/i;->c:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    return-void
.end method

.method public setAlpha(I)V
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/view/i;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/view/i;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    return-void
.end method
