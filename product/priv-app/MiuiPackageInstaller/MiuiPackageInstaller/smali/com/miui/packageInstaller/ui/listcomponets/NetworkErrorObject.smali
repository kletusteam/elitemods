.class public Lcom/miui/packageInstaller/ui/listcomponets/NetworkErrorObject;
.super Lm6/a;

# interfaces
.implements Lcom/miui/packageInstaller/ui/listcomponets/u;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/packageInstaller/ui/listcomponets/NetworkErrorObject$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lm6/a<",
        "Lcom/miui/packageInstaller/ui/listcomponets/NetworkErrorObject$ViewHolder;",
        ">;",
        "Lcom/miui/packageInstaller/ui/listcomponets/u;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Ll6/c;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "actionDelegateFactory"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2, v0}, Lm6/a;-><init>(Landroid/content/Context;Ljava/lang/Object;Ll6/c;Lm6/b;)V

    return-void
.end method

.method private static final B(Lcom/miui/packageInstaller/ui/listcomponets/NetworkErrorObject;Landroid/view/View;)V
    .locals 0

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const p1, 0x7f0a02c5

    invoke-virtual {p0, p1}, Lm6/a;->r(I)V

    return-void
.end method

.method public static synthetic z(Lcom/miui/packageInstaller/ui/listcomponets/NetworkErrorObject;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/miui/packageInstaller/ui/listcomponets/NetworkErrorObject;->B(Lcom/miui/packageInstaller/ui/listcomponets/NetworkErrorObject;Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public A(Lcom/miui/packageInstaller/ui/listcomponets/NetworkErrorObject$ViewHolder;)V
    .locals 1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/NetworkErrorObject$ViewHolder;->getRetryButton()Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_0

    new-instance v0, Lcom/miui/packageInstaller/ui/listcomponets/i;

    invoke-direct {v0, p0}, Lcom/miui/packageInstaller/ui/listcomponets/i;-><init>(Lcom/miui/packageInstaller/ui/listcomponets/NetworkErrorObject;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void
.end method

.method public k()I
    .locals 1

    const v0, 0x7f0d0089

    return v0
.end method

.method public bridge synthetic o(Landroidx/recyclerview/widget/RecyclerView$d0;)V
    .locals 0

    check-cast p1, Lcom/miui/packageInstaller/ui/listcomponets/NetworkErrorObject$ViewHolder;

    invoke-virtual {p0, p1}, Lcom/miui/packageInstaller/ui/listcomponets/NetworkErrorObject;->A(Lcom/miui/packageInstaller/ui/listcomponets/NetworkErrorObject$ViewHolder;)V

    return-void
.end method
