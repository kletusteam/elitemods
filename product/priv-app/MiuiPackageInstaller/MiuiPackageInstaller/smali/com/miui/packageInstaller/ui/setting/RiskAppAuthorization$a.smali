.class public final Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization$a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "a"
.end annotation


# instance fields
.field private final a:Lx5/a;

.field private final b:Landroid/view/View;

.field private final c:Landroid/widget/ImageView;

.field final synthetic d:Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;


# direct methods
.method public constructor <init>(Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;Lx5/a;Landroid/view/View;Landroid/widget/ImageView;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lx5/a;",
            "Landroid/view/View;",
            "Landroid/widget/ImageView;",
            ")V"
        }
    .end annotation

    const-string v0, "type"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "view"

    invoke-static {p3, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "icon"

    invoke-static {p4, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization$a;->d:Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization$a;->a:Lx5/a;

    iput-object p3, p0, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization$a;->b:Landroid/view/View;

    iput-object p4, p0, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization$a;->c:Landroid/widget/ImageView;

    new-instance p2, Le6/d;

    invoke-direct {p2, p1, p0}, Le6/d;-><init>(Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization$a;)V

    invoke-virtual {p3, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 p1, 0x1

    new-array p1, p1, [Landroid/view/View;

    const/4 p2, 0x0

    aput-object p3, p1, p2

    invoke-static {p1}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object p1

    invoke-interface {p1}, Lmiuix/animation/e;->c()Lmiuix/animation/j;

    move-result-object p1

    new-array p4, p2, [Lmiuix/animation/j$b;

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-interface {p1, v0, p4}, Lmiuix/animation/j;->G(F[Lmiuix/animation/j$b;)Lmiuix/animation/j;

    move-result-object p1

    new-array p2, p2, [Lc9/a;

    invoke-interface {p1, p3, p2}, Lmiuix/animation/j;->z(Landroid/view/View;[Lc9/a;)V

    return-void
.end method

.method public static synthetic a(Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization$a;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization$a;->b(Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization$a;Landroid/view/View;)V

    return-void
.end method

.method private static final b(Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization$a;Landroid/view/View;)V
    .locals 0

    const-string p2, "this$0"

    invoke-static {p0, p2}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "this$1"

    invoke-static {p1, p2}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p1, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization$a;->a:Lx5/a;

    invoke-virtual {p0, p1}, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization;->Z0(Lx5/a;)V

    return-void
.end method


# virtual methods
.method public final c()Lx5/a;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization$a;->a:Lx5/a;

    return-object v0
.end method

.method public final d()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization$a;->b:Landroid/view/View;

    return-object v0
.end method

.method public final e(Z)V
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization$a;->b:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setSelected(Z)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/setting/RiskAppAuthorization$a;->c:Landroid/widget/ImageView;

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const/16 p1, 0x8

    :goto_0
    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void
.end method
