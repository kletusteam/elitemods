.class public Lcom/miui/packageInstaller/ui/listcomponets/InstallFailedViewObject;
.super Lm6/a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/packageInstaller/ui/listcomponets/InstallFailedViewObject$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lm6/a<",
        "Lcom/miui/packageInstaller/ui/listcomponets/InstallFailedViewObject$ViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private l:Ls5/h$a;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ls5/h$a;Ll6/c;Lm6/b;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "failReason"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2, p3, p4}, Lm6/a;-><init>(Landroid/content/Context;Ljava/lang/Object;Ll6/c;Lm6/b;)V

    iput-object p2, p0, Lcom/miui/packageInstaller/ui/listcomponets/InstallFailedViewObject;->l:Ls5/h$a;

    return-void
.end method


# virtual methods
.method public k()I
    .locals 1

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0d007b

    goto :goto_0

    :cond_0
    const v0, 0x7f0d007a

    :goto_0
    return v0
.end method

.method public bridge synthetic o(Landroidx/recyclerview/widget/RecyclerView$d0;)V
    .locals 0

    check-cast p1, Lcom/miui/packageInstaller/ui/listcomponets/InstallFailedViewObject$ViewHolder;

    invoke-virtual {p0, p1}, Lcom/miui/packageInstaller/ui/listcomponets/InstallFailedViewObject;->z(Lcom/miui/packageInstaller/ui/listcomponets/InstallFailedViewObject$ViewHolder;)V

    return-void
.end method

.method public z(Lcom/miui/packageInstaller/ui/listcomponets/InstallFailedViewObject$ViewHolder;)V
    .locals 7

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/InstallFailedViewObject$ViewHolder;->getTvFailedReason()Landroid/widget/TextView;

    move-result-object v1

    goto :goto_0

    :cond_0
    move-object v1, v0

    :goto_0
    if-nez v1, :cond_1

    goto :goto_1

    :cond_1
    iget-object v2, p0, Lcom/miui/packageInstaller/ui/listcomponets/InstallFailedViewObject;->l:Ls5/h$a;

    iget-object v2, v2, Ls5/h$a;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/InstallFailedViewObject$ViewHolder;->getTitle()Landroid/widget/TextView;

    move-result-object v1

    goto :goto_2

    :cond_2
    move-object v1, v0

    :goto_2
    const/4 v2, 0x0

    if-nez v1, :cond_3

    goto :goto_3

    :cond_3
    sget-object v3, Lm8/w;->a:Lm8/w;

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f1101d7

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "context.getString(R.string.miui_install_fail)"

    invoke-static {v3, v4}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x1

    new-array v5, v4, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/miui/packageInstaller/ui/listcomponets/InstallFailedViewObject;->l:Ls5/h$a;

    iget v6, v6, Ls5/h$a;->a:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-static {v5, v4}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v4

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "format(format, *args)"

    invoke-static {v3, v4}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_3
    iget-object v1, p0, Lcom/miui/packageInstaller/ui/listcomponets/InstallFailedViewObject;->l:Ls5/h$a;

    iget-object v1, v1, Ls5/h$a;->c:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    if-eqz p1, :cond_4

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/InstallFailedViewObject$ViewHolder;->getTvFailedSuggestionTitle()Landroid/widget/TextView;

    move-result-object v1

    goto :goto_4

    :cond_4
    move-object v1, v0

    :goto_4
    if-nez v1, :cond_5

    goto :goto_5

    :cond_5
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_5
    if-eqz p1, :cond_6

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/InstallFailedViewObject$ViewHolder;->getTvFailedSuggestion()Landroid/widget/TextView;

    move-result-object v1

    goto :goto_6

    :cond_6
    move-object v1, v0

    :goto_6
    if-nez v1, :cond_7

    goto :goto_7

    :cond_7
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_7
    if-eqz p1, :cond_8

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/InstallFailedViewObject$ViewHolder;->getTvFailedSuggestion()Landroid/widget/TextView;

    move-result-object v0

    :cond_8
    if-nez v0, :cond_9

    goto :goto_a

    :cond_9
    iget-object v1, p0, Lcom/miui/packageInstaller/ui/listcomponets/InstallFailedViewObject;->l:Ls5/h$a;

    iget-object v1, v1, Ls5/h$a;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_a

    :cond_a
    if-eqz p1, :cond_b

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/InstallFailedViewObject$ViewHolder;->getTvFailedSuggestionTitle()Landroid/widget/TextView;

    move-result-object v1

    goto :goto_8

    :cond_b
    move-object v1, v0

    :goto_8
    const/16 v3, 0x8

    if-nez v1, :cond_c

    goto :goto_9

    :cond_c
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_9
    if-eqz p1, :cond_d

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/InstallFailedViewObject$ViewHolder;->getTvFailedSuggestion()Landroid/widget/TextView;

    move-result-object v0

    :cond_d
    if-nez v0, :cond_e

    goto :goto_a

    :cond_e
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_a
    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->isSafeModelEnable(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_f

    if-eqz p1, :cond_f

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/InstallFailedViewObject$ViewHolder;->getFlRootLayout()Landroid/widget/FrameLayout;

    move-result-object p1

    if-eqz p1, :cond_f

    invoke-virtual {p1, v2, v2, v2, v2}, Landroid/view/View;->setPadding(IIII)V

    :cond_f
    return-void
.end method
