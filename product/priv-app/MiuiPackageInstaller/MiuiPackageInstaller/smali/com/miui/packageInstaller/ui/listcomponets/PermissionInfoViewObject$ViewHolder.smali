.class public final Lcom/miui/packageInstaller/ui/listcomponets/PermissionInfoViewObject$ViewHolder;
.super Landroidx/recyclerview/widget/RecyclerView$d0;


# instance fields
.field private tvDes:Landroid/widget/TextView;

.field private tvName:Landroid/widget/TextView;

.field private tvTag:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    const-string v0, "itemView"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$d0;-><init>(Landroid/view/View;)V

    const v0, 0x7f0a026e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "itemView.findViewById(R.id.name)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/PermissionInfoViewObject$ViewHolder;->tvName:Landroid/widget/TextView;

    const v0, 0x7f0a0107

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "itemView.findViewById(R.id.des)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/PermissionInfoViewObject$ViewHolder;->tvDes:Landroid/widget/TextView;

    const v0, 0x7f0a0350

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/PermissionInfoViewObject$ViewHolder;->tvTag:Landroid/view/View;

    return-void
.end method


# virtual methods
.method public final getTvDes()Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/PermissionInfoViewObject$ViewHolder;->tvDes:Landroid/widget/TextView;

    return-object v0
.end method

.method public final getTvName()Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/PermissionInfoViewObject$ViewHolder;->tvName:Landroid/widget/TextView;

    return-object v0
.end method

.method public final getTvTag()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/PermissionInfoViewObject$ViewHolder;->tvTag:Landroid/view/View;

    return-object v0
.end method

.method public final setTvDes(Landroid/widget/TextView;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/PermissionInfoViewObject$ViewHolder;->tvDes:Landroid/widget/TextView;

    return-void
.end method

.method public final setTvName(Landroid/widget/TextView;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/PermissionInfoViewObject$ViewHolder;->tvName:Landroid/widget/TextView;

    return-void
.end method

.method public final setTvTag(Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/PermissionInfoViewObject$ViewHolder;->tvTag:Landroid/view/View;

    return-void
.end method
