.class public final Lcom/miui/packageInstaller/model/InstallHistory;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation runtime La5/k;
    value = "install_history"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/packageInstaller/model/InstallHistory$InstallResult;,
        Lcom/miui/packageInstaller/model/InstallHistory$Companion;
    }
.end annotation


# static fields
.field public static final COLUMN_ID:Ljava/lang/String; = "id"

.field public static final COLUMN_INSTALLER_PACKAGE_NAME:Ljava/lang/String; = "installer_package_name"

.field public static final COLUMN_INSTALL_RESULT:Ljava/lang/String; = "install_result"

.field public static final COLUMN_INSTALL_TIME:Ljava/lang/String; = "install_time"

.field public static final COLUMN_LABEL:Ljava/lang/String; = "label"

.field public static final COLUMN_PACKAGE_NAME:Ljava/lang/String; = "package_name"

.field public static final Companion:Lcom/miui/packageInstaller/model/InstallHistory$Companion;

.field public static final INSTALL_RESULT_CANCELED:Ljava/lang/String; = "canceled"

.field public static final INSTALL_RESULT_FAILED:Ljava/lang/String; = "failed"

.field public static final INSTALL_RESULT_NONE:Ljava/lang/String; = ""

.field public static final INSTALL_RESULT_SUCCESS:Ljava/lang/String; = "success"


# instance fields
.field private id:Ljava/lang/String;
    .annotation runtime La5/c;
        value = "id"
    .end annotation

    .annotation runtime La5/j;
        value = .enum Lc5/a;->a:Lc5/a;
    .end annotation
.end field

.field private installResult:Ljava/lang/String;
    .annotation runtime La5/c;
        value = "install_result"
    .end annotation
.end field

.field private installTime:J
    .annotation runtime La5/c;
        value = "install_time"
    .end annotation
.end field

.field private installerPackageName:Ljava/lang/String;
    .annotation runtime La5/c;
        value = "installer_package_name"
    .end annotation
.end field

.field private label:Ljava/lang/String;
    .annotation runtime La5/c;
        value = "label"
    .end annotation
.end field

.field private packageName:Ljava/lang/String;
    .annotation runtime La5/c;
        value = "package_name"
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/miui/packageInstaller/model/InstallHistory$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/miui/packageInstaller/model/InstallHistory$Companion;-><init>(Lm8/g;)V

    sput-object v0, Lcom/miui/packageInstaller/model/InstallHistory;->Companion:Lcom/miui/packageInstaller/model/InstallHistory$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/miui/packageInstaller/model/InstallHistory;->id:Ljava/lang/String;

    iput-object v0, p0, Lcom/miui/packageInstaller/model/InstallHistory;->packageName:Ljava/lang/String;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/miui/packageInstaller/model/InstallHistory;->installTime:J

    iput-object v0, p0, Lcom/miui/packageInstaller/model/InstallHistory;->installResult:Ljava/lang/String;

    iput-object v0, p0, Lcom/miui/packageInstaller/model/InstallHistory;->installerPackageName:Ljava/lang/String;

    iput-object v0, p0, Lcom/miui/packageInstaller/model/InstallHistory;->label:Ljava/lang/String;

    sget-object v1, Lcom/miui/packageInstaller/model/InstallHistory;->Companion:Lcom/miui/packageInstaller/model/InstallHistory$Companion;

    iget-object v2, p0, Lcom/miui/packageInstaller/model/InstallHistory;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/miui/packageInstaller/model/InstallHistory$Companion;->curateID(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/packageInstaller/model/InstallHistory;->id:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    const-string v0, "installerPackageName"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "packageName"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "label"

    invoke-static {p3, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/miui/packageInstaller/model/InstallHistory;->id:Ljava/lang/String;

    iput-object v0, p0, Lcom/miui/packageInstaller/model/InstallHistory;->packageName:Ljava/lang/String;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/miui/packageInstaller/model/InstallHistory;->installTime:J

    iput-object v0, p0, Lcom/miui/packageInstaller/model/InstallHistory;->installResult:Ljava/lang/String;

    iput-object v0, p0, Lcom/miui/packageInstaller/model/InstallHistory;->installerPackageName:Ljava/lang/String;

    iput-object v0, p0, Lcom/miui/packageInstaller/model/InstallHistory;->label:Ljava/lang/String;

    sget-object v1, Lcom/miui/packageInstaller/model/InstallHistory;->Companion:Lcom/miui/packageInstaller/model/InstallHistory$Companion;

    iget-object v2, p0, Lcom/miui/packageInstaller/model/InstallHistory;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/miui/packageInstaller/model/InstallHistory$Companion;->curateID(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/miui/packageInstaller/model/InstallHistory;->id:Ljava/lang/String;

    iput-object p2, p0, Lcom/miui/packageInstaller/model/InstallHistory;->packageName:Ljava/lang/String;

    iput-object p1, p0, Lcom/miui/packageInstaller/model/InstallHistory;->installerPackageName:Ljava/lang/String;

    iput-object p3, p0, Lcom/miui/packageInstaller/model/InstallHistory;->label:Ljava/lang/String;

    return-void
.end method

.method public static synthetic getInstallResult$annotations()V
    .locals 0

    return-void
.end method


# virtual methods
.method public final getId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/model/InstallHistory;->id:Ljava/lang/String;

    return-object v0
.end method

.method public final getInstallResult()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/model/InstallHistory;->installResult:Ljava/lang/String;

    return-object v0
.end method

.method public final getInstallTime()J
    .locals 2

    iget-wide v0, p0, Lcom/miui/packageInstaller/model/InstallHistory;->installTime:J

    return-wide v0
.end method

.method public final getInstallerPackageName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/model/InstallHistory;->installerPackageName:Ljava/lang/String;

    return-object v0
.end method

.method public final getLabel()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/model/InstallHistory;->label:Ljava/lang/String;

    return-object v0
.end method

.method public final getPackageName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/model/InstallHistory;->packageName:Ljava/lang/String;

    return-object v0
.end method

.method public final setId(Ljava/lang/String;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/model/InstallHistory;->id:Ljava/lang/String;

    return-void
.end method

.method public final setInstallResult(Ljava/lang/String;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/model/InstallHistory;->installResult:Ljava/lang/String;

    return-void
.end method

.method public final setInstallTime(J)V
    .locals 0

    iput-wide p1, p0, Lcom/miui/packageInstaller/model/InstallHistory;->installTime:J

    return-void
.end method

.method public final setInstallerPackageName(Ljava/lang/String;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/model/InstallHistory;->installerPackageName:Ljava/lang/String;

    return-void
.end method

.method public final setLabel(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/model/InstallHistory;->label:Ljava/lang/String;

    return-void
.end method

.method public final setPackageName(Ljava/lang/String;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/model/InstallHistory;->packageName:Ljava/lang/String;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "InstallHistory(id=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/miui/packageInstaller/model/InstallHistory;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\', packageName=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/miui/packageInstaller/model/InstallHistory;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\', installTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "yyyy-MM-dd-hh-mm-ss"

    invoke-direct {v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    new-instance v2, Ljava/util/Date;

    iget-wide v3, p0, Lcom/miui/packageInstaller/model/InstallHistory;->installTime:J

    invoke-direct {v2, v3, v4}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", installResult=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/miui/packageInstaller/model/InstallHistory;->installResult:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\', installerPackageName=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/miui/packageInstaller/model/InstallHistory;->installerPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\', label="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/miui/packageInstaller/model/InstallHistory;->label:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
