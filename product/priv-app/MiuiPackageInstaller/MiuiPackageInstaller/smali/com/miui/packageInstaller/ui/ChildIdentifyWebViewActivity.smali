.class public final Lcom/miui/packageInstaller/ui/ChildIdentifyWebViewActivity;
.super Lq2/b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/packageInstaller/ui/ChildIdentifyWebViewActivity$a;,
        Lcom/miui/packageInstaller/ui/ChildIdentifyWebViewActivity$b;
    }
.end annotation


# static fields
.field public static final w:Lcom/miui/packageInstaller/ui/ChildIdentifyWebViewActivity$a;


# instance fields
.field private u:Landroid/webkit/WebView;

.field private v:Lm5/e;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/miui/packageInstaller/ui/ChildIdentifyWebViewActivity$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/miui/packageInstaller/ui/ChildIdentifyWebViewActivity$a;-><init>(Lm8/g;)V

    sput-object v0, Lcom/miui/packageInstaller/ui/ChildIdentifyWebViewActivity;->w:Lcom/miui/packageInstaller/ui/ChildIdentifyWebViewActivity$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lq2/b;-><init>()V

    return-void
.end method

.method public static synthetic J0(Lcom/miui/packageInstaller/ui/ChildIdentifyWebViewActivity;)V
    .locals 0

    invoke-static {p0}, Lcom/miui/packageInstaller/ui/ChildIdentifyWebViewActivity;->T0(Lcom/miui/packageInstaller/ui/ChildIdentifyWebViewActivity;)V

    return-void
.end method

.method public static synthetic K0(Lcom/miui/packageInstaller/ui/ChildIdentifyWebViewActivity;Lcom/miui/packageInstaller/model/PassportIdentityUrlModel;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/miui/packageInstaller/ui/ChildIdentifyWebViewActivity;->U0(Lcom/miui/packageInstaller/ui/ChildIdentifyWebViewActivity;Lcom/miui/packageInstaller/model/PassportIdentityUrlModel;)V

    return-void
.end method

.method public static synthetic L0(Lcom/miui/packageInstaller/ui/ChildIdentifyWebViewActivity;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/miui/packageInstaller/ui/ChildIdentifyWebViewActivity;->P0(Lcom/miui/packageInstaller/ui/ChildIdentifyWebViewActivity;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic M0(Ljava/lang/String;Lcom/miui/packageInstaller/ui/ChildIdentifyWebViewActivity;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/miui/packageInstaller/ui/ChildIdentifyWebViewActivity;->S0(Ljava/lang/String;Lcom/miui/packageInstaller/ui/ChildIdentifyWebViewActivity;)V

    return-void
.end method

.method public static final synthetic N0(Lcom/miui/packageInstaller/ui/ChildIdentifyWebViewActivity;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/miui/packageInstaller/ui/ChildIdentifyWebViewActivity;->R0(Ljava/lang/String;)V

    return-void
.end method

.method private final O0()V
    .locals 0

    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/ChildIdentifyWebViewActivity;->Q0()V

    return-void
.end method

.method private static final P0(Lcom/miui/packageInstaller/ui/ChildIdentifyWebViewActivity;Landroid/view/View;)V
    .locals 0

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/miui/packageInstaller/ui/ChildIdentifyWebViewActivity;->onBackPressed()V

    return-void
.end method

.method private final Q0()V
    .locals 5

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    const/high16 v1, 0x2000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v2, 0x1

    const/16 v3, 0x1d

    if-le v1, v3, :cond_0

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    const-string v3, "content"

    invoke-static {v3, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :try_start_0
    invoke-virtual {p0}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {p0, v1, v3, v2}, Landroid/app/Activity;->grantUriPermission(Ljava/lang/String;Landroid/net/Uri;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/miui/packageInstaller/ui/ChildIdentifyWebViewActivity;->v:Lm5/e;

    const-string v2, "EXTRA_CALLING_PACKAGE"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-static {p0}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->isSafeModelEnable(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-class v1, Lcom/miui/packageInstaller/PurePackageInstallerActivity;

    goto :goto_1

    :cond_2
    const-class v1, Lcom/miui/packageInstaller/NewPackageInstallerActivity;

    :goto_1
    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    :try_start_1
    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    :catch_1
    move-exception v0

    const-string v1, "ChildIdentifyVerifyActivity"

    const-string v2, "start next Activity error : "

    invoke-static {v1, v2, v0}, Lf6/o;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    const-string v2, "getDefault()"

    invoke-static {v1, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "this as java.lang.String).toLowerCase(locale)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x2

    const/4 v2, 0x0

    const-string v3, "not have permission"

    const/4 v4, 0x0

    invoke-static {v0, v3, v4, v1, v2}, Lu8/g;->A(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZILjava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const v0, 0x7f1103dd

    invoke-static {p0, v0, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_3
    :goto_2
    invoke-virtual {p0}, Lmiuix/appcompat/app/j;->finish()V

    return-void
.end method

.method private final R0(Ljava/lang/String;)V
    .locals 2

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object v0

    new-instance v1, Lz5/i;

    invoke-direct {v1, p1, p0}, Lz5/i;-><init>(Ljava/lang/String;Lcom/miui/packageInstaller/ui/ChildIdentifyWebViewActivity;)V

    invoke-virtual {v0, v1}, Lf6/z;->g(Ljava/lang/Runnable;)V

    return-void
.end method

.method private static final S0(Ljava/lang/String;Lcom/miui/packageInstaller/ui/ChildIdentifyWebViewActivity;)V
    .locals 3

    const-string v0, "ChildIdentifyVerifyActivity"

    const-string v1, "$url"

    invoke-static {p0, v1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "this$0"

    invoke-static {p1, v1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "verifyUrl=  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lf6/o;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    const/4 v1, 0x1

    invoke-static {p0, v1}, Lcom/android/packageinstaller/utils/n;->f(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object p0

    invoke-static {}, Lcom/android/packageinstaller/utils/j;->b()Lp4/e;

    move-result-object v1

    const-class v2, Lcom/miui/packageInstaller/model/PassportIdentityUrlModel;

    invoke-virtual {v1, p0, v2}, Lp4/e;->h(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/miui/packageInstaller/model/PassportIdentityUrlModel;

    invoke-virtual {p0}, Lcom/miui/packageInstaller/model/PassportIdentityUrlModel;->getCode()I

    move-result v1

    const/16 v2, 0xc8

    if-ne v1, v2, :cond_0

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object p0

    new-instance v1, Lz5/g;

    invoke-direct {v1, p1}, Lz5/g;-><init>(Lcom/miui/packageInstaller/ui/ChildIdentifyWebViewActivity;)V

    invoke-virtual {p0, v1}, Lf6/z;->e(Ljava/lang/Runnable;)V

    goto :goto_0

    :cond_0
    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object v1

    new-instance v2, Lz5/h;

    invoke-direct {v2, p1, p0}, Lz5/h;-><init>(Lcom/miui/packageInstaller/ui/ChildIdentifyWebViewActivity;Lcom/miui/packageInstaller/model/PassportIdentityUrlModel;)V

    invoke-virtual {v1, v2}, Lf6/z;->e(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    invoke-virtual {p0}, Ljava/lang/Exception;->printStackTrace()V

    invoke-virtual {p0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, p0}, Lf6/o;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    :goto_0
    return-void
.end method

.method private static final T0(Lcom/miui/packageInstaller/ui/ChildIdentifyWebViewActivity;)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/ChildIdentifyWebViewActivity;->O0()V

    return-void
.end method

.method private static final U0(Lcom/miui/packageInstaller/ui/ChildIdentifyWebViewActivity;Lcom/miui/packageInstaller/model/PassportIdentityUrlModel;)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/model/PassportIdentityUrlModel;->getMsg()Ljava/lang/String;

    move-result-object p1

    if-nez p1, :cond_0

    const-string p1, ""

    :cond_0
    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object p0

    invoke-virtual {p0}, Landroid/widget/Toast;->show()V

    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 3

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/ChildIdentifyWebViewActivity;->u:Landroid/webkit/WebView;

    const/4 v1, 0x0

    const-string v2, "webView"

    if-nez v0, :cond_0

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_0
    invoke-virtual {v0}, Landroid/webkit/WebView;->canGoBack()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/ChildIdentifyWebViewActivity;->u:Landroid/webkit/WebView;

    if-nez v0, :cond_1

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move-object v1, v0

    :goto_0
    invoke-virtual {v1}, Landroid/webkit/WebView;->goBack()V

    goto :goto_1

    :cond_2
    invoke-super {p0}, Lmiuix/appcompat/app/j;->onBackPressed()V

    :goto_1
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetJavaScriptEnabled"
        }
    .end annotation

    invoke-super {p0, p1}, Lq2/b;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "jump_url"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-nez p1, :cond_0

    const-string p1, ""

    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "hasTitle"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v3, "EXTRA_CALLING_PACKAGE"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lm5/e;

    iput-object v1, p0, Lcom/miui/packageInstaller/ui/ChildIdentifyWebViewActivity;->v:Lm5/e;

    const v1, 0x7f0d01d2

    invoke-virtual {p0, v1}, Lmiuix/appcompat/app/j;->setContentView(I)V

    const v1, 0x7f0a02d1

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-static {p0}, Lcom/android/packageinstaller/utils/u;->b(Landroid/app/Activity;)I

    move-result v3

    invoke-virtual {v1, v2, v3, v2, v2}, Landroid/view/View;->setPadding(IIII)V

    :cond_1
    const v1, 0x7f0a03d3

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    if-eqz v0, :cond_2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_2
    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    const v0, 0x7f0a03f3

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById<WebView>(R.id.wb_view)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/ChildIdentifyWebViewActivity;->u:Landroid/webkit/WebView;

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x1d

    const/4 v4, 0x1

    const/4 v5, 0x0

    const-string v6, "webView"

    if-lt v1, v3, :cond_4

    if-nez v0, :cond_3

    invoke-static {v6}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v5

    :cond_3
    invoke-virtual {v0, v4}, Landroid/webkit/WebView;->setForceDarkAllowed(Z)V

    :cond_4
    iget-object v0, p0, Lcom/miui/packageInstaller/ui/ChildIdentifyWebViewActivity;->u:Landroid/webkit/WebView;

    if-nez v0, :cond_5

    invoke-static {v6}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v5

    :cond_5
    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    const-string v1, "webView.settings"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    const-string v1, "utf-8"

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setDefaultTextEncodingName(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setAllowContentAccess(Z)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/ChildIdentifyWebViewActivity;->u:Landroid/webkit/WebView;

    if-nez v0, :cond_6

    invoke-static {v6}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v5

    :cond_6
    new-instance v1, Lcom/miui/packageInstaller/ui/ChildIdentifyWebViewActivity$b;

    invoke-direct {v1, p0}, Lcom/miui/packageInstaller/ui/ChildIdentifyWebViewActivity$b;-><init>(Lcom/miui/packageInstaller/ui/ChildIdentifyWebViewActivity;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/ChildIdentifyWebViewActivity;->u:Landroid/webkit/WebView;

    if-nez v0, :cond_7

    invoke-static {v6}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v5

    :cond_7
    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setBackgroundColor(I)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/ChildIdentifyWebViewActivity;->u:Landroid/webkit/WebView;

    if-nez v0, :cond_8

    invoke-static {v6}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v5

    :cond_8
    invoke-virtual {v0}, Landroid/webkit/WebView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_9

    goto :goto_1

    :cond_9
    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    :goto_1
    iget-object v0, p0, Lcom/miui/packageInstaller/ui/ChildIdentifyWebViewActivity;->u:Landroid/webkit/WebView;

    if-nez v0, :cond_a

    invoke-static {v6}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_2

    :cond_a
    move-object v5, v0

    :goto_2
    invoke-virtual {v5, p1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "load url =  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "ChildIdentifyVerifyActivity"

    invoke-static {v0, p1}, Lf6/o;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    const p1, 0x7f0a01cb

    invoke-virtual {p0, p1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    new-instance v0, Lz5/f;

    invoke-direct {v0, p0}, Lz5/f;-><init>(Lcom/miui/packageInstaller/ui/ChildIdentifyWebViewActivity;)V

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method protected onDestroy()V
    .locals 4

    :try_start_0
    iget-object v0, p0, Lcom/miui/packageInstaller/ui/ChildIdentifyWebViewActivity;->u:Landroid/webkit/WebView;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v1, 0x0

    const-string v2, "webView"

    if-nez v0, :cond_0

    :try_start_1
    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_0
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/webkit/WebView;->clearCache(Z)V

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/ChildIdentifyWebViewActivity;->u:Landroid/webkit/WebView;

    if-nez v0, :cond_1

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move-object v1, v0

    :goto_0
    invoke-virtual {v1}, Landroid/webkit/WebView;->destroy()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :goto_1
    invoke-super {p0}, Landroidx/fragment/app/e;->onDestroy()V

    return-void
.end method
