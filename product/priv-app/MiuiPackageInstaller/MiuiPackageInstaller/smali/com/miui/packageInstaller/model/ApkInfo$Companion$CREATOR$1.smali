.class public final Lcom/miui/packageInstaller/model/ApkInfo$Companion$CREATOR$1;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/packageInstaller/model/ApkInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator<",
        "Lcom/miui/packageInstaller/model/ApkInfo;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/miui/packageInstaller/model/ApkInfo;
    .locals 1

    const-string v0, "in"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/miui/packageInstaller/model/ApkInfo;

    invoke-direct {v0, p1}, Lcom/miui/packageInstaller/model/ApkInfo;-><init>(Landroid/os/Parcel;)V

    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 0

    invoke-virtual {p0, p1}, Lcom/miui/packageInstaller/model/ApkInfo$Companion$CREATOR$1;->createFromParcel(Landroid/os/Parcel;)Lcom/miui/packageInstaller/model/ApkInfo;

    move-result-object p1

    return-object p1
.end method

.method public newArray(I)[Lcom/miui/packageInstaller/model/ApkInfo;
    .locals 0

    new-array p1, p1, [Lcom/miui/packageInstaller/model/ApkInfo;

    return-object p1
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 0

    invoke-virtual {p0, p1}, Lcom/miui/packageInstaller/model/ApkInfo$Companion$CREATOR$1;->newArray(I)[Lcom/miui/packageInstaller/model/ApkInfo;

    move-result-object p1

    return-object p1
.end method
