.class public final Lcom/miui/packageInstaller/ui/secure/SecurityModeFeedBackActivity;
.super Lq2/b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/packageInstaller/ui/secure/SecurityModeFeedBackActivity$a;
    }
.end annotation


# static fields
.field public static final v:Lcom/miui/packageInstaller/ui/secure/SecurityModeFeedBackActivity$a;


# instance fields
.field private u:Landroidx/fragment/app/m;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/miui/packageInstaller/ui/secure/SecurityModeFeedBackActivity$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/miui/packageInstaller/ui/secure/SecurityModeFeedBackActivity$a;-><init>(Lm8/g;)V

    sput-object v0, Lcom/miui/packageInstaller/ui/secure/SecurityModeFeedBackActivity;->v:Lcom/miui/packageInstaller/ui/secure/SecurityModeFeedBackActivity$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lq2/b;-><init>()V

    return-void
.end method

.method private final J0()V
    .locals 4

    invoke-virtual {p0}, Landroidx/fragment/app/e;->X()Landroidx/fragment/app/m;

    move-result-object v0

    const-string v1, "supportFragmentManager"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/secure/SecurityModeFeedBackActivity;->u:Landroidx/fragment/app/m;

    const/4 v1, 0x0

    const-string v2, "fragmentManager"

    if-nez v0, :cond_0

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_0
    const-string v3, "tag_fragment"

    invoke-virtual {v0, v3}, Landroidx/fragment/app/m;->h0(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/secure/SecurityModeFeedBackActivity;->u:Landroidx/fragment/app/m;

    if-nez v0, :cond_1

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move-object v1, v0

    :goto_0
    invoke-virtual {v1}, Landroidx/fragment/app/m;->l()Landroidx/fragment/app/v;

    move-result-object v0

    const-string v1, "fragmentManager.beginTransaction()"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v1, Lcom/miui/packageInstaller/view/h;

    invoke-direct {v1}, Lcom/miui/packageInstaller/view/h;-><init>()V

    const v2, 0x7f0a0179

    invoke-virtual {v0, v2, v1, v3}, Landroidx/fragment/app/v;->b(ILandroidx/fragment/app/Fragment;Ljava/lang/String;)Landroidx/fragment/app/v;

    invoke-virtual {v0}, Landroidx/fragment/app/v;->h()I

    :cond_2
    return-void
.end method


# virtual methods
.method public E0()Ljava/lang/String;
    .locals 1

    const-string v0, "safe_mode_shut_research"

    return-object v0
.end method

.method public onBackPressed()V
    .locals 3

    invoke-super {p0}, Lmiuix/appcompat/app/j;->onBackPressed()V

    new-instance v0, Lp5/b;

    const-string v1, "page_back_btn"

    const-string v2, "button"

    invoke-direct {v0, v1, v2, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    const-string v1, "back_type"

    const-string v2, "system"

    invoke-virtual {v0, v1, v2}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object v0

    invoke-virtual {v0}, Lp5/f;->c()Z

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Lq2/b;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0d002a

    invoke-virtual {p0, p1}, Lmiuix/appcompat/app/j;->setContentView(I)V

    invoke-direct {p0}, Lcom/miui/packageInstaller/ui/secure/SecurityModeFeedBackActivity;->J0()V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    const-string v0, "item"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    new-instance v0, Lp5/b;

    const-string v1, "page_back_btn"

    const-string v2, "button"

    invoke-direct {v0, v1, v2, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    const-string v1, "back_type"

    const-string v2, "click_icon"

    invoke-virtual {v0, v1, v2}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object v0

    invoke-virtual {v0}, Lp5/f;->c()Z

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result p1

    return p1
.end method

.method protected onStop()V
    .locals 0

    invoke-super {p0}, Lq2/b;->onStop()V

    return-void
.end method
