.class public final Lcom/miui/packageInstaller/secure/view/FacePassword;
.super Landroid/widget/LinearLayout;

# interfaces
.implements Lx5/c;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/packageInstaller/secure/view/FacePassword$a;
    }
.end annotation


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Lcom/airbnb/lottie/LottieAnimationView;

.field private c:I

.field private d:Lg6/c;

.field private final e:I

.field private final f:I

.field private final g:I

.field private final h:I

.field private i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private k:Lx5/h;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/miui/packageInstaller/secure/view/FacePassword;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 p1, 0x5

    iput p1, p0, Lcom/miui/packageInstaller/secure/view/FacePassword;->c:I

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lg6/c;->m(Landroid/content/Context;)Lg6/c;

    move-result-object p1

    const-string p2, "getInstance(context)"

    invoke-static {p1, p2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/secure/view/FacePassword;->d:Lg6/c;

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p2, 0x7f07012c

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result p1

    iput p1, p0, Lcom/miui/packageInstaller/secure/view/FacePassword;->e:I

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p2, 0x7f070106

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result p1

    iput p1, p0, Lcom/miui/packageInstaller/secure/view/FacePassword;->f:I

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p2, 0x7f0700a8

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result p1

    iput p1, p0, Lcom/miui/packageInstaller/secure/view/FacePassword;->g:I

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p2, 0x7f070137

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result p1

    iput p1, p0, Lcom/miui/packageInstaller/secure/view/FacePassword;->h:I

    invoke-static {}, Lb8/j;->f()Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/miui/packageInstaller/secure/view/FacePassword;->i:Ljava/util/List;

    invoke-static {}, Lb8/j;->f()Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/miui/packageInstaller/secure/view/FacePassword;->j:Ljava/util/List;

    return-void
.end method

.method public static synthetic b(Lcom/miui/packageInstaller/secure/view/FacePassword;Ll8/l;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/miui/packageInstaller/secure/view/FacePassword;->k(Lcom/miui/packageInstaller/secure/view/FacePassword;Ll8/l;)V

    return-void
.end method

.method public static synthetic c(Lcom/miui/packageInstaller/secure/view/FacePassword;)V
    .locals 0

    invoke-static {p0}, Lcom/miui/packageInstaller/secure/view/FacePassword;->i(Lcom/miui/packageInstaller/secure/view/FacePassword;)V

    return-void
.end method

.method public static synthetic e(Lcom/miui/packageInstaller/secure/view/FacePassword;Ll8/l;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/miui/packageInstaller/secure/view/FacePassword;->l(Lcom/miui/packageInstaller/secure/view/FacePassword;Ll8/l;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic f(Lcom/miui/packageInstaller/secure/view/FacePassword;Ll8/l;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/miui/packageInstaller/secure/view/FacePassword;->m(Lcom/miui/packageInstaller/secure/view/FacePassword;Ll8/l;)V

    return-void
.end method

.method public static synthetic g(Lcom/miui/packageInstaller/secure/view/FacePassword;)V
    .locals 0

    invoke-static {p0}, Lcom/miui/packageInstaller/secure/view/FacePassword;->r(Lcom/miui/packageInstaller/secure/view/FacePassword;)V

    return-void
.end method

.method public static final synthetic h(Lcom/miui/packageInstaller/secure/view/FacePassword;)Landroid/widget/TextView;
    .locals 0

    iget-object p0, p0, Lcom/miui/packageInstaller/secure/view/FacePassword;->a:Landroid/widget/TextView;

    return-object p0
.end method

.method private static final i(Lcom/miui/packageInstaller/secure/view/FacePassword;)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p0, p0, Lcom/miui/packageInstaller/secure/view/FacePassword;->d:Lg6/c;

    invoke-virtual {p0}, Lg6/c;->x()V

    return-void
.end method

.method private final j(Ll8/l;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ll8/l<",
            "-",
            "Ljava/lang/Integer;",
            "La8/v;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/miui/packageInstaller/secure/view/FacePassword;->d:Lg6/c;

    new-instance v1, Lcom/miui/packageInstaller/secure/view/FacePassword$b;

    invoke-direct {v1, p0, p1}, Lcom/miui/packageInstaller/secure/view/FacePassword$b;-><init>(Lcom/miui/packageInstaller/secure/view/FacePassword;Ll8/l;)V

    invoke-virtual {v0, v1}, Lg6/c;->w(Lg6/b;)V

    return-void
.end method

.method private static final k(Lcom/miui/packageInstaller/secure/view/FacePassword;Ll8/l;)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$callBack"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/miui/packageInstaller/secure/view/FacePassword;->j(Ll8/l;)V

    return-void
.end method

.method private static final l(Lcom/miui/packageInstaller/secure/view/FacePassword;Ll8/l;Landroid/view/View;)V
    .locals 1

    const-string p2, "this$0"

    invoke-static {p0, p2}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "$callBack"

    invoke-static {p1, p2}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p2, p0, Lcom/miui/packageInstaller/secure/view/FacePassword;->d:Lg6/c;

    new-instance v0, Ly5/e;

    invoke-direct {v0, p0, p1}, Ly5/e;-><init>(Lcom/miui/packageInstaller/secure/view/FacePassword;Ll8/l;)V

    invoke-virtual {p2, v0}, Lg6/c;->v(Ljava/lang/Runnable;)V

    iget-object p1, p0, Lcom/miui/packageInstaller/secure/view/FacePassword;->a:Landroid/widget/TextView;

    if-nez p1, :cond_0

    const-string p1, "tipsTextView"

    invoke-static {p1}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 p1, 0x0

    :cond_0
    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object p0

    const p2, 0x7f11010d

    invoke-virtual {p0, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, p0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private static final m(Lcom/miui/packageInstaller/secure/view/FacePassword;Ll8/l;)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$callBack"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/miui/packageInstaller/secure/view/FacePassword;->j(Ll8/l;)V

    return-void
.end method

.method public static synthetic o(Lcom/miui/packageInstaller/secure/view/FacePassword;Ll8/a;ILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    const/4 p1, 0x0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/miui/packageInstaller/secure/view/FacePassword;->n(Ll8/a;)V

    return-void
.end method

.method private static final r(Lcom/miui/packageInstaller/secure/view/FacePassword;)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p0, p0, Lcom/miui/packageInstaller/secure/view/FacePassword;->d:Lg6/c;

    invoke-virtual {p0}, Lg6/c;->x()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    iget-object v0, p0, Lcom/miui/packageInstaller/secure/view/FacePassword;->d:Lg6/c;

    new-instance v1, Ly5/b;

    invoke-direct {v1, p0}, Ly5/b;-><init>(Lcom/miui/packageInstaller/secure/view/FacePassword;)V

    invoke-virtual {v0, v1}, Lg6/c;->v(Ljava/lang/Runnable;)V

    return-void
.end method

.method public d(Lx5/h;Ll8/l;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lx5/h;",
            "Ll8/l<",
            "-",
            "Ljava/lang/Integer;",
            "La8/v;",
            ">;)V"
        }
    .end annotation

    const-string v0, "dialog"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "callBack"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/secure/view/FacePassword;->k:Lx5/h;

    iget-object p1, p0, Lcom/miui/packageInstaller/secure/view/FacePassword;->d:Lg6/c;

    new-instance v0, Ly5/d;

    invoke-direct {v0, p0, p2}, Ly5/d;-><init>(Lcom/miui/packageInstaller/secure/view/FacePassword;Ll8/l;)V

    invoke-virtual {p1, v0}, Lg6/c;->v(Ljava/lang/Runnable;)V

    iget-object p1, p0, Lcom/miui/packageInstaller/secure/view/FacePassword;->b:Lcom/airbnb/lottie/LottieAnimationView;

    if-nez p1, :cond_0

    const-string p1, "faceIconView"

    invoke-static {p1}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 p1, 0x0

    :cond_0
    new-instance v0, Ly5/a;

    invoke-direct {v0, p0, p2}, Ly5/a;-><init>(Lcom/miui/packageInstaller/secure/view/FacePassword;Ll8/l;)V

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final getDialog()Lx5/h;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/secure/view/FacePassword;->k:Lx5/h;

    return-object v0
.end method

.method public final n(Ll8/a;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ll8/a<",
            "La8/v;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/miui/packageInstaller/secure/view/FacePassword;->a:Landroid/widget/TextView;

    const/4 v1, 0x0

    const-string v2, "tipsTextView"

    if-nez v0, :cond_0

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_0
    invoke-virtual {v0}, Landroid/widget/TextView;->getTranslationX()F

    iget-object v0, p0, Lcom/miui/packageInstaller/secure/view/FacePassword;->a:Landroid/widget/TextView;

    if-nez v0, :cond_1

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move-object v1, v0

    :goto_0
    const/4 v0, 0x2

    new-array v0, v0, [F

    const/4 v2, 0x0

    const/4 v3, 0x0

    aput v3, v0, v2

    const/4 v2, 0x1

    const/high16 v3, 0x41600000    # 14.0f

    invoke-static {v3}, Lf6/d;->a(F)I

    move-result v3

    int-to-float v3, v3

    aput v3, v0, v2

    const-string v2, "translationX"

    invoke-static {v1, v2, v0}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    new-instance v1, Lcom/miui/packageInstaller/secure/view/FacePassword$a;

    const/high16 v2, 0x40200000    # 2.5f

    invoke-direct {v1, v2}, Lcom/miui/packageInstaller/secure/view/FacePassword$a;-><init>(F)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    const-wide/16 v1, 0x3d4

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    new-instance v1, Lcom/miui/packageInstaller/secure/view/FacePassword$c;

    invoke-direct {v1, p1}, Lcom/miui/packageInstaller/secure/view/FacePassword$c;-><init>(Ll8/a;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    const v0, 0x7f0a015c

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.face_unlock_tips)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/miui/packageInstaller/secure/view/FacePassword;->a:Landroid/widget/TextView;

    const v0, 0x7f0a015a

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.face_icon)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/airbnb/lottie/LottieAnimationView;

    iput-object v0, p0, Lcom/miui/packageInstaller/secure/view/FacePassword;->b:Lcom/airbnb/lottie/LottieAnimationView;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const-string v0, "faceIconView"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_0
    const v2, 0x7f080150

    invoke-virtual {v0, v2}, Lcom/airbnb/lottie/LottieAnimationView;->setImageResource(I)V

    iget-object v0, p0, Lcom/miui/packageInstaller/secure/view/FacePassword;->a:Landroid/widget/TextView;

    if-nez v0, :cond_1

    const-string v0, "tipsTextView"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move-object v1, v0

    :goto_0
    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f11010d

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DrawAllocation"
        }
    .end annotation

    invoke-super/range {p0 .. p5}, Landroid/widget/LinearLayout;->onLayout(ZIIII)V

    sget-boolean p1, Lcom/android/packageinstaller/utils/g;->h:Z

    if-eqz p1, :cond_4

    const/4 p1, 0x2

    new-array p1, p1, [I

    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->getLocationOnScreen([I)V

    iget-object p2, p0, Lcom/miui/packageInstaller/secure/view/FacePassword;->i:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    const/4 p3, 0x1

    if-le p2, p3, :cond_4

    iget-object p2, p0, Lcom/miui/packageInstaller/secure/view/FacePassword;->j:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    if-le p2, p3, :cond_4

    iget-object p2, p0, Lcom/miui/packageInstaller/secure/view/FacePassword;->i:Ljava/util/List;

    invoke-interface {p2, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p2

    aget p1, p1, p3

    sub-int/2addr p2, p1

    iget-object p1, p0, Lcom/miui/packageInstaller/secure/view/FacePassword;->j:Ljava/util/List;

    invoke-interface {p1, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p1

    add-int/2addr p2, p1

    iget p1, p0, Lcom/miui/packageInstaller/secure/view/FacePassword;->g:I

    add-int/2addr p2, p1

    iget-object p1, p0, Lcom/miui/packageInstaller/secure/view/FacePassword;->a:Landroid/widget/TextView;

    const/4 p3, 0x0

    const-string p4, "tipsTextView"

    if-nez p1, :cond_0

    invoke-static {p4}, Lm8/i;->s(Ljava/lang/String;)V

    move-object p1, p3

    :cond_0
    iget-object p5, p0, Lcom/miui/packageInstaller/secure/view/FacePassword;->a:Landroid/widget/TextView;

    if-nez p5, :cond_1

    invoke-static {p4}, Lm8/i;->s(Ljava/lang/String;)V

    move-object p5, p3

    :cond_1
    invoke-virtual {p5}, Landroid/widget/TextView;->getLeft()I

    move-result p5

    iget-object v0, p0, Lcom/miui/packageInstaller/secure/view/FacePassword;->a:Landroid/widget/TextView;

    if-nez v0, :cond_2

    invoke-static {p4}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, p3

    :cond_2
    invoke-virtual {v0}, Landroid/widget/TextView;->getRight()I

    move-result v0

    iget-object v1, p0, Lcom/miui/packageInstaller/secure/view/FacePassword;->a:Landroid/widget/TextView;

    if-nez v1, :cond_3

    invoke-static {p4}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    move-object p3, v1

    :goto_0
    invoke-virtual {p3}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result p3

    add-int/2addr p3, p2

    invoke-virtual {p1, p5, p2, v0, p3}, Landroid/widget/TextView;->layout(IIII)V

    :cond_4
    return-void
.end method

.method protected onMeasure(II)V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DrawAllocation"
        }
    .end annotation

    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    sget-boolean p2, Lcom/android/packageinstaller/utils/g;->h:Z

    if-eqz p2, :cond_0

    iget-object p2, p0, Lcom/miui/packageInstaller/secure/view/FacePassword;->i:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    const/4 v0, 0x1

    if-le p2, v0, :cond_0

    iget-object p2, p0, Lcom/miui/packageInstaller/secure/view/FacePassword;->j:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    if-le p2, v0, :cond_0

    invoke-static {}, Lf6/d;->d()I

    move-result p2

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lf6/d;->h(Landroid/content/Context;)I

    move-result v1

    add-int/2addr p2, v1

    iget-object v1, p0, Lcom/miui/packageInstaller/secure/view/FacePassword;->i:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    neg-int v0, v0

    add-int/2addr p2, v0

    iget v0, p0, Lcom/miui/packageInstaller/secure/view/FacePassword;->e:I

    add-int/2addr p2, v0

    iget v0, p0, Lcom/miui/packageInstaller/secure/view/FacePassword;->f:I

    sub-int/2addr p2, v0

    iget v0, p0, Lcom/miui/packageInstaller/secure/view/FacePassword;->h:I

    sub-int/2addr p2, v0

    invoke-virtual {p0, p1, p2}, Landroid/widget/LinearLayout;->setMeasuredDimension(II)V

    :cond_0
    return-void
.end method

.method public final p()V
    .locals 2

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f010034

    invoke-static {v0, v1}, Landroid/animation/AnimatorInflater;->loadAnimator(Landroid/content/Context;I)Landroid/animation/Animator;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/packageInstaller/secure/view/FacePassword;->b:Lcom/airbnb/lottie/LottieAnimationView;

    if-nez v1, :cond_0

    const-string v1, "faceIconView"

    invoke-static {v1}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v1, 0x0

    :cond_0
    invoke-virtual {v0, v1}, Landroid/animation/Animator;->setTarget(Ljava/lang/Object;)V

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    return-void
.end method

.method public final q(Ll8/a;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ll8/a<",
            "La8/v;",
            ">;)V"
        }
    .end annotation

    const-string v0, "onAnimDone"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/secure/view/FacePassword;->a:Landroid/widget/TextView;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const-string v0, "tipsTextView"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_0
    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f110118

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/secure/view/FacePassword;->b:Lcom/airbnb/lottie/LottieAnimationView;

    const-string v2, "faceIconView"

    if-nez v0, :cond_1

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_1
    const-string v3, "finger_authorization_success.json"

    invoke-virtual {v0, v3}, Lcom/airbnb/lottie/LottieAnimationView;->setAnimation(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/secure/view/FacePassword;->b:Lcom/airbnb/lottie/LottieAnimationView;

    if-nez v0, :cond_2

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_2
    invoke-virtual {v0}, Lcom/airbnb/lottie/LottieAnimationView;->p()V

    iget-object v0, p0, Lcom/miui/packageInstaller/secure/view/FacePassword;->b:Lcom/airbnb/lottie/LottieAnimationView;

    if-nez v0, :cond_3

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v1

    :cond_3
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/miui/packageInstaller/secure/view/FacePassword;->b:Lcom/airbnb/lottie/LottieAnimationView;

    if-nez v0, :cond_4

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    move-object v1, v0

    :goto_0
    new-instance v0, Lcom/miui/packageInstaller/secure/view/FacePassword$d;

    invoke-direct {v0, p1}, Lcom/miui/packageInstaller/secure/view/FacePassword$d;-><init>(Ll8/a;)V

    invoke-virtual {v1, v0}, Lcom/airbnb/lottie/LottieAnimationView;->f(Landroid/animation/Animator$AnimatorListener;)V

    return-void
.end method

.method public release()V
    .locals 2

    invoke-static {p0}, Lx5/c$a;->a(Lx5/c;)V

    iget-object v0, p0, Lcom/miui/packageInstaller/secure/view/FacePassword;->d:Lg6/c;

    new-instance v1, Ly5/c;

    invoke-direct {v1, p0}, Ly5/c;-><init>(Lcom/miui/packageInstaller/secure/view/FacePassword;)V

    invoke-virtual {v0, v1}, Lg6/c;->v(Ljava/lang/Runnable;)V

    return-void
.end method

.method public setCancelButtonText(Ljava/lang/CharSequence;)V
    .locals 0

    invoke-static {p0, p1}, Lx5/c$a;->b(Lx5/c;Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setConfirmButtonText(Ljava/lang/CharSequence;)V
    .locals 0

    invoke-static {p0, p1}, Lx5/c$a;->c(Lx5/c;Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final setDialog(Lx5/h;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/secure/view/FacePassword;->k:Lx5/h;

    return-void
.end method

.method public setTipMsgText(Ljava/lang/CharSequence;)V
    .locals 0

    invoke-static {p0, p1}, Lx5/c$a;->d(Lx5/c;Ljava/lang/CharSequence;)V

    return-void
.end method
