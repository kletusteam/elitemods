.class public Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject;
.super Lm6/a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lm6/a<",
        "Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private final l:Lcom/miui/packageInstaller/model/ApkInfo;

.field private final m:Lcom/miui/packageInstaller/model/CloudParams;

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;

.field private final p:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/ApkInfo;Lcom/miui/packageInstaller/model/CloudParams;Ll6/c;Lm6/b;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "apkInfo"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mData"

    invoke-static {p3, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p3, p4, p5}, Lm6/a;-><init>(Landroid/content/Context;Ljava/lang/Object;Ll6/c;Lm6/b;)V

    iput-object p2, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject;->l:Lcom/miui/packageInstaller/model/ApkInfo;

    iput-object p3, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject;->m:Lcom/miui/packageInstaller/model/CloudParams;

    const-string p2, ""

    iput-object p2, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject;->n:Ljava/lang/String;

    iput-object p2, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject;->o:Ljava/lang/String;

    new-instance p2, Lcom/miui/packageInstaller/ui/listcomponets/d;

    invoke-direct {p2, p1, p0}, Lcom/miui/packageInstaller/ui/listcomponets/d;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject;)V

    iput-object p2, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject;->p:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/ApkInfo;Lcom/miui/packageInstaller/model/CloudParams;Ll6/c;Lm6/b;ILm8/g;)V
    .locals 7

    and-int/lit8 p7, p6, 0x8

    const/4 v0, 0x0

    if-eqz p7, :cond_0

    move-object v5, v0

    goto :goto_0

    :cond_0
    move-object v5, p4

    :goto_0
    and-int/lit8 p4, p6, 0x10

    if-eqz p4, :cond_1

    move-object v6, v0

    goto :goto_1

    :cond_1
    move-object v6, p5

    :goto_1
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v1 .. v6}, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/ApkInfo;Lcom/miui/packageInstaller/model/CloudParams;Ll6/c;Lm6/b;)V

    return-void
.end method

.method private final B(Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;)V
    .locals 8

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject;->m:Lcom/miui/packageInstaller/model/CloudParams;

    iget-object v0, v0, Lcom/miui/packageInstaller/model/CloudParams;->appInfo:Lcom/miui/packageInstaller/model/MarketAppInfo;

    const/4 v1, 0x0

    if-eqz v0, :cond_8

    iget-object v2, v0, Lcom/miui/packageInstaller/model/MarketAppInfo;->headerCardInfos:Ljava/util/List;

    if-eqz v2, :cond_8

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    move-object v2, v1

    :goto_0
    invoke-static {v2}, Lm8/i;->c(Ljava/lang/Object;)V

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/miui/packageInstaller/model/MarketAppInfo$DetailHeaderCardInfo;

    iget-object v3, v2, Lcom/miui/packageInstaller/model/MarketAppInfo$DetailHeaderCardInfo;->type:Ljava/lang/String;

    if-eqz v3, :cond_1

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v4

    const v5, 0x354c2c

    if-eq v4, v5, :cond_6

    const v5, 0x59ad67

    const-string v6, "context"

    if-eq v4, v5, :cond_4

    const v5, 0x38a5ee5f

    if-eq v4, v5, :cond_2

    goto :goto_1

    :cond_2
    const-string v4, "comment"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->getAppStar()Landroid/widget/TextView;

    move-result-object v3

    iget-object v4, v2, Lcom/miui/packageInstaller/model/MarketAppInfo$DetailHeaderCardInfo;->topValue:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->getScoreNum()Landroid/widget/TextView;

    move-result-object v3

    sget-object v4, Lf6/b;->a:Lf6/b;

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, v6}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, v2, Lcom/miui/packageInstaller/model/MarketAppInfo$DetailHeaderCardInfo;->bottomValue:Ljava/lang/String;

    const-string v6, "card.bottomValue"

    invoke-static {v2, v6}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4, v5, v2}, Lf6/b;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    :cond_4
    const-string v4, "downloadCount"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    goto :goto_1

    :cond_5
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->getInstallNum()Landroid/widget/TextView;

    move-result-object v3

    sget-object v4, Lf6/b;->a:Lf6/b;

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, v6}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v6, v2, Lcom/miui/packageInstaller/model/MarketAppInfo$DetailHeaderCardInfo;->topValue:Ljava/lang/String;

    const-string v7, "card.topValue"

    invoke-static {v6, v7}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4, v5, v6}, Lf6/b;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->getInstallDes()Landroid/widget/TextView;

    move-result-object v3

    iget-object v2, v2, Lcom/miui/packageInstaller/model/MarketAppInfo$DetailHeaderCardInfo;->bottomValue:Ljava/lang/String;

    :goto_2
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_6
    const-string v4, "rank"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    goto/16 :goto_1

    :cond_7
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->getAppRanking()Landroid/widget/TextView;

    move-result-object v3

    iget-object v4, v2, Lcom/miui/packageInstaller/model/MarketAppInfo$DetailHeaderCardInfo;->topValue:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->getAppRankType()Landroid/widget/TextView;

    move-result-object v3

    iget-object v4, v2, Lcom/miui/packageInstaller/model/MarketAppInfo$DetailHeaderCardInfo;->bottomValue:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->getRankLayout()Landroid/widget/LinearLayout;

    move-result-object v3

    iget-object v4, v2, Lcom/miui/packageInstaller/model/MarketAppInfo$DetailHeaderCardInfo;->link:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setTag(Ljava/lang/Object;)V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, v2, Lcom/miui/packageInstaller/model/MarketAppInfo$DetailHeaderCardInfo;->topValue:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v4, 0x2f

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v2, v2, Lcom/miui/packageInstaller/model/MarketAppInfo$DetailHeaderCardInfo;->bottomValue:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject;->o:Ljava/lang/String;

    goto/16 :goto_1

    :cond_8
    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject;->l:Lcom/miui/packageInstaller/model/ApkInfo;

    if-eqz v2, :cond_9

    invoke-virtual {v2}, Lcom/miui/packageInstaller/model/ApkInfo;->getPackageInfo()Landroid/content/pm/PackageInfo;

    move-result-object v2

    goto :goto_3

    :cond_9
    move-object v2, v1

    :goto_3
    invoke-static {v0, v2}, Lcom/android/packageinstaller/miui/a;->n2(Landroid/content/Context;Landroid/content/pm/PackageInfo;)Lcom/android/packageinstaller/miui/a$a;

    move-result-object v0

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->getAppPermissionNum()Landroid/widget/TextView;

    move-result-object p1

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f1102a6

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    if-eqz v0, :cond_a

    invoke-virtual {v0}, Lcom/android/packageinstaller/miui/a$a;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    :cond_a
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private static final D(Landroid/content/Context;Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject;Landroid/view/View;)V
    .locals 3

    const-string v0, "$context"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "this$0"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    goto/16 :goto_2

    :sswitch_0
    new-instance p2, Landroid/content/Intent;

    const-class v0, Lcom/android/packageinstaller/miui/PermissionInfoActivity;

    invoke-direct {p2, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v0, p1, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject;->l:Lcom/miui/packageInstaller/model/ApkInfo;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/ApkInfo;->getPackageInfo()Landroid/content/pm/PackageInfo;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    const-string v2, "extra_package_info"

    invoke-virtual {p2, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    iget-object p1, p1, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject;->l:Lcom/miui/packageInstaller/model/ApkInfo;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/miui/packageInstaller/model/ApkInfo;->getLabel()Ljava/lang/String;

    move-result-object v1

    :cond_1
    const-string p1, "extra_package_name"

    invoke-virtual {p2, p1, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, p2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_2

    :sswitch_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_4

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_4

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p0, p1}, Lq2/d;->c(Landroid/content/Context;Ljava/lang/String;)Z

    invoke-virtual {p2}, Landroid/view/View;->getId()I

    move-result p1

    const v0, 0x7f0a03a5

    const-string v1, "button"

    if-eq p1, v0, :cond_3

    invoke-virtual {p2}, Landroid/view/View;->getId()I

    move-result p1

    const v0, 0x7f0a0005

    if-ne p1, v0, :cond_2

    goto :goto_1

    :cond_2
    instance-of p1, p0, Lo5/a;

    if-eqz p1, :cond_4

    new-instance p1, Lp5/b;

    check-cast p0, Lo5/a;

    const-string p2, "classify_btn"

    invoke-direct {p1, p2, v1, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p1}, Lp5/f;->c()Z

    goto :goto_2

    :cond_3
    :goto_1
    instance-of p1, p0, Lo5/a;

    if-eqz p1, :cond_4

    new-instance p1, Lp5/b;

    check-cast p0, Lo5/a;

    const-string v0, "tag_btn"

    invoke-direct {p1, v0, v1, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    const-string p0, "null cannot be cast to non-null type android.widget.TextView"

    invoke-static {p2, p0}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Landroid/widget/TextView;

    invoke-virtual {p2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p0

    const-string p2, "tag_name"

    invoke-virtual {p1, p2, p0}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p0

    invoke-virtual {p0}, Lp5/f;->c()Z

    :cond_4
    :goto_2
    return-void

    :sswitch_data_0
    .sparse-switch
        0x7f0a0005 -> :sswitch_1
        0x7f0a006d -> :sswitch_0
        0x7f0a01f6 -> :sswitch_1
        0x7f0a03a5 -> :sswitch_1
    .end sparse-switch
.end method

.method public static synthetic z(Landroid/content/Context;Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject;->D(Landroid/content/Context;Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject;Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public final A(Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;)V
    .locals 12

    const-string v0, "viewHolder"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->getLlAppTags()Landroid/widget/LinearLayout;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    :cond_0
    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0d0060

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->getLlAppTags()Landroid/widget/LinearLayout;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject;->m:Lcom/miui/packageInstaller/model/CloudParams;

    iget-object v1, v1, Lcom/miui/packageInstaller/model/CloudParams;->appInfo:Lcom/miui/packageInstaller/model/MarketAppInfo;

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    iget-object v4, v1, Lcom/miui/packageInstaller/model/MarketAppInfo;->appTagInfo:Lcom/miui/packageInstaller/model/MarketAppInfo$AppTagInfo;

    goto :goto_0

    :cond_1
    move-object v4, v2

    :goto_0
    if-eqz v4, :cond_3

    if-eqz v1, :cond_2

    iget-object v1, v1, Lcom/miui/packageInstaller/model/MarketAppInfo;->appTagInfo:Lcom/miui/packageInstaller/model/MarketAppInfo$AppTagInfo;

    if-eqz v1, :cond_2

    iget-boolean v1, v1, Lcom/miui/packageInstaller/model/MarketAppInfo$AppTagInfo;->isGoldenMiAward:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto :goto_1

    :cond_2
    move-object v1, v2

    :goto_1
    invoke-static {v1}, Lm8/i;->c(Ljava/lang/Object;)V

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->getLlAppTags()Landroid/widget/LinearLayout;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    :cond_3
    iget-object v1, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject;->m:Lcom/miui/packageInstaller/model/CloudParams;

    iget-object v1, v1, Lcom/miui/packageInstaller/model/CloudParams;->appInfo:Lcom/miui/packageInstaller/model/MarketAppInfo;

    if-eqz v1, :cond_4

    iget-object v4, v1, Lcom/miui/packageInstaller/model/MarketAppInfo;->appTagInfo:Lcom/miui/packageInstaller/model/MarketAppInfo$AppTagInfo;

    if-eqz v4, :cond_4

    iget-object v4, v4, Lcom/miui/packageInstaller/model/MarketAppInfo$AppTagInfo;->appTags:Ljava/util/List;

    goto :goto_2

    :cond_4
    move-object v4, v2

    :goto_2
    if-eqz v4, :cond_12

    if-eqz v1, :cond_5

    iget-object v1, v1, Lcom/miui/packageInstaller/model/MarketAppInfo;->appTagInfo:Lcom/miui/packageInstaller/model/MarketAppInfo$AppTagInfo;

    if-eqz v1, :cond_5

    iget-object v1, v1, Lcom/miui/packageInstaller/model/MarketAppInfo$AppTagInfo;->appTags:Ljava/util/List;

    if-eqz v1, :cond_5

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto :goto_3

    :cond_5
    move-object v1, v2

    :goto_3
    invoke-static {v1}, Lm8/i;->c(Ljava/lang/Object;)V

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_12

    iget-object v1, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject;->m:Lcom/miui/packageInstaller/model/CloudParams;

    iget-object v1, v1, Lcom/miui/packageInstaller/model/CloudParams;->appInfo:Lcom/miui/packageInstaller/model/MarketAppInfo;

    if-eqz v1, :cond_6

    iget-object v1, v1, Lcom/miui/packageInstaller/model/MarketAppInfo;->appTagInfo:Lcom/miui/packageInstaller/model/MarketAppInfo$AppTagInfo;

    if-eqz v1, :cond_6

    iget-object v1, v1, Lcom/miui/packageInstaller/model/MarketAppInfo$AppTagInfo;->appTags:Ljava/util/List;

    if-eqz v1, :cond_6

    invoke-static {v1}, Lb8/j;->H(Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object v1

    goto :goto_4

    :cond_6
    move-object v1, v2

    :goto_4
    invoke-static {v1}, Lm8/i;->c(Ljava/lang/Object;)V

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_7
    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_12

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lb8/y;

    invoke-virtual {v4}, Lb8/y;->a()I

    move-result v5

    invoke-virtual {v4}, Lb8/y;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/miui/packageInstaller/model/MarketAppInfo$AppTag;

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v6

    const v7, 0x7f11015d

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    iget-object v7, v4, Lcom/miui/packageInstaller/model/MarketAppInfo$AppTag;->name:Ljava/lang/String;

    invoke-static {v6, v7}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    const-string v7, "tag_name"

    const-string v8, "null cannot be cast to non-null type com.miui.packageInstaller.analytics.IPage"

    const-string v9, "button"

    const-string v10, "tag_btn"

    if-eqz v6, :cond_9

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v5

    instance-of v5, v5, Lo5/a;

    if-eqz v5, :cond_8

    new-instance v5, Lp5/g;

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6, v8}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v6, Lo5/a;

    invoke-direct {v5, v10, v9, v6}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    iget-object v6, v4, Lcom/miui/packageInstaller/model/MarketAppInfo$AppTag;->name:Ljava/lang/String;

    invoke-virtual {v5, v7, v6}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object v5

    invoke-virtual {v5}, Lp5/f;->c()Z

    :cond_8
    iget-object v4, v4, Lcom/miui/packageInstaller/model/MarketAppInfo$AppTag;->link:Ljava/lang/String;

    invoke-virtual {v0, v4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v4, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject;->p:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_5

    :cond_9
    iget-object v6, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject;->m:Lcom/miui/packageInstaller/model/CloudParams;

    iget-object v6, v6, Lcom/miui/packageInstaller/model/CloudParams;->appInfo:Lcom/miui/packageInstaller/model/MarketAppInfo;

    if-eqz v6, :cond_a

    iget-object v6, v6, Lcom/miui/packageInstaller/model/MarketAppInfo;->appTagInfo:Lcom/miui/packageInstaller/model/MarketAppInfo$AppTagInfo;

    if-eqz v6, :cond_a

    iget-boolean v6, v6, Lcom/miui/packageInstaller/model/MarketAppInfo$AppTagInfo;->isGoldenMiAward:Z

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    goto :goto_6

    :cond_a
    move-object v6, v2

    :goto_6
    invoke-static {v6}, Lm8/i;->c(Ljava/lang/Object;)V

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-eqz v6, :cond_b

    const/4 v6, 0x2

    if-le v5, v6, :cond_c

    goto/16 :goto_9

    :cond_b
    const/4 v6, 0x3

    if-le v5, v6, :cond_c

    goto/16 :goto_9

    :cond_c
    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v6

    instance-of v6, v6, Lo5/a;

    if-eqz v6, :cond_d

    new-instance v6, Lp5/g;

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v11

    invoke-static {v11, v8}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v11, Lo5/a;

    invoke-direct {v6, v10, v9, v11}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    iget-object v8, v4, Lcom/miui/packageInstaller/model/MarketAppInfo$AppTag;->name:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object v6

    invoke-virtual {v6}, Lp5/f;->c()Z

    :cond_d
    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v6

    const v7, 0x7f0d0036

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->getLlAppTags()Landroid/widget/LinearLayout;

    move-result-object v8

    invoke-virtual {v6, v7, v8, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    const-string v7, "null cannot be cast to non-null type android.widget.TextView"

    invoke-static {v6, v7}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v6, Landroid/widget/TextView;

    iget-object v7, v4, Lcom/miui/packageInstaller/model/MarketAppInfo$AppTag;->name:Ljava/lang/String;

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v7, v4, Lcom/miui/packageInstaller/model/MarketAppInfo$AppTag;->link:Ljava/lang/String;

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    if-lez v5, :cond_e

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject;->n:Ljava/lang/String;

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v7, 0x2c

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject;->n:Ljava/lang/String;

    :cond_e
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject;->n:Ljava/lang/String;

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, v4, Lcom/miui/packageInstaller/model/MarketAppInfo$AppTag;->name:Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject;->n:Ljava/lang/String;

    iget-object v4, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject;->p:Landroid/view/View$OnClickListener;

    invoke-virtual {v6, v4}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->getLlAppTags()Landroid/widget/LinearLayout;

    move-result-object v4

    if-eqz v4, :cond_f

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    goto :goto_7

    :cond_f
    move-object v4, v2

    :goto_7
    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v7, -0x2

    invoke-direct {v5, v7, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    if-eqz v4, :cond_11

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-lez v4, :cond_10

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v7, 0x7f070112

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    invoke-virtual {v5, v4}, Landroid/widget/LinearLayout$LayoutParams;->setMarginStart(I)V

    goto :goto_8

    :cond_10
    invoke-virtual {v5, v3}, Landroid/widget/LinearLayout$LayoutParams;->setMarginStart(I)V

    :cond_11
    :goto_8
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->getLlAppTags()Landroid/widget/LinearLayout;

    move-result-object v4

    if-eqz v4, :cond_7

    invoke-virtual {v4, v6, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_5

    :cond_12
    :goto_9
    return-void
.end method

.method public C(Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;)V
    .locals 8

    if-eqz p1, :cond_b

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject;->m:Lcom/miui/packageInstaller/model/CloudParams;

    iget-object v0, v0, Lcom/miui/packageInstaller/model/CloudParams;->appInfo:Lcom/miui/packageInstaller/model/MarketAppInfo;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/miui/packageInstaller/model/MarketAppInfo;->headImage:Ljava/lang/String;

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->getAppIcon()Landroid/widget/ImageView;

    move-result-object v0

    iget-object v2, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject;->l:Lcom/miui/packageInstaller/model/ApkInfo;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/miui/packageInstaller/model/ApkInfo;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    goto :goto_1

    :cond_1
    move-object v2, v1

    :goto_1
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_3

    :cond_2
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->getAppIcon()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/bumptech/glide/b;->t(Landroid/content/Context;)Lcom/bumptech/glide/k;

    move-result-object v2

    iget-object v3, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject;->m:Lcom/miui/packageInstaller/model/CloudParams;

    iget-object v3, v3, Lcom/miui/packageInstaller/model/CloudParams;->appInfo:Lcom/miui/packageInstaller/model/MarketAppInfo;

    if-eqz v3, :cond_3

    iget-object v3, v3, Lcom/miui/packageInstaller/model/MarketAppInfo;->headImage:Ljava/lang/String;

    goto :goto_2

    :cond_3
    move-object v3, v1

    :goto_2
    invoke-virtual {v2, v3}, Lcom/bumptech/glide/k;->t(Ljava/lang/String;)Lcom/bumptech/glide/j;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/bumptech/glide/j;->w0(Landroid/widget/ImageView;)Ln3/i;

    :goto_3
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->getAppName()Landroid/widget/TextView;

    move-result-object v0

    iget-object v2, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject;->m:Lcom/miui/packageInstaller/model/CloudParams;

    iget-object v2, v2, Lcom/miui/packageInstaller/model/CloudParams;->appInfo:Lcom/miui/packageInstaller/model/MarketAppInfo;

    if-eqz v2, :cond_4

    iget-object v2, v2, Lcom/miui/packageInstaller/model/MarketAppInfo;->displayName:Ljava/lang/String;

    goto :goto_4

    :cond_4
    move-object v2, v1

    :goto_4
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v0

    instance-of v0, v0, Lcom/miui/packageInstaller/InstallProgressActivity;

    const/4 v2, 0x2

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-eqz v0, :cond_7

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v0

    const-string v5, "null cannot be cast to non-null type com.miui.packageInstaller.InstallProgressActivity"

    invoke-static {v0, v5}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/miui/packageInstaller/InstallProgressActivity;

    invoke-virtual {v0}, Lcom/miui/packageInstaller/a;->h1()I

    move-result v0

    if-ne v0, v3, :cond_7

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->getVersion()Landroid/widget/TextView;

    move-result-object v0

    iget-object v5, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject;->l:Lcom/miui/packageInstaller/model/ApkInfo;

    invoke-virtual {v5}, Lcom/miui/packageInstaller/model/ApkInfo;->getInstalledVersionName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f11017d

    new-array v5, v3, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject;->m:Lcom/miui/packageInstaller/model/CloudParams;

    iget-object v6, v6, Lcom/miui/packageInstaller/model/CloudParams;->appInfo:Lcom/miui/packageInstaller/model/MarketAppInfo;

    iget-object v6, v6, Lcom/miui/packageInstaller/model/MarketAppInfo;->versionName:Ljava/lang/String;

    aput-object v6, v5, v4

    invoke-virtual {v1, v2, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_5

    :cond_5
    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f11017e

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject;->l:Lcom/miui/packageInstaller/model/ApkInfo;

    invoke-virtual {v7}, Lcom/miui/packageInstaller/model/ApkInfo;->getInstalledVersionName()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v2, v4

    iget-object v7, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject;->m:Lcom/miui/packageInstaller/model/CloudParams;

    iget-object v7, v7, Lcom/miui/packageInstaller/model/CloudParams;->appInfo:Lcom/miui/packageInstaller/model/MarketAppInfo;

    if-eqz v7, :cond_6

    iget-object v1, v7, Lcom/miui/packageInstaller/model/MarketAppInfo;->versionName:Ljava/lang/String;

    :cond_6
    aput-object v1, v2, v3

    invoke-virtual {v5, v6, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_5

    :cond_7
    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->getVersion()Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject;->l:Lcom/miui/packageInstaller/model/ApkInfo;

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/ApkInfo;->getInstalledVersionName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f110039

    new-array v5, v3, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject;->l:Lcom/miui/packageInstaller/model/ApkInfo;

    invoke-virtual {v6}, Lcom/miui/packageInstaller/model/ApkInfo;->getVersionName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v4

    invoke-virtual {v1, v2, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_5

    :cond_8
    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v1

    const v5, 0x7f11003a

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject;->l:Lcom/miui/packageInstaller/model/ApkInfo;

    invoke-virtual {v6}, Lcom/miui/packageInstaller/model/ApkInfo;->getInstalledVersionName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v4

    iget-object v6, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject;->l:Lcom/miui/packageInstaller/model/ApkInfo;

    invoke-virtual {v6}, Lcom/miui/packageInstaller/model/ApkInfo;->getVersionName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v3

    invoke-virtual {v1, v5, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    :goto_5
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->v()Z

    move-result v0

    if-nez v0, :cond_9

    invoke-direct {p0, p1}, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject;->B(Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;)V

    :cond_9
    invoke-virtual {p0, p1}, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject;->A(Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;)V

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result v0

    if-nez v0, :cond_a

    invoke-virtual {p0, p1}, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject;->E(Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;)V

    :cond_a
    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f060029

    invoke-virtual {v0, v1}, Landroid/content/Context;->getColor(I)I

    move-result v0

    new-array v1, v3, [Landroid/view/View;

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->getAppPermissionLayout()Landroid/widget/LinearLayout;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v1}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object v1

    invoke-interface {v1}, Lmiuix/animation/e;->c()Lmiuix/animation/j;

    move-result-object v1

    invoke-interface {v1, v0}, Lmiuix/animation/j;->i(I)Lmiuix/animation/j;

    move-result-object v1

    new-array v2, v4, [Lmiuix/animation/j$b;

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-interface {v1, v5, v2}, Lmiuix/animation/j;->G(F[Lmiuix/animation/j$b;)Lmiuix/animation/j;

    move-result-object v1

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->getAppPermissionLayout()Landroid/widget/LinearLayout;

    move-result-object v2

    new-array v6, v4, [Lc9/a;

    invoke-interface {v1, v2, v6}, Lmiuix/animation/j;->z(Landroid/view/View;[Lc9/a;)V

    new-array v1, v3, [Landroid/view/View;

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->getRankLayout()Landroid/widget/LinearLayout;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v1}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object v1

    invoke-interface {v1}, Lmiuix/animation/e;->c()Lmiuix/animation/j;

    move-result-object v1

    new-array v2, v4, [Lmiuix/animation/j$b;

    invoke-interface {v1, v5, v2}, Lmiuix/animation/j;->G(F[Lmiuix/animation/j$b;)Lmiuix/animation/j;

    move-result-object v1

    invoke-interface {v1, v0}, Lmiuix/animation/j;->i(I)Lmiuix/animation/j;

    move-result-object v1

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->getRankLayout()Landroid/widget/LinearLayout;

    move-result-object v2

    new-array v6, v4, [Lc9/a;

    invoke-interface {v1, v2, v6}, Lmiuix/animation/j;->z(Landroid/view/View;[Lc9/a;)V

    new-array v1, v3, [Landroid/view/View;

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->getLlAppScoreLayout()Landroid/widget/LinearLayout;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v1}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object v1

    invoke-interface {v1}, Lmiuix/animation/e;->c()Lmiuix/animation/j;

    move-result-object v1

    new-array v2, v4, [Lmiuix/animation/j$b;

    invoke-interface {v1, v5, v2}, Lmiuix/animation/j;->G(F[Lmiuix/animation/j$b;)Lmiuix/animation/j;

    move-result-object v1

    invoke-interface {v1, v0}, Lmiuix/animation/j;->i(I)Lmiuix/animation/j;

    move-result-object v1

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->getLlAppScoreLayout()Landroid/widget/LinearLayout;

    move-result-object v2

    new-array v6, v4, [Lc9/a;

    invoke-interface {v1, v2, v6}, Lmiuix/animation/j;->z(Landroid/view/View;[Lc9/a;)V

    new-array v1, v3, [Landroid/view/View;

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->getLlAppDownloadLayout()Landroid/widget/LinearLayout;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v1}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object v1

    invoke-interface {v1}, Lmiuix/animation/e;->c()Lmiuix/animation/j;

    move-result-object v1

    new-array v2, v4, [Lmiuix/animation/j$b;

    invoke-interface {v1, v5, v2}, Lmiuix/animation/j;->G(F[Lmiuix/animation/j$b;)Lmiuix/animation/j;

    move-result-object v1

    invoke-interface {v1, v0}, Lmiuix/animation/j;->i(I)Lmiuix/animation/j;

    move-result-object v0

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->getLlAppDownloadLayout()Landroid/widget/LinearLayout;

    move-result-object v1

    new-array v2, v4, [Lc9/a;

    invoke-interface {v0, v1, v2}, Lmiuix/animation/j;->z(Landroid/view/View;[Lc9/a;)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->getAppPermissionLayout()Landroid/widget/LinearLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject;->p:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->getRankLayout()Landroid/widget/LinearLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject;->p:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p1, Landroidx/recyclerview/widget/RecyclerView$d0;->itemView:Landroid/view/View;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->getAppName()Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->getVersion()Landroid/widget/TextView;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object p1

    instance-of p1, p1, Lo5/a;

    if-eqz p1, :cond_b

    new-instance p1, Lp5/g;

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v0

    const-string v1, "null cannot be cast to non-null type com.miui.packageInstaller.analytics.IPage"

    invoke-static {v0, v1}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lo5/a;

    const-string v1, "classify_btn"

    const-string v2, "button"

    invoke-direct {p1, v1, v2, v0}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p1}, Lp5/f;->c()Z

    :cond_b
    return-void
.end method

.method public final E(Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;)V
    .locals 3

    const-string v0, "viewHolder"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->getVersion()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->getVersion()Landroid/widget/TextView;

    move-result-object v1

    const/high16 v2, 0x41400000    # 12.0f

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextSize(F)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;->getLlAppTags()Landroid/widget/LinearLayout;

    move-result-object p1

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result p1

    if-nez p1, :cond_0

    const/4 v1, 0x1

    :cond_0
    instance-of p1, v0, Landroid/widget/LinearLayout$LayoutParams;

    if-nez v1, :cond_1

    if-eqz p1, :cond_2

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const v1, 0x7f0700eb

    goto :goto_0

    :cond_1
    if-eqz p1, :cond_2

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const v1, 0x7f0700a8

    :goto_0
    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result p1

    iput p1, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    :cond_2
    return-void
.end method

.method public k()I
    .locals 1

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0d00ad

    goto :goto_0

    :cond_0
    const v0, 0x7f0d00ab

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/android/packageinstaller/utils/g;->v()Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f0d00ac

    goto :goto_0

    :cond_2
    const v0, 0x7f0d00a9

    :goto_0
    return v0
.end method

.method public bridge synthetic o(Landroidx/recyclerview/widget/RecyclerView$d0;)V
    .locals 0

    check-cast p1, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;

    invoke-virtual {p0, p1}, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject;->C(Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject$ViewHolder;)V

    return-void
.end method
