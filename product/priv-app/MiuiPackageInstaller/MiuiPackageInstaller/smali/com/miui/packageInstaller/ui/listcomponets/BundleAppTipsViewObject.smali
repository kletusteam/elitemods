.class public final Lcom/miui/packageInstaller/ui/listcomponets/BundleAppTipsViewObject;
.super Lcom/miui/packageInstaller/ui/listcomponets/f0;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/miui/packageInstaller/ui/listcomponets/BundleAppTipsViewObject$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/miui/packageInstaller/ui/listcomponets/f0<",
        "Lcom/miui/packageInstaller/ui/listcomponets/BundleAppTipsViewObject$ViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private final m:Lcom/miui/packageInstaller/model/WarningCardInfo;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/WarningCardInfo;Ll6/c;Lm6/b;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "warningCardInfo"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/miui/packageInstaller/ui/listcomponets/f0;-><init>(Landroid/content/Context;Ljava/lang/Object;Ll6/c;Lm6/b;)V

    iput-object p2, p0, Lcom/miui/packageInstaller/ui/listcomponets/BundleAppTipsViewObject;->m:Lcom/miui/packageInstaller/model/WarningCardInfo;

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/WarningCardInfo;Ll6/c;Lm6/b;ILm8/g;)V
    .locals 1

    and-int/lit8 p6, p5, 0x4

    const/4 v0, 0x0

    if-eqz p6, :cond_0

    move-object p3, v0

    :cond_0
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_1

    move-object p4, v0

    :cond_1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/miui/packageInstaller/ui/listcomponets/BundleAppTipsViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/WarningCardInfo;Ll6/c;Lm6/b;)V

    return-void
.end method


# virtual methods
.method public k()I
    .locals 1

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0d003e

    goto :goto_0

    :cond_0
    const v0, 0x7f0d003d

    :goto_0
    return v0
.end method

.method public bridge synthetic o(Landroidx/recyclerview/widget/RecyclerView$d0;)V
    .locals 0

    check-cast p1, Lcom/miui/packageInstaller/ui/listcomponets/BundleAppTipsViewObject$ViewHolder;

    invoke-virtual {p0, p1}, Lcom/miui/packageInstaller/ui/listcomponets/BundleAppTipsViewObject;->z(Lcom/miui/packageInstaller/ui/listcomponets/BundleAppTipsViewObject$ViewHolder;)V

    return-void
.end method

.method public z(Lcom/miui/packageInstaller/ui/listcomponets/BundleAppTipsViewObject$ViewHolder;)V
    .locals 3

    const-string v0, "viewHolder"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/BundleAppTipsViewObject$ViewHolder;->getTitleText()Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/packageInstaller/ui/listcomponets/BundleAppTipsViewObject;->m:Lcom/miui/packageInstaller/model/WarningCardInfo;

    iget-object v1, v1, Lcom/miui/packageInstaller/model/WarningCardInfo;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/BundleAppTipsViewObject$ViewHolder;->getDesText()Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/miui/packageInstaller/ui/listcomponets/BundleAppTipsViewObject;->m:Lcom/miui/packageInstaller/model/WarningCardInfo;

    iget-object v1, v1, Lcom/miui/packageInstaller/model/WarningCardInfo;->text:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;I)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p1}, Lcom/miui/packageInstaller/ui/listcomponets/BundleAppTipsViewObject$ViewHolder;->getDesText()Landroid/widget/TextView;

    move-result-object p1

    new-instance v0, Lcom/miui/packageInstaller/ui/listcomponets/BundleAppTipsViewObject$a;

    invoke-direct {v0, p0}, Lcom/miui/packageInstaller/ui/listcomponets/BundleAppTipsViewObject$a;-><init>(Lcom/miui/packageInstaller/ui/listcomponets/BundleAppTipsViewObject;)V

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    return-void
.end method
