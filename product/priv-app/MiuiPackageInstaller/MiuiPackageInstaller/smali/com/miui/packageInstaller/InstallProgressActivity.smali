.class public Lcom/miui/packageInstaller/InstallProgressActivity;
.super Lcom/miui/packageInstaller/a;


# instance fields
.field private W:Ll6/d;

.field private X:Landroidx/recyclerview/widget/RecyclerView;

.field private Y:Landroidx/recyclerview/widget/RecyclerView;

.field private Z:Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject;

.field private e0:I

.field public f0:Lj6/b;

.field private g0:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/miui/packageInstaller/a;-><init>()V

    new-instance v0, Ll6/d;

    invoke-direct {v0}, Ll6/d;-><init>()V

    iput-object v0, p0, Lcom/miui/packageInstaller/InstallProgressActivity;->W:Ll6/d;

    const/4 v0, -0x1

    iput v0, p0, Lcom/miui/packageInstaller/InstallProgressActivity;->e0:I

    return-void
.end method

.method public static final synthetic L1(Lcom/miui/packageInstaller/InstallProgressActivity;Landroid/view/View;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/miui/packageInstaller/a;->backIconClick(Landroid/view/View;)V

    return-void
.end method

.method public static final synthetic M1(Lcom/miui/packageInstaller/InstallProgressActivity;)V
    .locals 0

    invoke-super {p0}, Lcom/miui/packageInstaller/a;->onBackPressed()V

    return-void
.end method

.method public static final synthetic N1(Lcom/miui/packageInstaller/InstallProgressActivity;)V
    .locals 0

    invoke-super {p0}, Lcom/miui/packageInstaller/a;->t1()V

    return-void
.end method

.method public static final synthetic O1(Lcom/miui/packageInstaller/InstallProgressActivity;Landroid/content/Intent;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/miui/packageInstaller/a;->D1(Landroid/content/Intent;)V

    return-void
.end method

.method private final Q1()Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lm6/a<",
            "*>;>;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/miui/packageInstaller/a;->g1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-boolean v1, v1, Lcom/miui/packageInstaller/model/CloudParams;->showSafeModeTip:Z

    if-eqz v1, :cond_0

    new-instance v3, Lcom/miui/packageInstaller/model/CloudParams;

    invoke-direct {v3}, Lcom/miui/packageInstaller/model/CloudParams;-><init>()V

    new-instance v1, Lcom/miui/packageInstaller/model/UiConfig;

    invoke-direct {v1}, Lcom/miui/packageInstaller/model/UiConfig;-><init>()V

    iput-object v1, v3, Lcom/miui/packageInstaller/model/CloudParams;->uiConfig:Lcom/miui/packageInstaller/model/UiConfig;

    const v1, 0x7f110171

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v3, Lcom/miui/packageInstaller/model/CloudParams;->unOpenSafeModeText:Ljava/lang/String;

    new-instance v8, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xc

    const/4 v7, 0x0

    move-object v1, v8

    move-object v2, p0

    invoke-direct/range {v1 .. v7}, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeTipViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/CloudParams;Ll6/c;Lm6/b;ILm8/g;)V

    invoke-virtual {v8}, Lcom/miui/packageInstaller/ui/listcomponets/f0;->a()V

    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-object v0
.end method

.method private final W1(Ll8/a;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ll8/a<",
            "La8/v;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p0}, Landroid/app/Activity;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    sget-object v0, La6/l;->n:La6/l$a;

    const/4 v1, 0x1

    invoke-virtual {v0, p0, v1, p1}, La6/l$a;->a(Landroid/content/Context;ZLl8/a;)V

    return-void
.end method

.method private final X1(Ll8/a;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ll8/a<",
            "La8/v;",
            ">;)Z"
        }
    .end annotation

    iget-boolean v0, p0, Lcom/miui/packageInstaller/InstallProgressActivity;->g0:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/miui/packageInstaller/InstallProgressActivity;->W1(Ll8/a;)V

    goto :goto_0

    :cond_0
    invoke-interface {p1}, Ll8/a;->a()Ljava/lang/Object;

    :goto_0
    iget-boolean p1, p0, Lcom/miui/packageInstaller/InstallProgressActivity;->g0:Z

    return p1
.end method


# virtual methods
.method public C(Ls5/l;II)V
    .locals 0

    invoke-super {p0, p1, p2, p3}, Lcom/miui/packageInstaller/a;->C(Ls5/l;II)V

    invoke-virtual {p0}, Lcom/miui/packageInstaller/InstallProgressActivity;->S1()Lj6/b;

    move-result-object p1

    invoke-direct {p0}, Lcom/miui/packageInstaller/InstallProgressActivity;->Q1()Ljava/util/List;

    move-result-object p2

    invoke-virtual {p1, p2}, Lj6/b;->M(Ljava/util/List;)I

    return-void
.end method

.method public D1(Landroid/content/Intent;)V
    .locals 1

    const-string v0, "intent"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/miui/packageInstaller/InstallProgressActivity$e;

    invoke-direct {v0, p0, p1}, Lcom/miui/packageInstaller/InstallProgressActivity$e;-><init>(Lcom/miui/packageInstaller/InstallProgressActivity;Landroid/content/Intent;)V

    invoke-direct {p0, v0}, Lcom/miui/packageInstaller/InstallProgressActivity;->X1(Ll8/a;)Z

    return-void
.end method

.method public final P1(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lm6/a<",
            "*>;>;)V"
        }
    .end annotation

    const-string v0, "layout"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Ln5/e;->a:Ln5/e;

    const-string v1, "08-0"

    invoke-virtual {v0, v1}, Ln5/e;->c(Ljava/lang/String;)Lcom/miui/packageInstaller/model/AdModel;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/miui/packageInstaller/InstallProgressActivity;->T1(Lcom/miui/packageInstaller/model/AdModel;)Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x1

    if-eqz v0, :cond_4

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    xor-int/2addr v2, v1

    if-eqz v2, :cond_4

    invoke-virtual {p0}, Lcom/miui/packageInstaller/a;->g1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v2

    const/4 v3, 0x0

    if-eqz v2, :cond_0

    iget-boolean v2, v2, Lcom/miui/packageInstaller/model/CloudParams;->showAdsAfter:Z

    if-ne v2, v1, :cond_0

    move v2, v1

    goto :goto_0

    :cond_0
    move v2, v3

    :goto_0
    if-nez v2, :cond_3

    invoke-virtual {p0}, Lcom/miui/packageInstaller/a;->g1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-boolean v2, v2, Lcom/miui/packageInstaller/model/CloudParams;->useSystemAppRules:Z

    if-ne v2, v1, :cond_1

    move v2, v1

    goto :goto_1

    :cond_1
    move v2, v3

    :goto_1
    if-nez v2, :cond_3

    invoke-virtual {p0}, Lcom/miui/packageInstaller/a;->g1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-boolean v2, v2, Lcom/miui/packageInstaller/model/CloudParams;->singletonAuthShowAdsAfter:Z

    if-ne v2, v1, :cond_2

    move v3, v1

    :cond_2
    if-eqz v3, :cond_4

    :cond_3
    invoke-interface {p1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_3

    :cond_4
    invoke-virtual {p0}, Lcom/miui/packageInstaller/a;->h1()I

    move-result v0

    if-ne v0, v1, :cond_7

    invoke-virtual {p0}, Lcom/miui/packageInstaller/a;->g1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-virtual {p0}, Lcom/miui/packageInstaller/a;->g1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_5

    iget-object v0, v0, Lcom/miui/packageInstaller/model/CloudParams;->appInfo:Lcom/miui/packageInstaller/model/MarketAppInfo;

    goto :goto_2

    :cond_5
    move-object v0, v1

    :goto_2
    if-eqz v0, :cond_7

    invoke-virtual {p0}, Lcom/miui/packageInstaller/a;->g1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v0

    if-eqz v0, :cond_6

    new-instance v2, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoPicViewObject;

    iget-object v3, p0, Lcom/miui/packageInstaller/InstallProgressActivity;->W:Ll6/d;

    invoke-direct {v2, p0, v0, v3, v1}, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoPicViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/CloudParams;Ll6/c;Lm6/b;)V

    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_6
    invoke-virtual {p0}, Lcom/miui/packageInstaller/a;->g1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v0

    if-eqz v0, :cond_7

    new-instance v2, Lcom/miui/packageInstaller/ui/listcomponets/AppDesViewObject;

    iget-object v3, p0, Lcom/miui/packageInstaller/InstallProgressActivity;->W:Ll6/d;

    invoke-direct {v2, p0, v0, v3, v1}, Lcom/miui/packageInstaller/ui/listcomponets/AppDesViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/CloudParams;Ll6/c;Lm6/b;)V

    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_7
    :goto_3
    return-void
.end method

.method public final R1()Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lm6/a<",
            "*>;>;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/miui/packageInstaller/a;->h1()I

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-ne v1, v2, :cond_3

    invoke-virtual {p0}, Lcom/miui/packageInstaller/a;->g1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/miui/packageInstaller/a;->g1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-boolean v1, v1, Lcom/miui/packageInstaller/model/CloudParams;->useSystemAppRules:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto :goto_0

    :cond_0
    move-object v1, v3

    :goto_0
    invoke-static {v1}, Lm8/i;->c(Ljava/lang/Object;)V

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_3

    invoke-virtual {p0}, Lcom/miui/packageInstaller/a;->g1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-boolean v1, v1, Lcom/miui/packageInstaller/model/CloudParams;->storeListed:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto :goto_1

    :cond_1
    move-object v1, v3

    :goto_1
    invoke-static {v1}, Lm8/i;->c(Ljava/lang/Object;)V

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/miui/packageInstaller/a;->g1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, v1, Lcom/miui/packageInstaller/model/CloudParams;->appInfo:Lcom/miui/packageInstaller/model/MarketAppInfo;

    goto :goto_2

    :cond_2
    move-object v1, v3

    :goto_2
    if-eqz v1, :cond_3

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    if-eqz v2, :cond_4

    new-instance v1, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject;

    invoke-virtual {p0}, Lcom/miui/packageInstaller/a;->f1()Lcom/miui/packageInstaller/model/ApkInfo;

    move-result-object v6

    invoke-static {v6}, Lm8/i;->c(Ljava/lang/Object;)V

    invoke-virtual {p0}, Lcom/miui/packageInstaller/a;->g1()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v7

    invoke-static {v7}, Lm8/i;->c(Ljava/lang/Object;)V

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v4, v1

    move-object v5, p0

    invoke-direct/range {v4 .. v9}, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/ApkInfo;Lcom/miui/packageInstaller/model/CloudParams;Ll6/c;Lm6/b;)V

    iput-object v1, p0, Lcom/miui/packageInstaller/InstallProgressActivity;->Z:Lcom/miui/packageInstaller/ui/listcomponets/AppInfoTwoViewObject;

    invoke-static {v1}, Lm8/i;->c(Ljava/lang/Object;)V

    :goto_4
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    :cond_4
    invoke-virtual {p0}, Lcom/miui/packageInstaller/a;->f1()Lcom/miui/packageInstaller/model/ApkInfo;

    move-result-object v1

    if-eqz v1, :cond_5

    new-instance v1, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoViewObject;

    invoke-virtual {p0}, Lcom/miui/packageInstaller/a;->f1()Lcom/miui/packageInstaller/model/ApkInfo;

    move-result-object v4

    invoke-static {v4}, Lm8/i;->c(Ljava/lang/Object;)V

    invoke-direct {v1, p0, v4, v3, v3}, Lcom/miui/packageInstaller/ui/listcomponets/AppInfoViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/ApkInfo;Ll6/c;Lm6/b;)V

    goto :goto_4

    :cond_5
    :goto_5
    const v1, 0x7f0a01b0

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/miui/packageInstaller/ui/InstallerActionBar;

    invoke-virtual {p0, v1}, Lcom/miui/packageInstaller/a;->l1(Lcom/miui/packageInstaller/ui/InstallerActionBar;)V

    if-nez v2, :cond_6

    new-instance v1, Lcom/miui/packageInstaller/ui/listcomponets/AppPermissionsInfoViewObject;

    invoke-virtual {p0}, Lcom/miui/packageInstaller/a;->f1()Lcom/miui/packageInstaller/model/ApkInfo;

    move-result-object v2

    invoke-static {v2}, Lm8/i;->c(Ljava/lang/Object;)V

    invoke-direct {v1, p0, v2, v3, v3}, Lcom/miui/packageInstaller/ui/listcomponets/AppPermissionsInfoViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/ApkInfo;Ll6/c;Lm6/b;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_6
    invoke-virtual {p0, v0}, Lcom/miui/packageInstaller/InstallProgressActivity;->P1(Ljava/util/List;)V

    new-instance v1, Lcom/miui/packageInstaller/ui/listcomponets/FootViewObject;

    invoke-direct {v1, p0}, Lcom/miui/packageInstaller/ui/listcomponets/FootViewObject;-><init>(Landroid/content/Context;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method public final S1()Lj6/b;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/InstallProgressActivity;->f0:Lj6/b;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    const-string v0, "mBottomAdapter"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v0, 0x0

    return-object v0
.end method

.method public final T1(Lcom/miui/packageInstaller/model/AdModel;)Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/miui/packageInstaller/model/AdModel;",
            ")",
            "Ljava/util/List<",
            "Lm6/a<",
            "*>;>;"
        }
    .end annotation

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/miui/packageInstaller/model/AdModel;->getData()Ljava/util/List;

    move-result-object p1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_1

    new-instance v2, Lcom/miui/packageInstaller/model/AdTitleModel;

    invoke-direct {v2}, Lcom/miui/packageInstaller/model/AdTitleModel;-><init>()V

    const/4 v3, 0x0

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/miui/packageInstaller/model/AdModel$DesData;

    invoke-virtual {v4}, Lcom/miui/packageInstaller/model/AdModel$DesData;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/miui/packageInstaller/model/AdTitleModel;->setTitle(Ljava/lang/String;)V

    new-instance v4, Lcom/miui/packageInstaller/ui/listcomponets/AdTitleViewObject;

    iget-object v5, p0, Lcom/miui/packageInstaller/InstallProgressActivity;->W:Ll6/d;

    invoke-direct {v4, p0, v2, v5, v0}, Lcom/miui/packageInstaller/ui/listcomponets/AdTitleViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/AdTitleModel;Ll6/c;Lm6/b;)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;

    invoke-virtual {p0}, Lcom/miui/packageInstaller/a;->f1()Lcom/miui/packageInstaller/model/ApkInfo;

    move-result-object v8

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    move-object v9, p1

    check-cast v9, Lcom/miui/packageInstaller/model/AdModel$DesData;

    iget-object v10, p0, Lcom/miui/packageInstaller/InstallProgressActivity;->W:Ll6/d;

    const/4 v11, 0x0

    move-object v6, v0

    move-object v7, p0

    invoke-direct/range {v6 .. v11}, Lcom/miui/packageInstaller/ui/listcomponets/RecommendAppViewObject;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/ApkInfo;Lcom/miui/packageInstaller/model/AdModel$DesData;Ll6/c;Lm6/b;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    return-object v1
.end method

.method public final U1(Lj6/b;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/InstallProgressActivity;->f0:Lj6/b;

    return-void
.end method

.method public final V1(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/miui/packageInstaller/InstallProgressActivity;->g0:Z

    return-void
.end method

.method public backIconClick(Landroid/view/View;)V
    .locals 1

    new-instance v0, Lcom/miui/packageInstaller/InstallProgressActivity$a;

    invoke-direct {v0, p0, p1}, Lcom/miui/packageInstaller/InstallProgressActivity$a;-><init>(Lcom/miui/packageInstaller/InstallProgressActivity;Landroid/view/View;)V

    invoke-direct {p0, v0}, Lcom/miui/packageInstaller/InstallProgressActivity;->X1(Ll8/a;)Z

    return-void
.end method

.method public m1()V
    .locals 10

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0d0021

    goto :goto_0

    :cond_0
    const v0, 0x7f0d0020

    :goto_0
    invoke-virtual {p0, v0}, Lmiuix/appcompat/app/j;->setContentView(I)V

    invoke-super {p0}, Lcom/miui/packageInstaller/a;->m1()V

    const v0, 0x7f0a02d1

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    invoke-static {p0}, Lcom/android/packageinstaller/utils/u;->b(Landroid/app/Activity;)I

    move-result v2

    invoke-virtual {v0, v1, v2, v1, v1}, Landroid/view/View;->setPadding(IIII)V

    :cond_1
    const v0, 0x7f0a0216

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v2, "findViewById(R.id.main_content)"

    invoke-static {v0, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    iput-object v0, p0, Lcom/miui/packageInstaller/InstallProgressActivity;->X:Landroidx/recyclerview/widget/RecyclerView;

    const-string v2, "mMainRecyclerView"

    const/4 v3, 0x0

    if-nez v0, :cond_2

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v3

    :cond_2
    const/4 v4, 0x2

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setOverScrollMode(I)V

    iget-object v0, p0, Lcom/miui/packageInstaller/InstallProgressActivity;->X:Landroidx/recyclerview/widget/RecyclerView;

    if-nez v0, :cond_3

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v3

    :cond_3
    new-instance v5, Landroidx/recyclerview/widget/LinearLayoutManager;

    const/4 v6, 0x1

    invoke-direct {v5, p0, v6, v1}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    invoke-virtual {v0, v5}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$o;)V

    const v0, 0x7f0a0094

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v5, "findViewById(R.id.bottom_content)"

    invoke-static {v0, v5}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    iput-object v0, p0, Lcom/miui/packageInstaller/InstallProgressActivity;->Y:Landroidx/recyclerview/widget/RecyclerView;

    const-string v5, "mBottomRecyclerView"

    if-nez v0, :cond_4

    invoke-static {v5}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v3

    :cond_4
    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setOverScrollMode(I)V

    iget-object v0, p0, Lcom/miui/packageInstaller/InstallProgressActivity;->Y:Landroidx/recyclerview/widget/RecyclerView;

    if-nez v0, :cond_5

    invoke-static {v5}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v0, v3

    :cond_5
    new-instance v4, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-direct {v4, p0, v6, v1}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    invoke-virtual {v0, v4}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$o;)V

    new-instance v0, Lj6/b;

    iget-object v1, p0, Lcom/miui/packageInstaller/InstallProgressActivity;->X:Landroidx/recyclerview/widget/RecyclerView;

    if-nez v1, :cond_6

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v1, v3

    :cond_6
    invoke-direct {v0, v1}, Lj6/b;-><init>(Landroidx/recyclerview/widget/RecyclerView;)V

    invoke-virtual {p0, v0}, Lcom/miui/packageInstaller/a;->K1(Lj6/b;)V

    new-instance v0, Lj6/b;

    iget-object v1, p0, Lcom/miui/packageInstaller/InstallProgressActivity;->Y:Landroidx/recyclerview/widget/RecyclerView;

    if-nez v1, :cond_7

    invoke-static {v5}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v1, v3

    :cond_7
    invoke-direct {v0, v1}, Lj6/b;-><init>(Landroidx/recyclerview/widget/RecyclerView;)V

    invoke-virtual {p0, v0}, Lcom/miui/packageInstaller/InstallProgressActivity;->U1(Lj6/b;)V

    invoke-virtual {p0}, Lcom/miui/packageInstaller/a;->e1()Lj6/b;

    move-result-object v0

    invoke-virtual {p0}, Lcom/miui/packageInstaller/InstallProgressActivity;->R1()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lj6/b;->M(Ljava/util/List;)I

    invoke-static {p0}, Landroidx/lifecycle/k;->a(Landroidx/lifecycle/j;)Landroidx/lifecycle/e;

    move-result-object v4

    invoke-static {}, Lv8/t0;->c()Lv8/v1;

    move-result-object v5

    const/4 v6, 0x0

    new-instance v7, Lcom/miui/packageInstaller/InstallProgressActivity$b;

    invoke-direct {v7, p0, v3}, Lcom/miui/packageInstaller/InstallProgressActivity$b;-><init>(Lcom/miui/packageInstaller/InstallProgressActivity;Ld8/d;)V

    const/4 v8, 0x2

    const/4 v9, 0x0

    invoke-static/range {v4 .. v9}, Lv8/f;->d(Lv8/e0;Ld8/g;Lv8/g0;Ll8/p;ILjava/lang/Object;)Lv8/k1;

    return-void
.end method

.method public onBackPressed()V
    .locals 1

    new-instance v0, Lcom/miui/packageInstaller/InstallProgressActivity$c;

    invoke-direct {v0, p0}, Lcom/miui/packageInstaller/InstallProgressActivity$c;-><init>(Lcom/miui/packageInstaller/InstallProgressActivity;)V

    invoke-direct {p0, v0}, Lcom/miui/packageInstaller/InstallProgressActivity;->X1(Ll8/a;)Z

    return-void
.end method

.method public t1()V
    .locals 1

    new-instance v0, Lcom/miui/packageInstaller/InstallProgressActivity$d;

    invoke-direct {v0, p0}, Lcom/miui/packageInstaller/InstallProgressActivity$d;-><init>(Lcom/miui/packageInstaller/InstallProgressActivity;)V

    invoke-direct {p0, v0}, Lcom/miui/packageInstaller/InstallProgressActivity;->X1(Ll8/a;)Z

    return-void
.end method
