.class public final Lcom/miui/packageInstaller/ui/listcomponets/SafeModeInDetailViewObject$ViewHolder;
.super Landroidx/recyclerview/widget/RecyclerView$d0;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/packageInstaller/ui/listcomponets/SafeModeInDetailViewObject;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ViewHolder"
.end annotation


# instance fields
.field private aboutSafeMode:Landroid/view/View;

.field private rootView:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 3

    const-string v0, "itemView"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$d0;-><init>(Landroid/view/View;)V

    const v0, 0x7f0a000f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "itemView.findViewById(R.id.about_safe_mode)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeInDetailViewObject$ViewHolder;->aboutSafeMode:Landroid/view/View;

    const v0, 0x7f0a02d1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeInDetailViewObject$ViewHolder;->rootView:Landroid/widget/LinearLayout;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    instance-of v0, v0, Lo5/a;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "null cannot be cast to non-null type com.miui.packageInstaller.analytics.IPage"

    invoke-static {v0, v1}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lo5/a;

    invoke-interface {v0}, Lo5/a;->getRef()Ljava/lang/String;

    move-result-object v0

    const-string v1, "itemView.context as IPage).ref"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeInDetailViewObject$ViewHolder;->aboutSafeMode:Landroid/view/View;

    new-instance v1, Lcom/miui/packageInstaller/ui/listcomponets/x;

    invoke-direct {v1, p1}, Lcom/miui/packageInstaller/ui/listcomponets/x;-><init>(Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    const v0, 0x7f060029

    invoke-virtual {p1, v0}, Landroid/content/Context;->getColor(I)I

    move-result p1

    const/4 v0, 0x1

    new-array v0, v0, [Landroid/view/View;

    iget-object v1, p0, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeInDetailViewObject$ViewHolder;->aboutSafeMode:Landroid/view/View;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static {v0}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object v0

    invoke-interface {v0}, Lmiuix/animation/e;->c()Lmiuix/animation/j;

    move-result-object v0

    invoke-interface {v0, p1}, Lmiuix/animation/j;->i(I)Lmiuix/animation/j;

    move-result-object p1

    const/high16 v0, 0x3f800000    # 1.0f

    new-array v1, v2, [Lmiuix/animation/j$b;

    invoke-interface {p1, v0, v1}, Lmiuix/animation/j;->G(F[Lmiuix/animation/j$b;)Lmiuix/animation/j;

    move-result-object p1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeInDetailViewObject$ViewHolder;->aboutSafeMode:Landroid/view/View;

    new-array v1, v2, [Lc9/a;

    invoke-interface {p1, v0, v1}, Lmiuix/animation/j;->z(Landroid/view/View;[Lc9/a;)V

    return-void
.end method

.method private static final _init_$lambda-0(Landroid/view/View;Landroid/view/View;)V
    .locals 0

    const-string p1, "$itemView"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p0

    const-string p1, "miui_packageinstaller://com.miui.packageinstaller/safe_mode?safe_mode_type=package_install&safe_mode_ref=safe_model"

    invoke-static {p0, p1}, Lq2/d;->b(Landroid/content/Context;Ljava/lang/String;)Z

    return-void
.end method

.method public static synthetic a(Landroid/view/View;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeInDetailViewObject$ViewHolder;->_init_$lambda-0(Landroid/view/View;Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public final getAboutSafeMode()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeInDetailViewObject$ViewHolder;->aboutSafeMode:Landroid/view/View;

    return-object v0
.end method

.method public final getRootView()Landroid/widget/LinearLayout;
    .locals 1

    iget-object v0, p0, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeInDetailViewObject$ViewHolder;->rootView:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method public final setAboutSafeMode(Landroid/view/View;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeInDetailViewObject$ViewHolder;->aboutSafeMode:Landroid/view/View;

    return-void
.end method

.method public final setRootView(Landroid/widget/LinearLayout;)V
    .locals 0

    iput-object p1, p0, Lcom/miui/packageInstaller/ui/listcomponets/SafeModeInDetailViewObject$ViewHolder;->rootView:Landroid/widget/LinearLayout;

    return-void
.end method
