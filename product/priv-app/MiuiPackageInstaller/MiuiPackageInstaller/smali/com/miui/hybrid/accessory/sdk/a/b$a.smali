.class public final enum Lcom/miui/hybrid/accessory/sdk/a/b$a;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/miui/hybrid/accessory/sdk/a/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/miui/hybrid/accessory/sdk/a/b$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/miui/hybrid/accessory/sdk/a/b$a;

.field public static final enum b:Lcom/miui/hybrid/accessory/sdk/a/b$a;

.field public static final enum c:Lcom/miui/hybrid/accessory/sdk/a/b$a;

.field public static final enum d:Lcom/miui/hybrid/accessory/sdk/a/b$a;

.field public static final enum e:Lcom/miui/hybrid/accessory/sdk/a/b$a;

.field public static final enum f:Lcom/miui/hybrid/accessory/sdk/a/b$a;

.field public static final enum g:Lcom/miui/hybrid/accessory/sdk/a/b$a;

.field public static final enum h:Lcom/miui/hybrid/accessory/sdk/a/b$a;

.field public static final enum i:Lcom/miui/hybrid/accessory/sdk/a/b$a;

.field public static final enum j:Lcom/miui/hybrid/accessory/sdk/a/b$a;

.field public static final enum k:Lcom/miui/hybrid/accessory/sdk/a/b$a;

.field public static final enum l:Lcom/miui/hybrid/accessory/sdk/a/b$a;

.field public static final enum m:Lcom/miui/hybrid/accessory/sdk/a/b$a;

.field private static final n:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/miui/hybrid/accessory/sdk/a/b$a;",
            ">;"
        }
    .end annotation
.end field

.field private static final synthetic q:[Lcom/miui/hybrid/accessory/sdk/a/b$a;


# instance fields
.field private final o:S

.field private final p:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 17

    new-instance v0, Lcom/miui/hybrid/accessory/sdk/a/b$a;

    const-string v1, "APP_INFO"

    const/4 v2, 0x0

    const/4 v3, 0x1

    const-string v4, "appInfo"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/miui/hybrid/accessory/sdk/a/b$a;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v0, Lcom/miui/hybrid/accessory/sdk/a/b$a;->a:Lcom/miui/hybrid/accessory/sdk/a/b$a;

    new-instance v1, Lcom/miui/hybrid/accessory/sdk/a/b$a;

    const-string v4, "MIN_MINA_VERSION_CODE"

    const/4 v5, 0x2

    const-string v6, "minMinaVersionCode"

    invoke-direct {v1, v4, v3, v5, v6}, Lcom/miui/hybrid/accessory/sdk/a/b$a;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v1, Lcom/miui/hybrid/accessory/sdk/a/b$a;->b:Lcom/miui/hybrid/accessory/sdk/a/b$a;

    new-instance v4, Lcom/miui/hybrid/accessory/sdk/a/b$a;

    const-string v6, "TEMPLATE"

    const/4 v7, 0x3

    const-string v8, "template"

    invoke-direct {v4, v6, v5, v7, v8}, Lcom/miui/hybrid/accessory/sdk/a/b$a;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v4, Lcom/miui/hybrid/accessory/sdk/a/b$a;->c:Lcom/miui/hybrid/accessory/sdk/a/b$a;

    new-instance v6, Lcom/miui/hybrid/accessory/sdk/a/b$a;

    const-string v8, "ICON"

    const/4 v9, 0x4

    const-string v10, "icon"

    invoke-direct {v6, v8, v7, v9, v10}, Lcom/miui/hybrid/accessory/sdk/a/b$a;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v6, Lcom/miui/hybrid/accessory/sdk/a/b$a;->d:Lcom/miui/hybrid/accessory/sdk/a/b$a;

    new-instance v8, Lcom/miui/hybrid/accessory/sdk/a/b$a;

    const-string v10, "TITLE"

    const/4 v11, 0x5

    const-string v12, "title"

    invoke-direct {v8, v10, v9, v11, v12}, Lcom/miui/hybrid/accessory/sdk/a/b$a;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v8, Lcom/miui/hybrid/accessory/sdk/a/b$a;->e:Lcom/miui/hybrid/accessory/sdk/a/b$a;

    new-instance v10, Lcom/miui/hybrid/accessory/sdk/a/b$a;

    const-string v12, "DESCRIPTION"

    const/4 v13, 0x6

    const-string v14, "description"

    invoke-direct {v10, v12, v11, v13, v14}, Lcom/miui/hybrid/accessory/sdk/a/b$a;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v10, Lcom/miui/hybrid/accessory/sdk/a/b$a;->f:Lcom/miui/hybrid/accessory/sdk/a/b$a;

    new-instance v12, Lcom/miui/hybrid/accessory/sdk/a/b$a;

    const-string v14, "CATEGORY"

    const/4 v15, 0x7

    const-string v11, "category"

    invoke-direct {v12, v14, v13, v15, v11}, Lcom/miui/hybrid/accessory/sdk/a/b$a;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v12, Lcom/miui/hybrid/accessory/sdk/a/b$a;->g:Lcom/miui/hybrid/accessory/sdk/a/b$a;

    new-instance v11, Lcom/miui/hybrid/accessory/sdk/a/b$a;

    const-string v14, "BUTTON_TEXT"

    const/16 v13, 0x8

    const-string v9, "buttonText"

    invoke-direct {v11, v14, v15, v13, v9}, Lcom/miui/hybrid/accessory/sdk/a/b$a;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v11, Lcom/miui/hybrid/accessory/sdk/a/b$a;->h:Lcom/miui/hybrid/accessory/sdk/a/b$a;

    new-instance v9, Lcom/miui/hybrid/accessory/sdk/a/b$a;

    const-string v14, "SIZE"

    const/16 v15, 0x9

    const-string v7, "size"

    invoke-direct {v9, v14, v13, v15, v7}, Lcom/miui/hybrid/accessory/sdk/a/b$a;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v9, Lcom/miui/hybrid/accessory/sdk/a/b$a;->i:Lcom/miui/hybrid/accessory/sdk/a/b$a;

    new-instance v7, Lcom/miui/hybrid/accessory/sdk/a/b$a;

    const-string v14, "SPECIAL_DATA"

    const/16 v13, 0xa

    const-string v5, "specialData"

    invoke-direct {v7, v14, v15, v13, v5}, Lcom/miui/hybrid/accessory/sdk/a/b$a;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v7, Lcom/miui/hybrid/accessory/sdk/a/b$a;->j:Lcom/miui/hybrid/accessory/sdk/a/b$a;

    new-instance v5, Lcom/miui/hybrid/accessory/sdk/a/b$a;

    const-string v14, "PAGE_NAME"

    const/16 v15, 0xb

    const-string v3, "pageName"

    invoke-direct {v5, v14, v13, v15, v3}, Lcom/miui/hybrid/accessory/sdk/a/b$a;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v5, Lcom/miui/hybrid/accessory/sdk/a/b$a;->k:Lcom/miui/hybrid/accessory/sdk/a/b$a;

    new-instance v3, Lcom/miui/hybrid/accessory/sdk/a/b$a;

    const-string v14, "STATE"

    const/16 v13, 0xc

    const-string v2, "state"

    invoke-direct {v3, v14, v15, v13, v2}, Lcom/miui/hybrid/accessory/sdk/a/b$a;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v3, Lcom/miui/hybrid/accessory/sdk/a/b$a;->l:Lcom/miui/hybrid/accessory/sdk/a/b$a;

    new-instance v2, Lcom/miui/hybrid/accessory/sdk/a/b$a;

    const-string v14, "SETTING"

    const/16 v15, 0xd

    move-object/from16 v16, v3

    const-string v3, "setting"

    invoke-direct {v2, v14, v13, v15, v3}, Lcom/miui/hybrid/accessory/sdk/a/b$a;-><init>(Ljava/lang/String;ISLjava/lang/String;)V

    sput-object v2, Lcom/miui/hybrid/accessory/sdk/a/b$a;->m:Lcom/miui/hybrid/accessory/sdk/a/b$a;

    new-array v3, v15, [Lcom/miui/hybrid/accessory/sdk/a/b$a;

    const/4 v14, 0x0

    aput-object v0, v3, v14

    const/4 v0, 0x1

    aput-object v1, v3, v0

    const/4 v0, 0x2

    aput-object v4, v3, v0

    const/4 v0, 0x3

    aput-object v6, v3, v0

    const/4 v0, 0x4

    aput-object v8, v3, v0

    const/4 v0, 0x5

    aput-object v10, v3, v0

    const/4 v0, 0x6

    aput-object v12, v3, v0

    const/4 v0, 0x7

    aput-object v11, v3, v0

    const/16 v0, 0x8

    aput-object v9, v3, v0

    const/16 v0, 0x9

    aput-object v7, v3, v0

    const/16 v0, 0xa

    aput-object v5, v3, v0

    const/16 v0, 0xb

    aput-object v16, v3, v0

    aput-object v2, v3, v13

    sput-object v3, Lcom/miui/hybrid/accessory/sdk/a/b$a;->q:[Lcom/miui/hybrid/accessory/sdk/a/b$a;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/miui/hybrid/accessory/sdk/a/b$a;->n:Ljava/util/Map;

    const-class v0, Lcom/miui/hybrid/accessory/sdk/a/b$a;

    invoke-static {v0}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/hybrid/accessory/sdk/a/b$a;

    sget-object v2, Lcom/miui/hybrid/accessory/sdk/a/b$a;->n:Ljava/util/Map;

    invoke-virtual {v1}, Lcom/miui/hybrid/accessory/sdk/a/b$a;->a()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ISLjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(S",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-short p3, p0, Lcom/miui/hybrid/accessory/sdk/a/b$a;->o:S

    iput-object p4, p0, Lcom/miui/hybrid/accessory/sdk/a/b$a;->p:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/miui/hybrid/accessory/sdk/a/b$a;
    .locals 1

    const-class v0, Lcom/miui/hybrid/accessory/sdk/a/b$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/miui/hybrid/accessory/sdk/a/b$a;

    return-object p0
.end method

.method public static values()[Lcom/miui/hybrid/accessory/sdk/a/b$a;
    .locals 1

    sget-object v0, Lcom/miui/hybrid/accessory/sdk/a/b$a;->q:[Lcom/miui/hybrid/accessory/sdk/a/b$a;

    invoke-virtual {v0}, [Lcom/miui/hybrid/accessory/sdk/a/b$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/miui/hybrid/accessory/sdk/a/b$a;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/miui/hybrid/accessory/sdk/a/b$a;->p:Ljava/lang/String;

    return-object v0
.end method
