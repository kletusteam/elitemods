.class public Lva/e;
.super Landroidx/preference/c;


# instance fields
.field private F0:Lva/g;

.field private G0:Lva/d;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Landroidx/preference/c;-><init>()V

    new-instance v0, Lva/e$a;

    invoke-direct {v0, p0}, Lva/e$a;-><init>(Lva/e;)V

    iput-object v0, p0, Lva/e;->G0:Lva/d;

    new-instance v1, Lva/g;

    invoke-direct {v1, v0, p0}, Lva/g;-><init>(Lva/d;Landroidx/preference/f;)V

    iput-object v1, p0, Lva/e;->F0:Lva/g;

    return-void
.end method

.method static synthetic e2(Lva/e;Landroid/content/Context;)Landroid/view/View;
    .locals 0

    invoke-virtual {p0, p1}, Landroidx/preference/f;->Y1(Landroid/content/Context;)Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method static synthetic f2(Lva/e;Landroid/view/View;)V
    .locals 0

    invoke-virtual {p0, p1}, Landroidx/preference/f;->X1(Landroid/view/View;)V

    return-void
.end method

.method public static g2(Ljava/lang/String;)Lva/e;
    .locals 3

    new-instance v0, Lva/e;

    invoke-direct {v0}, Lva/e;-><init>()V

    new-instance v1, Landroid/os/Bundle;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/os/Bundle;-><init>(I)V

    const-string v2, "key"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroidx/fragment/app/Fragment;->v1(Landroid/os/Bundle;)V

    return-object v0
.end method


# virtual methods
.method public O1(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 1

    iget-object v0, p0, Lva/e;->F0:Lva/g;

    invoke-virtual {v0, p1}, Lva/g;->a(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object p1

    return-object p1
.end method

.method protected final a2(Landroidx/appcompat/app/a$a;)V
    .locals 1

    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "using miuix builder instead"

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method protected h2(Lmiuix/appcompat/app/i$b;)V
    .locals 2

    new-instance v0, Lva/a;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->w()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lva/a;-><init>(Landroid/content/Context;Lmiuix/appcompat/app/i$b;)V

    invoke-super {p0, v0}, Landroidx/preference/c;->a2(Landroidx/appcompat/app/a$a;)V

    return-void
.end method
