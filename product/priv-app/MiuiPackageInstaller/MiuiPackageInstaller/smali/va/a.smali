.class Lva/a;
.super Landroidx/appcompat/app/a$a;


# instance fields
.field private c:Lmiuix/appcompat/app/i$b;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILmiuix/appcompat/app/i$b;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroidx/appcompat/app/a$a;-><init>(Landroid/content/Context;I)V

    iput-object p3, p0, Lva/a;->c:Lmiuix/appcompat/app/i$b;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lmiuix/appcompat/app/i$b;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lva/a;-><init>(Landroid/content/Context;ILmiuix/appcompat/app/i$b;)V

    return-void
.end method


# virtual methods
.method public c(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroidx/appcompat/app/a$a;
    .locals 1

    iget-object v0, p0, Lva/a;->c:Lmiuix/appcompat/app/i$b;

    invoke-virtual {v0, p1, p2}, Lmiuix/appcompat/app/i$b;->b(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/i$b;

    return-object p0
.end method

.method public d(Landroid/view/View;)Landroidx/appcompat/app/a$a;
    .locals 1

    iget-object v0, p0, Lva/a;->c:Lmiuix/appcompat/app/i$b;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/app/i$b;->c(Landroid/view/View;)Lmiuix/appcompat/app/i$b;

    return-object p0
.end method

.method public e(Landroid/graphics/drawable/Drawable;)Landroidx/appcompat/app/a$a;
    .locals 1

    iget-object v0, p0, Lva/a;->c:Lmiuix/appcompat/app/i$b;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/app/i$b;->d(Landroid/graphics/drawable/Drawable;)Lmiuix/appcompat/app/i$b;

    return-object p0
.end method

.method public f(Ljava/lang/CharSequence;)Landroidx/appcompat/app/a$a;
    .locals 1

    iget-object v0, p0, Lva/a;->c:Lmiuix/appcompat/app/i$b;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/app/i$b;->f(Ljava/lang/CharSequence;)Lmiuix/appcompat/app/i$b;

    return-object p0
.end method

.method public g([Ljava/lang/CharSequence;[ZLandroid/content/DialogInterface$OnMultiChoiceClickListener;)Landroidx/appcompat/app/a$a;
    .locals 1

    iget-object v0, p0, Lva/a;->c:Lmiuix/appcompat/app/i$b;

    invoke-virtual {v0, p1, p2, p3}, Lmiuix/appcompat/app/i$b;->g([Ljava/lang/CharSequence;[ZLandroid/content/DialogInterface$OnMultiChoiceClickListener;)Lmiuix/appcompat/app/i$b;

    return-object p0
.end method

.method public h(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroidx/appcompat/app/a$a;
    .locals 1

    iget-object v0, p0, Lva/a;->c:Lmiuix/appcompat/app/i$b;

    invoke-virtual {v0, p1, p2}, Lmiuix/appcompat/app/i$b;->i(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/i$b;

    return-object p0
.end method

.method public i(Landroid/content/DialogInterface$OnKeyListener;)Landroidx/appcompat/app/a$a;
    .locals 1

    iget-object v0, p0, Lva/a;->c:Lmiuix/appcompat/app/i$b;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/app/i$b;->n(Landroid/content/DialogInterface$OnKeyListener;)Lmiuix/appcompat/app/i$b;

    return-object p0
.end method

.method public j(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroidx/appcompat/app/a$a;
    .locals 1

    iget-object v0, p0, Lva/a;->c:Lmiuix/appcompat/app/i$b;

    invoke-virtual {v0, p1, p2}, Lmiuix/appcompat/app/i$b;->p(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/i$b;

    return-object p0
.end method

.method public k(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Landroidx/appcompat/app/a$a;
    .locals 1

    iget-object v0, p0, Lva/a;->c:Lmiuix/appcompat/app/i$b;

    invoke-virtual {v0, p1, p2, p3}, Lmiuix/appcompat/app/i$b;->q(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/i$b;

    return-object p0
.end method

.method public l([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroidx/appcompat/app/a$a;
    .locals 1

    iget-object v0, p0, Lva/a;->c:Lmiuix/appcompat/app/i$b;

    invoke-virtual {v0, p1, p2, p3}, Lmiuix/appcompat/app/i$b;->r([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/i$b;

    return-object p0
.end method

.method public m(Ljava/lang/CharSequence;)Landroidx/appcompat/app/a$a;
    .locals 1

    iget-object v0, p0, Lva/a;->c:Lmiuix/appcompat/app/i$b;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/app/i$b;->t(Ljava/lang/CharSequence;)Lmiuix/appcompat/app/i$b;

    return-object p0
.end method

.method public n(Landroid/view/View;)Landroidx/appcompat/app/a$a;
    .locals 1

    iget-object v0, p0, Lva/a;->c:Lmiuix/appcompat/app/i$b;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/app/i$b;->v(Landroid/view/View;)Lmiuix/appcompat/app/i$b;

    return-object p0
.end method
