.class Lva/j;
.super Landroidx/preference/h;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lva/j$b;
    }
.end annotation


# static fields
.field private static final C:[I

.field private static final D:[I

.field private static final E:[I

.field private static final F:[I

.field private static final G:[I

.field private static final H:[I


# instance fields
.field private A:I

.field private B:I

.field private i:[Lva/j$b;

.field private j:Landroidx/recyclerview/widget/RecyclerView$i;

.field private k:I

.field private l:I

.field private m:I

.field private n:Landroidx/recyclerview/widget/RecyclerView;

.field private o:I

.field private p:I

.field private q:Z

.field private r:I

.field private s:Landroid/view/View;

.field private t:Z

.field private u:Landroid/view/View$OnTouchListener;

.field private v:Landroidx/recyclerview/widget/RecyclerView$s;

.field private w:Landroid/graphics/Paint;

.field private x:I

.field private y:I

.field private z:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v0, 0x5

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x10100a3

    aput v2, v0, v1

    const/4 v3, 0x1

    const v4, 0x10100a4

    aput v4, v0, v3

    const/4 v5, 0x2

    const v6, 0x10100a5

    aput v6, v0, v5

    const/4 v5, 0x3

    const v7, 0x10100a6

    aput v7, v0, v5

    sget v5, Lva/k;->l:I

    const/4 v8, 0x4

    aput v5, v0, v8

    sput-object v0, Lva/j;->C:[I

    invoke-static {v0}, Ljava/util/Arrays;->sort([I)V

    new-array v0, v3, [I

    aput v2, v0, v1

    sput-object v0, Lva/j;->D:[I

    new-array v0, v3, [I

    aput v4, v0, v1

    sput-object v0, Lva/j;->E:[I

    new-array v0, v3, [I

    aput v6, v0, v1

    sput-object v0, Lva/j;->F:[I

    new-array v0, v3, [I

    aput v7, v0, v1

    sput-object v0, Lva/j;->G:[I

    new-array v0, v3, [I

    aput v5, v0, v1

    sput-object v0, Lva/j;->H:[I

    return-void
.end method

.method public constructor <init>(Landroidx/preference/PreferenceGroup;)V
    .locals 2

    invoke-direct {p0, p1}, Landroidx/preference/h;-><init>(Landroidx/preference/PreferenceGroup;)V

    new-instance v0, Lva/j$a;

    invoke-direct {v0, p0}, Lva/j$a;-><init>(Lva/j;)V

    iput-object v0, p0, Lva/j;->j:Landroidx/recyclerview/widget/RecyclerView$i;

    const/4 v0, 0x0

    iput v0, p0, Lva/j;->o:I

    iput v0, p0, Lva/j;->p:I

    iput-boolean v0, p0, Lva/j;->q:Z

    const/4 v1, -0x1

    iput v1, p0, Lva/j;->r:I

    const/4 v1, 0x0

    iput-object v1, p0, Lva/j;->s:Landroid/view/View;

    iput-boolean v0, p0, Lva/j;->t:Z

    iput-object v1, p0, Lva/j;->u:Landroid/view/View$OnTouchListener;

    iput-object v1, p0, Lva/j;->v:Landroidx/recyclerview/widget/RecyclerView$s;

    invoke-virtual {p0}, Landroidx/preference/h;->f()I

    move-result v0

    new-array v0, v0, [Lva/j$b;

    iput-object v0, p0, Lva/j;->i:[Lva/j$b;

    invoke-virtual {p1}, Landroidx/preference/Preference;->i()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p0, p1}, Lva/j;->X(Landroid/content/Context;)V

    return-void
.end method

.method private L(Landroidx/preference/Preference;)Z
    .locals 2

    instance-of v0, p1, Lmiuix/preference/DropDownPreference;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    :cond_0
    instance-of v0, p1, Lva/c;

    if-eqz v0, :cond_1

    check-cast p1, Lva/c;

    invoke-interface {p1}, Lva/c;->a()Z

    move-result p1

    if-nez p1, :cond_1

    return v1

    :cond_1
    const/4 p1, 0x1

    return p1
.end method

.method static synthetic M(Lva/j;[Lva/j$b;)[Lva/j$b;
    .locals 0

    iput-object p1, p0, Lva/j;->i:[Lva/j$b;

    return-object p1
.end method

.method private O(Landroid/graphics/drawable/Drawable;ZZ)V
    .locals 8

    instance-of v0, p1, Lwa/a;

    if-eqz v0, :cond_0

    check-cast p1, Lwa/a;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lwa/a;->i(Z)V

    iget-object v2, p0, Lva/j;->w:Landroid/graphics/Paint;

    iget v3, p0, Lva/j;->x:I

    iget v4, p0, Lva/j;->y:I

    iget v5, p0, Lva/j;->z:I

    iget v6, p0, Lva/j;->A:I

    iget v7, p0, Lva/j;->B:I

    move-object v1, p1

    invoke-virtual/range {v1 .. v7}, Lwa/a;->g(Landroid/graphics/Paint;IIIII)V

    iget-object v0, p0, Lva/j;->n:Landroidx/recyclerview/widget/RecyclerView;

    invoke-static {v0}, Landroidx/appcompat/widget/s0;->b(Landroid/view/View;)Z

    move-result v0

    iget-object v1, p0, Lva/j;->n:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {p0, v1, v0}, Lva/j;->U(Landroidx/recyclerview/widget/RecyclerView;Z)Landroid/util/Pair;

    move-result-object v1

    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v2, v1, v0}, Lwa/a;->h(IIZ)V

    invoke-virtual {p1, p2, p3}, Lwa/a;->j(ZZ)V

    :cond_0
    return-void
.end method

.method private P(Lmiuix/preference/RadioButtonPreferenceCategory;)V
    .locals 4

    invoke-virtual {p1}, Landroidx/preference/PreferenceGroup;->N0()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    invoke-virtual {p1, v1}, Landroidx/preference/PreferenceGroup;->M0(I)Landroidx/preference/Preference;

    move-result-object v2

    instance-of v3, v2, Lmiuix/preference/RadioSetPreferenceCategory;

    if-eqz v3, :cond_0

    check-cast v2, Lmiuix/preference/RadioSetPreferenceCategory;

    invoke-direct {p0, v2}, Lva/j;->Q(Lmiuix/preference/RadioSetPreferenceCategory;)V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private Q(Lmiuix/preference/RadioSetPreferenceCategory;)V
    .locals 5

    invoke-virtual {p1}, Landroidx/preference/PreferenceGroup;->N0()I

    move-result v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_1

    invoke-virtual {p1, v2}, Landroidx/preference/PreferenceGroup;->M0(I)Landroidx/preference/Preference;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {p0, v3}, Landroidx/preference/h;->G(Landroidx/preference/Preference;)I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    iget-object v4, p0, Lva/j;->n:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v4, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    invoke-direct {p0, v1}, Lva/j;->S(Ljava/util/List;)V

    return-void
.end method

.method private R(Landroid/view/View;ZZ)V
    .locals 0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object p1

    invoke-direct {p0, p1, p2, p3}, Lva/j;->O(Landroid/graphics/drawable/Drawable;ZZ)V

    :cond_0
    return-void
.end method

.method private S(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_2

    const/4 v2, 0x1

    if-nez v1, :cond_0

    move v3, v2

    goto :goto_1

    :cond_0
    move v3, v0

    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    sub-int/2addr v4, v2

    if-ne v1, v4, :cond_1

    goto :goto_2

    :cond_1
    move v2, v0

    :goto_2
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    invoke-direct {p0, v4, v3, v2}, Lva/j;->R(Landroid/view/View;ZZ)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method private T(Landroidx/preference/PreferenceGroup;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/preference/PreferenceGroup;",
            ")",
            "Ljava/util/List<",
            "Landroidx/preference/Preference;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p1}, Landroidx/preference/PreferenceGroup;->N0()I

    move-result v2

    if-ge v1, v2, :cond_1

    invoke-virtual {p1, v1}, Landroidx/preference/PreferenceGroup;->M0(I)Landroidx/preference/Preference;

    move-result-object v2

    invoke-virtual {v2}, Landroidx/preference/Preference;->L()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method private W(Landroidx/preference/Preference;I)V
    .locals 5

    if-ltz p2, :cond_1

    iget-object v0, p0, Lva/j;->i:[Lva/j$b;

    array-length v1, v0

    if-ge p2, v1, :cond_1

    aget-object v1, v0, p2

    if-nez v1, :cond_0

    new-instance v1, Lva/j$b;

    invoke-direct {v1, p0}, Lva/j$b;-><init>(Lva/j;)V

    aput-object v1, v0, p2

    :cond_0
    iget-object v0, p0, Lva/j;->i:[Lva/j$b;

    aget-object v0, v0, p2

    iget-object v0, v0, Lva/j$b;->a:[I

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_7

    invoke-virtual {p1}, Landroidx/preference/Preference;->u()Landroidx/preference/PreferenceGroup;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-direct {p0, v0}, Lva/j;->T(Landroidx/preference/PreferenceGroup;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    return-void

    :cond_2
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-ne v1, v2, :cond_3

    sget-object v0, Lva/j;->D:[I

    goto :goto_1

    :cond_3
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroidx/preference/Preference;

    invoke-virtual {p1, v1}, Landroidx/preference/Preference;->d(Landroidx/preference/Preference;)I

    move-result v1

    if-nez v1, :cond_4

    sget-object v0, Lva/j;->E:[I

    const/4 v2, 0x2

    goto :goto_1

    :cond_4
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    sub-int/2addr v1, v2

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/preference/Preference;

    invoke-virtual {p1, v0}, Landroidx/preference/Preference;->d(Landroidx/preference/Preference;)I

    move-result v0

    if-nez v0, :cond_5

    sget-object v0, Lva/j;->G:[I

    const/4 v2, 0x4

    goto :goto_1

    :cond_5
    sget-object v0, Lva/j;->F:[I

    const/4 v2, 0x3

    :goto_1
    instance-of v1, p1, Landroidx/preference/PreferenceCategory;

    if-eqz v1, :cond_6

    check-cast p1, Landroidx/preference/PreferenceCategory;

    invoke-virtual {p1}, Landroidx/preference/Preference;->E()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_6

    sget-object p1, Lva/j;->H:[I

    array-length v1, p1

    array-length v4, v0

    add-int/2addr v1, v4

    new-array v1, v1, [I

    array-length v4, p1

    invoke-static {p1, v3, v1, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    array-length p1, p1

    array-length v4, v0

    invoke-static {v0, v3, v1, p1, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object v0, v1

    :cond_6
    iget-object p1, p0, Lva/j;->i:[Lva/j$b;

    aget-object v1, p1, p2

    iput-object v0, v1, Lva/j$b;->a:[I

    aget-object p1, p1, p2

    iput v2, p1, Lva/j$b;->b:I

    :cond_7
    return-void
.end method

.method private Y(Landroidx/preference/Preference;)Z
    .locals 1

    invoke-virtual {p1}, Landroidx/preference/Preference;->o()Landroid/content/Intent;

    move-result-object v0

    if-nez v0, :cond_2

    invoke-virtual {p1}, Landroidx/preference/Preference;->m()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    invoke-virtual {p1}, Landroidx/preference/Preference;->s()Landroidx/preference/Preference$e;

    move-result-object v0

    if-eqz v0, :cond_0

    instance-of v0, p1, Landroidx/preference/TwoStatePreference;

    if-eqz v0, :cond_2

    :cond_0
    instance-of p1, p1, Landroidx/preference/DialogPreference;

    if-eqz p1, :cond_1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method private c0(Landroidx/preference/Preference;Landroidx/preference/PreferenceViewHolder;)V
    .locals 5

    iget-object p2, p2, Landroidx/recyclerview/widget/RecyclerView$d0;->itemView:Landroid/view/View;

    invoke-direct {p0, p1}, Lva/j;->L(Landroidx/preference/Preference;)Z

    move-result v0

    const/4 v1, 0x1

    new-array v2, v1, [Landroid/view/View;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    invoke-static {v2}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object v2

    invoke-interface {v2}, Lmiuix/animation/e;->a()Lmiuix/animation/f;

    move-result-object v2

    invoke-interface {v2}, Lmiuix/animation/g;->n()V

    if-eqz v0, :cond_0

    new-array v2, v1, [Landroid/view/View;

    aput-object p2, v2, v3

    invoke-static {v2}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object v2

    invoke-interface {v2}, Lmiuix/animation/e;->c()Lmiuix/animation/j;

    move-result-object v2

    invoke-interface {v2}, Lmiuix/animation/g;->n()V

    :cond_0
    invoke-virtual {p1}, Landroidx/preference/Preference;->I()Z

    move-result v2

    if-eqz v2, :cond_1

    new-array v2, v1, [Landroid/view/View;

    aput-object p2, v2, v3

    invoke-static {v2}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object v2

    invoke-interface {v2}, Lmiuix/animation/e;->a()Lmiuix/animation/f;

    move-result-object v2

    sget-object v4, Lmiuix/animation/f$a;->a:Lmiuix/animation/f$a;

    invoke-interface {v2, v4}, Lmiuix/animation/f;->e(Lmiuix/animation/f$a;)Lmiuix/animation/f;

    move-result-object v2

    new-array v4, v3, [Lc9/a;

    invoke-interface {v2, p2, v4}, Lmiuix/animation/f;->x(Landroid/view/View;[Lc9/a;)V

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Landroidx/preference/Preference;->K()Z

    move-result p1

    if-eqz p1, :cond_1

    new-array p1, v1, [Landroid/view/View;

    aput-object p2, p1, v3

    invoke-static {p1}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object p1

    invoke-interface {p1}, Lmiuix/animation/e;->c()Lmiuix/animation/j;

    move-result-object p1

    const/high16 v0, 0x3f800000    # 1.0f

    new-array v1, v3, [Lmiuix/animation/j$b;

    invoke-interface {p1, v0, v1}, Lmiuix/animation/j;->G(F[Lmiuix/animation/j$b;)Lmiuix/animation/j;

    move-result-object p1

    new-array v0, v3, [Lc9/a;

    invoke-interface {p1, p2, v0}, Lmiuix/animation/j;->z(Landroid/view/View;[Lc9/a;)V

    :cond_1
    return-void
.end method

.method private g0(Landroid/view/View;)V
    .locals 3

    sget v0, Lva/n;->h:I

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {p1, v0, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    const/4 v0, 0x1

    new-array v0, v0, [Landroid/view/View;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-static {v0}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object v0

    invoke-interface {v0}, Lmiuix/animation/e;->d()Lmiuix/animation/c;

    move-result-object v0

    new-array v1, v1, [Lc9/a;

    const/4 v2, 0x3

    invoke-interface {v0, v2, v1}, Lmiuix/animation/c;->l(I[Lc9/a;)V

    iput-object p1, p0, Lva/j;->s:Landroid/view/View;

    return-void
.end method

.method private j0(Landroidx/preference/Preference;)V
    .locals 1

    if-eqz p1, :cond_2

    iget-object v0, p0, Lva/j;->n:Landroidx/recyclerview/widget/RecyclerView;

    if-eqz v0, :cond_2

    instance-of v0, p1, Lmiuix/preference/RadioButtonPreferenceCategory;

    if-eqz v0, :cond_0

    check-cast p1, Lmiuix/preference/RadioButtonPreferenceCategory;

    invoke-direct {p0, p1}, Lva/j;->P(Lmiuix/preference/RadioButtonPreferenceCategory;)V

    goto :goto_0

    :cond_0
    instance-of v0, p1, Lmiuix/preference/RadioSetPreferenceCategory;

    if-eqz v0, :cond_1

    check-cast p1, Lmiuix/preference/RadioSetPreferenceCategory;

    invoke-direct {p0, p1}, Lva/j;->Q(Lmiuix/preference/RadioSetPreferenceCategory;)V

    goto :goto_0

    :cond_1
    instance-of p1, p1, Lmiuix/preference/RadioButtonPreference;

    :cond_2
    :goto_0
    return-void
.end method


# virtual methods
.method public I(Landroidx/preference/PreferenceViewHolder;I)V
    .locals 8

    invoke-super {p0, p1, p2}, Landroidx/preference/h;->I(Landroidx/preference/PreferenceViewHolder;I)V

    iget-object v0, p1, Landroidx/recyclerview/widget/RecyclerView$d0;->itemView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmiuix/view/b;->b(Landroid/view/View;Z)V

    invoke-virtual {p0, p2}, Landroidx/preference/h;->F(I)Landroidx/preference/Preference;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lva/j;->W(Landroidx/preference/Preference;I)V

    iget-object v2, p0, Lva/j;->i:[Lva/j$b;

    aget-object v2, v2, p2

    iget-object v2, v2, Lva/j$b;->a:[I

    iget-object v3, p1, Landroidx/recyclerview/widget/RecyclerView$d0;->itemView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    instance-of v4, v3, Landroid/graphics/drawable/LevelListDrawable;

    if-eqz v4, :cond_2

    instance-of v4, v0, Lmiuix/preference/RadioButtonPreference;

    if-nez v4, :cond_0

    instance-of v4, v0, Landroidx/preference/PreferenceCategory;

    if-eqz v4, :cond_2

    :cond_0
    iget-boolean v4, p0, Lva/j;->q:Z

    if-eqz v4, :cond_1

    iget v4, p0, Lva/j;->o:I

    goto :goto_0

    :cond_1
    move v4, v1

    :goto_0
    invoke-virtual {v3, v4}, Landroid/graphics/drawable/Drawable;->setLevel(I)Z

    new-instance v4, Lwa/a;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getCurrent()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-direct {v4, v3}, Lwa/a;-><init>(Landroid/graphics/drawable/Drawable;)V

    iget-object v3, p1, Landroidx/recyclerview/widget/RecyclerView$d0;->itemView:Landroid/view/View;

    invoke-virtual {v3, v4}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    move-object v3, v4

    :cond_2
    nop

    instance-of v4, v3, Landroid/graphics/drawable/StateListDrawable;

    if-eqz v4, :cond_3

    move-object v4, v3

    check-cast v4, Landroid/graphics/drawable/StateListDrawable;

    sget-object v5, Lva/j;->C:[I

    invoke-static {v4, v5}, Lha/b;->c(Landroid/graphics/drawable/StateListDrawable;[I)Z

    move-result v4

    if-eqz v4, :cond_3

    new-instance v4, Lwa/a;

    invoke-direct {v4, v3}, Lwa/a;-><init>(Landroid/graphics/drawable/Drawable;)V

    iget-object v3, p1, Landroidx/recyclerview/widget/RecyclerView$d0;->itemView:Landroid/view/View;

    invoke-virtual {v3, v4}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    move-object v3, v4

    :cond_3
    nop

    instance-of v4, v3, Lwa/a;

    if-eqz v4, :cond_11

    check-cast v3, Lwa/a;

    if-eqz v2, :cond_4

    invoke-virtual {v3, v2}, Lha/b;->e([I)Z

    :cond_4
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {v3, v2}, Lf/a;->getPadding(Landroid/graphics/Rect;)Z

    move-result v4

    if-eqz v4, :cond_10

    iget v4, v2, Landroid/graphics/Rect;->left:I

    iget v5, v2, Landroid/graphics/Rect;->right:I

    iget-object v6, p0, Lva/j;->n:Landroidx/recyclerview/widget/RecyclerView;

    invoke-static {v6}, Landroidx/appcompat/widget/s0;->b(Landroid/view/View;)Z

    move-result v6

    if-eqz v6, :cond_5

    move v6, v4

    goto :goto_1

    :cond_5
    move v6, v5

    :goto_1
    iput v6, v2, Landroid/graphics/Rect;->right:I

    iget-object v6, p0, Lva/j;->n:Landroidx/recyclerview/widget/RecyclerView;

    invoke-static {v6}, Landroidx/appcompat/widget/s0;->b(Landroid/view/View;)Z

    move-result v6

    if-eqz v6, :cond_6

    move v4, v5

    :cond_6
    iput v4, v2, Landroid/graphics/Rect;->left:I

    invoke-virtual {v0}, Landroidx/preference/Preference;->u()Landroidx/preference/PreferenceGroup;

    move-result-object v4

    instance-of v4, v4, Lmiuix/preference/RadioSetPreferenceCategory;

    if-eqz v4, :cond_c

    iget-object v4, p1, Landroidx/recyclerview/widget/RecyclerView$d0;->itemView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    instance-of v5, v4, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v5, :cond_7

    check-cast v4, Landroid/view/ViewGroup$MarginLayoutParams;

    goto :goto_2

    :cond_7
    new-instance v5, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-direct {v5, v4}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    move-object v4, v5

    :goto_2
    iget-object v5, p0, Lva/j;->n:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v5}, Landroid/view/ViewGroup;->getScrollBarSize()I

    move-result v5

    mul-int/lit8 v5, v5, 0x2

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup$MarginLayoutParams;->setMarginEnd(I)V

    iget-object v5, p1, Landroidx/recyclerview/widget/RecyclerView$d0;->itemView:Landroid/view/View;

    invoke-virtual {v5, v4}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v0}, Landroidx/preference/Preference;->u()Landroidx/preference/PreferenceGroup;

    move-result-object v4

    check-cast v4, Lmiuix/preference/RadioSetPreferenceCategory;

    invoke-virtual {v3, v1}, Lwa/a;->i(Z)V

    invoke-virtual {v4}, Lmiuix/preference/RadioSetPreferenceCategory;->isChecked()Z

    move-result v4

    if-eqz v4, :cond_8

    iget v4, p0, Lva/j;->l:I

    goto :goto_3

    :cond_8
    iget v4, p0, Lva/j;->m:I

    :goto_3
    sget-object v5, Landroid/graphics/PorterDuff$Mode;->SRC_OVER:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v3, v4, v5}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    iget-object v4, p0, Lva/j;->n:Landroidx/recyclerview/widget/RecyclerView;

    if-eqz v4, :cond_d

    instance-of v5, v0, Lmiuix/preference/RadioButtonPreference;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getScrollBarSize()I

    move-result v4

    iget-object v6, p0, Lva/j;->n:Landroidx/recyclerview/widget/RecyclerView;

    invoke-static {v6}, Landroidx/appcompat/widget/s0;->b(Landroid/view/View;)Z

    move-result v6

    if-eqz v6, :cond_a

    iget v6, v2, Landroid/graphics/Rect;->right:I

    if-eqz v5, :cond_9

    move v5, v1

    goto :goto_4

    :cond_9
    iget v5, p0, Lva/j;->k:I

    :goto_4
    add-int/2addr v6, v5

    iput v6, v2, Landroid/graphics/Rect;->right:I

    iget v5, v2, Landroid/graphics/Rect;->left:I

    mul-int/lit8 v4, v4, 0x3

    sub-int/2addr v5, v4

    iput v5, v2, Landroid/graphics/Rect;->left:I

    goto :goto_6

    :cond_a
    iget v6, v2, Landroid/graphics/Rect;->left:I

    if-eqz v5, :cond_b

    move v5, v1

    goto :goto_5

    :cond_b
    iget v5, p0, Lva/j;->k:I

    :goto_5
    add-int/2addr v6, v5

    iput v6, v2, Landroid/graphics/Rect;->left:I

    iget v5, v2, Landroid/graphics/Rect;->right:I

    mul-int/lit8 v4, v4, 0x3

    sub-int/2addr v5, v4

    iput v5, v2, Landroid/graphics/Rect;->right:I

    goto :goto_6

    :cond_c
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lf/a;->setColorFilter(Landroid/graphics/ColorFilter;)V

    :cond_d
    :goto_6
    iget v4, v2, Landroid/graphics/Rect;->left:I

    iget-boolean v5, p0, Lva/j;->q:Z

    if-eqz v5, :cond_e

    iget v6, p0, Lva/j;->p:I

    goto :goto_7

    :cond_e
    move v6, v1

    :goto_7
    add-int/2addr v4, v6

    iget v6, v2, Landroid/graphics/Rect;->right:I

    if-eqz v5, :cond_f

    iget v5, p0, Lva/j;->p:I

    goto :goto_8

    :cond_f
    move v5, v1

    :goto_8
    add-int/2addr v6, v5

    iget-object v5, p1, Landroidx/recyclerview/widget/RecyclerView$d0;->itemView:Landroid/view/View;

    iget v7, v2, Landroid/graphics/Rect;->top:I

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v5, v4, v7, v6, v2}, Landroid/view/View;->setPadding(IIII)V

    :cond_10
    instance-of v2, v0, Lmiuix/preference/RadioButtonPreference;

    if-eqz v2, :cond_11

    move-object v2, v0

    check-cast v2, Lmiuix/preference/RadioButtonPreference;

    invoke-virtual {v2}, Landroidx/preference/TwoStatePreference;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_11

    const/4 v2, 0x1

    new-array v2, v2, [I

    const v4, 0x10100a0

    aput v4, v2, v1

    invoke-virtual {v3, v2}, Lha/b;->e([I)Z

    :cond_11
    iget-object v2, p1, Landroidx/recyclerview/widget/RecyclerView$d0;->itemView:Landroid/view/View;

    sget v3, Lva/n;->a:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_13

    invoke-direct {p0, v0}, Lva/j;->Y(Landroidx/preference/Preference;)Z

    move-result v3

    if-eqz v3, :cond_12

    goto :goto_9

    :cond_12
    const/16 v1, 0x8

    :goto_9
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_13
    invoke-direct {p0, v0, p1}, Lva/j;->c0(Landroidx/preference/Preference;Landroidx/preference/PreferenceViewHolder;)V

    iget-object v0, p1, Landroidx/recyclerview/widget/RecyclerView$d0;->itemView:Landroid/view/View;

    const v1, 0x1020016

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lr9/d;->b(Landroid/widget/TextView;)V

    invoke-virtual {p0, p1, p2}, Lva/j;->N(Landroidx/preference/PreferenceViewHolder;I)V

    return-void
.end method

.method public N(Landroidx/preference/PreferenceViewHolder;I)V
    .locals 1

    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView$d0;->itemView:Landroid/view/View;

    iget v0, p0, Lva/j;->r:I

    if-ne p2, v0, :cond_2

    iget-boolean p2, p0, Lva/j;->t:Z

    if-nez p2, :cond_1

    sget-object p2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    sget v0, Lva/n;->h:I

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_0

    return-void

    :cond_0
    invoke-direct {p0, p1}, Lva/j;->g0(Landroid/view/View;)V

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    iput-boolean p1, p0, Lva/j;->t:Z

    goto :goto_0

    :cond_2
    sget-object p2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    sget v0, Lva/n;->h:I

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_3

    invoke-virtual {p0, p1}, Lva/j;->i0(Landroid/view/View;)V

    :cond_3
    :goto_0
    return-void
.end method

.method public U(Landroidx/recyclerview/widget/RecyclerView;Z)Landroid/util/Pair;
    .locals 1

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getScrollBarSize()I

    move-result v0

    if-eqz p2, :cond_0

    mul-int/lit8 v0, v0, 0x3

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getWidth()I

    move-result p1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getWidth()I

    move-result p1

    mul-int/lit8 v0, v0, 0x3

    sub-int/2addr p1, v0

    move v0, p2

    :goto_0
    new-instance p2, Landroid/util/Pair;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-direct {p2, v0, p1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object p2
.end method

.method V(I)I
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    iget p1, p1, Lva/j$b;->b:I

    goto/32 :goto_3

    nop

    :goto_1
    aget-object p1, v0, p1

    goto/32 :goto_0

    nop

    :goto_2
    iget-object v0, p0, Lva/j;->i:[Lva/j$b;

    goto/32 :goto_1

    nop

    :goto_3
    return p1
.end method

.method public X(Landroid/content/Context;)V
    .locals 1

    sget v0, Lva/k;->j:I

    invoke-static {p1, v0}, Lia/d;->g(Landroid/content/Context;I)I

    move-result v0

    iput v0, p0, Lva/j;->k:I

    sget v0, Lva/k;->a:I

    invoke-static {p1, v0}, Lia/d;->e(Landroid/content/Context;I)I

    move-result v0

    iput v0, p0, Lva/j;->l:I

    sget v0, Lva/k;->b:I

    invoke-static {p1, v0}, Lia/d;->e(Landroid/content/Context;I)I

    move-result p1

    iput p1, p0, Lva/j;->m:I

    return-void
.end method

.method public Z()Z
    .locals 2

    iget v0, p0, Lva/j;->r:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public a0(Landroidx/preference/PreferenceViewHolder;)V
    .locals 0

    invoke-super {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$g;->x(Landroidx/recyclerview/widget/RecyclerView$d0;)V

    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView$d0;->itemView:Landroid/view/View;

    invoke-virtual {p0, p1}, Lva/j;->i0(Landroid/view/View;)V

    return-void
.end method

.method public b(Landroidx/preference/Preference;)V
    .locals 2

    invoke-super {p0, p1}, Landroidx/preference/h;->b(Landroidx/preference/Preference;)V

    invoke-virtual {p1}, Landroidx/preference/Preference;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p1}, Landroidx/preference/Preference;->A()Landroidx/preference/j;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroidx/preference/j;->a(Ljava/lang/CharSequence;)Landroidx/preference/Preference;

    move-result-object v0

    if-eqz v0, :cond_2

    instance-of v1, p1, Landroidx/preference/PreferenceCategory;

    if-eqz v1, :cond_1

    instance-of v1, v0, Landroidx/preference/TwoStatePreference;

    if-eqz v1, :cond_0

    check-cast v0, Landroidx/preference/TwoStatePreference;

    invoke-virtual {v0}, Landroidx/preference/TwoStatePreference;->isChecked()Z

    move-result v0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Landroidx/preference/Preference;->I()Z

    move-result v0

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Landroidx/preference/Preference;->I()Z

    move-result v0

    :goto_0
    invoke-virtual {p1, v0}, Landroidx/preference/Preference;->B0(Z)V

    :cond_2
    return-void
.end method

.method public b0(Landroidx/preference/PreferenceViewHolder;)V
    .locals 0

    invoke-super {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$g;->y(Landroidx/recyclerview/widget/RecyclerView$d0;)V

    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView$d0;->itemView:Landroid/view/View;

    invoke-virtual {p0, p1}, Lva/j;->i0(Landroid/view/View;)V

    return-void
.end method

.method public c(Landroidx/preference/Preference;)V
    .locals 1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroidx/preference/Preference;->L()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lva/j;->j0(Landroidx/preference/Preference;)V

    :cond_0
    invoke-super {p0, p1}, Landroidx/preference/h;->c(Landroidx/preference/Preference;)V

    return-void
.end method

.method public d0(Landroid/graphics/Paint;IIIII)V
    .locals 0

    iput-object p1, p0, Lva/j;->w:Landroid/graphics/Paint;

    iput p2, p0, Lva/j;->x:I

    iput p3, p0, Lva/j;->y:I

    iput p4, p0, Lva/j;->z:I

    iput p5, p0, Lva/j;->A:I

    iput p6, p0, Lva/j;->B:I

    return-void
.end method

.method protected e0(IIZ)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lva/j;->f0(IIZZ)V

    return-void
.end method

.method protected f0(IIZZ)V
    .locals 0

    if-nez p4, :cond_0

    invoke-static {p1}, Lr9/e;->b(I)Z

    move-result p4

    if-eqz p4, :cond_1

    iget p4, p0, Lva/j;->o:I

    if-eq p4, p1, :cond_1

    :cond_0
    iput p1, p0, Lva/j;->o:I

    iput p2, p0, Lva/j;->p:I

    iput-boolean p3, p0, Lva/j;->q:Z

    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$g;->k()V

    :cond_1
    return-void
.end method

.method public h0()V
    .locals 1

    iget-object v0, p0, Lva/j;->s:Landroid/view/View;

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lva/j;->i0(Landroid/view/View;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lva/j;->t:Z

    :cond_0
    return-void
.end method

.method public i0(Landroid/view/View;)V
    .locals 3

    invoke-virtual {p0}, Lva/j;->Z()Z

    move-result v0

    if-eqz v0, :cond_2

    if-eqz p1, :cond_2

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    sget v1, Lva/n;->h:I

    invoke-virtual {p1, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/view/View;

    const/4 v2, 0x0

    aput-object p1, v0, v2

    invoke-static {v0}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object v0

    invoke-interface {v0}, Lmiuix/animation/e;->d()Lmiuix/animation/c;

    move-result-object v0

    invoke-interface {v0}, Lmiuix/animation/c;->u()V

    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {p1, v1, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    iget-object v0, p0, Lva/j;->s:Landroid/view/View;

    const/4 v1, 0x0

    if-ne v0, p1, :cond_1

    iput-object v1, p0, Lva/j;->s:Landroid/view/View;

    :cond_1
    const/4 p1, -0x1

    iput p1, p0, Lva/j;->r:I

    iget-object p1, p0, Lva/j;->n:Landroidx/recyclerview/widget/RecyclerView;

    if-eqz p1, :cond_2

    iget-object v0, p0, Lva/j;->v:Landroidx/recyclerview/widget/RecyclerView$s;

    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->Y0(Landroidx/recyclerview/widget/RecyclerView$s;)V

    iget-object p1, p0, Lva/j;->n:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iput-object v1, p0, Lva/j;->v:Landroidx/recyclerview/widget/RecyclerView$s;

    iput-object v1, p0, Lva/j;->u:Landroid/view/View$OnTouchListener;

    :cond_2
    :goto_0
    return-void
.end method

.method public q(Landroidx/recyclerview/widget/RecyclerView;)V
    .locals 1

    invoke-super {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$g;->q(Landroidx/recyclerview/widget/RecyclerView;)V

    iget-object v0, p0, Lva/j;->j:Landroidx/recyclerview/widget/RecyclerView$i;

    invoke-virtual {p0, v0}, Landroidx/recyclerview/widget/RecyclerView$g;->z(Landroidx/recyclerview/widget/RecyclerView$i;)V

    iput-object p1, p0, Lva/j;->n:Landroidx/recyclerview/widget/RecyclerView;

    return-void
.end method

.method public bridge synthetic r(Landroidx/recyclerview/widget/RecyclerView$d0;I)V
    .locals 0

    check-cast p1, Landroidx/preference/PreferenceViewHolder;

    invoke-virtual {p0, p1, p2}, Lva/j;->I(Landroidx/preference/PreferenceViewHolder;I)V

    return-void
.end method

.method public u(Landroidx/recyclerview/widget/RecyclerView;)V
    .locals 0

    invoke-super {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$g;->u(Landroidx/recyclerview/widget/RecyclerView;)V

    iget-object p1, p0, Lva/j;->j:Landroidx/recyclerview/widget/RecyclerView$i;

    invoke-virtual {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$g;->B(Landroidx/recyclerview/widget/RecyclerView$i;)V

    const/4 p1, 0x0

    iput-object p1, p0, Lva/j;->n:Landroidx/recyclerview/widget/RecyclerView;

    return-void
.end method

.method public bridge synthetic x(Landroidx/recyclerview/widget/RecyclerView$d0;)V
    .locals 0

    check-cast p1, Landroidx/preference/PreferenceViewHolder;

    invoke-virtual {p0, p1}, Lva/j;->a0(Landroidx/preference/PreferenceViewHolder;)V

    return-void
.end method

.method public bridge synthetic y(Landroidx/recyclerview/widget/RecyclerView$d0;)V
    .locals 0

    check-cast p1, Landroidx/preference/PreferenceViewHolder;

    invoke-virtual {p0, p1}, Lva/j;->b0(Landroidx/preference/PreferenceViewHolder;)V

    return-void
.end method
