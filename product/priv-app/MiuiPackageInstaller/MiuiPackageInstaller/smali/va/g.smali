.class Lva/g;
.super Ljava/lang/Object;


# instance fields
.field private a:Lva/d;

.field private b:Landroidx/preference/f;


# direct methods
.method public constructor <init>(Lva/d;Landroidx/preference/f;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lva/g;->a:Lva/d;

    iput-object p2, p0, Lva/g;->b:Landroidx/preference/f;

    return-void
.end method

.method private b(Landroid/app/Dialog;)V
    .locals 1

    invoke-virtual {p1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object p1

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Landroid/view/Window;->setSoftInputMode(I)V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5

    iget-object p1, p0, Lva/g;->b:Landroidx/preference/f;

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->w()Landroid/content/Context;

    move-result-object p1

    iget-object v0, p0, Lva/g;->b:Landroidx/preference/f;

    invoke-virtual {v0}, Landroidx/preference/f;->V1()Landroidx/preference/DialogPreference;

    move-result-object v0

    new-instance v1, Lmiuix/appcompat/app/i$b;

    invoke-direct {v1, p1}, Lmiuix/appcompat/app/i$b;-><init>(Landroid/content/Context;)V

    new-instance v2, Lva/a;

    invoke-direct {v2, p1, v1}, Lva/a;-><init>(Landroid/content/Context;Lmiuix/appcompat/app/i$b;)V

    invoke-virtual {v0}, Landroidx/preference/DialogPreference;->K0()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Lva/a;->m(Ljava/lang/CharSequence;)Landroidx/appcompat/app/a$a;

    invoke-virtual {v0}, Landroidx/preference/DialogPreference;->H0()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Lva/a;->e(Landroid/graphics/drawable/Drawable;)Landroidx/appcompat/app/a$a;

    invoke-virtual {v0}, Landroidx/preference/DialogPreference;->M0()Ljava/lang/CharSequence;

    move-result-object v3

    iget-object v4, p0, Lva/g;->b:Landroidx/preference/f;

    invoke-virtual {v2, v3, v4}, Lva/a;->j(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroidx/appcompat/app/a$a;

    invoke-virtual {v0}, Landroidx/preference/DialogPreference;->L0()Ljava/lang/CharSequence;

    move-result-object v3

    iget-object v4, p0, Lva/g;->b:Landroidx/preference/f;

    invoke-virtual {v2, v3, v4}, Lva/a;->h(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroidx/appcompat/app/a$a;

    iget-object v3, p0, Lva/g;->a:Lva/d;

    invoke-interface {v3, p1}, Lva/d;->b(Landroid/content/Context;)Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lva/g;->a:Lva/d;

    invoke-interface {v0, p1}, Lva/d;->d(Landroid/view/View;)V

    invoke-virtual {v2, p1}, Lva/a;->n(Landroid/view/View;)Landroidx/appcompat/app/a$a;

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Landroidx/preference/DialogPreference;->J0()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v2, p1}, Lva/a;->f(Ljava/lang/CharSequence;)Landroidx/appcompat/app/a$a;

    :goto_0
    iget-object p1, p0, Lva/g;->a:Lva/d;

    invoke-interface {p1, v1}, Lva/d;->a(Lmiuix/appcompat/app/i$b;)V

    invoke-virtual {v1}, Lmiuix/appcompat/app/i$b;->a()Lmiuix/appcompat/app/i;

    move-result-object p1

    iget-object v0, p0, Lva/g;->a:Lva/d;

    invoke-interface {v0}, Lva/d;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, p1}, Lva/g;->b(Landroid/app/Dialog;)V

    :cond_1
    return-object p1
.end method
