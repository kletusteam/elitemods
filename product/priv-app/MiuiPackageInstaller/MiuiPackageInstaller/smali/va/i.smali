.class public abstract Lva/i;
.super Landroidx/preference/g;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lva/i$b;,
        Lva/i$c;
    }
.end annotation


# instance fields
.field private n0:Lva/j;

.field private o0:Lva/i$b;

.field private p0:Z

.field private q0:Z

.field private r0:I

.field private s0:I

.field private t0:I

.field private u0:Z

.field private v0:Z

.field private w0:I

.field private x0:I

.field private y0:I


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Landroidx/preference/g;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lva/i;->p0:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lva/i;->q0:Z

    const/4 v1, -0x1

    iput v1, p0, Lva/i;->r0:I

    iput v0, p0, Lva/i;->s0:I

    return-void
.end method

.method static synthetic a2(Lva/i;)Z
    .locals 0

    iget-boolean p0, p0, Lva/i;->u0:Z

    return p0
.end method

.method static synthetic b2(Lva/i;)I
    .locals 0

    iget p0, p0, Lva/i;->t0:I

    return p0
.end method

.method static synthetic c2(Lva/i;)Lva/j;
    .locals 0

    iget-object p0, p0, Lva/i;->n0:Lva/j;

    return-object p0
.end method

.method static synthetic d2(Lva/i;)Z
    .locals 0

    iget-boolean p0, p0, Lva/i;->p0:Z

    return p0
.end method

.method private f2()Z
    .locals 1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->p()Landroidx/fragment/app/e;

    move-result-object v0

    invoke-static {v0}, Lia/e;->d(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lia/e;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method private g2()V
    .locals 4

    iget-object v0, p0, Lva/i;->n0:Lva/j;

    if-eqz v0, :cond_0

    iget v1, p0, Lva/i;->s0:I

    iget v2, p0, Lva/i;->t0:I

    iget-boolean v3, p0, Lva/i;->u0:Z

    invoke-virtual {v0, v1, v2, v3}, Lva/j;->e0(IIZ)V

    :cond_0
    return-void
.end method


# virtual methods
.method public M0()V
    .locals 0

    invoke-super {p0}, Landroidx/preference/g;->M0()V

    invoke-virtual {p0}, Lva/i;->i2()V

    return-void
.end method

.method protected final O1(Landroidx/preference/PreferenceScreen;)Landroidx/recyclerview/widget/RecyclerView$g;
    .locals 8

    new-instance v0, Lva/j;

    invoke-direct {v0, p1}, Lva/j;-><init>(Landroidx/preference/PreferenceGroup;)V

    iput-object v0, p0, Lva/i;->n0:Lva/j;

    iget p1, p0, Lva/i;->s0:I

    iget v1, p0, Lva/i;->t0:I

    iget-boolean v2, p0, Lva/i;->u0:Z

    invoke-virtual {v0, p1, v1, v2}, Lva/j;->e0(IIZ)V

    iget-object p1, p0, Lva/i;->n0:Lva/j;

    invoke-virtual {p1}, Landroidx/preference/h;->f()I

    move-result p1

    const/4 v0, 0x1

    if-ge p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lva/i;->p0:Z

    iget-object v1, p0, Lva/i;->n0:Lva/j;

    iget-object p1, p0, Lva/i;->o0:Lva/i$b;

    invoke-static {p1}, Lva/i$b;->j(Lva/i$b;)Landroid/graphics/Paint;

    move-result-object v2

    iget-object p1, p0, Lva/i;->o0:Lva/i$b;

    invoke-static {p1}, Lva/i$b;->k(Lva/i$b;)I

    move-result v3

    iget-object p1, p0, Lva/i;->o0:Lva/i$b;

    invoke-static {p1}, Lva/i$b;->l(Lva/i$b;)I

    move-result v4

    iget-object p1, p0, Lva/i;->o0:Lva/i$b;

    invoke-static {p1}, Lva/i$b;->m(Lva/i$b;)I

    move-result v5

    iget-object p1, p0, Lva/i;->o0:Lva/i$b;

    invoke-static {p1}, Lva/i$b;->n(Lva/i$b;)I

    move-result v6

    iget-object p1, p0, Lva/i;->o0:Lva/i$b;

    invoke-static {p1}, Lva/i$b;->o(Lva/i$b;)I

    move-result v7

    invoke-virtual/range {v1 .. v7}, Lva/j;->d0(Landroid/graphics/Paint;IIIII)V

    iget-object p1, p0, Lva/i;->n0:Lva/j;

    return-object p1
.end method

.method public R1(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroidx/recyclerview/widget/RecyclerView;
    .locals 2

    sget p3, Lva/o;->c:I

    const/4 v0, 0x0

    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView;

    instance-of p3, p1, Lmiuix/recyclerview/widget/RecyclerView;

    if-eqz p3, :cond_0

    move-object p3, p1

    check-cast p3, Lmiuix/recyclerview/widget/RecyclerView;

    invoke-virtual {p3, v0}, Landroidx/recyclerview/widget/SpringRecyclerView;->setSpringEnabled(Z)V

    :cond_0
    invoke-virtual {p0}, Landroidx/preference/g;->P1()Landroidx/recyclerview/widget/RecyclerView$o;

    move-result-object p3

    invoke-virtual {p1, p3}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$o;)V

    new-instance p3, Lva/i$b;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p3, p0, v0, v1}, Lva/i$b;-><init>(Lva/i;Landroid/content/Context;Lva/h;)V

    iput-object p3, p0, Lva/i;->o0:Lva/i$b;

    invoke-virtual {p1, p3}, Landroidx/recyclerview/widget/RecyclerView;->h(Landroidx/recyclerview/widget/RecyclerView$n;)V

    instance-of p3, p2, Lmiuix/springback/view/SpringBackLayout;

    if-eqz p3, :cond_1

    check-cast p2, Lmiuix/springback/view/SpringBackLayout;

    invoke-virtual {p2, p1}, Lmiuix/springback/view/SpringBackLayout;->setTarget(Landroid/view/View;)V

    :cond_1
    return-object p1
.end method

.method public e2()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public h(Landroidx/preference/Preference;)V
    .locals 3

    invoke-virtual {p0}, Landroidx/preference/g;->J1()Landroidx/fragment/app/Fragment;

    move-result-object v0

    instance-of v0, v0, Landroidx/preference/g$d;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroidx/preference/g;->J1()Landroidx/fragment/app/Fragment;

    move-result-object v0

    check-cast v0, Landroidx/preference/g$d;

    invoke-interface {v0, p0, p1}, Landroidx/preference/g$d;->a(Landroidx/preference/g;Landroidx/preference/Preference;)Z

    move-result v0

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_0
    if-nez v0, :cond_1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->p()Landroidx/fragment/app/e;

    move-result-object v2

    instance-of v2, v2, Landroidx/preference/g$d;

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->p()Landroidx/fragment/app/e;

    move-result-object v0

    check-cast v0, Landroidx/preference/g$d;

    invoke-interface {v0, p0, p1}, Landroidx/preference/g$d;->a(Landroidx/preference/g;Landroidx/preference/Preference;)Z

    move-result v0

    :cond_1
    if-eqz v0, :cond_2

    return-void

    :cond_2
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->C()Landroidx/fragment/app/m;

    move-result-object v0

    const-string v2, "androidx.preference.PreferenceFragment.DIALOG"

    invoke-virtual {v0, v2}, Landroidx/fragment/app/m;->h0(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_3

    return-void

    :cond_3
    instance-of v0, p1, Landroidx/preference/EditTextPreference;

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Landroidx/preference/Preference;->p()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lva/b;->g2(Ljava/lang/String;)Lva/b;

    move-result-object p1

    goto :goto_1

    :cond_4
    instance-of v0, p1, Landroidx/preference/ListPreference;

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Landroidx/preference/Preference;->p()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lva/e;->g2(Ljava/lang/String;)Lva/e;

    move-result-object p1

    goto :goto_1

    :cond_5
    instance-of v0, p1, Landroidx/preference/MultiSelectListPreference;

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Landroidx/preference/Preference;->p()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lva/f;->g2(Ljava/lang/String;)Lva/f;

    move-result-object p1

    :goto_1
    invoke-virtual {p1, p0, v1}, Landroidx/fragment/app/Fragment;->D1(Landroidx/fragment/app/Fragment;I)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->C()Landroidx/fragment/app/m;

    move-result-object v0

    invoke-virtual {p1, v0, v2}, Landroidx/fragment/app/d;->U1(Landroidx/fragment/app/m;Ljava/lang/String;)V

    return-void

    :cond_6
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot display dialog for an unknown Preference type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ". Make sure to implement onPreferenceDisplayDialog() to handle displaying a custom dialog for this Preference."

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method h2(IZ)V
    .locals 1

    goto/32 :goto_8

    nop

    :goto_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->w()Landroid/content/Context;

    move-result-object v0

    goto/32 :goto_2

    nop

    :goto_1
    return-void

    :goto_2
    invoke-static {v0, p1}, Lxa/a;->a(Landroid/content/Context;I)I

    move-result p1

    goto/32 :goto_6

    nop

    :goto_3
    iget v0, p0, Lva/i;->s0:I

    goto/32 :goto_9

    nop

    :goto_4
    if-nez v0, :cond_0

    goto/32 :goto_b

    :cond_0
    goto/32 :goto_3

    nop

    :goto_5
    iput p1, p0, Lva/i;->s0:I

    goto/32 :goto_0

    nop

    :goto_6
    iput p1, p0, Lva/i;->t0:I

    goto/32 :goto_7

    nop

    :goto_7
    if-nez p2, :cond_1

    goto/32 :goto_b

    :cond_1
    goto/32 :goto_a

    nop

    :goto_8
    invoke-static {p1}, Lr9/e;->b(I)Z

    move-result v0

    goto/32 :goto_4

    nop

    :goto_9
    if-ne v0, p1, :cond_2

    goto/32 :goto_b

    :cond_2
    goto/32 :goto_5

    nop

    :goto_a
    invoke-direct {p0}, Lva/i;->g2()V

    :goto_b
    goto/32 :goto_1

    nop
.end method

.method public i2()V
    .locals 1

    iget-object v0, p0, Lva/i;->n0:Lva/j;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lva/j;->h0()V

    :cond_0
    return-void
.end method

.method public j(Landroidx/preference/Preference;)Z
    .locals 3

    iget-boolean v0, p0, Lva/i;->q0:Z

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Landroidx/preference/Preference;->t()I

    move-result v0

    iget v1, p0, Lva/i;->r0:I

    if-eq v0, v1, :cond_1

    if-ltz v1, :cond_0

    invoke-virtual {p0}, Landroidx/preference/g;->K1()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v1

    iget v2, p0, Lva/i;->r0:I

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setSelected(Z)V

    :cond_0
    invoke-virtual {p0}, Landroidx/preference/g;->K1()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/view/View;->setSelected(Z)V

    iput v0, p0, Lva/i;->r0:I

    :cond_1
    invoke-super {p0, p1}, Landroidx/preference/g;->j(Landroidx/preference/Preference;)Z

    move-result p1

    return p1
.end method

.method public o0(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Landroidx/preference/g;->o0(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lva/i;->e2()Z

    move-result p1

    iput-boolean p1, p0, Lva/i;->v0:Z

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->N()Landroid/content/res/Resources;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object p1

    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lva/i;->w0:I

    iget v0, p1, Landroid/content/res/Configuration;->screenWidthDp:I

    iput v0, p0, Lva/i;->x0:I

    iget p1, p1, Landroid/content/res/Configuration;->screenHeightDp:I

    iput p1, p0, Lva/i;->y0:I

    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 8

    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    iget v1, p0, Lva/i;->w0:I

    if-ne v0, v1, :cond_0

    iget v1, p1, Landroid/content/res/Configuration;->screenWidthDp:I

    iget v2, p0, Lva/i;->x0:I

    if-ne v1, v2, :cond_0

    iget v1, p1, Landroid/content/res/Configuration;->screenHeightDp:I

    iget v2, p0, Lva/i;->y0:I

    if-ne v1, v2, :cond_0

    return-void

    :cond_0
    iput v0, p0, Lva/i;->w0:I

    iget v0, p1, Landroid/content/res/Configuration;->screenWidthDp:I

    iput v0, p0, Lva/i;->x0:I

    iget p1, p1, Landroid/content/res/Configuration;->screenHeightDp:I

    iput p1, p0, Lva/i;->y0:I

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->p()Landroidx/fragment/app/e;

    move-result-object p1

    if-eqz p1, :cond_6

    invoke-direct {p0}, Lva/i;->f2()Z

    move-result p1

    if-eqz p1, :cond_6

    iget-boolean p1, p0, Lva/i;->v0:Z

    if-nez p1, :cond_1

    goto/16 :goto_0

    :cond_1
    invoke-virtual {p0}, Landroidx/preference/g;->M1()Landroidx/preference/PreferenceScreen;

    move-result-object p1

    if-nez p1, :cond_2

    return-void

    :cond_2
    iget-object v0, p0, Lva/i;->o0:Lva/i$b;

    invoke-virtual {p1}, Landroidx/preference/Preference;->i()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lva/i$b;->t(Landroid/content/Context;)V

    iget-object v0, p0, Lva/i;->o0:Lva/i$b;

    invoke-virtual {v0}, Lva/i$b;->u()V

    iget-object v0, p0, Lva/i;->n0:Lva/j;

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Landroidx/preference/Preference;->i()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {v0, p1}, Lva/j;->X(Landroid/content/Context;)V

    iget-object v1, p0, Lva/i;->n0:Lva/j;

    iget-object p1, p0, Lva/i;->o0:Lva/i$b;

    invoke-static {p1}, Lva/i$b;->j(Lva/i$b;)Landroid/graphics/Paint;

    move-result-object v2

    iget-object p1, p0, Lva/i;->o0:Lva/i$b;

    invoke-static {p1}, Lva/i$b;->k(Lva/i$b;)I

    move-result v3

    iget-object p1, p0, Lva/i;->o0:Lva/i$b;

    invoke-static {p1}, Lva/i$b;->l(Lva/i$b;)I

    move-result v4

    iget-object p1, p0, Lva/i;->o0:Lva/i$b;

    invoke-static {p1}, Lva/i$b;->m(Lva/i$b;)I

    move-result v5

    iget-object p1, p0, Lva/i;->o0:Lva/i$b;

    invoke-static {p1}, Lva/i$b;->n(Lva/i$b;)I

    move-result v6

    iget-object p1, p0, Lva/i;->o0:Lva/i$b;

    invoke-static {p1}, Lva/i$b;->o(Lva/i$b;)I

    move-result v7

    invoke-virtual/range {v1 .. v7}, Lva/j;->d0(Landroid/graphics/Paint;IIIII)V

    :cond_3
    invoke-virtual {p0}, Landroidx/preference/g;->K1()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object p1

    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$o;

    move-result-object p1

    instance-of v0, p1, Landroidx/recyclerview/widget/LinearLayoutManager;

    if-eqz v0, :cond_4

    move-object v0, p1

    check-cast v0, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {v0}, Landroidx/recyclerview/widget/LinearLayoutManager;->a2()I

    move-result v1

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/LinearLayoutManager;->C(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    invoke-virtual {p0}, Landroidx/preference/g;->K1()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v2

    new-instance v3, Lva/i$a;

    invoke-direct {v3, p0, p1, v1, v0}, Lva/i$a;-><init>(Lva/i;Landroidx/recyclerview/widget/RecyclerView$o;II)V

    invoke-virtual {v2, v3}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    :cond_4
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->w()Landroid/content/Context;

    move-result-object p1

    iget v0, p0, Lva/i;->s0:I

    invoke-static {p1, v0}, Lxa/a;->a(Landroid/content/Context;I)I

    move-result p1

    iput p1, p0, Lva/i;->t0:I

    iget-object v0, p0, Lva/i;->n0:Lva/j;

    if-eqz v0, :cond_5

    iget v1, p0, Lva/i;->s0:I

    iget-boolean v2, p0, Lva/i;->u0:Z

    const/4 v3, 0x1

    invoke-virtual {v0, v1, p1, v2, v3}, Lva/j;->f0(IIZZ)V

    :cond_5
    invoke-virtual {p0}, Landroidx/preference/g;->K1()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object p1

    iget-object v0, p0, Lva/i;->n0:Lva/j;

    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$g;)V

    :cond_6
    :goto_0
    return-void
.end method

.method public s0(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->p()Landroidx/fragment/app/e;

    move-result-object v0

    instance-of v1, v0, Lmiuix/appcompat/app/j;

    if-eqz v1, :cond_1

    check-cast v0, Lmiuix/appcompat/app/j;

    invoke-virtual {v0}, Lmiuix/appcompat/app/j;->s0()I

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    move v3, v2

    :goto_0
    iput-boolean v3, p0, Lva/i;->u0:Z

    invoke-virtual {p0, v1, v2}, Lva/i;->h2(IZ)V

    invoke-virtual {v0, v2}, Lmiuix/appcompat/app/j;->A0(Z)V

    :cond_1
    invoke-super {p0, p1, p2, p3}, Landroidx/preference/g;->s0(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method
