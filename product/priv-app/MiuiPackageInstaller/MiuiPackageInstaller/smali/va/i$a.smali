.class Lva/i$a;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lva/i;->onConfigurationChanged(Landroid/content/res/Configuration;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroidx/recyclerview/widget/RecyclerView$o;

.field final synthetic b:I

.field final synthetic c:I

.field final synthetic d:Lva/i;


# direct methods
.method constructor <init>(Lva/i;Landroidx/recyclerview/widget/RecyclerView$o;II)V
    .locals 0

    iput-object p1, p0, Lva/i$a;->d:Lva/i;

    iput-object p2, p0, Lva/i$a;->a:Landroidx/recyclerview/widget/RecyclerView$o;

    iput p3, p0, Lva/i$a;->b:I

    iput p4, p0, Lva/i$a;->c:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 3

    iget-object v0, p0, Lva/i$a;->a:Landroidx/recyclerview/widget/RecyclerView$o;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView$o;->I(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lva/i$a;->a:Landroidx/recyclerview/widget/RecyclerView$o;

    check-cast v0, Landroidx/recyclerview/widget/LinearLayoutManager;

    iget v1, p0, Lva/i$a;->b:I

    iget v2, p0, Lva/i$a;->c:I

    invoke-virtual {v0, v1, v2}, Landroidx/recyclerview/widget/LinearLayoutManager;->E2(II)V

    iget-object v0, p0, Lva/i$a;->d:Lva/i;

    invoke-virtual {v0}, Landroidx/preference/g;->K1()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    return-void
.end method
