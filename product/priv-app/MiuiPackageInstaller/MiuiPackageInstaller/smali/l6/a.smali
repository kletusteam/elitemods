.class abstract Ll6/a;
.super Ljava/lang/Object;

# interfaces
.implements Ll6/c;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ll6/a$b;
    }
.end annotation


# instance fields
.field protected a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Class;",
            "Ll6/a$b;",
            ">;"
        }
    .end annotation
.end field

.field protected b:Ll6/a$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ll6/a$b<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Ll6/a;->a:Ljava/util/Map;

    new-instance v0, Ll6/a$b;

    invoke-direct {v0}, Ll6/a$b;-><init>()V

    iput-object v0, p0, Ll6/a;->b:Ll6/a$b;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;)Lk6/a;
    .locals 1

    if-nez p1, :cond_0

    const-class p1, Ljava/lang/Void;

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    :cond_1
    :goto_0
    iget-object v0, p0, Ll6/a;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ll6/a$b;

    invoke-virtual {p1}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object p1

    if-nez v0, :cond_2

    if-nez p1, :cond_1

    :cond_2
    new-instance p1, Ll6/a$a;

    invoke-direct {p1, p0, v0}, Ll6/a$a;-><init>(Ll6/a;Ll6/a$b;)V

    return-object p1
.end method

.method protected b(ILl6/e;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ll6/e<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Ll6/a;->b:Ll6/a$b;

    invoke-virtual {v0, p1, p2}, Ll6/a$b;->d(ILl6/e;)V

    return-void
.end method
