.class public final Lt8/e;
.super Ljava/lang/Object;

# interfaces
.implements Lt8/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lt8/f<",
        "TT;>;"
    }
.end annotation


# instance fields
.field private final a:Lt8/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lt8/f<",
            "TT;>;"
        }
    .end annotation
.end field

.field private final b:Z

.field private final c:Ll8/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ll8/l<",
            "TT;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lt8/f;ZLl8/l;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lt8/f<",
            "+TT;>;Z",
            "Ll8/l<",
            "-TT;",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    const-string v0, "sequence"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "predicate"

    invoke-static {p3, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lt8/e;->a:Lt8/f;

    iput-boolean p2, p0, Lt8/e;->b:Z

    iput-object p3, p0, Lt8/e;->c:Ll8/l;

    return-void
.end method

.method public static final synthetic b(Lt8/e;)Ll8/l;
    .locals 0

    iget-object p0, p0, Lt8/e;->c:Ll8/l;

    return-object p0
.end method

.method public static final synthetic c(Lt8/e;)Z
    .locals 0

    iget-boolean p0, p0, Lt8/e;->b:Z

    return p0
.end method

.method public static final synthetic d(Lt8/e;)Lt8/f;
    .locals 0

    iget-object p0, p0, Lt8/e;->a:Lt8/f;

    return-object p0
.end method


# virtual methods
.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "TT;>;"
        }
    .end annotation

    new-instance v0, Lt8/e$a;

    invoke-direct {v0, p0}, Lt8/e$a;-><init>(Lt8/e;)V

    return-object v0
.end method
