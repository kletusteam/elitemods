.class public final Lt8/m;
.super Ljava/lang/Object;

# interfaces
.implements Lt8/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lt8/f<",
        "TR;>;"
    }
.end annotation


# instance fields
.field private final a:Lt8/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lt8/f<",
            "TT;>;"
        }
    .end annotation
.end field

.field private final b:Ll8/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ll8/l<",
            "TT;TR;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lt8/f;Ll8/l;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lt8/f<",
            "+TT;>;",
            "Ll8/l<",
            "-TT;+TR;>;)V"
        }
    .end annotation

    const-string v0, "sequence"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transformer"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lt8/m;->a:Lt8/f;

    iput-object p2, p0, Lt8/m;->b:Ll8/l;

    return-void
.end method

.method public static final synthetic b(Lt8/m;)Lt8/f;
    .locals 0

    iget-object p0, p0, Lt8/m;->a:Lt8/f;

    return-object p0
.end method

.method public static final synthetic c(Lt8/m;)Ll8/l;
    .locals 0

    iget-object p0, p0, Lt8/m;->b:Ll8/l;

    return-object p0
.end method


# virtual methods
.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "TR;>;"
        }
    .end annotation

    new-instance v0, Lt8/m$a;

    invoke-direct {v0, p0}, Lt8/m$a;-><init>(Lt8/m;)V

    return-object v0
.end method
