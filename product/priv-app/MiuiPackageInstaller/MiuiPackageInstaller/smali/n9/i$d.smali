.class Ln9/i$d;
.super Lf9/b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ln9/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "d"
.end annotation


# instance fields
.field private a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Ln9/i;",
            ">;"
        }
    .end annotation
.end field

.field private b:I

.field private c:Z

.field private d:I

.field private e:Z


# direct methods
.method private constructor <init>(Ln9/i;ZII)V
    .locals 1

    invoke-direct {p0}, Lf9/b;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Ln9/i$d;->e:Z

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Ln9/i$d;->a:Ljava/lang/ref/WeakReference;

    iput p4, p0, Ln9/i$d;->b:I

    iput-boolean p2, p0, Ln9/i$d;->c:Z

    iput p3, p0, Ln9/i$d;->d:I

    return-void
.end method

.method synthetic constructor <init>(Ln9/i;ZIILn9/i$a;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Ln9/i$d;-><init>(Ln9/i;ZII)V

    return-void
.end method


# virtual methods
.method public c(Ljava/lang/Object;)V
    .locals 1

    invoke-super {p0, p1}, Lf9/b;->c(Ljava/lang/Object;)V

    iget-object v0, p0, Ln9/i$d;->a:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ln9/i;

    :goto_0
    if-eqz v0, :cond_1

    invoke-static {v0, p1}, Ln9/i;->w(Ln9/i;Ljava/lang/Object;)V

    :cond_1
    return-void
.end method

.method public e(Ljava/lang/Object;)V
    .locals 1

    invoke-super {p0, p1}, Lf9/b;->e(Ljava/lang/Object;)V

    iget-object v0, p0, Ln9/i$d;->a:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ln9/i;

    :goto_0
    if-eqz v0, :cond_1

    invoke-static {v0, p1}, Ln9/i;->w(Ln9/i;Ljava/lang/Object;)V

    :cond_1
    return-void
.end method

.method public j(Ljava/lang/Object;Ljava/util/Collection;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/util/Collection<",
            "Lf9/c;",
            ">;)V"
        }
    .end annotation

    sget-object p1, Lh9/h;->c:Lh9/h;

    invoke-static {p2, p1}, Lf9/c;->a(Ljava/util/Collection;Lh9/b;)Lf9/c;

    move-result-object p1

    iget-boolean p2, p0, Ln9/i$d;->c:Z

    if-eqz p2, :cond_1

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    iget-object p2, p0, Ln9/i$d;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {p2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ln9/i;

    iget-boolean v0, p0, Ln9/i$d;->e:Z

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lf9/c;->c()F

    move-result p1

    const v0, 0x3f19999a    # 0.6f

    iget v1, p0, Ln9/i$d;->d:I

    int-to-float v1, v1

    mul-float/2addr v1, v0

    cmpl-float p1, p1, v1

    if-lez p1, :cond_1

    if-eqz p2, :cond_1

    const/4 p1, 0x1

    iput-boolean p1, p0, Ln9/i$d;->e:Z

    invoke-virtual {p2}, Ln9/i;->I()V

    :cond_1
    :goto_0
    return-void
.end method
