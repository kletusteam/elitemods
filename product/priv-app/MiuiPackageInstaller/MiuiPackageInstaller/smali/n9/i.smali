.class public abstract Ln9/i;
.super Ln9/a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ln9/i$d;,
        Ln9/i$c;
    }
.end annotation


# instance fields
.field protected a:Lmiuix/appcompat/app/j;

.field private b:Landroid/view/View;

.field private c:Landroid/view/View;

.field private d:Landroid/view/View;

.field private e:Landroid/view/View;

.field private f:Landroid/view/View;

.field private g:Lja/g;

.field private h:Landroid/view/GestureDetector;

.field private i:Landroid/view/ViewGroup$LayoutParams;

.field private j:Lm9/f;

.field private k:Lm9/g;

.field private l:F

.field private m:F

.field private n:F

.field private o:F

.field private final p:I

.field private q:Z

.field private r:F

.field private final s:Landroid/os/Handler;

.field private t:Z

.field private u:Z

.field private v:Z

.field private final w:Landroid/graphics/drawable/Drawable;

.field private x:I


# direct methods
.method public constructor <init>(Lmiuix/appcompat/app/j;)V
    .locals 3

    invoke-direct {p0}, Ln9/a;-><init>()V

    const/16 v0, 0x5a

    iput v0, p0, Ln9/i;->p:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Ln9/i;->q:Z

    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Ln9/i;->s:Landroid/os/Handler;

    const/4 v1, 0x0

    iput-boolean v1, p0, Ln9/i;->t:Z

    iput-boolean v0, p0, Ln9/i;->u:Z

    iput-boolean v0, p0, Ln9/i;->v:Z

    iput v1, p0, Ln9/i;->x:I

    iput-object p1, p0, Ln9/i;->a:Lmiuix/appcompat/app/j;

    const v0, 0x1010054

    invoke-static {p1, v0}, Lia/d;->h(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    iput-object p1, p0, Ln9/i;->w:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method static synthetic A(Ln9/i;)Landroid/view/GestureDetector;
    .locals 0

    iget-object p0, p0, Ln9/i;->h:Landroid/view/GestureDetector;

    return-object p0
.end method

.method static synthetic B(Ln9/i;I)V
    .locals 0

    invoke-direct {p0, p1}, Ln9/i;->k0(I)V

    return-void
.end method

.method static synthetic C(Ln9/i;)Z
    .locals 0

    invoke-direct {p0}, Ln9/i;->S()Z

    move-result p0

    return p0
.end method

.method private D(I)V
    .locals 1

    invoke-direct {p0, p1}, Ln9/i;->k0(I)V

    invoke-direct {p0}, Ln9/i;->S()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object p1, p0, Ln9/i;->a:Lmiuix/appcompat/app/j;

    invoke-virtual {p1}, Lmiuix/appcompat/app/j;->y0()V

    iget-object p1, p0, Ln9/i;->a:Lmiuix/appcompat/app/j;

    invoke-static {p1}, Lm9/b;->k(Lmiuix/appcompat/app/j;)V

    goto :goto_0

    :cond_0
    iget-boolean v0, p0, Ln9/i;->t:Z

    if-nez v0, :cond_1

    invoke-direct {p0, p1}, Ln9/i;->i0(I)V

    :cond_1
    :goto_0
    invoke-virtual {p0}, Ln9/i;->G()V

    return-void
.end method

.method private E()Z
    .locals 2

    new-instance v0, Ln9/i$c;

    iget-object v1, p0, Ln9/i;->a:Lmiuix/appcompat/app/j;

    invoke-direct {v0, p0, v1}, Ln9/i$c;-><init>(Ln9/i;Lmiuix/appcompat/app/j;)V

    const/4 v1, 0x1

    invoke-static {v0, v1}, Ln9/i$c;->a(Ln9/i$c;Z)V

    return v1
.end method

.method private F(F)V
    .locals 2

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-static {p1, v0}, Ljava/lang/Math;->min(FF)F

    move-result p1

    const/4 v1, 0x0

    invoke-static {v1, p1}, Ljava/lang/Math;->max(FF)F

    move-result p1

    iget-object v1, p0, Ln9/i;->c:Landroid/view/View;

    sub-float/2addr v0, p1

    const p1, 0x3e99999a    # 0.3f

    mul-float/2addr v0, p1

    invoke-virtual {v1, v0}, Landroid/view/View;->setAlpha(F)V

    return-void
.end method

.method private H(ZI)V
    .locals 15

    move-object v6, p0

    iget-boolean v0, v6, Ln9/i;->t:Z

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    return-void

    :cond_0
    const/4 v7, 0x1

    iput-boolean v7, v6, Ln9/i;->t:Z

    const/4 v8, 0x0

    if-eqz p1, :cond_1

    iget v0, v6, Ln9/i;->r:F

    float-to-int v0, v0

    const/4 v1, 0x0

    const-string v2, "dismiss"

    move v9, v0

    move v10, v1

    move-object v11, v2

    goto :goto_0

    :cond_1
    const v1, 0x3e99999a    # 0.3f

    const-string v2, "init"

    move v10, v1

    move-object v11, v2

    move v9, v8

    :goto_0
    const/4 v0, 0x0

    invoke-static {v7, v0}, Lm9/c;->l(ILjava/lang/Runnable;)Lc9/a;

    move-result-object v12

    new-array v13, v7, [Lf9/b;

    new-instance v14, Ln9/i$d;

    const/4 v5, 0x0

    move-object v0, v14

    move-object v1, p0

    move/from16 v2, p1

    move v3, v9

    move/from16 v4, p2

    invoke-direct/range {v0 .. v5}, Ln9/i$d;-><init>(Ln9/i;ZIILn9/i$a;)V

    aput-object v14, v13, v8

    invoke-virtual {v12, v13}, Lc9/a;->a([Lf9/b;)Lc9/a;

    new-instance v0, Ld9/a;

    invoke-direct {v0, v11}, Ld9/a;-><init>(Ljava/lang/Object;)V

    sget-object v1, Lh9/h;->c:Lh9/h;

    int-to-double v2, v9

    invoke-virtual {v0, v1, v2, v3}, Ld9/a;->a(Ljava/lang/Object;D)Ld9/a;

    move-result-object v0

    new-instance v1, Ld9/a;

    invoke-direct {v1, v11}, Ld9/a;-><init>(Ljava/lang/Object;)V

    sget-object v2, Lh9/h;->o:Lh9/h;

    float-to-double v3, v10

    invoke-virtual {v1, v2, v3, v4}, Ld9/a;->a(Ljava/lang/Object;D)Ld9/a;

    move-result-object v1

    new-array v2, v7, [Landroid/view/View;

    invoke-direct {p0}, Ln9/i;->M()Landroid/view/View;

    move-result-object v3

    aput-object v3, v2, v8

    invoke-static {v2}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object v2

    invoke-interface {v2}, Lmiuix/animation/e;->b()Lmiuix/animation/h;

    move-result-object v2

    new-array v3, v7, [Lc9/a;

    aput-object v12, v3, v8

    invoke-interface {v2, v0, v3}, Lmiuix/animation/h;->s(Ljava/lang/Object;[Lc9/a;)Lmiuix/animation/h;

    new-array v0, v7, [Landroid/view/View;

    iget-object v2, v6, Ln9/i;->c:Landroid/view/View;

    aput-object v2, v0, v8

    invoke-static {v0}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object v0

    invoke-interface {v0}, Lmiuix/animation/e;->b()Lmiuix/animation/h;

    move-result-object v0

    new-array v2, v8, [Lc9/a;

    invoke-interface {v0, v1, v2}, Lmiuix/animation/h;->s(Ljava/lang/Object;[Lc9/a;)Lmiuix/animation/h;

    return-void
.end method

.method private K()V
    .locals 2

    iget-object v0, p0, Ln9/i;->d:Landroid/view/View;

    new-instance v1, Ln9/g;

    invoke-direct {v1, p0}, Ln9/g;-><init>(Ln9/i;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private L()V
    .locals 7

    invoke-direct {p0}, Ln9/i;->M()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v1

    iget-object v2, p0, Ln9/i;->f:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v3

    sub-int/2addr v2, v3

    const/4 v3, 0x2

    div-int/2addr v2, v3

    add-int/2addr v1, v2

    const/4 v2, 0x1

    new-array v4, v2, [Landroid/view/View;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-static {v4}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object v0

    invoke-interface {v0}, Lmiuix/animation/e;->b()Lmiuix/animation/h;

    move-result-object v0

    new-array v4, v3, [Ljava/lang/Object;

    sget-object v6, Lh9/h;->c:Lh9/h;

    aput-object v6, v4, v5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v2

    invoke-interface {v0, v4}, Lmiuix/animation/h;->D([Ljava/lang/Object;)Lmiuix/animation/h;

    move-result-object v0

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v6, v1, v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    const/4 v4, 0x0

    invoke-static {v2, v4}, Lm9/c;->l(ILjava/lang/Runnable;)Lc9/a;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-interface {v0, v1}, Lmiuix/animation/h;->v([Ljava/lang/Object;)Lmiuix/animation/h;

    iget-object v0, p0, Ln9/i;->c:Landroid/view/View;

    invoke-static {v0}, Lx9/a;->b(Landroid/view/View;)V

    return-void
.end method

.method private M()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Ln9/i;->e:Landroid/view/View;

    if-nez v0, :cond_0

    iget-object v0, p0, Ln9/i;->d:Landroid/view/View;

    :cond_0
    return-object v0
.end method

.method private N()V
    .locals 2

    invoke-static {}, Lm9/b;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Ln9/i;->k:Lm9/g;

    if-eqz v0, :cond_1

    iget-boolean v1, p0, Ln9/i;->q:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Ln9/i;->a:Lmiuix/appcompat/app/j;

    invoke-interface {v0, v1}, Lm9/g;->e(Lmiuix/appcompat/app/j;)V

    :cond_1
    return-void
.end method

.method private O(Landroid/view/MotionEvent;)V
    .locals 3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_6

    const/4 v2, 0x1

    if-eq v0, v2, :cond_2

    const/4 v2, 0x2

    if-eq v0, v2, :cond_0

    goto :goto_1

    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result p1

    iget v0, p0, Ln9/i;->n:F

    iget v2, p0, Ln9/i;->m:F

    sub-float v2, p1, v2

    add-float/2addr v0, v2

    iput v0, p0, Ln9/i;->n:F

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_1

    invoke-direct {p0, v0}, Ln9/i;->Z(F)V

    iget v0, p0, Ln9/i;->n:F

    iget v1, p0, Ln9/i;->r:F

    div-float/2addr v0, v1

    invoke-direct {p0, v0}, Ln9/i;->F(F)V

    :cond_1
    iput p1, p0, Ln9/i;->m:F

    goto :goto_1

    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result p1

    iget v0, p0, Ln9/i;->l:F

    sub-float/2addr p1, v0

    iget-object v0, p0, Ln9/i;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x3f000000    # 0.5f

    mul-float/2addr v0, v1

    cmpl-float p1, p1, v0

    const/4 v0, 0x0

    if-lez p1, :cond_3

    move p1, v2

    goto :goto_0

    :cond_3
    move p1, v0

    :goto_0
    invoke-direct {p0, v2}, Ln9/i;->k0(I)V

    if-eqz p1, :cond_5

    invoke-direct {p0}, Ln9/i;->N()V

    iget-object p1, p0, Ln9/i;->k:Lm9/g;

    if-eqz p1, :cond_4

    invoke-interface {p1, v2}, Lm9/f;->h(I)Z

    move-result p1

    if-nez p1, :cond_5

    :cond_4
    move v0, v2

    :cond_5
    invoke-direct {p0, v0, v2}, Ln9/i;->H(ZI)V

    goto :goto_1

    :cond_6
    invoke-direct {p0}, Ln9/i;->b0()V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result p1

    iput p1, p0, Ln9/i;->l:F

    iput p1, p0, Ln9/i;->m:F

    iput v1, p0, Ln9/i;->n:F

    invoke-direct {p0}, Ln9/i;->X()V

    :goto_1
    return-void
.end method

.method private Q()Z
    .locals 1

    iget-boolean v0, p0, Ln9/i;->u:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Ln9/i;->R()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private R()Z
    .locals 1

    iget-object v0, p0, Ln9/i;->k:Lm9/g;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Lm9/g;->d()Z

    move-result v0

    :goto_0
    return v0
.end method

.method private S()Z
    .locals 1

    iget-boolean v0, p0, Ln9/i;->u:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Ln9/i;->k:Lm9/g;

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Lm9/g;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    :goto_0
    const/4 v0, 0x1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    return v0
.end method

.method private synthetic U()V
    .locals 1

    invoke-direct {p0}, Ln9/i;->Q()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Ln9/i;->Y()V

    invoke-direct {p0}, Ln9/i;->L()V

    :cond_0
    return-void
.end method

.method private synthetic V(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 0

    iget-boolean p1, p0, Ln9/i;->q:Z

    if-eqz p1, :cond_0

    invoke-direct {p0, p2}, Ln9/i;->O(Landroid/view/MotionEvent;)V

    :cond_0
    const/4 p1, 0x1

    return p1
.end method

.method private synthetic W(F)V
    .locals 1

    iget-object v0, p0, Ln9/i;->g:Lja/g;

    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->setAlpha(F)V

    return-void
.end method

.method private X()V
    .locals 3

    invoke-direct {p0}, Ln9/i;->M()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v1

    iget-object v2, p0, Ln9/i;->f:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    sub-int/2addr v2, v0

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    int-to-float v0, v1

    iput v0, p0, Ln9/i;->r:F

    return-void
.end method

.method private Y()V
    .locals 2

    iget-object v0, p0, Ln9/i;->k:Lm9/g;

    if-eqz v0, :cond_0

    iget-object v1, p0, Ln9/i;->a:Lmiuix/appcompat/app/j;

    invoke-interface {v0, v1}, Lm9/g;->i(Lmiuix/appcompat/app/j;)V

    :cond_0
    return-void
.end method

.method private Z(F)V
    .locals 1

    invoke-direct {p0}, Ln9/i;->M()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setTranslationY(F)V

    return-void
.end method

.method private a0()V
    .locals 1

    iget-object v0, p0, Ln9/i;->k:Lm9/g;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lm9/g;->f()V

    :cond_0
    return-void
.end method

.method private b0()V
    .locals 1

    iget-object v0, p0, Ln9/i;->k:Lm9/g;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lm9/g;->g()V

    :cond_0
    return-void
.end method

.method private c0()V
    .locals 1

    iget-object v0, p0, Ln9/i;->k:Lm9/g;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lm9/g;->b()V

    :cond_0
    return-void
.end method

.method private d0(Ljava/lang/Object;)V
    .locals 2

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "dismiss"

    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p1, p0, Ln9/i;->a:Lmiuix/appcompat/app/j;

    invoke-virtual {p1}, Lmiuix/appcompat/app/j;->y0()V

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "init"

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-direct {p0}, Ln9/i;->a0()V

    :cond_1
    :goto_0
    const/4 p1, 0x0

    iput-boolean p1, p0, Ln9/i;->t:Z

    return-void
.end method

.method private e0()V
    .locals 5

    iget-boolean v0, p0, Ln9/i;->u:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Ln9/i;->g:Lja/g;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getAlpha()F

    move-result v0

    iget-object v1, p0, Ln9/i;->g:Lja/g;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setAlpha(F)V

    iget-object v1, p0, Ln9/i;->g:Lja/g;

    new-instance v2, Ln9/h;

    invoke-direct {v2, p0, v0}, Ln9/h;-><init>(Ln9/i;F)V

    const-wide/16 v3, 0x5a

    invoke-virtual {v1, v2, v3, v4}, Landroid/widget/FrameLayout;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method private f0(Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Ln9/i;->e:Landroid/view/View;

    return-void
.end method

.method private g0(Lja/g;)V
    .locals 4

    iget-boolean v0, p0, Ln9/i;->u:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Ln9/i;->v:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Ln9/i;->a:Lmiuix/appcompat/app/j;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lk9/e;->K:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    iget-object v2, p0, Ln9/i;->a:Lmiuix/appcompat/app/j;

    sget v3, Lk9/b;->w:I

    invoke-static {v2, v3, v1}, Lia/d;->f(Landroid/content/Context;II)I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v0, v1}, Lja/g;->e(FI)V

    return-void
.end method

.method private h0(ZI)V
    .locals 0

    if-eqz p1, :cond_0

    iget-boolean p1, p0, Ln9/i;->t:Z

    if-nez p1, :cond_0

    invoke-direct {p0}, Ln9/i;->X()V

    invoke-direct {p0}, Ln9/i;->c0()V

    const/4 p1, 0x1

    invoke-direct {p0, p1, p2}, Ln9/i;->H(ZI)V

    :cond_0
    return-void
.end method

.method private i0(I)V
    .locals 1

    invoke-direct {p0}, Ln9/i;->X()V

    invoke-direct {p0}, Ln9/i;->c0()V

    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Ln9/i;->H(ZI)V

    return-void
.end method

.method private j0(ZI)V
    .locals 1

    invoke-direct {p0, p2}, Ln9/i;->k0(I)V

    const/4 v0, 0x0

    if-eqz p1, :cond_2

    iget-object p1, p0, Ln9/i;->j:Lm9/f;

    if-eqz p1, :cond_0

    invoke-interface {p1, p2}, Lm9/f;->h(I)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    iget-object p1, p0, Ln9/i;->k:Lm9/g;

    if-eqz p1, :cond_1

    invoke-interface {p1, p2}, Lm9/f;->h(I)Z

    move-result p1

    if-nez p1, :cond_2

    :cond_1
    const/4 v0, 0x1

    :cond_2
    :goto_0
    invoke-direct {p0, v0, p2}, Ln9/i;->H(ZI)V

    return-void
.end method

.method private k0(I)V
    .locals 0

    iput p1, p0, Ln9/i;->x:I

    return-void
.end method

.method public static synthetic p(Ln9/i;Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 0

    invoke-direct {p0, p1, p2}, Ln9/i;->V(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result p0

    return p0
.end method

.method public static synthetic q(Ln9/i;F)V
    .locals 0

    invoke-direct {p0, p1}, Ln9/i;->W(F)V

    return-void
.end method

.method public static synthetic r(Ln9/i;)V
    .locals 0

    invoke-direct {p0}, Ln9/i;->U()V

    return-void
.end method

.method static synthetic s(Ln9/i;)Z
    .locals 0

    iget-boolean p0, p0, Ln9/i;->q:Z

    return p0
.end method

.method static synthetic t(Ln9/i;)V
    .locals 0

    invoke-direct {p0}, Ln9/i;->N()V

    return-void
.end method

.method static synthetic u(Ln9/i;ZI)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ln9/i;->h0(ZI)V

    return-void
.end method

.method static synthetic v(Ln9/i;)Z
    .locals 0

    iget-boolean p0, p0, Ln9/i;->u:Z

    return p0
.end method

.method static synthetic w(Ln9/i;Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0, p1}, Ln9/i;->d0(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic x(Ln9/i;)V
    .locals 0

    invoke-direct {p0}, Ln9/i;->X()V

    return-void
.end method

.method static synthetic y(Ln9/i;)V
    .locals 0

    invoke-direct {p0}, Ln9/i;->c0()V

    return-void
.end method

.method static synthetic z(Ln9/i;ZI)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ln9/i;->j0(ZI)V

    return-void
.end method


# virtual methods
.method public G()V
    .locals 0

    return-void
.end method

.method public I()V
    .locals 1

    iget-object v0, p0, Ln9/i;->k:Lm9/g;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lm9/g;->a()V

    :cond_0
    return-void
.end method

.method public J()V
    .locals 1

    iget-boolean v0, p0, Ln9/i;->u:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Ln9/i;->d:Landroid/view/View;

    invoke-static {v0}, Lm9/c;->g(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method public P()V
    .locals 1

    iget-boolean v0, p0, Ln9/i;->u:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Ln9/i;->d:Landroid/view/View;

    invoke-static {v0}, Lm9/c;->e(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method protected T()Z
    .locals 1

    iget-boolean v0, p0, Ln9/i;->u:Z

    return v0
.end method

.method public a()Z
    .locals 4

    invoke-static {}, Lm9/b;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Ln9/i;->E()Z

    move-result v0

    return v0

    :cond_0
    iget-boolean v0, p0, Ln9/i;->u:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Ln9/i;->N()V

    iget-object v0, p0, Ln9/i;->s:Landroid/os/Handler;

    new-instance v1, Ln9/i$c;

    iget-object v2, p0, Ln9/i;->a:Lmiuix/appcompat/app/j;

    invoke-direct {v1, p0, v2}, Ln9/i$c;-><init>(Ln9/i;Lmiuix/appcompat/app/j;)V

    const-wide/16 v2, 0x6e

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Ln9/i;->a:Lmiuix/appcompat/app/j;

    invoke-virtual {v0}, Lmiuix/appcompat/app/j;->y0()V

    invoke-virtual {p0}, Ln9/i;->G()V

    :goto_0
    const/4 v0, 0x1

    return v0
.end method

.method public b()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Ln9/i;->d:Landroid/view/View;

    return-object v0
.end method

.method public c()Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    iget-object v0, p0, Ln9/i;->i:Landroid/view/ViewGroup$LayoutParams;

    return-object v0
.end method

.method public d()V
    .locals 2

    iget-object v0, p0, Ln9/i;->d:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public e()V
    .locals 2

    iget-object v0, p0, Ln9/i;->c:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public f(Landroid/view/View;Z)V
    .locals 2

    sget v0, Lk9/g;->R:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ln9/i;->b:Landroid/view/View;

    sget v0, Lk9/g;->h:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ln9/i;->c:Landroid/view/View;

    const v1, 0x3e99999a    # 0.3f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    sget v0, Lk9/g;->j:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ln9/i;->d:Landroid/view/View;

    sget v0, Lk9/g;->i:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ln9/i;->f:Landroid/view/View;

    iput-boolean p2, p0, Ln9/i;->u:Z

    const/4 p2, 0x0

    iput-boolean p2, p0, Ln9/i;->q:Z

    new-instance p2, Landroid/view/GestureDetector;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    new-instance v0, Ln9/i$a;

    invoke-direct {v0, p0}, Ln9/i$a;-><init>(Ln9/i;)V

    invoke-direct {p2, p1, v0}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object p2, p0, Ln9/i;->h:Landroid/view/GestureDetector;

    iget-object p1, p0, Ln9/i;->f:Landroid/view/View;

    new-instance p2, Ln9/i$b;

    invoke-direct {p2, p0}, Ln9/i$b;-><init>(Ln9/i;)V

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object p1, p0, Ln9/i;->b:Landroid/view/View;

    new-instance p2, Ln9/f;

    invoke-direct {p2, p0}, Ln9/f;-><init>(Ln9/i;)V

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    invoke-direct {p0}, Ln9/i;->K()V

    iget-object p1, p0, Ln9/i;->a:Lmiuix/appcompat/app/j;

    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object p1

    sget p2, Lk9/d;->a:I

    invoke-virtual {p1, p2}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    iget-boolean p1, p0, Ln9/i;->u:Z

    if-nez p1, :cond_0

    iget-object p1, p0, Ln9/i;->a:Lmiuix/appcompat/app/j;

    invoke-static {p1}, Lia/i;->b(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Ln9/i;->d:Landroid/view/View;

    new-instance p2, Landroid/graphics/drawable/ColorDrawable;

    const/high16 v0, -0x1000000

    invoke-direct {p2, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Ln9/i;->d:Landroid/view/View;

    iget-object p2, p0, Ln9/i;->w:Landroid/graphics/drawable/Drawable;

    :goto_0
    invoke-virtual {p1, p2}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public i()Z
    .locals 1

    iget-boolean v0, p0, Ln9/i;->u:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lm9/b;->f()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Ln9/i;->N()V

    :cond_0
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Ln9/i;->D(I)V

    const/4 v0, 0x1

    return v0
.end method

.method public j(Landroid/view/View;Z)Landroid/view/ViewGroup;
    .locals 4

    iget-object v0, p0, Ln9/i;->a:Lmiuix/appcompat/app/j;

    sget v1, Lk9/i;->D:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    sget v1, Lk9/g;->j:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    sget v2, Lk9/g;->R:I

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    instance-of v3, v3, Landroid/view/ViewGroup;

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    invoke-virtual {v3, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_0
    instance-of v3, p1, Landroid/view/ViewGroup;

    if-eqz v3, :cond_1

    move-object v3, p1

    check-cast v3, Landroid/view/ViewGroup;

    invoke-virtual {v3, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :cond_1
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iput-object v2, p0, Ln9/i;->i:Landroid/view/ViewGroup$LayoutParams;

    if-nez p2, :cond_2

    const/4 v3, -0x1

    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_0

    :cond_2
    const/4 v3, -0x2

    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    :goto_0
    invoke-virtual {p1, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    instance-of v2, v1, Landroid/view/ViewGroup;

    if-eqz v2, :cond_3

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_3
    iget-object v1, p0, Ln9/i;->a:Lmiuix/appcompat/app/j;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lk9/e;->L:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Ln9/i;->o:F

    new-instance v1, Lja/g;

    iget-object v2, p0, Ln9/i;->a:Lmiuix/appcompat/app/j;

    invoke-direct {v1, v2}, Lja/g;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Ln9/i;->g:Lja/g;

    iget-object v2, p0, Ln9/i;->i:Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Ln9/i;->g:Lja/g;

    invoke-virtual {v1, p1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    iget-object p1, p0, Ln9/i;->g:Lja/g;

    if-eqz p2, :cond_4

    iget p2, p0, Ln9/i;->o:F

    goto :goto_1

    :cond_4
    const/4 p2, 0x0

    :goto_1
    invoke-virtual {p1, p2}, Lja/g;->setRadius(F)V

    iget-object p1, p0, Ln9/i;->g:Lja/g;

    invoke-direct {p0, p1}, Ln9/i;->g0(Lja/g;)V

    invoke-direct {p0}, Ln9/i;->e0()V

    iget-object p1, p0, Ln9/i;->g:Lja/g;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object p1, p0, Ln9/i;->g:Lja/g;

    invoke-direct {p0, p1}, Ln9/i;->f0(Landroid/view/View;)V

    return-object v0
.end method

.method public k(Z)V
    .locals 1

    iput-boolean p1, p0, Ln9/i;->q:Z

    iget-object v0, p0, Ln9/i;->b:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const/16 p1, 0x8

    :goto_0
    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public l(Z)V
    .locals 2

    iput-boolean p1, p0, Ln9/i;->u:Z

    iget-object v0, p0, Ln9/i;->a:Lmiuix/appcompat/app/j;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lm9/k;->b(Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Ln9/i;->a:Lmiuix/appcompat/app/j;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lmiuix/view/b;->a(Landroid/app/Activity;Z)V

    :cond_0
    iget-object v0, p0, Ln9/i;->g:Lja/g;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ln9/i;->a:Lmiuix/appcompat/app/j;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lk9/e;->L:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Ln9/i;->o:F

    iget-object v1, p0, Ln9/i;->g:Lja/g;

    if-eqz p1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lja/g;->setRadius(F)V

    iget-object v0, p0, Ln9/i;->g:Lja/g;

    invoke-direct {p0, v0}, Ln9/i;->g0(Lja/g;)V

    :cond_2
    iget-object v0, p0, Ln9/i;->d:Landroid/view/View;

    if-eqz v0, :cond_4

    if-nez p1, :cond_3

    iget-object p1, p0, Ln9/i;->a:Lmiuix/appcompat/app/j;

    invoke-static {p1}, Lia/i;->b(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_3

    iget-object p1, p0, Ln9/i;->d:Landroid/view/View;

    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    const/high16 v1, -0x1000000

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    goto :goto_1

    :cond_3
    iget-object p1, p0, Ln9/i;->d:Landroid/view/View;

    iget-object v0, p0, Ln9/i;->w:Landroid/graphics/drawable/Drawable;

    :goto_1
    invoke-virtual {p1, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    :cond_4
    return-void
.end method

.method public m(Lm9/g;)V
    .locals 0

    iput-object p1, p0, Ln9/i;->k:Lm9/g;

    return-void
.end method

.method public n()V
    .locals 2

    iget-object v0, p0, Ln9/i;->d:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public o()V
    .locals 1

    iget-boolean v0, p0, Ln9/i;->u:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Ln9/i;->d:Landroid/view/View;

    invoke-static {v0}, Lm9/c;->b(Landroid/view/View;)V

    :cond_0
    return-void
.end method
