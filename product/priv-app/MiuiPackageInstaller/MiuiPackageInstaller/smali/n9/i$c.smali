.class Ln9/i$c;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ln9/i;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "c"
.end annotation


# instance fields
.field private a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Ln9/i;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lmiuix/appcompat/app/j;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ln9/i;Lmiuix/appcompat/app/j;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Ln9/i$c;->a:Ljava/lang/ref/WeakReference;

    new-instance p1, Ljava/lang/ref/WeakReference;

    invoke-direct {p1, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object p1, p0, Ln9/i$c;->b:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method static synthetic a(Ln9/i$c;Z)V
    .locals 0

    invoke-direct {p0, p1}, Ln9/i$c;->c(Z)V

    return-void
.end method

.method private b(Lmiuix/appcompat/app/j;Ln9/i;ZIZ)V
    .locals 1

    invoke-static {p2}, Ln9/i;->C(Ln9/i;)Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lmiuix/appcompat/app/j;->y0()V

    invoke-direct {p0, p1, p2, p5}, Ln9/i$c;->d(Lmiuix/appcompat/app/j;Ln9/i;Z)V

    goto :goto_0

    :cond_0
    invoke-static {p2, p3, p4}, Ln9/i;->u(Ln9/i;ZI)V

    :cond_1
    :goto_0
    return-void
.end method

.method private c(Z)V
    .locals 7

    iget-object v0, p0, Ln9/i$c;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Ln9/i;

    if-eqz v3, :cond_0

    const/4 v0, 0x3

    invoke-static {v3, v0}, Ln9/i;->B(Ln9/i;I)V

    :cond_0
    iget-object v0, p0, Ln9/i$c;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lmiuix/appcompat/app/j;

    if-eqz v3, :cond_1

    const/4 v4, 0x1

    const/4 v5, 0x3

    move-object v1, p0

    move v6, p1

    invoke-direct/range {v1 .. v6}, Ln9/i$c;->b(Lmiuix/appcompat/app/j;Ln9/i;ZIZ)V

    :cond_1
    return-void
.end method

.method private d(Lmiuix/appcompat/app/j;Ln9/i;Z)V
    .locals 0

    if-eqz p3, :cond_0

    invoke-static {p2}, Ln9/i;->v(Ln9/i;)Z

    move-result p2

    invoke-static {p1, p2}, Lm9/b;->i(Lmiuix/appcompat/app/j;Z)V

    :cond_0
    return-void
.end method


# virtual methods
.method public run()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Ln9/i$c;->c(Z)V

    return-void
.end method
