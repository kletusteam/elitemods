.class public Ln9/c;
.super Ln9/i;


# direct methods
.method public constructor <init>(Lmiuix/appcompat/app/j;)V
    .locals 0

    invoke-direct {p0, p1}, Ln9/i;-><init>(Lmiuix/appcompat/app/j;)V

    return-void
.end method


# virtual methods
.method public G()V
    .locals 1

    invoke-virtual {p0}, Ln9/i;->T()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ln9/i;->a:Lmiuix/appcompat/app/j;

    invoke-static {v0}, Lm9/b;->a(Lmiuix/appcompat/app/j;)V

    return-void

    :cond_0
    iget-object v0, p0, Ln9/i;->a:Lmiuix/appcompat/app/j;

    invoke-static {v0}, Lm9/b;->h(Lmiuix/appcompat/app/j;)I

    move-result v0

    if-ltz v0, :cond_1

    iget-object v0, p0, Ln9/i;->a:Lmiuix/appcompat/app/j;

    invoke-static {v0}, Lm9/b;->c(Lmiuix/appcompat/app/j;)V

    :cond_1
    return-void
.end method

.method public g()Z
    .locals 4

    iget-object v0, p0, Ln9/i;->a:Lmiuix/appcompat/app/j;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/16 v3, 0x258

    if-lt v0, v3, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    move v0, v2

    :goto_0
    iget-object v3, p0, Ln9/i;->a:Lmiuix/appcompat/app/j;

    invoke-static {v3}, Lba/a;->b(Landroid/content/Context;)I

    move-result v3

    if-eqz v0, :cond_1

    const/16 v0, 0x2000

    if-eq v3, v0, :cond_2

    const/16 v0, 0x2003

    if-ne v3, v0, :cond_1

    goto :goto_1

    :cond_1
    move v1, v2

    :cond_2
    :goto_1
    return v1
.end method
