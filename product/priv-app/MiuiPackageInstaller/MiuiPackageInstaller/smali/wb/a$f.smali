.class final Lwb/a$f;
.super Ljava/lang/Object;

# interfaces
.implements Ldc/w;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lwb/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "f"
.end annotation


# instance fields
.field private final a:Ldc/k;

.field private b:Z

.field final synthetic c:Lwb/a;


# direct methods
.method public constructor <init>(Lwb/a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    iput-object p1, p0, Lwb/a$f;->c:Lwb/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ldc/k;

    invoke-static {p1}, Lwb/a;->k(Lwb/a;)Ldc/f;

    move-result-object p1

    invoke-interface {p1}, Ldc/w;->d()Ldc/z;

    move-result-object p1

    invoke-direct {v0, p1}, Ldc/k;-><init>(Ldc/z;)V

    iput-object v0, p0, Lwb/a$f;->a:Ldc/k;

    return-void
.end method


# virtual methods
.method public I(Ldc/e;J)V
    .locals 7

    const-string v0, "source"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    iget-boolean v0, p0, Lwb/a$f;->b:Z

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ldc/e;->d0()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    move-wide v5, p2

    invoke-static/range {v1 .. v6}, Lrb/b;->i(JJJ)V

    iget-object v0, p0, Lwb/a$f;->c:Lwb/a;

    invoke-static {v0}, Lwb/a;->k(Lwb/a;)Ldc/f;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Ldc/w;->I(Ldc/e;J)V

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "closed"

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public close()V
    .locals 2

    iget-boolean v0, p0, Lwb/a$f;->b:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lwb/a$f;->b:Z

    iget-object v0, p0, Lwb/a$f;->c:Lwb/a;

    iget-object v1, p0, Lwb/a$f;->a:Ldc/k;

    invoke-static {v0, v1}, Lwb/a;->i(Lwb/a;Ldc/k;)V

    iget-object v0, p0, Lwb/a$f;->c:Lwb/a;

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lwb/a;->p(Lwb/a;I)V

    return-void
.end method

.method public d()Ldc/z;
    .locals 1

    iget-object v0, p0, Lwb/a$f;->a:Ldc/k;

    return-object v0
.end method

.method public flush()V
    .locals 1

    iget-boolean v0, p0, Lwb/a$f;->b:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lwb/a$f;->c:Lwb/a;

    invoke-static {v0}, Lwb/a;->k(Lwb/a;)Ldc/f;

    move-result-object v0

    invoke-interface {v0}, Ldc/f;->flush()V

    return-void
.end method
