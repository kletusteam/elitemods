.class public final Lwb/a;
.super Ljava/lang/Object;

# interfaces
.implements Lvb/d;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lwb/a$f;,
        Lwb/a$b;,
        Lwb/a$a;,
        Lwb/a$e;,
        Lwb/a$c;,
        Lwb/a$g;,
        Lwb/a$d;
    }
.end annotation


# static fields
.field public static final h:Lwb/a$d;


# instance fields
.field private a:I

.field private b:J

.field private c:Lpb/s;

.field private final d:Lpb/x;

.field private final e:Lub/f;

.field private final f:Ldc/g;

.field private final g:Ldc/f;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lwb/a$d;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lwb/a$d;-><init>(Lm8/g;)V

    sput-object v0, Lwb/a;->h:Lwb/a$d;

    return-void
.end method

.method public constructor <init>(Lpb/x;Lub/f;Ldc/g;Ldc/f;)V
    .locals 1

    const-string v0, "connection"

    invoke-static {p2, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source"

    invoke-static {p3, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "sink"

    invoke-static {p4, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lwb/a;->d:Lpb/x;

    iput-object p2, p0, Lwb/a;->e:Lub/f;

    iput-object p3, p0, Lwb/a;->f:Ldc/g;

    iput-object p4, p0, Lwb/a;->g:Ldc/f;

    const/high16 p1, 0x40000

    int-to-long p1, p1

    iput-wide p1, p0, Lwb/a;->b:J

    return-void
.end method

.method private final A()Lpb/s;
    .locals 3

    new-instance v0, Lpb/s$a;

    invoke-direct {v0}, Lpb/s$a;-><init>()V

    :goto_0
    invoke-direct {p0}, Lwb/a;->z()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-lez v2, :cond_0

    const/4 v2, 0x1

    goto :goto_1

    :cond_0
    const/4 v2, 0x0

    :goto_1
    if-eqz v2, :cond_1

    invoke-virtual {v0, v1}, Lpb/s$a;->c(Ljava/lang/String;)Lpb/s$a;

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lpb/s$a;->e()Lpb/s;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic i(Lwb/a;Ldc/k;)V
    .locals 0

    invoke-direct {p0, p1}, Lwb/a;->r(Ldc/k;)V

    return-void
.end method

.method public static final synthetic j(Lwb/a;)Lpb/x;
    .locals 0

    iget-object p0, p0, Lwb/a;->d:Lpb/x;

    return-object p0
.end method

.method public static final synthetic k(Lwb/a;)Ldc/f;
    .locals 0

    iget-object p0, p0, Lwb/a;->g:Ldc/f;

    return-object p0
.end method

.method public static final synthetic l(Lwb/a;)Ldc/g;
    .locals 0

    iget-object p0, p0, Lwb/a;->f:Ldc/g;

    return-object p0
.end method

.method public static final synthetic m(Lwb/a;)I
    .locals 0

    iget p0, p0, Lwb/a;->a:I

    return p0
.end method

.method public static final synthetic n(Lwb/a;)Lpb/s;
    .locals 0

    iget-object p0, p0, Lwb/a;->c:Lpb/s;

    return-object p0
.end method

.method public static final synthetic o(Lwb/a;)Lpb/s;
    .locals 0

    invoke-direct {p0}, Lwb/a;->A()Lpb/s;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic p(Lwb/a;I)V
    .locals 0

    iput p1, p0, Lwb/a;->a:I

    return-void
.end method

.method public static final synthetic q(Lwb/a;Lpb/s;)V
    .locals 0

    iput-object p1, p0, Lwb/a;->c:Lpb/s;

    return-void
.end method

.method private final r(Ldc/k;)V
    .locals 2

    invoke-virtual {p1}, Ldc/k;->i()Ldc/z;

    move-result-object v0

    sget-object v1, Ldc/z;->d:Ldc/z;

    invoke-virtual {p1, v1}, Ldc/k;->j(Ldc/z;)Ldc/k;

    invoke-virtual {v0}, Ldc/z;->a()Ldc/z;

    invoke-virtual {v0}, Ldc/z;->b()Ldc/z;

    return-void
.end method

.method private final s(Lpb/z;)Z
    .locals 2

    const-string v0, "Transfer-Encoding"

    invoke-virtual {p1, v0}, Lpb/z;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "chunked"

    const/4 v1, 0x1

    invoke-static {v0, p1, v1}, Lu8/g;->j(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result p1

    return p1
.end method

.method private final t(Lpb/b0;)Z
    .locals 3

    const-string v0, "Transfer-Encoding"

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-static {p1, v0, v1, v2, v1}, Lpb/b0;->y(Lpb/b0;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "chunked"

    const/4 v1, 0x1

    invoke-static {v0, p1, v1}, Lu8/g;->j(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result p1

    return p1
.end method

.method private final u()Ldc/w;
    .locals 2

    iget v0, p0, Lwb/a;->a:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    const/4 v0, 0x2

    iput v0, p0, Lwb/a;->a:I

    new-instance v0, Lwb/a$b;

    invoke-direct {v0, p0}, Lwb/a$b;-><init>(Lwb/a;)V

    return-object v0

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "state: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lwb/a;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private final v(Lpb/t;)Ldc/y;
    .locals 2

    iget v0, p0, Lwb/a;->a:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    const/4 v0, 0x5

    iput v0, p0, Lwb/a;->a:I

    new-instance v0, Lwb/a$c;

    invoke-direct {v0, p0, p1}, Lwb/a$c;-><init>(Lwb/a;Lpb/t;)V

    return-object v0

    :cond_1
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "state: "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v0, p0, Lwb/a;->a:I

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private final w(J)Ldc/y;
    .locals 2

    iget v0, p0, Lwb/a;->a:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    const/4 v0, 0x5

    iput v0, p0, Lwb/a;->a:I

    new-instance v0, Lwb/a$e;

    invoke-direct {v0, p0, p1, p2}, Lwb/a$e;-><init>(Lwb/a;J)V

    return-object v0

    :cond_1
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "state: "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p2, p0, Lwb/a;->a:I

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance p2, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method private final x()Ldc/w;
    .locals 2

    iget v0, p0, Lwb/a;->a:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    const/4 v0, 0x2

    iput v0, p0, Lwb/a;->a:I

    new-instance v0, Lwb/a$f;

    invoke-direct {v0, p0}, Lwb/a$f;-><init>(Lwb/a;)V

    return-object v0

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "state: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lwb/a;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private final y()Ldc/y;
    .locals 2

    iget v0, p0, Lwb/a;->a:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    const/4 v0, 0x5

    iput v0, p0, Lwb/a;->a:I

    invoke-virtual {p0}, Lwb/a;->h()Lub/f;

    move-result-object v0

    invoke-virtual {v0}, Lub/f;->A()V

    new-instance v0, Lwb/a$g;

    invoke-direct {v0, p0}, Lwb/a$g;-><init>(Lwb/a;)V

    return-object v0

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "state: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lwb/a;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private final z()Ljava/lang/String;
    .locals 5

    iget-object v0, p0, Lwb/a;->f:Ldc/g;

    iget-wide v1, p0, Lwb/a;->b:J

    invoke-interface {v0, v1, v2}, Ldc/g;->E(J)Ljava/lang/String;

    move-result-object v0

    iget-wide v1, p0, Lwb/a;->b:J

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    int-to-long v3, v3

    sub-long/2addr v1, v3

    iput-wide v1, p0, Lwb/a;->b:J

    return-object v0
.end method


# virtual methods
.method public final B(Lpb/b0;)V
    .locals 4

    const-string v0, "response"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lrb/b;->s(Lpb/b0;)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long p1, v0, v2

    if-nez p1, :cond_0

    return-void

    :cond_0
    invoke-direct {p0, v0, v1}, Lwb/a;->w(J)Ldc/y;

    move-result-object p1

    const v0, 0x7fffffff

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {p1, v0, v1}, Lrb/b;->G(Ldc/y;ILjava/util/concurrent/TimeUnit;)Z

    invoke-interface {p1}, Ldc/y;->close()V

    return-void
.end method

.method public final C(Lpb/s;Ljava/lang/String;)V
    .locals 5

    const-string v0, "headers"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "requestLine"

    invoke-static {p2, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    iget v0, p0, Lwb/a;->a:I

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-nez v0, :cond_0

    move v0, v2

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_0
    if-eqz v0, :cond_2

    iget-object v0, p0, Lwb/a;->g:Ldc/f;

    invoke-interface {v0, p2}, Ldc/f;->O(Ljava/lang/String;)Ldc/f;

    move-result-object p2

    const-string v0, "\r\n"

    invoke-interface {p2, v0}, Ldc/f;->O(Ljava/lang/String;)Ldc/f;

    invoke-virtual {p1}, Lpb/s;->size()I

    move-result p2

    :goto_1
    if-ge v1, p2, :cond_1

    iget-object v3, p0, Lwb/a;->g:Ldc/f;

    invoke-virtual {p1, v1}, Lpb/s;->c(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ldc/f;->O(Ljava/lang/String;)Ldc/f;

    move-result-object v3

    const-string v4, ": "

    invoke-interface {v3, v4}, Ldc/f;->O(Ljava/lang/String;)Ldc/f;

    move-result-object v3

    invoke-virtual {p1, v1}, Lpb/s;->f(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ldc/f;->O(Ljava/lang/String;)Ldc/f;

    move-result-object v3

    invoke-interface {v3, v0}, Ldc/f;->O(Ljava/lang/String;)Ldc/f;

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    iget-object p1, p0, Lwb/a;->g:Ldc/f;

    invoke-interface {p1, v0}, Ldc/f;->O(Ljava/lang/String;)Ldc/f;

    iput v2, p0, Lwb/a;->a:I

    return-void

    :cond_2
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "state: "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p2, p0, Lwb/a;->a:I

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance p2, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method public a(Lpb/b0;)J
    .locals 2

    const-string v0, "response"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lvb/e;->a(Lpb/b0;)Z

    move-result v0

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-direct {p0, p1}, Lwb/a;->t(Lpb/b0;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-wide/16 v0, -0x1

    goto :goto_0

    :cond_1
    invoke-static {p1}, Lrb/b;->s(Lpb/b0;)J

    move-result-wide v0

    :goto_0
    return-wide v0
.end method

.method public b(Lpb/z;)V
    .locals 3

    const-string v0, "request"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lvb/i;->a:Lvb/i;

    invoke-virtual {p0}, Lwb/a;->h()Lub/f;

    move-result-object v1

    invoke-virtual {v1}, Lub/f;->b()Lpb/d0;

    move-result-object v1

    invoke-virtual {v1}, Lpb/d0;->b()Ljava/net/Proxy;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v1

    const-string v2, "connection.route().proxy.type()"

    invoke-static {v1, v2}, Lm8/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1, v1}, Lvb/i;->a(Lpb/z;Ljava/net/Proxy$Type;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lpb/z;->e()Lpb/s;

    move-result-object p1

    invoke-virtual {p0, p1, v0}, Lwb/a;->C(Lpb/s;Ljava/lang/String;)V

    return-void
.end method

.method public c()V
    .locals 1

    iget-object v0, p0, Lwb/a;->g:Ldc/f;

    invoke-interface {v0}, Ldc/f;->flush()V

    return-void
.end method

.method public cancel()V
    .locals 1

    invoke-virtual {p0}, Lwb/a;->h()Lub/f;

    move-result-object v0

    invoke-virtual {v0}, Lub/f;->f()V

    return-void
.end method

.method public d()V
    .locals 1

    iget-object v0, p0, Lwb/a;->g:Ldc/f;

    invoke-interface {v0}, Ldc/f;->flush()V

    return-void
.end method

.method public e(Lpb/b0;)Ldc/y;
    .locals 4

    const-string v0, "response"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lvb/e;->a(Lpb/b0;)Z

    move-result v0

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    invoke-direct {p0, v0, v1}, Lwb/a;->w(J)Ldc/y;

    move-result-object p1

    goto :goto_1

    :cond_0
    invoke-direct {p0, p1}, Lwb/a;->t(Lpb/b0;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lpb/b0;->U()Lpb/z;

    move-result-object p1

    invoke-virtual {p1}, Lpb/z;->j()Lpb/t;

    move-result-object p1

    invoke-direct {p0, p1}, Lwb/a;->v(Lpb/t;)Ldc/y;

    move-result-object p1

    goto :goto_1

    :cond_1
    invoke-static {p1}, Lrb/b;->s(Lpb/b0;)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long p1, v0, v2

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lwb/a;->y()Ldc/y;

    move-result-object p1

    :goto_1
    return-object p1
.end method

.method public f(Lpb/z;J)Ldc/w;
    .locals 2

    const-string v0, "request"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lpb/z;->a()Lpb/a0;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lpb/z;->a()Lpb/a0;

    move-result-object v0

    invoke-virtual {v0}, Lpb/a0;->e()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/net/ProtocolException;

    const-string p2, "Duplex connections are not supported for HTTP/1"

    invoke-direct {p1, p2}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    :goto_0
    invoke-direct {p0, p1}, Lwb/a;->s(Lpb/z;)Z

    move-result p1

    if-eqz p1, :cond_2

    invoke-direct {p0}, Lwb/a;->u()Ldc/w;

    move-result-object p1

    goto :goto_1

    :cond_2
    const-wide/16 v0, -0x1

    cmp-long p1, p2, v0

    if-eqz p1, :cond_3

    invoke-direct {p0}, Lwb/a;->x()Ldc/w;

    move-result-object p1

    :goto_1
    return-object p1

    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Cannot stream a request body without chunked encoding or a known content length!"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public g(Z)Lpb/b0$a;
    .locals 4

    iget v0, p0, Lwb/a;->a:I

    const/4 v1, 0x3

    const/4 v2, 0x1

    if-eq v0, v2, :cond_1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :cond_1
    :goto_0
    if-eqz v2, :cond_4

    :try_start_0
    sget-object v0, Lvb/k;->d:Lvb/k$a;

    invoke-direct {p0}, Lwb/a;->z()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lvb/k$a;->a(Ljava/lang/String;)Lvb/k;

    move-result-object v0

    new-instance v2, Lpb/b0$a;

    invoke-direct {v2}, Lpb/b0$a;-><init>()V

    iget-object v3, v0, Lvb/k;->a:Lpb/y;

    invoke-virtual {v2, v3}, Lpb/b0$a;->p(Lpb/y;)Lpb/b0$a;

    move-result-object v2

    iget v3, v0, Lvb/k;->b:I

    invoke-virtual {v2, v3}, Lpb/b0$a;->g(I)Lpb/b0$a;

    move-result-object v2

    iget-object v3, v0, Lvb/k;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lpb/b0$a;->m(Ljava/lang/String;)Lpb/b0$a;

    move-result-object v2

    invoke-direct {p0}, Lwb/a;->A()Lpb/s;

    move-result-object v3

    invoke-virtual {v2, v3}, Lpb/b0$a;->k(Lpb/s;)Lpb/b0$a;

    move-result-object v2

    const/16 v3, 0x64

    if-eqz p1, :cond_2

    iget p1, v0, Lvb/k;->b:I

    if-ne p1, v3, :cond_2

    const/4 v2, 0x0

    goto :goto_1

    :cond_2
    iget p1, v0, Lvb/k;->b:I

    if-ne p1, v3, :cond_3

    iput v1, p0, Lwb/a;->a:I

    goto :goto_1

    :cond_3
    const/4 p1, 0x4

    iput p1, p0, Lwb/a;->a:I
    :try_end_0
    .catch Ljava/io/EOFException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    return-object v2

    :catch_0
    move-exception p1

    invoke-virtual {p0}, Lwb/a;->h()Lub/f;

    move-result-object v0

    invoke-virtual {v0}, Lub/f;->b()Lpb/d0;

    move-result-object v0

    invoke-virtual {v0}, Lpb/d0;->a()Lpb/a;

    move-result-object v0

    invoke-virtual {v0}, Lpb/a;->l()Lpb/t;

    move-result-object v0

    invoke-virtual {v0}, Lpb/t;->p()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unexpected end of stream on "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :cond_4
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "state: "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v0, p0, Lwb/a;->a:I

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public h()Lub/f;
    .locals 1

    iget-object v0, p0, Lwb/a;->e:Lub/f;

    return-object v0
.end method
