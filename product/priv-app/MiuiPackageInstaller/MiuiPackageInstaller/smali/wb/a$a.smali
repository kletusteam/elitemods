.class abstract Lwb/a$a;
.super Ljava/lang/Object;

# interfaces
.implements Ldc/y;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lwb/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x402
    name = "a"
.end annotation


# instance fields
.field private final a:Ldc/k;

.field private b:Z

.field final synthetic c:Lwb/a;


# direct methods
.method public constructor <init>(Lwb/a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    iput-object p1, p0, Lwb/a$a;->c:Lwb/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ldc/k;

    invoke-static {p1}, Lwb/a;->l(Lwb/a;)Ldc/g;

    move-result-object p1

    invoke-interface {p1}, Ldc/y;->d()Ldc/z;

    move-result-object p1

    invoke-direct {v0, p1}, Ldc/k;-><init>(Ldc/z;)V

    iput-object v0, p0, Lwb/a$a;->a:Ldc/k;

    return-void
.end method


# virtual methods
.method protected final b()Z
    .locals 1

    iget-boolean v0, p0, Lwb/a$a;->b:Z

    return v0
.end method

.method public d()Ldc/z;
    .locals 1

    iget-object v0, p0, Lwb/a$a;->a:Ldc/k;

    return-object v0
.end method

.method public final g()V
    .locals 3

    iget-object v0, p0, Lwb/a$a;->c:Lwb/a;

    invoke-static {v0}, Lwb/a;->m(Lwb/a;)I

    move-result v0

    const/4 v1, 0x6

    if-ne v0, v1, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lwb/a$a;->c:Lwb/a;

    invoke-static {v0}, Lwb/a;->m(Lwb/a;)I

    move-result v0

    const/4 v2, 0x5

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lwb/a$a;->c:Lwb/a;

    iget-object v2, p0, Lwb/a$a;->a:Ldc/k;

    invoke-static {v0, v2}, Lwb/a;->i(Lwb/a;Ldc/k;)V

    iget-object v0, p0, Lwb/a$a;->c:Lwb/a;

    invoke-static {v0, v1}, Lwb/a;->p(Lwb/a;I)V

    return-void

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lwb/a$a;->c:Lwb/a;

    invoke-static {v2}, Lwb/a;->m(Lwb/a;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected final m(Z)V
    .locals 0

    iput-boolean p1, p0, Lwb/a$a;->b:Z

    return-void
.end method

.method public q(Ldc/e;J)J
    .locals 1

    const-string v0, "sink"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    :try_start_0
    iget-object v0, p0, Lwb/a$a;->c:Lwb/a;

    invoke-static {v0}, Lwb/a;->l(Lwb/a;)Ldc/g;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Ldc/y;->q(Ldc/e;J)J

    move-result-wide p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-wide p1

    :catch_0
    move-exception p1

    iget-object p2, p0, Lwb/a$a;->c:Lwb/a;

    invoke-virtual {p2}, Lwb/a;->h()Lub/f;

    move-result-object p2

    invoke-virtual {p2}, Lub/f;->A()V

    invoke-virtual {p0}, Lwb/a$a;->g()V

    throw p1
.end method
