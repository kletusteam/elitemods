.class public final Lt5/n;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lt5/n$a;
    }
.end annotation


# static fields
.field public static final b:Lt5/n$a;

.field private static final c:Lt5/n;


# instance fields
.field private a:Lorg/json/JSONObject;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lt5/n$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lt5/n$a;-><init>(Lm8/g;)V

    sput-object v0, Lt5/n;->b:Lt5/n$a;

    new-instance v0, Lt5/n;

    invoke-direct {v0}, Lt5/n;-><init>()V

    sput-object v0, Lt5/n;->c:Lt5/n;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    iput-object v0, p0, Lt5/n;->a:Lorg/json/JSONObject;

    return-void
.end method

.method public static synthetic a(Lt5/n;)V
    .locals 0

    invoke-static {p0}, Lt5/n;->g(Lt5/n;)V

    return-void
.end method

.method public static final synthetic b()Lt5/n;
    .locals 1

    sget-object v0, Lt5/n;->c:Lt5/n;

    return-object v0
.end method

.method public static final e()Lt5/n;
    .locals 1

    sget-object v0, Lt5/n;->b:Lt5/n$a;

    invoke-virtual {v0}, Lt5/n$a;->b()Lt5/n;

    move-result-object v0

    return-object v0
.end method

.method private final f()V
    .locals 2

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object v0

    new-instance v1, Lt5/m;

    invoke-direct {v1, p0}, Lt5/m;-><init>(Lt5/n;)V

    invoke-virtual {v0, v1}, Lf6/z;->g(Ljava/lang/Runnable;)V

    return-void
.end method

.method private static final g(Lt5/n;)V
    .locals 4

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Lcom/android/packageinstaller/InstallerApplication;->g()Lcom/android/packageinstaller/InstallerApplication;

    move-result-object v0

    const-string v1, "experiments_manager"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/Application;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    const-string v3, ""

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lt5/n;->a:Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method private final i(Lorg/json/JSONObject;)V
    .locals 3

    const-string v0, "data"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object p1

    if-eqz p1, :cond_0

    iput-object p1, p0, Lt5/n;->a:Lorg/json/JSONObject;

    invoke-static {}, Lcom/android/packageinstaller/InstallerApplication;->g()Lcom/android/packageinstaller/InstallerApplication;

    move-result-object v0

    const/4 v1, 0x0

    const-string v2, "experiments_manager"

    invoke-virtual {v0, v2, v1}, Landroid/app/Application;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_0
    return-void
.end method


# virtual methods
.method public final c()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lt5/n;->a:Lorg/json/JSONObject;

    const-string v1, "usedExpId"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "mSavedConfig.optString(\"usedExpId\", \"\")"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final d(Ljava/lang/String;)Lt5/l;
    .locals 3

    const-string v0, "experimentName"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lt5/l;

    iget-object v1, p0, Lt5/n;->a:Lorg/json/JSONObject;

    const-string v2, ""

    invoke-virtual {v1, p1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v1, "mSavedConfig.optString(experimentName, \"\")"

    invoke-static {p1, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p1}, Lt5/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public final h()V
    .locals 0

    invoke-direct {p0}, Lt5/n;->f()V

    return-void
.end method

.method public final j([Ljava/lang/String;Ld8/d;)Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            "Ld8/d<",
            "-",
            "La8/v;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    instance-of v0, p2, Lt5/n$b;

    if-eqz v0, :cond_0

    move-object v0, p2

    check-cast v0, Lt5/n$b;

    iget v1, v0, Lt5/n$b;->g:I

    const/high16 v2, -0x80000000

    and-int v3, v1, v2

    if-eqz v3, :cond_0

    sub-int/2addr v1, v2

    iput v1, v0, Lt5/n$b;->g:I

    goto :goto_0

    :cond_0
    new-instance v0, Lt5/n$b;

    invoke-direct {v0, p0, p2}, Lt5/n$b;-><init>(Lt5/n;Ld8/d;)V

    :goto_0
    iget-object p2, v0, Lt5/n$b;->e:Ljava/lang/Object;

    invoke-static {}, Le8/b;->c()Ljava/lang/Object;

    move-result-object v1

    iget v2, v0, Lt5/n$b;->g:I

    const/4 v3, 0x1

    if-eqz v2, :cond_2

    if-ne v2, v3, :cond_1

    iget-object p1, v0, Lt5/n$b;->d:Ljava/lang/Object;

    check-cast p1, Lt5/n;

    :try_start_0
    invoke-static {p2}, La8/n;->b(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    invoke-static {p2}, La8/n;->b(Ljava/lang/Object;)V

    :try_start_1
    const-class p2, Lu5/b;

    invoke-static {p2}, Lu5/k;->f(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lu5/b;

    invoke-virtual {p0}, Lt5/n;->c()Ljava/lang/String;

    move-result-object v2

    sget-object v4, Lt5/n;->b:Lt5/n$a;

    invoke-static {p1}, Lb8/d;->r([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {v4, p1}, Lt5/n$a;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object p1

    iput-object p0, v0, Lt5/n$b;->d:Ljava/lang/Object;

    iput v3, v0, Lt5/n$b;->g:I

    invoke-interface {p2, v2, p1, v0}, Lu5/b;->e(Ljava/lang/String;Ljava/lang/String;Ld8/d;)Ljava/lang/Object;

    move-result-object p2

    if-ne p2, v1, :cond_3

    return-object v1

    :cond_3
    move-object p1, p0

    :goto_1
    check-cast p2, Lgc/t;

    invoke-virtual {p2}, Lgc/t;->d()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p2}, Lgc/t;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lorg/json/JSONObject;

    if-eqz p2, :cond_4

    invoke-direct {p1, p2}, Lt5/n;->i(Lorg/json/JSONObject;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :cond_4
    :goto_2
    sget-object p1, La8/v;->a:La8/v;

    return-object p1
.end method
