.class public final Lt5/k0;
.super Ljava/lang/Object;


# static fields
.field public static final a:Lt5/k0;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lt5/k0;

    invoke-direct {v0}, Lt5/k0;-><init>()V

    sput-object v0, Lt5/k0;->a:Lt5/k0;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/miui/packageInstaller/model/InstallSourceTips;Landroid/content/Context;Lm5/e;Lcom/miui/packageInstaller/model/ApkInfo;)La6/c0;
    .locals 2

    const-string v0, "context"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mCallingPackage"

    invoke-static {p3, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    if-nez p1, :cond_0

    new-instance p1, La6/b0;

    invoke-direct {p1, p2, p3}, La6/b0;-><init>(Landroid/content/Context;Lm5/e;)V

    return-object p1

    :cond_0
    invoke-virtual {p1}, Lcom/miui/packageInstaller/model/InstallSourceTips;->getType()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    new-instance p1, La6/b0;

    invoke-direct {p1, p2, p3}, La6/b0;-><init>(Landroid/content/Context;Lm5/e;)V

    goto :goto_0

    :cond_1
    new-instance v0, La6/o;

    invoke-direct {v0, p2, p1, p3, p4}, La6/o;-><init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/InstallSourceTips;Lm5/e;Lcom/miui/packageInstaller/model/ApkInfo;)V

    move-object p1, v0

    :goto_0
    return-object p1
.end method
