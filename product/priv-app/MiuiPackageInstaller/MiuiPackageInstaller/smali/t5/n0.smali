.class public final Lt5/n0;
.super Lt5/a0;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lt5/n0$a;
    }
.end annotation


# instance fields
.field private c:Lcom/miui/packageInstaller/model/ApkInfo;

.field private d:Lcom/miui/packageInstaller/model/Virus;

.field private final e:Lm5/e;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/miui/packageInstaller/model/ApkInfo;Lcom/miui/packageInstaller/model/Virus;Lm5/e;)V
    .locals 1

    const-string v0, "mContext"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "apkinfo"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "virus"

    invoke-static {p3, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "caller"

    invoke-static {p4, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lt5/a0;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lt5/n0;->c:Lcom/miui/packageInstaller/model/ApkInfo;

    iput-object p3, p0, Lt5/n0;->d:Lcom/miui/packageInstaller/model/Virus;

    iput-object p4, p0, Lt5/n0;->e:Lm5/e;

    new-instance p2, Lt5/a;

    iget-object p3, p0, Lt5/n0;->c:Lcom/miui/packageInstaller/model/ApkInfo;

    const/4 v0, 0x1

    invoke-direct {p2, p1, v0, p3, p4}, Lt5/a;-><init>(Landroid/content/Context;ILcom/miui/packageInstaller/model/ApkInfo;Lm5/e;)V

    invoke-virtual {p0, p2}, Lt5/a0;->c(Lt5/g0;)V

    return-void
.end method

.method public static synthetic j(Lt5/n0;Lt5/g0$a;Landroid/content/DialogInterface;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lt5/n0;->n(Lt5/n0;Lt5/g0$a;Landroid/content/DialogInterface;)V

    return-void
.end method

.method public static final synthetic k(Lt5/n0;)Z
    .locals 0

    invoke-direct {p0}, Lt5/n0;->l()Z

    move-result p0

    return p0
.end method

.method private final l()Z
    .locals 4

    iget-object v0, p0, Lt5/n0;->c:Lcom/miui/packageInstaller/model/ApkInfo;

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/ApkInfo;->getCloudParams()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    iget-boolean v0, v0, Lcom/miui/packageInstaller/model/CloudParams;->storeListed:Z

    if-ne v0, v1, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    move v0, v2

    :goto_0
    if-eqz v0, :cond_3

    iget-object v0, p0, Lt5/n0;->c:Lcom/miui/packageInstaller/model/ApkInfo;

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/ApkInfo;->getCloudParams()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v0

    const/4 v3, 0x0

    if-eqz v0, :cond_1

    iget-object v0, v0, Lcom/miui/packageInstaller/model/CloudParams;->positiveButtonTip:Lcom/miui/packageInstaller/model/PositiveButtonRules;

    if-eqz v0, :cond_1

    iget-object v0, v0, Lcom/miui/packageInstaller/model/PositiveButtonRules;->actionUrl:Ljava/lang/String;

    goto :goto_1

    :cond_1
    move-object v0, v3

    :goto_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lt5/n0;->c:Lcom/miui/packageInstaller/model/ApkInfo;

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/ApkInfo;->getCloudParams()Lcom/miui/packageInstaller/model/CloudParams;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, v0, Lcom/miui/packageInstaller/model/CloudParams;->positiveButtonTip:Lcom/miui/packageInstaller/model/PositiveButtonRules;

    if-eqz v0, :cond_2

    iget-object v3, v0, Lcom/miui/packageInstaller/model/PositiveButtonRules;->method:Ljava/lang/String;

    :cond_2
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    goto :goto_2

    :cond_3
    move v1, v2

    :goto_2
    return v1
.end method

.method private final m(Lt5/g0$a;)V
    .locals 3

    new-instance v0, Lt5/n0$a;

    invoke-virtual {p0}, Lt5/g0;->b()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lt5/n0;->d:Lcom/miui/packageInstaller/model/Virus;

    invoke-direct {v0, p0, v1, v2, p1}, Lt5/n0$a;-><init>(Lt5/n0;Landroid/content/Context;Lcom/miui/packageInstaller/model/Virus;Lt5/g0$a;)V

    :try_start_0
    new-instance v1, Lt5/l0;

    invoke-direct {v1, p0, p1}, Lt5/l0;-><init>(Lt5/n0;Lt5/g0$a;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method private static final n(Lt5/n0;Lt5/g0$a;Landroid/content/DialogInterface;)V
    .locals 3

    const-string p2, "this$0"

    invoke-static {p0, p2}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "$authorizeListener"

    invoke-static {p1, p2}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p2, Lp5/b;

    invoke-virtual {p0}, Lt5/g0;->b()Landroid/content/Context;

    move-result-object v0

    const-string v1, "null cannot be cast to non-null type com.miui.packageInstaller.analytics.IPage"

    invoke-static {v0, v1}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lo5/a;

    const-string v1, "virus_cue_popup_back_btn"

    const-string v2, "button"

    invoke-direct {p2, v1, v2, v0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p2}, Lp5/f;->c()Z

    invoke-interface {p1, p0}, Lt5/g0$a;->b(Lt5/g0;)V

    return-void
.end method


# virtual methods
.method public a(Lt5/g0$a;)V
    .locals 1

    const-string v0, "authorizeListener"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lt5/n0;->m(Lt5/g0$a;)V

    return-void
.end method
