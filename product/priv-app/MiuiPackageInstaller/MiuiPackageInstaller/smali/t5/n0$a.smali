.class final Lt5/n0$a;
.super La6/e;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lt5/n0;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "a"
.end annotation


# instance fields
.field private k:Landroid/content/Context;

.field private l:Lcom/miui/packageInstaller/model/Virus;

.field private m:Lt5/g0$a;

.field final synthetic n:Lt5/n0;


# direct methods
.method public constructor <init>(Lt5/n0;Landroid/content/Context;Lcom/miui/packageInstaller/model/Virus;Lt5/g0$a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/miui/packageInstaller/model/Virus;",
            "Lt5/g0$a;",
            ")V"
        }
    .end annotation

    const-string v0, "mContext"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "virus"

    invoke-static {p3, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p4, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lt5/n0$a;->n:Lt5/n0;

    invoke-direct {p0, p2}, La6/e;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lt5/n0$a;->k:Landroid/content/Context;

    iput-object p3, p0, Lt5/n0$a;->l:Lcom/miui/packageInstaller/model/Virus;

    iput-object p4, p0, Lt5/n0$a;->m:Lt5/g0$a;

    return-void
.end method

.method public static synthetic g(Lt5/n0;Lt5/n0$a;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lt5/n0$a;->j(Lt5/n0;Lt5/n0$a;Landroid/view/View;)V

    return-void
.end method

.method private static final j(Lt5/n0;Lt5/n0$a;Landroid/view/View;)V
    .locals 1

    const-string p2, "this$0"

    invoke-static {p0, p2}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "this$1"

    invoke-static {p1, p2}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0}, Lt5/n0;->k(Lt5/n0;)Z

    move-result p2

    if-eqz p2, :cond_0

    iget-object p1, p1, Lt5/n0$a;->m:Lt5/g0$a;

    invoke-interface {p1, p0}, Lt5/g0$a;->c(Lt5/g0;)V

    goto :goto_0

    :cond_0
    iget-object p0, p1, Lt5/n0$a;->k:Landroid/content/Context;

    const-string p2, "null cannot be cast to non-null type android.app.Activity"

    invoke-static {p0, p2}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p0, Landroid/app/Activity;

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    new-instance p0, Lp5/b;

    iget-object p1, p1, Lt5/n0$a;->k:Landroid/content/Context;

    const-string p2, "null cannot be cast to non-null type com.miui.packageInstaller.analytics.IPage"

    invoke-static {p1, p2}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lo5/a;

    const-string p2, "virus_cue_popup_cancel_install_btn"

    const-string v0, "button"

    invoke-direct {p0, p2, v0, p1}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p0}, Lp5/f;->c()Z

    :goto_0
    return-void
.end method


# virtual methods
.method public final h()Lt5/g0$a;
    .locals 1

    iget-object v0, p0, Lt5/n0$a;->m:Lt5/g0$a;

    return-object v0
.end method

.method public final i()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lt5/n0$a;->k:Landroid/content/Context;

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 12

    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result p1

    if-nez p1, :cond_1

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->u()Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const p1, 0x7f0d009e

    goto :goto_1

    :cond_1
    :goto_0
    const p1, 0x7f0d009f

    :goto_1
    invoke-virtual {p0, p1}, Landroid/app/Dialog;->setContentView(I)V

    const p1, 0x7f0a02d1

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->u()Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_2

    :cond_2
    const v0, 0x7f080139

    goto :goto_3

    :cond_3
    :goto_2
    const v0, 0x7f080143

    :goto_3
    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    const p1, 0x7f0a03ef

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f1103f1

    const/4 v2, 0x1

    new-array v3, v2, [Ljava/lang/Object;

    iget-object v4, p0, Lt5/n0$a;->l:Lcom/miui/packageInstaller/model/Virus;

    iget-object v4, v4, Lcom/miui/packageInstaller/model/Virus;->name:Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const p1, 0x7f0a0255

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iget-object v0, p0, Lt5/n0$a;->l:Lcom/miui/packageInstaller/model/Virus;

    iget-object v0, v0, Lcom/miui/packageInstaller/model/Virus;->virusInfo:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const p1, 0x7f0a02af

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    iget-object v0, p0, Lt5/n0$a;->n:Lt5/n0;

    invoke-static {v0}, Lt5/n0;->k(Lt5/n0;)Z

    move-result v0

    const-string v1, "null cannot be cast to non-null type com.miui.packageInstaller.analytics.IPage"

    const-string v3, "button"

    if-eqz v0, :cond_4

    iget-object v0, p0, Lt5/n0$a;->k:Landroid/content/Context;

    const v4, 0x7f110302

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_4

    :cond_4
    iget-object v0, p0, Lt5/n0$a;->k:Landroid/content/Context;

    const v4, 0x7f110055

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    new-instance v0, Lp5/g;

    iget-object v4, p0, Lt5/n0$a;->k:Landroid/content/Context;

    invoke-static {v4, v1}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v4, Lo5/a;

    const-string v6, "virus_cue_popup_cancel_install_btn"

    invoke-direct {v0, v6, v3, v4}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    :goto_4
    iget-object v0, p0, Lt5/n0$a;->n:Lt5/n0;

    new-instance v4, Lt5/m0;

    invoke-direct {v4, v0, p0}, Lt5/m0;-><init>(Lt5/n0;Lt5/n0$a;)V

    invoke-virtual {p1, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance p1, Lp5/g;

    iget-object v0, p0, Lt5/n0$a;->k:Landroid/content/Context;

    invoke-static {v0, v1}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lo5/a;

    const-string v1, "virus_cue_popup_install_btn"

    invoke-direct {p1, v1, v3, v0}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p1}, Lp5/f;->c()Z

    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const v0, 0x7f060501

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/res/Resources;->getColor(ILandroid/content/res/Resources$Theme;)I

    move-result p1

    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f060502

    invoke-virtual {v0, v3, v1}, Landroid/content/res/Resources;->getColor(ILandroid/content/res/Resources$Theme;)I

    move-result v0

    new-instance v1, Lt5/n0$a$a;

    iget-object v3, p0, Lt5/n0$a;->n:Lt5/n0;

    invoke-direct {v1, p1, v0, v3, p0}, Lt5/n0$a$a;-><init>(IILt5/n0;Lt5/n0$a;)V

    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object p1

    const v0, 0x7f11016b

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string v0, "getContext().getString(R\u2026s_install_text_clickable)"

    invoke-static {p1, v0}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v0

    const v3, 0x7f11016a

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v5

    invoke-virtual {v0, v3, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    const-string v0, "getContext().getString(R\u2026ll_text, clickAbleString)"

    invoke-static {v6, v0}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0, v6}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x6

    const/4 v11, 0x0

    move-object v7, p1

    invoke-static/range {v6 .. v11}, Lu8/g;->L(Ljava/lang/CharSequence;Ljava/lang/String;IZILjava/lang/Object;)I

    move-result v2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    add-int/2addr p1, v2

    const/16 v3, 0x21

    invoke-virtual {v0, v1, v2, p1, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    const p1, 0x7f0a01ae

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    invoke-static {}, Ln2/a;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
