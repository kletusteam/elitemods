.class public Lt5/a0;
.super Lt5/g0;


# instance fields
.field private final b:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList<",
            "Lt5/g0;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const-string v0, "mContext"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lt5/g0;-><init>(Landroid/content/Context;)V

    new-instance p1, Ljava/util/LinkedList;

    invoke-direct {p1}, Ljava/util/LinkedList;-><init>()V

    iput-object p1, p0, Lt5/a0;->b:Ljava/util/LinkedList;

    return-void
.end method


# virtual methods
.method public a(Lt5/g0$a;)V
    .locals 1

    const-string v0, "authorizeListener"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lt5/a0;->d(Lt5/g0$a;)V

    return-void
.end method

.method public final c(Lt5/g0;)V
    .locals 1

    const-string v0, "authorize"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lt5/a0;->b:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final d(Lt5/g0$a;)V
    .locals 2

    const-string v0, "authorizeListener"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lt5/a0;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1, p0}, Lt5/g0$a;->a(Lt5/g0;)V

    return-void

    :cond_0
    iget-object v0, p0, Lt5/a0;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lt5/g0;

    const-string v1, "authorize"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lt5/a0;->h(Lt5/g0;)V

    new-instance v1, Lt5/a0$a;

    invoke-direct {v1, p0, p1}, Lt5/a0$a;-><init>(Lt5/a0;Lt5/g0$a;)V

    invoke-virtual {v0, v1}, Lt5/g0;->a(Lt5/g0$a;)V

    return-void
.end method

.method public e(Lt5/g0;)V
    .locals 1

    const-string v0, "authorize"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public f(Lt5/g0;)V
    .locals 1

    const-string v0, "authorize"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public g(Lt5/g0;)V
    .locals 1

    const-string v0, "authorize"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public h(Lt5/g0;)V
    .locals 1

    const-string v0, "authorize"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public i(Lt5/g0;)V
    .locals 1

    const-string v0, "authorize"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method
