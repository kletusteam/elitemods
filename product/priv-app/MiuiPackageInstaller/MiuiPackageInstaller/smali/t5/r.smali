.class public final Lt5/r;
.super Ljava/lang/Object;


# static fields
.field public static final a:Lt5/r;

.field private static b:Ly4/a;

.field private static c:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Lt5/r;

    invoke-direct {v0}, Lt5/r;-><init>()V

    sput-object v0, Lt5/r;->a:Lt5/r;

    const/4 v0, 0x1

    :try_start_0
    new-instance v1, Lz4/b;

    invoke-static {}, Lcom/android/packageinstaller/InstallerApplication;->g()Lcom/android/packageinstaller/InstallerApplication;

    move-result-object v2

    const-string v3, "app_security_risk_app.db"

    invoke-direct {v1, v2, v3, v0}, Lz4/b;-><init>(Landroid/content/Context;Ljava/lang/String;I)V

    iput-boolean v0, v1, Lz4/b;->g:Z

    const/4 v2, 0x0

    iput-boolean v2, v1, Lz4/b;->b:Z

    invoke-static {v1}, Ly4/a;->C(Lz4/b;)Ly4/a;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ly4/a;->D()V

    :cond_0
    sput-object v1, Lt5/r;->b:Ly4/a;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v2, "InstallHistoryManager"

    const-string v3, "DB instance create failed, DB lock down!"

    invoke-static {v2, v3, v1}, Lf6/o;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v1, 0x0

    sput-object v1, Lt5/r;->b:Ly4/a;

    sput-boolean v0, Lt5/r;->c:Z

    :goto_0
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static synthetic a(Ly4/a;Lb5/i;)V
    .locals 0

    invoke-static {p0, p1}, Lt5/r;->f(Ly4/a;Lb5/i;)V

    return-void
.end method

.method public static synthetic b(Ly4/a;Lcom/miui/packageInstaller/model/InstallHistory;)V
    .locals 0

    invoke-static {p0, p1}, Lt5/r;->i(Ly4/a;Lcom/miui/packageInstaller/model/InstallHistory;)V

    return-void
.end method

.method public static synthetic c(Ly4/a;Lb5/i;Le5/a;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lt5/r;->k(Ly4/a;Lb5/i;Le5/a;)V

    return-void
.end method

.method public static final e()V
    .locals 8

    sget-object v0, Lt5/r;->b:Ly4/a;

    if-eqz v0, :cond_0

    new-instance v1, Lb5/i;

    const-class v2, Lcom/miui/packageInstaller/model/InstallHistory;

    invoke-direct {v1, v2}, Lb5/i;-><init>(Ljava/lang/Class;)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-wide v6, 0x9a7ec800L

    sub-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const-string v3, "install_time< ?"

    invoke-virtual {v1, v3, v2}, Lb5/i;->j(Ljava/lang/String;[Ljava/lang/Object;)Lb5/i;

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object v2

    new-instance v3, Lt5/o;

    invoke-direct {v3, v0, v1}, Lt5/o;-><init>(Ly4/a;Lb5/i;)V

    invoke-virtual {v2, v3}, Lf6/z;->g(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.method private static final f(Ly4/a;Lb5/i;)V
    .locals 1

    const-string v0, "$db"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$wb"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p0, p1}, Lz4/a;->p(Lb5/i;)I

    return-void
.end method

.method private static final i(Ly4/a;Lcom/miui/packageInstaller/model/InstallHistory;)V
    .locals 1

    const-string v0, "$db"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$installHistory"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p0, p1}, Lz4/a;->g(Ljava/lang/Object;)J

    return-void
.end method

.method private static final k(Ly4/a;Lb5/i;Le5/a;)V
    .locals 1

    const-string v0, "$db"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$wb"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$columns"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Le5/b;->d:Le5/b;

    invoke-virtual {p0, p1, p2, v0}, Ly4/a;->J(Lb5/i;Le5/a;Le5/b;)I

    return-void
.end method


# virtual methods
.method public final d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    const-string v0, "installerPackageName"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "packageName"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "label"

    invoke-static {p3, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/miui/packageInstaller/model/InstallHistory;

    invoke-direct {v0, p1, p2, p3}, Lcom/miui/packageInstaller/model/InstallHistory;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lt5/r;->h(Lcom/miui/packageInstaller/model/InstallHistory;)V

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/InstallHistory;->getId()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final g(Ljava/lang/String;J)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "J)",
            "Ljava/util/List<",
            "Lcom/miui/packageInstaller/model/InstallHistory;",
            ">;"
        }
    .end annotation

    const-string v0, "packageName"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sget-object v1, Lt5/r;->b:Ly4/a;

    if-eqz v1, :cond_0

    new-instance v2, Lb5/d;

    const-class v3, Lcom/miui/packageInstaller/model/InstallHistory;

    invoke-direct {v2, v3}, Lb5/d;-><init>(Ljava/lang/Class;)V

    const/4 v3, 0x1

    new-array v4, v3, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const-string p1, "installer_package_name= ?"

    invoke-virtual {v2, p1, v4}, Lb5/d;->i(Ljava/lang/String;[Ljava/lang/Object;)Lb5/d;

    new-array p1, v3, [Ljava/lang/Object;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    sub-long/2addr v3, p2

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    aput-object p2, p1, v5

    const-string p2, "install_time> ?"

    invoke-virtual {v2, p2, p1}, Lb5/d;->j(Ljava/lang/String;[Ljava/lang/Object;)Lb5/d;

    invoke-interface {v1, v2}, Lz4/a;->r(Lb5/d;)Ljava/util/ArrayList;

    move-result-object p1

    if-eqz p1, :cond_0

    const-string p2, "query(qb)"

    invoke-static {p1, p2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_0
    return-object v0
.end method

.method public final h(Lcom/miui/packageInstaller/model/InstallHistory;)V
    .locals 3

    const-string v0, "installHistory"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lt5/r;->b:Ly4/a;

    if-eqz v0, :cond_0

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object v1

    new-instance v2, Lt5/q;

    invoke-direct {v2, v0, p1}, Lt5/q;-><init>(Ly4/a;Lcom/miui/packageInstaller/model/InstallHistory;)V

    invoke-virtual {v1, v2}, Lf6/z;->g(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.method public final j(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    const-string v0, "id"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "result"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lt5/r;->b:Ly4/a;

    if-eqz v0, :cond_0

    new-instance v1, Lb5/i;

    const-class v2, Lcom/miui/packageInstaller/model/InstallHistory;

    invoke-direct {v1, v2}, Lb5/i;-><init>(Ljava/lang/Class;)V

    const/4 v2, 0x1

    new-array v3, v2, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const-string p1, "id= ?"

    invoke-virtual {v1, p1, v3}, Lb5/i;->j(Ljava/lang/String;[Ljava/lang/Object;)Lb5/i;

    new-instance p1, Le5/a;

    const-string v3, "install_result"

    filled-new-array {v3}, [Ljava/lang/String;

    move-result-object v3

    new-array v2, v2, [Ljava/lang/String;

    aput-object p2, v2, v4

    invoke-direct {p1, v3, v2}, Le5/a;-><init>([Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object p2

    new-instance v2, Lt5/p;

    invoke-direct {v2, v0, v1, p1}, Lt5/p;-><init>(Ly4/a;Lb5/i;Le5/a;)V

    invoke-virtual {p2, v2}, Lf6/z;->g(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method
