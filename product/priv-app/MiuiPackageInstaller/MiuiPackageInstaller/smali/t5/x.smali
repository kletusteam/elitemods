.class public final Lt5/x;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lt5/x$a;
    }
.end annotation


# static fields
.field public static final j:Lt5/x$a;

.field private static final k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Lm5/z0;

.field private final b:Ljava/lang/String;

.field private c:Ll8/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ll8/a<",
            "La8/v;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Landroid/os/UserManager;

.field private final e:Landroid/app/AppOpsManager;

.field private final f:La8/f;

.field private final g:Landroid/content/pm/PackageManager;

.field private h:Lcom/miui/packageInstaller/model/ApkInfo;

.field private final i:Landroid/content/DialogInterface$OnCancelListener;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lt5/x$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lt5/x$a;-><init>(Lm8/g;)V

    sput-object v0, Lt5/x;->j:Lt5/x$a;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lt5/x;->k:Ljava/util/List;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sput-object v1, Lt5/x;->l:Ljava/util/List;

    const-string v2, "com.xiaomi.mihomemanager"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v0, "com.miui.securityadd"

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public constructor <init>(Lm5/z0;)V
    .locals 2

    const-string v0, "mInstallerActivity"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lt5/x;->a:Lm5/z0;

    const-string v0, "InstallSourceManager"

    iput-object v0, p0, Lt5/x;->b:Ljava/lang/String;

    new-instance v0, Lt5/x$b;

    invoke-direct {v0, p0}, Lt5/x$b;-><init>(Lt5/x;)V

    invoke-static {v0}, La8/g;->b(Ll8/a;)La8/f;

    move-result-object v0

    iput-object v0, p0, Lt5/x;->f:La8/f;

    new-instance v0, Lt5/s;

    invoke-direct {v0, p0}, Lt5/s;-><init>(Lt5/x;)V

    iput-object v0, p0, Lt5/x;->i:Landroid/content/DialogInterface$OnCancelListener;

    const-string v0, "user"

    invoke-virtual {p1, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    const-string v1, "null cannot be cast to non-null type android.os.UserManager"

    invoke-static {v0, v1}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/os/UserManager;

    iput-object v0, p0, Lt5/x;->d:Landroid/os/UserManager;

    const-string v0, "appops"

    invoke-virtual {p1, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    const-string v1, "null cannot be cast to non-null type android.app.AppOpsManager"

    invoke-static {v0, v1}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/app/AppOpsManager;

    iput-object v0, p0, Lt5/x;->e:Landroid/app/AppOpsManager;

    invoke-virtual {p1}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object p1

    const-string v0, "mInstallerActivity.packageManager"

    invoke-static {p1, v0}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lt5/x;->g:Landroid/content/pm/PackageManager;

    return-void
.end method

.method public static synthetic a(Lt5/x;)V
    .locals 0

    invoke-static {p0}, Lt5/x;->u(Lt5/x;)V

    return-void
.end method

.method public static synthetic b(Lt5/x;Landroid/content/DialogInterface;)V
    .locals 0

    invoke-static {p0, p1}, Lt5/x;->j(Lt5/x;Landroid/content/DialogInterface;)V

    return-void
.end method

.method public static synthetic c(Lt5/x;I)V
    .locals 0

    invoke-static {p0, p1}, Lt5/x;->w(Lt5/x;I)V

    return-void
.end method

.method public static synthetic d(Lt5/x;)V
    .locals 0

    invoke-static {p0}, Lt5/x;->n(Lt5/x;)V

    return-void
.end method

.method public static synthetic e(Lt5/x;)V
    .locals 0

    invoke-static {p0}, Lt5/x;->l(Lt5/x;)V

    return-void
.end method

.method public static final synthetic f(Lt5/x;)Lm5/z0;
    .locals 0

    iget-object p0, p0, Lt5/x;->a:Lm5/z0;

    return-object p0
.end method

.method public static final synthetic g(Lt5/x;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lt5/x;->b:Ljava/lang/String;

    return-object p0
.end method

.method public static final synthetic h(Lt5/x;)V
    .locals 0

    invoke-direct {p0}, Lt5/x;->t()V

    return-void
.end method

.method public static final synthetic i(Lt5/x;ZZ)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lt5/x;->z(ZZ)V

    return-void
.end method

.method private static final j(Lt5/x;Landroid/content/DialogInterface;)V
    .locals 3

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Lp5/b;

    iget-object v0, p0, Lt5/x;->a:Lm5/z0;

    const-string v1, "file_manager_authorize_popup_back_btn"

    const-string v2, "button"

    invoke-direct {p1, v1, v2, v0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p1}, Lp5/f;->c()Z

    iget-object p0, p0, Lt5/x;->a:Lm5/z0;

    invoke-virtual {p0}, Lmiuix/appcompat/app/j;->finish()V

    return-void
.end method

.method private static final l(Lt5/x;)V
    .locals 6

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lf6/u;->a:Lf6/u$a;

    invoke-direct {p0}, Lt5/x;->p()Lm5/e;

    move-result-object v1

    invoke-virtual {v0, v1}, Lf6/u$a;->e(Lm5/e;)V

    invoke-direct {p0}, Lt5/x;->p()Lm5/e;

    move-result-object v0

    invoke-virtual {v0}, Lm5/e;->k()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lt5/x;->h:Lcom/miui/packageInstaller/model/ApkInfo;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/ApkInfo;->getPackageName()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-static {v0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lt5/x;->t()V

    return-void

    :cond_1
    iget-object v0, p0, Lt5/x;->a:Lm5/z0;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x0

    const-string v2, "skip_unknown_source_dialog"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lt5/x;->t()V

    return-void

    :cond_2
    iget-object v0, p0, Lt5/x;->a:Lm5/z0;

    const/4 v1, 0x1

    invoke-static {v1, v0}, Lcom/android/packageinstaller/compat/UnknownSourceCompat;->isUnknownSourcesEnabled(ZLm5/z0;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-direct {p0, v1}, Lt5/x;->v(I)V

    return-void

    :cond_3
    iget-object v0, p0, Lt5/x;->a:Lm5/z0;

    invoke-static {v0}, Lcom/android/packageinstaller/compat/UnknownSourceCompat;->checkUnknownSourcesEnabled(Lm5/z0;)Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lt5/x;->e:Landroid/app/AppOpsManager;

    invoke-direct {p0}, Lt5/x;->p()Lm5/e;

    move-result-object v1

    invoke-virtual {v1}, Lm5/e;->l()I

    move-result v1

    invoke-direct {p0}, Lt5/x;->p()Lm5/e;

    move-result-object v2

    invoke-virtual {v2}, Lm5/e;->k()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x42

    invoke-static {v0, v3, v1, v2}, Lcom/android/packageinstaller/compat/AppOpsManagerCompat;->checkOpNoThrow(Landroid/app/AppOpsManager;IILjava/lang/String;)I

    move-result v0

    iget-object v1, p0, Lt5/x;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "appOpMode="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v0, :cond_6

    const/16 v1, 0x9

    const/4 v2, 0x2

    if-eq v0, v2, :cond_5

    const/4 v4, 0x3

    if-eq v0, v4, :cond_4

    iget-object v1, p0, Lt5/x;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid app op mode "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, " for OP_REQUEST_INSTALL_PACKAGES found for uid "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-direct {p0}, Lt5/x;->p()Lm5/e;

    move-result-object v0

    invoke-virtual {v0}, Lm5/e;->l()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object p0, p0, Lt5/x;->a:Lm5/z0;

    invoke-virtual {p0}, Lmiuix/appcompat/app/j;->finish()V

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lt5/x;->e:Landroid/app/AppOpsManager;

    invoke-direct {p0}, Lt5/x;->p()Lm5/e;

    move-result-object v4

    invoke-virtual {v4}, Lm5/e;->l()I

    move-result v4

    invoke-direct {p0}, Lt5/x;->p()Lm5/e;

    move-result-object v5

    invoke-virtual {v5}, Lm5/e;->k()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v3, v4, v5, v2}, Lcom/android/packageinstaller/compat/AppOpsManagerCompat;->setMode(Landroid/app/AppOpsManager;IILjava/lang/String;I)V

    :cond_5
    invoke-direct {p0, v1}, Lt5/x;->v(I)V

    goto :goto_1

    :cond_6
    invoke-direct {p0}, Lt5/x;->t()V

    :goto_1
    return-void
.end method

.method private static final n(Lt5/x;)V
    .locals 3

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lt5/x;->q()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lt5/x;->t()V

    return-void

    :cond_0
    iget-object v0, p0, Lt5/x;->a:Lm5/z0;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "skip_unknown_source_dialog"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lt5/x;->t()V

    return-void

    :cond_1
    invoke-direct {p0}, Lt5/x;->r()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lt5/x;->a:Lm5/z0;

    invoke-static {v0}, Lcom/android/packageinstaller/compat/SettingsCompat;->isIntentSafe(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lt5/x;->a:Lm5/z0;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.settings.SHOW_ADMIN_SUPPORT_DETAILS"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    :cond_2
    iget-object p0, p0, Lt5/x;->a:Lm5/z0;

    invoke-virtual {p0}, Lmiuix/appcompat/app/j;->finish()V

    return-void

    :cond_3
    invoke-direct {p0}, Lt5/x;->p()Lm5/e;

    move-result-object v0

    invoke-virtual {v0}, Lm5/e;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    invoke-direct {p0}, Lt5/x;->p()Lm5/e;

    move-result-object v0

    invoke-virtual {v0}, Lm5/e;->m()Ljava/lang/Boolean;

    move-result-object v0

    const-string v1, "mCallingPackage.isSystemApp"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, Lt5/x;->l:Ljava/util/List;

    invoke-direct {p0}, Lt5/x;->p()Lm5/e;

    move-result-object v1

    invoke-virtual {v1}, Lm5/e;->j()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-direct {p0}, Lt5/x;->t()V

    return-void

    :cond_4
    sget-object v0, Lt5/x;->k:Ljava/util/List;

    iget-object v1, p0, Lt5/x;->h:Lcom/miui/packageInstaller/model/ApkInfo;

    invoke-static {v1}, Lm8/i;->c(Ljava/lang/Object;)V

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/ApkInfo;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lt5/x;->h:Lcom/miui/packageInstaller/model/ApkInfo;

    invoke-static {v0}, Lm8/i;->c(Ljava/lang/Object;)V

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/ApkInfo;->getPackageInfo()Landroid/content/pm/PackageInfo;

    move-result-object v0

    invoke-static {v0}, Lm8/i;->c(Ljava/lang/Object;)V

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->sharedUserId:Ljava/lang/String;

    const-string v1, "android.uid.system"

    invoke-static {v1, v0}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-direct {p0}, Lt5/x;->t()V

    return-void

    :cond_5
    invoke-direct {p0}, Lt5/x;->p()Lm5/e;

    move-result-object v0

    invoke-virtual {v0}, Lm5/e;->k()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lt5/x;->b:Ljava/lang/String;

    const-string v1, "No source found for package "

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lt5/x;->v(I)V

    return-void

    :cond_6
    iget-object v0, p0, Lt5/x;->d:Landroid/os/UserManager;

    invoke-static {v0}, Lcom/android/packageinstaller/compat/UserManagerCompat;->isManagedProfile(Landroid/os/UserManager;)Z

    move-result v0

    iget-object v1, p0, Lt5/x;->a:Lm5/z0;

    invoke-static {v2, v1}, Lcom/android/packageinstaller/compat/UnknownSourceCompat;->isUnknownSourcesEnabled(ZLm5/z0;)Z

    move-result v1

    if-nez v1, :cond_7

    if-eqz v0, :cond_7

    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lt5/x;->v(I)V

    return-void

    :cond_7
    invoke-direct {p0}, Lt5/x;->t()V

    return-void
.end method

.method private final p()Lm5/e;
    .locals 1

    iget-object v0, p0, Lt5/x;->f:La8/f;

    invoke-interface {v0}, La8/f;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lm5/e;

    return-object v0
.end method

.method private final q()Z
    .locals 4

    iget-object v0, p0, Lt5/x;->a:Lm5/z0;

    invoke-virtual {v0}, Lm5/z0;->c()Lm5/e;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lm5/e;->j()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lt5/x;->a:Lm5/z0;

    invoke-static {v1}, Lcom/android/packageinstaller/utils/b;->a(Landroid/content/Context;)Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lt5/x;->a:Lm5/z0;

    invoke-static {v2}, Lcom/android/packageinstaller/utils/b;->b(Landroid/content/Context;)Z

    move-result v2

    const/4 v3, 0x0

    if-eqz v2, :cond_1

    if-eqz v1, :cond_1

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " is restricted to install package"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Enterprise"

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lt5/x;->a:Lm5/z0;

    invoke-virtual {v0, v3}, Landroid/app/Activity;->setResult(I)V

    iget-object v0, p0, Lt5/x;->a:Lm5/z0;

    invoke-virtual {v0}, Lmiuix/appcompat/app/j;->finish()V

    return v3

    :cond_1
    if-eqz v0, :cond_2

    :try_start_0
    iget-object v1, p0, Lt5/x;->a:Lm5/z0;

    invoke-virtual {v1}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    iget-object v1, p0, Lt5/x;->a:Lm5/z0;

    invoke-virtual {v1}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v1, v0, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    const-string v1, "mInstallerActivity.packa\u2026ionInfo(callerPackage, 0)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lt5/x;->a:Lm5/z0;

    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "android.intent.extra.NOT_UNKNOWN_SOURCE"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {v0}, Lcom/android/packageinstaller/compat/ApplicationInfoCompat;->privateFlags(Landroid/content/pm/ApplicationInfo;)I

    move-result v0
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_2

    return v3

    :catch_0
    :cond_2
    const/4 v0, 0x1

    return v0
.end method

.method private final r()Z
    .locals 2

    iget-object v0, p0, Lt5/x;->d:Landroid/os/UserManager;

    const-string v1, "no_install_unknown_sources"

    invoke-virtual {v0, v1}, Landroid/os/UserManager;->hasUserRestriction(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private final t()V
    .locals 2

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object v0

    new-instance v1, Lt5/t;

    invoke-direct {v1, p0}, Lt5/t;-><init>(Lt5/x;)V

    invoke-virtual {v0, v1}, Lf6/z;->e(Ljava/lang/Runnable;)V

    return-void
.end method

.method private static final u(Lt5/x;)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p0, p0, Lt5/x;->c:Ll8/a;

    if-eqz p0, :cond_0

    invoke-interface {p0}, Ll8/a;->a()Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method private final v(I)V
    .locals 2

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object v0

    new-instance v1, Lt5/w;

    invoke-direct {v1, p0, p1}, Lt5/w;-><init>(Lt5/x;I)V

    invoke-virtual {v0, v1}, Lf6/z;->e(Ljava/lang/Runnable;)V

    return-void
.end method

.method private static final w(Lt5/x;I)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lt5/x;->x(I)V

    return-void
.end method

.method private final x(I)V
    .locals 8

    const-string v0, "Did not find app info for DLG_UNKNOWN_SOURCES: "

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eq p1, v1, :cond_5

    const/4 v3, 0x6

    if-eq p1, v3, :cond_4

    const/16 v3, 0x9

    if-eq p1, v3, :cond_1

    const/16 v0, 0xa

    if-eq p1, v0, :cond_0

    goto/16 :goto_3

    :cond_0
    new-instance p1, La6/g;

    iget-object v0, p0, Lt5/x;->a:Lm5/z0;

    invoke-direct {p1, v0}, La6/g;-><init>(Landroid/content/Context;)V

    iget-object v0, p0, Lt5/x;->a:Lm5/z0;

    const v1, 0x7f11002d

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, La6/g;->i(Ljava/lang/String;)La6/g;

    move-result-object p1

    iget-object v0, p0, Lt5/x;->a:Lm5/z0;

    const v1, 0x7f11002c

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "mInstallerActivity.getSt\u2026nonymous_source_continue)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v1, Lt5/x$g;

    invoke-direct {v1, p0}, Lt5/x$g;-><init>(Lt5/x;)V

    goto/16 :goto_0

    :cond_1
    :try_start_0
    sget-object p1, Lt5/n;->b:Lt5/n$a;

    invoke-virtual {p1}, Lt5/n$a;->b()Lt5/n;

    move-result-object p1

    const-string v3, "installSourceAuth"

    invoke-virtual {p1, v3}, Lt5/n;->d(Ljava/lang/String;)Lt5/l;

    move-result-object p1

    const-string v3, "style"

    invoke-virtual {p1, v3, v1}, Lt5/l;->a(Ljava/lang/String;I)I

    move-result p1

    if-nez p1, :cond_2

    iget-object p1, p0, Lt5/x;->a:Lm5/z0;

    invoke-virtual {p1}, Lm5/z0;->p1()Lcom/miui/packageInstaller/model/InstallSourceTips;

    move-result-object p1

    if-eqz p1, :cond_2

    new-instance p1, Landroid/content/Intent;

    iget-object v1, p0, Lt5/x;->a:Lm5/z0;

    const-class v3, Lcom/miui/packageInstaller/UnknownSourceActivity;

    invoke-direct {p1, v1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "currentApkInfo"

    iget-object v3, p0, Lt5/x;->h:Lcom/miui/packageInstaller/model/ApkInfo;

    invoke-virtual {p1, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "callerPackage"

    invoke-direct {p0}, Lt5/x;->p()Lm5/e;

    move-result-object v3

    invoke-virtual {p1, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "installTips"

    iget-object v3, p0, Lt5/x;->a:Lm5/z0;

    invoke-virtual {v3}, Lm5/z0;->p1()Lcom/miui/packageInstaller/model/InstallSourceTips;

    move-result-object v3

    invoke-virtual {p1, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    iget-object v1, p0, Lt5/x;->a:Lm5/z0;

    const/16 v3, 0x232a

    invoke-virtual {v1, p1, v3}, Landroidx/activity/ComponentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_3

    :cond_2
    sget-object p1, Lt5/k0;->a:Lt5/k0;

    iget-object v1, p0, Lt5/x;->a:Lm5/z0;

    invoke-virtual {v1}, Lm5/z0;->p1()Lcom/miui/packageInstaller/model/InstallSourceTips;

    move-result-object v1

    iget-object v3, p0, Lt5/x;->a:Lm5/z0;

    invoke-direct {p0}, Lt5/x;->p()Lm5/e;

    move-result-object v4

    iget-object v5, p0, Lt5/x;->h:Lcom/miui/packageInstaller/model/ApkInfo;

    invoke-virtual {p1, v1, v3, v4, v5}, Lt5/k0;->a(Lcom/miui/packageInstaller/model/InstallSourceTips;Landroid/content/Context;Lm5/e;Lcom/miui/packageInstaller/model/ApkInfo;)La6/c0;

    move-result-object p1

    iget-object v1, p0, Lt5/x;->i:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {p1, v1}, Landroid/app/Dialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    new-instance v1, Lt5/x$c;

    invoke-direct {v1, p0}, Lt5/x$c;-><init>(Lt5/x;)V

    invoke-virtual {p1, v1}, La6/c0;->h(La6/c0$a;)V

    invoke-virtual {p1}, Landroid/app/Dialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_3

    :catch_0
    iget-object p1, p0, Lt5/x;->b:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lt5/x;->a:Lm5/z0;

    invoke-virtual {v0}, Lm5/z0;->c()Lm5/e;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lm5/e;->k()Ljava/lang/String;

    move-result-object v2

    :cond_3
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    :cond_4
    new-instance p1, La6/g;

    iget-object v0, p0, Lt5/x;->a:Lm5/z0;

    invoke-direct {p1, v0}, La6/g;-><init>(Landroid/content/Context;)V

    iget-object v0, p0, Lt5/x;->a:Lm5/z0;

    const v1, 0x7f1103d2

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, La6/g;->i(Ljava/lang/String;)La6/g;

    move-result-object p1

    iget-object v0, p0, Lt5/x;->a:Lm5/z0;

    const v1, 0x104000a

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "mInstallerActivity.getString(android.R.string.ok)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v1, Lt5/x$h;

    invoke-direct {v1, p0}, Lt5/x$h;-><init>(Lt5/x;)V

    :goto_0
    invoke-virtual {p1, v0, v1}, La6/g;->m(Ljava/lang/String;La6/g$a;)La6/g;

    move-result-object p1

    iget-object v0, p0, Lt5/x;->i:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {p1, v0}, Landroid/app/Dialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    invoke-virtual {p1}, Landroid/app/Dialog;->show()V

    goto/16 :goto_3

    :cond_5
    :try_start_1
    iget-object p1, p0, Lt5/x;->a:Lm5/z0;

    invoke-virtual {p1}, Lm5/z0;->c()Lm5/e;

    move-result-object p1

    const/4 v3, 0x0

    if-eqz p1, :cond_6

    invoke-virtual {p1}, Lm5/e;->k()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_6

    iget-object v4, p0, Lt5/x;->g:Landroid/content/pm/PackageManager;

    invoke-virtual {v4, p1, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object p1

    goto :goto_1

    :cond_6
    move-object p1, v2

    :goto_1
    new-instance v4, La6/g;

    iget-object v5, p0, Lt5/x;->a:Lm5/z0;

    invoke-direct {v4, v5}, La6/g;-><init>(Landroid/content/Context;)V

    iget-object v5, p0, Lt5/x;->a:Lm5/z0;

    const v6, 0x7f1103da

    new-array v1, v1, [Ljava/lang/Object;

    if-eqz p1, :cond_7

    iget-object v7, p0, Lt5/x;->g:Landroid/content/pm/PackageManager;

    invoke-virtual {v7, p1}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object p1

    goto :goto_2

    :cond_7
    move-object p1, v2

    :goto_2
    aput-object p1, v1, v3

    invoke-virtual {v5, v6, v1}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v4, p1}, La6/g;->l(Ljava/lang/String;)La6/g;

    move-result-object p1

    iget-object v1, p0, Lt5/x;->a:Lm5/z0;

    const v3, 0x7f1103db

    invoke-virtual {v1, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, La6/g;->i(Ljava/lang/String;)La6/g;

    move-result-object p1

    iget-object v1, p0, Lt5/x;->a:Lm5/z0;

    const v3, 0x7f110109

    invoke-virtual {v1, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v3, "mInstallerActivity.getSt\u2026ources_settings_positive)"

    invoke-static {v1, v3}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v3, Lt5/x$d;

    invoke-direct {v3, p0}, Lt5/x$d;-><init>(Lt5/x;)V

    invoke-virtual {p1, v1, v3}, La6/g;->j(Ljava/lang/String;La6/g$a;)La6/g;

    move-result-object p1

    iget-object v1, p0, Lt5/x;->a:Lm5/z0;

    const v3, 0x7f1103d9

    invoke-virtual {v1, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v3, "mInstallerActivity.getSt\u2026rnal_source_always_click)"

    invoke-static {v1, v3}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v3, Lt5/x$e;

    invoke-direct {v3, p0}, Lt5/x$e;-><init>(Lt5/x;)V

    invoke-virtual {p1, v1, v3}, La6/g;->m(Ljava/lang/String;La6/g$a;)La6/g;

    move-result-object p1

    iget-object v1, p0, Lt5/x;->a:Lm5/z0;

    const v3, 0x7f110108

    invoke-virtual {v1, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v3, "mInstallerActivity.getSt\u2026ources_settings_negative)"

    invoke-static {v1, v3}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v3, Lt5/x$f;

    invoke-direct {v3, p0}, Lt5/x$f;-><init>(Lt5/x;)V

    invoke-virtual {p1, v1, v3}, La6/g;->k(Ljava/lang/String;La6/g$a;)La6/g;

    move-result-object p1

    iget-object v1, p0, Lt5/x;->i:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {p1, v1}, Landroid/app/Dialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    invoke-virtual {p1}, Landroid/app/Dialog;->show()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_3

    :catch_1
    iget-object p1, p0, Lt5/x;->b:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lt5/x;->a:Lm5/z0;

    invoke-virtual {v0}, Lm5/z0;->c()Lm5/e;

    move-result-object v0

    if-eqz v0, :cond_8

    invoke-virtual {v0}, Lm5/e;->k()Ljava/lang/String;

    move-result-object v2

    :cond_8
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object p1, p0, Lt5/x;->a:Lm5/z0;

    invoke-virtual {p1}, Lmiuix/appcompat/app/j;->finish()V

    :goto_3
    return-void
.end method

.method private final z(ZZ)V
    .locals 3

    const/4 v0, 0x0

    if-eqz p1, :cond_2

    if-eqz p2, :cond_1

    invoke-static {}, Lt5/z;->e()Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lt5/x;->p()Lm5/e;

    move-result-object p1

    invoke-virtual {p1}, Lm5/e;->k()Ljava/lang/String;

    move-result-object p1

    const-string p2, "mCallingPackage.originatingPackage"

    invoke-static {p1, p2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p2, p0, Lt5/x;->a:Lm5/z0;

    const v1, 0x7f11017c

    invoke-virtual {p2, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object p2

    const-string v1, "mInstallerActivity.getSt\u2026ng.install_unknow_source)"

    invoke-static {p2, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "perm_install_unknown"

    invoke-virtual {p0, p1, v1, p2}, Lt5/x;->y(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_4

    :cond_0
    iget-object p1, p0, Lt5/x;->e:Landroid/app/AppOpsManager;

    const/16 p2, 0x42

    invoke-direct {p0}, Lt5/x;->p()Lm5/e;

    move-result-object v1

    invoke-virtual {v1}, Lm5/e;->l()I

    move-result v1

    invoke-direct {p0}, Lt5/x;->p()Lm5/e;

    move-result-object v2

    invoke-virtual {v2}, Lm5/e;->j()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, p2, v1, v2, v0}, Lcom/android/packageinstaller/compat/AppOpsManagerCompat;->setMode(Landroid/app/AppOpsManager;IILjava/lang/String;I)V

    invoke-direct {p0}, Lt5/x;->p()Lm5/e;

    move-result-object p1

    const/4 p2, 0x1

    invoke-static {p1, p2}, Lt5/z;->d(Lm5/e;Z)V

    sget-object p1, Lf6/u;->a:Lf6/u$a;

    invoke-direct {p0}, Lt5/x;->p()Lm5/e;

    move-result-object p2

    invoke-virtual {p1, p2}, Lf6/u$a;->a(Lm5/e;)V

    :cond_1
    invoke-direct {p0}, Lt5/x;->t()V

    goto :goto_0

    :cond_2
    if-eqz p2, :cond_3

    invoke-direct {p0}, Lt5/x;->p()Lm5/e;

    move-result-object p1

    invoke-static {p1, v0}, Lt5/z;->d(Lm5/e;Z)V

    :cond_3
    iget-object p1, p0, Lt5/x;->a:Lm5/z0;

    invoke-virtual {p1}, Lmiuix/appcompat/app/j;->finish()V

    :cond_4
    :goto_0
    return-void
.end method


# virtual methods
.method public final k(Lcom/miui/packageInstaller/model/ApkInfo;Ll8/a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/miui/packageInstaller/model/ApkInfo;",
            "Ll8/a<",
            "La8/v;",
            ">;)V"
        }
    .end annotation

    const-string v0, "apkInfo"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lt5/x;->h:Lcom/miui/packageInstaller/model/ApkInfo;

    iput-object p2, p0, Lt5/x;->c:Ll8/a;

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object p1

    new-instance p2, Lt5/v;

    invoke-direct {p2, p0}, Lt5/v;-><init>(Lt5/x;)V

    invoke-virtual {p1, p2}, Lf6/z;->g(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final m(Lcom/miui/packageInstaller/model/ApkInfo;Ll8/a;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/miui/packageInstaller/model/ApkInfo;",
            "Ll8/a<",
            "La8/v;",
            ">;)V"
        }
    .end annotation

    const-string v0, "apkInfo"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lt5/x;->h:Lcom/miui/packageInstaller/model/ApkInfo;

    iput-object p2, p0, Lt5/x;->c:Ll8/a;

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object p1

    new-instance p2, Lt5/u;

    invoke-direct {p2, p0}, Lt5/u;-><init>(Lt5/x;)V

    invoke-virtual {p1, p2}, Lf6/z;->g(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final o(Lm5/z0;)V
    .locals 2

    const-string v0, "activity"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lt5/x;->a:Lm5/z0;

    invoke-virtual {p1}, Lm5/z0;->c()Lm5/e;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lm5/e;->l()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p1}, Lm5/z0;->c()Lm5/e;

    move-result-object p1

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lm5/e;->k()Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    :goto_1
    invoke-static {v0, v1, p1}, Lcom/android/packageinstaller/compat/UnknownSourceCompat;->setNonMarketAppsAllowed(Landroid/content/Context;ILjava/lang/String;)V

    iget-object p1, p0, Lt5/x;->a:Lm5/z0;

    invoke-static {p1}, Lm2/b;->g(Landroid/content/Context;)Lm2/b;

    move-result-object p1

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lm2/b;->M(Z)V

    const-string p1, "persist.security.uks_opened"

    const-string v0, "1"

    invoke-static {p1, v0}, Lcom/android/packageinstaller/compat/SystemPropertiesCompat;->set(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lt5/x;->t()V

    return-void
.end method

.method public final s(IILandroid/content/Intent;)V
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, v0, :cond_1

    iget-object p1, p0, Lt5/x;->a:Lm5/z0;

    invoke-static {v1, p1}, Lcom/android/packageinstaller/compat/UnknownSourceCompat;->isUnknownSourcesEnabled(ZLm5/z0;)Z

    move-result p1

    if-eqz p1, :cond_0

    :goto_0
    invoke-direct {p0}, Lt5/x;->t()V

    goto :goto_3

    :cond_0
    iget-object p1, p0, Lt5/x;->a:Lm5/z0;

    invoke-virtual {p1}, Lmiuix/appcompat/app/j;->finish()V

    goto :goto_3

    :cond_1
    const/16 v2, 0x66

    if-ne p1, v2, :cond_2

    const/4 p1, -0x1

    if-ne p2, p1, :cond_0

    iget-object p1, p0, Lt5/x;->e:Landroid/app/AppOpsManager;

    const/16 p2, 0x42

    invoke-direct {p0}, Lt5/x;->p()Lm5/e;

    move-result-object p3

    invoke-virtual {p3}, Lm5/e;->l()I

    move-result p3

    invoke-direct {p0}, Lt5/x;->p()Lm5/e;

    move-result-object v2

    invoke-virtual {v2}, Lm5/e;->j()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, p2, p3, v2, v1}, Lcom/android/packageinstaller/compat/AppOpsManagerCompat;->setMode(Landroid/app/AppOpsManager;IILjava/lang/String;I)V

    invoke-direct {p0}, Lt5/x;->p()Lm5/e;

    move-result-object p1

    invoke-static {p1, v0}, Lt5/z;->d(Lm5/e;Z)V

    goto :goto_0

    :cond_2
    const/16 v2, 0x232a

    if-ne p1, v2, :cond_5

    const/16 p1, 0x232c

    if-ne p2, p1, :cond_5

    if-eqz p3, :cond_3

    const-string p1, "allowButton"

    invoke-virtual {p3, p1, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result p1

    if-ne p1, v0, :cond_3

    move p1, v0

    goto :goto_1

    :cond_3
    move p1, v1

    :goto_1
    if-eqz p3, :cond_4

    const-string p2, "doNotShowAgain"

    invoke-virtual {p3, p2, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result p2

    if-ne p2, v0, :cond_4

    goto :goto_2

    :cond_4
    move v0, v1

    :goto_2
    invoke-direct {p0, p1, v0}, Lt5/x;->z(ZZ)V

    :cond_5
    :goto_3
    return-void
.end method

.method public final y(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 5

    const-string v0, "pkgName"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "permName"

    invoke-static {p2, v1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "permDesc"

    invoke-static {p3, v2}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v3, Landroid/content/Intent;

    const-string v4, "miui.intent.action.SPECIAL_PERMISSIO_NINTERCEPT"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v3, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v3, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string p1, "com.miui.securitycenter"

    invoke-virtual {v3, p1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    iget-object p1, p0, Lt5/x;->a:Lm5/z0;

    invoke-static {p1, v3}, Lq2/d;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lt5/x;->a:Lm5/z0;

    const/16 p2, 0x66

    invoke-virtual {p1, v3, p2}, Landroidx/activity/ComponentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method
