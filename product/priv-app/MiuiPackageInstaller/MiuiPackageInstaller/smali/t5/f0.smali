.class public final Lt5/f0;
.super Ljava/lang/Object;


# static fields
.field public static final a:Lt5/f0;

.field private static final b:Landroid/content/SharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lt5/f0;

    invoke-direct {v0}, Lt5/f0;-><init>()V

    sput-object v0, Lt5/f0;->a:Lt5/f0;

    invoke-static {}, Lcom/android/packageinstaller/InstallerApplication;->g()Lcom/android/packageinstaller/InstallerApplication;

    move-result-object v0

    const-string v1, "risk_control"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/Application;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    sput-object v0, Lt5/f0;->b:Landroid/content/SharedPreferences;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static synthetic a()V
    .locals 0

    invoke-static {}, Lt5/f0;->d()V

    return-void
.end method

.method public static synthetic b(Lcom/miui/packageInstaller/model/RiskControlConfig;Ljava/lang/String;)V
    .locals 0

    invoke-static {p0, p1}, Lt5/f0;->k(Lcom/miui/packageInstaller/model/RiskControlConfig;Ljava/lang/String;)V

    return-void
.end method

.method public static final c()V
    .locals 2

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object v0

    sget-object v1, Lt5/e0;->a:Lt5/e0;

    invoke-virtual {v0, v1}, Lf6/z;->g(Ljava/lang/Runnable;)V

    return-void
.end method

.method private static final d()V
    .locals 4

    sget-object v0, Lp6/a;->c:Lp6/a;

    const-class v1, Lcom/miui/packageInstaller/model/RiskControlRules;

    invoke-virtual {v0, v1}, Lp6/a;->e(Ljava/lang/Class;)Ljava/util/List;

    move-result-object v0

    const-string v1, "MAIN.queryAll(RiskControlRules::class.java)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/miui/packageInstaller/model/RiskControlRules;

    invoke-static {}, Lcom/android/packageinstaller/InstallerApplication;->g()Lcom/android/packageinstaller/InstallerApplication;

    move-result-object v2

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/RiskControlRules;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lj2/f;->o(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Lp6/a;->c:Lp6/a;

    invoke-virtual {v2, v1}, Lp6/a;->a(Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private final f()Z
    .locals 3

    sget-object v0, Lt5/f0;->b:Landroid/content/SharedPreferences;

    const-string v1, "isEnable"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private final h(Lcom/miui/packageInstaller/model/RiskControlRules;)V
    .locals 1

    sget-object v0, Lp6/a;->c:Lp6/a;

    invoke-virtual {v0, p1}, Lp6/a;->i(Ljava/lang/Object;)V

    return-void
.end method

.method private static final k(Lcom/miui/packageInstaller/model/RiskControlConfig;Ljava/lang/String;)V
    .locals 2

    const-string v0, "$it"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$installer"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lt5/f0;->a:Lt5/f0;

    invoke-virtual {p0}, Lcom/miui/packageInstaller/model/RiskControlConfig;->getRcl()Z

    move-result v1

    invoke-virtual {v0, v1}, Lt5/f0;->i(Z)V

    invoke-virtual {p0}, Lcom/miui/packageInstaller/model/RiskControlConfig;->getRcl()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/miui/packageInstaller/model/RiskControlRules;->Companion:Lcom/miui/packageInstaller/model/RiskControlRules$Companion;

    invoke-virtual {v1, p1, p0}, Lcom/miui/packageInstaller/model/RiskControlRules$Companion;->createFromCloudConfig(Ljava/lang/String;Lcom/miui/packageInstaller/model/RiskControlConfig;)Lcom/miui/packageInstaller/model/RiskControlRules;

    move-result-object p0

    invoke-direct {v0, p0}, Lt5/f0;->h(Lcom/miui/packageInstaller/model/RiskControlRules;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public final e(Ljava/lang/String;)Lcom/miui/packageInstaller/model/RiskControlRules;
    .locals 2

    const-string v0, "installer"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lt5/f0;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lp6/a;->c:Lp6/a;

    const-class v1, Lcom/miui/packageInstaller/model/RiskControlRules;

    invoke-virtual {v0, v1, p1}, Lp6/a;->h(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/miui/packageInstaller/model/RiskControlRules;

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public final g(Ljava/lang/String;)Z
    .locals 4

    const-string v0, "callerPackageName"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Lcom/android/packageinstaller/InstallerApplication;->g()Lcom/android/packageinstaller/InstallerApplication;

    move-result-object v0

    invoke-static {v0}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->isSafeModelEnable(Landroid/content/Context;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_4

    invoke-static {}, Lcom/android/packageinstaller/InstallerApplication;->g()Lcom/android/packageinstaller/InstallerApplication;

    move-result-object v0

    invoke-static {v0}, Lm2/b;->g(Landroid/content/Context;)Lm2/b;

    move-result-object v0

    invoke-virtual {v0}, Lm2/b;->l()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v0, Lf6/s;->a:Lf6/s$a;

    invoke-virtual {v0}, Lf6/s$a;->a()Lf6/s;

    move-result-object v2

    invoke-virtual {v2}, Lf6/s;->b()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0}, Lf6/s$a;->a()Lf6/s;

    move-result-object v0

    invoke-virtual {v0}, Lf6/s;->c()Ljava/util/List;

    move-result-object v0

    invoke-static {}, Lcom/android/packageinstaller/InstallerApplication;->g()Lcom/android/packageinstaller/InstallerApplication;

    move-result-object v3

    invoke-static {v3, p1}, Lj2/f;->p(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    if-eqz v0, :cond_1

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    return v1

    :cond_2
    if-eqz v2, :cond_3

    invoke-interface {v2, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_4

    :cond_3
    const/4 v1, 0x1

    :cond_4
    :goto_0
    return v1
.end method

.method public final i(Z)V
    .locals 2

    sget-object v0, Lt5/f0;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "isEnable"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public final j(Ljava/lang/String;Lcom/miui/packageInstaller/model/CloudParams;)V
    .locals 2

    const-string v0, "installer"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cloudParams"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p2, p2, Lcom/miui/packageInstaller/model/CloudParams;->rc:Lcom/miui/packageInstaller/model/RiskControlConfig;

    if-eqz p2, :cond_0

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object v0

    new-instance v1, Lt5/d0;

    invoke-direct {v1, p2, p1}, Lt5/d0;-><init>(Lcom/miui/packageInstaller/model/RiskControlConfig;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lf6/z;->g(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method
