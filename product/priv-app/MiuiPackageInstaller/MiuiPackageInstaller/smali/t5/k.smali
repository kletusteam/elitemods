.class public final Lt5/k;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lt5/k$a;
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "AuthorManager"

    iput-object v0, p0, Lt5/k;->a:Ljava/lang/String;

    return-void
.end method

.method public static synthetic a(Lt5/k$a;Lt5/k;Landroid/accounts/AccountManagerFuture;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lt5/k;->g(Lt5/k$a;Lt5/k;Landroid/accounts/AccountManagerFuture;)V

    return-void
.end method

.method public static synthetic b(Lt5/k$a;Landroid/app/Activity;Ljava/util/Map;Lt5/k;Landroid/accounts/AccountManagerFuture;)V
    .locals 0

    invoke-static {p0, p1, p2, p3, p4}, Lt5/k;->e(Lt5/k$a;Landroid/app/Activity;Ljava/util/Map;Lt5/k;Landroid/accounts/AccountManagerFuture;)V

    return-void
.end method

.method public static synthetic d(Lt5/k;Landroid/accounts/Account;Landroid/app/Activity;Lt5/k$a;Ljava/lang/String;Ljava/util/Map;ILjava/lang/Object;)V
    .locals 6

    and-int/lit8 p6, p6, 0x10

    if-eqz p6, :cond_0

    const/4 p5, 0x0

    :cond_0
    move-object v5, p5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lt5/k;->c(Landroid/accounts/Account;Landroid/app/Activity;Lt5/k$a;Ljava/lang/String;Ljava/util/Map;)V

    return-void
.end method

.method private static final e(Lt5/k$a;Landroid/app/Activity;Ljava/util/Map;Lt5/k;Landroid/accounts/AccountManagerFuture;)V
    .locals 5

    const-string v0, "authentication_result"

    const-string v1, "button"

    const-string v2, "authentication_popup_close_btn"

    const-string v3, "$authorVerifyListener"

    invoke-static {p0, v3}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "$activity"

    invoke-static {p1, v3}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "this$0"

    invoke-static {p3, v3}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    :try_start_0
    invoke-interface {p4}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Landroid/os/Bundle;

    const-string v3, "booleanResult"

    invoke-virtual {p4, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result p4

    if-eqz p4, :cond_1

    const/4 p4, 0x1

    const/4 v3, 0x0

    invoke-interface {p0, p4, v3}, Lt5/k$a;->a(ZLjava/lang/String;)V

    instance-of p4, p1, Lo5/a;

    if-eqz p4, :cond_0

    new-instance p4, Lp5/b;

    move-object v3, p1

    check-cast v3, Lo5/a;

    invoke-direct {p4, v2, v1, v3}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    const-string v3, "success"

    invoke-virtual {p4, v0, v3}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p4

    invoke-virtual {p4, p2}, Lp5/f;->g(Ljava/util/Map;)Lp5/f;

    move-result-object p4

    invoke-virtual {p4}, Lp5/f;->c()Z

    :cond_0
    sget-object p4, Lf6/r;->a:Lf6/r$a;

    invoke-virtual {p4, p1}, Lf6/r$a;->f(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p4

    iget-object p3, p3, Lt5/k;->a:Ljava/lang/String;

    const-string v3, "verifyAccount error: "

    invoke-static {p3, v3, p4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 p3, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p4

    invoke-interface {p0, p3, p4}, Lt5/k$a;->a(ZLjava/lang/String;)V

    instance-of p0, p1, Lo5/a;

    if-eqz p0, :cond_1

    new-instance p0, Lp5/b;

    check-cast p1, Lo5/a;

    invoke-direct {p0, v2, v1, p1}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    const-string p1, "fail"

    invoke-virtual {p0, v0, p1}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p0

    invoke-virtual {p0, p2}, Lp5/f;->g(Ljava/util/Map;)Lp5/f;

    move-result-object p0

    invoke-virtual {p0}, Lp5/f;->c()Z

    :cond_1
    :goto_0
    return-void
.end method

.method private final f(Landroid/app/Activity;Lt5/k$a;)V
    .locals 8

    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    new-instance v6, Lt5/j;

    invoke-direct {v6, p2, p0}, Lt5/j;-><init>(Lt5/k$a;Lt5/k;)V

    const-string v1, "com.xiaomi"

    const-string v2, "passportapi"

    const/4 v3, 0x0

    const/4 v7, 0x0

    move-object v5, p1

    invoke-virtual/range {v0 .. v7}, Landroid/accounts/AccountManager;->addAccount(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    return-void
.end method

.method private static final g(Lt5/k$a;Lt5/k;Landroid/accounts/AccountManagerFuture;)V
    .locals 2

    const-string v0, "login failed"

    const-string v1, "$authorVerifyListener"

    invoke-static {p0, v1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "this$0"

    invoke-static {p1, v1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    :try_start_0
    invoke-interface {p2}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/os/Bundle;

    const-string v1, "booleanResult"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result p2

    if-eqz p2, :cond_0

    const/4 p2, 0x1

    const/4 v0, 0x0

    invoke-interface {p0, p2, v0}, Lt5/k$a;->a(ZLjava/lang/String;)V

    goto :goto_0

    :cond_0
    iget-object p2, p1, Lt5/k;->a:Ljava/lang/String;

    invoke-static {p2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 p2, 0x0

    invoke-interface {p0, p2, v0}, Lt5/k$a;->a(ZLjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    iget-object p1, p1, Lt5/k;->a:Ljava/lang/String;

    const-string p2, "login error"

    invoke-static {p1, p2, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-virtual {p0}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    return-void
.end method


# virtual methods
.method public final c(Landroid/accounts/Account;Landroid/app/Activity;Lt5/k$a;Ljava/lang/String;Ljava/util/Map;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            "Landroid/app/Activity;",
            "Lt5/k$a;",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const-string v0, "account"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "activity"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "authorVerifyListener"

    invoke-static {p3, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "titleMessage"

    invoke-static {p4, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    :try_start_0
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    instance-of v0, p2, Lo5/a;

    if-eqz v0, :cond_0

    new-instance v0, Lp5/g;

    const-string v1, "authentication_popup"

    const-string v2, "popup"

    move-object v4, p2

    check-cast v4, Lo5/a;

    invoke-direct {v0, v1, v2, v4}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0, p5}, Lp5/f;->g(Ljava/util/Map;)Lp5/f;

    move-result-object v0

    invoke-virtual {v0}, Lp5/f;->c()Z

    :cond_0
    const-string v0, "title"

    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    const p4, 0x7f1103e4

    invoke-virtual {p2, p4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object p4

    :cond_1
    invoke-virtual {v3, v0, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p2}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    new-instance v5, Lt5/i;

    invoke-direct {v5, p3, p2, p5, p0}, Lt5/i;-><init>(Lt5/k$a;Landroid/app/Activity;Ljava/util/Map;Lt5/k;)V

    const/4 v6, 0x0

    move-object v2, p1

    move-object v4, p2

    invoke-virtual/range {v1 .. v6}, Landroid/accounts/AccountManager;->confirmCredentials(Landroid/accounts/Account;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-direct {p0, p2, p3}, Lt5/k;->f(Landroid/app/Activity;Lt5/k$a;)V

    iget-object p2, p0, Lt5/k;->a:Ljava/lang/String;

    const-string p3, "account confirmCredentials error: "

    invoke-static {p2, p3, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_0
    return-void
.end method

.method public final h(Landroid/app/Activity;Landroid/accounts/Account;Lt5/k$a;)V
    .locals 9

    const-string v0, "activity"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "account"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "authorVerifyListener"

    invoke-static {p3, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v5, ""

    const/4 v6, 0x0

    const/16 v7, 0x10

    const/4 v8, 0x0

    move-object v1, p0

    move-object v2, p2

    move-object v3, p1

    move-object v4, p3

    invoke-static/range {v1 .. v8}, Lt5/k;->d(Lt5/k;Landroid/accounts/Account;Landroid/app/Activity;Lt5/k$a;Ljava/lang/String;Ljava/util/Map;ILjava/lang/Object;)V

    return-void
.end method

.method public final i(Landroid/app/Activity;Lt5/k$a;Ljava/lang/String;)V
    .locals 9

    const-string v0, "activity"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "authorVerifyListener"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "titleMessage"

    invoke-static {p3, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Lcom/android/packageinstaller/utils/a;->b()Lcom/android/packageinstaller/utils/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/packageinstaller/utils/a;->a()Landroid/accounts/Account;

    move-result-object v2

    if-eqz v2, :cond_0

    const/4 v6, 0x0

    const/16 v7, 0x10

    const/4 v8, 0x0

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-static/range {v1 .. v8}, Lt5/k;->d(Lt5/k;Landroid/accounts/Account;Landroid/app/Activity;Lt5/k$a;Ljava/lang/String;Ljava/util/Map;ILjava/lang/Object;)V

    sget-object p3, La8/v;->a:La8/v;

    goto :goto_0

    :cond_0
    const/4 p3, 0x0

    :goto_0
    if-nez p3, :cond_1

    invoke-direct {p0, p1, p2}, Lt5/k;->f(Landroid/app/Activity;Lt5/k$a;)V

    :cond_1
    return-void
.end method
