.class public final Lt5/j0;
.super Lt5/g0;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lt5/j0$a;
    }
.end annotation


# instance fields
.field private final b:Lq2/b;

.field private final c:Lcom/miui/packageInstaller/model/RiskControlConfig;

.field private final d:Lcom/miui/packageInstaller/model/ApkInfo;

.field private final e:Lm5/e;


# direct methods
.method public constructor <init>(Lq2/b;Lcom/miui/packageInstaller/model/RiskControlConfig;Lcom/miui/packageInstaller/model/ApkInfo;Lm5/e;)V
    .locals 1

    const-string v0, "activity"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rc"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "caller"

    invoke-static {p4, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lt5/g0;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lt5/j0;->b:Lq2/b;

    iput-object p2, p0, Lt5/j0;->c:Lcom/miui/packageInstaller/model/RiskControlConfig;

    iput-object p3, p0, Lt5/j0;->d:Lcom/miui/packageInstaller/model/ApkInfo;

    iput-object p4, p0, Lt5/j0;->e:Lm5/e;

    return-void
.end method

.method public static synthetic c(Lt5/j0;Lt5/g0$a;Landroid/content/DialogInterface;I)V
    .locals 0

    invoke-static {p0, p1, p2, p3}, Lt5/j0;->l(Lt5/j0;Lt5/g0$a;Landroid/content/DialogInterface;I)V

    return-void
.end method

.method public static synthetic d(Lt5/j0;Landroid/content/DialogInterface;I)V
    .locals 0

    invoke-static {p0, p1, p2}, Lt5/j0;->m(Lt5/j0;Landroid/content/DialogInterface;I)V

    return-void
.end method

.method public static final synthetic e(Lt5/j0;)V
    .locals 0

    invoke-direct {p0}, Lt5/j0;->i()V

    return-void
.end method

.method public static final synthetic f(Lt5/j0;)V
    .locals 0

    invoke-direct {p0}, Lt5/j0;->j()V

    return-void
.end method

.method public static final synthetic g(Lt5/j0;)Ljava/lang/String;
    .locals 0

    invoke-direct {p0}, Lt5/j0;->r()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic h(Lt5/j0;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lt5/j0;->s(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private final i()V
    .locals 2

    invoke-static {}, Lcom/android/packageinstaller/utils/a;->b()Lcom/android/packageinstaller/utils/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/packageinstaller/utils/a;->a()Landroid/accounts/Account;

    move-result-object v0

    const-string v1, "fail"

    if-eqz v0, :cond_0

    const-string v0, "mi_account"

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lt5/j0;->r()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {p0, v0, v1}, Lt5/j0;->s(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private final j()V
    .locals 2

    invoke-static {}, Lcom/android/packageinstaller/utils/a;->b()Lcom/android/packageinstaller/utils/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/packageinstaller/utils/a;->a()Landroid/accounts/Account;

    move-result-object v0

    const-string v1, "success"

    if-eqz v0, :cond_0

    const-string v0, "mi_account"

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lt5/j0;->r()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {p0, v0, v1}, Lt5/j0;->s(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private final k(Lt5/g0$a;)V
    .locals 5

    new-instance v0, Lmiuix/appcompat/app/i$b;

    iget-object v1, p0, Lt5/j0;->b:Lq2/b;

    invoke-direct {v0, v1}, Lmiuix/appcompat/app/i$b;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lt5/j0;->c:Lcom/miui/packageInstaller/model/RiskControlConfig;

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/RiskControlConfig;->getRcit()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-nez v2, :cond_0

    move v2, v3

    goto :goto_0

    :cond_0
    move v2, v4

    :goto_0
    if-eqz v2, :cond_1

    iget-object v1, p0, Lt5/j0;->b:Lq2/b;

    const v2, 0x7f1102eb

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "activity.getString(R.string.risk_verification)"

    invoke-static {v1, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0, v1}, Lmiuix/appcompat/app/i$b;->t(Ljava/lang/CharSequence;)Lmiuix/appcompat/app/i$b;

    move-result-object v0

    iget-object v1, p0, Lt5/j0;->c:Lcom/miui/packageInstaller/model/RiskControlConfig;

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/RiskControlConfig;->getRcic()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-nez v2, :cond_2

    move v2, v3

    goto :goto_1

    :cond_2
    move v2, v4

    :goto_1
    if-eqz v2, :cond_3

    iget-object v1, p0, Lt5/j0;->b:Lq2/b;

    const v2, 0x7f1102ec

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "activity.getString(R.string.risk_verification_des)"

    invoke-static {v1, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_3
    invoke-virtual {v0, v1}, Lmiuix/appcompat/app/i$b;->f(Ljava/lang/CharSequence;)Lmiuix/appcompat/app/i$b;

    move-result-object v0

    iget-object v1, p0, Lt5/j0;->c:Lcom/miui/packageInstaller/model/RiskControlConfig;

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/RiskControlConfig;->getRcibl()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-nez v2, :cond_4

    move v2, v3

    goto :goto_2

    :cond_4
    move v2, v4

    :goto_2
    if-eqz v2, :cond_5

    iget-object v1, p0, Lt5/j0;->b:Lq2/b;

    const v2, 0x7f1103e1

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "activity.getString(R.string.verification_word)"

    invoke-static {v1, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_5
    new-instance v2, Lt5/i0;

    invoke-direct {v2, p0, p1}, Lt5/i0;-><init>(Lt5/j0;Lt5/g0$a;)V

    invoke-virtual {v0, v1, v2}, Lmiuix/appcompat/app/i$b;->i(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/i$b;

    move-result-object p1

    iget-object v0, p0, Lt5/j0;->c:Lcom/miui/packageInstaller/model/RiskControlConfig;

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/RiskControlConfig;->getRcibr()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-nez v1, :cond_6

    goto :goto_3

    :cond_6
    move v3, v4

    :goto_3
    if-eqz v3, :cond_7

    iget-object v0, p0, Lt5/j0;->b:Lq2/b;

    const v1, 0x7f1100ad

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "activity.getString(R.str\u2026kscreen_authorize_cancel)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_7
    new-instance v1, Lt5/h0;

    invoke-direct {v1, p0}, Lt5/h0;-><init>(Lt5/j0;)V

    invoke-virtual {p1, v0, v1}, Lmiuix/appcompat/app/i$b;->p(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/i$b;

    move-result-object p1

    invoke-direct {p0}, Lt5/j0;->n()V

    invoke-virtual {p1}, Lmiuix/appcompat/app/i$b;->a()Lmiuix/appcompat/app/i;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method private static final l(Lt5/j0;Lt5/g0$a;Landroid/content/DialogInterface;I)V
    .locals 2

    const-string p3, "this$0"

    invoke-static {p0, p3}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p3, "$authorizeListener"

    invoke-static {p1, p3}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2}, Landroid/content/DialogInterface;->dismiss()V

    new-instance p2, Lp5/b;

    iget-object p3, p0, Lt5/j0;->b:Lq2/b;

    invoke-virtual {p3}, Lq2/b;->F0()Lo5/b;

    move-result-object p3

    const-string v0, "activity.mPage"

    invoke-static {p3, v0}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "risk_verify_popup_verify_btn"

    const-string v1, "button"

    invoke-direct {p2, v0, v1, p3}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p2}, Lp5/f;->c()Z

    iget-object p2, p0, Lt5/j0;->c:Lcom/miui/packageInstaller/model/RiskControlConfig;

    invoke-virtual {p2}, Lcom/miui/packageInstaller/model/RiskControlConfig;->getRcivt()I

    move-result p2

    const/4 p3, 0x1

    if-ne p2, p3, :cond_0

    const-string p2, "risk_verify"

    invoke-direct {p0, p2}, Lt5/j0;->o(Ljava/lang/String;)V

    new-instance p2, Lx5/b;

    iget-object p3, p0, Lt5/j0;->b:Lq2/b;

    invoke-direct {p2, p3}, Lx5/b;-><init>(Landroid/app/Activity;)V

    new-instance p3, Lt5/j0$b;

    invoke-direct {p3, p1, p0}, Lt5/j0$b;-><init>(Lt5/g0$a;Lt5/j0;)V

    invoke-virtual {p2, p3}, Lx5/b;->b(Ll8/p;)V

    goto :goto_1

    :cond_0
    iget-object p2, p0, Lt5/j0;->c:Lcom/miui/packageInstaller/model/RiskControlConfig;

    invoke-virtual {p2}, Lcom/miui/packageInstaller/model/RiskControlConfig;->getRcivt()I

    move-result p2

    const/4 p3, 0x2

    if-ne p2, p3, :cond_2

    invoke-direct {p0, p1}, Lt5/j0;->p(Lt5/g0$a;)V

    :cond_1
    invoke-direct {p0}, Lt5/j0;->r()Ljava/lang/String;

    move-result-object p1

    :goto_0
    invoke-direct {p0, p1}, Lt5/j0;->o(Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    iget-object p2, p0, Lt5/j0;->c:Lcom/miui/packageInstaller/model/RiskControlConfig;

    invoke-virtual {p2}, Lcom/miui/packageInstaller/model/RiskControlConfig;->getRcivt()I

    move-result p2

    const/4 p3, 0x3

    if-ne p2, p3, :cond_3

    invoke-direct {p0, p1}, Lt5/j0;->q(Lt5/g0$a;)V

    invoke-static {}, Lcom/android/packageinstaller/utils/a;->b()Lcom/android/packageinstaller/utils/a;

    move-result-object p1

    invoke-virtual {p1}, Lcom/android/packageinstaller/utils/a;->a()Landroid/accounts/Account;

    move-result-object p1

    if-eqz p1, :cond_1

    const-string p1, "mi_account"

    goto :goto_0

    :cond_3
    :goto_1
    return-void
.end method

.method private static final m(Lt5/j0;Landroid/content/DialogInterface;I)V
    .locals 2

    const-string p2, "this$0"

    invoke-static {p0, p2}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p2, Lp5/b;

    iget-object p0, p0, Lt5/j0;->b:Lq2/b;

    invoke-virtual {p0}, Lq2/b;->F0()Lo5/b;

    move-result-object p0

    const-string v0, "activity.mPage"

    invoke-static {p0, v0}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "risk_verify_popup_cancel_btn"

    const-string v1, "button"

    invoke-direct {p2, v0, v1, p0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p2}, Lp5/f;->c()Z

    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    return-void
.end method

.method private final n()V
    .locals 5

    new-instance v0, Lp5/g;

    iget-object v1, p0, Lt5/j0;->b:Lq2/b;

    invoke-virtual {v1}, Lq2/b;->F0()Lo5/b;

    move-result-object v1

    const-string v2, "activity.mPage"

    invoke-static {v1, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "risk_verify_popup"

    const-string v4, "popup"

    invoke-direct {v0, v3, v4, v1}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    new-instance v0, Lp5/g;

    iget-object v1, p0, Lt5/j0;->b:Lq2/b;

    invoke-virtual {v1}, Lq2/b;->F0()Lo5/b;

    move-result-object v1

    invoke-static {v1, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "risk_verify_popup_verify_btn"

    const-string v4, "button"

    invoke-direct {v0, v3, v4, v1}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    new-instance v0, Lp5/g;

    iget-object v1, p0, Lt5/j0;->b:Lq2/b;

    invoke-virtual {v1}, Lq2/b;->F0()Lo5/b;

    move-result-object v1

    invoke-static {v1, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "risk_verify_popup_cancel_btn"

    invoke-direct {v0, v2, v4, v1}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    return-void
.end method

.method private final o(Ljava/lang/String;)V
    .locals 5

    new-instance v0, Lp5/g;

    iget-object v1, p0, Lt5/j0;->b:Lq2/b;

    invoke-virtual {v1}, Lq2/b;->F0()Lo5/b;

    move-result-object v1

    const-string v2, "activity.mPage"

    invoke-static {v1, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "risk_verify_popup_verify_btn"

    const-string v4, "button"

    invoke-direct {v0, v3, v4, v1}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    const-string v1, "verify_method"

    invoke-virtual {v0, v1, p1}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object v0

    invoke-virtual {v0}, Lp5/f;->c()Z

    new-instance v0, Lp5/g;

    iget-object v3, p0, Lt5/j0;->b:Lq2/b;

    invoke-virtual {v3}, Lq2/b;->F0()Lo5/b;

    move-result-object v3

    invoke-static {v3, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "risk_verifying_popup"

    const-string v4, "popup"

    invoke-direct {v0, v2, v4, v3}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0, v1, p1}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p1

    invoke-virtual {p1}, Lp5/f;->c()Z

    return-void
.end method

.method private final p(Lt5/g0$a;)V
    .locals 5

    iget-object v0, p0, Lt5/j0;->d:Lcom/miui/packageInstaller/model/ApkInfo;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lt5/j0;->b:Lq2/b;

    invoke-static {v1}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->isSafeModelEnable(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Lx5/d;

    iget-object v1, p0, Lt5/j0;->b:Lq2/b;

    invoke-direct {v0, v1}, Lx5/d;-><init>(Landroid/app/Activity;)V

    new-instance v1, Lt5/j0$c;

    invoke-direct {v1, p1, p0}, Lt5/j0$c;-><init>(Lt5/g0$a;Lt5/j0;)V

    invoke-virtual {v0, v1}, Lx5/d;->g(Ll8/p;)V

    goto :goto_0

    :cond_0
    new-instance v1, Lt5/a;

    iget-object v2, p0, Lt5/j0;->b:Lq2/b;

    const/4 v3, 0x1

    iget-object v4, p0, Lt5/j0;->e:Lm5/e;

    invoke-direct {v1, v2, v3, v0, v4}, Lt5/a;-><init>(Landroid/content/Context;ILcom/miui/packageInstaller/model/ApkInfo;Lm5/e;)V

    new-instance v0, Lt5/j0$d;

    invoke-direct {v0, p1, p0}, Lt5/j0$d;-><init>(Lt5/g0$a;Lt5/j0;)V

    invoke-virtual {v1, v0}, Lt5/a;->a(Lt5/g0$a;)V

    :cond_1
    :goto_0
    return-void
.end method

.method private final q(Lt5/g0$a;)V
    .locals 5

    iget-object v0, p0, Lt5/j0;->d:Lcom/miui/packageInstaller/model/ApkInfo;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lt5/j0;->b:Lq2/b;

    invoke-static {v1}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->isSafeModelEnable(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Lx5/d;

    iget-object v1, p0, Lt5/j0;->b:Lq2/b;

    invoke-direct {v0, v1}, Lx5/d;-><init>(Landroid/app/Activity;)V

    new-instance v1, Lt5/j0$e;

    invoke-direct {v1, p1, p0}, Lt5/j0$e;-><init>(Lt5/g0$a;Lt5/j0;)V

    sget-object p1, Lx5/a;->f:Lx5/a;

    invoke-virtual {v0, v1, p1}, Lx5/d;->h(Ll8/p;Lx5/a;)V

    goto :goto_0

    :cond_0
    new-instance v1, Lt5/a;

    iget-object v2, p0, Lt5/j0;->b:Lq2/b;

    const/4 v3, 0x1

    iget-object v4, p0, Lt5/j0;->e:Lm5/e;

    invoke-direct {v1, v2, v3, v0, v4}, Lt5/a;-><init>(Landroid/content/Context;ILcom/miui/packageInstaller/model/ApkInfo;Lm5/e;)V

    new-instance v0, Lt5/j0$f;

    invoke-direct {v0, p1, p0}, Lt5/j0$f;-><init>(Lt5/g0$a;Lt5/j0;)V

    sget-object p1, Lx5/a;->f:Lx5/a;

    invoke-virtual {v1, v0, p1}, Lt5/a;->d(Lt5/g0$a;Lx5/a;)V

    :cond_1
    :goto_0
    return-void
.end method

.method private final r()Ljava/lang/String;
    .locals 2

    sget-object v0, Lx5/d;->b:Lx5/d$a;

    invoke-virtual {v0}, Lx5/d$a;->c()Lx5/a;

    move-result-object v0

    sget-object v1, Lt5/j0$a;->a:[I

    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    const-string v0, "NONE"

    goto :goto_0

    :cond_0
    const-string v0, "screen_password"

    goto :goto_0

    :cond_1
    const-string v0, "face_password"

    goto :goto_0

    :cond_2
    const-string v0, "fingerprint_password"

    goto :goto_0

    :cond_3
    const-string v0, "mi_account"

    :goto_0
    return-object v0
.end method

.method private final s(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    new-instance v0, Lp5/b;

    iget-object v1, p0, Lt5/j0;->b:Lq2/b;

    invoke-virtual {v1}, Lq2/b;->F0()Lo5/b;

    move-result-object v1

    const-string v2, "activity.mPage"

    invoke-static {v1, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "risk_verifying_popup_close_btn"

    const-string v3, "button"

    invoke-direct {v0, v2, v3, v1}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    const-string v1, "verify_method"

    invoke-virtual {v0, v1, p1}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p1

    const-string v0, "authentication_result"

    invoke-virtual {p1, v0, p2}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p1

    invoke-virtual {p1}, Lp5/f;->c()Z

    return-void
.end method


# virtual methods
.method public a(Lt5/g0$a;)V
    .locals 2

    const-string v0, "authorizeListener"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lt5/j0;->c:Lcom/miui/packageInstaller/model/RiskControlConfig;

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/RiskControlConfig;->getRcivt()I

    move-result v0

    if-eqz v0, :cond_3

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    goto :goto_2

    :cond_0
    iget-object v0, p0, Lt5/j0;->d:Lcom/miui/packageInstaller/model/ApkInfo;

    if-eqz v0, :cond_1

    invoke-direct {p0, p1}, Lt5/j0;->k(Lt5/g0$a;)V

    sget-object v0, La8/v;->a:La8/v;

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_4

    goto :goto_1

    :cond_2
    invoke-direct {p0, p1}, Lt5/j0;->k(Lt5/g0$a;)V

    goto :goto_2

    :cond_3
    :goto_1
    invoke-interface {p1, p0}, Lt5/g0$a;->a(Lt5/g0;)V

    :cond_4
    :goto_2
    return-void
.end method
