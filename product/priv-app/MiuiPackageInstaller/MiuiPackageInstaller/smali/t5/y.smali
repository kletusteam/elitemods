.class public final Lt5/y;
.super Lt5/a0;


# instance fields
.field private final c:Lm5/e;

.field private final d:Lcom/miui/packageInstaller/model/ApkInfo;

.field private final e:Lcom/miui/packageInstaller/model/CloudParams;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lm5/e;Lcom/miui/packageInstaller/model/ApkInfo;Lcom/miui/packageInstaller/model/CloudParams;)V
    .locals 1

    const-string v0, "mContext"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mCallingPackage"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mApkInfo"

    invoke-static {p3, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mMarketControlInfo"

    invoke-static {p4, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lt5/a0;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lt5/y;->c:Lm5/e;

    iput-object p3, p0, Lt5/y;->d:Lcom/miui/packageInstaller/model/ApkInfo;

    iput-object p4, p0, Lt5/y;->e:Lcom/miui/packageInstaller/model/CloudParams;

    iget v0, p4, Lcom/miui/packageInstaller/model/CloudParams;->verifyAccount:I

    if-lez v0, :cond_0

    new-instance v0, Lt5/a;

    iget p4, p4, Lcom/miui/packageInstaller/model/CloudParams;->verifyAccount:I

    invoke-direct {v0, p1, p4, p3, p2}, Lt5/a;-><init>(Landroid/content/Context;ILcom/miui/packageInstaller/model/ApkInfo;Lm5/e;)V

    invoke-virtual {p0, v0}, Lt5/a0;->c(Lt5/g0;)V

    :cond_0
    return-void
.end method
