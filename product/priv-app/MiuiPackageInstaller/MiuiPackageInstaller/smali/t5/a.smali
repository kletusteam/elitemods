.class public final Lt5/a;
.super Lt5/g0;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lt5/a$a;,
        Lt5/a$b;
    }
.end annotation


# static fields
.field public static final e:Lt5/a$a;

.field private static f:I


# instance fields
.field private b:I

.field private final c:Lcom/miui/packageInstaller/model/ApkInfo;

.field private final d:Lm5/e;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lt5/a$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lt5/a$a;-><init>(Lm8/g;)V

    sput-object v0, Lt5/a;->e:Lt5/a$a;

    const/4 v0, 0x7

    sput v0, Lt5/a;->f:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILcom/miui/packageInstaller/model/ApkInfo;Lm5/e;)V
    .locals 1

    const-string v0, "mContext"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mApkInfo"

    invoke-static {p3, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "caller"

    invoke-static {p4, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lt5/g0;-><init>(Landroid/content/Context;)V

    iput p2, p0, Lt5/a;->b:I

    iput-object p3, p0, Lt5/a;->c:Lcom/miui/packageInstaller/model/ApkInfo;

    iput-object p4, p0, Lt5/a;->d:Lm5/e;

    return-void
.end method

.method public static final synthetic c()I
    .locals 1

    sget v0, Lt5/a;->f:I

    return v0
.end method


# virtual methods
.method public a(Lt5/g0$a;)V
    .locals 2

    const-string v0, "authorizeListener"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lt5/g0;->b()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lm2/b;->g(Landroid/content/Context;)Lm2/b;

    move-result-object v0

    invoke-virtual {v0}, Lm2/b;->i()Lx5/a;

    move-result-object v0

    const-string v1, "getInstance(mContext).riskType"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, p1, v0}, Lt5/a;->d(Lt5/g0$a;Lx5/a;)V

    return-void
.end method

.method public final d(Lt5/g0$a;Lx5/a;)V
    .locals 8

    const-string v0, "authorizeListener"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "type"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lt5/g0;->b()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lm2/b;->g(Landroid/content/Context;)Lm2/b;

    move-result-object v0

    invoke-virtual {v0}, Lm2/b;->h()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p1, p0}, Lt5/g0$a;->a(Lt5/g0;)V

    return-void

    :cond_0
    iget-object v0, p0, Lt5/a;->c:Lcom/miui/packageInstaller/model/ApkInfo;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/ApkInfo;->getLabel()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    move-object v0, v1

    :goto_0
    iget-object v2, p0, Lt5/a;->c:Lcom/miui/packageInstaller/model/ApkInfo;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/miui/packageInstaller/model/ApkInfo;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    goto :goto_1

    :cond_2
    move-object v2, v1

    :goto_1
    const/4 v3, 0x1

    const/4 v4, 0x0

    if-eqz v2, :cond_3

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    invoke-virtual {p0}, Lt5/g0;->b()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    const v6, 0x7f0d006d

    invoke-virtual {v5, v6, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const v5, 0x7f0a0073

    invoke-virtual {v1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    invoke-virtual {v5, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    const v2, 0x7f0a0074

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0a01ad

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lt5/g0;->b()Landroid/content/Context;

    move-result-object v2

    const v5, 0x7f1100de

    new-array v6, v3, [Ljava/lang/Object;

    iget-object v7, p0, Lt5/a;->d:Lm5/e;

    iget-object v7, v7, Lm5/e;->e:Ljava/lang/String;

    aput-object v7, v6, v4

    invoke-virtual {v2, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    new-instance v0, Lx5/j$a;

    invoke-virtual {p0}, Lt5/g0;->b()Landroid/content/Context;

    move-result-object v2

    const-string v5, "null cannot be cast to non-null type android.app.Activity"

    invoke-static {v2, v5}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Landroid/app/Activity;

    invoke-direct {v0, v2}, Lx5/j$a;-><init>(Landroid/app/Activity;)V

    const v2, 0x7f11030e

    invoke-virtual {v0, v2}, Lx5/j$a;->l(I)Lx5/j$a;

    move-result-object v0

    const v2, 0x7f1103e3

    invoke-virtual {v0, v2}, Lx5/j$a;->h(I)Lx5/j$a;

    move-result-object v0

    const v2, 0x7f1100ae

    invoke-virtual {v0, v2}, Lx5/j$a;->f(I)Lx5/j$a;

    move-result-object v0

    const v2, 0x7f1100ad

    invoke-virtual {v0, v2}, Lx5/j$a;->e(I)Lx5/j$a;

    move-result-object v0

    if-eqz v1, :cond_4

    invoke-virtual {v0, v1}, Lx5/j$a;->g(Landroid/view/View;)Lx5/j$a;

    :cond_4
    sget-object v1, Lt5/a$b;->a:[I

    invoke-virtual {p2}, Ljava/lang/Enum;->ordinal()I

    move-result p2

    aget p2, v1, p2

    const/4 v1, 0x4

    const/4 v2, 0x3

    const/4 v5, 0x2

    if-eq p2, v3, :cond_8

    if-eq p2, v5, :cond_7

    if-eq p2, v2, :cond_6

    if-eq p2, v1, :cond_5

    new-array p2, v1, [Lx5/a;

    sget-object v1, Lx5/a;->f:Lx5/a;

    aput-object v1, p2, v4

    sget-object v1, Lx5/a;->d:Lx5/a;

    aput-object v1, p2, v3

    sget-object v1, Lx5/a;->e:Lx5/a;

    aput-object v1, p2, v5

    sget-object v1, Lx5/a;->c:Lx5/a;

    aput-object v1, p2, v2

    invoke-virtual {v0, p2}, Lx5/j$a;->d([Lx5/a;)Lx5/j$a;

    goto :goto_2

    :cond_5
    new-array p2, v1, [Lx5/a;

    sget-object v1, Lx5/a;->e:Lx5/a;

    aput-object v1, p2, v4

    sget-object v1, Lx5/a;->c:Lx5/a;

    aput-object v1, p2, v3

    sget-object v1, Lx5/a;->f:Lx5/a;

    aput-object v1, p2, v5

    sget-object v1, Lx5/a;->d:Lx5/a;

    aput-object v1, p2, v2

    invoke-virtual {v0, p2}, Lx5/j$a;->d([Lx5/a;)Lx5/j$a;

    goto :goto_2

    :cond_6
    new-array p2, v1, [Lx5/a;

    sget-object v1, Lx5/a;->c:Lx5/a;

    aput-object v1, p2, v4

    sget-object v1, Lx5/a;->f:Lx5/a;

    aput-object v1, p2, v3

    sget-object v1, Lx5/a;->d:Lx5/a;

    aput-object v1, p2, v5

    sget-object v1, Lx5/a;->e:Lx5/a;

    aput-object v1, p2, v2

    invoke-virtual {v0, p2}, Lx5/j$a;->d([Lx5/a;)Lx5/j$a;

    goto :goto_2

    :cond_7
    new-array p2, v1, [Lx5/a;

    sget-object v1, Lx5/a;->d:Lx5/a;

    aput-object v1, p2, v4

    sget-object v1, Lx5/a;->f:Lx5/a;

    aput-object v1, p2, v3

    sget-object v1, Lx5/a;->e:Lx5/a;

    aput-object v1, p2, v5

    sget-object v1, Lx5/a;->c:Lx5/a;

    aput-object v1, p2, v2

    invoke-virtual {v0, p2}, Lx5/j$a;->d([Lx5/a;)Lx5/j$a;

    goto :goto_2

    :cond_8
    new-array p2, v1, [Lx5/a;

    sget-object v1, Lx5/a;->f:Lx5/a;

    aput-object v1, p2, v4

    sget-object v1, Lx5/a;->d:Lx5/a;

    aput-object v1, p2, v3

    sget-object v1, Lx5/a;->e:Lx5/a;

    aput-object v1, p2, v5

    sget-object v1, Lx5/a;->c:Lx5/a;

    aput-object v1, p2, v2

    invoke-virtual {v0, p2}, Lx5/j$a;->d([Lx5/a;)Lx5/j$a;

    :goto_2
    invoke-virtual {v0}, Lx5/j$a;->a()Lx5/j;

    move-result-object p2

    new-instance v0, Lt5/a$c;

    invoke-direct {v0, p1, p0}, Lt5/a$c;-><init>(Lt5/g0$a;Lt5/a;)V

    invoke-virtual {p2, v0}, Lx5/j;->p(Ll8/p;)V

    return-void
.end method
