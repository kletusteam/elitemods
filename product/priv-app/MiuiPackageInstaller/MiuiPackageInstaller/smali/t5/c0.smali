.class public final Lt5/c0;
.super Lt5/g0;


# instance fields
.field private final b:Landroid/app/Activity;

.field private final c:Lcom/miui/packageInstaller/model/RiskAppProtectionGuardTip;

.field private final d:Lcom/miui/packageInstaller/model/ApkInfo;

.field private final e:Lm5/e;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/miui/packageInstaller/model/RiskAppProtectionGuardTip;Lcom/miui/packageInstaller/model/ApkInfo;Lm5/e;)V
    .locals 1

    const-string v0, "activity"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "riskAppTipModel"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "caller"

    invoke-static {p4, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lt5/g0;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lt5/c0;->b:Landroid/app/Activity;

    iput-object p2, p0, Lt5/c0;->c:Lcom/miui/packageInstaller/model/RiskAppProtectionGuardTip;

    iput-object p3, p0, Lt5/c0;->d:Lcom/miui/packageInstaller/model/ApkInfo;

    iput-object p4, p0, Lt5/c0;->e:Lm5/e;

    return-void
.end method

.method public static final synthetic c(Lt5/c0;Lt5/g0$a;)V
    .locals 0

    invoke-direct {p0, p1}, Lt5/c0;->e(Lt5/g0$a;)V

    return-void
.end method

.method private final d(Ll8/a;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ll8/a<",
            "La8/v;",
            ">;)V"
        }
    .end annotation

    new-instance v0, La6/s;

    iget-object v1, p0, Lt5/c0;->d:Lcom/miui/packageInstaller/model/ApkInfo;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/miui/packageInstaller/model/ApkInfo;->getLabel()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    iget-object v2, p0, Lt5/c0;->c:Lcom/miui/packageInstaller/model/RiskAppProtectionGuardTip;

    iget-object v3, p0, Lt5/c0;->b:Landroid/app/Activity;

    invoke-direct {v0, v1, v2, v3}, La6/s;-><init>(Ljava/lang/String;Lcom/miui/packageInstaller/model/RiskAppProtectionGuardTip;Landroid/content/Context;)V

    invoke-virtual {v0, p1}, La6/s;->n(Ll8/a;)V

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method private final e(Lt5/g0$a;)V
    .locals 5

    iget-object v0, p0, Lt5/c0;->d:Lcom/miui/packageInstaller/model/ApkInfo;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lt5/c0;->b:Landroid/app/Activity;

    invoke-static {v1}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->isSafeModelEnable(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Lx5/d;

    iget-object v1, p0, Lt5/c0;->b:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lx5/d;-><init>(Landroid/app/Activity;)V

    new-instance v1, Lt5/c0$b;

    invoke-direct {v1, p1, p0}, Lt5/c0$b;-><init>(Lt5/g0$a;Lt5/c0;)V

    sget-object p1, Lx5/a;->f:Lx5/a;

    invoke-virtual {v0, v1, p1}, Lx5/d;->h(Ll8/p;Lx5/a;)V

    goto :goto_0

    :cond_0
    new-instance v1, Lt5/a;

    iget-object v2, p0, Lt5/c0;->b:Landroid/app/Activity;

    const/4 v3, 0x1

    iget-object v4, p0, Lt5/c0;->e:Lm5/e;

    invoke-direct {v1, v2, v3, v0, v4}, Lt5/a;-><init>(Landroid/content/Context;ILcom/miui/packageInstaller/model/ApkInfo;Lm5/e;)V

    new-instance v0, Lt5/c0$c;

    invoke-direct {v0, p1, p0}, Lt5/c0$c;-><init>(Lt5/g0$a;Lt5/c0;)V

    sget-object p1, Lx5/a;->f:Lx5/a;

    invoke-virtual {v1, v0, p1}, Lt5/a;->d(Lt5/g0$a;Lx5/a;)V

    :cond_1
    :goto_0
    return-void
.end method


# virtual methods
.method public a(Lt5/g0$a;)V
    .locals 1

    const-string v0, "authorizeListener"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lt5/c0$a;

    invoke-direct {v0, p0, p1}, Lt5/c0$a;-><init>(Lt5/c0;Lt5/g0$a;)V

    invoke-direct {p0, v0}, Lt5/c0;->d(Ll8/a;)V

    return-void
.end method
