.class public final Lt5/g;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lt5/g$a;
    }
.end annotation


# static fields
.field public static final b:Lt5/g$a;


# instance fields
.field private a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lm5/z0;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lt5/g$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lt5/g$a;-><init>(Lm8/g;)V

    sput-object v0, Lt5/g;->b:Lt5/g$a;

    return-void
.end method

.method public constructor <init>(Lm5/z0;)V
    .locals 1

    const-string v0, "activity"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lt5/g;->a:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method public static synthetic a(Lm5/z0;Landroid/content/DialogInterface;)V
    .locals 0

    invoke-static {p0, p1}, Lt5/g;->f(Lm5/z0;Landroid/content/DialogInterface;)V

    return-void
.end method

.method public static synthetic b(Lm5/z0;Landroid/content/DialogInterface;)V
    .locals 0

    invoke-static {p0, p1}, Lt5/g;->g(Lm5/z0;Landroid/content/DialogInterface;)V

    return-void
.end method

.method public static synthetic c(Lm5/z0;Landroid/content/DialogInterface;)V
    .locals 0

    invoke-static {p0, p1}, Lt5/g;->e(Lm5/z0;Landroid/content/DialogInterface;)V

    return-void
.end method

.method private static final e(Lm5/z0;Landroid/content/DialogInterface;)V
    .locals 0

    const-string p1, "$activity"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lmiuix/appcompat/app/j;->finish()V

    return-void
.end method

.method private static final f(Lm5/z0;Landroid/content/DialogInterface;)V
    .locals 0

    const-string p1, "$activity"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lmiuix/appcompat/app/j;->finish()V

    return-void
.end method

.method private static final g(Lm5/z0;Landroid/content/DialogInterface;)V
    .locals 0

    const-string p1, "$activity"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lmiuix/appcompat/app/j;->finish()V

    return-void
.end method


# virtual methods
.method public final d(I)V
    .locals 4

    iget-object v0, p0, Lt5/g;->a:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lm5/z0;

    if-nez v0, :cond_0

    goto/16 :goto_1

    :cond_0
    const/16 v1, 0xb

    const-string v2, "activity.getString(R.string.ok)"

    const v3, 0x7f110259

    if-eq p1, v1, :cond_3

    const/16 v1, 0x16

    if-eq p1, v1, :cond_2

    const/16 v1, 0x2c

    if-eq p1, v1, :cond_1

    const/16 v1, 0x21

    if-eq p1, v1, :cond_3

    const/16 v1, 0x22

    if-eq p1, v1, :cond_3

    goto/16 :goto_1

    :cond_1
    new-instance p1, La6/g;

    invoke-direct {p1, v0}, La6/g;-><init>(Landroid/content/Context;)V

    const v1, 0x7f1100e5

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, La6/g;->i(Ljava/lang/String;)La6/g;

    move-result-object p1

    invoke-virtual {v0, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v2, Lt5/g$d;

    invoke-direct {v2, v0}, Lt5/g$d;-><init>(Lm5/z0;)V

    invoke-virtual {p1, v1, v2}, La6/g;->k(Ljava/lang/String;La6/g$a;)La6/g;

    move-result-object p1

    new-instance v1, Lt5/e;

    invoke-direct {v1, v0}, Lt5/e;-><init>(Lm5/z0;)V

    :goto_0
    invoke-virtual {p1, v1}, Landroid/app/Dialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    :try_start_0
    invoke-virtual {p1}, Landroid/app/Dialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :cond_2
    const/4 p1, -0x4

    invoke-virtual {v0, p1}, Lq2/b;->H0(I)V

    new-instance p1, La6/g;

    invoke-direct {p1, v0}, La6/g;-><init>(Landroid/content/Context;)V

    const v1, 0x7f1101bc

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, La6/g;->i(Ljava/lang/String;)La6/g;

    move-result-object p1

    invoke-virtual {v0, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v2, Lt5/g$c;

    invoke-direct {v2, v0}, Lt5/g$c;-><init>(Lm5/z0;)V

    invoke-virtual {p1, v1, v2}, La6/g;->k(Ljava/lang/String;La6/g$a;)La6/g;

    move-result-object p1

    new-instance v1, Lt5/d;

    invoke-direct {v1, v0}, Lt5/d;-><init>(Lm5/z0;)V

    goto :goto_0

    :cond_3
    const/4 p1, -0x2

    invoke-virtual {v0, p1}, Lq2/b;->H0(I)V

    new-instance p1, La6/g;

    invoke-direct {p1, v0}, La6/g;-><init>(Landroid/content/Context;)V

    const/high16 v1, 0x7f110000

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, La6/g;->i(Ljava/lang/String;)La6/g;

    move-result-object p1

    invoke-virtual {v0, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v2, Lt5/g$b;

    invoke-direct {v2, v0}, Lt5/g$b;-><init>(Lm5/z0;)V

    invoke-virtual {p1, v1, v2}, La6/g;->k(Ljava/lang/String;La6/g$a;)La6/g;

    move-result-object p1

    new-instance v1, Lt5/f;

    invoke-direct {v1, v0}, Lt5/f;-><init>(Lm5/z0;)V

    goto :goto_0

    :catch_0
    :cond_4
    :goto_1
    return-void
.end method
