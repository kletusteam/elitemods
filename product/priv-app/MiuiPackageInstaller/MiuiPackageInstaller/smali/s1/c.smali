.class public Ls1/c;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ls1/c$b;
    }
.end annotation


# instance fields
.field private a:Ljava/lang/Object;

.field private b:Ls1/c$b;

.field private c:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ls1/c$b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Ls1/c;->a:Ljava/lang/Object;

    new-instance v0, Ls1/c$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ls1/c$b;-><init>(Ls1/c$a;)V

    iput-object v0, p0, Ls1/c;->b:Ls1/c$b;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Ls1/c;->c:Ljava/util/HashMap;

    return-void
.end method

.method private a(Ljava/lang/String;)Ls1/c$b;
    .locals 3

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Ls1/c;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ls1/c$b;

    if-nez v0, :cond_3

    iget-object v1, p0, Ls1/c;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Ls1/c;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ls1/c$b;

    if-nez v0, :cond_1

    new-instance v0, Ls1/c$b;

    const/4 v2, 0x0

    invoke-direct {v0, v2}, Ls1/c$b;-><init>(Ls1/c$a;)V

    iget-object v2, p0, Ls1/c;->c:Ljava/util/HashMap;

    invoke-virtual {v2, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    monitor-exit v1

    goto :goto_1

    :catchall_0
    move-exception p1

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    :cond_2
    :goto_0
    iget-object v0, p0, Ls1/c;->b:Ls1/c$b;

    :cond_3
    :goto_1
    return-object v0
.end method


# virtual methods
.method public b(Ljava/lang/String;Lo1/g;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Ls1/c;->c(Ljava/lang/String;Lo1/g;Ljava/lang/String;)V

    return-void
.end method

.method public c(Ljava/lang/String;Lo1/g;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p3}, Ls1/c;->a(Ljava/lang/String;)Ls1/c$b;

    move-result-object p3

    invoke-virtual {p3, p1, p2}, Ls1/c$b;->a(Ljava/lang/String;Lo1/g;)V

    return-void
.end method

.method public d(Ljava/lang/String;Lo1/g;)Z
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Ls1/c;->e(Ljava/lang/String;Lo1/g;Ljava/lang/String;)Z

    move-result p1

    return p1
.end method

.method public e(Ljava/lang/String;Lo1/g;Ljava/lang/String;)Z
    .locals 0

    invoke-direct {p0, p3}, Ls1/c;->a(Ljava/lang/String;)Ls1/c$b;

    move-result-object p3

    invoke-virtual {p3, p1, p2}, Ls1/c$b;->b(Ljava/lang/String;Lo1/g;)Z

    move-result p1

    return p1
.end method
