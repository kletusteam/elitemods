.class Ls1/g$b;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Ls1/g;->g(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ls1/g;


# direct methods
.method constructor <init>(Ls1/g;)V
    .locals 0

    iput-object p1, p0, Ls1/g$b;->a:Ls1/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    iget-object v0, p0, Ls1/g$b;->a:Ls1/g;

    invoke-static {v0}, Ls1/g;->j(Ls1/g;)Lt1/j;

    move-result-object v0

    invoke-virtual {v0}, Lt1/j;->a()Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "network change, clean record"

    invoke-static {v1}, Lz1/a;->b(Ljava/lang/String;)V

    iget-object v1, p0, Ls1/g$b;->a:Ls1/g;

    invoke-static {v1}, Ls1/g;->j(Ls1/g;)Lt1/j;

    move-result-object v1

    invoke-virtual {v1}, Lt1/j;->j()V

    iget-object v1, p0, Ls1/g$b;->a:Ls1/g;

    invoke-static {v1}, Ls1/g;->m(Ls1/g;)Z

    move-result v1

    if-eqz v1, :cond_7

    iget-object v1, p0, Ls1/g$b;->a:Ls1/g;

    iget-object v1, v1, Ls1/g;->a:Ls1/d;

    invoke-virtual {v1}, Ls1/d;->C()Z

    move-result v1

    if-eqz v1, :cond_7

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    sget-object v6, Lo1/g;->a:Lo1/g;

    if-ne v5, v6, :cond_0

    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    sget-object v6, Lo1/g;->b:Lo1/g;

    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    if-ne v5, v6, :cond_1

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_3

    iget-object v0, p0, Ls1/g$b;->a:Ls1/g;

    iget-object v0, v0, Ls1/g;->g:Lt1/o;

    sget-object v4, Lo1/g;->a:Lo1/g;

    invoke-virtual {v0, v1, v4}, Lt1/o;->d(Ljava/util/ArrayList;Lo1/g;)V

    :cond_3
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_4

    iget-object v0, p0, Ls1/g$b;->a:Ls1/g;

    iget-object v0, v0, Ls1/g;->g:Lt1/o;

    sget-object v4, Lo1/g;->b:Lo1/g;

    invoke-virtual {v0, v2, v4}, Lt1/o;->d(Ljava/util/ArrayList;Lo1/g;)V

    :cond_4
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_5

    iget-object v0, p0, Ls1/g$b;->a:Ls1/g;

    iget-object v0, v0, Ls1/g;->g:Lt1/o;

    sget-object v4, Lo1/g;->c:Lo1/g;

    invoke-virtual {v0, v3, v4}, Lt1/o;->d(Ljava/util/ArrayList;Lo1/g;)V

    :cond_5
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_6

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_6

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_7

    :cond_6
    const-string v0, "network change, resolve hosts"

    invoke-static {v0}, Lz1/a;->b(Ljava/lang/String;)V

    :cond_7
    return-void
.end method
