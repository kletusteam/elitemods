.class public Ls1/d;
.super Ljava/lang/Object;


# instance fields
.field private a:Landroid/content/Context;

.field private b:Z

.field private c:[Ljava/lang/String;

.field private d:[Ljava/lang/String;

.field private e:[I

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:[Ljava/lang/String;

.field private i:[I

.field private j:I

.field private k:I

.field private l:I

.field private m:Ljava/lang/String;

.field private n:J

.field private o:I

.field private p:Z

.field private q:Z

.field private r:Z

.field protected s:Ljava/util/concurrent/ExecutorService;

.field protected t:Ljava/util/concurrent/ExecutorService;


# direct methods
.method private constructor <init>()V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Ls1/d;->b:Z

    sget-object v0, Lo1/h;->a:[Ljava/lang/String;

    iput-object v0, p0, Ls1/d;->c:[Ljava/lang/String;

    sget-object v1, Lo1/h;->b:[Ljava/lang/String;

    iput-object v1, p0, Ls1/d;->d:[Ljava/lang/String;

    const/4 v1, 0x0

    iput-object v1, p0, Ls1/d;->e:[I

    const-string v2, "http://"

    iput-object v2, p0, Ls1/d;->g:Ljava/lang/String;

    iput-object v0, p0, Ls1/d;->h:[Ljava/lang/String;

    iput-object v1, p0, Ls1/d;->i:[I

    const/4 v0, 0x0

    iput v0, p0, Ls1/d;->j:I

    iput v0, p0, Ls1/d;->k:I

    iput v0, p0, Ls1/d;->l:I

    iput-object v1, p0, Ls1/d;->m:Ljava/lang/String;

    const-wide/16 v1, 0x0

    iput-wide v1, p0, Ls1/d;->n:J

    const/16 v1, 0x3a98

    iput v1, p0, Ls1/d;->o:I

    iput-boolean v0, p0, Ls1/d;->q:Z

    iput-boolean v0, p0, Ls1/d;->r:Z

    invoke-static {}, Ly1/c;->c()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Ls1/d;->s:Ljava/util/concurrent/ExecutorService;

    invoke-static {}, Ly1/c;->d()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Ls1/d;->t:Ljava/util/concurrent/ExecutorService;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Ls1/d;->b:Z

    sget-object v0, Lo1/h;->a:[Ljava/lang/String;

    iput-object v0, p0, Ls1/d;->c:[Ljava/lang/String;

    sget-object v1, Lo1/h;->b:[Ljava/lang/String;

    iput-object v1, p0, Ls1/d;->d:[Ljava/lang/String;

    const/4 v1, 0x0

    iput-object v1, p0, Ls1/d;->e:[I

    const-string v2, "http://"

    iput-object v2, p0, Ls1/d;->g:Ljava/lang/String;

    iput-object v0, p0, Ls1/d;->h:[Ljava/lang/String;

    iput-object v1, p0, Ls1/d;->i:[I

    const/4 v0, 0x0

    iput v0, p0, Ls1/d;->j:I

    iput v0, p0, Ls1/d;->k:I

    iput v0, p0, Ls1/d;->l:I

    iput-object v1, p0, Ls1/d;->m:Ljava/lang/String;

    const-wide/16 v1, 0x0

    iput-wide v1, p0, Ls1/d;->n:J

    const/16 v1, 0x3a98

    iput v1, p0, Ls1/d;->o:I

    iput-boolean v0, p0, Ls1/d;->q:Z

    iput-boolean v0, p0, Ls1/d;->r:Z

    invoke-static {}, Ly1/c;->c()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Ls1/d;->s:Ljava/util/concurrent/ExecutorService;

    invoke-static {}, Ly1/c;->d()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Ls1/d;->t:Ljava/util/concurrent/ExecutorService;

    iput-object p1, p0, Ls1/d;->a:Landroid/content/Context;

    iput-object p2, p0, Ls1/d;->f:Ljava/lang/String;

    invoke-static {p1, p0}, Ls1/d;->d(Landroid/content/Context;Ls1/d;)V

    return-void
.end method

.method private static d(Landroid/content/Context;Ls1/d;)V
    .locals 5

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "httpdns_config_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ls1/d;->x()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p0

    sget-object v0, Lo1/h;->a:[Ljava/lang/String;

    invoke-static {v0}, Ly1/a;->b([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "serverIps"

    invoke-interface {p0, v2, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ly1/a;->i(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Ls1/d;->h:[Ljava/lang/String;

    const-string v0, "ports"

    const/4 v2, 0x0

    invoke-interface {p0, v0, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ly1/a;->h(Ljava/lang/String;)[I

    move-result-object v0

    iput-object v0, p1, Ls1/d;->i:[I

    const-string v0, "current"

    invoke-interface {p0, v0, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p1, Ls1/d;->k:I

    const-string v0, "last"

    invoke-interface {p0, v0, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p1, Ls1/d;->j:I

    const-string v0, "servers_last_updated_time"

    const-wide/16 v3, 0x0

    invoke-interface {p0, v0, v3, v4}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p1, Ls1/d;->n:J

    const-string v0, "region"

    invoke-interface {p0, v0, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Ls1/d;->m:Ljava/lang/String;

    const-string v0, "enable"

    const/4 v1, 0x1

    invoke-interface {p0, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result p0

    iput-boolean p0, p1, Ls1/d;->b:Z

    return-void
.end method

.method private h([Ljava/lang/String;[I[Ljava/lang/String;[I)Z
    .locals 0

    invoke-static {p1, p3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-static {p2, p4}, Ljava/util/Arrays;->equals([I[I)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private static n(Landroid/content/Context;Ls1/d;)V
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ApplySharedPref"
        }
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "httpdns_config_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ls1/d;->x()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p0

    invoke-interface {p0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p0

    iget-object v0, p1, Ls1/d;->h:[Ljava/lang/String;

    invoke-static {v0}, Ly1/a;->b([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "serverIps"

    invoke-interface {p0, v1, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    iget-object v0, p1, Ls1/d;->i:[I

    invoke-static {v0}, Ly1/a;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "ports"

    invoke-interface {p0, v1, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    iget v0, p1, Ls1/d;->k:I

    const-string v1, "current"

    invoke-interface {p0, v1, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    iget v0, p1, Ls1/d;->j:I

    const-string v1, "last"

    invoke-interface {p0, v1, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    iget-wide v0, p1, Ls1/d;->n:J

    const-string v2, "servers_last_updated_time"

    invoke-interface {p0, v2, v0, v1}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    iget-object v0, p1, Ls1/d;->m:Ljava/lang/String;

    const-string v1, "region"

    invoke-interface {p0, v1, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    iget-boolean p1, p1, Ls1/d;->b:Z

    const-string v0, "enable"

    invoke-interface {p0, v0, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {p0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method private z()I
    .locals 2

    iget-object v0, p0, Ls1/d;->g:Ljava/lang/String;

    const-string v1, "http://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x50

    return v0

    :cond_0
    const/16 v0, 0x1bb

    return v0
.end method


# virtual methods
.method public A()I
    .locals 3

    iget-object v0, p0, Ls1/d;->i:[I

    if-eqz v0, :cond_1

    iget v1, p0, Ls1/d;->k:I

    array-length v2, v0

    if-ge v1, v2, :cond_1

    if-gez v1, :cond_0

    goto :goto_0

    :cond_0
    aget v0, v0, v1

    return v0

    :cond_1
    :goto_0
    invoke-direct {p0}, Ls1/d;->z()I

    move-result v0

    return v0
.end method

.method public B()I
    .locals 1

    iget v0, p0, Ls1/d;->o:I

    return v0
.end method

.method public C()Z
    .locals 1

    iget-boolean v0, p0, Ls1/d;->b:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Ls1/d;->p:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Ls1/d;->q:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public D(Z)V
    .locals 0

    iput-boolean p1, p0, Ls1/d;->b:Z

    iget-object p1, p0, Ls1/d;->a:Landroid/content/Context;

    invoke-static {p1, p0}, Ls1/d;->n(Landroid/content/Context;Ls1/d;)V

    return-void
.end method

.method public E(Z)V
    .locals 0

    if-eqz p1, :cond_0

    const-string p1, "https://"

    goto :goto_0

    :cond_0
    const-string p1, "http://"

    :goto_0
    iput-object p1, p0, Ls1/d;->g:Ljava/lang/String;

    return-void
.end method

.method public a()Ljava/util/concurrent/ExecutorService;
    .locals 1

    iget-object v0, p0, Ls1/d;->s:Ljava/util/concurrent/ExecutorService;

    return-object v0
.end method

.method public b()Ls1/d;
    .locals 4

    new-instance v0, Ls1/d;

    invoke-direct {v0}, Ls1/d;-><init>()V

    iget-object v1, p0, Ls1/d;->a:Landroid/content/Context;

    iput-object v1, v0, Ls1/d;->a:Landroid/content/Context;

    iget-object v1, p0, Ls1/d;->f:Ljava/lang/String;

    iput-object v1, v0, Ls1/d;->f:Ljava/lang/String;

    iget-object v1, p0, Ls1/d;->g:Ljava/lang/String;

    iput-object v1, v0, Ls1/d;->g:Ljava/lang/String;

    iget-object v1, p0, Ls1/d;->m:Ljava/lang/String;

    iput-object v1, v0, Ls1/d;->m:Ljava/lang/String;

    iget-object v1, p0, Ls1/d;->h:[Ljava/lang/String;

    const/4 v2, 0x0

    if-nez v1, :cond_0

    move-object v1, v2

    goto :goto_0

    :cond_0
    array-length v3, v1

    invoke-static {v1, v3}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    :goto_0
    iput-object v1, v0, Ls1/d;->h:[Ljava/lang/String;

    iget-object v1, p0, Ls1/d;->i:[I

    if-nez v1, :cond_1

    goto :goto_1

    :cond_1
    array-length v2, v1

    invoke-static {v1, v2}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v2

    :goto_1
    iput-object v2, v0, Ls1/d;->i:[I

    iget v1, p0, Ls1/d;->j:I

    iput v1, v0, Ls1/d;->j:I

    iget v1, p0, Ls1/d;->k:I

    iput v1, v0, Ls1/d;->k:I

    iget-wide v1, p0, Ls1/d;->n:J

    iput-wide v1, v0, Ls1/d;->n:J

    iget v1, p0, Ls1/d;->o:I

    iput v1, v0, Ls1/d;->o:I

    iget-object v1, p0, Ls1/d;->s:Ljava/util/concurrent/ExecutorService;

    iput-object v1, v0, Ls1/d;->s:Ljava/util/concurrent/ExecutorService;

    iget-object v1, p0, Ls1/d;->c:[Ljava/lang/String;

    iput-object v1, v0, Ls1/d;->c:[Ljava/lang/String;

    iget-object v1, p0, Ls1/d;->e:[I

    iput-object v1, v0, Ls1/d;->e:[I

    return-object v0
.end method

.method public c()V
    .locals 2

    iget-object v0, p0, Ls1/d;->d:[Ljava/lang/String;

    if-eqz v0, :cond_0

    array-length v1, v0

    if-lez v1, :cond_0

    iget v1, p0, Ls1/d;->l:I

    add-int/lit8 v1, v1, 0x1

    array-length v0, v0

    rem-int/2addr v1, v0

    iput v1, p0, Ls1/d;->l:I

    :cond_0
    return-void
.end method

.method public e(Ljava/lang/String;I)Z
    .locals 3

    iget-object v0, p0, Ls1/d;->h:[Ljava/lang/String;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    iget v2, p0, Ls1/d;->k:I

    aget-object v0, v0, v2

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    iget-object p1, p0, Ls1/d;->i:[I

    if-eqz p1, :cond_1

    iget v0, p0, Ls1/d;->k:I

    aget p1, p1, v0

    if-eq p1, p2, :cond_1

    goto :goto_0

    :cond_1
    iget p1, p0, Ls1/d;->k:I

    const/4 p2, 0x1

    add-int/2addr p1, p2

    iput p1, p0, Ls1/d;->k:I

    iget-object v0, p0, Ls1/d;->h:[Ljava/lang/String;

    array-length v0, v0

    if-lt p1, v0, :cond_2

    iput v1, p0, Ls1/d;->k:I

    :cond_2
    iget p1, p0, Ls1/d;->k:I

    iget v0, p0, Ls1/d;->j:I

    if-ne p1, v0, :cond_3

    move v1, p2

    :cond_3
    :goto_0
    return v1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_3

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto/16 :goto_1

    :cond_1
    check-cast p1, Ls1/d;

    iget-boolean v2, p0, Ls1/d;->b:Z

    iget-boolean v3, p1, Ls1/d;->b:Z

    if-ne v2, v3, :cond_2

    iget v2, p0, Ls1/d;->j:I

    iget v3, p1, Ls1/d;->j:I

    if-ne v2, v3, :cond_2

    iget v2, p0, Ls1/d;->k:I

    iget v3, p1, Ls1/d;->k:I

    if-ne v2, v3, :cond_2

    iget-wide v2, p0, Ls1/d;->n:J

    iget-wide v4, p1, Ls1/d;->n:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    iget v2, p0, Ls1/d;->o:I

    iget v3, p1, Ls1/d;->o:I

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Ls1/d;->a:Landroid/content/Context;

    iget-object v3, p1, Ls1/d;->a:Landroid/content/Context;

    invoke-static {v2, v3}, Ly1/a;->m(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Ls1/d;->c:[Ljava/lang/String;

    iget-object v3, p1, Ls1/d;->c:[Ljava/lang/String;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Ls1/d;->e:[I

    iget-object v3, p1, Ls1/d;->e:[I

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Ls1/d;->f:Ljava/lang/String;

    iget-object v3, p1, Ls1/d;->f:Ljava/lang/String;

    invoke-static {v2, v3}, Ly1/a;->m(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Ls1/d;->g:Ljava/lang/String;

    iget-object v3, p1, Ls1/d;->g:Ljava/lang/String;

    invoke-static {v2, v3}, Ly1/a;->m(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Ls1/d;->h:[Ljava/lang/String;

    iget-object v3, p1, Ls1/d;->h:[Ljava/lang/String;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Ls1/d;->i:[I

    iget-object v3, p1, Ls1/d;->i:[I

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Ls1/d;->m:Ljava/lang/String;

    iget-object v3, p1, Ls1/d;->m:Ljava/lang/String;

    invoke-static {v2, v3}, Ly1/a;->m(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Ls1/d;->s:Ljava/util/concurrent/ExecutorService;

    iget-object p1, p1, Ls1/d;->s:Ljava/util/concurrent/ExecutorService;

    invoke-static {v2, p1}, Ly1/a;->m(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    move v0, v1

    :goto_0
    return v0

    :cond_3
    :goto_1
    return v1
.end method

.method public f(Ljava/lang/String;[Ljava/lang/String;[I)Z
    .locals 2

    iget-object v0, p0, Ls1/d;->h:[Ljava/lang/String;

    iget-object v1, p0, Ls1/d;->i:[I

    invoke-direct {p0, v0, v1, p2, p3}, Ls1/d;->h([Ljava/lang/String;[I[Ljava/lang/String;[I)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    :cond_0
    iput-object p1, p0, Ls1/d;->m:Ljava/lang/String;

    iput-object p2, p0, Ls1/d;->h:[Ljava/lang/String;

    iput-object p3, p0, Ls1/d;->i:[I

    iput v1, p0, Ls1/d;->j:I

    iput v1, p0, Ls1/d;->k:I

    iget-object p1, p0, Ls1/d;->c:[Ljava/lang/String;

    invoke-static {p2, p1}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide p1

    iput-wide p1, p0, Ls1/d;->n:J

    :cond_1
    iget-object p1, p0, Ls1/d;->a:Landroid/content/Context;

    invoke-static {p1, p0}, Ls1/d;->n(Landroid/content/Context;Ls1/d;)V

    const/4 p1, 0x1

    return p1
.end method

.method public g([Ljava/lang/String;[I)Z
    .locals 1

    iget-object v0, p0, Ls1/d;->m:Ljava/lang/String;

    invoke-virtual {p0, v0, p1, p2}, Ls1/d;->f(Ljava/lang/String;[Ljava/lang/String;[I)Z

    move-result p1

    return p1
.end method

.method public hashCode()I
    .locals 3

    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/Object;

    iget-object v1, p0, Ls1/d;->a:Landroid/content/Context;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-boolean v1, p0, Ls1/d;->b:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-object v1, p0, Ls1/d;->f:Ljava/lang/String;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    iget-object v1, p0, Ls1/d;->g:Ljava/lang/String;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    iget v1, p0, Ls1/d;->j:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x4

    aput-object v1, v0, v2

    iget v1, p0, Ls1/d;->k:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x5

    aput-object v1, v0, v2

    iget-object v1, p0, Ls1/d;->m:Ljava/lang/String;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    iget-wide v1, p0, Ls1/d;->n:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const/4 v2, 0x7

    aput-object v1, v0, v2

    iget v1, p0, Ls1/d;->o:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x8

    aput-object v1, v0, v2

    iget-object v1, p0, Ls1/d;->s:Ljava/util/concurrent/ExecutorService;

    const/16 v2, 0x9

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Ls1/d;->c:[Ljava/lang/String;

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Ls1/d;->e:[I

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Ls1/d;->h:[Ljava/lang/String;

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Ls1/d;->i:[I

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public i()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ls1/d;->h:[Ljava/lang/String;

    return-object v0
.end method

.method public j()I
    .locals 1

    iget-object v0, p0, Ls1/d;->c:[Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    array-length v0, v0

    :goto_0
    return v0
.end method

.method public k()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ls1/d;->m:Ljava/lang/String;

    return-object v0
.end method

.method public l()Ljava/util/concurrent/ExecutorService;
    .locals 1

    iget-object v0, p0, Ls1/d;->t:Ljava/util/concurrent/ExecutorService;

    return-object v0
.end method

.method public m()V
    .locals 2

    iget-object v0, p0, Ls1/d;->c:[Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v1, p0, Ls1/d;->e:[I

    invoke-virtual {p0, v0, v1}, Ls1/d;->g([Ljava/lang/String;[I)Z

    :cond_0
    return-void
.end method

.method public o(Z)V
    .locals 0

    iput-boolean p1, p0, Ls1/d;->p:Z

    return-void
.end method

.method public p(Ljava/lang/String;I)Z
    .locals 3

    iget-object v0, p0, Ls1/d;->h:[Ljava/lang/String;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    iget v2, p0, Ls1/d;->k:I

    aget-object v0, v0, v2

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    iget-object p1, p0, Ls1/d;->i:[I

    if-eqz p1, :cond_1

    iget v0, p0, Ls1/d;->k:I

    aget p1, p1, v0

    if-ne p1, p2, :cond_2

    :cond_1
    iget p1, p0, Ls1/d;->k:I

    iput p1, p0, Ls1/d;->j:I

    iget-object p1, p0, Ls1/d;->a:Landroid/content/Context;

    invoke-static {p1, p0}, Ls1/d;->n(Landroid/content/Context;Ls1/d;)V

    const/4 p1, 0x1

    return p1

    :cond_2
    return v1
.end method

.method public q()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ls1/d;->g:Ljava/lang/String;

    return-object v0
.end method

.method public r(Z)V
    .locals 0

    iput-boolean p1, p0, Ls1/d;->q:Z

    return-void
.end method

.method public s()Z
    .locals 4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Ls1/d;->n:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x5265c00

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    iget-object v0, p0, Ls1/d;->h:[Ljava/lang/String;

    if-eqz v0, :cond_0

    array-length v0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public t()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Ls1/d;->h:[Ljava/lang/String;

    if-eqz v0, :cond_1

    iget v1, p0, Ls1/d;->k:I

    array-length v2, v0

    if-ge v1, v2, :cond_1

    if-gez v1, :cond_0

    goto :goto_0

    :cond_0
    aget-object v0, v0, v1

    return-object v0

    :cond_1
    :goto_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public u(Z)V
    .locals 0

    iput-boolean p1, p0, Ls1/d;->r:Z

    return-void
.end method

.method public v()Z
    .locals 1

    iget-boolean v0, p0, Ls1/d;->r:Z

    return v0
.end method

.method public w()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Ls1/d;->d:[Ljava/lang/String;

    if-eqz v0, :cond_1

    iget v1, p0, Ls1/d;->l:I

    array-length v2, v0

    if-lt v1, v2, :cond_0

    goto :goto_0

    :cond_0
    aget-object v0, v0, v1

    return-object v0

    :cond_1
    :goto_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public x()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ls1/d;->f:Ljava/lang/String;

    return-object v0
.end method

.method public y()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Ls1/d;->a:Landroid/content/Context;

    return-object v0
.end method
