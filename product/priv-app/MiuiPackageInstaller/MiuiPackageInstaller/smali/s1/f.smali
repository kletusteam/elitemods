.class public Ls1/f;
.super Ljava/lang/Object;


# instance fields
.field private a:Ls1/e;

.field private b:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Lo1/d;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ls1/a;


# direct methods
.method public constructor <init>(Ls1/e;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ls1/a;

    invoke-direct {v0}, Ls1/a;-><init>()V

    iput-object v0, p0, Ls1/f;->c:Ls1/a;

    iput-object p1, p0, Ls1/f;->a:Ls1/e;

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Ls1/f;->b:Ljava/util/HashMap;

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lo1/d;
    .locals 1

    if-nez p1, :cond_0

    const-string p1, "init httpdns with null context!!"

    :goto_0
    invoke-static {p1}, Lz1/a;->c(Ljava/lang/String;)V

    iget-object p1, p0, Ls1/f;->c:Ls1/a;

    return-object p1

    :cond_0
    if-eqz p2, :cond_4

    const-string v0, ""

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_2

    :cond_1
    iget-object v0, p0, Ls1/f;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo1/d;

    if-nez v0, :cond_2

    iget-object v0, p0, Ls1/f;->a:Ls1/e;

    invoke-interface {v0, p1, p2, p3}, Ls1/e;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lo1/d;

    move-result-object v0

    iget-object p1, p0, Ls1/f;->b:Ljava/util/HashMap;

    invoke-virtual {p1, p2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_2
    instance-of p1, v0, Ls1/g;

    if-eqz p1, :cond_3

    move-object p1, v0

    check-cast p1, Ls1/g;

    invoke-virtual {p1, p3}, Ls1/g;->o(Ljava/lang/String;)V

    :cond_3
    :goto_1
    return-object v0

    :cond_4
    :goto_2
    const-string p1, "init httpdns with emtpy account!!"

    goto :goto_0
.end method
