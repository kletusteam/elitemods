.class public Ls1/g;
.super Ljava/lang/Object;

# interfaces
.implements Lo1/d;
.implements Lw1/a$b;
.implements La2/d$c;


# instance fields
.field protected a:Ls1/d;

.field private b:Lt1/j;

.field protected c:Lt1/g;

.field protected d:Lw1/a;

.field protected e:Lb2/c;

.field protected f:Lt1/k;

.field protected g:Lt1/o;

.field private h:Lt1/b;

.field private i:Ls1/i;

.field private j:Z

.field private k:Ls1/c;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 17

    move-object/from16 v1, p0

    move-object/from16 v0, p1

    move-object/from16 v2, p2

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    const/4 v3, 0x1

    iput-boolean v3, v1, Ls1/g;->j:Z

    new-instance v3, Ls1/c;

    invoke-direct {v3}, Ls1/c;-><init>()V

    iput-object v3, v1, Ls1/g;->k:Ls1/c;

    :try_start_0
    new-instance v3, Ls1/d;

    invoke-direct {v3, v0, v2}, Ls1/d;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v3, v1, Ls1/g;->a:Ls1/d;

    invoke-virtual {v1, v0, v3}, Ls1/g;->n(Landroid/content/Context;Ls1/d;)V

    iget-object v3, v1, Ls1/g;->a:Ls1/d;

    invoke-virtual {v3}, Ls1/d;->C()Z

    move-result v3

    if-nez v3, :cond_0

    const-string v0, "init fail, crashdefend"

    invoke-static {v0}, Lz1/a;->i(Ljava/lang/String;)V

    return-void

    :cond_0
    invoke-static {}, La2/d;->d()La2/d;

    move-result-object v3

    invoke-virtual {v3, v0}, La2/d;->l(Landroid/content/Context;)V

    invoke-static {}, Lo1/e;->b()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-static {}, La2/d;->d()La2/d;

    move-result-object v3

    invoke-static {v3}, La2/a;->h(La2/b;)V

    :cond_1
    new-instance v3, Lt1/b;

    invoke-direct {v3}, Lt1/b;-><init>()V

    iput-object v3, v1, Ls1/g;->h:Lt1/b;

    new-instance v3, Ls1/i;

    move-object/from16 v4, p3

    invoke-direct {v3, v4}, Ls1/i;-><init>(Ljava/lang/String;)V

    iput-object v3, v1, Ls1/g;->i:Ls1/i;

    new-instance v3, Lb2/c;

    iget-object v4, v1, Ls1/g;->a:Ls1/d;

    invoke-direct {v3, v4}, Lb2/c;-><init>(Ls1/d;)V

    iput-object v3, v1, Ls1/g;->e:Lb2/c;

    new-instance v4, Lt1/j;

    iget-object v5, v1, Ls1/g;->a:Ls1/d;

    new-instance v6, Lq1/b;

    iget-object v7, v1, Ls1/g;->a:Ls1/d;

    invoke-virtual {v7}, Ls1/d;->y()Landroid/content/Context;

    move-result-object v7

    iget-object v8, v1, Ls1/g;->a:Ls1/d;

    invoke-virtual {v8}, Ls1/d;->x()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Lq1/b;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    new-instance v7, Lt1/d;

    invoke-direct {v7}, Lt1/d;-><init>()V

    invoke-direct {v4, v5, v3, v6, v7}, Lt1/j;-><init>(Ls1/d;Lb2/c;Lq1/b;Lt1/d;)V

    iput-object v4, v1, Ls1/g;->b:Lt1/j;

    new-instance v3, Lw1/a;

    iget-object v4, v1, Ls1/g;->a:Ls1/d;

    invoke-direct {v3, v4, v1}, Lw1/a;-><init>(Ls1/d;Lw1/a$b;)V

    iput-object v3, v1, Ls1/g;->d:Lw1/a;

    new-instance v7, Lt1/g;

    iget-object v4, v1, Ls1/g;->a:Ls1/d;

    iget-object v5, v1, Ls1/g;->i:Ls1/i;

    invoke-direct {v7, v4, v3, v5}, Lt1/g;-><init>(Ls1/d;Lw1/a;Ls1/i;)V

    iput-object v7, v1, Ls1/g;->c:Lt1/g;

    new-instance v3, Lt1/k;

    iget-object v6, v1, Ls1/g;->e:Lb2/c;

    iget-object v8, v1, Ls1/g;->b:Lt1/j;

    iget-object v9, v1, Ls1/g;->h:Lt1/b;

    iget-object v10, v1, Ls1/g;->k:Ls1/c;

    move-object v5, v3

    invoke-direct/range {v5 .. v10}, Lt1/k;-><init>(Lb2/c;Lt1/g;Lt1/j;Lt1/b;Ls1/c;)V

    iput-object v3, v1, Ls1/g;->f:Lt1/k;

    new-instance v3, Lt1/o;

    iget-object v12, v1, Ls1/g;->b:Lt1/j;

    iget-object v13, v1, Ls1/g;->c:Lt1/g;

    iget-object v14, v1, Ls1/g;->e:Lb2/c;

    iget-object v15, v1, Ls1/g;->h:Lt1/b;

    iget-object v4, v1, Ls1/g;->k:Ls1/c;

    move-object v11, v3

    move-object/from16 v16, v4

    invoke-direct/range {v11 .. v16}, Lt1/o;-><init>(Lt1/j;Lt1/g;Lb2/c;Lt1/b;Ls1/c;)V

    iput-object v3, v1, Ls1/g;->g:Lt1/o;

    invoke-static {}, La2/d;->d()La2/d;

    move-result-object v3

    invoke-virtual {v3, v1}, La2/d;->j(La2/d$c;)V

    iget-object v3, v1, Ls1/g;->a:Ls1/d;

    invoke-virtual {v3}, Ls1/d;->s()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, v1, Ls1/g;->d:Lw1/a;

    invoke-virtual {v3}, Lw1/a;->f()V

    :cond_2
    invoke-static/range {p1 .. p1}, Lu1/b;->c(Landroid/content/Context;)V

    invoke-static/range {p2 .. p2}, Lu1/b;->b(Ljava/lang/String;)Lu1/b;

    move-result-object v3

    invoke-virtual {v3, v2}, Lu1/b;->g(Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p2}, Ls1/g;->k(Landroid/content/Context;Ljava/lang/String;)V

    iget-object v3, v1, Ls1/g;->a:Ls1/d;

    invoke-virtual {v1, v0, v2, v3}, Ls1/g;->l(Landroid/content/Context;Ljava/lang/String;Ls1/d;)V

    invoke-static {}, Lz1/a;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "httpdns service is inited "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lz1/a;->b(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :cond_3
    :goto_0
    return-void
.end method

.method static synthetic j(Ls1/g;)Lt1/j;
    .locals 0

    iget-object p0, p0, Ls1/g;->b:Lt1/j;

    return-object p0
.end method

.method static synthetic m(Ls1/g;)Z
    .locals 0

    iget-boolean p0, p0, Ls1/g;->j:Z

    return p0
.end method


# virtual methods
.method public a(Lo1/f;)V
    .locals 0

    invoke-static {p1}, Lz1/a;->h(Lo1/f;)V

    return-void
.end method

.method public b(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lb2/a;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Ls1/g;->a:Ls1/d;

    invoke-virtual {v0}, Ls1/d;->C()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Ls1/g;->e:Lb2/c;

    invoke-virtual {v0, p1}, Lb2/c;->d(Ljava/util/List;)V

    return-void
.end method

.method public c(Z)V
    .locals 1

    iget-object v0, p0, Ls1/g;->a:Ls1/d;

    invoke-virtual {v0}, Ls1/d;->C()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Ls1/g;->f:Lt1/k;

    invoke-virtual {v0, p1}, Lt1/k;->e(Z)V

    return-void
.end method

.method public d(Z)V
    .locals 1

    iget-object v0, p0, Ls1/g;->a:Ls1/d;

    invoke-virtual {v0}, Ls1/d;->C()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Ls1/g;->a:Ls1/d;

    invoke-virtual {v0, p1}, Ls1/d;->E(Z)V

    if-eqz p1, :cond_1

    iget-object p1, p0, Ls1/g;->a:Ls1/d;

    invoke-virtual {p1}, Ls1/d;->s()Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Ls1/g;->d:Lw1/a;

    invoke-virtual {p1}, Lw1/a;->f()V

    :cond_1
    return-void
.end method

.method public e(ZZ)V
    .locals 1

    iget-object v0, p0, Ls1/g;->a:Ls1/d;

    invoke-virtual {v0}, Ls1/d;->C()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Ls1/g;->b:Lt1/j;

    invoke-virtual {v0, p1, p2}, Lt1/j;->l(ZZ)V

    return-void
.end method

.method public f(Ljava/lang/String;)[Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Ls1/g;->a:Ls1/d;

    invoke-virtual {v0}, Ls1/d;->C()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const-string p1, "service is disabled"

    invoke-static {p1}, Lz1/a;->g(Ljava/lang/String;)V

    new-array p1, v1, [Ljava/lang/String;

    return-object p1

    :cond_0
    invoke-static {p1}, Ly1/a;->k(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "host is invalid. "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lz1/a;->g(Ljava/lang/String;)V

    new-array p1, v1, [Ljava/lang/String;

    return-object p1

    :cond_1
    invoke-static {p1}, Ly1/a;->n(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "host is ip. "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lz1/a;->g(Ljava/lang/String;)V

    new-array p1, v1, [Ljava/lang/String;

    return-object p1

    :cond_2
    iget-object v0, p0, Ls1/g;->f:Lt1/k;

    sget-object v1, Lo1/g;->a:Lo1/g;

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2, v2}, Lt1/k;->b(Ljava/lang/String;Lo1/g;Ljava/util/Map;Ljava/lang/String;)Lo1/b;

    move-result-object p1

    invoke-virtual {p1}, Lo1/b;->a()[Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public g(Ljava/lang/String;)V
    .locals 1

    iget-object p1, p0, Ls1/g;->a:Ls1/d;

    invoke-virtual {p1}, Ls1/d;->C()Z

    move-result p1

    if-nez p1, :cond_0

    return-void

    :cond_0
    :try_start_0
    iget-object p1, p0, Ls1/g;->a:Ls1/d;

    invoke-virtual {p1}, Ls1/d;->a()Ljava/util/concurrent/ExecutorService;

    move-result-object p1

    new-instance v0, Ls1/g$b;

    invoke-direct {v0, p0}, Ls1/g$b;-><init>(Ls1/g;)V

    invoke-interface {p1, v0}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method public h(Z)V
    .locals 3

    iget-object v0, p0, Ls1/g;->a:Ls1/d;

    invoke-virtual {v0}, Ls1/d;->C()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "------> log control "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v2, " account "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Ls1/g;->a:Ls1/d;

    invoke-virtual {v2}, Ls1/d;->x()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-static {p1}, Lz1/a;->f(Z)V

    return-void
.end method

.method public i(Z)V
    .locals 1

    iget-object v0, p0, Ls1/g;->a:Ls1/d;

    invoke-virtual {v0}, Ls1/d;->C()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_1

    iget-object p1, p0, Ls1/g;->b:Lt1/j;

    invoke-virtual {p1}, Lt1/j;->j()V

    :cond_1
    iget-object p1, p0, Ls1/g;->c:Lt1/g;

    invoke-virtual {p1}, Lt1/g;->c()V

    return-void
.end method

.method protected k(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    invoke-static {}, Lo1/e;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    :try_start_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "accountId"

    invoke-virtual {v0, v1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance p2, Ld2/b;

    invoke-direct {p2}, Ld2/b;-><init>()V

    const-string v1, "httpdns"

    invoke-virtual {p2, v1}, Ld2/b;->e(Ljava/lang/String;)Ld2/b;

    const-string v1, "2.1.0"

    invoke-virtual {p2, v1}, Ld2/b;->f(Ljava/lang/String;)Ld2/b;

    invoke-virtual {p2, v0}, Ld2/b;->d(Ljava/util/Map;)Ld2/b;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    instance-of v0, v0, Landroid/app/Application;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    check-cast p1, Landroid/app/Application;

    invoke-static {p1, p2}, Ld2/a;->g(Landroid/app/Application;Ld2/b;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1, p2}, Ld2/a;->h(Landroid/content/Context;Ld2/b;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_1
    :goto_0
    return-void
.end method

.method protected l(Landroid/content/Context;Ljava/lang/String;Ls1/d;)V
    .locals 0

    invoke-static {p1, p2, p3}, Lp1/a;->a(Landroid/content/Context;Ljava/lang/String;Ls1/d;)V

    return-void
.end method

.method protected n(Landroid/content/Context;Ls1/d;)V
    .locals 6

    new-instance v5, Ls1/g$a;

    invoke-direct {v5, p0, p2}, Ls1/g$a;-><init>(Ls1/g;Ls1/d;)V

    const-string v1, "httpdns"

    const-string v2, "2.1.0"

    const/4 v3, 0x2

    const/4 v4, 0x7

    move-object v0, p1

    invoke-static/range {v0 .. v5}, Lk1/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IILk1/b;)V

    return-void
.end method

.method public o(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Ls1/g;->a:Ls1/d;

    invoke-virtual {v0}, Ls1/d;->C()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_1

    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const-string v0, "set empty secret!?"

    invoke-static {v0}, Lz1/a;->c(Ljava/lang/String;)V

    :cond_2
    iget-object v0, p0, Ls1/g;->i:Ls1/i;

    invoke-virtual {v0, p1}, Ls1/i;->b(Ljava/lang/String;)V

    return-void
.end method
