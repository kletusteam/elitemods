.class public Ls/l;
.super Ls/p;


# static fields
.field private static k:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x2

    new-array v0, v0, [I

    sput-object v0, Ls/l;->k:[I

    return-void
.end method

.method public constructor <init>(Lr/e;)V
    .locals 1

    invoke-direct {p0, p1}, Ls/p;-><init>(Lr/e;)V

    iget-object p1, p0, Ls/p;->h:Ls/f;

    sget-object v0, Ls/f$a;->d:Ls/f$a;

    iput-object v0, p1, Ls/f;->e:Ls/f$a;

    iget-object p1, p0, Ls/p;->i:Ls/f;

    sget-object v0, Ls/f$a;->e:Ls/f$a;

    iput-object v0, p1, Ls/f;->e:Ls/f$a;

    const/4 p1, 0x0

    iput p1, p0, Ls/p;->f:I

    return-void
.end method

.method private q([IIIIIFI)V
    .locals 2

    sub-int/2addr p3, p2

    sub-int/2addr p5, p4

    const/4 p2, -0x1

    const/4 p4, 0x0

    const/high16 v0, 0x3f000000    # 0.5f

    const/4 v1, 0x1

    if-eq p7, p2, :cond_2

    if-eqz p7, :cond_1

    if-eq p7, v1, :cond_0

    goto :goto_0

    :cond_0
    int-to-float p2, p3

    mul-float/2addr p2, p6

    add-float/2addr p2, v0

    float-to-int p2, p2

    aput p3, p1, p4

    aput p2, p1, v1

    goto :goto_0

    :cond_1
    int-to-float p2, p5

    mul-float/2addr p2, p6

    add-float/2addr p2, v0

    float-to-int p2, p2

    aput p2, p1, p4

    aput p5, p1, v1

    goto :goto_0

    :cond_2
    int-to-float p2, p5

    mul-float/2addr p2, p6

    add-float/2addr p2, v0

    float-to-int p2, p2

    int-to-float p7, p3

    div-float/2addr p7, p6

    add-float/2addr p7, v0

    float-to-int p6, p7

    if-gt p2, p3, :cond_3

    aput p2, p1, p4

    aput p5, p1, v1

    goto :goto_0

    :cond_3
    if-gt p6, p5, :cond_4

    aput p3, p1, p4

    aput p6, p1, v1

    :cond_4
    :goto_0
    return-void
.end method


# virtual methods
.method public a(Ls/d;)V
    .locals 16

    move-object/from16 v8, p0

    sget-object v0, Ls/l$a;->a:[I

    iget-object v1, v8, Ls/p;->j:Ls/p$b;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x2

    const/4 v2, 0x3

    const/4 v9, 0x1

    const/4 v10, 0x0

    if-eq v0, v9, :cond_2

    if-eq v0, v1, :cond_1

    if-eq v0, v2, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, v8, Ls/p;->b:Lr/e;

    iget-object v1, v0, Lr/e;->O:Lr/d;

    iget-object v0, v0, Lr/e;->Q:Lr/d;

    move-object/from16 v3, p1

    invoke-virtual {v8, v3, v1, v0, v10}, Ls/p;->n(Ls/d;Lr/d;Lr/d;I)V

    return-void

    :cond_1
    move-object/from16 v3, p1

    invoke-virtual/range {p0 .. p1}, Ls/p;->o(Ls/d;)V

    goto :goto_0

    :cond_2
    move-object/from16 v3, p1

    invoke-virtual/range {p0 .. p1}, Ls/p;->p(Ls/d;)V

    :goto_0
    iget-object v0, v8, Ls/p;->e:Ls/g;

    iget-boolean v0, v0, Ls/f;->j:Z

    const/high16 v11, 0x3f000000    # 0.5f

    if-nez v0, :cond_21

    iget-object v0, v8, Ls/p;->d:Lr/e$b;

    sget-object v3, Lr/e$b;->c:Lr/e$b;

    if-ne v0, v3, :cond_21

    iget-object v0, v8, Ls/p;->b:Lr/e;

    iget v3, v0, Lr/e;->w:I

    if-eq v3, v1, :cond_20

    if-eq v3, v2, :cond_3

    goto/16 :goto_11

    :cond_3
    iget v1, v0, Lr/e;->x:I

    const/4 v3, -0x1

    if-eqz v1, :cond_7

    if-ne v1, v2, :cond_4

    goto :goto_3

    :cond_4
    invoke-virtual {v0}, Lr/e;->u()I

    move-result v0

    if-eq v0, v3, :cond_6

    if-eqz v0, :cond_5

    if-eq v0, v9, :cond_6

    move v0, v10

    goto :goto_2

    :cond_5
    iget-object v0, v8, Ls/p;->b:Lr/e;

    iget-object v1, v0, Lr/e;->f:Ls/n;

    iget-object v1, v1, Ls/p;->e:Ls/g;

    iget v1, v1, Ls/f;->g:I

    int-to-float v1, v1

    invoke-virtual {v0}, Lr/e;->t()F

    move-result v0

    div-float/2addr v1, v0

    goto :goto_1

    :cond_6
    iget-object v0, v8, Ls/p;->b:Lr/e;

    iget-object v1, v0, Lr/e;->f:Ls/n;

    iget-object v1, v1, Ls/p;->e:Ls/g;

    iget v1, v1, Ls/f;->g:I

    int-to-float v1, v1

    invoke-virtual {v0}, Lr/e;->t()F

    move-result v0

    mul-float/2addr v1, v0

    :goto_1
    add-float/2addr v1, v11

    float-to-int v0, v1

    :goto_2
    iget-object v1, v8, Ls/p;->e:Ls/g;

    invoke-virtual {v1, v0}, Ls/g;->d(I)V

    goto/16 :goto_11

    :cond_7
    :goto_3
    iget-object v1, v0, Lr/e;->f:Ls/n;

    iget-object v12, v1, Ls/p;->h:Ls/f;

    iget-object v13, v1, Ls/p;->i:Ls/f;

    iget-object v1, v0, Lr/e;->O:Lr/d;

    iget-object v1, v1, Lr/d;->f:Lr/d;

    if-eqz v1, :cond_8

    move v1, v9

    goto :goto_4

    :cond_8
    move v1, v10

    :goto_4
    iget-object v2, v0, Lr/e;->P:Lr/d;

    iget-object v2, v2, Lr/d;->f:Lr/d;

    if-eqz v2, :cond_9

    move v2, v9

    goto :goto_5

    :cond_9
    move v2, v10

    :goto_5
    iget-object v4, v0, Lr/e;->Q:Lr/d;

    iget-object v4, v4, Lr/d;->f:Lr/d;

    if-eqz v4, :cond_a

    move v4, v9

    goto :goto_6

    :cond_a
    move v4, v10

    :goto_6
    iget-object v5, v0, Lr/e;->R:Lr/d;

    iget-object v5, v5, Lr/d;->f:Lr/d;

    if-eqz v5, :cond_b

    move v5, v9

    goto :goto_7

    :cond_b
    move v5, v10

    :goto_7
    invoke-virtual {v0}, Lr/e;->u()I

    move-result v14

    if-eqz v1, :cond_14

    if-eqz v2, :cond_14

    if-eqz v4, :cond_14

    if-eqz v5, :cond_14

    iget-object v0, v8, Ls/p;->b:Lr/e;

    invoke-virtual {v0}, Lr/e;->t()F

    move-result v15

    iget-boolean v0, v12, Ls/f;->j:Z

    if-eqz v0, :cond_e

    iget-boolean v0, v13, Ls/f;->j:Z

    if-eqz v0, :cond_e

    iget-object v0, v8, Ls/p;->h:Ls/f;

    iget-boolean v1, v0, Ls/f;->c:Z

    if-eqz v1, :cond_d

    iget-object v1, v8, Ls/p;->i:Ls/f;

    iget-boolean v1, v1, Ls/f;->c:Z

    if-nez v1, :cond_c

    goto :goto_8

    :cond_c
    iget-object v0, v0, Ls/f;->l:Ljava/util/List;

    invoke-interface {v0, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ls/f;

    iget v0, v0, Ls/f;->g:I

    iget-object v1, v8, Ls/p;->h:Ls/f;

    iget v1, v1, Ls/f;->f:I

    add-int v2, v0, v1

    iget-object v0, v8, Ls/p;->i:Ls/f;

    iget-object v0, v0, Ls/f;->l:Ljava/util/List;

    invoke-interface {v0, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ls/f;

    iget v0, v0, Ls/f;->g:I

    iget-object v1, v8, Ls/p;->i:Ls/f;

    iget v1, v1, Ls/f;->f:I

    sub-int v3, v0, v1

    iget v0, v12, Ls/f;->g:I

    iget v1, v12, Ls/f;->f:I

    add-int v4, v0, v1

    iget v0, v13, Ls/f;->g:I

    iget v1, v13, Ls/f;->f:I

    sub-int v5, v0, v1

    sget-object v1, Ls/l;->k:[I

    move-object/from16 v0, p0

    move v6, v15

    move v7, v14

    invoke-direct/range {v0 .. v7}, Ls/l;->q([IIIIIFI)V

    iget-object v0, v8, Ls/p;->e:Ls/g;

    sget-object v1, Ls/l;->k:[I

    aget v1, v1, v10

    invoke-virtual {v0, v1}, Ls/g;->d(I)V

    iget-object v0, v8, Ls/p;->b:Lr/e;

    iget-object v0, v0, Lr/e;->f:Ls/n;

    iget-object v0, v0, Ls/p;->e:Ls/g;

    sget-object v1, Ls/l;->k:[I

    aget v1, v1, v9

    invoke-virtual {v0, v1}, Ls/g;->d(I)V

    :cond_d
    :goto_8
    return-void

    :cond_e
    iget-object v0, v8, Ls/p;->h:Ls/f;

    iget-boolean v1, v0, Ls/f;->j:Z

    if-eqz v1, :cond_11

    iget-object v1, v8, Ls/p;->i:Ls/f;

    iget-boolean v2, v1, Ls/f;->j:Z

    if-eqz v2, :cond_11

    iget-boolean v2, v12, Ls/f;->c:Z

    if-eqz v2, :cond_10

    iget-boolean v2, v13, Ls/f;->c:Z

    if-nez v2, :cond_f

    goto :goto_9

    :cond_f
    iget v2, v0, Ls/f;->g:I

    iget v0, v0, Ls/f;->f:I

    add-int/2addr v2, v0

    iget v0, v1, Ls/f;->g:I

    iget v1, v1, Ls/f;->f:I

    sub-int v3, v0, v1

    iget-object v0, v12, Ls/f;->l:Ljava/util/List;

    invoke-interface {v0, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ls/f;

    iget v0, v0, Ls/f;->g:I

    iget v1, v12, Ls/f;->f:I

    add-int v4, v0, v1

    iget-object v0, v13, Ls/f;->l:Ljava/util/List;

    invoke-interface {v0, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ls/f;

    iget v0, v0, Ls/f;->g:I

    iget v1, v13, Ls/f;->f:I

    sub-int v5, v0, v1

    sget-object v1, Ls/l;->k:[I

    move-object/from16 v0, p0

    move v6, v15

    move v7, v14

    invoke-direct/range {v0 .. v7}, Ls/l;->q([IIIIIFI)V

    iget-object v0, v8, Ls/p;->e:Ls/g;

    sget-object v1, Ls/l;->k:[I

    aget v1, v1, v10

    invoke-virtual {v0, v1}, Ls/g;->d(I)V

    iget-object v0, v8, Ls/p;->b:Lr/e;

    iget-object v0, v0, Lr/e;->f:Ls/n;

    iget-object v0, v0, Ls/p;->e:Ls/g;

    sget-object v1, Ls/l;->k:[I

    aget v1, v1, v9

    invoke-virtual {v0, v1}, Ls/g;->d(I)V

    goto :goto_a

    :cond_10
    :goto_9
    return-void

    :cond_11
    :goto_a
    iget-object v0, v8, Ls/p;->h:Ls/f;

    iget-boolean v1, v0, Ls/f;->c:Z

    if-eqz v1, :cond_13

    iget-object v1, v8, Ls/p;->i:Ls/f;

    iget-boolean v1, v1, Ls/f;->c:Z

    if-eqz v1, :cond_13

    iget-boolean v1, v12, Ls/f;->c:Z

    if-eqz v1, :cond_13

    iget-boolean v1, v13, Ls/f;->c:Z

    if-nez v1, :cond_12

    goto :goto_b

    :cond_12
    iget-object v0, v0, Ls/f;->l:Ljava/util/List;

    invoke-interface {v0, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ls/f;

    iget v0, v0, Ls/f;->g:I

    iget-object v1, v8, Ls/p;->h:Ls/f;

    iget v1, v1, Ls/f;->f:I

    add-int v2, v0, v1

    iget-object v0, v8, Ls/p;->i:Ls/f;

    iget-object v0, v0, Ls/f;->l:Ljava/util/List;

    invoke-interface {v0, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ls/f;

    iget v0, v0, Ls/f;->g:I

    iget-object v1, v8, Ls/p;->i:Ls/f;

    iget v1, v1, Ls/f;->f:I

    sub-int v3, v0, v1

    iget-object v0, v12, Ls/f;->l:Ljava/util/List;

    invoke-interface {v0, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ls/f;

    iget v0, v0, Ls/f;->g:I

    iget v1, v12, Ls/f;->f:I

    add-int v4, v0, v1

    iget-object v0, v13, Ls/f;->l:Ljava/util/List;

    invoke-interface {v0, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ls/f;

    iget v0, v0, Ls/f;->g:I

    iget v1, v13, Ls/f;->f:I

    sub-int v5, v0, v1

    sget-object v1, Ls/l;->k:[I

    move-object/from16 v0, p0

    move v6, v15

    move v7, v14

    invoke-direct/range {v0 .. v7}, Ls/l;->q([IIIIIFI)V

    iget-object v0, v8, Ls/p;->e:Ls/g;

    sget-object v1, Ls/l;->k:[I

    aget v1, v1, v10

    invoke-virtual {v0, v1}, Ls/g;->d(I)V

    iget-object v0, v8, Ls/p;->b:Lr/e;

    iget-object v0, v0, Lr/e;->f:Ls/n;

    iget-object v0, v0, Ls/p;->e:Ls/g;

    sget-object v1, Ls/l;->k:[I

    aget v1, v1, v9

    goto/16 :goto_f

    :cond_13
    :goto_b
    return-void

    :cond_14
    if-eqz v1, :cond_1a

    if-eqz v4, :cond_1a

    iget-object v0, v8, Ls/p;->h:Ls/f;

    iget-boolean v0, v0, Ls/f;->c:Z

    if-eqz v0, :cond_19

    iget-object v0, v8, Ls/p;->i:Ls/f;

    iget-boolean v0, v0, Ls/f;->c:Z

    if-nez v0, :cond_15

    goto :goto_d

    :cond_15
    iget-object v0, v8, Ls/p;->b:Lr/e;

    invoke-virtual {v0}, Lr/e;->t()F

    move-result v0

    iget-object v1, v8, Ls/p;->h:Ls/f;

    iget-object v1, v1, Ls/f;->l:Ljava/util/List;

    invoke-interface {v1, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ls/f;

    iget v1, v1, Ls/f;->g:I

    iget-object v2, v8, Ls/p;->h:Ls/f;

    iget v2, v2, Ls/f;->f:I

    add-int/2addr v1, v2

    iget-object v2, v8, Ls/p;->i:Ls/f;

    iget-object v2, v2, Ls/f;->l:Ljava/util/List;

    invoke-interface {v2, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ls/f;

    iget v2, v2, Ls/f;->g:I

    iget-object v4, v8, Ls/p;->i:Ls/f;

    iget v4, v4, Ls/f;->f:I

    sub-int/2addr v2, v4

    if-eq v14, v3, :cond_17

    if-eqz v14, :cond_17

    if-eq v14, v9, :cond_16

    goto/16 :goto_11

    :cond_16
    sub-int/2addr v2, v1

    invoke-virtual {v8, v2, v10}, Ls/p;->g(II)I

    move-result v1

    int-to-float v2, v1

    div-float/2addr v2, v0

    add-float/2addr v2, v11

    float-to-int v2, v2

    invoke-virtual {v8, v2, v9}, Ls/p;->g(II)I

    move-result v3

    if-eq v2, v3, :cond_18

    int-to-float v1, v3

    mul-float/2addr v1, v0

    goto :goto_c

    :cond_17
    sub-int/2addr v2, v1

    invoke-virtual {v8, v2, v10}, Ls/p;->g(II)I

    move-result v1

    int-to-float v2, v1

    mul-float/2addr v2, v0

    add-float/2addr v2, v11

    float-to-int v2, v2

    invoke-virtual {v8, v2, v9}, Ls/p;->g(II)I

    move-result v3

    if-eq v2, v3, :cond_18

    int-to-float v1, v3

    div-float/2addr v1, v0

    :goto_c
    add-float/2addr v1, v11

    float-to-int v1, v1

    :cond_18
    iget-object v0, v8, Ls/p;->e:Ls/g;

    invoke-virtual {v0, v1}, Ls/g;->d(I)V

    iget-object v0, v8, Ls/p;->b:Lr/e;

    iget-object v0, v0, Lr/e;->f:Ls/n;

    iget-object v0, v0, Ls/p;->e:Ls/g;

    invoke-virtual {v0, v3}, Ls/g;->d(I)V

    goto/16 :goto_11

    :cond_19
    :goto_d
    return-void

    :cond_1a
    if-eqz v2, :cond_21

    if-eqz v5, :cond_21

    iget-boolean v0, v12, Ls/f;->c:Z

    if-eqz v0, :cond_1f

    iget-boolean v0, v13, Ls/f;->c:Z

    if-nez v0, :cond_1b

    goto :goto_10

    :cond_1b
    iget-object v0, v8, Ls/p;->b:Lr/e;

    invoke-virtual {v0}, Lr/e;->t()F

    move-result v0

    iget-object v1, v12, Ls/f;->l:Ljava/util/List;

    invoke-interface {v1, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ls/f;

    iget v1, v1, Ls/f;->g:I

    iget v2, v12, Ls/f;->f:I

    add-int/2addr v1, v2

    iget-object v2, v13, Ls/f;->l:Ljava/util/List;

    invoke-interface {v2, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ls/f;

    iget v2, v2, Ls/f;->g:I

    iget v4, v13, Ls/f;->f:I

    sub-int/2addr v2, v4

    if-eq v14, v3, :cond_1d

    if-eqz v14, :cond_1c

    if-eq v14, v9, :cond_1d

    goto :goto_11

    :cond_1c
    sub-int/2addr v2, v1

    invoke-virtual {v8, v2, v9}, Ls/p;->g(II)I

    move-result v1

    int-to-float v2, v1

    mul-float/2addr v2, v0

    add-float/2addr v2, v11

    float-to-int v2, v2

    invoke-virtual {v8, v2, v10}, Ls/p;->g(II)I

    move-result v3

    if-eq v2, v3, :cond_1e

    int-to-float v1, v3

    div-float/2addr v1, v0

    goto :goto_e

    :cond_1d
    sub-int/2addr v2, v1

    invoke-virtual {v8, v2, v9}, Ls/p;->g(II)I

    move-result v1

    int-to-float v2, v1

    div-float/2addr v2, v0

    add-float/2addr v2, v11

    float-to-int v2, v2

    invoke-virtual {v8, v2, v10}, Ls/p;->g(II)I

    move-result v3

    if-eq v2, v3, :cond_1e

    int-to-float v1, v3

    mul-float/2addr v1, v0

    :goto_e
    add-float/2addr v1, v11

    float-to-int v1, v1

    :cond_1e
    iget-object v0, v8, Ls/p;->e:Ls/g;

    invoke-virtual {v0, v3}, Ls/g;->d(I)V

    iget-object v0, v8, Ls/p;->b:Lr/e;

    iget-object v0, v0, Lr/e;->f:Ls/n;

    iget-object v0, v0, Ls/p;->e:Ls/g;

    :goto_f
    invoke-virtual {v0, v1}, Ls/g;->d(I)V

    goto :goto_11

    :cond_1f
    :goto_10
    return-void

    :cond_20
    invoke-virtual {v0}, Lr/e;->I()Lr/e;

    move-result-object v0

    if-eqz v0, :cond_21

    iget-object v0, v0, Lr/e;->e:Ls/l;

    iget-object v0, v0, Ls/p;->e:Ls/g;

    iget-boolean v1, v0, Ls/f;->j:Z

    if-eqz v1, :cond_21

    iget-object v1, v8, Ls/p;->b:Lr/e;

    iget v1, v1, Lr/e;->B:F

    iget v0, v0, Ls/f;->g:I

    int-to-float v0, v0

    mul-float/2addr v0, v1

    add-float/2addr v0, v11

    float-to-int v0, v0

    goto/16 :goto_2

    :cond_21
    :goto_11
    iget-object v0, v8, Ls/p;->h:Ls/f;

    iget-boolean v1, v0, Ls/f;->c:Z

    if-eqz v1, :cond_29

    iget-object v1, v8, Ls/p;->i:Ls/f;

    iget-boolean v2, v1, Ls/f;->c:Z

    if-nez v2, :cond_22

    goto/16 :goto_12

    :cond_22
    iget-boolean v0, v0, Ls/f;->j:Z

    if-eqz v0, :cond_23

    iget-boolean v0, v1, Ls/f;->j:Z

    if-eqz v0, :cond_23

    iget-object v0, v8, Ls/p;->e:Ls/g;

    iget-boolean v0, v0, Ls/f;->j:Z

    if-eqz v0, :cond_23

    return-void

    :cond_23
    iget-object v0, v8, Ls/p;->e:Ls/g;

    iget-boolean v0, v0, Ls/f;->j:Z

    if-nez v0, :cond_24

    iget-object v0, v8, Ls/p;->d:Lr/e$b;

    sget-object v1, Lr/e$b;->c:Lr/e$b;

    if-ne v0, v1, :cond_24

    iget-object v0, v8, Ls/p;->b:Lr/e;

    iget v1, v0, Lr/e;->w:I

    if-nez v1, :cond_24

    invoke-virtual {v0}, Lr/e;->g0()Z

    move-result v0

    if-nez v0, :cond_24

    iget-object v0, v8, Ls/p;->h:Ls/f;

    iget-object v0, v0, Ls/f;->l:Ljava/util/List;

    invoke-interface {v0, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ls/f;

    iget-object v1, v8, Ls/p;->i:Ls/f;

    iget-object v1, v1, Ls/f;->l:Ljava/util/List;

    invoke-interface {v1, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ls/f;

    iget v0, v0, Ls/f;->g:I

    iget-object v2, v8, Ls/p;->h:Ls/f;

    iget v3, v2, Ls/f;->f:I

    add-int/2addr v0, v3

    iget v1, v1, Ls/f;->g:I

    iget-object v3, v8, Ls/p;->i:Ls/f;

    iget v3, v3, Ls/f;->f:I

    add-int/2addr v1, v3

    sub-int v3, v1, v0

    invoke-virtual {v2, v0}, Ls/f;->d(I)V

    iget-object v0, v8, Ls/p;->i:Ls/f;

    invoke-virtual {v0, v1}, Ls/f;->d(I)V

    iget-object v0, v8, Ls/p;->e:Ls/g;

    invoke-virtual {v0, v3}, Ls/g;->d(I)V

    return-void

    :cond_24
    iget-object v0, v8, Ls/p;->e:Ls/g;

    iget-boolean v0, v0, Ls/f;->j:Z

    if-nez v0, :cond_26

    iget-object v0, v8, Ls/p;->d:Lr/e$b;

    sget-object v1, Lr/e$b;->c:Lr/e$b;

    if-ne v0, v1, :cond_26

    iget v0, v8, Ls/p;->a:I

    if-ne v0, v9, :cond_26

    iget-object v0, v8, Ls/p;->h:Ls/f;

    iget-object v0, v0, Ls/f;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_26

    iget-object v0, v8, Ls/p;->i:Ls/f;

    iget-object v0, v0, Ls/f;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_26

    iget-object v0, v8, Ls/p;->h:Ls/f;

    iget-object v0, v0, Ls/f;->l:Ljava/util/List;

    invoke-interface {v0, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ls/f;

    iget-object v1, v8, Ls/p;->i:Ls/f;

    iget-object v1, v1, Ls/f;->l:Ljava/util/List;

    invoke-interface {v1, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ls/f;

    iget v0, v0, Ls/f;->g:I

    iget-object v2, v8, Ls/p;->h:Ls/f;

    iget v2, v2, Ls/f;->f:I

    add-int/2addr v0, v2

    iget v1, v1, Ls/f;->g:I

    iget-object v2, v8, Ls/p;->i:Ls/f;

    iget v2, v2, Ls/f;->f:I

    add-int/2addr v1, v2

    sub-int/2addr v1, v0

    iget-object v0, v8, Ls/p;->e:Ls/g;

    iget v0, v0, Ls/g;->m:I

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    iget-object v1, v8, Ls/p;->b:Lr/e;

    iget v2, v1, Lr/e;->A:I

    iget v1, v1, Lr/e;->z:I

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    if-lez v2, :cond_25

    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    :cond_25
    iget-object v1, v8, Ls/p;->e:Ls/g;

    invoke-virtual {v1, v0}, Ls/g;->d(I)V

    :cond_26
    iget-object v0, v8, Ls/p;->e:Ls/g;

    iget-boolean v0, v0, Ls/f;->j:Z

    if-nez v0, :cond_27

    return-void

    :cond_27
    iget-object v0, v8, Ls/p;->h:Ls/f;

    iget-object v0, v0, Ls/f;->l:Ljava/util/List;

    invoke-interface {v0, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ls/f;

    iget-object v1, v8, Ls/p;->i:Ls/f;

    iget-object v1, v1, Ls/f;->l:Ljava/util/List;

    invoke-interface {v1, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ls/f;

    iget v2, v0, Ls/f;->g:I

    iget-object v3, v8, Ls/p;->h:Ls/f;

    iget v3, v3, Ls/f;->f:I

    add-int/2addr v2, v3

    iget v3, v1, Ls/f;->g:I

    iget-object v4, v8, Ls/p;->i:Ls/f;

    iget v4, v4, Ls/f;->f:I

    add-int/2addr v3, v4

    iget-object v4, v8, Ls/p;->b:Lr/e;

    invoke-virtual {v4}, Lr/e;->w()F

    move-result v4

    if-ne v0, v1, :cond_28

    iget v2, v0, Ls/f;->g:I

    iget v3, v1, Ls/f;->g:I

    move v4, v11

    :cond_28
    sub-int/2addr v3, v2

    iget-object v0, v8, Ls/p;->e:Ls/g;

    iget v0, v0, Ls/f;->g:I

    sub-int/2addr v3, v0

    iget-object v0, v8, Ls/p;->h:Ls/f;

    int-to-float v1, v2

    add-float/2addr v1, v11

    int-to-float v2, v3

    mul-float/2addr v2, v4

    add-float/2addr v1, v2

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Ls/f;->d(I)V

    iget-object v0, v8, Ls/p;->i:Ls/f;

    iget-object v1, v8, Ls/p;->h:Ls/f;

    iget v1, v1, Ls/f;->g:I

    iget-object v2, v8, Ls/p;->e:Ls/g;

    iget v2, v2, Ls/f;->g:I

    add-int/2addr v1, v2

    invoke-virtual {v0, v1}, Ls/f;->d(I)V

    :cond_29
    :goto_12
    return-void
.end method

.method d()V
    .locals 6

    goto/32 :goto_36

    nop

    :goto_0
    iget-object v0, v0, Lr/e;->f:Ls/n;

    goto/32 :goto_ae

    nop

    :goto_1
    iget-object v0, v0, Ls/p;->h:Ls/f;

    goto/32 :goto_126

    nop

    :goto_2
    iget-object v0, v0, Ls/f;->k:Ljava/util/List;

    goto/32 :goto_156

    nop

    :goto_3
    iget-object v1, p0, Ls/p;->b:Lr/e;

    goto/32 :goto_e3

    nop

    :goto_4
    iget-object v1, p0, Ls/p;->e:Ls/g;

    goto/32 :goto_104

    nop

    :goto_5
    if-nez v0, :cond_0

    goto/32 :goto_193

    :cond_0
    goto/32 :goto_6

    nop

    :goto_6
    iget-object v0, p0, Ls/p;->b:Lr/e;

    goto/32 :goto_c7

    nop

    :goto_7
    iget-object v0, v0, Lr/d;->f:Lr/d;

    goto/32 :goto_8a

    nop

    :goto_8
    if-nez v0, :cond_1

    goto/32 :goto_44

    :cond_1
    goto/32 :goto_162

    nop

    :goto_9
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/32 :goto_127

    nop

    :goto_a
    goto/16 :goto_193

    :goto_b
    goto/32 :goto_63

    nop

    :goto_c
    iput v1, v0, Ls/f;->f:I

    goto/32 :goto_9b

    nop

    :goto_d
    aget-object v4, v1, v3

    goto/32 :goto_ec

    nop

    :goto_e
    iget-object v0, v0, Lr/e;->W:[Lr/d;

    goto/32 :goto_181

    nop

    :goto_f
    aget-object v0, v0, v2

    goto/32 :goto_12d

    nop

    :goto_10
    iget-object v1, p0, Ls/p;->h:Ls/f;

    goto/32 :goto_1d

    nop

    :goto_11
    goto/16 :goto_193

    :goto_12
    goto/32 :goto_132

    nop

    :goto_13
    iget-object v1, p0, Ls/p;->b:Lr/e;

    goto/32 :goto_8d

    nop

    :goto_14
    iget-object v3, v3, Ls/p;->h:Ls/f;

    goto/32 :goto_19b

    nop

    :goto_15
    iget-object v0, v0, Lr/e;->f:Ls/n;

    goto/32 :goto_178

    nop

    :goto_16
    iget-object v1, p0, Ls/p;->b:Lr/e;

    goto/32 :goto_10d

    nop

    :goto_17
    iget-object v2, v2, Lr/e;->Q:Lr/d;

    goto/32 :goto_b1

    nop

    :goto_18
    iget-object v2, p0, Ls/p;->e:Ls/g;

    goto/32 :goto_17b

    nop

    :goto_19
    iget-object v1, p0, Ls/p;->b:Lr/e;

    goto/32 :goto_45

    nop

    :goto_1a
    iget-object v0, p0, Ls/p;->d:Lr/e$b;

    goto/32 :goto_19d

    nop

    :goto_1b
    if-eq v4, v5, :cond_2

    goto/32 :goto_a4

    :cond_2
    goto/32 :goto_7c

    nop

    :goto_1c
    invoke-virtual {p0, v0}, Ls/p;->h(Lr/d;)Ls/f;

    move-result-object v0

    goto/32 :goto_1a1

    nop

    :goto_1d
    iget-object v2, p0, Ls/p;->e:Ls/g;

    goto/32 :goto_192

    nop

    :goto_1e
    iget-object v0, v0, Ls/f;->k:Ljava/util/List;

    goto/32 :goto_d2

    nop

    :goto_1f
    iget-object v0, p0, Ls/p;->b:Lr/e;

    goto/32 :goto_4b

    nop

    :goto_20
    aget-object v1, v1, v3

    goto/32 :goto_195

    nop

    :goto_21
    iget-object v0, v0, Ls/p;->h:Ls/f;

    goto/32 :goto_f3

    nop

    :goto_22
    invoke-virtual {v0}, Lr/e;->I()Lr/e;

    move-result-object v0

    goto/32 :goto_154

    nop

    :goto_23
    if-nez v0, :cond_3

    goto/32 :goto_193

    :cond_3
    goto/32 :goto_68

    nop

    :goto_24
    iget-object v2, v2, Lr/d;->f:Lr/d;

    goto/32 :goto_103

    nop

    :goto_25
    iget-object v0, p0, Ls/p;->b:Lr/e;

    goto/32 :goto_7e

    nop

    :goto_26
    invoke-virtual {p0, v0, v1, v2}, Ls/p;->b(Ls/f;Ls/f;I)V

    goto/32 :goto_101

    nop

    :goto_27
    aget-object v4, v0, v2

    goto/32 :goto_d0

    nop

    :goto_28
    iget-object v2, p0, Ls/p;->b:Lr/e;

    goto/32 :goto_17

    nop

    :goto_29
    iget-object v0, v0, Ls/f;->l:Ljava/util/List;

    goto/32 :goto_6e

    nop

    :goto_2a
    if-ne v2, v3, :cond_4

    goto/32 :goto_7a

    :cond_4
    goto/32 :goto_105

    nop

    :goto_2b
    iget-object v1, p0, Ls/p;->h:Ls/f;

    goto/32 :goto_106

    nop

    :goto_2c
    iget-object v1, p0, Ls/p;->i:Ls/f;

    goto/32 :goto_76

    nop

    :goto_2d
    iget-object v4, v4, Lr/e;->O:Lr/d;

    goto/32 :goto_11f

    nop

    :goto_2e
    goto/16 :goto_50

    :goto_2f
    goto/32 :goto_1a3

    nop

    :goto_30
    const/4 v2, 0x0

    goto/32 :goto_198

    nop

    :goto_31
    iget-object v0, v0, Ls/p;->i:Ls/f;

    goto/32 :goto_8b

    nop

    :goto_32
    aget-object v4, v1, v2

    goto/32 :goto_142

    nop

    :goto_33
    invoke-virtual {v0}, Lr/e;->y()Lr/e$b;

    move-result-object v2

    goto/32 :goto_ba

    nop

    :goto_34
    iget-object v2, v2, Lr/e;->W:[Lr/d;

    goto/32 :goto_e4

    nop

    :goto_35
    aget-object v2, v2, v3

    goto/32 :goto_1ac

    nop

    :goto_36
    iget-object v0, p0, Ls/p;->b:Lr/e;

    goto/32 :goto_90

    nop

    :goto_37
    iget-object v2, p0, Ls/p;->b:Lr/e;

    goto/32 :goto_c2

    nop

    :goto_38
    goto/16 :goto_97

    :goto_39
    goto/32 :goto_12c

    nop

    :goto_3a
    iget-object v1, p0, Ls/p;->e:Ls/g;

    goto/32 :goto_197

    nop

    :goto_3b
    iget-object v0, p0, Ls/p;->b:Lr/e;

    goto/32 :goto_62

    nop

    :goto_3c
    iput-object v0, p0, Ls/p;->d:Lr/e$b;

    goto/32 :goto_144

    nop

    :goto_3d
    iget-object v1, v1, Lr/e;->f:Ls/n;

    goto/32 :goto_7d

    nop

    :goto_3e
    invoke-virtual {v0, v1}, Ls/g;->d(I)V

    goto/32 :goto_1c3

    nop

    :goto_3f
    iget-object v3, v3, Lr/e;->O:Lr/d;

    goto/32 :goto_a7

    nop

    :goto_40
    iget-object v3, p0, Ls/p;->b:Lr/e;

    goto/32 :goto_8c

    nop

    :goto_41
    iget-object v3, p0, Ls/p;->b:Lr/e;

    goto/32 :goto_ca

    nop

    :goto_42
    iget-object v2, p0, Ls/p;->b:Lr/e;

    goto/32 :goto_13d

    nop

    :goto_43
    goto/16 :goto_97

    :goto_44
    goto/32 :goto_180

    nop

    :goto_45
    iget-object v1, v1, Lr/e;->f:Ls/n;

    goto/32 :goto_80

    nop

    :goto_46
    neg-int v3, v3

    goto/32 :goto_182

    nop

    :goto_47
    iget-object v1, v1, Lr/e;->W:[Lr/d;

    goto/32 :goto_20

    nop

    :goto_48
    iget-object v1, p0, Ls/p;->e:Ls/g;

    goto/32 :goto_fa

    nop

    :goto_49
    invoke-virtual {v1}, Lr/e;->g0()Z

    move-result v0

    goto/32 :goto_a0

    nop

    :goto_4a
    iget-object v0, p0, Ls/p;->b:Lr/e;

    goto/32 :goto_91

    nop

    :goto_4b
    invoke-virtual {v0}, Lr/e;->I()Lr/e;

    move-result-object v0

    goto/32 :goto_5c

    nop

    :goto_4c
    iget-object v1, p0, Ls/p;->b:Lr/e;

    goto/32 :goto_f9

    nop

    :goto_4d
    iget-object v1, p0, Ls/p;->e:Ls/g;

    goto/32 :goto_5d

    nop

    :goto_4e
    sub-int/2addr v1, v2

    goto/32 :goto_ef

    nop

    :goto_4f
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_50
    goto/32 :goto_4a

    nop

    :goto_51
    iget-object v0, v0, Ls/f;->l:Ljava/util/List;

    :goto_52
    goto/32 :goto_c5

    nop

    :goto_53
    goto/16 :goto_17c

    :goto_54
    goto/32 :goto_12e

    nop

    :goto_55
    iget-object v0, v1, Ls/f;->k:Ljava/util/List;

    goto/32 :goto_16f

    nop

    :goto_56
    invoke-virtual {v0}, Lr/e;->I()Lr/e;

    move-result-object v0

    goto/32 :goto_5

    nop

    :goto_57
    iget-object v1, p0, Ls/p;->b:Lr/e;

    goto/32 :goto_174

    nop

    :goto_58
    iget-object v4, p0, Ls/p;->i:Ls/f;

    goto/32 :goto_b5

    nop

    :goto_59
    iget-object v2, v2, Ls/p;->h:Ls/f;

    goto/32 :goto_188

    nop

    :goto_5a
    if-nez v0, :cond_5

    goto/32 :goto_1c0

    :cond_5
    goto/32 :goto_ab

    nop

    :goto_5b
    invoke-virtual {v1}, Lr/d;->e()I

    move-result v1

    goto/32 :goto_84

    nop

    :goto_5c
    if-nez v0, :cond_6

    goto/32 :goto_75

    :cond_6
    goto/32 :goto_19c

    nop

    :goto_5d
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/32 :goto_3b

    nop

    :goto_5e
    const/4 v5, 0x3

    goto/32 :goto_e1

    nop

    :goto_5f
    invoke-virtual {v1}, Lr/e;->I()Lr/e;

    move-result-object v0

    goto/32 :goto_23

    nop

    :goto_60
    aget-object v1, v1, v3

    :goto_61
    goto/32 :goto_145

    nop

    :goto_62
    iget-object v0, v0, Lr/e;->f:Ls/n;

    goto/32 :goto_d7

    nop

    :goto_63
    instance-of v1, v0, Lr/h;

    goto/32 :goto_113

    nop

    :goto_64
    if-eq v1, v4, :cond_7

    goto/32 :goto_50

    :cond_7
    goto/32 :goto_13

    nop

    :goto_65
    sget-object v3, Lr/e$b;->a:Lr/e$b;

    goto/32 :goto_e6

    nop

    :goto_66
    iget-object v2, p0, Ls/p;->b:Lr/e;

    goto/32 :goto_119

    nop

    :goto_67
    aget-object v0, v0, v2

    goto/32 :goto_1ba

    nop

    :goto_68
    iget-object v0, p0, Ls/p;->b:Lr/e;

    goto/32 :goto_f4

    nop

    :goto_69
    if-nez v1, :cond_8

    goto/32 :goto_1a9

    :cond_8
    goto/32 :goto_4

    nop

    :goto_6a
    if-eqz v0, :cond_9

    goto/32 :goto_2f

    :cond_9
    goto/32 :goto_2e

    nop

    :goto_6b
    aget-object v1, v1, v3

    goto/32 :goto_71

    nop

    :goto_6c
    if-nez v2, :cond_a

    goto/32 :goto_b

    :cond_a
    goto/32 :goto_117

    nop

    :goto_6d
    sget-object v4, Lr/e$b;->c:Lr/e$b;

    goto/32 :goto_64

    nop

    :goto_6e
    iget-object v1, p0, Ls/p;->e:Ls/g;

    goto/32 :goto_ac

    nop

    :goto_6f
    iget-object v2, v0, Lr/e;->e:Ls/l;

    goto/32 :goto_59

    nop

    :goto_70
    iget-object v1, v1, Ls/f;->l:Ljava/util/List;

    goto/32 :goto_143

    nop

    :goto_71
    invoke-virtual {p0, v1}, Ls/p;->h(Lr/d;)Ls/f;

    move-result-object v1

    goto/32 :goto_ff

    nop

    :goto_72
    iget-object v2, v2, Lr/e;->O:Lr/d;

    goto/32 :goto_b7

    nop

    :goto_73
    iput-object p0, v4, Ls/f;->a:Ls/d;

    goto/32 :goto_1b4

    nop

    :goto_74
    return-void

    :goto_75
    goto/32 :goto_c1

    nop

    :goto_76
    iget-object v2, p0, Ls/p;->b:Lr/e;

    goto/32 :goto_7b

    nop

    :goto_77
    sget-object v3, Lr/e$b;->a:Lr/e$b;

    goto/32 :goto_2a

    nop

    :goto_78
    aget-object v1, v1, v2

    goto/32 :goto_5b

    nop

    :goto_79
    if-eq v2, v1, :cond_b

    goto/32 :goto_150

    :cond_b
    :goto_7a
    goto/32 :goto_82

    nop

    :goto_7b
    iget-object v2, v2, Lr/e;->W:[Lr/d;

    goto/32 :goto_1a5

    nop

    :goto_7c
    iget-object v4, p0, Ls/p;->h:Ls/f;

    goto/32 :goto_114

    nop

    :goto_7d
    iget-object v1, v1, Ls/p;->e:Ls/g;

    goto/32 :goto_b3

    nop

    :goto_7e
    invoke-virtual {v0}, Lr/e;->g0()Z

    move-result v0

    goto/32 :goto_8

    nop

    :goto_7f
    iput-boolean v3, v0, Ls/f;->b:Z

    goto/32 :goto_11

    nop

    :goto_80
    iget-object v1, v1, Ls/p;->e:Ls/g;

    goto/32 :goto_a6

    nop

    :goto_81
    iget-object v0, p0, Ls/p;->e:Ls/g;

    goto/32 :goto_4c

    nop

    :goto_82
    invoke-virtual {v0}, Lr/e;->U()I

    move-result v1

    goto/32 :goto_1bc

    nop

    :goto_83
    iget-object v4, v4, Ls/p;->i:Ls/f;

    goto/32 :goto_73

    nop

    :goto_84
    iput v1, v0, Ls/f;->f:I

    goto/32 :goto_e5

    nop

    :goto_85
    if-nez v0, :cond_c

    goto/32 :goto_1ab

    :cond_c
    goto/32 :goto_1b6

    nop

    :goto_86
    sget-object v1, Lr/e$b;->d:Lr/e$b;

    goto/32 :goto_115

    nop

    :goto_87
    iget-object v4, v4, Lr/d;->f:Lr/d;

    goto/32 :goto_16d

    nop

    :goto_88
    if-nez v1, :cond_d

    goto/32 :goto_102

    :cond_d
    goto/32 :goto_14b

    nop

    :goto_89
    iget-object v1, v1, Lr/e;->f:Ls/n;

    goto/32 :goto_13e

    nop

    :goto_8a
    if-eqz v0, :cond_e

    goto/32 :goto_193

    :cond_e
    goto/32 :goto_99

    nop

    :goto_8b
    iget-object v0, v0, Ls/f;->k:Ljava/util/List;

    goto/32 :goto_1aa

    nop

    :goto_8c
    iget-object v3, v3, Lr/e;->Q:Lr/d;

    goto/32 :goto_185

    nop

    :goto_8d
    iget v4, v1, Lr/e;->w:I

    goto/32 :goto_1bd

    nop

    :goto_8e
    iget-object v0, p0, Ls/p;->e:Ls/g;

    goto/32 :goto_151

    nop

    :goto_8f
    iget-object v2, v2, Lr/d;->f:Lr/d;

    goto/32 :goto_6c

    nop

    :goto_90
    iget-boolean v1, v0, Lr/e;->a:Z

    goto/32 :goto_69

    nop

    :goto_91
    iget-object v1, v0, Lr/e;->W:[Lr/d;

    goto/32 :goto_fd

    nop

    :goto_92
    goto/16 :goto_11a

    :goto_93
    goto/32 :goto_bf

    nop

    :goto_94
    invoke-virtual {v2}, Lr/d;->e()I

    move-result v2

    goto/32 :goto_92

    nop

    :goto_95
    iget-object v0, v0, Lr/e;->f:Ls/n;

    goto/32 :goto_f1

    nop

    :goto_96
    iget-object v1, p0, Ls/p;->i:Ls/f;

    :goto_97
    goto/32 :goto_4f

    nop

    :goto_98
    sub-int/2addr v1, v2

    goto/32 :goto_28

    nop

    :goto_99
    iget-object v0, p0, Ls/p;->b:Lr/e;

    goto/32 :goto_22

    nop

    :goto_9a
    iget-object v1, p0, Ls/p;->b:Lr/e;

    goto/32 :goto_89

    nop

    :goto_9b
    goto/16 :goto_193

    :goto_9c
    goto/32 :goto_9d

    nop

    :goto_9d
    iget-object v0, p0, Ls/p;->b:Lr/e;

    goto/32 :goto_cd

    nop

    :goto_9e
    invoke-virtual {v0, p0}, Ls/f;->b(Ls/d;)V

    :goto_9f
    goto/32 :goto_124

    nop

    :goto_a0
    if-nez v0, :cond_f

    goto/32 :goto_196

    :cond_f
    goto/32 :goto_15d

    nop

    :goto_a1
    iget-object v0, v0, Ls/f;->k:Ljava/util/List;

    goto/32 :goto_3a

    nop

    :goto_a2
    aget-object v0, v0, v2

    goto/32 :goto_140

    nop

    :goto_a3
    goto/16 :goto_131

    :goto_a4
    goto/32 :goto_3d

    nop

    :goto_a5
    iget-object v0, v0, Ls/f;->k:Ljava/util/List;

    goto/32 :goto_96

    nop

    :goto_a6
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/32 :goto_129

    nop

    :goto_a7
    invoke-virtual {v3}, Lr/d;->e()I

    move-result v3

    goto/32 :goto_134

    nop

    :goto_a8
    iget-object v2, p0, Ls/p;->b:Lr/e;

    goto/32 :goto_173

    nop

    :goto_a9
    iget-object v1, v1, Lr/e;->W:[Lr/d;

    goto/32 :goto_60

    nop

    :goto_aa
    neg-int v2, v2

    goto/32 :goto_15a

    nop

    :goto_ab
    iget-object v1, p0, Ls/p;->h:Ls/f;

    goto/32 :goto_1b0

    nop

    :goto_ac
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/32 :goto_190

    nop

    :goto_ad
    invoke-virtual {p0, v0}, Ls/p;->h(Lr/d;)Ls/f;

    move-result-object v0

    goto/32 :goto_19e

    nop

    :goto_ae
    iget-object v0, v0, Ls/p;->e:Ls/g;

    goto/32 :goto_29

    nop

    :goto_af
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/32 :goto_184

    nop

    :goto_b0
    invoke-virtual {v0}, Lr/e;->y()Lr/e$b;

    move-result-object v0

    goto/32 :goto_3c

    nop

    :goto_b1
    invoke-virtual {v2}, Lr/d;->e()I

    move-result v2

    goto/32 :goto_4e

    nop

    :goto_b2
    iget-object v0, v0, Lr/e;->f:Ls/n;

    goto/32 :goto_12f

    nop

    :goto_b3
    iget-object v0, v0, Ls/f;->l:Ljava/util/List;

    goto/32 :goto_de

    nop

    :goto_b4
    iget-object v0, v0, Lr/e;->W:[Lr/d;

    goto/32 :goto_67

    nop

    :goto_b5
    iput-object p0, v4, Ls/f;->a:Ls/d;

    goto/32 :goto_16e

    nop

    :goto_b6
    if-eqz v0, :cond_10

    goto/32 :goto_1c4

    :cond_10
    goto/32 :goto_c0

    nop

    :goto_b7
    invoke-virtual {v2}, Lr/d;->e()I

    move-result v2

    goto/32 :goto_98

    nop

    :goto_b8
    iget-object v0, v0, Lr/e;->f:Ls/n;

    goto/32 :goto_120

    nop

    :goto_b9
    iget-object v0, p0, Ls/p;->b:Lr/e;

    goto/32 :goto_1af

    nop

    :goto_ba
    if-eq v2, v1, :cond_11

    goto/32 :goto_75

    :cond_11
    :goto_bb
    goto/32 :goto_109

    nop

    :goto_bc
    iget-object v1, p0, Ls/p;->h:Ls/f;

    goto/32 :goto_41

    nop

    :goto_bd
    iget-object v0, p0, Ls/p;->e:Ls/g;

    goto/32 :goto_153

    nop

    :goto_be
    iget-boolean v1, v0, Ls/f;->j:Z

    goto/32 :goto_30

    nop

    :goto_bf
    aget-object v2, v1, v3

    goto/32 :goto_8f

    nop

    :goto_c0
    iget-object v0, p0, Ls/p;->b:Lr/e;

    goto/32 :goto_b0

    nop

    :goto_c1
    iget-object v0, p0, Ls/p;->e:Ls/g;

    goto/32 :goto_be

    nop

    :goto_c2
    invoke-virtual {v2}, Lr/e;->V()I

    move-result v2

    :goto_c3
    goto/32 :goto_dd

    nop

    :goto_c4
    iget-object v0, p0, Ls/p;->e:Ls/g;

    goto/32 :goto_a5

    nop

    :goto_c5
    iget-object v1, p0, Ls/p;->e:Ls/g;

    goto/32 :goto_38

    nop

    :goto_c6
    if-ne v4, v5, :cond_12

    goto/32 :goto_39

    :cond_12
    goto/32 :goto_5e

    nop

    :goto_c7
    invoke-virtual {v0}, Lr/e;->I()Lr/e;

    move-result-object v0

    goto/32 :goto_fb

    nop

    :goto_c8
    iget-object v2, p0, Ls/p;->i:Ls/f;

    goto/32 :goto_f8

    nop

    :goto_c9
    aget-object v2, v4, v2

    goto/32 :goto_1bb

    nop

    :goto_ca
    iget-object v3, v3, Lr/e;->W:[Lr/d;

    goto/32 :goto_135

    nop

    :goto_cb
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/32 :goto_138

    nop

    :goto_cc
    iget-object v0, p0, Ls/p;->b:Lr/e;

    goto/32 :goto_b8

    nop

    :goto_cd
    iget-object v0, v0, Lr/e;->W:[Lr/d;

    goto/32 :goto_a2

    nop

    :goto_ce
    iget-object v1, p0, Ls/p;->h:Ls/f;

    goto/32 :goto_189

    nop

    :goto_cf
    iget-boolean v0, v0, Ls/f;->j:Z

    goto/32 :goto_b6

    nop

    :goto_d0
    iget-object v4, v4, Lr/d;->f:Lr/d;

    goto/32 :goto_19f

    nop

    :goto_d1
    iget-object v0, p0, Ls/p;->b:Lr/e;

    goto/32 :goto_146

    nop

    :goto_d2
    iget-object v1, p0, Ls/p;->e:Ls/g;

    goto/32 :goto_9

    nop

    :goto_d3
    iget-object v0, v0, Lr/e;->e:Ls/l;

    goto/32 :goto_ea

    nop

    :goto_d4
    goto :goto_c3

    :goto_d5
    goto/32 :goto_11e

    nop

    :goto_d6
    return-void

    :goto_d7
    iget-object v1, v0, Ls/p;->e:Ls/g;

    goto/32 :goto_11b

    nop

    :goto_d8
    neg-int v1, v1

    goto/32 :goto_c

    nop

    :goto_d9
    iget-object v0, p0, Ls/p;->h:Ls/f;

    goto/32 :goto_1ad

    nop

    :goto_da
    aget-object v0, v0, v3

    goto/32 :goto_17d

    nop

    :goto_db
    iget-object v0, v0, Ls/p;->i:Ls/f;

    goto/32 :goto_40

    nop

    :goto_dc
    invoke-virtual {v1}, Lr/d;->e()I

    move-result v1

    goto/32 :goto_12a

    nop

    :goto_dd
    invoke-virtual {p0, v1, v0, v2}, Ls/p;->b(Ls/f;Ls/f;I)V

    goto/32 :goto_139

    nop

    :goto_de
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/32 :goto_55

    nop

    :goto_df
    iget-object v0, p0, Ls/p;->i:Ls/f;

    goto/32 :goto_7f

    nop

    :goto_e0
    iget-object v1, p0, Ls/p;->d:Lr/e$b;

    goto/32 :goto_6d

    nop

    :goto_e1
    if-ne v4, v5, :cond_13

    goto/32 :goto_f6

    :cond_13
    goto/32 :goto_f5

    nop

    :goto_e2
    iget-object v4, v4, Lr/d;->f:Lr/d;

    goto/32 :goto_187

    nop

    :goto_e3
    iget-object v1, v1, Lr/e;->f:Ls/n;

    goto/32 :goto_f2

    nop

    :goto_e4
    aget-object v2, v2, v3

    goto/32 :goto_16a

    nop

    :goto_e5
    iget-object v0, p0, Ls/p;->i:Ls/f;

    goto/32 :goto_1b8

    nop

    :goto_e6
    if-ne v2, v3, :cond_14

    goto/32 :goto_bb

    :cond_14
    goto/32 :goto_33

    nop

    :goto_e7
    invoke-virtual {v2}, Lr/d;->e()I

    move-result v2

    goto/32 :goto_d4

    nop

    :goto_e8
    invoke-virtual {p0, v1, v0, v2}, Ls/p;->b(Ls/f;Ls/f;I)V

    goto/32 :goto_1a0

    nop

    :goto_e9
    iput-object p0, v5, Ls/f;->a:Ls/d;

    goto/32 :goto_83

    nop

    :goto_ea
    iget-object v0, v0, Ls/p;->i:Ls/f;

    goto/32 :goto_a8

    nop

    :goto_eb
    iget-object v0, v0, Ls/f;->l:Ljava/util/List;

    goto/32 :goto_9a

    nop

    :goto_ec
    iget-object v4, v4, Lr/d;->f:Lr/d;

    goto/32 :goto_13f

    nop

    :goto_ed
    invoke-virtual {p0, v1, v0, v2}, Ls/p;->b(Ls/f;Ls/f;I)V

    :goto_ee
    goto/32 :goto_1a6

    nop

    :goto_ef
    iget-object v2, p0, Ls/p;->h:Ls/f;

    goto/32 :goto_10b

    nop

    :goto_f0
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/32 :goto_d9

    nop

    :goto_f1
    iget-object v0, v0, Ls/p;->e:Ls/g;

    goto/32 :goto_a3

    nop

    :goto_f2
    iget-object v1, v1, Ls/p;->e:Ls/g;

    goto/32 :goto_43

    nop

    :goto_f3
    iget-object v1, p0, Ls/p;->h:Ls/f;

    goto/32 :goto_37

    nop

    :goto_f4
    sget-object v1, Lr/d$b;->g:Lr/d$b;

    goto/32 :goto_17a

    nop

    :goto_f5
    goto/16 :goto_50

    :goto_f6
    goto/32 :goto_1b9

    nop

    :goto_f7
    invoke-virtual {p0, v0}, Ls/p;->h(Lr/d;)Ls/f;

    move-result-object v0

    goto/32 :goto_122

    nop

    :goto_f8
    iget-object v0, v0, Lr/e;->e:Ls/l;

    goto/32 :goto_db

    nop

    :goto_f9
    invoke-virtual {v1}, Lr/e;->U()I

    move-result v1

    goto/32 :goto_3e

    nop

    :goto_fa
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/32 :goto_8e

    nop

    :goto_fb
    iget-object v0, v0, Lr/e;->e:Ls/l;

    goto/32 :goto_191

    nop

    :goto_fc
    if-nez v4, :cond_15

    goto/32 :goto_102

    :cond_15
    goto/32 :goto_15e

    nop

    :goto_fd
    aget-object v4, v1, v2

    goto/32 :goto_e2

    nop

    :goto_fe
    iget-object v1, p0, Ls/p;->h:Ls/f;

    goto/32 :goto_66

    nop

    :goto_ff
    if-nez v0, :cond_16

    goto/32 :goto_9f

    :cond_16
    goto/32 :goto_9e

    nop

    :goto_100
    sget-object v1, Lr/e$b;->d:Lr/e$b;

    goto/32 :goto_123

    nop

    :goto_101
    goto/16 :goto_193

    :goto_102
    goto/32 :goto_e0

    nop

    :goto_103
    if-nez v2, :cond_17

    goto/32 :goto_54

    :cond_17
    goto/32 :goto_da

    nop

    :goto_104
    invoke-virtual {v0}, Lr/e;->U()I

    move-result v0

    goto/32 :goto_1a8

    nop

    :goto_105
    invoke-virtual {v0}, Lr/e;->y()Lr/e$b;

    move-result-object v2

    goto/32 :goto_79

    nop

    :goto_106
    iget-object v4, p0, Ls/p;->b:Lr/e;

    goto/32 :goto_16c

    nop

    :goto_107
    iput-boolean v3, v0, Ls/f;->b:Z

    goto/32 :goto_df

    nop

    :goto_108
    iget-object v0, p0, Ls/p;->e:Ls/g;

    goto/32 :goto_cf

    nop

    :goto_109
    iget-object v1, p0, Ls/p;->h:Ls/f;

    goto/32 :goto_6f

    nop

    :goto_10a
    iget-object v1, p0, Ls/p;->h:Ls/f;

    goto/32 :goto_18

    nop

    :goto_10b
    iget-object v3, v0, Lr/e;->e:Ls/l;

    goto/32 :goto_14

    nop

    :goto_10c
    iget-object v1, p0, Ls/p;->i:Ls/f;

    goto/32 :goto_f0

    nop

    :goto_10d
    iget-object v1, v1, Lr/e;->W:[Lr/d;

    goto/32 :goto_78

    nop

    :goto_10e
    iget-object v1, v1, Ls/f;->l:Ljava/util/List;

    goto/32 :goto_1

    nop

    :goto_10f
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/32 :goto_130

    nop

    :goto_110
    iget-object v1, p0, Ls/p;->e:Ls/g;

    goto/32 :goto_70

    nop

    :goto_111
    invoke-virtual {p0, v1, v0, v2}, Ls/p;->b(Ls/f;Ls/f;I)V

    goto/32 :goto_183

    nop

    :goto_112
    iget-object v0, v0, Ls/f;->k:Ljava/util/List;

    goto/32 :goto_10c

    nop

    :goto_113
    if-eqz v1, :cond_18

    goto/32 :goto_193

    :cond_18
    goto/32 :goto_56

    nop

    :goto_114
    iput-object p0, v4, Ls/f;->a:Ls/d;

    goto/32 :goto_58

    nop

    :goto_115
    if-eq v0, v1, :cond_19

    goto/32 :goto_75

    :cond_19
    goto/32 :goto_1f

    nop

    :goto_116
    if-eq v0, v1, :cond_1a

    goto/32 :goto_75

    :cond_1a
    goto/32 :goto_81

    nop

    :goto_117
    aget-object v0, v1, v3

    goto/32 :goto_f7

    nop

    :goto_118
    iget-object v0, v0, Ls/p;->e:Ls/g;

    goto/32 :goto_110

    nop

    :goto_119
    invoke-virtual {v2}, Lr/e;->V()I

    move-result v2

    :goto_11a
    goto/32 :goto_111

    nop

    :goto_11b
    iput-object p0, v1, Ls/f;->a:Ls/d;

    goto/32 :goto_1c1

    nop

    :goto_11c
    iget-object v1, p0, Ls/p;->b:Lr/e;

    goto/32 :goto_a9

    nop

    :goto_11d
    iget-object v1, p0, Ls/p;->e:Ls/g;

    goto/32 :goto_10f

    nop

    :goto_11e
    aget-object v2, v0, v3

    goto/32 :goto_24

    nop

    :goto_11f
    invoke-virtual {v4}, Lr/d;->e()I

    move-result v4

    goto/32 :goto_18f

    nop

    :goto_120
    iget-object v0, v0, Ls/p;->h:Ls/f;

    goto/32 :goto_a1

    nop

    :goto_121
    invoke-virtual {p0, v1, v0, v2}, Ls/p;->b(Ls/f;Ls/f;I)V

    goto/32 :goto_74

    nop

    :goto_122
    if-nez v0, :cond_1b

    goto/32 :goto_193

    :cond_1b
    goto/32 :goto_2c

    nop

    :goto_123
    if-eq v0, v1, :cond_1c

    goto/32 :goto_150

    :cond_1c
    goto/32 :goto_b9

    nop

    :goto_124
    if-nez v1, :cond_1d

    goto/32 :goto_14a

    :cond_1d
    goto/32 :goto_149

    nop

    :goto_125
    const/4 v2, -0x1

    goto/32 :goto_1a7

    nop

    :goto_126
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/32 :goto_155

    nop

    :goto_127
    iget-object v0, p0, Ls/p;->e:Ls/g;

    goto/32 :goto_14d

    nop

    :goto_128
    if-nez v4, :cond_1e

    goto/32 :goto_d5

    :cond_1e
    goto/32 :goto_f

    nop

    :goto_129
    iget-object v0, p0, Ls/p;->b:Lr/e;

    goto/32 :goto_15

    nop

    :goto_12a
    iput v1, v0, Ls/f;->f:I

    goto/32 :goto_165

    nop

    :goto_12b
    if-nez v0, :cond_1f

    goto/32 :goto_9c

    :cond_1f
    goto/32 :goto_172

    nop

    :goto_12c
    invoke-virtual {v1}, Lr/e;->I()Lr/e;

    move-result-object v0

    goto/32 :goto_6a

    nop

    :goto_12d
    invoke-virtual {p0, v0}, Ls/p;->h(Lr/d;)Ls/f;

    move-result-object v0

    goto/32 :goto_1ae

    nop

    :goto_12e
    instance-of v0, v1, Lr/h;

    goto/32 :goto_170

    nop

    :goto_12f
    iget-object v0, v0, Ls/p;->h:Ls/f;

    goto/32 :goto_166

    nop

    :goto_130
    iget-object v0, p0, Ls/p;->i:Ls/f;

    :goto_131
    goto/32 :goto_51

    nop

    :goto_132
    aget-object v4, v0, v2

    goto/32 :goto_175

    nop

    :goto_133
    iget-boolean v4, v1, Lr/e;->a:Z

    goto/32 :goto_fc

    nop

    :goto_134
    invoke-virtual {p0, v1, v2, v3}, Ls/p;->b(Ls/f;Ls/f;I)V

    goto/32 :goto_1b2

    nop

    :goto_135
    aget-object v2, v3, v2

    goto/32 :goto_e7

    nop

    :goto_136
    neg-int v2, v2

    goto/32 :goto_e8

    nop

    :goto_137
    iget-object v1, p0, Ls/p;->i:Ls/f;

    goto/32 :goto_158

    nop

    :goto_138
    iget-object v0, p0, Ls/p;->b:Lr/e;

    goto/32 :goto_1b7

    nop

    :goto_139
    iget-object v0, p0, Ls/p;->i:Ls/f;

    goto/32 :goto_10a

    nop

    :goto_13a
    iget-object v0, p0, Ls/p;->d:Lr/e$b;

    goto/32 :goto_86

    nop

    :goto_13b
    if-ne v0, v1, :cond_20

    goto/32 :goto_75

    :cond_20
    goto/32 :goto_100

    nop

    :goto_13c
    iget-object v0, p0, Ls/p;->e:Ls/g;

    goto/32 :goto_112

    nop

    :goto_13d
    iget-object v2, v2, Lr/e;->W:[Lr/d;

    goto/32 :goto_35

    nop

    :goto_13e
    iget-object v1, v1, Ls/p;->i:Ls/f;

    goto/32 :goto_af

    nop

    :goto_13f
    if-nez v4, :cond_21

    goto/32 :goto_164

    :cond_21
    goto/32 :goto_167

    nop

    :goto_140
    invoke-virtual {p0, v0}, Ls/p;->h(Lr/d;)Ls/f;

    move-result-object v0

    goto/32 :goto_14c

    nop

    :goto_141
    invoke-virtual {v0}, Lr/e;->y()Lr/e$b;

    move-result-object v2

    goto/32 :goto_77

    nop

    :goto_142
    iget-object v4, v4, Lr/d;->f:Lr/d;

    goto/32 :goto_18e

    nop

    :goto_143
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/32 :goto_17f

    nop

    :goto_144
    sget-object v1, Lr/e$b;->c:Lr/e$b;

    goto/32 :goto_13b

    nop

    :goto_145
    invoke-virtual {v1}, Lr/d;->e()I

    move-result v1

    goto/32 :goto_d8

    nop

    :goto_146
    iget-object v0, v0, Lr/e;->f:Ls/n;

    goto/32 :goto_1c2

    nop

    :goto_147
    iget-object v0, p0, Ls/p;->h:Ls/f;

    goto/32 :goto_137

    nop

    :goto_148
    if-nez v0, :cond_22

    goto/32 :goto_150

    :cond_22
    goto/32 :goto_141

    nop

    :goto_149
    invoke-virtual {v1, p0}, Ls/f;->b(Ls/d;)V

    :goto_14a
    goto/32 :goto_179

    nop

    :goto_14b
    iget-object v1, p0, Ls/p;->b:Lr/e;

    goto/32 :goto_133

    nop

    :goto_14c
    iget-object v1, p0, Ls/p;->b:Lr/e;

    goto/32 :goto_1a4

    nop

    :goto_14d
    iput-boolean v3, v0, Ls/f;->b:Z

    goto/32 :goto_2

    nop

    :goto_14e
    iget-object v1, p0, Ls/p;->i:Ls/f;

    goto/32 :goto_125

    nop

    :goto_14f
    return-void

    :goto_150
    goto/32 :goto_1a

    nop

    :goto_151
    iput-boolean v3, v0, Ls/f;->b:Z

    goto/32 :goto_15f

    nop

    :goto_152
    iget-object v0, v0, Ls/f;->k:Ljava/util/List;

    goto/32 :goto_3

    nop

    :goto_153
    invoke-virtual {v0, v1}, Ls/g;->d(I)V

    goto/32 :goto_14f

    nop

    :goto_154
    iget-object v0, v0, Lr/e;->e:Ls/l;

    goto/32 :goto_21

    nop

    :goto_155
    iget-object v0, p0, Ls/p;->e:Ls/g;

    goto/32 :goto_eb

    nop

    :goto_156
    iget-object v1, p0, Ls/p;->h:Ls/f;

    goto/32 :goto_168

    nop

    :goto_157
    neg-int v2, v2

    goto/32 :goto_53

    nop

    :goto_158
    iget-object v2, p0, Ls/p;->e:Ls/g;

    goto/32 :goto_1b5

    nop

    :goto_159
    neg-int v2, v2

    goto/32 :goto_121

    nop

    :goto_15a
    invoke-virtual {p0, v1, v0, v2}, Ls/p;->b(Ls/f;Ls/f;I)V

    goto/32 :goto_147

    nop

    :goto_15b
    aget-object v4, v0, v3

    goto/32 :goto_87

    nop

    :goto_15c
    invoke-virtual {p0, v0, v1, v2, v3}, Ls/p;->c(Ls/f;Ls/f;ILs/g;)V

    goto/32 :goto_a

    nop

    :goto_15d
    iget-object v0, p0, Ls/p;->h:Ls/f;

    goto/32 :goto_16

    nop

    :goto_15e
    iget-object v0, v1, Lr/e;->W:[Lr/d;

    goto/32 :goto_27

    nop

    :goto_15f
    iget-object v0, v0, Ls/f;->k:Ljava/util/List;

    goto/32 :goto_ce

    nop

    :goto_160
    iget-object v0, v0, Ls/f;->l:Ljava/util/List;

    goto/32 :goto_19

    nop

    :goto_161
    invoke-virtual {v1}, Lr/e;->i0()Z

    move-result v0

    goto/32 :goto_85

    nop

    :goto_162
    iget-object v0, p0, Ls/p;->b:Lr/e;

    goto/32 :goto_0

    nop

    :goto_163
    goto/16 :goto_193

    :goto_164
    goto/32 :goto_32

    nop

    :goto_165
    iget-object v0, p0, Ls/p;->i:Ls/f;

    goto/32 :goto_11c

    nop

    :goto_166
    iget-object v0, v0, Ls/f;->k:Ljava/util/List;

    goto/32 :goto_18d

    nop

    :goto_167
    invoke-virtual {v0}, Lr/e;->g0()Z

    move-result v0

    goto/32 :goto_12b

    nop

    :goto_168
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/32 :goto_13c

    nop

    :goto_169
    invoke-virtual {v2}, Lr/d;->e()I

    move-result v2

    goto/32 :goto_136

    nop

    :goto_16a
    invoke-virtual {v2}, Lr/d;->e()I

    move-result v2

    goto/32 :goto_aa

    nop

    :goto_16b
    iget-object v4, v4, Lr/e;->W:[Lr/d;

    goto/32 :goto_c9

    nop

    :goto_16c
    iget-object v4, v4, Lr/e;->W:[Lr/d;

    goto/32 :goto_18c

    nop

    :goto_16d
    if-nez v4, :cond_23

    goto/32 :goto_12

    :cond_23
    goto/32 :goto_49

    nop

    :goto_16e
    iget-object v4, v1, Lr/e;->f:Ls/n;

    goto/32 :goto_194

    nop

    :goto_16f
    iget-object v1, p0, Ls/p;->e:Ls/g;

    goto/32 :goto_1be

    nop

    :goto_170
    if-eqz v0, :cond_24

    goto/32 :goto_193

    :cond_24
    goto/32 :goto_5f

    nop

    :goto_171
    iput-object v0, p0, Ls/p;->j:Ls/p$b;

    goto/32 :goto_163

    nop

    :goto_172
    iget-object v0, p0, Ls/p;->h:Ls/f;

    goto/32 :goto_57

    nop

    :goto_173
    iget-object v2, v2, Lr/e;->Q:Lr/d;

    goto/32 :goto_176

    nop

    :goto_174
    iget-object v1, v1, Lr/e;->W:[Lr/d;

    goto/32 :goto_1b3

    nop

    :goto_175
    iget-object v4, v4, Lr/d;->f:Lr/d;

    goto/32 :goto_128

    nop

    :goto_176
    invoke-virtual {v2}, Lr/d;->e()I

    move-result v2

    goto/32 :goto_159

    nop

    :goto_177
    iget-object v1, p0, Ls/p;->i:Ls/f;

    goto/32 :goto_19a

    nop

    :goto_178
    iget-object v0, v0, Ls/p;->e:Ls/g;

    goto/32 :goto_186

    nop

    :goto_179
    sget-object v0, Ls/p$b;->d:Ls/p$b;

    goto/32 :goto_171

    nop

    :goto_17a
    invoke-virtual {v0, v1}, Lr/e;->m(Lr/d$b;)Lr/d;

    move-result-object v0

    goto/32 :goto_7

    nop

    :goto_17b
    iget v2, v2, Ls/f;->g:I

    :goto_17c
    goto/32 :goto_26

    nop

    :goto_17d
    invoke-virtual {p0, v0}, Ls/p;->h(Lr/d;)Ls/f;

    move-result-object v0

    goto/32 :goto_199

    nop

    :goto_17e
    iget-object v0, p0, Ls/p;->b:Lr/e;

    goto/32 :goto_e

    nop

    :goto_17f
    iget-object v0, v0, Ls/f;->k:Ljava/util/List;

    goto/32 :goto_48

    nop

    :goto_180
    iget-object v0, p0, Ls/p;->b:Lr/e;

    goto/32 :goto_95

    nop

    :goto_181
    aget-object v0, v0, v3

    goto/32 :goto_1c

    nop

    :goto_182
    invoke-virtual {p0, v2, v0, v3}, Ls/p;->b(Ls/f;Ls/f;I)V

    goto/32 :goto_bd

    nop

    :goto_183
    iget-object v0, p0, Ls/p;->i:Ls/f;

    goto/32 :goto_10

    nop

    :goto_184
    iget-object v0, p0, Ls/p;->b:Lr/e;

    goto/32 :goto_b2

    nop

    :goto_185
    invoke-virtual {v3}, Lr/d;->e()I

    move-result v3

    goto/32 :goto_46

    nop

    :goto_186
    iget-object v0, v0, Ls/f;->k:Ljava/util/List;

    goto/32 :goto_4d

    nop

    :goto_187
    if-nez v4, :cond_25

    goto/32 :goto_164

    :cond_25
    goto/32 :goto_d

    nop

    :goto_188
    iget-object v3, p0, Ls/p;->b:Lr/e;

    goto/32 :goto_3f

    nop

    :goto_189
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/32 :goto_c4

    nop

    :goto_18a
    aget-object v0, v1, v2

    goto/32 :goto_ad

    nop

    :goto_18b
    neg-int v2, v2

    goto/32 :goto_ed

    nop

    :goto_18c
    aget-object v2, v4, v2

    goto/32 :goto_94

    nop

    :goto_18d
    iget-object v1, p0, Ls/p;->e:Ls/g;

    goto/32 :goto_cb

    nop

    :goto_18e
    if-nez v4, :cond_26

    goto/32 :goto_93

    :cond_26
    goto/32 :goto_18a

    nop

    :goto_18f
    invoke-virtual {p0, v2, v3, v4}, Ls/p;->b(Ls/f;Ls/f;I)V

    goto/32 :goto_c8

    nop

    :goto_190
    iget-object v0, p0, Ls/p;->e:Ls/g;

    goto/32 :goto_152

    nop

    :goto_191
    iget-object v0, v0, Ls/p;->h:Ls/f;

    goto/32 :goto_fe

    nop

    :goto_192
    invoke-virtual {p0, v0, v1, v3, v2}, Ls/p;->c(Ls/f;Ls/f;ILs/g;)V

    :goto_193
    goto/32 :goto_d6

    nop

    :goto_194
    iget-object v5, v4, Ls/p;->h:Ls/f;

    goto/32 :goto_e9

    nop

    :goto_195
    goto/16 :goto_61

    :goto_196
    goto/32 :goto_1b1

    nop

    :goto_197
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/32 :goto_d1

    nop

    :goto_198
    const/4 v3, 0x1

    goto/32 :goto_88

    nop

    :goto_199
    if-nez v0, :cond_27

    goto/32 :goto_193

    :cond_27
    goto/32 :goto_177

    nop

    :goto_19a
    iget-object v2, p0, Ls/p;->b:Lr/e;

    goto/32 :goto_34

    nop

    :goto_19b
    iget-object v4, p0, Ls/p;->b:Lr/e;

    goto/32 :goto_2d

    nop

    :goto_19c
    invoke-virtual {v0}, Lr/e;->y()Lr/e$b;

    move-result-object v2

    goto/32 :goto_65

    nop

    :goto_19d
    sget-object v1, Lr/e$b;->a:Lr/e$b;

    goto/32 :goto_116

    nop

    :goto_19e
    if-nez v0, :cond_28

    goto/32 :goto_193

    :cond_28
    goto/32 :goto_2b

    nop

    :goto_19f
    if-nez v4, :cond_29

    goto/32 :goto_12

    :cond_29
    goto/32 :goto_15b

    nop

    :goto_1a0
    iget-object v0, p0, Ls/p;->h:Ls/f;

    goto/32 :goto_14e

    nop

    :goto_1a1
    if-nez v0, :cond_2a

    goto/32 :goto_ee

    :cond_2a
    goto/32 :goto_1a2

    nop

    :goto_1a2
    iget-object v1, p0, Ls/p;->i:Ls/f;

    goto/32 :goto_42

    nop

    :goto_1a3
    iget-object v0, v0, Lr/e;->f:Ls/n;

    goto/32 :goto_118

    nop

    :goto_1a4
    iget-object v1, v1, Lr/e;->W:[Lr/d;

    goto/32 :goto_6b

    nop

    :goto_1a5
    aget-object v2, v2, v3

    goto/32 :goto_169

    nop

    :goto_1a6
    iget-object v0, p0, Ls/p;->h:Ls/f;

    goto/32 :goto_107

    nop

    :goto_1a7
    iget-object v3, p0, Ls/p;->e:Ls/g;

    goto/32 :goto_15c

    nop

    :goto_1a8
    invoke-virtual {v1, v0}, Ls/g;->d(I)V

    :goto_1a9
    goto/32 :goto_108

    nop

    :goto_1aa
    goto/16 :goto_52

    :goto_1ab
    goto/32 :goto_25

    nop

    :goto_1ac
    invoke-virtual {v2}, Lr/d;->e()I

    move-result v2

    goto/32 :goto_18b

    nop

    :goto_1ad
    iget-object v0, v0, Ls/f;->l:Ljava/util/List;

    goto/32 :goto_11d

    nop

    :goto_1ae
    if-nez v0, :cond_2b

    goto/32 :goto_193

    :cond_2b
    goto/32 :goto_bc

    nop

    :goto_1af
    invoke-virtual {v0}, Lr/e;->I()Lr/e;

    move-result-object v0

    goto/32 :goto_148

    nop

    :goto_1b0
    iget-object v4, p0, Ls/p;->b:Lr/e;

    goto/32 :goto_16b

    nop

    :goto_1b1
    iget-object v0, p0, Ls/p;->b:Lr/e;

    goto/32 :goto_b4

    nop

    :goto_1b2
    iget-object v1, p0, Ls/p;->i:Ls/f;

    goto/32 :goto_d3

    nop

    :goto_1b3
    aget-object v1, v1, v2

    goto/32 :goto_dc

    nop

    :goto_1b4
    iput-object p0, v0, Ls/f;->a:Ls/d;

    goto/32 :goto_161

    nop

    :goto_1b5
    iget v2, v2, Ls/f;->g:I

    goto/32 :goto_157

    nop

    :goto_1b6
    iget-object v0, p0, Ls/p;->e:Ls/g;

    goto/32 :goto_160

    nop

    :goto_1b7
    iget-object v0, v0, Lr/e;->f:Ls/n;

    goto/32 :goto_31

    nop

    :goto_1b8
    iget-object v1, p0, Ls/p;->b:Lr/e;

    goto/32 :goto_47

    nop

    :goto_1b9
    iget v4, v1, Lr/e;->x:I

    goto/32 :goto_1b

    nop

    :goto_1ba
    invoke-virtual {p0, v0}, Ls/p;->h(Lr/d;)Ls/f;

    move-result-object v0

    goto/32 :goto_5a

    nop

    :goto_1bb
    invoke-virtual {v2}, Lr/d;->e()I

    move-result v2

    goto/32 :goto_1bf

    nop

    :goto_1bc
    iget-object v2, p0, Ls/p;->b:Lr/e;

    goto/32 :goto_72

    nop

    :goto_1bd
    const/4 v5, 0x2

    goto/32 :goto_c6

    nop

    :goto_1be
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/32 :goto_cc

    nop

    :goto_1bf
    invoke-virtual {p0, v1, v0, v2}, Ls/p;->b(Ls/f;Ls/f;I)V

    :goto_1c0
    goto/32 :goto_17e

    nop

    :goto_1c1
    iget-object v1, p0, Ls/p;->e:Ls/g;

    goto/32 :goto_10e

    nop

    :goto_1c2
    iget-object v0, v0, Ls/p;->i:Ls/f;

    goto/32 :goto_1e

    nop

    :goto_1c3
    goto/16 :goto_75

    :goto_1c4
    goto/32 :goto_13a

    nop
.end method

.method public e()V
    .locals 2

    iget-object v0, p0, Ls/p;->h:Ls/f;

    iget-boolean v1, v0, Ls/f;->j:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Ls/p;->b:Lr/e;

    iget v0, v0, Ls/f;->g:I

    invoke-virtual {v1, v0}, Lr/e;->j1(I)V

    :cond_0
    return-void
.end method

.method f()V
    .locals 1

    goto/32 :goto_8

    nop

    :goto_0
    invoke-virtual {v0}, Ls/f;->c()V

    goto/32 :goto_9

    nop

    :goto_1
    iput-boolean v0, p0, Ls/p;->g:Z

    goto/32 :goto_5

    nop

    :goto_2
    invoke-virtual {v0}, Ls/f;->c()V

    goto/32 :goto_7

    nop

    :goto_3
    iget-object v0, p0, Ls/p;->i:Ls/f;

    goto/32 :goto_2

    nop

    :goto_4
    iput-object v0, p0, Ls/p;->c:Ls/m;

    goto/32 :goto_6

    nop

    :goto_5
    return-void

    :goto_6
    iget-object v0, p0, Ls/p;->h:Ls/f;

    goto/32 :goto_a

    nop

    :goto_7
    iget-object v0, p0, Ls/p;->e:Ls/g;

    goto/32 :goto_0

    nop

    :goto_8
    const/4 v0, 0x0

    goto/32 :goto_4

    nop

    :goto_9
    const/4 v0, 0x0

    goto/32 :goto_1

    nop

    :goto_a
    invoke-virtual {v0}, Ls/f;->c()V

    goto/32 :goto_3

    nop
.end method

.method m()Z
    .locals 3

    goto/32 :goto_a

    nop

    :goto_0
    const/4 v0, 0x0

    goto/32 :goto_8

    nop

    :goto_1
    return v2

    :goto_2
    goto/32 :goto_0

    nop

    :goto_3
    if-eqz v0, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_1

    nop

    :goto_4
    return v2

    :goto_5
    const/4 v2, 0x1

    goto/32 :goto_c

    nop

    :goto_6
    iget-object v0, p0, Ls/p;->b:Lr/e;

    goto/32 :goto_b

    nop

    :goto_7
    sget-object v1, Lr/e$b;->c:Lr/e$b;

    goto/32 :goto_5

    nop

    :goto_8
    return v0

    :goto_9
    goto/32 :goto_4

    nop

    :goto_a
    iget-object v0, p0, Ls/p;->d:Lr/e$b;

    goto/32 :goto_7

    nop

    :goto_b
    iget v0, v0, Lr/e;->w:I

    goto/32 :goto_3

    nop

    :goto_c
    if-eq v0, v1, :cond_1

    goto/32 :goto_9

    :cond_1
    goto/32 :goto_6

    nop
.end method

.method r()V
    .locals 2

    goto/32 :goto_8

    nop

    :goto_0
    iget-object v1, p0, Ls/p;->h:Ls/f;

    goto/32 :goto_b

    nop

    :goto_1
    iget-object v1, p0, Ls/p;->h:Ls/f;

    goto/32 :goto_5

    nop

    :goto_2
    iget-object v1, p0, Ls/p;->e:Ls/g;

    goto/32 :goto_9

    nop

    :goto_3
    invoke-virtual {v1}, Ls/f;->c()V

    goto/32 :goto_c

    nop

    :goto_4
    return-void

    :goto_5
    iput-boolean v0, v1, Ls/f;->j:Z

    goto/32 :goto_7

    nop

    :goto_6
    iput-boolean v0, v1, Ls/f;->j:Z

    goto/32 :goto_2

    nop

    :goto_7
    iget-object v1, p0, Ls/p;->i:Ls/f;

    goto/32 :goto_3

    nop

    :goto_8
    const/4 v0, 0x0

    goto/32 :goto_a

    nop

    :goto_9
    iput-boolean v0, v1, Ls/f;->j:Z

    goto/32 :goto_4

    nop

    :goto_a
    iput-boolean v0, p0, Ls/p;->g:Z

    goto/32 :goto_0

    nop

    :goto_b
    invoke-virtual {v1}, Ls/f;->c()V

    goto/32 :goto_1

    nop

    :goto_c
    iget-object v1, p0, Ls/p;->i:Ls/f;

    goto/32 :goto_6

    nop
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "HorizontalRun "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Ls/p;->b:Lr/e;

    invoke-virtual {v1}, Lr/e;->r()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
