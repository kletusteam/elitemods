.class Ls/j;
.super Ls/p;


# direct methods
.method public constructor <init>(Lr/e;)V
    .locals 1

    invoke-direct {p0, p1}, Ls/p;-><init>(Lr/e;)V

    iget-object v0, p1, Lr/e;->e:Ls/l;

    invoke-virtual {v0}, Ls/l;->f()V

    iget-object v0, p1, Lr/e;->f:Ls/n;

    invoke-virtual {v0}, Ls/n;->f()V

    check-cast p1, Lr/g;

    invoke-virtual {p1}, Lr/g;->p1()I

    move-result p1

    iput p1, p0, Ls/p;->f:I

    return-void
.end method

.method private q(Ls/f;)V
    .locals 1

    iget-object v0, p0, Ls/p;->h:Ls/f;

    iget-object v0, v0, Ls/f;->k:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object p1, p1, Ls/f;->l:Ljava/util/List;

    iget-object v0, p0, Ls/p;->h:Ls/f;

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method


# virtual methods
.method public a(Ls/d;)V
    .locals 2

    iget-object p1, p0, Ls/p;->h:Ls/f;

    iget-boolean v0, p1, Ls/f;->c:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-boolean v0, p1, Ls/f;->j:Z

    if-eqz v0, :cond_1

    return-void

    :cond_1
    iget-object p1, p1, Ls/f;->l:Ljava/util/List;

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ls/f;

    iget-object v0, p0, Ls/p;->b:Lr/e;

    check-cast v0, Lr/g;

    const/high16 v1, 0x3f000000    # 0.5f

    iget p1, p1, Ls/f;->g:I

    int-to-float p1, p1

    invoke-virtual {v0}, Lr/g;->s1()F

    move-result v0

    mul-float/2addr p1, v0

    add-float/2addr p1, v1

    float-to-int p1, p1

    iget-object v0, p0, Ls/p;->h:Ls/f;

    invoke-virtual {v0, p1}, Ls/f;->d(I)V

    return-void
.end method

.method d()V
    .locals 5

    goto/32 :goto_6b

    nop

    :goto_0
    invoke-direct {p0, v0}, Ls/j;->q(Ls/f;)V

    goto/32 :goto_6

    nop

    :goto_1
    iget-object v1, v1, Lr/e;->a0:Lr/e;

    goto/32 :goto_c

    nop

    :goto_2
    iget-object v0, v0, Ls/p;->i:Ls/f;

    goto/32 :goto_80

    nop

    :goto_3
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/32 :goto_9

    nop

    :goto_4
    iget-object v0, v0, Lr/e;->a0:Lr/e;

    goto/32 :goto_62

    nop

    :goto_5
    iget-object v0, v0, Lr/e;->f:Ls/n;

    goto/32 :goto_7e

    nop

    :goto_6
    iget-object v0, p0, Ls/p;->b:Lr/e;

    goto/32 :goto_25

    nop

    :goto_7
    iget-object v0, p0, Ls/p;->b:Lr/e;

    goto/32 :goto_60

    nop

    :goto_8
    iget-object v1, v1, Lr/e;->e:Ls/l;

    goto/32 :goto_10

    nop

    :goto_9
    iget-object v0, p0, Ls/p;->b:Lr/e;

    goto/32 :goto_52

    nop

    :goto_a
    iget-object v0, p0, Ls/p;->b:Lr/e;

    goto/32 :goto_53

    nop

    :goto_b
    iget-object v0, v0, Ls/p;->i:Ls/f;

    goto/32 :goto_4f

    nop

    :goto_c
    iget-object v1, v1, Lr/e;->e:Ls/l;

    goto/32 :goto_50

    nop

    :goto_d
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/32 :goto_37

    nop

    :goto_e
    iget-object v1, p0, Ls/p;->h:Ls/f;

    goto/32 :goto_4a

    nop

    :goto_f
    iget-object v0, v0, Ls/f;->l:Ljava/util/List;

    goto/32 :goto_7a

    nop

    :goto_10
    iget-object v1, v1, Ls/p;->i:Ls/f;

    goto/32 :goto_5e

    nop

    :goto_11
    return-void

    :goto_12
    iget-object v2, v2, Ls/p;->h:Ls/f;

    goto/32 :goto_3

    nop

    :goto_13
    iget-object v0, p0, Ls/p;->h:Ls/f;

    goto/32 :goto_48

    nop

    :goto_14
    iget-object v2, v2, Lr/e;->e:Ls/l;

    goto/32 :goto_12

    nop

    :goto_15
    iget-object v1, v1, Lr/e;->a0:Lr/e;

    goto/32 :goto_7c

    nop

    :goto_16
    if-ne v2, v4, :cond_0

    goto/32 :goto_20

    :cond_0
    goto/32 :goto_f

    nop

    :goto_17
    iget-object v0, v0, Lr/e;->e:Ls/l;

    goto/32 :goto_4c

    nop

    :goto_18
    iget-object v0, v0, Ls/p;->h:Ls/f;

    goto/32 :goto_71

    nop

    :goto_19
    iget-object v0, v0, Ls/f;->k:Ljava/util/List;

    goto/32 :goto_64

    nop

    :goto_1a
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/32 :goto_a

    nop

    :goto_1b
    iget-object v1, p0, Ls/p;->b:Lr/e;

    goto/32 :goto_1e

    nop

    :goto_1c
    if-ne v2, v4, :cond_1

    goto/32 :goto_83

    :cond_1
    goto/32 :goto_2f

    nop

    :goto_1d
    iget-object v0, v0, Lr/e;->e:Ls/l;

    goto/32 :goto_21

    nop

    :goto_1e
    iget-object v1, v1, Lr/e;->a0:Lr/e;

    goto/32 :goto_65

    nop

    :goto_1f
    goto/16 :goto_38

    :goto_20
    goto/32 :goto_54

    nop

    :goto_21
    iget-object v0, v0, Ls/p;->h:Ls/f;

    goto/32 :goto_74

    nop

    :goto_22
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/32 :goto_45

    nop

    :goto_23
    iput-boolean v3, v0, Ls/f;->b:Z

    goto/32 :goto_7b

    nop

    :goto_24
    iget-object v0, v0, Ls/p;->h:Ls/f;

    goto/32 :goto_0

    nop

    :goto_25
    iget-object v0, v0, Lr/e;->f:Ls/n;

    :goto_26
    goto/32 :goto_2

    nop

    :goto_27
    if-eq v0, v3, :cond_2

    goto/32 :goto_4d

    :cond_2
    goto/32 :goto_5a

    nop

    :goto_28
    iget-object v2, v2, Lr/e;->a0:Lr/e;

    goto/32 :goto_6f

    nop

    :goto_29
    iget-object v1, p0, Ls/p;->h:Ls/f;

    goto/32 :goto_72

    nop

    :goto_2a
    iget-object v0, v0, Lr/e;->a0:Lr/e;

    goto/32 :goto_70

    nop

    :goto_2b
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/32 :goto_30

    nop

    :goto_2c
    goto/16 :goto_73

    :goto_2d
    goto/32 :goto_1c

    nop

    :goto_2e
    iget-object v0, v0, Lr/e;->e:Ls/l;

    goto/32 :goto_40

    nop

    :goto_2f
    iget-object v0, v0, Ls/f;->l:Ljava/util/List;

    goto/32 :goto_5f

    nop

    :goto_30
    iget-object v0, p0, Ls/p;->h:Ls/f;

    goto/32 :goto_77

    nop

    :goto_31
    iput v1, v0, Ls/f;->f:I

    goto/32 :goto_2c

    nop

    :goto_32
    iget-object v1, v1, Lr/e;->a0:Lr/e;

    goto/32 :goto_8

    nop

    :goto_33
    iget-object v2, p0, Ls/p;->h:Ls/f;

    goto/32 :goto_d

    nop

    :goto_34
    iget-object v1, v1, Ls/p;->i:Ls/f;

    goto/32 :goto_3e

    nop

    :goto_35
    iget-object v0, v0, Ls/f;->l:Ljava/util/List;

    goto/32 :goto_1b

    nop

    :goto_36
    iget-object v0, p0, Ls/p;->b:Lr/e;

    goto/32 :goto_2a

    nop

    :goto_37
    iget-object v0, p0, Ls/p;->h:Ls/f;

    :goto_38
    goto/32 :goto_7d

    nop

    :goto_39
    iget-object v0, v0, Lr/e;->f:Ls/n;

    goto/32 :goto_24

    nop

    :goto_3a
    invoke-virtual {v0}, Lr/g;->q1()I

    move-result v1

    goto/32 :goto_51

    nop

    :goto_3b
    iget-object v0, p0, Ls/p;->b:Lr/e;

    goto/32 :goto_4

    nop

    :goto_3c
    goto :goto_4b

    :goto_3d
    goto/32 :goto_16

    nop

    :goto_3e
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/32 :goto_3b

    nop

    :goto_3f
    iget-object v2, p0, Ls/p;->b:Lr/e;

    goto/32 :goto_28

    nop

    :goto_40
    iget-object v0, v0, Ls/p;->i:Ls/f;

    goto/32 :goto_63

    nop

    :goto_41
    invoke-virtual {v0}, Lr/g;->s1()F

    goto/32 :goto_76

    nop

    :goto_42
    iget-object v0, v0, Ls/f;->k:Ljava/util/List;

    goto/32 :goto_33

    nop

    :goto_43
    iget-object v0, v0, Ls/p;->h:Ls/f;

    goto/32 :goto_42

    nop

    :goto_44
    iget-object v0, p0, Ls/p;->h:Ls/f;

    goto/32 :goto_61

    nop

    :goto_45
    iget-object v0, p0, Ls/p;->b:Lr/e;

    goto/32 :goto_84

    nop

    :goto_46
    iget-object v2, p0, Ls/p;->b:Lr/e;

    goto/32 :goto_6c

    nop

    :goto_47
    iget-object v0, v0, Ls/f;->l:Ljava/util/List;

    goto/32 :goto_3f

    nop

    :goto_48
    neg-int v1, v2

    goto/32 :goto_1f

    nop

    :goto_49
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/32 :goto_13

    nop

    :goto_4a
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_4b
    goto/32 :goto_7f

    nop

    :goto_4c
    goto/16 :goto_26

    :goto_4d
    goto/32 :goto_44

    nop

    :goto_4e
    iget-object v1, p0, Ls/p;->b:Lr/e;

    goto/32 :goto_32

    nop

    :goto_4f
    iget-object v0, v0, Ls/f;->k:Ljava/util/List;

    goto/32 :goto_e

    nop

    :goto_50
    iget-object v1, v1, Ls/p;->i:Ls/f;

    goto/32 :goto_1a

    nop

    :goto_51
    invoke-virtual {v0}, Lr/g;->r1()I

    move-result v2

    goto/32 :goto_41

    nop

    :goto_52
    iget-object v0, v0, Lr/e;->a0:Lr/e;

    goto/32 :goto_1d

    nop

    :goto_53
    iget-object v0, v0, Lr/e;->a0:Lr/e;

    goto/32 :goto_2e

    nop

    :goto_54
    iput-boolean v3, v0, Ls/f;->b:Z

    goto/32 :goto_35

    nop

    :goto_55
    iget-object v0, p0, Ls/p;->h:Ls/f;

    :goto_56
    goto/32 :goto_31

    nop

    :goto_57
    iget-object v0, v0, Ls/p;->i:Ls/f;

    goto/32 :goto_6e

    nop

    :goto_58
    iget-object v2, v2, Ls/p;->h:Ls/f;

    goto/32 :goto_66

    nop

    :goto_59
    if-ne v1, v4, :cond_3

    goto/32 :goto_2d

    :cond_3
    goto/32 :goto_79

    nop

    :goto_5a
    iget-object v0, p0, Ls/p;->h:Ls/f;

    goto/32 :goto_59

    nop

    :goto_5b
    iget-object v0, p0, Ls/p;->b:Lr/e;

    goto/32 :goto_17

    nop

    :goto_5c
    iget-object v0, v0, Lr/e;->a0:Lr/e;

    goto/32 :goto_75

    nop

    :goto_5d
    iget-object v1, p0, Ls/p;->h:Ls/f;

    goto/32 :goto_2b

    nop

    :goto_5e
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/32 :goto_36

    nop

    :goto_5f
    iget-object v1, p0, Ls/p;->b:Lr/e;

    goto/32 :goto_1

    nop

    :goto_60
    iget-object v0, v0, Lr/e;->e:Ls/l;

    goto/32 :goto_18

    nop

    :goto_61
    if-ne v1, v4, :cond_4

    goto/32 :goto_3d

    :cond_4
    goto/32 :goto_47

    nop

    :goto_62
    iget-object v0, v0, Lr/e;->f:Ls/n;

    goto/32 :goto_b

    nop

    :goto_63
    iget-object v0, v0, Ls/f;->k:Ljava/util/List;

    goto/32 :goto_5d

    nop

    :goto_64
    iget-object v1, p0, Ls/p;->h:Ls/f;

    goto/32 :goto_49

    nop

    :goto_65
    iget-object v1, v1, Lr/e;->f:Ls/n;

    goto/32 :goto_34

    nop

    :goto_66
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/32 :goto_6d

    nop

    :goto_67
    iget-object v2, p0, Ls/p;->h:Ls/f;

    goto/32 :goto_78

    nop

    :goto_68
    iget-object v1, v1, Ls/p;->i:Ls/f;

    goto/32 :goto_22

    nop

    :goto_69
    const/4 v4, -0x1

    goto/32 :goto_27

    nop

    :goto_6a
    const/4 v3, 0x1

    goto/32 :goto_69

    nop

    :goto_6b
    iget-object v0, p0, Ls/p;->b:Lr/e;

    goto/32 :goto_81

    nop

    :goto_6c
    iget-object v2, v2, Lr/e;->a0:Lr/e;

    goto/32 :goto_14

    nop

    :goto_6d
    iget-object v0, p0, Ls/p;->b:Lr/e;

    goto/32 :goto_5c

    nop

    :goto_6e
    iget-object v0, v0, Ls/f;->k:Ljava/util/List;

    goto/32 :goto_29

    nop

    :goto_6f
    iget-object v2, v2, Lr/e;->f:Ls/n;

    goto/32 :goto_58

    nop

    :goto_70
    iget-object v0, v0, Lr/e;->e:Ls/l;

    goto/32 :goto_57

    nop

    :goto_71
    invoke-direct {p0, v0}, Ls/j;->q(Ls/f;)V

    goto/32 :goto_5b

    nop

    :goto_72
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_73
    goto/32 :goto_7

    nop

    :goto_74
    iget-object v0, v0, Ls/f;->k:Ljava/util/List;

    goto/32 :goto_67

    nop

    :goto_75
    iget-object v0, v0, Lr/e;->f:Ls/n;

    goto/32 :goto_43

    nop

    :goto_76
    invoke-virtual {v0}, Lr/g;->p1()I

    move-result v0

    goto/32 :goto_6a

    nop

    :goto_77
    neg-int v1, v2

    goto/32 :goto_82

    nop

    :goto_78
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/32 :goto_55

    nop

    :goto_79
    iget-object v0, v0, Ls/f;->l:Ljava/util/List;

    goto/32 :goto_46

    nop

    :goto_7a
    iget-object v1, p0, Ls/p;->b:Lr/e;

    goto/32 :goto_15

    nop

    :goto_7b
    iget-object v0, v0, Ls/f;->l:Ljava/util/List;

    goto/32 :goto_4e

    nop

    :goto_7c
    iget-object v1, v1, Lr/e;->f:Ls/n;

    goto/32 :goto_68

    nop

    :goto_7d
    iput v1, v0, Ls/f;->f:I

    goto/32 :goto_3c

    nop

    :goto_7e
    iget-object v0, v0, Ls/p;->i:Ls/f;

    goto/32 :goto_19

    nop

    :goto_7f
    iget-object v0, p0, Ls/p;->b:Lr/e;

    goto/32 :goto_39

    nop

    :goto_80
    invoke-direct {p0, v0}, Ls/j;->q(Ls/f;)V

    goto/32 :goto_11

    nop

    :goto_81
    check-cast v0, Lr/g;

    goto/32 :goto_3a

    nop

    :goto_82
    goto/16 :goto_56

    :goto_83
    goto/32 :goto_23

    nop

    :goto_84
    iget-object v0, v0, Lr/e;->a0:Lr/e;

    goto/32 :goto_5

    nop
.end method

.method public e()V
    .locals 2

    iget-object v0, p0, Ls/p;->b:Lr/e;

    check-cast v0, Lr/g;

    invoke-virtual {v0}, Lr/g;->p1()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Ls/p;->b:Lr/e;

    iget-object v1, p0, Ls/p;->h:Ls/f;

    iget v1, v1, Ls/f;->g:I

    invoke-virtual {v0, v1}, Lr/e;->j1(I)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Ls/p;->b:Lr/e;

    iget-object v1, p0, Ls/p;->h:Ls/f;

    iget v1, v1, Ls/f;->g:I

    invoke-virtual {v0, v1}, Lr/e;->k1(I)V

    :goto_0
    return-void
.end method

.method f()V
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    iget-object v0, p0, Ls/p;->h:Ls/f;

    goto/32 :goto_2

    nop

    :goto_2
    invoke-virtual {v0}, Ls/f;->c()V

    goto/32 :goto_0

    nop
.end method

.method m()Z
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return v0

    :goto_1
    const/4 v0, 0x0

    goto/32 :goto_0

    nop
.end method
