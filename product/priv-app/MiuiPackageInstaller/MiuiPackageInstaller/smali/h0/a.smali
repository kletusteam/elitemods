.class public abstract Lh0/a;
.super Landroid/widget/BaseAdapter;

# interfaces
.implements Landroid/widget/Filterable;
.implements Lh0/b$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lh0/a$b;,
        Lh0/a$a;
    }
.end annotation


# instance fields
.field protected a:Z

.field protected b:Z

.field protected c:Landroid/database/Cursor;

.field protected d:Landroid/content/Context;

.field protected e:I

.field protected f:Lh0/a$a;

.field protected g:Landroid/database/DataSetObserver;

.field protected h:Lh0/b;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;Z)V
    .locals 0

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    if-eqz p3, :cond_0

    const/4 p3, 0x1

    goto :goto_0

    :cond_0
    const/4 p3, 0x2

    :goto_0
    invoke-virtual {p0, p1, p2, p3}, Lh0/a;->f(Landroid/content/Context;Landroid/database/Cursor;I)V

    return-void
.end method


# virtual methods
.method public a(Landroid/database/Cursor;)V
    .locals 0

    invoke-virtual {p0, p1}, Lh0/a;->j(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    :cond_0
    return-void
.end method

.method public b()Landroid/database/Cursor;
    .locals 1

    iget-object v0, p0, Lh0/a;->c:Landroid/database/Cursor;

    return-object v0
.end method

.method public abstract c(Landroid/database/Cursor;)Ljava/lang/CharSequence;
.end method

.method public abstract e(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
.end method

.method f(Landroid/content/Context;Landroid/database/Cursor;I)V
    .locals 3

    goto/32 :goto_19

    nop

    :goto_0
    or-int/lit8 p3, p3, 0x2

    goto/32 :goto_1b

    nop

    :goto_1
    const/4 p1, 0x0

    goto/32 :goto_2

    nop

    :goto_2
    iput-object p1, p0, Lh0/a;->f:Lh0/a$a;

    :goto_3
    goto/32 :goto_1f

    nop

    :goto_4
    iput p1, p0, Lh0/a;->e:I

    goto/32 :goto_14

    nop

    :goto_5
    return-void

    :goto_6
    const-string p1, "_id"

    goto/32 :goto_1a

    nop

    :goto_7
    if-nez v1, :cond_0

    goto/32 :goto_e

    :cond_0
    goto/32 :goto_6

    nop

    :goto_8
    iget-object p1, p0, Lh0/a;->f:Lh0/a$a;

    goto/32 :goto_1c

    nop

    :goto_9
    const/4 v2, 0x1

    goto/32 :goto_24

    nop

    :goto_a
    invoke-direct {p1, p0}, Lh0/a$a;-><init>(Lh0/a;)V

    goto/32 :goto_1e

    nop

    :goto_b
    if-nez p1, :cond_1

    goto/32 :goto_11

    :cond_1
    goto/32 :goto_10

    nop

    :goto_c
    iput-object p2, p0, Lh0/a;->c:Landroid/database/Cursor;

    goto/32 :goto_13

    nop

    :goto_d
    goto :goto_21

    :goto_e
    goto/32 :goto_20

    nop

    :goto_f
    invoke-direct {p1, p0}, Lh0/a$b;-><init>(Lh0/a;)V

    goto/32 :goto_2e

    nop

    :goto_10
    invoke-interface {p2, p1}, Landroid/database/Cursor;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    :goto_11
    goto/32 :goto_5

    nop

    :goto_12
    new-instance p1, Lh0/a$a;

    goto/32 :goto_a

    nop

    :goto_13
    iput-boolean v1, p0, Lh0/a;->a:Z

    goto/32 :goto_28

    nop

    :goto_14
    const/4 p1, 0x2

    goto/32 :goto_26

    nop

    :goto_15
    if-nez p2, :cond_2

    goto/32 :goto_23

    :cond_2
    goto/32 :goto_22

    nop

    :goto_16
    iget-object p1, p0, Lh0/a;->g:Landroid/database/DataSetObserver;

    goto/32 :goto_b

    nop

    :goto_17
    iput-boolean v1, p0, Lh0/a;->b:Z

    :goto_18
    goto/32 :goto_15

    nop

    :goto_19
    and-int/lit8 v0, p3, 0x1

    goto/32 :goto_25

    nop

    :goto_1a
    invoke-interface {p2, p1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result p1

    goto/32 :goto_d

    nop

    :goto_1b
    iput-boolean v2, p0, Lh0/a;->b:Z

    goto/32 :goto_2c

    nop

    :goto_1c
    if-nez p1, :cond_3

    goto/32 :goto_2a

    :cond_3
    goto/32 :goto_29

    nop

    :goto_1d
    if-eq p3, p1, :cond_4

    goto/32 :goto_2f

    :cond_4
    goto/32 :goto_12

    nop

    :goto_1e
    iput-object p1, p0, Lh0/a;->f:Lh0/a$a;

    goto/32 :goto_2b

    nop

    :goto_1f
    iput-object p1, p0, Lh0/a;->g:Landroid/database/DataSetObserver;

    goto/32 :goto_27

    nop

    :goto_20
    const/4 p1, -0x1

    :goto_21
    goto/32 :goto_4

    nop

    :goto_22
    move v1, v2

    :goto_23
    goto/32 :goto_c

    nop

    :goto_24
    if-eq v0, v2, :cond_5

    goto/32 :goto_2d

    :cond_5
    goto/32 :goto_0

    nop

    :goto_25
    const/4 v1, 0x0

    goto/32 :goto_9

    nop

    :goto_26
    and-int/2addr p3, p1

    goto/32 :goto_1d

    nop

    :goto_27
    if-nez v1, :cond_6

    goto/32 :goto_11

    :cond_6
    goto/32 :goto_8

    nop

    :goto_28
    iput-object p1, p0, Lh0/a;->d:Landroid/content/Context;

    goto/32 :goto_7

    nop

    :goto_29
    invoke-interface {p2, p1}, Landroid/database/Cursor;->registerContentObserver(Landroid/database/ContentObserver;)V

    :goto_2a
    goto/32 :goto_16

    nop

    :goto_2b
    new-instance p1, Lh0/a$b;

    goto/32 :goto_f

    nop

    :goto_2c
    goto :goto_18

    :goto_2d
    goto/32 :goto_17

    nop

    :goto_2e
    goto/16 :goto_3

    :goto_2f
    goto/32 :goto_1

    nop
.end method

.method public abstract g(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
.end method

.method public getCount()I
    .locals 1

    iget-boolean v0, p0, Lh0/a;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lh0/a;->c:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    iget-boolean v0, p0, Lh0/a;->a:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lh0/a;->c:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    if-nez p2, :cond_0

    iget-object p1, p0, Lh0/a;->d:Landroid/content/Context;

    iget-object p2, p0, Lh0/a;->c:Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2, p3}, Lh0/a;->g(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    :cond_0
    iget-object p1, p0, Lh0/a;->d:Landroid/content/Context;

    iget-object p3, p0, Lh0/a;->c:Landroid/database/Cursor;

    invoke-virtual {p0, p2, p1, p3}, Lh0/a;->e(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    return-object p2

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method public getFilter()Landroid/widget/Filter;
    .locals 1

    iget-object v0, p0, Lh0/a;->h:Lh0/b;

    if-nez v0, :cond_0

    new-instance v0, Lh0/b;

    invoke-direct {v0, p0}, Lh0/b;-><init>(Lh0/b$a;)V

    iput-object v0, p0, Lh0/a;->h:Lh0/b;

    :cond_0
    iget-object v0, p0, Lh0/a;->h:Lh0/b;

    return-object v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    iget-boolean v0, p0, Lh0/a;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lh0/a;->c:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    iget-object p1, p0, Lh0/a;->c:Landroid/database/Cursor;

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public getItemId(I)J
    .locals 3

    iget-boolean v0, p0, Lh0/a;->a:Z

    const-wide/16 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lh0/a;->c:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lh0/a;->c:Landroid/database/Cursor;

    iget v0, p0, Lh0/a;->e:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0

    :cond_0
    return-wide v1
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    iget-boolean v0, p0, Lh0/a;->a:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lh0/a;->c:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    if-eqz v0, :cond_1

    if-nez p2, :cond_0

    iget-object p1, p0, Lh0/a;->d:Landroid/content/Context;

    iget-object p2, p0, Lh0/a;->c:Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2, p3}, Lh0/a;->h(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    :cond_0
    iget-object p1, p0, Lh0/a;->d:Landroid/content/Context;

    iget-object p3, p0, Lh0/a;->c:Landroid/database/Cursor;

    invoke-virtual {p0, p2, p1, p3}, Lh0/a;->e(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    return-object p2

    :cond_1
    new-instance p2, Ljava/lang/IllegalStateException;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "couldn\'t move cursor to position "

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p2

    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "this should only be called when the cursor is valid"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public abstract h(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
.end method

.method protected i()V
    .locals 1

    iget-boolean v0, p0, Lh0/a;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lh0/a;->c:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lh0/a;->c:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->requery()Z

    move-result v0

    iput-boolean v0, p0, Lh0/a;->a:Z

    :cond_0
    return-void
.end method

.method public j(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 2

    iget-object v0, p0, Lh0/a;->c:Landroid/database/Cursor;

    if-ne p1, v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    if-eqz v0, :cond_2

    iget-object v1, p0, Lh0/a;->f:Lh0/a$a;

    if-eqz v1, :cond_1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    :cond_1
    iget-object v1, p0, Lh0/a;->g:Landroid/database/DataSetObserver;

    if-eqz v1, :cond_2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    :cond_2
    iput-object p1, p0, Lh0/a;->c:Landroid/database/Cursor;

    if-eqz p1, :cond_5

    iget-object v1, p0, Lh0/a;->f:Lh0/a$a;

    if-eqz v1, :cond_3

    invoke-interface {p1, v1}, Landroid/database/Cursor;->registerContentObserver(Landroid/database/ContentObserver;)V

    :cond_3
    iget-object v1, p0, Lh0/a;->g:Landroid/database/DataSetObserver;

    if-eqz v1, :cond_4

    invoke-interface {p1, v1}, Landroid/database/Cursor;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    :cond_4
    const-string v1, "_id"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lh0/a;->e:I

    const/4 p1, 0x1

    iput-boolean p1, p0, Lh0/a;->a:Z

    invoke-virtual {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    goto :goto_0

    :cond_5
    const/4 p1, -0x1

    iput p1, p0, Lh0/a;->e:I

    const/4 p1, 0x0

    iput-boolean p1, p0, Lh0/a;->a:Z

    invoke-virtual {p0}, Landroid/widget/BaseAdapter;->notifyDataSetInvalidated()V

    :goto_0
    return-object v0
.end method
