.class public final Lx8/a;
.super Ljava/lang/Object;


# direct methods
.method private static final a(Ld8/d;Ljava/lang/Throwable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ld8/d<",
            "*>;",
            "Ljava/lang/Throwable;",
            ")V"
        }
    .end annotation

    sget-object v0, La8/m;->a:La8/m$a;

    invoke-static {p1}, La8/n;->a(Ljava/lang/Throwable;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, La8/m;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p0, v0}, Ld8/d;->i(Ljava/lang/Object;)V

    throw p1
.end method

.method public static final b(Ld8/d;Ld8/d;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ld8/d<",
            "-",
            "La8/v;",
            ">;",
            "Ld8/d<",
            "*>;)V"
        }
    .end annotation

    :try_start_0
    invoke-static {p0}, Le8/b;->b(Ld8/d;)Ld8/d;

    move-result-object p0

    sget-object v0, La8/m;->a:La8/m$a;

    sget-object v0, La8/v;->a:La8/v;

    invoke-static {v0}, La8/m;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {p0, v0, v2, v1, v2}, Lkotlinx/coroutines/internal/f;->c(Ld8/d;Ljava/lang/Object;Ll8/l;ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p0

    invoke-static {p1, p0}, Lx8/a;->a(Ld8/d;Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method public static final c(Ll8/p;Ljava/lang/Object;Ld8/d;Ll8/l;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            "T:",
            "Ljava/lang/Object;",
            ">(",
            "Ll8/p<",
            "-TR;-",
            "Ld8/d<",
            "-TT;>;+",
            "Ljava/lang/Object;",
            ">;TR;",
            "Ld8/d<",
            "-TT;>;",
            "Ll8/l<",
            "-",
            "Ljava/lang/Throwable;",
            "La8/v;",
            ">;)V"
        }
    .end annotation

    :try_start_0
    invoke-static {p0, p1, p2}, Le8/b;->a(Ll8/p;Ljava/lang/Object;Ld8/d;)Ld8/d;

    move-result-object p0

    invoke-static {p0}, Le8/b;->b(Ld8/d;)Ld8/d;

    move-result-object p0

    sget-object p1, La8/m;->a:La8/m$a;

    sget-object p1, La8/v;->a:La8/v;

    invoke-static {p1}, La8/m;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    invoke-static {p0, p1, p3}, Lkotlinx/coroutines/internal/f;->b(Ld8/d;Ljava/lang/Object;Ll8/l;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p0

    invoke-static {p2, p0}, Lx8/a;->a(Ld8/d;Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method public static synthetic d(Ll8/p;Ljava/lang/Object;Ld8/d;Ll8/l;ILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    const/4 p3, 0x0

    :cond_0
    invoke-static {p0, p1, p2, p3}, Lx8/a;->c(Ll8/p;Ljava/lang/Object;Ld8/d;Ll8/l;)V

    return-void
.end method
