.class public Le7/c;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Le7/c$b;,
        Le7/c$a;
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Le7/f$b;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-static {}, Le7/f;->a()Le7/f$b;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Le7/c;-><init>(Landroid/content/Context;Le7/f$b;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Le7/f$b;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p2, :cond_1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    :goto_0
    iput-object p1, p0, Le7/c;->a:Landroid/content/Context;

    iput-object p2, p0, Le7/c;->b:Le7/f$b;

    return-void

    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "plainDeviceIdFetcher == null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private static g()Z
    .locals 2

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method


# virtual methods
.method a()Ljava/lang/String;
    .locals 3

    goto/32 :goto_5

    nop

    :goto_0
    return-object v0

    :goto_1
    aput-object v1, v0, v2

    goto/32 :goto_b

    nop

    :goto_2
    const/4 v1, 0x0

    goto/32 :goto_7

    nop

    :goto_3
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    goto/32 :goto_9

    nop

    :goto_4
    const/4 v2, 0x1

    goto/32 :goto_1

    nop

    :goto_5
    const/4 v0, 0x2

    goto/32 :goto_8

    nop

    :goto_6
    aput-object v2, v0, v1

    goto/32 :goto_3

    nop

    :goto_7
    const-string v2, "android_pseudo_"

    goto/32 :goto_6

    nop

    :goto_8
    new-array v0, v0, [Ljava/lang/Object;

    goto/32 :goto_2

    nop

    :goto_9
    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_4

    nop

    :goto_a
    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_0

    nop

    :goto_b
    const-string v1, "%s%s"

    goto/32 :goto_a

    nop
.end method

.method public declared-synchronized b(Z)Ljava/lang/String;
    .locals 3

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Le7/c;->j()Le7/c$a;

    move-result-object v0

    sget-object v1, Le7/c$a;->a:Le7/c$a;

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Le7/c;->d()Ljava/lang/String;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p1

    :cond_0
    :try_start_1
    sget-object v1, Le7/c$a;->b:Le7/c$a;

    if-ne v0, v1, :cond_4

    invoke-virtual {p0}, Le7/c;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Le7/c;->h(Ljava/lang/String;)Z

    move-result v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v1, :cond_1

    monitor-exit p0

    return-object v0

    :cond_1
    :try_start_2
    invoke-virtual {p0}, Le7/c;->d()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0, v0}, Le7/c;->k(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-object v0

    :cond_2
    if-eqz p1, :cond_3

    :try_start_3
    invoke-static {}, Le7/c;->g()Z

    move-result p1

    if-nez p1, :cond_3

    invoke-static {}, Le7/c$b;->b()Le7/c$b;

    move-result-object p1

    invoke-virtual {p1}, Le7/c$b;->c()Le7/d;

    move-result-object p1

    if-eqz p1, :cond_3

    iget-object v0, p0, Le7/c;->a:Landroid/content/Context;

    invoke-interface {p1, v0}, Le7/d;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p0, p1}, Le7/c;->k(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    monitor-exit p0

    return-object p1

    :cond_3
    :try_start_4
    invoke-virtual {p0}, Le7/c;->a()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Le7/c;->k(Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    monitor-exit p0

    return-object p1

    :cond_4
    :try_start_5
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unknown policy "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized c()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p0, v0}, Le7/c;->b(Z)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method d()Ljava/lang/String;
    .locals 3

    :try_start_0
    invoke-virtual {p0}, Le7/c;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Le7/c;->h(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v0}, Le7/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_5

    nop

    :goto_0
    return-object v0

    :goto_1
    const/4 v0, 0x0

    goto/32 :goto_0

    nop

    :goto_2
    const-string v2, "can\'t get deviceid."

    goto/32 :goto_4

    nop

    :goto_3
    const-string v1, "HashedDeviceIdUtil"

    goto/32 :goto_2

    nop

    :goto_4
    invoke-static {v1, v2, v0}, Lg7/b;->j(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    goto/32 :goto_1

    nop

    :goto_5
    return-object v0

    :catch_0
    move-exception v0

    goto/32 :goto_3

    nop
.end method

.method e()Landroid/content/SharedPreferences;
    .locals 3

    goto/32 :goto_8

    nop

    :goto_0
    const/4 v1, 0x0

    goto/32 :goto_9

    nop

    :goto_1
    return-object v0

    :goto_2
    const/4 v0, 0x0

    goto/32 :goto_4

    nop

    :goto_3
    if-eqz v0, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_2

    nop

    :goto_4
    goto :goto_7

    :goto_5
    goto/32 :goto_0

    nop

    :goto_6
    invoke-virtual {v0, v2, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    :goto_7
    goto/32 :goto_1

    nop

    :goto_8
    iget-object v0, p0, Le7/c;->a:Landroid/content/Context;

    goto/32 :goto_3

    nop

    :goto_9
    const-string v2, "deviceId"

    goto/32 :goto_6

    nop
.end method

.method f()Ljava/lang/String;
    .locals 2

    goto/32 :goto_2

    nop

    :goto_0
    invoke-interface {v0, v1}, Le7/f$b;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_1

    nop

    :goto_1
    return-object v0

    :goto_2
    iget-object v0, p0, Le7/c;->b:Le7/f$b;

    goto/32 :goto_3

    nop

    :goto_3
    iget-object v1, p0, Le7/c;->a:Landroid/content/Context;

    goto/32 :goto_0

    nop
.end method

.method h(Ljava/lang/String;)Z
    .locals 0

    goto/32 :goto_2

    nop

    :goto_0
    return p1

    :goto_1
    xor-int/lit8 p1, p1, 0x1

    goto/32 :goto_0

    nop

    :goto_2
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    goto/32 :goto_1

    nop
.end method

.method public i()Ljava/lang/String;
    .locals 3

    invoke-virtual {p0}, Le7/c;->e()Landroid/content/SharedPreferences;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const-string v2, "hashedDeviceId"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1
.end method

.method j()Le7/c$a;
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    return-object v0

    :goto_1
    invoke-static {v0}, Le7/c$b;->a(Le7/c$b;)Le7/c$a;

    move-result-object v0

    goto/32 :goto_0

    nop

    :goto_2
    invoke-static {}, Le7/c$b;->b()Le7/c$b;

    move-result-object v0

    goto/32 :goto_1

    nop
.end method

.method public k(Ljava/lang/String;)V
    .locals 2

    invoke-virtual {p0}, Le7/c;->e()Landroid/content/SharedPreferences;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "hashedDeviceId"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_0
    return-void
.end method
