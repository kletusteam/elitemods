.class public final enum Le7/c$a;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Le7/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Le7/c$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Le7/c$a;

.field public static final enum b:Le7/c$a;

.field private static final synthetic c:[Le7/c$a;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    new-instance v0, Le7/c$a;

    const-string v1, "RUNTIME_DEVICE_ID_ONLY"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Le7/c$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Le7/c$a;->a:Le7/c$a;

    new-instance v1, Le7/c$a;

    const-string v3, "CACHED_THEN_RUNTIME_THEN_PSEUDO"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Le7/c$a;-><init>(Ljava/lang/String;I)V

    sput-object v1, Le7/c$a;->b:Le7/c$a;

    const/4 v3, 0x2

    new-array v3, v3, [Le7/c$a;

    aput-object v0, v3, v2

    aput-object v1, v3, v4

    sput-object v3, Le7/c$a;->c:[Le7/c$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Le7/c$a;
    .locals 1

    const-class v0, Le7/c$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Le7/c$a;

    return-object p0
.end method

.method public static values()[Le7/c$a;
    .locals 1

    sget-object v0, Le7/c$a;->c:[Le7/c$a;

    invoke-virtual {v0}, [Le7/c$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Le7/c$a;

    return-object v0
.end method
