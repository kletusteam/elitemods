.class Li/h$a;
.super Landroidx/core/view/a0;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Li/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private a:Z

.field private b:I

.field final synthetic c:Li/h;


# direct methods
.method constructor <init>(Li/h;)V
    .locals 0

    iput-object p1, p0, Li/h$a;->c:Li/h;

    invoke-direct {p0}, Landroidx/core/view/a0;-><init>()V

    const/4 p1, 0x0

    iput-boolean p1, p0, Li/h$a;->a:Z

    iput p1, p0, Li/h$a;->b:I

    return-void
.end method


# virtual methods
.method public b(Landroid/view/View;)V
    .locals 1

    iget p1, p0, Li/h$a;->b:I

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, Li/h$a;->b:I

    iget-object v0, p0, Li/h$a;->c:Li/h;

    iget-object v0, v0, Li/h;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne p1, v0, :cond_1

    iget-object p1, p0, Li/h$a;->c:Li/h;

    iget-object p1, p1, Li/h;->d:Landroidx/core/view/z;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroidx/core/view/z;->b(Landroid/view/View;)V

    :cond_0
    invoke-virtual {p0}, Li/h$a;->d()V

    :cond_1
    return-void
.end method

.method public c(Landroid/view/View;)V
    .locals 1

    iget-boolean p1, p0, Li/h$a;->a:Z

    if-eqz p1, :cond_0

    return-void

    :cond_0
    const/4 p1, 0x1

    iput-boolean p1, p0, Li/h$a;->a:Z

    iget-object p1, p0, Li/h$a;->c:Li/h;

    iget-object p1, p1, Li/h;->d:Landroidx/core/view/z;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroidx/core/view/z;->c(Landroid/view/View;)V

    :cond_1
    return-void
.end method

.method d()V
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    invoke-virtual {v0}, Li/h;->b()V

    goto/32 :goto_2

    nop

    :goto_1
    const/4 v0, 0x0

    goto/32 :goto_5

    nop

    :goto_2
    return-void

    :goto_3
    iput-boolean v0, p0, Li/h$a;->a:Z

    goto/32 :goto_4

    nop

    :goto_4
    iget-object v0, p0, Li/h$a;->c:Li/h;

    goto/32 :goto_0

    nop

    :goto_5
    iput v0, p0, Li/h$a;->b:I

    goto/32 :goto_3

    nop
.end method
