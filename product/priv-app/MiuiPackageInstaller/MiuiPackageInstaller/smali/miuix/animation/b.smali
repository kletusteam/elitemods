.class public abstract Lmiuix/animation/b;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field static final j:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field public final a:Le9/n;

.field public final b:Le9/d;

.field c:Le9/k;

.field d:F

.field e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Object;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field f:J

.field g:J

.field public final h:I

.field final i:Le9/o;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const v1, 0x7fffffff

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lmiuix/animation/b;->j:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method public constructor <init>()V
    .locals 7

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Le9/n;

    invoke-direct {v0, p0}, Le9/n;-><init>(Lmiuix/animation/b;)V

    iput-object v0, p0, Lmiuix/animation/b;->a:Le9/n;

    new-instance v0, Le9/d;

    invoke-direct {v0}, Le9/d;-><init>()V

    iput-object v0, p0, Lmiuix/animation/b;->b:Le9/d;

    new-instance v1, Le9/k;

    invoke-direct {v1, p0}, Le9/k;-><init>(Lmiuix/animation/b;)V

    iput-object v1, p0, Lmiuix/animation/b;->c:Le9/k;

    const v1, 0x7f7fffff    # Float.MAX_VALUE

    iput v1, p0, Lmiuix/animation/b;->d:F

    new-instance v1, Landroid/util/ArrayMap;

    invoke-direct {v1}, Landroid/util/ArrayMap;-><init>()V

    iput-object v1, p0, Lmiuix/animation/b;->e:Ljava/util/Map;

    sget-object v1, Lmiuix/animation/b;->j:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v1

    iput v1, p0, Lmiuix/animation/b;->h:I

    new-instance v1, Le9/o;

    invoke-direct {v1}, Le9/o;-><init>()V

    iput-object v1, p0, Lmiuix/animation/b;->i:Le9/o;

    invoke-static {}, Lj9/f;->d()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    new-array v1, v2, [Ljava/lang/Object;

    const-string v3, "IAnimTarget create ! "

    invoke-static {v3, v1}, Lj9/f;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    invoke-virtual {v0, p0}, Le9/d;->n(Lmiuix/animation/b;)V

    const v0, 0x3dcccccd    # 0.1f

    const/4 v1, 0x3

    new-array v3, v1, [Lh9/b;

    sget-object v4, Lh9/h;->g:Lh9/h;

    aput-object v4, v3, v2

    sget-object v4, Lh9/h;->h:Lh9/h;

    const/4 v5, 0x1

    aput-object v4, v3, v5

    sget-object v4, Lh9/h;->i:Lh9/h;

    const/4 v6, 0x2

    aput-object v4, v3, v6

    invoke-virtual {p0, v0, v3}, Lmiuix/animation/b;->q(F[Lh9/b;)Lmiuix/animation/b;

    const/high16 v0, 0x3b800000    # 0.00390625f

    const/4 v3, 0x4

    new-array v3, v3, [Lh9/b;

    sget-object v4, Lh9/h;->o:Lh9/h;

    aput-object v4, v3, v2

    sget-object v4, Lh9/h;->p:Lh9/h;

    aput-object v4, v3, v5

    sget-object v4, Lh9/i;->a:Lh9/i$c;

    aput-object v4, v3, v6

    sget-object v4, Lh9/i;->b:Lh9/i$b;

    aput-object v4, v3, v1

    invoke-virtual {p0, v0, v3}, Lmiuix/animation/b;->q(F[Lh9/b;)Lmiuix/animation/b;

    const v0, 0x3b03126f    # 0.002f

    new-array v1, v6, [Lh9/b;

    sget-object v3, Lh9/h;->e:Lh9/h;

    aput-object v3, v1, v2

    sget-object v2, Lh9/h;->f:Lh9/h;

    aput-object v2, v1, v5

    invoke-virtual {p0, v0, v1}, Lmiuix/animation/b;->q(F[Lh9/b;)Lmiuix/animation/b;

    return-void
.end method


# virtual methods
.method public abstract a()V
.end method

.method public b(Ljava/lang/Runnable;)V
    .locals 0

    invoke-virtual {p0, p1}, Lmiuix/animation/b;->n(Ljava/lang/Runnable;)V

    return-void
.end method

.method public c()F
    .locals 1

    const/high16 v0, 0x3f800000    # 1.0f

    return v0
.end method

.method public d()I
    .locals 1

    iget v0, p0, Lmiuix/animation/b;->h:I

    return v0
.end method

.method public e(Lh9/c;)I
    .locals 1

    invoke-virtual {p0}, Lmiuix/animation/b;->h()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p1, v0}, Lh9/c;->a(Ljava/lang/Object;)I

    move-result p1

    return p1

    :cond_0
    const p1, 0x7fffffff

    return p1
.end method

.method public f(Ljava/lang/Object;)F
    .locals 1

    iget-object v0, p0, Lmiuix/animation/b;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Float;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result p1

    return p1

    :cond_0
    iget p1, p0, Lmiuix/animation/b;->d:F

    const v0, 0x7f7fffff    # Float.MAX_VALUE

    cmpl-float v0, p1, v0

    if-eqz v0, :cond_1

    return p1

    :cond_1
    invoke-virtual {p0}, Lmiuix/animation/b;->c()F

    move-result p1

    return p1
.end method

.method protected finalize()V
    .locals 2

    invoke-static {}, Lj9/f;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "IAnimTarget was destroyed \uff01"

    invoke-static {v1, v0}, Lj9/f;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    return-void
.end method

.method public g()Lf9/a;
    .locals 1

    iget-object v0, p0, Lmiuix/animation/b;->c:Le9/k;

    invoke-virtual {v0}, Le9/k;->a()Lf9/a;

    move-result-object v0

    return-object v0
.end method

.method public abstract h()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method public i(Lh9/b;)F
    .locals 1

    invoke-virtual {p0}, Lmiuix/animation/b;->h()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, v0}, Lh9/b;->d(Ljava/lang/Object;)F

    move-result p1

    return p1

    :cond_0
    const p1, 0x7f7fffff    # Float.MAX_VALUE

    return p1
.end method

.method public j(J)Z
    .locals 2

    iget-wide v0, p0, Lmiuix/animation/b;->f:J

    invoke-static {v0, v1, p1, p2}, Lj9/a;->h(JJ)Z

    move-result p1

    return p1
.end method

.method public varargs k([Lh9/b;)Z
    .locals 1

    iget-object v0, p0, Lmiuix/animation/b;->b:Le9/d;

    invoke-virtual {v0, p1}, Le9/d;->g([Lh9/b;)Z

    move-result p1

    return p1
.end method

.method public l()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public m()Z
    .locals 4

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lmiuix/animation/b;->g:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public n(Ljava/lang/Runnable;)V
    .locals 4

    iget-object v0, p0, Lmiuix/animation/b;->a:Le9/n;

    iget-wide v0, v0, Le9/n;->c:J

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getId()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lmiuix/animation/b;->a:Le9/n;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :goto_0
    return-void
.end method

.method public o(J)V
    .locals 0

    iput-wide p1, p0, Lmiuix/animation/b;->f:J

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide p1

    iput-wide p1, p0, Lmiuix/animation/b;->g:J

    return-void
.end method

.method public p(Lh9/c;I)V
    .locals 3

    invoke-virtual {p0}, Lmiuix/animation/b;->h()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result v1

    const v2, 0x7fffffff

    if-eq v1, v2, :cond_0

    invoke-interface {p1, v0, p2}, Lh9/c;->b(Ljava/lang/Object;I)V

    :cond_0
    return-void
.end method

.method public varargs q(F[Lh9/b;)Lmiuix/animation/b;
    .locals 5

    array-length v0, p2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    aget-object v2, p2, v1

    iget-object v3, p0, Lmiuix/animation/b;->e:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-interface {v3, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object p0
.end method

.method public r(Ld9/a;Lc9/b;)V
    .locals 1

    iget-object v0, p0, Lmiuix/animation/b;->c:Le9/k;

    invoke-virtual {v0, p1, p2}, Le9/k;->b(Ld9/a;Lc9/b;)V

    return-void
.end method

.method public s(Lh9/b;F)V
    .locals 3

    invoke-virtual {p0}, Lmiuix/animation/b;->h()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result v1

    const v2, 0x7f7fffff    # Float.MAX_VALUE

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_0

    invoke-virtual {p1, v0, p2}, Lh9/b;->f(Ljava/lang/Object;F)V

    :cond_0
    return-void
.end method

.method public t(Lh9/b;D)V
    .locals 2

    const-wide v0, 0x47efffffe0000000L    # 3.4028234663852886E38

    cmpl-double v0, p2, v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiuix/animation/b;->b:Le9/d;

    double-to-float p2, p2

    invoke-virtual {v0, p1, p2}, Le9/d;->q(Lh9/b;F)V

    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "IAnimTarget{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lmiuix/animation/b;->h()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public u(Lh9/b;D)V
    .locals 1

    iget-object v0, p0, Lmiuix/animation/b;->i:Le9/o;

    invoke-virtual {v0, p0, p1, p2, p3}, Le9/o;->b(Lmiuix/animation/b;Lh9/b;D)V

    return-void
.end method
