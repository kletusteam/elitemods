.class public Lmiuix/animation/ViewTarget$ViewLifecyclerObserver;
.super Ljava/lang/Object;

# interfaces
.implements Landroidx/lifecycle/i;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiuix/animation/ViewTarget;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "ViewLifecyclerObserver"
.end annotation


# instance fields
.field final synthetic a:Lmiuix/animation/ViewTarget;


# direct methods
.method protected constructor <init>(Lmiuix/animation/ViewTarget;)V
    .locals 0

    iput-object p1, p0, Lmiuix/animation/ViewTarget$ViewLifecyclerObserver;->a:Lmiuix/animation/ViewTarget;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method onDestroy()V
    .locals 1
    .annotation runtime Landroidx/lifecycle/r;
        value = .enum Landroidx/lifecycle/d$b;->ON_DESTROY:Landroidx/lifecycle/d$b;
    .end annotation

    goto/32 :goto_2

    nop

    :goto_0
    invoke-static {v0}, Lmiuix/animation/ViewTarget;->w(Lmiuix/animation/ViewTarget;)V

    goto/32 :goto_1

    nop

    :goto_1
    return-void

    :goto_2
    iget-object v0, p0, Lmiuix/animation/ViewTarget$ViewLifecyclerObserver;->a:Lmiuix/animation/ViewTarget;

    goto/32 :goto_0

    nop
.end method
