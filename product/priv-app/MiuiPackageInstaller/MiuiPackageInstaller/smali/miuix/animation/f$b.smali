.class public final enum Lmiuix/animation/f$b;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiuix/animation/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lmiuix/animation/f$b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lmiuix/animation/f$b;

.field public static final enum b:Lmiuix/animation/f$b;

.field private static final synthetic c:[Lmiuix/animation/f$b;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    new-instance v0, Lmiuix/animation/f$b;

    const-string v1, "ENTER"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lmiuix/animation/f$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmiuix/animation/f$b;->a:Lmiuix/animation/f$b;

    new-instance v1, Lmiuix/animation/f$b;

    const-string v3, "EXIT"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lmiuix/animation/f$b;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lmiuix/animation/f$b;->b:Lmiuix/animation/f$b;

    const/4 v3, 0x2

    new-array v3, v3, [Lmiuix/animation/f$b;

    aput-object v0, v3, v2

    aput-object v1, v3, v4

    sput-object v3, Lmiuix/animation/f$b;->c:[Lmiuix/animation/f$b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lmiuix/animation/f$b;
    .locals 1

    const-class v0, Lmiuix/animation/f$b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lmiuix/animation/f$b;

    return-object p0
.end method

.method public static values()[Lmiuix/animation/f$b;
    .locals 1

    sget-object v0, Lmiuix/animation/f$b;->c:[Lmiuix/animation/f$b;

    invoke-virtual {v0}, [Lmiuix/animation/f$b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmiuix/animation/f$b;

    return-object v0
.end method
