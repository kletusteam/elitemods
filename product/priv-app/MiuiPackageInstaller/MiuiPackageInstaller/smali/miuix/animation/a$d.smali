.class Lmiuix/animation/a$d;
.super Ljava/lang/Object;

# interfaces
.implements Lmiuix/animation/e;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiuix/animation/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "d"
.end annotation


# instance fields
.field private a:Lmiuix/animation/h;

.field private b:Lmiuix/animation/j;

.field private c:Lmiuix/animation/l;

.field private d:Lmiuix/animation/f;

.field private e:Lmiuix/animation/c;

.field private f:[Lmiuix/animation/b;


# direct methods
.method private varargs constructor <init>([Lmiuix/animation/b;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmiuix/animation/a$d;->f:[Lmiuix/animation/b;

    const/4 p1, 0x0

    invoke-static {p1}, Lmiuix/animation/a;->a(Z)V

    invoke-static {}, Lmiuix/animation/a;->b()V

    return-void
.end method

.method synthetic constructor <init>([Lmiuix/animation/b;Lmiuix/animation/a$a;)V
    .locals 0

    invoke-direct {p0, p1}, Lmiuix/animation/a$d;-><init>([Lmiuix/animation/b;)V

    return-void
.end method


# virtual methods
.method public a()Lmiuix/animation/f;
    .locals 2

    iget-object v0, p0, Lmiuix/animation/a$d;->d:Lmiuix/animation/f;

    if-nez v0, :cond_0

    new-instance v0, Ld9/e;

    iget-object v1, p0, Lmiuix/animation/a$d;->f:[Lmiuix/animation/b;

    invoke-direct {v0, v1}, Ld9/e;-><init>([Lmiuix/animation/b;)V

    iput-object v0, p0, Lmiuix/animation/a$d;->d:Lmiuix/animation/f;

    :cond_0
    iget-object v0, p0, Lmiuix/animation/a$d;->d:Lmiuix/animation/f;

    return-object v0
.end method

.method public b()Lmiuix/animation/h;
    .locals 1

    iget-object v0, p0, Lmiuix/animation/a$d;->a:Lmiuix/animation/h;

    if-nez v0, :cond_0

    iget-object v0, p0, Lmiuix/animation/a$d;->f:[Lmiuix/animation/b;

    invoke-static {v0}, Ld9/j;->a([Lmiuix/animation/b;)Ld9/h;

    move-result-object v0

    iput-object v0, p0, Lmiuix/animation/a$d;->a:Lmiuix/animation/h;

    :cond_0
    iget-object v0, p0, Lmiuix/animation/a$d;->a:Lmiuix/animation/h;

    return-object v0
.end method

.method public c()Lmiuix/animation/j;
    .locals 2

    iget-object v0, p0, Lmiuix/animation/a$d;->b:Lmiuix/animation/j;

    if-nez v0, :cond_0

    new-instance v0, Ld9/g;

    iget-object v1, p0, Lmiuix/animation/a$d;->f:[Lmiuix/animation/b;

    invoke-direct {v0, v1}, Ld9/g;-><init>([Lmiuix/animation/b;)V

    new-instance v1, Ld9/d;

    invoke-direct {v1}, Ld9/d;-><init>()V

    invoke-virtual {v0, v1}, Ld9/g;->r0(Ld9/d;)V

    iput-object v0, p0, Lmiuix/animation/a$d;->b:Lmiuix/animation/j;

    :cond_0
    iget-object v0, p0, Lmiuix/animation/a$d;->b:Lmiuix/animation/j;

    return-object v0
.end method

.method public d()Lmiuix/animation/c;
    .locals 2

    iget-object v0, p0, Lmiuix/animation/a$d;->e:Lmiuix/animation/c;

    if-nez v0, :cond_0

    new-instance v0, Ld9/c;

    iget-object v1, p0, Lmiuix/animation/a$d;->f:[Lmiuix/animation/b;

    invoke-direct {v0, v1}, Ld9/c;-><init>([Lmiuix/animation/b;)V

    iput-object v0, p0, Lmiuix/animation/a$d;->e:Lmiuix/animation/c;

    :cond_0
    iget-object v0, p0, Lmiuix/animation/a$d;->e:Lmiuix/animation/c;

    return-object v0
.end method

.method e()V
    .locals 1

    goto/32 :goto_4

    nop

    :goto_0
    invoke-interface {v0}, Lmiuix/animation/g;->n()V

    :goto_1
    goto/32 :goto_e

    nop

    :goto_2
    return-void

    :goto_3
    if-nez v0, :cond_0

    goto/32 :goto_d

    :cond_0
    goto/32 :goto_c

    nop

    :goto_4
    iget-object v0, p0, Lmiuix/animation/a$d;->b:Lmiuix/animation/j;

    goto/32 :goto_9

    nop

    :goto_5
    invoke-interface {v0}, Lmiuix/animation/g;->n()V

    :goto_6
    goto/32 :goto_b

    nop

    :goto_7
    if-nez v0, :cond_1

    goto/32 :goto_6

    :cond_1
    goto/32 :goto_5

    nop

    :goto_8
    if-nez v0, :cond_2

    goto/32 :goto_10

    :cond_2
    goto/32 :goto_f

    nop

    :goto_9
    if-nez v0, :cond_3

    goto/32 :goto_1

    :cond_3
    goto/32 :goto_0

    nop

    :goto_a
    iget-object v0, p0, Lmiuix/animation/a$d;->a:Lmiuix/animation/h;

    goto/32 :goto_7

    nop

    :goto_b
    iget-object v0, p0, Lmiuix/animation/a$d;->d:Lmiuix/animation/f;

    goto/32 :goto_3

    nop

    :goto_c
    invoke-interface {v0}, Lmiuix/animation/g;->n()V

    :goto_d
    goto/32 :goto_2

    nop

    :goto_e
    iget-object v0, p0, Lmiuix/animation/a$d;->c:Lmiuix/animation/l;

    goto/32 :goto_8

    nop

    :goto_f
    invoke-interface {v0}, Lmiuix/animation/g;->n()V

    :goto_10
    goto/32 :goto_a

    nop
.end method

.method f()V
    .locals 3

    goto/32 :goto_b

    nop

    :goto_0
    if-nez v0, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_5

    nop

    :goto_1
    iget-object v0, p0, Lmiuix/animation/a$d;->c:Lmiuix/animation/l;

    goto/32 :goto_11

    nop

    :goto_2
    invoke-interface {v0, v2}, Lmiuix/animation/d;->k([Ljava/lang/Object;)V

    :goto_3
    goto/32 :goto_f

    nop

    :goto_4
    new-array v2, v1, [Ljava/lang/Object;

    goto/32 :goto_13

    nop

    :goto_5
    new-array v2, v1, [Ljava/lang/Object;

    goto/32 :goto_6

    nop

    :goto_6
    invoke-interface {v0, v2}, Lmiuix/animation/d;->k([Ljava/lang/Object;)V

    :goto_7
    goto/32 :goto_a

    nop

    :goto_8
    invoke-interface {v0, v1}, Lmiuix/animation/d;->k([Ljava/lang/Object;)V

    :goto_9
    goto/32 :goto_15

    nop

    :goto_a
    iget-object v0, p0, Lmiuix/animation/a$d;->d:Lmiuix/animation/f;

    goto/32 :goto_e

    nop

    :goto_b
    iget-object v0, p0, Lmiuix/animation/a$d;->b:Lmiuix/animation/j;

    goto/32 :goto_d

    nop

    :goto_c
    if-nez v0, :cond_1

    goto/32 :goto_14

    :cond_1
    goto/32 :goto_4

    nop

    :goto_d
    const/4 v1, 0x0

    goto/32 :goto_c

    nop

    :goto_e
    if-nez v0, :cond_2

    goto/32 :goto_9

    :cond_2
    goto/32 :goto_10

    nop

    :goto_f
    iget-object v0, p0, Lmiuix/animation/a$d;->a:Lmiuix/animation/h;

    goto/32 :goto_0

    nop

    :goto_10
    new-array v1, v1, [Ljava/lang/Object;

    goto/32 :goto_8

    nop

    :goto_11
    if-nez v0, :cond_3

    goto/32 :goto_3

    :cond_3
    goto/32 :goto_12

    nop

    :goto_12
    new-array v2, v1, [Ljava/lang/Object;

    goto/32 :goto_2

    nop

    :goto_13
    invoke-interface {v0, v2}, Lmiuix/animation/d;->k([Ljava/lang/Object;)V

    :goto_14
    goto/32 :goto_1

    nop

    :goto_15
    return-void
.end method
