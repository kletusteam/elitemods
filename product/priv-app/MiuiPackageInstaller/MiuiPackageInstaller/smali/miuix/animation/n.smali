.class public Lmiuix/animation/n;
.super Lmiuix/animation/b;


# static fields
.field static m:Lmiuix/animation/i;


# instance fields
.field private k:Lh9/g;

.field private l:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lmiuix/animation/n$a;

    invoke-direct {v0}, Lmiuix/animation/n$a;-><init>()V

    sput-object v0, Lmiuix/animation/n;->m:Lmiuix/animation/i;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lmiuix/animation/n;-><init>(Ljava/lang/Object;)V

    return-void
.end method

.method private constructor <init>(Ljava/lang/Object;)V
    .locals 2

    invoke-direct {p0}, Lmiuix/animation/b;-><init>()V

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/16 v1, 0x3e8

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lmiuix/animation/n;->l:Ljava/util/concurrent/atomic/AtomicInteger;

    new-instance v0, Lh9/g;

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lmiuix/animation/b;->d()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    :cond_0
    invoke-direct {v0, p1}, Lh9/g;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lmiuix/animation/n;->k:Lh9/g;

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/Object;Lmiuix/animation/n$a;)V
    .locals 0

    invoke-direct {p0, p1}, Lmiuix/animation/n;-><init>(Ljava/lang/Object;)V

    return-void
.end method

.method private x(Ljava/lang/Object;)Z
    .locals 1

    instance-of v0, p1, Lh9/f;

    if-nez v0, :cond_1

    instance-of v0, p1, Lh9/h;

    if-nez v0, :cond_1

    instance-of p1, p1, Lh9/a;

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method


# virtual methods
.method public a()V
    .locals 0

    return-void
.end method

.method public c()F
    .locals 1

    const v0, 0x3b03126f    # 0.002f

    return v0
.end method

.method public e(Lh9/c;)I
    .locals 2

    invoke-direct {p0, p1}, Lmiuix/animation/n;->x(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmiuix/animation/n;->k:Lh9/g;

    invoke-interface {p1}, Lh9/c;->getName()Ljava/lang/String;

    move-result-object p1

    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-virtual {v0, p1, v1}, Lh9/g;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    if-nez p1, :cond_0

    const p1, 0x7fffffff

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    :goto_0
    return p1

    :cond_1
    iget-object v0, p0, Lmiuix/animation/n;->k:Lh9/g;

    invoke-virtual {v0}, Lh9/g;->b()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p1, v0}, Lh9/c;->a(Ljava/lang/Object;)I

    move-result p1

    return p1
.end method

.method public f(Ljava/lang/Object;)F
    .locals 1

    instance-of v0, p1, Lh9/c;

    if-eqz v0, :cond_0

    instance-of v0, p1, Lh9/a;

    if-nez v0, :cond_0

    const/high16 p1, 0x3f800000    # 1.0f

    return p1

    :cond_0
    invoke-super {p0, p1}, Lmiuix/animation/b;->f(Ljava/lang/Object;)F

    move-result p1

    return p1
.end method

.method public h()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lmiuix/animation/n;->k:Lh9/g;

    return-object v0
.end method

.method public i(Lh9/b;)F
    .locals 2

    invoke-direct {p0, p1}, Lmiuix/animation/n;->x(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmiuix/animation/n;->k:Lh9/g;

    invoke-virtual {p1}, Landroid/util/Property;->getName()Ljava/lang/String;

    move-result-object p1

    sget-object v1, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    invoke-virtual {v0, p1, v1}, Lh9/g;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Float;

    if-nez p1, :cond_0

    const p1, 0x7f7fffff    # Float.MAX_VALUE

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result p1

    :goto_0
    return p1

    :cond_1
    iget-object v0, p0, Lmiuix/animation/n;->k:Lh9/g;

    invoke-virtual {v0}, Lh9/g;->b()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Lh9/b;->d(Ljava/lang/Object;)F

    move-result p1

    return p1
.end method

.method public l()Z
    .locals 1

    iget-object v0, p0, Lmiuix/animation/n;->k:Lh9/g;

    invoke-virtual {v0}, Lh9/g;->c()Z

    move-result v0

    return v0
.end method

.method public p(Lh9/c;I)V
    .locals 2

    invoke-direct {p0, p1}, Lmiuix/animation/n;->x(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiuix/animation/n;->k:Lh9/g;

    invoke-interface {p1}, Lh9/c;->getName()Ljava/lang/String;

    move-result-object p1

    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {v0, p1, v1, p2}, Lh9/g;->d(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lmiuix/animation/n;->k:Lh9/g;

    invoke-virtual {v0}, Lh9/g;->b()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p1, v0, p2}, Lh9/c;->b(Ljava/lang/Object;I)V

    :goto_0
    return-void
.end method

.method public s(Lh9/b;F)V
    .locals 2

    invoke-direct {p0, p1}, Lmiuix/animation/n;->x(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiuix/animation/n;->k:Lh9/g;

    invoke-virtual {p1}, Landroid/util/Property;->getName()Ljava/lang/String;

    move-result-object p1

    sget-object v1, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p2

    invoke-virtual {v0, p1, v1, p2}, Lh9/g;->d(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lmiuix/animation/n;->k:Lh9/g;

    invoke-virtual {v0}, Lh9/g;->b()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Lh9/b;->f(Ljava/lang/Object;F)V

    :goto_0
    return-void
.end method

.method public v(Ljava/lang/String;Ljava/lang/Class;)Lh9/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Class<",
            "*>;)",
            "Lh9/b;"
        }
    .end annotation

    sget-object v0, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    if-eq p2, v0, :cond_1

    const-class v0, Ljava/lang/Integer;

    if-ne p2, v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance p2, Lh9/f;

    invoke-direct {p2, p1}, Lh9/f;-><init>(Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    :goto_0
    new-instance p2, Lh9/e;

    invoke-direct {p2, p1}, Lh9/e;-><init>(Ljava/lang/String;)V

    :goto_1
    return-object p2
.end method

.method public w(Ljava/lang/String;)Lh9/b;
    .locals 1

    sget-object v0, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    invoke-virtual {p0, p1, v0}, Lmiuix/animation/n;->v(Ljava/lang/String;Ljava/lang/Class;)Lh9/b;

    move-result-object p1

    return-object p1
.end method
