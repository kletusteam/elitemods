.class public final Lmiuix/appcompat/app/floatingactivity/multiapp/c;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;,
        Lmiuix/appcompat/app/floatingactivity/multiapp/c$e;,
        Lmiuix/appcompat/app/floatingactivity/multiapp/c$f;,
        Lmiuix/appcompat/app/floatingactivity/multiapp/c$d;
    }
.end annotation


# static fields
.field private static k:Lmiuix/appcompat/app/floatingactivity/multiapp/c;


# instance fields
.field private final a:Landroid/os/Handler;

.field private final b:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Ljava/util/ArrayList<",
            "Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;",
            ">;>;"
        }
    .end annotation
.end field

.field private c:Z

.field private d:Lmiuix/appcompat/app/floatingactivity/multiapp/a;

.field private e:J

.field private f:J

.field private g:J

.field private h:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private i:Z

.field private final j:Landroid/content/ServiceConnection;


# direct methods
.method private constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->a:Landroid/os/Handler;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->b:Landroid/util/SparseArray;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->c:Z

    new-instance v0, Lmiuix/appcompat/app/floatingactivity/multiapp/c$a;

    invoke-direct {v0, p0}, Lmiuix/appcompat/app/floatingactivity/multiapp/c$a;-><init>(Lmiuix/appcompat/app/floatingactivity/multiapp/c;)V

    iput-object v0, p0, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->j:Landroid/content/ServiceConnection;

    return-void
.end method

.method static B()Lmiuix/appcompat/app/floatingactivity/multiapp/c;
    .locals 1

    sget-object v0, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->k:Lmiuix/appcompat/app/floatingactivity/multiapp/c;

    return-object v0
.end method

.method private E()V
    .locals 4

    iget-wide v0, p0, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->f:J

    invoke-direct {p0, v0, v1}, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->M(J)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->f:J

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->b:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    if-ge v0, v1, :cond_3

    iget-object v1, p0, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->b:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;

    iget-boolean v3, v2, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;->b:Z

    if-nez v3, :cond_1

    iget-object v2, v2, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;->g:Lmiuix/appcompat/app/j;

    if-eqz v2, :cond_1

    new-instance v3, Lo9/a;

    invoke-direct {v3, v2}, Lo9/a;-><init>(Lmiuix/appcompat/app/j;)V

    invoke-virtual {v2, v3}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_1

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    return-void
.end method

.method private F(I)V
    .locals 3

    iget-object v0, p0, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/ArrayList;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;

    iget v1, v1, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;->a:I

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;

    iget-object v2, v2, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;->g:Lmiuix/appcompat/app/j;

    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {v2}, Lmiuix/appcompat/app/j;->v0()V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private G(Lmiuix/appcompat/app/j;Landroid/content/Intent;Landroid/os/Bundle;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->c0(Lmiuix/appcompat/app/j;Landroid/content/Intent;Landroid/os/Bundle;)V

    invoke-direct {p0, p1}, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->W(Lmiuix/appcompat/app/j;)V

    invoke-virtual {p1}, Landroidx/activity/ComponentActivity;->a()Landroidx/lifecycle/d;

    move-result-object p2

    new-instance p3, Lmiuix/appcompat/app/floatingactivity/multiapp/MultiAppFloatingLifecycleObserver;

    invoke-direct {p3, p1}, Lmiuix/appcompat/app/floatingactivity/multiapp/MultiAppFloatingLifecycleObserver;-><init>(Lmiuix/appcompat/app/j;)V

    invoke-virtual {p2, p3}, Landroidx/lifecycle/d;->a(Landroidx/lifecycle/i;)V

    iget-boolean p2, p0, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->c:Z

    invoke-virtual {p1, p2}, Lmiuix/appcompat/app/j;->z0(Z)V

    new-instance p2, Lmiuix/appcompat/app/floatingactivity/multiapp/c$d;

    invoke-direct {p2, p0, p1}, Lmiuix/appcompat/app/floatingactivity/multiapp/c$d;-><init>(Lmiuix/appcompat/app/floatingactivity/multiapp/c;Lmiuix/appcompat/app/j;)V

    invoke-virtual {p1, p2}, Lmiuix/appcompat/app/j;->B0(Lm9/g;)V

    return-void
.end method

.method public static H(Lmiuix/appcompat/app/j;Landroid/content/Intent;Landroid/os/Bundle;)V
    .locals 1

    invoke-static {p1}, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->N(Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0, p2}, Lmiuix/appcompat/app/floatingactivity/a;->w(Lmiuix/appcompat/app/j;Landroid/os/Bundle;)V

    return-void

    :cond_0
    sget-object v0, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->k:Lmiuix/appcompat/app/floatingactivity/multiapp/c;

    if-nez v0, :cond_1

    new-instance v0, Lmiuix/appcompat/app/floatingactivity/multiapp/c;

    invoke-direct {v0}, Lmiuix/appcompat/app/floatingactivity/multiapp/c;-><init>()V

    sput-object v0, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->k:Lmiuix/appcompat/app/floatingactivity/multiapp/c;

    invoke-direct {v0, p0, p1}, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->q(Landroid/content/Context;Landroid/content/Intent;)V

    :cond_1
    sget-object v0, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->k:Lmiuix/appcompat/app/floatingactivity/multiapp/c;

    invoke-direct {v0, p0, p1, p2}, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->G(Lmiuix/appcompat/app/j;Landroid/content/Intent;Landroid/os/Bundle;)V

    return-void
.end method

.method private I(Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;)V
    .locals 3

    if-nez p1, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->d:Lmiuix/appcompat/app/floatingactivity/multiapp/a;

    if-eqz v0, :cond_3

    :try_start_0
    iget-object v1, p1, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;->c:Lmiuix/appcompat/app/floatingactivity/multiapp/c$f;

    iget v2, p1, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;->h:I

    invoke-virtual {p0, v1, v2}, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->A(Ljava/lang/Object;I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lmiuix/appcompat/app/floatingactivity/multiapp/a;->a(Lmiuix/appcompat/app/floatingactivity/multiapp/b;Ljava/lang/String;)I

    iget-object v0, p1, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;->c:Lmiuix/appcompat/app/floatingactivity/multiapp/c$f;

    iget v1, p1, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;->h:I

    invoke-virtual {p0, v0, v1}, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->A(Ljava/lang/Object;I)Ljava/lang/String;

    move-result-object v0

    iget v1, p1, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;->a:I

    invoke-direct {p0, v0, v1}, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->h0(Ljava/lang/String;I)V

    iget-boolean v0, p1, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;->e:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p1, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;->e:Z

    iget v0, p1, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;->a:I

    iput v0, p1, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;->d:I

    :cond_1
    iget-object v0, p1, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Runnable;

    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    :cond_2
    iget-object p1, p1, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;->f:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->clear()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    const-string v0, "MFloatingSwitcher"

    const-string v1, "catch register service notify exception"

    invoke-static {v0, v1, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_3
    :goto_1
    return-void
.end method

.method private L(Lmiuix/appcompat/app/j;)Z
    .locals 2

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/app/Activity;->getTaskId()I

    move-result v1

    invoke-virtual {p1}, Lmiuix/appcompat/app/j;->q0()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, v1, p1}, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->y(ILjava/lang/String;)Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;

    move-result-object p1

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method private M(J)Z
    .locals 2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sub-long/2addr v0, p1

    const-wide/16 p1, 0x64

    cmp-long p1, v0, p1

    if-gtz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public static N(Landroid/content/Intent;)Z
    .locals 1

    const-string v0, "floating_service_pkg"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "floating_service_path"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private R(I)Landroid/os/Bundle;
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->S(ILandroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object p1

    return-object p1
.end method

.method private S(ILandroid/os/Bundle;)Landroid/os/Bundle;
    .locals 2

    iget-object v0, p0, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->d:Lmiuix/appcompat/app/floatingactivity/multiapp/a;

    const-string v1, "MFloatingSwitcher"

    if-eqz v0, :cond_0

    :try_start_0
    invoke-interface {v0, p1, p2}, Lmiuix/appcompat/app/floatingactivity/multiapp/a;->i(ILandroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object p1
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    const-string p2, "catch call service method exception"

    invoke-static {v1, p2, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_0
    const-string p1, "ifloatingservice is null"

    invoke-static {v1, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    const/4 p1, 0x0

    return-object p1
.end method

.method private T()V
    .locals 4

    iget-wide v0, p0, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->g:J

    invoke-direct {p0, v0, v1}, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->M(J)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->g:J

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->b:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    if-ge v0, v1, :cond_3

    iget-object v1, p0, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->b:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;

    iget-boolean v3, v2, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;->b:Z

    if-nez v3, :cond_1

    iget-object v2, v2, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;->g:Lmiuix/appcompat/app/j;

    if-eqz v2, :cond_1

    new-instance v3, Lo9/b;

    invoke-direct {v3, v2}, Lo9/b;-><init>(Lmiuix/appcompat/app/j;)V

    invoke-virtual {v2, v3}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_1

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    return-void
.end method

.method public static U(ILjava/lang/String;Landroid/os/Bundle;)V
    .locals 1

    invoke-static {}, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->B()Lmiuix/appcompat/app/floatingactivity/multiapp/c;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {v0, p0, p1}, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->y(ILjava/lang/String;)Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;

    move-result-object p0

    if-eqz p0, :cond_0

    const-string p1, "floating_switcher_saved_key"

    invoke-virtual {p2, p1, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_0
    return-void
.end method

.method private W(Lmiuix/appcompat/app/j;)V
    .locals 2

    invoke-virtual {p1}, Landroid/app/Activity;->getTaskId()I

    move-result v0

    invoke-virtual {p1}, Lmiuix/appcompat/app/j;->q0()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->y(ILjava/lang/String;)Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, v0, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;->c:Lmiuix/appcompat/app/floatingactivity/multiapp/c$f;

    if-nez v1, :cond_0

    new-instance v1, Lmiuix/appcompat/app/floatingactivity/multiapp/c$f;

    invoke-direct {v1, p0, p1}, Lmiuix/appcompat/app/floatingactivity/multiapp/c$f;-><init>(Lmiuix/appcompat/app/floatingactivity/multiapp/c;Lmiuix/appcompat/app/j;)V

    iput-object v1, v0, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;->c:Lmiuix/appcompat/app/floatingactivity/multiapp/c$f;

    goto :goto_0

    :cond_0
    if-eqz v0, :cond_1

    iget-object v1, v0, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;->c:Lmiuix/appcompat/app/floatingactivity/multiapp/c$f;

    invoke-virtual {v1, p1}, Lmiuix/appcompat/app/floatingactivity/multiapp/c$f;->q(Lmiuix/appcompat/app/j;)V

    :cond_1
    :goto_0
    invoke-direct {p0, v0}, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->I(Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;)V

    return-void
.end method

.method private Z(Lmiuix/appcompat/app/floatingactivity/multiapp/a;)V
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->d:Lmiuix/appcompat/app/floatingactivity/multiapp/a;

    const/4 p1, 0x1

    iput-boolean p1, p0, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->i:Z

    return-void
.end method

.method static synthetic a()Lmiuix/appcompat/app/floatingactivity/multiapp/c;
    .locals 1

    sget-object v0, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->k:Lmiuix/appcompat/app/floatingactivity/multiapp/c;

    return-object v0
.end method

.method static synthetic b(Lmiuix/appcompat/app/floatingactivity/multiapp/c;Lmiuix/appcompat/app/floatingactivity/multiapp/a;)V
    .locals 0

    invoke-direct {p0, p1}, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->Z(Lmiuix/appcompat/app/floatingactivity/multiapp/a;)V

    return-void
.end method

.method private b0(II)Z
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x4

    if-eq p1, v1, :cond_0

    const/4 v1, 0x3

    if-ne p1, v1, :cond_2

    :cond_0
    invoke-virtual {p0, p2}, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->D(I)I

    move-result p1

    if-gt p1, v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :cond_2
    :goto_0
    return v0
.end method

.method static synthetic c(Lmiuix/appcompat/app/floatingactivity/multiapp/c;)V
    .locals 0

    invoke-direct {p0}, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->T()V

    return-void
.end method

.method private c0(Lmiuix/appcompat/app/j;Landroid/content/Intent;Landroid/os/Bundle;)V
    .locals 5

    invoke-direct {p0, p1}, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->L(Lmiuix/appcompat/app/j;)Z

    move-result v0

    if-nez v0, :cond_6

    const/4 v0, 0x0

    if-eqz p3, :cond_0

    const-string v0, "floating_switcher_saved_key"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p3

    move-object v0, p3

    check-cast v0, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;

    :cond_0
    const/4 p3, 0x0

    const/4 v1, 0x1

    if-nez v0, :cond_2

    new-instance v0, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;

    invoke-direct {v0, v1}, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;-><init>(Z)V

    if-nez p2, :cond_1

    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p2

    :cond_1
    const-string v2, "service_page_index"

    invoke-virtual {p2, v2, p3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result p2

    iput p2, v0, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;->a:I

    :cond_2
    iput-object p1, v0, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;->g:Lmiuix/appcompat/app/j;

    invoke-virtual {p1}, Landroid/app/Activity;->getTaskId()I

    move-result p2

    iput p2, v0, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;->h:I

    invoke-virtual {p1}, Lmiuix/appcompat/app/j;->q0()Ljava/lang/String;

    move-result-object p2

    iput-object p2, v0, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;->i:Ljava/lang/String;

    iget-object p2, p0, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->b:Landroid/util/SparseArray;

    iget v2, v0, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;->h:I

    invoke-virtual {p2, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/util/ArrayList;

    if-nez p2, :cond_3

    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    iget-object v2, p0, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->b:Landroid/util/SparseArray;

    iget v3, v0, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;->h:I

    invoke-virtual {v2, v3, p2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    :cond_3
    iget v2, v0, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;->a:I

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v3

    sub-int/2addr v3, v1

    :goto_0
    if-ltz v3, :cond_5

    invoke-virtual {p2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;

    iget v4, v4, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;->a:I

    if-le v2, v4, :cond_4

    add-int/lit8 p3, v3, 0x1

    goto :goto_1

    :cond_4
    add-int/lit8 v3, v3, -0x1

    goto :goto_0

    :cond_5
    :goto_1
    invoke-virtual {p2, p3, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    iget p2, v0, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;->a:I

    invoke-static {p1, p2}, Lm9/b;->g(Lmiuix/appcompat/app/j;I)V

    :cond_6
    invoke-virtual {p1}, Landroid/app/Activity;->getTaskId()I

    move-result p1

    invoke-direct {p0, p1}, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->F(I)V

    return-void
.end method

.method static synthetic d(Lmiuix/appcompat/app/floatingactivity/multiapp/c;)V
    .locals 0

    invoke-direct {p0}, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->u()V

    return-void
.end method

.method private d0(ILjava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->d:Lmiuix/appcompat/app/floatingactivity/multiapp/a;

    if-eqz v0, :cond_0

    :try_start_0
    invoke-direct {p0, p1, p2}, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->y(ILjava/lang/String;)Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object p2, p0, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->d:Lmiuix/appcompat/app/floatingactivity/multiapp/a;

    iget-object p1, p1, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;->c:Lmiuix/appcompat/app/floatingactivity/multiapp/c$f;

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, p1, v0}, Lmiuix/appcompat/app/floatingactivity/multiapp/a;->f(Lmiuix/appcompat/app/floatingactivity/multiapp/b;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    const-string p2, "MFloatingSwitcher"

    const-string v0, "catch unregister service notify exception"

    invoke-static {p2, v0, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    :goto_0
    return-void
.end method

.method static synthetic e(Lmiuix/appcompat/app/floatingactivity/multiapp/c;Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->f0(Landroid/content/Context;)V

    return-void
.end method

.method private e0()V
    .locals 4

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->b:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->b:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;

    iget v3, v2, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;->h:I

    iget-object v2, v2, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;->i:Ljava/lang/String;

    invoke-direct {p0, v3, v2}, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->d0(ILjava/lang/String;)V

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method static synthetic f(Lmiuix/appcompat/app/floatingactivity/multiapp/c;)V
    .locals 0

    invoke-direct {p0}, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->v()V

    return-void
.end method

.method private f0(Landroid/content/Context;)V
    .locals 1

    iget-boolean v0, p0, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->i:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->i:Z

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iget-object v0, p0, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->j:Landroid/content/ServiceConnection;

    invoke-virtual {p1, v0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    :cond_0
    return-void
.end method

.method static synthetic g(Lmiuix/appcompat/app/floatingactivity/multiapp/c;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->a:Landroid/os/Handler;

    return-object p0
.end method

.method static synthetic h(Lmiuix/appcompat/app/floatingactivity/multiapp/c;)Ljava/lang/ref/WeakReference;
    .locals 0

    iget-object p0, p0, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->h:Ljava/lang/ref/WeakReference;

    return-object p0
.end method

.method private h0(Ljava/lang/String;I)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->d:Lmiuix/appcompat/app/floatingactivity/multiapp/a;

    if-eqz v0, :cond_0

    :try_start_0
    invoke-interface {v0, p1, p2}, Lmiuix/appcompat/app/floatingactivity/multiapp/a;->h(Ljava/lang/String;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    const-string p2, "MFloatingSwitcher"

    const-string v0, "catch updateServerActivityIndex service notify exception"

    invoke-static {p2, v0, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    :goto_0
    return-void
.end method

.method static synthetic i(Lmiuix/appcompat/app/floatingactivity/multiapp/c;)V
    .locals 0

    invoke-direct {p0}, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->s()V

    return-void
.end method

.method static synthetic j(Lmiuix/appcompat/app/floatingactivity/multiapp/c;)V
    .locals 0

    invoke-direct {p0}, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->e0()V

    return-void
.end method

.method static synthetic k(Lmiuix/appcompat/app/floatingactivity/multiapp/c;ILandroid/os/Bundle;)Landroid/os/Bundle;
    .locals 0

    invoke-direct {p0, p1, p2}, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->S(ILandroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object p0

    return-object p0
.end method

.method static synthetic l(Lmiuix/appcompat/app/floatingactivity/multiapp/c;II)Z
    .locals 0

    invoke-direct {p0, p1, p2}, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->b0(II)Z

    move-result p0

    return p0
.end method

.method static synthetic m(Lmiuix/appcompat/app/floatingactivity/multiapp/c;I)Landroid/os/Bundle;
    .locals 0

    invoke-direct {p0, p1}, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->R(I)Landroid/os/Bundle;

    move-result-object p0

    return-object p0
.end method

.method static synthetic n(Lmiuix/appcompat/app/floatingactivity/multiapp/c;)Z
    .locals 0

    iget-boolean p0, p0, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->c:Z

    return p0
.end method

.method static synthetic o(Lmiuix/appcompat/app/floatingactivity/multiapp/c;)Landroid/util/SparseArray;
    .locals 0

    iget-object p0, p0, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->b:Landroid/util/SparseArray;

    return-object p0
.end method

.method static synthetic p(Lmiuix/appcompat/app/floatingactivity/multiapp/c;)V
    .locals 0

    invoke-direct {p0}, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->E()V

    return-void
.end method

.method private q(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "floating_service_pkg"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    return-void

    :cond_0
    invoke-virtual {v0, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "floating_service_path"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    return-void

    :cond_1
    new-instance v3, Landroid/content/ComponentName;

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-direct {v3, p2, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iget-object p2, p0, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->j:Landroid/content/ServiceConnection;

    const/4 v1, 0x1

    invoke-virtual {p1, v0, p2, v1}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    return-void
.end method

.method private s()V
    .locals 4

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->b:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->b:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;

    iget-boolean v3, v2, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;->e:Z

    if-nez v3, :cond_0

    invoke-direct {p0, v2}, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->I(Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;)V

    iget v3, v2, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;->h:I

    iget-object v2, v2, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;->i:Ljava/lang/String;

    invoke-virtual {p0, v3, v2}, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->r(ILjava/lang/String;)V

    goto :goto_1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method private u()V
    .locals 6

    iget-wide v0, p0, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->e:J

    invoke-direct {p0, v0, v1}, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->M(J)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->e:J

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->b:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    if-ge v0, v1, :cond_3

    iget-object v1, p0, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->b:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    :goto_1
    if-ltz v2, :cond_2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;

    iget-object v3, v3, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;->g:Lmiuix/appcompat/app/j;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;

    iget v4, v4, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;->a:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;

    iget v5, v5, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;->h:I

    invoke-virtual {p0, v5}, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->D(I)I

    move-result v5

    if-eqz v3, :cond_1

    add-int/lit8 v5, v5, -0x1

    if-eq v4, v5, :cond_1

    invoke-virtual {v3}, Lmiuix/appcompat/app/j;->y0()V

    :cond_1
    add-int/lit8 v2, v2, -0x1

    goto :goto_1

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    return-void
.end method

.method private v()V
    .locals 6

    iget-wide v0, p0, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->e:J

    invoke-direct {p0, v0, v1}, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->M(J)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->e:J

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->b:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    if-ge v0, v1, :cond_3

    iget-object v1, p0, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->b:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    :goto_1
    if-ltz v2, :cond_2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;

    iget-object v3, v3, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;->g:Lmiuix/appcompat/app/j;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;

    iget v4, v4, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;->a:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;

    iget v5, v5, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;->h:I

    invoke-virtual {p0, v5}, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->D(I)I

    move-result v5

    if-eqz v3, :cond_1

    add-int/lit8 v5, v5, -0x1

    if-eq v4, v5, :cond_1

    invoke-virtual {v3}, Lmiuix/appcompat/app/j;->y0()V

    :cond_1
    add-int/lit8 v2, v2, -0x1

    goto :goto_1

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    return-void
.end method

.method private y(ILjava/lang/String;)Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;
    .locals 2

    iget-object v0, p0, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/ArrayList;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;

    iget-object v1, v0, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;->i:Ljava/lang/String;

    invoke-static {v1, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    return-object v0

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method


# virtual methods
.method A(Ljava/lang/Object;I)Ljava/lang/String;
    .locals 1

    goto/32 :goto_4

    nop

    :goto_0
    return-object p1

    :goto_1
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_3

    nop

    :goto_2
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_7

    nop

    :goto_3
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result p1

    goto/32 :goto_6

    nop

    :goto_4
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_1

    nop

    :goto_5
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_0

    nop

    :goto_6
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/32 :goto_8

    nop

    :goto_7
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/32 :goto_5

    nop

    :goto_8
    const-string p1, ":"

    goto/32 :goto_2

    nop
.end method

.method C()Landroid/view/View;
    .locals 1

    goto/32 :goto_3

    nop

    :goto_0
    if-eqz v0, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_6

    nop

    :goto_1
    goto :goto_5

    :goto_2
    goto/32 :goto_8

    nop

    :goto_3
    iget-object v0, p0, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->h:Ljava/lang/ref/WeakReference;

    goto/32 :goto_0

    nop

    :goto_4
    check-cast v0, Landroid/view/View;

    :goto_5
    goto/32 :goto_7

    nop

    :goto_6
    const/4 v0, 0x0

    goto/32 :goto_1

    nop

    :goto_7
    return-object v0

    :goto_8
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_4

    nop
.end method

.method D(I)I
    .locals 3

    goto/32 :goto_a

    nop

    :goto_0
    check-cast p1, Ljava/util/ArrayList;

    goto/32 :goto_18

    nop

    :goto_1
    const/4 v0, 0x0

    :goto_2
    goto/32 :goto_19

    nop

    :goto_3
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_4
    goto/32 :goto_17

    nop

    :goto_5
    return v0

    :goto_6
    if-nez v0, :cond_0

    goto/32 :goto_9

    :cond_0
    goto/32 :goto_1a

    nop

    :goto_7
    const-string v1, "key_task_id"

    goto/32 :goto_12

    nop

    :goto_8
    goto :goto_2

    :goto_9
    goto/32 :goto_1

    nop

    :goto_a
    new-instance v0, Landroid/os/Bundle;

    goto/32 :goto_d

    nop

    :goto_b
    if-gt v2, v0, :cond_1

    goto/32 :goto_4

    :cond_1
    goto/32 :goto_1b

    nop

    :goto_c
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_16

    nop

    :goto_d
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    goto/32 :goto_7

    nop

    :goto_e
    const/4 v1, 0x6

    goto/32 :goto_11

    nop

    :goto_f
    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object p1

    goto/32 :goto_0

    nop

    :goto_10
    add-int/lit8 v2, v1, 0x1

    goto/32 :goto_b

    nop

    :goto_11
    invoke-direct {p0, v1, v0}, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->S(ILandroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    goto/32 :goto_6

    nop

    :goto_12
    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto/32 :goto_e

    nop

    :goto_13
    iget v1, v1, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;->a:I

    goto/32 :goto_10

    nop

    :goto_14
    goto :goto_4

    :goto_15
    goto/32 :goto_5

    nop

    :goto_16
    check-cast v1, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;

    goto/32 :goto_13

    nop

    :goto_17
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_1d

    nop

    :goto_18
    if-nez p1, :cond_2

    goto/32 :goto_15

    :cond_2
    goto/32 :goto_3

    nop

    :goto_19
    iget-object v1, p0, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->b:Landroid/util/SparseArray;

    goto/32 :goto_f

    nop

    :goto_1a
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_1c

    nop

    :goto_1b
    add-int/lit8 v0, v1, 0x1

    goto/32 :goto_14

    nop

    :goto_1c
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    goto/32 :goto_8

    nop

    :goto_1d
    if-nez v1, :cond_3

    goto/32 :goto_15

    :cond_3
    goto/32 :goto_c

    nop
.end method

.method J(ILjava/lang/String;)Z
    .locals 3

    goto/32 :goto_0

    nop

    :goto_0
    invoke-direct {p0, p1, p2}, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->y(ILjava/lang/String;)Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;

    move-result-object p2

    goto/32 :goto_14

    nop

    :goto_1
    if-eqz p2, :cond_0

    goto/32 :goto_f

    :cond_0
    goto/32 :goto_e

    nop

    :goto_2
    new-instance v1, Landroid/os/Bundle;

    goto/32 :goto_b

    nop

    :goto_3
    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p2

    goto/32 :goto_13

    nop

    :goto_4
    invoke-direct {p0, p1, v1}, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->S(ILandroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object p1

    goto/32 :goto_7

    nop

    :goto_5
    if-nez p1, :cond_1

    goto/32 :goto_11

    :cond_1
    goto/32 :goto_10

    nop

    :goto_6
    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result p1

    goto/32 :goto_5

    nop

    :goto_7
    if-nez p1, :cond_2

    goto/32 :goto_11

    :cond_2
    goto/32 :goto_c

    nop

    :goto_8
    invoke-virtual {v1, p2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto/32 :goto_d

    nop

    :goto_9
    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/32 :goto_16

    nop

    :goto_a
    iget-object p2, p2, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;->c:Lmiuix/appcompat/app/floatingactivity/multiapp/c$f;

    goto/32 :goto_15

    nop

    :goto_b
    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    goto/32 :goto_a

    nop

    :goto_c
    const-string p2, "check_finishing"

    goto/32 :goto_6

    nop

    :goto_d
    const/16 p1, 0x9

    goto/32 :goto_4

    nop

    :goto_e
    return v0

    :goto_f
    goto/32 :goto_2

    nop

    :goto_10
    const/4 v0, 0x1

    :goto_11
    goto/32 :goto_12

    nop

    :goto_12
    return v0

    :goto_13
    const-string v2, "key_request_identity"

    goto/32 :goto_9

    nop

    :goto_14
    const/4 v0, 0x0

    goto/32 :goto_1

    nop

    :goto_15
    invoke-virtual {p2}, Ljava/lang/Object;->hashCode()I

    move-result p2

    goto/32 :goto_3

    nop

    :goto_16
    const-string p2, "key_task_id"

    goto/32 :goto_8

    nop
.end method

.method public K(ILjava/lang/String;)Z
    .locals 0

    invoke-direct {p0, p1, p2}, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->y(ILjava/lang/String;)Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-boolean p1, p1, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;->j:Z

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method O()Z
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->d:Lmiuix/appcompat/app/floatingactivity/multiapp/a;

    goto/32 :goto_7

    nop

    :goto_1
    return v0

    :goto_2
    const/4 v0, 0x0

    :goto_3
    goto/32 :goto_1

    nop

    :goto_4
    const/4 v0, 0x1

    goto/32 :goto_5

    nop

    :goto_5
    goto :goto_3

    :goto_6
    goto/32 :goto_2

    nop

    :goto_7
    if-nez v0, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_4

    nop
.end method

.method P(ILjava/lang/String;)V
    .locals 0

    goto/32 :goto_2

    nop

    :goto_0
    const/4 p2, 0x1

    goto/32 :goto_3

    nop

    :goto_1
    if-nez p1, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_0

    nop

    :goto_2
    invoke-direct {p0, p1, p2}, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->y(ILjava/lang/String;)Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;

    move-result-object p1

    goto/32 :goto_1

    nop

    :goto_3
    iput-boolean p2, p1, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;->j:Z

    :goto_4
    goto/32 :goto_5

    nop

    :goto_5
    return-void
.end method

.method Q(ILjava/lang/String;)V
    .locals 1

    goto/32 :goto_b

    nop

    :goto_0
    invoke-interface {p2}, Ljava/lang/Runnable;->run()V

    goto/32 :goto_9

    nop

    :goto_1
    return-void

    :goto_2
    invoke-virtual {p0}, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->O()Z

    move-result v0

    goto/32 :goto_e

    nop

    :goto_3
    new-instance p2, Lmiuix/appcompat/app/floatingactivity/multiapp/c$b;

    goto/32 :goto_6

    nop

    :goto_4
    return-void

    :goto_5
    goto/32 :goto_3

    nop

    :goto_6
    invoke-direct {p2, p0, p1}, Lmiuix/appcompat/app/floatingactivity/multiapp/c$b;-><init>(Lmiuix/appcompat/app/floatingactivity/multiapp/c;Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;)V

    goto/32 :goto_2

    nop

    :goto_7
    iget-object p1, p1, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;->f:Ljava/util/List;

    goto/32 :goto_c

    nop

    :goto_8
    if-eqz p1, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_4

    nop

    :goto_9
    goto :goto_d

    :goto_a
    goto/32 :goto_7

    nop

    :goto_b
    invoke-direct {p0, p1, p2}, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->y(ILjava/lang/String;)Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;

    move-result-object p1

    goto/32 :goto_8

    nop

    :goto_c
    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_d
    goto/32 :goto_1

    nop

    :goto_e
    if-nez v0, :cond_1

    goto/32 :goto_a

    :cond_1
    goto/32 :goto_0

    nop
.end method

.method V(ILjava/lang/String;Ljava/lang/Runnable;)V
    .locals 2

    goto/32 :goto_16

    nop

    :goto_0
    return-void

    :goto_1
    goto/32 :goto_a

    nop

    :goto_2
    if-nez v0, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_0

    nop

    :goto_3
    invoke-virtual {p0, p1}, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->D(I)I

    move-result v0

    goto/32 :goto_6

    nop

    :goto_4
    if-nez v0, :cond_1

    goto/32 :goto_d

    :cond_1
    goto/32 :goto_14

    nop

    :goto_5
    if-le v0, v1, :cond_2

    goto/32 :goto_7

    :cond_2
    goto/32 :goto_3

    nop

    :goto_6
    if-gt v0, v1, :cond_3

    goto/32 :goto_10

    :cond_3
    :goto_7
    goto/32 :goto_f

    nop

    :goto_8
    return-void

    :goto_9
    const/4 v1, 0x1

    goto/32 :goto_5

    nop

    :goto_a
    invoke-virtual {p0, p1}, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->z(I)I

    move-result v0

    goto/32 :goto_9

    nop

    :goto_b
    invoke-direct {p0, p1, p2}, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->y(ILjava/lang/String;)Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;

    move-result-object p1

    goto/32 :goto_13

    nop

    :goto_c
    goto :goto_12

    :goto_d
    goto/32 :goto_b

    nop

    :goto_e
    iget-object p1, p1, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;->f:Ljava/util/List;

    goto/32 :goto_11

    nop

    :goto_f
    invoke-virtual {p0, p1, p2}, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->P(ILjava/lang/String;)V

    :goto_10
    goto/32 :goto_15

    nop

    :goto_11
    invoke-interface {p1, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_12
    goto/32 :goto_8

    nop

    :goto_13
    if-nez p1, :cond_4

    goto/32 :goto_12

    :cond_4
    goto/32 :goto_e

    nop

    :goto_14
    invoke-interface {p3}, Ljava/lang/Runnable;->run()V

    goto/32 :goto_c

    nop

    :goto_15
    invoke-virtual {p0}, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->O()Z

    move-result v0

    goto/32 :goto_4

    nop

    :goto_16
    invoke-virtual {p0, p1, p2}, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->K(ILjava/lang/String;)Z

    move-result v0

    goto/32 :goto_2

    nop
.end method

.method X(ILjava/lang/String;)V
    .locals 2

    goto/32 :goto_10

    nop

    :goto_0
    invoke-virtual {p1}, Landroid/util/SparseArray;->size()I

    move-result p1

    goto/32 :goto_a

    nop

    :goto_1
    return-void

    :goto_2
    if-nez v0, :cond_0

    goto/32 :goto_9

    :cond_0
    goto/32 :goto_b

    nop

    :goto_3
    if-nez p2, :cond_1

    goto/32 :goto_16

    :cond_1
    goto/32 :goto_5

    nop

    :goto_4
    invoke-virtual {p2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result p2

    goto/32 :goto_3

    nop

    :goto_5
    iget-object p2, p0, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->b:Landroid/util/SparseArray;

    goto/32 :goto_15

    nop

    :goto_6
    if-nez v1, :cond_2

    goto/32 :goto_9

    :cond_2
    goto/32 :goto_11

    nop

    :goto_7
    iget-object p1, p0, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->b:Landroid/util/SparseArray;

    goto/32 :goto_0

    nop

    :goto_8
    invoke-virtual {p0}, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->t()V

    :goto_9
    goto/32 :goto_1

    nop

    :goto_a
    if-eqz p1, :cond_3

    goto/32 :goto_9

    :cond_3
    goto/32 :goto_f

    nop

    :goto_b
    iget-object v1, v0, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;->g:Lmiuix/appcompat/app/j;

    goto/32 :goto_6

    nop

    :goto_c
    invoke-direct {p0, p1}, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->f0(Landroid/content/Context;)V

    goto/32 :goto_8

    nop

    :goto_d
    iget-object p2, p0, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->b:Landroid/util/SparseArray;

    goto/32 :goto_e

    nop

    :goto_e
    invoke-virtual {p2, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object p2

    goto/32 :goto_12

    nop

    :goto_f
    iget-object p1, v0, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;->g:Lmiuix/appcompat/app/j;

    goto/32 :goto_c

    nop

    :goto_10
    invoke-direct {p0, p1, p2}, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->y(ILjava/lang/String;)Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;

    move-result-object v0

    goto/32 :goto_2

    nop

    :goto_11
    invoke-direct {p0, p1, p2}, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->d0(ILjava/lang/String;)V

    goto/32 :goto_d

    nop

    :goto_12
    check-cast p2, Ljava/util/ArrayList;

    goto/32 :goto_13

    nop

    :goto_13
    if-nez p2, :cond_4

    goto/32 :goto_16

    :cond_4
    goto/32 :goto_14

    nop

    :goto_14
    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto/32 :goto_4

    nop

    :goto_15
    invoke-virtual {p2, p1}, Landroid/util/SparseArray;->remove(I)V

    :goto_16
    goto/32 :goto_7

    nop
.end method

.method Y(Landroid/graphics/Bitmap;ILjava/lang/String;)V
    .locals 7

    goto/32 :goto_3

    nop

    :goto_0
    move v4, p1

    goto/32 :goto_4

    nop

    :goto_1
    return-void

    :goto_2
    if-eqz p3, :cond_0

    goto/32 :goto_d

    :cond_0
    goto/32 :goto_c

    nop

    :goto_3
    if-eqz p1, :cond_1

    goto/32 :goto_17

    :cond_1
    goto/32 :goto_16

    nop

    :goto_4
    move v6, p2

    goto/32 :goto_9

    nop

    :goto_5
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result p1

    goto/32 :goto_15

    nop

    :goto_6
    invoke-virtual {p1, v0}, Landroid/graphics/Bitmap;->copyPixelsToBuffer(Ljava/nio/Buffer;)V

    goto/32 :goto_11

    nop

    :goto_7
    move v3, v4

    goto/32 :goto_0

    nop

    :goto_8
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getByteCount()I

    move-result v2

    goto/32 :goto_f

    nop

    :goto_9
    invoke-static/range {v0 .. v6}, Lm9/e;->c(Lmiuix/appcompat/app/floatingactivity/multiapp/a;[BIIILjava/lang/String;I)V

    goto/32 :goto_1

    nop

    :goto_a
    move-object v0, v1

    goto/32 :goto_13

    nop

    :goto_b
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    goto/32 :goto_5

    nop

    :goto_c
    return-void

    :goto_d
    goto/32 :goto_8

    nop

    :goto_e
    invoke-virtual {p3}, Ljava/lang/Object;->hashCode()I

    move-result p3

    goto/32 :goto_14

    nop

    :goto_f
    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    goto/32 :goto_6

    nop

    :goto_10
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v3

    goto/32 :goto_b

    nop

    :goto_11
    iget-object v1, p0, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->d:Lmiuix/appcompat/app/floatingactivity/multiapp/a;

    goto/32 :goto_10

    nop

    :goto_12
    invoke-direct {p0, p2, p3}, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->y(ILjava/lang/String;)Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;

    move-result-object p3

    goto/32 :goto_2

    nop

    :goto_13
    move-object v1, v3

    goto/32 :goto_7

    nop

    :goto_14
    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    goto/32 :goto_a

    nop

    :goto_15
    iget-object p3, p3, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;->c:Lmiuix/appcompat/app/floatingactivity/multiapp/c$f;

    goto/32 :goto_e

    nop

    :goto_16
    return-void

    :goto_17
    goto/32 :goto_12

    nop
.end method

.method a0(Landroid/view/View;)V
    .locals 1

    goto/32 :goto_3

    nop

    :goto_0
    iput-object v0, p0, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->h:Ljava/lang/ref/WeakReference;

    goto/32 :goto_1

    nop

    :goto_1
    return-void

    :goto_2
    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    goto/32 :goto_0

    nop

    :goto_3
    new-instance v0, Ljava/lang/ref/WeakReference;

    goto/32 :goto_2

    nop
.end method

.method g0(ILjava/lang/String;Z)V
    .locals 0

    goto/32 :goto_2

    nop

    :goto_0
    if-nez p1, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_3

    nop

    :goto_1
    return-void

    :goto_2
    invoke-direct {p0, p1, p2}, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->y(ILjava/lang/String;)Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;

    move-result-object p1

    goto/32 :goto_0

    nop

    :goto_3
    iput-boolean p3, p1, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;->b:Z

    :goto_4
    goto/32 :goto_1

    nop
.end method

.method r(ILjava/lang/String;)V
    .locals 2

    goto/32 :goto_7

    nop

    :goto_0
    iget p2, p1, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;->d:I

    goto/32 :goto_13

    nop

    :goto_1
    if-nez p1, :cond_0

    goto/32 :goto_9

    :cond_0
    goto/32 :goto_8

    nop

    :goto_2
    if-nez v0, :cond_1

    goto/32 :goto_4

    :cond_1
    goto/32 :goto_6

    nop

    :goto_3
    if-le v0, v1, :cond_2

    goto/32 :goto_c

    :cond_2
    :goto_4
    goto/32 :goto_11

    nop

    :goto_5
    const/4 v1, 0x1

    goto/32 :goto_2

    nop

    :goto_6
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto/32 :goto_3

    nop

    :goto_7
    iget-object v0, p0, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->b:Landroid/util/SparseArray;

    goto/32 :goto_f

    nop

    :goto_8
    invoke-virtual {p1}, Lmiuix/appcompat/app/j;->v0()V

    :goto_9
    goto/32 :goto_12

    nop

    :goto_a
    invoke-direct {p0, p1, p2}, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->y(ILjava/lang/String;)Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;

    move-result-object p1

    goto/32 :goto_e

    nop

    :goto_b
    if-gt v0, v1, :cond_3

    goto/32 :goto_9

    :cond_3
    :goto_c
    goto/32 :goto_a

    nop

    :goto_d
    check-cast v0, Ljava/util/ArrayList;

    goto/32 :goto_5

    nop

    :goto_e
    if-nez p1, :cond_4

    goto/32 :goto_9

    :cond_4
    goto/32 :goto_0

    nop

    :goto_f
    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_d

    nop

    :goto_10
    iget-object p1, p1, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;->g:Lmiuix/appcompat/app/j;

    goto/32 :goto_1

    nop

    :goto_11
    invoke-virtual {p0, p1}, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->D(I)I

    move-result v0

    goto/32 :goto_b

    nop

    :goto_12
    return-void

    :goto_13
    if-gtz p2, :cond_5

    goto/32 :goto_9

    :cond_5
    goto/32 :goto_10

    nop
.end method

.method public t()V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->b:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    const/4 v0, 0x0

    iput-object v0, p0, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->h:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method w()V
    .locals 1

    goto/32 :goto_2

    nop

    :goto_0
    const/4 v0, 0x0

    goto/32 :goto_5

    nop

    :goto_1
    return-void

    :goto_2
    iget-object v0, p0, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->b:Landroid/util/SparseArray;

    goto/32 :goto_4

    nop

    :goto_3
    if-eqz v0, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_0

    nop

    :goto_4
    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    goto/32 :goto_3

    nop

    :goto_5
    sput-object v0, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->k:Lmiuix/appcompat/app/floatingactivity/multiapp/c;

    :goto_6
    goto/32 :goto_1

    nop
.end method

.method x(ILjava/lang/String;)Lmiuix/appcompat/app/j;
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    invoke-direct {p0, p1, p2}, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->y(ILjava/lang/String;)Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;

    move-result-object p1

    goto/32 :goto_1

    nop

    :goto_1
    if-nez p1, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_5

    nop

    :goto_2
    return-object p1

    :goto_3
    return-object p1

    :goto_4
    goto/32 :goto_6

    nop

    :goto_5
    iget-object p1, p1, Lmiuix/appcompat/app/floatingactivity/multiapp/c$c;->g:Lmiuix/appcompat/app/j;

    goto/32 :goto_3

    nop

    :goto_6
    const/4 p1, 0x0

    goto/32 :goto_2

    nop
.end method

.method z(I)I
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result p1

    goto/32 :goto_2

    nop

    :goto_1
    iget-object v0, p0, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->b:Landroid/util/SparseArray;

    goto/32 :goto_6

    nop

    :goto_2
    return p1

    :goto_3
    goto/32 :goto_7

    nop

    :goto_4
    if-nez p1, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_0

    nop

    :goto_5
    return p1

    :goto_6
    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object p1

    goto/32 :goto_8

    nop

    :goto_7
    const/4 p1, 0x0

    goto/32 :goto_5

    nop

    :goto_8
    check-cast p1, Ljava/util/ArrayList;

    goto/32 :goto_4

    nop
.end method
