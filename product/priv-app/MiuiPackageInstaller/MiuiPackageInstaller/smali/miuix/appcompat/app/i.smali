.class public Lmiuix/appcompat/app/i;
.super Ld/f;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiuix/appcompat/app/i$c;,
        Lmiuix/appcompat/app/i$d;,
        Lmiuix/appcompat/app/i$b;
    }
.end annotation


# instance fields
.field final c:Lmiuix/appcompat/app/AlertController;

.field private d:Lw9/a$a;


# direct methods
.method protected constructor <init>(Landroid/content/Context;I)V
    .locals 1

    invoke-static {p1, p2}, Lmiuix/appcompat/app/i;->o(Landroid/content/Context;I)I

    move-result p2

    invoke-direct {p0, p1, p2}, Ld/f;-><init>(Landroid/content/Context;I)V

    new-instance p2, Lmiuix/appcompat/app/h;

    invoke-direct {p2, p0}, Lmiuix/appcompat/app/h;-><init>(Lmiuix/appcompat/app/i;)V

    iput-object p2, p0, Lmiuix/appcompat/app/i;->d:Lw9/a$a;

    new-instance p2, Lmiuix/appcompat/app/AlertController;

    invoke-direct {p0, p1}, Lmiuix/appcompat/app/i;->m(Landroid/content/Context;)Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-direct {p2, p1, p0, v0}, Lmiuix/appcompat/app/AlertController;-><init>(Landroid/content/Context;Ld/f;Landroid/view/Window;)V

    iput-object p2, p0, Lmiuix/appcompat/app/i;->c:Lmiuix/appcompat/app/AlertController;

    return-void
.end method

.method public static synthetic e(Lmiuix/appcompat/app/i;)V
    .locals 0

    invoke-direct {p0}, Lmiuix/appcompat/app/i;->k()V

    return-void
.end method

.method static synthetic g(Lmiuix/appcompat/app/i;)Lw9/a$a;
    .locals 0

    iget-object p0, p0, Lmiuix/appcompat/app/i;->d:Lw9/a$a;

    return-object p0
.end method

.method private synthetic k()V
    .locals 1

    invoke-virtual {p0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->isAttachedToWindow()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lmiuix/appcompat/app/i;->n()V

    :cond_0
    return-void
.end method

.method private m(Landroid/content/Context;)Landroid/content/Context;
    .locals 2

    if-nez p1, :cond_0

    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object p1

    return-object p1

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Landroid/view/ContextThemeWrapper;

    if-ne v0, v1, :cond_1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method static o(Landroid/content/Context;I)I
    .locals 2

    ushr-int/lit8 v0, p1, 0x18

    and-int/lit16 v0, v0, 0xff

    const/4 v1, 0x1

    if-lt v0, v1, :cond_0

    return p1

    :cond_0
    new-instance p1, Landroid/util/TypedValue;

    invoke-direct {p1}, Landroid/util/TypedValue;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object p0

    sget v0, Lk9/b;->u:I

    invoke-virtual {p0, v0, p1, v1}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    iget p0, p1, Landroid/util/TypedValue;->resourceId:I

    return p0
.end method


# virtual methods
.method public dismiss()V
    .locals 3

    iget-object v0, p0, Lmiuix/appcompat/app/i;->c:Lmiuix/appcompat/app/AlertController;

    invoke-virtual {v0}, Lmiuix/appcompat/app/AlertController;->X()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lmiuix/appcompat/app/i;->h()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/view/View;->getHandler()Landroid/os/Handler;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v0}, Landroid/view/View;->getHandler()Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v2

    if-ne v1, v2, :cond_1

    iget-object v0, p0, Lmiuix/appcompat/app/i;->c:Lmiuix/appcompat/app/AlertController;

    iget-object v1, p0, Lmiuix/appcompat/app/i;->d:Lw9/a$a;

    invoke-virtual {v0, v1}, Lmiuix/appcompat/app/AlertController;->H(Lw9/a$a;)V

    goto :goto_1

    :cond_1
    new-instance v1, Lmiuix/appcompat/app/i$a;

    invoke-direct {v1, p0}, Lmiuix/appcompat/app/i$a;-><init>(Lmiuix/appcompat/app/i;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    goto :goto_1

    :cond_2
    :goto_0
    invoke-super {p0}, Ld/f;->dismiss()V

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->isAttachedToWindow()Z

    move-result v0

    if-nez v0, :cond_4

    return-void

    :cond_4
    invoke-virtual {p0}, Lmiuix/appcompat/app/i;->n()V

    :goto_1
    return-void
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/i;->c:Lmiuix/appcompat/app/AlertController;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/app/AlertController;->I(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    invoke-super {p0, p1}, Ld/f;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result p1

    return p1
.end method

.method h()Landroid/app/Activity;
    .locals 3

    goto/32 :goto_11

    nop

    :goto_0
    if-eqz v0, :cond_0

    goto/32 :goto_e

    :cond_0
    goto/32 :goto_10

    nop

    :goto_1
    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v1

    :goto_2
    goto/32 :goto_0

    nop

    :goto_3
    goto :goto_2

    :goto_4
    goto/32 :goto_c

    nop

    :goto_5
    check-cast v1, Landroid/content/ContextWrapper;

    goto/32 :goto_12

    nop

    :goto_6
    move-object v0, v1

    goto/32 :goto_b

    nop

    :goto_7
    instance-of v2, v1, Landroid/app/Activity;

    goto/32 :goto_9

    nop

    :goto_8
    if-nez v2, :cond_1

    goto/32 :goto_14

    :cond_1
    goto/32 :goto_5

    nop

    :goto_9
    if-nez v2, :cond_2

    goto/32 :goto_4

    :cond_2
    goto/32 :goto_6

    nop

    :goto_a
    const/4 v1, 0x0

    goto/32 :goto_d

    nop

    :goto_b
    check-cast v0, Landroid/app/Activity;

    goto/32 :goto_3

    nop

    :goto_c
    instance-of v2, v1, Landroid/content/ContextWrapper;

    goto/32 :goto_8

    nop

    :goto_d
    goto :goto_2

    :goto_e
    goto/32 :goto_f

    nop

    :goto_f
    return-object v0

    :goto_10
    if-nez v1, :cond_3

    goto/32 :goto_e

    :cond_3
    goto/32 :goto_7

    nop

    :goto_11
    invoke-virtual {p0}, Landroid/app/Dialog;->getOwnerActivity()Landroid/app/Activity;

    move-result-object v0

    goto/32 :goto_1

    nop

    :goto_12
    invoke-virtual {v1}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    goto/32 :goto_13

    nop

    :goto_13
    goto :goto_2

    :goto_14
    goto/32 :goto_a

    nop
.end method

.method public i()Landroid/widget/ListView;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/i;->c:Lmiuix/appcompat/app/AlertController;

    invoke-virtual {v0}, Lmiuix/appcompat/app/AlertController;->O()Landroid/widget/ListView;

    move-result-object v0

    return-object v0
.end method

.method protected l()V
    .locals 0

    return-void
.end method

.method n()V
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    invoke-super {p0}, Ld/f;->dismiss()V

    goto/32 :goto_1

    nop

    :goto_1
    return-void
.end method

.method public onAttachedToWindow()V
    .locals 3

    invoke-super {p0}, Landroid/app/Dialog;->onAttachedToWindow()V

    invoke-virtual {p0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lmiuix/appcompat/app/i;->c:Lmiuix/appcompat/app/AlertController;

    iget-boolean v1, v1, Lmiuix/appcompat/app/AlertController;->Z:Z

    if-eqz v1, :cond_0

    sget v1, Lmiuix/view/c;->E:I

    sget v2, Lmiuix/view/c;->n:I

    invoke-static {v0, v1, v2}, Lmiuix/view/HapticCompat;->performHapticFeedbackAsync(Landroid/view/View;II)V

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/app/i;->c:Lmiuix/appcompat/app/AlertController;

    invoke-virtual {v0}, Lmiuix/appcompat/app/AlertController;->g0()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    iget-object v0, p0, Lmiuix/appcompat/app/i;->c:Lmiuix/appcompat/app/AlertController;

    invoke-virtual {v0}, Lmiuix/appcompat/app/AlertController;->X()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lmiuix/appcompat/app/i;->c:Lmiuix/appcompat/app/AlertController;

    iget-boolean v0, v0, Lmiuix/appcompat/app/AlertController;->f:Z

    if-nez v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/Window;->setWindowAnimations(I)V

    :cond_1
    invoke-super {p0, p1}, Ld/f;->onCreate(Landroid/os/Bundle;)V

    iget-object v0, p0, Lmiuix/appcompat/app/i;->c:Lmiuix/appcompat/app/AlertController;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/app/AlertController;->T(Landroid/os/Bundle;)V

    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 1

    invoke-super {p0}, Landroid/app/Dialog;->onDetachedFromWindow()V

    iget-object v0, p0, Lmiuix/appcompat/app/i;->c:Lmiuix/appcompat/app/AlertController;

    invoke-virtual {v0}, Lmiuix/appcompat/app/AlertController;->i0()V

    return-void
.end method

.method protected onStart()V
    .locals 1

    invoke-super {p0}, Landroid/app/Dialog;->onStart()V

    iget-object v0, p0, Lmiuix/appcompat/app/i;->c:Lmiuix/appcompat/app/AlertController;

    invoke-virtual {v0}, Lmiuix/appcompat/app/AlertController;->k0()V

    return-void
.end method

.method protected onStop()V
    .locals 1

    invoke-super {p0}, Ld/f;->onStop()V

    iget-object v0, p0, Lmiuix/appcompat/app/i;->c:Lmiuix/appcompat/app/AlertController;

    invoke-virtual {v0}, Lmiuix/appcompat/app/AlertController;->l0()V

    return-void
.end method

.method public p(Lmiuix/appcompat/app/i$d;)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/i;->c:Lmiuix/appcompat/app/AlertController;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/app/AlertController;->C0(Lmiuix/appcompat/app/i$d;)V

    return-void
.end method

.method public setCancelable(Z)V
    .locals 1

    invoke-super {p0, p1}, Landroid/app/Dialog;->setCancelable(Z)V

    iget-object v0, p0, Lmiuix/appcompat/app/i;->c:Lmiuix/appcompat/app/AlertController;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/app/AlertController;->q0(Z)V

    return-void
.end method

.method public setCanceledOnTouchOutside(Z)V
    .locals 1

    invoke-super {p0, p1}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    iget-object v0, p0, Lmiuix/appcompat/app/i;->c:Lmiuix/appcompat/app/AlertController;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/app/AlertController;->r0(Z)V

    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1

    invoke-super {p0, p1}, Ld/f;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lmiuix/appcompat/app/i;->c:Lmiuix/appcompat/app/AlertController;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/app/AlertController;->D0(Ljava/lang/CharSequence;)V

    return-void
.end method
