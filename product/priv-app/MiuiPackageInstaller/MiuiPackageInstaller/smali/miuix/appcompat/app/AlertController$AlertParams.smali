.class Lmiuix/appcompat/app/AlertController$AlertParams;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiuix/appcompat/app/AlertController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "AlertParams"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiuix/appcompat/app/AlertController$AlertParams$OnPrepareListViewListener;
    }
.end annotation


# instance fields
.field mAdapter:Landroid/widget/ListAdapter;

.field mCancelable:Z

.field mCheckBoxMessage:Ljava/lang/CharSequence;

.field mCheckedItem:I

.field mCheckedItems:[Z

.field mComment:Ljava/lang/CharSequence;

.field final mContext:Landroid/content/Context;

.field mCursor:Landroid/database/Cursor;

.field mCustomTitleView:Landroid/view/View;

.field mEnableDialogImmersive:Z

.field mEnableEnterAnim:Z

.field mExtraButtonList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lmiuix/appcompat/app/AlertController$ButtonInfo;",
            ">;"
        }
    .end annotation
.end field

.field mHapticFeedbackEnabled:Z

.field mIcon:Landroid/graphics/drawable/Drawable;

.field mIconAttrId:I

.field mIconId:I

.field final mInflater:Landroid/view/LayoutInflater;

.field mIsChecked:Z

.field mIsCheckedColumn:Ljava/lang/String;

.field mIsMultiChoice:Z

.field mIsSingleChoice:Z

.field mItems:[Ljava/lang/CharSequence;

.field mLabelColumn:Ljava/lang/String;

.field mLiteVersion:I

.field mMessage:Ljava/lang/CharSequence;

.field mNegativeButtonListener:Landroid/content/DialogInterface$OnClickListener;

.field mNegativeButtonText:Ljava/lang/CharSequence;

.field mNeutralButtonListener:Landroid/content/DialogInterface$OnClickListener;

.field mNeutralButtonText:Ljava/lang/CharSequence;

.field mOnCancelListener:Landroid/content/DialogInterface$OnCancelListener;

.field mOnCheckboxClickListener:Landroid/content/DialogInterface$OnMultiChoiceClickListener;

.field mOnClickListener:Landroid/content/DialogInterface$OnClickListener;

.field mOnDialogShowAnimListener:Lmiuix/appcompat/app/i$d;

.field mOnDismissListener:Landroid/content/DialogInterface$OnDismissListener;

.field mOnItemSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

.field mOnKeyListener:Landroid/content/DialogInterface$OnKeyListener;

.field mOnPrepareListViewListener:Lmiuix/appcompat/app/AlertController$AlertParams$OnPrepareListViewListener;

.field mOnShowListener:Landroid/content/DialogInterface$OnShowListener;

.field mPositiveButtonListener:Landroid/content/DialogInterface$OnClickListener;

.field mPositiveButtonText:Ljava/lang/CharSequence;

.field mPreferLandscape:Z

.field mTitle:Ljava/lang/CharSequence;

.field mView:Landroid/view/View;

.field mViewLayoutResId:I


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lmiuix/appcompat/app/AlertController$AlertParams;->mIconId:I

    iput v0, p0, Lmiuix/appcompat/app/AlertController$AlertParams;->mIconAttrId:I

    const/4 v1, -0x1

    iput v1, p0, Lmiuix/appcompat/app/AlertController$AlertParams;->mCheckedItem:I

    iput-object p1, p0, Lmiuix/appcompat/app/AlertController$AlertParams;->mContext:Landroid/content/Context;

    const/4 v1, 0x1

    iput-boolean v1, p0, Lmiuix/appcompat/app/AlertController$AlertParams;->mCancelable:Z

    invoke-static {}, Lea/a;->E()Z

    move-result v2

    xor-int/2addr v2, v1

    iput-boolean v2, p0, Lmiuix/appcompat/app/AlertController$AlertParams;->mEnableDialogImmersive:Z

    invoke-static {}, Lea/a;->t()I

    move-result v2

    iput v2, p0, Lmiuix/appcompat/app/AlertController$AlertParams;->mLiteVersion:I

    if-gez v2, :cond_0

    iput v0, p0, Lmiuix/appcompat/app/AlertController$AlertParams;->mLiteVersion:I

    :cond_0
    iput-boolean v1, p0, Lmiuix/appcompat/app/AlertController$AlertParams;->mEnableEnterAnim:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiuix/appcompat/app/AlertController$AlertParams;->mExtraButtonList:Ljava/util/List;

    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/LayoutInflater;

    iput-object p1, p0, Lmiuix/appcompat/app/AlertController$AlertParams;->mInflater:Landroid/view/LayoutInflater;

    return-void
.end method

.method private createListView(Lmiuix/appcompat/app/AlertController;)V
    .locals 11

    iget-object v0, p0, Lmiuix/appcompat/app/AlertController$AlertParams;->mInflater:Landroid/view/LayoutInflater;

    iget v1, p1, Lmiuix/appcompat/app/AlertController;->M:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Landroid/widget/ListView;

    iget-boolean v0, p0, Lmiuix/appcompat/app/AlertController$AlertParams;->mIsMultiChoice:Z

    const/4 v8, 0x1

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmiuix/appcompat/app/AlertController$AlertParams;->mCursor:Landroid/database/Cursor;

    if-nez v0, :cond_0

    new-instance v9, Lmiuix/appcompat/app/AlertController$AlertParams$1;

    iget-object v2, p0, Lmiuix/appcompat/app/AlertController$AlertParams;->mContext:Landroid/content/Context;

    iget v3, p1, Lmiuix/appcompat/app/AlertController;->N:I

    const v4, 0x1020014

    iget-object v5, p0, Lmiuix/appcompat/app/AlertController$AlertParams;->mItems:[Ljava/lang/CharSequence;

    move-object v0, v9

    move-object v1, p0

    move-object v6, v7

    invoke-direct/range {v0 .. v6}, Lmiuix/appcompat/app/AlertController$AlertParams$1;-><init>(Lmiuix/appcompat/app/AlertController$AlertParams;Landroid/content/Context;II[Ljava/lang/CharSequence;Landroid/widget/ListView;)V

    goto :goto_1

    :cond_0
    new-instance v9, Lmiuix/appcompat/app/AlertController$AlertParams$2;

    iget-object v2, p0, Lmiuix/appcompat/app/AlertController$AlertParams;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lmiuix/appcompat/app/AlertController$AlertParams;->mCursor:Landroid/database/Cursor;

    const/4 v4, 0x0

    move-object v0, v9

    move-object v1, p0

    move-object v5, v7

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lmiuix/appcompat/app/AlertController$AlertParams$2;-><init>(Lmiuix/appcompat/app/AlertController$AlertParams;Landroid/content/Context;Landroid/database/Cursor;ZLandroid/widget/ListView;Lmiuix/appcompat/app/AlertController;)V

    goto :goto_1

    :cond_1
    iget-boolean v0, p0, Lmiuix/appcompat/app/AlertController$AlertParams;->mIsSingleChoice:Z

    if-eqz v0, :cond_2

    iget v0, p1, Lmiuix/appcompat/app/AlertController;->O:I

    goto :goto_0

    :cond_2
    iget v0, p1, Lmiuix/appcompat/app/AlertController;->P:I

    :goto_0
    move v3, v0

    iget-object v0, p0, Lmiuix/appcompat/app/AlertController$AlertParams;->mCursor:Landroid/database/Cursor;

    const v1, 0x1020014

    if-eqz v0, :cond_3

    new-instance v9, Lmiuix/appcompat/app/AlertController$AlertParams$3;

    iget-object v2, p0, Lmiuix/appcompat/app/AlertController$AlertParams;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lmiuix/appcompat/app/AlertController$AlertParams;->mCursor:Landroid/database/Cursor;

    new-array v5, v8, [Ljava/lang/String;

    iget-object v0, p0, Lmiuix/appcompat/app/AlertController$AlertParams;->mLabelColumn:Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    new-array v10, v8, [I

    aput v1, v10, v6

    move-object v0, v9

    move-object v1, p0

    move-object v6, v10

    invoke-direct/range {v0 .. v6}, Lmiuix/appcompat/app/AlertController$AlertParams$3;-><init>(Lmiuix/appcompat/app/AlertController$AlertParams;Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[I)V

    goto :goto_1

    :cond_3
    iget-object v9, p0, Lmiuix/appcompat/app/AlertController$AlertParams;->mAdapter:Landroid/widget/ListAdapter;

    if-eqz v9, :cond_4

    goto :goto_1

    :cond_4
    new-instance v9, Lmiuix/appcompat/app/AlertController$CheckedItemAdapter;

    iget-object v0, p0, Lmiuix/appcompat/app/AlertController$AlertParams;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lmiuix/appcompat/app/AlertController$AlertParams;->mItems:[Ljava/lang/CharSequence;

    invoke-direct {v9, v0, v3, v1, v2}, Lmiuix/appcompat/app/AlertController$CheckedItemAdapter;-><init>(Landroid/content/Context;II[Ljava/lang/CharSequence;)V

    :goto_1
    iget-object v0, p0, Lmiuix/appcompat/app/AlertController$AlertParams;->mOnPrepareListViewListener:Lmiuix/appcompat/app/AlertController$AlertParams$OnPrepareListViewListener;

    if-eqz v0, :cond_5

    invoke-interface {v0, v7}, Lmiuix/appcompat/app/AlertController$AlertParams$OnPrepareListViewListener;->onPrepareListView(Landroid/widget/ListView;)V

    :cond_5
    iput-object v9, p1, Lmiuix/appcompat/app/AlertController;->J:Landroid/widget/ListAdapter;

    iget v0, p0, Lmiuix/appcompat/app/AlertController$AlertParams;->mCheckedItem:I

    iput v0, p1, Lmiuix/appcompat/app/AlertController;->K:I

    iget-object v0, p0, Lmiuix/appcompat/app/AlertController$AlertParams;->mOnClickListener:Landroid/content/DialogInterface$OnClickListener;

    if-eqz v0, :cond_6

    new-instance v0, Lmiuix/appcompat/app/AlertController$AlertParams$4;

    invoke-direct {v0, p0, p1}, Lmiuix/appcompat/app/AlertController$AlertParams$4;-><init>(Lmiuix/appcompat/app/AlertController$AlertParams;Lmiuix/appcompat/app/AlertController;)V

    :goto_2
    invoke-virtual {v7, v0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    goto :goto_3

    :cond_6
    iget-object v0, p0, Lmiuix/appcompat/app/AlertController$AlertParams;->mOnCheckboxClickListener:Landroid/content/DialogInterface$OnMultiChoiceClickListener;

    if-eqz v0, :cond_7

    new-instance v0, Lmiuix/appcompat/app/AlertController$AlertParams$5;

    invoke-direct {v0, p0, v7, p1}, Lmiuix/appcompat/app/AlertController$AlertParams$5;-><init>(Lmiuix/appcompat/app/AlertController$AlertParams;Landroid/widget/ListView;Lmiuix/appcompat/app/AlertController;)V

    goto :goto_2

    :cond_7
    :goto_3
    iget-object v0, p0, Lmiuix/appcompat/app/AlertController$AlertParams;->mOnItemSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    if-eqz v0, :cond_8

    invoke-virtual {v7, v0}, Landroid/widget/ListView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    :cond_8
    iget-boolean v0, p0, Lmiuix/appcompat/app/AlertController$AlertParams;->mIsSingleChoice:Z

    if-eqz v0, :cond_9

    invoke-virtual {v7, v8}, Landroid/widget/ListView;->setChoiceMode(I)V

    goto :goto_4

    :cond_9
    iget-boolean v0, p0, Lmiuix/appcompat/app/AlertController$AlertParams;->mIsMultiChoice:Z

    if-eqz v0, :cond_a

    const/4 v0, 0x2

    invoke-virtual {v7, v0}, Landroid/widget/ListView;->setChoiceMode(I)V

    :cond_a
    :goto_4
    iput-object v7, p1, Lmiuix/appcompat/app/AlertController;->j:Landroid/widget/ListView;

    return-void
.end method


# virtual methods
.method apply(Lmiuix/appcompat/app/AlertController;)V
    .locals 4

    goto/32 :goto_1f

    nop

    :goto_0
    const/4 v2, -0x1

    goto/32 :goto_49

    nop

    :goto_1
    invoke-virtual {p1, v0}, Lmiuix/appcompat/app/AlertController;->t0(Ljava/lang/CharSequence;)V

    :goto_2
    goto/32 :goto_39

    nop

    :goto_3
    iget-boolean v0, p0, Lmiuix/appcompat/app/AlertController$AlertParams;->mPreferLandscape:Z

    goto/32 :goto_17

    nop

    :goto_4
    goto/16 :goto_21

    :goto_5
    goto/32 :goto_14

    nop

    :goto_6
    invoke-virtual {p1, v0}, Lmiuix/appcompat/app/AlertController;->F0(Landroid/view/View;)V

    goto/32 :goto_4

    nop

    :goto_7
    if-nez v0, :cond_0

    goto/32 :goto_4c

    :cond_0
    goto/32 :goto_4b

    nop

    :goto_8
    iget v0, p0, Lmiuix/appcompat/app/AlertController$AlertParams;->mIconId:I

    goto/32 :goto_23

    nop

    :goto_9
    invoke-virtual {p1, v0}, Lmiuix/appcompat/app/AlertController;->u0(Landroid/view/View;)V

    goto/32 :goto_36

    nop

    :goto_a
    invoke-virtual {p1, v0}, Lmiuix/appcompat/app/AlertController;->w0(Z)V

    goto/32 :goto_42

    nop

    :goto_b
    iget-boolean v0, p0, Lmiuix/appcompat/app/AlertController$AlertParams;->mEnableEnterAnim:Z

    goto/32 :goto_4e

    nop

    :goto_c
    iget-object v0, p0, Lmiuix/appcompat/app/AlertController$AlertParams;->mAdapter:Landroid/widget/ListAdapter;

    goto/32 :goto_45

    nop

    :goto_d
    invoke-virtual {p1, v0}, Lmiuix/appcompat/app/AlertController;->z0(I)V

    goto/32 :goto_3

    nop

    :goto_e
    iput-boolean v0, p1, Lmiuix/appcompat/app/AlertController;->Z:Z

    goto/32 :goto_58

    nop

    :goto_f
    iget-object v0, p0, Lmiuix/appcompat/app/AlertController$AlertParams;->mCursor:Landroid/database/Cursor;

    goto/32 :goto_53

    nop

    :goto_10
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    goto/32 :goto_26

    nop

    :goto_11
    if-nez v0, :cond_1

    goto/32 :goto_16

    :cond_1
    goto/32 :goto_0

    nop

    :goto_12
    if-nez v0, :cond_2

    goto/32 :goto_37

    :cond_2
    goto/32 :goto_9

    nop

    :goto_13
    if-nez v0, :cond_3

    goto/32 :goto_34

    :cond_3
    goto/32 :goto_22

    nop

    :goto_14
    iget v0, p0, Lmiuix/appcompat/app/AlertController$AlertParams;->mViewLayoutResId:I

    goto/32 :goto_3d

    nop

    :goto_15
    invoke-virtual {p1, v2, v0, v3, v1}, Lmiuix/appcompat/app/AlertController;->p0(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;Landroid/os/Message;)V

    :goto_16
    goto/32 :goto_4f

    nop

    :goto_17
    invoke-virtual {p1, v0}, Lmiuix/appcompat/app/AlertController;->B0(Z)V

    goto/32 :goto_b

    nop

    :goto_18
    iget-object v3, p0, Lmiuix/appcompat/app/AlertController$AlertParams;->mNeutralButtonListener:Landroid/content/DialogInterface$OnClickListener;

    goto/32 :goto_28

    nop

    :goto_19
    invoke-virtual {p1, v0}, Lmiuix/appcompat/app/AlertController;->N(I)I

    move-result v0

    goto/32 :goto_51

    nop

    :goto_1a
    iget-object v1, p0, Lmiuix/appcompat/app/AlertController$AlertParams;->mExtraButtonList:Ljava/util/List;

    goto/32 :goto_10

    nop

    :goto_1b
    iget-object v0, p0, Lmiuix/appcompat/app/AlertController$AlertParams;->mTitle:Ljava/lang/CharSequence;

    goto/32 :goto_7

    nop

    :goto_1c
    invoke-virtual {p1, v2, v0, v3, v1}, Lmiuix/appcompat/app/AlertController;->p0(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;Landroid/os/Message;)V

    :goto_1d
    goto/32 :goto_59

    nop

    :goto_1e
    return-void

    :goto_1f
    iget-object v0, p0, Lmiuix/appcompat/app/AlertController$AlertParams;->mCustomTitleView:Landroid/view/View;

    goto/32 :goto_12

    nop

    :goto_20
    invoke-virtual {p1, v0}, Lmiuix/appcompat/app/AlertController;->E0(I)V

    :goto_21
    goto/32 :goto_3c

    nop

    :goto_22
    iget-boolean v1, p0, Lmiuix/appcompat/app/AlertController$AlertParams;->mIsChecked:Z

    goto/32 :goto_33

    nop

    :goto_23
    if-nez v0, :cond_4

    goto/32 :goto_48

    :cond_4
    goto/32 :goto_47

    nop

    :goto_24
    if-nez v0, :cond_5

    goto/32 :goto_27

    :cond_5
    goto/32 :goto_2a

    nop

    :goto_25
    iget-object v0, p0, Lmiuix/appcompat/app/AlertController$AlertParams;->mMessage:Ljava/lang/CharSequence;

    goto/32 :goto_3e

    nop

    :goto_26
    invoke-static {p1, v0}, Lmiuix/appcompat/app/AlertController;->w(Lmiuix/appcompat/app/AlertController;Ljava/util/List;)Ljava/util/List;

    :goto_27
    goto/32 :goto_4d

    nop

    :goto_28
    invoke-virtual {p1, v2, v0, v3, v1}, Lmiuix/appcompat/app/AlertController;->p0(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;Landroid/os/Message;)V

    :goto_29
    goto/32 :goto_2c

    nop

    :goto_2a
    new-instance v0, Ljava/util/ArrayList;

    goto/32 :goto_1a

    nop

    :goto_2b
    iget-object v0, p0, Lmiuix/appcompat/app/AlertController$AlertParams;->mView:Landroid/view/View;

    goto/32 :goto_35

    nop

    :goto_2c
    iget-object v0, p0, Lmiuix/appcompat/app/AlertController$AlertParams;->mExtraButtonList:Ljava/util/List;

    goto/32 :goto_24

    nop

    :goto_2d
    invoke-direct {p0, p1}, Lmiuix/appcompat/app/AlertController$AlertParams;->createListView(Lmiuix/appcompat/app/AlertController;)V

    :goto_2e
    goto/32 :goto_2b

    nop

    :goto_2f
    iget v0, p0, Lmiuix/appcompat/app/AlertController$AlertParams;->mIconAttrId:I

    goto/32 :goto_3f

    nop

    :goto_30
    iget-object v0, p0, Lmiuix/appcompat/app/AlertController$AlertParams;->mComment:Ljava/lang/CharSequence;

    goto/32 :goto_57

    nop

    :goto_31
    if-eqz v0, :cond_6

    goto/32 :goto_46

    :cond_6
    goto/32 :goto_f

    nop

    :goto_32
    const/4 v2, -0x2

    goto/32 :goto_43

    nop

    :goto_33
    invoke-virtual {p1, v1, v0}, Lmiuix/appcompat/app/AlertController;->s0(ZLjava/lang/CharSequence;)V

    :goto_34
    goto/32 :goto_56

    nop

    :goto_35
    if-nez v0, :cond_7

    goto/32 :goto_5

    :cond_7
    goto/32 :goto_6

    nop

    :goto_36
    goto/16 :goto_52

    :goto_37
    goto/32 :goto_1b

    nop

    :goto_38
    if-nez v0, :cond_8

    goto/32 :goto_1d

    :cond_8
    goto/32 :goto_32

    nop

    :goto_39
    iget-object v0, p0, Lmiuix/appcompat/app/AlertController$AlertParams;->mPositiveButtonText:Ljava/lang/CharSequence;

    goto/32 :goto_4a

    nop

    :goto_3a
    invoke-virtual {p1, v0}, Lmiuix/appcompat/app/AlertController;->A0(Ljava/lang/CharSequence;)V

    :goto_3b
    goto/32 :goto_30

    nop

    :goto_3c
    iget-object v0, p0, Lmiuix/appcompat/app/AlertController$AlertParams;->mCheckBoxMessage:Ljava/lang/CharSequence;

    goto/32 :goto_13

    nop

    :goto_3d
    if-nez v0, :cond_9

    goto/32 :goto_21

    :cond_9
    goto/32 :goto_20

    nop

    :goto_3e
    if-nez v0, :cond_a

    goto/32 :goto_3b

    :cond_a
    goto/32 :goto_3a

    nop

    :goto_3f
    if-nez v0, :cond_b

    goto/32 :goto_52

    :cond_b
    goto/32 :goto_19

    nop

    :goto_40
    if-nez v0, :cond_c

    goto/32 :goto_29

    :cond_c
    goto/32 :goto_41

    nop

    :goto_41
    const/4 v2, -0x3

    goto/32 :goto_18

    nop

    :goto_42
    iget v0, p0, Lmiuix/appcompat/app/AlertController$AlertParams;->mLiteVersion:I

    goto/32 :goto_d

    nop

    :goto_43
    iget-object v3, p0, Lmiuix/appcompat/app/AlertController$AlertParams;->mNegativeButtonListener:Landroid/content/DialogInterface$OnClickListener;

    goto/32 :goto_1c

    nop

    :goto_44
    iget-object v0, p0, Lmiuix/appcompat/app/AlertController$AlertParams;->mIcon:Landroid/graphics/drawable/Drawable;

    goto/32 :goto_50

    nop

    :goto_45
    if-nez v0, :cond_d

    goto/32 :goto_2e

    :cond_d
    :goto_46
    goto/32 :goto_2d

    nop

    :goto_47
    invoke-virtual {p1, v0}, Lmiuix/appcompat/app/AlertController;->x0(I)V

    :goto_48
    goto/32 :goto_2f

    nop

    :goto_49
    iget-object v3, p0, Lmiuix/appcompat/app/AlertController$AlertParams;->mPositiveButtonListener:Landroid/content/DialogInterface$OnClickListener;

    goto/32 :goto_15

    nop

    :goto_4a
    const/4 v1, 0x0

    goto/32 :goto_11

    nop

    :goto_4b
    invoke-virtual {p1, v0}, Lmiuix/appcompat/app/AlertController;->D0(Ljava/lang/CharSequence;)V

    :goto_4c
    goto/32 :goto_44

    nop

    :goto_4d
    iget-object v0, p0, Lmiuix/appcompat/app/AlertController$AlertParams;->mItems:[Ljava/lang/CharSequence;

    goto/32 :goto_31

    nop

    :goto_4e
    invoke-virtual {p1, v0}, Lmiuix/appcompat/app/AlertController;->v0(Z)V

    goto/32 :goto_1e

    nop

    :goto_4f
    iget-object v0, p0, Lmiuix/appcompat/app/AlertController$AlertParams;->mNegativeButtonText:Ljava/lang/CharSequence;

    goto/32 :goto_38

    nop

    :goto_50
    if-nez v0, :cond_e

    goto/32 :goto_55

    :cond_e
    goto/32 :goto_54

    nop

    :goto_51
    invoke-virtual {p1, v0}, Lmiuix/appcompat/app/AlertController;->x0(I)V

    :goto_52
    goto/32 :goto_25

    nop

    :goto_53
    if-eqz v0, :cond_f

    goto/32 :goto_46

    :cond_f
    goto/32 :goto_c

    nop

    :goto_54
    invoke-virtual {p1, v0}, Lmiuix/appcompat/app/AlertController;->y0(Landroid/graphics/drawable/Drawable;)V

    :goto_55
    goto/32 :goto_8

    nop

    :goto_56
    iget-boolean v0, p0, Lmiuix/appcompat/app/AlertController$AlertParams;->mHapticFeedbackEnabled:Z

    goto/32 :goto_e

    nop

    :goto_57
    if-nez v0, :cond_10

    goto/32 :goto_2

    :cond_10
    goto/32 :goto_1

    nop

    :goto_58
    iget-boolean v0, p0, Lmiuix/appcompat/app/AlertController$AlertParams;->mEnableDialogImmersive:Z

    goto/32 :goto_a

    nop

    :goto_59
    iget-object v0, p0, Lmiuix/appcompat/app/AlertController$AlertParams;->mNeutralButtonText:Ljava/lang/CharSequence;

    goto/32 :goto_40

    nop
.end method
