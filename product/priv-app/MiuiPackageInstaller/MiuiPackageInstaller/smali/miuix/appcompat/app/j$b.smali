.class Lmiuix/appcompat/app/j$b;
.super Ljava/lang/Object;

# interfaces
.implements Lmiuix/appcompat/app/e;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiuix/appcompat/app/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lmiuix/appcompat/app/j;


# direct methods
.method private constructor <init>(Lmiuix/appcompat/app/j;)V
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/app/j$b;->a:Lmiuix/appcompat/app/j;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lmiuix/appcompat/app/j;Lmiuix/appcompat/app/j$a;)V
    .locals 0

    invoke-direct {p0, p1}, Lmiuix/appcompat/app/j$b;-><init>(Lmiuix/appcompat/app/j;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/j$b;->a:Lmiuix/appcompat/app/j;

    invoke-static {v0}, Lmiuix/appcompat/app/j;->k0(Lmiuix/appcompat/app/j;)V

    return-void
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/j$b;->a:Lmiuix/appcompat/app/j;

    invoke-static {v0}, Lmiuix/appcompat/app/j;->p0(Lmiuix/appcompat/app/j;)V

    return-void
.end method

.method public c()V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/j$b;->a:Lmiuix/appcompat/app/j;

    invoke-static {v0}, Lmiuix/appcompat/app/j;->j0(Lmiuix/appcompat/app/j;)V

    return-void
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/j$b;->a:Lmiuix/appcompat/app/j;

    invoke-static {v0, p1}, Lmiuix/appcompat/app/j;->h0(Lmiuix/appcompat/app/j;Landroid/os/Bundle;)V

    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/j$b;->a:Lmiuix/appcompat/app/j;

    invoke-static {v0, p1}, Lmiuix/appcompat/app/j;->g0(Lmiuix/appcompat/app/j;Landroid/os/Bundle;)V

    return-void
.end method

.method public f(Landroid/os/Bundle;)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/j$b;->a:Lmiuix/appcompat/app/j;

    invoke-static {v0, p1}, Lmiuix/appcompat/app/j;->i0(Lmiuix/appcompat/app/j;Landroid/os/Bundle;)V

    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/j$b;->a:Lmiuix/appcompat/app/j;

    invoke-static {v0, p1}, Lmiuix/appcompat/app/j;->f0(Lmiuix/appcompat/app/j;Landroid/content/res/Configuration;)V

    return-void
.end method

.method public onCreatePanelMenu(ILandroid/view/Menu;)Z
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/j$b;->a:Lmiuix/appcompat/app/j;

    invoke-static {v0, p1, p2}, Lmiuix/appcompat/app/j;->n0(Lmiuix/appcompat/app/j;ILandroid/view/Menu;)Z

    move-result p1

    return p1
.end method

.method public onCreatePanelView(I)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/j$b;->a:Lmiuix/appcompat/app/j;

    invoke-static {v0, p1}, Lmiuix/appcompat/app/j;->m0(Lmiuix/appcompat/app/j;I)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public onMenuItemSelected(ILandroid/view/MenuItem;)Z
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/j$b;->a:Lmiuix/appcompat/app/j;

    invoke-static {v0, p1, p2}, Lmiuix/appcompat/app/j;->l0(Lmiuix/appcompat/app/j;ILandroid/view/MenuItem;)Z

    move-result p1

    return p1
.end method

.method public onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/j$b;->a:Lmiuix/appcompat/app/j;

    invoke-static {v0, p1, p2, p3}, Lmiuix/appcompat/app/j;->o0(Lmiuix/appcompat/app/j;ILandroid/view/View;Landroid/view/Menu;)Z

    move-result p1

    return p1
.end method
