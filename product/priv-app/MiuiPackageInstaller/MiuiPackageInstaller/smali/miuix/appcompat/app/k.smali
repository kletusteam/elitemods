.class Lmiuix/appcompat/app/k;
.super Lmiuix/appcompat/app/c;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiuix/appcompat/app/k$b;
    }
.end annotation


# instance fields
.field private A:Ln9/a;

.field private B:Landroid/view/ViewGroup;

.field private final C:Ljava/lang/String;

.field private D:Z

.field private E:Ljava/lang/CharSequence;

.field F:Landroid/view/Window;

.field private G:Lmiuix/appcompat/app/k$b;

.field private final H:Ljava/lang/Runnable;

.field private r:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

.field private s:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

.field private t:Landroid/view/ViewGroup;

.field private u:Landroid/view/LayoutInflater;

.field private v:Lmiuix/appcompat/app/e;

.field private w:Lm9/h;

.field private x:Z

.field private y:Z

.field private z:I


# direct methods
.method constructor <init>(Lmiuix/appcompat/app/j;Lmiuix/appcompat/app/e;Lm9/h;)V
    .locals 2

    invoke-direct {p0, p1}, Lmiuix/appcompat/app/c;-><init>(Lmiuix/appcompat/app/j;)V

    const/4 p1, 0x0

    iput-boolean p1, p0, Lmiuix/appcompat/app/k;->x:Z

    iput-boolean p1, p0, Lmiuix/appcompat/app/k;->y:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lmiuix/appcompat/app/k;->B:Landroid/view/ViewGroup;

    iput-boolean p1, p0, Lmiuix/appcompat/app/k;->D:Z

    new-instance p1, Lmiuix/appcompat/app/k$a;

    invoke-direct {p1, p0}, Lmiuix/appcompat/app/k$a;-><init>(Lmiuix/appcompat/app/k;)V

    iput-object p1, p0, Lmiuix/appcompat/app/k;->H:Ljava/lang/Runnable;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtimeNanos()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lmiuix/appcompat/app/k;->C:Ljava/lang/String;

    iput-object p2, p0, Lmiuix/appcompat/app/k;->v:Lmiuix/appcompat/app/e;

    iput-object p3, p0, Lmiuix/appcompat/app/k;->w:Lm9/h;

    return-void
.end method

.method static synthetic E(Lmiuix/appcompat/app/k;)Lmiuix/appcompat/app/e;
    .locals 0

    iget-object p0, p0, Lmiuix/appcompat/app/k;->v:Lmiuix/appcompat/app/e;

    return-object p0
.end method

.method private G(Landroid/view/Window;)V
    .locals 3

    iget-object v0, p0, Lmiuix/appcompat/app/k;->F:Landroid/view/Window;

    const-string v1, "AppCompat has already installed itself into the Window"

    if-nez v0, :cond_1

    invoke-virtual {p1}, Landroid/view/Window;->getCallback()Landroid/view/Window$Callback;

    move-result-object v0

    instance-of v2, v0, Lmiuix/appcompat/app/k$b;

    if-nez v2, :cond_0

    new-instance v1, Lmiuix/appcompat/app/k$b;

    invoke-direct {v1, p0, v0}, Lmiuix/appcompat/app/k$b;-><init>(Lmiuix/appcompat/app/k;Landroid/view/Window$Callback;)V

    iput-object v1, p0, Lmiuix/appcompat/app/k;->G:Lmiuix/appcompat/app/k$b;

    invoke-virtual {p1, v1}, Landroid/view/Window;->setCallback(Landroid/view/Window$Callback;)V

    iput-object p1, p0, Lmiuix/appcompat/app/k;->F:Landroid/view/Window;

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private H()V
    .locals 2

    iget-object v0, p0, Lmiuix/appcompat/app/k;->F:Landroid/view/Window;

    if-eqz v0, :cond_0

    return-void

    :cond_0
    if-nez v0, :cond_1

    iget-object v0, p0, Lmiuix/appcompat/app/c;->a:Lmiuix/appcompat/app/j;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-direct {p0, v0}, Lmiuix/appcompat/app/k;->G(Landroid/view/Window;)V

    :cond_1
    iget-object v0, p0, Lmiuix/appcompat/app/k;->F:Landroid/view/Window;

    if-eqz v0, :cond_2

    return-void

    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "We have not been given a Window"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private M(Landroid/view/Window;)I
    .locals 5

    invoke-virtual {p1}, Landroid/view/Window;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lk9/b;->I:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lia/d;->d(Landroid/content/Context;IZ)Z

    move-result v1

    if-eqz v1, :cond_1

    sget v1, Lk9/b;->J:I

    invoke-static {v0, v1, v2}, Lia/d;->d(Landroid/content/Context;IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    sget v1, Lk9/i;->C:I

    goto :goto_0

    :cond_0
    sget v1, Lk9/i;->B:I

    goto :goto_0

    :cond_1
    sget v1, Lk9/i;->E:I

    :goto_0
    sget v3, Lk9/b;->A:I

    invoke-static {v0, v3}, Lia/d;->c(Landroid/content/Context;I)I

    move-result v3

    if-lez v3, :cond_2

    invoke-direct {p0}, Lmiuix/appcompat/app/k;->W()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-static {v0}, Lmiuix/appcompat/app/k;->X(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_2

    move v1, v3

    :cond_2
    invoke-virtual {p1}, Landroid/view/Window;->isFloating()Z

    move-result v3

    if-nez v3, :cond_3

    invoke-virtual {p1}, Landroid/view/Window;->getCallback()Landroid/view/Window$Callback;

    move-result-object v3

    instance-of v3, v3, Landroid/app/Dialog;

    if-eqz v3, :cond_3

    sget v3, Lk9/b;->U:I

    invoke-static {v0, v3, v2}, Lia/d;->j(Landroid/content/Context;II)I

    move-result v0

    invoke-static {p1, v0}, Lca/a;->a(Landroid/view/Window;I)Z

    :cond_3
    return v1
.end method

.method private S()V
    .locals 6

    iget-boolean v0, p0, Lmiuix/appcompat/app/c;->e:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    invoke-direct {p0}, Lmiuix/appcompat/app/k;->H()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiuix/appcompat/app/c;->e:Z

    iget-object v1, p0, Lmiuix/appcompat/app/c;->a:Lmiuix/appcompat/app/j;

    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    iput-object v2, p0, Lmiuix/appcompat/app/k;->u:Landroid/view/LayoutInflater;

    iget-object v2, p0, Lmiuix/appcompat/app/c;->a:Lmiuix/appcompat/app/j;

    sget-object v3, Lk9/l;->v2:[I

    invoke-virtual {v2, v3}, Landroid/app/Activity;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v2

    sget v3, Lk9/l;->K2:I

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v3

    if-ne v3, v0, :cond_1

    iget-object v3, p0, Lmiuix/appcompat/app/c;->a:Lmiuix/appcompat/app/j;

    invoke-virtual {v3}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v3

    const/16 v5, 0x50

    invoke-virtual {v3, v5}, Landroid/view/Window;->setGravity(I)V

    :cond_1
    sget v3, Lk9/l;->B2:I

    invoke-virtual {v2, v3}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v5

    if-eqz v5, :cond_c

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v3

    if-eqz v3, :cond_2

    const/16 v3, 0x8

    invoke-virtual {p0, v3}, Lmiuix/appcompat/app/c;->z(I)Z

    :cond_2
    sget v3, Lk9/l;->C2:I

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v3

    if-eqz v3, :cond_3

    const/16 v3, 0x9

    invoke-virtual {p0, v3}, Lmiuix/appcompat/app/c;->z(I)Z

    :cond_3
    sget v3, Lk9/l;->A2:I

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v3

    iput-boolean v3, p0, Lmiuix/appcompat/app/k;->x:Z

    sget v3, Lk9/l;->J2:I

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v3

    iput-boolean v3, p0, Lmiuix/appcompat/app/k;->y:Z

    sget v3, Lk9/l;->Q2:I

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v3

    invoke-virtual {p0, v3}, Lmiuix/appcompat/app/c;->C(I)V

    iget-object v3, p0, Lmiuix/appcompat/app/c;->a:Lmiuix/appcompat/app/j;

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->uiMode:I

    iput v3, p0, Lmiuix/appcompat/app/k;->z:I

    invoke-direct {p0, v1}, Lmiuix/appcompat/app/k;->T(Landroid/view/Window;)V

    iget-object v1, p0, Lmiuix/appcompat/app/k;->r:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    if-eqz v1, :cond_4

    iget-object v3, p0, Lmiuix/appcompat/app/c;->a:Lmiuix/appcompat/app/j;

    invoke-virtual {v1, v3}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->setCallback(Landroid/view/Window$Callback;)V

    iget-object v1, p0, Lmiuix/appcompat/app/k;->r:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    invoke-virtual {p0}, Lmiuix/appcompat/app/c;->p()I

    move-result v3

    invoke-virtual {v1, v3}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->setTranslucentStatus(I)V

    :cond_4
    iget-boolean v1, p0, Lmiuix/appcompat/app/c;->h:Z

    if-eqz v1, :cond_a

    iget-object v1, p0, Lmiuix/appcompat/app/k;->r:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    if-eqz v1, :cond_a

    sget v3, Lk9/g;->d:I

    invoke-virtual {v1, v3}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    iput-object v1, p0, Lmiuix/appcompat/app/k;->s:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    iget-object v1, p0, Lmiuix/appcompat/app/k;->r:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    iget-boolean v3, p0, Lmiuix/appcompat/app/c;->i:Z

    invoke-virtual {v1, v3}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->setOverlayMode(Z)V

    iget-object v1, p0, Lmiuix/appcompat/app/k;->r:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    sget v3, Lk9/g;->a:I

    invoke-virtual {v1, v3}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lmiuix/appcompat/internal/app/widget/ActionBarView;

    iput-object v1, p0, Lmiuix/appcompat/app/c;->b:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    iget-object v3, p0, Lmiuix/appcompat/app/c;->a:Lmiuix/appcompat/app/j;

    invoke-virtual {v1, v3}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->setWindowCallback(Landroid/view/Window$Callback;)V

    iget-boolean v1, p0, Lmiuix/appcompat/app/c;->g:Z

    if-eqz v1, :cond_5

    iget-object v1, p0, Lmiuix/appcompat/app/c;->b:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {v1}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->E0()V

    :cond_5
    sget v1, Lk9/l;->z2:I

    invoke-virtual {v2, v1, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lmiuix/appcompat/app/c;->m:I

    invoke-virtual {p0}, Lmiuix/appcompat/app/c;->r()Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lmiuix/appcompat/app/c;->b:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    iget v3, p0, Lmiuix/appcompat/app/c;->m:I

    invoke-virtual {v1, v3, p0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->D0(ILmiuix/appcompat/app/c;)V

    :cond_6
    iget-object v1, p0, Lmiuix/appcompat/app/c;->b:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {v1}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->getCustomNavigationView()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_7

    iget-object v1, p0, Lmiuix/appcompat/app/c;->b:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {v1}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->getDisplayOptions()I

    move-result v3

    or-int/lit8 v3, v3, 0x10

    invoke-virtual {v1, v3}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->setDisplayOptions(I)V

    :cond_7
    invoke-virtual {p0}, Lmiuix/appcompat/app/c;->q()Ljava/lang/String;

    move-result-object v1

    const-string v3, "splitActionBarWhenNarrow"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    iget-object v3, p0, Lmiuix/appcompat/app/c;->a:Lmiuix/appcompat/app/j;

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v5, Lk9/c;->c:I

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v3

    goto :goto_0

    :cond_8
    sget v3, Lk9/l;->P2:I

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v3

    :goto_0
    if-eqz v3, :cond_9

    iget-object v5, p0, Lmiuix/appcompat/app/k;->r:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    invoke-virtual {p0, v3, v1, v5}, Lmiuix/appcompat/app/c;->h(ZZLmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;)V

    :cond_9
    iget-object v1, p0, Lmiuix/appcompat/app/c;->a:Lmiuix/appcompat/app/j;

    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    iget-object v3, p0, Lmiuix/appcompat/app/k;->H:Ljava/lang/Runnable;

    invoke-virtual {v1, v3}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    :cond_a
    sget v1, Lk9/l;->y2:I

    invoke-virtual {v2, v1, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    if-eqz v1, :cond_b

    invoke-virtual {p0, v0}, Lmiuix/appcompat/app/c;->A(Z)V

    :cond_b
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    return-void

    :cond_c
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "You need to use a miui theme (or descendant) with this activity."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private T(Landroid/view/Window;)V
    .locals 7

    iget-boolean v0, p0, Lmiuix/appcompat/app/k;->x:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiuix/appcompat/app/c;->a:Lmiuix/appcompat/app/j;

    invoke-static {v0}, Ln9/b;->a(Lmiuix/appcompat/app/j;)Ln9/a;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    iput-object v0, p0, Lmiuix/appcompat/app/k;->A:Ln9/a;

    iput-object v1, p0, Lmiuix/appcompat/app/k;->B:Landroid/view/ViewGroup;

    iget-object v0, p0, Lmiuix/appcompat/app/c;->a:Lmiuix/appcompat/app/j;

    invoke-direct {p0, p1}, Lmiuix/appcompat/app/k;->M(Landroid/view/Window;)I

    move-result v2

    invoke-static {v0, v2, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iget-object v2, p0, Lmiuix/appcompat/app/k;->A:Ln9/a;

    if-eqz v2, :cond_1

    invoke-direct {p0}, Lmiuix/appcompat/app/k;->u0()Z

    move-result v2

    iput-boolean v2, p0, Lmiuix/appcompat/app/k;->y:Z

    iget-object v3, p0, Lmiuix/appcompat/app/k;->A:Ln9/a;

    invoke-virtual {v3, v2}, Ln9/a;->l(Z)V

    iget-object v2, p0, Lmiuix/appcompat/app/k;->A:Ln9/a;

    iget-boolean v3, p0, Lmiuix/appcompat/app/k;->y:Z

    invoke-virtual {v2, v0, v3}, Ln9/a;->j(Landroid/view/View;Z)Landroid/view/ViewGroup;

    move-result-object v0

    iput-object v0, p0, Lmiuix/appcompat/app/k;->B:Landroid/view/ViewGroup;

    iget-boolean v2, p0, Lmiuix/appcompat/app/k;->y:Z

    invoke-direct {p0, v2}, Lmiuix/appcompat/app/k;->x0(Z)V

    :cond_1
    sget v2, Lk9/g;->j:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    instance-of v3, v2, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    const v4, 0x1020002

    if-eqz v3, :cond_3

    check-cast v2, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    iput-object v2, p0, Lmiuix/appcompat/app/k;->r:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    invoke-virtual {v2, v4}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    invoke-virtual {p1, v4}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    if-eqz v3, :cond_3

    :goto_1
    invoke-virtual {v3}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v5

    if-lez v5, :cond_2

    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v3, v5}, Landroid/view/ViewGroup;->removeViewAt(I)V

    invoke-virtual {v2, v6}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_1

    :cond_2
    const/4 v5, -0x1

    invoke-virtual {v3, v5}, Landroid/view/ViewGroup;->setId(I)V

    invoke-virtual {v2, v4}, Landroid/view/ViewGroup;->setId(I)V

    instance-of v2, v3, Landroid/widget/FrameLayout;

    if-eqz v2, :cond_3

    check-cast v3, Landroid/widget/FrameLayout;

    invoke-virtual {v3, v1}, Landroid/widget/FrameLayout;->setForeground(Landroid/graphics/drawable/Drawable;)V

    :cond_3
    invoke-virtual {p1, v0}, Landroid/view/Window;->setContentView(Landroid/view/View;)V

    iget-object p1, p0, Lmiuix/appcompat/app/k;->r:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    if-eqz p1, :cond_4

    invoke-virtual {p1, v4}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    iput-object p1, p0, Lmiuix/appcompat/app/k;->t:Landroid/view/ViewGroup;

    :cond_4
    iget-object p1, p0, Lmiuix/appcompat/app/k;->A:Ln9/a;

    if-eqz p1, :cond_5

    iget-object v0, p0, Lmiuix/appcompat/app/k;->B:Landroid/view/ViewGroup;

    invoke-direct {p0}, Lmiuix/appcompat/app/k;->u0()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Ln9/a;->f(Landroid/view/View;Z)V

    :cond_5
    return-void
.end method

.method private W()Z
    .locals 2

    invoke-virtual {p0}, Lmiuix/appcompat/app/c;->m()Lmiuix/appcompat/app/j;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    const-string v1, "android"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private static X(Landroid/content/Context;)Z
    .locals 2

    sget v0, Lk9/b;->I:I

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lia/d;->d(Landroid/content/Context;IZ)Z

    move-result p0

    return p0
.end method

.method private Y(Z)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/k;->w:Lm9/h;

    invoke-interface {v0, p1}, Lm9/h;->b(Z)V

    return-void
.end method

.method private q0(ZIZZ)V
    .locals 1

    iget-boolean v0, p0, Lmiuix/appcompat/app/k;->x:Z

    if-eqz v0, :cond_5

    if-nez p4, :cond_0

    iget-object p4, p0, Lmiuix/appcompat/app/c;->a:Lmiuix/appcompat/app/j;

    invoke-static {p4}, Lia/e;->d(Landroid/content/Context;)Z

    move-result p4

    if-nez p4, :cond_0

    goto :goto_1

    :cond_0
    iget-boolean p4, p0, Lmiuix/appcompat/app/k;->y:Z

    if-eq p4, p1, :cond_4

    iget-object p4, p0, Lmiuix/appcompat/app/k;->w:Lm9/h;

    invoke-interface {p4, p1}, Lm9/h;->a(Z)Z

    move-result p4

    if-eqz p4, :cond_4

    iput-boolean p1, p0, Lmiuix/appcompat/app/k;->y:Z

    iget-object p2, p0, Lmiuix/appcompat/app/k;->A:Ln9/a;

    invoke-virtual {p2, p1}, Ln9/a;->l(Z)V

    iget-boolean p2, p0, Lmiuix/appcompat/app/k;->y:Z

    invoke-direct {p0, p2}, Lmiuix/appcompat/app/k;->x0(Z)V

    iget-object p2, p0, Lmiuix/appcompat/app/k;->r:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    if-eqz p2, :cond_2

    iget-object p2, p0, Lmiuix/appcompat/app/k;->A:Ln9/a;

    invoke-virtual {p2}, Ln9/a;->c()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p2

    if-eqz p1, :cond_1

    const/4 p4, -0x2

    goto :goto_0

    :cond_1
    const/4 p4, -0x1

    :goto_0
    iput p4, p2, Landroid/view/ViewGroup$LayoutParams;->height:I

    iput p4, p2, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget-object p4, p0, Lmiuix/appcompat/app/k;->r:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    invoke-virtual {p4, p2}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object p2, p0, Lmiuix/appcompat/app/k;->r:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    invoke-virtual {p2}, Landroid/widget/FrameLayout;->requestLayout()V

    :cond_2
    iget-object p2, p0, Lmiuix/appcompat/app/k;->r:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    if-eqz p2, :cond_3

    invoke-virtual {p2, p1}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->y(Z)V

    :cond_3
    if-eqz p3, :cond_5

    invoke-direct {p0, p1}, Lmiuix/appcompat/app/k;->Y(Z)V

    goto :goto_1

    :cond_4
    iget p3, p0, Lmiuix/appcompat/app/k;->z:I

    if-eq p2, p3, :cond_5

    iput p2, p0, Lmiuix/appcompat/app/k;->z:I

    iget-object p2, p0, Lmiuix/appcompat/app/k;->A:Ln9/a;

    invoke-virtual {p2, p1}, Ln9/a;->l(Z)V

    :cond_5
    :goto_1
    return-void
.end method

.method private u0()Z
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/k;->A:Ln9/a;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ln9/a;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private x0(Z)V
    .locals 6

    iget-object v0, p0, Lmiuix/appcompat/app/c;->a:Lmiuix/appcompat/app/j;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getSystemUiVisibility()I

    move-result v1

    and-int/lit16 v2, v1, 0x400

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-eqz v2, :cond_0

    move v2, v3

    goto :goto_0

    :cond_0
    move v2, v4

    :goto_0
    invoke-virtual {p0}, Lmiuix/appcompat/app/c;->p()I

    move-result v5

    if-eqz v5, :cond_1

    move v5, v3

    goto :goto_1

    :cond_1
    move v5, v4

    :goto_1
    if-nez v2, :cond_3

    if-eqz v5, :cond_2

    goto :goto_2

    :cond_2
    move v2, v4

    goto :goto_3

    :cond_3
    :goto_2
    move v2, v3

    :goto_3
    const/16 v5, 0x1e

    if-nez p1, :cond_6

    if-eqz v2, :cond_4

    or-int/lit16 p1, v1, 0x400

    goto :goto_4

    :cond_4
    and-int/lit16 p1, v1, -0x401

    :goto_4
    move v1, p1

    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt p1, v5, :cond_7

    if-eqz v2, :cond_5

    :goto_5
    invoke-virtual {v0, v4}, Landroid/view/Window;->setDecorFitsSystemWindows(Z)V

    goto :goto_6

    :cond_5
    invoke-virtual {v0, v3}, Landroid/view/Window;->setDecorFitsSystemWindows(Z)V

    goto :goto_6

    :cond_6
    const/high16 p1, 0xc000000

    invoke-virtual {v0, p1}, Landroid/view/Window;->addFlags(I)V

    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt p1, v5, :cond_7

    goto :goto_5

    :cond_7
    :goto_6
    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    return-void
.end method


# virtual methods
.method public F(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    iget-boolean v0, p0, Lmiuix/appcompat/app/c;->e:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lmiuix/appcompat/app/k;->S()V

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/app/k;->t:Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    invoke-virtual {v0, p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :cond_1
    iget-object p1, p0, Lmiuix/appcompat/app/k;->G:Lmiuix/appcompat/app/k$b;

    invoke-virtual {p1}, Li/i;->a()Landroid/view/Window$Callback;

    move-result-object p1

    invoke-interface {p1}, Landroid/view/Window$Callback;->onContentChanged()V

    return-void
.end method

.method public I()V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/k;->A:Ln9/a;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lm9/d;->o()V

    :cond_0
    return-void
.end method

.method public J()V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/k;->A:Ln9/a;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lm9/d;->P()V

    :cond_0
    return-void
.end method

.method public K()V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/k;->A:Ln9/a;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lm9/d;->J()V

    :cond_0
    return-void
.end method

.method public L()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/k;->C:Ljava/lang/String;

    return-object v0
.end method

.method public N()I
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/k;->r:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->getExtraHorizontalPaddingLevel()I

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public O()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/k;->A:Ln9/a;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ln9/a;->b()Landroid/view/View;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public P()V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/k;->A:Ln9/a;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ln9/a;->d()V

    :cond_0
    return-void
.end method

.method public Q()V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/k;->A:Ln9/a;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ln9/a;->e()V

    :cond_0
    return-void
.end method

.method public R(ZLandroid/os/Bundle;)V
    .locals 1

    if-nez p1, :cond_0

    return-void

    :cond_0
    iget-object p1, p0, Lmiuix/appcompat/app/c;->a:Lmiuix/appcompat/app/j;

    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    if-eqz p1, :cond_1

    invoke-static {p1}, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->N(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmiuix/appcompat/app/c;->a:Lmiuix/appcompat/app/j;

    invoke-static {v0, p1, p2}, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->H(Lmiuix/appcompat/app/j;Landroid/content/Intent;Landroid/os/Bundle;)V

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lmiuix/appcompat/app/c;->a:Lmiuix/appcompat/app/j;

    invoke-static {p1, p2}, Lmiuix/appcompat/app/floatingactivity/a;->w(Lmiuix/appcompat/app/j;Landroid/os/Bundle;)V

    :goto_0
    return-void
.end method

.method public U()Z
    .locals 1

    iget-boolean v0, p0, Lmiuix/appcompat/app/k;->D:Z

    return v0
.end method

.method public V()Z
    .locals 1

    invoke-direct {p0}, Lmiuix/appcompat/app/k;->u0()Z

    move-result v0

    return v0
.end method

.method public Z(Landroid/view/ActionMode;)V
    .locals 0

    const/4 p1, 0x0

    iput-object p1, p0, Lmiuix/appcompat/app/c;->d:Landroid/view/ActionMode;

    return-void
.end method

.method public a()V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/k;->H:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    return-void
.end method

.method public a0(Landroid/view/ActionMode;)V
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/app/c;->d:Landroid/view/ActionMode;

    return-void
.end method

.method b0()V
    .locals 1

    goto/32 :goto_d

    nop

    :goto_0
    iget-object v0, p0, Lmiuix/appcompat/app/k;->v:Lmiuix/appcompat/app/e;

    goto/32 :goto_7

    nop

    :goto_1
    invoke-virtual {v0}, Ln9/a;->i()Z

    move-result v0

    goto/32 :goto_5

    nop

    :goto_2
    invoke-virtual {v0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->w0()Z

    move-result v0

    goto/32 :goto_10

    nop

    :goto_3
    if-nez v0, :cond_0

    goto/32 :goto_14

    :cond_0
    goto/32 :goto_1

    nop

    :goto_4
    iget-object v0, p0, Lmiuix/appcompat/app/c;->b:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    goto/32 :goto_6

    nop

    :goto_5
    if-nez v0, :cond_1

    goto/32 :goto_14

    :cond_1
    goto/32 :goto_13

    nop

    :goto_6
    invoke-virtual {v0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->m0()V

    goto/32 :goto_9

    nop

    :goto_7
    invoke-interface {v0}, Lmiuix/appcompat/app/e;->b()V

    goto/32 :goto_12

    nop

    :goto_8
    iget-object v0, p0, Lmiuix/appcompat/app/c;->b:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    goto/32 :goto_c

    nop

    :goto_9
    return-void

    :goto_a
    goto/32 :goto_b

    nop

    :goto_b
    iget-object v0, p0, Lmiuix/appcompat/app/k;->A:Ln9/a;

    goto/32 :goto_3

    nop

    :goto_c
    if-nez v0, :cond_2

    goto/32 :goto_a

    :cond_2
    goto/32 :goto_2

    nop

    :goto_d
    iget-object v0, p0, Lmiuix/appcompat/app/c;->d:Landroid/view/ActionMode;

    goto/32 :goto_11

    nop

    :goto_e
    return-void

    :goto_f
    goto/32 :goto_8

    nop

    :goto_10
    if-nez v0, :cond_3

    goto/32 :goto_a

    :cond_3
    goto/32 :goto_4

    nop

    :goto_11
    if-nez v0, :cond_4

    goto/32 :goto_f

    :cond_4
    goto/32 :goto_15

    nop

    :goto_12
    return-void

    :goto_13
    return-void

    :goto_14
    goto/32 :goto_0

    nop

    :goto_15
    invoke-virtual {v0}, Landroid/view/ActionMode;->finish()V

    goto/32 :goto_e

    nop
.end method

.method public c0(Landroid/os/Bundle;)V
    .locals 7

    sget-boolean v0, Lr9/c;->a:Z

    const/4 v1, 0x1

    if-nez v0, :cond_0

    sput-boolean v1, Lr9/c;->a:Z

    invoke-virtual {p0}, Lmiuix/appcompat/app/k;->o()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lr9/c;->b(Landroid/content/Context;)V

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/app/k;->v:Lmiuix/appcompat/app/e;

    invoke-interface {v0, p1}, Lmiuix/appcompat/app/e;->f(Landroid/os/Bundle;)V

    invoke-direct {p0}, Lmiuix/appcompat/app/k;->S()V

    iget-boolean v0, p0, Lmiuix/appcompat/app/k;->x:Z

    invoke-virtual {p0, v0, p1}, Lmiuix/appcompat/app/k;->R(ZLandroid/os/Bundle;)V

    const/16 p1, 0x80

    const/4 v0, 0x0

    :try_start_0
    iget-object v2, p0, Lmiuix/appcompat/app/c;->a:Lmiuix/appcompat/app/j;

    invoke-virtual {v2}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    iget-object v3, p0, Lmiuix/appcompat/app/c;->a:Lmiuix/appcompat/app/j;

    invoke-virtual {v3}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, p1}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v2
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    move-object v2, v0

    :goto_0
    const-string v3, "miui.extra.window.padding.level"

    const/4 v4, 0x0

    if-eqz v2, :cond_1

    iget-object v2, v2, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    if-eqz v2, :cond_1

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    goto :goto_1

    :cond_1
    move v2, v4

    :goto_1
    :try_start_1
    iget-object v5, p0, Lmiuix/appcompat/app/c;->a:Lmiuix/appcompat/app/j;

    invoke-virtual {v5}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    iget-object v6, p0, Lmiuix/appcompat/app/c;->a:Lmiuix/appcompat/app/j;

    invoke-virtual {v6}, Landroid/app/Activity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v6

    invoke-virtual {v5, v6, p1}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    move-result-object v0
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    :catch_1
    move-exception p1

    invoke-virtual {p1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    :goto_2
    if-eqz v0, :cond_2

    iget-object p1, v0, Landroid/content/pm/ActivityInfo;->metaData:Landroid/os/Bundle;

    if-eqz p1, :cond_2

    invoke-virtual {p1, v3, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    :cond_2
    iget-object p1, p0, Lmiuix/appcompat/app/c;->a:Lmiuix/appcompat/app/j;

    sget v0, Lk9/b;->K:I

    invoke-static {p1, v0, v2}, Lia/d;->j(Landroid/content/Context;II)I

    move-result p1

    if-eqz p1, :cond_3

    goto :goto_3

    :cond_3
    move v1, v4

    :goto_3
    iget-object v0, p0, Lmiuix/appcompat/app/c;->a:Lmiuix/appcompat/app/j;

    sget v2, Lk9/b;->L:I

    invoke-static {v0, v2, v1}, Lia/d;->d(Landroid/content/Context;IZ)Z

    move-result v0

    invoke-virtual {p0, p1}, Lmiuix/appcompat/app/k;->p0(I)V

    invoke-virtual {p0, v0}, Lmiuix/appcompat/app/k;->o0(Z)V

    return-void
.end method

.method public d(Lmiuix/appcompat/internal/view/menu/c;Landroid/view/MenuItem;)Z
    .locals 1

    iget-object p1, p0, Lmiuix/appcompat/app/c;->a:Lmiuix/appcompat/app/j;

    const/4 v0, 0x0

    invoke-virtual {p1, v0, p2}, Lmiuix/appcompat/app/j;->onMenuItemSelected(ILandroid/view/MenuItem;)Z

    move-result p1

    return p1
.end method

.method public d0(ILandroid/view/Menu;)Z
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lmiuix/appcompat/app/k;->v:Lmiuix/appcompat/app/e;

    invoke-interface {v0, p1, p2}, Lmiuix/appcompat/app/e;->onCreatePanelMenu(ILandroid/view/Menu;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public e()Lmiuix/appcompat/app/a;
    .locals 3

    iget-boolean v0, p0, Lmiuix/appcompat/app/c;->e:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lmiuix/appcompat/app/k;->S()V

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/app/k;->r:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    return-object v0

    :cond_1
    new-instance v0, Lmiuix/appcompat/internal/app/widget/b;

    iget-object v1, p0, Lmiuix/appcompat/app/c;->a:Lmiuix/appcompat/app/j;

    iget-object v2, p0, Lmiuix/appcompat/app/k;->r:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    invoke-direct {v0, v1, v2}, Lmiuix/appcompat/internal/app/widget/b;-><init>(Lmiuix/appcompat/app/j;Landroid/view/ViewGroup;)V

    return-object v0
.end method

.method public e0(I)Landroid/view/View;
    .locals 4

    if-eqz p1, :cond_0

    iget-object v0, p0, Lmiuix/appcompat/app/k;->v:Lmiuix/appcompat/app/e;

    invoke-interface {v0, p1}, Lmiuix/appcompat/app/e;->onCreatePanelView(I)Landroid/view/View;

    move-result-object p1

    return-object p1

    :cond_0
    invoke-virtual {p0}, Lmiuix/appcompat/app/c;->r()Z

    move-result p1

    const/4 v0, 0x0

    if-nez p1, :cond_6

    iget-object p1, p0, Lmiuix/appcompat/app/c;->c:Lmiuix/appcompat/internal/view/menu/c;

    iget-object v1, p0, Lmiuix/appcompat/app/c;->d:Landroid/view/ActionMode;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-nez v1, :cond_2

    if-nez p1, :cond_1

    invoke-virtual {p0}, Lmiuix/appcompat/app/c;->i()Lmiuix/appcompat/internal/view/menu/c;

    move-result-object p1

    invoke-virtual {p0, p1}, Lmiuix/appcompat/app/c;->B(Lmiuix/appcompat/internal/view/menu/c;)V

    invoke-virtual {p1}, Lmiuix/appcompat/internal/view/menu/c;->V()V

    iget-object v1, p0, Lmiuix/appcompat/app/k;->v:Lmiuix/appcompat/app/e;

    invoke-interface {v1, v3, p1}, Lmiuix/appcompat/app/e;->onCreatePanelMenu(ILandroid/view/Menu;)Z

    move-result v2

    :cond_1
    if-eqz v2, :cond_4

    invoke-virtual {p1}, Lmiuix/appcompat/internal/view/menu/c;->V()V

    iget-object v1, p0, Lmiuix/appcompat/app/k;->v:Lmiuix/appcompat/app/e;

    invoke-interface {v1, v3, v0, p1}, Lmiuix/appcompat/app/e;->onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z

    move-result v2

    goto :goto_0

    :cond_2
    if-eqz p1, :cond_3

    goto :goto_0

    :cond_3
    move v2, v3

    :cond_4
    :goto_0
    if-eqz v2, :cond_5

    invoke-virtual {p1}, Lmiuix/appcompat/internal/view/menu/c;->U()V

    goto :goto_1

    :cond_5
    invoke-virtual {p0, v0}, Lmiuix/appcompat/app/c;->B(Lmiuix/appcompat/internal/view/menu/c;)V

    :cond_6
    :goto_1
    return-object v0
.end method

.method public f0()V
    .locals 2

    iget-object v0, p0, Lmiuix/appcompat/app/k;->v:Lmiuix/appcompat/app/e;

    invoke-interface {v0}, Lmiuix/appcompat/app/e;->c()V

    invoke-virtual {p0}, Lmiuix/appcompat/app/c;->k()Lmiuix/appcompat/app/a;

    move-result-object v0

    check-cast v0, Lmiuix/appcompat/internal/app/widget/b;

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lmiuix/appcompat/internal/app/widget/b;->u(Z)V

    :cond_0
    return-void
.end method

.method public g0(ILandroid/view/View;Landroid/view/Menu;)Z
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lmiuix/appcompat/app/k;->v:Lmiuix/appcompat/app/e;

    invoke-interface {v0, p1, p2, p3}, Lmiuix/appcompat/app/e;->onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public h0(Landroid/os/Bundle;)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/k;->v:Lmiuix/appcompat/app/e;

    invoke-interface {v0, p1}, Lmiuix/appcompat/app/e;->d(Landroid/os/Bundle;)V

    iget-object v0, p0, Lmiuix/appcompat/app/k;->s:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    if-eqz v0, :cond_0

    const-string v0, "miuix:ActionBar"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSparseParcelableArray(Ljava/lang/String;)Landroid/util/SparseArray;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lmiuix/appcompat/app/k;->s:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->restoreHierarchyState(Landroid/util/SparseArray;)V

    :cond_0
    return-void
.end method

.method public i0(Landroid/os/Bundle;)V
    .locals 2

    iget-object v0, p0, Lmiuix/appcompat/app/k;->v:Lmiuix/appcompat/app/e;

    invoke-interface {v0, p1}, Lmiuix/appcompat/app/e;->e(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lmiuix/appcompat/app/k;->A:Ln9/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiuix/appcompat/app/c;->a:Lmiuix/appcompat/app/j;

    invoke-static {v0, p1}, Lmiuix/appcompat/app/floatingactivity/a;->B(Lmiuix/appcompat/app/j;Landroid/os/Bundle;)V

    iget-object v0, p0, Lmiuix/appcompat/app/c;->a:Lmiuix/appcompat/app/j;

    invoke-virtual {v0}, Landroid/app/Activity;->getTaskId()I

    move-result v0

    iget-object v1, p0, Lmiuix/appcompat/app/c;->a:Lmiuix/appcompat/app/j;

    invoke-virtual {v1}, Lmiuix/appcompat/app/j;->q0()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p1}, Lmiuix/appcompat/app/floatingactivity/multiapp/c;->U(ILjava/lang/String;Landroid/os/Bundle;)V

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/app/k;->s:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    if-eqz v0, :cond_1

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iget-object v1, p0, Lmiuix/appcompat/app/k;->s:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->saveHierarchyState(Landroid/util/SparseArray;)V

    const-string v1, "miuix:ActionBar"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putSparseParcelableArray(Ljava/lang/String;Landroid/util/SparseArray;)V

    :cond_1
    return-void
.end method

.method public j0()V
    .locals 2

    iget-object v0, p0, Lmiuix/appcompat/app/k;->v:Lmiuix/appcompat/app/e;

    invoke-interface {v0}, Lmiuix/appcompat/app/e;->a()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lmiuix/appcompat/app/c;->j(Z)V

    invoke-virtual {p0}, Lmiuix/appcompat/app/c;->k()Lmiuix/appcompat/app/a;

    move-result-object v1

    check-cast v1, Lmiuix/appcompat/internal/app/widget/b;

    if-eqz v1, :cond_0

    invoke-virtual {v1, v0}, Lmiuix/appcompat/internal/app/widget/b;->u(Z)V

    :cond_0
    return-void
.end method

.method public k0(I)V
    .locals 2

    iget-boolean v0, p0, Lmiuix/appcompat/app/c;->e:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lmiuix/appcompat/app/k;->S()V

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/app/k;->t:Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    iget-object v0, p0, Lmiuix/appcompat/app/k;->u:Landroid/view/LayoutInflater;

    iget-object v1, p0, Lmiuix/appcompat/app/k;->t:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    :cond_1
    iget-object p1, p0, Lmiuix/appcompat/app/k;->G:Lmiuix/appcompat/app/k$b;

    invoke-virtual {p1}, Li/i;->a()Landroid/view/Window$Callback;

    move-result-object p1

    invoke-interface {p1}, Landroid/view/Window$Callback;->onContentChanged()V

    return-void
.end method

.method public l0(Landroid/view/View;)V
    .locals 2

    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v1, -0x1

    invoke-direct {v0, v1, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, p1, v0}, Lmiuix/appcompat/app/k;->m0(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public m0(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    iget-boolean v0, p0, Lmiuix/appcompat/app/c;->e:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lmiuix/appcompat/app/k;->S()V

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/app/k;->t:Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    iget-object v0, p0, Lmiuix/appcompat/app/k;->t:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :cond_1
    iget-object p1, p0, Lmiuix/appcompat/app/k;->G:Lmiuix/appcompat/app/k$b;

    invoke-virtual {p1}, Li/i;->a()Landroid/view/Window$Callback;

    move-result-object p1

    invoke-interface {p1}, Landroid/view/Window$Callback;->onContentChanged()V

    return-void
.end method

.method public n0(Z)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/k;->A:Ln9/a;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Ln9/a;->k(Z)V

    :cond_0
    return-void
.end method

.method public o()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/c;->a:Lmiuix/appcompat/app/j;

    return-object v0
.end method

.method public o0(Z)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/k;->r:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->setExtraHorizontalPaddingEnable(Z)V

    :cond_0
    return-void
.end method

.method public p0(I)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/k;->r:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->setExtraHorizontalPaddingLevel(I)V

    :cond_0
    return-void
.end method

.method public r0(Lm9/g;)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/k;->A:Ln9/a;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Ln9/a;->m(Lm9/g;)V

    :cond_0
    return-void
.end method

.method public s(Landroid/content/res/Configuration;)V
    .locals 4

    invoke-super {p0, p1}, Lmiuix/appcompat/app/c;->s(Landroid/content/res/Configuration;)V

    invoke-virtual {p0}, Lmiuix/appcompat/app/k;->V()Z

    move-result v0

    iget v1, p1, Landroid/content/res/Configuration;->uiMode:I

    invoke-static {}, Lia/e;->b()Z

    move-result v2

    const/4 v3, 0x1

    invoke-direct {p0, v0, v1, v3, v2}, Lmiuix/appcompat/app/k;->q0(ZIZZ)V

    iget-object v0, p0, Lmiuix/appcompat/app/k;->v:Lmiuix/appcompat/app/e;

    invoke-interface {v0, p1}, Lmiuix/appcompat/app/e;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    return-void
.end method

.method s0(Ljava/lang/CharSequence;)V
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    iput-object p1, p0, Lmiuix/appcompat/app/k;->E:Ljava/lang/CharSequence;

    goto/32 :goto_2

    nop

    :goto_2
    iget-object v0, p0, Lmiuix/appcompat/app/c;->b:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    goto/32 :goto_5

    nop

    :goto_3
    invoke-virtual {v0, p1}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->setWindowTitle(Ljava/lang/CharSequence;)V

    :goto_4
    goto/32 :goto_0

    nop

    :goto_5
    if-nez v0, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_3

    nop
.end method

.method protected t(Lmiuix/appcompat/internal/view/menu/c;)Z
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/c;->a:Lmiuix/appcompat/app/j;

    invoke-virtual {v0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result p1

    return p1
.end method

.method public t0()Z
    .locals 2

    iget-object v0, p0, Lmiuix/appcompat/app/k;->A:Ln9/a;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ln9/a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lmiuix/appcompat/app/k;->D:Z

    :cond_0
    return v0

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public u(ILandroid/view/MenuItem;)Z
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/k;->v:Lmiuix/appcompat/app/e;

    invoke-interface {v0, p1, p2}, Lmiuix/appcompat/app/e;->onMenuItemSelected(ILandroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 v0, 0x0

    if-eqz p1, :cond_1

    return v0

    :cond_1
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result p1

    const p2, 0x102002c

    if-ne p1, p2, :cond_3

    invoke-virtual {p0}, Lmiuix/appcompat/app/c;->k()Lmiuix/appcompat/app/a;

    move-result-object p1

    if-eqz p1, :cond_3

    invoke-virtual {p0}, Lmiuix/appcompat/app/c;->k()Lmiuix/appcompat/app/a;

    move-result-object p1

    invoke-virtual {p1}, Ld/a;->j()I

    move-result p1

    and-int/lit8 p1, p1, 0x4

    if-eqz p1, :cond_3

    iget-object p1, p0, Lmiuix/appcompat/app/c;->a:Lmiuix/appcompat/app/j;

    invoke-virtual {p1}, Landroid/app/Activity;->getParent()Landroid/app/Activity;

    move-result-object p1

    if-nez p1, :cond_2

    iget-object p1, p0, Lmiuix/appcompat/app/c;->a:Lmiuix/appcompat/app/j;

    invoke-virtual {p1}, Landroid/app/Activity;->onNavigateUp()Z

    move-result p1

    goto :goto_0

    :cond_2
    iget-object p1, p0, Lmiuix/appcompat/app/c;->a:Lmiuix/appcompat/app/j;

    invoke-virtual {p1}, Landroid/app/Activity;->getParent()Landroid/app/Activity;

    move-result-object p1

    iget-object p2, p0, Lmiuix/appcompat/app/c;->a:Lmiuix/appcompat/app/j;

    invoke-virtual {p1, p2}, Landroid/app/Activity;->onNavigateUpFromChild(Landroid/app/Activity;)Z

    move-result p1

    :goto_0
    if-nez p1, :cond_3

    iget-object p1, p0, Lmiuix/appcompat/app/c;->a:Lmiuix/appcompat/app/j;

    invoke-virtual {p1}, Lmiuix/appcompat/app/j;->finish()V

    :cond_3
    return v0
.end method

.method protected v(Lmiuix/appcompat/internal/view/menu/c;)Z
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/c;->a:Lmiuix/appcompat/app/j;

    invoke-virtual {v0, p1}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result p1

    return p1
.end method

.method public v0()V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/k;->A:Ln9/a;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ln9/a;->n()V

    :cond_0
    return-void
.end method

.method public w(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;
    .locals 1

    invoke-virtual {p0}, Lmiuix/appcompat/app/c;->k()Lmiuix/appcompat/app/a;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lmiuix/appcompat/app/c;->k()Lmiuix/appcompat/app/a;

    move-result-object v0

    check-cast v0, Lmiuix/appcompat/internal/app/widget/b;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/internal/app/widget/b;->Z(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object p1

    return-object p1

    :cond_0
    invoke-super {p0, p1}, Lmiuix/appcompat/app/c;->w(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object p1

    return-object p1
.end method

.method public w0(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;
    .locals 1

    instance-of v0, p1, Lmiuix/view/e$a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiuix/appcompat/app/k;->r:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    invoke-virtual {p0, v0}, Lmiuix/appcompat/app/c;->g(Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;)V

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/app/k;->r:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    if-eqz v0, :cond_1

    invoke-virtual {v0, p1}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object p1

    return-object p1

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method
