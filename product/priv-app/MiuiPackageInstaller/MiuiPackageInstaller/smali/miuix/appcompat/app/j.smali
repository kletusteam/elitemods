.class public Lmiuix/appcompat/app/j;
.super Landroidx/fragment/app/e;

# interfaces
.implements Lmiuix/appcompat/app/l;
.implements Lm9/d;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "MissingSuperCall"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiuix/appcompat/app/j$c;,
        Lmiuix/appcompat/app/j$b;
    }
.end annotation


# instance fields
.field private p:Lmiuix/appcompat/app/k;


# direct methods
.method public constructor <init>()V
    .locals 4

    invoke-direct {p0}, Landroidx/fragment/app/e;-><init>()V

    new-instance v0, Lmiuix/appcompat/app/k;

    new-instance v1, Lmiuix/appcompat/app/j$b;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lmiuix/appcompat/app/j$b;-><init>(Lmiuix/appcompat/app/j;Lmiuix/appcompat/app/j$a;)V

    new-instance v3, Lmiuix/appcompat/app/j$c;

    invoke-direct {v3, p0, v2}, Lmiuix/appcompat/app/j$c;-><init>(Lmiuix/appcompat/app/j;Lmiuix/appcompat/app/j$a;)V

    invoke-direct {v0, p0, v1, v3}, Lmiuix/appcompat/app/k;-><init>(Lmiuix/appcompat/app/j;Lmiuix/appcompat/app/e;Lm9/h;)V

    iput-object v0, p0, Lmiuix/appcompat/app/j;->p:Lmiuix/appcompat/app/k;

    return-void
.end method

.method static synthetic f0(Lmiuix/appcompat/app/j;Landroid/content/res/Configuration;)V
    .locals 0

    invoke-super {p0, p1}, Landroidx/fragment/app/e;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    return-void
.end method

.method static synthetic g0(Lmiuix/appcompat/app/j;Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Landroidx/activity/ComponentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method static synthetic h0(Lmiuix/appcompat/app/j;Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/app/Activity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method static synthetic i0(Lmiuix/appcompat/app/j;Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Landroidx/fragment/app/e;->onCreate(Landroid/os/Bundle;)V

    return-void
.end method

.method static synthetic j0(Lmiuix/appcompat/app/j;)V
    .locals 0

    invoke-super {p0}, Landroidx/fragment/app/e;->onPostResume()V

    return-void
.end method

.method static synthetic k0(Lmiuix/appcompat/app/j;)V
    .locals 0

    invoke-super {p0}, Landroidx/fragment/app/e;->onStop()V

    return-void
.end method

.method static synthetic l0(Lmiuix/appcompat/app/j;ILandroid/view/MenuItem;)Z
    .locals 0

    invoke-super {p0, p1, p2}, Landroidx/fragment/app/e;->onMenuItemSelected(ILandroid/view/MenuItem;)Z

    move-result p0

    return p0
.end method

.method static synthetic m0(Lmiuix/appcompat/app/j;I)Landroid/view/View;
    .locals 0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreatePanelView(I)Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method static synthetic n0(Lmiuix/appcompat/app/j;ILandroid/view/Menu;)Z
    .locals 0

    invoke-super {p0, p1, p2}, Landroidx/fragment/app/e;->onCreatePanelMenu(ILandroid/view/Menu;)Z

    move-result p0

    return p0
.end method

.method static synthetic o0(Lmiuix/appcompat/app/j;ILandroid/view/View;Landroid/view/Menu;)Z
    .locals 0

    invoke-super {p0, p1, p2, p3}, Landroidx/fragment/app/e;->onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z

    move-result p0

    return p0
.end method

.method static synthetic p0(Lmiuix/appcompat/app/j;)V
    .locals 0

    invoke-super {p0}, Landroidx/activity/ComponentActivity;->onBackPressed()V

    return-void
.end method


# virtual methods
.method public A0(Z)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/j;->p:Lmiuix/appcompat/app/k;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/app/k;->o0(Z)V

    return-void
.end method

.method public B0(Lm9/g;)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/j;->p:Lmiuix/appcompat/app/k;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/app/k;->r0(Lm9/g;)V

    return-void
.end method

.method public C0()V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/j;->p:Lmiuix/appcompat/app/k;

    invoke-virtual {v0}, Lmiuix/appcompat/app/k;->v0()V

    return-void
.end method

.method public J()V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/j;->p:Lmiuix/appcompat/app/k;

    invoke-virtual {v0}, Lmiuix/appcompat/app/k;->K()V

    return-void
.end method

.method public K()Z
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/j;->p:Lmiuix/appcompat/app/k;

    invoke-virtual {v0}, Lmiuix/appcompat/app/k;->V()Z

    move-result v0

    return v0
.end method

.method public P()V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/j;->p:Lmiuix/appcompat/app/k;

    invoke-virtual {v0}, Lmiuix/appcompat/app/k;->J()V

    return-void
.end method

.method public addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/j;->p:Lmiuix/appcompat/app/k;

    invoke-virtual {v0, p1, p2}, Lmiuix/appcompat/app/k;->F(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public finish()V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/j;->p:Lmiuix/appcompat/app/k;

    invoke-virtual {v0}, Lmiuix/appcompat/app/k;->t0()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lmiuix/appcompat/app/j;->y0()V

    :cond_0
    return-void
.end method

.method public getMenuInflater()Landroid/view/MenuInflater;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/j;->p:Lmiuix/appcompat/app/k;

    invoke-virtual {v0}, Lmiuix/appcompat/app/c;->n()Landroid/view/MenuInflater;

    move-result-object v0

    return-object v0
.end method

.method public invalidateOptionsMenu()V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/j;->p:Lmiuix/appcompat/app/k;

    invoke-virtual {v0}, Lmiuix/appcompat/app/k;->a()V

    return-void
.end method

.method public isFinishing()Z
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/j;->p:Lmiuix/appcompat/app/k;

    invoke-virtual {v0}, Lmiuix/appcompat/app/k;->U()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-super {p0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public o()V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/j;->p:Lmiuix/appcompat/app/k;

    invoke-virtual {v0}, Lmiuix/appcompat/app/k;->I()V

    return-void
.end method

.method public onActionModeFinished(Landroid/view/ActionMode;)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/j;->p:Lmiuix/appcompat/app/k;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/app/k;->Z(Landroid/view/ActionMode;)V

    return-void
.end method

.method public onActionModeStarted(Landroid/view/ActionMode;)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/j;->p:Lmiuix/appcompat/app/k;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/app/k;->a0(Landroid/view/ActionMode;)V

    return-void
.end method

.method public onBackPressed()V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/j;->p:Lmiuix/appcompat/app/k;

    invoke-virtual {v0}, Lmiuix/appcompat/app/k;->b0()V

    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/j;->p:Lmiuix/appcompat/app/k;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/app/k;->s(Landroid/content/res/Configuration;)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/j;->p:Lmiuix/appcompat/app/k;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/app/k;->c0(Landroid/os/Bundle;)V

    return-void
.end method

.method public onCreatePanelMenu(ILandroid/view/Menu;)Z
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/j;->p:Lmiuix/appcompat/app/k;

    invoke-virtual {v0, p1, p2}, Lmiuix/appcompat/app/k;->d0(ILandroid/view/Menu;)Z

    move-result p1

    return p1
.end method

.method public onCreatePanelView(I)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/j;->p:Lmiuix/appcompat/app/k;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/app/k;->e0(I)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public onMenuItemSelected(ILandroid/view/MenuItem;)Z
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/j;->p:Lmiuix/appcompat/app/k;

    invoke-virtual {v0, p1, p2}, Lmiuix/appcompat/app/k;->u(ILandroid/view/MenuItem;)Z

    move-result p1

    return p1
.end method

.method protected onPostResume()V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/j;->p:Lmiuix/appcompat/app/k;

    invoke-virtual {v0}, Lmiuix/appcompat/app/k;->f0()V

    return-void
.end method

.method public onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/j;->p:Lmiuix/appcompat/app/k;

    invoke-virtual {v0, p1, p2, p3}, Lmiuix/appcompat/app/k;->g0(ILandroid/view/View;Landroid/view/Menu;)Z

    move-result p1

    return p1
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/j;->p:Lmiuix/appcompat/app/k;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/app/k;->h0(Landroid/os/Bundle;)V

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/j;->p:Lmiuix/appcompat/app/k;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/app/k;->i0(Landroid/os/Bundle;)V

    return-void
.end method

.method protected onStop()V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/j;->p:Lmiuix/appcompat/app/k;

    invoke-virtual {v0}, Lmiuix/appcompat/app/k;->j0()V

    return-void
.end method

.method protected onTitleChanged(Ljava/lang/CharSequence;I)V
    .locals 0

    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onTitleChanged(Ljava/lang/CharSequence;I)V

    iget-object p2, p0, Lmiuix/appcompat/app/j;->p:Lmiuix/appcompat/app/k;

    invoke-virtual {p2, p1}, Lmiuix/appcompat/app/k;->s0(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public onWindowStartingActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/j;->p:Lmiuix/appcompat/app/k;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/app/k;->w(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object p1

    return-object p1
.end method

.method public onWindowStartingActionMode(Landroid/view/ActionMode$Callback;I)Landroid/view/ActionMode;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/j;->p:Lmiuix/appcompat/app/k;

    invoke-virtual {v0, p1, p2}, Lmiuix/appcompat/app/c;->x(Landroid/view/ActionMode$Callback;I)Landroid/view/ActionMode;

    move-result-object p1

    return-object p1
.end method

.method public q0()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/j;->p:Lmiuix/appcompat/app/k;

    invoke-virtual {v0}, Lmiuix/appcompat/app/k;->L()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public r0()Lmiuix/appcompat/app/a;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/j;->p:Lmiuix/appcompat/app/k;

    invoke-virtual {v0}, Lmiuix/appcompat/app/c;->k()Lmiuix/appcompat/app/a;

    move-result-object v0

    return-object v0
.end method

.method public s0()I
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/j;->p:Lmiuix/appcompat/app/k;

    invoke-virtual {v0}, Lmiuix/appcompat/app/k;->N()I

    move-result v0

    return v0
.end method

.method public setContentView(I)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/j;->p:Lmiuix/appcompat/app/k;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/app/k;->k0(I)V

    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/j;->p:Lmiuix/appcompat/app/k;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/app/k;->l0(Landroid/view/View;)V

    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/j;->p:Lmiuix/appcompat/app/k;

    invoke-virtual {v0, p1, p2}, Lmiuix/appcompat/app/k;->m0(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/j;->p:Lmiuix/appcompat/app/k;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/app/k;->w0(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object p1

    return-object p1
.end method

.method public t0()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/j;->p:Lmiuix/appcompat/app/k;

    invoke-virtual {v0}, Lmiuix/appcompat/app/k;->O()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public u0()V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/j;->p:Lmiuix/appcompat/app/k;

    invoke-virtual {v0}, Lmiuix/appcompat/app/k;->P()V

    return-void
.end method

.method public v0()V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/j;->p:Lmiuix/appcompat/app/k;

    invoke-virtual {v0}, Lmiuix/appcompat/app/k;->Q()V

    return-void
.end method

.method public w0(Z)V
    .locals 0

    return-void
.end method

.method public x0(Z)Z
    .locals 0

    const/4 p1, 0x1

    return p1
.end method

.method public y0()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method public z0(Z)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/app/j;->p:Lmiuix/appcompat/app/k;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/app/k;->n0(Z)V

    return-void
.end method
