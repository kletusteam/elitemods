.class Lmiuix/appcompat/widget/Spinner$f;
.super Lja/e;

# interfaces
.implements Lmiuix/appcompat/widget/Spinner$j;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiuix/appcompat/widget/Spinner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "f"
.end annotation


# instance fields
.field private A:I

.field private B:I

.field private C:I

.field private D:I

.field private E:Landroid/view/View;

.field private F:I

.field private G:I

.field final synthetic H:Lmiuix/appcompat/widget/Spinner;

.field private x:Ljava/lang/CharSequence;

.field y:Landroid/widget/ListAdapter;

.field private final z:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Lmiuix/appcompat/widget/Spinner;Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/widget/Spinner$f;->H:Lmiuix/appcompat/widget/Spinner;

    invoke-direct {p0, p2}, Lja/e;-><init>(Landroid/content/Context;)V

    new-instance p3, Landroid/graphics/Rect;

    invoke-direct {p3}, Landroid/graphics/Rect;-><init>()V

    iput-object p3, p0, Lmiuix/appcompat/widget/Spinner$f;->z:Landroid/graphics/Rect;

    const/4 p3, -0x1

    iput p3, p0, Lmiuix/appcompat/widget/Spinner$f;->D:I

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    sget p4, Lk9/e;->X:I

    invoke-virtual {p3, p4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p3

    iput p3, p0, Lmiuix/appcompat/widget/Spinner$f;->B:I

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    sget p4, Lk9/e;->Z:I

    invoke-virtual {p3, p4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p3

    iput p3, p0, Lmiuix/appcompat/widget/Spinner$f;->F:I

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    sget p4, Lk9/e;->a0:I

    invoke-virtual {p3, p4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p3

    iput p3, p0, Lmiuix/appcompat/widget/Spinner$f;->G:I

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    sget p3, Lk9/e;->Y:I

    invoke-virtual {p2, p3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p2

    iput p2, p0, Lmiuix/appcompat/widget/Spinner$f;->C:I

    const p2, 0x800033

    invoke-virtual {p0, p2}, Lja/e;->N(I)V

    new-instance p2, Lmiuix/appcompat/widget/Spinner$f$a;

    invoke-direct {p2, p0, p1}, Lmiuix/appcompat/widget/Spinner$f$a;-><init>(Lmiuix/appcompat/widget/Spinner$f;Lmiuix/appcompat/widget/Spinner;)V

    invoke-virtual {p0, p2}, Lja/e;->Q(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method

.method private V(Landroid/view/View;)V
    .locals 8

    const/4 v0, 0x2

    new-array v0, v0, [I

    invoke-virtual {p1, v0}, Landroid/view/View;->getLocationInWindow([I)V

    const/4 v1, 0x0

    aget v2, v0, v1

    const/4 v3, 0x1

    aget v4, v0, v3

    iget-object v5, p0, Lmiuix/appcompat/widget/Spinner$f;->E:Landroid/view/View;

    if-nez v5, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v5

    :cond_0
    invoke-virtual {v5, v0}, Landroid/view/View;->getLocationInWindow([I)V

    aget v6, v0, v1

    aget v0, v0, v3

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v3

    invoke-virtual {v5}, Landroid/view/View;->getWidth()I

    move-result v7

    invoke-direct {p0, v2, v3, v6, v7}, Lmiuix/appcompat/widget/Spinner$f;->a0(IIII)I

    move-result v2

    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    move-result v3

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v5

    invoke-direct {p0, v0, v3, v4, v5}, Lmiuix/appcompat/widget/Spinner$f;->b0(IIII)F

    move-result v0

    invoke-virtual {p0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v3

    if-nez v3, :cond_1

    float-to-int v0, v0

    invoke-virtual {p0, p1, v1, v2, v0}, Lja/e;->showAtLocation(Landroid/view/View;III)V

    iget-object p1, p0, Lja/e;->g:Landroid/widget/FrameLayout;

    invoke-virtual {p1}, Landroid/widget/FrameLayout;->getRootView()Landroid/view/View;

    move-result-object p1

    invoke-static {p1}, Lja/e;->x(Landroid/view/View;)V

    goto :goto_0

    :cond_1
    float-to-int p1, v0

    invoke-virtual {p0}, Landroid/widget/PopupWindow;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Landroid/widget/PopupWindow;->getHeight()I

    move-result v1

    invoke-virtual {p0, v2, p1, v0, v1}, Landroid/widget/PopupWindow;->update(IIII)V

    :goto_0
    return-void
.end method

.method private W()V
    .locals 2

    iget-object v0, p0, Lmiuix/appcompat/widget/Spinner$f;->E:Landroid/view/View;

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/widget/Spinner$f;->H:Lmiuix/appcompat/widget/Spinner;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    instance-of v1, v1, Lmiuix/appcompat/app/l;

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lmiuix/appcompat/app/l;

    invoke-interface {v1}, Lmiuix/appcompat/app/l;->K()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    sget v1, Lk9/g;->j:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmiuix/appcompat/widget/Spinner$f;->d0(Landroid/view/View;)V

    :cond_1
    return-void
.end method

.method private Z()I
    .locals 7

    invoke-virtual {p0}, Lja/e;->C()Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lja/e;->C()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    move v2, v1

    move v3, v2

    :goto_0
    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v4

    if-ge v2, v4, :cond_1

    const/4 v4, 0x0

    invoke-virtual {p0}, Lja/e;->C()Landroid/widget/ListView;

    move-result-object v5

    invoke-interface {v0, v2, v4, v5}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/view/View;->measure(II)V

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v3, v4

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lja/e;->h:Landroid/view/View;

    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/view/View;->measure(II)V

    iget-object v0, p0, Lja/e;->h:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    add-int/lit8 v3, v0, 0x0

    :cond_1
    return v3
.end method

.method private a0(IIII)I
    .locals 9

    invoke-virtual {p0}, Landroid/widget/PopupWindow;->getWidth()I

    move-result v0

    iget v1, p0, Lmiuix/appcompat/widget/Spinner$f;->G:I

    if-le v0, v1, :cond_0

    invoke-virtual {p0, v1}, Landroid/widget/PopupWindow;->setWidth(I)V

    iget v0, p0, Lmiuix/appcompat/widget/Spinner$f;->G:I

    :cond_0
    add-int v1, p1, p2

    add-int v2, p3, p4

    add-int v3, p1, v0

    iget v4, p0, Lmiuix/appcompat/widget/Spinner$f;->B:I

    mul-int/lit8 v5, v4, 0x2

    add-int/2addr v3, v5

    const/4 v5, 0x1

    const/4 v6, 0x0

    if-gt v3, v2, :cond_1

    move v3, v5

    goto :goto_0

    :cond_1
    move v3, v6

    :goto_0
    sub-int v7, v1, v0

    mul-int/lit8 v8, v4, 0x2

    sub-int v8, v7, v8

    if-lt v8, p3, :cond_2

    goto :goto_1

    :cond_2
    move v5, v6

    :goto_1
    if-eqz v3, :cond_3

    add-int p2, p3, v4

    if-ge p1, p2, :cond_7

    :goto_2
    add-int p1, p3, v4

    goto :goto_4

    :cond_3
    if-eqz v5, :cond_5

    sub-int p1, v2, v4

    if-le v1, p1, :cond_4

    goto :goto_3

    :cond_4
    move p1, v7

    goto :goto_4

    :cond_5
    sub-int p1, v2, v1

    sub-int/2addr p4, p2

    div-int/lit8 p4, p4, 0x2

    if-lt p1, p4, :cond_6

    goto :goto_2

    :cond_6
    :goto_3
    sub-int/2addr v2, v4

    sub-int p1, v2, v0

    :cond_7
    :goto_4
    return p1
.end method

.method private b0(IIII)F
    .locals 5

    invoke-direct {p0}, Lmiuix/appcompat/widget/Spinner$f;->Z()I

    move-result v0

    iget v1, p0, Lmiuix/appcompat/widget/Spinner$f;->F:I

    if-le v0, v1, :cond_0

    invoke-virtual {p0, v1}, Landroid/widget/PopupWindow;->setHeight(I)V

    iget v0, p0, Lmiuix/appcompat/widget/Spinner$f;->F:I

    :cond_0
    add-int v1, p1, p2

    add-int/2addr p4, p3

    add-int v2, p4, v0

    iget v3, p0, Lmiuix/appcompat/widget/Spinner$f;->C:I

    sub-int v4, v1, v3

    if-ge v2, v4, :cond_1

    int-to-float p2, p4

    add-int p3, p1, v3

    if-ge p4, p3, :cond_6

    add-int/2addr p1, v3

    int-to-float p2, p1

    goto :goto_1

    :cond_1
    sub-int v2, p3, v0

    add-int v4, p1, v3

    if-le v2, v4, :cond_2

    int-to-float p2, v2

    sub-int p1, v1, v3

    if-le p3, p1, :cond_6

    sub-int/2addr v1, v3

    sub-int/2addr v1, v0

    int-to-float p2, v1

    goto :goto_1

    :cond_2
    add-int v2, p1, v3

    if-ge p4, v2, :cond_3

    add-int/2addr p1, v3

    int-to-float p1, p1

    :goto_0
    mul-int/lit8 v3, v3, 0x2

    sub-int/2addr p2, v3

    int-to-float p2, p2

    float-to-int p2, p2

    invoke-virtual {p0, p2}, Landroid/widget/PopupWindow;->setHeight(I)V

    move p2, p1

    goto :goto_1

    :cond_3
    sub-int v2, v1, v3

    if-le p3, v2, :cond_4

    sub-int/2addr v1, v3

    sub-int/2addr v1, v0

    int-to-float p1, v1

    goto :goto_0

    :cond_4
    div-int/lit8 p2, p2, 0x2

    if-ge p3, p2, :cond_5

    int-to-float p2, p4

    sub-int/2addr v1, v3

    sub-int/2addr v1, p4

    int-to-float p1, v1

    float-to-int p1, p1

    invoke-virtual {p0, p1}, Landroid/widget/PopupWindow;->setHeight(I)V

    goto :goto_1

    :cond_5
    sub-int p2, p3, v3

    sub-int/2addr p2, p1

    int-to-float p1, p2

    float-to-int p2, p1

    invoke-virtual {p0, p2}, Landroid/widget/PopupWindow;->setHeight(I)V

    int-to-float p2, p3

    sub-float/2addr p2, p1

    :cond_6
    :goto_1
    invoke-virtual {p0}, Lja/e;->F()Z

    move-result p1

    invoke-virtual {p0, p1}, Lja/e;->T(Z)V

    return p2
.end method

.method private c0(II)V
    .locals 2

    invoke-virtual {p0}, Lja/e;->C()Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setChoiceMode(I)V

    invoke-virtual {v0, p1}, Landroid/widget/ListView;->setTextDirection(I)V

    invoke-virtual {v0, p2}, Landroid/widget/ListView;->setTextAlignment(I)V

    iget-object p1, p0, Lmiuix/appcompat/widget/Spinner$f;->H:Lmiuix/appcompat/widget/Spinner;

    invoke-virtual {p1}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result p1

    invoke-virtual {v0, p1}, Landroid/widget/ListView;->setSelection(I)V

    invoke-virtual {v0, p1, v1}, Landroid/widget/ListView;->setItemChecked(IZ)V

    return-void
.end method


# virtual methods
.method public M(I)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/widget/Spinner$f;->H:Lmiuix/appcompat/widget/Spinner;

    iget v0, v0, Lmiuix/appcompat/widget/Spinner;->g:I

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result p1

    iget-object v0, p0, Lmiuix/appcompat/widget/Spinner$f;->H:Lmiuix/appcompat/widget/Spinner;

    iget v0, v0, Lmiuix/appcompat/widget/Spinner;->f:I

    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result p1

    invoke-super {p0, p1}, Lja/e;->M(I)V

    return-void
.end method

.method X()V
    .locals 8

    goto/32 :goto_5

    nop

    :goto_0
    if-gt v4, v5, :cond_0

    goto/32 :goto_14

    :cond_0
    goto/32 :goto_13

    nop

    :goto_1
    invoke-virtual {p0}, Lmiuix/appcompat/widget/Spinner$f;->Y()I

    move-result v2

    goto/32 :goto_32

    nop

    :goto_2
    iget-object v1, p0, Lmiuix/appcompat/widget/Spinner$f;->H:Lmiuix/appcompat/widget/Spinner;

    goto/32 :goto_12

    nop

    :goto_3
    invoke-virtual {v3}, Landroid/widget/Spinner;->getWidth()I

    move-result v3

    goto/32 :goto_19

    nop

    :goto_4
    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    goto/32 :goto_10

    nop

    :goto_5
    invoke-virtual {p0}, Landroid/widget/PopupWindow;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto/32 :goto_34

    nop

    :goto_6
    return-void

    :goto_7
    iget-object v0, p0, Lmiuix/appcompat/widget/Spinner$f;->H:Lmiuix/appcompat/widget/Spinner;

    goto/32 :goto_e

    nop

    :goto_8
    sub-int/2addr v5, v2

    goto/32 :goto_11

    nop

    :goto_9
    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v4

    goto/32 :goto_47

    nop

    :goto_a
    iget v6, v6, Landroid/graphics/Rect;->right:I

    goto/32 :goto_1a

    nop

    :goto_b
    invoke-virtual {p0, v1}, Lja/e;->f(I)V

    goto/32 :goto_6

    nop

    :goto_c
    sub-int/2addr v3, v0

    goto/32 :goto_56

    nop

    :goto_d
    iget-object v0, v0, Lmiuix/appcompat/widget/Spinner;->j:Landroid/graphics/Rect;

    goto/32 :goto_3f

    nop

    :goto_e
    invoke-virtual {v0}, Landroid/widget/Spinner;->getPaddingLeft()I

    move-result v0

    goto/32 :goto_1c

    nop

    :goto_f
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    goto/32 :goto_18

    nop

    :goto_10
    iget v5, v5, Landroid/util/DisplayMetrics;->widthPixels:I

    goto/32 :goto_15

    nop

    :goto_11
    mul-int/lit8 v6, v6, 0x2

    goto/32 :goto_58

    nop

    :goto_12
    iget-object v1, v1, Lmiuix/appcompat/widget/Spinner;->j:Landroid/graphics/Rect;

    goto/32 :goto_f

    nop

    :goto_13
    move v4, v5

    :goto_14
    goto/32 :goto_23

    nop

    :goto_15
    iget-object v6, p0, Lmiuix/appcompat/widget/Spinner$f;->H:Lmiuix/appcompat/widget/Spinner;

    goto/32 :goto_21

    nop

    :goto_16
    add-int/2addr v1, v3

    goto/32 :goto_51

    nop

    :goto_17
    iget v0, v0, Landroid/graphics/Rect;->right:I

    goto/32 :goto_59

    nop

    :goto_18
    iget-object v0, p0, Lmiuix/appcompat/widget/Spinner$f;->H:Lmiuix/appcompat/widget/Spinner;

    goto/32 :goto_1f

    nop

    :goto_19
    iget-object v4, p0, Lmiuix/appcompat/widget/Spinner$f;->H:Lmiuix/appcompat/widget/Spinner;

    goto/32 :goto_5c

    nop

    :goto_1a
    sub-int/2addr v5, v6

    goto/32 :goto_55

    nop

    :goto_1b
    invoke-virtual {p0, v4}, Lmiuix/appcompat/widget/Spinner$f;->M(I)V

    goto/32 :goto_28

    nop

    :goto_1c
    iget-object v2, p0, Lmiuix/appcompat/widget/Spinner$f;->H:Lmiuix/appcompat/widget/Spinner;

    goto/32 :goto_4c

    nop

    :goto_1d
    invoke-virtual {p0, v5}, Lmiuix/appcompat/widget/Spinner$f;->M(I)V

    :goto_1e
    goto/32 :goto_50

    nop

    :goto_1f
    invoke-static {v0}, Landroidx/appcompat/widget/s0;->b(Landroid/view/View;)Z

    move-result v0

    goto/32 :goto_44

    nop

    :goto_20
    if-nez v0, :cond_1

    goto/32 :goto_3b

    :cond_1
    goto/32 :goto_2

    nop

    :goto_21
    iget-object v6, v6, Lmiuix/appcompat/widget/Spinner;->j:Landroid/graphics/Rect;

    goto/32 :goto_4e

    nop

    :goto_22
    sub-int/2addr v5, v7

    goto/32 :goto_a

    nop

    :goto_23
    sub-int v5, v3, v0

    goto/32 :goto_8

    nop

    :goto_24
    mul-int/lit8 v5, v5, 0x2

    goto/32 :goto_2e

    nop

    :goto_25
    iget-object v3, p0, Lmiuix/appcompat/widget/Spinner$f;->H:Lmiuix/appcompat/widget/Spinner;

    goto/32 :goto_3

    nop

    :goto_26
    const/4 v4, -0x1

    goto/32 :goto_3d

    nop

    :goto_27
    sub-int/2addr v4, v2

    goto/32 :goto_54

    nop

    :goto_28
    goto :goto_1e

    :goto_29
    goto/32 :goto_1d

    nop

    :goto_2a
    invoke-virtual {p0}, Landroid/widget/PopupWindow;->getWidth()I

    move-result v0

    goto/32 :goto_c

    nop

    :goto_2b
    iput v1, v0, Landroid/graphics/Rect;->left:I

    :goto_2c
    goto/32 :goto_7

    nop

    :goto_2d
    invoke-static {v4}, Landroidx/appcompat/widget/s0;->b(Landroid/view/View;)Z

    move-result v4

    goto/32 :goto_33

    nop

    :goto_2e
    sub-int/2addr v4, v5

    :goto_2f
    goto/32 :goto_1b

    nop

    :goto_30
    const/4 v6, -0x2

    goto/32 :goto_4f

    nop

    :goto_31
    invoke-virtual {p0}, Landroid/widget/PopupWindow;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v6

    goto/32 :goto_4a

    nop

    :goto_32
    add-int/2addr v0, v2

    goto/32 :goto_45

    nop

    :goto_33
    if-nez v4, :cond_2

    goto/32 :goto_52

    :cond_2
    goto/32 :goto_39

    nop

    :goto_34
    const/4 v1, 0x0

    goto/32 :goto_20

    nop

    :goto_35
    iget-object v5, p0, Lmiuix/appcompat/widget/Spinner$f;->H:Lmiuix/appcompat/widget/Spinner;

    goto/32 :goto_4b

    nop

    :goto_36
    iget-object v0, v0, Lmiuix/appcompat/widget/Spinner;->j:Landroid/graphics/Rect;

    goto/32 :goto_41

    nop

    :goto_37
    sub-int v4, v3, v0

    goto/32 :goto_27

    nop

    :goto_38
    sub-int/2addr v3, v0

    goto/32 :goto_16

    nop

    :goto_39
    sub-int/2addr v3, v2

    goto/32 :goto_2a

    nop

    :goto_3a
    goto :goto_2c

    :goto_3b
    goto/32 :goto_57

    nop

    :goto_3c
    iget-object v5, p0, Lmiuix/appcompat/widget/Spinner$f;->y:Landroid/widget/ListAdapter;

    goto/32 :goto_4d

    nop

    :goto_3d
    if-eq v5, v4, :cond_3

    goto/32 :goto_29

    :cond_3
    goto/32 :goto_37

    nop

    :goto_3e
    move v1, v0

    goto/32 :goto_3a

    nop

    :goto_3f
    iput v1, v0, Landroid/graphics/Rect;->right:I

    goto/32 :goto_2b

    nop

    :goto_40
    iget-object v0, p0, Lmiuix/appcompat/widget/Spinner$f;->H:Lmiuix/appcompat/widget/Spinner;

    goto/32 :goto_36

    nop

    :goto_41
    iget v0, v0, Landroid/graphics/Rect;->left:I

    goto/32 :goto_42

    nop

    :goto_42
    neg-int v0, v0

    :goto_43
    goto/32 :goto_3e

    nop

    :goto_44
    if-nez v0, :cond_4

    goto/32 :goto_5a

    :cond_4
    goto/32 :goto_53

    nop

    :goto_45
    add-int/2addr v1, v0

    :goto_46
    goto/32 :goto_b

    nop

    :goto_47
    goto :goto_2f

    :goto_48
    goto/32 :goto_26

    nop

    :goto_49
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    goto/32 :goto_4

    nop

    :goto_4a
    invoke-virtual {v4, v5, v6}, Lmiuix/appcompat/widget/Spinner;->g(Landroid/widget/SpinnerAdapter;Landroid/graphics/drawable/Drawable;)I

    move-result v4

    goto/32 :goto_35

    nop

    :goto_4b
    invoke-virtual {v5}, Landroid/widget/Spinner;->getContext()Landroid/content/Context;

    move-result-object v5

    goto/32 :goto_49

    nop

    :goto_4c
    invoke-virtual {v2}, Landroid/widget/Spinner;->getPaddingRight()I

    move-result v2

    goto/32 :goto_25

    nop

    :goto_4d
    check-cast v5, Landroid/widget/SpinnerAdapter;

    goto/32 :goto_31

    nop

    :goto_4e
    iget v7, v6, Landroid/graphics/Rect;->left:I

    goto/32 :goto_22

    nop

    :goto_4f
    if-eq v5, v6, :cond_5

    goto/32 :goto_48

    :cond_5
    goto/32 :goto_3c

    nop

    :goto_50
    iget-object v4, p0, Lmiuix/appcompat/widget/Spinner$f;->H:Lmiuix/appcompat/widget/Spinner;

    goto/32 :goto_2d

    nop

    :goto_51
    goto :goto_46

    :goto_52
    goto/32 :goto_1

    nop

    :goto_53
    iget-object v0, p0, Lmiuix/appcompat/widget/Spinner$f;->H:Lmiuix/appcompat/widget/Spinner;

    goto/32 :goto_5e

    nop

    :goto_54
    iget v5, p0, Lmiuix/appcompat/widget/Spinner$f;->B:I

    goto/32 :goto_24

    nop

    :goto_55
    iget v6, p0, Lmiuix/appcompat/widget/Spinner$f;->B:I

    goto/32 :goto_5d

    nop

    :goto_56
    invoke-virtual {p0}, Lmiuix/appcompat/widget/Spinner$f;->Y()I

    move-result v0

    goto/32 :goto_38

    nop

    :goto_57
    iget-object v0, p0, Lmiuix/appcompat/widget/Spinner$f;->H:Lmiuix/appcompat/widget/Spinner;

    goto/32 :goto_d

    nop

    :goto_58
    sub-int/2addr v5, v6

    goto/32 :goto_9

    nop

    :goto_59
    goto :goto_43

    :goto_5a
    goto/32 :goto_40

    nop

    :goto_5b
    sub-int/2addr v5, v7

    goto/32 :goto_0

    nop

    :goto_5c
    iget v5, v4, Lmiuix/appcompat/widget/Spinner;->e:I

    goto/32 :goto_30

    nop

    :goto_5d
    mul-int/lit8 v7, v6, 0x2

    goto/32 :goto_5b

    nop

    :goto_5e
    iget-object v0, v0, Lmiuix/appcompat/widget/Spinner;->j:Landroid/graphics/Rect;

    goto/32 :goto_17

    nop
.end method

.method public Y()I
    .locals 1

    iget v0, p0, Lmiuix/appcompat/widget/Spinner$f;->A:I

    return v0
.end method

.method public c(Ljava/lang/CharSequence;)V
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/widget/Spinner$f;->x:Ljava/lang/CharSequence;

    return-void
.end method

.method public d0(Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/widget/Spinner$f;->E:Landroid/view/View;

    return-void
.end method

.method public e(I)V
    .locals 0

    iput p1, p0, Lmiuix/appcompat/widget/Spinner$f;->A:I

    return-void
.end method

.method public e0(I)V
    .locals 0

    iput p1, p0, Lmiuix/appcompat/widget/Spinner$f;->D:I

    return-void
.end method

.method public h()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/widget/Spinner$f;->x:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public i(Landroid/widget/ListAdapter;)V
    .locals 0

    invoke-super {p0, p1}, Lja/e;->i(Landroid/widget/ListAdapter;)V

    iput-object p1, p0, Lmiuix/appcompat/widget/Spinner$f;->y:Landroid/widget/ListAdapter;

    return-void
.end method

.method public m(IIFF)V
    .locals 1

    invoke-direct {p0}, Lmiuix/appcompat/widget/Spinner$f;->W()V

    invoke-virtual {p0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result p3

    invoke-virtual {p0}, Lmiuix/appcompat/widget/Spinner$f;->X()V

    const/4 p4, 0x2

    invoke-virtual {p0, p4}, Landroid/widget/PopupWindow;->setInputMethodMode(I)V

    iget-object p4, p0, Lmiuix/appcompat/widget/Spinner$f;->H:Lmiuix/appcompat/widget/Spinner;

    const/4 v0, 0x0

    invoke-virtual {p0, p4, v0}, Lja/e;->L(Landroid/view/View;Landroid/view/ViewGroup;)Z

    move-result p4

    if-eqz p4, :cond_0

    iget-object p4, p0, Lmiuix/appcompat/widget/Spinner$f;->H:Lmiuix/appcompat/widget/Spinner;

    invoke-direct {p0, p4}, Lmiuix/appcompat/widget/Spinner$f;->V(Landroid/view/View;)V

    :cond_0
    invoke-direct {p0, p1, p2}, Lmiuix/appcompat/widget/Spinner$f;->c0(II)V

    if-eqz p3, :cond_1

    return-void

    :cond_1
    new-instance p1, Lmiuix/appcompat/widget/Spinner$f$b;

    invoke-direct {p1, p0}, Lmiuix/appcompat/widget/Spinner$f$b;-><init>(Lmiuix/appcompat/widget/Spinner$f;)V

    invoke-virtual {p0, p1}, Lja/e;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    return-void
.end method
