.class Lmiuix/appcompat/internal/view/menu/b$a;
.super Landroid/widget/BaseAdapter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiuix/appcompat/internal/view/menu/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field private a:I

.field final synthetic b:Lmiuix/appcompat/internal/view/menu/b;


# direct methods
.method public constructor <init>(Lmiuix/appcompat/internal/view/menu/b;)V
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/b$a;->b:Lmiuix/appcompat/internal/view/menu/b;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    const/4 p1, -0x1

    iput p1, p0, Lmiuix/appcompat/internal/view/menu/b$a;->a:I

    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/b$a;->a()V

    return-void
.end method


# virtual methods
.method a()V
    .locals 5

    goto/32 :goto_d

    nop

    :goto_0
    iput v3, p0, Lmiuix/appcompat/internal/view/menu/b$a;->a:I

    goto/32 :goto_13

    nop

    :goto_1
    return-void

    :goto_2
    if-eq v4, v0, :cond_0

    goto/32 :goto_14

    :cond_0
    goto/32 :goto_0

    nop

    :goto_3
    invoke-virtual {v0}, Lmiuix/appcompat/internal/view/menu/c;->s()Lmiuix/appcompat/internal/view/menu/e;

    move-result-object v0

    goto/32 :goto_16

    nop

    :goto_4
    const/4 v3, 0x0

    :goto_5
    goto/32 :goto_7

    nop

    :goto_6
    iget-object v1, p0, Lmiuix/appcompat/internal/view/menu/b$a;->b:Lmiuix/appcompat/internal/view/menu/b;

    goto/32 :goto_8

    nop

    :goto_7
    if-lt v3, v2, :cond_1

    goto/32 :goto_b

    :cond_1
    goto/32 :goto_11

    nop

    :goto_8
    iget-object v1, v1, Lmiuix/appcompat/internal/view/menu/b;->c:Lmiuix/appcompat/internal/view/menu/c;

    goto/32 :goto_e

    nop

    :goto_9
    check-cast v4, Lmiuix/appcompat/internal/view/menu/e;

    goto/32 :goto_2

    nop

    :goto_a
    goto :goto_5

    :goto_b
    goto/32 :goto_10

    nop

    :goto_c
    iget-object v0, v0, Lmiuix/appcompat/internal/view/menu/b;->c:Lmiuix/appcompat/internal/view/menu/c;

    goto/32 :goto_3

    nop

    :goto_d
    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/b$a;->b:Lmiuix/appcompat/internal/view/menu/b;

    goto/32 :goto_c

    nop

    :goto_e
    invoke-virtual {v1}, Lmiuix/appcompat/internal/view/menu/c;->w()Ljava/util/ArrayList;

    move-result-object v1

    goto/32 :goto_f

    nop

    :goto_f
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    goto/32 :goto_4

    nop

    :goto_10
    const/4 v0, -0x1

    goto/32 :goto_15

    nop

    :goto_11
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    goto/32 :goto_9

    nop

    :goto_12
    add-int/lit8 v3, v3, 0x1

    goto/32 :goto_a

    nop

    :goto_13
    return-void

    :goto_14
    goto/32 :goto_12

    nop

    :goto_15
    iput v0, p0, Lmiuix/appcompat/internal/view/menu/b$a;->a:I

    goto/32 :goto_1

    nop

    :goto_16
    if-nez v0, :cond_2

    goto/32 :goto_b

    :cond_2
    goto/32 :goto_6

    nop
.end method

.method public b(I)Lmiuix/appcompat/internal/view/menu/e;
    .locals 2

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/b$a;->b:Lmiuix/appcompat/internal/view/menu/b;

    iget-object v0, v0, Lmiuix/appcompat/internal/view/menu/b;->c:Lmiuix/appcompat/internal/view/menu/c;

    invoke-virtual {v0}, Lmiuix/appcompat/internal/view/menu/c;->w()Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lmiuix/appcompat/internal/view/menu/b$a;->b:Lmiuix/appcompat/internal/view/menu/b;

    invoke-static {v1}, Lmiuix/appcompat/internal/view/menu/b;->a(Lmiuix/appcompat/internal/view/menu/b;)I

    move-result v1

    add-int/2addr p1, v1

    iget v1, p0, Lmiuix/appcompat/internal/view/menu/b$a;->a:I

    if-ltz v1, :cond_0

    if-lt p1, v1, :cond_0

    add-int/lit8 p1, p1, 0x1

    :cond_0
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lmiuix/appcompat/internal/view/menu/e;

    return-object p1
.end method

.method public getCount()I
    .locals 2

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/b$a;->b:Lmiuix/appcompat/internal/view/menu/b;

    iget-object v0, v0, Lmiuix/appcompat/internal/view/menu/b;->c:Lmiuix/appcompat/internal/view/menu/c;

    invoke-virtual {v0}, Lmiuix/appcompat/internal/view/menu/c;->w()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v1, p0, Lmiuix/appcompat/internal/view/menu/b$a;->b:Lmiuix/appcompat/internal/view/menu/b;

    invoke-static {v1}, Lmiuix/appcompat/internal/view/menu/b;->a(Lmiuix/appcompat/internal/view/menu/b;)I

    move-result v1

    sub-int/2addr v0, v1

    iget v1, p0, Lmiuix/appcompat/internal/view/menu/b$a;->a:I

    if-gez v1, :cond_0

    return v0

    :cond_0
    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 0

    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/view/menu/b$a;->b(I)Lmiuix/appcompat/internal/view/menu/e;

    move-result-object p1

    return-object p1
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    const/4 v0, 0x0

    if-nez p2, :cond_0

    iget-object p2, p0, Lmiuix/appcompat/internal/view/menu/b$a;->b:Lmiuix/appcompat/internal/view/menu/b;

    iget-object v1, p2, Lmiuix/appcompat/internal/view/menu/b;->b:Landroid/view/LayoutInflater;

    iget p2, p2, Lmiuix/appcompat/internal/view/menu/b;->g:I

    invoke-virtual {v1, p2, p3, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    invoke-static {p2}, Lia/c;->c(Landroid/view/View;)V

    :cond_0
    move-object p3, p2

    check-cast p3, Lmiuix/appcompat/internal/view/menu/h$a;

    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/view/menu/b$a;->b(I)Lmiuix/appcompat/internal/view/menu/e;

    move-result-object p1

    invoke-interface {p3, p1, v0}, Lmiuix/appcompat/internal/view/menu/h$a;->b(Lmiuix/appcompat/internal/view/menu/e;I)V

    return-object p2
.end method

.method public notifyDataSetChanged()V
    .locals 0

    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/b$a;->a()V

    invoke-super {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    return-void
.end method
