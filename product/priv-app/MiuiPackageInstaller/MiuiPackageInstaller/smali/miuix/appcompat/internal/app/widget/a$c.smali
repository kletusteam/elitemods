.class public Lmiuix/appcompat/internal/app/widget/a$c;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiuix/appcompat/internal/app/widget/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "c"
.end annotation


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private b:Z

.field private c:F

.field private d:Z


# direct methods
.method protected constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/a$c;->a:Ljava/util/List;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/a$c;->b:Z

    return-void
.end method


# virtual methods
.method public a(FIILc9/a;)V
    .locals 6

    iget-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/a$c;->d:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/a$c;->b:Z

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    iget p1, p0, Lmiuix/appcompat/internal/app/widget/a$c;->c:F

    :goto_0
    new-instance v0, Ld9/a;

    const-string v1, "to"

    invoke-direct {v0, v1}, Ld9/a;-><init>(Ljava/lang/Object;)V

    sget-object v1, Lh9/h;->o:Lh9/h;

    float-to-double v2, p1

    invoke-virtual {v0, v1, v2, v3}, Ld9/a;->a(Ljava/lang/Object;D)Ld9/a;

    move-result-object v0

    sget-object v1, Lh9/h;->b:Lh9/h;

    int-to-double v2, p2

    invoke-virtual {v0, v1, v2, v3}, Ld9/a;->a(Ljava/lang/Object;D)Ld9/a;

    move-result-object v0

    sget-object v1, Lh9/h;->c:Lh9/h;

    int-to-double v2, p3

    invoke-virtual {v0, v1, v2, v3}, Ld9/a;->a(Ljava/lang/Object;D)Ld9/a;

    move-result-object v0

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/a$c;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->isAttachedToWindow()Z

    move-result v3

    if-nez v3, :cond_3

    goto :goto_1

    :cond_3
    invoke-virtual {v2}, Landroid/view/View;->getAlpha()F

    move-result v3

    cmpl-float v3, v3, p1

    if-nez v3, :cond_4

    invoke-virtual {v2}, Landroid/view/View;->getTranslationX()F

    move-result v3

    int-to-float v4, p2

    cmpl-float v3, v3, v4

    if-nez v3, :cond_4

    invoke-virtual {v2}, Landroid/view/View;->getTranslationY()F

    move-result v3

    int-to-float v4, p3

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_2

    :cond_4
    const/4 v3, 0x1

    new-array v4, v3, [Landroid/view/View;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    invoke-static {v4}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object v2

    invoke-interface {v2}, Lmiuix/animation/e;->b()Lmiuix/animation/h;

    move-result-object v2

    new-array v3, v3, [Lc9/a;

    aput-object p4, v3, v5

    invoke-interface {v2, v0, v3}, Lmiuix/animation/h;->s(Ljava/lang/Object;[Lc9/a;)Lmiuix/animation/h;

    goto :goto_1

    :cond_5
    return-void
.end method

.method public b(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/a$c;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Lmiuix/appcompat/internal/app/widget/a$c$a;

    invoke-direct {v0, p0}, Lmiuix/appcompat/internal/app/widget/a$c$a;-><init>(Lmiuix/appcompat/internal/app/widget/a$c;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/a$c;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public c(Landroid/view/View;)V
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/a$c;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/a$c;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public d()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/a$c;->d:Z

    return-void
.end method

.method public e()V
    .locals 5

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/a$c;->d:Z

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/a$c;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    new-array v3, v0, [Landroid/view/View;

    const/4 v4, 0x0

    aput-object v2, v3, v4

    invoke-static {v3}, Lmiuix/animation/a;->f([Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public f()V
    .locals 3

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/a$c;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->clearFocus()V

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setEnabled(Z)V

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public g()V
    .locals 3

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/a$c;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public h(Z)V
    .locals 0

    iput-boolean p1, p0, Lmiuix/appcompat/internal/app/widget/a$c;->b:Z

    return-void
.end method

.method public i(F)V
    .locals 6

    iget-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/a$c;->d:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iput p1, p0, Lmiuix/appcompat/internal/app/widget/a$c;->c:F

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/a$c;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    const/4 v2, 0x1

    new-array v3, v2, [Landroid/view/View;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    invoke-static {v3}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object v1

    invoke-interface {v1}, Lmiuix/animation/e;->b()Lmiuix/animation/h;

    move-result-object v1

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    sget-object v5, Lh9/h;->o:Lh9/h;

    aput-object v5, v3, v4

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v3, v2

    invoke-interface {v1, v3}, Lmiuix/animation/h;->D([Ljava/lang/Object;)Lmiuix/animation/h;

    goto :goto_0

    :cond_1
    return-void
.end method

.method public j(FII)V
    .locals 4

    iget-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/a$c;->d:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Ld9/a;

    const-string v1, "from"

    invoke-direct {v0, v1}, Ld9/a;-><init>(Ljava/lang/Object;)V

    sget-object v1, Lh9/h;->o:Lh9/h;

    iget-boolean v2, p0, Lmiuix/appcompat/internal/app/widget/a$c;->b:Z

    if-eqz v2, :cond_1

    goto :goto_0

    :cond_1
    iget p1, p0, Lmiuix/appcompat/internal/app/widget/a$c;->c:F

    :goto_0
    float-to-double v2, p1

    invoke-virtual {v0, v1, v2, v3}, Ld9/a;->a(Ljava/lang/Object;D)Ld9/a;

    move-result-object p1

    sget-object v0, Lh9/h;->b:Lh9/h;

    int-to-double v1, p2

    invoke-virtual {p1, v0, v1, v2}, Ld9/a;->a(Ljava/lang/Object;D)Ld9/a;

    move-result-object p1

    sget-object p2, Lh9/h;->c:Lh9/h;

    int-to-double v0, p3

    invoke-virtual {p1, p2, v0, v1}, Ld9/a;->a(Ljava/lang/Object;D)Ld9/a;

    move-result-object p1

    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/a$c;->a:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_3

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Landroid/view/View;

    invoke-virtual {p3}, Landroid/view/View;->isAttachedToWindow()Z

    move-result v0

    if-nez v0, :cond_2

    goto :goto_1

    :cond_2
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/view/View;

    const/4 v1, 0x0

    aput-object p3, v0, v1

    invoke-static {v0}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object p3

    invoke-interface {p3}, Lmiuix/animation/e;->b()Lmiuix/animation/h;

    move-result-object p3

    invoke-interface {p3, p1}, Lmiuix/animation/h;->C(Ljava/lang/Object;)Lmiuix/animation/h;

    goto :goto_1

    :cond_3
    return-void
.end method

.method public k(I)V
    .locals 2

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/a$c;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v1, p1}, Landroid/view/View;->setVisibility(I)V

    if-eqz p1, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->clearFocus()V

    goto :goto_0

    :cond_1
    return-void
.end method
