.class public Lmiuix/appcompat/internal/view/menu/i;
.super Lmiuix/appcompat/internal/view/menu/c;

# interfaces
.implements Landroid/view/SubMenu;


# instance fields
.field private y:Lmiuix/appcompat/internal/view/menu/c;

.field private z:Lmiuix/appcompat/internal/view/menu/e;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lmiuix/appcompat/internal/view/menu/c;Lmiuix/appcompat/internal/view/menu/e;)V
    .locals 0

    invoke-direct {p0, p1}, Lmiuix/appcompat/internal/view/menu/c;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lmiuix/appcompat/internal/view/menu/i;->y:Lmiuix/appcompat/internal/view/menu/c;

    iput-object p3, p0, Lmiuix/appcompat/internal/view/menu/i;->z:Lmiuix/appcompat/internal/view/menu/e;

    return-void
.end method


# virtual methods
.method public A()Lmiuix/appcompat/internal/view/menu/c;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/i;->y:Lmiuix/appcompat/internal/view/menu/c;

    return-object v0
.end method

.method public C()Z
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/i;->y:Lmiuix/appcompat/internal/view/menu/c;

    invoke-virtual {v0}, Lmiuix/appcompat/internal/view/menu/c;->C()Z

    move-result v0

    return v0
.end method

.method public D()Z
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/i;->y:Lmiuix/appcompat/internal/view/menu/c;

    invoke-virtual {v0}, Lmiuix/appcompat/internal/view/menu/c;->D()Z

    move-result v0

    return v0
.end method

.method public K(Lmiuix/appcompat/internal/view/menu/c$a;)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/i;->y:Lmiuix/appcompat/internal/view/menu/c;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/internal/view/menu/c;->K(Lmiuix/appcompat/internal/view/menu/c$a;)V

    return-void
.end method

.method public W()Landroid/view/Menu;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/i;->y:Lmiuix/appcompat/internal/view/menu/c;

    return-object v0
.end method

.method public clearHeader()V
    .locals 0

    return-void
.end method

.method public f(Lmiuix/appcompat/internal/view/menu/e;)Z
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/i;->y:Lmiuix/appcompat/internal/view/menu/c;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/internal/view/menu/c;->f(Lmiuix/appcompat/internal/view/menu/e;)Z

    move-result p1

    return p1
.end method

.method public g(Lmiuix/appcompat/internal/view/menu/c;Landroid/view/MenuItem;)Z
    .locals 1

    invoke-super {p0, p1, p2}, Lmiuix/appcompat/internal/view/menu/c;->g(Lmiuix/appcompat/internal/view/menu/c;Landroid/view/MenuItem;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/i;->y:Lmiuix/appcompat/internal/view/menu/c;

    invoke-virtual {v0, p1, p2}, Lmiuix/appcompat/internal/view/menu/c;->g(Lmiuix/appcompat/internal/view/menu/c;Landroid/view/MenuItem;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method public getItem()Landroid/view/MenuItem;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/i;->z:Lmiuix/appcompat/internal/view/menu/e;

    return-object v0
.end method

.method public j(Lmiuix/appcompat/internal/view/menu/e;)Z
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/i;->y:Lmiuix/appcompat/internal/view/menu/c;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/internal/view/menu/c;->j(Lmiuix/appcompat/internal/view/menu/e;)Z

    move-result p1

    return p1
.end method

.method public setHeaderIcon(I)Landroid/view/SubMenu;
    .locals 1

    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/c;->r()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    invoke-super {p0, p1}, Lmiuix/appcompat/internal/view/menu/c;->O(Landroid/graphics/drawable/Drawable;)Lmiuix/appcompat/internal/view/menu/c;

    return-object p0
.end method

.method public setHeaderIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/SubMenu;
    .locals 0

    invoke-super {p0, p1}, Lmiuix/appcompat/internal/view/menu/c;->O(Landroid/graphics/drawable/Drawable;)Lmiuix/appcompat/internal/view/menu/c;

    return-object p0
.end method

.method public setHeaderTitle(I)Landroid/view/SubMenu;
    .locals 1

    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/c;->r()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-super {p0, p1}, Lmiuix/appcompat/internal/view/menu/c;->R(Ljava/lang/CharSequence;)Lmiuix/appcompat/internal/view/menu/c;

    return-object p0
.end method

.method public setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/SubMenu;
    .locals 0

    invoke-super {p0, p1}, Lmiuix/appcompat/internal/view/menu/c;->R(Ljava/lang/CharSequence;)Lmiuix/appcompat/internal/view/menu/c;

    return-object p0
.end method

.method public setHeaderView(Landroid/view/View;)Landroid/view/SubMenu;
    .locals 0

    invoke-super {p0, p1}, Lmiuix/appcompat/internal/view/menu/c;->S(Landroid/view/View;)Lmiuix/appcompat/internal/view/menu/c;

    return-object p0
.end method

.method public setIcon(I)Landroid/view/SubMenu;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/i;->z:Lmiuix/appcompat/internal/view/menu/e;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/internal/view/menu/e;->setIcon(I)Landroid/view/MenuItem;

    return-object p0
.end method

.method public setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/SubMenu;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/i;->z:Lmiuix/appcompat/internal/view/menu/e;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/internal/view/menu/e;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    return-object p0
.end method

.method public setQwertyMode(Z)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/i;->y:Lmiuix/appcompat/internal/view/menu/c;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/internal/view/menu/c;->setQwertyMode(Z)V

    return-void
.end method
