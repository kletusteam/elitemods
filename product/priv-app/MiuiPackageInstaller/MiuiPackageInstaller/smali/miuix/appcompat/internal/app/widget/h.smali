.class public Lmiuix/appcompat/internal/app/widget/h;
.super Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;


# virtual methods
.method getDefaultTabTextStyle()I
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return v0

    :goto_1
    const v0, 0x10102f5

    goto/32 :goto_0

    nop
.end method

.method getTabBarLayoutRes()I
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    sget v0, Lk9/i;->g:I

    goto/32 :goto_1

    nop

    :goto_1
    return v0
.end method

.method getTabContainerHeight()I
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    invoke-virtual {v0}, Ls9/a;->f()I

    move-result v0

    goto/32 :goto_3

    nop

    :goto_1
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getContext()Landroid/content/Context;

    move-result-object v0

    goto/32 :goto_2

    nop

    :goto_2
    invoke-static {v0}, Ls9/a;->b(Landroid/content/Context;)Ls9/a;

    move-result-object v0

    goto/32 :goto_0

    nop

    :goto_3
    return v0
.end method

.method getTabViewLayoutRes()I
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return v0

    :goto_1
    sget v0, Lk9/i;->d:I

    goto/32 :goto_0

    nop
.end method

.method getTabViewMarginHorizontal()I
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    const/4 v0, 0x0

    goto/32 :goto_1

    nop

    :goto_1
    return v0
.end method

.method protected onLayout(ZIIII)V
    .locals 0

    invoke-super/range {p0 .. p5}, Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;->onLayout(ZIIII)V

    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getMeasuredHeight()I

    move-result p1

    iget-object p3, p0, Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;->d:Landroid/widget/LinearLayout;

    invoke-virtual {p3}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result p3

    iget-object p4, p0, Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;->d:Landroid/widget/LinearLayout;

    invoke-virtual {p4}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result p4

    sub-int/2addr p1, p3

    div-int/lit8 p1, p1, 0x2

    iget-object p5, p0, Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;->d:Landroid/widget/LinearLayout;

    add-int/2addr p3, p1

    invoke-virtual {p5, p2, p1, p4, p3}, Landroid/widget/LinearLayout;->layout(IIII)V

    return-void
.end method
