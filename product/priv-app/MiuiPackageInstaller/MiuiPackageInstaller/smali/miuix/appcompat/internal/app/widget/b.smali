.class public Lmiuix/appcompat/internal/app/widget/b;
.super Lmiuix/appcompat/app/a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiuix/appcompat/internal/app/widget/b$e;
    }
.end annotation


# static fields
.field private static J:Ld/a$e;


# instance fields
.field private A:Z

.field private B:Z

.field private C:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

.field private D:Ls9/b$a;

.field private E:Lmiuix/animation/h;

.field private F:Lmiuix/animation/h;

.field private G:I

.field private H:Z

.field private I:I

.field a:Landroid/view/ActionMode;

.field private b:Landroid/content/Context;

.field private c:Landroid/content/Context;

.field private d:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

.field private e:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

.field private f:Lmiuix/appcompat/internal/app/widget/ActionBarView;

.field private g:Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

.field private h:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

.field private i:Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView;

.field private j:Landroid/view/View;

.field private k:Landroid/view/View$OnClickListener;

.field private l:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

.field private m:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

.field private n:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

.field private o:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

.field private p:Lmiuix/appcompat/internal/app/widget/g;

.field private q:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private r:Landroidx/fragment/app/m;

.field private s:I

.field private t:Z

.field private u:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ld/a$b;",
            ">;"
        }
    .end annotation
.end field

.field private v:I

.field private w:I

.field private x:Z

.field private y:Z

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lmiuix/appcompat/internal/app/widget/b$a;

    invoke-direct {v0}, Lmiuix/appcompat/internal/app/widget/b$a;-><init>()V

    sput-object v0, Lmiuix/appcompat/internal/app/widget/b;->J:Ld/a$e;

    return-void
.end method

.method public constructor <init>(Lmiuix/appcompat/app/j;Landroid/view/ViewGroup;)V
    .locals 1

    invoke-direct {p0}, Lmiuix/appcompat/app/a;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/b;->q:Ljava/util/ArrayList;

    const/4 v0, -0x1

    iput v0, p0, Lmiuix/appcompat/internal/app/widget/b;->s:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/b;->u:Ljava/util/ArrayList;

    const/4 v0, 0x0

    iput v0, p0, Lmiuix/appcompat/internal/app/widget/b;->w:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/b;->A:Z

    new-instance v0, Lmiuix/appcompat/internal/app/widget/b$b;

    invoke-direct {v0, p0}, Lmiuix/appcompat/internal/app/widget/b$b;-><init>(Lmiuix/appcompat/internal/app/widget/b;)V

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/b;->D:Ls9/b$a;

    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/b;->b:Landroid/content/Context;

    invoke-virtual {p1}, Landroidx/fragment/app/e;->X()Landroidx/fragment/app/m;

    move-result-object v0

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/b;->r:Landroidx/fragment/app/m;

    invoke-virtual {p0, p2}, Lmiuix/appcompat/internal/app/widget/b;->Q(Landroid/view/ViewGroup;)V

    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/b;->f:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {p1}, Landroid/app/Activity;->getTitle()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p2, p1}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->setWindowTitle(Ljava/lang/CharSequence;)V

    return-void
.end method

.method static synthetic B(Lmiuix/appcompat/internal/app/widget/b;)Z
    .locals 0

    iget-boolean p0, p0, Lmiuix/appcompat/internal/app/widget/b;->A:Z

    return p0
.end method

.method static synthetic C(Lmiuix/appcompat/internal/app/widget/b;)Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView;
    .locals 0

    iget-object p0, p0, Lmiuix/appcompat/internal/app/widget/b;->i:Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView;

    return-object p0
.end method

.method private static E(ZZZ)Z
    .locals 1

    const/4 v0, 0x1

    if-eqz p2, :cond_0

    return v0

    :cond_0
    if-nez p0, :cond_2

    if-eqz p1, :cond_1

    goto :goto_0

    :cond_1
    return v0

    :cond_2
    :goto_0
    const/4 p0, 0x0

    return p0
.end method

.method private F(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;
    .locals 2

    instance-of v0, p1, Lmiuix/view/e$a;

    if-eqz v0, :cond_0

    new-instance v0, Ls9/d;

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/b;->b:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Ls9/d;-><init>(Landroid/content/Context;Landroid/view/ActionMode$Callback;)V

    goto :goto_0

    :cond_0
    new-instance v0, Ls9/c;

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/b;->b:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Ls9/c;-><init>(Landroid/content/Context;Landroid/view/ActionMode$Callback;)V

    :goto_0
    return-object v0
.end method

.method private I(Z)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lmiuix/appcompat/internal/app/widget/b;->J(ZLd9/a;)V

    return-void
.end method

.method private J(ZLd9/a;)V
    .locals 6

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/b;->E:Lmiuix/animation/h;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lmiuix/animation/h;->j()Ld9/a;

    move-result-object v0

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/b;->E:Lmiuix/animation/h;

    invoke-interface {v2}, Lmiuix/animation/d;->cancel()V

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/b;->T()Z

    move-result v2

    const/4 v3, 0x0

    if-nez v2, :cond_2

    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    move p1, v3

    goto :goto_2

    :cond_2
    :goto_1
    const/4 p1, 0x1

    :goto_2
    const/16 v2, 0x8

    const/4 v4, 0x0

    if-eqz p1, :cond_3

    const-string v5, "HideActionBar"

    invoke-direct {p0, v3, v5, v0, p2}, Lmiuix/appcompat/internal/app/widget/b;->a0(ZLjava/lang/String;Ld9/a;Ld9/a;)Lmiuix/animation/h;

    move-result-object p2

    iput-object p2, p0, Lmiuix/appcompat/internal/app/widget/b;->E:Lmiuix/animation/h;

    goto :goto_3

    :cond_3
    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/b;->e:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-virtual {p2}, Landroid/widget/FrameLayout;->getHeight()I

    move-result v0

    neg-int v0, v0

    int-to-float v0, v0

    invoke-virtual {p2, v0}, Landroid/widget/FrameLayout;->setTranslationY(F)V

    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/b;->e:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-virtual {p2, v4}, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;->setAlpha(F)V

    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/b;->e:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-virtual {p2, v2}, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;->setVisibility(I)V

    :goto_3
    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/b;->h:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    if-eqz p2, :cond_6

    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/b;->F:Lmiuix/animation/h;

    if-eqz p2, :cond_4

    invoke-interface {p2}, Lmiuix/animation/h;->j()Ld9/a;

    move-result-object v1

    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/b;->F:Lmiuix/animation/h;

    invoke-interface {p2}, Lmiuix/animation/d;->cancel()V

    :cond_4
    if-eqz p1, :cond_5

    const-string p1, "SpliterHide"

    invoke-direct {p0, v3, p1, v1}, Lmiuix/appcompat/internal/app/widget/b;->b0(ZLjava/lang/String;Ld9/a;)Lmiuix/animation/h;

    move-result-object p1

    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/b;->F:Lmiuix/animation/h;

    goto :goto_4

    :cond_5
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/b;->h:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-direct {p0}, Lmiuix/appcompat/internal/app/widget/b;->O()I

    move-result p2

    int-to-float p2, p2

    invoke-virtual {p1, p2}, Landroid/widget/FrameLayout;->setTranslationY(F)V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/b;->h:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-virtual {p1, v4}, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;->setAlpha(F)V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/b;->h:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-virtual {p1, v2}, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;->setVisibility(I)V

    :goto_4
    invoke-direct {p0, v3}, Lmiuix/appcompat/internal/app/widget/b;->c0(Z)V

    :cond_6
    return-void
.end method

.method private K(Z)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lmiuix/appcompat/internal/app/widget/b;->L(ZLd9/a;)V

    return-void
.end method

.method private L(ZLd9/a;)V
    .locals 7

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/b;->E:Lmiuix/animation/h;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lmiuix/animation/h;->j()Ld9/a;

    move-result-object v0

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/b;->E:Lmiuix/animation/h;

    invoke-interface {v2}, Lmiuix/animation/d;->cancel()V

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/b;->T()Z

    move-result v2

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-nez v2, :cond_2

    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    move p1, v3

    goto :goto_2

    :cond_2
    :goto_1
    move p1, v4

    :goto_2
    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/b;->e:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    iget-object v5, p0, Lmiuix/appcompat/internal/app/widget/b;->a:Landroid/view/ActionMode;

    instance-of v5, v5, Lmiuix/view/e;

    if-eqz v5, :cond_3

    const/16 v5, 0x8

    goto :goto_3

    :cond_3
    move v5, v3

    :goto_3
    invoke-virtual {v2, v5}, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;->setVisibility(I)V

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    if-eqz p1, :cond_4

    const-string v6, "ShowActionBar"

    invoke-direct {p0, v4, v6, v0, p2}, Lmiuix/appcompat/internal/app/widget/b;->a0(ZLjava/lang/String;Ld9/a;Ld9/a;)Lmiuix/animation/h;

    move-result-object p2

    iput-object p2, p0, Lmiuix/appcompat/internal/app/widget/b;->E:Lmiuix/animation/h;

    goto :goto_4

    :cond_4
    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/b;->e:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-virtual {p2, v5}, Landroid/widget/FrameLayout;->setTranslationY(F)V

    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/b;->e:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-virtual {p2, v2}, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;->setAlpha(F)V

    :goto_4
    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/b;->h:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    if-eqz p2, :cond_8

    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/b;->F:Lmiuix/animation/h;

    if-eqz p2, :cond_5

    invoke-interface {p2}, Lmiuix/animation/h;->j()Ld9/a;

    move-result-object v1

    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/b;->F:Lmiuix/animation/h;

    invoke-interface {p2}, Lmiuix/animation/d;->cancel()V

    :cond_5
    iget-object p2, p0, Lmiuix/appcompat/internal/app/widget/b;->h:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-virtual {p2, v3}, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;->setVisibility(I)V

    if-eqz p1, :cond_6

    const-string p1, "SpliterShow"

    invoke-direct {p0, v4, p1, v1}, Lmiuix/appcompat/internal/app/widget/b;->b0(ZLjava/lang/String;Ld9/a;)Lmiuix/animation/h;

    move-result-object p1

    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/b;->F:Lmiuix/animation/h;

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/b;->f:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {p1}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->L0()Z

    move-result p1

    if-eqz p1, :cond_7

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/b;->h:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-virtual {p1}, Landroid/widget/FrameLayout;->getChildCount()I

    move-result p1

    if-lez p1, :cond_7

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/b;->h:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-virtual {p1, v3}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_7

    instance-of p2, p1, Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView;

    if-eqz p2, :cond_7

    move-object p2, p1

    check-cast p2, Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView;

    invoke-virtual {p2}, Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView;->t()Z

    move-result p2

    xor-int/2addr p2, v4

    if-eqz p2, :cond_7

    check-cast p1, Lmiuix/appcompat/internal/view/menu/action/c;

    invoke-virtual {p1}, Landroid/widget/LinearLayout;->startLayoutAnimation()V

    goto :goto_5

    :cond_6
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/b;->h:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-virtual {p1, v5}, Landroid/widget/FrameLayout;->setTranslationY(F)V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/b;->h:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-virtual {p1, v2}, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;->setAlpha(F)V

    :cond_7
    :goto_5
    invoke-direct {p0, v4}, Lmiuix/appcompat/internal/app/widget/b;->c0(Z)V

    :cond_8
    return-void
.end method

.method private O()I
    .locals 3

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/b;->h:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getHeight()I

    move-result v0

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/b;->h:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getChildCount()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/b;->h:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    instance-of v2, v1, Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView;

    if-eqz v2, :cond_0

    check-cast v1, Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView;

    invoke-virtual {v1}, Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView;->t()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView;->getCollapsedHeight()I

    move-result v0

    :cond_0
    return v0
.end method

.method private U()V
    .locals 4

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/b;->d:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getMeasuredWidth()I

    move-result v0

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/b;->C:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iget v1, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    const/4 v2, 0x0

    invoke-static {v0, v2, v1}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    move-result v0

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/b;->d:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result v1

    iget-object v3, p0, Lmiuix/appcompat/internal/app/widget/b;->C:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    iget v3, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {v1, v2, v3}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    move-result v1

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/b;->C:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    invoke-virtual {v2, v0, v1}, Landroid/widget/FrameLayout;->measure(II)V

    return-void
.end method

.method private W(Z)V
    .locals 4

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/b;->e:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;->setTabContainer(Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;)V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/b;->f:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/b;->l:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/b;->m:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/b;->n:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    iget-object v3, p0, Lmiuix/appcompat/internal/app/widget/b;->o:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    invoke-virtual {p1, v0, v1, v2, v3}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->e1(Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;)V

    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/b;->N()I

    move-result p1

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x2

    if-ne p1, v2, :cond_0

    move p1, v0

    goto :goto_0

    :cond_0
    move p1, v1

    :goto_0
    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/b;->l:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    const/16 v3, 0x8

    if-eqz v2, :cond_2

    if-eqz p1, :cond_1

    invoke-virtual {v2, v1}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    goto :goto_1

    :cond_1
    invoke-virtual {v2, v3}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    :goto_1
    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/b;->l:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    invoke-virtual {v2, v0}, Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;->setEmbeded(Z)V

    :cond_2
    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/b;->m:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/b;->o:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    if-eqz p1, :cond_3

    invoke-virtual {v2, v1}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    goto :goto_2

    :cond_3
    invoke-virtual {v2, v3}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    :goto_2
    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/b;->o:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    invoke-virtual {v2, v0}, Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;->setEmbeded(Z)V

    :cond_4
    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/b;->n:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    if-eqz v2, :cond_6

    if-eqz p1, :cond_5

    invoke-virtual {v2, v1}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    goto :goto_3

    :cond_5
    invoke-virtual {v2, v3}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    :goto_3
    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/b;->n:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    invoke-virtual {v2, v0}, Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;->setEmbeded(Z)V

    :cond_6
    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/b;->o:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    if-eqz v2, :cond_8

    if-eqz p1, :cond_7

    invoke-virtual {v2, v1}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    goto :goto_4

    :cond_7
    invoke-virtual {v2, v3}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    :goto_4
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/b;->o:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    invoke-virtual {p1, v0}, Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;->setEmbeded(Z)V

    :cond_8
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/b;->f:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {p1, v1}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->setCollapsable(Z)V

    return-void
.end method

.method private a0(ZLjava/lang/String;Ld9/a;Ld9/a;)Lmiuix/animation/h;
    .locals 9

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/b;->e:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getHeight()I

    move-result v0

    const-wide/16 v1, 0x0

    const/4 v3, 0x2

    const/4 v4, -0x2

    const/4 v5, 0x0

    const/4 v6, 0x1

    if-eqz p1, :cond_2

    new-instance p1, Lc9/a;

    invoke-direct {p1}, Lc9/a;-><init>()V

    new-array v0, v3, [F

    fill-array-data v0, :array_0

    invoke-static {v4, v0}, Lj9/c;->e(I[F)Lj9/c$a;

    move-result-object v0

    invoke-virtual {p1, v0}, Lc9/a;->l(Lj9/c$a;)Lc9/a;

    if-nez p4, :cond_0

    new-instance p4, Ld9/a;

    invoke-direct {p4, p2}, Ld9/a;-><init>(Ljava/lang/Object;)V

    sget-object v0, Lh9/h;->c:Lh9/h;

    invoke-virtual {p4, v0, v1, v2}, Ld9/a;->a(Ljava/lang/Object;D)Ld9/a;

    move-result-object p4

    sget-object v0, Lh9/h;->o:Lh9/h;

    const-wide/high16 v1, 0x3ff0000000000000L    # 1.0

    invoke-virtual {p4, v0, v1, v2}, Ld9/a;->a(Ljava/lang/Object;D)Ld9/a;

    move-result-object p4

    :cond_0
    new-array v0, v6, [Landroid/view/View;

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/b;->e:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    aput-object v1, v0, v5

    invoke-static {v0}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object v0

    invoke-interface {v0}, Lmiuix/animation/e;->b()Lmiuix/animation/h;

    move-result-object v0

    if-eqz p3, :cond_1

    invoke-virtual {p3, p2}, Ld9/a;->s(Ljava/lang/Object;)V

    invoke-interface {v0, p3}, Lmiuix/animation/h;->C(Ljava/lang/Object;)Lmiuix/animation/h;

    move-result-object v0

    :cond_1
    new-array p2, v6, [Lc9/a;

    aput-object p1, p2, v5

    invoke-interface {v0, p4, p2}, Lmiuix/animation/h;->s(Ljava/lang/Object;[Lc9/a;)Lmiuix/animation/h;

    move-result-object p1

    goto :goto_0

    :cond_2
    new-instance p1, Lc9/a;

    invoke-direct {p1}, Lc9/a;-><init>()V

    new-array v3, v3, [F

    fill-array-data v3, :array_1

    invoke-static {v4, v3}, Lj9/c;->e(I[F)Lj9/c$a;

    move-result-object v3

    invoke-virtual {p1, v3}, Lc9/a;->l(Lj9/c$a;)Lc9/a;

    new-array v3, v6, [Lf9/b;

    new-instance v4, Lmiuix/appcompat/internal/app/widget/b$e;

    iget-object v7, p0, Lmiuix/appcompat/internal/app/widget/b;->e:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-direct {v4, v7, p0}, Lmiuix/appcompat/internal/app/widget/b$e;-><init>(Landroid/view/View;Lmiuix/appcompat/internal/app/widget/b;)V

    aput-object v4, v3, v5

    invoke-virtual {p1, v3}, Lc9/a;->a([Lf9/b;)Lc9/a;

    if-nez p4, :cond_3

    new-instance p4, Ld9/a;

    invoke-direct {p4, p2}, Ld9/a;-><init>(Ljava/lang/Object;)V

    sget-object v3, Lh9/h;->c:Lh9/h;

    neg-int v0, v0

    add-int/lit8 v0, v0, -0x64

    int-to-double v7, v0

    invoke-virtual {p4, v3, v7, v8}, Ld9/a;->a(Ljava/lang/Object;D)Ld9/a;

    move-result-object p4

    sget-object v0, Lh9/h;->o:Lh9/h;

    invoke-virtual {p4, v0, v1, v2}, Ld9/a;->a(Ljava/lang/Object;D)Ld9/a;

    move-result-object p4

    :cond_3
    new-array v0, v6, [Landroid/view/View;

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/b;->e:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    aput-object v1, v0, v5

    invoke-static {v0}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object v0

    invoke-interface {v0}, Lmiuix/animation/e;->b()Lmiuix/animation/h;

    move-result-object v0

    if-eqz p3, :cond_4

    invoke-virtual {p3, p2}, Ld9/a;->s(Ljava/lang/Object;)V

    invoke-interface {v0, p3}, Lmiuix/animation/h;->C(Ljava/lang/Object;)Lmiuix/animation/h;

    move-result-object v0

    :cond_4
    new-array p2, v6, [Lc9/a;

    aput-object p1, p2, v5

    invoke-interface {v0, p4, p2}, Lmiuix/animation/h;->s(Ljava/lang/Object;[Lc9/a;)Lmiuix/animation/h;

    move-result-object p1

    :goto_0
    return-object p1

    :array_0
    .array-data 4
        0x3f666666    # 0.9f
        0x3e800000    # 0.25f
    .end array-data

    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x3eb33333    # 0.35f
    .end array-data
.end method

.method private b0(ZLjava/lang/String;Ld9/a;)Lmiuix/animation/h;
    .locals 9

    invoke-direct {p0}, Lmiuix/appcompat/internal/app/widget/b;->O()I

    move-result v0

    const-wide/16 v1, 0x0

    const/4 v3, 0x2

    const/4 v4, -0x2

    const/4 v5, 0x0

    const/4 v6, 0x1

    if-eqz p1, :cond_1

    new-instance p1, Lc9/a;

    invoke-direct {p1}, Lc9/a;-><init>()V

    new-array v0, v3, [F

    fill-array-data v0, :array_0

    invoke-static {v4, v0}, Lj9/c;->e(I[F)Lj9/c$a;

    move-result-object v0

    invoke-virtual {p1, v0}, Lc9/a;->l(Lj9/c$a;)Lc9/a;

    new-instance v0, Ld9/a;

    invoke-direct {v0, p2}, Ld9/a;-><init>(Ljava/lang/Object;)V

    sget-object v3, Lh9/h;->c:Lh9/h;

    invoke-virtual {v0, v3, v1, v2}, Ld9/a;->a(Ljava/lang/Object;D)Ld9/a;

    move-result-object v0

    sget-object v1, Lh9/h;->o:Lh9/h;

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v0, v1, v2, v3}, Ld9/a;->a(Ljava/lang/Object;D)Ld9/a;

    move-result-object v0

    new-array v1, v6, [Landroid/view/View;

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/b;->h:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    aput-object v2, v1, v5

    invoke-static {v1}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object v1

    invoke-interface {v1}, Lmiuix/animation/e;->b()Lmiuix/animation/h;

    move-result-object v1

    if-eqz p3, :cond_0

    invoke-virtual {p3, p2}, Ld9/a;->s(Ljava/lang/Object;)V

    invoke-interface {v1, p3}, Lmiuix/animation/h;->C(Ljava/lang/Object;)Lmiuix/animation/h;

    move-result-object v1

    :cond_0
    new-array p2, v6, [Lc9/a;

    aput-object p1, p2, v5

    invoke-interface {v1, v0, p2}, Lmiuix/animation/h;->s(Ljava/lang/Object;[Lc9/a;)Lmiuix/animation/h;

    move-result-object p1

    goto :goto_0

    :cond_1
    new-instance p1, Lc9/a;

    invoke-direct {p1}, Lc9/a;-><init>()V

    new-array v3, v3, [F

    fill-array-data v3, :array_1

    invoke-static {v4, v3}, Lj9/c;->e(I[F)Lj9/c$a;

    move-result-object v3

    invoke-virtual {p1, v3}, Lc9/a;->l(Lj9/c$a;)Lc9/a;

    new-array v3, v6, [Lf9/b;

    new-instance v4, Lmiuix/appcompat/internal/app/widget/b$e;

    iget-object v7, p0, Lmiuix/appcompat/internal/app/widget/b;->h:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-direct {v4, v7, p0}, Lmiuix/appcompat/internal/app/widget/b$e;-><init>(Landroid/view/View;Lmiuix/appcompat/internal/app/widget/b;)V

    aput-object v4, v3, v5

    invoke-virtual {p1, v3}, Lc9/a;->a([Lf9/b;)Lc9/a;

    new-instance v3, Ld9/a;

    invoke-direct {v3, p2}, Ld9/a;-><init>(Ljava/lang/Object;)V

    sget-object v4, Lh9/h;->c:Lh9/h;

    add-int/lit8 v0, v0, 0x64

    int-to-double v7, v0

    invoke-virtual {v3, v4, v7, v8}, Ld9/a;->a(Ljava/lang/Object;D)Ld9/a;

    move-result-object v0

    sget-object v3, Lh9/h;->o:Lh9/h;

    invoke-virtual {v0, v3, v1, v2}, Ld9/a;->a(Ljava/lang/Object;D)Ld9/a;

    move-result-object v0

    new-array v1, v6, [Landroid/view/View;

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/b;->h:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    aput-object v2, v1, v5

    invoke-static {v1}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object v1

    invoke-interface {v1}, Lmiuix/animation/e;->b()Lmiuix/animation/h;

    move-result-object v1

    if-eqz p3, :cond_2

    invoke-virtual {p3, p2}, Ld9/a;->s(Ljava/lang/Object;)V

    invoke-interface {v1, p3}, Lmiuix/animation/h;->C(Ljava/lang/Object;)Lmiuix/animation/h;

    move-result-object v1

    :cond_2
    new-array p2, v6, [Lc9/a;

    aput-object p1, p2, v5

    invoke-interface {v1, v0, p2}, Lmiuix/animation/h;->s(Ljava/lang/Object;[Lc9/a;)Lmiuix/animation/h;

    move-result-object p1

    :goto_0
    return-object p1

    nop

    :array_0
    .array-data 4
        0x3f666666    # 0.9f
        0x3e800000    # 0.25f
    .end array-data

    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x3eb33333    # 0.35f
    .end array-data
.end method

.method private c0(Z)V
    .locals 2

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/b;->h:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getChildCount()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/b;->h:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    instance-of v0, v0, Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/b;->h:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView;

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/b;->i:Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView;

    invoke-virtual {v0}, Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView;->t()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/b;->j:Landroid/view/View;

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/b;->d:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/b;->k:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->r(Landroid/view/View$OnClickListener;)Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout$c;

    move-result-object p1

    invoke-virtual {p1}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout$c;->b()Landroid/animation/Animator;

    move-result-object p1

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/b;->d:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->r(Landroid/view/View$OnClickListener;)Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout$c;

    move-result-object p1

    invoke-virtual {p1}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout$c;->a()Landroid/animation/Animator;

    move-result-object p1

    :goto_0
    invoke-virtual {p1}, Landroid/animation/Animator;->start()V

    :cond_1
    return-void
.end method

.method private d0(Z)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lmiuix/appcompat/internal/app/widget/b;->e0(ZLd9/a;)V

    return-void
.end method

.method private e0(ZLd9/a;)V
    .locals 3

    iget-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/b;->x:Z

    iget-boolean v1, p0, Lmiuix/appcompat/internal/app/widget/b;->y:Z

    iget-boolean v2, p0, Lmiuix/appcompat/internal/app/widget/b;->z:Z

    invoke-static {v0, v1, v2}, Lmiuix/appcompat/internal/app/widget/b;->E(ZZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/b;->A:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/b;->A:Z

    invoke-direct {p0, p1, p2}, Lmiuix/appcompat/internal/app/widget/b;->L(ZLd9/a;)V

    goto :goto_0

    :cond_0
    iget-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/b;->A:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/b;->A:Z

    invoke-direct {p0, p1, p2}, Lmiuix/appcompat/internal/app/widget/b;->J(ZLd9/a;)V

    :cond_1
    :goto_0
    return-void
.end method


# virtual methods
.method public A(Z)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/b;->f:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->setResizable(Z)V

    return-void
.end method

.method D(Z)V
    .locals 2

    goto/32 :goto_b

    nop

    :goto_0
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/b;->f:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    goto/32 :goto_17

    nop

    :goto_1
    invoke-virtual {v0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->I0()Z

    move-result v0

    goto/32 :goto_12

    nop

    :goto_2
    if-eqz v0, :cond_0

    goto/32 :goto_11

    :cond_0
    goto/32 :goto_1d

    nop

    :goto_3
    invoke-interface {v0, p1}, Lmiuix/appcompat/internal/app/widget/g;->h(Z)V

    goto/32 :goto_14

    nop

    :goto_4
    if-nez v0, :cond_1

    goto/32 :goto_11

    :cond_1
    goto/32 :goto_0

    nop

    :goto_5
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/b;->p:Lmiuix/appcompat/internal/app/widget/g;

    goto/32 :goto_3

    nop

    :goto_6
    xor-int/lit8 v1, p1, 0x1

    goto/32 :goto_f

    nop

    :goto_7
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/b;->o:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    goto/32 :goto_c

    nop

    :goto_8
    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/b;->Y()V

    goto/32 :goto_d

    nop

    :goto_9
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/b;->l:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    goto/32 :goto_15

    nop

    :goto_a
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/b;->m:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    goto/32 :goto_16

    nop

    :goto_b
    if-nez p1, :cond_2

    goto/32 :goto_e

    :cond_2
    goto/32 :goto_8

    nop

    :goto_c
    xor-int/lit8 p1, p1, 0x1

    goto/32 :goto_10

    nop

    :goto_d
    goto :goto_1a

    :goto_e
    goto/32 :goto_19

    nop

    :goto_f
    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->setEnabled(Z)V

    goto/32 :goto_7

    nop

    :goto_10
    invoke-virtual {v0, p1}, Landroid/widget/HorizontalScrollView;->setEnabled(Z)V

    :goto_11
    goto/32 :goto_1b

    nop

    :goto_12
    if-nez v0, :cond_3

    goto/32 :goto_11

    :cond_3
    goto/32 :goto_9

    nop

    :goto_13
    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->setEnabled(Z)V

    goto/32 :goto_a

    nop

    :goto_14
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/b;->l:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    goto/32 :goto_4

    nop

    :goto_15
    xor-int/lit8 v1, p1, 0x1

    goto/32 :goto_13

    nop

    :goto_16
    xor-int/lit8 v1, p1, 0x1

    goto/32 :goto_18

    nop

    :goto_17
    invoke-virtual {v0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->M0()Z

    move-result v0

    goto/32 :goto_2

    nop

    :goto_18
    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->setEnabled(Z)V

    goto/32 :goto_1c

    nop

    :goto_19
    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/b;->P()V

    :goto_1a
    goto/32 :goto_5

    nop

    :goto_1b
    return-void

    :goto_1c
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/b;->n:Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;

    goto/32 :goto_6

    nop

    :goto_1d
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/b;->f:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    goto/32 :goto_1

    nop
.end method

.method public G(Landroid/view/ActionMode$Callback;)Lmiuix/appcompat/internal/app/widget/g;
    .locals 1

    instance-of p1, p1, Lmiuix/view/e$a;

    if-eqz p1, :cond_3

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/b;->C:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/b;->H()Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    move-result-object p1

    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/b;->C:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    :cond_0
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/b;->d:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    invoke-virtual {p1}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->getBaseInnerInsets()Landroid/graphics/Rect;

    move-result-object p1

    if-eqz p1, :cond_1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/b;->C:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    iget p1, p1, Landroid/graphics/Rect;->top:I

    invoke-virtual {v0, p1}, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;->setStatusBarPaddingTop(I)V

    :cond_1
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/b;->d:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/b;->C:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eq p1, v0, :cond_2

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/b;->d:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/b;->C:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    invoke-virtual {p1, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    :cond_2
    invoke-direct {p0}, Lmiuix/appcompat/internal/app/widget/b;->U()V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/b;->C:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/b;->f:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {p1, v0}, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;->b(Lmiuix/view/a;)V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/b;->C:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    goto :goto_0

    :cond_3
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/b;->g:Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

    if-eqz p1, :cond_4

    :goto_0
    return-object p1

    :cond_4
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "not set windowSplitActionBar true in activity style!"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public H()Lmiuix/appcompat/internal/app/widget/SearchActionModeView;
    .locals 4

    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/b;->k()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lk9/i;->F:I

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/b;->d:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    new-instance v1, Lmiuix/appcompat/internal/app/widget/b$d;

    invoke-direct {v1, p0}, Lmiuix/appcompat/internal/app/widget/b$d;-><init>(Lmiuix/appcompat/internal/app/widget/b;)V

    invoke-virtual {v0, v1}, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;->setOnBackClickListener(Landroid/view/View$OnClickListener;)V

    return-object v0
.end method

.method public M()I
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/b;->f:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {v0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->getExpandState()I

    move-result v0

    return v0
.end method

.method public N()I
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/b;->f:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {v0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->getNavigationMode()I

    move-result v0

    return v0
.end method

.method P()V
    .locals 4

    goto/32 :goto_5

    nop

    :goto_0
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/b;->f:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    goto/32 :goto_29

    nop

    :goto_1
    invoke-virtual {p0, v0}, Lmiuix/appcompat/internal/app/widget/b;->A(Z)V

    goto/32 :goto_0

    nop

    :goto_2
    instance-of v1, v0, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    goto/32 :goto_f

    nop

    :goto_3
    move v2, v3

    goto/32 :goto_2a

    nop

    :goto_4
    invoke-direct {p0, v0}, Lmiuix/appcompat/internal/app/widget/b;->d0(Z)V

    goto/32 :goto_1b

    nop

    :goto_5
    iget-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/b;->z:Z

    goto/32 :goto_20

    nop

    :goto_6
    check-cast v0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

    goto/32 :goto_1d

    nop

    :goto_7
    return-void

    :goto_8
    iput-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/b;->z:Z

    goto/32 :goto_9

    nop

    :goto_9
    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/b;->f:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    goto/32 :goto_d

    nop

    :goto_a
    invoke-virtual {p0, v0}, Lmiuix/appcompat/internal/app/widget/b;->A(Z)V

    goto/32 :goto_2c

    nop

    :goto_b
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setImportantForAccessibility(I)V

    :goto_c
    goto/32 :goto_7

    nop

    :goto_d
    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/b;->j()I

    move-result v2

    goto/32 :goto_1e

    nop

    :goto_e
    invoke-virtual {v1, v2}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->T0(Z)V

    goto/32 :goto_4

    nop

    :goto_f
    if-nez v1, :cond_0

    goto/32 :goto_22

    :cond_0
    goto/32 :goto_23

    nop

    :goto_10
    iget-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/b;->H:Z

    goto/32 :goto_1

    nop

    :goto_11
    iget v1, p0, Lmiuix/appcompat/internal/app/widget/b;->I:I

    goto/32 :goto_b

    nop

    :goto_12
    iget v1, p0, Lmiuix/appcompat/internal/app/widget/b;->G:I

    goto/32 :goto_1f

    nop

    :goto_13
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/b;->f:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    goto/32 :goto_11

    nop

    :goto_14
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/b;->p:Lmiuix/appcompat/internal/app/widget/g;

    goto/32 :goto_25

    nop

    :goto_15
    const/4 v3, 0x1

    goto/32 :goto_19

    nop

    :goto_16
    invoke-virtual {v0, v1}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->setExpandState(I)V

    :goto_17
    goto/32 :goto_13

    nop

    :goto_18
    iput v0, p0, Lmiuix/appcompat/internal/app/widget/b;->G:I

    goto/32 :goto_10

    nop

    :goto_19
    if-nez v2, :cond_1

    goto/32 :goto_2b

    :cond_1
    goto/32 :goto_3

    nop

    :goto_1a
    invoke-virtual {v0}, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->getExpandState()I

    move-result v0

    goto/32 :goto_18

    nop

    :goto_1b
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/b;->p:Lmiuix/appcompat/internal/app/widget/g;

    goto/32 :goto_2

    nop

    :goto_1c
    iput-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/b;->H:Z

    goto/32 :goto_14

    nop

    :goto_1d
    invoke-virtual {v0}, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->m()Z

    move-result v0

    goto/32 :goto_1c

    nop

    :goto_1e
    const v3, 0x8000

    goto/32 :goto_26

    nop

    :goto_1f
    invoke-virtual {v0, v1, v3, v3}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->v(IZZ)V

    goto/32 :goto_21

    nop

    :goto_20
    if-nez v0, :cond_2

    goto/32 :goto_c

    :cond_2
    goto/32 :goto_24

    nop

    :goto_21
    goto :goto_17

    :goto_22
    goto/32 :goto_6

    nop

    :goto_23
    iget-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/b;->H:Z

    goto/32 :goto_a

    nop

    :goto_24
    const/4 v0, 0x0

    goto/32 :goto_8

    nop

    :goto_25
    check-cast v0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

    goto/32 :goto_1a

    nop

    :goto_26
    and-int/2addr v2, v3

    goto/32 :goto_15

    nop

    :goto_27
    move v2, v0

    :goto_28
    goto/32 :goto_e

    nop

    :goto_29
    iget v1, p0, Lmiuix/appcompat/internal/app/widget/b;->G:I

    goto/32 :goto_16

    nop

    :goto_2a
    goto :goto_28

    :goto_2b
    goto/32 :goto_27

    nop

    :goto_2c
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/b;->f:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    goto/32 :goto_12

    nop
.end method

.method protected Q(Landroid/view/ViewGroup;)V
    .locals 4

    if-nez p1, :cond_0

    return-void

    :cond_0
    move-object v0, p1

    check-cast v0, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/b;->d:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    invoke-virtual {v0, p0}, Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;->setActionBar(Lmiuix/appcompat/app/a;)V

    sget v0, Lk9/g;->a:I

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiuix/appcompat/internal/app/widget/ActionBarView;

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/b;->f:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    sget v0, Lk9/g;->o:I

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/b;->g:Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

    sget v0, Lk9/g;->d:I

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/b;->e:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    sget v0, Lk9/g;->T:I

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/b;->h:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    sget v0, Lk9/g;->A:I

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/b;->j:Landroid/view/View;

    if-eqz p1, :cond_1

    new-instance p1, Lmiuix/appcompat/internal/app/widget/b$c;

    invoke-direct {p1, p0}, Lmiuix/appcompat/internal/app/widget/b$c;-><init>(Lmiuix/appcompat/internal/app/widget/b;)V

    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/b;->k:Landroid/view/View$OnClickListener;

    :cond_1
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/b;->f:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    if-nez p1, :cond_3

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/b;->g:Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

    if-nez v0, :cond_3

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/b;->e:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    if-eqz v0, :cond_2

    goto :goto_0

    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " can only be used with a compatible window decor layout"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_3
    :goto_0
    invoke-virtual {p1}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->L0()Z

    move-result p1

    iput p1, p0, Lmiuix/appcompat/internal/app/widget/b;->v:I

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/b;->f:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {p1}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->getDisplayOptions()I

    move-result p1

    and-int/lit8 p1, p1, 0x4

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-eqz p1, :cond_4

    move p1, v1

    goto :goto_1

    :cond_4
    move p1, v0

    :goto_1
    if-eqz p1, :cond_5

    iput-boolean v1, p0, Lmiuix/appcompat/internal/app/widget/b;->t:Z

    :cond_5
    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/b;->b:Landroid/content/Context;

    invoke-static {v2}, Ls9/a;->b(Landroid/content/Context;)Ls9/a;

    move-result-object v2

    invoke-virtual {v2}, Ls9/a;->a()Z

    move-result v3

    if-nez v3, :cond_6

    if-eqz p1, :cond_7

    :cond_6
    move v0, v1

    :cond_7
    invoke-virtual {p0, v0}, Lmiuix/appcompat/internal/app/widget/b;->X(Z)V

    invoke-virtual {v2}, Ls9/a;->g()Z

    move-result p1

    invoke-direct {p0, p1}, Lmiuix/appcompat/internal/app/widget/b;->W(Z)V

    return-void
.end method

.method public R()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public S()Z
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/b;->f:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {v0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->m()Z

    move-result v0

    return v0
.end method

.method T()Z
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return v0

    :goto_1
    iget-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/b;->B:Z

    goto/32 :goto_0

    nop
.end method

.method public V(Z)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/b;->e:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;->setIsMiuixFloating(Z)V

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/b;->C:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;->T(Z)V

    :cond_0
    return-void
.end method

.method public X(Z)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/b;->f:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->setHomeButtonEnabled(Z)V

    return-void
.end method

.method Y()V
    .locals 6

    goto/32 :goto_26

    nop

    :goto_0
    const/4 v1, 0x0

    goto/32 :goto_15

    nop

    :goto_1
    invoke-virtual {p0, v1}, Lmiuix/appcompat/internal/app/widget/b;->A(Z)V

    goto/32 :goto_18

    nop

    :goto_2
    iget v3, p0, Lmiuix/appcompat/internal/app/widget/b;->G:I

    goto/32 :goto_1d

    nop

    :goto_3
    iput v2, p0, Lmiuix/appcompat/internal/app/widget/b;->I:I

    goto/32 :goto_f

    nop

    :goto_4
    instance-of v3, v3, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    goto/32 :goto_a

    nop

    :goto_5
    invoke-virtual {v2, v3}, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->setResizable(Z)V

    :goto_6
    goto/32 :goto_8

    nop

    :goto_7
    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/b;->f:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    goto/32 :goto_28

    nop

    :goto_8
    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/b;->f:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    goto/32 :goto_24

    nop

    :goto_9
    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/b;->M()I

    move-result v2

    goto/32 :goto_1f

    nop

    :goto_a
    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/b;->j()I

    move-result v4

    goto/32 :goto_11

    nop

    :goto_b
    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->setImportantForAccessibility(I)V

    goto/32 :goto_7

    nop

    :goto_c
    check-cast v2, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

    goto/32 :goto_2

    nop

    :goto_d
    move v0, v1

    :goto_e
    goto/32 :goto_12

    nop

    :goto_f
    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/b;->f:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    goto/32 :goto_1c

    nop

    :goto_10
    return-void

    :goto_11
    const v5, 0x8000

    goto/32 :goto_2d

    nop

    :goto_12
    invoke-virtual {v2, v3, v0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->U0(ZZ)V

    :goto_13
    goto/32 :goto_10

    nop

    :goto_14
    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/b;->p:Lmiuix/appcompat/internal/app/widget/g;

    goto/32 :goto_2c

    nop

    :goto_15
    invoke-direct {p0, v1}, Lmiuix/appcompat/internal/app/widget/b;->d0(Z)V

    goto/32 :goto_9

    nop

    :goto_16
    goto :goto_e

    :goto_17
    goto/32 :goto_d

    nop

    :goto_18
    goto :goto_6

    :goto_19
    goto/32 :goto_c

    nop

    :goto_1a
    check-cast v2, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

    goto/32 :goto_21

    nop

    :goto_1b
    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/b;->S()Z

    move-result v2

    goto/32 :goto_29

    nop

    :goto_1c
    const/4 v3, 0x4

    goto/32 :goto_b

    nop

    :goto_1d
    invoke-virtual {v2, v3}, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;->setExpandState(I)V

    goto/32 :goto_2a

    nop

    :goto_1e
    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/b;->f:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    goto/32 :goto_23

    nop

    :goto_1f
    iput v2, p0, Lmiuix/appcompat/internal/app/widget/b;->G:I

    goto/32 :goto_1b

    nop

    :goto_20
    if-nez v4, :cond_0

    goto/32 :goto_17

    :cond_0
    goto/32 :goto_16

    nop

    :goto_21
    iget-boolean v3, p0, Lmiuix/appcompat/internal/app/widget/b;->H:Z

    goto/32 :goto_5

    nop

    :goto_22
    if-nez v3, :cond_1

    goto/32 :goto_19

    :cond_1
    goto/32 :goto_1e

    nop

    :goto_23
    invoke-virtual {v2, v1, v0, v0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->v(IZZ)V

    goto/32 :goto_1

    nop

    :goto_24
    invoke-virtual {v2}, Landroid/view/ViewGroup;->getImportantForAccessibility()I

    move-result v2

    goto/32 :goto_3

    nop

    :goto_25
    const/4 v0, 0x1

    goto/32 :goto_27

    nop

    :goto_26
    iget-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/b;->z:Z

    goto/32 :goto_2b

    nop

    :goto_27
    iput-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/b;->z:Z

    goto/32 :goto_0

    nop

    :goto_28
    iget-object v3, p0, Lmiuix/appcompat/internal/app/widget/b;->p:Lmiuix/appcompat/internal/app/widget/g;

    goto/32 :goto_4

    nop

    :goto_29
    iput-boolean v2, p0, Lmiuix/appcompat/internal/app/widget/b;->H:Z

    goto/32 :goto_14

    nop

    :goto_2a
    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/b;->p:Lmiuix/appcompat/internal/app/widget/g;

    goto/32 :goto_1a

    nop

    :goto_2b
    if-eqz v0, :cond_2

    goto/32 :goto_13

    :cond_2
    goto/32 :goto_25

    nop

    :goto_2c
    instance-of v3, v2, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    goto/32 :goto_22

    nop

    :goto_2d
    and-int/2addr v4, v5

    goto/32 :goto_20

    nop
.end method

.method public Z(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;
    .locals 3

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/b;->a:Landroid/view/ActionMode;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/ActionMode;->finish()V

    :cond_0
    invoke-direct {p0, p1}, Lmiuix/appcompat/internal/app/widget/b;->F(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object v0

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/b;->p:Lmiuix/appcompat/internal/app/widget/g;

    instance-of v2, v1, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    if-eqz v2, :cond_1

    instance-of v2, v0, Ls9/d;

    if-nez v2, :cond_2

    :cond_1
    instance-of v2, v1, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

    if-eqz v2, :cond_3

    instance-of v2, v0, Ls9/c;

    if-eqz v2, :cond_3

    :cond_2
    invoke-interface {v1}, Lmiuix/appcompat/internal/app/widget/g;->d()V

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/b;->p:Lmiuix/appcompat/internal/app/widget/g;

    invoke-interface {v1}, Lmiuix/appcompat/internal/app/widget/g;->a()V

    :cond_3
    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/app/widget/b;->G(Landroid/view/ActionMode$Callback;)Lmiuix/appcompat/internal/app/widget/g;

    move-result-object p1

    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/b;->p:Lmiuix/appcompat/internal/app/widget/g;

    if-eqz p1, :cond_7

    instance-of v1, v0, Ls9/b;

    if-eqz v1, :cond_6

    move-object v1, v0

    check-cast v1, Ls9/b;

    invoke-virtual {v1, p1}, Ls9/b;->h(Lmiuix/appcompat/internal/app/widget/g;)V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/b;->D:Ls9/b$a;

    invoke-virtual {v1, p1}, Ls9/b;->b(Ls9/b$a;)V

    invoke-virtual {v1}, Ls9/b;->a()Z

    move-result p1

    if-eqz p1, :cond_6

    invoke-virtual {v0}, Landroid/view/ActionMode;->invalidate()V

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/b;->p:Lmiuix/appcompat/internal/app/widget/g;

    invoke-interface {p1, v0}, Lmiuix/appcompat/internal/app/widget/g;->c(Landroid/view/ActionMode;)V

    const/4 p1, 0x1

    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/app/widget/b;->D(Z)V

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/b;->h:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    if-eqz v1, :cond_4

    iget v2, p0, Lmiuix/appcompat/internal/app/widget/b;->v:I

    if-ne v2, p1, :cond_4

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result p1

    if-eqz p1, :cond_4

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/b;->h:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;->setVisibility(I)V

    :cond_4
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/b;->p:Lmiuix/appcompat/internal/app/widget/g;

    instance-of v1, p1, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

    if-eqz v1, :cond_5

    check-cast p1, Lmiuix/appcompat/internal/app/widget/ActionBarContextView;

    const/16 v1, 0x20

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->sendAccessibilityEvent(I)V

    :cond_5
    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/b;->a:Landroid/view/ActionMode;

    return-object v0

    :cond_6
    const/4 p1, 0x0

    return-object p1

    :cond_7
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "not set windowSplitActionBar true in activity style!"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public j()I
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/b;->f:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {v0}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->getDisplayOptions()I

    move-result v0

    return v0
.end method

.method public k()Landroid/content/Context;
    .locals 4

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/b;->c:Landroid/content/Context;

    if-nez v0, :cond_1

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/b;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const v2, 0x1010397

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    iget v0, v0, Landroid/util/TypedValue;->resourceId:I

    if-eqz v0, :cond_0

    new-instance v1, Landroid/view/ContextThemeWrapper;

    iget-object v2, p0, Lmiuix/appcompat/internal/app/widget/b;->b:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lmiuix/appcompat/internal/app/widget/b;->c:Landroid/content/Context;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/b;->b:Landroid/content/Context;

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/b;->c:Landroid/content/Context;

    :cond_1
    :goto_0
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/b;->c:Landroid/content/Context;

    return-object v0
.end method

.method public m()Z
    .locals 1

    iget-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/b;->A:Z

    return v0
.end method

.method public n(Landroid/content/res/Configuration;)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/b;->b:Landroid/content/Context;

    invoke-static {v0}, Ls9/a;->b(Landroid/content/Context;)Ls9/a;

    move-result-object v0

    invoke-virtual {v0}, Ls9/a;->g()Z

    move-result v0

    invoke-direct {p0, v0}, Lmiuix/appcompat/internal/app/widget/b;->W(Z)V

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/b;->C:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->isAttachedToWindow()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/b;->C:Lmiuix/appcompat/internal/app/widget/SearchActionModeView;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/internal/app/widget/SearchActionModeView;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    :cond_0
    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/b;->f:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {p1}, Lmiuix/appcompat/internal/app/widget/a;->j()Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 p1, 0x0

    iput p1, p0, Lmiuix/appcompat/internal/app/widget/b;->G:I

    iget-object p1, p0, Lmiuix/appcompat/internal/app/widget/b;->f:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {p1}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->o1()V

    :cond_1
    return-void
.end method

.method public s(Landroid/graphics/drawable/Drawable;)V
    .locals 2

    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/b;->j()I

    move-result v0

    const v1, 0x8000

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lmiuix/appcompat/internal/app/widget/b;->e:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    if-eqz v0, :cond_1

    const/4 p1, 0x0

    :cond_1
    invoke-virtual {v1, p1}, Lmiuix/appcompat/internal/app/widget/ActionBarContainer;->setPrimaryBackground(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public u(Z)V
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "RestrictedApi"
        }
    .end annotation

    iput-boolean p1, p0, Lmiuix/appcompat/internal/app/widget/b;->B:Z

    if-nez p1, :cond_1

    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/b;->m()Z

    move-result p1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    invoke-direct {p0, v0}, Lmiuix/appcompat/internal/app/widget/b;->K(Z)V

    goto :goto_0

    :cond_0
    invoke-direct {p0, v0}, Lmiuix/appcompat/internal/app/widget/b;->I(Z)V

    :cond_1
    :goto_0
    return-void
.end method

.method public v(Ljava/lang/CharSequence;)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/b;->f:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->setTitle(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public y(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/b;->f:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->setEndView(Landroid/view/View;)V

    return-void
.end method

.method public z(I)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/b;->f:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/internal/app/widget/a;->setExpandStateByUser(I)V

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/b;->f:Lmiuix/appcompat/internal/app/widget/ActionBarView;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/internal/app/widget/ActionBarView;->setExpandState(I)V

    return-void
.end method
