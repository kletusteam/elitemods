.class public abstract Lmiuix/appcompat/internal/view/menu/action/c;
.super Landroid/widget/LinearLayout;

# interfaces
.implements Lmiuix/appcompat/internal/view/menu/c$b;
.implements Lmiuix/appcompat/internal/view/menu/h;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiuix/appcompat/internal/view/menu/action/c$b;,
        Lmiuix/appcompat/internal/view/menu/action/c$a;
    }
.end annotation


# instance fields
.field private a:Lmiuix/appcompat/internal/view/menu/c;

.field private b:Z

.field private c:Lmiuix/appcompat/internal/view/menu/action/b;

.field private d:Lmiuix/appcompat/internal/view/menu/action/c$b;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->setBaselineAligned(Z)V

    new-instance p1, Lmiuix/appcompat/internal/view/menu/action/c$b;

    invoke-direct {p1, p0}, Lmiuix/appcompat/internal/view/menu/action/c$b;-><init>(Lmiuix/appcompat/internal/view/menu/action/c;)V

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/action/c;->d:Lmiuix/appcompat/internal/view/menu/action/c$b;

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->setLayoutAnimation(Landroid/view/animation/LayoutAnimationController;)V

    return-void
.end method


# virtual methods
.method public a()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public b(Lmiuix/appcompat/internal/view/menu/c;)V
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/action/c;->a:Lmiuix/appcompat/internal/view/menu/c;

    return-void
.end method

.method public c(Lmiuix/appcompat/internal/view/menu/e;)Z
    .locals 2

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/action/c;->a:Lmiuix/appcompat/internal/view/menu/c;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lmiuix/appcompat/internal/view/menu/c;->H(Landroid/view/MenuItem;I)Z

    move-result p1

    return p1
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 0

    if-eqz p1, :cond_0

    instance-of p1, p1, Lmiuix/appcompat/internal/view/menu/action/c$a;

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public d(I)Z
    .locals 0

    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/View;->clearAnimation()V

    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    const/4 p1, 0x1

    return p1
.end method

.method public dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public e()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected f()Lmiuix/appcompat/internal/view/menu/action/c$a;
    .locals 2

    new-instance v0, Lmiuix/appcompat/internal/view/menu/action/c$a;

    const/4 v1, -0x2

    invoke-direct {v0, v1, v1}, Lmiuix/appcompat/internal/view/menu/action/c$a;-><init>(II)V

    return-object v0
.end method

.method public g(Landroid/util/AttributeSet;)Lmiuix/appcompat/internal/view/menu/action/c$a;
    .locals 2

    new-instance v0, Lmiuix/appcompat/internal/view/menu/action/c$a;

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lmiuix/appcompat/internal/view/menu/action/c$a;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected bridge synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/action/c;->f()Lmiuix/appcompat/internal/view/menu/action/c$a;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic generateDefaultLayoutParams()Landroid/widget/LinearLayout$LayoutParams;
    .locals 1

    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/action/c;->f()Lmiuix/appcompat/internal/view/menu/action/c$a;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 0

    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/view/menu/action/c;->g(Landroid/util/AttributeSet;)Lmiuix/appcompat/internal/view/menu/action/c$a;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 0

    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/view/menu/action/c;->h(Landroid/view/ViewGroup$LayoutParams;)Lmiuix/appcompat/internal/view/menu/action/c$a;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/widget/LinearLayout$LayoutParams;
    .locals 0

    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/view/menu/action/c;->g(Landroid/util/AttributeSet;)Lmiuix/appcompat/internal/view/menu/action/c$a;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/widget/LinearLayout$LayoutParams;
    .locals 0

    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/view/menu/action/c;->h(Landroid/view/ViewGroup$LayoutParams;)Lmiuix/appcompat/internal/view/menu/action/c$a;

    move-result-object p1

    return-object p1
.end method

.method public abstract getCollapsedHeight()I
.end method

.method public getPresenter()Lmiuix/appcompat/internal/view/menu/action/b;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/action/c;->c:Lmiuix/appcompat/internal/view/menu/action/b;

    return-object v0
.end method

.method public getWindowAnimations()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected h(Landroid/view/ViewGroup$LayoutParams;)Lmiuix/appcompat/internal/view/menu/action/c$a;
    .locals 1

    instance-of v0, p1, Lmiuix/appcompat/internal/view/menu/action/c$a;

    if-eqz v0, :cond_0

    new-instance v0, Lmiuix/appcompat/internal/view/menu/action/c$a;

    check-cast p1, Lmiuix/appcompat/internal/view/menu/action/c$a;

    invoke-direct {v0, p1}, Lmiuix/appcompat/internal/view/menu/action/c$a;-><init>(Lmiuix/appcompat/internal/view/menu/action/c$a;)V

    return-object v0

    :cond_0
    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/action/c;->f()Lmiuix/appcompat/internal/view/menu/action/c$a;

    move-result-object p1

    return-object p1
.end method

.method public i()Lmiuix/appcompat/internal/view/menu/action/c$a;
    .locals 2

    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/action/c;->f()Lmiuix/appcompat/internal/view/menu/action/c$a;

    move-result-object v0

    const/4 v1, 0x1

    iput-boolean v1, v0, Lmiuix/appcompat/internal/view/menu/action/c$a;->a:Z

    return-object v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/action/c;->c:Lmiuix/appcompat/internal/view/menu/action/b;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lmiuix/appcompat/internal/view/menu/action/b;->c(Z)V

    :cond_0
    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 2

    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/action/c;->c:Lmiuix/appcompat/internal/view/menu/action/b;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lmiuix/appcompat/internal/view/menu/action/b;->M(Z)Z

    return-void
.end method

.method protected onMeasure(II)V
    .locals 1

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    invoke-virtual {p0, p1, p1}, Landroid/widget/LinearLayout;->setMeasuredDimension(II)V

    return-void

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    return-void
.end method

.method public setOverflowReserved(Z)V
    .locals 0

    iput-boolean p1, p0, Lmiuix/appcompat/internal/view/menu/action/c;->b:Z

    return-void
.end method

.method public setPresenter(Lmiuix/appcompat/internal/view/menu/action/b;)V
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/action/c;->c:Lmiuix/appcompat/internal/view/menu/action/b;

    return-void
.end method
