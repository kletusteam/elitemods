.class public final Lmiuix/appcompat/internal/view/menu/e;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/MenuItem;


# static fields
.field private static A:Ljava/lang/String;

.field private static x:Ljava/lang/String;

.field private static y:Ljava/lang/String;

.field private static z:Ljava/lang/String;


# instance fields
.field private final a:I

.field private final b:I

.field private final c:I

.field private final d:I

.field private e:Ljava/lang/CharSequence;

.field private f:Ljava/lang/CharSequence;

.field private g:Ljava/lang/CharSequence;

.field private h:Landroid/content/Intent;

.field private i:C

.field private j:C

.field private k:Landroid/graphics/drawable/Drawable;

.field private l:I

.field private m:Lmiuix/appcompat/internal/view/menu/c;

.field private n:Lmiuix/appcompat/internal/view/menu/i;

.field private o:Ljava/lang/Runnable;

.field private p:Landroid/view/MenuItem$OnMenuItemClickListener;

.field private q:I

.field private r:I

.field private s:Landroid/view/View;

.field private t:Landroid/view/ActionProvider;

.field private u:Landroid/view/MenuItem$OnActionExpandListener;

.field private v:Z

.field private w:Landroid/view/ContextMenu$ContextMenuInfo;


# direct methods
.method constructor <init>(Lmiuix/appcompat/internal/view/menu/c;IIIILjava/lang/CharSequence;I)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lmiuix/appcompat/internal/view/menu/e;->l:I

    const/16 v1, 0x10

    iput v1, p0, Lmiuix/appcompat/internal/view/menu/e;->q:I

    iput-boolean v0, p0, Lmiuix/appcompat/internal/view/menu/e;->v:Z

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/e;->m:Lmiuix/appcompat/internal/view/menu/c;

    iput p3, p0, Lmiuix/appcompat/internal/view/menu/e;->a:I

    iput p2, p0, Lmiuix/appcompat/internal/view/menu/e;->b:I

    iput p4, p0, Lmiuix/appcompat/internal/view/menu/e;->c:I

    iput p5, p0, Lmiuix/appcompat/internal/view/menu/e;->d:I

    iput-object p6, p0, Lmiuix/appcompat/internal/view/menu/e;->e:Ljava/lang/CharSequence;

    iput p7, p0, Lmiuix/appcompat/internal/view/menu/e;->r:I

    return-void
.end method


# virtual methods
.method public c()I
    .locals 1

    iget v0, p0, Lmiuix/appcompat/internal/view/menu/e;->d:I

    return v0
.end method

.method public collapseActionView()Z
    .locals 1

    iget v0, p0, Lmiuix/appcompat/internal/view/menu/e;->r:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/e;->s:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/e;->u:Landroid/view/MenuItem$OnActionExpandListener;

    if-eqz v0, :cond_0

    invoke-interface {v0, p0}, Landroid/view/MenuItem$OnActionExpandListener;->onMenuItemActionCollapse(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/e;->m:Lmiuix/appcompat/internal/view/menu/c;

    invoke-virtual {v0, p0}, Lmiuix/appcompat/internal/view/menu/c;->f(Lmiuix/appcompat/internal/view/menu/e;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method d()C
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return v0

    :goto_1
    iget-char v0, p0, Lmiuix/appcompat/internal/view/menu/e;->j:C

    goto/32 :goto_0

    nop
.end method

.method e()Ljava/lang/String;
    .locals 3

    goto/32 :goto_1

    nop

    :goto_0
    sget-object v0, Lmiuix/appcompat/internal/view/menu/e;->A:Ljava/lang/String;

    goto/32 :goto_18

    nop

    :goto_1
    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/e;->d()C

    move-result v0

    goto/32 :goto_6

    nop

    :goto_2
    sget-object v2, Lmiuix/appcompat/internal/view/menu/e;->x:Ljava/lang/String;

    goto/32 :goto_7

    nop

    :goto_3
    sget-object v0, Lmiuix/appcompat/internal/view/menu/e;->y:Ljava/lang/String;

    goto/32 :goto_10

    nop

    :goto_4
    goto :goto_16

    :goto_5
    goto/32 :goto_0

    nop

    :goto_6
    if-eqz v0, :cond_0

    goto/32 :goto_d

    :cond_0
    goto/32 :goto_1b

    nop

    :goto_7
    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    goto/32 :goto_14

    nop

    :goto_8
    return-object v0

    :goto_9
    if-ne v0, v2, :cond_1

    goto/32 :goto_5

    :cond_1
    goto/32 :goto_b

    nop

    :goto_a
    if-ne v0, v2, :cond_2

    goto/32 :goto_11

    :cond_2
    goto/32 :goto_f

    nop

    :goto_b
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto/32 :goto_4

    nop

    :goto_c
    return-object v0

    :goto_d
    goto/32 :goto_1c

    nop

    :goto_e
    const/16 v2, 0x20

    goto/32 :goto_9

    nop

    :goto_f
    const/16 v2, 0xa

    goto/32 :goto_17

    nop

    :goto_10
    goto :goto_13

    :goto_11
    goto/32 :goto_12

    nop

    :goto_12
    sget-object v0, Lmiuix/appcompat/internal/view/menu/e;->z:Ljava/lang/String;

    :goto_13
    goto/32 :goto_15

    nop

    :goto_14
    const/16 v2, 0x8

    goto/32 :goto_a

    nop

    :goto_15
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_16
    goto/32 :goto_1a

    nop

    :goto_17
    if-ne v0, v2, :cond_3

    goto/32 :goto_19

    :cond_3
    goto/32 :goto_e

    nop

    :goto_18
    goto :goto_13

    :goto_19
    goto/32 :goto_3

    nop

    :goto_1a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_8

    nop

    :goto_1b
    const-string v0, ""

    goto/32 :goto_c

    nop

    :goto_1c
    new-instance v1, Ljava/lang/StringBuilder;

    goto/32 :goto_2

    nop
.end method

.method public expandActionView()Z
    .locals 1

    iget v0, p0, Lmiuix/appcompat/internal/view/menu/e;->r:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/e;->s:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/e;->u:Landroid/view/MenuItem$OnActionExpandListener;

    if-eqz v0, :cond_0

    invoke-interface {v0, p0}, Landroid/view/MenuItem$OnActionExpandListener;->onMenuItemActionExpand(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/e;->m:Lmiuix/appcompat/internal/view/menu/c;

    invoke-virtual {v0, p0}, Lmiuix/appcompat/internal/view/menu/c;->j(Lmiuix/appcompat/internal/view/menu/e;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public f()Landroid/view/ActionProvider;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/e;->t:Landroid/view/ActionProvider;

    return-object v0
.end method

.method g(Lmiuix/appcompat/internal/view/menu/h$a;)Ljava/lang/CharSequence;
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    if-nez p1, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_4

    nop

    :goto_1
    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/e;->getTitle()Ljava/lang/CharSequence;

    move-result-object p1

    :goto_2
    goto/32 :goto_3

    nop

    :goto_3
    return-object p1

    :goto_4
    invoke-interface {p1}, Lmiuix/appcompat/internal/view/menu/h$a;->a()Z

    move-result p1

    goto/32 :goto_7

    nop

    :goto_5
    goto :goto_2

    :goto_6
    goto/32 :goto_1

    nop

    :goto_7
    if-nez p1, :cond_1

    goto/32 :goto_6

    :cond_1
    goto/32 :goto_8

    nop

    :goto_8
    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/e;->getTitleCondensed()Ljava/lang/CharSequence;

    move-result-object p1

    goto/32 :goto_5

    nop
.end method

.method public getActionProvider()Landroid/view/ActionProvider;
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Implementation should use getSupportActionProvider!"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getActionView()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/e;->s:Landroid/view/View;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/e;->t:Landroid/view/ActionProvider;

    if-eqz v0, :cond_1

    invoke-virtual {v0, p0}, Landroid/view/ActionProvider;->onCreateActionView(Landroid/view/MenuItem;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lmiuix/appcompat/internal/view/menu/e;->s:Landroid/view/View;

    return-object v0

    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method public getAlphabeticShortcut()C
    .locals 1

    iget-char v0, p0, Lmiuix/appcompat/internal/view/menu/e;->j:C

    return v0
.end method

.method public getContentDescription()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/e;->g:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getGroupId()I
    .locals 1

    iget v0, p0, Lmiuix/appcompat/internal/view/menu/e;->b:I

    return v0
.end method

.method public getIcon()Landroid/graphics/drawable/Drawable;
    .locals 2

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/e;->k:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    iget v0, p0, Lmiuix/appcompat/internal/view/menu/e;->l:I

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/e;->m:Lmiuix/appcompat/internal/view/menu/c;

    invoke-virtual {v0}, Lmiuix/appcompat/internal/view/menu/c;->z()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p0, Lmiuix/appcompat/internal/view/menu/e;->l:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/4 v1, 0x0

    iput v1, p0, Lmiuix/appcompat/internal/view/menu/e;->l:I

    iput-object v0, p0, Lmiuix/appcompat/internal/view/menu/e;->k:Landroid/graphics/drawable/Drawable;

    return-object v0

    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method public getIntent()Landroid/content/Intent;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/e;->h:Landroid/content/Intent;

    return-object v0
.end method

.method public getItemId()I
    .locals 1
    .annotation runtime Landroid/view/ViewDebug$CapturedViewProperty;
    .end annotation

    iget v0, p0, Lmiuix/appcompat/internal/view/menu/e;->a:I

    return v0
.end method

.method public getMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/e;->w:Landroid/view/ContextMenu$ContextMenuInfo;

    return-object v0
.end method

.method public getNumericShortcut()C
    .locals 1

    iget-char v0, p0, Lmiuix/appcompat/internal/view/menu/e;->i:C

    return v0
.end method

.method public getOrder()I
    .locals 1

    iget v0, p0, Lmiuix/appcompat/internal/view/menu/e;->c:I

    return v0
.end method

.method public getSubMenu()Landroid/view/SubMenu;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/e;->n:Lmiuix/appcompat/internal/view/menu/i;

    return-object v0
.end method

.method public getTitle()Ljava/lang/CharSequence;
    .locals 1
    .annotation runtime Landroid/view/ViewDebug$CapturedViewProperty;
    .end annotation

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/e;->e:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getTitleCondensed()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/e;->f:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/e;->e:Ljava/lang/CharSequence;

    :goto_0
    return-object v0
.end method

.method public h()Z
    .locals 1

    iget v0, p0, Lmiuix/appcompat/internal/view/menu/e;->r:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/e;->s:Landroid/view/View;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hasSubMenu()Z
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/e;->n:Lmiuix/appcompat/internal/view/menu/i;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public i()Z
    .locals 4

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/e;->p:Landroid/view/MenuItem$OnMenuItemClickListener;

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    invoke-interface {v0, p0}, Landroid/view/MenuItem$OnMenuItemClickListener;->onMenuItemClick(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    return v1

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/e;->m:Lmiuix/appcompat/internal/view/menu/c;

    invoke-virtual {v0}, Lmiuix/appcompat/internal/view/menu/c;->A()Lmiuix/appcompat/internal/view/menu/c;

    move-result-object v2

    invoke-virtual {v0, v2, p0}, Lmiuix/appcompat/internal/view/menu/c;->g(Lmiuix/appcompat/internal/view/menu/c;Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_1

    return v1

    :cond_1
    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/e;->o:Ljava/lang/Runnable;

    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    return v1

    :cond_2
    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/e;->h:Landroid/content/Intent;

    if-eqz v0, :cond_3

    :try_start_0
    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/e;->m:Lmiuix/appcompat/internal/view/menu/c;

    invoke-virtual {v0}, Lmiuix/appcompat/internal/view/menu/c;->r()Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, Lmiuix/appcompat/internal/view/menu/e;->h:Landroid/content/Intent;

    invoke-virtual {v0, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    return v1

    :catch_0
    move-exception v0

    const-string v2, "MenuItemImpl"

    const-string v3, "Can\'t find activity to handle intent; ignoring"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_3
    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/e;->t:Landroid/view/ActionProvider;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/view/ActionProvider;->onPerformDefaultAction()Z

    move-result v0

    if-eqz v0, :cond_4

    goto :goto_0

    :cond_4
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public isActionViewExpanded()Z
    .locals 1

    iget-boolean v0, p0, Lmiuix/appcompat/internal/view/menu/e;->v:Z

    return v0
.end method

.method public isCheckable()Z
    .locals 2

    iget v0, p0, Lmiuix/appcompat/internal/view/menu/e;->q:I

    const/4 v1, 0x1

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public isChecked()Z
    .locals 2

    iget v0, p0, Lmiuix/appcompat/internal/view/menu/e;->q:I

    const/4 v1, 0x2

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isEnabled()Z
    .locals 1

    iget v0, p0, Lmiuix/appcompat/internal/view/menu/e;->q:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isVisible()Z
    .locals 3

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/e;->t:Landroid/view/ActionProvider;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/view/ActionProvider;->overridesItemVisibility()Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lmiuix/appcompat/internal/view/menu/e;->q:I

    and-int/lit8 v0, v0, 0x8

    if-nez v0, :cond_0

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/e;->t:Landroid/view/ActionProvider;

    invoke-virtual {v0}, Landroid/view/ActionProvider;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    move v1, v2

    :goto_0
    return v1

    :cond_1
    iget v0, p0, Lmiuix/appcompat/internal/view/menu/e;->q:I

    and-int/lit8 v0, v0, 0x8

    if-nez v0, :cond_2

    goto :goto_1

    :cond_2
    move v1, v2

    :goto_1
    return v1
.end method

.method public j()Z
    .locals 2

    iget v0, p0, Lmiuix/appcompat/internal/view/menu/e;->q:I

    const/16 v1, 0x20

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public k()Z
    .locals 1

    iget v0, p0, Lmiuix/appcompat/internal/view/menu/e;->q:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public l()Z
    .locals 2

    iget v0, p0, Lmiuix/appcompat/internal/view/menu/e;->r:I

    const/4 v1, 0x1

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public m()Z
    .locals 2

    iget v0, p0, Lmiuix/appcompat/internal/view/menu/e;->r:I

    const/4 v1, 0x2

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public n(Z)V
    .locals 1

    iput-boolean p1, p0, Lmiuix/appcompat/internal/view/menu/e;->v:Z

    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/e;->m:Lmiuix/appcompat/internal/view/menu/c;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lmiuix/appcompat/internal/view/menu/c;->G(Z)V

    return-void
.end method

.method o(Z)V
    .locals 3

    goto/32 :goto_5

    nop

    :goto_0
    return-void

    :goto_1
    invoke-virtual {p1, v2}, Lmiuix/appcompat/internal/view/menu/c;->G(Z)V

    :goto_2
    goto/32 :goto_0

    nop

    :goto_3
    if-ne v0, p1, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_7

    nop

    :goto_4
    or-int/2addr p1, v1

    goto/32 :goto_f

    nop

    :goto_5
    iget v0, p0, Lmiuix/appcompat/internal/view/menu/e;->q:I

    goto/32 :goto_8

    nop

    :goto_6
    const/4 v2, 0x0

    goto/32 :goto_e

    nop

    :goto_7
    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/e;->m:Lmiuix/appcompat/internal/view/menu/c;

    goto/32 :goto_1

    nop

    :goto_8
    and-int/lit8 v1, v0, -0x3

    goto/32 :goto_6

    nop

    :goto_9
    move p1, v2

    :goto_a
    goto/32 :goto_4

    nop

    :goto_b
    goto :goto_a

    :goto_c
    goto/32 :goto_9

    nop

    :goto_d
    const/4 p1, 0x2

    goto/32 :goto_b

    nop

    :goto_e
    if-nez p1, :cond_1

    goto/32 :goto_c

    :cond_1
    goto/32 :goto_d

    nop

    :goto_f
    iput p1, p0, Lmiuix/appcompat/internal/view/menu/e;->q:I

    goto/32 :goto_3

    nop
.end method

.method public p(Z)V
    .locals 1

    iget v0, p0, Lmiuix/appcompat/internal/view/menu/e;->q:I

    and-int/lit8 v0, v0, -0x5

    if-eqz p1, :cond_0

    const/4 p1, 0x4

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    or-int/2addr p1, v0

    iput p1, p0, Lmiuix/appcompat/internal/view/menu/e;->q:I

    return-void
.end method

.method public q(Z)V
    .locals 0

    if-eqz p1, :cond_0

    iget p1, p0, Lmiuix/appcompat/internal/view/menu/e;->q:I

    or-int/lit8 p1, p1, 0x20

    goto :goto_0

    :cond_0
    iget p1, p0, Lmiuix/appcompat/internal/view/menu/e;->q:I

    and-int/lit8 p1, p1, -0x21

    :goto_0
    iput p1, p0, Lmiuix/appcompat/internal/view/menu/e;->q:I

    return-void
.end method

.method r(Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/e;->w:Landroid/view/ContextMenu$ContextMenuInfo;

    goto/32 :goto_0

    nop
.end method

.method s(Lmiuix/appcompat/internal/view/menu/i;)V
    .locals 1

    goto/32 :goto_3

    nop

    :goto_0
    invoke-virtual {p1, v0}, Lmiuix/appcompat/internal/view/menu/i;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/SubMenu;

    goto/32 :goto_1

    nop

    :goto_1
    return-void

    :goto_2
    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/e;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    goto/32 :goto_0

    nop

    :goto_3
    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/e;->n:Lmiuix/appcompat/internal/view/menu/i;

    goto/32 :goto_2

    nop
.end method

.method public setActionProvider(Landroid/view/ActionProvider;)Landroid/view/MenuItem;
    .locals 1

    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "Implementation should use setSupportActionProvider!"

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setActionView(I)Landroid/view/MenuItem;
    .locals 3

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/e;->m:Lmiuix/appcompat/internal/view/menu/c;

    invoke-virtual {v0}, Lmiuix/appcompat/internal/view/menu/c;->r()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    new-instance v2, Landroid/widget/LinearLayout;

    invoke-direct {v2, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    invoke-virtual {v1, p1, v2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/view/menu/e;->setActionView(Landroid/view/View;)Landroid/view/MenuItem;

    return-object p0
.end method

.method public setActionView(Landroid/view/View;)Landroid/view/MenuItem;
    .locals 2

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/e;->s:Landroid/view/View;

    const/4 v0, 0x0

    iput-object v0, p0, Lmiuix/appcompat/internal/view/menu/e;->t:Landroid/view/ActionProvider;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    iget v0, p0, Lmiuix/appcompat/internal/view/menu/e;->a:I

    if-lez v0, :cond_0

    invoke-virtual {p1, v0}, Landroid/view/View;->setId(I)V

    :cond_0
    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/e;->m:Lmiuix/appcompat/internal/view/menu/c;

    invoke-virtual {p1, p0}, Lmiuix/appcompat/internal/view/menu/c;->E(Lmiuix/appcompat/internal/view/menu/e;)V

    return-object p0
.end method

.method public setAlphabeticShortcut(C)Landroid/view/MenuItem;
    .locals 1

    iget-char v0, p0, Lmiuix/appcompat/internal/view/menu/e;->j:C

    if-ne v0, p1, :cond_0

    return-object p0

    :cond_0
    invoke-static {p1}, Ljava/lang/Character;->toLowerCase(C)C

    move-result p1

    iput-char p1, p0, Lmiuix/appcompat/internal/view/menu/e;->j:C

    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/e;->m:Lmiuix/appcompat/internal/view/menu/c;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lmiuix/appcompat/internal/view/menu/c;->G(Z)V

    return-object p0
.end method

.method public setCheckable(Z)Landroid/view/MenuItem;
    .locals 2

    iget v0, p0, Lmiuix/appcompat/internal/view/menu/e;->q:I

    and-int/lit8 v1, v0, -0x2

    or-int/2addr p1, v1

    iput p1, p0, Lmiuix/appcompat/internal/view/menu/e;->q:I

    if-eq v0, p1, :cond_0

    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/e;->m:Lmiuix/appcompat/internal/view/menu/c;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lmiuix/appcompat/internal/view/menu/c;->G(Z)V

    :cond_0
    return-object p0
.end method

.method public setChecked(Z)Landroid/view/MenuItem;
    .locals 1

    iget v0, p0, Lmiuix/appcompat/internal/view/menu/e;->q:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/e;->m:Lmiuix/appcompat/internal/view/menu/c;

    invoke-virtual {p1, p0}, Lmiuix/appcompat/internal/view/menu/c;->M(Landroid/view/MenuItem;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/view/menu/e;->o(Z)V

    :goto_0
    return-object p0
.end method

.method public setContentDescription(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 1

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/e;->g:Ljava/lang/CharSequence;

    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/e;->m:Lmiuix/appcompat/internal/view/menu/c;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lmiuix/appcompat/internal/view/menu/c;->G(Z)V

    return-object p0
.end method

.method public setEnabled(Z)Landroid/view/MenuItem;
    .locals 1

    if-eqz p1, :cond_0

    iget p1, p0, Lmiuix/appcompat/internal/view/menu/e;->q:I

    or-int/lit8 p1, p1, 0x10

    goto :goto_0

    :cond_0
    iget p1, p0, Lmiuix/appcompat/internal/view/menu/e;->q:I

    and-int/lit8 p1, p1, -0x11

    :goto_0
    iput p1, p0, Lmiuix/appcompat/internal/view/menu/e;->q:I

    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/e;->m:Lmiuix/appcompat/internal/view/menu/c;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lmiuix/appcompat/internal/view/menu/c;->G(Z)V

    return-object p0
.end method

.method public setIcon(I)Landroid/view/MenuItem;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lmiuix/appcompat/internal/view/menu/e;->k:Landroid/graphics/drawable/Drawable;

    iput p1, p0, Lmiuix/appcompat/internal/view/menu/e;->l:I

    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/e;->m:Lmiuix/appcompat/internal/view/menu/c;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lmiuix/appcompat/internal/view/menu/c;->G(Z)V

    return-object p0
.end method

.method public setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lmiuix/appcompat/internal/view/menu/e;->l:I

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/e;->k:Landroid/graphics/drawable/Drawable;

    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/e;->m:Lmiuix/appcompat/internal/view/menu/c;

    invoke-virtual {p1, v0}, Lmiuix/appcompat/internal/view/menu/c;->G(Z)V

    return-object p0
.end method

.method public setIntent(Landroid/content/Intent;)Landroid/view/MenuItem;
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/e;->h:Landroid/content/Intent;

    return-object p0
.end method

.method public setNumericShortcut(C)Landroid/view/MenuItem;
    .locals 1

    iget-char v0, p0, Lmiuix/appcompat/internal/view/menu/e;->i:C

    if-ne v0, p1, :cond_0

    return-object p0

    :cond_0
    iput-char p1, p0, Lmiuix/appcompat/internal/view/menu/e;->i:C

    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/e;->m:Lmiuix/appcompat/internal/view/menu/c;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lmiuix/appcompat/internal/view/menu/c;->G(Z)V

    return-object p0
.end method

.method public setOnActionExpandListener(Landroid/view/MenuItem$OnActionExpandListener;)Landroid/view/MenuItem;
    .locals 1

    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "Implementation should use setSupportOnActionExpandListener!"

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/e;->p:Landroid/view/MenuItem$OnMenuItemClickListener;

    return-object p0
.end method

.method public setShortcut(CC)Landroid/view/MenuItem;
    .locals 0

    iput-char p1, p0, Lmiuix/appcompat/internal/view/menu/e;->i:C

    invoke-static {p2}, Ljava/lang/Character;->toLowerCase(C)C

    move-result p1

    iput-char p1, p0, Lmiuix/appcompat/internal/view/menu/e;->j:C

    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/e;->m:Lmiuix/appcompat/internal/view/menu/c;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Lmiuix/appcompat/internal/view/menu/c;->G(Z)V

    return-object p0
.end method

.method public setShowAsAction(I)V
    .locals 2

    and-int/lit8 v0, p1, 0x3

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "SHOW_AS_ACTION_ALWAYS, SHOW_AS_ACTION_IF_ROOM, and SHOW_AS_ACTION_NEVER are mutually exclusive."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    :goto_0
    iput p1, p0, Lmiuix/appcompat/internal/view/menu/e;->r:I

    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/e;->m:Lmiuix/appcompat/internal/view/menu/c;

    invoke-virtual {p1, p0}, Lmiuix/appcompat/internal/view/menu/c;->E(Lmiuix/appcompat/internal/view/menu/e;)V

    return-void
.end method

.method public setShowAsActionFlags(I)Landroid/view/MenuItem;
    .locals 0

    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/view/menu/e;->setShowAsAction(I)V

    return-object p0
.end method

.method public setTitle(I)Landroid/view/MenuItem;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/e;->m:Lmiuix/appcompat/internal/view/menu/c;

    invoke-virtual {v0}, Lmiuix/appcompat/internal/view/menu/c;->r()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/view/menu/e;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object p1

    return-object p1
.end method

.method public setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 2

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/e;->e:Ljava/lang/CharSequence;

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/e;->m:Lmiuix/appcompat/internal/view/menu/c;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lmiuix/appcompat/internal/view/menu/c;->G(Z)V

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/e;->n:Lmiuix/appcompat/internal/view/menu/i;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lmiuix/appcompat/internal/view/menu/i;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/SubMenu;

    :cond_0
    return-object p0
.end method

.method public setTitleCondensed(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 1

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/e;->f:Ljava/lang/CharSequence;

    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/e;->m:Lmiuix/appcompat/internal/view/menu/c;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lmiuix/appcompat/internal/view/menu/c;->G(Z)V

    return-object p0
.end method

.method public setVisible(Z)Landroid/view/MenuItem;
    .locals 0

    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/view/menu/e;->t(Z)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/e;->m:Lmiuix/appcompat/internal/view/menu/c;

    invoke-virtual {p1, p0}, Lmiuix/appcompat/internal/view/menu/c;->F(Lmiuix/appcompat/internal/view/menu/e;)V

    :cond_0
    return-object p0
.end method

.method t(Z)Z
    .locals 3

    goto/32 :goto_d

    nop

    :goto_0
    or-int/2addr p1, v1

    goto/32 :goto_8

    nop

    :goto_1
    goto :goto_b

    :goto_2
    goto/32 :goto_a

    nop

    :goto_3
    return v2

    :goto_4
    if-nez p1, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_9

    nop

    :goto_5
    const/4 v2, 0x1

    :goto_6
    goto/32 :goto_3

    nop

    :goto_7
    if-ne v0, p1, :cond_1

    goto/32 :goto_6

    :cond_1
    goto/32 :goto_5

    nop

    :goto_8
    iput p1, p0, Lmiuix/appcompat/internal/view/menu/e;->q:I

    goto/32 :goto_7

    nop

    :goto_9
    move p1, v2

    goto/32 :goto_1

    nop

    :goto_a
    const/16 p1, 0x8

    :goto_b
    goto/32 :goto_0

    nop

    :goto_c
    const/4 v2, 0x0

    goto/32 :goto_4

    nop

    :goto_d
    iget v0, p0, Lmiuix/appcompat/internal/view/menu/e;->q:I

    goto/32 :goto_e

    nop

    :goto_e
    and-int/lit8 v1, v0, -0x9

    goto/32 :goto_c

    nop
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/e;->e:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public u()Z
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/e;->m:Lmiuix/appcompat/internal/view/menu/c;

    invoke-virtual {v0}, Lmiuix/appcompat/internal/view/menu/c;->x()Z

    move-result v0

    return v0
.end method

.method v()Z
    .locals 1

    goto/32 :goto_9

    nop

    :goto_0
    if-nez v0, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_a

    nop

    :goto_1
    invoke-virtual {v0}, Lmiuix/appcompat/internal/view/menu/c;->D()Z

    move-result v0

    goto/32 :goto_8

    nop

    :goto_2
    const/4 v0, 0x0

    :goto_3
    goto/32 :goto_6

    nop

    :goto_4
    goto :goto_3

    :goto_5
    goto/32 :goto_2

    nop

    :goto_6
    return v0

    :goto_7
    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/e;->d()C

    move-result v0

    goto/32 :goto_0

    nop

    :goto_8
    if-nez v0, :cond_1

    goto/32 :goto_5

    :cond_1
    goto/32 :goto_7

    nop

    :goto_9
    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/e;->m:Lmiuix/appcompat/internal/view/menu/c;

    goto/32 :goto_1

    nop

    :goto_a
    const/4 v0, 0x1

    goto/32 :goto_4

    nop
.end method
