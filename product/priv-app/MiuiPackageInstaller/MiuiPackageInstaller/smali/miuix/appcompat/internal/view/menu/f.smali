.class public Lmiuix/appcompat/internal/view/menu/f;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/view/View$OnKeyListener;
.implements Landroid/widget/PopupWindow$OnDismissListener;
.implements Lmiuix/appcompat/internal/view/menu/g;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiuix/appcompat/internal/view/menu/f$a;
    }
.end annotation


# static fields
.field private static final k:I


# instance fields
.field a:Z

.field private b:Landroid/content/Context;

.field private c:Landroid/view/LayoutInflater;

.field private d:Lja/e;

.field private e:Lmiuix/appcompat/internal/view/menu/c;

.field private f:Landroid/view/View;

.field private g:Z

.field private h:Lmiuix/appcompat/internal/view/menu/f$a;

.field private i:Lmiuix/appcompat/internal/view/menu/g$a;

.field private j:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget v0, Lk9/i;->A:I

    sput v0, Lmiuix/appcompat/internal/view/menu/f;->k:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lmiuix/appcompat/internal/view/menu/c;Landroid/view/View;Z)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget v0, Lmiuix/appcompat/internal/view/menu/f;->k:I

    iput v0, p0, Lmiuix/appcompat/internal/view/menu/f;->j:I

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/f;->b:Landroid/content/Context;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/f;->c:Landroid/view/LayoutInflater;

    iput-object p2, p0, Lmiuix/appcompat/internal/view/menu/f;->e:Lmiuix/appcompat/internal/view/menu/c;

    iput-boolean p4, p0, Lmiuix/appcompat/internal/view/menu/f;->g:Z

    iput-object p3, p0, Lmiuix/appcompat/internal/view/menu/f;->f:Landroid/view/View;

    invoke-virtual {p2, p0}, Lmiuix/appcompat/internal/view/menu/c;->b(Lmiuix/appcompat/internal/view/menu/g;)V

    return-void
.end method

.method static synthetic k(Lmiuix/appcompat/internal/view/menu/f;)Z
    .locals 0

    iget-boolean p0, p0, Lmiuix/appcompat/internal/view/menu/f;->g:Z

    return p0
.end method

.method static synthetic l(Lmiuix/appcompat/internal/view/menu/f;)I
    .locals 0

    iget p0, p0, Lmiuix/appcompat/internal/view/menu/f;->j:I

    return p0
.end method

.method static synthetic m(Lmiuix/appcompat/internal/view/menu/f;)Landroid/view/LayoutInflater;
    .locals 0

    iget-object p0, p0, Lmiuix/appcompat/internal/view/menu/f;->c:Landroid/view/LayoutInflater;

    return-object p0
.end method

.method static synthetic n(Lmiuix/appcompat/internal/view/menu/f;)Lmiuix/appcompat/internal/view/menu/c;
    .locals 0

    iget-object p0, p0, Lmiuix/appcompat/internal/view/menu/f;->e:Lmiuix/appcompat/internal/view/menu/c;

    return-object p0
.end method


# virtual methods
.method public a(Z)V
    .locals 0

    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/f;->isShowing()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/f;->d:Lja/e;

    invoke-virtual {p1}, Lja/e;->dismiss()V

    :cond_0
    return-void
.end method

.method public b(Lmiuix/appcompat/internal/view/menu/c;Z)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/f;->e:Lmiuix/appcompat/internal/view/menu/c;

    if-eq p1, v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lmiuix/appcompat/internal/view/menu/f;->a(Z)V

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/f;->i:Lmiuix/appcompat/internal/view/menu/g$a;

    if-eqz v0, :cond_1

    invoke-interface {v0, p1, p2}, Lmiuix/appcompat/internal/view/menu/g$a;->b(Lmiuix/appcompat/internal/view/menu/c;Z)V

    :cond_1
    return-void
.end method

.method public c(Z)V
    .locals 0

    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/f;->h:Lmiuix/appcompat/internal/view/menu/f$a;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lmiuix/appcompat/internal/view/menu/f$a;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public d()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public e(Lmiuix/appcompat/internal/view/menu/i;)Z
    .locals 7

    invoke-virtual {p1}, Lmiuix/appcompat/internal/view/menu/c;->hasVisibleItems()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_3

    new-instance v0, Lmiuix/appcompat/internal/view/menu/f;

    iget-object v2, p0, Lmiuix/appcompat/internal/view/menu/f;->b:Landroid/content/Context;

    iget-object v3, p0, Lmiuix/appcompat/internal/view/menu/f;->f:Landroid/view/View;

    invoke-direct {v0, v2, p1, v3, v1}, Lmiuix/appcompat/internal/view/menu/f;-><init>(Landroid/content/Context;Lmiuix/appcompat/internal/view/menu/c;Landroid/view/View;Z)V

    iget-object v2, p0, Lmiuix/appcompat/internal/view/menu/f;->i:Lmiuix/appcompat/internal/view/menu/g$a;

    invoke-virtual {v0, v2}, Lmiuix/appcompat/internal/view/menu/f;->o(Lmiuix/appcompat/internal/view/menu/g$a;)V

    invoke-virtual {p1}, Lmiuix/appcompat/internal/view/menu/c;->size()I

    move-result v2

    move v3, v1

    :goto_0
    const/4 v4, 0x1

    if-ge v3, v2, :cond_1

    invoke-virtual {p1, v3}, Lmiuix/appcompat/internal/view/menu/c;->getItem(I)Landroid/view/MenuItem;

    move-result-object v5

    invoke-interface {v5}, Landroid/view/MenuItem;->isVisible()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v5}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    if-eqz v5, :cond_0

    move v2, v4

    goto :goto_1

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    move v2, v1

    :goto_1
    invoke-virtual {v0, v2}, Lmiuix/appcompat/internal/view/menu/f;->p(Z)V

    invoke-virtual {v0}, Lmiuix/appcompat/internal/view/menu/f;->f()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/f;->i:Lmiuix/appcompat/internal/view/menu/g$a;

    if-eqz v0, :cond_2

    invoke-interface {v0, p1}, Lmiuix/appcompat/internal/view/menu/g$a;->f(Lmiuix/appcompat/internal/view/menu/c;)Z

    :cond_2
    return v4

    :cond_3
    return v1
.end method

.method public f()Z
    .locals 3

    new-instance v0, Lja/e;

    iget-object v1, p0, Lmiuix/appcompat/internal/view/menu/f;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, Lja/e;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lmiuix/appcompat/internal/view/menu/f;->d:Lja/e;

    iget-object v1, p0, Lmiuix/appcompat/internal/view/menu/f;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lk9/e;->R:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lja/e;->P(I)V

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/f;->d:Lja/e;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lja/e;->O(Z)V

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/f;->d:Lja/e;

    invoke-virtual {v0, p0}, Lja/e;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/f;->d:Lja/e;

    invoke-virtual {v0, p0}, Lja/e;->Q(Landroid/widget/AdapterView$OnItemClickListener;)V

    new-instance v0, Lmiuix/appcompat/internal/view/menu/f$a;

    iget-object v2, p0, Lmiuix/appcompat/internal/view/menu/f;->e:Lmiuix/appcompat/internal/view/menu/c;

    invoke-direct {v0, p0, v2}, Lmiuix/appcompat/internal/view/menu/f$a;-><init>(Lmiuix/appcompat/internal/view/menu/f;Lmiuix/appcompat/internal/view/menu/c;)V

    iput-object v0, p0, Lmiuix/appcompat/internal/view/menu/f;->h:Lmiuix/appcompat/internal/view/menu/f$a;

    iget-object v2, p0, Lmiuix/appcompat/internal/view/menu/f;->d:Lja/e;

    invoke-virtual {v2, v0}, Lja/e;->i(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/f;->d:Lja/e;

    invoke-virtual {v0}, Lja/e;->D()I

    move-result v2

    neg-int v2, v2

    invoke-virtual {v0, v2}, Lja/e;->f(I)V

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/f;->d:Lja/e;

    invoke-virtual {v0, v1}, Lja/e;->d(I)V

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/f;->d:Lja/e;

    iget-object v1, p0, Lmiuix/appcompat/internal/view/menu/f;->f:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lja/e;->k(Landroid/view/View;Landroid/view/ViewGroup;)V

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/f;->d:Lja/e;

    invoke-virtual {v0}, Lja/e;->C()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    const/4 v0, 0x1

    return v0
.end method

.method public g(Lmiuix/appcompat/internal/view/menu/c;Lmiuix/appcompat/internal/view/menu/e;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public h(Landroid/content/Context;Lmiuix/appcompat/internal/view/menu/c;)V
    .locals 0

    return-void
.end method

.method public i(Lmiuix/appcompat/internal/view/menu/c;Lmiuix/appcompat/internal/view/menu/e;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public isShowing()Z
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/f;->d:Lja/e;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public o(Lmiuix/appcompat/internal/view/menu/g$a;)V
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/f;->i:Lmiuix/appcompat/internal/view/menu/g$a;

    return-void
.end method

.method public onDismiss()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lmiuix/appcompat/internal/view/menu/f;->d:Lja/e;

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/f;->e:Lmiuix/appcompat/internal/view/menu/c;

    invoke-virtual {v0}, Lmiuix/appcompat/internal/view/menu/c;->close()V

    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView<",
            "*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/f;->h:Lmiuix/appcompat/internal/view/menu/f$a;

    invoke-static {p1}, Lmiuix/appcompat/internal/view/menu/f$a;->a(Lmiuix/appcompat/internal/view/menu/f$a;)Lmiuix/appcompat/internal/view/menu/c;

    move-result-object p2

    invoke-virtual {p1, p3}, Lmiuix/appcompat/internal/view/menu/f$a;->c(I)Lmiuix/appcompat/internal/view/menu/e;

    move-result-object p1

    const/4 p3, 0x0

    invoke-virtual {p2, p1, p3}, Lmiuix/appcompat/internal/view/menu/c;->H(Landroid/view/MenuItem;I)Z

    return-void
.end method

.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 1

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result p1

    const/4 p3, 0x0

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    const/16 p1, 0x52

    if-ne p2, p1, :cond_0

    invoke-virtual {p0, p3}, Lmiuix/appcompat/internal/view/menu/f;->a(Z)V

    return v0

    :cond_0
    return p3
.end method

.method public p(Z)V
    .locals 0

    iput-boolean p1, p0, Lmiuix/appcompat/internal/view/menu/f;->a:Z

    return-void
.end method

.method public q(I)V
    .locals 0

    iput p1, p0, Lmiuix/appcompat/internal/view/menu/f;->j:I

    return-void
.end method
