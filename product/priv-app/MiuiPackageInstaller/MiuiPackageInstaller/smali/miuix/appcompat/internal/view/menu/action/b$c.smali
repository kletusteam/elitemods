.class Lmiuix/appcompat/internal/view/menu/action/b$c;
.super Ljava/lang/Object;

# interfaces
.implements Lmiuix/appcompat/internal/view/menu/action/b$e;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiuix/appcompat/internal/view/menu/action/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "c"
.end annotation


# instance fields
.field private a:Lmiuix/appcompat/internal/view/menu/b;

.field final synthetic b:Lmiuix/appcompat/internal/view/menu/action/b;


# direct methods
.method private constructor <init>(Lmiuix/appcompat/internal/view/menu/action/b;)V
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/action/b$c;->b:Lmiuix/appcompat/internal/view/menu/action/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lmiuix/appcompat/internal/view/menu/action/b;Lmiuix/appcompat/internal/view/menu/action/b$a;)V
    .locals 0

    invoke-direct {p0, p1}, Lmiuix/appcompat/internal/view/menu/action/b$c;-><init>(Lmiuix/appcompat/internal/view/menu/action/b;)V

    return-void
.end method

.method private b(Lmiuix/appcompat/internal/view/menu/c;)Lmiuix/appcompat/internal/view/menu/b;
    .locals 4

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/action/b$c;->a:Lmiuix/appcompat/internal/view/menu/b;

    if-nez v0, :cond_0

    new-instance v0, Lmiuix/appcompat/internal/view/menu/b;

    iget-object v1, p0, Lmiuix/appcompat/internal/view/menu/action/b$c;->b:Lmiuix/appcompat/internal/view/menu/action/b;

    invoke-static {v1}, Lmiuix/appcompat/internal/view/menu/action/b;->D(Lmiuix/appcompat/internal/view/menu/action/b;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lmiuix/appcompat/internal/view/menu/action/b$c;->b:Lmiuix/appcompat/internal/view/menu/action/b;

    invoke-static {v2}, Lmiuix/appcompat/internal/view/menu/action/b;->E(Lmiuix/appcompat/internal/view/menu/action/b;)I

    move-result v2

    iget-object v3, p0, Lmiuix/appcompat/internal/view/menu/action/b$c;->b:Lmiuix/appcompat/internal/view/menu/action/b;

    invoke-static {v3}, Lmiuix/appcompat/internal/view/menu/action/b;->F(Lmiuix/appcompat/internal/view/menu/action/b;)I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lmiuix/appcompat/internal/view/menu/b;-><init>(Landroid/content/Context;II)V

    iput-object v0, p0, Lmiuix/appcompat/internal/view/menu/action/b$c;->a:Lmiuix/appcompat/internal/view/menu/b;

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/action/b$c;->a:Lmiuix/appcompat/internal/view/menu/b;

    invoke-virtual {p1, v0}, Lmiuix/appcompat/internal/view/menu/c;->b(Lmiuix/appcompat/internal/view/menu/g;)V

    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/action/b$c;->a:Lmiuix/appcompat/internal/view/menu/b;

    return-object p1
.end method


# virtual methods
.method public a(Z)V
    .locals 1

    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/action/b$c;->b:Lmiuix/appcompat/internal/view/menu/action/b;

    invoke-static {p1}, Lmiuix/appcompat/internal/view/menu/action/b;->K(Lmiuix/appcompat/internal/view/menu/action/b;)Lmiuix/appcompat/internal/view/menu/h;

    move-result-object p1

    check-cast p1, Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView;

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/action/b$c;->b:Lmiuix/appcompat/internal/view/menu/action/b;

    invoke-static {v0}, Lmiuix/appcompat/internal/view/menu/action/b;->H(Lmiuix/appcompat/internal/view/menu/action/b;)Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    move-result-object v0

    invoke-virtual {p1, v0}, Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView;->r(Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;)Z

    return-void
.end method

.method public c(Lmiuix/appcompat/internal/view/menu/c;)Landroid/view/View;
    .locals 1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lmiuix/appcompat/internal/view/menu/c;->w()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    invoke-direct {p0, p1}, Lmiuix/appcompat/internal/view/menu/action/b$c;->b(Lmiuix/appcompat/internal/view/menu/c;)Lmiuix/appcompat/internal/view/menu/b;

    move-result-object p1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/action/b$c;->b:Lmiuix/appcompat/internal/view/menu/action/b;

    invoke-static {v0}, Lmiuix/appcompat/internal/view/menu/action/b;->G(Lmiuix/appcompat/internal/view/menu/action/b;)Lmiuix/appcompat/internal/view/menu/h;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {p1, v0}, Lmiuix/appcompat/internal/view/menu/b;->j(Landroid/view/ViewGroup;)Lmiuix/appcompat/internal/view/menu/h;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method public f()Z
    .locals 2

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/action/b$c;->b:Lmiuix/appcompat/internal/view/menu/action/b;

    invoke-static {v0}, Lmiuix/appcompat/internal/view/menu/action/b;->I(Lmiuix/appcompat/internal/view/menu/action/b;)Lmiuix/appcompat/internal/view/menu/h;

    move-result-object v0

    check-cast v0, Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView;

    iget-object v1, p0, Lmiuix/appcompat/internal/view/menu/action/b$c;->b:Lmiuix/appcompat/internal/view/menu/action/b;

    invoke-static {v1}, Lmiuix/appcompat/internal/view/menu/action/b;->H(Lmiuix/appcompat/internal/view/menu/action/b;)Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView;->v(Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;)Z

    move-result v0

    return v0
.end method

.method public isShowing()Z
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/action/b$c;->b:Lmiuix/appcompat/internal/view/menu/action/b;

    invoke-static {v0}, Lmiuix/appcompat/internal/view/menu/action/b;->J(Lmiuix/appcompat/internal/view/menu/action/b;)Lmiuix/appcompat/internal/view/menu/h;

    move-result-object v0

    check-cast v0, Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView;

    invoke-virtual {v0}, Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView;->t()Z

    move-result v0

    return v0
.end method

.method public j(Lmiuix/appcompat/internal/view/menu/c;)V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/action/b$c;->b:Lmiuix/appcompat/internal/view/menu/action/b;

    invoke-static {v0}, Lmiuix/appcompat/internal/view/menu/action/b;->u(Lmiuix/appcompat/internal/view/menu/action/b;)Lmiuix/appcompat/internal/view/menu/h;

    move-result-object v0

    check-cast v0, Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView;

    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/view/menu/action/b$c;->c(Lmiuix/appcompat/internal/view/menu/c;)Landroid/view/View;

    move-result-object p1

    invoke-virtual {v0, p1}, Lmiuix/appcompat/internal/view/menu/action/PhoneActionMenuView;->setOverflowMenuView(Landroid/view/View;)V

    return-void
.end method
