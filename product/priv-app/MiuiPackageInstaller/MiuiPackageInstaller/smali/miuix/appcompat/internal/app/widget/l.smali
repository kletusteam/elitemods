.class public Lmiuix/appcompat/internal/app/widget/l;
.super Lmiuix/appcompat/internal/app/widget/ScrollingTabContainerView;


# virtual methods
.method getDefaultTabTextStyle()I
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return v0

    :goto_1
    sget v0, Lk9/b;->h:I

    goto/32 :goto_0

    nop
.end method

.method getTabBarLayoutRes()I
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    sget v0, Lk9/i;->j:I

    goto/32 :goto_1

    nop

    :goto_1
    return v0
.end method

.method getTabContainerHeight()I
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    const/4 v0, -0x2

    goto/32 :goto_1

    nop

    :goto_1
    return v0
.end method

.method getTabViewLayoutRes()I
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    sget v0, Lk9/i;->f:I

    goto/32 :goto_1

    nop

    :goto_1
    return v0
.end method

.method getTabViewMarginHorizontal()I
    .locals 2

    goto/32 :goto_3

    nop

    :goto_0
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    goto/32 :goto_2

    nop

    :goto_1
    return v0

    :goto_2
    sget v1, Lk9/e;->i:I

    goto/32 :goto_4

    nop

    :goto_3
    invoke-virtual {p0}, Landroid/widget/HorizontalScrollView;->getContext()Landroid/content/Context;

    move-result-object v0

    goto/32 :goto_0

    nop

    :goto_4
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    goto/32 :goto_1

    nop
.end method
