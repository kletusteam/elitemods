.class public Lmiuix/appcompat/internal/view/menu/c;
.super Lcom/android/internal/view/menu/MenuBuilder;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiuix/appcompat/internal/view/menu/c$b;,
        Lmiuix/appcompat/internal/view/menu/c$a;
    }
.end annotation


# static fields
.field private static final x:[I


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/content/res/Resources;

.field private c:Z

.field private d:Z

.field private e:Lmiuix/appcompat/internal/view/menu/c$a;

.field private f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lmiuix/appcompat/internal/view/menu/e;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lmiuix/appcompat/internal/view/menu/e;",
            ">;"
        }
    .end annotation
.end field

.field private h:Z

.field private i:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lmiuix/appcompat/internal/view/menu/e;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lmiuix/appcompat/internal/view/menu/e;",
            ">;"
        }
    .end annotation
.end field

.field private k:Z

.field private l:I

.field private m:Landroid/view/ContextMenu$ContextMenuInfo;

.field n:Ljava/lang/CharSequence;

.field o:Landroid/graphics/drawable/Drawable;

.field p:Landroid/view/View;

.field private q:Z

.field private r:Z

.field private s:Z

.field private t:Z

.field private u:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lmiuix/appcompat/internal/view/menu/e;",
            ">;"
        }
    .end annotation
.end field

.field private v:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList<",
            "Ljava/lang/ref/WeakReference<",
            "Lmiuix/appcompat/internal/view/menu/g;",
            ">;>;"
        }
    .end annotation
.end field

.field private w:Lmiuix/appcompat/internal/view/menu/e;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lmiuix/appcompat/internal/view/menu/c;->x:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x1
        0x4
        0x5
        0x3
        0x2
        0x0
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Lcom/android/internal/view/menu/MenuBuilder;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lmiuix/appcompat/internal/view/menu/c;->l:I

    iput-boolean v0, p0, Lmiuix/appcompat/internal/view/menu/c;->q:Z

    iput-boolean v0, p0, Lmiuix/appcompat/internal/view/menu/c;->r:Z

    iput-boolean v0, p0, Lmiuix/appcompat/internal/view/menu/c;->s:Z

    iput-boolean v0, p0, Lmiuix/appcompat/internal/view/menu/c;->t:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiuix/appcompat/internal/view/menu/c;->u:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lmiuix/appcompat/internal/view/menu/c;->v:Ljava/util/concurrent/CopyOnWriteArrayList;

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/c;->a:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/c;->b:Landroid/content/res/Resources;

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/c;->f:Ljava/util/ArrayList;

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/c;->g:Ljava/util/ArrayList;

    const/4 p1, 0x1

    iput-boolean p1, p0, Lmiuix/appcompat/internal/view/menu/c;->h:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiuix/appcompat/internal/view/menu/c;->i:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmiuix/appcompat/internal/view/menu/c;->j:Ljava/util/ArrayList;

    iput-boolean p1, p0, Lmiuix/appcompat/internal/view/menu/c;->k:Z

    invoke-direct {p0, p1}, Lmiuix/appcompat/internal/view/menu/c;->T(Z)V

    return-void
.end method

.method private I(IZ)V
    .locals 1

    if-ltz p1, :cond_1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/c;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/c;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    if-eqz p2, :cond_1

    const/4 p1, 0x1

    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/view/menu/c;->G(Z)V

    :cond_1
    :goto_0
    return-void
.end method

.method private P(ILjava/lang/CharSequence;ILandroid/graphics/drawable/Drawable;Landroid/view/View;)V
    .locals 2

    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/c;->z()Landroid/content/res/Resources;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz p5, :cond_0

    iput-object p5, p0, Lmiuix/appcompat/internal/view/menu/c;->p:Landroid/view/View;

    iput-object v1, p0, Lmiuix/appcompat/internal/view/menu/c;->n:Ljava/lang/CharSequence;

    iput-object v1, p0, Lmiuix/appcompat/internal/view/menu/c;->o:Landroid/graphics/drawable/Drawable;

    goto :goto_2

    :cond_0
    if-lez p1, :cond_1

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/c;->n:Ljava/lang/CharSequence;

    goto :goto_0

    :cond_1
    if-eqz p2, :cond_2

    iput-object p2, p0, Lmiuix/appcompat/internal/view/menu/c;->n:Ljava/lang/CharSequence;

    :cond_2
    :goto_0
    if-lez p3, :cond_3

    invoke-virtual {v0, p3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/c;->o:Landroid/graphics/drawable/Drawable;

    goto :goto_1

    :cond_3
    if-eqz p4, :cond_4

    iput-object p4, p0, Lmiuix/appcompat/internal/view/menu/c;->o:Landroid/graphics/drawable/Drawable;

    :cond_4
    :goto_1
    iput-object v1, p0, Lmiuix/appcompat/internal/view/menu/c;->p:Landroid/view/View;

    :goto_2
    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/view/menu/c;->G(Z)V

    return-void
.end method

.method private T(Z)V
    .locals 2

    const/4 v0, 0x1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/c;->b:Landroid/content/res/Resources;

    invoke-virtual {p1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object p1

    iget p1, p1, Landroid/content/res/Configuration;->keyboard:I

    if-eq p1, v0, :cond_0

    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/c;->b:Landroid/content/res/Resources;

    sget v1, Lk9/c;->b:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lmiuix/appcompat/internal/view/menu/c;->d:Z

    return-void
.end method

.method private a(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 10

    invoke-static {p3}, Lmiuix/appcompat/internal/view/menu/c;->y(I)I

    move-result v8

    new-instance v9, Lmiuix/appcompat/internal/view/menu/e;

    iget v7, p0, Lmiuix/appcompat/internal/view/menu/c;->l:I

    move-object v0, v9

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, v8

    move-object v6, p4

    invoke-direct/range {v0 .. v7}, Lmiuix/appcompat/internal/view/menu/e;-><init>(Lmiuix/appcompat/internal/view/menu/c;IIIILjava/lang/CharSequence;I)V

    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/c;->m:Landroid/view/ContextMenu$ContextMenuInfo;

    if-eqz p1, :cond_0

    invoke-virtual {v9, p1}, Lmiuix/appcompat/internal/view/menu/e;->r(Landroid/view/ContextMenu$ContextMenuInfo;)V

    :cond_0
    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/c;->f:Ljava/util/ArrayList;

    invoke-static {p1, v8}, Lmiuix/appcompat/internal/view/menu/c;->m(Ljava/util/ArrayList;I)I

    move-result p2

    invoke-virtual {p1, p2, v9}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    const/4 p1, 0x1

    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/view/menu/c;->G(Z)V

    return-object v9
.end method

.method private h(Z)V
    .locals 3

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/c;->v:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/c;->V()V

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/c;->v:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiuix/appcompat/internal/view/menu/g;

    if-nez v2, :cond_1

    iget-object v2, p0, Lmiuix/appcompat/internal/view/menu/c;->v:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-interface {v2, p1}, Lmiuix/appcompat/internal/view/menu/g;->c(Z)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/c;->U()V

    return-void
.end method

.method private i(Lmiuix/appcompat/internal/view/menu/i;)Z
    .locals 4

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/c;->v:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/c;->v:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lmiuix/appcompat/internal/view/menu/g;

    if-nez v3, :cond_2

    iget-object v3, p0, Lmiuix/appcompat/internal/view/menu/c;->v:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v3, v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    if-nez v1, :cond_1

    invoke-interface {v3, p1}, Lmiuix/appcompat/internal/view/menu/g;->e(Lmiuix/appcompat/internal/view/menu/i;)Z

    move-result v1

    goto :goto_0

    :cond_3
    return v1
.end method

.method private static m(Ljava/util/ArrayList;I)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lmiuix/appcompat/internal/view/menu/e;",
            ">;I)I"
        }
    .end annotation

    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiuix/appcompat/internal/view/menu/e;

    invoke-virtual {v1}, Lmiuix/appcompat/internal/view/menu/e;->c()I

    move-result v1

    if-gt v1, p1, :cond_0

    add-int/lit8 v0, v0, 0x1

    return v0

    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    return p0
.end method

.method private static y(I)I
    .locals 3

    const/high16 v0, -0x10000

    and-int/2addr v0, p0

    shr-int/lit8 v0, v0, 0x10

    if-ltz v0, :cond_0

    sget-object v1, Lmiuix/appcompat/internal/view/menu/c;->x:[I

    array-length v2, v1

    if-ge v0, v2, :cond_0

    aget v0, v1, v0

    shl-int/lit8 v0, v0, 0x10

    const v1, 0xffff

    and-int/2addr p0, v1

    or-int/2addr p0, v0

    return p0

    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string v0, "order does not contain a valid category."

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method


# virtual methods
.method public A()Lmiuix/appcompat/internal/view/menu/c;
    .locals 0

    return-object p0
.end method

.method public B()Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lmiuix/appcompat/internal/view/menu/e;",
            ">;"
        }
    .end annotation

    iget-boolean v0, p0, Lmiuix/appcompat/internal/view/menu/c;->h:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/c;->g:Ljava/util/ArrayList;

    return-object v0

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/c;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/c;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiuix/appcompat/internal/view/menu/e;

    invoke-virtual {v1}, Lmiuix/appcompat/internal/view/menu/e;->isVisible()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lmiuix/appcompat/internal/view/menu/c;->g:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiuix/appcompat/internal/view/menu/c;->h:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiuix/appcompat/internal/view/menu/c;->k:Z

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/c;->g:Ljava/util/ArrayList;

    return-object v0
.end method

.method C()Z
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-boolean v0, p0, Lmiuix/appcompat/internal/view/menu/c;->c:Z

    goto/32 :goto_1

    nop

    :goto_1
    return v0
.end method

.method public D()Z
    .locals 1

    iget-boolean v0, p0, Lmiuix/appcompat/internal/view/menu/c;->d:Z

    return v0
.end method

.method E(Lmiuix/appcompat/internal/view/menu/e;)V
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    const/4 p1, 0x1

    goto/32 :goto_2

    nop

    :goto_2
    iput-boolean p1, p0, Lmiuix/appcompat/internal/view/menu/c;->k:Z

    goto/32 :goto_3

    nop

    :goto_3
    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/view/menu/c;->G(Z)V

    goto/32 :goto_0

    nop
.end method

.method F(Lmiuix/appcompat/internal/view/menu/e;)V
    .locals 0

    goto/32 :goto_3

    nop

    :goto_0
    return-void

    :goto_1
    iput-boolean p1, p0, Lmiuix/appcompat/internal/view/menu/c;->h:Z

    goto/32 :goto_2

    nop

    :goto_2
    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/view/menu/c;->G(Z)V

    goto/32 :goto_0

    nop

    :goto_3
    const/4 p1, 0x1

    goto/32 :goto_1

    nop
.end method

.method G(Z)V
    .locals 2

    goto/32 :goto_8

    nop

    :goto_0
    goto :goto_a

    :goto_1
    goto/32 :goto_9

    nop

    :goto_2
    if-eqz v0, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_4

    nop

    :goto_3
    invoke-direct {p0, p1}, Lmiuix/appcompat/internal/view/menu/c;->h(Z)V

    goto/32 :goto_0

    nop

    :goto_4
    if-nez p1, :cond_1

    goto/32 :goto_7

    :cond_1
    goto/32 :goto_c

    nop

    :goto_5
    const/4 v1, 0x1

    goto/32 :goto_2

    nop

    :goto_6
    iput-boolean v1, p0, Lmiuix/appcompat/internal/view/menu/c;->k:Z

    :goto_7
    goto/32 :goto_3

    nop

    :goto_8
    iget-boolean v0, p0, Lmiuix/appcompat/internal/view/menu/c;->q:Z

    goto/32 :goto_5

    nop

    :goto_9
    iput-boolean v1, p0, Lmiuix/appcompat/internal/view/menu/c;->r:Z

    :goto_a
    goto/32 :goto_b

    nop

    :goto_b
    return-void

    :goto_c
    iput-boolean v1, p0, Lmiuix/appcompat/internal/view/menu/c;->h:Z

    goto/32 :goto_6

    nop
.end method

.method public H(Landroid/view/MenuItem;I)Z
    .locals 6

    check-cast p1, Lmiuix/appcompat/internal/view/menu/e;

    const/4 v0, 0x0

    if-eqz p1, :cond_8

    invoke-virtual {p1}, Lmiuix/appcompat/internal/view/menu/e;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_4

    :cond_0
    invoke-virtual {p1}, Lmiuix/appcompat/internal/view/menu/e;->i()Z

    move-result v1

    invoke-virtual {p1}, Lmiuix/appcompat/internal/view/menu/e;->f()Landroid/view/ActionProvider;

    move-result-object v2

    const/4 v3, 0x1

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Landroid/view/ActionProvider;->hasSubMenu()Z

    move-result v4

    if-eqz v4, :cond_1

    move v4, v3

    goto :goto_0

    :cond_1
    move v4, v0

    :goto_0
    invoke-virtual {p1}, Lmiuix/appcompat/internal/view/menu/e;->h()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {p1}, Lmiuix/appcompat/internal/view/menu/e;->expandActionView()Z

    move-result p1

    or-int/2addr v1, p1

    if-eqz v1, :cond_7

    :goto_1
    invoke-virtual {p0, v3}, Lmiuix/appcompat/internal/view/menu/c;->e(Z)V

    goto :goto_3

    :cond_2
    invoke-virtual {p1}, Lmiuix/appcompat/internal/view/menu/e;->hasSubMenu()Z

    move-result v5

    if-nez v5, :cond_4

    if-eqz v4, :cond_3

    goto :goto_2

    :cond_3
    and-int/lit8 p1, p2, 0x1

    if-nez p1, :cond_7

    goto :goto_1

    :cond_4
    :goto_2
    invoke-virtual {p0, v0}, Lmiuix/appcompat/internal/view/menu/c;->e(Z)V

    invoke-virtual {p1}, Lmiuix/appcompat/internal/view/menu/e;->hasSubMenu()Z

    move-result p2

    if-nez p2, :cond_5

    new-instance p2, Lmiuix/appcompat/internal/view/menu/i;

    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/c;->r()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p2, v0, p0, p1}, Lmiuix/appcompat/internal/view/menu/i;-><init>(Landroid/content/Context;Lmiuix/appcompat/internal/view/menu/c;Lmiuix/appcompat/internal/view/menu/e;)V

    invoke-virtual {p1, p2}, Lmiuix/appcompat/internal/view/menu/e;->s(Lmiuix/appcompat/internal/view/menu/i;)V

    :cond_5
    invoke-virtual {p1}, Lmiuix/appcompat/internal/view/menu/e;->getSubMenu()Landroid/view/SubMenu;

    move-result-object p1

    check-cast p1, Lmiuix/appcompat/internal/view/menu/i;

    if-eqz v4, :cond_6

    invoke-virtual {v2, p1}, Landroid/view/ActionProvider;->onPrepareSubMenu(Landroid/view/SubMenu;)V

    :cond_6
    invoke-direct {p0, p1}, Lmiuix/appcompat/internal/view/menu/c;->i(Lmiuix/appcompat/internal/view/menu/i;)Z

    move-result p1

    or-int/2addr v1, p1

    if-nez v1, :cond_7

    goto :goto_1

    :cond_7
    :goto_3
    return v1

    :cond_8
    :goto_4
    return v0
.end method

.method public J(Lmiuix/appcompat/internal/view/menu/g;)V
    .locals 3

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/c;->v:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiuix/appcompat/internal/view/menu/g;

    if-eqz v2, :cond_1

    if-ne v2, p1, :cond_0

    :cond_1
    iget-object v2, p0, Lmiuix/appcompat/internal/view/menu/c;->v:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    return-void
.end method

.method public K(Lmiuix/appcompat/internal/view/menu/c$a;)V
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/c;->e:Lmiuix/appcompat/internal/view/menu/c$a;

    return-void
.end method

.method public L(I)Lmiuix/appcompat/internal/view/menu/c;
    .locals 0

    iput p1, p0, Lmiuix/appcompat/internal/view/menu/c;->l:I

    return-object p0
.end method

.method M(Landroid/view/MenuItem;)V
    .locals 4

    goto/32 :goto_5

    nop

    :goto_0
    goto :goto_14

    :goto_1
    goto/32 :goto_2

    nop

    :goto_2
    invoke-virtual {v2}, Lmiuix/appcompat/internal/view/menu/e;->isCheckable()Z

    move-result v3

    goto/32 :goto_6

    nop

    :goto_3
    goto :goto_19

    :goto_4
    goto/32 :goto_18

    nop

    :goto_5
    invoke-interface {p1}, Landroid/view/MenuItem;->getGroupId()I

    move-result v0

    goto/32 :goto_f

    nop

    :goto_6
    if-eqz v3, :cond_0

    goto/32 :goto_16

    :cond_0
    goto/32 :goto_15

    nop

    :goto_7
    invoke-virtual {v2}, Lmiuix/appcompat/internal/view/menu/e;->k()Z

    move-result v3

    goto/32 :goto_17

    nop

    :goto_8
    check-cast v2, Lmiuix/appcompat/internal/view/menu/e;

    goto/32 :goto_1b

    nop

    :goto_9
    if-nez v2, :cond_1

    goto/32 :goto_c

    :cond_1
    goto/32 :goto_12

    nop

    :goto_a
    invoke-virtual {v2, v3}, Lmiuix/appcompat/internal/view/menu/e;->o(Z)V

    goto/32 :goto_b

    nop

    :goto_b
    goto :goto_14

    :goto_c
    goto/32 :goto_e

    nop

    :goto_d
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    goto/32 :goto_9

    nop

    :goto_e
    return-void

    :goto_f
    iget-object v1, p0, Lmiuix/appcompat/internal/view/menu/c;->f:Ljava/util/ArrayList;

    goto/32 :goto_13

    nop

    :goto_10
    const/4 v3, 0x1

    goto/32 :goto_3

    nop

    :goto_11
    if-eq v3, v0, :cond_2

    goto/32 :goto_14

    :cond_2
    goto/32 :goto_7

    nop

    :goto_12
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_8

    nop

    :goto_13
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_14
    goto/32 :goto_d

    nop

    :goto_15
    goto :goto_14

    :goto_16
    goto/32 :goto_1a

    nop

    :goto_17
    if-eqz v3, :cond_3

    goto/32 :goto_1

    :cond_3
    goto/32 :goto_0

    nop

    :goto_18
    const/4 v3, 0x0

    :goto_19
    goto/32 :goto_a

    nop

    :goto_1a
    if-eq v2, p1, :cond_4

    goto/32 :goto_4

    :cond_4
    goto/32 :goto_10

    nop

    :goto_1b
    invoke-virtual {v2}, Lmiuix/appcompat/internal/view/menu/e;->getGroupId()I

    move-result v3

    goto/32 :goto_11

    nop
.end method

.method protected N(I)Lmiuix/appcompat/internal/view/menu/c;
    .locals 6

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move v3, p1

    invoke-direct/range {v0 .. v5}, Lmiuix/appcompat/internal/view/menu/c;->P(ILjava/lang/CharSequence;ILandroid/graphics/drawable/Drawable;Landroid/view/View;)V

    return-object p0
.end method

.method protected O(Landroid/graphics/drawable/Drawable;)Lmiuix/appcompat/internal/view/menu/c;
    .locals 6

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lmiuix/appcompat/internal/view/menu/c;->P(ILjava/lang/CharSequence;ILandroid/graphics/drawable/Drawable;Landroid/view/View;)V

    return-object p0
.end method

.method protected Q(I)Lmiuix/appcompat/internal/view/menu/c;
    .locals 6

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move v1, p1

    invoke-direct/range {v0 .. v5}, Lmiuix/appcompat/internal/view/menu/c;->P(ILjava/lang/CharSequence;ILandroid/graphics/drawable/Drawable;Landroid/view/View;)V

    return-object p0
.end method

.method protected R(Ljava/lang/CharSequence;)Lmiuix/appcompat/internal/view/menu/c;
    .locals 6

    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lmiuix/appcompat/internal/view/menu/c;->P(ILjava/lang/CharSequence;ILandroid/graphics/drawable/Drawable;Landroid/view/View;)V

    return-object p0
.end method

.method protected S(Landroid/view/View;)Lmiuix/appcompat/internal/view/menu/c;
    .locals 6

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object v0, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lmiuix/appcompat/internal/view/menu/c;->P(ILjava/lang/CharSequence;ILandroid/graphics/drawable/Drawable;Landroid/view/View;)V

    return-object p0
.end method

.method public U()V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiuix/appcompat/internal/view/menu/c;->q:Z

    iget-boolean v1, p0, Lmiuix/appcompat/internal/view/menu/c;->r:Z

    if-eqz v1, :cond_0

    iput-boolean v0, p0, Lmiuix/appcompat/internal/view/menu/c;->r:Z

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lmiuix/appcompat/internal/view/menu/c;->G(Z)V

    :cond_0
    return-void
.end method

.method public V()V
    .locals 1

    iget-boolean v0, p0, Lmiuix/appcompat/internal/view/menu/c;->q:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiuix/appcompat/internal/view/menu/c;->q:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiuix/appcompat/internal/view/menu/c;->r:Z

    :cond_0
    return-void
.end method

.method public add(I)Landroid/view/MenuItem;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/c;->b:Landroid/content/res/Resources;

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x0

    invoke-direct {p0, v0, v0, v0, p1}, Lmiuix/appcompat/internal/view/menu/c;->a(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object p1

    return-object p1
.end method

.method public add(IIII)Landroid/view/MenuItem;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/c;->b:Landroid/content/res/Resources;

    invoke-virtual {v0, p4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p4

    invoke-direct {p0, p1, p2, p3, p4}, Lmiuix/appcompat/internal/view/menu/c;->a(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object p1

    return-object p1
.end method

.method public add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lmiuix/appcompat/internal/view/menu/c;->a(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object p1

    return-object p1
.end method

.method public add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0, v0, v0, p1}, Lmiuix/appcompat/internal/view/menu/c;->a(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object p1

    return-object p1
.end method

.method public addIntentOptions(IIILandroid/content/ComponentName;[Landroid/content/Intent;Landroid/content/Intent;I[Landroid/view/MenuItem;)I
    .locals 7

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/c;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p4, p5, p6, v1}, Landroid/content/pm/PackageManager;->queryIntentActivityOptions(Landroid/content/ComponentName;[Landroid/content/Intent;Landroid/content/Intent;I)Ljava/util/List;

    move-result-object p4

    if-eqz p4, :cond_0

    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v2

    goto :goto_0

    :cond_0
    move v2, v1

    :goto_0
    and-int/lit8 p7, p7, 0x1

    if-nez p7, :cond_1

    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/view/menu/c;->removeGroup(I)V

    :cond_1
    :goto_1
    if-ge v1, v2, :cond_4

    invoke-interface {p4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p7

    check-cast p7, Landroid/content/pm/ResolveInfo;

    new-instance v3, Landroid/content/Intent;

    iget v4, p7, Landroid/content/pm/ResolveInfo;->specificIndex:I

    if-gez v4, :cond_2

    move-object v4, p6

    goto :goto_2

    :cond_2
    aget-object v4, p5, v4

    :goto_2
    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    new-instance v4, Landroid/content/ComponentName;

    iget-object v5, p7, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v6, v5, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v6, v6, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v4, v6, v5}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    invoke-virtual {p7, v0}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {p0, p1, p2, p3, v4}, Lmiuix/appcompat/internal/view/menu/c;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v4

    invoke-virtual {p7, v0}, Landroid/content/pm/ResolveInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    move-result-object v4

    invoke-interface {v4, v3}, Landroid/view/MenuItem;->setIntent(Landroid/content/Intent;)Landroid/view/MenuItem;

    move-result-object v3

    if-eqz p8, :cond_3

    iget p7, p7, Landroid/content/pm/ResolveInfo;->specificIndex:I

    if-ltz p7, :cond_3

    aput-object v3, p8, p7

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    return v2
.end method

.method public addSubMenu(I)Landroid/view/SubMenu;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/c;->b:Landroid/content/res/Resources;

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v0, v0, p1}, Lmiuix/appcompat/internal/view/menu/c;->addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;

    move-result-object p1

    return-object p1
.end method

.method public addSubMenu(IIII)Landroid/view/SubMenu;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/c;->b:Landroid/content/res/Resources;

    invoke-virtual {v0, p4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p4

    invoke-virtual {p0, p1, p2, p3, p4}, Lmiuix/appcompat/internal/view/menu/c;->addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;

    move-result-object p1

    return-object p1
.end method

.method public addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lmiuix/appcompat/internal/view/menu/c;->a(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object p1

    check-cast p1, Lmiuix/appcompat/internal/view/menu/e;

    new-instance p2, Lmiuix/appcompat/internal/view/menu/i;

    iget-object p3, p0, Lmiuix/appcompat/internal/view/menu/c;->a:Landroid/content/Context;

    invoke-direct {p2, p3, p0, p1}, Lmiuix/appcompat/internal/view/menu/i;-><init>(Landroid/content/Context;Lmiuix/appcompat/internal/view/menu/c;Lmiuix/appcompat/internal/view/menu/e;)V

    invoke-virtual {p1, p2}, Lmiuix/appcompat/internal/view/menu/e;->s(Lmiuix/appcompat/internal/view/menu/i;)V

    return-object p2
.end method

.method public addSubMenu(Ljava/lang/CharSequence;)Landroid/view/SubMenu;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v0, v0, p1}, Lmiuix/appcompat/internal/view/menu/c;->addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;

    move-result-object p1

    return-object p1
.end method

.method public b(Lmiuix/appcompat/internal/view/menu/g;)V
    .locals 2

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/c;->v:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/c;->a:Landroid/content/Context;

    invoke-interface {p1, v0, p0}, Lmiuix/appcompat/internal/view/menu/g;->h(Landroid/content/Context;Lmiuix/appcompat/internal/view/menu/c;)V

    const/4 p1, 0x1

    iput-boolean p1, p0, Lmiuix/appcompat/internal/view/menu/c;->k:Z

    return-void
.end method

.method public c()V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/c;->e:Lmiuix/appcompat/internal/view/menu/c$a;

    if-eqz v0, :cond_0

    invoke-interface {v0, p0}, Lmiuix/appcompat/internal/view/menu/c$a;->c(Lmiuix/appcompat/internal/view/menu/c;)V

    :cond_0
    return-void
.end method

.method public clear()V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/c;->w:Lmiuix/appcompat/internal/view/menu/e;

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lmiuix/appcompat/internal/view/menu/c;->f(Lmiuix/appcompat/internal/view/menu/e;)Z

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/c;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lmiuix/appcompat/internal/view/menu/c;->G(Z)V

    return-void
.end method

.method public clearHeader()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lmiuix/appcompat/internal/view/menu/c;->o:Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Lmiuix/appcompat/internal/view/menu/c;->n:Ljava/lang/CharSequence;

    iput-object v0, p0, Lmiuix/appcompat/internal/view/menu/c;->p:Landroid/view/View;

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lmiuix/appcompat/internal/view/menu/c;->G(Z)V

    return-void
.end method

.method public close()V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lmiuix/appcompat/internal/view/menu/c;->e(Z)V

    return-void
.end method

.method public d()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiuix/appcompat/internal/view/menu/c;->q:Z

    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/c;->clear()V

    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/c;->clearHeader()V

    const/4 v1, 0x0

    iput-boolean v1, p0, Lmiuix/appcompat/internal/view/menu/c;->q:Z

    iput-boolean v1, p0, Lmiuix/appcompat/internal/view/menu/c;->r:Z

    invoke-virtual {p0, v0}, Lmiuix/appcompat/internal/view/menu/c;->G(Z)V

    return-void
.end method

.method final e(Z)V
    .locals 3

    goto/32 :goto_12

    nop

    :goto_0
    return-void

    :goto_1
    goto/32 :goto_13

    nop

    :goto_2
    const/4 p1, 0x0

    goto/32 :goto_a

    nop

    :goto_3
    check-cast v2, Lmiuix/appcompat/internal/view/menu/g;

    goto/32 :goto_19

    nop

    :goto_4
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_3

    nop

    :goto_5
    return-void

    :goto_6
    if-nez v1, :cond_0

    goto/32 :goto_c

    :cond_0
    goto/32 :goto_d

    nop

    :goto_7
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_6

    nop

    :goto_8
    goto :goto_11

    :goto_9
    goto/32 :goto_e

    nop

    :goto_a
    iput-boolean p1, p0, Lmiuix/appcompat/internal/view/menu/c;->t:Z

    goto/32 :goto_5

    nop

    :goto_b
    goto :goto_11

    :goto_c
    goto/32 :goto_2

    nop

    :goto_d
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_18

    nop

    :goto_e
    invoke-interface {v2, p0, p1}, Lmiuix/appcompat/internal/view/menu/g;->b(Lmiuix/appcompat/internal/view/menu/c;Z)V

    goto/32 :goto_b

    nop

    :goto_f
    iput-boolean v0, p0, Lmiuix/appcompat/internal/view/menu/c;->t:Z

    goto/32 :goto_16

    nop

    :goto_10
    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_11
    goto/32 :goto_7

    nop

    :goto_12
    iget-boolean v0, p0, Lmiuix/appcompat/internal/view/menu/c;->t:Z

    goto/32 :goto_14

    nop

    :goto_13
    const/4 v0, 0x1

    goto/32 :goto_f

    nop

    :goto_14
    if-nez v0, :cond_1

    goto/32 :goto_1

    :cond_1
    goto/32 :goto_0

    nop

    :goto_15
    invoke-virtual {v2, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto/32 :goto_8

    nop

    :goto_16
    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/c;->v:Ljava/util/concurrent/CopyOnWriteArrayList;

    goto/32 :goto_10

    nop

    :goto_17
    iget-object v2, p0, Lmiuix/appcompat/internal/view/menu/c;->v:Ljava/util/concurrent/CopyOnWriteArrayList;

    goto/32 :goto_15

    nop

    :goto_18
    check-cast v1, Ljava/lang/ref/WeakReference;

    goto/32 :goto_4

    nop

    :goto_19
    if-eqz v2, :cond_2

    goto/32 :goto_9

    :cond_2
    goto/32 :goto_17

    nop
.end method

.method public f(Lmiuix/appcompat/internal/view/menu/e;)Z
    .locals 4

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/c;->v:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_4

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/c;->w:Lmiuix/appcompat/internal/view/menu/e;

    if-eq v0, p1, :cond_0

    goto :goto_1

    :cond_0
    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/c;->V()V

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/c;->v:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lmiuix/appcompat/internal/view/menu/g;

    if-nez v3, :cond_2

    iget-object v3, p0, Lmiuix/appcompat/internal/view/menu/c;->v:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v3, v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    invoke-interface {v3, p0, p1}, Lmiuix/appcompat/internal/view/menu/g;->i(Lmiuix/appcompat/internal/view/menu/c;Lmiuix/appcompat/internal/view/menu/e;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_3
    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/c;->U()V

    if-eqz v1, :cond_4

    const/4 p1, 0x0

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/c;->w:Lmiuix/appcompat/internal/view/menu/e;

    :cond_4
    :goto_1
    return v1
.end method

.method public findItem(I)Landroid/view/MenuItem;
    .locals 4

    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/c;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_2

    iget-object v2, p0, Lmiuix/appcompat/internal/view/menu/c;->f:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiuix/appcompat/internal/view/menu/e;

    invoke-virtual {v2}, Lmiuix/appcompat/internal/view/menu/e;->getItemId()I

    move-result v3

    if-ne v3, p1, :cond_0

    return-object v2

    :cond_0
    invoke-virtual {v2}, Lmiuix/appcompat/internal/view/menu/e;->hasSubMenu()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2}, Lmiuix/appcompat/internal/view/menu/e;->getSubMenu()Landroid/view/SubMenu;

    move-result-object v2

    invoke-interface {v2, p1}, Landroid/view/SubMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    if-eqz v2, :cond_1

    return-object v2

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    const/4 p1, 0x0

    return-object p1
.end method

.method g(Lmiuix/appcompat/internal/view/menu/c;Landroid/view/MenuItem;)Z
    .locals 1

    goto/32 :goto_6

    nop

    :goto_0
    const/4 p1, 0x0

    :goto_1
    goto/32 :goto_8

    nop

    :goto_2
    if-nez v0, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_7

    nop

    :goto_3
    const/4 p1, 0x1

    goto/32 :goto_4

    nop

    :goto_4
    goto :goto_1

    :goto_5
    goto/32 :goto_0

    nop

    :goto_6
    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/c;->e:Lmiuix/appcompat/internal/view/menu/c$a;

    goto/32 :goto_2

    nop

    :goto_7
    invoke-interface {v0, p1, p2}, Lmiuix/appcompat/internal/view/menu/c$a;->d(Lmiuix/appcompat/internal/view/menu/c;Landroid/view/MenuItem;)Z

    move-result p1

    goto/32 :goto_9

    nop

    :goto_8
    return p1

    :goto_9
    if-nez p1, :cond_1

    goto/32 :goto_5

    :cond_1
    goto/32 :goto_3

    nop
.end method

.method public getItem(I)Landroid/view/MenuItem;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/c;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/MenuItem;

    return-object p1
.end method

.method public hasVisibleItems()Z
    .locals 4

    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/c;->size()I

    move-result v0

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v0, :cond_1

    iget-object v3, p0, Lmiuix/appcompat/internal/view/menu/c;->f:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lmiuix/appcompat/internal/view/menu/e;

    invoke-virtual {v3}, Lmiuix/appcompat/internal/view/menu/e;->isVisible()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return v1
.end method

.method public isShortcutKey(ILandroid/view/KeyEvent;)Z
    .locals 0

    invoke-virtual {p0, p1, p2}, Lmiuix/appcompat/internal/view/menu/c;->o(ILandroid/view/KeyEvent;)Lmiuix/appcompat/internal/view/menu/e;

    move-result-object p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public j(Lmiuix/appcompat/internal/view/menu/e;)Z
    .locals 4

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/c;->v:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    :cond_0
    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/c;->V()V

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/c;->v:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lmiuix/appcompat/internal/view/menu/g;

    if-nez v3, :cond_2

    iget-object v3, p0, Lmiuix/appcompat/internal/view/menu/c;->v:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v3, v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    invoke-interface {v3, p0, p1}, Lmiuix/appcompat/internal/view/menu/g;->g(Lmiuix/appcompat/internal/view/menu/c;Lmiuix/appcompat/internal/view/menu/e;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_3
    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/c;->U()V

    if-eqz v1, :cond_4

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/c;->w:Lmiuix/appcompat/internal/view/menu/e;

    :cond_4
    return v1
.end method

.method public k(I)I
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lmiuix/appcompat/internal/view/menu/c;->l(II)I

    move-result p1

    return p1
.end method

.method public l(II)I
    .locals 2

    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/c;->size()I

    move-result v0

    if-gez p2, :cond_0

    const/4 p2, 0x0

    :cond_0
    :goto_0
    if-ge p2, v0, :cond_2

    iget-object v1, p0, Lmiuix/appcompat/internal/view/menu/c;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiuix/appcompat/internal/view/menu/e;

    invoke-virtual {v1}, Lmiuix/appcompat/internal/view/menu/e;->getGroupId()I

    move-result v1

    if-ne v1, p1, :cond_1

    return p2

    :cond_1
    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    :cond_2
    const/4 p1, -0x1

    return p1
.end method

.method public n(I)I
    .locals 3

    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/c;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    iget-object v2, p0, Lmiuix/appcompat/internal/view/menu/c;->f:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiuix/appcompat/internal/view/menu/e;

    invoke-virtual {v2}, Lmiuix/appcompat/internal/view/menu/e;->getItemId()I

    move-result v2

    if-ne v2, p1, :cond_0

    return v1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, -0x1

    return p1
.end method

.method o(ILandroid/view/KeyEvent;)Lmiuix/appcompat/internal/view/menu/e;
    .locals 9

    goto/32 :goto_1

    nop

    :goto_0
    if-nez p2, :cond_0

    goto/32 :goto_26

    :cond_0
    goto/32 :goto_1c

    nop

    :goto_1
    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/c;->u:Ljava/util/ArrayList;

    goto/32 :goto_28

    nop

    :goto_2
    return-object v4

    :goto_3
    goto/32 :goto_10

    nop

    :goto_4
    check-cast v4, Lmiuix/appcompat/internal/view/menu/e;

    goto/32 :goto_0

    nop

    :goto_5
    if-eqz v7, :cond_1

    goto/32 :goto_12

    :cond_1
    :goto_6
    goto/32 :goto_2a

    nop

    :goto_7
    iget-object v7, v3, Landroid/view/KeyCharacterMap$KeyData;->meta:[C

    goto/32 :goto_2e

    nop

    :goto_8
    invoke-virtual {v4}, Lmiuix/appcompat/internal/view/menu/e;->getNumericShortcut()C

    move-result v6

    :goto_9
    goto/32 :goto_7

    nop

    :goto_a
    if-nez v8, :cond_2

    goto/32 :goto_12

    :cond_2
    :goto_b
    goto/32 :goto_1a

    nop

    :goto_c
    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    goto/32 :goto_15

    nop

    :goto_d
    if-eq v6, v7, :cond_3

    goto/32 :goto_33

    :cond_3
    goto/32 :goto_34

    nop

    :goto_e
    invoke-direct {v3}, Landroid/view/KeyCharacterMap$KeyData;-><init>()V

    goto/32 :goto_f

    nop

    :goto_f
    invoke-virtual {p2, v3}, Landroid/view/KeyEvent;->getKeyData(Landroid/view/KeyCharacterMap$KeyData;)Z

    goto/32 :goto_20

    nop

    :goto_10
    return-object v2

    :goto_11
    if-eq p1, v6, :cond_4

    goto/32 :goto_33

    :cond_4
    :goto_12
    goto/32 :goto_2

    nop

    :goto_13
    const/4 v5, 0x0

    goto/32 :goto_1e

    nop

    :goto_14
    const/16 v7, 0x8

    goto/32 :goto_d

    nop

    :goto_15
    check-cast p1, Lmiuix/appcompat/internal/view/menu/e;

    goto/32 :goto_18

    nop

    :goto_16
    and-int/lit8 v8, v1, 0x2

    goto/32 :goto_a

    nop

    :goto_17
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getMetaState()I

    move-result v1

    goto/32 :goto_1d

    nop

    :goto_18
    return-object p1

    :goto_19
    goto/32 :goto_23

    nop

    :goto_1a
    const/4 v8, 0x2

    goto/32 :goto_2b

    nop

    :goto_1b
    if-nez v1, :cond_5

    goto/32 :goto_22

    :cond_5
    goto/32 :goto_21

    nop

    :goto_1c
    invoke-virtual {v4}, Lmiuix/appcompat/internal/view/menu/e;->getAlphabeticShortcut()C

    move-result v6

    goto/32 :goto_25

    nop

    :goto_1d
    new-instance v3, Landroid/view/KeyCharacterMap$KeyData;

    goto/32 :goto_e

    nop

    :goto_1e
    if-eq p2, v4, :cond_6

    goto/32 :goto_19

    :cond_6
    goto/32 :goto_c

    nop

    :goto_1f
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    goto/32 :goto_2d

    nop

    :goto_20
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result p2

    goto/32 :goto_24

    nop

    :goto_21
    return-object v2

    :goto_22
    goto/32 :goto_17

    nop

    :goto_23
    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/c;->C()Z

    move-result p2

    goto/32 :goto_32

    nop

    :goto_24
    const/4 v4, 0x1

    goto/32 :goto_13

    nop

    :goto_25
    goto/16 :goto_9

    :goto_26
    goto/32 :goto_8

    nop

    :goto_27
    invoke-virtual {p0, v0, p1, p2}, Lmiuix/appcompat/internal/view/menu/c;->p(Ljava/util/List;ILandroid/view/KeyEvent;)V

    goto/32 :goto_1f

    nop

    :goto_28
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto/32 :goto_27

    nop

    :goto_29
    if-eq v6, v7, :cond_7

    goto/32 :goto_6

    :cond_7
    goto/32 :goto_35

    nop

    :goto_2a
    if-nez p2, :cond_8

    goto/32 :goto_33

    :cond_8
    goto/32 :goto_14

    nop

    :goto_2b
    aget-char v7, v7, v8

    goto/32 :goto_29

    nop

    :goto_2c
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    goto/32 :goto_4

    nop

    :goto_2d
    const/4 v2, 0x0

    goto/32 :goto_1b

    nop

    :goto_2e
    aget-char v8, v7, v5

    goto/32 :goto_30

    nop

    :goto_2f
    if-nez v4, :cond_9

    goto/32 :goto_3

    :cond_9
    goto/32 :goto_2c

    nop

    :goto_30
    if-eq v6, v8, :cond_a

    goto/32 :goto_b

    :cond_a
    goto/32 :goto_16

    nop

    :goto_31
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    goto/32 :goto_2f

    nop

    :goto_32
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_33
    goto/32 :goto_31

    nop

    :goto_34
    const/16 v6, 0x43

    goto/32 :goto_11

    nop

    :goto_35
    and-int/lit8 v7, v1, 0x2

    goto/32 :goto_5

    nop
.end method

.method p(Ljava/util/List;ILandroid/view/KeyEvent;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lmiuix/appcompat/internal/view/menu/e;",
            ">;I",
            "Landroid/view/KeyEvent;",
            ")V"
        }
    .end annotation

    goto/32 :goto_9

    nop

    :goto_0
    invoke-direct {v2}, Landroid/view/KeyCharacterMap$KeyData;-><init>()V

    goto/32 :goto_29

    nop

    :goto_1
    invoke-virtual {v6, p1, p2, p3}, Lmiuix/appcompat/internal/view/menu/c;->p(Ljava/util/List;ILandroid/view/KeyEvent;)V

    :goto_2
    goto/32 :goto_2f

    nop

    :goto_3
    iget-object v7, v2, Landroid/view/KeyCharacterMap$KeyData;->meta:[C

    goto/32 :goto_2c

    nop

    :goto_4
    aget-char v7, v7, v8

    goto/32 :goto_e

    nop

    :goto_5
    goto/16 :goto_2b

    :goto_6
    goto/32 :goto_2a

    nop

    :goto_7
    iget-object v3, p0, Lmiuix/appcompat/internal/view/menu/c;->f:Ljava/util/ArrayList;

    goto/32 :goto_c

    nop

    :goto_8
    new-instance v2, Landroid/view/KeyCharacterMap$KeyData;

    goto/32 :goto_0

    nop

    :goto_9
    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/c;->C()Z

    move-result v0

    goto/32 :goto_15

    nop

    :goto_a
    return-void

    :goto_b
    if-eq v6, v7, :cond_0

    goto/32 :goto_d

    :cond_0
    goto/32 :goto_25

    nop

    :goto_c
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_d
    goto/32 :goto_17

    nop

    :goto_e
    if-ne v6, v7, :cond_1

    goto/32 :goto_26

    :cond_1
    goto/32 :goto_20

    nop

    :goto_f
    invoke-virtual {v5}, Lmiuix/appcompat/internal/view/menu/e;->isEnabled()Z

    move-result v6

    goto/32 :goto_28

    nop

    :goto_10
    check-cast v5, Lmiuix/appcompat/internal/view/menu/e;

    goto/32 :goto_14

    nop

    :goto_11
    if-nez v5, :cond_2

    goto/32 :goto_1f

    :cond_2
    goto/32 :goto_24

    nop

    :goto_12
    if-ne p2, v4, :cond_3

    goto/32 :goto_1d

    :cond_3
    goto/32 :goto_1c

    nop

    :goto_13
    if-nez v6, :cond_4

    goto/32 :goto_d

    :cond_4
    goto/32 :goto_3

    nop

    :goto_14
    invoke-virtual {v5}, Lmiuix/appcompat/internal/view/menu/e;->hasSubMenu()Z

    move-result v6

    goto/32 :goto_23

    nop

    :goto_15
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getMetaState()I

    move-result v1

    goto/32 :goto_8

    nop

    :goto_16
    if-ne v6, v8, :cond_5

    goto/32 :goto_26

    :cond_5
    goto/32 :goto_2d

    nop

    :goto_17
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    goto/32 :goto_11

    nop

    :goto_18
    if-eqz v3, :cond_6

    goto/32 :goto_1d

    :cond_6
    goto/32 :goto_12

    nop

    :goto_19
    aget-char v8, v7, v8

    goto/32 :goto_16

    nop

    :goto_1a
    invoke-virtual {v5}, Lmiuix/appcompat/internal/view/menu/e;->getSubMenu()Landroid/view/SubMenu;

    move-result-object v6

    goto/32 :goto_27

    nop

    :goto_1b
    and-int/lit8 v7, v1, 0x5

    goto/32 :goto_22

    nop

    :goto_1c
    return-void

    :goto_1d
    goto/32 :goto_7

    nop

    :goto_1e
    goto :goto_d

    :goto_1f
    goto/32 :goto_a

    nop

    :goto_20
    if-nez v0, :cond_7

    goto/32 :goto_d

    :cond_7
    goto/32 :goto_30

    nop

    :goto_21
    invoke-interface {p1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/32 :goto_1e

    nop

    :goto_22
    if-eqz v7, :cond_8

    goto/32 :goto_d

    :cond_8
    goto/32 :goto_13

    nop

    :goto_23
    if-nez v6, :cond_9

    goto/32 :goto_2

    :cond_9
    goto/32 :goto_1a

    nop

    :goto_24
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    goto/32 :goto_10

    nop

    :goto_25
    if-eq p2, v4, :cond_a

    goto/32 :goto_d

    :cond_a
    :goto_26
    goto/32 :goto_f

    nop

    :goto_27
    check-cast v6, Lmiuix/appcompat/internal/view/menu/c;

    goto/32 :goto_1

    nop

    :goto_28
    if-nez v6, :cond_b

    goto/32 :goto_d

    :cond_b
    goto/32 :goto_21

    nop

    :goto_29
    invoke-virtual {p3, v2}, Landroid/view/KeyEvent;->getKeyData(Landroid/view/KeyCharacterMap$KeyData;)Z

    move-result v3

    goto/32 :goto_31

    nop

    :goto_2a
    invoke-virtual {v5}, Lmiuix/appcompat/internal/view/menu/e;->getNumericShortcut()C

    move-result v6

    :goto_2b
    goto/32 :goto_1b

    nop

    :goto_2c
    const/4 v8, 0x0

    goto/32 :goto_19

    nop

    :goto_2d
    const/4 v8, 0x2

    goto/32 :goto_4

    nop

    :goto_2e
    invoke-virtual {v5}, Lmiuix/appcompat/internal/view/menu/e;->getAlphabeticShortcut()C

    move-result v6

    goto/32 :goto_5

    nop

    :goto_2f
    if-nez v0, :cond_c

    goto/32 :goto_6

    :cond_c
    goto/32 :goto_2e

    nop

    :goto_30
    const/16 v7, 0x8

    goto/32 :goto_b

    nop

    :goto_31
    const/16 v4, 0x43

    goto/32 :goto_18

    nop
.end method

.method public performIdentifierAction(II)Z
    .locals 0

    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/view/menu/c;->findItem(I)Landroid/view/MenuItem;

    move-result-object p1

    invoke-virtual {p0, p1, p2}, Lmiuix/appcompat/internal/view/menu/c;->H(Landroid/view/MenuItem;I)Z

    move-result p1

    return p1
.end method

.method public performShortcut(ILandroid/view/KeyEvent;I)Z
    .locals 0

    invoke-virtual {p0, p1, p2}, Lmiuix/appcompat/internal/view/menu/c;->o(ILandroid/view/KeyEvent;)Lmiuix/appcompat/internal/view/menu/e;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p0, p1, p3}, Lmiuix/appcompat/internal/view/menu/c;->H(Landroid/view/MenuItem;I)Z

    move-result p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    and-int/lit8 p2, p3, 0x2

    if-eqz p2, :cond_1

    const/4 p2, 0x1

    invoke-virtual {p0, p2}, Lmiuix/appcompat/internal/view/menu/c;->e(Z)V

    :cond_1
    return p1
.end method

.method public q()V
    .locals 5

    iget-boolean v0, p0, Lmiuix/appcompat/internal/view/menu/c;->k:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/c;->v:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/ref/WeakReference;

    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lmiuix/appcompat/internal/view/menu/g;

    if-nez v4, :cond_1

    iget-object v4, p0, Lmiuix/appcompat/internal/view/menu/c;->v:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v4, v3}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-interface {v4}, Lmiuix/appcompat/internal/view/menu/g;->d()Z

    move-result v3

    or-int/2addr v2, v3

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/c;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/c;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    if-eqz v2, :cond_4

    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/c;->B()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiuix/appcompat/internal/view/menu/e;

    invoke-virtual {v2}, Lmiuix/appcompat/internal/view/menu/e;->j()Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lmiuix/appcompat/internal/view/menu/c;->i:Ljava/util/ArrayList;

    goto :goto_2

    :cond_3
    iget-object v3, p0, Lmiuix/appcompat/internal/view/menu/c;->j:Ljava/util/ArrayList;

    :goto_2
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/c;->j:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/c;->B()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_5
    iput-boolean v1, p0, Lmiuix/appcompat/internal/view/menu/c;->k:Z

    return-void
.end method

.method public r()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/c;->a:Landroid/content/Context;

    return-object v0
.end method

.method public removeGroup(I)V
    .locals 5

    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/view/menu/c;->k(I)I

    move-result v0

    if-ltz v0, :cond_1

    iget-object v1, p0, Lmiuix/appcompat/internal/view/menu/c;->f:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    sub-int/2addr v1, v0

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    add-int/lit8 v4, v3, 0x1

    if-ge v3, v1, :cond_0

    iget-object v3, p0, Lmiuix/appcompat/internal/view/menu/c;->f:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lmiuix/appcompat/internal/view/menu/e;

    invoke-virtual {v3}, Lmiuix/appcompat/internal/view/menu/e;->getGroupId()I

    move-result v3

    if-ne v3, p1, :cond_0

    invoke-direct {p0, v0, v2}, Lmiuix/appcompat/internal/view/menu/c;->I(IZ)V

    move v3, v4

    goto :goto_0

    :cond_0
    const/4 p1, 0x1

    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/view/menu/c;->G(Z)V

    :cond_1
    return-void
.end method

.method public removeItem(I)V
    .locals 1

    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/view/menu/c;->n(I)I

    move-result p1

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lmiuix/appcompat/internal/view/menu/c;->I(IZ)V

    return-void
.end method

.method public s()Lmiuix/appcompat/internal/view/menu/e;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/c;->w:Lmiuix/appcompat/internal/view/menu/e;

    return-object v0
.end method

.method public setGroupCheckable(IZZ)V
    .locals 3

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/c;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiuix/appcompat/internal/view/menu/e;

    invoke-virtual {v1}, Lmiuix/appcompat/internal/view/menu/e;->getGroupId()I

    move-result v2

    if-ne v2, p1, :cond_0

    invoke-virtual {v1, p3}, Lmiuix/appcompat/internal/view/menu/e;->p(Z)V

    invoke-virtual {v1, p2}, Lmiuix/appcompat/internal/view/menu/e;->setCheckable(Z)Landroid/view/MenuItem;

    goto :goto_0

    :cond_1
    return-void
.end method

.method public setGroupEnabled(IZ)V
    .locals 3

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/c;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmiuix/appcompat/internal/view/menu/e;

    invoke-virtual {v1}, Lmiuix/appcompat/internal/view/menu/e;->getGroupId()I

    move-result v2

    if-ne v2, p1, :cond_0

    invoke-virtual {v1, p2}, Lmiuix/appcompat/internal/view/menu/e;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0

    :cond_1
    return-void
.end method

.method public setGroupVisible(IZ)V
    .locals 5

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/c;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const/4 v3, 0x1

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiuix/appcompat/internal/view/menu/e;

    invoke-virtual {v2}, Lmiuix/appcompat/internal/view/menu/e;->getGroupId()I

    move-result v4

    if-ne v4, p1, :cond_0

    invoke-virtual {v2, p2}, Lmiuix/appcompat/internal/view/menu/e;->t(Z)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v3

    goto :goto_0

    :cond_1
    if-eqz v1, :cond_2

    invoke-virtual {p0, v3}, Lmiuix/appcompat/internal/view/menu/c;->G(Z)V

    :cond_2
    return-void
.end method

.method public setQwertyMode(Z)V
    .locals 0

    iput-boolean p1, p0, Lmiuix/appcompat/internal/view/menu/c;->c:Z

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/view/menu/c;->G(Z)V

    return-void
.end method

.method public size()I
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/c;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public t()Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/c;->o:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public u()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/c;->n:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public v()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/c;->p:Landroid/view/View;

    return-object v0
.end method

.method public w()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lmiuix/appcompat/internal/view/menu/e;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/c;->q()V

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/c;->j:Ljava/util/ArrayList;

    return-object v0
.end method

.method x()Z
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-boolean v0, p0, Lmiuix/appcompat/internal/view/menu/c;->s:Z

    goto/32 :goto_1

    nop

    :goto_1
    return v0
.end method

.method z()Landroid/content/res/Resources;
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/c;->b:Landroid/content/res/Resources;

    goto/32 :goto_1

    nop

    :goto_1
    return-object v0
.end method
