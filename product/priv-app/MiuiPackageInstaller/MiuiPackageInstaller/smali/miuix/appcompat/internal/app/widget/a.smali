.class abstract Lmiuix/appcompat/internal/app/widget/a;
.super Landroid/view/ViewGroup;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiuix/appcompat/internal/app/widget/a$c;
    }
.end annotation


# instance fields
.field protected a:Lc9/a;

.field protected b:Lc9/a;

.field protected c:Lc9/a;

.field protected d:Lc9/a;

.field protected e:Lc9/a;

.field protected f:Lc9/a;

.field protected g:Lmiuix/appcompat/internal/view/menu/action/c;

.field protected h:Lmiuix/appcompat/internal/view/menu/action/b;

.field protected i:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

.field protected j:Z

.field protected k:Z

.field protected l:I

.field protected m:I

.field protected n:I

.field protected o:I

.field p:Lmiuix/appcompat/app/d;

.field q:I

.field r:I

.field private s:Z

.field protected t:Z

.field protected u:Z

.field protected v:I

.field w:F

.field protected x:Lf9/b;

.field protected y:Landroid/view/View$OnClickListener;


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lmiuix/appcompat/internal/app/widget/a;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6

    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 p3, 0x1

    iput p3, p0, Lmiuix/appcompat/internal/app/widget/a;->q:I

    iput p3, p0, Lmiuix/appcompat/internal/app/widget/a;->r:I

    iput-boolean p3, p0, Lmiuix/appcompat/internal/app/widget/a;->s:Z

    iput-boolean p3, p0, Lmiuix/appcompat/internal/app/widget/a;->t:Z

    const/4 v0, 0x0

    iput v0, p0, Lmiuix/appcompat/internal/app/widget/a;->w:F

    new-instance v0, Lmiuix/appcompat/internal/app/widget/a$a;

    invoke-direct {v0, p0}, Lmiuix/appcompat/internal/app/widget/a$a;-><init>(Lmiuix/appcompat/internal/app/widget/a;)V

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/a;->x:Lf9/b;

    const/4 v0, 0x0

    iput-object v0, p0, Lmiuix/appcompat/internal/app/widget/a;->y:Landroid/view/View$OnClickListener;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/a;->u:Z

    const/4 v1, -0x1

    iput v1, p0, Lmiuix/appcompat/internal/app/widget/a;->v:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lk9/e;->k:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lmiuix/appcompat/internal/app/widget/a;->n:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lk9/e;->f:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lmiuix/appcompat/internal/app/widget/a;->o:I

    new-instance v1, Lc9/a;

    invoke-direct {v1}, Lc9/a;-><init>()V

    const/4 v2, 0x2

    new-array v3, v2, [F

    fill-array-data v3, :array_0

    const/4 v4, -0x2

    invoke-virtual {v1, v4, v3}, Lc9/a;->k(I[F)Lc9/a;

    move-result-object v1

    iput-object v1, p0, Lmiuix/appcompat/internal/app/widget/a;->a:Lc9/a;

    new-instance v1, Lc9/a;

    invoke-direct {v1}, Lc9/a;-><init>()V

    new-array v3, v2, [F

    fill-array-data v3, :array_1

    invoke-virtual {v1, v4, v3}, Lc9/a;->k(I[F)Lc9/a;

    move-result-object v1

    new-array v3, p3, [Lf9/b;

    iget-object v5, p0, Lmiuix/appcompat/internal/app/widget/a;->x:Lf9/b;

    aput-object v5, v3, v0

    invoke-virtual {v1, v3}, Lc9/a;->a([Lf9/b;)Lc9/a;

    move-result-object v1

    iput-object v1, p0, Lmiuix/appcompat/internal/app/widget/a;->c:Lc9/a;

    new-instance v1, Lc9/a;

    invoke-direct {v1}, Lc9/a;-><init>()V

    new-array v3, v2, [F

    fill-array-data v3, :array_2

    invoke-virtual {v1, v4, v3}, Lc9/a;->k(I[F)Lc9/a;

    move-result-object v1

    iput-object v1, p0, Lmiuix/appcompat/internal/app/widget/a;->b:Lc9/a;

    new-instance v1, Lc9/a;

    invoke-direct {v1}, Lc9/a;-><init>()V

    new-array v3, v2, [F

    fill-array-data v3, :array_3

    invoke-virtual {v1, v4, v3}, Lc9/a;->k(I[F)Lc9/a;

    move-result-object v1

    new-array v3, p3, [Lf9/b;

    iget-object v5, p0, Lmiuix/appcompat/internal/app/widget/a;->x:Lf9/b;

    aput-object v5, v3, v0

    invoke-virtual {v1, v3}, Lc9/a;->a([Lf9/b;)Lc9/a;

    move-result-object v1

    iput-object v1, p0, Lmiuix/appcompat/internal/app/widget/a;->d:Lc9/a;

    new-instance v1, Lc9/a;

    invoke-direct {v1}, Lc9/a;-><init>()V

    new-array v3, v2, [F

    fill-array-data v3, :array_4

    invoke-virtual {v1, v4, v3}, Lc9/a;->k(I[F)Lc9/a;

    move-result-object v1

    iput-object v1, p0, Lmiuix/appcompat/internal/app/widget/a;->e:Lc9/a;

    new-instance v1, Lc9/a;

    invoke-direct {v1}, Lc9/a;-><init>()V

    new-array v2, v2, [F

    fill-array-data v2, :array_5

    invoke-virtual {v1, v4, v2}, Lc9/a;->k(I[F)Lc9/a;

    move-result-object v1

    iput-object v1, p0, Lmiuix/appcompat/internal/app/widget/a;->f:Lc9/a;

    sget-object v1, Lk9/l;->a:[I

    const v2, 0x10102ce

    invoke-virtual {p1, p2, v1, v2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p1

    sget p2, Lk9/l;->v:I

    invoke-virtual {p1, p2, p3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result p2

    sget v1, Lk9/l;->w:I

    invoke-virtual {p1, v1, p3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    sget v2, Lk9/l;->y:I

    invoke-virtual {p1, v2, p3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/a;->n()Z

    move-result p1

    if-eqz p1, :cond_0

    iget p1, p0, Lmiuix/appcompat/internal/app/widget/a;->v:I

    iput p1, p0, Lmiuix/appcompat/internal/app/widget/a;->q:I

    iput p1, p0, Lmiuix/appcompat/internal/app/widget/a;->r:I

    goto :goto_1

    :cond_0
    if-eqz p2, :cond_2

    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/a;->j()Z

    move-result p1

    if-eqz p1, :cond_1

    goto :goto_0

    :cond_1
    iput p3, p0, Lmiuix/appcompat/internal/app/widget/a;->q:I

    iput p3, p0, Lmiuix/appcompat/internal/app/widget/a;->r:I

    goto :goto_1

    :cond_2
    :goto_0
    iput v0, p0, Lmiuix/appcompat/internal/app/widget/a;->q:I

    iput v0, p0, Lmiuix/appcompat/internal/app/widget/a;->r:I

    :goto_1
    iput-boolean v1, p0, Lmiuix/appcompat/internal/app/widget/a;->s:Z

    iput-boolean v2, p0, Lmiuix/appcompat/internal/app/widget/a;->t:Z

    return-void

    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x3e99999a    # 0.3f
    .end array-data

    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x3e99999a    # 0.3f
    .end array-data

    :array_2
    .array-data 4
        0x3f800000    # 1.0f
        0x3e19999a    # 0.15f
    .end array-data

    :array_3
    .array-data 4
        0x3f800000    # 1.0f
        0x3e19999a    # 0.15f
    .end array-data

    :array_4
    .array-data 4
        0x3f800000    # 1.0f
        0x3f19999a    # 0.6f
    .end array-data

    :array_5
    .array-data 4
        0x3f800000    # 1.0f
        0x3f19999a    # 0.6f
    .end array-data
.end method

.method private setTitleMaxHeight(I)V
    .locals 0

    iput p1, p0, Lmiuix/appcompat/internal/app/widget/a;->m:I

    invoke-virtual {p0}, Landroid/view/ViewGroup;->requestLayout()V

    return-void
.end method

.method private setTitleMinHeight(I)V
    .locals 0

    iput p1, p0, Lmiuix/appcompat/internal/app/widget/a;->l:I

    invoke-virtual {p0}, Landroid/view/ViewGroup;->requestLayout()V

    return-void
.end method


# virtual methods
.method getActionBarStyle()I
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    const v0, 0x10102ce

    goto/32 :goto_1

    nop

    :goto_1
    return v0
.end method

.method public getActionBarTransitionListener()Lmiuix/appcompat/app/d;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/a;->p:Lmiuix/appcompat/app/d;

    return-object v0
.end method

.method public getActionMenuView()Lmiuix/appcompat/internal/view/menu/action/c;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/a;->g:Lmiuix/appcompat/internal/view/menu/action/c;

    return-object v0
.end method

.method public getAnimatedVisibility()I
    .locals 1

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    return v0
.end method

.method public getExpandState()I
    .locals 1

    iget v0, p0, Lmiuix/appcompat/internal/app/widget/a;->r:I

    return v0
.end method

.method public getMenuView()Lmiuix/appcompat/internal/view/menu/action/c;
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/a;->g:Lmiuix/appcompat/internal/view/menu/action/c;

    return-object v0
.end method

.method public i()Z
    .locals 2

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/a;->h:Lmiuix/appcompat/internal/view/menu/action/b;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0, v1}, Lmiuix/appcompat/internal/view/menu/action/b;->Q(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method protected j()Z
    .locals 2

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lia/e;->d(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/a;->n()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public k()Z
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/a;->h:Lmiuix/appcompat/internal/view/menu/action/b;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lmiuix/appcompat/internal/view/menu/action/b;->S()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public l()Z
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/a;->h:Lmiuix/appcompat/internal/view/menu/action/b;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lmiuix/appcompat/internal/view/menu/action/b;->T()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public m()Z
    .locals 1

    iget-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/a;->s:Z

    return v0
.end method

.method protected n()Z
    .locals 1

    iget-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/a;->u:Z

    return v0
.end method

.method protected o(Landroid/view/View;III)I
    .locals 1

    const/high16 v0, -0x80000000

    invoke-static {p2, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {p1, v0, p3}, Landroid/view/View;->measure(II)V

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result p1

    sub-int/2addr p2, p1

    sub-int/2addr p2, p4

    const/4 p1, 0x0

    invoke-static {p1, p2}, Ljava/lang/Math;->max(II)I

    move-result p1

    return p1
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 5

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lk9/l;->a:[I

    invoke-virtual {p0}, Lmiuix/appcompat/internal/app/widget/a;->getActionBarStyle()I

    move-result v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v1, v2, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    sget v1, Lk9/l;->e:I

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getLayoutDimension(II)I

    move-result v1

    invoke-direct {p0, v1}, Lmiuix/appcompat/internal/app/widget/a;->setTitleMinHeight(I)V

    sget v1, Lk9/l;->d:I

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getLayoutDimension(II)I

    move-result v1

    invoke-direct {p0, v1}, Lmiuix/appcompat/internal/app/widget/a;->setTitleMaxHeight(I)V

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    iget-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/a;->k:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lk9/c;->c:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    invoke-virtual {p0, v0}, Lmiuix/appcompat/internal/app/widget/a;->setSplitActionBar(Z)V

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/a;->h:Lmiuix/appcompat/internal/view/menu/action/b;

    if-eqz v0, :cond_1

    invoke-virtual {v0, p1}, Lmiuix/appcompat/internal/view/menu/action/b;->V(Landroid/content/res/Configuration;)V

    :cond_1
    return-void
.end method

.method protected p(II)V
    .locals 0

    return-void
.end method

.method protected q(II)V
    .locals 0

    return-void
.end method

.method protected r(Landroid/view/View;III)I
    .locals 6

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Lmiuix/appcompat/internal/app/widget/a;->s(Landroid/view/View;IIIZ)I

    move-result p1

    return p1
.end method

.method protected s(Landroid/view/View;IIIZ)I
    .locals 8

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    sub-int/2addr p4, v1

    div-int/lit8 p4, p4, 0x2

    add-int/2addr p3, p4

    if-nez p5, :cond_0

    iget p3, p0, Lmiuix/appcompat/internal/app/widget/a;->l:I

    sub-int/2addr p3, v1

    div-int/lit8 p3, p3, 0x2

    :cond_0
    move v5, p3

    add-int v6, p2, v0

    add-int v7, v5, v1

    move-object v2, p0

    move-object v3, p1

    move v4, p2

    invoke-static/range {v2 .. v7}, Lia/i;->c(Landroid/view/ViewGroup;Landroid/view/View;IIII)V

    return v0
.end method

.method public setActionBarTransitionListener(Lmiuix/appcompat/app/d;)V
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/a;->p:Lmiuix/appcompat/app/d;

    return-void
.end method

.method public setExpandState(I)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v0}, Lmiuix/appcompat/internal/app/widget/a;->v(IZZ)V

    return-void
.end method

.method protected setExpandStateByUser(I)V
    .locals 1

    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/a;->u:Z

    iput p1, p0, Lmiuix/appcompat/internal/app/widget/a;->v:I

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    iput-boolean p1, p0, Lmiuix/appcompat/internal/app/widget/a;->u:Z

    iput v0, p0, Lmiuix/appcompat/internal/app/widget/a;->v:I

    :goto_0
    return-void
.end method

.method public setResizable(Z)V
    .locals 0

    iput-boolean p1, p0, Lmiuix/appcompat/internal/app/widget/a;->s:Z

    return-void
.end method

.method public setSplitActionBar(Z)V
    .locals 0

    iput-boolean p1, p0, Lmiuix/appcompat/internal/app/widget/a;->j:Z

    return-void
.end method

.method public setSplitView(Lmiuix/appcompat/internal/app/widget/ActionBarContainer;)V
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/a;->i:Lmiuix/appcompat/internal/app/widget/ActionBarContainer;

    return-void
.end method

.method public setSplitWhenNarrow(Z)V
    .locals 0

    iput-boolean p1, p0, Lmiuix/appcompat/internal/app/widget/a;->k:Z

    return-void
.end method

.method public setSubTitleClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/internal/app/widget/a;->y:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public setTitleClickable(Z)V
    .locals 0

    iput-boolean p1, p0, Lmiuix/appcompat/internal/app/widget/a;->t:Z

    return-void
.end method

.method public setVisibility(I)V
    .locals 1

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    if-eq p1, v0, :cond_0

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method protected t(Landroid/view/View;III)I
    .locals 7

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result p3

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result p4

    iget v0, p0, Lmiuix/appcompat/internal/app/widget/a;->l:I

    sub-int/2addr v0, p4

    div-int/lit8 v4, v0, 0x2

    sub-int v3, p2, p3

    add-int v6, v4, p4

    move-object v1, p0

    move-object v2, p1

    move v5, p2

    invoke-static/range {v1 .. v6}, Lia/i;->c(Landroid/view/ViewGroup;Landroid/view/View;IIII)V

    return p3
.end method

.method public u()V
    .locals 1

    new-instance v0, Lmiuix/appcompat/internal/app/widget/a$b;

    invoke-direct {v0, p0}, Lmiuix/appcompat/internal/app/widget/a$b;-><init>(Lmiuix/appcompat/internal/app/widget/a;)V

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public v(IZZ)V
    .locals 1

    iget-boolean v0, p0, Lmiuix/appcompat/internal/app/widget/a;->s:Z

    if-nez v0, :cond_0

    if-eqz p3, :cond_4

    :cond_0
    iget p3, p0, Lmiuix/appcompat/internal/app/widget/a;->q:I

    if-eq p3, p1, :cond_4

    if-eqz p2, :cond_1

    invoke-virtual {p0, p3, p1}, Lmiuix/appcompat/internal/app/widget/a;->p(II)V

    goto :goto_2

    :cond_1
    iput p1, p0, Lmiuix/appcompat/internal/app/widget/a;->q:I

    if-nez p1, :cond_2

    const/4 p2, 0x0

    :goto_0
    iput p2, p0, Lmiuix/appcompat/internal/app/widget/a;->r:I

    goto :goto_1

    :cond_2
    const/4 p2, 0x1

    if-ne p1, p2, :cond_3

    goto :goto_0

    :cond_3
    :goto_1
    invoke-virtual {p0, p3, p1}, Lmiuix/appcompat/internal/app/widget/a;->q(II)V

    invoke-virtual {p0}, Landroid/view/ViewGroup;->requestLayout()V

    :cond_4
    :goto_2
    return-void
.end method

.method public w()Z
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/app/widget/a;->h:Lmiuix/appcompat/internal/view/menu/action/b;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lmiuix/appcompat/internal/view/menu/action/b;->b0()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
