.class public Lmiuix/appcompat/internal/view/menu/action/b;
.super Lmiuix/appcompat/internal/view/menu/a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiuix/appcompat/internal/view/menu/action/b$d;,
        Lmiuix/appcompat/internal/view/menu/action/b$g;,
        Lmiuix/appcompat/internal/view/menu/action/b$b;,
        Lmiuix/appcompat/internal/view/menu/action/b$f;,
        Lmiuix/appcompat/internal/view/menu/action/b$c;,
        Lmiuix/appcompat/internal/view/menu/action/b$e;
    }
.end annotation


# instance fields
.field private A:Lmiuix/appcompat/internal/view/menu/action/b$e;

.field private B:Lmiuix/appcompat/internal/view/menu/e;

.field private C:Lmiuix/appcompat/internal/view/menu/action/b$b;

.field private D:Lmiuix/appcompat/internal/view/menu/action/b$d;

.field private E:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

.field final F:Lmiuix/appcompat/internal/view/menu/action/b$g;

.field G:I

.field private k:Landroid/view/View;

.field private l:Z

.field private m:Z

.field private n:I

.field private o:I

.field private p:I

.field private q:I

.field private r:I

.field private s:Z

.field private t:Z

.field private u:Z

.field private v:Z

.field private w:I

.field private final x:Landroid/util/SparseBooleanArray;

.field private y:Landroid/view/View;

.field private z:Lmiuix/appcompat/internal/view/menu/action/b$e;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;IIII)V
    .locals 0

    invoke-direct {p0, p1, p3, p4}, Lmiuix/appcompat/internal/view/menu/a;-><init>(Landroid/content/Context;II)V

    const p1, 0x10102f6

    iput p1, p0, Lmiuix/appcompat/internal/view/menu/action/b;->w:I

    new-instance p1, Landroid/util/SparseBooleanArray;

    invoke-direct {p1}, Landroid/util/SparseBooleanArray;-><init>()V

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/action/b;->x:Landroid/util/SparseBooleanArray;

    new-instance p1, Lmiuix/appcompat/internal/view/menu/action/b$g;

    const/4 p3, 0x0

    invoke-direct {p1, p0, p3}, Lmiuix/appcompat/internal/view/menu/action/b$g;-><init>(Lmiuix/appcompat/internal/view/menu/action/b;Lmiuix/appcompat/internal/view/menu/action/b$a;)V

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/action/b;->F:Lmiuix/appcompat/internal/view/menu/action/b$g;

    iput p5, p0, Lmiuix/appcompat/internal/view/menu/action/b;->r:I

    iput p6, p0, Lmiuix/appcompat/internal/view/menu/action/b;->q:I

    iput-object p2, p0, Lmiuix/appcompat/internal/view/menu/action/b;->E:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    return-void
.end method

.method static synthetic A(Lmiuix/appcompat/internal/view/menu/action/b;)Lmiuix/appcompat/internal/view/menu/c;
    .locals 0

    iget-object p0, p0, Lmiuix/appcompat/internal/view/menu/a;->c:Lmiuix/appcompat/internal/view/menu/c;

    return-object p0
.end method

.method static synthetic B(Lmiuix/appcompat/internal/view/menu/action/b;)Lmiuix/appcompat/internal/view/menu/h;
    .locals 0

    iget-object p0, p0, Lmiuix/appcompat/internal/view/menu/a;->i:Lmiuix/appcompat/internal/view/menu/h;

    return-object p0
.end method

.method static synthetic C(Lmiuix/appcompat/internal/view/menu/action/b;Lmiuix/appcompat/internal/view/menu/action/b$d;)Lmiuix/appcompat/internal/view/menu/action/b$d;
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/action/b;->D:Lmiuix/appcompat/internal/view/menu/action/b$d;

    return-object p1
.end method

.method static synthetic D(Lmiuix/appcompat/internal/view/menu/action/b;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lmiuix/appcompat/internal/view/menu/a;->b:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic E(Lmiuix/appcompat/internal/view/menu/action/b;)I
    .locals 0

    iget p0, p0, Lmiuix/appcompat/internal/view/menu/action/b;->r:I

    return p0
.end method

.method static synthetic F(Lmiuix/appcompat/internal/view/menu/action/b;)I
    .locals 0

    iget p0, p0, Lmiuix/appcompat/internal/view/menu/action/b;->q:I

    return p0
.end method

.method static synthetic G(Lmiuix/appcompat/internal/view/menu/action/b;)Lmiuix/appcompat/internal/view/menu/h;
    .locals 0

    iget-object p0, p0, Lmiuix/appcompat/internal/view/menu/a;->i:Lmiuix/appcompat/internal/view/menu/h;

    return-object p0
.end method

.method static synthetic H(Lmiuix/appcompat/internal/view/menu/action/b;)Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;
    .locals 0

    iget-object p0, p0, Lmiuix/appcompat/internal/view/menu/action/b;->E:Lmiuix/appcompat/internal/app/widget/ActionBarOverlayLayout;

    return-object p0
.end method

.method static synthetic I(Lmiuix/appcompat/internal/view/menu/action/b;)Lmiuix/appcompat/internal/view/menu/h;
    .locals 0

    iget-object p0, p0, Lmiuix/appcompat/internal/view/menu/a;->i:Lmiuix/appcompat/internal/view/menu/h;

    return-object p0
.end method

.method static synthetic J(Lmiuix/appcompat/internal/view/menu/action/b;)Lmiuix/appcompat/internal/view/menu/h;
    .locals 0

    iget-object p0, p0, Lmiuix/appcompat/internal/view/menu/a;->i:Lmiuix/appcompat/internal/view/menu/h;

    return-object p0
.end method

.method static synthetic K(Lmiuix/appcompat/internal/view/menu/action/b;)Lmiuix/appcompat/internal/view/menu/h;
    .locals 0

    iget-object p0, p0, Lmiuix/appcompat/internal/view/menu/a;->i:Lmiuix/appcompat/internal/view/menu/h;

    return-object p0
.end method

.method private N(Landroid/view/MenuItem;)Landroid/view/View;
    .locals 6

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/a;->i:Lmiuix/appcompat/internal/view/menu/h;

    check-cast v0, Landroid/view/ViewGroup;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_2

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    instance-of v5, v4, Lmiuix/appcompat/internal/view/menu/h$a;

    if-eqz v5, :cond_1

    move-object v5, v4

    check-cast v5, Lmiuix/appcompat/internal/view/menu/h$a;

    invoke-interface {v5}, Lmiuix/appcompat/internal/view/menu/h$a;->getItemData()Lmiuix/appcompat/internal/view/menu/e;

    move-result-object v5

    if-ne v5, p1, :cond_1

    return-object v4

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    return-object v1
.end method

.method private O()Lmiuix/appcompat/internal/view/menu/action/b$e;
    .locals 7

    invoke-direct {p0}, Lmiuix/appcompat/internal/view/menu/action/b;->a0()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lmiuix/appcompat/internal/view/menu/action/b$f;

    iget-object v3, p0, Lmiuix/appcompat/internal/view/menu/a;->b:Landroid/content/Context;

    iget-object v4, p0, Lmiuix/appcompat/internal/view/menu/a;->c:Lmiuix/appcompat/internal/view/menu/c;

    iget-object v5, p0, Lmiuix/appcompat/internal/view/menu/action/b;->k:Landroid/view/View;

    const/4 v6, 0x1

    move-object v1, v0

    move-object v2, p0

    invoke-direct/range {v1 .. v6}, Lmiuix/appcompat/internal/view/menu/action/b$f;-><init>(Lmiuix/appcompat/internal/view/menu/action/b;Landroid/content/Context;Lmiuix/appcompat/internal/view/menu/c;Landroid/view/View;Z)V

    return-object v0

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/action/b;->A:Lmiuix/appcompat/internal/view/menu/action/b$e;

    if-nez v0, :cond_1

    new-instance v0, Lmiuix/appcompat/internal/view/menu/action/b$c;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lmiuix/appcompat/internal/view/menu/action/b$c;-><init>(Lmiuix/appcompat/internal/view/menu/action/b;Lmiuix/appcompat/internal/view/menu/action/b$a;)V

    iput-object v0, p0, Lmiuix/appcompat/internal/view/menu/action/b;->A:Lmiuix/appcompat/internal/view/menu/action/b$e;

    :cond_1
    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/action/b;->A:Lmiuix/appcompat/internal/view/menu/action/b$e;

    return-object v0
.end method

.method private P()Lmiuix/appcompat/internal/view/menu/e;
    .locals 8

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/action/b;->B:Lmiuix/appcompat/internal/view/menu/e;

    if-nez v0, :cond_0

    iget-object v1, p0, Lmiuix/appcompat/internal/view/menu/a;->c:Lmiuix/appcompat/internal/view/menu/c;

    const/4 v2, 0x0

    sget v3, Lk9/g;->J:I

    const/4 v4, 0x0

    const/4 v5, 0x0

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/a;->b:Landroid/content/Context;

    sget v6, Lk9/j;->e:I

    invoke-virtual {v0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-static/range {v1 .. v7}, Lmiuix/appcompat/internal/view/menu/a;->l(Lmiuix/appcompat/internal/view/menu/c;IIIILjava/lang/CharSequence;I)Lmiuix/appcompat/internal/view/menu/e;

    move-result-object v0

    iput-object v0, p0, Lmiuix/appcompat/internal/view/menu/action/b;->B:Lmiuix/appcompat/internal/view/menu/e;

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/action/b;->B:Lmiuix/appcompat/internal/view/menu/e;

    return-object v0
.end method

.method private synthetic U()V
    .locals 3

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/a;->c:Lmiuix/appcompat/internal/view/menu/c;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lmiuix/appcompat/internal/view/menu/c;->A()Lmiuix/appcompat/internal/view/menu/c;

    move-result-object v1

    invoke-direct {p0}, Lmiuix/appcompat/internal/view/menu/action/b;->P()Lmiuix/appcompat/internal/view/menu/e;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lmiuix/appcompat/internal/view/menu/a;->m(Lmiuix/appcompat/internal/view/menu/c;Lmiuix/appcompat/internal/view/menu/c;Landroid/view/MenuItem;)Z

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/action/b;->k:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lmiuix/appcompat/internal/view/menu/action/b;->Q(Z)Z

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/action/b;->b0()Z

    :goto_0
    return-void
.end method

.method private a0()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public static synthetic t(Lmiuix/appcompat/internal/view/menu/action/b;)V
    .locals 0

    invoke-direct {p0}, Lmiuix/appcompat/internal/view/menu/action/b;->U()V

    return-void
.end method

.method static synthetic u(Lmiuix/appcompat/internal/view/menu/action/b;)Lmiuix/appcompat/internal/view/menu/h;
    .locals 0

    iget-object p0, p0, Lmiuix/appcompat/internal/view/menu/a;->i:Lmiuix/appcompat/internal/view/menu/h;

    return-object p0
.end method

.method static synthetic v(Lmiuix/appcompat/internal/view/menu/action/b;)Lmiuix/appcompat/internal/view/menu/c;
    .locals 0

    iget-object p0, p0, Lmiuix/appcompat/internal/view/menu/a;->c:Lmiuix/appcompat/internal/view/menu/c;

    return-object p0
.end method

.method static synthetic w(Lmiuix/appcompat/internal/view/menu/action/b;Lmiuix/appcompat/internal/view/menu/action/b$e;)Lmiuix/appcompat/internal/view/menu/action/b$e;
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/action/b;->z:Lmiuix/appcompat/internal/view/menu/action/b$e;

    return-object p1
.end method

.method static synthetic x(Lmiuix/appcompat/internal/view/menu/action/b;)Landroid/view/View;
    .locals 0

    iget-object p0, p0, Lmiuix/appcompat/internal/view/menu/action/b;->k:Landroid/view/View;

    return-object p0
.end method

.method static synthetic y(Lmiuix/appcompat/internal/view/menu/action/b;Lmiuix/appcompat/internal/view/menu/action/b$b;)Lmiuix/appcompat/internal/view/menu/action/b$b;
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/action/b;->C:Lmiuix/appcompat/internal/view/menu/action/b$b;

    return-object p1
.end method

.method static synthetic z(Lmiuix/appcompat/internal/view/menu/c;Z)V
    .locals 0

    invoke-static {p0, p1}, Lmiuix/appcompat/internal/view/menu/a;->j(Lmiuix/appcompat/internal/view/menu/c;Z)V

    return-void
.end method


# virtual methods
.method protected L(Landroid/content/Context;)Landroid/view/View;
    .locals 3

    new-instance v0, Lmiuix/appcompat/internal/view/menu/action/d;

    iget v1, p0, Lmiuix/appcompat/internal/view/menu/action/b;->w:I

    const/4 v2, 0x0

    invoke-direct {v0, p1, v2, v1}, Lmiuix/appcompat/internal/view/menu/action/d;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance p1, Lmiuix/appcompat/internal/view/menu/action/a;

    invoke-direct {p1, p0}, Lmiuix/appcompat/internal/view/menu/action/a;-><init>(Lmiuix/appcompat/internal/view/menu/action/b;)V

    invoke-virtual {v0, p1}, Lmiuix/appcompat/internal/view/menu/action/d;->b(Lmiuix/appcompat/internal/view/menu/action/d$a;)V

    return-object v0
.end method

.method public M(Z)Z
    .locals 0

    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/view/menu/action/b;->Q(Z)Z

    move-result p1

    return p1
.end method

.method public Q(Z)Z
    .locals 3

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/action/b;->D:Lmiuix/appcompat/internal/view/menu/action/b$d;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/a;->i:Lmiuix/appcompat/internal/view/menu/h;

    if-eqz v0, :cond_0

    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/action/b;->k:Landroid/view/View;

    invoke-virtual {p1, v1}, Landroid/view/View;->setSelected(Z)V

    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/a;->i:Lmiuix/appcompat/internal/view/menu/h;

    check-cast p1, Landroid/view/View;

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/action/b;->D:Lmiuix/appcompat/internal/view/menu/action/b$d;

    invoke-virtual {p1, v0}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    const/4 p1, 0x0

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/action/b;->D:Lmiuix/appcompat/internal/view/menu/action/b$d;

    const/4 p1, 0x1

    return p1

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/action/b;->z:Lmiuix/appcompat/internal/view/menu/action/b$e;

    if-eqz v0, :cond_2

    invoke-interface {v0}, Lmiuix/appcompat/internal/view/menu/action/b$e;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v2, p0, Lmiuix/appcompat/internal/view/menu/action/b;->k:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setSelected(Z)V

    :cond_1
    iget-object v1, p0, Lmiuix/appcompat/internal/view/menu/action/b;->z:Lmiuix/appcompat/internal/view/menu/action/b$e;

    invoke-interface {v1, p1}, Lmiuix/appcompat/internal/view/menu/action/b$e;->a(Z)V

    return v0

    :cond_2
    return v1
.end method

.method public R()Z
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/action/b;->C:Lmiuix/appcompat/internal/view/menu/action/b$b;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lmiuix/appcompat/internal/view/menu/d;->a()V

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public S()Z
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/action/b;->z:Lmiuix/appcompat/internal/view/menu/action/b$e;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lmiuix/appcompat/internal/view/menu/action/b$e;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public T()Z
    .locals 1

    iget-boolean v0, p0, Lmiuix/appcompat/internal/view/menu/action/b;->l:Z

    return v0
.end method

.method public V(Landroid/content/res/Configuration;)V
    .locals 2

    iget-boolean v0, p0, Lmiuix/appcompat/internal/view/menu/action/b;->s:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/a;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lk9/h;->a:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lmiuix/appcompat/internal/view/menu/action/b;->p:I

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/a;->c:Lmiuix/appcompat/internal/view/menu/c;

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lmiuix/appcompat/internal/view/menu/a;->p(Lmiuix/appcompat/internal/view/menu/c;Z)V

    :cond_1
    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/action/b;->k:Landroid/view/View;

    instance-of v1, v0, Lmiuix/appcompat/internal/view/menu/action/d;

    if-eqz v1, :cond_2

    check-cast v0, Lmiuix/appcompat/internal/view/menu/action/d;

    invoke-virtual {v0, p1}, Lmiuix/appcompat/internal/view/menu/action/d;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    :cond_2
    return-void
.end method

.method public W(Z)V
    .locals 0

    if-eqz p1, :cond_0

    sget p1, Lk9/b;->l:I

    iput p1, p0, Lmiuix/appcompat/internal/view/menu/action/b;->w:I

    :cond_0
    return-void
.end method

.method public X(Z)V
    .locals 0

    iput-boolean p1, p0, Lmiuix/appcompat/internal/view/menu/action/b;->v:Z

    return-void
.end method

.method public Y(Z)V
    .locals 0

    iput-boolean p1, p0, Lmiuix/appcompat/internal/view/menu/action/b;->l:Z

    const/4 p1, 0x1

    iput-boolean p1, p0, Lmiuix/appcompat/internal/view/menu/action/b;->m:Z

    return-void
.end method

.method public Z(IZ)V
    .locals 0

    iput p1, p0, Lmiuix/appcompat/internal/view/menu/action/b;->n:I

    iput-boolean p2, p0, Lmiuix/appcompat/internal/view/menu/action/b;->t:Z

    const/4 p1, 0x1

    iput-boolean p1, p0, Lmiuix/appcompat/internal/view/menu/action/b;->u:Z

    return-void
.end method

.method public b(Lmiuix/appcompat/internal/view/menu/c;Z)V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lmiuix/appcompat/internal/view/menu/action/b;->M(Z)Z

    invoke-super {p0, p1, p2}, Lmiuix/appcompat/internal/view/menu/a;->b(Lmiuix/appcompat/internal/view/menu/c;Z)V

    return-void
.end method

.method public b0()Z
    .locals 2

    iget-boolean v0, p0, Lmiuix/appcompat/internal/view/menu/action/b;->l:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/action/b;->S()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/a;->c:Lmiuix/appcompat/internal/view/menu/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/a;->i:Lmiuix/appcompat/internal/view/menu/h;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/action/b;->D:Lmiuix/appcompat/internal/view/menu/action/b$d;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lmiuix/appcompat/internal/view/menu/action/b;->O()Lmiuix/appcompat/internal/view/menu/action/b$e;

    move-result-object v0

    new-instance v1, Lmiuix/appcompat/internal/view/menu/action/b$d;

    invoke-direct {v1, p0, v0}, Lmiuix/appcompat/internal/view/menu/action/b$d;-><init>(Lmiuix/appcompat/internal/view/menu/action/b;Lmiuix/appcompat/internal/view/menu/action/b$e;)V

    iput-object v1, p0, Lmiuix/appcompat/internal/view/menu/action/b;->D:Lmiuix/appcompat/internal/view/menu/action/b$d;

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/a;->i:Lmiuix/appcompat/internal/view/menu/h;

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    const/4 v0, 0x0

    invoke-super {p0, v0}, Lmiuix/appcompat/internal/view/menu/a;->e(Lmiuix/appcompat/internal/view/menu/i;)Z

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/action/b;->k:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    return v1

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public c(Z)V
    .locals 3

    invoke-super {p0, p1}, Lmiuix/appcompat/internal/view/menu/a;->c(Z)V

    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/a;->i:Lmiuix/appcompat/internal/view/menu/h;

    if-nez p1, :cond_0

    return-void

    :cond_0
    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/a;->c:Lmiuix/appcompat/internal/view/menu/c;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lmiuix/appcompat/internal/view/menu/c;->w()Ljava/util/ArrayList;

    move-result-object p1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    iget-boolean v0, p0, Lmiuix/appcompat/internal/view/menu/action/b;->l:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_3

    if-eqz p1, :cond_3

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_2

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lmiuix/appcompat/internal/view/menu/e;

    invoke-virtual {p1}, Lmiuix/appcompat/internal/view/menu/e;->isActionViewExpanded()Z

    move-result p1

    xor-int/lit8 v1, p1, 0x1

    goto :goto_1

    :cond_2
    if-lez v0, :cond_3

    move v1, v2

    :cond_3
    :goto_1
    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/action/b;->k:Landroid/view/View;

    if-eqz v1, :cond_6

    if-nez p1, :cond_4

    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/a;->a:Landroid/content/Context;

    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/view/menu/action/b;->L(Landroid/content/Context;)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/action/b;->k:Landroid/view/View;

    goto :goto_2

    :cond_4
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setTranslationY(F)V

    :goto_2
    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/action/b;->k:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/a;->i:Lmiuix/appcompat/internal/view/menu/h;

    if-eq p1, v0, :cond_7

    if-eqz p1, :cond_5

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/action/b;->k:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_5
    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/a;->i:Lmiuix/appcompat/internal/view/menu/h;

    check-cast p1, Lmiuix/appcompat/internal/view/menu/action/c;

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/action/b;->k:Landroid/view/View;

    invoke-virtual {p1}, Lmiuix/appcompat/internal/view/menu/action/c;->i()Lmiuix/appcompat/internal/view/menu/action/c$a;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_3

    :cond_6
    if-eqz p1, :cond_7

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/a;->i:Lmiuix/appcompat/internal/view/menu/h;

    if-ne p1, v0, :cond_7

    check-cast v0, Landroid/view/ViewGroup;

    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/action/b;->k:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_7
    :goto_3
    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/a;->i:Lmiuix/appcompat/internal/view/menu/h;

    check-cast p1, Lmiuix/appcompat/internal/view/menu/action/c;

    iget-boolean v0, p0, Lmiuix/appcompat/internal/view/menu/action/b;->l:Z

    invoke-virtual {p1, v0}, Lmiuix/appcompat/internal/view/menu/action/c;->setOverflowReserved(Z)V

    invoke-direct {p0}, Lmiuix/appcompat/internal/view/menu/action/b;->a0()Z

    move-result p1

    if-nez p1, :cond_8

    invoke-direct {p0}, Lmiuix/appcompat/internal/view/menu/action/b;->O()Lmiuix/appcompat/internal/view/menu/action/b$e;

    move-result-object p1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/a;->c:Lmiuix/appcompat/internal/view/menu/c;

    invoke-interface {p1, v0}, Lmiuix/appcompat/internal/view/menu/action/b$e;->j(Lmiuix/appcompat/internal/view/menu/c;)V

    :cond_8
    return-void
.end method

.method public d()Z
    .locals 8

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/a;->c:Lmiuix/appcompat/internal/view/menu/c;

    invoke-virtual {v0}, Lmiuix/appcompat/internal/view/menu/c;->B()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget v2, p0, Lmiuix/appcompat/internal/view/menu/action/b;->p:I

    if-ge v2, v1, :cond_0

    add-int/lit8 v2, v2, -0x1

    :cond_0
    const/4 v3, 0x0

    move v4, v3

    :goto_0
    const/4 v5, 0x1

    if-ge v4, v1, :cond_4

    if-lez v2, :cond_4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lmiuix/appcompat/internal/view/menu/e;

    invoke-virtual {v6}, Lmiuix/appcompat/internal/view/menu/e;->l()Z

    move-result v7

    if-nez v7, :cond_2

    invoke-virtual {v6}, Lmiuix/appcompat/internal/view/menu/e;->m()Z

    move-result v7

    if-eqz v7, :cond_1

    goto :goto_1

    :cond_1
    move v5, v3

    :cond_2
    :goto_1
    invoke-virtual {v6, v5}, Lmiuix/appcompat/internal/view/menu/e;->q(Z)V

    if-eqz v5, :cond_3

    add-int/lit8 v2, v2, -0x1

    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_4
    :goto_2
    if-ge v4, v1, :cond_5

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmiuix/appcompat/internal/view/menu/e;

    invoke-virtual {v2, v3}, Lmiuix/appcompat/internal/view/menu/e;->q(Z)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_5
    return v5
.end method

.method public e(Lmiuix/appcompat/internal/view/menu/i;)Z
    .locals 4

    invoke-virtual {p1}, Lmiuix/appcompat/internal/view/menu/c;->hasVisibleItems()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    move-object v0, p1

    :goto_0
    invoke-virtual {v0}, Lmiuix/appcompat/internal/view/menu/i;->W()Landroid/view/Menu;

    move-result-object v2

    iget-object v3, p0, Lmiuix/appcompat/internal/view/menu/a;->c:Lmiuix/appcompat/internal/view/menu/c;

    if-eq v2, v3, :cond_1

    invoke-virtual {v0}, Lmiuix/appcompat/internal/view/menu/i;->W()Landroid/view/Menu;

    move-result-object v0

    check-cast v0, Lmiuix/appcompat/internal/view/menu/i;

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lmiuix/appcompat/internal/view/menu/i;->getItem()Landroid/view/MenuItem;

    move-result-object v0

    invoke-direct {p0, v0}, Lmiuix/appcompat/internal/view/menu/action/b;->N(Landroid/view/MenuItem;)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/action/b;->k:Landroid/view/View;

    if-nez v0, :cond_2

    return v1

    :cond_2
    invoke-virtual {p1}, Lmiuix/appcompat/internal/view/menu/i;->getItem()Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    iput v0, p0, Lmiuix/appcompat/internal/view/menu/action/b;->G:I

    new-instance v0, Lmiuix/appcompat/internal/view/menu/action/b$b;

    invoke-direct {v0, p0, p1}, Lmiuix/appcompat/internal/view/menu/action/b$b;-><init>(Lmiuix/appcompat/internal/view/menu/action/b;Lmiuix/appcompat/internal/view/menu/i;)V

    iput-object v0, p0, Lmiuix/appcompat/internal/view/menu/action/b;->C:Lmiuix/appcompat/internal/view/menu/action/b$b;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lmiuix/appcompat/internal/view/menu/d;->d(Landroid/os/IBinder;)V

    invoke-super {p0, p1}, Lmiuix/appcompat/internal/view/menu/a;->e(Lmiuix/appcompat/internal/view/menu/i;)Z

    const/4 p1, 0x1

    return p1
.end method

.method public f(Lmiuix/appcompat/internal/view/menu/e;Lmiuix/appcompat/internal/view/menu/h$a;)V
    .locals 1

    const/4 v0, 0x0

    invoke-interface {p2, p1, v0}, Lmiuix/appcompat/internal/view/menu/h$a;->b(Lmiuix/appcompat/internal/view/menu/e;I)V

    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/a;->i:Lmiuix/appcompat/internal/view/menu/h;

    check-cast p1, Lmiuix/appcompat/internal/view/menu/c$b;

    invoke-interface {p2, p1}, Lmiuix/appcompat/internal/view/menu/h$a;->setItemInvoker(Lmiuix/appcompat/internal/view/menu/c$b;)V

    return-void
.end method

.method public h(Landroid/content/Context;Lmiuix/appcompat/internal/view/menu/c;)V
    .locals 2

    invoke-super {p0, p1, p2}, Lmiuix/appcompat/internal/view/menu/a;->h(Landroid/content/Context;Lmiuix/appcompat/internal/view/menu/c;)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    invoke-static {p1}, Ls9/a;->b(Landroid/content/Context;)Ls9/a;

    move-result-object p1

    iget-boolean p2, p0, Lmiuix/appcompat/internal/view/menu/action/b;->m:Z

    if-nez p2, :cond_0

    invoke-virtual {p1}, Ls9/a;->j()Z

    move-result p2

    iput-boolean p2, p0, Lmiuix/appcompat/internal/view/menu/action/b;->l:Z

    :cond_0
    iget-boolean p2, p0, Lmiuix/appcompat/internal/view/menu/action/b;->u:Z

    if-nez p2, :cond_1

    invoke-virtual {p1}, Ls9/a;->c()I

    move-result p2

    iput p2, p0, Lmiuix/appcompat/internal/view/menu/action/b;->n:I

    :cond_1
    iget-boolean p2, p0, Lmiuix/appcompat/internal/view/menu/action/b;->s:Z

    if-nez p2, :cond_2

    invoke-virtual {p1}, Ls9/a;->d()I

    move-result p1

    iput p1, p0, Lmiuix/appcompat/internal/view/menu/action/b;->p:I

    :cond_2
    iget p1, p0, Lmiuix/appcompat/internal/view/menu/action/b;->n:I

    iget-boolean p2, p0, Lmiuix/appcompat/internal/view/menu/action/b;->l:Z

    const/4 v0, 0x0

    if-eqz p2, :cond_4

    iget-object p2, p0, Lmiuix/appcompat/internal/view/menu/action/b;->k:Landroid/view/View;

    if-nez p2, :cond_3

    iget-object p2, p0, Lmiuix/appcompat/internal/view/menu/a;->a:Landroid/content/Context;

    invoke-virtual {p0, p2}, Lmiuix/appcompat/internal/view/menu/action/b;->L(Landroid/content/Context;)Landroid/view/View;

    move-result-object p2

    iput-object p2, p0, Lmiuix/appcompat/internal/view/menu/action/b;->k:Landroid/view/View;

    const/4 p2, 0x0

    invoke-static {p2, p2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    iget-object v1, p0, Lmiuix/appcompat/internal/view/menu/action/b;->k:Landroid/view/View;

    invoke-virtual {v1, p2, p2}, Landroid/view/View;->measure(II)V

    :cond_3
    iget-object p2, p0, Lmiuix/appcompat/internal/view/menu/action/b;->k:Landroid/view/View;

    invoke-virtual {p2}, Landroid/view/View;->getMeasuredWidth()I

    move-result p2

    sub-int/2addr p1, p2

    goto :goto_0

    :cond_4
    iput-object v0, p0, Lmiuix/appcompat/internal/view/menu/action/b;->k:Landroid/view/View;

    :goto_0
    iput p1, p0, Lmiuix/appcompat/internal/view/menu/action/b;->o:I

    iput-object v0, p0, Lmiuix/appcompat/internal/view/menu/action/b;->y:Landroid/view/View;

    return-void
.end method

.method public n(Lmiuix/appcompat/internal/view/menu/e;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    invoke-virtual {p1}, Lmiuix/appcompat/internal/view/menu/e;->getActionView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lmiuix/appcompat/internal/view/menu/e;->h()Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    instance-of v0, p2, Lmiuix/appcompat/internal/view/menu/action/ActionMenuItemView;

    if-nez v0, :cond_1

    const/4 p2, 0x0

    :cond_1
    invoke-super {p0, p1, p2, p3}, Lmiuix/appcompat/internal/view/menu/a;->n(Lmiuix/appcompat/internal/view/menu/e;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    :cond_2
    invoke-virtual {p1}, Lmiuix/appcompat/internal/view/menu/e;->isActionViewExpanded()Z

    move-result p1

    if-eqz p1, :cond_3

    const/16 p1, 0x8

    goto :goto_0

    :cond_3
    const/4 p1, 0x0

    :goto_0
    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    check-cast p3, Lmiuix/appcompat/internal/view/menu/action/c;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    invoke-virtual {p3, p1}, Lmiuix/appcompat/internal/view/menu/action/c;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result p2

    if-nez p2, :cond_4

    invoke-virtual {p3, p1}, Lmiuix/appcompat/internal/view/menu/action/c;->h(Landroid/view/ViewGroup$LayoutParams;)Lmiuix/appcompat/internal/view/menu/action/c$a;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_4
    return-object v0
.end method

.method public o(Landroid/view/ViewGroup;)Lmiuix/appcompat/internal/view/menu/h;
    .locals 1

    invoke-super {p0, p1}, Lmiuix/appcompat/internal/view/menu/a;->o(Landroid/view/ViewGroup;)Lmiuix/appcompat/internal/view/menu/h;

    move-result-object p1

    move-object v0, p1

    check-cast v0, Lmiuix/appcompat/internal/view/menu/action/c;

    invoke-virtual {v0, p0}, Lmiuix/appcompat/internal/view/menu/action/c;->setPresenter(Lmiuix/appcompat/internal/view/menu/action/b;)V

    return-object p1
.end method

.method public s(ILmiuix/appcompat/internal/view/menu/e;)Z
    .locals 0

    invoke-virtual {p2}, Lmiuix/appcompat/internal/view/menu/e;->j()Z

    move-result p1

    return p1
.end method
