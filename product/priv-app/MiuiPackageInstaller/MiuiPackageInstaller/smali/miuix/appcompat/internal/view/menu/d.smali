.class public Lmiuix/appcompat/internal/view/menu/d;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/DialogInterface$OnKeyListener;
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/content/DialogInterface$OnDismissListener;
.implements Lmiuix/appcompat/internal/view/menu/g$a;


# instance fields
.field private a:Lmiuix/appcompat/internal/view/menu/c;

.field private b:Lmiuix/appcompat/app/i;

.field c:Lmiuix/appcompat/internal/view/menu/b;

.field private d:Lmiuix/appcompat/internal/view/menu/g$a;


# direct methods
.method public constructor <init>(Lmiuix/appcompat/internal/view/menu/c;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/d;->a:Lmiuix/appcompat/internal/view/menu/c;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/d;->b:Lmiuix/appcompat/app/i;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lmiuix/appcompat/app/i;->dismiss()V

    :cond_0
    return-void
.end method

.method public b(Lmiuix/appcompat/internal/view/menu/c;Z)V
    .locals 1

    if-nez p2, :cond_0

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/d;->a:Lmiuix/appcompat/internal/view/menu/c;

    if-ne p1, v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/d;->a()V

    :cond_1
    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/d;->d:Lmiuix/appcompat/internal/view/menu/g$a;

    if-eqz v0, :cond_2

    invoke-interface {v0, p1, p2}, Lmiuix/appcompat/internal/view/menu/g$a;->b(Lmiuix/appcompat/internal/view/menu/c;Z)V

    :cond_2
    return-void
.end method

.method public c(Lmiuix/appcompat/internal/view/menu/g$a;)V
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/d;->d:Lmiuix/appcompat/internal/view/menu/g$a;

    return-void
.end method

.method public d(Landroid/os/IBinder;)V
    .locals 5

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/d;->a:Lmiuix/appcompat/internal/view/menu/c;

    new-instance v1, Lmiuix/appcompat/app/i$b;

    invoke-virtual {v0}, Lmiuix/appcompat/internal/view/menu/c;->r()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lmiuix/appcompat/app/i$b;-><init>(Landroid/content/Context;)V

    new-instance v2, Lmiuix/appcompat/internal/view/menu/b;

    invoke-virtual {v0}, Lmiuix/appcompat/internal/view/menu/c;->r()Landroid/content/Context;

    move-result-object v3

    sget v4, Lk9/i;->v:I

    invoke-direct {v2, v3, v4}, Lmiuix/appcompat/internal/view/menu/b;-><init>(Landroid/content/Context;I)V

    iput-object v2, p0, Lmiuix/appcompat/internal/view/menu/d;->c:Lmiuix/appcompat/internal/view/menu/b;

    invoke-virtual {v2, p0}, Lmiuix/appcompat/internal/view/menu/b;->k(Lmiuix/appcompat/internal/view/menu/g$a;)V

    iget-object v2, p0, Lmiuix/appcompat/internal/view/menu/d;->a:Lmiuix/appcompat/internal/view/menu/c;

    iget-object v3, p0, Lmiuix/appcompat/internal/view/menu/d;->c:Lmiuix/appcompat/internal/view/menu/b;

    invoke-virtual {v2, v3}, Lmiuix/appcompat/internal/view/menu/c;->b(Lmiuix/appcompat/internal/view/menu/g;)V

    iget-object v2, p0, Lmiuix/appcompat/internal/view/menu/d;->c:Lmiuix/appcompat/internal/view/menu/b;

    invoke-virtual {v2}, Lmiuix/appcompat/internal/view/menu/b;->f()Landroid/widget/ListAdapter;

    move-result-object v2

    invoke-virtual {v1, v2, p0}, Lmiuix/appcompat/app/i$b;->b(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Lmiuix/appcompat/app/i$b;

    invoke-virtual {v0}, Lmiuix/appcompat/internal/view/menu/c;->v()Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v2}, Lmiuix/appcompat/app/i$b;->c(Landroid/view/View;)Lmiuix/appcompat/app/i$b;

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Lmiuix/appcompat/internal/view/menu/c;->t()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmiuix/appcompat/app/i$b;->d(Landroid/graphics/drawable/Drawable;)Lmiuix/appcompat/app/i$b;

    move-result-object v2

    invoke-virtual {v0}, Lmiuix/appcompat/internal/view/menu/c;->u()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v2, v0}, Lmiuix/appcompat/app/i$b;->t(Ljava/lang/CharSequence;)Lmiuix/appcompat/app/i$b;

    :goto_0
    invoke-virtual {v1, p0}, Lmiuix/appcompat/app/i$b;->n(Landroid/content/DialogInterface$OnKeyListener;)Lmiuix/appcompat/app/i$b;

    invoke-virtual {v1}, Lmiuix/appcompat/app/i$b;->a()Lmiuix/appcompat/app/i;

    move-result-object v0

    iput-object v0, p0, Lmiuix/appcompat/internal/view/menu/d;->b:Lmiuix/appcompat/app/i;

    invoke-virtual {v0, p0}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/d;->b:Lmiuix/appcompat/app/i;

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    const/16 v1, 0x3eb

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    if-eqz p1, :cond_1

    iput-object p1, v0, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    :cond_1
    iget p1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    const/high16 v1, 0x20000

    or-int/2addr p1, v1

    iput p1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/d;->b:Lmiuix/appcompat/app/i;

    invoke-virtual {p1}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method public f(Lmiuix/appcompat/internal/view/menu/c;)Z
    .locals 1

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/d;->d:Lmiuix/appcompat/internal/view/menu/g$a;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lmiuix/appcompat/internal/view/menu/g$a;->f(Lmiuix/appcompat/internal/view/menu/c;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1

    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/d;->a:Lmiuix/appcompat/internal/view/menu/c;

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/d;->c:Lmiuix/appcompat/internal/view/menu/b;

    invoke-virtual {v0}, Lmiuix/appcompat/internal/view/menu/b;->f()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0, p2}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lmiuix/appcompat/internal/view/menu/e;

    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Lmiuix/appcompat/internal/view/menu/c;->H(Landroid/view/MenuItem;I)Z

    return-void
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 2

    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/d;->c:Lmiuix/appcompat/internal/view/menu/b;

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/d;->a:Lmiuix/appcompat/internal/view/menu/c;

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Lmiuix/appcompat/internal/view/menu/b;->b(Lmiuix/appcompat/internal/view/menu/c;Z)V

    return-void
.end method

.method public onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 2

    const/16 v0, 0x52

    if-eq p2, v0, :cond_0

    const/4 v0, 0x4

    if-ne p2, v0, :cond_2

    :cond_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_1

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v0

    if-nez v0, :cond_1

    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/d;->b:Lmiuix/appcompat/app/i;

    invoke-virtual {p1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object p1

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Landroid/view/View;->getKeyDispatcherState()Landroid/view/KeyEvent$DispatcherState;

    move-result-object p1

    if-eqz p1, :cond_2

    invoke-virtual {p1, p3, p0}, Landroid/view/KeyEvent$DispatcherState;->startTracking(Landroid/view/KeyEvent;Ljava/lang/Object;)V

    return v1

    :cond_1
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-ne v0, v1, :cond_2

    invoke-virtual {p3}, Landroid/view/KeyEvent;->isCanceled()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/d;->b:Lmiuix/appcompat/app/i;

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/view/View;->getKeyDispatcherState()Landroid/view/KeyEvent$DispatcherState;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0, p3}, Landroid/view/KeyEvent$DispatcherState;->isTracking(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object p2, p0, Lmiuix/appcompat/internal/view/menu/d;->a:Lmiuix/appcompat/internal/view/menu/c;

    invoke-virtual {p2, v1}, Lmiuix/appcompat/internal/view/menu/c;->e(Z)V

    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    return v1

    :cond_2
    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/d;->a:Lmiuix/appcompat/internal/view/menu/c;

    const/4 v0, 0x0

    invoke-virtual {p1, p2, p3, v0}, Lmiuix/appcompat/internal/view/menu/c;->performShortcut(ILandroid/view/KeyEvent;I)Z

    move-result p1

    return p1
.end method
