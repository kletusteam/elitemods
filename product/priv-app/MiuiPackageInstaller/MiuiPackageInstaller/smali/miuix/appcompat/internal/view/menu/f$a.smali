.class Lmiuix/appcompat/internal/view/menu/f$a;
.super Landroid/widget/BaseAdapter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiuix/appcompat/internal/view/menu/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field private a:Lmiuix/appcompat/internal/view/menu/c;

.field private b:I

.field final synthetic c:Lmiuix/appcompat/internal/view/menu/f;


# direct methods
.method public constructor <init>(Lmiuix/appcompat/internal/view/menu/f;Lmiuix/appcompat/internal/view/menu/c;)V
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/f$a;->c:Lmiuix/appcompat/internal/view/menu/f;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    const/4 p1, -0x1

    iput p1, p0, Lmiuix/appcompat/internal/view/menu/f$a;->b:I

    iput-object p2, p0, Lmiuix/appcompat/internal/view/menu/f$a;->a:Lmiuix/appcompat/internal/view/menu/c;

    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/f$a;->b()V

    return-void
.end method

.method static synthetic a(Lmiuix/appcompat/internal/view/menu/f$a;)Lmiuix/appcompat/internal/view/menu/c;
    .locals 0

    iget-object p0, p0, Lmiuix/appcompat/internal/view/menu/f$a;->a:Lmiuix/appcompat/internal/view/menu/c;

    return-object p0
.end method


# virtual methods
.method b()V
    .locals 5

    goto/32 :goto_9

    nop

    :goto_0
    const/4 v3, 0x0

    :goto_1
    goto/32 :goto_d

    nop

    :goto_2
    goto :goto_1

    :goto_3
    goto/32 :goto_f

    nop

    :goto_4
    invoke-virtual {v0}, Lmiuix/appcompat/internal/view/menu/c;->s()Lmiuix/appcompat/internal/view/menu/e;

    move-result-object v0

    goto/32 :goto_7

    nop

    :goto_5
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    goto/32 :goto_0

    nop

    :goto_6
    add-int/lit8 v3, v3, 0x1

    goto/32 :goto_2

    nop

    :goto_7
    if-nez v0, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_14

    nop

    :goto_8
    iput v3, p0, Lmiuix/appcompat/internal/view/menu/f$a;->b:I

    goto/32 :goto_11

    nop

    :goto_9
    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/f$a;->c:Lmiuix/appcompat/internal/view/menu/f;

    goto/32 :goto_16

    nop

    :goto_a
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    goto/32 :goto_c

    nop

    :goto_b
    iput v0, p0, Lmiuix/appcompat/internal/view/menu/f$a;->b:I

    goto/32 :goto_e

    nop

    :goto_c
    check-cast v4, Lmiuix/appcompat/internal/view/menu/e;

    goto/32 :goto_15

    nop

    :goto_d
    if-lt v3, v2, :cond_1

    goto/32 :goto_3

    :cond_1
    goto/32 :goto_a

    nop

    :goto_e
    return-void

    :goto_f
    const/4 v0, -0x1

    goto/32 :goto_b

    nop

    :goto_10
    invoke-virtual {v1}, Lmiuix/appcompat/internal/view/menu/c;->w()Ljava/util/ArrayList;

    move-result-object v1

    goto/32 :goto_5

    nop

    :goto_11
    return-void

    :goto_12
    goto/32 :goto_6

    nop

    :goto_13
    invoke-static {v1}, Lmiuix/appcompat/internal/view/menu/f;->n(Lmiuix/appcompat/internal/view/menu/f;)Lmiuix/appcompat/internal/view/menu/c;

    move-result-object v1

    goto/32 :goto_10

    nop

    :goto_14
    iget-object v1, p0, Lmiuix/appcompat/internal/view/menu/f$a;->c:Lmiuix/appcompat/internal/view/menu/f;

    goto/32 :goto_13

    nop

    :goto_15
    if-eq v4, v0, :cond_2

    goto/32 :goto_12

    :cond_2
    goto/32 :goto_8

    nop

    :goto_16
    invoke-static {v0}, Lmiuix/appcompat/internal/view/menu/f;->n(Lmiuix/appcompat/internal/view/menu/f;)Lmiuix/appcompat/internal/view/menu/c;

    move-result-object v0

    goto/32 :goto_4

    nop
.end method

.method public c(I)Lmiuix/appcompat/internal/view/menu/e;
    .locals 2

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/f$a;->c:Lmiuix/appcompat/internal/view/menu/f;

    invoke-static {v0}, Lmiuix/appcompat/internal/view/menu/f;->k(Lmiuix/appcompat/internal/view/menu/f;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/f$a;->a:Lmiuix/appcompat/internal/view/menu/c;

    invoke-virtual {v0}, Lmiuix/appcompat/internal/view/menu/c;->w()Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/f$a;->a:Lmiuix/appcompat/internal/view/menu/c;

    invoke-virtual {v0}, Lmiuix/appcompat/internal/view/menu/c;->B()Ljava/util/ArrayList;

    move-result-object v0

    :goto_0
    iget v1, p0, Lmiuix/appcompat/internal/view/menu/f$a;->b:I

    if-ltz v1, :cond_1

    if-lt p1, v1, :cond_1

    add-int/lit8 p1, p1, 0x1

    :cond_1
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lmiuix/appcompat/internal/view/menu/e;

    return-object p1
.end method

.method public getCount()I
    .locals 2

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/f$a;->c:Lmiuix/appcompat/internal/view/menu/f;

    invoke-static {v0}, Lmiuix/appcompat/internal/view/menu/f;->k(Lmiuix/appcompat/internal/view/menu/f;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/f$a;->a:Lmiuix/appcompat/internal/view/menu/c;

    invoke-virtual {v0}, Lmiuix/appcompat/internal/view/menu/c;->w()Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/f$a;->a:Lmiuix/appcompat/internal/view/menu/c;

    invoke-virtual {v0}, Lmiuix/appcompat/internal/view/menu/c;->B()Ljava/util/ArrayList;

    move-result-object v0

    :goto_0
    iget v1, p0, Lmiuix/appcompat/internal/view/menu/f$a;->b:I

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gez v1, :cond_1

    return v0

    :cond_1
    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 0

    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/view/menu/f$a;->c(I)Lmiuix/appcompat/internal/view/menu/e;

    move-result-object p1

    return-object p1
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    const/4 v0, 0x0

    if-nez p2, :cond_0

    iget-object p2, p0, Lmiuix/appcompat/internal/view/menu/f$a;->c:Lmiuix/appcompat/internal/view/menu/f;

    invoke-static {p2}, Lmiuix/appcompat/internal/view/menu/f;->m(Lmiuix/appcompat/internal/view/menu/f;)Landroid/view/LayoutInflater;

    move-result-object p2

    iget-object v1, p0, Lmiuix/appcompat/internal/view/menu/f$a;->c:Lmiuix/appcompat/internal/view/menu/f;

    invoke-static {v1}, Lmiuix/appcompat/internal/view/menu/f;->l(Lmiuix/appcompat/internal/view/menu/f;)I

    move-result v1

    invoke-virtual {p2, v1, p3, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    invoke-static {p2}, Lia/c;->c(Landroid/view/View;)V

    :cond_0
    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/f$a;->getCount()I

    move-result p3

    invoke-static {p2, p1, p3}, Lia/h;->d(Landroid/view/View;II)V

    move-object p3, p2

    check-cast p3, Lmiuix/appcompat/internal/view/menu/h$a;

    iget-object v1, p0, Lmiuix/appcompat/internal/view/menu/f$a;->c:Lmiuix/appcompat/internal/view/menu/f;

    iget-boolean v1, v1, Lmiuix/appcompat/internal/view/menu/f;->a:Z

    if-eqz v1, :cond_1

    move-object v1, p2

    check-cast v1, Lmiuix/appcompat/internal/view/menu/ListMenuItemView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lmiuix/appcompat/internal/view/menu/ListMenuItemView;->setForceShowIcon(Z)V

    :cond_1
    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/view/menu/f$a;->c(I)Lmiuix/appcompat/internal/view/menu/e;

    move-result-object p1

    invoke-interface {p3, p1, v0}, Lmiuix/appcompat/internal/view/menu/h$a;->b(Lmiuix/appcompat/internal/view/menu/e;I)V

    return-object p2
.end method

.method public notifyDataSetChanged()V
    .locals 0

    invoke-virtual {p0}, Lmiuix/appcompat/internal/view/menu/f$a;->b()V

    invoke-super {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    return-void
.end method
