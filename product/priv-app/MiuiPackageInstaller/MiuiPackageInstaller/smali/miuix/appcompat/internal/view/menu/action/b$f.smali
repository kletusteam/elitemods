.class Lmiuix/appcompat/internal/view/menu/action/b$f;
.super Lmiuix/appcompat/internal/view/menu/f;

# interfaces
.implements Lmiuix/appcompat/internal/view/menu/action/b$e;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmiuix/appcompat/internal/view/menu/action/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "f"
.end annotation


# instance fields
.field final synthetic l:Lmiuix/appcompat/internal/view/menu/action/b;


# direct methods
.method public constructor <init>(Lmiuix/appcompat/internal/view/menu/action/b;Landroid/content/Context;Lmiuix/appcompat/internal/view/menu/c;Landroid/view/View;Z)V
    .locals 0

    iput-object p1, p0, Lmiuix/appcompat/internal/view/menu/action/b$f;->l:Lmiuix/appcompat/internal/view/menu/action/b;

    invoke-direct {p0, p2, p3, p4, p5}, Lmiuix/appcompat/internal/view/menu/f;-><init>(Landroid/content/Context;Lmiuix/appcompat/internal/view/menu/c;Landroid/view/View;Z)V

    iget-object p1, p1, Lmiuix/appcompat/internal/view/menu/action/b;->F:Lmiuix/appcompat/internal/view/menu/action/b$g;

    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/view/menu/f;->o(Lmiuix/appcompat/internal/view/menu/g$a;)V

    sget p1, Lk9/i;->y:I

    invoke-virtual {p0, p1}, Lmiuix/appcompat/internal/view/menu/f;->q(I)V

    return-void
.end method


# virtual methods
.method public a(Z)V
    .locals 1

    invoke-super {p0, p1}, Lmiuix/appcompat/internal/view/menu/f;->a(Z)V

    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/action/b$f;->l:Lmiuix/appcompat/internal/view/menu/action/b;

    invoke-static {p1}, Lmiuix/appcompat/internal/view/menu/action/b;->x(Lmiuix/appcompat/internal/view/menu/action/b;)Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lmiuix/appcompat/internal/view/menu/action/b$f;->l:Lmiuix/appcompat/internal/view/menu/action/b;

    invoke-static {p1}, Lmiuix/appcompat/internal/view/menu/action/b;->x(Lmiuix/appcompat/internal/view/menu/action/b;)Landroid/view/View;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setSelected(Z)V

    :cond_0
    return-void
.end method

.method public j(Lmiuix/appcompat/internal/view/menu/c;)V
    .locals 0

    return-void
.end method

.method public onDismiss()V
    .locals 2

    invoke-super {p0}, Lmiuix/appcompat/internal/view/menu/f;->onDismiss()V

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/action/b$f;->l:Lmiuix/appcompat/internal/view/menu/action/b;

    invoke-static {v0}, Lmiuix/appcompat/internal/view/menu/action/b;->v(Lmiuix/appcompat/internal/view/menu/action/b;)Lmiuix/appcompat/internal/view/menu/c;

    move-result-object v0

    invoke-virtual {v0}, Lmiuix/appcompat/internal/view/menu/c;->close()V

    iget-object v0, p0, Lmiuix/appcompat/internal/view/menu/action/b$f;->l:Lmiuix/appcompat/internal/view/menu/action/b;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lmiuix/appcompat/internal/view/menu/action/b;->w(Lmiuix/appcompat/internal/view/menu/action/b;Lmiuix/appcompat/internal/view/menu/action/b$e;)Lmiuix/appcompat/internal/view/menu/action/b$e;

    return-void
.end method
