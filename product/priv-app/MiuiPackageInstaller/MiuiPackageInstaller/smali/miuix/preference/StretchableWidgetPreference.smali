.class public Lmiuix/preference/StretchableWidgetPreference;
.super Landroidx/preference/Preference;


# instance fields
.field private P:Landroid/widget/ImageView;

.field private Q:Landroid/widget/RelativeLayout;

.field private R:Lmiuix/stretchablewidget/WidgetContainer;

.field private S:Landroid/widget/TextView;

.field private T:Landroid/widget/TextView;

.field private U:Landroid/view/View;

.field private V:Landroid/view/View;

.field private W:Z

.field private X:Ljava/lang/String;

.field private Y:I

.field private Z:Ljb/b;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    sget v0, Lva/k;->n:I

    invoke-direct {p0, p1, p2, v0}, Lmiuix/preference/StretchableWidgetPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    invoke-direct {p0, p1, p2, p3}, Landroidx/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, 0x0

    iput v0, p0, Lmiuix/preference/StretchableWidgetPreference;->Y:I

    sget-object v1, Lva/q;->z1:[I

    invoke-virtual {p1, p2, v1, p3, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p1

    sget p2, Lva/q;->A1:I

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object p2

    iput-object p2, p0, Lmiuix/preference/StretchableWidgetPreference;->X:Ljava/lang/String;

    sget p2, Lva/q;->B1:I

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p2

    iput-boolean p2, p0, Lmiuix/preference/StretchableWidgetPreference;->W:Z

    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method static synthetic H0(Lmiuix/preference/StretchableWidgetPreference;)V
    .locals 0

    invoke-direct {p0}, Lmiuix/preference/StretchableWidgetPreference;->L0()V

    return-void
.end method

.method private I0(Z)V
    .locals 7

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    iget-object v2, p0, Lmiuix/preference/StretchableWidgetPreference;->R:Lmiuix/stretchablewidget/WidgetContainer;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v1}, Lmiuix/animation/a;->y([Ljava/lang/Object;)Lmiuix/animation/h;

    move-result-object v1

    const-string v2, "start"

    invoke-interface {v1, v2}, Lmiuix/animation/h;->y(Ljava/lang/Object;)Lmiuix/animation/h;

    move-result-object v1

    iget v4, p0, Lmiuix/preference/StretchableWidgetPreference;->Y:I

    const-string v5, "widgetHeight"

    invoke-interface {v1, v5, v4}, Lmiuix/animation/h;->o(Ljava/lang/String;I)Lmiuix/animation/h;

    move-result-object v1

    sget-object v4, Lh9/h;->o:Lh9/h;

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-interface {v1, v4, v6}, Lmiuix/animation/h;->B(Lh9/b;F)Lmiuix/animation/h;

    move-result-object v1

    const-string v6, "end"

    invoke-interface {v1, v6}, Lmiuix/animation/h;->y(Ljava/lang/Object;)Lmiuix/animation/h;

    move-result-object v1

    invoke-interface {v1, v5, v3}, Lmiuix/animation/h;->o(Ljava/lang/String;I)Lmiuix/animation/h;

    move-result-object v1

    const/4 v5, 0x0

    invoke-interface {v1, v4, v5}, Lmiuix/animation/h;->B(Lh9/b;F)Lmiuix/animation/h;

    new-array v0, v0, [Ljava/lang/Object;

    iget-object v1, p0, Lmiuix/preference/StretchableWidgetPreference;->R:Lmiuix/stretchablewidget/WidgetContainer;

    aput-object v1, v0, v3

    invoke-static {v0}, Lmiuix/animation/a;->y([Ljava/lang/Object;)Lmiuix/animation/h;

    move-result-object v0

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    move-object v2, v6

    :goto_0
    invoke-interface {v0, v2}, Lmiuix/animation/h;->C(Ljava/lang/Object;)Lmiuix/animation/h;

    return-void
.end method

.method private L0()V
    .locals 6

    iget-boolean v0, p0, Lmiuix/preference/StretchableWidgetPreference;->W:Z

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    iput-boolean v0, p0, Lmiuix/preference/StretchableWidgetPreference;->W:Z

    const/4 v2, 0x0

    const/4 v3, 0x2

    const/4 v4, -0x2

    const/4 v5, 0x0

    if-eqz v0, :cond_0

    new-instance v0, Lc9/c;

    invoke-direct {v0}, Lc9/c;-><init>()V

    new-array v3, v3, [F

    fill-array-data v3, :array_0

    invoke-virtual {v0, v4, v3}, Lc9/a;->k(I[F)Lc9/a;

    move-result-object v0

    check-cast v0, Lc9/c;

    new-array v3, v1, [Ljava/lang/Object;

    iget-object v4, p0, Lmiuix/preference/StretchableWidgetPreference;->R:Lmiuix/stretchablewidget/WidgetContainer;

    aput-object v4, v3, v5

    invoke-static {v3}, Lmiuix/animation/a;->y([Ljava/lang/Object;)Lmiuix/animation/h;

    move-result-object v3

    new-array v1, v1, [Lc9/a;

    new-instance v4, Lc9/a;

    invoke-direct {v4}, Lc9/a;-><init>()V

    invoke-virtual {v4, v2}, Lc9/a;->m(F)Lc9/a;

    move-result-object v2

    sget-object v4, Lh9/h;->o:Lh9/h;

    invoke-virtual {v2, v4, v0}, Lc9/a;->o(Lh9/b;Lc9/c;)Lc9/a;

    move-result-object v0

    aput-object v0, v1, v5

    const-string v0, "start"

    invoke-interface {v3, v0, v1}, Lmiuix/animation/h;->s(Ljava/lang/Object;[Lc9/a;)Lmiuix/animation/h;

    iget-object v0, p0, Lmiuix/preference/StretchableWidgetPreference;->P:Landroid/widget/ImageView;

    sget v1, Ljb/a;->b:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v0, p0, Lmiuix/preference/StretchableWidgetPreference;->U:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lmiuix/preference/StretchableWidgetPreference;->V:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_0
    new-instance v0, Lc9/c;

    invoke-direct {v0}, Lc9/c;-><init>()V

    new-array v3, v3, [F

    fill-array-data v3, :array_1

    invoke-virtual {v0, v4, v3}, Lc9/a;->k(I[F)Lc9/a;

    move-result-object v0

    check-cast v0, Lc9/c;

    new-array v3, v1, [Ljava/lang/Object;

    iget-object v4, p0, Lmiuix/preference/StretchableWidgetPreference;->R:Lmiuix/stretchablewidget/WidgetContainer;

    aput-object v4, v3, v5

    invoke-static {v3}, Lmiuix/animation/a;->y([Ljava/lang/Object;)Lmiuix/animation/h;

    move-result-object v3

    new-array v1, v1, [Lc9/a;

    new-instance v4, Lc9/a;

    invoke-direct {v4}, Lc9/a;-><init>()V

    invoke-virtual {v4, v2}, Lc9/a;->m(F)Lc9/a;

    move-result-object v2

    sget-object v4, Lh9/h;->o:Lh9/h;

    invoke-virtual {v2, v4, v0}, Lc9/a;->o(Lh9/b;Lc9/c;)Lc9/a;

    move-result-object v0

    aput-object v0, v1, v5

    const-string v0, "end"

    invoke-interface {v3, v0, v1}, Lmiuix/animation/h;->s(Ljava/lang/Object;[Lc9/a;)Lmiuix/animation/h;

    iget-object v0, p0, Lmiuix/preference/StretchableWidgetPreference;->P:Landroid/widget/ImageView;

    sget v1, Ljb/a;->a:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v0, p0, Lmiuix/preference/StretchableWidgetPreference;->U:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lmiuix/preference/StretchableWidgetPreference;->V:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    iget-object v0, p0, Lmiuix/preference/StretchableWidgetPreference;->Z:Ljb/b;

    if-eqz v0, :cond_1

    iget-boolean v1, p0, Lmiuix/preference/StretchableWidgetPreference;->W:Z

    invoke-interface {v0, v1}, Ljb/b;->a(Z)V

    :cond_1
    return-void

    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x3e4ccccd    # 0.2f
    .end array-data

    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x3e4ccccd    # 0.2f
    .end array-data
.end method


# virtual methods
.method public J0(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lmiuix/preference/StretchableWidgetPreference;->S:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public K0(Z)V
    .locals 2

    iget-object v0, p0, Lmiuix/preference/StretchableWidgetPreference;->P:Landroid/widget/ImageView;

    if-eqz p1, :cond_0

    sget v1, Lva/m;->b:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v0, p0, Lmiuix/preference/StretchableWidgetPreference;->U:Landroid/view/View;

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    sget v1, Lva/m;->a:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v0, p0, Lmiuix/preference/StretchableWidgetPreference;->U:Landroid/view/View;

    const/16 v1, 0x8

    :goto_0
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lmiuix/preference/StretchableWidgetPreference;->V:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0, p1}, Lmiuix/preference/StretchableWidgetPreference;->I0(Z)V

    return-void
.end method

.method public S(Landroidx/preference/PreferenceViewHolder;)V
    .locals 3

    invoke-super {p0, p1}, Landroidx/preference/Preference;->S(Landroidx/preference/PreferenceViewHolder;)V

    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView$d0;->itemView:Landroid/view/View;

    sget v0, Lva/n;->n:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lmiuix/preference/StretchableWidgetPreference;->Q:Landroid/widget/RelativeLayout;

    const v0, 0x1020018

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiuix/stretchablewidget/WidgetContainer;

    iput-object v0, p0, Lmiuix/preference/StretchableWidgetPreference;->R:Lmiuix/stretchablewidget/WidgetContainer;

    const/4 v1, 0x0

    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/widget/LinearLayout;->measure(II)V

    iget-object v0, p0, Lmiuix/preference/StretchableWidgetPreference;->R:Lmiuix/stretchablewidget/WidgetContainer;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v0

    iput v0, p0, Lmiuix/preference/StretchableWidgetPreference;->Y:I

    sget v0, Lva/n;->l:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lmiuix/preference/StretchableWidgetPreference;->T:Landroid/widget/TextView;

    sget v0, Lva/n;->d:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lmiuix/preference/StretchableWidgetPreference;->S:Landroid/widget/TextView;

    sget v0, Lva/n;->j:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lmiuix/preference/StretchableWidgetPreference;->P:Landroid/widget/ImageView;

    sget v1, Lva/m;->a:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    sget v0, Lva/n;->b:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lmiuix/preference/StretchableWidgetPreference;->U:Landroid/view/View;

    sget v0, Lva/n;->m:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lmiuix/preference/StretchableWidgetPreference;->V:Landroid/view/View;

    iget-object p1, p0, Lmiuix/preference/StretchableWidgetPreference;->X:Ljava/lang/String;

    invoke-virtual {p0, p1}, Lmiuix/preference/StretchableWidgetPreference;->J0(Ljava/lang/String;)V

    iget-boolean p1, p0, Lmiuix/preference/StretchableWidgetPreference;->W:Z

    invoke-virtual {p0, p1}, Lmiuix/preference/StretchableWidgetPreference;->K0(Z)V

    iget-object p1, p0, Lmiuix/preference/StretchableWidgetPreference;->Q:Landroid/widget/RelativeLayout;

    new-instance v0, Lmiuix/preference/StretchableWidgetPreference$a;

    invoke-direct {v0, p0}, Lmiuix/preference/StretchableWidgetPreference$a;-><init>(Lmiuix/preference/StretchableWidgetPreference;)V

    invoke-virtual {p1, v0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
