.class Lmiuix/view/LinearVibrator;
.super Ljava/lang/Object;

# interfaces
.implements Lmiuix/view/d;


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "LinearVibrator"


# instance fields
.field private final mIds:Ln/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ln/h<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 0

    invoke-static {}, Lmiuix/view/LinearVibrator;->initialize()V

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ln/h;

    invoke-direct {v0}, Ln/h;-><init>()V

    iput-object v0, p0, Lmiuix/view/LinearVibrator;->mIds:Ln/h;

    invoke-direct {p0}, Lmiuix/view/LinearVibrator;->buildIds()V

    return-void
.end method

.method private buildIds()V
    .locals 4

    iget-object v0, p0, Lmiuix/view/LinearVibrator;->mIds:Ln/h;

    sget v1, Lmiuix/view/c;->f:I

    const/high16 v2, 0x10000000

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ln/h;->a(ILjava/lang/Object;)V

    iget-object v0, p0, Lmiuix/view/LinearVibrator;->mIds:Ln/h;

    sget v1, Lmiuix/view/c;->g:I

    const v2, 0x10000001

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ln/h;->a(ILjava/lang/Object;)V

    iget-object v0, p0, Lmiuix/view/LinearVibrator;->mIds:Ln/h;

    sget v1, Lmiuix/view/c;->h:I

    const v2, 0x10000002

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ln/h;->a(ILjava/lang/Object;)V

    iget-object v0, p0, Lmiuix/view/LinearVibrator;->mIds:Ln/h;

    sget v1, Lmiuix/view/c;->i:I

    const v2, 0x10000003

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ln/h;->a(ILjava/lang/Object;)V

    iget-object v0, p0, Lmiuix/view/LinearVibrator;->mIds:Ln/h;

    sget v1, Lmiuix/view/c;->j:I

    const v2, 0x10000004

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ln/h;->a(ILjava/lang/Object;)V

    iget-object v0, p0, Lmiuix/view/LinearVibrator;->mIds:Ln/h;

    sget v1, Lmiuix/view/c;->k:I

    const v2, 0x10000005

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ln/h;->a(ILjava/lang/Object;)V

    iget-object v0, p0, Lmiuix/view/LinearVibrator;->mIds:Ln/h;

    sget v1, Lmiuix/view/c;->l:I

    const v2, 0x10000006

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ln/h;->a(ILjava/lang/Object;)V

    iget-object v0, p0, Lmiuix/view/LinearVibrator;->mIds:Ln/h;

    sget v1, Lmiuix/view/c;->m:I

    const v2, 0x10000007

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ln/h;->a(ILjava/lang/Object;)V

    iget-object v0, p0, Lmiuix/view/LinearVibrator;->mIds:Ln/h;

    sget v1, Lmiuix/view/c;->n:I

    const v2, 0x10000008

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ln/h;->a(ILjava/lang/Object;)V

    iget-object v0, p0, Lmiuix/view/LinearVibrator;->mIds:Ln/h;

    sget v1, Lmiuix/view/c;->o:I

    const v2, 0x10000009

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ln/h;->a(ILjava/lang/Object;)V

    sget v0, Lmiuix/view/PlatformConstants;->VERSION:I

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Lmiuix/view/LinearVibrator;->mIds:Ln/h;

    sget v2, Lmiuix/view/c;->p:I

    const v3, 0x1000000a

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ln/h;->a(ILjava/lang/Object;)V

    iget-object v1, p0, Lmiuix/view/LinearVibrator;->mIds:Ln/h;

    sget v2, Lmiuix/view/c;->q:I

    const v3, 0x1000000b

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ln/h;->a(ILjava/lang/Object;)V

    iget-object v1, p0, Lmiuix/view/LinearVibrator;->mIds:Ln/h;

    sget v2, Lmiuix/view/c;->r:I

    const v3, 0x1000000c

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ln/h;->a(ILjava/lang/Object;)V

    const/4 v1, 0x3

    if-ge v0, v1, :cond_1

    return-void

    :cond_1
    iget-object v1, p0, Lmiuix/view/LinearVibrator;->mIds:Ln/h;

    sget v2, Lmiuix/view/c;->s:I

    const v3, 0x1000000d

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ln/h;->a(ILjava/lang/Object;)V

    const/4 v1, 0x4

    if-ge v0, v1, :cond_2

    return-void

    :cond_2
    iget-object v1, p0, Lmiuix/view/LinearVibrator;->mIds:Ln/h;

    sget v2, Lmiuix/view/c;->t:I

    const v3, 0x1000000e

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ln/h;->a(ILjava/lang/Object;)V

    const/4 v1, 0x5

    if-ge v0, v1, :cond_3

    return-void

    :cond_3
    iget-object v0, p0, Lmiuix/view/LinearVibrator;->mIds:Ln/h;

    sget v1, Lmiuix/view/c;->w:I

    const v2, 0x1000000f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ln/h;->a(ILjava/lang/Object;)V

    iget-object v0, p0, Lmiuix/view/LinearVibrator;->mIds:Ln/h;

    sget v1, Lmiuix/view/c;->x:I

    const v2, 0x10000010

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ln/h;->a(ILjava/lang/Object;)V

    iget-object v0, p0, Lmiuix/view/LinearVibrator;->mIds:Ln/h;

    sget v1, Lmiuix/view/c;->y:I

    const v2, 0x10000011

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ln/h;->a(ILjava/lang/Object;)V

    iget-object v0, p0, Lmiuix/view/LinearVibrator;->mIds:Ln/h;

    sget v1, Lmiuix/view/c;->z:I

    const v2, 0x10000012

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ln/h;->a(ILjava/lang/Object;)V

    iget-object v0, p0, Lmiuix/view/LinearVibrator;->mIds:Ln/h;

    sget v1, Lmiuix/view/c;->A:I

    const v2, 0x10000013

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ln/h;->a(ILjava/lang/Object;)V

    iget-object v0, p0, Lmiuix/view/LinearVibrator;->mIds:Ln/h;

    sget v1, Lmiuix/view/c;->B:I

    const v2, 0x10000014

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ln/h;->a(ILjava/lang/Object;)V

    iget-object v0, p0, Lmiuix/view/LinearVibrator;->mIds:Ln/h;

    sget v1, Lmiuix/view/c;->C:I

    const v2, 0x10000015

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ln/h;->a(ILjava/lang/Object;)V

    iget-object v0, p0, Lmiuix/view/LinearVibrator;->mIds:Ln/h;

    sget v1, Lmiuix/view/c;->D:I

    const v2, 0x10000016

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ln/h;->a(ILjava/lang/Object;)V

    iget-object v0, p0, Lmiuix/view/LinearVibrator;->mIds:Ln/h;

    sget v1, Lmiuix/view/c;->E:I

    const v2, 0x10000017

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ln/h;->a(ILjava/lang/Object;)V

    iget-object v0, p0, Lmiuix/view/LinearVibrator;->mIds:Ln/h;

    sget v1, Lmiuix/view/c;->F:I

    const v2, 0x10000018

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ln/h;->a(ILjava/lang/Object;)V

    return-void
.end method

.method private static initialize()V
    .locals 3

    sget v0, Lmiuix/view/PlatformConstants;->VERSION:I

    const-string v1, "LinearVibrator"

    const/4 v2, 0x1

    if-ge v0, v2, :cond_0

    const-string v0, "MiuiHapticFeedbackConstants not found or not compatible for LinearVibrator."

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    :try_start_0
    invoke-static {}, Lmiui/util/HapticFeedbackUtil;->isSupportLinearMotorVibrate()Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    const-string v2, "MIUI Haptic Implementation is not available"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_1

    const-string v0, "linear motor is not supported in this platform."

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_1
    new-instance v0, Lmiuix/view/LinearVibrator;

    invoke-direct {v0}, Lmiuix/view/LinearVibrator;-><init>()V

    invoke-static {v0}, Lmiuix/view/HapticCompat;->registerProvider(Lmiuix/view/d;)V

    const-string v0, "setup LinearVibrator success."

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method


# virtual methods
.method obtainFeedBack(I)I
    .locals 1

    goto/32 :goto_3

    nop

    :goto_0
    check-cast p1, Ljava/lang/Integer;

    goto/32 :goto_4

    nop

    :goto_1
    return p1

    :goto_2
    iget-object v0, p0, Lmiuix/view/LinearVibrator;->mIds:Ln/h;

    goto/32 :goto_8

    nop

    :goto_3
    iget-object v0, p0, Lmiuix/view/LinearVibrator;->mIds:Ln/h;

    goto/32 :goto_7

    nop

    :goto_4
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    goto/32 :goto_5

    nop

    :goto_5
    return p1

    :goto_6
    goto/32 :goto_9

    nop

    :goto_7
    invoke-virtual {v0, p1}, Ln/h;->h(I)I

    move-result p1

    goto/32 :goto_a

    nop

    :goto_8
    invoke-virtual {v0, p1}, Ln/h;->m(I)Ljava/lang/Object;

    move-result-object p1

    goto/32 :goto_0

    nop

    :goto_9
    const/4 p1, -0x1

    goto/32 :goto_1

    nop

    :goto_a
    if-gez p1, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_2

    nop
.end method

.method public performHapticFeedback(Landroid/view/View;I)Z
    .locals 5

    iget-object v0, p0, Lmiuix/view/LinearVibrator;->mIds:Ln/h;

    invoke-virtual {v0, p2}, Ln/h;->h(I)I

    move-result v0

    const/4 v1, 0x2

    const/4 v2, 0x1

    const-string v3, "LinearVibrator"

    const/4 v4, 0x0

    if-gez v0, :cond_0

    const/4 p1, 0x3

    new-array p1, p1, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, p1, v4

    invoke-static {p2}, Lmiuix/view/c;->b(I)Ljava/lang/String;

    move-result-object p2

    aput-object p2, p1, v2

    sget p2, Lmiuix/view/PlatformConstants;->VERSION:I

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, p1, v1

    const-string p2, "feedback(0x%08x-%s) is not found in current platform(v%d)"

    invoke-static {p2, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    :goto_0
    invoke-static {v3, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return v4

    :cond_0
    iget-object p2, p0, Lmiuix/view/LinearVibrator;->mIds:Ln/h;

    invoke-virtual {p2, v0}, Ln/h;->m(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p2

    invoke-static {p2}, Lmiui/util/HapticFeedbackUtil;->isSupportLinearMotorVibrate(I)Z

    move-result v0

    if-nez v0, :cond_1

    new-array p1, v1, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, p1, v4

    sget p2, Lmiuix/view/PlatformConstants;->VERSION:I

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, p1, v2

    const-string p2, "unsupported feedback: 0x%08x. platform version: %d"

    invoke-static {p2, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_1
    invoke-virtual {p1, p2}, Landroid/view/View;->performHapticFeedback(I)Z

    move-result p1

    return p1
.end method

.method public supportLinearMotor(I)Z
    .locals 6

    iget-object v0, p0, Lmiuix/view/LinearVibrator;->mIds:Ln/h;

    invoke-virtual {v0, p1}, Ln/h;->h(I)I

    move-result v0

    const/4 v1, 0x2

    const/4 v2, 0x1

    const-string v3, "LinearVibrator"

    const/4 v4, 0x0

    if-gez v0, :cond_0

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v0, v4

    invoke-static {p1}, Lmiuix/view/c;->b(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v2

    sget p1, Lmiuix/view/PlatformConstants;->VERSION:I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v1

    const-string p1, "feedback(0x%08x-%s) is not found in current platform(v%d)"

    invoke-static {p1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    :goto_0
    invoke-static {v3, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return v4

    :cond_0
    iget-object p1, p0, Lmiuix/view/LinearVibrator;->mIds:Ln/h;

    invoke-virtual {p1, v0}, Ln/h;->m(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-static {p1}, Lmiui/util/HapticFeedbackUtil;->isSupportLinearMotorVibrate(I)Z

    move-result v0

    if-nez v0, :cond_1

    new-array v0, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v4

    sget p1, Lmiuix/view/PlatformConstants;->VERSION:I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v2

    const-string p1, "unsupported feedback: 0x%08x. platform version: %d"

    invoke-static {p1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_1
    invoke-static {p1}, Lmiui/util/HapticFeedbackUtil;->isSupportLinearMotorVibrate(I)Z

    move-result p1

    return p1
.end method
