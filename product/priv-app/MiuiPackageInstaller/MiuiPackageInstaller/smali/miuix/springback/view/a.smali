.class public Lmiuix/springback/view/a;
.super Ljava/lang/Object;


# instance fields
.field private a:I

.field b:F

.field c:F

.field d:I

.field e:I

.field f:I

.field private g:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lmiuix/springback/view/a;->d:I

    iput-object p1, p0, Lmiuix/springback/view/a;->g:Landroid/view/ViewGroup;

    iput p2, p0, Lmiuix/springback/view/a;->f:I

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result p1

    iput p1, p0, Lmiuix/springback/view/a;->a:I

    return-void
.end method


# virtual methods
.method a(Landroid/view/MotionEvent;)V
    .locals 5

    goto/32 :goto_30

    nop

    :goto_0
    const/4 v1, -0x1

    goto/32 :goto_26

    nop

    :goto_1
    const/4 p1, 0x3

    goto/32 :goto_21

    nop

    :goto_2
    if-gtz v0, :cond_0

    goto/32 :goto_2f

    :cond_0
    :goto_3
    goto/32 :goto_32

    nop

    :goto_4
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    goto/32 :goto_11

    nop

    :goto_5
    iget-object p1, p0, Lmiuix/springback/view/a;->g:Landroid/view/ViewGroup;

    goto/32 :goto_a

    nop

    :goto_6
    if-lez v0, :cond_1

    goto/32 :goto_3

    :cond_1
    goto/32 :goto_12

    nop

    :goto_7
    iput v2, p0, Lmiuix/springback/view/a;->e:I

    goto/32 :goto_39

    nop

    :goto_8
    if-ne v0, v3, :cond_2

    goto/32 :goto_24

    :cond_2
    goto/32 :goto_1

    nop

    :goto_9
    iget v0, p0, Lmiuix/springback/view/a;->c:F

    goto/32 :goto_10

    nop

    :goto_a
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->requestDisallowInterceptTouchEvent(Z)V

    goto/32 :goto_40

    nop

    :goto_b
    move v2, v3

    :goto_c
    goto/32 :goto_7

    nop

    :goto_d
    iput v0, p0, Lmiuix/springback/view/a;->d:I

    goto/32 :goto_1f

    nop

    :goto_e
    cmpl-float p1, p1, v0

    goto/32 :goto_3c

    nop

    :goto_f
    int-to-float v4, v4

    goto/32 :goto_13

    nop

    :goto_10
    sub-float/2addr p1, v0

    goto/32 :goto_20

    nop

    :goto_11
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result p1

    goto/32 :goto_16

    nop

    :goto_12
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v0

    goto/32 :goto_2d

    nop

    :goto_13
    cmpl-float v0, v0, v4

    goto/32 :goto_2

    nop

    :goto_14
    iput v1, p0, Lmiuix/springback/view/a;->e:I

    goto/32 :goto_5

    nop

    :goto_15
    const/4 v2, 0x1

    goto/32 :goto_36

    nop

    :goto_16
    iget v0, p0, Lmiuix/springback/view/a;->b:F

    goto/32 :goto_2a

    nop

    :goto_17
    iget v4, p0, Lmiuix/springback/view/a;->a:I

    goto/32 :goto_29

    nop

    :goto_18
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    goto/32 :goto_d

    nop

    :goto_19
    goto :goto_c

    :goto_1a
    goto/32 :goto_b

    nop

    :goto_1b
    return-void

    :goto_1c
    goto/32 :goto_34

    nop

    :goto_1d
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result p1

    goto/32 :goto_37

    nop

    :goto_1e
    const/4 v3, 0x2

    goto/32 :goto_8

    nop

    :goto_1f
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v0

    goto/32 :goto_35

    nop

    :goto_20
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v0

    goto/32 :goto_17

    nop

    :goto_21
    if-ne v0, p1, :cond_3

    goto/32 :goto_3a

    :cond_3
    goto/32 :goto_23

    nop

    :goto_22
    iput v2, p0, Lmiuix/springback/view/a;->b:F

    goto/32 :goto_1d

    nop

    :goto_23
    goto :goto_2f

    :goto_24
    goto/32 :goto_3d

    nop

    :goto_25
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v0

    goto/32 :goto_31

    nop

    :goto_26
    if-eq v0, v1, :cond_4

    goto/32 :goto_3f

    :cond_4
    goto/32 :goto_3e

    nop

    :goto_27
    return-void

    :goto_28
    goto/32 :goto_4

    nop

    :goto_29
    int-to-float v4, v4

    goto/32 :goto_38

    nop

    :goto_2a
    sub-float/2addr v1, v0

    goto/32 :goto_9

    nop

    :goto_2b
    if-nez v0, :cond_5

    goto/32 :goto_41

    :cond_5
    goto/32 :goto_15

    nop

    :goto_2c
    return-void

    :goto_2d
    iget v4, p0, Lmiuix/springback/view/a;->a:I

    goto/32 :goto_f

    nop

    :goto_2e
    iput v1, p0, Lmiuix/springback/view/a;->e:I

    :goto_2f
    goto/32 :goto_2c

    nop

    :goto_30
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    goto/32 :goto_33

    nop

    :goto_31
    if-ltz v0, :cond_6

    goto/32 :goto_28

    :cond_6
    goto/32 :goto_27

    nop

    :goto_32
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result p1

    goto/32 :goto_3b

    nop

    :goto_33
    const/4 v1, 0x0

    goto/32 :goto_2b

    nop

    :goto_34
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    goto/32 :goto_22

    nop

    :goto_35
    if-ltz v0, :cond_7

    goto/32 :goto_1c

    :cond_7
    goto/32 :goto_1b

    nop

    :goto_36
    if-ne v0, v2, :cond_8

    goto/32 :goto_3a

    :cond_8
    goto/32 :goto_1e

    nop

    :goto_37
    iput p1, p0, Lmiuix/springback/view/a;->c:F

    goto/32 :goto_2e

    nop

    :goto_38
    cmpl-float v0, v0, v4

    goto/32 :goto_6

    nop

    :goto_39
    goto :goto_2f

    :goto_3a
    goto/32 :goto_14

    nop

    :goto_3b
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v0

    goto/32 :goto_e

    nop

    :goto_3c
    if-gtz p1, :cond_9

    goto/32 :goto_1a

    :cond_9
    goto/32 :goto_19

    nop

    :goto_3d
    iget v0, p0, Lmiuix/springback/view/a;->d:I

    goto/32 :goto_0

    nop

    :goto_3e
    return-void

    :goto_3f
    goto/32 :goto_25

    nop

    :goto_40
    goto :goto_2f

    :goto_41
    goto/32 :goto_18

    nop
.end method

.method public b(Landroid/view/MotionEvent;)Z
    .locals 6

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v1

    if-ltz v1, :cond_0

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result p1

    const/4 v1, 0x2

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    iget-object v3, p0, Lmiuix/springback/view/a;->g:Landroid/view/ViewGroup;

    invoke-virtual {v3, v1}, Landroid/view/ViewGroup;->getLocationInWindow([I)V

    aget v0, v1, v0

    const/4 v3, 0x1

    aget v1, v1, v3

    iget-object v3, p0, Lmiuix/springback/view/a;->g:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getHeight()I

    move-result v3

    add-int/2addr v3, v1

    iget-object v4, p0, Lmiuix/springback/view/a;->g:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getWidth()I

    move-result v4

    add-int/2addr v4, v0

    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5, v0, v1, v4, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    float-to-int p1, p1

    float-to-int v0, v2

    invoke-virtual {v5, p1, v0}, Landroid/graphics/Rect;->contains(II)Z

    move-result p1

    return p1

    :cond_0
    return v0

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method
