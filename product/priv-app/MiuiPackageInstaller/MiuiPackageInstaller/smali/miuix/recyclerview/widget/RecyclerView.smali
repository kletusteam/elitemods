.class public Lmiuix/recyclerview/widget/RecyclerView;
.super Landroidx/recyclerview/widget/SpringRecyclerView;


# instance fields
.field private final X0:Lab/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    sget v0, Lq0/a;->a:I

    invoke-direct {p0, p1, p2, v0}, Lmiuix/recyclerview/widget/RecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Landroidx/recyclerview/widget/SpringRecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance p1, Lbb/b;

    invoke-direct {p1}, Lbb/b;-><init>()V

    invoke-virtual {p0, p1}, Landroidx/recyclerview/widget/RecyclerView;->setItemAnimator(Landroidx/recyclerview/widget/RecyclerView$l;)V

    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 p2, 0x1e

    if-le p1, p2, :cond_0

    new-instance p1, Lab/a;

    invoke-direct {p1, p0}, Lab/a;-><init>(Lmiuix/recyclerview/widget/RecyclerView;)V

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput-object p1, p0, Lmiuix/recyclerview/widget/RecyclerView;->X0:Lab/a;

    return-void
.end method


# virtual methods
.method public L0(I)V
    .locals 1

    invoke-super {p0, p1}, Landroidx/recyclerview/widget/SpringRecyclerView;->L0(I)V

    iget-object v0, p0, Lmiuix/recyclerview/widget/RecyclerView;->X0:Lab/a;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p0, p1}, Lab/a;->e(Lmiuix/recyclerview/widget/RecyclerView;I)V

    :cond_0
    return-void
.end method

.method public M0(II)V
    .locals 1

    iget-object v0, p0, Lmiuix/recyclerview/widget/RecyclerView;->X0:Lab/a;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1, p2}, Lab/a;->b(II)V

    :cond_0
    invoke-super {p0, p1, p2}, Landroidx/recyclerview/widget/RecyclerView;->M0(II)V

    return-void
.end method

.method public a0(II)Z
    .locals 3

    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v0

    const/16 v1, 0x12c

    const/4 v2, 0x0

    if-ge v0, v1, :cond_0

    move p1, v2

    :cond_0
    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result v0

    if-ge v0, v1, :cond_1

    move p2, v2

    :cond_1
    if-nez p1, :cond_2

    if-nez p2, :cond_2

    return v2

    :cond_2
    invoke-super {p0, p1, p2}, Landroidx/recyclerview/widget/RecyclerView;->a0(II)Z

    move-result p1

    return p1
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    iget-object v0, p0, Lmiuix/recyclerview/widget/RecyclerView;->X0:Lab/a;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lab/a;->g(Landroid/view/MotionEvent;)V

    :cond_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1
.end method

.method protected onFocusChanged(ZILandroid/graphics/Rect;)V
    .locals 0

    invoke-super {p0, p1, p2, p3}, Landroid/view/ViewGroup;->onFocusChanged(ZILandroid/graphics/Rect;)V

    iget-object p2, p0, Lmiuix/recyclerview/widget/RecyclerView;->X0:Lab/a;

    if-eqz p2, :cond_0

    invoke-virtual {p2, p1}, Lab/a;->d(Z)V

    :cond_0
    return-void
.end method
