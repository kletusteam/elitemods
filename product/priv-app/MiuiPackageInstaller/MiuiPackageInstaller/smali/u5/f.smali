.class public final Lu5/f;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lu5/f$b;
    }
.end annotation


# static fields
.field public static final c:Lu5/f$b;

.field private static final d:La8/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "La8/f<",
            "Lu5/f;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Z

.field private b:Lo1/d;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lu5/f$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lu5/f$b;-><init>(Lm8/g;)V

    sput-object v0, Lu5/f;->c:Lu5/f$b;

    sget-object v0, La8/j;->a:La8/j;

    sget-object v1, Lu5/f$a;->b:Lu5/f$a;

    invoke-static {v0, v1}, La8/g;->a(La8/j;Ll8/a;)La8/f;

    move-result-object v0

    sput-object v0, Lu5/f;->d:La8/f;

    return-void
.end method

.method private constructor <init>()V
    .locals 8

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lu5/f;->a:Z

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "httpdns shouldUseHttpDns:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lu5/f;->e()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "HttpDnsManager"

    invoke-static {v2, v1}, Lf6/o;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    invoke-virtual {p0}, Lu5/f;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/android/packageinstaller/InstallerApplication;->g()Lcom/android/packageinstaller/InstallerApplication;

    move-result-object v1

    const-string v2, "164566"

    const-string v3, "747b348fa62303a76c22777a182f8cad"

    invoke-static {v1, v2, v3}, Lo1/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lo1/d;

    move-result-object v1

    iput-object v1, p0, Lu5/f;->b:Lo1/d;

    if-eqz v1, :cond_1

    sget-boolean v2, Lq2/c;->c:Z

    invoke-interface {v1, v2}, Lo1/d;->h(Z)V

    sget-object v2, Lu5/e;->a:Lu5/e;

    invoke-interface {v1, v2}, Lo1/d;->a(Lo1/f;)V

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lo1/d;->d(Z)V

    invoke-interface {v1, v0}, Lo1/d;->c(Z)V

    invoke-interface {v1, v0, v2}, Lo1/d;->e(ZZ)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sget-object v3, Lcom/android/packageinstaller/utils/e;->a:[Ljava/lang/String;

    const-string v4, "HOST_NAME_ARRAY"

    invoke-static {v3, v4}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    array-length v4, v3

    :goto_0
    if-ge v2, v4, :cond_0

    aget-object v5, v3, v2

    new-instance v6, Lb2/a;

    const/16 v7, 0x1bb

    invoke-direct {v6, v5, v7}, Lb2/a;-><init>(Ljava/lang/String;I)V

    invoke-static {v0, v6}, Lb8/j;->z(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    invoke-interface {v1, v0}, Lo1/d;->b(Ljava/util/List;)V

    :cond_1
    return-void
.end method

.method public synthetic constructor <init>(Lm8/g;)V
    .locals 0

    invoke-direct {p0}, Lu5/f;-><init>()V

    return-void
.end method

.method public static synthetic a(Ljava/lang/String;)V
    .locals 0

    invoke-static {p0}, Lu5/f;->b(Ljava/lang/String;)V

    return-void
.end method

.method private static final b(Ljava/lang/String;)V
    .locals 1

    const-string v0, "HttpDnsManager"

    invoke-static {v0, p0}, Lf6/o;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    return-void
.end method

.method public static final synthetic c()La8/f;
    .locals 1

    sget-object v0, Lu5/f;->d:La8/f;

    return-object v0
.end method


# virtual methods
.method public d(Ljava/lang/String;)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Ljava/net/InetAddress;",
            ">;"
        }
    .end annotation

    const-string v0, "hostname"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sget-object v1, Lu5/f;->c:Lu5/f$b;

    invoke-virtual {v1}, Lu5/f$b;->a()Lu5/f;

    move-result-object v1

    iget-object v1, v1, Lu5/f;->b:Lo1/d;

    if-eqz v1, :cond_0

    invoke-interface {v1, p1}, Lo1/d;->f(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v1, :cond_3

    array-length v4, v1

    if-nez v4, :cond_1

    move v4, v3

    goto :goto_1

    :cond_1
    move v4, v2

    :goto_1
    if-eqz v4, :cond_2

    goto :goto_2

    :cond_2
    move v4, v2

    goto :goto_3

    :cond_3
    :goto_2
    move v4, v3

    :goto_3
    if-eqz v4, :cond_4

    return-object v0

    :cond_4
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    array-length v5, v1

    move v6, v2

    :goto_4
    if-ge v6, v5, :cond_7

    aget-object v7, v1, v6

    invoke-static {v7}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v8

    if-eqz v8, :cond_5

    move v8, v3

    goto :goto_5

    :cond_5
    move v8, v2

    :goto_5
    if-eqz v8, :cond_6

    invoke-interface {v4, v7}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    :cond_6
    add-int/lit8 v6, v6, 0x1

    goto :goto_4

    :cond_7
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_6
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v3

    invoke-virtual {v3}, Ljava/net/InetAddress;->getAddress()[B

    move-result-object v3

    invoke-static {p1, v3}, Ljava/net/InetAddress;->getByAddress(Ljava/lang/String;[B)Ljava/net/InetAddress;

    move-result-object v3

    const-string v4, "getByAddress(hostname, addr)"

    invoke-static {v3, v4}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {v1, v0}, Lb8/j;->r(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    goto :goto_6

    :cond_8
    return-object v1
.end method

.method public e()Z
    .locals 1

    iget-boolean v0, p0, Lu5/f;->a:Z

    return v0
.end method
