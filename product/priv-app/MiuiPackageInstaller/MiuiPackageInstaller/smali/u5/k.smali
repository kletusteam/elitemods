.class public final Lu5/k;
.super Ljava/lang/Object;


# static fields
.field public static final a:Lu5/k;

.field private static final b:Ljava/lang/String;

.field private static final c:J

.field private static final d:J

.field private static final e:J

.field private static final f:Ljava/lang/String;

.field private static final g:Ljava/lang/String;

.field private static final h:Ljava/lang/String;

.field private static final i:Ljava/lang/String;

.field private static final j:Ljava/lang/String;

.field private static final k:Ljava/lang/String;

.field private static final l:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lpb/y;",
            ">;"
        }
    .end annotation
.end field

.field private static final n:Lpb/j;

.field private static o:Ljavax/net/ssl/X509TrustManager;

.field private static p:Ljavax/net/ssl/SSLSocketFactory;

.field private static final q:Lpb/n;

.field private static final r:Lu5/d;

.field private static s:Lgc/u;

.field private static final t:Ljava/util/Random;


# direct methods
.method static constructor <clinit>()V
    .locals 11

    new-instance v0, Lu5/k;

    invoke-direct {v0}, Lu5/k;-><init>()V

    sput-object v0, Lu5/k;->a:Lu5/k;

    const-string v0, "RetrofitUtils"

    sput-object v0, Lu5/k;->b:Ljava/lang/String;

    const-wide/16 v0, 0x14

    sput-wide v0, Lu5/k;->c:J

    sput-wide v0, Lu5/k;->d:J

    sput-wide v0, Lu5/k;->e:J

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->c()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lu5/k;->f:Ljava/lang/String;

    const-string v1, "ro.carrier.name"

    const-string v2, "unknown"

    invoke-static {v1, v2}, Lcom/android/packageinstaller/compat/SystemPropertiesCompat;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lu5/k;->g:Ljava/lang/String;

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->k()Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lu5/k;->h:Ljava/lang/String;

    sget-object v4, Landroid/os/Build$VERSION;->INCREMENTAL:Ljava/lang/String;

    sput-object v4, Lu5/k;->i:Ljava/lang/String;

    const-string v5, "ro.miui.ui.version.name"

    invoke-static {v5, v2}, Lcom/android/packageinstaller/compat/SystemPropertiesCompat;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lu5/k;->j:Ljava/lang/String;

    sget-object v5, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    sput-object v5, Lu5/k;->k:Ljava/lang/String;

    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    sput-object v6, Lu5/k;->l:Ljava/util/HashMap;

    const/4 v7, 0x2

    new-array v7, v7, [Lpb/y;

    sget-object v8, Lpb/y;->e:Lpb/y;

    const/4 v9, 0x0

    aput-object v8, v7, v9

    sget-object v8, Lpb/y;->c:Lpb/y;

    const/4 v10, 0x1

    aput-object v8, v7, v10

    invoke-static {v7}, Lb8/j;->h([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v7

    sput-object v7, Lu5/k;->m:Ljava/util/List;

    new-instance v7, Lpb/j;

    invoke-direct {v7}, Lpb/j;-><init>()V

    sput-object v7, Lu5/k;->n:Lpb/j;

    new-instance v7, Lpb/n;

    invoke-direct {v7}, Lpb/n;-><init>()V

    sput-object v7, Lu5/k;->q:Lpb/n;

    new-instance v7, Lu5/d;

    invoke-direct {v7}, Lu5/d;-><init>()V

    sput-object v7, Lu5/k;->r:Lu5/d;

    new-instance v7, Ljava/util/Random;

    invoke-direct {v7}, Ljava/util/Random;-><init>()V

    sput-object v7, Lu5/k;->t:Ljava/util/Random;

    const-string v7, "DEVICE"

    invoke-static {v0, v7}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v7, "d"

    invoke-interface {v6, v7, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "CARRIER"

    invoke-static {v1, v0}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "c"

    invoke-interface {v6, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "REGION"

    invoke-static {v3, v0}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "r"

    invoke-interface {v6, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "MIUI_VERSION"

    invoke-static {v4, v0}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "v"

    invoke-interface {v6, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->h()Ljava/lang/String;

    move-result-object v0

    const-string v1, "getMiuiVersionType()"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "t"

    invoke-interface {v6, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mo"

    invoke-interface {v6, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Lcom/android/packageinstaller/InstallerApplication;->g()Lcom/android/packageinstaller/InstallerApplication;

    move-result-object v0

    const-string v1, "getInstance()"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x1c

    if-gt v1, v3, :cond_0

    invoke-static {v0}, Lcom/android/packageinstaller/utils/g;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "imeiMd5"

    invoke-static {v1, v3}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "m"

    invoke-interface {v6, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-static {}, Lcom/android/packageinstaller/utils/g;->x()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    const-string v3, "ri"

    invoke-interface {v6, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "MIUI_VERSION_NAME"

    invoke-static {v2, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "vn"

    invoke-interface {v6, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "ANDROID_VERSION"

    invoke-static {v5, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "av"

    invoke-interface {v6, v1, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v0}, Lcom/android/packageinstaller/utils/g;->j(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "getOAID(context)"

    invoke-static {v1, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "oa"

    invoke-interface {v6, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0}, Landroid/app/Application;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    :try_start_0
    invoke-virtual {v0}, Landroid/app/Application;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v9}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    const-string v2, "e"

    iget v1, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v6, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    :goto_0
    sget-object v1, Lu5/k;->l:Ljava/util/HashMap;

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->b()Ljava/lang/String;

    move-result-object v2

    const-string v3, "getCpuArchitecture()"

    invoke-static {v2, v3}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "ca"

    invoke-interface {v1, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "getDefault().toString()"

    invoke-static {v2, v3}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "l"

    invoke-interface {v1, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v0}, Lcom/android/packageinstaller/utils/g;->i(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "getNetworkType(context)"

    invoke-static {v0, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "n"

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :try_start_1
    sget-object v0, Lyb/h;->c:Lyb/h$a;

    invoke-virtual {v0}, Lyb/h$a;->e()Lyb/h;

    move-result-object v1

    invoke-virtual {v1}, Lyb/h;->p()Ljavax/net/ssl/X509TrustManager;

    move-result-object v1

    sput-object v1, Lu5/k;->o:Ljavax/net/ssl/X509TrustManager;

    invoke-virtual {v0}, Lyb/h$a;->e()Lyb/h;

    move-result-object v0

    invoke-virtual {v0}, Lyb/h;->o()Ljavax/net/ssl/SSLContext;

    move-result-object v0

    new-array v1, v10, [Ljavax/net/ssl/X509TrustManager;

    sget-object v2, Lu5/k;->o:Ljavax/net/ssl/X509TrustManager;

    aput-object v2, v1, v9

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1, v2}, Ljavax/net/ssl/SSLContext;->init([Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;Ljava/security/SecureRandom;)V

    invoke-virtual {v0}, Ljavax/net/ssl/SSLContext;->getSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v0

    sput-object v0, Lu5/k;->p:Ljavax/net/ssl/SSLSocketFactory;
    :try_end_1
    .catch Ljava/security/KeyManagementException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    sget-object v1, Lu5/k;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "RetrofitUtils KeyManagementException "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lf6/o;->g(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    :goto_1
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final synthetic a()Ljava/lang/String;
    .locals 1

    sget-object v0, Lu5/k;->b:Ljava/lang/String;

    return-object v0
.end method

.method public static final b(Ljava/lang/Runnable;)V
    .locals 4

    const-string v0, "nextAction"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lu5/k;->a:Lu5/k;

    invoke-direct {v0}, Lu5/k;->c()Lpb/x$a;

    move-result-object v0

    sget-object v1, Lpb/p;->a:Lpb/p;

    invoke-static {v1}, Lrb/b;->e(Lpb/p;)Lpb/p$c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lpb/x$a;->h(Lpb/p$c;)Lpb/x$a;

    move-result-object v0

    invoke-virtual {v0}, Lpb/x$a;->b()Lpb/x;

    move-result-object v0

    sget-object v1, Lj2/e;->a:Ljava/lang/String;

    new-instance v2, Lpb/z$a;

    invoke-direct {v2}, Lpb/z$a;-><init>()V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "/favicon.ico"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lpb/z$a;->j(Ljava/lang/String;)Lpb/z$a;

    move-result-object v1

    invoke-virtual {v1}, Lpb/z$a;->c()Lpb/z$a;

    move-result-object v1

    invoke-virtual {v1}, Lpb/z$a;->b()Lpb/z;

    move-result-object v1

    invoke-virtual {v0, v1}, Lpb/x;->a(Lpb/z;)Lpb/e;

    move-result-object v0

    new-instance v1, Lu5/k$a;

    invoke-direct {v1, p0}, Lu5/k$a;-><init>(Ljava/lang/Runnable;)V

    invoke-interface {v0, v1}, Lpb/e;->b(Lpb/f;)V

    return-void
.end method

.method private final c()Lpb/x$a;
    .locals 6

    const/4 v0, 0x2

    new-array v0, v0, [Lpb/k;

    sget-object v1, Lpb/k;->i:Lpb/k;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lpb/k;->j:Lpb/k;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {v0}, Lb8/j;->h([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    new-instance v1, Lpb/x$a;

    invoke-direct {v1}, Lpb/x$a;-><init>()V

    sget-object v3, Lu5/k;->r:Lu5/d;

    invoke-virtual {v1, v3}, Lpb/x$a;->g(Lpb/o;)Lpb/x$a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lpb/x$a;->e(Ljava/util/List;)Lpb/x$a;

    move-result-object v0

    sget-object v1, Lu5/k;->m:Ljava/util/List;

    invoke-virtual {v0, v1}, Lpb/x$a;->L(Ljava/util/List;)Lpb/x$a;

    move-result-object v0

    sget-object v1, Lu5/k;->n:Lpb/j;

    invoke-virtual {v0, v1}, Lpb/x$a;->d(Lpb/j;)Lpb/x$a;

    move-result-object v0

    sget-object v1, Lu5/k;->q:Lpb/n;

    invoke-virtual {v0, v1}, Lpb/x$a;->f(Lpb/n;)Lpb/x$a;

    move-result-object v0

    new-instance v1, Lu5/c;

    invoke-direct {v1}, Lu5/c;-><init>()V

    invoke-virtual {v0, v1}, Lpb/x$a;->a(Lpb/u;)Lpb/x$a;

    move-result-object v0

    sget-object v1, Lqb/a;->a:Lqb/a;

    invoke-virtual {v0, v1}, Lpb/x$a;->a(Lpb/u;)Lpb/x$a;

    move-result-object v0

    new-instance v1, Lu5/i;

    invoke-direct {v1}, Lu5/i;-><init>()V

    invoke-virtual {v0, v1}, Lpb/x$a;->a(Lpb/u;)Lpb/x$a;

    move-result-object v0

    new-instance v1, Lu5/m;

    invoke-direct {v1}, Lu5/m;-><init>()V

    invoke-virtual {v0, v1}, Lpb/x$a;->a(Lpb/u;)Lpb/x$a;

    move-result-object v0

    new-instance v1, Lu5/n;

    invoke-direct {v1}, Lu5/n;-><init>()V

    invoke-virtual {v0, v1}, Lpb/x$a;->a(Lpb/u;)Lpb/x$a;

    move-result-object v0

    new-instance v1, Lu5/o;

    invoke-direct {v1}, Lu5/o;-><init>()V

    invoke-virtual {v0, v1}, Lpb/x$a;->h(Lpb/p$c;)Lpb/x$a;

    move-result-object v0

    invoke-virtual {v0, v2}, Lpb/x$a;->N(Z)Lpb/x$a;

    move-result-object v0

    sget-object v1, Lu5/k;->p:Ljavax/net/ssl/SSLSocketFactory;

    if-eqz v1, :cond_0

    sget-object v2, Lu5/k;->o:Ljavax/net/ssl/X509TrustManager;

    if-eqz v2, :cond_0

    invoke-static {v1}, Lm8/i;->c(Ljava/lang/Object;)V

    sget-object v2, Lu5/k;->o:Ljavax/net/ssl/X509TrustManager;

    invoke-static {v2}, Lm8/i;->c(Ljava/lang/Object;)V

    invoke-virtual {v0, v1, v2}, Lpb/x$a;->O(Ljavax/net/ssl/SSLSocketFactory;Ljavax/net/ssl/X509TrustManager;)Lpb/x$a;

    :cond_0
    sget-boolean v1, Lq2/c;->c:Z

    if-eqz v1, :cond_1

    new-instance v1, Lcc/a;

    new-instance v2, Lu5/g;

    invoke-direct {v2}, Lu5/g;-><init>()V

    invoke-direct {v1, v2}, Lcc/a;-><init>(Lcc/a$b;)V

    sget-object v2, Lcc/a$a;->d:Lcc/a$a;

    invoke-virtual {v1, v2}, Lcc/a;->c(Lcc/a$a;)V

    invoke-virtual {v0, v1}, Lpb/x$a;->a(Lpb/u;)Lpb/x$a;

    :cond_1
    sget-wide v1, Lu5/k;->c:J

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1, v2, v3}, Lpb/x$a;->c(JLjava/util/concurrent/TimeUnit;)Lpb/x$a;

    move-result-object v1

    sget-wide v4, Lu5/k;->d:J

    invoke-virtual {v1, v4, v5, v3}, Lpb/x$a;->M(JLjava/util/concurrent/TimeUnit;)Lpb/x$a;

    move-result-object v1

    sget-wide v4, Lu5/k;->e:J

    invoke-virtual {v1, v4, v5, v3}, Lpb/x$a;->P(JLjava/util/concurrent/TimeUnit;)Lpb/x$a;

    return-object v0
.end method

.method private final d()Lgc/u;
    .locals 2

    new-instance v0, Lgc/u$b;

    invoke-direct {v0}, Lgc/u$b;-><init>()V

    invoke-direct {p0}, Lu5/k;->c()Lpb/x$a;

    move-result-object v1

    invoke-virtual {v1}, Lpb/x$a;->b()Lpb/x;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgc/u$b;->f(Lpb/x;)Lgc/u$b;

    move-result-object v0

    sget-object v1, Lj2/e;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lgc/u$b;->b(Ljava/lang/String;)Lgc/u$b;

    move-result-object v0

    sget-object v1, Lm7/a;->a:Lm7/a$a;

    invoke-virtual {v1}, Lm7/a$a;->a()Lm7/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgc/u$b;->a(Lgc/f$a;)Lgc/u$b;

    move-result-object v0

    invoke-static {}, Lhc/a;->f()Lhc/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgc/u$b;->a(Lgc/f$a;)Lgc/u$b;

    move-result-object v0

    invoke-virtual {v0}, Lgc/u$b;->d()Lgc/u;

    move-result-object v0

    const-string v1, "Builder()\n            .c\u2026e())\n            .build()"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public static final f(Ljava/lang/Class;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    const-string v0, "clazz"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lu5/k;->s:Lgc/u;

    if-nez v0, :cond_0

    sget-object v0, Lu5/k;->a:Lu5/k;

    invoke-direct {v0}, Lu5/k;->d()Lgc/u;

    move-result-object v0

    sput-object v0, Lu5/k;->s:Lgc/u;

    :cond_0
    sget-object v0, Lu5/k;->s:Lgc/u;

    invoke-static {v0}, Lm8/i;->c(Ljava/lang/Object;)V

    invoke-virtual {v0, p0}, Lgc/u;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final e()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    sget-object v0, Lu5/k;->l:Ljava/util/HashMap;

    return-object v0
.end method

.method public final g()Z
    .locals 3

    sget-object v0, Lf6/o;->a:Lf6/o;

    invoke-virtual {v0}, Lf6/o;->d()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    :cond_0
    sget-object v0, Lu5/k;->t:Ljava/util/Random;

    const/16 v2, 0x3e8

    invoke-virtual {v0, v2}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    const/4 v2, 0x1

    if-gt v0, v2, :cond_1

    move v1, v2

    :cond_1
    return v1
.end method
