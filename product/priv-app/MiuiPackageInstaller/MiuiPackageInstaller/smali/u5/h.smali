.class public final Lu5/h;
.super Ljava/lang/Object;


# direct methods
.method public static final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 12

    const-string v0, "theString"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1, v0}, Ljava/lang/StringBuffer;-><init>(I)V

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v0, :cond_30

    add-int/lit8 v4, v3, 0x1

    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v5, 0x5c

    if-ne v3, v5, :cond_2f

    add-int/lit8 v3, v4, 0x1

    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/16 v5, 0x75

    const/16 v6, 0x66

    const/16 v7, 0xa

    if-ne v4, v5, :cond_2a

    move v4, v2

    move v5, v4

    :goto_1
    const/4 v8, 0x4

    if-ge v4, v8, :cond_29

    add-int/lit8 v8, v3, 0x1

    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v9, 0x30

    const/4 v10, 0x1

    if-ne v3, v9, :cond_0

    :goto_2
    move v11, v10

    goto :goto_3

    :cond_0
    const/16 v11, 0x31

    if-ne v3, v11, :cond_1

    goto :goto_2

    :cond_1
    move v11, v2

    :goto_3
    if-eqz v11, :cond_2

    :goto_4
    move v11, v10

    goto :goto_5

    :cond_2
    const/16 v11, 0x32

    if-ne v3, v11, :cond_3

    goto :goto_4

    :cond_3
    move v11, v2

    :goto_5
    if-eqz v11, :cond_4

    :goto_6
    move v11, v10

    goto :goto_7

    :cond_4
    const/16 v11, 0x33

    if-ne v3, v11, :cond_5

    goto :goto_6

    :cond_5
    move v11, v2

    :goto_7
    if-eqz v11, :cond_6

    :goto_8
    move v11, v10

    goto :goto_9

    :cond_6
    const/16 v11, 0x34

    if-ne v3, v11, :cond_7

    goto :goto_8

    :cond_7
    move v11, v2

    :goto_9
    if-eqz v11, :cond_8

    :goto_a
    move v11, v10

    goto :goto_b

    :cond_8
    const/16 v11, 0x35

    if-ne v3, v11, :cond_9

    goto :goto_a

    :cond_9
    move v11, v2

    :goto_b
    if-eqz v11, :cond_a

    :goto_c
    move v11, v10

    goto :goto_d

    :cond_a
    const/16 v11, 0x36

    if-ne v3, v11, :cond_b

    goto :goto_c

    :cond_b
    move v11, v2

    :goto_d
    if-eqz v11, :cond_c

    :goto_e
    move v11, v10

    goto :goto_f

    :cond_c
    const/16 v11, 0x37

    if-ne v3, v11, :cond_d

    goto :goto_e

    :cond_d
    move v11, v2

    :goto_f
    if-eqz v11, :cond_e

    :goto_10
    move v11, v10

    goto :goto_11

    :cond_e
    const/16 v11, 0x38

    if-ne v3, v11, :cond_f

    goto :goto_10

    :cond_f
    move v11, v2

    :goto_11
    if-eqz v11, :cond_10

    :goto_12
    move v11, v10

    goto :goto_13

    :cond_10
    const/16 v11, 0x39

    if-ne v3, v11, :cond_11

    goto :goto_12

    :cond_11
    move v11, v2

    :goto_13
    if-eqz v11, :cond_12

    shl-int/lit8 v5, v5, 0x4

    :goto_14
    add-int/2addr v5, v3

    sub-int/2addr v5, v9

    goto/16 :goto_29

    :cond_12
    const/16 v9, 0x61

    if-ne v3, v9, :cond_13

    :goto_15
    move v11, v10

    goto :goto_16

    :cond_13
    const/16 v11, 0x62

    if-ne v3, v11, :cond_14

    goto :goto_15

    :cond_14
    move v11, v2

    :goto_16
    if-eqz v11, :cond_15

    :goto_17
    move v11, v10

    goto :goto_18

    :cond_15
    const/16 v11, 0x63

    if-ne v3, v11, :cond_16

    goto :goto_17

    :cond_16
    move v11, v2

    :goto_18
    if-eqz v11, :cond_17

    :goto_19
    move v11, v10

    goto :goto_1a

    :cond_17
    const/16 v11, 0x64

    if-ne v3, v11, :cond_18

    goto :goto_19

    :cond_18
    move v11, v2

    :goto_1a
    if-eqz v11, :cond_19

    :goto_1b
    move v11, v10

    goto :goto_1c

    :cond_19
    const/16 v11, 0x65

    if-ne v3, v11, :cond_1a

    goto :goto_1b

    :cond_1a
    move v11, v2

    :goto_1c
    if-eqz v11, :cond_1b

    :goto_1d
    move v11, v10

    goto :goto_1e

    :cond_1b
    if-ne v3, v6, :cond_1c

    goto :goto_1d

    :cond_1c
    move v11, v2

    :goto_1e
    if-eqz v11, :cond_1d

    :goto_1f
    shl-int/lit8 v5, v5, 0x4

    add-int/2addr v5, v7

    goto :goto_14

    :cond_1d
    const/16 v9, 0x41

    if-ne v3, v9, :cond_1e

    :goto_20
    move v11, v10

    goto :goto_21

    :cond_1e
    const/16 v11, 0x42

    if-ne v3, v11, :cond_1f

    goto :goto_20

    :cond_1f
    move v11, v2

    :goto_21
    if-eqz v11, :cond_20

    :goto_22
    move v11, v10

    goto :goto_23

    :cond_20
    const/16 v11, 0x43

    if-ne v3, v11, :cond_21

    goto :goto_22

    :cond_21
    move v11, v2

    :goto_23
    if-eqz v11, :cond_22

    :goto_24
    move v11, v10

    goto :goto_25

    :cond_22
    const/16 v11, 0x44

    if-ne v3, v11, :cond_23

    goto :goto_24

    :cond_23
    move v11, v2

    :goto_25
    if-eqz v11, :cond_24

    :goto_26
    move v11, v10

    goto :goto_27

    :cond_24
    const/16 v11, 0x45

    if-ne v3, v11, :cond_25

    goto :goto_26

    :cond_25
    move v11, v2

    :goto_27
    if-eqz v11, :cond_26

    goto :goto_28

    :cond_26
    const/16 v11, 0x46

    if-ne v3, v11, :cond_27

    goto :goto_28

    :cond_27
    move v10, v2

    :goto_28
    if-eqz v10, :cond_28

    goto :goto_1f

    :goto_29
    add-int/lit8 v4, v4, 0x1

    move v3, v8

    goto/16 :goto_1

    :cond_28
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string v0, "Malformed   \\uxxxx   encoding."

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_29
    int-to-char v4, v5

    goto :goto_2a

    :cond_2a
    const/16 v5, 0x74

    if-ne v4, v5, :cond_2b

    const/16 v4, 0x9

    goto :goto_2a

    :cond_2b
    const/16 v5, 0x72

    if-ne v4, v5, :cond_2c

    const/16 v4, 0xd

    goto :goto_2a

    :cond_2c
    const/16 v5, 0x6e

    if-ne v4, v5, :cond_2d

    move v4, v7

    goto :goto_2a

    :cond_2d
    if-ne v4, v6, :cond_2e

    const/16 v4, 0xc

    :cond_2e
    :goto_2a
    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto/16 :goto_0

    :cond_2f
    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move v3, v4

    goto/16 :goto_0

    :cond_30
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p0

    const-string v0, "outBuffer.toString()"

    invoke-static {p0, v0}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method
