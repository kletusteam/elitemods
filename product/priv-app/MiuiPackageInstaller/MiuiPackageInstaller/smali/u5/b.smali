.class public interface abstract Lu5/b;
.super Ljava/lang/Object;


# virtual methods
.method public abstract a(Ld8/d;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ld8/d<",
            "-",
            "Lgc/t<",
            "Lcom/miui/packageInstaller/model/CloseReasonListBean;",
            ">;>;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .annotation runtime Lic/f;
        value = "/safemode/close/reason/list"
    .end annotation

    .annotation runtime Lic/k;
        value = {
            "CONNECT_TIMEOUT:5000"
        }
    .end annotation
.end method

.method public abstract b(Ljava/util/Map;Ld8/d;)Ljava/lang/Object;
    .param p1    # Ljava/util/Map;
        .annotation runtime Lic/d;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ld8/d<",
            "-",
            "Lgc/t<",
            "Lcom/miui/packageInstaller/model/AdModel;",
            ">;>;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .annotation runtime Lic/e;
    .end annotation

    .annotation runtime Lic/o;
        value = "/info/layout"
    .end annotation
.end method

.method public abstract c(Ld8/d;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ld8/d<",
            "-",
            "Lgc/t<",
            "Ljava/lang/Long;",
            ">;>;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .annotation runtime Lic/f;
        value = "/v3/game/safeModeOpenCount"
    .end annotation
.end method

.method public abstract d(Ljava/util/Map;Ld8/d;)Ljava/lang/Object;
    .param p1    # Ljava/util/Map;
        .annotation runtime Lic/d;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ld8/d<",
            "-",
            "Lgc/t<",
            "Lcom/miui/packageInstaller/model/PassportIdentityUrlModel;",
            ">;>;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .annotation runtime Lic/e;
    .end annotation

    .annotation runtime Lic/o;
        value = "/passport/identity/url"
    .end annotation
.end method

.method public abstract e(Ljava/lang/String;Ljava/lang/String;Ld8/d;)Ljava/lang/Object;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lic/c;
            value = "usedExpId"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lic/c;
            value = "exp"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ld8/d<",
            "-",
            "Lgc/t<",
            "Lorg/json/JSONObject;",
            ">;>;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .annotation runtime Lic/e;
    .end annotation

    .annotation runtime Lic/o;
        value = "/testplatform/test/info"
    .end annotation
.end method

.method public abstract f(Ljava/util/Map;Ld8/d;)Ljava/lang/Object;
    .param p1    # Ljava/util/Map;
        .annotation runtime Lic/d;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ld8/d<",
            "-",
            "Lgc/t<",
            "Lpb/c0;",
            ">;>;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .annotation runtime Lic/e;
    .end annotation

    .annotation runtime Lic/o;
        value = " /v4/game/interceptcheck/normal"
    .end annotation
.end method

.method public abstract g(Ljava/util/Map;Ld8/d;)Ljava/lang/Object;
    .param p1    # Ljava/util/Map;
        .annotation runtime Lic/d;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ld8/d<",
            "-",
            "Lgc/t<",
            "Lpb/c0;",
            ">;>;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .annotation runtime Lic/e;
    .end annotation

    .annotation runtime Lic/o;
        value = "/v4/game/interceptcheck/safemode"
    .end annotation
.end method
