.class public interface abstract Lu5/j;
.super Ljava/lang/Object;


# virtual methods
.method public abstract a(Ljava/util/Map;)Lgc/b;
    .param p1    # Ljava/util/Map;
        .annotation runtime Lic/d;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lgc/b<",
            "Lcom/miui/packageInstaller/model/MiResponse<",
            "Lcom/miui/packageInstaller/model/CloudConfigModel;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Lic/e;
    .end annotation

    .annotation runtime Lic/o;
        value = "/v2/cloud_config/query"
    .end annotation
.end method

.method public abstract b(Ljava/util/Map;)Lgc/b;
    .param p1    # Ljava/util/Map;
        .annotation runtime Lic/d;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lgc/b<",
            "Lcom/miui/packageInstaller/model/AdModel;",
            ">;"
        }
    .end annotation

    .annotation runtime Lic/e;
    .end annotation

    .annotation runtime Lic/o;
        value = "/info/layout"
    .end annotation
.end method

.method public abstract c(Lorg/json/JSONObject;)Lgc/b;
    .param p1    # Lorg/json/JSONObject;
        .annotation runtime Lic/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            ")",
            "Lgc/b<",
            "Lorg/json/JSONObject;",
            ">;"
        }
    .end annotation

    .annotation runtime Lic/o;
        value = "/pkg/info/type/query"
    .end annotation
.end method

.method public abstract d(Ljava/util/Map;)Lgc/b;
    .param p1    # Ljava/util/Map;
        .annotation runtime Lic/d;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lgc/b<",
            "Lcom/miui/packageInstaller/model/RiskTypeResult;",
            ">;"
        }
    .end annotation

    .annotation runtime Lic/e;
    .end annotation

    .annotation runtime Lic/o;
        value = "/pkg/risk/developer/appeal"
    .end annotation
.end method
