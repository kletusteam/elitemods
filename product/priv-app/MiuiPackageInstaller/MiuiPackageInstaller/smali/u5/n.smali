.class public final Lu5/n;
.super Ljava/lang/Object;

# interfaces
.implements Lpb/u;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lu5/n$a;
    }
.end annotation


# static fields
.field public static final a:Lu5/n$a;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lu5/n$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lu5/n$a;-><init>(Lm8/g;)V

    sput-object v0, Lu5/n;->a:Lu5/n$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lpb/u$a;)Lpb/b0;
    .locals 3

    const-string v0, "chain"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1}, Lpb/u$a;->J()Lpb/z;

    move-result-object v0

    :try_start_0
    const-string v1, "CONNECT_TIMEOUT"

    invoke-virtual {v0, v1}, Lpb/z;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "valueOf(it)"

    invoke-static {v1, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Number;->intValue()I

    move-result v1

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {p1, v1, v2}, Lpb/u$a;->d(ILjava/util/concurrent/TimeUnit;)Lpb/u$a;

    move-result-object v1

    invoke-interface {v1, v0}, Lpb/u$a;->e(Lpb/z;)Lpb/b0;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    :cond_0
    invoke-interface {p1, v0}, Lpb/u$a;->e(Lpb/z;)Lpb/b0;

    move-result-object p1

    return-object p1
.end method
