.class final Ld8/g$a$a;
.super Lm8/j;

# interfaces
.implements Ll8/p;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Ld8/g$a;->a(Ld8/g;Ld8/g;)Ld8/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lm8/j;",
        "Ll8/p<",
        "Ld8/g;",
        "Ld8/g$b;",
        "Ld8/g;",
        ">;"
    }
.end annotation


# static fields
.field public static final b:Ld8/g$a$a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ld8/g$a$a;

    invoke-direct {v0}, Ld8/g$a$a;-><init>()V

    sput-object v0, Ld8/g$a$a;->b:Ld8/g$a$a;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lm8/j;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final b(Ld8/g;Ld8/g$b;)Ld8/g;
    .locals 3

    const-string v0, "acc"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "element"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2}, Ld8/g$b;->getKey()Ld8/g$c;

    move-result-object v0

    invoke-interface {p1, v0}, Ld8/g;->minusKey(Ld8/g$c;)Ld8/g;

    move-result-object p1

    sget-object v0, Ld8/h;->a:Ld8/h;

    if-ne p1, v0, :cond_0

    goto :goto_1

    :cond_0
    sget-object v1, Ld8/e;->b0:Ld8/e$b;

    invoke-interface {p1, v1}, Ld8/g;->get(Ld8/g$c;)Ld8/g$b;

    move-result-object v2

    check-cast v2, Ld8/e;

    if-nez v2, :cond_1

    new-instance v0, Ld8/c;

    invoke-direct {v0, p1, p2}, Ld8/c;-><init>(Ld8/g;Ld8/g$b;)V

    :goto_0
    move-object p2, v0

    goto :goto_1

    :cond_1
    invoke-interface {p1, v1}, Ld8/g;->minusKey(Ld8/g$c;)Ld8/g;

    move-result-object p1

    if-ne p1, v0, :cond_2

    new-instance p1, Ld8/c;

    invoke-direct {p1, p2, v2}, Ld8/c;-><init>(Ld8/g;Ld8/g$b;)V

    move-object p2, p1

    goto :goto_1

    :cond_2
    new-instance v0, Ld8/c;

    new-instance v1, Ld8/c;

    invoke-direct {v1, p1, p2}, Ld8/c;-><init>(Ld8/g;Ld8/g$b;)V

    invoke-direct {v0, v1, v2}, Ld8/c;-><init>(Ld8/g;Ld8/g$b;)V

    goto :goto_0

    :goto_1
    return-object p2
.end method

.method public bridge synthetic g(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ld8/g;

    check-cast p2, Ld8/g$b;

    invoke-virtual {p0, p1, p2}, Ld8/g$a$a;->b(Ld8/g;Ld8/g$b;)Ld8/g;

    move-result-object p1

    return-object p1
.end method
