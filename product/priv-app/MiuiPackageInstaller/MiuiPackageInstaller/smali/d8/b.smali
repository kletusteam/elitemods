.class public abstract Ld8/b;
.super Ljava/lang/Object;

# interfaces
.implements Ld8/g$c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<B::",
        "Ld8/g$b;",
        "E::TB;>",
        "Ljava/lang/Object;",
        "Ld8/g$c<",
        "TE;>;"
    }
.end annotation


# instance fields
.field private final a:Ll8/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ll8/l<",
            "Ld8/g$b;",
            "TE;>;"
        }
    .end annotation
.end field

.field private final b:Ld8/g$c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ld8/g$c<",
            "*>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ld8/g$c;Ll8/l;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ld8/g$c<",
            "TB;>;",
            "Ll8/l<",
            "-",
            "Ld8/g$b;",
            "+TE;>;)V"
        }
    .end annotation

    const-string v0, "baseKey"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "safeCast"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Ld8/b;->a:Ll8/l;

    instance-of p2, p1, Ld8/b;

    if-eqz p2, :cond_0

    check-cast p1, Ld8/b;

    iget-object p1, p1, Ld8/b;->b:Ld8/g$c;

    :cond_0
    iput-object p1, p0, Ld8/b;->b:Ld8/g$c;

    return-void
.end method


# virtual methods
.method public final a(Ld8/g$c;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ld8/g$c<",
            "*>;)Z"
        }
    .end annotation

    const-string v0, "key"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    if-eq p1, p0, :cond_1

    iget-object v0, p0, Ld8/b;->b:Ld8/g$c;

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method public final b(Ld8/g$b;)Ld8/g$b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ld8/g$b;",
            ")TE;"
        }
    .end annotation

    const-string v0, "element"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Ld8/b;->a:Ll8/l;

    invoke-interface {v0, p1}, Ll8/l;->j(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ld8/g$b;

    return-object p1
.end method
