.class public abstract Ld8/a;
.super Ljava/lang/Object;

# interfaces
.implements Ld8/g$b;


# instance fields
.field private final key:Ld8/g$c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ld8/g$c<",
            "*>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ld8/g$c;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ld8/g$c<",
            "*>;)V"
        }
    .end annotation

    const-string v0, "key"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Ld8/a;->key:Ld8/g$c;

    return-void
.end method


# virtual methods
.method public fold(Ljava/lang/Object;Ll8/p;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(TR;",
            "Ll8/p<",
            "-TR;-",
            "Ld8/g$b;",
            "+TR;>;)TR;"
        }
    .end annotation

    invoke-static {p0, p1, p2}, Ld8/g$b$a;->a(Ld8/g$b;Ljava/lang/Object;Ll8/p;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public get(Ld8/g$c;)Ld8/g$b;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Ld8/g$b;",
            ">(",
            "Ld8/g$c<",
            "TE;>;)TE;"
        }
    .end annotation

    invoke-static {p0, p1}, Ld8/g$b$a;->b(Ld8/g$b;Ld8/g$c;)Ld8/g$b;

    move-result-object p1

    return-object p1
.end method

.method public getKey()Ld8/g$c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ld8/g$c<",
            "*>;"
        }
    .end annotation

    iget-object v0, p0, Ld8/a;->key:Ld8/g$c;

    return-object v0
.end method

.method public minusKey(Ld8/g$c;)Ld8/g;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ld8/g$c<",
            "*>;)",
            "Ld8/g;"
        }
    .end annotation

    invoke-static {p0, p1}, Ld8/g$b$a;->c(Ld8/g$b;Ld8/g$c;)Ld8/g;

    move-result-object p1

    return-object p1
.end method

.method public plus(Ld8/g;)Ld8/g;
    .locals 0

    invoke-static {p0, p1}, Ld8/g$b$a;->d(Ld8/g$b;Ld8/g;)Ld8/g;

    move-result-object p1

    return-object p1
.end method
