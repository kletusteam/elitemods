.class public interface abstract Ld8/g;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ld8/g$c;,
        Ld8/g$b;,
        Ld8/g$a;
    }
.end annotation


# virtual methods
.method public abstract fold(Ljava/lang/Object;Ll8/p;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(TR;",
            "Ll8/p<",
            "-TR;-",
            "Ld8/g$b;",
            "+TR;>;)TR;"
        }
    .end annotation
.end method

.method public abstract get(Ld8/g$c;)Ld8/g$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Ld8/g$b;",
            ">(",
            "Ld8/g$c<",
            "TE;>;)TE;"
        }
    .end annotation
.end method

.method public abstract minusKey(Ld8/g$c;)Ld8/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ld8/g$c<",
            "*>;)",
            "Ld8/g;"
        }
    .end annotation
.end method

.method public abstract plus(Ld8/g;)Ld8/g;
.end method
