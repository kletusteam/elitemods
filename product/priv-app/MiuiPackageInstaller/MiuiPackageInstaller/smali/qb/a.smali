.class public final Lqb/a;
.super Ljava/lang/Object;

# interfaces
.implements Lpb/u;


# static fields
.field public static final a:Lqb/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lqb/a;

    invoke-direct {v0}, Lqb/a;-><init>()V

    sput-object v0, Lqb/a;->a:Lqb/a;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lpb/u$a;)Lpb/b0;
    .locals 3

    const-string v0, "chain"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1}, Lpb/u$a;->J()Lpb/z;

    move-result-object v0

    const-string v1, "Accept-Encoding"

    invoke-virtual {v0, v1}, Lpb/z;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-interface {p1}, Lpb/u$a;->J()Lpb/z;

    move-result-object v0

    invoke-virtual {v0}, Lpb/z;->h()Lpb/z$a;

    move-result-object v0

    const-string v2, "br,gzip"

    invoke-virtual {v0, v1, v2}, Lpb/z$a;->d(Ljava/lang/String;Ljava/lang/String;)Lpb/z$a;

    move-result-object v0

    invoke-virtual {v0}, Lpb/z$a;->b()Lpb/z;

    move-result-object v0

    invoke-interface {p1, v0}, Lpb/u$a;->e(Lpb/z;)Lpb/b0;

    move-result-object p1

    invoke-virtual {p0, p1}, Lqb/a;->b(Lpb/b0;)Lpb/b0;

    move-result-object p1

    goto :goto_0

    :cond_0
    invoke-interface {p1}, Lpb/u$a;->J()Lpb/z;

    move-result-object v0

    invoke-interface {p1, v0}, Lpb/u$a;->e(Lpb/z;)Lpb/b0;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public final b(Lpb/b0;)Lpb/b0;
    .locals 5

    const-string v0, "response"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lpb/b0;->b()Lpb/c0;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v1, 0x2

    const-string v2, "Content-Encoding"

    const/4 v3, 0x0

    invoke-static {p1, v2, v3, v1, v3}, Lpb/b0;->y(Lpb/b0;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    const-string v3, "br"

    const/4 v4, 0x1

    invoke-static {v1, v3, v4}, Lu8/g;->j(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_0

    new-instance v1, Lfc/b;

    invoke-virtual {v0}, Lpb/c0;->r()Ldc/g;

    move-result-object v3

    invoke-interface {v3}, Ldc/g;->S()Ljava/io/InputStream;

    move-result-object v3

    invoke-direct {v1, v3}, Lfc/b;-><init>(Ljava/io/InputStream;)V

    invoke-static {v1}, Ldc/o;->e(Ljava/io/InputStream;)Ldc/y;

    move-result-object v1

    :goto_0
    invoke-static {v1}, Ldc/o;->b(Ldc/y;)Ldc/g;

    move-result-object v1

    goto :goto_1

    :cond_0
    const-string v3, "gzip"

    invoke-static {v1, v3, v4}, Lu8/g;->j(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Ldc/l;

    invoke-virtual {v0}, Lpb/c0;->r()Ldc/g;

    move-result-object v3

    invoke-direct {v1, v3}, Ldc/l;-><init>(Ldc/y;)V

    goto :goto_0

    :goto_1
    invoke-virtual {p1}, Lpb/b0;->J()Lpb/b0$a;

    move-result-object p1

    invoke-virtual {p1, v2}, Lpb/b0$a;->r(Ljava/lang/String;)Lpb/b0$a;

    move-result-object p1

    const-string v2, "Content-Length"

    invoke-virtual {p1, v2}, Lpb/b0$a;->r(Ljava/lang/String;)Lpb/b0$a;

    move-result-object p1

    sget-object v2, Lpb/c0;->b:Lpb/c0$b;

    invoke-virtual {v0}, Lpb/c0;->n()Lpb/v;

    move-result-object v0

    const-wide/16 v3, -0x1

    invoke-virtual {v2, v1, v0, v3, v4}, Lpb/c0$b;->a(Ldc/g;Lpb/v;J)Lpb/c0;

    move-result-object v0

    invoke-virtual {p1, v0}, Lpb/b0$a;->b(Lpb/c0;)Lpb/b0$a;

    move-result-object p1

    invoke-virtual {p1}, Lpb/b0$a;->c()Lpb/b0;

    move-result-object p1

    :cond_1
    return-object p1
.end method
