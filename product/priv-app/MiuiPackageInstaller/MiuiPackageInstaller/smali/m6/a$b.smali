.class public final enum Lm6/a$b;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lm6/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lm6/a$b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lm6/a$b;

.field public static final enum b:Lm6/a$b;

.field public static final enum c:Lm6/a$b;

.field public static final enum d:Lm6/a$b;

.field public static final enum e:Lm6/a$b;

.field public static final enum f:Lm6/a$b;

.field public static final enum g:Lm6/a$b;

.field public static final enum h:Lm6/a$b;

.field public static final enum i:Lm6/a$b;

.field public static final enum j:Lm6/a$b;

.field public static final enum k:Lm6/a$b;

.field public static final enum l:Lm6/a$b;

.field public static final enum m:Lm6/a$b;

.field public static final enum n:Lm6/a$b;

.field public static final enum o:Lm6/a$b;

.field public static final enum p:Lm6/a$b;

.field public static final enum q:Lm6/a$b;

.field private static final synthetic r:[Lm6/a$b;


# direct methods
.method static constructor <clinit>()V
    .locals 20

    new-instance v0, Lm6/a$b;

    const-string v1, "onPageShow"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lm6/a$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lm6/a$b;->a:Lm6/a$b;

    new-instance v1, Lm6/a$b;

    const-string v3, "onPageHide"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lm6/a$b;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lm6/a$b;->b:Lm6/a$b;

    new-instance v3, Lm6/a$b;

    const-string v5, "onContextPause"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lm6/a$b;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lm6/a$b;->c:Lm6/a$b;

    new-instance v5, Lm6/a$b;

    const-string v7, "onContextStop"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8}, Lm6/a$b;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lm6/a$b;->d:Lm6/a$b;

    new-instance v7, Lm6/a$b;

    const-string v9, "onContextResume"

    const/4 v10, 0x4

    invoke-direct {v7, v9, v10}, Lm6/a$b;-><init>(Ljava/lang/String;I)V

    sput-object v7, Lm6/a$b;->e:Lm6/a$b;

    new-instance v9, Lm6/a$b;

    const-string v11, "onRecyclerViewDetached"

    const/4 v12, 0x5

    invoke-direct {v9, v11, v12}, Lm6/a$b;-><init>(Ljava/lang/String;I)V

    sput-object v9, Lm6/a$b;->f:Lm6/a$b;

    new-instance v11, Lm6/a$b;

    const-string v13, "onRecyclerViewAttached"

    const/4 v14, 0x6

    invoke-direct {v11, v13, v14}, Lm6/a$b;-><init>(Ljava/lang/String;I)V

    sput-object v11, Lm6/a$b;->g:Lm6/a$b;

    new-instance v13, Lm6/a$b;

    const-string v15, "onViewObjectRecycled"

    const/4 v14, 0x7

    invoke-direct {v13, v15, v14}, Lm6/a$b;-><init>(Ljava/lang/String;I)V

    sput-object v13, Lm6/a$b;->h:Lm6/a$b;

    new-instance v15, Lm6/a$b;

    const-string v14, "onStartScrollInFromTop"

    const/16 v12, 0x8

    invoke-direct {v15, v14, v12}, Lm6/a$b;-><init>(Ljava/lang/String;I)V

    sput-object v15, Lm6/a$b;->i:Lm6/a$b;

    new-instance v14, Lm6/a$b;

    const-string v12, "onStartScrollInFromBottom"

    const/16 v10, 0x9

    invoke-direct {v14, v12, v10}, Lm6/a$b;-><init>(Ljava/lang/String;I)V

    sput-object v14, Lm6/a$b;->j:Lm6/a$b;

    new-instance v12, Lm6/a$b;

    const-string v10, "onCompletelyScrollInFromTop"

    const/16 v8, 0xa

    invoke-direct {v12, v10, v8}, Lm6/a$b;-><init>(Ljava/lang/String;I)V

    sput-object v12, Lm6/a$b;->k:Lm6/a$b;

    new-instance v10, Lm6/a$b;

    const-string v8, "onCompletelyScrollInFromBottom"

    const/16 v6, 0xb

    invoke-direct {v10, v8, v6}, Lm6/a$b;-><init>(Ljava/lang/String;I)V

    sput-object v10, Lm6/a$b;->l:Lm6/a$b;

    new-instance v8, Lm6/a$b;

    const-string v6, "onStartScrollOutFromTop"

    const/16 v4, 0xc

    invoke-direct {v8, v6, v4}, Lm6/a$b;-><init>(Ljava/lang/String;I)V

    sput-object v8, Lm6/a$b;->m:Lm6/a$b;

    new-instance v6, Lm6/a$b;

    const-string v4, "onStartScrollOutFromBottom"

    const/16 v2, 0xd

    invoke-direct {v6, v4, v2}, Lm6/a$b;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lm6/a$b;->n:Lm6/a$b;

    new-instance v4, Lm6/a$b;

    const-string v2, "onCompletelyScrollOutFromTop"

    move-object/from16 v17, v6

    const/16 v6, 0xe

    invoke-direct {v4, v2, v6}, Lm6/a$b;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lm6/a$b;->o:Lm6/a$b;

    new-instance v2, Lm6/a$b;

    const-string v6, "onCompletelyScrollOutFromBottom"

    move-object/from16 v18, v4

    const/16 v4, 0xf

    invoke-direct {v2, v6, v4}, Lm6/a$b;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lm6/a$b;->p:Lm6/a$b;

    new-instance v6, Lm6/a$b;

    const-string v4, "onCompletelyScrollOutFromDetail"

    move-object/from16 v19, v2

    const/16 v2, 0x10

    invoke-direct {v6, v4, v2}, Lm6/a$b;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lm6/a$b;->q:Lm6/a$b;

    const/16 v4, 0x11

    new-array v4, v4, [Lm6/a$b;

    const/16 v16, 0x0

    aput-object v0, v4, v16

    const/4 v0, 0x1

    aput-object v1, v4, v0

    const/4 v0, 0x2

    aput-object v3, v4, v0

    const/4 v0, 0x3

    aput-object v5, v4, v0

    const/4 v0, 0x4

    aput-object v7, v4, v0

    const/4 v0, 0x5

    aput-object v9, v4, v0

    const/4 v0, 0x6

    aput-object v11, v4, v0

    const/4 v0, 0x7

    aput-object v13, v4, v0

    const/16 v0, 0x8

    aput-object v15, v4, v0

    const/16 v0, 0x9

    aput-object v14, v4, v0

    const/16 v0, 0xa

    aput-object v12, v4, v0

    const/16 v0, 0xb

    aput-object v10, v4, v0

    const/16 v0, 0xc

    aput-object v8, v4, v0

    const/16 v0, 0xd

    aput-object v17, v4, v0

    const/16 v0, 0xe

    aput-object v18, v4, v0

    const/16 v0, 0xf

    aput-object v19, v4, v0

    aput-object v6, v4, v2

    sput-object v4, Lm6/a$b;->r:[Lm6/a$b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lm6/a$b;
    .locals 1

    const-class v0, Lm6/a$b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lm6/a$b;

    return-object p0
.end method

.method public static values()[Lm6/a$b;
    .locals 1

    sget-object v0, Lm6/a$b;->r:[Lm6/a$b;

    invoke-virtual {v0}, [Lm6/a$b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lm6/a$b;

    return-object v0
.end method
