.class public abstract Lm6/a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lm6/a$b;,
        Lm6/a$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Landroidx/recyclerview/widget/RecyclerView$d0;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static k:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ll6/c;

.field protected final c:Lm6/b;

.field private d:Lcom/miui/packageInstaller/view/recyclerview/adapter/b;

.field private e:Lj6/b;

.field protected f:Ljava/lang/Object;

.field private g:Z

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lm6/a$a;",
            ">;"
        }
    .end annotation
.end field

.field private i:I

.field private j:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lm6/a;->k:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/Object;Ll6/c;Lm6/b;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lm6/a;->g:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lm6/a;->h:Ljava/util/List;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lm6/a;->j:Z

    sget-object v0, Lm6/a;->k:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    iput v0, p0, Lm6/a;->i:I

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lm6/a;->a:Ljava/lang/ref/WeakReference;

    iput-object p2, p0, Lm6/a;->f:Ljava/lang/Object;

    iput-object p3, p0, Lm6/a;->b:Ll6/c;

    iput-object p4, p0, Lm6/a;->c:Lm6/b;

    return-void
.end method

.method private c()V
    .locals 2

    iget-object v0, p0, Lm6/a;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_0

    iget-object v1, p0, Lm6/a;->h:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lm6/a$a;

    invoke-virtual {p0, v1}, Lm6/a;->x(Lm6/a$a;)V

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public d(Lm6/a$b;)V
    .locals 2

    iget-object v0, p0, Lm6/a;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_0

    :try_start_0
    iget-object v1, p0, Lm6/a;->h:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lm6/a$a;

    invoke-interface {v1, p0, p1}, Lm6/a$a;->a(Lm6/a;Lm6/a$b;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    :goto_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public e()Ll6/c;
    .locals 1

    iget-object v0, p0, Lm6/a;->b:Ll6/c;

    return-object v0
.end method

.method public f()Lj6/b;
    .locals 1

    iget-object v0, p0, Lm6/a;->e:Lj6/b;

    return-object v0
.end method

.method public final g()Lcom/miui/packageInstaller/view/recyclerview/adapter/AdapterDelegate;
    .locals 4

    iget-object v0, p0, Lm6/a;->d:Lcom/miui/packageInstaller/view/recyclerview/adapter/b;

    if-nez v0, :cond_0

    new-instance v0, Lcom/miui/packageInstaller/view/recyclerview/adapter/a;

    invoke-virtual {p0}, Lm6/a;->k()I

    move-result v1

    invoke-virtual {p0}, Lm6/a;->l()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p0}, Lm6/a;->y()Z

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/miui/packageInstaller/view/recyclerview/adapter/a;-><init>(ILjava/lang/Class;Z)V

    iput-object v0, p0, Lm6/a;->d:Lcom/miui/packageInstaller/view/recyclerview/adapter/b;

    :cond_0
    new-instance v0, Lcom/miui/packageInstaller/view/recyclerview/adapter/AdapterDelegate;

    iget-object v1, p0, Lm6/a;->d:Lcom/miui/packageInstaller/view/recyclerview/adapter/b;

    invoke-direct {v0, p0, v1}, Lcom/miui/packageInstaller/view/recyclerview/adapter/AdapterDelegate;-><init>(Lm6/a;Lcom/miui/packageInstaller/view/recyclerview/adapter/b;)V

    return-object v0
.end method

.method public final h()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lm6/a;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    return-object v0
.end method

.method public i()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lm6/a;->f:Ljava/lang/Object;

    return-object v0
.end method

.method public j()I
    .locals 1

    iget v0, p0, Lm6/a;->i:I

    return v0
.end method

.method public abstract k()I
.end method

.method public final l()Ljava/lang/Class;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "TT;>;"
        }
    .end annotation

    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Class;->getGenericSuperclass()Ljava/lang/reflect/Type;

    move-result-object v1

    instance-of v1, v1, Ljava/lang/reflect/ParameterizedType;

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v0

    goto :goto_0

    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Class;->getGenericSuperclass()Ljava/lang/reflect/Type;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/ParameterizedType;

    invoke-interface {v0}, Ljava/lang/reflect/ParameterizedType;->getActualTypeArguments()[Ljava/lang/reflect/Type;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    check-cast v0, Ljava/lang/Class;

    return-object v0

    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "No view holder class found."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public m()V
    .locals 0

    invoke-virtual {p0, p0}, Lm6/a;->u(Lm6/a;)V

    return-void
.end method

.method public n(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lm6/a;->e:Lj6/b;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p0, p1}, Lj6/b;->V(Lm6/a;Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public abstract o(Landroidx/recyclerview/widget/RecyclerView$d0;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation
.end method

.method public p(Landroidx/recyclerview/widget/RecyclerView$d0;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Ljava/util/List<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public q(Landroidx/recyclerview/widget/RecyclerView$d0;)V
    .locals 0

    sget-object p1, Lm6/a$b;->h:Lm6/a$b;

    invoke-virtual {p0, p1}, Lm6/a;->d(Lm6/a$b;)V

    invoke-direct {p0}, Lm6/a;->c()V

    return-void
.end method

.method public r(I)V
    .locals 1

    iget-object v0, p0, Lm6/a;->f:Ljava/lang/Object;

    invoke-virtual {p0, p1, v0}, Lm6/a;->s(ILjava/lang/Object;)V

    return-void
.end method

.method public s(ILjava/lang/Object;)V
    .locals 7

    iget-object v0, p0, Lm6/a;->b:Ll6/c;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-interface {v0, p2}, Ll6/c;->a(Ljava/lang/Object;)Lk6/a;

    move-result-object v1

    if-eqz v1, :cond_1

    :try_start_0
    invoke-virtual {p0}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    move v4, p1

    move-object v5, p2

    move-object v6, p0

    invoke-interface/range {v1 .. v6}, Lk6/a;->a(Landroid/content/Context;Ljava/lang/Class;ILjava/lang/Object;Lm6/a;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_1
    :goto_0
    return-void
.end method

.method public t()V
    .locals 1

    iget-object v0, p0, Lm6/a;->e:Lj6/b;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, Lj6/b;->c0(Lm6/a;)I

    :cond_0
    return-void
.end method

.method public u(Lm6/a;)V
    .locals 4

    iget-boolean v0, p1, Lm6/a;->g:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lm6/a;->c:Lm6/b;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lm6/a;->i()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p1}, Lm6/a;->h()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p1}, Lm6/a;->e()Ll6/c;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lm6/b;->a(Ljava/lang/Object;Landroid/content/Context;Ll6/c;)Lm6/a;

    move-result-object v0

    if-eqz v0, :cond_0

    move-object p1, v0

    :cond_0
    iget-object v0, p0, Lm6/a;->e:Lj6/b;

    if-eqz v0, :cond_1

    invoke-virtual {v0, p0, p1}, Lj6/b;->h0(Lm6/a;Lm6/a;)V

    :cond_1
    return-void
.end method

.method public final v(Lj6/b;)V
    .locals 1

    iget-object v0, p0, Lm6/a;->e:Lj6/b;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, Lj6/b;->m0(Lm6/a;)V

    :cond_0
    iput-object p1, p0, Lm6/a;->e:Lj6/b;

    if-eqz p1, :cond_1

    iget-object p1, p0, Lm6/a;->h:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    if-lez p1, :cond_1

    iget-object p1, p0, Lm6/a;->e:Lj6/b;

    invoke-virtual {p1, p0}, Lj6/b;->b0(Lm6/a;)V

    :cond_1
    return-void
.end method

.method public w(Z)V
    .locals 0

    iput-boolean p1, p0, Lm6/a;->j:Z

    return-void
.end method

.method public x(Lm6/a$a;)V
    .locals 1

    iget-object v0, p0, Lm6/a;->h:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lm6/a;->h:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    iget-object p1, p0, Lm6/a;->e:Lj6/b;

    if-eqz p1, :cond_1

    iget-object p1, p0, Lm6/a;->h:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    if-nez p1, :cond_1

    iget-object p1, p0, Lm6/a;->e:Lj6/b;

    invoke-virtual {p1, p0}, Lj6/b;->m0(Lm6/a;)V

    :cond_1
    return-void
.end method

.method protected y()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
