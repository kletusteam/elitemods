.class public abstract Lf8/d;
.super Lf8/a;


# instance fields
.field private final b:Ld8/g;

.field private transient c:Ld8/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ld8/d<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ld8/d;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ld8/d<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ld8/d;->c()Ld8/g;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-direct {p0, p1, v0}, Lf8/d;-><init>(Ld8/d;Ld8/g;)V

    return-void
.end method

.method public constructor <init>(Ld8/d;Ld8/g;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ld8/d<",
            "Ljava/lang/Object;",
            ">;",
            "Ld8/g;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1}, Lf8/a;-><init>(Ld8/d;)V

    iput-object p2, p0, Lf8/d;->b:Ld8/g;

    return-void
.end method


# virtual methods
.method public c()Ld8/g;
    .locals 1

    iget-object v0, p0, Lf8/d;->b:Ld8/g;

    invoke-static {v0}, Lm8/i;->c(Ljava/lang/Object;)V

    return-object v0
.end method

.method protected o()V
    .locals 3

    iget-object v0, p0, Lf8/d;->c:Ld8/d;

    if-eqz v0, :cond_0

    if-eq v0, p0, :cond_0

    invoke-virtual {p0}, Lf8/d;->c()Ld8/g;

    move-result-object v1

    sget-object v2, Ld8/e;->b0:Ld8/e$b;

    invoke-interface {v1, v2}, Ld8/g;->get(Ld8/g$c;)Ld8/g$b;

    move-result-object v1

    invoke-static {v1}, Lm8/i;->c(Ljava/lang/Object;)V

    check-cast v1, Ld8/e;

    invoke-interface {v1, v0}, Ld8/e;->K(Ld8/d;)V

    :cond_0
    sget-object v0, Lf8/c;->a:Lf8/c;

    iput-object v0, p0, Lf8/d;->c:Ld8/d;

    return-void
.end method

.method public final p()Ld8/d;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ld8/d<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lf8/d;->c:Ld8/d;

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lf8/d;->c()Ld8/g;

    move-result-object v0

    sget-object v1, Ld8/e;->b0:Ld8/e$b;

    invoke-interface {v0, v1}, Ld8/g;->get(Ld8/g$c;)Ld8/g$b;

    move-result-object v0

    check-cast v0, Ld8/e;

    if-eqz v0, :cond_0

    invoke-interface {v0, p0}, Ld8/e;->y(Ld8/d;)Ld8/d;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    move-object v0, p0

    :cond_1
    iput-object v0, p0, Lf8/d;->c:Ld8/d;

    :cond_2
    return-object v0
.end method
