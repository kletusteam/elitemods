.class public abstract Lf8/k;
.super Lf8/d;

# interfaces
.implements Lm8/h;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf8/d;",
        "Lm8/h<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field private final d:I


# direct methods
.method public constructor <init>(ILd8/d;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ld8/d<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0, p2}, Lf8/d;-><init>(Ld8/d;)V

    iput p1, p0, Lf8/k;->d:I

    return-void
.end method


# virtual methods
.method public d()I
    .locals 1

    iget v0, p0, Lf8/k;->d:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lf8/a;->l()Ld8/d;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lm8/u;->e(Lm8/h;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "renderLambdaToString(this)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    invoke-super {p0}, Lf8/a;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0
.end method
