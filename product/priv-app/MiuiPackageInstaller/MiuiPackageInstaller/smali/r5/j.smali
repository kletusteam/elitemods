.class public final Lr5/j;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lr5/j$a;,
        Lr5/j$b;
    }
.end annotation


# static fields
.field public static final b:Lr5/j$a;


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lr5/j$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lr5/j$a;-><init>(Lm8/g;)V

    sput-object v0, Lr5/j;->b:Lr5/j$a;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const-string v0, "mContext"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lr5/j;->a:Landroid/content/Context;

    return-void
.end method

.method private static final A(Lr5/j;)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lr5/j;->K()V

    return-void
.end method

.method private static final B(Lr5/j;)V
    .locals 11

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lr5/j$b;

    iget-object v3, p0, Lr5/j;->a:Landroid/content/Context;

    const v1, 0x7f1100e2

    invoke-virtual {v3, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v1, "mContext.getString(R.str\u2026ity_mode_guide_elder_msg)"

    invoke-static {v4, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const v5, 0x7f0d01a7

    const-string v6, "elder"

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x30

    const/4 v10, 0x0

    move-object v1, v0

    move-object v2, p0

    invoke-direct/range {v1 .. v10}, Lr5/j$b;-><init>(Lr5/j;Landroid/content/Context;Ljava/lang/CharSequence;ILjava/lang/String;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILm8/g;)V

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method private static final C(Lr5/j;Landroid/net/Uri;)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$data"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lr5/j;->y(Landroid/net/Uri;)V

    return-void
.end method

.method private static final D(Lr5/j;)V
    .locals 11

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lr5/j$b;

    iget-object v3, p0, Lr5/j;->a:Landroid/content/Context;

    const v1, 0x7f1100e1

    invoke-virtual {v3, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v1, "mContext.getString(R.str\u2026de_guide_delete_apps_msg)"

    invoke-static {v4, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const v5, 0x7f0d01a5

    const-string v6, "normal"

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x30

    const/4 v10, 0x0

    move-object v1, v0

    move-object v2, p0

    invoke-direct/range {v1 .. v10}, Lr5/j$b;-><init>(Lr5/j;Landroid/content/Context;Ljava/lang/CharSequence;ILjava/lang/String;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILm8/g;)V

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method private static final E(Lr5/j;Landroid/net/Uri;)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$data"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lr5/j;->y(Landroid/net/Uri;)V

    return-void
.end method

.method private static final F(Lr5/j;Lcom/miui/packageInstaller/model/AppStoreGuideOpenSafeModePopTips;)V
    .locals 9

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lr5/j$b;

    iget-object v3, p0, Lr5/j;->a:Landroid/content/Context;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/miui/packageInstaller/model/AppStoreGuideOpenSafeModePopTips;->getContent()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    iget-object v1, p0, Lr5/j;->a:Landroid/content/Context;

    const v2, 0x7f1100e2

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "mContext.getString(R.str\u2026ity_mode_guide_elder_msg)"

    invoke-static {v1, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_1
    move-object v4, v1

    const v5, 0x7f0d01a6

    const/4 v1, 0x0

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/miui/packageInstaller/model/AppStoreGuideOpenSafeModePopTips;->getTitle()Ljava/lang/String;

    move-result-object v2

    move-object v7, v2

    goto :goto_0

    :cond_2
    move-object v7, v1

    :goto_0
    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lcom/miui/packageInstaller/model/AppStoreGuideOpenSafeModePopTips;->getButton()Ljava/lang/String;

    move-result-object p1

    move-object v8, p1

    goto :goto_1

    :cond_3
    move-object v8, v1

    :goto_1
    const-string v6, "child_mode"

    move-object v1, v0

    move-object v2, p0

    invoke-direct/range {v1 .. v8}, Lr5/j$b;-><init>(Lr5/j;Landroid/content/Context;Ljava/lang/CharSequence;ILjava/lang/String;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method private static final G(Lr5/j;Landroid/net/Uri;)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$data"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lr5/j;->y(Landroid/net/Uri;)V

    return-void
.end method

.method private final H()V
    .locals 4

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iget-object v1, p0, Lr5/j;->a:Landroid/content/Context;

    const-class v2, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    new-instance v1, Lo5/b;

    const-string v2, "security_guide"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lo5/b;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "fromPage"

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v1, "safe_mode_type"

    const-string v3, "packageinstaller"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "safe_mode_ref"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lr5/j;->a:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private final I()V
    .locals 5

    new-instance v0, Lp5/b;

    new-instance v1, Lo5/b;

    const-string v2, "miui_font_setting"

    const/4 v3, 0x0

    const/4 v4, 0x2

    invoke-direct {v1, v2, v3, v4, v3}, Lo5/b;-><init>(Ljava/lang/String;Ljava/lang/String;ILm8/g;)V

    const-string v2, "protect_mode_guidance_toast_open_btn"

    const-string v3, "button"

    invoke-direct {v0, v2, v3, v1}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    return-void
.end method

.method private final J(Ljava/lang/String;Z)V
    .locals 2

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    const/4 v1, 0x1

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    const-string v0, "easy_mode"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :sswitch_1
    const-string v0, "child_mode"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :sswitch_2
    const-string v0, "elder"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :sswitch_3
    const-string v0, "normal"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lr5/j;->a:Landroid/content/Context;

    invoke-static {v0, v1, p1}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->setSafeModelEnabled(Landroid/content/Context;ZLjava/lang/String;)V

    :goto_0
    if-eqz p2, :cond_1

    invoke-direct {p0, p1}, Lr5/j;->M(Ljava/lang/String;)V

    :cond_1
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x3df94319 -> :sswitch_3
        0x5c1e20a -> :sswitch_2
        0x50c2cee6 -> :sswitch_1
        0x72a6bfc0 -> :sswitch_0
    .end sparse-switch
.end method

.method private final K()V
    .locals 13

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "miui_packageinstaller://com.miui.packageinstaller/safe_mode"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    iget-object v2, p0, Lr5/j;->a:Landroid/content/Context;

    const/4 v3, 0x0

    const/high16 v4, 0xc000000

    invoke-static {v2, v3, v0, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v11

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "security://security_mode/open?style=elder&safe_mode_ref=notification_from"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    iget-object v1, p0, Lr5/j;->a:Landroid/content/Context;

    invoke-static {v1, v3, v0, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v12

    iget-object v0, p0, Lr5/j;->a:Landroid/content/Context;

    const v1, 0x7f110326

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    const-string v0, "mContext.getString(R.str\u2026e_open_guide_elder_title)"

    invoke-static {v7, v0}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lr5/j;->a:Landroid/content/Context;

    const v1, 0x7f110325

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    const-string v0, "mContext.getString(R.str\u2026ode_open_guide_elder_msg)"

    invoke-static {v8, v0}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lr5/j;->a:Landroid/content/Context;

    const v1, 0x7f110324

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    const-string v0, "mContext.getString(R.str\u2026uide_elder_action_button)"

    invoke-static {v10, v0}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "contentPendingIntent"

    invoke-static {v11, v0}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "actionPendingIntent"

    invoke-static {v12, v0}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v6, 0x65

    const v9, 0x7f080588

    move-object v5, p0

    invoke-direct/range {v5 .. v12}, Lr5/j;->L(ILjava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V

    invoke-direct {p0}, Lr5/j;->j()V

    return-void
.end method

.method private final L(ILjava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V
    .locals 4

    iget-object v0, p0, Lr5/j;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/packageinstaller/compat/NotificationCompat;->getNotificationBuilder(Landroid/content/Context;)Landroid/app/Notification$Builder;

    move-result-object v0

    new-instance v1, Landroid/widget/RemoteViews;

    iget-object v2, p0, Lr5/j;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0d008a

    invoke-direct {v1, v2, v3}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    const v2, 0x7f0a0380

    invoke-virtual {v1, v2, p2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    const p2, 0x7f0a0255

    invoke-virtual {v1, p2, p3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    const p2, 0x7f0a0198

    invoke-virtual {v1, p2, p4}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    const p2, 0x7f0a0046

    invoke-virtual {v1, p2, p5}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    invoke-virtual {v1, p2, p7}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setCustomContentView(Landroid/widget/RemoteViews;)Landroid/app/Notification$Builder;

    invoke-virtual {v0, p6}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    invoke-virtual {v0, p4}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    const/4 p2, 0x1

    invoke-virtual {v0, p2}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    iget-object p2, p0, Lr5/j;->a:Landroid/content/Context;

    const-string p3, "notification"

    invoke-virtual {p2, p3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p2

    const-string p3, "null cannot be cast to non-null type android.app.NotificationManager"

    invoke-static {p2, p3}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Landroid/app/NotificationManager;

    iget-object p3, p0, Lr5/j;->a:Landroid/content/Context;

    invoke-static {p2, p3}, Lcom/android/packageinstaller/compat/NotificationCompat;->createNotificationChannel(Landroid/app/NotificationManager;Landroid/content/Context;)V

    invoke-virtual {v0}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object p3

    invoke-virtual {p2, p1, p3}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    return-void
.end method

.method private final M(Ljava/lang/String;)V
    .locals 12

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "security://security_mode_advantage?safe_mode_type=notification_in&safe_mode_ref=notification_from"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "&style="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    iget-object v1, p0, Lr5/j;->a:Landroid/content/Context;

    const/4 v2, 0x0

    const/high16 v3, 0xc000000

    invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v11

    iget-object v0, p0, Lr5/j;->a:Landroid/content/Context;

    const v1, 0x7f110329

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "mContext.getString(R.str\u2026_open_notification_title)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lr5/j;->a:Landroid/content/Context;

    const v2, 0x7f110328

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "mContext.getString(R.str\u2026de_open_notification_msg)"

    invoke-static {v1, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "child_mode"

    invoke-static {p1, v2}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    sget-object p1, Lf6/s;->a:Lf6/s$a;

    invoke-virtual {p1}, Lf6/s$a;->a()Lf6/s;

    move-result-object p1

    const-string v2, "smofcTips"

    invoke-virtual {p1, v2}, Lf6/s;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-class v2, Lcom/miui/packageInstaller/model/SafeModeOpenedFloatCardTips;

    invoke-static {p1, v2}, Lcom/android/packageinstaller/utils/j;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/miui/packageInstaller/model/SafeModeOpenedFloatCardTips;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/miui/packageInstaller/model/SafeModeOpenedFloatCardTips;->getTitle()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    move-object v0, v2

    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/miui/packageInstaller/model/SafeModeOpenedFloatCardTips;->getContent()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_1

    move-object v1, p1

    :cond_1
    invoke-direct {p0}, Lr5/j;->k()V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lr5/j;->l()V

    :goto_0
    move-object v6, v0

    move-object v7, v1

    const/16 v5, 0x65

    const v8, 0x7f080588

    iget-object p1, p0, Lr5/j;->a:Landroid/content/Context;

    const v0, 0x7f110327

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    const-string p1, "mContext.getString(R.str\u2026tification_action_button)"

    invoke-static {v9, p1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "pendingIntent"

    invoke-static {v11, p1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v4, p0

    move-object v10, v11

    invoke-direct/range {v4 .. v11}, Lr5/j;->L(ILjava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object p1

    sget-object v0, Lr5/i;->a:Lr5/i;

    const-wide/16 v1, 0x1388

    invoke-virtual {p1, v0, v1, v2}, Lf6/z;->d(Ljava/lang/Runnable;J)V

    return-void
.end method

.method private static final N()V
    .locals 2

    invoke-static {}, Lcom/android/packageinstaller/InstallerApplication;->g()Lcom/android/packageinstaller/InstallerApplication;

    move-result-object v0

    const-string v1, "notification"

    invoke-virtual {v0, v1}, Landroid/app/Application;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    const-string v1, "null cannot be cast to non-null type android.app.NotificationManager"

    invoke-static {v0, v1}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/app/NotificationManager;

    const/16 v1, 0x65

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    return-void
.end method

.method public static synthetic a(Lr5/j;)V
    .locals 0

    invoke-static {p0}, Lr5/j;->B(Lr5/j;)V

    return-void
.end method

.method public static synthetic b(Lr5/j;)V
    .locals 0

    invoke-static {p0}, Lr5/j;->A(Lr5/j;)V

    return-void
.end method

.method public static synthetic c(Lr5/j;Landroid/net/Uri;)V
    .locals 0

    invoke-static {p0, p1}, Lr5/j;->x(Lr5/j;Landroid/net/Uri;)V

    return-void
.end method

.method public static synthetic d(Lr5/j;Lcom/miui/packageInstaller/model/AppStoreGuideOpenSafeModePopTips;)V
    .locals 0

    invoke-static {p0, p1}, Lr5/j;->F(Lr5/j;Lcom/miui/packageInstaller/model/AppStoreGuideOpenSafeModePopTips;)V

    return-void
.end method

.method public static synthetic e(Lr5/j;Landroid/net/Uri;)V
    .locals 0

    invoke-static {p0, p1}, Lr5/j;->G(Lr5/j;Landroid/net/Uri;)V

    return-void
.end method

.method public static synthetic f(Lr5/j;)V
    .locals 0

    invoke-static {p0}, Lr5/j;->D(Lr5/j;)V

    return-void
.end method

.method public static synthetic g(Lr5/j;Landroid/net/Uri;)V
    .locals 0

    invoke-static {p0, p1}, Lr5/j;->C(Lr5/j;Landroid/net/Uri;)V

    return-void
.end method

.method public static synthetic h(Lr5/j;Landroid/net/Uri;)V
    .locals 0

    invoke-static {p0, p1}, Lr5/j;->E(Lr5/j;Landroid/net/Uri;)V

    return-void
.end method

.method public static synthetic i()V
    .locals 0

    invoke-static {}, Lr5/j;->N()V

    return-void
.end method

.method private final j()V
    .locals 7

    new-instance v0, Lp5/g;

    new-instance v1, Lo5/b;

    const-string v2, "miui_font_setting"

    const/4 v3, 0x0

    const/4 v4, 0x2

    invoke-direct {v1, v2, v3, v4, v3}, Lo5/b;-><init>(Ljava/lang/String;Ljava/lang/String;ILm8/g;)V

    const-string v5, "protect_mode_guidance_toast"

    const-string v6, "toast"

    invoke-direct {v0, v5, v6, v1}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    new-instance v0, Lp5/g;

    new-instance v1, Lo5/b;

    invoke-direct {v1, v2, v3, v4, v3}, Lo5/b;-><init>(Ljava/lang/String;Ljava/lang/String;ILm8/g;)V

    const-string v2, "protect_mode_guidance_toast_open_btn"

    const-string v3, "button"

    invoke-direct {v0, v2, v3, v1}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    return-void
.end method

.method private final k()V
    .locals 7

    new-instance v0, Lp5/g;

    new-instance v1, Lo5/b;

    const-string v2, "appstore_for_child"

    const/4 v3, 0x0

    const/4 v4, 0x2

    invoke-direct {v1, v2, v3, v4, v3}, Lo5/b;-><init>(Ljava/lang/String;Ljava/lang/String;ILm8/g;)V

    const-string v5, "safe_mode_opened_toast"

    const-string v6, "toast"

    invoke-direct {v0, v5, v6, v1}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    new-instance v0, Lp5/g;

    new-instance v1, Lo5/b;

    invoke-direct {v1, v2, v3, v4, v3}, Lo5/b;-><init>(Ljava/lang/String;Ljava/lang/String;ILm8/g;)V

    const-string v2, "safe_mode_opened_toast_know_btn"

    const-string v3, "button"

    invoke-direct {v0, v2, v3, v1}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    return-void
.end method

.method private final l()V
    .locals 7

    new-instance v0, Lp5/g;

    new-instance v1, Lo5/b;

    const-string v2, "appstore_for_old"

    const/4 v3, 0x0

    const/4 v4, 0x2

    invoke-direct {v1, v2, v3, v4, v3}, Lo5/b;-><init>(Ljava/lang/String;Ljava/lang/String;ILm8/g;)V

    const-string v5, "protect_mode_opened_toast"

    const-string v6, "toast"

    invoke-direct {v0, v5, v6, v1}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    new-instance v0, Lp5/g;

    new-instance v1, Lo5/b;

    invoke-direct {v1, v2, v3, v4, v3}, Lo5/b;-><init>(Ljava/lang/String;Ljava/lang/String;ILm8/g;)V

    const-string v2, "protect_mode_opened_toast_know_btn"

    const-string v3, "button"

    invoke-direct {v0, v2, v3, v1}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    return-void
.end method

.method public static final synthetic m(Lr5/j;)V
    .locals 0

    invoke-direct {p0}, Lr5/j;->r()V

    return-void
.end method

.method public static final synthetic n(Lr5/j;)V
    .locals 0

    invoke-direct {p0}, Lr5/j;->s()V

    return-void
.end method

.method public static final synthetic o(Lr5/j;)V
    .locals 0

    invoke-direct {p0}, Lr5/j;->t()V

    return-void
.end method

.method public static final synthetic p(Lr5/j;)V
    .locals 0

    invoke-direct {p0}, Lr5/j;->H()V

    return-void
.end method

.method public static final synthetic q(Lr5/j;Ljava/lang/String;Z)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lr5/j;->J(Ljava/lang/String;Z)V

    return-void
.end method

.method private final r()V
    .locals 7

    new-instance v0, Lp5/g;

    new-instance v1, Lo5/b;

    const-string v2, "appstore_for_child"

    const/4 v3, 0x0

    const/4 v4, 0x2

    invoke-direct {v1, v2, v3, v4, v3}, Lo5/b;-><init>(Ljava/lang/String;Ljava/lang/String;ILm8/g;)V

    const-string v5, "safe_mode_guidance_popup"

    const-string v6, "popup"

    invoke-direct {v0, v5, v6, v1}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    new-instance v0, Lp5/g;

    new-instance v1, Lo5/b;

    invoke-direct {v1, v2, v3, v4, v3}, Lo5/b;-><init>(Ljava/lang/String;Ljava/lang/String;ILm8/g;)V

    const-string v5, "safe_mode_guidance_popup_know_btn"

    const-string v6, "button"

    invoke-direct {v0, v5, v6, v1}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    new-instance v0, Lp5/g;

    new-instance v1, Lo5/b;

    invoke-direct {v1, v2, v3, v4, v3}, Lo5/b;-><init>(Ljava/lang/String;Ljava/lang/String;ILm8/g;)V

    const-string v5, "safe_mode_guidance_popup_open_btn"

    invoke-direct {v0, v5, v6, v1}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    new-instance v0, Lp5/g;

    new-instance v1, Lo5/b;

    invoke-direct {v1, v2, v3, v4, v3}, Lo5/b;-><init>(Ljava/lang/String;Ljava/lang/String;ILm8/g;)V

    const-string v2, "safe_mode_guidance_popup_cancel_btn"

    invoke-direct {v0, v2, v6, v1}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    return-void
.end method

.method private final s()V
    .locals 7

    new-instance v0, Lp5/g;

    new-instance v1, Lo5/b;

    const-string v2, "appstore_for_old"

    const/4 v3, 0x0

    const/4 v4, 0x2

    invoke-direct {v1, v2, v3, v4, v3}, Lo5/b;-><init>(Ljava/lang/String;Ljava/lang/String;ILm8/g;)V

    const-string v5, "protect_mode_guidance_popup"

    const-string v6, "popup"

    invoke-direct {v0, v5, v6, v1}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    new-instance v0, Lp5/g;

    new-instance v1, Lo5/b;

    invoke-direct {v1, v2, v3, v4, v3}, Lo5/b;-><init>(Ljava/lang/String;Ljava/lang/String;ILm8/g;)V

    const-string v5, "protect_mode_guidance_popup_know_btn"

    const-string v6, "button"

    invoke-direct {v0, v5, v6, v1}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    new-instance v0, Lp5/g;

    new-instance v1, Lo5/b;

    invoke-direct {v1, v2, v3, v4, v3}, Lo5/b;-><init>(Ljava/lang/String;Ljava/lang/String;ILm8/g;)V

    const-string v5, "protect_mode_guidance_popup_cancel_btn"

    invoke-direct {v0, v5, v6, v1}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    new-instance v0, Lp5/g;

    new-instance v1, Lo5/b;

    invoke-direct {v1, v2, v3, v4, v3}, Lo5/b;-><init>(Ljava/lang/String;Ljava/lang/String;ILm8/g;)V

    const-string v2, "protect_mode_guidance_popup_open_btn"

    invoke-direct {v0, v2, v6, v1}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    return-void
.end method

.method private final t()V
    .locals 7

    new-instance v0, Lp5/g;

    new-instance v1, Lo5/b;

    const-string v2, "miui_desktop"

    const/4 v3, 0x0

    const/4 v4, 0x2

    invoke-direct {v1, v2, v3, v4, v3}, Lo5/b;-><init>(Ljava/lang/String;Ljava/lang/String;ILm8/g;)V

    const-string v5, "safe_mode_guidance_popup"

    const-string v6, "popup"

    invoke-direct {v0, v5, v6, v1}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    new-instance v0, Lp5/g;

    new-instance v1, Lo5/b;

    invoke-direct {v1, v2, v3, v4, v3}, Lo5/b;-><init>(Ljava/lang/String;Ljava/lang/String;ILm8/g;)V

    const-string v5, "safe_mode_guidance_popup_know_btn"

    const-string v6, "button"

    invoke-direct {v0, v5, v6, v1}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    new-instance v0, Lp5/g;

    new-instance v1, Lo5/b;

    invoke-direct {v1, v2, v3, v4, v3}, Lo5/b;-><init>(Ljava/lang/String;Ljava/lang/String;ILm8/g;)V

    const-string v5, "safe_mode_guidance_popup_open_btn"

    invoke-direct {v0, v5, v6, v1}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    new-instance v0, Lp5/g;

    new-instance v1, Lo5/b;

    invoke-direct {v1, v2, v3, v4, v3}, Lo5/b;-><init>(Ljava/lang/String;Ljava/lang/String;ILm8/g;)V

    const-string v2, "safe_mode_guidance_popup_cancel_btn"

    invoke-direct {v0, v2, v6, v1}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    return-void
.end method

.method private final u(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lr5/j;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->getSecurityModeStyle(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lr5/j;->a:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, v1, p1}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->setSafeModelEnabled(Landroid/content/Context;ZLjava/lang/String;)V

    :cond_0
    return-void
.end method

.method private static final x(Lr5/j;Landroid/net/Uri;)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$data"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lr5/j;->z(Landroid/net/Uri;)V

    return-void
.end method

.method private final y(Landroid/net/Uri;)V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    iget-object p1, p0, Lr5/j;->a:Landroid/content/Context;

    const-class v1, Lcom/miui/packageInstaller/guide/SecurityModeGuideUI;

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const/high16 p1, 0x10000000

    invoke-virtual {v0, p1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    iget-object p1, p0, Lr5/j;->a:Landroid/content/Context;

    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private final z(Landroid/net/Uri;)V
    .locals 11

    iget-object v0, p0, Lr5/j;->a:Landroid/content/Context;

    const-string v1, "security_mode_guide_config"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "behavior"

    invoke-virtual {p1, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    const-string v1, ""

    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "last_show_time_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-wide/16 v5, 0x0

    invoke-interface {v0, v3, v5, v6}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v7

    new-instance v3, Ljava/text/SimpleDateFormat;

    const-string v9, "yyyyMMdd"

    invoke-direct {v3, v9}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    new-instance v9, Ljava/util/Date;

    invoke-direct {v9, v7, v8}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v3, v9}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    invoke-direct {v8, v9, v10}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v3, v8}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v7, v3}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    return-void

    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "guide_enable_"

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v7, 0x1

    invoke-interface {v0, v3, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_2

    return-void

    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "guide_delay_"

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3, v5, v6}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v3

    const/4 v8, 0x2

    const-string v9, "guide_show_count_"

    sparse-switch v3, :sswitch_data_0

    goto/16 :goto_3

    :sswitch_0
    const-string v3, "install_elder_app"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    goto/16 :goto_3

    :cond_3
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    if-le v2, v8, :cond_4

    return-void

    :cond_4
    iget-object v3, p0, Lr5/j;->a:Landroid/content/Context;

    instance-of v3, v3, Lcom/miui/packageInstaller/guide/SecurityModeGuideUI;

    if-eqz v3, :cond_5

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object p1

    new-instance v3, Lr5/a;

    invoke-direct {v3, p0}, Lr5/a;-><init>(Lr5/j;)V

    invoke-virtual {p1, v3}, Lf6/z;->e(Ljava/lang/Runnable;)V

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-interface {p1, v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/16 :goto_2

    :cond_5
    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object v0

    new-instance v1, Lr5/f;

    invoke-direct {v1, p0, p1}, Lr5/f;-><init>(Lr5/j;Landroid/net/Uri;)V

    :goto_0
    invoke-virtual {v0, v1, v5, v6}, Lf6/z;->d(Ljava/lang/Runnable;J)V

    goto/16 :goto_3

    :sswitch_1
    const-string p1, "set_font_size"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_6

    goto/16 :goto_3

    :cond_6
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result p1

    if-le p1, v8, :cond_7

    return-void

    :cond_7
    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object v2

    new-instance v3, Lr5/b;

    invoke-direct {v3, p0}, Lr5/b;-><init>(Lr5/j;)V

    invoke-virtual {v2, v3, v5, v6}, Lf6/z;->d(Ljava/lang/Runnable;J)V

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-interface {v0, v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    add-int/2addr p1, v7

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto/16 :goto_3

    :sswitch_2
    const-string v3, "install_child_app"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    goto/16 :goto_3

    :cond_8
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    if-le v2, v8, :cond_9

    return-void

    :cond_9
    sget-object v3, Lf6/s;->a:Lf6/s$a;

    invoke-virtual {v3}, Lf6/s$a;->a()Lf6/s;

    move-result-object v3

    const-string v5, "minorASGOSMPTips"

    invoke-virtual {v3, v5}, Lf6/s;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-class v5, Lcom/miui/packageInstaller/model/AppStoreGuideOpenSafeModePopTips;

    invoke-static {v3, v5}, Lcom/android/packageinstaller/utils/j;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/miui/packageInstaller/model/AppStoreGuideOpenSafeModePopTips;

    iget-object v5, p0, Lr5/j;->a:Landroid/content/Context;

    instance-of v5, v5, Lcom/miui/packageInstaller/guide/SecurityModeGuideUI;

    if-eqz v5, :cond_a

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object p1

    new-instance v5, Lr5/h;

    invoke-direct {v5, p0, v3}, Lr5/h;-><init>(Lr5/j;Lcom/miui/packageInstaller/model/AppStoreGuideOpenSafeModePopTips;)V

    invoke-virtual {p1, v5}, Lf6/z;->e(Ljava/lang/Runnable;)V

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-interface {p1, v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    :goto_2
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    add-int/2addr v2, v7

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    goto :goto_1

    :cond_a
    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object v0

    new-instance v1, Lr5/e;

    invoke-direct {v1, p0, p1}, Lr5/e;-><init>(Lr5/j;Landroid/net/Uri;)V

    invoke-virtual {v0, v1}, Lf6/z;->e(Ljava/lang/Runnable;)V

    goto/16 :goto_3

    :sswitch_3
    const-string v3, "delete_apps"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_b

    goto/16 :goto_3

    :cond_b
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    if-le v3, v8, :cond_c

    return-void

    :cond_c
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "guide_do_not_show_again_"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v0, v8, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_d

    return-void

    :cond_d
    iget-object v2, p0, Lr5/j;->a:Landroid/content/Context;

    instance-of v2, v2, Lcom/miui/packageInstaller/guide/SecurityModeGuideUI;

    if-eqz v2, :cond_e

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object p1

    new-instance v2, Lr5/c;

    invoke-direct {v2, p0}, Lr5/c;-><init>(Lr5/j;)V

    invoke-virtual {p1, v2}, Lf6/z;->e(Ljava/lang/Runnable;)V

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-interface {p1, v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    add-int/2addr v3, v7

    invoke-interface {p1, v0, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    goto/16 :goto_1

    :cond_e
    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object v0

    new-instance v1, Lr5/g;

    invoke-direct {v1, p0, p1}, Lr5/g;-><init>(Lr5/j;Landroid/net/Uri;)V

    goto/16 :goto_0

    :goto_3
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        -0x42e62f7a -> :sswitch_3
        -0x36ea3526 -> :sswitch_2
        -0xef4f5ac -> :sswitch_1
        0x7d7790c8 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final v(Landroid/content/Intent;)V
    .locals 1

    const-string v0, "intent"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object p1

    if-nez p1, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0, p1}, Lr5/j;->w(Landroid/net/Uri;)V

    return-void
.end method

.method public final w(Landroid/net/Uri;)V
    .locals 5

    const-string v0, "data"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_8

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    const v2, 0x2ca76f9

    const-string v3, ""

    const-string v4, "style"

    if-eq v1, v2, :cond_5

    const v2, 0x55d9a329

    if-eq v1, v2, :cond_2

    const v2, 0x5616002d

    if-eq v1, v2, :cond_0

    goto :goto_2

    :cond_0
    const-string v1, "/guide"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    goto :goto_2

    :cond_1
    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object v0

    new-instance v1, Lr5/d;

    invoke-direct {v1, p0, p1}, Lr5/d;-><init>(Lr5/j;Landroid/net/Uri;)V

    invoke-virtual {v0, v1}, Lf6/z;->g(Ljava/lang/Runnable;)V

    goto :goto_3

    :cond_2
    const-string v1, "/close"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    goto :goto_2

    :cond_3
    invoke-virtual {p1, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-nez p1, :cond_4

    goto :goto_0

    :cond_4
    move-object v3, p1

    :goto_0
    invoke-direct {p0, v3}, Lr5/j;->u(Ljava/lang/String;)V

    goto :goto_3

    :cond_5
    const-string v1, "/open"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    goto :goto_2

    :cond_6
    invoke-virtual {p1, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_7

    goto :goto_1

    :cond_7
    move-object v3, v0

    :goto_1
    const/4 v0, 0x1

    const-string v1, "showNotification"

    invoke-virtual {p1, v1, v0}, Landroid/net/Uri;->getBooleanQueryParameter(Ljava/lang/String;Z)Z

    move-result v0

    invoke-direct {p0, v3, v0}, Lr5/j;->J(Ljava/lang/String;Z)V

    const-string v0, "safe_mode_ref"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "notification_from"

    invoke-static {p1, v0}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_9

    invoke-direct {p0}, Lr5/j;->I()V

    goto :goto_3

    :cond_8
    :goto_2
    iget-object p1, p0, Lr5/j;->a:Landroid/content/Context;

    instance-of v0, p1, Landroid/app/Activity;

    if-eqz v0, :cond_9

    check-cast p1, Landroid/app/Activity;

    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    :cond_9
    :goto_3
    return-void
.end method
