.class public final Lr5/j$b;
.super La6/e;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lr5/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "b"
.end annotation


# instance fields
.field private final k:Landroid/content/Context;

.field private final l:Ljava/lang/CharSequence;

.field private final m:I

.field private final n:Ljava/lang/String;

.field private final o:Ljava/lang/CharSequence;

.field private final p:Ljava/lang/CharSequence;

.field final synthetic q:Lr5/j;


# direct methods
.method public constructor <init>(Lr5/j;Landroid/content/Context;Ljava/lang/CharSequence;ILjava/lang/String;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/CharSequence;",
            "I",
            "Ljava/lang/String;",
            "Ljava/lang/CharSequence;",
            "Ljava/lang/CharSequence;",
            ")V"
        }
    .end annotation

    const-string v0, "mContext"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "msg"

    invoke-static {p3, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "securityModeStyle"

    invoke-static {p5, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lr5/j$b;->q:Lr5/j;

    invoke-direct {p0, p2}, La6/e;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lr5/j$b;->k:Landroid/content/Context;

    iput-object p3, p0, Lr5/j$b;->l:Ljava/lang/CharSequence;

    iput p4, p0, Lr5/j$b;->m:I

    iput-object p5, p0, Lr5/j$b;->n:Ljava/lang/String;

    iput-object p6, p0, Lr5/j$b;->o:Ljava/lang/CharSequence;

    iput-object p7, p0, Lr5/j$b;->p:Ljava/lang/CharSequence;

    return-void
.end method

.method public synthetic constructor <init>(Lr5/j;Landroid/content/Context;Ljava/lang/CharSequence;ILjava/lang/String;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILm8/g;)V
    .locals 10

    and-int/lit8 v0, p8, 0x10

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    move-object v8, v1

    goto :goto_0

    :cond_0
    move-object/from16 v8, p6

    :goto_0
    and-int/lit8 v0, p8, 0x20

    if-eqz v0, :cond_1

    move-object v9, v1

    goto :goto_1

    :cond_1
    move-object/from16 v9, p7

    :goto_1
    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move v6, p4

    move-object v7, p5

    invoke-direct/range {v2 .. v9}, Lr5/j$b;-><init>(Lr5/j;Landroid/content/Context;Ljava/lang/CharSequence;ILjava/lang/String;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    return-void
.end method

.method public static synthetic g(Lr5/j$b;Landroid/content/DialogInterface;)V
    .locals 0

    invoke-static {p0, p1}, Lr5/j$b;->o(Lr5/j$b;Landroid/content/DialogInterface;)V

    return-void
.end method

.method public static synthetic h(Lr5/j$b;Lr5/j;Lm8/t;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1, p2, p3}, Lr5/j$b;->n(Lr5/j$b;Lr5/j;Lm8/t;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic i(Lr5/j$b;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Lr5/j$b;->l(Lr5/j$b;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic j(Lr5/j$b;)V
    .locals 0

    invoke-static {p0}, Lr5/j$b;->q(Lr5/j$b;)V

    return-void
.end method

.method public static synthetic k(Lr5/j$b;Lm8/t;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lr5/j$b;->m(Lr5/j$b;Lm8/t;Landroid/view/View;)V

    return-void
.end method

.method private static final l(Lr5/j$b;Landroid/view/View;)V
    .locals 5

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p0, Lr5/j$b;->k:Landroid/content/Context;

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lr5/j$b;->k:Landroid/content/Context;

    const-class v2, Lcom/miui/packageInstaller/ui/SecureModeAdvantageActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Landroid/app/Dialog;->dismiss()V

    iget-object p1, p0, Lr5/j$b;->n:Ljava/lang/String;

    const-string v0, "normal"

    invoke-static {v0, p1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    const-string v0, "safe_mode_guidance_popup_know_btn"

    const/4 v1, 0x2

    const-string v2, "button"

    const/4 v3, 0x0

    if-eqz p1, :cond_0

    new-instance p0, Lp5/b;

    new-instance p1, Lo5/b;

    const-string v4, "miui_desktop"

    invoke-direct {p1, v4, v3, v1, v3}, Lo5/b;-><init>(Ljava/lang/String;Ljava/lang/String;ILm8/g;)V

    invoke-direct {p0, v0, v2, p1}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    :goto_0
    invoke-virtual {p0}, Lp5/f;->c()Z

    goto :goto_1

    :cond_0
    iget-object p1, p0, Lr5/j$b;->n:Ljava/lang/String;

    const-string v4, "elder"

    invoke-static {v4, p1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    new-instance p0, Lp5/b;

    new-instance p1, Lo5/b;

    const-string v0, "appstore_for_old"

    invoke-direct {p1, v0, v3, v1, v3}, Lo5/b;-><init>(Ljava/lang/String;Ljava/lang/String;ILm8/g;)V

    const-string v0, "protect_mode_guidance_popup_know_btn"

    invoke-direct {p0, v0, v2, p1}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    goto :goto_0

    :cond_1
    iget-object p0, p0, Lr5/j$b;->n:Ljava/lang/String;

    const-string p1, "child_mode"

    invoke-static {p1, p0}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_2

    new-instance p0, Lp5/b;

    new-instance p1, Lo5/b;

    const-string v4, "appstore_for_child"

    invoke-direct {p1, v4, v3, v1, v3}, Lo5/b;-><init>(Ljava/lang/String;Ljava/lang/String;ILm8/g;)V

    invoke-direct {p0, v0, v2, p1}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    goto :goto_0

    :cond_2
    :goto_1
    return-void
.end method

.method private static final m(Lr5/j$b;Lm8/t;Landroid/view/View;)V
    .locals 5

    const-string p2, "this$0"

    invoke-static {p0, p2}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "$checkBoxView"

    invoke-static {p1, p2}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/app/Dialog;->dismiss()V

    iget-object p2, p1, Lm8/t;->a:Ljava/lang/Object;

    check-cast p2, Landroid/widget/CheckBox;

    invoke-direct {p0, p2}, Lr5/j$b;->p(Landroid/widget/CheckBox;)V

    iget-object p2, p0, Lr5/j$b;->n:Ljava/lang/String;

    const-string v0, "normal"

    invoke-static {v0, p2}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    const-string v0, "safe_mode_guidance_popup_cancel_btn"

    const/4 v1, 0x2

    const-string v2, "button"

    const/4 v3, 0x0

    if-eqz p2, :cond_2

    new-instance p0, Lp5/b;

    new-instance p2, Lo5/b;

    const-string v4, "miui_desktop"

    invoke-direct {p2, v4, v3, v1, v3}, Lo5/b;-><init>(Ljava/lang/String;Ljava/lang/String;ILm8/g;)V

    invoke-direct {p0, v0, v2, p2}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    iget-object p1, p1, Lm8/t;->a:Ljava/lang/Object;

    check-cast p1, Landroid/widget/CheckBox;

    const/4 p2, 0x1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result p1

    if-ne p1, p2, :cond_0

    goto :goto_0

    :cond_0
    move p2, v0

    :goto_0
    if-eqz p2, :cond_1

    const-string p1, "true"

    goto :goto_1

    :cond_1
    const-string p1, "false"

    :goto_1
    const-string p2, "is_remember"

    invoke-virtual {p0, p2, p1}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p0

    goto :goto_2

    :cond_2
    iget-object p1, p0, Lr5/j$b;->n:Ljava/lang/String;

    const-string p2, "elder"

    invoke-static {p2, p1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    new-instance p0, Lp5/b;

    new-instance p1, Lo5/b;

    const-string p2, "appstore_for_old"

    invoke-direct {p1, p2, v3, v1, v3}, Lo5/b;-><init>(Ljava/lang/String;Ljava/lang/String;ILm8/g;)V

    const-string p2, "protect_mode_guidance_popup_cancel_btn"

    invoke-direct {p0, p2, v2, p1}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    :goto_2
    invoke-virtual {p0}, Lp5/f;->c()Z

    goto :goto_3

    :cond_3
    iget-object p0, p0, Lr5/j$b;->n:Ljava/lang/String;

    const-string p1, "child_mode"

    invoke-static {p1, p0}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    new-instance p0, Lp5/b;

    new-instance p1, Lo5/b;

    const-string p2, "appstore_for_child"

    invoke-direct {p1, p2, v3, v1, v3}, Lo5/b;-><init>(Ljava/lang/String;Ljava/lang/String;ILm8/g;)V

    invoke-direct {p0, v0, v2, p1}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    goto :goto_2

    :cond_4
    :goto_3
    return-void
.end method

.method private static final n(Lr5/j$b;Lr5/j;Lm8/t;Landroid/view/View;)V
    .locals 6

    const-string p3, "this$0"

    invoke-static {p0, p3}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p3, "this$1"

    invoke-static {p1, p3}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p3, "$checkBoxView"

    invoke-static {p2, p3}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget p3, p0, Lr5/j$b;->m:I

    const/4 v0, 0x0

    const/4 v1, 0x1

    const v2, 0x7f0d01a5

    if-ne p3, v2, :cond_0

    iget-object p3, p0, Lr5/j$b;->n:Ljava/lang/String;

    invoke-static {p1, p3, v0}, Lr5/j;->q(Lr5/j;Ljava/lang/String;Z)V

    invoke-static {p1}, Lr5/j;->p(Lr5/j;)V

    goto :goto_0

    :cond_0
    iget-object p3, p0, Lr5/j$b;->n:Ljava/lang/String;

    invoke-static {p1, p3, v1}, Lr5/j;->q(Lr5/j;Ljava/lang/String;Z)V

    :goto_0
    iget-object p1, p2, Lm8/t;->a:Ljava/lang/Object;

    check-cast p1, Landroid/widget/CheckBox;

    invoke-direct {p0, p1}, Lr5/j$b;->p(Landroid/widget/CheckBox;)V

    iget-object p1, p0, Lr5/j$b;->n:Ljava/lang/String;

    const-string p3, "normal"

    invoke-static {p3, p1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    const-string p3, "safe_mode_guidance_popup_open_btn"

    const/4 v2, 0x2

    const-string v3, "button"

    const/4 v4, 0x0

    if-eqz p1, :cond_3

    new-instance p0, Lp5/b;

    new-instance p1, Lo5/b;

    const-string v5, "miui_desktop"

    invoke-direct {p1, v5, v4, v2, v4}, Lo5/b;-><init>(Ljava/lang/String;Ljava/lang/String;ILm8/g;)V

    invoke-direct {p0, p3, v3, p1}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    iget-object p1, p2, Lm8/t;->a:Ljava/lang/Object;

    check-cast p1, Landroid/widget/CheckBox;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result p1

    if-ne p1, v1, :cond_1

    move v0, v1

    :cond_1
    if-eqz v0, :cond_2

    const-string p1, "true"

    goto :goto_1

    :cond_2
    const-string p1, "false"

    :goto_1
    const-string p2, "is_remember"

    invoke-virtual {p0, p2, p1}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object p0

    goto :goto_2

    :cond_3
    iget-object p1, p0, Lr5/j$b;->n:Ljava/lang/String;

    const-string p2, "elder"

    invoke-static {p2, p1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_4

    new-instance p0, Lp5/b;

    new-instance p1, Lo5/b;

    const-string p2, "appstore_for_old"

    invoke-direct {p1, p2, v4, v2, v4}, Lo5/b;-><init>(Ljava/lang/String;Ljava/lang/String;ILm8/g;)V

    const-string p2, "protect_mode_guidance_popup_open_btn"

    invoke-direct {p0, p2, v3, p1}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    :goto_2
    invoke-virtual {p0}, Lp5/f;->c()Z

    goto :goto_3

    :cond_4
    iget-object p0, p0, Lr5/j$b;->n:Ljava/lang/String;

    const-string p1, "child_mode"

    invoke-static {p1, p0}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    new-instance p0, Lp5/b;

    new-instance p1, Lo5/b;

    const-string p2, "appstore_for_child"

    invoke-direct {p1, p2, v4, v2, v4}, Lo5/b;-><init>(Ljava/lang/String;Ljava/lang/String;ILm8/g;)V

    invoke-direct {p0, p3, v3, p1}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    goto :goto_2

    :cond_5
    :goto_3
    return-void
.end method

.method private static final o(Lr5/j$b;Landroid/content/DialogInterface;)V
    .locals 0

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p0, p0, Lr5/j$b;->k:Landroid/content/Context;

    instance-of p1, p0, Landroid/app/Activity;

    if-eqz p1, :cond_0

    check-cast p0, Landroid/app/Activity;

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :cond_0
    return-void
.end method

.method private final p(Landroid/widget/CheckBox;)V
    .locals 1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object p1

    new-instance v0, Lr5/o;

    invoke-direct {v0, p0}, Lr5/o;-><init>(Lr5/j$b;)V

    invoke-virtual {p1, v0}, Lf6/z;->g(Ljava/lang/Runnable;)V

    :cond_0
    invoke-virtual {p0}, Landroid/app/Dialog;->dismiss()V

    return-void
.end method

.method private static final q(Lr5/j$b;)V
    .locals 2

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p0, p0, Lr5/j$b;->k:Landroid/content/Context;

    const-string v0, "security_mode_guide_config"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p0

    invoke-interface {p0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p0

    const-string v0, "guide_do_not_show_again_delete_apps"

    const/4 v1, 0x1

    invoke-interface {p0, v0, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object p0

    invoke-interface {p0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0d0058

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->setContentView(I)V

    iget-object p1, p0, Lr5/j$b;->n:Ljava/lang/String;

    const-string v0, "normal"

    invoke-static {v0, p1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lr5/j$b;->q:Lr5/j;

    invoke-static {p1}, Lr5/j;->o(Lr5/j;)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lr5/j$b;->n:Ljava/lang/String;

    const-string v0, "elder"

    invoke-static {v0, p1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lr5/j$b;->q:Lr5/j;

    invoke-static {p1}, Lr5/j;->n(Lr5/j;)V

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lr5/j$b;->n:Ljava/lang/String;

    const-string v0, "child_mode"

    invoke-static {v0, p1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    iget-object p1, p0, Lr5/j$b;->q:Lr5/j;

    invoke-static {p1}, Lr5/j;->m(Lr5/j;)V

    :cond_2
    :goto_0
    const p1, 0x7f0a0108

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    const v0, 0x7f0a0255

    invoke-virtual {p0, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0a0380

    invoke-virtual {p0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v2, p0, Lr5/j$b;->o:Ljava/lang/CharSequence;

    if-eqz v2, :cond_3

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    iget-object v1, p0, Lr5/j$b;->l:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget v1, p0, Lr5/j$b;->m:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    const p1, 0x7f0a000f

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    new-instance v0, Lr5/l;

    invoke-direct {v0, p0}, Lr5/l;-><init>(Lr5/j$b;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p1, 0x7f0a0338

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    new-instance v0, Lm8/t;

    invoke-direct {v0}, Lm8/t;-><init>()V

    const v1, 0x7f0a00bb

    invoke-virtual {p0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, v0, Lm8/t;->a:Ljava/lang/Object;

    new-instance v1, Lr5/m;

    invoke-direct {v1, p0, v0}, Lr5/m;-><init>(Lr5/j$b;Lm8/t;)V

    invoke-virtual {p1, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-array v1, v2, [Landroid/view/View;

    const/4 v3, 0x0

    aput-object p1, v1, v3

    invoke-static {v1}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object v1

    invoke-interface {v1}, Lmiuix/animation/e;->c()Lmiuix/animation/j;

    move-result-object v1

    new-array v4, v3, [Lmiuix/animation/j$b;

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-interface {v1, v5, v4}, Lmiuix/animation/j;->G(F[Lmiuix/animation/j$b;)Lmiuix/animation/j;

    move-result-object v1

    new-array v4, v3, [Lc9/a;

    invoke-interface {v1, p1, v4}, Lmiuix/animation/j;->z(Landroid/view/View;[Lc9/a;)V

    const p1, 0x7f0a0152

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroidx/appcompat/widget/AppCompatButton;

    iget-object v1, p0, Lr5/j$b;->p:Ljava/lang/CharSequence;

    if-eqz v1, :cond_4

    invoke-virtual {p1, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :cond_4
    iget-object v1, p0, Lr5/j$b;->q:Lr5/j;

    new-instance v4, Lr5/n;

    invoke-direct {v4, p0, v1, v0}, Lr5/n;-><init>(Lr5/j$b;Lr5/j;Lm8/t;)V

    invoke-virtual {p1, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-array v0, v2, [Landroid/view/View;

    aput-object p1, v0, v3

    invoke-static {v0}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object v0

    invoke-interface {v0}, Lmiuix/animation/e;->c()Lmiuix/animation/j;

    move-result-object v0

    new-array v1, v3, [Lmiuix/animation/j$b;

    invoke-interface {v0, v5, v1}, Lmiuix/animation/j;->G(F[Lmiuix/animation/j$b;)Lmiuix/animation/j;

    move-result-object v0

    new-array v1, v3, [Lc9/a;

    invoke-interface {v0, p1, v1}, Lmiuix/animation/j;->z(Landroid/view/View;[Lc9/a;)V

    new-instance p1, Lr5/k;

    invoke-direct {p1, p0}, Lr5/k;-><init>(Lr5/j$b;)V

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    return-void
.end method
