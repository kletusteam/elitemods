.class public Lw1/a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lw1/a$b;
    }
.end annotation


# instance fields
.field private a:Ls1/d;

.field private b:Lw1/a$b;

.field private c:Lw1/c;


# direct methods
.method public constructor <init>(Ls1/d;Lw1/a$b;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lw1/a;->a:Ls1/d;

    iput-object p2, p0, Lw1/a;->b:Lw1/a$b;

    new-instance p1, Lw1/c;

    invoke-direct {p1}, Lw1/c;-><init>()V

    iput-object p1, p0, Lw1/a;->c:Lw1/c;

    return-void
.end method

.method static synthetic a(Lw1/a;)Ls1/d;
    .locals 0

    iget-object p0, p0, Lw1/a;->a:Ls1/d;

    return-object p0
.end method

.method static synthetic b(Lw1/a;)Lw1/c;
    .locals 0

    iget-object p0, p0, Lw1/a;->c:Lw1/c;

    return-object p0
.end method

.method private c(Ljava/lang/String;[Ljava/lang/String;[I)V
    .locals 2

    iget-object v0, p0, Lw1/a;->a:Ls1/d;

    invoke-virtual {v0}, Ls1/d;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Ly1/a;->d(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lw1/a;->a:Ls1/d;

    invoke-virtual {v1, p1, p2, p3}, Ls1/d;->f(Ljava/lang/String;[Ljava/lang/String;[I)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lw1/a;->b:Lw1/a$b;

    if-eqz p1, :cond_0

    invoke-interface {p1, v0}, Lw1/a$b;->i(Z)V

    :cond_0
    return-void
.end method

.method static synthetic d(Lw1/a;Ljava/lang/String;[Ljava/lang/String;[I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lw1/a;->c(Ljava/lang/String;[Ljava/lang/String;[I)V

    return-void
.end method


# virtual methods
.method public e(Ljava/lang/String;Z)V
    .locals 1

    if-nez p2, :cond_1

    iget-object p2, p0, Lw1/a;->a:Ls1/d;

    invoke-virtual {p2}, Ls1/d;->k()Ljava/lang/String;

    move-result-object p2

    invoke-static {p2, p1}, Ly1/a;->d(Ljava/lang/String;Ljava/lang/String;)Z

    move-result p2

    if-eqz p2, :cond_1

    invoke-static {}, Lz1/a;->e()Z

    move-result p2

    if-eqz p2, :cond_0

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "region "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " is same, do not update serverIps"

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lz1/a;->b(Ljava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    iget-object p2, p0, Lw1/a;->c:Lw1/c;

    invoke-virtual {p2, p1}, Lw1/c;->b(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p2

    iget-object v0, p0, Lw1/a;->c:Lw1/c;

    invoke-virtual {v0, p1}, Lw1/c;->a(Ljava/lang/String;)[I

    move-result-object v0

    if-eqz p2, :cond_2

    invoke-direct {p0, p1, p2, v0}, Lw1/a;->c(Ljava/lang/String;[Ljava/lang/String;[I)V

    return-void

    :cond_2
    iget-object p2, p0, Lw1/a;->a:Ls1/d;

    invoke-virtual {p2}, Ls1/d;->b()Ls1/d;

    move-result-object p2

    new-instance v0, Lw1/a$a;

    invoke-direct {v0, p0, p1}, Lw1/a$a;-><init>(Lw1/a;Ljava/lang/String;)V

    invoke-static {p2, p1, v0}, Lw1/f;->a(Ls1/d;Ljava/lang/String;Lv1/j;)V

    return-void
.end method

.method public f()V
    .locals 2

    iget-object v0, p0, Lw1/a;->a:Ls1/d;

    invoke-virtual {v0}, Ls1/d;->k()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lw1/a;->e(Ljava/lang/String;Z)V

    return-void
.end method
