.class Lw1/a$a;
.super Ljava/lang/Object;

# interfaces
.implements Lv1/j;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lw1/a;->e(Ljava/lang/String;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lv1/j<",
        "Lw1/e;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lw1/a;


# direct methods
.method constructor <init>(Lw1/a;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lw1/a$a;->b:Lw1/a;

    iput-object p2, p0, Lw1/a$a;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lw1/e;

    invoke-virtual {p0, p1}, Lw1/a$a;->b(Lw1/e;)V

    return-void
.end method

.method public a(Ljava/lang/Throwable;)V
    .locals 1

    const-string v0, "update server ips fail"

    invoke-static {v0, p1}, Lz1/a;->j(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public b(Lw1/e;)V
    .locals 4

    invoke-virtual {p1}, Lw1/e;->d()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "disable service by server response "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lw1/e;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lz1/a;->g(Ljava/lang/String;)V

    iget-object p1, p0, Lw1/a$a;->b:Lw1/a;

    invoke-static {p1}, Lw1/a;->a(Lw1/a;)Ls1/d;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ls1/d;->D(Z)V

    return-void

    :cond_0
    iget-object v0, p0, Lw1/a$a;->b:Lw1/a;

    invoke-static {v0}, Lw1/a;->a(Lw1/a;)Ls1/d;

    move-result-object v0

    invoke-virtual {v0}, Ls1/d;->C()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lw1/a$a;->b:Lw1/a;

    invoke-static {v0}, Lw1/a;->a(Lw1/a;)Ls1/d;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ls1/d;->D(Z)V

    :cond_1
    invoke-virtual {p1}, Lw1/e;->c()[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lw1/a$a;->b:Lw1/a;

    iget-object v1, p0, Lw1/a$a;->a:Ljava/lang/String;

    invoke-virtual {p1}, Lw1/e;->c()[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lw1/e;->b()[I

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lw1/a;->d(Lw1/a;Ljava/lang/String;[Ljava/lang/String;[I)V

    iget-object v0, p0, Lw1/a$a;->b:Lw1/a;

    invoke-static {v0}, Lw1/a;->b(Lw1/a;)Lw1/c;

    move-result-object v0

    iget-object v1, p0, Lw1/a$a;->a:Ljava/lang/String;

    invoke-virtual {p1}, Lw1/e;->c()[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lw1/e;->b()[I

    move-result-object p1

    invoke-virtual {v0, v1, v2, p1}, Lw1/c;->c(Ljava/lang/String;[Ljava/lang/String;[I)V

    :cond_2
    return-void
.end method
