.class public Lw1/f;
.super Ljava/lang/Object;


# direct methods
.method public static a(Ls1/d;Ljava/lang/String;Lv1/j;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ls1/d;",
            "Ljava/lang/String;",
            "Lv1/j<",
            "Lw1/e;",
            ">;)V"
        }
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ls1/d;->x()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "/ss?platform=android&sdk_version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "2.1.0"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string p1, ""

    goto :goto_0

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "&region="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Lt1/f;->i()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Lt1/f;->j()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :goto_0
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    new-instance p1, Lv1/d;

    invoke-virtual {p0}, Ls1/d;->q()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Ls1/d;->t()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Ls1/d;->A()I

    move-result v4

    invoke-virtual {p0}, Ls1/d;->B()I

    move-result v6

    move-object v1, p1

    invoke-direct/range {v1 .. v6}, Lv1/d;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;I)V

    new-instance v0, Lv1/c;

    new-instance v1, Lw1/f$a;

    invoke-direct {v1}, Lw1/f$a;-><init>()V

    invoke-direct {v0, p1, v1}, Lv1/c;-><init>(Lv1/d;Lv1/k;)V

    new-instance p1, Lv1/g;

    new-instance v1, Lv1/e;

    invoke-virtual {p0}, Ls1/d;->x()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lu1/b;->b(Ljava/lang/String;)Lu1/b;

    move-result-object v2

    invoke-direct {v1, v2}, Lv1/e;-><init>(Lu1/b;)V

    invoke-direct {p1, v0, v1}, Lv1/g;-><init>(Lv1/c;Lv1/g$a;)V

    new-instance v0, Lv1/g;

    new-instance v1, Lv1/i;

    invoke-direct {v1, p0}, Lv1/i;-><init>(Ls1/d;)V

    invoke-direct {v0, p1, v1}, Lv1/g;-><init>(Lv1/c;Lv1/g$a;)V

    new-instance p1, Lv1/g;

    new-instance v1, Lw1/d;

    invoke-direct {v1, p0}, Lw1/d;-><init>(Ls1/d;)V

    invoke-direct {p1, v0, v1}, Lv1/g;-><init>(Lv1/c;Lv1/g$a;)V

    new-instance v0, Lv1/l;

    invoke-virtual {p0}, Ls1/d;->i()[Ljava/lang/String;

    move-result-object v1

    array-length v1, v1

    invoke-virtual {p0}, Ls1/d;->j()I

    move-result v2

    add-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x1

    invoke-direct {v0, p1, v1}, Lv1/l;-><init>(Lv1/c;I)V

    :try_start_0
    invoke-virtual {p0}, Ls1/d;->a()Ljava/util/concurrent/ExecutorService;

    move-result-object p0

    new-instance p1, Lv1/f;

    invoke-direct {p1, v0, p2}, Lv1/f;-><init>(Lv1/c;Lv1/j;)V

    invoke-interface {p0, p1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception p0

    invoke-interface {p2, p0}, Lv1/j;->a(Ljava/lang/Throwable;)V

    :goto_1
    return-void
.end method
