.class public final Lpb/w;
.super Lpb/a0;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lpb/w$c;,
        Lpb/w$a;,
        Lpb/w$b;
    }
.end annotation


# static fields
.field public static final g:Lpb/v;

.field public static final h:Lpb/v;

.field public static final i:Lpb/v;

.field public static final j:Lpb/v;

.field public static final k:Lpb/v;

.field private static final l:[B

.field private static final m:[B

.field private static final n:[B

.field public static final o:Lpb/w$b;


# instance fields
.field private final b:Lpb/v;

.field private c:J

.field private final d:Ldc/h;

.field private final e:Lpb/v;

.field private final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lpb/w$c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    new-instance v0, Lpb/w$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lpb/w$b;-><init>(Lm8/g;)V

    sput-object v0, Lpb/w;->o:Lpb/w$b;

    sget-object v0, Lpb/v;->g:Lpb/v$a;

    const-string v1, "multipart/mixed"

    invoke-virtual {v0, v1}, Lpb/v$a;->a(Ljava/lang/String;)Lpb/v;

    move-result-object v1

    sput-object v1, Lpb/w;->g:Lpb/v;

    const-string v1, "multipart/alternative"

    invoke-virtual {v0, v1}, Lpb/v$a;->a(Ljava/lang/String;)Lpb/v;

    move-result-object v1

    sput-object v1, Lpb/w;->h:Lpb/v;

    const-string v1, "multipart/digest"

    invoke-virtual {v0, v1}, Lpb/v$a;->a(Ljava/lang/String;)Lpb/v;

    move-result-object v1

    sput-object v1, Lpb/w;->i:Lpb/v;

    const-string v1, "multipart/parallel"

    invoke-virtual {v0, v1}, Lpb/v$a;->a(Ljava/lang/String;)Lpb/v;

    move-result-object v1

    sput-object v1, Lpb/w;->j:Lpb/v;

    const-string v1, "multipart/form-data"

    invoke-virtual {v0, v1}, Lpb/v$a;->a(Ljava/lang/String;)Lpb/v;

    move-result-object v0

    sput-object v0, Lpb/w;->k:Lpb/v;

    const/4 v0, 0x2

    new-array v1, v0, [B

    const/16 v2, 0x3a

    int-to-byte v2, v2

    const/4 v3, 0x0

    aput-byte v2, v1, v3

    const/16 v2, 0x20

    int-to-byte v2, v2

    const/4 v4, 0x1

    aput-byte v2, v1, v4

    sput-object v1, Lpb/w;->l:[B

    new-array v1, v0, [B

    const/16 v2, 0xd

    int-to-byte v2, v2

    aput-byte v2, v1, v3

    const/16 v2, 0xa

    int-to-byte v2, v2

    aput-byte v2, v1, v4

    sput-object v1, Lpb/w;->m:[B

    new-array v0, v0, [B

    const/16 v1, 0x2d

    int-to-byte v1, v1

    aput-byte v1, v0, v3

    aput-byte v1, v0, v4

    sput-object v0, Lpb/w;->n:[B

    return-void
.end method

.method public constructor <init>(Ldc/h;Lpb/v;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldc/h;",
            "Lpb/v;",
            "Ljava/util/List<",
            "Lpb/w$c;",
            ">;)V"
        }
    .end annotation

    const-string v0, "boundaryByteString"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "type"

    invoke-static {p2, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parts"

    invoke-static {p3, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lpb/a0;-><init>()V

    iput-object p1, p0, Lpb/w;->d:Ldc/h;

    iput-object p2, p0, Lpb/w;->e:Lpb/v;

    iput-object p3, p0, Lpb/w;->f:Ljava/util/List;

    sget-object p1, Lpb/v;->g:Lpb/v$a;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p2, "; boundary="

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lpb/w;->h()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lpb/v$a;->a(Ljava/lang/String;)Lpb/v;

    move-result-object p1

    iput-object p1, p0, Lpb/w;->b:Lpb/v;

    const-wide/16 p1, -0x1

    iput-wide p1, p0, Lpb/w;->c:J

    return-void
.end method

.method private final i(Ldc/f;Z)J
    .locals 12

    if-eqz p2, :cond_0

    new-instance p1, Ldc/e;

    invoke-direct {p1}, Ldc/e;-><init>()V

    move-object v0, p1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lpb/w;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x0

    const-wide/16 v3, 0x0

    move v5, v2

    :goto_1
    if-ge v5, v1, :cond_8

    iget-object v6, p0, Lpb/w;->f:Ljava/util/List;

    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lpb/w$c;

    invoke-virtual {v6}, Lpb/w$c;->b()Lpb/s;

    move-result-object v7

    invoke-virtual {v6}, Lpb/w$c;->a()Lpb/a0;

    move-result-object v6

    if-nez p1, :cond_1

    invoke-static {}, Lm8/i;->o()V

    :cond_1
    sget-object v8, Lpb/w;->n:[B

    invoke-interface {p1, v8}, Ldc/f;->A([B)Ldc/f;

    iget-object v8, p0, Lpb/w;->d:Ldc/h;

    invoke-interface {p1, v8}, Ldc/f;->G(Ldc/h;)Ldc/f;

    sget-object v8, Lpb/w;->m:[B

    invoke-interface {p1, v8}, Ldc/f;->A([B)Ldc/f;

    if-eqz v7, :cond_2

    invoke-virtual {v7}, Lpb/s;->size()I

    move-result v8

    move v9, v2

    :goto_2
    if-ge v9, v8, :cond_2

    invoke-virtual {v7, v9}, Lpb/s;->c(I)Ljava/lang/String;

    move-result-object v10

    invoke-interface {p1, v10}, Ldc/f;->O(Ljava/lang/String;)Ldc/f;

    move-result-object v10

    sget-object v11, Lpb/w;->l:[B

    invoke-interface {v10, v11}, Ldc/f;->A([B)Ldc/f;

    move-result-object v10

    invoke-virtual {v7, v9}, Lpb/s;->f(I)Ljava/lang/String;

    move-result-object v11

    invoke-interface {v10, v11}, Ldc/f;->O(Ljava/lang/String;)Ldc/f;

    move-result-object v10

    sget-object v11, Lpb/w;->m:[B

    invoke-interface {v10, v11}, Ldc/f;->A([B)Ldc/f;

    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    :cond_2
    invoke-virtual {v6}, Lpb/a0;->b()Lpb/v;

    move-result-object v7

    if-eqz v7, :cond_3

    const-string v8, "Content-Type: "

    invoke-interface {p1, v8}, Ldc/f;->O(Ljava/lang/String;)Ldc/f;

    move-result-object v8

    invoke-virtual {v7}, Lpb/v;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v8, v7}, Ldc/f;->O(Ljava/lang/String;)Ldc/f;

    move-result-object v7

    sget-object v8, Lpb/w;->m:[B

    invoke-interface {v7, v8}, Ldc/f;->A([B)Ldc/f;

    :cond_3
    invoke-virtual {v6}, Lpb/a0;->a()J

    move-result-wide v7

    const-wide/16 v9, -0x1

    cmp-long v11, v7, v9

    if-eqz v11, :cond_4

    const-string v9, "Content-Length: "

    invoke-interface {p1, v9}, Ldc/f;->O(Ljava/lang/String;)Ldc/f;

    move-result-object v9

    invoke-interface {v9, v7, v8}, Ldc/f;->P(J)Ldc/f;

    move-result-object v9

    sget-object v10, Lpb/w;->m:[B

    invoke-interface {v9, v10}, Ldc/f;->A([B)Ldc/f;

    goto :goto_3

    :cond_4
    if-eqz p2, :cond_6

    if-nez v0, :cond_5

    invoke-static {}, Lm8/i;->o()V

    :cond_5
    invoke-virtual {v0}, Ldc/e;->g()V

    return-wide v9

    :cond_6
    :goto_3
    sget-object v9, Lpb/w;->m:[B

    invoke-interface {p1, v9}, Ldc/f;->A([B)Ldc/f;

    if-eqz p2, :cond_7

    add-long/2addr v3, v7

    goto :goto_4

    :cond_7
    invoke-virtual {v6, p1}, Lpb/a0;->g(Ldc/f;)V

    :goto_4
    invoke-interface {p1, v9}, Ldc/f;->A([B)Ldc/f;

    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_1

    :cond_8
    if-nez p1, :cond_9

    invoke-static {}, Lm8/i;->o()V

    :cond_9
    sget-object v1, Lpb/w;->n:[B

    invoke-interface {p1, v1}, Ldc/f;->A([B)Ldc/f;

    iget-object v2, p0, Lpb/w;->d:Ldc/h;

    invoke-interface {p1, v2}, Ldc/f;->G(Ldc/h;)Ldc/f;

    invoke-interface {p1, v1}, Ldc/f;->A([B)Ldc/f;

    sget-object v1, Lpb/w;->m:[B

    invoke-interface {p1, v1}, Ldc/f;->A([B)Ldc/f;

    if-eqz p2, :cond_b

    if-nez v0, :cond_a

    invoke-static {}, Lm8/i;->o()V

    :cond_a
    invoke-virtual {v0}, Ldc/e;->d0()J

    move-result-wide p1

    add-long/2addr v3, p1

    invoke-virtual {v0}, Ldc/e;->g()V

    :cond_b
    return-wide v3
.end method


# virtual methods
.method public a()J
    .locals 4

    iget-wide v0, p0, Lpb/w;->c:J

    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lpb/w;->i(Ldc/f;Z)J

    move-result-wide v0

    iput-wide v0, p0, Lpb/w;->c:J

    :cond_0
    return-wide v0
.end method

.method public b()Lpb/v;
    .locals 1

    iget-object v0, p0, Lpb/w;->b:Lpb/v;

    return-object v0
.end method

.method public g(Ldc/f;)V
    .locals 1

    const-string v0, "sink"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lpb/w;->i(Ldc/f;Z)J

    return-void
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lpb/w;->d:Ldc/h;

    invoke-virtual {v0}, Ldc/h;->v()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
