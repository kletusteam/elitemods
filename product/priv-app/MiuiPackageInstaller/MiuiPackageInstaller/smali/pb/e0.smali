.class public final enum Lpb/e0;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lpb/e0$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lpb/e0;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum b:Lpb/e0;

.field public static final enum c:Lpb/e0;

.field public static final enum d:Lpb/e0;

.field public static final enum e:Lpb/e0;

.field public static final enum f:Lpb/e0;

.field private static final synthetic g:[Lpb/e0;

.field public static final h:Lpb/e0$a;


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x5

    new-array v0, v0, [Lpb/e0;

    new-instance v1, Lpb/e0;

    const-string v2, "TLS_1_3"

    const/4 v3, 0x0

    const-string v4, "TLSv1.3"

    invoke-direct {v1, v2, v3, v4}, Lpb/e0;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lpb/e0;->b:Lpb/e0;

    aput-object v1, v0, v3

    new-instance v1, Lpb/e0;

    const-string v2, "TLS_1_2"

    const/4 v3, 0x1

    const-string v4, "TLSv1.2"

    invoke-direct {v1, v2, v3, v4}, Lpb/e0;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lpb/e0;->c:Lpb/e0;

    aput-object v1, v0, v3

    new-instance v1, Lpb/e0;

    const-string v2, "TLS_1_1"

    const/4 v3, 0x2

    const-string v4, "TLSv1.1"

    invoke-direct {v1, v2, v3, v4}, Lpb/e0;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lpb/e0;->d:Lpb/e0;

    aput-object v1, v0, v3

    new-instance v1, Lpb/e0;

    const-string v2, "TLS_1_0"

    const/4 v3, 0x3

    const-string v4, "TLSv1"

    invoke-direct {v1, v2, v3, v4}, Lpb/e0;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lpb/e0;->e:Lpb/e0;

    aput-object v1, v0, v3

    new-instance v1, Lpb/e0;

    const-string v2, "SSL_3_0"

    const/4 v3, 0x4

    const-string v4, "SSLv3"

    invoke-direct {v1, v2, v3, v4}, Lpb/e0;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lpb/e0;->f:Lpb/e0;

    aput-object v1, v0, v3

    sput-object v0, Lpb/e0;->g:[Lpb/e0;

    new-instance v0, Lpb/e0$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lpb/e0$a;-><init>(Lm8/g;)V

    sput-object v0, Lpb/e0;->h:Lpb/e0$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lpb/e0;->a:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lpb/e0;
    .locals 1

    const-class v0, Lpb/e0;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lpb/e0;

    return-object p0
.end method

.method public static values()[Lpb/e0;
    .locals 1

    sget-object v0, Lpb/e0;->g:[Lpb/e0;

    invoke-virtual {v0}, [Lpb/e0;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lpb/e0;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lpb/e0;->a:Ljava/lang/String;

    return-object v0
.end method
