.class public abstract Lpb/a0;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lpb/a0$a;
    }
.end annotation


# static fields
.field public static final a:Lpb/a0$a;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lpb/a0$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lpb/a0$a;-><init>(Lm8/g;)V

    sput-object v0, Lpb/a0;->a:Lpb/a0$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final c(Lpb/v;Ldc/h;)Lpb/a0;
    .locals 1

    sget-object v0, Lpb/a0;->a:Lpb/a0$a;

    invoke-virtual {v0, p0, p1}, Lpb/a0$a;->c(Lpb/v;Ldc/h;)Lpb/a0;

    move-result-object p0

    return-object p0
.end method

.method public static final d(Lpb/v;[B)Lpb/a0;
    .locals 7

    sget-object v0, Lpb/a0;->a:Lpb/a0$a;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xc

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    invoke-static/range {v0 .. v6}, Lpb/a0$a;->g(Lpb/a0$a;Lpb/v;[BIIILjava/lang/Object;)Lpb/a0;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public a()J
    .locals 2

    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public abstract b()Lpb/v;
.end method

.method public e()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public f()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public abstract g(Ldc/f;)V
.end method
