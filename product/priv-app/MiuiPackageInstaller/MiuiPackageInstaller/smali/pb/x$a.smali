.class public final Lpb/x$a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lpb/x;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field private A:I

.field private B:Lub/i;

.field private a:Lpb/n;

.field private b:Lpb/j;

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lpb/u;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lpb/u;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lpb/p$c;

.field private f:Z

.field private g:Lpb/b;

.field private h:Z

.field private i:Z

.field private j:Lpb/m;

.field private k:Lpb/o;

.field private l:Ljava/net/Proxy;

.field private m:Ljava/net/ProxySelector;

.field private n:Lpb/b;

.field private o:Ljavax/net/SocketFactory;

.field private p:Ljavax/net/ssl/SSLSocketFactory;

.field private q:Ljavax/net/ssl/X509TrustManager;

.field private r:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lpb/k;",
            ">;"
        }
    .end annotation
.end field

.field private s:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "+",
            "Lpb/y;",
            ">;"
        }
    .end annotation
.end field

.field private t:Ljavax/net/ssl/HostnameVerifier;

.field private u:Lpb/g;

.field private v:Lbc/c;

.field private w:I

.field private x:I

.field private y:I

.field private z:I


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lpb/n;

    invoke-direct {v0}, Lpb/n;-><init>()V

    iput-object v0, p0, Lpb/x$a;->a:Lpb/n;

    new-instance v0, Lpb/j;

    invoke-direct {v0}, Lpb/j;-><init>()V

    iput-object v0, p0, Lpb/x$a;->b:Lpb/j;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lpb/x$a;->c:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lpb/x$a;->d:Ljava/util/List;

    sget-object v0, Lpb/p;->a:Lpb/p;

    invoke-static {v0}, Lrb/b;->e(Lpb/p;)Lpb/p$c;

    move-result-object v0

    iput-object v0, p0, Lpb/x$a;->e:Lpb/p$c;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lpb/x$a;->f:Z

    sget-object v1, Lpb/b;->a:Lpb/b;

    iput-object v1, p0, Lpb/x$a;->g:Lpb/b;

    iput-boolean v0, p0, Lpb/x$a;->h:Z

    iput-boolean v0, p0, Lpb/x$a;->i:Z

    sget-object v0, Lpb/m;->a:Lpb/m;

    iput-object v0, p0, Lpb/x$a;->j:Lpb/m;

    sget-object v0, Lpb/o;->a:Lpb/o;

    iput-object v0, p0, Lpb/x$a;->k:Lpb/o;

    iput-object v1, p0, Lpb/x$a;->n:Lpb/b;

    invoke-static {}, Ljavax/net/SocketFactory;->getDefault()Ljavax/net/SocketFactory;

    move-result-object v0

    const-string v1, "SocketFactory.getDefault()"

    invoke-static {v0, v1}, Lm8/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lpb/x$a;->o:Ljavax/net/SocketFactory;

    sget-object v0, Lpb/x;->E:Lpb/x$b;

    invoke-virtual {v0}, Lpb/x$b;->b()Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lpb/x$a;->r:Ljava/util/List;

    invoke-virtual {v0}, Lpb/x$b;->c()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lpb/x$a;->s:Ljava/util/List;

    sget-object v0, Lbc/d;->a:Lbc/d;

    iput-object v0, p0, Lpb/x$a;->t:Ljavax/net/ssl/HostnameVerifier;

    sget-object v0, Lpb/g;->c:Lpb/g;

    iput-object v0, p0, Lpb/x$a;->u:Lpb/g;

    const/16 v0, 0x2710

    iput v0, p0, Lpb/x$a;->x:I

    iput v0, p0, Lpb/x$a;->y:I

    iput v0, p0, Lpb/x$a;->z:I

    return-void
.end method


# virtual methods
.method public final A()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lpb/y;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lpb/x$a;->s:Ljava/util/List;

    return-object v0
.end method

.method public final B()Ljava/net/Proxy;
    .locals 1

    iget-object v0, p0, Lpb/x$a;->l:Ljava/net/Proxy;

    return-object v0
.end method

.method public final C()Lpb/b;
    .locals 1

    iget-object v0, p0, Lpb/x$a;->n:Lpb/b;

    return-object v0
.end method

.method public final D()Ljava/net/ProxySelector;
    .locals 1

    iget-object v0, p0, Lpb/x$a;->m:Ljava/net/ProxySelector;

    return-object v0
.end method

.method public final E()I
    .locals 1

    iget v0, p0, Lpb/x$a;->y:I

    return v0
.end method

.method public final F()Z
    .locals 1

    iget-boolean v0, p0, Lpb/x$a;->f:Z

    return v0
.end method

.method public final G()Lub/i;
    .locals 1

    iget-object v0, p0, Lpb/x$a;->B:Lub/i;

    return-object v0
.end method

.method public final H()Ljavax/net/SocketFactory;
    .locals 1

    iget-object v0, p0, Lpb/x$a;->o:Ljavax/net/SocketFactory;

    return-object v0
.end method

.method public final I()Ljavax/net/ssl/SSLSocketFactory;
    .locals 1

    iget-object v0, p0, Lpb/x$a;->p:Ljavax/net/ssl/SSLSocketFactory;

    return-object v0
.end method

.method public final J()I
    .locals 1

    iget v0, p0, Lpb/x$a;->z:I

    return v0
.end method

.method public final K()Ljavax/net/ssl/X509TrustManager;
    .locals 1

    iget-object v0, p0, Lpb/x$a;->q:Ljavax/net/ssl/X509TrustManager;

    return-object v0
.end method

.method public final L(Ljava/util/List;)Lpb/x$a;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lpb/y;",
            ">;)",
            "Lpb/x$a;"
        }
    .end annotation

    const-string v0, "protocols"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lb8/j;->F(Ljava/util/Collection;)Ljava/util/List;

    move-result-object p1

    sget-object v0, Lpb/y;->f:Lpb/y;

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-nez v1, :cond_1

    sget-object v1, Lpb/y;->c:Lpb/y;

    invoke-interface {p1, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    move v1, v2

    goto :goto_1

    :cond_1
    :goto_0
    move v1, v3

    :goto_1
    if-eqz v1, :cond_8

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-gt v0, v3, :cond_3

    :cond_2
    move v2, v3

    :cond_3
    if-eqz v2, :cond_7

    sget-object v0, Lpb/y;->b:Lpb/y;

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    xor-int/2addr v0, v3

    if-eqz v0, :cond_6

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    xor-int/2addr v1, v3

    if-eqz v1, :cond_5

    sget-object v1, Lpb/y;->d:Lpb/y;

    invoke-interface {p1, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    iget-object v1, p0, Lpb/x$a;->s:Ljava/util/List;

    invoke-static {p1, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    xor-int/2addr v1, v3

    if-eqz v1, :cond_4

    iput-object v0, p0, Lpb/x$a;->B:Lub/i;

    :cond_4
    invoke-static {p1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    const-string v0, "Collections.unmodifiableList(protocolsCopy)"

    invoke-static {p1, v0}, Lm8/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lpb/x$a;->s:Ljava/util/List;

    return-object p0

    :cond_5
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "protocols must not contain null"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "protocols must not contain http/1.0: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_7
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "protocols containing h2_prior_knowledge cannot use other protocols: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_8
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "protocols must contain h2_prior_knowledge or http/1.1: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final M(JLjava/util/concurrent/TimeUnit;)Lpb/x$a;
    .locals 1

    const-string v0, "unit"

    invoke-static {p3, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "timeout"

    invoke-static {v0, p1, p2, p3}, Lrb/b;->h(Ljava/lang/String;JLjava/util/concurrent/TimeUnit;)I

    move-result p1

    iput p1, p0, Lpb/x$a;->y:I

    return-object p0
.end method

.method public final N(Z)Lpb/x$a;
    .locals 0

    iput-boolean p1, p0, Lpb/x$a;->f:Z

    return-object p0
.end method

.method public final O(Ljavax/net/ssl/SSLSocketFactory;Ljavax/net/ssl/X509TrustManager;)Lpb/x$a;
    .locals 1

    const-string v0, "sslSocketFactory"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "trustManager"

    invoke-static {p2, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lpb/x$a;->p:Ljavax/net/ssl/SSLSocketFactory;

    invoke-static {p1, v0}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_0

    iget-object v0, p0, Lpb/x$a;->q:Ljavax/net/ssl/X509TrustManager;

    invoke-static {p2, v0}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lpb/x$a;->B:Lub/i;

    :cond_1
    iput-object p1, p0, Lpb/x$a;->p:Ljavax/net/ssl/SSLSocketFactory;

    sget-object p1, Lbc/c;->a:Lbc/c$a;

    invoke-virtual {p1, p2}, Lbc/c$a;->a(Ljavax/net/ssl/X509TrustManager;)Lbc/c;

    move-result-object p1

    iput-object p1, p0, Lpb/x$a;->v:Lbc/c;

    iput-object p2, p0, Lpb/x$a;->q:Ljavax/net/ssl/X509TrustManager;

    return-object p0
.end method

.method public final P(JLjava/util/concurrent/TimeUnit;)Lpb/x$a;
    .locals 1

    const-string v0, "unit"

    invoke-static {p3, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "timeout"

    invoke-static {v0, p1, p2, p3}, Lrb/b;->h(Ljava/lang/String;JLjava/util/concurrent/TimeUnit;)I

    move-result p1

    iput p1, p0, Lpb/x$a;->z:I

    return-object p0
.end method

.method public final a(Lpb/u;)Lpb/x$a;
    .locals 1

    const-string v0, "interceptor"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lpb/x$a;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final b()Lpb/x;
    .locals 1

    new-instance v0, Lpb/x;

    invoke-direct {v0, p0}, Lpb/x;-><init>(Lpb/x$a;)V

    return-object v0
.end method

.method public final c(JLjava/util/concurrent/TimeUnit;)Lpb/x$a;
    .locals 1

    const-string v0, "unit"

    invoke-static {p3, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "timeout"

    invoke-static {v0, p1, p2, p3}, Lrb/b;->h(Ljava/lang/String;JLjava/util/concurrent/TimeUnit;)I

    move-result p1

    iput p1, p0, Lpb/x$a;->x:I

    return-object p0
.end method

.method public final d(Lpb/j;)Lpb/x$a;
    .locals 1

    const-string v0, "connectionPool"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lpb/x$a;->b:Lpb/j;

    return-object p0
.end method

.method public final e(Ljava/util/List;)Lpb/x$a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lpb/k;",
            ">;)",
            "Lpb/x$a;"
        }
    .end annotation

    const-string v0, "connectionSpecs"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lpb/x$a;->r:Ljava/util/List;

    invoke-static {p1, v0}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lpb/x$a;->B:Lub/i;

    :cond_0
    invoke-static {p1}, Lrb/b;->M(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lpb/x$a;->r:Ljava/util/List;

    return-object p0
.end method

.method public final f(Lpb/n;)Lpb/x$a;
    .locals 1

    const-string v0, "dispatcher"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lpb/x$a;->a:Lpb/n;

    return-object p0
.end method

.method public final g(Lpb/o;)Lpb/x$a;
    .locals 1

    const-string v0, "dns"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lpb/x$a;->k:Lpb/o;

    invoke-static {p1, v0}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lpb/x$a;->B:Lub/i;

    :cond_0
    iput-object p1, p0, Lpb/x$a;->k:Lpb/o;

    return-object p0
.end method

.method public final h(Lpb/p$c;)Lpb/x$a;
    .locals 1

    const-string v0, "eventListenerFactory"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lpb/x$a;->e:Lpb/p$c;

    return-object p0
.end method

.method public final i()Lpb/b;
    .locals 1

    iget-object v0, p0, Lpb/x$a;->g:Lpb/b;

    return-object v0
.end method

.method public final j()Lpb/c;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final k()I
    .locals 1

    iget v0, p0, Lpb/x$a;->w:I

    return v0
.end method

.method public final l()Lbc/c;
    .locals 1

    iget-object v0, p0, Lpb/x$a;->v:Lbc/c;

    return-object v0
.end method

.method public final m()Lpb/g;
    .locals 1

    iget-object v0, p0, Lpb/x$a;->u:Lpb/g;

    return-object v0
.end method

.method public final n()I
    .locals 1

    iget v0, p0, Lpb/x$a;->x:I

    return v0
.end method

.method public final o()Lpb/j;
    .locals 1

    iget-object v0, p0, Lpb/x$a;->b:Lpb/j;

    return-object v0
.end method

.method public final p()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lpb/k;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lpb/x$a;->r:Ljava/util/List;

    return-object v0
.end method

.method public final q()Lpb/m;
    .locals 1

    iget-object v0, p0, Lpb/x$a;->j:Lpb/m;

    return-object v0
.end method

.method public final r()Lpb/n;
    .locals 1

    iget-object v0, p0, Lpb/x$a;->a:Lpb/n;

    return-object v0
.end method

.method public final s()Lpb/o;
    .locals 1

    iget-object v0, p0, Lpb/x$a;->k:Lpb/o;

    return-object v0
.end method

.method public final t()Lpb/p$c;
    .locals 1

    iget-object v0, p0, Lpb/x$a;->e:Lpb/p$c;

    return-object v0
.end method

.method public final u()Z
    .locals 1

    iget-boolean v0, p0, Lpb/x$a;->h:Z

    return v0
.end method

.method public final v()Z
    .locals 1

    iget-boolean v0, p0, Lpb/x$a;->i:Z

    return v0
.end method

.method public final w()Ljavax/net/ssl/HostnameVerifier;
    .locals 1

    iget-object v0, p0, Lpb/x$a;->t:Ljavax/net/ssl/HostnameVerifier;

    return-object v0
.end method

.method public final x()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lpb/u;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lpb/x$a;->c:Ljava/util/List;

    return-object v0
.end method

.method public final y()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lpb/u;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lpb/x$a;->d:Ljava/util/List;

    return-object v0
.end method

.method public final z()I
    .locals 1

    iget v0, p0, Lpb/x$a;->A:I

    return v0
.end method
