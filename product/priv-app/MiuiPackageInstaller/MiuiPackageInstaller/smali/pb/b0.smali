.class public final Lpb/b0;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lpb/b0$a;
    }
.end annotation


# instance fields
.field private a:Lpb/d;

.field private final b:Lpb/z;

.field private final c:Lpb/y;

.field private final d:Ljava/lang/String;

.field private final e:I

.field private final f:Lpb/r;

.field private final g:Lpb/s;

.field private final h:Lpb/c0;

.field private final i:Lpb/b0;

.field private final j:Lpb/b0;

.field private final k:Lpb/b0;

.field private final l:J

.field private final m:J

.field private final n:Lub/c;


# direct methods
.method public constructor <init>(Lpb/z;Lpb/y;Ljava/lang/String;ILpb/r;Lpb/s;Lpb/c0;Lpb/b0;Lpb/b0;Lpb/b0;JJLub/c;)V
    .locals 6

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p6

    const-string v5, "request"

    invoke-static {p1, v5}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v5, "protocol"

    invoke-static {p2, v5}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v5, "message"

    invoke-static {p3, v5}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v5, "headers"

    invoke-static {p6, v5}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, v0, Lpb/b0;->b:Lpb/z;

    iput-object v2, v0, Lpb/b0;->c:Lpb/y;

    iput-object v3, v0, Lpb/b0;->d:Ljava/lang/String;

    move v1, p4

    iput v1, v0, Lpb/b0;->e:I

    move-object v1, p5

    iput-object v1, v0, Lpb/b0;->f:Lpb/r;

    iput-object v4, v0, Lpb/b0;->g:Lpb/s;

    move-object v1, p7

    iput-object v1, v0, Lpb/b0;->h:Lpb/c0;

    move-object v1, p8

    iput-object v1, v0, Lpb/b0;->i:Lpb/b0;

    move-object v1, p9

    iput-object v1, v0, Lpb/b0;->j:Lpb/b0;

    move-object/from16 v1, p10

    iput-object v1, v0, Lpb/b0;->k:Lpb/b0;

    move-wide/from16 v1, p11

    iput-wide v1, v0, Lpb/b0;->l:J

    move-wide/from16 v1, p13

    iput-wide v1, v0, Lpb/b0;->m:J

    move-object/from16 v1, p15

    iput-object v1, v0, Lpb/b0;->n:Lub/c;

    return-void
.end method

.method public static synthetic y(Lpb/b0;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/String;
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    :cond_0
    invoke-virtual {p0, p1, p2}, Lpb/b0;->s(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final B()Z
    .locals 2

    iget v0, p0, Lpb/b0;->e:I

    const/16 v1, 0x133

    if-eq v0, v1, :cond_0

    const/16 v1, 0x134

    if-eq v0, v1, :cond_0

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    :pswitch_0
    const/4 v0, 0x1

    :goto_0
    return v0

    nop

    :pswitch_data_0
    .packed-switch 0x12c
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final C()Z
    .locals 2

    iget v0, p0, Lpb/b0;->e:I

    const/16 v1, 0xc8

    if-le v1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/16 v1, 0x12b

    if-lt v1, v0, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x0

    :goto_1
    return v0
.end method

.method public final D()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lpb/b0;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final F()Lpb/b0;
    .locals 1

    iget-object v0, p0, Lpb/b0;->i:Lpb/b0;

    return-object v0
.end method

.method public final J()Lpb/b0$a;
    .locals 1

    new-instance v0, Lpb/b0$a;

    invoke-direct {v0, p0}, Lpb/b0$a;-><init>(Lpb/b0;)V

    return-object v0
.end method

.method public final K()Lpb/b0;
    .locals 1

    iget-object v0, p0, Lpb/b0;->k:Lpb/b0;

    return-object v0
.end method

.method public final L()Lpb/y;
    .locals 1

    iget-object v0, p0, Lpb/b0;->c:Lpb/y;

    return-object v0
.end method

.method public final M()J
    .locals 2

    iget-wide v0, p0, Lpb/b0;->m:J

    return-wide v0
.end method

.method public final U()Lpb/z;
    .locals 1

    iget-object v0, p0, Lpb/b0;->b:Lpb/z;

    return-object v0
.end method

.method public final V()J
    .locals 2

    iget-wide v0, p0, Lpb/b0;->l:J

    return-wide v0
.end method

.method public final b()Lpb/c0;
    .locals 1

    iget-object v0, p0, Lpb/b0;->h:Lpb/c0;

    return-object v0
.end method

.method public close()V
    .locals 2

    iget-object v0, p0, Lpb/b0;->h:Lpb/c0;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lpb/c0;->close()V

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "response is not eligible for a body and must not be closed"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final g()Lpb/d;
    .locals 2

    iget-object v0, p0, Lpb/b0;->a:Lpb/d;

    if-nez v0, :cond_0

    sget-object v0, Lpb/d;->p:Lpb/d$b;

    iget-object v1, p0, Lpb/b0;->g:Lpb/s;

    invoke-virtual {v0, v1}, Lpb/d$b;->b(Lpb/s;)Lpb/d;

    move-result-object v0

    iput-object v0, p0, Lpb/b0;->a:Lpb/d;

    :cond_0
    return-object v0
.end method

.method public final m()Lpb/b0;
    .locals 1

    iget-object v0, p0, Lpb/b0;->j:Lpb/b0;

    return-object v0
.end method

.method public final n()I
    .locals 1

    iget v0, p0, Lpb/b0;->e:I

    return v0
.end method

.method public final p()Lub/c;
    .locals 1

    iget-object v0, p0, Lpb/b0;->n:Lub/c;

    return-object v0
.end method

.method public final r()Lpb/r;
    .locals 1

    iget-object v0, p0, Lpb/b0;->f:Lpb/r;

    return-object v0
.end method

.method public final s(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    const-string v0, "name"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lpb/b0;->g:Lpb/s;

    invoke-virtual {v0, p1}, Lpb/s;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    move-object p2, p1

    :cond_0
    return-object p2
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Response{protocol="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lpb/b0;->c:Lpb/y;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lpb/b0;->e:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", message="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lpb/b0;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", url="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lpb/b0;->b:Lpb/z;

    invoke-virtual {v1}, Lpb/z;->j()Lpb/t;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final z()Lpb/s;
    .locals 1

    iget-object v0, p0, Lpb/b0;->g:Lpb/s;

    return-object v0
.end method
