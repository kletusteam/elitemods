.class public Lpb/b0$a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lpb/b0;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field private a:Lpb/z;

.field private b:Lpb/y;

.field private c:I

.field private d:Ljava/lang/String;

.field private e:Lpb/r;

.field private f:Lpb/s$a;

.field private g:Lpb/c0;

.field private h:Lpb/b0;

.field private i:Lpb/b0;

.field private j:Lpb/b0;

.field private k:J

.field private l:J

.field private m:Lub/c;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lpb/b0$a;->c:I

    new-instance v0, Lpb/s$a;

    invoke-direct {v0}, Lpb/s$a;-><init>()V

    iput-object v0, p0, Lpb/b0$a;->f:Lpb/s$a;

    return-void
.end method

.method public constructor <init>(Lpb/b0;)V
    .locals 2

    const-string v0, "response"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lpb/b0$a;->c:I

    invoke-virtual {p1}, Lpb/b0;->U()Lpb/z;

    move-result-object v0

    iput-object v0, p0, Lpb/b0$a;->a:Lpb/z;

    invoke-virtual {p1}, Lpb/b0;->L()Lpb/y;

    move-result-object v0

    iput-object v0, p0, Lpb/b0$a;->b:Lpb/y;

    invoke-virtual {p1}, Lpb/b0;->n()I

    move-result v0

    iput v0, p0, Lpb/b0$a;->c:I

    invoke-virtual {p1}, Lpb/b0;->D()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpb/b0$a;->d:Ljava/lang/String;

    invoke-virtual {p1}, Lpb/b0;->r()Lpb/r;

    move-result-object v0

    iput-object v0, p0, Lpb/b0$a;->e:Lpb/r;

    invoke-virtual {p1}, Lpb/b0;->z()Lpb/s;

    move-result-object v0

    invoke-virtual {v0}, Lpb/s;->d()Lpb/s$a;

    move-result-object v0

    iput-object v0, p0, Lpb/b0$a;->f:Lpb/s$a;

    invoke-virtual {p1}, Lpb/b0;->b()Lpb/c0;

    move-result-object v0

    iput-object v0, p0, Lpb/b0$a;->g:Lpb/c0;

    invoke-virtual {p1}, Lpb/b0;->F()Lpb/b0;

    move-result-object v0

    iput-object v0, p0, Lpb/b0$a;->h:Lpb/b0;

    invoke-virtual {p1}, Lpb/b0;->m()Lpb/b0;

    move-result-object v0

    iput-object v0, p0, Lpb/b0$a;->i:Lpb/b0;

    invoke-virtual {p1}, Lpb/b0;->K()Lpb/b0;

    move-result-object v0

    iput-object v0, p0, Lpb/b0$a;->j:Lpb/b0;

    invoke-virtual {p1}, Lpb/b0;->V()J

    move-result-wide v0

    iput-wide v0, p0, Lpb/b0$a;->k:J

    invoke-virtual {p1}, Lpb/b0;->M()J

    move-result-wide v0

    iput-wide v0, p0, Lpb/b0$a;->l:J

    invoke-virtual {p1}, Lpb/b0;->p()Lub/c;

    move-result-object p1

    iput-object p1, p0, Lpb/b0$a;->m:Lub/c;

    return-void
.end method

.method private final e(Lpb/b0;)V
    .locals 1

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lpb/b0;->b()Lpb/c0;

    move-result-object p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "priorResponse.body != null"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    :goto_1
    return-void
.end method

.method private final f(Ljava/lang/String;Lpb/b0;)V
    .locals 3

    if-eqz p2, :cond_8

    invoke-virtual {p2}, Lpb/b0;->b()Lpb/c0;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez v0, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    move v0, v2

    :goto_0
    if-eqz v0, :cond_7

    invoke-virtual {p2}, Lpb/b0;->F()Lpb/b0;

    move-result-object v0

    if-nez v0, :cond_1

    move v0, v1

    goto :goto_1

    :cond_1
    move v0, v2

    :goto_1
    if-eqz v0, :cond_6

    invoke-virtual {p2}, Lpb/b0;->m()Lpb/b0;

    move-result-object v0

    if-nez v0, :cond_2

    move v0, v1

    goto :goto_2

    :cond_2
    move v0, v2

    :goto_2
    if-eqz v0, :cond_5

    invoke-virtual {p2}, Lpb/b0;->K()Lpb/b0;

    move-result-object p2

    if-nez p2, :cond_3

    goto :goto_3

    :cond_3
    move v1, v2

    :goto_3
    if-eqz v1, :cond_4

    goto :goto_4

    :cond_4
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ".priorResponse != null"

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance p2, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2

    :cond_5
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ".cacheResponse != null"

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance p2, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2

    :cond_6
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ".networkResponse != null"

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance p2, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2

    :cond_7
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ".body != null"

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance p2, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2

    :cond_8
    :goto_4
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/String;)Lpb/b0$a;
    .locals 1

    const-string v0, "name"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "value"

    invoke-static {p2, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lpb/b0$a;->f:Lpb/s$a;

    invoke-virtual {v0, p1, p2}, Lpb/s$a;->a(Ljava/lang/String;Ljava/lang/String;)Lpb/s$a;

    return-object p0
.end method

.method public b(Lpb/c0;)Lpb/b0$a;
    .locals 0

    iput-object p1, p0, Lpb/b0$a;->g:Lpb/c0;

    return-object p0
.end method

.method public c()Lpb/b0;
    .locals 18

    move-object/from16 v0, p0

    iget v5, v0, Lpb/b0$a;->c:I

    if-ltz v5, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_4

    iget-object v2, v0, Lpb/b0$a;->a:Lpb/z;

    if-eqz v2, :cond_3

    iget-object v3, v0, Lpb/b0$a;->b:Lpb/y;

    if-eqz v3, :cond_2

    iget-object v4, v0, Lpb/b0$a;->d:Ljava/lang/String;

    if-eqz v4, :cond_1

    iget-object v6, v0, Lpb/b0$a;->e:Lpb/r;

    iget-object v1, v0, Lpb/b0$a;->f:Lpb/s$a;

    invoke-virtual {v1}, Lpb/s$a;->e()Lpb/s;

    move-result-object v7

    iget-object v8, v0, Lpb/b0$a;->g:Lpb/c0;

    iget-object v9, v0, Lpb/b0$a;->h:Lpb/b0;

    iget-object v10, v0, Lpb/b0$a;->i:Lpb/b0;

    iget-object v11, v0, Lpb/b0$a;->j:Lpb/b0;

    iget-wide v12, v0, Lpb/b0$a;->k:J

    iget-wide v14, v0, Lpb/b0$a;->l:J

    iget-object v1, v0, Lpb/b0$a;->m:Lub/c;

    new-instance v17, Lpb/b0;

    move-object/from16 v16, v1

    move-object/from16 v1, v17

    invoke-direct/range {v1 .. v16}, Lpb/b0;-><init>(Lpb/z;Lpb/y;Ljava/lang/String;ILpb/r;Lpb/s;Lpb/c0;Lpb/b0;Lpb/b0;Lpb/b0;JJLub/c;)V

    return-object v17

    :cond_1
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "message == null"

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "protocol == null"

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_3
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "request == null"

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_4
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "code < 0: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, v0, Lpb/b0$a;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public d(Lpb/b0;)Lpb/b0$a;
    .locals 1

    const-string v0, "cacheResponse"

    invoke-direct {p0, v0, p1}, Lpb/b0$a;->f(Ljava/lang/String;Lpb/b0;)V

    iput-object p1, p0, Lpb/b0$a;->i:Lpb/b0;

    return-object p0
.end method

.method public g(I)Lpb/b0$a;
    .locals 0

    iput p1, p0, Lpb/b0$a;->c:I

    return-object p0
.end method

.method public final h()I
    .locals 1

    iget v0, p0, Lpb/b0$a;->c:I

    return v0
.end method

.method public i(Lpb/r;)Lpb/b0$a;
    .locals 0

    iput-object p1, p0, Lpb/b0$a;->e:Lpb/r;

    return-object p0
.end method

.method public j(Ljava/lang/String;Ljava/lang/String;)Lpb/b0$a;
    .locals 1

    const-string v0, "name"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "value"

    invoke-static {p2, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lpb/b0$a;->f:Lpb/s$a;

    invoke-virtual {v0, p1, p2}, Lpb/s$a;->h(Ljava/lang/String;Ljava/lang/String;)Lpb/s$a;

    return-object p0
.end method

.method public k(Lpb/s;)Lpb/b0$a;
    .locals 1

    const-string v0, "headers"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lpb/s;->d()Lpb/s$a;

    move-result-object p1

    iput-object p1, p0, Lpb/b0$a;->f:Lpb/s$a;

    return-object p0
.end method

.method public final l(Lub/c;)V
    .locals 1

    const-string v0, "deferredTrailers"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lpb/b0$a;->m:Lub/c;

    return-void
.end method

.method public m(Ljava/lang/String;)Lpb/b0$a;
    .locals 1

    const-string v0, "message"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lpb/b0$a;->d:Ljava/lang/String;

    return-object p0
.end method

.method public n(Lpb/b0;)Lpb/b0$a;
    .locals 1

    const-string v0, "networkResponse"

    invoke-direct {p0, v0, p1}, Lpb/b0$a;->f(Ljava/lang/String;Lpb/b0;)V

    iput-object p1, p0, Lpb/b0$a;->h:Lpb/b0;

    return-object p0
.end method

.method public o(Lpb/b0;)Lpb/b0$a;
    .locals 0

    invoke-direct {p0, p1}, Lpb/b0$a;->e(Lpb/b0;)V

    iput-object p1, p0, Lpb/b0$a;->j:Lpb/b0;

    return-object p0
.end method

.method public p(Lpb/y;)Lpb/b0$a;
    .locals 1

    const-string v0, "protocol"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lpb/b0$a;->b:Lpb/y;

    return-object p0
.end method

.method public q(J)Lpb/b0$a;
    .locals 0

    iput-wide p1, p0, Lpb/b0$a;->l:J

    return-object p0
.end method

.method public r(Ljava/lang/String;)Lpb/b0$a;
    .locals 1

    const-string v0, "name"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lpb/b0$a;->f:Lpb/s$a;

    invoke-virtual {v0, p1}, Lpb/s$a;->g(Ljava/lang/String;)Lpb/s$a;

    return-object p0
.end method

.method public s(Lpb/z;)Lpb/b0$a;
    .locals 1

    const-string v0, "request"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lpb/b0$a;->a:Lpb/z;

    return-object p0
.end method

.method public t(J)Lpb/b0$a;
    .locals 0

    iput-wide p1, p0, Lpb/b0$a;->k:J

    return-object p0
.end method
