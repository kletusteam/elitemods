.class public Lpb/x;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Cloneable;
.implements Lpb/e$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lpb/x$a;,
        Lpb/x$b;
    }
.end annotation


# static fields
.field private static final C:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lpb/y;",
            ">;"
        }
    .end annotation
.end field

.field private static final D:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lpb/k;",
            ">;"
        }
    .end annotation
.end field

.field public static final E:Lpb/x$b;


# instance fields
.field private final A:I

.field private final B:Lub/i;

.field private final a:Lpb/n;

.field private final b:Lpb/j;

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lpb/u;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lpb/u;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lpb/p$c;

.field private final f:Z

.field private final g:Lpb/b;

.field private final h:Z

.field private final i:Z

.field private final j:Lpb/m;

.field private final k:Lpb/o;

.field private final l:Ljava/net/Proxy;

.field private final m:Ljava/net/ProxySelector;

.field private final n:Lpb/b;

.field private final o:Ljavax/net/SocketFactory;

.field private final p:Ljavax/net/ssl/SSLSocketFactory;

.field private final q:Ljavax/net/ssl/X509TrustManager;

.field private final r:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lpb/k;",
            ">;"
        }
    .end annotation
.end field

.field private final s:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lpb/y;",
            ">;"
        }
    .end annotation
.end field

.field private final t:Ljavax/net/ssl/HostnameVerifier;

.field private final u:Lpb/g;

.field private final v:Lbc/c;

.field private final w:I

.field private final x:I

.field private final y:I

.field private final z:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    new-instance v0, Lpb/x$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lpb/x$b;-><init>(Lm8/g;)V

    sput-object v0, Lpb/x;->E:Lpb/x$b;

    const/4 v0, 0x2

    new-array v1, v0, [Lpb/y;

    sget-object v2, Lpb/y;->e:Lpb/y;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    sget-object v2, Lpb/y;->c:Lpb/y;

    const/4 v4, 0x1

    aput-object v2, v1, v4

    invoke-static {v1}, Lrb/b;->t([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    sput-object v1, Lpb/x;->C:Ljava/util/List;

    new-array v0, v0, [Lpb/k;

    sget-object v1, Lpb/k;->h:Lpb/k;

    aput-object v1, v0, v3

    sget-object v1, Lpb/k;->j:Lpb/k;

    aput-object v1, v0, v4

    invoke-static {v0}, Lrb/b;->t([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lpb/x;->D:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    new-instance v0, Lpb/x$a;

    invoke-direct {v0}, Lpb/x$a;-><init>()V

    invoke-direct {p0, v0}, Lpb/x;-><init>(Lpb/x$a;)V

    return-void
.end method

.method public constructor <init>(Lpb/x$a;)V
    .locals 3

    const-string v0, "builder"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Lpb/x$a;->r()Lpb/n;

    move-result-object v0

    iput-object v0, p0, Lpb/x;->a:Lpb/n;

    invoke-virtual {p1}, Lpb/x$a;->o()Lpb/j;

    move-result-object v0

    iput-object v0, p0, Lpb/x;->b:Lpb/j;

    invoke-virtual {p1}, Lpb/x$a;->x()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lrb/b;->M(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lpb/x;->c:Ljava/util/List;

    invoke-virtual {p1}, Lpb/x$a;->y()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lrb/b;->M(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lpb/x;->d:Ljava/util/List;

    invoke-virtual {p1}, Lpb/x$a;->t()Lpb/p$c;

    move-result-object v0

    iput-object v0, p0, Lpb/x;->e:Lpb/p$c;

    invoke-virtual {p1}, Lpb/x$a;->F()Z

    move-result v0

    iput-boolean v0, p0, Lpb/x;->f:Z

    invoke-virtual {p1}, Lpb/x$a;->i()Lpb/b;

    move-result-object v0

    iput-object v0, p0, Lpb/x;->g:Lpb/b;

    invoke-virtual {p1}, Lpb/x$a;->u()Z

    move-result v0

    iput-boolean v0, p0, Lpb/x;->h:Z

    invoke-virtual {p1}, Lpb/x$a;->v()Z

    move-result v0

    iput-boolean v0, p0, Lpb/x;->i:Z

    invoke-virtual {p1}, Lpb/x$a;->q()Lpb/m;

    move-result-object v0

    iput-object v0, p0, Lpb/x;->j:Lpb/m;

    invoke-virtual {p1}, Lpb/x$a;->j()Lpb/c;

    invoke-virtual {p1}, Lpb/x$a;->s()Lpb/o;

    move-result-object v0

    iput-object v0, p0, Lpb/x;->k:Lpb/o;

    invoke-virtual {p1}, Lpb/x$a;->B()Ljava/net/Proxy;

    move-result-object v0

    iput-object v0, p0, Lpb/x;->l:Ljava/net/Proxy;

    invoke-virtual {p1}, Lpb/x$a;->B()Ljava/net/Proxy;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lac/a;->a:Lac/a;

    goto :goto_1

    :cond_1
    invoke-virtual {p1}, Lpb/x$a;->D()Ljava/net/ProxySelector;

    move-result-object v0

    if-eqz v0, :cond_2

    goto :goto_0

    :cond_2
    invoke-static {}, Ljava/net/ProxySelector;->getDefault()Ljava/net/ProxySelector;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_0

    :goto_1
    iput-object v0, p0, Lpb/x;->m:Ljava/net/ProxySelector;

    invoke-virtual {p1}, Lpb/x$a;->C()Lpb/b;

    move-result-object v0

    iput-object v0, p0, Lpb/x;->n:Lpb/b;

    invoke-virtual {p1}, Lpb/x$a;->H()Ljavax/net/SocketFactory;

    move-result-object v0

    iput-object v0, p0, Lpb/x;->o:Ljavax/net/SocketFactory;

    invoke-virtual {p1}, Lpb/x$a;->p()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lpb/x;->r:Ljava/util/List;

    invoke-virtual {p1}, Lpb/x$a;->A()Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lpb/x;->s:Ljava/util/List;

    invoke-virtual {p1}, Lpb/x$a;->w()Ljavax/net/ssl/HostnameVerifier;

    move-result-object v1

    iput-object v1, p0, Lpb/x;->t:Ljavax/net/ssl/HostnameVerifier;

    invoke-virtual {p1}, Lpb/x$a;->k()I

    move-result v1

    iput v1, p0, Lpb/x;->w:I

    invoke-virtual {p1}, Lpb/x$a;->n()I

    move-result v1

    iput v1, p0, Lpb/x;->x:I

    invoke-virtual {p1}, Lpb/x$a;->E()I

    move-result v1

    iput v1, p0, Lpb/x;->y:I

    invoke-virtual {p1}, Lpb/x$a;->J()I

    move-result v1

    iput v1, p0, Lpb/x;->z:I

    invoke-virtual {p1}, Lpb/x$a;->z()I

    move-result v1

    iput v1, p0, Lpb/x;->A:I

    invoke-virtual {p1}, Lpb/x$a;->G()Lub/i;

    move-result-object v1

    if-eqz v1, :cond_3

    goto :goto_2

    :cond_3
    new-instance v1, Lub/i;

    invoke-direct {v1}, Lub/i;-><init>()V

    :goto_2
    iput-object v1, p0, Lpb/x;->B:Lub/i;

    invoke-virtual {p1}, Lpb/x$a;->I()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v1

    const/4 v2, 0x1

    if-nez v1, :cond_a

    instance-of v1, v0, Ljava/util/Collection;

    if-eqz v1, :cond_5

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_5

    :cond_4
    move v0, v2

    goto :goto_3

    :cond_5
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_6
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lpb/k;

    invoke-virtual {v1}, Lpb/k;->f()Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v0, 0x0

    :goto_3
    if-eqz v0, :cond_7

    goto :goto_4

    :cond_7
    sget-object v0, Lyb/h;->c:Lyb/h$a;

    invoke-virtual {v0}, Lyb/h$a;->e()Lyb/h;

    move-result-object v1

    invoke-virtual {v1}, Lyb/h;->p()Ljavax/net/ssl/X509TrustManager;

    move-result-object v1

    iput-object v1, p0, Lpb/x;->q:Ljavax/net/ssl/X509TrustManager;

    invoke-virtual {v0}, Lyb/h$a;->e()Lyb/h;

    move-result-object v0

    invoke-virtual {v0, v1}, Lyb/h;->g(Ljavax/net/ssl/X509TrustManager;)V

    sget-object v0, Lpb/x;->E:Lpb/x$b;

    if-nez v1, :cond_8

    invoke-static {}, Lm8/i;->o()V

    :cond_8
    invoke-static {v0, v1}, Lpb/x$b;->a(Lpb/x$b;Ljavax/net/ssl/X509TrustManager;)Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v0

    iput-object v0, p0, Lpb/x;->p:Ljavax/net/ssl/SSLSocketFactory;

    sget-object v0, Lbc/c;->a:Lbc/c$a;

    if-nez v1, :cond_9

    invoke-static {}, Lm8/i;->o()V

    :cond_9
    invoke-virtual {v0, v1}, Lbc/c$a;->a(Ljavax/net/ssl/X509TrustManager;)Lbc/c;

    move-result-object v0

    iput-object v0, p0, Lpb/x;->v:Lbc/c;

    goto :goto_5

    :cond_a
    :goto_4
    invoke-virtual {p1}, Lpb/x$a;->I()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v0

    iput-object v0, p0, Lpb/x;->p:Ljavax/net/ssl/SSLSocketFactory;

    invoke-virtual {p1}, Lpb/x$a;->l()Lbc/c;

    move-result-object v0

    iput-object v0, p0, Lpb/x;->v:Lbc/c;

    invoke-virtual {p1}, Lpb/x$a;->K()Ljavax/net/ssl/X509TrustManager;

    move-result-object v0

    iput-object v0, p0, Lpb/x;->q:Ljavax/net/ssl/X509TrustManager;

    :goto_5
    iget-object v0, p0, Lpb/x;->p:Ljavax/net/ssl/SSLSocketFactory;

    if-eqz v0, :cond_b

    sget-object v0, Lyb/h;->c:Lyb/h$a;

    invoke-virtual {v0}, Lyb/h$a;->e()Lyb/h;

    move-result-object v0

    iget-object v1, p0, Lpb/x;->p:Ljavax/net/ssl/SSLSocketFactory;

    invoke-virtual {v0, v1}, Lyb/h;->e(Ljavax/net/ssl/SSLSocketFactory;)V

    :cond_b
    invoke-virtual {p1}, Lpb/x$a;->m()Lpb/g;

    move-result-object p1

    iget-object v0, p0, Lpb/x;->v:Lbc/c;

    invoke-virtual {p1, v0}, Lpb/g;->e(Lbc/c;)Lpb/g;

    move-result-object p1

    iput-object p1, p0, Lpb/x;->u:Lpb/g;

    iget-object p1, p0, Lpb/x;->c:Ljava/util/List;

    const-string v0, "null cannot be cast to non-null type kotlin.collections.List<okhttp3.Interceptor?>"

    if-eqz p1, :cond_f

    const/4 v1, 0x0

    invoke-interface {p1, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p1

    xor-int/2addr p1, v2

    if-eqz p1, :cond_e

    iget-object p1, p0, Lpb/x;->d:Ljava/util/List;

    if-eqz p1, :cond_d

    invoke-interface {p1, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p1

    xor-int/2addr p1, v2

    if-eqz p1, :cond_c

    return-void

    :cond_c
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Null network interceptor: "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lpb/x;->d:Ljava/util/List;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_d
    new-instance p1, La8/s;

    invoke-direct {p1, v0}, La8/s;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_e
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Null interceptor: "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lpb/x;->c:Ljava/util/List;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_f
    new-instance p1, La8/s;

    invoke-direct {p1, v0}, La8/s;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public static final synthetic c()Ljava/util/List;
    .locals 1

    sget-object v0, Lpb/x;->D:Ljava/util/List;

    return-object v0
.end method

.method public static final synthetic d()Ljava/util/List;
    .locals 1

    sget-object v0, Lpb/x;->C:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public final A()I
    .locals 1

    iget v0, p0, Lpb/x;->y:I

    return v0
.end method

.method public final B()Z
    .locals 1

    iget-boolean v0, p0, Lpb/x;->f:Z

    return v0
.end method

.method public final C()Ljavax/net/SocketFactory;
    .locals 1

    iget-object v0, p0, Lpb/x;->o:Ljavax/net/SocketFactory;

    return-object v0
.end method

.method public final D()Ljavax/net/ssl/SSLSocketFactory;
    .locals 2

    iget-object v0, p0, Lpb/x;->p:Ljavax/net/ssl/SSLSocketFactory;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "CLEARTEXT-only client"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final E()I
    .locals 1

    iget v0, p0, Lpb/x;->z:I

    return v0
.end method

.method public a(Lpb/z;)Lpb/e;
    .locals 2

    const-string v0, "request"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lub/e;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lub/e;-><init>(Lpb/x;Lpb/z;Z)V

    return-object v0
.end method

.method public clone()Ljava/lang/Object;
    .locals 1

    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final e()Lpb/b;
    .locals 1

    iget-object v0, p0, Lpb/x;->g:Lpb/b;

    return-object v0
.end method

.method public final f()Lpb/c;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final g()I
    .locals 1

    iget v0, p0, Lpb/x;->w:I

    return v0
.end method

.method public final h()Lpb/g;
    .locals 1

    iget-object v0, p0, Lpb/x;->u:Lpb/g;

    return-object v0
.end method

.method public final i()I
    .locals 1

    iget v0, p0, Lpb/x;->x:I

    return v0
.end method

.method public final j()Lpb/j;
    .locals 1

    iget-object v0, p0, Lpb/x;->b:Lpb/j;

    return-object v0
.end method

.method public final k()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lpb/k;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lpb/x;->r:Ljava/util/List;

    return-object v0
.end method

.method public final l()Lpb/m;
    .locals 1

    iget-object v0, p0, Lpb/x;->j:Lpb/m;

    return-object v0
.end method

.method public final m()Lpb/n;
    .locals 1

    iget-object v0, p0, Lpb/x;->a:Lpb/n;

    return-object v0
.end method

.method public final n()Lpb/o;
    .locals 1

    iget-object v0, p0, Lpb/x;->k:Lpb/o;

    return-object v0
.end method

.method public final o()Lpb/p$c;
    .locals 1

    iget-object v0, p0, Lpb/x;->e:Lpb/p$c;

    return-object v0
.end method

.method public final p()Z
    .locals 1

    iget-boolean v0, p0, Lpb/x;->h:Z

    return v0
.end method

.method public final q()Z
    .locals 1

    iget-boolean v0, p0, Lpb/x;->i:Z

    return v0
.end method

.method public final r()Lub/i;
    .locals 1

    iget-object v0, p0, Lpb/x;->B:Lub/i;

    return-object v0
.end method

.method public final s()Ljavax/net/ssl/HostnameVerifier;
    .locals 1

    iget-object v0, p0, Lpb/x;->t:Ljavax/net/ssl/HostnameVerifier;

    return-object v0
.end method

.method public final t()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lpb/u;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lpb/x;->c:Ljava/util/List;

    return-object v0
.end method

.method public final u()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lpb/u;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lpb/x;->d:Ljava/util/List;

    return-object v0
.end method

.method public final v()I
    .locals 1

    iget v0, p0, Lpb/x;->A:I

    return v0
.end method

.method public final w()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lpb/y;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lpb/x;->s:Ljava/util/List;

    return-object v0
.end method

.method public final x()Ljava/net/Proxy;
    .locals 1

    iget-object v0, p0, Lpb/x;->l:Ljava/net/Proxy;

    return-object v0
.end method

.method public final y()Lpb/b;
    .locals 1

    iget-object v0, p0, Lpb/x;->n:Lpb/b;

    return-object v0
.end method

.method public final z()Ljava/net/ProxySelector;
    .locals 1

    iget-object v0, p0, Lpb/x;->m:Ljava/net/ProxySelector;

    return-object v0
.end method
