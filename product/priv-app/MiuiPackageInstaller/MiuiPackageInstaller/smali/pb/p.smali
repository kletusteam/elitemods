.class public abstract Lpb/p;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lpb/p$c;,
        Lpb/p$b;
    }
.end annotation


# static fields
.field public static final a:Lpb/p;

.field public static final b:Lpb/p$b;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lpb/p$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lpb/p$b;-><init>(Lm8/g;)V

    sput-object v0, Lpb/p;->b:Lpb/p$b;

    new-instance v0, Lpb/p$a;

    invoke-direct {v0}, Lpb/p$a;-><init>()V

    sput-object v0, Lpb/p;->a:Lpb/p;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lpb/e;)V
    .locals 1

    const-string v0, "call"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public b(Lpb/e;Ljava/io/IOException;)V
    .locals 1

    const-string v0, "call"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "ioe"

    invoke-static {p2, p1}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public c(Lpb/e;)V
    .locals 1

    const-string v0, "call"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public d(Lpb/e;)V
    .locals 1

    const-string v0, "call"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public e(Lpb/e;Ljava/net/InetSocketAddress;Ljava/net/Proxy;Lpb/y;)V
    .locals 0

    const-string p4, "call"

    invoke-static {p1, p4}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "inetSocketAddress"

    invoke-static {p2, p1}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "proxy"

    invoke-static {p3, p1}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public f(Lpb/e;Ljava/net/InetSocketAddress;Ljava/net/Proxy;Lpb/y;Ljava/io/IOException;)V
    .locals 0

    const-string p4, "call"

    invoke-static {p1, p4}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "inetSocketAddress"

    invoke-static {p2, p1}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "proxy"

    invoke-static {p3, p1}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "ioe"

    invoke-static {p5, p1}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public g(Lpb/e;Ljava/net/InetSocketAddress;Ljava/net/Proxy;)V
    .locals 1

    const-string v0, "call"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "inetSocketAddress"

    invoke-static {p2, p1}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "proxy"

    invoke-static {p3, p1}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public h(Lpb/e;Lpb/i;)V
    .locals 1

    const-string v0, "call"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "connection"

    invoke-static {p2, p1}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public i(Lpb/e;Lpb/i;)V
    .locals 1

    const-string v0, "call"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "connection"

    invoke-static {p2, p1}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public j(Lpb/e;Ljava/lang/String;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lpb/e;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/net/InetAddress;",
            ">;)V"
        }
    .end annotation

    const-string v0, "call"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "domainName"

    invoke-static {p2, p1}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "inetAddressList"

    invoke-static {p3, p1}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public k(Lpb/e;Ljava/lang/String;)V
    .locals 1

    const-string v0, "call"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "domainName"

    invoke-static {p2, p1}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public l(Lpb/e;Lpb/t;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lpb/e;",
            "Lpb/t;",
            "Ljava/util/List<",
            "Ljava/net/Proxy;",
            ">;)V"
        }
    .end annotation

    const-string v0, "call"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "url"

    invoke-static {p2, p1}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "proxies"

    invoke-static {p3, p1}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public m(Lpb/e;Lpb/t;)V
    .locals 1

    const-string v0, "call"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "url"

    invoke-static {p2, p1}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public n(Lpb/e;J)V
    .locals 0

    const-string p2, "call"

    invoke-static {p1, p2}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public o(Lpb/e;)V
    .locals 1

    const-string v0, "call"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public p(Lpb/e;Ljava/io/IOException;)V
    .locals 1

    const-string v0, "call"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "ioe"

    invoke-static {p2, p1}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public q(Lpb/e;Lpb/z;)V
    .locals 1

    const-string v0, "call"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "request"

    invoke-static {p2, p1}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public r(Lpb/e;)V
    .locals 1

    const-string v0, "call"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public s(Lpb/e;J)V
    .locals 0

    const-string p2, "call"

    invoke-static {p1, p2}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public t(Lpb/e;)V
    .locals 1

    const-string v0, "call"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public u(Lpb/e;Ljava/io/IOException;)V
    .locals 1

    const-string v0, "call"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "ioe"

    invoke-static {p2, p1}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public v(Lpb/e;Lpb/b0;)V
    .locals 1

    const-string v0, "call"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "response"

    invoke-static {p2, p1}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public w(Lpb/e;)V
    .locals 1

    const-string v0, "call"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public x(Lpb/e;Lpb/r;)V
    .locals 0

    const-string p2, "call"

    invoke-static {p1, p2}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public y(Lpb/e;)V
    .locals 1

    const-string v0, "call"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method
