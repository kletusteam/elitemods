.class public abstract Lpb/c0;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lpb/c0$a;,
        Lpb/c0$b;
    }
.end annotation


# static fields
.field public static final b:Lpb/c0$b;


# instance fields
.field private a:Ljava/io/Reader;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lpb/c0$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lpb/c0$b;-><init>(Lm8/g;)V

    sput-object v0, Lpb/c0;->b:Lpb/c0$b;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private final g()Ljava/nio/charset/Charset;
    .locals 2

    invoke-virtual {p0}, Lpb/c0;->n()Lpb/v;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v1, Lu8/d;->b:Ljava/nio/charset/Charset;

    invoke-virtual {v0, v1}, Lpb/v;->c(Ljava/nio/charset/Charset;)Ljava/nio/charset/Charset;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v0, Lu8/d;->b:Ljava/nio/charset/Charset;

    :goto_0
    return-object v0
.end method

.method public static final p(Lpb/v;JLdc/g;)Lpb/c0;
    .locals 1

    sget-object v0, Lpb/c0;->b:Lpb/c0$b;

    invoke-virtual {v0, p0, p1, p2, p3}, Lpb/c0$b;->b(Lpb/v;JLdc/g;)Lpb/c0;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final b()Ljava/io/Reader;
    .locals 3

    iget-object v0, p0, Lpb/c0;->a:Ljava/io/Reader;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Lpb/c0$a;

    invoke-virtual {p0}, Lpb/c0;->r()Ldc/g;

    move-result-object v1

    invoke-direct {p0}, Lpb/c0;->g()Ljava/nio/charset/Charset;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lpb/c0$a;-><init>(Ldc/g;Ljava/nio/charset/Charset;)V

    iput-object v0, p0, Lpb/c0;->a:Ljava/io/Reader;

    :goto_0
    return-object v0
.end method

.method public close()V
    .locals 1

    invoke-virtual {p0}, Lpb/c0;->r()Ldc/g;

    move-result-object v0

    invoke-static {v0}, Lrb/b;->j(Ljava/io/Closeable;)V

    return-void
.end method

.method public abstract m()J
.end method

.method public abstract n()Lpb/v;
.end method

.method public abstract r()Ldc/g;
.end method

.method public final s()Ljava/lang/String;
    .locals 3

    invoke-virtual {p0}, Lpb/c0;->r()Ldc/g;

    move-result-object v0

    :try_start_0
    invoke-direct {p0}, Lpb/c0;->g()Ljava/nio/charset/Charset;

    move-result-object v1

    invoke-static {v0, v1}, Lrb/b;->E(Ldc/g;Ljava/nio/charset/Charset;)Ljava/nio/charset/Charset;

    move-result-object v1

    invoke-interface {v0, v1}, Ldc/g;->R(Ljava/nio/charset/Charset;)Ljava/lang/String;

    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lj8/a;->a(Ljava/io/Closeable;Ljava/lang/Throwable;)V

    return-object v1

    :catchall_0
    move-exception v1

    :try_start_1
    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :catchall_1
    move-exception v2

    invoke-static {v0, v1}, Lj8/a;->a(Ljava/io/Closeable;Ljava/lang/Throwable;)V

    throw v2
.end method
