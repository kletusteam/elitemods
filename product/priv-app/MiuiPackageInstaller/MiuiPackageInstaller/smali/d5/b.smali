.class public final Ld5/b;
.super Ly4/a;


# static fields
.field public static final e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Ld5/b;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ld5/b;->e:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Lz4/b;)V
    .locals 0

    invoke-direct {p0, p1}, Ly4/a;-><init>(Lz4/b;)V

    return-void
.end method

.method public static declared-synchronized K(Lz4/b;)Ly4/a;
    .locals 2

    const-class v0, Ld5/b;

    monitor-enter v0

    :try_start_0
    new-instance v1, Ld5/b;

    invoke-direct {v1, p0}, Ld5/b;-><init>(Lz4/b;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method


# virtual methods
.method public L(Ljava/lang/Object;Le5/a;Le5/b;)I
    .locals 2

    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteClosable;->acquireReference()V

    :try_start_0
    iget-object v0, p0, Ly4/a;->a:Lb5/g;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iget-object v1, p0, Ly4/a;->c:Lz4/c;

    invoke-virtual {v1, v0, p1}, Lz4/c;->f(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/Object;)Le5/c;

    invoke-static {p1, p2, p3}, Lb5/e;->t(Ljava/lang/Object;Le5/a;Le5/b;)Lb5/f;

    move-result-object p1

    invoke-virtual {p1, v0}, Lb5/f;->h(Landroid/database/sqlite/SQLiteDatabase;)I

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteClosable;->releaseReference()V

    return p1

    :catchall_0
    move-exception p1

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_1
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteClosable;->releaseReference()V

    const/4 p1, -0x1

    return p1

    :goto_0
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteClosable;->releaseReference()V

    throw p1
.end method

.method public b(Ljava/lang/Object;)I
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v0}, Ld5/b;->L(Ljava/lang/Object;Le5/a;Le5/b;)I

    move-result p1

    return p1
.end method

.method public g(Ljava/lang/Object;)J
    .locals 2

    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteClosable;->acquireReference()V

    :try_start_0
    iget-object v0, p0, Ly4/a;->a:Lb5/g;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iget-object v1, p0, Ly4/a;->c:Lz4/c;

    invoke-virtual {v1, v0, p1}, Lz4/c;->f(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/Object;)Le5/c;

    invoke-static {p1}, Lb5/e;->q(Ljava/lang/Object;)Lb5/f;

    move-result-object v1

    invoke-virtual {v1, v0, p1}, Lb5/f;->f(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/Object;)J

    move-result-wide v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteClosable;->releaseReference()V

    return-wide v0

    :catchall_0
    move-exception p1

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_1
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteClosable;->releaseReference()V

    const-wide/16 v0, -0x1

    return-wide v0

    :goto_0
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteClosable;->releaseReference()V

    throw p1
.end method

.method public m(Ljava/lang/Class;)Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)",
            "Ljava/util/ArrayList<",
            "TT;>;"
        }
    .end annotation

    new-instance v0, Lb5/d;

    invoke-direct {v0, p1}, Lb5/d;-><init>(Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Ld5/b;->r(Lb5/d;)Ljava/util/ArrayList;

    move-result-object p1

    return-object p1
.end method

.method public n(Ljava/lang/Object;)I
    .locals 2

    invoke-static {p1}, Lz4/c;->r(Ljava/lang/Object;)Le5/c;

    move-result-object v0

    iget-object v1, p0, Ly4/a;->c:Lz4/c;

    iget-object v0, v0, Le5/c;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lz4/c;->w(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteClosable;->acquireReference()V

    :try_start_0
    iget-object v0, p0, Ly4/a;->a:Lb5/g;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-static {p1}, Lb5/e;->e(Ljava/lang/Object;)Lb5/f;

    move-result-object p1

    invoke-virtual {p1, v0}, Lb5/f;->c(Landroid/database/sqlite/SQLiteDatabase;)I

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteClosable;->releaseReference()V

    return p1

    :catchall_0
    move-exception p1

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_1
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteClosable;->releaseReference()V

    goto :goto_1

    :goto_0
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteClosable;->releaseReference()V

    throw p1

    :cond_0
    :goto_1
    const/4 p1, -0x1

    return p1
.end method

.method public p(Lb5/i;)I
    .locals 2

    invoke-virtual {p1}, Lb5/i;->f()Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lz4/c;->q(Ljava/lang/Class;Z)Le5/c;

    move-result-object v0

    iget-object v1, p0, Ly4/a;->c:Lz4/c;

    iget-object v0, v0, Le5/c;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lz4/c;->w(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteClosable;->acquireReference()V

    :try_start_0
    iget-object v0, p0, Ly4/a;->a:Lb5/g;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {p1}, Lb5/i;->d()Lb5/f;

    move-result-object p1

    invoke-virtual {p1, v0}, Lb5/f;->c(Landroid/database/sqlite/SQLiteDatabase;)I

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteClosable;->releaseReference()V

    return p1

    :catchall_0
    move-exception p1

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_1
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteClosable;->releaseReference()V

    goto :goto_1

    :goto_0
    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteClosable;->releaseReference()V

    throw p1

    :cond_0
    :goto_1
    const/4 p1, -0x1

    return p1
.end method

.method public r(Lb5/d;)Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lb5/d<",
            "TT;>;)",
            "Ljava/util/ArrayList<",
            "TT;>;"
        }
    .end annotation

    invoke-virtual {p1}, Lb5/d;->f()Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lz4/c;->q(Ljava/lang/Class;Z)Le5/c;

    move-result-object v0

    iget-object v1, p0, Ly4/a;->c:Lz4/c;

    iget-object v0, v0, Le5/c;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lz4/c;->w(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteClosable;->acquireReference()V

    :try_start_0
    invoke-virtual {p1}, Lb5/d;->e()Lb5/f;

    move-result-object v0

    iget-object v1, p0, Ly4/a;->a:Lb5/g;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-virtual {p1}, Lb5/d;->f()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lb5/f;->m(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteClosable;->releaseReference()V

    return-object p1

    :catchall_0
    move-exception p1

    invoke-virtual {p0}, Landroid/database/sqlite/SQLiteClosable;->releaseReference()V

    throw p1

    :cond_0
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    return-object p1
.end method
