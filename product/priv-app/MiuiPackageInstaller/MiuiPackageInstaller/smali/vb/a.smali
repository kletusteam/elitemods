.class public final Lvb/a;
.super Ljava/lang/Object;

# interfaces
.implements Lpb/u;


# instance fields
.field private final a:Lpb/m;


# direct methods
.method public constructor <init>(Lpb/m;)V
    .locals 1

    const-string v0, "cookieJar"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lvb/a;->a:Lpb/m;

    return-void
.end method

.method private final b(Ljava/util/List;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lpb/l;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 v1, 0x0

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    add-int/lit8 v3, v1, 0x1

    if-gez v1, :cond_0

    invoke-static {}, Lb8/j;->m()V

    :cond_0
    check-cast v2, Lpb/l;

    if-lez v1, :cond_1

    const-string v1, "; "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    invoke-virtual {v2}, Lpb/l;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x3d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Lpb/l;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v1, v3

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "StringBuilder().apply(builderAction).toString()"

    invoke-static {p1, v0}, Lm8/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method


# virtual methods
.method public a(Lpb/u$a;)Lpb/b0;
    .locals 12

    const-string v0, "chain"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1}, Lpb/u$a;->J()Lpb/z;

    move-result-object v0

    invoke-virtual {v0}, Lpb/z;->h()Lpb/z$a;

    move-result-object v1

    invoke-virtual {v0}, Lpb/z;->a()Lpb/a0;

    move-result-object v2

    const-string v3, "Content-Type"

    const-wide/16 v4, -0x1

    const-string v6, "Content-Length"

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lpb/a0;->b()Lpb/v;

    move-result-object v7

    if-eqz v7, :cond_0

    invoke-virtual {v7}, Lpb/v;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v3, v7}, Lpb/z$a;->d(Ljava/lang/String;Ljava/lang/String;)Lpb/z$a;

    :cond_0
    invoke-virtual {v2}, Lpb/a0;->a()J

    move-result-wide v7

    cmp-long v2, v7, v4

    const-string v9, "Transfer-Encoding"

    if-eqz v2, :cond_1

    invoke-static {v7, v8}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v6, v2}, Lpb/z$a;->d(Ljava/lang/String;Ljava/lang/String;)Lpb/z$a;

    invoke-virtual {v1, v9}, Lpb/z$a;->h(Ljava/lang/String;)Lpb/z$a;

    goto :goto_0

    :cond_1
    const-string v2, "chunked"

    invoke-virtual {v1, v9, v2}, Lpb/z$a;->d(Ljava/lang/String;Ljava/lang/String;)Lpb/z$a;

    invoke-virtual {v1, v6}, Lpb/z$a;->h(Ljava/lang/String;)Lpb/z$a;

    :cond_2
    :goto_0
    const-string v2, "Host"

    invoke-virtual {v0, v2}, Lpb/z;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x1

    const/4 v10, 0x0

    if-nez v7, :cond_3

    invoke-virtual {v0}, Lpb/z;->j()Lpb/t;

    move-result-object v7

    invoke-static {v7, v8, v9, v10}, Lrb/b;->L(Lpb/t;ZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v2, v7}, Lpb/z$a;->d(Ljava/lang/String;Ljava/lang/String;)Lpb/z$a;

    :cond_3
    const-string v2, "Connection"

    invoke-virtual {v0, v2}, Lpb/z;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-nez v7, :cond_4

    const-string v7, "Keep-Alive"

    invoke-virtual {v1, v2, v7}, Lpb/z$a;->d(Ljava/lang/String;Ljava/lang/String;)Lpb/z$a;

    :cond_4
    const-string v2, "Accept-Encoding"

    invoke-virtual {v0, v2}, Lpb/z;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v11, "gzip"

    if-nez v7, :cond_5

    const-string v7, "Range"

    invoke-virtual {v0, v7}, Lpb/z;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-nez v7, :cond_5

    invoke-virtual {v1, v2, v11}, Lpb/z$a;->d(Ljava/lang/String;Ljava/lang/String;)Lpb/z$a;

    move v8, v9

    :cond_5
    iget-object v2, p0, Lvb/a;->a:Lpb/m;

    invoke-virtual {v0}, Lpb/z;->j()Lpb/t;

    move-result-object v7

    invoke-interface {v2, v7}, Lpb/m;->b(Lpb/t;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v7

    xor-int/2addr v7, v9

    if-eqz v7, :cond_6

    invoke-direct {p0, v2}, Lvb/a;->b(Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    const-string v7, "Cookie"

    invoke-virtual {v1, v7, v2}, Lpb/z$a;->d(Ljava/lang/String;Ljava/lang/String;)Lpb/z$a;

    :cond_6
    const-string v2, "User-Agent"

    invoke-virtual {v0, v2}, Lpb/z;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-nez v7, :cond_7

    const-string v7, "okhttp/4.4.1"

    invoke-virtual {v1, v2, v7}, Lpb/z$a;->d(Ljava/lang/String;Ljava/lang/String;)Lpb/z$a;

    :cond_7
    invoke-virtual {v1}, Lpb/z$a;->b()Lpb/z;

    move-result-object v1

    invoke-interface {p1, v1}, Lpb/u$a;->e(Lpb/z;)Lpb/b0;

    move-result-object p1

    iget-object v1, p0, Lvb/a;->a:Lpb/m;

    invoke-virtual {v0}, Lpb/z;->j()Lpb/t;

    move-result-object v2

    invoke-virtual {p1}, Lpb/b0;->z()Lpb/s;

    move-result-object v7

    invoke-static {v1, v2, v7}, Lvb/e;->b(Lpb/m;Lpb/t;Lpb/s;)V

    invoke-virtual {p1}, Lpb/b0;->J()Lpb/b0$a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lpb/b0$a;->s(Lpb/z;)Lpb/b0$a;

    move-result-object v0

    if-eqz v8, :cond_8

    const-string v1, "Content-Encoding"

    const/4 v2, 0x2

    invoke-static {p1, v1, v10, v2, v10}, Lpb/b0;->y(Lpb/b0;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v11, v7, v9}, Lu8/g;->j(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v7

    if-eqz v7, :cond_8

    invoke-static {p1}, Lvb/e;->a(Lpb/b0;)Z

    move-result v7

    if-eqz v7, :cond_8

    invoke-virtual {p1}, Lpb/b0;->b()Lpb/c0;

    move-result-object v7

    if-eqz v7, :cond_8

    new-instance v8, Ldc/l;

    invoke-virtual {v7}, Lpb/c0;->r()Ldc/g;

    move-result-object v7

    invoke-direct {v8, v7}, Ldc/l;-><init>(Ldc/y;)V

    invoke-virtual {p1}, Lpb/b0;->z()Lpb/s;

    move-result-object v7

    invoke-virtual {v7}, Lpb/s;->d()Lpb/s$a;

    move-result-object v7

    invoke-virtual {v7, v1}, Lpb/s$a;->g(Ljava/lang/String;)Lpb/s$a;

    move-result-object v1

    invoke-virtual {v1, v6}, Lpb/s$a;->g(Ljava/lang/String;)Lpb/s$a;

    move-result-object v1

    invoke-virtual {v1}, Lpb/s$a;->e()Lpb/s;

    move-result-object v1

    invoke-virtual {v0, v1}, Lpb/b0$a;->k(Lpb/s;)Lpb/b0$a;

    invoke-static {p1, v3, v10, v2, v10}, Lpb/b0;->y(Lpb/b0;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    new-instance v1, Lvb/h;

    invoke-static {v8}, Ldc/o;->b(Ldc/y;)Ldc/g;

    move-result-object v2

    invoke-direct {v1, p1, v4, v5, v2}, Lvb/h;-><init>(Ljava/lang/String;JLdc/g;)V

    invoke-virtual {v0, v1}, Lpb/b0$a;->b(Lpb/c0;)Lpb/b0$a;

    :cond_8
    invoke-virtual {v0}, Lpb/b0$a;->c()Lpb/b0;

    move-result-object p1

    return-object p1
.end method
