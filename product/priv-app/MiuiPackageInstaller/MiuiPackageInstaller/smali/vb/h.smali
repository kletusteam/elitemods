.class public final Lvb/h;
.super Lpb/c0;


# instance fields
.field private final c:Ljava/lang/String;

.field private final d:J

.field private final e:Ldc/g;


# direct methods
.method public constructor <init>(Ljava/lang/String;JLdc/g;)V
    .locals 1

    const-string v0, "source"

    invoke-static {p4, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lpb/c0;-><init>()V

    iput-object p1, p0, Lvb/h;->c:Ljava/lang/String;

    iput-wide p2, p0, Lvb/h;->d:J

    iput-object p4, p0, Lvb/h;->e:Ldc/g;

    return-void
.end method


# virtual methods
.method public m()J
    .locals 2

    iget-wide v0, p0, Lvb/h;->d:J

    return-wide v0
.end method

.method public n()Lpb/v;
    .locals 2

    iget-object v0, p0, Lvb/h;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-object v1, Lpb/v;->g:Lpb/v$a;

    invoke-virtual {v1, v0}, Lpb/v$a;->b(Ljava/lang/String;)Lpb/v;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public r()Ldc/g;
    .locals 1

    iget-object v0, p0, Lvb/h;->e:Ldc/g;

    return-object v0
.end method
