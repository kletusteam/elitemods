.class public final Lvb/b;
.super Ljava/lang/Object;

# interfaces
.implements Lpb/u;


# instance fields
.field private final a:Z


# direct methods
.method public constructor <init>(Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lvb/b;->a:Z

    return-void
.end method


# virtual methods
.method public a(Lpb/u$a;)Lpb/b0;
    .locals 11

    const-string v0, "chain"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lvb/g;

    invoke-virtual {p1}, Lvb/g;->k()Lub/c;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lm8/i;->o()V

    :cond_0
    invoke-virtual {p1}, Lvb/g;->m()Lpb/z;

    move-result-object p1

    invoke-virtual {p1}, Lpb/z;->a()Lpb/a0;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, p1}, Lub/c;->t(Lpb/z;)V

    invoke-virtual {p1}, Lpb/z;->g()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lvb/f;->a(Ljava/lang/String;)Z

    move-result v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    if-eqz v4, :cond_4

    if-eqz v1, :cond_4

    const-string v4, "Expect"

    invoke-virtual {p1, v4}, Lpb/z;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v8, "100-continue"

    invoke-static {v8, v4, v7}, Lu8/g;->j(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v0}, Lub/c;->f()V

    invoke-virtual {v0, v7}, Lub/c;->p(Z)Lpb/b0$a;

    move-result-object v4

    invoke-virtual {v0}, Lub/c;->r()V

    move v8, v6

    goto :goto_0

    :cond_1
    move-object v4, v5

    move v8, v7

    :goto_0
    if-nez v4, :cond_3

    invoke-virtual {v1}, Lpb/a0;->e()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-virtual {v0}, Lub/c;->f()V

    invoke-virtual {v0, p1, v7}, Lub/c;->c(Lpb/z;Z)Ldc/w;

    move-result-object v9

    invoke-static {v9}, Ldc/o;->a(Ldc/w;)Ldc/f;

    move-result-object v9

    invoke-virtual {v1, v9}, Lpb/a0;->g(Ldc/f;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v0, p1, v6}, Lub/c;->c(Lpb/z;Z)Ldc/w;

    move-result-object v9

    invoke-static {v9}, Ldc/o;->a(Ldc/w;)Ldc/f;

    move-result-object v9

    invoke-virtual {v1, v9}, Lpb/a0;->g(Ldc/f;)V

    invoke-interface {v9}, Ldc/w;->close()V

    goto :goto_1

    :cond_3
    invoke-virtual {v0}, Lub/c;->n()V

    invoke-virtual {v0}, Lub/c;->h()Lub/f;

    move-result-object v9

    invoke-virtual {v9}, Lub/f;->x()Z

    move-result v9

    if-nez v9, :cond_5

    invoke-virtual {v0}, Lub/c;->m()V

    goto :goto_1

    :cond_4
    invoke-virtual {v0}, Lub/c;->n()V

    move-object v4, v5

    move v8, v7

    :cond_5
    :goto_1
    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lpb/a0;->e()Z

    move-result v1

    if-nez v1, :cond_7

    :cond_6
    invoke-virtual {v0}, Lub/c;->e()V

    :cond_7
    if-nez v4, :cond_9

    invoke-virtual {v0, v6}, Lub/c;->p(Z)Lpb/b0$a;

    move-result-object v4

    if-nez v4, :cond_8

    invoke-static {}, Lm8/i;->o()V

    :cond_8
    if-eqz v8, :cond_9

    invoke-virtual {v0}, Lub/c;->r()V

    move v8, v6

    :cond_9
    invoke-virtual {v4, p1}, Lpb/b0$a;->s(Lpb/z;)Lpb/b0$a;

    move-result-object v1

    invoke-virtual {v0}, Lub/c;->h()Lub/f;

    move-result-object v4

    invoke-virtual {v4}, Lub/f;->u()Lpb/r;

    move-result-object v4

    invoke-virtual {v1, v4}, Lpb/b0$a;->i(Lpb/r;)Lpb/b0$a;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Lpb/b0$a;->t(J)Lpb/b0$a;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    invoke-virtual {v1, v9, v10}, Lpb/b0$a;->q(J)Lpb/b0$a;

    move-result-object v1

    invoke-virtual {v1}, Lpb/b0$a;->c()Lpb/b0;

    move-result-object v1

    invoke-virtual {v1}, Lpb/b0;->n()I

    move-result v4

    const/16 v9, 0x64

    if-ne v4, v9, :cond_c

    invoke-virtual {v0, v6}, Lub/c;->p(Z)Lpb/b0$a;

    move-result-object v1

    if-nez v1, :cond_a

    invoke-static {}, Lm8/i;->o()V

    :cond_a
    if-eqz v8, :cond_b

    invoke-virtual {v0}, Lub/c;->r()V

    :cond_b
    invoke-virtual {v1, p1}, Lpb/b0$a;->s(Lpb/z;)Lpb/b0$a;

    move-result-object p1

    invoke-virtual {v0}, Lub/c;->h()Lub/f;

    move-result-object v1

    invoke-virtual {v1}, Lub/f;->u()Lpb/r;

    move-result-object v1

    invoke-virtual {p1, v1}, Lpb/b0$a;->i(Lpb/r;)Lpb/b0$a;

    move-result-object p1

    invoke-virtual {p1, v2, v3}, Lpb/b0$a;->t(J)Lpb/b0$a;

    move-result-object p1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {p1, v1, v2}, Lpb/b0$a;->q(J)Lpb/b0$a;

    move-result-object p1

    invoke-virtual {p1}, Lpb/b0$a;->c()Lpb/b0;

    move-result-object v1

    invoke-virtual {v1}, Lpb/b0;->n()I

    move-result v4

    :cond_c
    invoke-virtual {v0, v1}, Lub/c;->q(Lpb/b0;)V

    iget-boolean p1, p0, Lvb/b;->a:Z

    if-eqz p1, :cond_d

    const/16 p1, 0x65

    if-ne v4, p1, :cond_d

    invoke-virtual {v1}, Lpb/b0;->J()Lpb/b0$a;

    move-result-object p1

    sget-object v1, Lrb/b;->c:Lpb/c0;

    goto :goto_2

    :cond_d
    invoke-virtual {v1}, Lpb/b0;->J()Lpb/b0$a;

    move-result-object p1

    invoke-virtual {v0, v1}, Lub/c;->o(Lpb/b0;)Lpb/c0;

    move-result-object v1

    :goto_2
    invoke-virtual {p1, v1}, Lpb/b0$a;->b(Lpb/c0;)Lpb/b0$a;

    move-result-object p1

    invoke-virtual {p1}, Lpb/b0$a;->c()Lpb/b0;

    move-result-object p1

    invoke-virtual {p1}, Lpb/b0;->U()Lpb/z;

    move-result-object v1

    const-string v2, "Connection"

    invoke-virtual {v1, v2}, Lpb/z;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "close"

    invoke-static {v3, v1, v7}, Lu8/g;->j(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_e

    const/4 v1, 0x2

    invoke-static {p1, v2, v5, v1, v5}, Lpb/b0;->y(Lpb/b0;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1, v7}, Lu8/g;->j(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_f

    :cond_e
    invoke-virtual {v0}, Lub/c;->m()V

    :cond_f
    const/16 v0, 0xcc

    if-eq v4, v0, :cond_10

    const/16 v0, 0xcd

    if-ne v4, v0, :cond_13

    :cond_10
    invoke-virtual {p1}, Lpb/b0;->b()Lpb/c0;

    move-result-object v0

    if-eqz v0, :cond_11

    invoke-virtual {v0}, Lpb/c0;->m()J

    move-result-wide v0

    goto :goto_3

    :cond_11
    const-wide/16 v0, -0x1

    :goto_3
    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_13

    new-instance v0, Ljava/net/ProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "HTTP "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, " had non-zero Content-Length: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lpb/b0;->b()Lpb/c0;

    move-result-object p1

    if-eqz p1, :cond_12

    invoke-virtual {p1}, Lpb/c0;->m()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    :cond_12
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_13
    return-object p1
.end method
