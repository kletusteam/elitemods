.class public Lb2/c;
.super Ljava/lang/Object;


# instance fields
.field private a:Ls1/d;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lb2/a;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lb2/d$a;

.field private d:Ljava/util/concurrent/ConcurrentSkipListSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentSkipListSet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ls1/d;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lb2/c$a;

    invoke-direct {v0, p0}, Lb2/c$a;-><init>(Lb2/c;)V

    iput-object v0, p0, Lb2/c;->c:Lb2/d$a;

    new-instance v0, Ljava/util/concurrent/ConcurrentSkipListSet;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentSkipListSet;-><init>()V

    iput-object v0, p0, Lb2/c;->d:Ljava/util/concurrent/ConcurrentSkipListSet;

    iput-object p1, p0, Lb2/c;->a:Ls1/d;

    return-void
.end method

.method private a(Ljava/lang/String;)Lb2/a;
    .locals 3

    iget-object v0, p0, Lb2/c;->b:Ljava/util/List;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lb2/c;->b:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lb2/a;

    invoke-virtual {v1}, Lb2/a;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    return-object v1

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method static synthetic b(Lb2/c;)Ljava/util/concurrent/ConcurrentSkipListSet;
    .locals 0

    iget-object p0, p0, Lb2/c;->d:Ljava/util/concurrent/ConcurrentSkipListSet;

    return-object p0
.end method


# virtual methods
.method public c(Ljava/lang/String;[Ljava/lang/String;Lb2/b;)V
    .locals 8

    iget-object v0, p0, Lb2/c;->a:Ls1/d;

    invoke-virtual {v0}, Ls1/d;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    invoke-direct {p0, p1}, Lb2/c;->a(Ljava/lang/String;)Lb2/a;

    move-result-object v5

    if-eqz v5, :cond_2

    if-eqz p2, :cond_2

    array-length v0, p2

    const/4 v1, 0x1

    if-le v0, v1, :cond_2

    iget-object v0, p0, Lb2/c;->d:Ljava/util/concurrent/ConcurrentSkipListSet;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentSkipListSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    :cond_1
    iget-object v0, p0, Lb2/c;->d:Ljava/util/concurrent/ConcurrentSkipListSet;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentSkipListSet;->add(Ljava/lang/Object;)Z

    :try_start_0
    iget-object v0, p0, Lb2/c;->a:Ls1/d;

    invoke-virtual {v0}, Ls1/d;->a()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v7, Lb2/d;

    iget-object v2, p0, Lb2/c;->c:Lb2/d$a;

    new-instance v6, Lb2/c$b;

    invoke-direct {v6, p0, p3}, Lb2/c$b;-><init>(Lb2/c;Lb2/b;)V

    move-object v1, v7

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v1 .. v6}, Lb2/d;-><init>(Lb2/d$a;Ljava/lang/String;[Ljava/lang/String;Lb2/a;Lb2/b;)V

    invoke-interface {v0, v7}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    iget-object p2, p0, Lb2/c;->d:Ljava/util/concurrent/ConcurrentSkipListSet;

    invoke-virtual {p2, p1}, Ljava/util/concurrent/ConcurrentSkipListSet;->remove(Ljava/lang/Object;)Z

    :cond_2
    :goto_0
    return-void
.end method

.method public d(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lb2/a;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lb2/c;->b:Ljava/util/List;

    return-void
.end method
