.class public Lb2/d;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lb2/d$a;
    }
.end annotation


# instance fields
.field private a:Lb2/d$a;

.field private b:Ljava/lang/String;

.field private c:[Ljava/lang/String;

.field private d:Lb2/a;

.field private e:Lb2/b;


# direct methods
.method public constructor <init>(Lb2/d$a;Ljava/lang/String;[Ljava/lang/String;Lb2/a;Lb2/b;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lb2/d;->a:Lb2/d$a;

    iput-object p2, p0, Lb2/d;->b:Ljava/lang/String;

    iput-object p3, p0, Lb2/d;->c:[Ljava/lang/String;

    iput-object p4, p0, Lb2/d;->d:Lb2/a;

    iput-object p5, p0, Lb2/d;->e:Lb2/b;

    return-void
.end method

.method private a(Ljava/lang/String;I)I
    .locals 6

    iget-object v0, p0, Lb2/d;->a:Lb2/d$a;

    invoke-interface {v0}, Lb2/d$a;->a()Ljava/net/Socket;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    new-instance v3, Ljava/net/InetSocketAddress;

    invoke-direct {v3, p1, p2}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    const/16 p1, 0x1388

    const-wide v4, 0x7fffffffffffffffL

    :try_start_0
    invoke-virtual {v0, v3, p1}, Ljava/net/Socket;->connect(Ljava/net/SocketAddress;I)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/io/IOException;->printStackTrace()V

    move-wide p1, v4

    :goto_0
    cmp-long v0, p1, v4

    if-nez v0, :cond_0

    const p1, 0x7fffffff

    return p1

    :cond_0
    sub-long/2addr p1, v1

    long-to-int p1, p1

    return p1
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-object v0, p0, Lb2/d;->c:[Ljava/lang/String;

    array-length v0, v0

    new-array v0, v0, [I

    const/4 v1, 0x0

    :goto_0
    iget-object v2, p0, Lb2/d;->c:[Ljava/lang/String;

    array-length v3, v2

    if-ge v1, v3, :cond_0

    aget-object v2, v2, v1

    iget-object v3, p0, Lb2/d;->d:Lb2/a;

    invoke-virtual {v3}, Lb2/a;->b()I

    move-result v3

    invoke-direct {p0, v2, v3}, Lb2/d;->a(Ljava/lang/String;I)I

    move-result v2

    aput v2, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    invoke-static {v2, v0}, Ly1/a;->e([Ljava/lang/String;[I)[Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lb2/d;->e:Lb2/b;

    if-eqz v1, :cond_1

    iget-object v2, p0, Lb2/d;->b:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Lb2/b;->a(Ljava/lang/String;[Ljava/lang/String;)V

    :cond_1
    return-void
.end method
