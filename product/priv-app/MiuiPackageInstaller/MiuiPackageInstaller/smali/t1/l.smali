.class public Lt1/l;
.super Ljava/lang/Object;

# interfaces
.implements Lt1/e;


# instance fields
.field private a:Lt1/s;

.field private b:Lw1/a;


# direct methods
.method public constructor <init>(Lw1/a;Lt1/s;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lt1/l;->b:Lw1/a;

    iput-object p2, p0, Lt1/l;->a:Lt1/s;

    return-void
.end method


# virtual methods
.method public a(Ls1/d;Lv1/d;Lv1/j;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ls1/d;",
            "Lv1/d;",
            "Lv1/j<",
            "Lt1/h;",
            ">;)V"
        }
    .end annotation

    new-instance v0, Lv1/c;

    new-instance v1, Lt1/i;

    invoke-direct {v1}, Lt1/i;-><init>()V

    invoke-direct {v0, p2, v1}, Lv1/c;-><init>(Lv1/d;Lv1/k;)V

    new-instance p2, Lv1/g;

    new-instance v1, Lv1/e;

    invoke-virtual {p1}, Ls1/d;->x()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lu1/b;->b(Ljava/lang/String;)Lu1/b;

    move-result-object v2

    invoke-direct {v1, v2}, Lv1/e;-><init>(Lu1/b;)V

    invoke-direct {p2, v0, v1}, Lv1/g;-><init>(Lv1/c;Lv1/g$a;)V

    new-instance v0, Lv1/g;

    new-instance v1, Lv1/i;

    invoke-direct {v1, p1}, Lv1/i;-><init>(Ls1/d;)V

    invoke-direct {v0, p2, v1}, Lv1/g;-><init>(Lv1/c;Lv1/g$a;)V

    new-instance p2, Lv1/g;

    new-instance v1, Lt1/p;

    iget-object v2, p0, Lt1/l;->b:Lw1/a;

    iget-object v3, p0, Lt1/l;->a:Lt1/s;

    invoke-direct {v1, p1, v2, v3}, Lt1/p;-><init>(Ls1/d;Lw1/a;Lt1/s;)V

    invoke-direct {p2, v0, v1}, Lv1/g;-><init>(Lv1/c;Lv1/g$a;)V

    new-instance v0, Lv1/l;

    const/4 v1, 0x1

    invoke-direct {v0, p2, v1}, Lv1/l;-><init>(Lv1/c;I)V

    :try_start_0
    invoke-virtual {p1}, Ls1/d;->a()Ljava/util/concurrent/ExecutorService;

    move-result-object p1

    new-instance p2, Lv1/f;

    invoke-direct {p2, v0, p3}, Lv1/f;-><init>(Lv1/c;Lv1/j;)V

    invoke-interface {p1, p2}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    invoke-interface {p3, p1}, Lv1/j;->a(Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method
