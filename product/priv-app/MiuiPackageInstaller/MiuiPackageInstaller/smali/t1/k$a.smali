.class Lt1/k$a;
.super Ljava/lang/Object;

# interfaces
.implements Lv1/j;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lt1/k;->b(Ljava/lang/String;Lo1/g;Ljava/util/Map;Ljava/lang/String;)Lo1/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lv1/j<",
        "Lt1/h;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lo1/g;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Lt1/k;


# direct methods
.method constructor <init>(Lt1/k;Ljava/lang/String;Lo1/g;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lt1/k$a;->d:Lt1/k;

    iput-object p2, p0, Lt1/k$a;->a:Ljava/lang/String;

    iput-object p3, p0, Lt1/k$a;->b:Lo1/g;

    iput-object p4, p0, Lt1/k$a;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lt1/h;

    invoke-virtual {p0, p1}, Lt1/k$a;->b(Lt1/h;)V

    return-void
.end method

.method public a(Ljava/lang/Throwable;)V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ip request for "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lt1/k$a;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " fail"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lz1/a;->j(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object p1, p0, Lt1/k$a;->d:Lt1/k;

    invoke-static {p1}, Lt1/k;->c(Lt1/k;)Ls1/c;

    move-result-object p1

    iget-object v0, p0, Lt1/k$a;->a:Ljava/lang/String;

    iget-object v1, p0, Lt1/k$a;->b:Lo1/g;

    iget-object v2, p0, Lt1/k$a;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1, v2}, Ls1/c;->c(Ljava/lang/String;Lo1/g;Ljava/lang/String;)V

    return-void
.end method

.method public b(Lt1/h;)V
    .locals 7

    invoke-static {}, Lz1/a;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ip request for "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lt1/k$a;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lt1/k$a;->b:Lo1/g;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, " return "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lt1/h;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lz1/a;->g(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lt1/k$a;->d:Lt1/k;

    invoke-static {v0}, Lt1/k;->d(Lt1/k;)Lt1/j;

    move-result-object v1

    iget-object v2, p0, Lt1/k$a;->a:Ljava/lang/String;

    iget-object v3, p0, Lt1/k$a;->b:Lo1/g;

    invoke-virtual {p1}, Lt1/h;->e()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lt1/k$a;->c:Ljava/lang/String;

    move-object v6, p1

    invoke-virtual/range {v1 .. v6}, Lt1/j;->d(Ljava/lang/String;Lo1/g;Ljava/lang/String;Ljava/lang/String;Lt1/h;)V

    iget-object v0, p0, Lt1/k$a;->b:Lo1/g;

    sget-object v1, Lo1/g;->a:Lo1/g;

    if-eq v0, v1, :cond_1

    sget-object v1, Lo1/g;->c:Lo1/g;

    if-ne v0, v1, :cond_2

    :cond_1
    iget-object v0, p0, Lt1/k$a;->d:Lt1/k;

    invoke-static {v0}, Lt1/k;->a(Lt1/k;)Lb2/c;

    move-result-object v0

    iget-object v1, p0, Lt1/k$a;->a:Ljava/lang/String;

    invoke-virtual {p1}, Lt1/h;->d()[Ljava/lang/String;

    move-result-object p1

    new-instance v2, Lt1/k$a$a;

    invoke-direct {v2, p0}, Lt1/k$a$a;-><init>(Lt1/k$a;)V

    invoke-virtual {v0, v1, p1, v2}, Lb2/c;->c(Ljava/lang/String;[Ljava/lang/String;Lb2/b;)V

    :cond_2
    iget-object p1, p0, Lt1/k$a;->d:Lt1/k;

    invoke-static {p1}, Lt1/k;->c(Lt1/k;)Ls1/c;

    move-result-object p1

    iget-object v0, p0, Lt1/k$a;->a:Ljava/lang/String;

    iget-object v1, p0, Lt1/k$a;->b:Lo1/g;

    iget-object v2, p0, Lt1/k$a;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1, v2}, Ls1/c;->c(Ljava/lang/String;Lo1/g;Ljava/lang/String;)V

    return-void
.end method
