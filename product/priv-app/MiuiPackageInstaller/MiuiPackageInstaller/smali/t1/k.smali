.class public Lt1/k;
.super Ljava/lang/Object;


# instance fields
.field private a:Lt1/j;

.field private b:Lt1/g;

.field private c:Lb2/c;

.field private d:Lt1/b;

.field private e:Z

.field private f:Ls1/c;

.field private g:Ls1/b;


# direct methods
.method public constructor <init>(Lb2/c;Lt1/g;Lt1/j;Lt1/b;Ls1/c;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lt1/k;->e:Z

    iput-object p1, p0, Lt1/k;->c:Lb2/c;

    iput-object p2, p0, Lt1/k;->b:Lt1/g;

    iput-object p3, p0, Lt1/k;->a:Lt1/j;

    iput-object p4, p0, Lt1/k;->d:Lt1/b;

    iput-object p5, p0, Lt1/k;->f:Ls1/c;

    new-instance p1, Ls1/b;

    invoke-direct {p1}, Ls1/b;-><init>()V

    iput-object p1, p0, Lt1/k;->g:Ls1/b;

    return-void
.end method

.method static synthetic a(Lt1/k;)Lb2/c;
    .locals 0

    iget-object p0, p0, Lt1/k;->c:Lb2/c;

    return-object p0
.end method

.method static synthetic c(Lt1/k;)Ls1/c;
    .locals 0

    iget-object p0, p0, Lt1/k;->f:Ls1/c;

    return-object p0
.end method

.method static synthetic d(Lt1/k;)Lt1/j;
    .locals 0

    iget-object p0, p0, Lt1/k;->a:Lt1/j;

    return-object p0
.end method


# virtual methods
.method public b(Ljava/lang/String;Lo1/g;Ljava/util/Map;Ljava/lang/String;)Lo1/b;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lo1/g;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lo1/b;"
        }
    .end annotation

    iget-object v0, p0, Lt1/k;->d:Lt1/b;

    invoke-virtual {v0, p1}, Lt1/b;->a(Ljava/lang/String;)Z

    move-result v0

    const-string v1, "request host "

    if-eqz v0, :cond_1

    invoke-static {}, Lz1/a;->e()Z

    move-result p2

    if-eqz p2, :cond_0

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ", which is filtered"

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lz1/a;->b(Ljava/lang/String;)V

    :cond_0
    sget-object p1, Ly1/b;->c:Lo1/b;

    return-object p1

    :cond_1
    invoke-static {}, Lz1/a;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " with type "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, " extras : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p3}, Ly1/a;->g(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " cacheKey "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lz1/a;->b(Ljava/lang/String;)V

    :cond_2
    iget-object v0, p0, Lt1/k;->a:Lt1/j;

    invoke-virtual {v0, p1, p2, p4}, Lt1/j;->b(Ljava/lang/String;Lo1/g;Ljava/lang/String;)Lo1/b;

    move-result-object v0

    invoke-static {}, Lz1/a;->e()Z

    move-result v2

    if-eqz v2, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "host "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " result is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v0}, Ly1/a;->o(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lz1/a;->b(Ljava/lang/String;)V

    :cond_3
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lo1/b;->b()Z

    move-result v2

    if-eqz v2, :cond_5

    :cond_4
    iget-object v2, p0, Lt1/k;->f:Ls1/c;

    invoke-virtual {v2, p1, p2, p4}, Ls1/c;->e(Ljava/lang/String;Lo1/g;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v3, p0, Lt1/k;->b:Lt1/g;

    new-instance v8, Lt1/k$a;

    invoke-direct {v8, p0, p1, p2, p4}, Lt1/k$a;-><init>(Lt1/k;Ljava/lang/String;Lo1/g;Ljava/lang/String;)V

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    move-object v7, p4

    invoke-virtual/range {v3 .. v8}, Lt1/g;->a(Ljava/lang/String;Lo1/g;Ljava/util/Map;Ljava/lang/String;Lv1/j;)V

    :cond_5
    if-eqz v0, :cond_8

    invoke-virtual {v0}, Lo1/b;->b()Z

    move-result p3

    if-eqz p3, :cond_6

    iget-boolean p3, p0, Lt1/k;->e:Z

    if-nez p3, :cond_6

    invoke-virtual {v0}, Lo1/b;->c()Z

    move-result p3

    if-eqz p3, :cond_8

    :cond_6
    invoke-static {}, Lz1/a;->e()Z

    move-result p3

    if-eqz p3, :cond_7

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " for "

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, " and return "

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lo1/b;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " immediately"

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lz1/a;->g(Ljava/lang/String;)V

    :cond_7
    return-object v0

    :cond_8
    invoke-static {}, Lz1/a;->e()Z

    move-result p2

    if-eqz p2, :cond_9

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " and return empty immediately"

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lz1/a;->g(Ljava/lang/String;)V

    :cond_9
    sget-object p1, Ly1/b;->c:Lo1/b;

    return-object p1
.end method

.method public e(Z)V
    .locals 0

    iput-boolean p1, p0, Lt1/k;->e:Z

    return-void
.end method
