.class public Lt1/c;
.super Ljava/lang/Object;


# instance fields
.field private a:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/String;",
            "Lq1/a;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/String;",
            "Lq1/a;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/String;",
            "Lo1/b;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/String;",
            "Lo1/b;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/String;",
            "Lo1/b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lt1/c;->a:Ljava/util/concurrent/ConcurrentHashMap;

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lt1/c;->b:Ljava/util/concurrent/ConcurrentHashMap;

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lt1/c;->c:Ljava/util/concurrent/ConcurrentHashMap;

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lt1/c;->d:Ljava/util/concurrent/ConcurrentHashMap;

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lt1/c;->e:Ljava/util/concurrent/ConcurrentHashMap;

    return-void
.end method

.method private d(Ljava/lang/String;Lo1/g;Lq1/a;)V
    .locals 1

    sget-object v0, Lo1/g;->a:Lo1/g;

    if-ne p2, v0, :cond_0

    iget-object p2, p0, Lt1/c;->c:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p2, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lo1/b;

    if-eqz p2, :cond_1

    goto :goto_0

    :cond_0
    sget-object v0, Lo1/g;->b:Lo1/g;

    if-ne p2, v0, :cond_1

    iget-object p2, p0, Lt1/c;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p2, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lo1/b;

    if-eqz p2, :cond_1

    :goto_0
    invoke-virtual {p2, p3}, Lo1/b;->e(Lq1/a;)V

    :cond_1
    iget-object p2, p0, Lt1/c;->e:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p2, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lo1/b;

    if-eqz p1, :cond_2

    invoke-virtual {p1, p3}, Lo1/b;->e(Lq1/a;)V

    :cond_2
    return-void
.end method

.method private e(Ljava/lang/String;Lo1/g;[Ljava/lang/String;)V
    .locals 1

    sget-object v0, Lo1/g;->a:Lo1/g;

    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lt1/c;->c:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo1/b;

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_0
    sget-object v0, Lo1/g;->b:Lo1/g;

    if-ne p2, v0, :cond_1

    iget-object v0, p0, Lt1/c;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo1/b;

    if-eqz v0, :cond_1

    :goto_0
    invoke-virtual {v0, p3, p2}, Lo1/b;->f([Ljava/lang/String;Lo1/g;)V

    :cond_1
    iget-object v0, p0, Lt1/c;->e:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lo1/b;

    if-eqz p1, :cond_2

    invoke-virtual {p1, p3, p2}, Lo1/b;->f([Ljava/lang/String;Lo1/g;)V

    :cond_2
    return-void
.end method

.method private h(Ljava/lang/String;Lo1/g;)Lo1/b;
    .locals 2

    sget-object v0, Lt1/c$a;->a:[I

    invoke-virtual {p2}, Ljava/lang/Enum;->ordinal()I

    move-result p2

    aget p2, v0, p2

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eq p2, v0, :cond_3

    const/4 v0, 0x2

    if-eq p2, v0, :cond_2

    iget-object p2, p0, Lt1/c;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p2, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lq1/a;

    iget-object v0, p0, Lt1/c;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lq1/a;

    if-eqz p2, :cond_1

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v1, Lo1/b;

    invoke-direct {v1, p1}, Lo1/b;-><init>(Ljava/lang/String;)V

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v1, p1}, Lo1/b;->d(Ljava/util/List;)V

    goto :goto_2

    :cond_1
    :goto_0
    return-object v1

    :cond_2
    iget-object p2, p0, Lt1/c;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p2, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lq1/a;

    if-eqz p2, :cond_4

    new-instance v1, Lo1/b;

    invoke-direct {v1, p1}, Lo1/b;-><init>(Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    iget-object p2, p0, Lt1/c;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p2, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lq1/a;

    if-eqz p2, :cond_4

    new-instance v1, Lo1/b;

    invoke-direct {v1, p1}, Lo1/b;-><init>(Ljava/lang/String;)V

    :goto_1
    invoke-virtual {v1, p2}, Lo1/b;->e(Lq1/a;)V

    :cond_4
    :goto_2
    return-object v1
.end method


# virtual methods
.method public a()Ljava/util/HashMap;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Lo1/g;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iget-object v1, p0, Lt1/c;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lq1/a;

    invoke-virtual {v2}, Lq1/a;->m()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lo1/g;->a:Lo1/g;

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lt1/c;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lq1/a;

    invoke-virtual {v2}, Lq1/a;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lo1/g;

    invoke-virtual {v2}, Lq1/a;->m()Ljava/lang/String;

    move-result-object v2

    if-nez v3, :cond_1

    sget-object v3, Lo1/g;->b:Lo1/g;

    goto :goto_2

    :cond_1
    sget-object v3, Lo1/g;->c:Lo1/g;

    :goto_2
    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_2
    return-object v0
.end method

.method public b(Ljava/lang/String;Lo1/g;)Lo1/b;
    .locals 5

    sget-object v0, Lt1/c$a;->a:[I

    invoke-virtual {p2}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v1, v0, v1

    const/4 v2, 0x3

    const/4 v3, 0x2

    const/4 v4, 0x1

    if-eq v1, v4, :cond_2

    if-eq v1, v3, :cond_1

    if-eq v1, v2, :cond_0

    const/4 v1, 0x0

    goto :goto_1

    :cond_0
    iget-object v1, p0, Lt1/c;->e:Ljava/util/concurrent/ConcurrentHashMap;

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lt1/c;->c:Ljava/util/concurrent/ConcurrentHashMap;

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lt1/c;->d:Ljava/util/concurrent/ConcurrentHashMap;

    :goto_0
    invoke-virtual {v1, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lo1/b;

    :goto_1
    if-nez v1, :cond_6

    invoke-direct {p0, p1, p2}, Lt1/c;->h(Ljava/lang/String;Lo1/g;)Lo1/b;

    move-result-object v1

    if-eqz v1, :cond_6

    invoke-virtual {p2}, Ljava/lang/Enum;->ordinal()I

    move-result p2

    aget p2, v0, p2

    if-eq p2, v4, :cond_5

    if-eq p2, v3, :cond_4

    if-eq p2, v2, :cond_3

    goto :goto_3

    :cond_3
    iget-object p2, p0, Lt1/c;->e:Ljava/util/concurrent/ConcurrentHashMap;

    goto :goto_2

    :cond_4
    iget-object p2, p0, Lt1/c;->c:Ljava/util/concurrent/ConcurrentHashMap;

    goto :goto_2

    :cond_5
    iget-object p2, p0, Lt1/c;->d:Ljava/util/concurrent/ConcurrentHashMap;

    :goto_2
    invoke-virtual {p2, p1, v1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_6
    :goto_3
    return-object v1
.end method

.method public c(Ljava/lang/String;Lo1/g;[Ljava/lang/String;)Lq1/a;
    .locals 3

    sget-object v0, Lt1/c$a;->a:[I

    invoke-virtual {p2}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lt1/c;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lq1/a;

    if-nez v0, :cond_2

    return-object v2

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "type should be v4 or b6"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    iget-object v0, p0, Lt1/c;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lq1/a;

    if-nez v0, :cond_2

    return-object v2

    :cond_2
    invoke-virtual {v0, p3}, Lq1/a;->i([Ljava/lang/String;)V

    invoke-direct {p0, p1, p2, p3}, Lt1/c;->e(Ljava/lang/String;Lo1/g;[Ljava/lang/String;)V

    return-object v0
.end method

.method public f(Lq1/a;)V
    .locals 2

    invoke-virtual {p1}, Lq1/a;->p()I

    move-result v0

    sget-object v1, Lo1/g;->a:Lo1/g;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lt1/c;->a:Ljava/util/concurrent/ConcurrentHashMap;

    :goto_0
    invoke-virtual {p1}, Lq1/a;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_0
    invoke-virtual {p1}, Lq1/a;->p()I

    move-result v0

    sget-object v1, Lo1/g;->b:Lo1/g;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lt1/c;->b:Ljava/util/concurrent/ConcurrentHashMap;

    goto :goto_0

    :cond_1
    :goto_1
    return-void
.end method

.method public g()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lq1/a;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lt1/c;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    iget-object v1, p0, Lt1/c;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    iget-object v1, p0, Lt1/c;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    iget-object v1, p0, Lt1/c;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    iget-object v1, p0, Lt1/c;->c:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    iget-object v1, p0, Lt1/c;->d:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    iget-object v1, p0, Lt1/c;->e:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    return-object v0
.end method

.method public i(Ljava/lang/String;Lo1/g;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;I)Lq1/a;
    .locals 5

    sget-object v0, Lt1/c$a;->a:[I

    invoke-virtual {p2}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lt1/c;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lq1/a;

    if-nez v0, :cond_2

    invoke-static/range {p1 .. p6}, Lq1/a;->d(Ljava/lang/String;Lo1/g;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;I)Lq1/a;

    move-result-object v0

    iget-object p3, p0, Lt1/c;->a:Ljava/util/concurrent/ConcurrentHashMap;

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "type should be v4 or b6"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    iget-object v0, p0, Lt1/c;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lq1/a;

    if-nez v0, :cond_2

    invoke-static/range {p1 .. p6}, Lq1/a;->d(Ljava/lang/String;Lo1/g;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;I)Lq1/a;

    move-result-object v0

    iget-object p3, p0, Lt1/c;->b:Ljava/util/concurrent/ConcurrentHashMap;

    :goto_0
    invoke-virtual {p3, p1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-virtual {v0, v3, v4}, Lq1/a;->f(J)V

    invoke-virtual {v0, p5}, Lq1/a;->i([Ljava/lang/String;)V

    invoke-virtual {v0, p6}, Lq1/a;->e(I)V

    invoke-virtual {v0, p3}, Lq1/a;->g(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lq1/a;->h(Z)V

    :goto_1
    invoke-direct {p0, p1, p2, v0}, Lt1/c;->d(Ljava/lang/String;Lo1/g;Lq1/a;)V

    return-object v0
.end method
