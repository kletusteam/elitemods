.class public Lt1/p;
.super Ljava/lang/Object;

# interfaces
.implements Lv1/g$a;


# instance fields
.field private a:Ls1/d;

.field private b:Lw1/a;

.field private c:Lt1/s;

.field private d:J


# direct methods
.method public constructor <init>(Ls1/d;Lw1/a;Lt1/s;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lt1/p;->a:Ls1/d;

    iput-object p2, p0, Lt1/p;->b:Lw1/a;

    iput-object p3, p0, Lt1/p;->c:Lt1/s;

    return-void
.end method

.method private d(Ljava/lang/Throwable;)Z
    .locals 1

    instance-of v0, p1, Ljava/net/SocketTimeoutException;

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    instance-of v0, p1, Lv1/b;

    if-eqz v0, :cond_1

    check-cast p1, Lv1/b;

    invoke-virtual {p1}, Lv1/b;->d()Z

    move-result p1

    return p1

    :cond_1
    const/4 p1, 0x0

    return p1
.end method


# virtual methods
.method public a(Lv1/d;)V
    .locals 2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lt1/p;->d:J

    return-void
.end method

.method public b(Lv1/d;Ljava/lang/Throwable;)V
    .locals 4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lt1/p;->d:J

    sub-long/2addr v0, v2

    invoke-direct {p0, p2}, Lt1/p;->d(Ljava/lang/Throwable;)Z

    move-result p2

    if-nez p2, :cond_0

    invoke-virtual {p1}, Lv1/d;->b()I

    move-result p2

    int-to-long v2, p2

    cmp-long p2, v0, v2

    if-lez p2, :cond_2

    :cond_0
    iget-object p2, p0, Lt1/p;->a:Ls1/d;

    invoke-virtual {p1}, Lv1/d;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lv1/d;->a()I

    move-result v1

    invoke-virtual {p2, v0, v1}, Ls1/d;->e(Ljava/lang/String;I)Z

    move-result p2

    iget-object v0, p0, Lt1/p;->a:Ls1/d;

    invoke-virtual {v0}, Ls1/d;->t()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lv1/d;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lt1/p;->a:Ls1/d;

    invoke-virtual {v0}, Ls1/d;->A()I

    move-result v0

    invoke-virtual {p1, v0}, Lv1/d;->f(I)V

    if-eqz p2, :cond_1

    iget-object p1, p0, Lt1/p;->b:Lw1/a;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lw1/a;->f()V

    :cond_1
    iget-object p1, p0, Lt1/p;->c:Lt1/s;

    if-eqz p1, :cond_2

    invoke-interface {p1}, Lt1/s;->c()V

    :cond_2
    return-void
.end method

.method public c(Lv1/d;Ljava/lang/Object;)V
    .locals 1

    iget-object p2, p0, Lt1/p;->a:Ls1/d;

    invoke-virtual {p1}, Lv1/d;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lv1/d;->a()I

    move-result p1

    invoke-virtual {p2, v0, p1}, Ls1/d;->p(Ljava/lang/String;I)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lt1/p;->c:Lt1/s;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Lt1/s;->d()V

    :cond_0
    return-void
.end method
