.class Lt1/o$a;
.super Ljava/lang/Object;

# interfaces
.implements Lv1/j;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lt1/o;->d(Ljava/util/ArrayList;Lo1/g;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lv1/j<",
        "Lt1/m;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/util/ArrayList;

.field final synthetic b:Lo1/g;

.field final synthetic c:Lt1/o;


# direct methods
.method constructor <init>(Lt1/o;Ljava/util/ArrayList;Lo1/g;)V
    .locals 0

    iput-object p1, p0, Lt1/o$a;->c:Lt1/o;

    iput-object p2, p0, Lt1/o$a;->a:Ljava/util/ArrayList;

    iput-object p3, p0, Lt1/o$a;->b:Lo1/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lt1/m;

    invoke-virtual {p0, p1}, Lt1/o$a;->b(Lt1/m;)V

    return-void
.end method

.method public a(Ljava/lang/Throwable;)V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "resolve hosts for "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lt1/o$a;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " fail"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lz1/a;->j(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object p1, p0, Lt1/o$a;->a:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lt1/o$a;->c:Lt1/o;

    invoke-static {v1}, Lt1/o;->b(Lt1/o;)Ls1/c;

    move-result-object v1

    iget-object v2, p0, Lt1/o$a;->b:Lo1/g;

    invoke-virtual {v1, v0, v2}, Ls1/c;->b(Ljava/lang/String;Lo1/g;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public b(Lt1/m;)V
    .locals 5

    invoke-static {}, Lz1/a;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "resolve hosts for "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lt1/o$a;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lt1/o$a;->b:Lo1/g;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, " return "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lz1/a;->b(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lt1/o$a;->c:Lt1/o;

    invoke-static {v0}, Lt1/o;->c(Lt1/o;)Lt1/j;

    move-result-object v0

    iget-object v1, p0, Lt1/o$a;->b:Lo1/g;

    invoke-virtual {v0, v1, p1}, Lt1/j;->f(Lo1/g;Lt1/m;)V

    iget-object v0, p0, Lt1/o$a;->b:Lo1/g;

    sget-object v1, Lo1/g;->a:Lo1/g;

    if-eq v0, v1, :cond_1

    sget-object v1, Lo1/g;->c:Lo1/g;

    if-ne v0, v1, :cond_2

    :cond_1
    invoke-virtual {p1}, Lt1/m;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v2, p0, Lt1/o$a;->c:Lt1/o;

    invoke-static {v2}, Lt1/o;->a(Lt1/o;)Lb2/c;

    move-result-object v2

    invoke-virtual {p1, v1}, Lt1/m;->a(Ljava/lang/String;)Lt1/m$a;

    move-result-object v3

    invoke-virtual {v3}, Lt1/m$a;->d()[Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lt1/o$a$a;

    invoke-direct {v4, p0}, Lt1/o$a$a;-><init>(Lt1/o$a;)V

    invoke-virtual {v2, v1, v3, v4}, Lb2/c;->c(Ljava/lang/String;[Ljava/lang/String;Lb2/b;)V

    goto :goto_0

    :cond_2
    iget-object p1, p0, Lt1/o$a;->a:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lt1/o$a;->c:Lt1/o;

    invoke-static {v1}, Lt1/o;->b(Lt1/o;)Ls1/c;

    move-result-object v1

    iget-object v2, p0, Lt1/o$a;->b:Lo1/g;

    invoke-virtual {v1, v0, v2}, Ls1/c;->b(Ljava/lang/String;Lo1/g;)V

    goto :goto_1

    :cond_3
    return-void
.end method
