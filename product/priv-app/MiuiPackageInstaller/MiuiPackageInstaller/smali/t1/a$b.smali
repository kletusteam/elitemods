.class final enum Lt1/a$b;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lt1/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lt1/a$b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lt1/a$b;

.field public static final enum b:Lt1/a$b;

.field public static final enum c:Lt1/a$b;

.field private static final synthetic d:[Lt1/a$b;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    new-instance v0, Lt1/a$b;

    const-string v1, "NORMAL"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lt1/a$b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lt1/a$b;->a:Lt1/a$b;

    new-instance v1, Lt1/a$b;

    const-string v3, "PRE_DISABLE"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lt1/a$b;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lt1/a$b;->b:Lt1/a$b;

    new-instance v3, Lt1/a$b;

    const-string v5, "DISABLE"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lt1/a$b;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lt1/a$b;->c:Lt1/a$b;

    const/4 v5, 0x3

    new-array v5, v5, [Lt1/a$b;

    aput-object v0, v5, v2

    aput-object v1, v5, v4

    aput-object v3, v5, v6

    sput-object v5, Lt1/a$b;->d:[Lt1/a$b;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lt1/a$b;
    .locals 1

    const-class v0, Lt1/a$b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lt1/a$b;

    return-object p0
.end method

.method public static values()[Lt1/a$b;
    .locals 1

    sget-object v0, Lt1/a$b;->d:[Lt1/a$b;

    invoke-virtual {v0}, [Lt1/a$b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lt1/a$b;

    return-object v0
.end method
