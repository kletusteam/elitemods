.class public Lt1/o;
.super Ljava/lang/Object;


# instance fields
.field private a:Lt1/j;

.field private b:Lt1/g;

.field private c:Lb2/c;

.field private d:Lt1/b;

.field private e:Ls1/c;


# direct methods
.method public constructor <init>(Lt1/j;Lt1/g;Lb2/c;Lt1/b;Ls1/c;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lt1/o;->a:Lt1/j;

    iput-object p2, p0, Lt1/o;->b:Lt1/g;

    iput-object p3, p0, Lt1/o;->c:Lb2/c;

    iput-object p4, p0, Lt1/o;->d:Lt1/b;

    iput-object p5, p0, Lt1/o;->e:Ls1/c;

    return-void
.end method

.method static synthetic a(Lt1/o;)Lb2/c;
    .locals 0

    iget-object p0, p0, Lt1/o;->c:Lb2/c;

    return-object p0
.end method

.method static synthetic b(Lt1/o;)Ls1/c;
    .locals 0

    iget-object p0, p0, Lt1/o;->e:Ls1/c;

    return-object p0
.end method

.method static synthetic c(Lt1/o;)Lt1/j;
    .locals 0

    iget-object p0, p0, Lt1/o;->a:Lt1/j;

    return-object p0
.end method


# virtual methods
.method public d(Ljava/util/ArrayList;Lo1/g;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;",
            "Lo1/g;",
            ")V"
        }
    .end annotation

    invoke-static {}, Lz1/a;->e()Z

    move-result v0

    const-string v1, " "

    const-string v2, "resolve host "

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lz1/a;->b(Ljava/lang/String;)V

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result p1

    const/4 v3, 0x5

    div-int/2addr p1, v3

    add-int/lit8 p1, p1, 0x1

    const/4 v4, 0x0

    move v5, v4

    :goto_0
    if-ge v5, p1, :cond_7

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    :cond_1
    :goto_1
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v7, v3, :cond_4

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-lez v7, :cond_4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    iget-object v8, p0, Lt1/o;->a:Lt1/j;

    const/4 v9, 0x0

    invoke-virtual {v8, v7, p2, v9}, Lt1/j;->b(Ljava/lang/String;Lo1/g;Ljava/lang/String;)Lo1/b;

    move-result-object v8

    invoke-static {v7}, Ly1/a;->k(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-static {v7}, Ly1/a;->n(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_3

    iget-object v9, p0, Lt1/o;->d:Lt1/b;

    invoke-virtual {v9, v7}, Lt1/b;->a(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_3

    if-eqz v8, :cond_2

    invoke-virtual {v8}, Lo1/b;->b()Z

    move-result v8

    if-eqz v8, :cond_3

    :cond_2
    iget-object v8, p0, Lt1/o;->e:Ls1/c;

    invoke-virtual {v8, v7, p2}, Ls1/c;->d(Ljava/lang/String;Lo1/g;)Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    invoke-static {}, Lz1/a;->e()Z

    move-result v8

    if-eqz v8, :cond_1

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "resolve ignore host "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lz1/a;->b(Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-gtz v7, :cond_5

    goto :goto_2

    :cond_5
    invoke-static {}, Lz1/a;->e()Z

    move-result v7

    if-eqz v7, :cond_6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lz1/a;->g(Ljava/lang/String;)V

    :cond_6
    iget-object v7, p0, Lt1/o;->b:Lt1/g;

    new-instance v8, Lt1/o$a;

    invoke-direct {v8, p0, v6, p2}, Lt1/o$a;-><init>(Lt1/o;Ljava/util/ArrayList;Lo1/g;)V

    invoke-virtual {v7, v6, p2, v8}, Lt1/g;->b(Ljava/util/ArrayList;Lo1/g;Lv1/j;)V

    :goto_2
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_0

    :cond_7
    return-void
.end method
