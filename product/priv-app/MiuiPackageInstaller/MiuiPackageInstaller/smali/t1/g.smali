.class public Lt1/g;
.super Ljava/lang/Object;


# instance fields
.field private a:Ls1/d;

.field private b:Lw1/a;

.field private c:Lt1/a;

.field private d:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ls1/i;


# direct methods
.method public constructor <init>(Ls1/d;Lw1/a;Ls1/i;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lt1/g;->a:Ls1/d;

    iput-object p2, p0, Lt1/g;->b:Lw1/a;

    new-instance p1, Lt1/a;

    invoke-direct {p1, p2}, Lt1/a;-><init>(Lw1/a;)V

    iput-object p1, p0, Lt1/g;->c:Lt1/a;

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lt1/g;->d:Ljava/util/HashMap;

    iput-object p3, p0, Lt1/g;->e:Ls1/i;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Lo1/g;Ljava/util/Map;Ljava/lang/String;Lv1/j;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lo1/g;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Lv1/j<",
            "Lt1/h;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lt1/g;->a:Ls1/d;

    iget-object v5, p0, Lt1/g;->d:Ljava/util/HashMap;

    iget-object v6, p0, Lt1/g;->e:Ls1/i;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-static/range {v0 .. v6}, Lt1/f;->e(Ls1/d;Ljava/lang/String;Lo1/g;Ljava/util/Map;Ljava/lang/String;Ljava/util/Map;Ls1/i;)Lv1/d;

    move-result-object p3

    invoke-static {}, Lz1/a;->e()Z

    move-result p4

    if-eqz p4, :cond_0

    new-instance p4, Ljava/lang/StringBuilder;

    invoke-direct {p4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "start async ip request for "

    invoke-virtual {p4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " "

    invoke-virtual {p4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lz1/a;->b(Ljava/lang/String;)V

    :cond_0
    iget-object p1, p0, Lt1/g;->c:Lt1/a;

    invoke-virtual {p1}, Lt1/a;->a()Lt1/e;

    move-result-object p1

    iget-object p2, p0, Lt1/g;->a:Ls1/d;

    invoke-interface {p1, p2, p3, p5}, Lt1/e;->a(Ls1/d;Lv1/d;Lv1/j;)V

    return-void
.end method

.method public b(Ljava/util/ArrayList;Lo1/g;Lv1/j;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;",
            "Lo1/g;",
            "Lv1/j<",
            "Lt1/m;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lt1/g;->a:Ls1/d;

    iget-object v1, p0, Lt1/g;->e:Ls1/i;

    invoke-static {v0, p1, p2, v1}, Lt1/f;->f(Ls1/d;Ljava/util/ArrayList;Lo1/g;Ls1/i;)Lv1/d;

    move-result-object v0

    invoke-static {}, Lz1/a;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "start resolve hosts async for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " "

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lz1/a;->b(Ljava/lang/String;)V

    :cond_0
    new-instance p1, Lv1/c;

    new-instance p2, Lt1/n;

    invoke-direct {p2}, Lt1/n;-><init>()V

    invoke-direct {p1, v0, p2}, Lv1/c;-><init>(Lv1/d;Lv1/k;)V

    new-instance p2, Lv1/g;

    new-instance v0, Lv1/e;

    iget-object v1, p0, Lt1/g;->a:Ls1/d;

    invoke-virtual {v1}, Ls1/d;->x()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lu1/b;->b(Ljava/lang/String;)Lu1/b;

    move-result-object v1

    invoke-direct {v0, v1}, Lv1/e;-><init>(Lu1/b;)V

    invoke-direct {p2, p1, v0}, Lv1/g;-><init>(Lv1/c;Lv1/g$a;)V

    new-instance p1, Lv1/g;

    new-instance v0, Lv1/i;

    iget-object v1, p0, Lt1/g;->a:Ls1/d;

    invoke-direct {v0, v1}, Lv1/i;-><init>(Ls1/d;)V

    invoke-direct {p1, p2, v0}, Lv1/g;-><init>(Lv1/c;Lv1/g$a;)V

    new-instance p2, Lv1/g;

    new-instance v0, Lt1/p;

    iget-object v1, p0, Lt1/g;->a:Ls1/d;

    iget-object v2, p0, Lt1/g;->b:Lw1/a;

    iget-object v3, p0, Lt1/g;->c:Lt1/a;

    invoke-direct {v0, v1, v2, v3}, Lt1/p;-><init>(Ls1/d;Lw1/a;Lt1/s;)V

    invoke-direct {p2, p1, v0}, Lv1/g;-><init>(Lv1/c;Lv1/g$a;)V

    new-instance p1, Lv1/l;

    const/4 v0, 0x1

    invoke-direct {p1, p2, v0}, Lv1/l;-><init>(Lv1/c;I)V

    :try_start_0
    iget-object p2, p0, Lt1/g;->a:Ls1/d;

    invoke-virtual {p2}, Ls1/d;->a()Ljava/util/concurrent/ExecutorService;

    move-result-object p2

    new-instance v0, Lv1/f;

    invoke-direct {v0, p1, p3}, Lv1/f;-><init>(Lv1/c;Lv1/j;)V

    invoke-interface {p2, v0}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    invoke-interface {p3, p1}, Lv1/j;->a(Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method public c()V
    .locals 1

    iget-object v0, p0, Lt1/g;->c:Lt1/a;

    invoke-virtual {v0}, Lt1/a;->b()V

    return-void
.end method
