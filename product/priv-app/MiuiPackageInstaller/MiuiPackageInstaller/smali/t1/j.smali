.class public Lt1/j;
.super Ljava/lang/Object;


# instance fields
.field private a:Lq1/b;

.field private b:Z

.field private c:Ls1/d;

.field private d:Lb2/c;

.field private e:Lt1/d;


# direct methods
.method public constructor <init>(Ls1/d;Lb2/c;Lq1/b;Lt1/d;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lt1/j;->b:Z

    iput-object p1, p0, Lt1/j;->c:Ls1/d;

    iput-object p2, p0, Lt1/j;->d:Lb2/c;

    iput-object p3, p0, Lt1/j;->a:Lq1/b;

    iput-object p4, p0, Lt1/j;->e:Lt1/d;

    return-void
.end method

.method static synthetic c(Lt1/j;)Lq1/b;
    .locals 0

    iget-object p0, p0, Lt1/j;->a:Lq1/b;

    return-object p0
.end method

.method private e(Ljava/lang/String;Lo1/g;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lt1/j;->e:Lt1/d;

    invoke-virtual {v0, p3}, Lt1/d;->a(Ljava/lang/String;)Lt1/c;

    move-result-object p3

    invoke-virtual {p3, p1, p2, p4}, Lt1/c;->c(Ljava/lang/String;Lo1/g;[Ljava/lang/String;)Lq1/a;

    move-result-object p1

    iget-boolean p2, p0, Lt1/j;->b:Z

    if-eqz p2, :cond_0

    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :try_start_0
    iget-object p1, p0, Lt1/j;->c:Ls1/d;

    invoke-virtual {p1}, Ls1/d;->l()Ljava/util/concurrent/ExecutorService;

    move-result-object p1

    new-instance p3, Lt1/j$b;

    invoke-direct {p3, p0, p2}, Lt1/j$b;-><init>(Lt1/j;Ljava/util/ArrayList;)V

    invoke-interface {p1, p3}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_0
    return-void
.end method

.method static synthetic g(Lt1/j;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lt1/j;->k(Z)V

    return-void
.end method

.method private i(Ljava/lang/String;Lo1/g;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;I)Lq1/a;
    .locals 8

    iget-object v0, p0, Lt1/j;->e:Lt1/d;

    invoke-virtual {v0, p4}, Lt1/d;->a(Ljava/lang/String;)Lt1/c;

    move-result-object v1

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move v7, p6

    invoke-virtual/range {v1 .. v7}, Lt1/c;->i(Ljava/lang/String;Lo1/g;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;I)Lq1/a;

    move-result-object p1

    return-object p1
.end method

.method private k(Z)V
    .locals 5

    iget-object v0, p0, Lt1/j;->a:Lq1/b;

    invoke-virtual {v0}, Lq1/b;->g()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lq1/a;

    iget-object v3, p0, Lt1/j;->e:Lt1/d;

    invoke-virtual {v2}, Lq1/a;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lt1/d;->a(Ljava/lang/String;)Lt1/c;

    move-result-object v3

    invoke-virtual {v3, v2}, Lt1/c;->f(Lq1/a;)V

    goto :goto_0

    :cond_0
    if-eqz p1, :cond_1

    iget-object p1, p0, Lt1/j;->a:Lq1/b;

    invoke-virtual {p1, v0}, Lq1/b;->m(Ljava/util/List;)V

    goto :goto_2

    :cond_1
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lq1/a;

    invoke-virtual {v2}, Lq1/a;->q()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lt1/j;->a:Lq1/b;

    invoke-virtual {v1, p1}, Lq1/b;->m(Ljava/util/List;)V

    :goto_2
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_4
    :goto_3
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lq1/a;

    invoke-virtual {v0}, Lq1/a;->q()Z

    move-result v1

    if-nez v1, :cond_4

    invoke-static {}, Lo1/g;->values()[Lo1/g;

    move-result-object v1

    invoke-virtual {v0}, Lq1/a;->p()I

    move-result v2

    aget-object v1, v1, v2

    sget-object v2, Lo1/g;->a:Lo1/g;

    if-ne v1, v2, :cond_4

    iget-object v1, p0, Lt1/j;->d:Lb2/c;

    invoke-virtual {v0}, Lq1/a;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lq1/a;->o()[Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lt1/j$a;

    invoke-direct {v4, p0, v0}, Lt1/j$a;-><init>(Lt1/j;Lq1/a;)V

    invoke-virtual {v1, v2, v3, v4}, Lb2/c;->c(Ljava/lang/String;[Ljava/lang/String;Lb2/b;)V

    goto :goto_3

    :cond_5
    return-void
.end method


# virtual methods
.method public a()Ljava/util/HashMap;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Lo1/g;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lt1/j;->e:Lt1/d;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lt1/d;->a(Ljava/lang/String;)Lt1/c;

    move-result-object v0

    invoke-virtual {v0}, Lt1/c;->a()Ljava/util/HashMap;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;Lo1/g;Ljava/lang/String;)Lo1/b;
    .locals 1

    iget-object v0, p0, Lt1/j;->e:Lt1/d;

    invoke-virtual {v0, p3}, Lt1/d;->a(Ljava/lang/String;)Lt1/c;

    move-result-object p3

    invoke-virtual {p3, p1, p2}, Lt1/c;->b(Ljava/lang/String;Lo1/g;)Lo1/b;

    move-result-object p1

    return-object p1
.end method

.method public d(Ljava/lang/String;Lo1/g;Ljava/lang/String;Ljava/lang/String;Lt1/h;)V
    .locals 9

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sget-object v1, Lt1/j$g;->a:[I

    invoke-virtual {p2}, Ljava/lang/Enum;->ordinal()I

    move-result p2

    aget p2, v1, p2

    const/4 v1, 0x1

    if-eq p2, v1, :cond_2

    const/4 v1, 0x2

    if-eq p2, v1, :cond_1

    const/4 v1, 0x3

    if-eq p2, v1, :cond_0

    goto :goto_1

    :cond_0
    sget-object v4, Lo1/g;->a:Lo1/g;

    invoke-virtual {p5}, Lt1/h;->d()[Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p5}, Lt1/h;->a()I

    move-result v8

    move-object v2, p0

    move-object v3, p1

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v2 .. v8}, Lt1/j;->i(Ljava/lang/String;Lo1/g;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;I)Lq1/a;

    move-result-object p2

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v3, Lo1/g;->b:Lo1/g;

    invoke-virtual {p5}, Lt1/h;->c()[Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p5}, Lt1/h;->a()I

    move-result v7

    move-object v1, p0

    move-object v2, p1

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v1 .. v7}, Lt1/j;->i(Ljava/lang/String;Lo1/g;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;I)Lq1/a;

    move-result-object p1

    goto :goto_0

    :cond_1
    sget-object v3, Lo1/g;->b:Lo1/g;

    invoke-virtual {p5}, Lt1/h;->c()[Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p5}, Lt1/h;->a()I

    move-result v7

    move-object v1, p0

    move-object v2, p1

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v1 .. v7}, Lt1/j;->i(Ljava/lang/String;Lo1/g;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;I)Lq1/a;

    move-result-object p1

    goto :goto_0

    :cond_2
    sget-object v3, Lo1/g;->a:Lo1/g;

    invoke-virtual {p5}, Lt1/h;->d()[Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p5}, Lt1/h;->a()I

    move-result v7

    move-object v1, p0

    move-object v2, p1

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v1 .. v7}, Lt1/j;->i(Ljava/lang/String;Lo1/g;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;I)Lq1/a;

    move-result-object p1

    :goto_0
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_1
    iget-boolean p1, p0, Lt1/j;->b:Z

    if-eqz p1, :cond_3

    :try_start_0
    iget-object p1, p0, Lt1/j;->c:Ls1/d;

    invoke-virtual {p1}, Ls1/d;->l()Ljava/util/concurrent/ExecutorService;

    move-result-object p1

    new-instance p2, Lt1/j$c;

    invoke-direct {p2, p0, v0}, Lt1/j$c;-><init>(Lt1/j;Ljava/util/ArrayList;)V

    invoke-interface {p1, p2}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_3
    return-void
.end method

.method public f(Lo1/g;Lt1/m;)V
    .locals 10

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p2}, Lt1/m;->c()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    sget-object v3, Lt1/j$g;->a:[I

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result v4

    aget v3, v3, v4

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    goto :goto_0

    :cond_0
    sget-object v5, Lo1/g;->a:Lo1/g;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {p2, v2}, Lt1/m;->a(Ljava/lang/String;)Lt1/m$a;

    move-result-object v3

    invoke-virtual {v3}, Lt1/m$a;->d()[Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p2, v2}, Lt1/m;->a(Ljava/lang/String;)Lt1/m$a;

    move-result-object v3

    invoke-virtual {v3}, Lt1/m$a;->a()I

    move-result v9

    move-object v3, p0

    move-object v4, v2

    invoke-direct/range {v3 .. v9}, Lt1/j;->i(Ljava/lang/String;Lo1/g;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;I)Lq1/a;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v5, Lo1/g;->b:Lo1/g;

    invoke-virtual {p2, v2}, Lt1/m;->a(Ljava/lang/String;)Lt1/m$a;

    move-result-object v3

    invoke-virtual {v3}, Lt1/m$a;->e()[Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p2, v2}, Lt1/m;->a(Ljava/lang/String;)Lt1/m$a;

    move-result-object v3

    invoke-virtual {v3}, Lt1/m$a;->a()I

    move-result v9

    move-object v3, p0

    invoke-direct/range {v3 .. v9}, Lt1/j;->i(Ljava/lang/String;Lo1/g;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;I)Lq1/a;

    move-result-object v2

    goto :goto_1

    :cond_1
    sget-object v5, Lo1/g;->b:Lo1/g;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {p2, v2}, Lt1/m;->a(Ljava/lang/String;)Lt1/m$a;

    move-result-object v3

    invoke-virtual {v3}, Lt1/m$a;->e()[Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p2, v2}, Lt1/m;->a(Ljava/lang/String;)Lt1/m$a;

    move-result-object v3

    invoke-virtual {v3}, Lt1/m$a;->a()I

    move-result v9

    move-object v3, p0

    move-object v4, v2

    invoke-direct/range {v3 .. v9}, Lt1/j;->i(Ljava/lang/String;Lo1/g;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;I)Lq1/a;

    move-result-object v2

    goto :goto_1

    :cond_2
    sget-object v5, Lo1/g;->a:Lo1/g;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {p2, v2}, Lt1/m;->a(Ljava/lang/String;)Lt1/m$a;

    move-result-object v3

    invoke-virtual {v3}, Lt1/m$a;->d()[Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p2, v2}, Lt1/m;->a(Ljava/lang/String;)Lt1/m$a;

    move-result-object v3

    invoke-virtual {v3}, Lt1/m$a;->a()I

    move-result v9

    move-object v3, p0

    move-object v4, v2

    invoke-direct/range {v3 .. v9}, Lt1/j;->i(Ljava/lang/String;Lo1/g;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;I)Lq1/a;

    move-result-object v2

    :goto_1
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_3
    iget-boolean p1, p0, Lt1/j;->b:Z

    if-eqz p1, :cond_4

    :try_start_0
    iget-object p1, p0, Lt1/j;->c:Ls1/d;

    invoke-virtual {p1}, Ls1/d;->l()Ljava/util/concurrent/ExecutorService;

    move-result-object p1

    new-instance p2, Lt1/j$d;

    invoke-direct {p2, p0, v0}, Lt1/j$d;-><init>(Lt1/j;Ljava/util/ArrayList;)V

    invoke-interface {p1, p2}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_4
    return-void
.end method

.method public h(Ljava/lang/String;Lo1/g;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 1

    sget-object v0, Lt1/j$g;->a:[I

    invoke-virtual {p2}, Ljava/lang/Enum;->ordinal()I

    move-result p2

    aget p2, v0, p2

    const/4 v0, 0x1

    if-eq p2, v0, :cond_1

    const/4 v0, 0x2

    if-eq p2, v0, :cond_0

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "update both is impossible for "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lz1/a;->c(Ljava/lang/String;)V

    goto :goto_1

    :cond_0
    sget-object p2, Lo1/g;->b:Lo1/g;

    goto :goto_0

    :cond_1
    sget-object p2, Lo1/g;->a:Lo1/g;

    :goto_0
    invoke-direct {p0, p1, p2, p3, p4}, Lt1/j;->e(Ljava/lang/String;Lo1/g;Ljava/lang/String;[Ljava/lang/String;)V

    :goto_1
    return-void
.end method

.method public j()V
    .locals 3

    iget-object v0, p0, Lt1/j;->e:Lt1/d;

    invoke-virtual {v0}, Lt1/d;->b()Ljava/util/List;

    move-result-object v0

    iget-boolean v1, p0, Lt1/j;->b:Z

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lt1/j;->c:Ls1/d;

    invoke-virtual {v1}, Ls1/d;->l()Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    new-instance v2, Lt1/j$e;

    invoke-direct {v2, p0, v0}, Lt1/j$e;-><init>(Lt1/j;Ljava/util/List;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_0
    return-void
.end method

.method public l(ZZ)V
    .locals 1

    iput-boolean p1, p0, Lt1/j;->b:Z

    :try_start_0
    iget-object p1, p0, Lt1/j;->c:Ls1/d;

    invoke-virtual {p1}, Ls1/d;->l()Ljava/util/concurrent/ExecutorService;

    move-result-object p1

    new-instance v0, Lt1/j$f;

    invoke-direct {v0, p0, p2}, Lt1/j$f;-><init>(Lt1/j;Z)V

    invoke-interface {p1, v0}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    return-void
.end method
