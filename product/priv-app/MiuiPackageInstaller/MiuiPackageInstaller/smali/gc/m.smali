.class public final Lgc/m;
.super Ljava/lang/Object;


# direct methods
.method public static final a(Lgc/b;Ld8/d;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lgc/b<",
            "TT;>;",
            "Ld8/d<",
            "-TT;>;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    new-instance v0, Lv8/k;

    invoke-static {p1}, Le8/b;->b(Ld8/d;)Ld8/d;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lv8/k;-><init>(Ld8/d;I)V

    new-instance v1, Lgc/m$a;

    invoke-direct {v1, p0}, Lgc/m$a;-><init>(Lgc/b;)V

    invoke-interface {v0, v1}, Lv8/j;->k(Ll8/l;)V

    new-instance v1, Lgc/m$c;

    invoke-direct {v1, v0}, Lgc/m$c;-><init>(Lv8/j;)V

    invoke-interface {p0, v1}, Lgc/b;->M(Lgc/d;)V

    invoke-virtual {v0}, Lv8/k;->v()Ljava/lang/Object;

    move-result-object p0

    invoke-static {}, Le8/b;->c()Ljava/lang/Object;

    move-result-object v0

    if-ne p0, v0, :cond_0

    invoke-static {p1}, Lf8/h;->c(Ld8/d;)V

    :cond_0
    return-object p0
.end method

.method public static final b(Lgc/b;Ld8/d;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lgc/b<",
            "TT;>;",
            "Ld8/d<",
            "-TT;>;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    new-instance v0, Lv8/k;

    invoke-static {p1}, Le8/b;->b(Ld8/d;)Ld8/d;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lv8/k;-><init>(Ld8/d;I)V

    new-instance v1, Lgc/m$b;

    invoke-direct {v1, p0}, Lgc/m$b;-><init>(Lgc/b;)V

    invoke-interface {v0, v1}, Lv8/j;->k(Ll8/l;)V

    new-instance v1, Lgc/m$d;

    invoke-direct {v1, v0}, Lgc/m$d;-><init>(Lv8/j;)V

    invoke-interface {p0, v1}, Lgc/b;->M(Lgc/d;)V

    invoke-virtual {v0}, Lv8/k;->v()Ljava/lang/Object;

    move-result-object p0

    invoke-static {}, Le8/b;->c()Ljava/lang/Object;

    move-result-object v0

    if-ne p0, v0, :cond_0

    invoke-static {p1}, Lf8/h;->c(Ld8/d;)V

    :cond_0
    return-object p0
.end method

.method public static final c(Lgc/b;Ld8/d;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lgc/b<",
            "TT;>;",
            "Ld8/d<",
            "-",
            "Lgc/t<",
            "TT;>;>;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    new-instance v0, Lv8/k;

    invoke-static {p1}, Le8/b;->b(Ld8/d;)Ld8/d;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lv8/k;-><init>(Ld8/d;I)V

    new-instance v1, Lgc/m$e;

    invoke-direct {v1, p0}, Lgc/m$e;-><init>(Lgc/b;)V

    invoke-interface {v0, v1}, Lv8/j;->k(Ll8/l;)V

    new-instance v1, Lgc/m$f;

    invoke-direct {v1, v0}, Lgc/m$f;-><init>(Lv8/j;)V

    invoke-interface {p0, v1}, Lgc/b;->M(Lgc/d;)V

    invoke-virtual {v0}, Lv8/k;->v()Ljava/lang/Object;

    move-result-object p0

    invoke-static {}, Le8/b;->c()Ljava/lang/Object;

    move-result-object v0

    if-ne p0, v0, :cond_0

    invoke-static {p1}, Lf8/h;->c(Ld8/d;)V

    :cond_0
    return-object p0
.end method

.method public static final d(Ljava/lang/Exception;Ld8/d;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Exception;",
            "Ld8/d<",
            "*>;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    instance-of v0, p1, Lgc/m$h;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lgc/m$h;

    iget v1, v0, Lgc/m$h;->e:I

    const/high16 v2, -0x80000000

    and-int v3, v1, v2

    if-eqz v3, :cond_0

    sub-int/2addr v1, v2

    iput v1, v0, Lgc/m$h;->e:I

    goto :goto_0

    :cond_0
    new-instance v0, Lgc/m$h;

    invoke-direct {v0, p1}, Lgc/m$h;-><init>(Ld8/d;)V

    :goto_0
    iget-object p1, v0, Lgc/m$h;->d:Ljava/lang/Object;

    invoke-static {}, Le8/b;->c()Ljava/lang/Object;

    move-result-object v1

    iget v2, v0, Lgc/m$h;->e:I

    const/4 v3, 0x1

    if-eqz v2, :cond_2

    if-ne v2, v3, :cond_1

    iget-object p0, v0, Lgc/m$h;->f:Ljava/lang/Object;

    check-cast p0, Ljava/lang/Exception;

    invoke-static {p1}, La8/n;->b(Ljava/lang/Object;)V

    goto :goto_1

    :cond_1
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string p1, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {p0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_2
    invoke-static {p1}, La8/n;->b(Ljava/lang/Object;)V

    iput-object p0, v0, Lgc/m$h;->f:Ljava/lang/Object;

    iput v3, v0, Lgc/m$h;->e:I

    invoke-static {}, Lv8/t0;->a()Lv8/a0;

    move-result-object p1

    invoke-interface {v0}, Ld8/d;->c()Ld8/g;

    move-result-object v2

    new-instance v3, Lgc/m$g;

    invoke-direct {v3, v0, p0}, Lgc/m$g;-><init>(Ld8/d;Ljava/lang/Exception;)V

    invoke-virtual {p1, v2, v3}, Lv8/a0;->U(Ld8/g;Ljava/lang/Runnable;)V

    invoke-static {}, Le8/b;->c()Ljava/lang/Object;

    move-result-object p0

    invoke-static {}, Le8/b;->c()Ljava/lang/Object;

    move-result-object p1

    if-ne p0, p1, :cond_3

    invoke-static {v0}, Lf8/h;->c(Ld8/d;)V

    :cond_3
    if-ne p0, v1, :cond_4

    return-object v1

    :cond_4
    :goto_1
    sget-object p0, La8/v;->a:La8/v;

    return-object p0
.end method
