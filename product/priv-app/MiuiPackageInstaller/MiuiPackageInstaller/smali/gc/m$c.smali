.class public final Lgc/m$c;
.super Ljava/lang/Object;

# interfaces
.implements Lgc/d;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lgc/m;->a(Lgc/b;Ld8/d;)Ljava/lang/Object;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lgc/d<",
        "TT;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lv8/j;


# direct methods
.method constructor <init>(Lv8/j;)V
    .locals 0

    iput-object p1, p0, Lgc/m$c;->a:Lv8/j;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lgc/b;Lgc/t;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lgc/b<",
            "TT;>;",
            "Lgc/t<",
            "TT;>;)V"
        }
    .end annotation

    const-string v0, "call"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "response"

    invoke-static {p2, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Lgc/t;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p2}, Lgc/t;->a()Ljava/lang/Object;

    move-result-object p2

    if-nez p2, :cond_1

    invoke-interface {p1}, Lgc/b;->J()Lpb/z;

    move-result-object p1

    const-class p2, Lgc/l;

    invoke-virtual {p1, p2}, Lpb/z;->i(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    if-nez p1, :cond_0

    invoke-static {}, Lm8/i;->o()V

    :cond_0
    const-string p2, "call.request().tag(Invocation::class.java)!!"

    invoke-static {p1, p2}, Lm8/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lgc/l;

    invoke-virtual {p1}, Lgc/l;->a()Ljava/lang/reflect/Method;

    move-result-object p1

    new-instance p2, La8/e;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Response from "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "method"

    invoke-static {p1, v1}, Lm8/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/reflect/Method;->getDeclaringClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "method.declaringClass"

    invoke-static {v1, v2}, Lm8/i;->b(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x2e

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " was null but response body type was declared as non-null"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, La8/e;-><init>(Ljava/lang/String;)V

    iget-object p1, p0, Lgc/m$c;->a:Lv8/j;

    sget-object v0, La8/m;->a:La8/m$a;

    invoke-static {p2}, La8/n;->a(Ljava/lang/Throwable;)Ljava/lang/Object;

    move-result-object p2

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lgc/m$c;->a:Lv8/j;

    sget-object v0, La8/m;->a:La8/m$a;

    goto :goto_0

    :cond_2
    iget-object p1, p0, Lgc/m$c;->a:Lv8/j;

    new-instance v0, Lgc/j;

    invoke-direct {v0, p2}, Lgc/j;-><init>(Lgc/t;)V

    sget-object p2, La8/m;->a:La8/m$a;

    invoke-static {v0}, La8/n;->a(Ljava/lang/Throwable;)Ljava/lang/Object;

    move-result-object p2

    :goto_0
    invoke-static {p2}, La8/m;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    invoke-interface {p1, p2}, Ld8/d;->i(Ljava/lang/Object;)V

    return-void
.end method

.method public b(Lgc/b;Ljava/lang/Throwable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lgc/b<",
            "TT;>;",
            "Ljava/lang/Throwable;",
            ")V"
        }
    .end annotation

    const-string v0, "call"

    invoke-static {p1, v0}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "t"

    invoke-static {p2, p1}, Lm8/i;->g(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p0, Lgc/m$c;->a:Lv8/j;

    sget-object v0, La8/m;->a:La8/m$a;

    invoke-static {p2}, La8/n;->a(Ljava/lang/Throwable;)Ljava/lang/Object;

    move-result-object p2

    invoke-static {p2}, La8/m;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    invoke-interface {p1, p2}, Ld8/d;->i(Ljava/lang/Object;)V

    return-void
.end method
