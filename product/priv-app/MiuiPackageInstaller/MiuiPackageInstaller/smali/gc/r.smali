.class final Lgc/r;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgc/r$a;
    }
.end annotation


# static fields
.field private static final l:[C

.field private static final m:Ljava/util/regex/Pattern;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lpb/t;

.field private c:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private d:Lpb/t$a;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final e:Lpb/z$a;

.field private final f:Lpb/s$a;

.field private g:Lpb/v;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final h:Z

.field private i:Lpb/w$a;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lpb/q$a;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lpb/a0;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x10

    new-array v0, v0, [C

    fill-array-data v0, :array_0

    sput-object v0, Lgc/r;->l:[C

    const-string v0, "(.*/)?(\\.|%2e|%2E){1,2}(/.*)?"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lgc/r;->m:Ljava/util/regex/Pattern;

    return-void

    :array_0
    .array-data 2
        0x30s
        0x31s
        0x32s
        0x33s
        0x34s
        0x35s
        0x36s
        0x37s
        0x38s
        0x39s
        0x41s
        0x42s
        0x43s
        0x44s
        0x45s
        0x46s
    .end array-data
.end method

.method constructor <init>(Ljava/lang/String;Lpb/t;Ljava/lang/String;Lpb/s;Lpb/v;ZZZ)V
    .locals 0
    .param p3    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Lpb/s;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Lpb/v;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lgc/r;->a:Ljava/lang/String;

    iput-object p2, p0, Lgc/r;->b:Lpb/t;

    iput-object p3, p0, Lgc/r;->c:Ljava/lang/String;

    new-instance p1, Lpb/z$a;

    invoke-direct {p1}, Lpb/z$a;-><init>()V

    iput-object p1, p0, Lgc/r;->e:Lpb/z$a;

    iput-object p5, p0, Lgc/r;->g:Lpb/v;

    iput-boolean p6, p0, Lgc/r;->h:Z

    if-eqz p4, :cond_0

    invoke-virtual {p4}, Lpb/s;->d()Lpb/s$a;

    move-result-object p1

    goto :goto_0

    :cond_0
    new-instance p1, Lpb/s$a;

    invoke-direct {p1}, Lpb/s$a;-><init>()V

    :goto_0
    iput-object p1, p0, Lgc/r;->f:Lpb/s$a;

    if-eqz p7, :cond_1

    new-instance p1, Lpb/q$a;

    invoke-direct {p1}, Lpb/q$a;-><init>()V

    iput-object p1, p0, Lgc/r;->j:Lpb/q$a;

    goto :goto_1

    :cond_1
    if-eqz p8, :cond_2

    new-instance p1, Lpb/w$a;

    invoke-direct {p1}, Lpb/w$a;-><init>()V

    iput-object p1, p0, Lgc/r;->i:Lpb/w$a;

    sget-object p2, Lpb/w;->k:Lpb/v;

    invoke-virtual {p1, p2}, Lpb/w$a;->d(Lpb/v;)Lpb/w$a;

    :cond_2
    :goto_1
    return-void
.end method

.method private static i(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 6

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v0, :cond_2

    invoke-virtual {p0, v2}, Ljava/lang/String;->codePointAt(I)I

    move-result v3

    const/16 v4, 0x20

    if-lt v3, v4, :cond_1

    const/16 v4, 0x7f

    if-ge v3, v4, :cond_1

    const-string v4, " \"<>^`{}|\\?#"

    invoke-virtual {v4, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    const/4 v5, -0x1

    if-ne v4, v5, :cond_1

    if-nez p1, :cond_0

    const/16 v4, 0x2f

    if-eq v3, v4, :cond_1

    const/16 v4, 0x25

    if-ne v3, v4, :cond_0

    goto :goto_1

    :cond_0
    invoke-static {v3}, Ljava/lang/Character;->charCount(I)I

    move-result v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_1
    :goto_1
    new-instance v3, Ldc/e;

    invoke-direct {v3}, Ldc/e;-><init>()V

    invoke-virtual {v3, p0, v1, v2}, Ldc/e;->s0(Ljava/lang/String;II)Ldc/e;

    invoke-static {v3, p0, v2, v0, p1}, Lgc/r;->j(Ldc/e;Ljava/lang/String;IIZ)V

    invoke-virtual {v3}, Ldc/e;->Z()Ljava/lang/String;

    move-result-object p0

    :cond_2
    return-object p0
.end method

.method private static j(Ldc/e;Ljava/lang/String;IIZ)V
    .locals 6

    const/4 v0, 0x0

    :goto_0
    if-ge p2, p3, :cond_5

    invoke-virtual {p1, p2}, Ljava/lang/String;->codePointAt(I)I

    move-result v1

    if-eqz p4, :cond_0

    const/16 v2, 0x9

    if-eq v1, v2, :cond_4

    const/16 v2, 0xa

    if-eq v1, v2, :cond_4

    const/16 v2, 0xc

    if-eq v1, v2, :cond_4

    const/16 v2, 0xd

    if-ne v1, v2, :cond_0

    goto :goto_3

    :cond_0
    const/16 v2, 0x20

    const/16 v3, 0x25

    if-lt v1, v2, :cond_2

    const/16 v2, 0x7f

    if-ge v1, v2, :cond_2

    const-string v2, " \"<>^`{}|\\?#"

    invoke-virtual {v2, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    const/4 v4, -0x1

    if-ne v2, v4, :cond_2

    if-nez p4, :cond_1

    const/16 v2, 0x2f

    if-eq v1, v2, :cond_2

    if-ne v1, v3, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {p0, v1}, Ldc/e;->t0(I)Ldc/e;

    goto :goto_3

    :cond_2
    :goto_1
    if-nez v0, :cond_3

    new-instance v0, Ldc/e;

    invoke-direct {v0}, Ldc/e;-><init>()V

    :cond_3
    invoke-virtual {v0, v1}, Ldc/e;->t0(I)Ldc/e;

    :goto_2
    invoke-virtual {v0}, Ldc/e;->u()Z

    move-result v2

    if-nez v2, :cond_4

    invoke-virtual {v0}, Ldc/e;->readByte()B

    move-result v2

    and-int/lit16 v2, v2, 0xff

    invoke-virtual {p0, v3}, Ldc/e;->l0(I)Ldc/e;

    sget-object v4, Lgc/r;->l:[C

    shr-int/lit8 v5, v2, 0x4

    and-int/lit8 v5, v5, 0xf

    aget-char v5, v4, v5

    invoke-virtual {p0, v5}, Ldc/e;->l0(I)Ldc/e;

    and-int/lit8 v2, v2, 0xf

    aget-char v2, v4, v2

    invoke-virtual {p0, v2}, Ldc/e;->l0(I)Ldc/e;

    goto :goto_2

    :cond_4
    :goto_3
    invoke-static {v1}, Ljava/lang/Character;->charCount(I)I

    move-result v1

    add-int/2addr p2, v1

    goto :goto_0

    :cond_5
    return-void
.end method


# virtual methods
.method a(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    goto/32 :goto_2

    nop

    :goto_0
    return-void

    :goto_1
    iget-object p3, p0, Lgc/r;->j:Lpb/q$a;

    goto/32 :goto_3

    nop

    :goto_2
    if-nez p3, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_1

    nop

    :goto_3
    invoke-virtual {p3, p1, p2}, Lpb/q$a;->b(Ljava/lang/String;Ljava/lang/String;)Lpb/q$a;

    goto/32 :goto_5

    nop

    :goto_4
    iget-object p3, p0, Lgc/r;->j:Lpb/q$a;

    goto/32 :goto_7

    nop

    :goto_5
    goto :goto_8

    :goto_6
    goto/32 :goto_4

    nop

    :goto_7
    invoke-virtual {p3, p1, p2}, Lpb/q$a;->a(Ljava/lang/String;Ljava/lang/String;)Lpb/q$a;

    :goto_8
    goto/32 :goto_0

    nop
.end method

.method b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    goto/32 :goto_f

    nop

    :goto_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    goto/32 :goto_7

    nop

    :goto_1
    iget-object v0, p0, Lgc/r;->f:Lpb/s$a;

    goto/32 :goto_3

    nop

    :goto_2
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_0

    nop

    :goto_3
    invoke-virtual {v0, p1, p2}, Lpb/s$a;->a(Ljava/lang/String;Ljava/lang/String;)Lpb/s$a;

    :goto_4
    goto/32 :goto_d

    nop

    :goto_5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_8

    nop

    :goto_6
    new-instance v0, Ljava/lang/IllegalArgumentException;

    goto/32 :goto_9

    nop

    :goto_7
    invoke-direct {v0, p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/32 :goto_10

    nop

    :goto_8
    const-string v2, "Malformed content type: "

    goto/32 :goto_c

    nop

    :goto_9
    new-instance v1, Ljava/lang/StringBuilder;

    goto/32 :goto_5

    nop

    :goto_a
    if-nez v0, :cond_0

    goto/32 :goto_11

    :cond_0
    :try_start_0
    invoke-static {p2}, Lpb/v;->e(Ljava/lang/String;)Lpb/v;

    move-result-object p1

    iput-object p1, p0, Lgc/r;->g:Lpb/v;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_b

    nop

    :goto_b
    goto :goto_4

    :catch_0
    move-exception p1

    goto/32 :goto_6

    nop

    :goto_c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_2

    nop

    :goto_d
    return-void

    :goto_e
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    goto/32 :goto_a

    nop

    :goto_f
    const-string v0, "Content-Type"

    goto/32 :goto_e

    nop

    :goto_10
    throw v0

    :goto_11
    goto/32 :goto_1

    nop
.end method

.method c(Lpb/s;)V
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Lgc/r;->f:Lpb/s$a;

    goto/32 :goto_1

    nop

    :goto_1
    invoke-virtual {v0, p1}, Lpb/s$a;->b(Lpb/s;)Lpb/s$a;

    goto/32 :goto_2

    nop

    :goto_2
    return-void
.end method

.method d(Lpb/s;Lpb/a0;)V
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    invoke-virtual {v0, p1, p2}, Lpb/w$a;->a(Lpb/s;Lpb/a0;)Lpb/w$a;

    goto/32 :goto_2

    nop

    :goto_1
    iget-object v0, p0, Lgc/r;->i:Lpb/w$a;

    goto/32 :goto_0

    nop

    :goto_2
    return-void
.end method

.method e(Lpb/w$c;)V
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Lgc/r;->i:Lpb/w$a;

    goto/32 :goto_1

    nop

    :goto_1
    invoke-virtual {v0, p1}, Lpb/w$a;->b(Lpb/w$c;)Lpb/w$a;

    goto/32 :goto_2

    nop

    :goto_2
    return-void
.end method

.method f(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 3

    goto/32 :goto_11

    nop

    :goto_0
    invoke-virtual {p3}, Ljava/util/regex/Matcher;->matches()Z

    move-result p3

    goto/32 :goto_b

    nop

    :goto_1
    const-string p1, "}"

    goto/32 :goto_12

    nop

    :goto_2
    invoke-static {p2, p3}, Lgc/r;->i(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object p3

    goto/32 :goto_9

    nop

    :goto_3
    invoke-virtual {p3, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object p3

    goto/32 :goto_0

    nop

    :goto_4
    throw p1

    :goto_5
    goto/32 :goto_17

    nop

    :goto_6
    throw p1

    :goto_7
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_1f

    nop

    :goto_8
    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    goto/32 :goto_16

    nop

    :goto_9
    iget-object v0, p0, Lgc/r;->c:Ljava/lang/String;

    goto/32 :goto_20

    nop

    :goto_a
    new-instance p3, Ljava/lang/StringBuilder;

    goto/32 :goto_1e

    nop

    :goto_b
    if-eqz p3, :cond_0

    goto/32 :goto_1a

    :cond_0
    goto/32 :goto_d

    nop

    :goto_c
    const-string v0, "@Path parameters shouldn\'t perform path traversal (\'.\' or \'..\'): "

    goto/32 :goto_1d

    nop

    :goto_d
    iput-object p1, p0, Lgc/r;->c:Ljava/lang/String;

    goto/32 :goto_19

    nop

    :goto_e
    invoke-virtual {v0, p1, p3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_1c

    nop

    :goto_f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_e

    nop

    :goto_10
    new-instance p1, Ljava/lang/IllegalArgumentException;

    goto/32 :goto_a

    nop

    :goto_11
    iget-object v0, p0, Lgc/r;->c:Ljava/lang/String;

    goto/32 :goto_15

    nop

    :goto_12
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_f

    nop

    :goto_13
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_18

    nop

    :goto_14
    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    goto/32 :goto_6

    nop

    :goto_15
    if-nez v0, :cond_1

    goto/32 :goto_5

    :cond_1
    goto/32 :goto_2

    nop

    :goto_16
    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_4

    nop

    :goto_17
    new-instance p1, Ljava/lang/AssertionError;

    goto/32 :goto_14

    nop

    :goto_18
    const-string v2, "{"

    goto/32 :goto_7

    nop

    :goto_19
    return-void

    :goto_1a
    goto/32 :goto_10

    nop

    :goto_1b
    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_8

    nop

    :goto_1c
    sget-object p3, Lgc/r;->m:Ljava/util/regex/Pattern;

    goto/32 :goto_3

    nop

    :goto_1d
    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_1b

    nop

    :goto_1e
    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_c

    nop

    :goto_1f
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_1

    nop

    :goto_20
    new-instance v1, Ljava/lang/StringBuilder;

    goto/32 :goto_13

    nop
.end method

.method g(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 2
    .param p2    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    goto/32 :goto_2

    nop

    :goto_0
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    goto/32 :goto_14

    nop

    :goto_1
    const-string p3, "Malformed URL. Base: "

    goto/32 :goto_8

    nop

    :goto_2
    iget-object v0, p0, Lgc/r;->c:Ljava/lang/String;

    goto/32 :goto_3

    nop

    :goto_3
    if-nez v0, :cond_0

    goto/32 :goto_20

    :cond_0
    goto/32 :goto_10

    nop

    :goto_4
    iput-object v0, p0, Lgc/r;->d:Lpb/t$a;

    goto/32 :goto_1e

    nop

    :goto_5
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_9

    nop

    :goto_6
    const-string p3, ", Relative: "

    goto/32 :goto_5

    nop

    :goto_7
    invoke-virtual {v1, v0}, Lpb/t;->l(Ljava/lang/String;)Lpb/t$a;

    move-result-object v0

    goto/32 :goto_4

    nop

    :goto_8
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_e

    nop

    :goto_9
    iget-object p3, p0, Lgc/r;->c:Ljava/lang/String;

    goto/32 :goto_a

    nop

    :goto_a
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_0

    nop

    :goto_b
    const/4 v0, 0x0

    goto/32 :goto_21

    nop

    :goto_c
    goto :goto_18

    :goto_d
    goto/32 :goto_13

    nop

    :goto_e
    iget-object p3, p0, Lgc/r;->b:Lpb/t;

    goto/32 :goto_19

    nop

    :goto_f
    if-nez p3, :cond_1

    goto/32 :goto_d

    :cond_1
    goto/32 :goto_11

    nop

    :goto_10
    iget-object v1, p0, Lgc/r;->b:Lpb/t;

    goto/32 :goto_7

    nop

    :goto_11
    iget-object p3, p0, Lgc/r;->d:Lpb/t$a;

    goto/32 :goto_16

    nop

    :goto_12
    new-instance p2, Ljava/lang/StringBuilder;

    goto/32 :goto_1a

    nop

    :goto_13
    iget-object p3, p0, Lgc/r;->d:Lpb/t$a;

    goto/32 :goto_17

    nop

    :goto_14
    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_1f

    nop

    :goto_15
    return-void

    :goto_16
    invoke-virtual {p3, p1, p2}, Lpb/t$a;->a(Ljava/lang/String;Ljava/lang/String;)Lpb/t$a;

    goto/32 :goto_c

    nop

    :goto_17
    invoke-virtual {p3, p1, p2}, Lpb/t$a;->b(Ljava/lang/String;Ljava/lang/String;)Lpb/t$a;

    :goto_18
    goto/32 :goto_15

    nop

    :goto_19
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto/32 :goto_6

    nop

    :goto_1a
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_1

    nop

    :goto_1b
    new-instance p1, Ljava/lang/IllegalArgumentException;

    goto/32 :goto_12

    nop

    :goto_1c
    goto :goto_20

    :goto_1d
    goto/32 :goto_1b

    nop

    :goto_1e
    if-nez v0, :cond_2

    goto/32 :goto_1d

    :cond_2
    goto/32 :goto_b

    nop

    :goto_1f
    throw p1

    :goto_20
    goto/32 :goto_f

    nop

    :goto_21
    iput-object v0, p0, Lgc/r;->c:Ljava/lang/String;

    goto/32 :goto_1c

    nop
.end method

.method h(Ljava/lang/Class;Ljava/lang/Object;)V
    .locals 1
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;TT;)V"
        }
    .end annotation

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    iget-object v0, p0, Lgc/r;->e:Lpb/z$a;

    goto/32 :goto_2

    nop

    :goto_2
    invoke-virtual {v0, p1, p2}, Lpb/z$a;->i(Ljava/lang/Class;Ljava/lang/Object;)Lpb/z$a;

    goto/32 :goto_0

    nop
.end method

.method k()Lpb/z$a;
    .locals 5

    goto/32 :goto_1f

    nop

    :goto_0
    new-instance v3, Lgc/r$a;

    goto/32 :goto_8

    nop

    :goto_1
    if-nez v2, :cond_0

    goto/32 :goto_40

    :cond_0
    goto/32 :goto_3

    nop

    :goto_2
    invoke-virtual {v0, v2, v1}, Lpb/z$a;->f(Ljava/lang/String;Lpb/a0;)Lpb/z$a;

    move-result-object v0

    goto/32 :goto_14

    nop

    :goto_3
    if-nez v1, :cond_1

    goto/32 :goto_1a

    :cond_1
    goto/32 :goto_0

    nop

    :goto_4
    if-nez v2, :cond_2

    goto/32 :goto_17

    :cond_2
    goto/32 :goto_26

    nop

    :goto_5
    invoke-static {v1, v2}, Lpb/a0;->d(Lpb/v;[B)Lpb/a0;

    move-result-object v1

    :goto_6
    goto/32 :goto_3e

    nop

    :goto_7
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_21

    nop

    :goto_8
    invoke-direct {v3, v1, v2}, Lgc/r$a;-><init>(Lpb/a0;Lpb/v;)V

    goto/32 :goto_34

    nop

    :goto_9
    goto :goto_6

    :goto_a
    goto/32 :goto_33

    nop

    :goto_b
    const-string v4, "Content-Type"

    goto/32 :goto_3f

    nop

    :goto_c
    invoke-virtual {v0, v2}, Lpb/z$a;->e(Lpb/s;)Lpb/z$a;

    move-result-object v0

    goto/32 :goto_24

    nop

    :goto_d
    if-eqz v1, :cond_3

    goto/32 :goto_6

    :cond_3
    goto/32 :goto_1d

    nop

    :goto_e
    goto :goto_11

    :goto_f
    goto/32 :goto_2f

    nop

    :goto_10
    if-nez v0, :cond_4

    goto/32 :goto_15

    :cond_4
    :goto_11
    goto/32 :goto_3a

    nop

    :goto_12
    new-instance v1, Ljava/lang/StringBuilder;

    goto/32 :goto_35

    nop

    :goto_13
    const/4 v1, 0x0

    goto/32 :goto_32

    nop

    :goto_14
    return-object v0

    :goto_15
    goto/32 :goto_38

    nop

    :goto_16
    goto :goto_6

    :goto_17
    goto/32 :goto_28

    nop

    :goto_18
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto/32 :goto_3c

    nop

    :goto_19
    goto/16 :goto_40

    :goto_1a
    goto/32 :goto_25

    nop

    :goto_1b
    iget-object v2, p0, Lgc/r;->f:Lpb/s$a;

    goto/32 :goto_31

    nop

    :goto_1c
    throw v0

    :goto_1d
    iget-object v2, p0, Lgc/r;->j:Lpb/q$a;

    goto/32 :goto_2d

    nop

    :goto_1e
    invoke-virtual {v2}, Lpb/v;->toString()Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_b

    nop

    :goto_1f
    iget-object v0, p0, Lgc/r;->d:Lpb/t$a;

    goto/32 :goto_2a

    nop

    :goto_20
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_7

    nop

    :goto_21
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_1c

    nop

    :goto_22
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_2c

    nop

    :goto_23
    invoke-virtual {v2, v0}, Lpb/z$a;->k(Lpb/t;)Lpb/z$a;

    move-result-object v0

    goto/32 :goto_1b

    nop

    :goto_24
    iget-object v2, p0, Lgc/r;->a:Ljava/lang/String;

    goto/32 :goto_2

    nop

    :goto_25
    iget-object v3, p0, Lgc/r;->f:Lpb/s$a;

    goto/32 :goto_1e

    nop

    :goto_26
    invoke-virtual {v2}, Lpb/w$a;->c()Lpb/w;

    move-result-object v1

    goto/32 :goto_16

    nop

    :goto_27
    iget-object v1, p0, Lgc/r;->c:Ljava/lang/String;

    goto/32 :goto_39

    nop

    :goto_28
    iget-boolean v2, p0, Lgc/r;->h:Z

    goto/32 :goto_29

    nop

    :goto_29
    if-nez v2, :cond_5

    goto/32 :goto_6

    :cond_5
    goto/32 :goto_13

    nop

    :goto_2a
    if-nez v0, :cond_6

    goto/32 :goto_f

    :cond_6
    goto/32 :goto_37

    nop

    :goto_2b
    const-string v2, "Malformed URL. Base: "

    goto/32 :goto_30

    nop

    :goto_2c
    iget-object v2, p0, Lgc/r;->c:Ljava/lang/String;

    goto/32 :goto_20

    nop

    :goto_2d
    if-nez v2, :cond_7

    goto/32 :goto_a

    :cond_7
    goto/32 :goto_2e

    nop

    :goto_2e
    invoke-virtual {v2}, Lpb/q$a;->c()Lpb/q;

    move-result-object v1

    goto/32 :goto_9

    nop

    :goto_2f
    iget-object v0, p0, Lgc/r;->b:Lpb/t;

    goto/32 :goto_27

    nop

    :goto_30
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_36

    nop

    :goto_31
    invoke-virtual {v2}, Lpb/s$a;->e()Lpb/s;

    move-result-object v2

    goto/32 :goto_c

    nop

    :goto_32
    const/4 v2, 0x0

    goto/32 :goto_3b

    nop

    :goto_33
    iget-object v2, p0, Lgc/r;->i:Lpb/w$a;

    goto/32 :goto_4

    nop

    :goto_34
    move-object v1, v3

    goto/32 :goto_19

    nop

    :goto_35
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_2b

    nop

    :goto_36
    iget-object v2, p0, Lgc/r;->b:Lpb/t;

    goto/32 :goto_18

    nop

    :goto_37
    invoke-virtual {v0}, Lpb/t$a;->c()Lpb/t;

    move-result-object v0

    goto/32 :goto_e

    nop

    :goto_38
    new-instance v0, Ljava/lang/IllegalArgumentException;

    goto/32 :goto_12

    nop

    :goto_39
    invoke-virtual {v0, v1}, Lpb/t;->q(Ljava/lang/String;)Lpb/t;

    move-result-object v0

    goto/32 :goto_10

    nop

    :goto_3a
    iget-object v1, p0, Lgc/r;->k:Lpb/a0;

    goto/32 :goto_d

    nop

    :goto_3b
    new-array v2, v2, [B

    goto/32 :goto_5

    nop

    :goto_3c
    const-string v2, ", Relative: "

    goto/32 :goto_22

    nop

    :goto_3d
    iget-object v2, p0, Lgc/r;->e:Lpb/z$a;

    goto/32 :goto_23

    nop

    :goto_3e
    iget-object v2, p0, Lgc/r;->g:Lpb/v;

    goto/32 :goto_1

    nop

    :goto_3f
    invoke-virtual {v3, v4, v2}, Lpb/s$a;->a(Ljava/lang/String;Ljava/lang/String;)Lpb/s$a;

    :goto_40
    goto/32 :goto_3d

    nop
.end method

.method l(Lpb/a0;)V
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    iput-object p1, p0, Lgc/r;->k:Lpb/a0;

    goto/32 :goto_1

    nop

    :goto_1
    return-void
.end method

.method m(Ljava/lang/Object;)V
    .locals 0

    goto/32 :goto_2

    nop

    :goto_0
    iput-object p1, p0, Lgc/r;->c:Ljava/lang/String;

    goto/32 :goto_1

    nop

    :goto_1
    return-void

    :goto_2
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_0

    nop
.end method
