.class final Lgc/p$i;
.super Lgc/p;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgc/p;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "i"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lgc/p<",
        "TT;>;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/reflect/Method;

.field private final b:I

.field private final c:Lpb/s;

.field private final d:Lgc/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lgc/f<",
            "TT;",
            "Lpb/a0;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/reflect/Method;ILpb/s;Lgc/f;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Method;",
            "I",
            "Lpb/s;",
            "Lgc/f<",
            "TT;",
            "Lpb/a0;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Lgc/p;-><init>()V

    iput-object p1, p0, Lgc/p$i;->a:Ljava/lang/reflect/Method;

    iput p2, p0, Lgc/p$i;->b:I

    iput-object p3, p0, Lgc/p$i;->c:Lpb/s;

    iput-object p4, p0, Lgc/p$i;->d:Lgc/f;

    return-void
.end method


# virtual methods
.method a(Lgc/r;Ljava/lang/Object;)V
    .locals 4
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lgc/r;",
            "TT;)V"
        }
    .end annotation

    goto/32 :goto_0

    nop

    :goto_0
    if-eqz p2, :cond_0

    goto/32 :goto_f

    :cond_0
    goto/32 :goto_e

    nop

    :goto_1
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_3

    nop

    :goto_2
    invoke-static {v0, v1, p2, v2}, Lgc/y;->o(Ljava/lang/reflect/Method;ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object p1

    goto/32 :goto_a

    nop

    :goto_3
    const-string v3, "Unable to convert "

    goto/32 :goto_5

    nop

    :goto_4
    const-string p2, " to RequestBody"

    goto/32 :goto_13

    nop

    :goto_5
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_6

    nop

    :goto_6
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto/32 :goto_4

    nop

    :goto_7
    const/4 v2, 0x1

    goto/32 :goto_12

    nop

    :goto_8
    const/4 v3, 0x0

    goto/32 :goto_d

    nop

    :goto_9
    iget v1, p0, Lgc/p$i;->b:I

    goto/32 :goto_11

    nop

    :goto_a
    throw p1

    :goto_b
    return-void

    :catch_0
    move-exception p1

    goto/32 :goto_10

    nop

    :goto_c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    goto/32 :goto_7

    nop

    :goto_d
    aput-object p1, v2, v3

    goto/32 :goto_2

    nop

    :goto_e
    return-void

    :goto_f
    :try_start_0
    iget-object v0, p0, Lgc/p$i;->d:Lgc/f;

    invoke-interface {v0, p2}, Lgc/f;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpb/a0;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_14

    nop

    :goto_10
    iget-object v0, p0, Lgc/p$i;->a:Ljava/lang/reflect/Method;

    goto/32 :goto_9

    nop

    :goto_11
    new-instance v2, Ljava/lang/StringBuilder;

    goto/32 :goto_1

    nop

    :goto_12
    new-array v2, v2, [Ljava/lang/Object;

    goto/32 :goto_8

    nop

    :goto_13
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_c

    nop

    :goto_14
    iget-object p2, p0, Lgc/p$i;->c:Lpb/s;

    goto/32 :goto_15

    nop

    :goto_15
    invoke-virtual {p1, p2, v0}, Lgc/r;->d(Lpb/s;Lpb/a0;)V

    goto/32 :goto_b

    nop
.end method
