.class final Lgc/p$c;
.super Lgc/p;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgc/p;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lgc/p<",
        "TT;>;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/reflect/Method;

.field private final b:I

.field private final c:Lgc/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lgc/f<",
            "TT;",
            "Lpb/a0;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/reflect/Method;ILgc/f;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Method;",
            "I",
            "Lgc/f<",
            "TT;",
            "Lpb/a0;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Lgc/p;-><init>()V

    iput-object p1, p0, Lgc/p$c;->a:Ljava/lang/reflect/Method;

    iput p2, p0, Lgc/p$c;->b:I

    iput-object p3, p0, Lgc/p$c;->c:Lgc/f;

    return-void
.end method


# virtual methods
.method a(Lgc/r;Ljava/lang/Object;)V
    .locals 5
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lgc/r;",
            "TT;)V"
        }
    .end annotation

    goto/32 :goto_11

    nop

    :goto_0
    const-string p2, " to RequestBody"

    goto/32 :goto_a

    nop

    :goto_1
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_2

    nop

    :goto_2
    const-string v4, "Unable to convert "

    goto/32 :goto_16

    nop

    :goto_3
    invoke-static {p1, p2, v1, v0}, Lgc/y;->o(Ljava/lang/reflect/Method;ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object p1

    goto/32 :goto_5

    nop

    :goto_4
    iget v2, p0, Lgc/p$c;->b:I

    goto/32 :goto_17

    nop

    :goto_5
    throw p1

    :goto_6
    return-void

    :catch_0
    move-exception p1

    goto/32 :goto_b

    nop

    :goto_7
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto/32 :goto_0

    nop

    :goto_8
    new-array v0, v0, [Ljava/lang/Object;

    goto/32 :goto_14

    nop

    :goto_9
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    goto/32 :goto_e

    nop

    :goto_a
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_9

    nop

    :goto_b
    iget-object v1, p0, Lgc/p$c;->a:Ljava/lang/reflect/Method;

    goto/32 :goto_4

    nop

    :goto_c
    iget p2, p0, Lgc/p$c;->b:I

    goto/32 :goto_8

    nop

    :goto_d
    invoke-static {v1, p1, v2, p2, v0}, Lgc/y;->p(Ljava/lang/reflect/Method;Ljava/lang/Throwable;ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object p1

    goto/32 :goto_f

    nop

    :goto_e
    new-array v0, v0, [Ljava/lang/Object;

    goto/32 :goto_d

    nop

    :goto_f
    throw p1

    :goto_10
    goto/32 :goto_15

    nop

    :goto_11
    const/4 v0, 0x0

    goto/32 :goto_12

    nop

    :goto_12
    if-nez p2, :cond_0

    goto/32 :goto_10

    :cond_0
    :try_start_0
    iget-object v1, p0, Lgc/p$c;->c:Lgc/f;

    invoke-interface {v1, p2}, Lgc/f;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lpb/a0;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_13

    nop

    :goto_13
    invoke-virtual {p1, v1}, Lgc/r;->l(Lpb/a0;)V

    goto/32 :goto_6

    nop

    :goto_14
    const-string v1, "Body parameter value must not be null."

    goto/32 :goto_3

    nop

    :goto_15
    iget-object p1, p0, Lgc/p$c;->a:Ljava/lang/reflect/Method;

    goto/32 :goto_c

    nop

    :goto_16
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_7

    nop

    :goto_17
    new-instance v3, Ljava/lang/StringBuilder;

    goto/32 :goto_1

    nop
.end method
