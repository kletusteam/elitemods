.class final Lgc/s;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgc/s$a;
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/reflect/Method;

.field private final b:Lpb/t;

.field final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final e:Lpb/s;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final f:Lpb/v;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final g:Z

.field private final h:Z

.field private final i:Z

.field private final j:[Lgc/p;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lgc/p<",
            "*>;"
        }
    .end annotation
.end field

.field final k:Z


# direct methods
.method constructor <init>(Lgc/s$a;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget-object v0, p1, Lgc/s$a;->b:Ljava/lang/reflect/Method;

    iput-object v0, p0, Lgc/s;->a:Ljava/lang/reflect/Method;

    iget-object v0, p1, Lgc/s$a;->a:Lgc/u;

    iget-object v0, v0, Lgc/u;->c:Lpb/t;

    iput-object v0, p0, Lgc/s;->b:Lpb/t;

    iget-object v0, p1, Lgc/s$a;->n:Ljava/lang/String;

    iput-object v0, p0, Lgc/s;->c:Ljava/lang/String;

    iget-object v0, p1, Lgc/s$a;->r:Ljava/lang/String;

    iput-object v0, p0, Lgc/s;->d:Ljava/lang/String;

    iget-object v0, p1, Lgc/s$a;->s:Lpb/s;

    iput-object v0, p0, Lgc/s;->e:Lpb/s;

    iget-object v0, p1, Lgc/s$a;->t:Lpb/v;

    iput-object v0, p0, Lgc/s;->f:Lpb/v;

    iget-boolean v0, p1, Lgc/s$a;->o:Z

    iput-boolean v0, p0, Lgc/s;->g:Z

    iget-boolean v0, p1, Lgc/s$a;->p:Z

    iput-boolean v0, p0, Lgc/s;->h:Z

    iget-boolean v0, p1, Lgc/s$a;->q:Z

    iput-boolean v0, p0, Lgc/s;->i:Z

    iget-object v0, p1, Lgc/s$a;->v:[Lgc/p;

    iput-object v0, p0, Lgc/s;->j:[Lgc/p;

    iget-boolean p1, p1, Lgc/s$a;->w:Z

    iput-boolean p1, p0, Lgc/s;->k:Z

    return-void
.end method

.method static b(Lgc/u;Ljava/lang/reflect/Method;)Lgc/s;
    .locals 1

    new-instance v0, Lgc/s$a;

    invoke-direct {v0, p0, p1}, Lgc/s$a;-><init>(Lgc/u;Ljava/lang/reflect/Method;)V

    invoke-virtual {v0}, Lgc/s$a;->b()Lgc/s;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method a([Ljava/lang/Object;)Lpb/z;
    .locals 12

    goto/32 :goto_22

    nop

    :goto_0
    invoke-virtual {p1}, Lpb/z$a;->b()Lpb/z;

    move-result-object p1

    goto/32 :goto_1b

    nop

    :goto_1
    const-string v0, ")"

    goto/32 :goto_10

    nop

    :goto_2
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_1a

    nop

    :goto_3
    iget-boolean v3, p0, Lgc/s;->k:Z

    goto/32 :goto_30

    nop

    :goto_4
    throw p1

    :goto_5
    const-class v0, Lgc/l;

    goto/32 :goto_1d

    nop

    :goto_6
    new-instance p1, Ljava/lang/IllegalArgumentException;

    goto/32 :goto_23

    nop

    :goto_7
    move-object v3, v2

    goto/32 :goto_13

    nop

    :goto_8
    add-int/lit8 v1, v1, -0x1

    :goto_9
    goto/32 :goto_1f

    nop

    :goto_a
    invoke-direct {v3, v1}, Ljava/util/ArrayList;-><init>(I)V

    goto/32 :goto_16

    nop

    :goto_b
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_c

    nop

    :goto_c
    array-length v0, v0

    goto/32 :goto_12

    nop

    :goto_d
    if-eq v1, v2, :cond_0

    goto/32 :goto_1c

    :cond_0
    goto/32 :goto_18

    nop

    :goto_e
    goto :goto_17

    :goto_f
    goto/32 :goto_31

    nop

    :goto_10
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_20

    nop

    :goto_11
    iget-boolean v11, p0, Lgc/s;->i:Z

    goto/32 :goto_7

    nop

    :goto_12
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/32 :goto_1

    nop

    :goto_13
    invoke-direct/range {v3 .. v11}, Lgc/r;-><init>(Ljava/lang/String;Lpb/t;Ljava/lang/String;Lpb/s;Lpb/v;ZZZ)V

    goto/32 :goto_3

    nop

    :goto_14
    aget-object v6, p1, v4

    goto/32 :goto_2a

    nop

    :goto_15
    array-length v2, v0

    goto/32 :goto_d

    nop

    :goto_16
    const/4 v4, 0x0

    :goto_17
    goto/32 :goto_25

    nop

    :goto_18
    new-instance v2, Lgc/r;

    goto/32 :goto_34

    nop

    :goto_19
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/32 :goto_1e

    nop

    :goto_1a
    const-string v3, "Argument count ("

    goto/32 :goto_36

    nop

    :goto_1b
    return-object p1

    :goto_1c
    goto/32 :goto_6

    nop

    :goto_1d
    new-instance v1, Lgc/l;

    goto/32 :goto_2f

    nop

    :goto_1e
    const-string v1, ") doesn\'t match expected count ("

    goto/32 :goto_b

    nop

    :goto_1f
    new-instance v3, Ljava/util/ArrayList;

    goto/32 :goto_a

    nop

    :goto_20
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_2e

    nop

    :goto_21
    iget-boolean v9, p0, Lgc/s;->g:Z

    goto/32 :goto_27

    nop

    :goto_22
    iget-object v0, p0, Lgc/s;->j:[Lgc/p;

    goto/32 :goto_26

    nop

    :goto_23
    new-instance v2, Ljava/lang/StringBuilder;

    goto/32 :goto_2

    nop

    :goto_24
    add-int/lit8 v4, v4, 0x1

    goto/32 :goto_e

    nop

    :goto_25
    if-lt v4, v1, :cond_1

    goto/32 :goto_f

    :cond_1
    goto/32 :goto_35

    nop

    :goto_26
    array-length v1, p1

    goto/32 :goto_15

    nop

    :goto_27
    iget-boolean v10, p0, Lgc/s;->h:Z

    goto/32 :goto_11

    nop

    :goto_28
    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/32 :goto_2c

    nop

    :goto_29
    invoke-direct {v1, v2, v3}, Lgc/l;-><init>(Ljava/lang/reflect/Method;Ljava/util/List;)V

    goto/32 :goto_37

    nop

    :goto_2a
    invoke-virtual {v5, v2, v6}, Lgc/p;->a(Lgc/r;Ljava/lang/Object;)V

    goto/32 :goto_24

    nop

    :goto_2b
    iget-object v8, p0, Lgc/s;->f:Lpb/v;

    goto/32 :goto_21

    nop

    :goto_2c
    aget-object v5, v0, v4

    goto/32 :goto_14

    nop

    :goto_2d
    iget-object v5, p0, Lgc/s;->b:Lpb/t;

    goto/32 :goto_33

    nop

    :goto_2e
    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_4

    nop

    :goto_2f
    iget-object v2, p0, Lgc/s;->a:Ljava/lang/reflect/Method;

    goto/32 :goto_29

    nop

    :goto_30
    if-nez v3, :cond_2

    goto/32 :goto_9

    :cond_2
    goto/32 :goto_8

    nop

    :goto_31
    invoke-virtual {v2}, Lgc/r;->k()Lpb/z$a;

    move-result-object p1

    goto/32 :goto_5

    nop

    :goto_32
    iget-object v7, p0, Lgc/s;->e:Lpb/s;

    goto/32 :goto_2b

    nop

    :goto_33
    iget-object v6, p0, Lgc/s;->d:Ljava/lang/String;

    goto/32 :goto_32

    nop

    :goto_34
    iget-object v4, p0, Lgc/s;->c:Ljava/lang/String;

    goto/32 :goto_2d

    nop

    :goto_35
    aget-object v5, p1, v4

    goto/32 :goto_28

    nop

    :goto_36
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_19

    nop

    :goto_37
    invoke-virtual {p1, v0, v1}, Lpb/z$a;->i(Ljava/lang/Class;Ljava/lang/Object;)Lpb/z$a;

    move-result-object p1

    goto/32 :goto_0

    nop
.end method
