.class final Lgc/p$d;
.super Lgc/p;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgc/p;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "d"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lgc/p<",
        "TT;>;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lgc/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lgc/f<",
            "TT;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Z


# direct methods
.method constructor <init>(Ljava/lang/String;Lgc/f;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lgc/f<",
            "TT;",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    invoke-direct {p0}, Lgc/p;-><init>()V

    const-string v0, "name == null"

    invoke-static {p1, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    iput-object p1, p0, Lgc/p$d;->a:Ljava/lang/String;

    iput-object p2, p0, Lgc/p$d;->b:Lgc/f;

    iput-boolean p3, p0, Lgc/p$d;->c:Z

    return-void
.end method


# virtual methods
.method a(Lgc/r;Ljava/lang/Object;)V
    .locals 2
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lgc/r;",
            "TT;)V"
        }
    .end annotation

    goto/32 :goto_5

    nop

    :goto_0
    invoke-interface {v0, p2}, Lgc/f;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    goto/32 :goto_6

    nop

    :goto_1
    iget-object v0, p0, Lgc/p$d;->b:Lgc/f;

    goto/32 :goto_0

    nop

    :goto_2
    return-void

    :goto_3
    return-void

    :goto_4
    goto/32 :goto_1

    nop

    :goto_5
    if-eqz p2, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_3

    nop

    :goto_6
    check-cast p2, Ljava/lang/String;

    goto/32 :goto_8

    nop

    :goto_7
    iget-object v0, p0, Lgc/p$d;->a:Ljava/lang/String;

    goto/32 :goto_b

    nop

    :goto_8
    if-eqz p2, :cond_1

    goto/32 :goto_a

    :cond_1
    goto/32 :goto_9

    nop

    :goto_9
    return-void

    :goto_a
    goto/32 :goto_7

    nop

    :goto_b
    iget-boolean v1, p0, Lgc/p$d;->c:Z

    goto/32 :goto_c

    nop

    :goto_c
    invoke-virtual {p1, v0, p2, v1}, Lgc/r;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    goto/32 :goto_2

    nop
.end method
