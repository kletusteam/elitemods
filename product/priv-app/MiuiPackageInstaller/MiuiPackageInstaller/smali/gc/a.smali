.class final Lgc/a;
.super Lgc/f$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgc/a$c;,
        Lgc/a$a;,
        Lgc/a$f;,
        Lgc/a$e;,
        Lgc/a$b;,
        Lgc/a$d;
    }
.end annotation


# instance fields
.field private a:Z


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lgc/f$a;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lgc/a;->a:Z

    return-void
.end method


# virtual methods
.method public c(Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;[Ljava/lang/annotation/Annotation;Lgc/u;)Lgc/f;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Type;",
            "[",
            "Ljava/lang/annotation/Annotation;",
            "[",
            "Ljava/lang/annotation/Annotation;",
            "Lgc/u;",
            ")",
            "Lgc/f<",
            "*",
            "Lpb/a0;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    const-class p2, Lpb/a0;

    invoke-static {p1}, Lgc/y;->h(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result p1

    if-eqz p1, :cond_0

    sget-object p1, Lgc/a$b;->a:Lgc/a$b;

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public d(Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;Lgc/u;)Lgc/f;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Type;",
            "[",
            "Ljava/lang/annotation/Annotation;",
            "Lgc/u;",
            ")",
            "Lgc/f<",
            "Lpb/c0;",
            "*>;"
        }
    .end annotation

    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    const-class p3, Lpb/c0;

    if-ne p1, p3, :cond_1

    const-class p1, Lic/w;

    invoke-static {p2, p1}, Lgc/y;->l([Ljava/lang/annotation/Annotation;Ljava/lang/Class;)Z

    move-result p1

    if-eqz p1, :cond_0

    sget-object p1, Lgc/a$c;->a:Lgc/a$c;

    goto :goto_0

    :cond_0
    sget-object p1, Lgc/a$a;->a:Lgc/a$a;

    :goto_0
    return-object p1

    :cond_1
    const-class p2, Ljava/lang/Void;

    if-ne p1, p2, :cond_2

    sget-object p1, Lgc/a$f;->a:Lgc/a$f;

    return-object p1

    :cond_2
    iget-boolean p2, p0, Lgc/a;->a:Z

    if-eqz p2, :cond_3

    :try_start_0
    const-class p2, La8/v;

    if-ne p1, p2, :cond_3

    sget-object p1, Lgc/a$e;->a:Lgc/a$e;
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    const/4 p1, 0x0

    iput-boolean p1, p0, Lgc/a;->a:Z

    :cond_3
    const/4 p1, 0x0

    return-object p1
.end method
