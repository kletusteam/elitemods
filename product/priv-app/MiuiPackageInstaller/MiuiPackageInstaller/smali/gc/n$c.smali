.class final Lgc/n$c;
.super Lpb/c0;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgc/n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "c"
.end annotation


# instance fields
.field private final c:Lpb/v;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final d:J


# direct methods
.method constructor <init>(Lpb/v;J)V
    .locals 0
    .param p1    # Lpb/v;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0}, Lpb/c0;-><init>()V

    iput-object p1, p0, Lgc/n$c;->c:Lpb/v;

    iput-wide p2, p0, Lgc/n$c;->d:J

    return-void
.end method


# virtual methods
.method public m()J
    .locals 2

    iget-wide v0, p0, Lgc/n$c;->d:J

    return-wide v0
.end method

.method public n()Lpb/v;
    .locals 1

    iget-object v0, p0, Lgc/n$c;->c:Lpb/v;

    return-object v0
.end method

.method public r()Ldc/g;
    .locals 2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot read raw response body of a converted body."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
