.class final Lgc/p$j;
.super Lgc/p;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgc/p;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "j"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lgc/p<",
        "Ljava/util/Map<",
        "Ljava/lang/String;",
        "TT;>;>;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/reflect/Method;

.field private final b:I

.field private final c:Lgc/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lgc/f<",
            "TT;",
            "Lpb/a0;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/reflect/Method;ILgc/f;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Method;",
            "I",
            "Lgc/f<",
            "TT;",
            "Lpb/a0;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Lgc/p;-><init>()V

    iput-object p1, p0, Lgc/p$j;->a:Ljava/lang/reflect/Method;

    iput p2, p0, Lgc/p$j;->b:I

    iput-object p3, p0, Lgc/p$j;->c:Lgc/f;

    iput-object p4, p0, Lgc/p$j;->d:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method bridge synthetic a(Lgc/r;Ljava/lang/Object;)V
    .locals 0
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    goto/32 :goto_1

    nop

    :goto_0
    invoke-virtual {p0, p1, p2}, Lgc/p$j;->d(Lgc/r;Ljava/util/Map;)V

    goto/32 :goto_2

    nop

    :goto_1
    check-cast p2, Ljava/util/Map;

    goto/32 :goto_0

    nop

    :goto_2
    return-void
.end method

.method d(Lgc/r;Ljava/util/Map;)V
    .locals 7
    .param p2    # Ljava/util/Map;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lgc/r;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "TT;>;)V"
        }
    .end annotation

    goto/32 :goto_5

    nop

    :goto_0
    check-cast v2, Ljava/lang/String;

    goto/32 :goto_2a

    nop

    :goto_1
    const-string v2, "\""

    goto/32 :goto_17

    nop

    :goto_2
    const-string v6, "form-data; name=\""

    goto/32 :goto_39

    nop

    :goto_3
    aput-object v4, v3, v2

    goto/32 :goto_2c

    nop

    :goto_4
    const-string v4, "Content-Disposition"

    goto/32 :goto_31

    nop

    :goto_5
    const/4 v0, 0x0

    goto/32 :goto_42

    nop

    :goto_6
    const-string v1, "Part map contained null key."

    goto/32 :goto_34

    nop

    :goto_7
    const/4 v3, 0x4

    goto/32 :goto_11

    nop

    :goto_8
    iget p2, p0, Lgc/p$j;->b:I

    goto/32 :goto_3f

    nop

    :goto_9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_1c

    nop

    :goto_a
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_2f

    nop

    :goto_b
    return-void

    :goto_c
    goto/32 :goto_45

    nop

    :goto_d
    const/4 v2, 0x2

    goto/32 :goto_3d

    nop

    :goto_e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_19

    nop

    :goto_f
    iget-object p1, p0, Lgc/p$j;->a:Ljava/lang/reflect/Method;

    goto/32 :goto_33

    nop

    :goto_10
    aput-object v4, v3, v2

    goto/32 :goto_22

    nop

    :goto_11
    new-array v3, v3, [Ljava/lang/String;

    goto/32 :goto_4

    nop

    :goto_12
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/32 :goto_41

    nop

    :goto_13
    new-array v0, v0, [Ljava/lang/Object;

    goto/32 :goto_27

    nop

    :goto_14
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p2

    goto/32 :goto_35

    nop

    :goto_15
    iget-object v4, p0, Lgc/p$j;->d:Ljava/lang/String;

    goto/32 :goto_10

    nop

    :goto_16
    new-instance v5, Ljava/lang/StringBuilder;

    goto/32 :goto_21

    nop

    :goto_17
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_12

    nop

    :goto_18
    invoke-static {p1, p2, v1, v0}, Lgc/y;->o(Ljava/lang/reflect/Method;ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object p1

    goto/32 :goto_3c

    nop

    :goto_19
    const-string v3, "Part map contained null value for key \'"

    goto/32 :goto_3e

    nop

    :goto_1a
    goto/16 :goto_36

    :goto_1b
    goto/32 :goto_2e

    nop

    :goto_1c
    const-string v2, "\'."

    goto/32 :goto_2b

    nop

    :goto_1d
    if-nez v1, :cond_0

    goto/32 :goto_38

    :cond_0
    goto/32 :goto_20

    nop

    :goto_1e
    new-array v0, v0, [Ljava/lang/Object;

    goto/32 :goto_43

    nop

    :goto_1f
    new-array v0, v0, [Ljava/lang/Object;

    goto/32 :goto_6

    nop

    :goto_20
    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_2d

    nop

    :goto_21
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_2

    nop

    :goto_22
    invoke-static {v3}, Lpb/s;->e([Ljava/lang/String;)Lpb/s;

    move-result-object v2

    goto/32 :goto_32

    nop

    :goto_23
    invoke-virtual {p1, v2, v1}, Lgc/r;->d(Lpb/s;Lpb/a0;)V

    goto/32 :goto_1a

    nop

    :goto_24
    throw p1

    :goto_25
    goto/32 :goto_f

    nop

    :goto_26
    invoke-interface {v3, v1}, Lgc/f;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_30

    nop

    :goto_27
    invoke-static {p1, p2, v1, v0}, Lgc/y;->o(Ljava/lang/reflect/Method;ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object p1

    goto/32 :goto_24

    nop

    :goto_28
    const/4 v4, 0x1

    goto/32 :goto_16

    nop

    :goto_29
    iget p2, p0, Lgc/p$j;->b:I

    goto/32 :goto_1e

    nop

    :goto_2a
    if-nez v2, :cond_1

    goto/32 :goto_25

    :cond_1
    goto/32 :goto_a

    nop

    :goto_2b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_44

    nop

    :goto_2c
    const/4 v2, 0x3

    goto/32 :goto_15

    nop

    :goto_2d
    check-cast v1, Ljava/util/Map$Entry;

    goto/32 :goto_3b

    nop

    :goto_2e
    iget-object p1, p0, Lgc/p$j;->a:Ljava/lang/reflect/Method;

    goto/32 :goto_8

    nop

    :goto_2f
    if-nez v1, :cond_2

    goto/32 :goto_1b

    :cond_2
    goto/32 :goto_7

    nop

    :goto_30
    check-cast v1, Lpb/a0;

    goto/32 :goto_23

    nop

    :goto_31
    aput-object v4, v3, v0

    goto/32 :goto_28

    nop

    :goto_32
    iget-object v3, p0, Lgc/p$j;->c:Lgc/f;

    goto/32 :goto_26

    nop

    :goto_33
    iget p2, p0, Lgc/p$j;->b:I

    goto/32 :goto_1f

    nop

    :goto_34
    invoke-static {p1, p2, v1, v0}, Lgc/y;->o(Ljava/lang/reflect/Method;ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object p1

    goto/32 :goto_37

    nop

    :goto_35
    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_36
    goto/32 :goto_40

    nop

    :goto_37
    throw p1

    :goto_38
    goto/32 :goto_b

    nop

    :goto_39
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_3a

    nop

    :goto_3a
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_1

    nop

    :goto_3b
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_0

    nop

    :goto_3c
    throw p1

    :goto_3d
    const-string v4, "Content-Transfer-Encoding"

    goto/32 :goto_3

    nop

    :goto_3e
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_9

    nop

    :goto_3f
    new-instance v1, Ljava/lang/StringBuilder;

    goto/32 :goto_e

    nop

    :goto_40
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_1d

    nop

    :goto_41
    aput-object v2, v3, v4

    goto/32 :goto_d

    nop

    :goto_42
    if-nez p2, :cond_3

    goto/32 :goto_c

    :cond_3
    goto/32 :goto_14

    nop

    :goto_43
    const-string v1, "Part map was null."

    goto/32 :goto_18

    nop

    :goto_44
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_13

    nop

    :goto_45
    iget-object p1, p0, Lgc/p$j;->a:Ljava/lang/reflect/Method;

    goto/32 :goto_29

    nop
.end method
