.class final Lgc/n$b;
.super Lpb/c0;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgc/n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "b"
.end annotation


# instance fields
.field private final c:Lpb/c0;

.field private final d:Ldc/g;

.field e:Ljava/io/IOException;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method constructor <init>(Lpb/c0;)V
    .locals 1

    invoke-direct {p0}, Lpb/c0;-><init>()V

    iput-object p1, p0, Lgc/n$b;->c:Lpb/c0;

    new-instance v0, Lgc/n$b$a;

    invoke-virtual {p1}, Lpb/c0;->r()Ldc/g;

    move-result-object p1

    invoke-direct {v0, p0, p1}, Lgc/n$b$a;-><init>(Lgc/n$b;Ldc/y;)V

    invoke-static {v0}, Ldc/o;->b(Ldc/y;)Ldc/g;

    move-result-object p1

    iput-object p1, p0, Lgc/n$b;->d:Ldc/g;

    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    iget-object v0, p0, Lgc/n$b;->c:Lpb/c0;

    invoke-virtual {v0}, Lpb/c0;->close()V

    return-void
.end method

.method public m()J
    .locals 2

    iget-object v0, p0, Lgc/n$b;->c:Lpb/c0;

    invoke-virtual {v0}, Lpb/c0;->m()J

    move-result-wide v0

    return-wide v0
.end method

.method public n()Lpb/v;
    .locals 1

    iget-object v0, p0, Lgc/n$b;->c:Lpb/c0;

    invoke-virtual {v0}, Lpb/c0;->n()Lpb/v;

    move-result-object v0

    return-object v0
.end method

.method public r()Ldc/g;
    .locals 1

    iget-object v0, p0, Lgc/n$b;->d:Ldc/g;

    return-object v0
.end method

.method y()V
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    if-eqz v0, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_2

    nop

    :goto_1
    iget-object v0, p0, Lgc/n$b;->e:Ljava/io/IOException;

    goto/32 :goto_0

    nop

    :goto_2
    return-void

    :goto_3
    goto/32 :goto_4

    nop

    :goto_4
    throw v0
.end method
