.class final Lgc/p$m;
.super Lgc/p;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgc/p;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "m"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lgc/p<",
        "Ljava/util/Map<",
        "Ljava/lang/String;",
        "TT;>;>;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/reflect/Method;

.field private final b:I

.field private final c:Lgc/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lgc/f<",
            "TT;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Z


# direct methods
.method constructor <init>(Ljava/lang/reflect/Method;ILgc/f;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Method;",
            "I",
            "Lgc/f<",
            "TT;",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    invoke-direct {p0}, Lgc/p;-><init>()V

    iput-object p1, p0, Lgc/p$m;->a:Ljava/lang/reflect/Method;

    iput p2, p0, Lgc/p$m;->b:I

    iput-object p3, p0, Lgc/p$m;->c:Lgc/f;

    iput-boolean p4, p0, Lgc/p$m;->d:Z

    return-void
.end method


# virtual methods
.method bridge synthetic a(Lgc/r;Ljava/lang/Object;)V
    .locals 0
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    goto/32 :goto_0

    nop

    :goto_0
    check-cast p2, Ljava/util/Map;

    goto/32 :goto_1

    nop

    :goto_1
    invoke-virtual {p0, p1, p2}, Lgc/p$m;->d(Lgc/r;Ljava/util/Map;)V

    goto/32 :goto_2

    nop

    :goto_2
    return-void
.end method

.method d(Lgc/r;Ljava/util/Map;)V
    .locals 6
    .param p2    # Ljava/util/Map;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lgc/r;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "TT;>;)V"
        }
    .end annotation

    goto/32 :goto_3

    nop

    :goto_0
    iget p2, p0, Lgc/p$m;->b:I

    goto/32 :goto_18

    nop

    :goto_1
    check-cast v2, Ljava/lang/String;

    goto/32 :goto_30

    nop

    :goto_2
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_8

    nop

    :goto_3
    const/4 v0, 0x0

    goto/32 :goto_b

    nop

    :goto_4
    return-void

    :goto_5
    goto/32 :goto_10

    nop

    :goto_6
    throw p1

    :goto_7
    new-array v0, v0, [Ljava/lang/Object;

    goto/32 :goto_c

    nop

    :goto_8
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_16

    nop

    :goto_9
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_1

    nop

    :goto_a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_3c

    nop

    :goto_b
    if-nez p2, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_48

    nop

    :goto_c
    invoke-static {p1, p2, v1, v0}, Lgc/y;->o(Ljava/lang/reflect/Method;ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object p1

    goto/32 :goto_33

    nop

    :goto_d
    iget-object v4, p0, Lgc/p$m;->c:Lgc/f;

    goto/32 :goto_3d

    nop

    :goto_e
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    goto/32 :goto_20

    nop

    :goto_f
    iget-object v1, p0, Lgc/p$m;->c:Lgc/f;

    goto/32 :goto_e

    nop

    :goto_10
    iget-object p1, p0, Lgc/p$m;->a:Ljava/lang/reflect/Method;

    goto/32 :goto_45

    nop

    :goto_11
    new-array v0, v0, [Ljava/lang/Object;

    goto/32 :goto_12

    nop

    :goto_12
    const-string v1, "Query map contained null key."

    goto/32 :goto_47

    nop

    :goto_13
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_f

    nop

    :goto_14
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_2a

    nop

    :goto_15
    iget p2, p0, Lgc/p$m;->b:I

    goto/32 :goto_11

    nop

    :goto_16
    new-array v0, v0, [Ljava/lang/Object;

    goto/32 :goto_3e

    nop

    :goto_17
    const-string v1, "\' converted to null by "

    goto/32 :goto_13

    nop

    :goto_18
    new-instance v1, Ljava/lang/StringBuilder;

    goto/32 :goto_14

    nop

    :goto_19
    iget-object p1, p0, Lgc/p$m;->a:Ljava/lang/reflect/Method;

    goto/32 :goto_1d

    nop

    :goto_1a
    if-nez v1, :cond_1

    goto/32 :goto_28

    :cond_1
    goto/32 :goto_d

    nop

    :goto_1b
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_1c

    nop

    :goto_1c
    const-string v1, " for key \'"

    goto/32 :goto_36

    nop

    :goto_1d
    iget p2, p0, Lgc/p$m;->b:I

    goto/32 :goto_29

    nop

    :goto_1e
    iget-object p1, p0, Lgc/p$m;->a:Ljava/lang/reflect/Method;

    goto/32 :goto_15

    nop

    :goto_1f
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_a

    nop

    :goto_20
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_1b

    nop

    :goto_21
    const-string v1, "Query map was null"

    goto/32 :goto_3b

    nop

    :goto_22
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto/32 :goto_17

    nop

    :goto_23
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_24

    nop

    :goto_24
    if-nez v1, :cond_2

    goto/32 :goto_38

    :cond_2
    goto/32 :goto_39

    nop

    :goto_25
    goto :goto_2e

    :goto_26
    goto/32 :goto_19

    nop

    :goto_27
    throw p1

    :goto_28
    goto/32 :goto_46

    nop

    :goto_29
    new-instance v4, Ljava/lang/StringBuilder;

    goto/32 :goto_3f

    nop

    :goto_2a
    const-string v4, "Query map contained null value for key \'"

    goto/32 :goto_1f

    nop

    :goto_2b
    const-string v3, "\'."

    goto/32 :goto_1a

    nop

    :goto_2c
    iget-boolean v1, p0, Lgc/p$m;->d:Z

    goto/32 :goto_41

    nop

    :goto_2d
    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_2e
    goto/32 :goto_23

    nop

    :goto_2f
    if-nez v4, :cond_3

    goto/32 :goto_26

    :cond_3
    goto/32 :goto_2c

    nop

    :goto_30
    if-nez v2, :cond_4

    goto/32 :goto_34

    :cond_4
    goto/32 :goto_3a

    nop

    :goto_31
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_7

    nop

    :goto_32
    const-string v5, "Query map value \'"

    goto/32 :goto_43

    nop

    :goto_33
    throw p1

    :goto_34
    goto/32 :goto_1e

    nop

    :goto_35
    check-cast v1, Ljava/util/Map$Entry;

    goto/32 :goto_9

    nop

    :goto_36
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_40

    nop

    :goto_37
    throw p1

    :goto_38
    goto/32 :goto_4

    nop

    :goto_39
    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_35

    nop

    :goto_3a
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_2b

    nop

    :goto_3b
    invoke-static {p1, p2, v1, v0}, Lgc/y;->o(Ljava/lang/reflect/Method;ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object p1

    goto/32 :goto_6

    nop

    :goto_3c
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_31

    nop

    :goto_3d
    invoke-interface {v4, v1}, Lgc/f;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    goto/32 :goto_42

    nop

    :goto_3e
    invoke-static {p1, p2, v1, v0}, Lgc/y;->o(Ljava/lang/reflect/Method;ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object p1

    goto/32 :goto_27

    nop

    :goto_3f
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_32

    nop

    :goto_40
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_2

    nop

    :goto_41
    invoke-virtual {p1, v2, v4, v1}, Lgc/r;->g(Ljava/lang/String;Ljava/lang/String;Z)V

    goto/32 :goto_25

    nop

    :goto_42
    check-cast v4, Ljava/lang/String;

    goto/32 :goto_2f

    nop

    :goto_43
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_22

    nop

    :goto_44
    new-array v0, v0, [Ljava/lang/Object;

    goto/32 :goto_21

    nop

    :goto_45
    iget p2, p0, Lgc/p$m;->b:I

    goto/32 :goto_44

    nop

    :goto_46
    iget-object p1, p0, Lgc/p$m;->a:Ljava/lang/reflect/Method;

    goto/32 :goto_0

    nop

    :goto_47
    invoke-static {p1, p2, v1, v0}, Lgc/y;->o(Ljava/lang/reflect/Method;ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object p1

    goto/32 :goto_37

    nop

    :goto_48
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p2

    goto/32 :goto_2d

    nop
.end method
