.class final Lgc/p$h;
.super Lgc/p;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgc/p;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "h"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lgc/p<",
        "Lpb/s;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/reflect/Method;

.field private final b:I


# direct methods
.method constructor <init>(Ljava/lang/reflect/Method;I)V
    .locals 0

    invoke-direct {p0}, Lgc/p;-><init>()V

    iput-object p1, p0, Lgc/p$h;->a:Ljava/lang/reflect/Method;

    iput p2, p0, Lgc/p$h;->b:I

    return-void
.end method


# virtual methods
.method bridge synthetic a(Lgc/r;Ljava/lang/Object;)V
    .locals 0
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    goto/32 :goto_1

    nop

    :goto_0
    invoke-virtual {p0, p1, p2}, Lgc/p$h;->d(Lgc/r;Lpb/s;)V

    goto/32 :goto_2

    nop

    :goto_1
    check-cast p2, Lpb/s;

    goto/32 :goto_0

    nop

    :goto_2
    return-void
.end method

.method d(Lgc/r;Lpb/s;)V
    .locals 2
    .param p2    # Lpb/s;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    goto/32 :goto_0

    nop

    :goto_0
    if-nez p2, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_8

    nop

    :goto_1
    new-array v0, v0, [Ljava/lang/Object;

    goto/32 :goto_4

    nop

    :goto_2
    throw p1

    :goto_3
    iget p2, p0, Lgc/p$h;->b:I

    goto/32 :goto_9

    nop

    :goto_4
    const-string v1, "Headers parameter must not be null."

    goto/32 :goto_a

    nop

    :goto_5
    iget-object p1, p0, Lgc/p$h;->a:Ljava/lang/reflect/Method;

    goto/32 :goto_3

    nop

    :goto_6
    return-void

    :goto_7
    goto/32 :goto_5

    nop

    :goto_8
    invoke-virtual {p1, p2}, Lgc/r;->c(Lpb/s;)V

    goto/32 :goto_6

    nop

    :goto_9
    const/4 v0, 0x0

    goto/32 :goto_1

    nop

    :goto_a
    invoke-static {p1, p2, v1, v0}, Lgc/y;->o(Ljava/lang/reflect/Method;ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object p1

    goto/32 :goto_2

    nop
.end method
