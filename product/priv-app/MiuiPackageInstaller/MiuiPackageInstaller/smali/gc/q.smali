.class Lgc/q;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgc/q$a;
    }
.end annotation


# static fields
.field private static final c:Lgc/q;


# instance fields
.field private final a:Z

.field private final b:Ljava/lang/reflect/Constructor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/reflect/Constructor<",
            "Ljava/lang/invoke/MethodHandles$Lookup;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lgc/q;->e()Lgc/q;

    move-result-object v0

    sput-object v0, Lgc/q;->c:Lgc/q;

    return-void
.end method

.method constructor <init>(Z)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lgc/q;->a:Z

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    :try_start_0
    const-class p1, Ljava/lang/invoke/MethodHandles$Lookup;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Class;

    const/4 v2, 0x0

    const-class v3, Ljava/lang/Class;

    aput-object v3, v1, v2

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-virtual {p1, v1}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/reflect/Constructor;->setAccessible(Z)V
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_0
    iput-object v0, p0, Lgc/q;->b:Ljava/lang/reflect/Constructor;

    return-void
.end method

.method private static e()Lgc/q;
    .locals 2

    const-string v0, "java.vm.name"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "Dalvik"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lgc/q$a;

    invoke-direct {v0}, Lgc/q$a;-><init>()V

    goto :goto_0

    :cond_0
    new-instance v0, Lgc/q;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lgc/q;-><init>(Z)V

    :goto_0
    return-object v0
.end method

.method static f()Lgc/q;
    .locals 1

    sget-object v0, Lgc/q;->c:Lgc/q;

    return-object v0
.end method


# virtual methods
.method a(Ljava/util/concurrent/Executor;)Ljava/util/List;
    .locals 3
    .param p1    # Ljava/util/concurrent/Executor;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Executor;",
            ")",
            "Ljava/util/List<",
            "+",
            "Lgc/c$a;",
            ">;"
        }
    .end annotation

    goto/32 :goto_e

    nop

    :goto_0
    aput-object v0, p1, v1

    goto/32 :goto_a

    nop

    :goto_1
    if-nez p1, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_2

    nop

    :goto_2
    const/4 p1, 0x2

    goto/32 :goto_9

    nop

    :goto_3
    sget-object v2, Lgc/e;->a:Lgc/c$a;

    goto/32 :goto_b

    nop

    :goto_4
    goto :goto_10

    :goto_5
    goto/32 :goto_f

    nop

    :goto_6
    const/4 v1, 0x1

    goto/32 :goto_0

    nop

    :goto_7
    invoke-direct {v0, p1}, Lgc/g;-><init>(Ljava/util/concurrent/Executor;)V

    goto/32 :goto_8

    nop

    :goto_8
    iget-boolean p1, p0, Lgc/q;->a:Z

    goto/32 :goto_1

    nop

    :goto_9
    new-array p1, p1, [Lgc/c$a;

    goto/32 :goto_c

    nop

    :goto_a
    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    goto/32 :goto_4

    nop

    :goto_b
    aput-object v2, p1, v1

    goto/32 :goto_6

    nop

    :goto_c
    const/4 v1, 0x0

    goto/32 :goto_3

    nop

    :goto_d
    return-object p1

    :goto_e
    new-instance v0, Lgc/g;

    goto/32 :goto_7

    nop

    :goto_f
    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    :goto_10
    goto/32 :goto_d

    nop
.end method

.method b()Ljava/util/concurrent/Executor;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    goto/32 :goto_0

    nop

    :goto_0
    const/4 v0, 0x0

    goto/32 :goto_1

    nop

    :goto_1
    return-object v0
.end method

.method c()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "+",
            "Lgc/f$a;",
            ">;"
        }
    .end annotation

    goto/32 :goto_4

    nop

    :goto_0
    goto :goto_3

    :goto_1
    goto/32 :goto_2

    nop

    :goto_2
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    :goto_3
    goto/32 :goto_7

    nop

    :goto_4
    iget-boolean v0, p0, Lgc/q;->a:Z

    goto/32 :goto_8

    nop

    :goto_5
    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto/32 :goto_0

    nop

    :goto_6
    sget-object v0, Lgc/o;->a:Lgc/f$a;

    goto/32 :goto_5

    nop

    :goto_7
    return-object v0

    :goto_8
    if-nez v0, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_6

    nop
.end method

.method d()I
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return v0

    :goto_1
    iget-boolean v0, p0, Lgc/q;->a:Z

    goto/32 :goto_0

    nop
.end method

.method varargs g(Ljava/lang/reflect/Method;Ljava/lang/Class;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Method;",
            "Ljava/lang/Class<",
            "*>;",
            "Ljava/lang/Object;",
            "[",
            "Ljava/lang/Object;",
            ")",
            "Ljava/lang/Object;"
        }
    .end annotation

    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .annotation build Lorg/codehaus/mojo/animal_sniffer/IgnoreJRERequirement;
    .end annotation

    goto/32 :goto_10

    nop

    :goto_0
    goto :goto_5

    :goto_1
    goto/32 :goto_4

    nop

    :goto_2
    return-object p1

    :goto_3
    check-cast v0, Ljava/lang/invoke/MethodHandles$Lookup;

    goto/32 :goto_0

    nop

    :goto_4
    invoke-static {}, Ljava/lang/invoke/MethodHandles;->lookup()Ljava/lang/invoke/MethodHandles$Lookup;

    move-result-object v0

    :goto_5
    goto/32 :goto_8

    nop

    :goto_6
    invoke-virtual {p1, p3}, Ljava/lang/invoke/MethodHandle;->bindTo(Ljava/lang/Object;)Ljava/lang/invoke/MethodHandle;

    move-result-object p1

    goto/32 :goto_13

    nop

    :goto_7
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    goto/32 :goto_12

    nop

    :goto_8
    invoke-virtual {v0, p1, p2}, Ljava/lang/invoke/MethodHandles$Lookup;->unreflectSpecial(Ljava/lang/reflect/Method;Ljava/lang/Class;)Ljava/lang/invoke/MethodHandle;

    move-result-object p1

    goto/32 :goto_6

    nop

    :goto_9
    aput-object p2, v1, v2

    goto/32 :goto_d

    nop

    :goto_a
    const/4 v2, 0x0

    goto/32 :goto_9

    nop

    :goto_b
    const/4 v1, 0x2

    goto/32 :goto_e

    nop

    :goto_c
    const/4 v3, -0x1

    goto/32 :goto_7

    nop

    :goto_d
    const/4 v2, 0x1

    goto/32 :goto_c

    nop

    :goto_e
    new-array v1, v1, [Ljava/lang/Object;

    goto/32 :goto_a

    nop

    :goto_f
    if-nez v0, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_b

    nop

    :goto_10
    iget-object v0, p0, Lgc/q;->b:Ljava/lang/reflect/Constructor;

    goto/32 :goto_f

    nop

    :goto_11
    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_3

    nop

    :goto_12
    aput-object v3, v1, v2

    goto/32 :goto_11

    nop

    :goto_13
    invoke-virtual {p1, p4}, Ljava/lang/invoke/MethodHandle;->invokeWithArguments([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    goto/32 :goto_2

    nop
.end method

.method h(Ljava/lang/reflect/Method;)Z
    .locals 1
    .annotation build Lorg/codehaus/mojo/animal_sniffer/IgnoreJRERequirement;
    .end annotation

    goto/32 :goto_9

    nop

    :goto_0
    goto :goto_6

    :goto_1
    goto/32 :goto_5

    nop

    :goto_2
    return p1

    :goto_3
    if-nez v0, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_7

    nop

    :goto_4
    if-nez p1, :cond_1

    goto/32 :goto_1

    :cond_1
    goto/32 :goto_8

    nop

    :goto_5
    const/4 p1, 0x0

    :goto_6
    goto/32 :goto_2

    nop

    :goto_7
    invoke-virtual {p1}, Ljava/lang/reflect/Method;->isDefault()Z

    move-result p1

    goto/32 :goto_4

    nop

    :goto_8
    const/4 p1, 0x1

    goto/32 :goto_0

    nop

    :goto_9
    iget-boolean v0, p0, Lgc/q;->a:Z

    goto/32 :goto_3

    nop
.end method
