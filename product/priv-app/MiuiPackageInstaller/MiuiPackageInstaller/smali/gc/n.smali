.class final Lgc/n;
.super Ljava/lang/Object;

# interfaces
.implements Lgc/b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgc/n$c;,
        Lgc/n$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lgc/b<",
        "TT;>;"
    }
.end annotation


# instance fields
.field private final a:Lgc/s;

.field private final b:[Ljava/lang/Object;

.field private final c:Lpb/e$a;

.field private final d:Lgc/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lgc/f<",
            "Lpb/c0;",
            "TT;>;"
        }
    .end annotation
.end field

.field private volatile e:Z

.field private f:Lpb/e;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private g:Ljava/lang/Throwable;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private h:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method constructor <init>(Lgc/s;[Ljava/lang/Object;Lpb/e$a;Lgc/f;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lgc/s;",
            "[",
            "Ljava/lang/Object;",
            "Lpb/e$a;",
            "Lgc/f<",
            "Lpb/c0;",
            "TT;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lgc/n;->a:Lgc/s;

    iput-object p2, p0, Lgc/n;->b:[Ljava/lang/Object;

    iput-object p3, p0, Lgc/n;->c:Lpb/e$a;

    iput-object p4, p0, Lgc/n;->d:Lgc/f;

    return-void
.end method

.method private c()Lpb/e;
    .locals 3

    iget-object v0, p0, Lgc/n;->c:Lpb/e$a;

    iget-object v1, p0, Lgc/n;->a:Lgc/s;

    iget-object v2, p0, Lgc/n;->b:[Ljava/lang/Object;

    invoke-virtual {v1, v2}, Lgc/s;->a([Ljava/lang/Object;)Lpb/z;

    move-result-object v1

    invoke-interface {v0, v1}, Lpb/e$a;->a(Lpb/z;)Lpb/e;

    move-result-object v0

    const-string v1, "Call.Factory returned null."

    invoke-static {v0, v1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    return-object v0
.end method

.method private d()Lpb/e;
    .locals 2
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation

    iget-object v0, p0, Lgc/n;->f:Lpb/e;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    iget-object v0, p0, Lgc/n;->g:Ljava/lang/Throwable;

    if-eqz v0, :cond_3

    instance-of v1, v0, Ljava/io/IOException;

    if-nez v1, :cond_2

    instance-of v1, v0, Ljava/lang/RuntimeException;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/lang/RuntimeException;

    throw v0

    :cond_1
    check-cast v0, Ljava/lang/Error;

    throw v0

    :cond_2
    check-cast v0, Ljava/io/IOException;

    throw v0

    :cond_3
    :try_start_0
    invoke-direct {p0}, Lgc/n;->c()Lpb/e;

    move-result-object v0

    iput-object v0, p0, Lgc/n;->f:Lpb/e;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_0

    :catch_2
    move-exception v0

    :goto_0
    invoke-static {v0}, Lgc/y;->s(Ljava/lang/Throwable;)V

    iput-object v0, p0, Lgc/n;->g:Ljava/lang/Throwable;

    throw v0
.end method


# virtual methods
.method public declared-synchronized J()Lpb/z;
    .locals 3

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lgc/n;->d()Lpb/e;

    move-result-object v0

    invoke-interface {v0}, Lpb/e;->J()Lpb/z;

    move-result-object v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_1
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Unable to create request."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    monitor-exit p0

    throw v0
.end method

.method public K()Z
    .locals 2

    iget-boolean v0, p0, Lgc/n;->e:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    :cond_0
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lgc/n;->f:Lpb/e;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lpb/e;->K()Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    monitor-exit p0

    return v1

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public bridge synthetic L()Lgc/b;
    .locals 1

    invoke-virtual {p0}, Lgc/n;->a()Lgc/n;

    move-result-object v0

    return-object v0
.end method

.method public M(Lgc/d;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lgc/d<",
            "TT;>;)V"
        }
    .end annotation

    const-string v0, "callback == null"

    invoke-static {p1, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lgc/n;->h:Z

    if-nez v0, :cond_3

    const/4 v0, 0x1

    iput-boolean v0, p0, Lgc/n;->h:Z

    iget-object v0, p0, Lgc/n;->f:Lpb/e;

    iget-object v1, p0, Lgc/n;->g:Ljava/lang/Throwable;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-nez v0, :cond_0

    if-nez v1, :cond_0

    :try_start_1
    invoke-direct {p0}, Lgc/n;->c()Lpb/e;

    move-result-object v2

    iput-object v2, p0, Lgc/n;->f:Lpb/e;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v0, v2

    goto :goto_0

    :catchall_0
    move-exception v1

    :try_start_2
    invoke-static {v1}, Lgc/y;->s(Ljava/lang/Throwable;)V

    iput-object v1, p0, Lgc/n;->g:Ljava/lang/Throwable;

    :cond_0
    :goto_0
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    if-eqz v1, :cond_1

    invoke-interface {p1, p0, v1}, Lgc/d;->b(Lgc/b;Ljava/lang/Throwable;)V

    return-void

    :cond_1
    iget-boolean v1, p0, Lgc/n;->e:Z

    if-eqz v1, :cond_2

    invoke-interface {v0}, Lpb/e;->cancel()V

    :cond_2
    new-instance v1, Lgc/n$a;

    invoke-direct {v1, p0, p1}, Lgc/n$a;-><init>(Lgc/n;Lgc/d;)V

    invoke-interface {v0, v1}, Lpb/e;->b(Lpb/f;)V

    return-void

    :cond_3
    :try_start_3
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Already executed."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :catchall_1
    move-exception p1

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw p1
.end method

.method public a()Lgc/n;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lgc/n<",
            "TT;>;"
        }
    .end annotation

    new-instance v0, Lgc/n;

    iget-object v1, p0, Lgc/n;->a:Lgc/s;

    iget-object v2, p0, Lgc/n;->b:[Ljava/lang/Object;

    iget-object v3, p0, Lgc/n;->c:Lpb/e$a;

    iget-object v4, p0, Lgc/n;->d:Lgc/f;

    invoke-direct {v0, v1, v2, v3, v4}, Lgc/n;-><init>(Lgc/s;[Ljava/lang/Object;Lpb/e$a;Lgc/f;)V

    return-object v0
.end method

.method public cancel()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lgc/n;->e:Z

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lgc/n;->f:Lpb/e;

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lpb/e;->cancel()V

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lgc/n;->a()Lgc/n;

    move-result-object v0

    return-object v0
.end method

.method e(Lpb/b0;)Lgc/t;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lpb/b0;",
            ")",
            "Lgc/t<",
            "TT;>;"
        }
    .end annotation

    goto/32 :goto_1d

    nop

    :goto_0
    invoke-virtual {v0}, Lpb/c0;->close()V

    goto/32 :goto_21

    nop

    :goto_1
    return-object p1

    :catch_0
    move-exception p1

    goto/32 :goto_1c

    nop

    :goto_2
    invoke-virtual {v0}, Lpb/c0;->n()Lpb/v;

    move-result-object v2

    goto/32 :goto_11

    nop

    :goto_3
    new-instance v1, Lgc/n$b;

    goto/32 :goto_7

    nop

    :goto_4
    if-ne v1, v2, :cond_0

    goto/32 :goto_1b

    :cond_0
    goto/32 :goto_9

    nop

    :goto_5
    return-object p1

    :catchall_0
    move-exception p1

    goto/32 :goto_1e

    nop

    :goto_6
    invoke-static {v0, p1}, Lgc/t;->f(Ljava/lang/Object;Lpb/b0;)Lgc/t;

    move-result-object p1

    goto/32 :goto_1f

    nop

    :goto_7
    invoke-direct {v1, v0}, Lgc/n$b;-><init>(Lpb/c0;)V

    :try_start_0
    iget-object v0, p0, Lgc/n;->d:Lgc/f;

    invoke-interface {v0, v1}, Lgc/f;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0, p1}, Lgc/t;->f(Ljava/lang/Object;Lpb/b0;)Lgc/t;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_1

    nop

    :goto_8
    invoke-virtual {v0}, Lpb/c0;->close()V

    goto/32 :goto_5

    nop

    :goto_9
    const/16 v2, 0xcd

    goto/32 :goto_23

    nop

    :goto_a
    if-ge v1, v2, :cond_1

    goto/32 :goto_20

    :cond_1
    goto/32 :goto_15

    nop

    :goto_b
    invoke-virtual {p1, v1}, Lpb/b0$a;->b(Lpb/c0;)Lpb/b0$a;

    move-result-object p1

    goto/32 :goto_c

    nop

    :goto_c
    invoke-virtual {p1}, Lpb/b0$a;->c()Lpb/b0;

    move-result-object p1

    goto/32 :goto_22

    nop

    :goto_d
    const/16 v2, 0xc8

    goto/32 :goto_a

    nop

    :goto_e
    const/16 v2, 0xcc

    goto/32 :goto_4

    nop

    :goto_f
    invoke-virtual {p1}, Lpb/b0;->J()Lpb/b0$a;

    move-result-object p1

    goto/32 :goto_16

    nop

    :goto_10
    throw p1

    :goto_11
    invoke-virtual {v0}, Lpb/c0;->m()J

    move-result-wide v3

    goto/32 :goto_12

    nop

    :goto_12
    invoke-direct {v1, v2, v3, v4}, Lgc/n$c;-><init>(Lpb/v;J)V

    goto/32 :goto_b

    nop

    :goto_13
    goto :goto_1b

    :goto_14
    goto/32 :goto_3

    nop

    :goto_15
    const/16 v2, 0x12c

    goto/32 :goto_19

    nop

    :goto_16
    new-instance v1, Lgc/n$c;

    goto/32 :goto_2

    nop

    :goto_17
    goto :goto_20

    :goto_18
    goto/32 :goto_e

    nop

    :goto_19
    if-ge v1, v2, :cond_2

    goto/32 :goto_18

    :cond_2
    goto/32 :goto_17

    nop

    :goto_1a
    throw p1

    :goto_1b
    goto/32 :goto_0

    nop

    :goto_1c
    invoke-virtual {v1}, Lgc/n$b;->y()V

    goto/32 :goto_1a

    nop

    :goto_1d
    invoke-virtual {p1}, Lpb/b0;->b()Lpb/c0;

    move-result-object v0

    goto/32 :goto_f

    nop

    :goto_1e
    invoke-virtual {v0}, Lpb/c0;->close()V

    goto/32 :goto_10

    nop

    :goto_1f
    return-object p1

    :goto_20
    :try_start_1
    invoke-static {v0}, Lgc/y;->a(Lpb/c0;)Lpb/c0;

    move-result-object v1

    invoke-static {v1, p1}, Lgc/t;->c(Lpb/c0;Lpb/b0;)Lgc/t;

    move-result-object p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/32 :goto_8

    nop

    :goto_21
    const/4 v0, 0x0

    goto/32 :goto_6

    nop

    :goto_22
    invoke-virtual {p1}, Lpb/b0;->n()I

    move-result v1

    goto/32 :goto_d

    nop

    :goto_23
    if-eq v1, v2, :cond_3

    goto/32 :goto_14

    :cond_3
    goto/32 :goto_13

    nop
.end method
