.class final Lgc/p$g;
.super Lgc/p;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgc/p;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "g"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lgc/p<",
        "Ljava/util/Map<",
        "Ljava/lang/String;",
        "TT;>;>;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/reflect/Method;

.field private final b:I

.field private final c:Lgc/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lgc/f<",
            "TT;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/reflect/Method;ILgc/f;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Method;",
            "I",
            "Lgc/f<",
            "TT;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Lgc/p;-><init>()V

    iput-object p1, p0, Lgc/p$g;->a:Ljava/lang/reflect/Method;

    iput p2, p0, Lgc/p$g;->b:I

    iput-object p3, p0, Lgc/p$g;->c:Lgc/f;

    return-void
.end method


# virtual methods
.method bridge synthetic a(Lgc/r;Ljava/lang/Object;)V
    .locals 0
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    goto/32 :goto_0

    nop

    :goto_0
    check-cast p2, Ljava/util/Map;

    goto/32 :goto_1

    nop

    :goto_1
    invoke-virtual {p0, p1, p2}, Lgc/p$g;->d(Lgc/r;Ljava/util/Map;)V

    goto/32 :goto_2

    nop

    :goto_2
    return-void
.end method

.method d(Lgc/r;Ljava/util/Map;)V
    .locals 4
    .param p2    # Ljava/util/Map;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lgc/r;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "TT;>;)V"
        }
    .end annotation

    goto/32 :goto_10

    nop

    :goto_0
    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_1
    goto/32 :goto_2c

    nop

    :goto_2
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p2

    goto/32 :goto_0

    nop

    :goto_3
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_7

    nop

    :goto_4
    invoke-static {p1, p2, v1, v0}, Lgc/y;->o(Ljava/lang/reflect/Method;ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object p1

    goto/32 :goto_12

    nop

    :goto_5
    const-string v1, "Header map was null."

    goto/32 :goto_25

    nop

    :goto_6
    invoke-virtual {p1, v2, v1}, Lgc/r;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/32 :goto_27

    nop

    :goto_7
    const-string v3, "Header map contained null value for key \'"

    goto/32 :goto_a

    nop

    :goto_8
    throw p1

    :goto_9
    goto/32 :goto_14

    nop

    :goto_a
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_11

    nop

    :goto_b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_2e

    nop

    :goto_c
    if-nez v1, :cond_0

    goto/32 :goto_28

    :cond_0
    goto/32 :goto_d

    nop

    :goto_d
    iget-object v3, p0, Lgc/p$g;->c:Lgc/f;

    goto/32 :goto_24

    nop

    :goto_e
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_2b

    nop

    :goto_f
    check-cast v1, Ljava/lang/String;

    goto/32 :goto_6

    nop

    :goto_10
    const/4 v0, 0x0

    goto/32 :goto_26

    nop

    :goto_11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_1d

    nop

    :goto_12
    throw p1

    :goto_13
    goto/32 :goto_16

    nop

    :goto_14
    iget-object p1, p0, Lgc/p$g;->a:Ljava/lang/reflect/Method;

    goto/32 :goto_20

    nop

    :goto_15
    new-array v0, v0, [Ljava/lang/Object;

    goto/32 :goto_5

    nop

    :goto_16
    return-void

    :goto_17
    goto/32 :goto_18

    nop

    :goto_18
    iget-object p1, p0, Lgc/p$g;->a:Ljava/lang/reflect/Method;

    goto/32 :goto_30

    nop

    :goto_19
    if-nez v1, :cond_1

    goto/32 :goto_13

    :cond_1
    goto/32 :goto_2f

    nop

    :goto_1a
    new-instance v1, Ljava/lang/StringBuilder;

    goto/32 :goto_3

    nop

    :goto_1b
    check-cast v1, Ljava/util/Map$Entry;

    goto/32 :goto_e

    nop

    :goto_1c
    new-array v0, v0, [Ljava/lang/Object;

    goto/32 :goto_23

    nop

    :goto_1d
    const-string v2, "\'."

    goto/32 :goto_b

    nop

    :goto_1e
    if-nez v2, :cond_2

    goto/32 :goto_9

    :cond_2
    goto/32 :goto_22

    nop

    :goto_1f
    throw p1

    :goto_20
    iget p2, p0, Lgc/p$g;->b:I

    goto/32 :goto_1c

    nop

    :goto_21
    iget-object p1, p0, Lgc/p$g;->a:Ljava/lang/reflect/Method;

    goto/32 :goto_29

    nop

    :goto_22
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_c

    nop

    :goto_23
    const-string v1, "Header map contained null key."

    goto/32 :goto_4

    nop

    :goto_24
    invoke-interface {v3, v1}, Lgc/f;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_f

    nop

    :goto_25
    invoke-static {p1, p2, v1, v0}, Lgc/y;->o(Ljava/lang/reflect/Method;ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object p1

    goto/32 :goto_1f

    nop

    :goto_26
    if-nez p2, :cond_3

    goto/32 :goto_17

    :cond_3
    goto/32 :goto_2

    nop

    :goto_27
    goto/16 :goto_1

    :goto_28
    goto/32 :goto_21

    nop

    :goto_29
    iget p2, p0, Lgc/p$g;->b:I

    goto/32 :goto_1a

    nop

    :goto_2a
    invoke-static {p1, p2, v1, v0}, Lgc/y;->o(Ljava/lang/reflect/Method;ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object p1

    goto/32 :goto_8

    nop

    :goto_2b
    check-cast v2, Ljava/lang/String;

    goto/32 :goto_1e

    nop

    :goto_2c
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_19

    nop

    :goto_2d
    new-array v0, v0, [Ljava/lang/Object;

    goto/32 :goto_2a

    nop

    :goto_2e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_2d

    nop

    :goto_2f
    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_1b

    nop

    :goto_30
    iget p2, p0, Lgc/p$g;->b:I

    goto/32 :goto_15

    nop
.end method
