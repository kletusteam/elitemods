.class final Lgc/p$e;
.super Lgc/p;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgc/p;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "e"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lgc/p<",
        "Ljava/util/Map<",
        "Ljava/lang/String;",
        "TT;>;>;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/reflect/Method;

.field private final b:I

.field private final c:Lgc/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lgc/f<",
            "TT;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Z


# direct methods
.method constructor <init>(Ljava/lang/reflect/Method;ILgc/f;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Method;",
            "I",
            "Lgc/f<",
            "TT;",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    invoke-direct {p0}, Lgc/p;-><init>()V

    iput-object p1, p0, Lgc/p$e;->a:Ljava/lang/reflect/Method;

    iput p2, p0, Lgc/p$e;->b:I

    iput-object p3, p0, Lgc/p$e;->c:Lgc/f;

    iput-boolean p4, p0, Lgc/p$e;->d:Z

    return-void
.end method


# virtual methods
.method bridge synthetic a(Lgc/r;Ljava/lang/Object;)V
    .locals 0
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    check-cast p2, Ljava/util/Map;

    goto/32 :goto_2

    nop

    :goto_2
    invoke-virtual {p0, p1, p2}, Lgc/p$e;->d(Lgc/r;Ljava/util/Map;)V

    goto/32 :goto_0

    nop
.end method

.method d(Lgc/r;Ljava/util/Map;)V
    .locals 6
    .param p2    # Ljava/util/Map;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lgc/r;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "TT;>;)V"
        }
    .end annotation

    goto/32 :goto_1e

    nop

    :goto_0
    const-string v1, "Field map contained null key."

    goto/32 :goto_48

    nop

    :goto_1
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    goto/32 :goto_2b

    nop

    :goto_2
    if-nez v1, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_35

    nop

    :goto_3
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_9

    nop

    :goto_4
    throw p1

    :goto_5
    goto/32 :goto_27

    nop

    :goto_6
    invoke-virtual {p1, v2, v4, v1}, Lgc/r;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    goto/32 :goto_44

    nop

    :goto_7
    invoke-static {p1, p2, v1, v0}, Lgc/y;->o(Ljava/lang/reflect/Method;ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object p1

    goto/32 :goto_4

    nop

    :goto_8
    throw p1

    :goto_9
    new-array v0, v0, [Ljava/lang/Object;

    goto/32 :goto_7

    nop

    :goto_a
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_3e

    nop

    :goto_b
    const-string v1, "Field map was null."

    goto/32 :goto_d

    nop

    :goto_c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_3c

    nop

    :goto_d
    invoke-static {p1, p2, v1, v0}, Lgc/y;->o(Ljava/lang/reflect/Method;ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object p1

    goto/32 :goto_8

    nop

    :goto_e
    iget p2, p0, Lgc/p$e;->b:I

    goto/32 :goto_40

    nop

    :goto_f
    if-nez p2, :cond_1

    goto/32 :goto_14

    :cond_1
    goto/32 :goto_2f

    nop

    :goto_10
    check-cast v4, Ljava/lang/String;

    goto/32 :goto_37

    nop

    :goto_11
    throw p1

    :goto_12
    goto/32 :goto_13

    nop

    :goto_13
    return-void

    :goto_14
    goto/32 :goto_15

    nop

    :goto_15
    iget-object p1, p0, Lgc/p$e;->a:Ljava/lang/reflect/Method;

    goto/32 :goto_e

    nop

    :goto_16
    iget p2, p0, Lgc/p$e;->b:I

    goto/32 :goto_47

    nop

    :goto_17
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_1d

    nop

    :goto_18
    new-array v0, v0, [Ljava/lang/Object;

    goto/32 :goto_1c

    nop

    :goto_19
    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_22

    nop

    :goto_1a
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_38

    nop

    :goto_1b
    if-nez v2, :cond_2

    goto/32 :goto_2d

    :cond_2
    goto/32 :goto_43

    nop

    :goto_1c
    invoke-static {p1, p2, v1, v0}, Lgc/y;->o(Ljava/lang/reflect/Method;ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object p1

    goto/32 :goto_2c

    nop

    :goto_1d
    const-string v5, "Field map value \'"

    goto/32 :goto_29

    nop

    :goto_1e
    const/4 v0, 0x0

    goto/32 :goto_f

    nop

    :goto_1f
    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_20
    goto/32 :goto_30

    nop

    :goto_21
    new-instance v1, Ljava/lang/StringBuilder;

    goto/32 :goto_c

    nop

    :goto_22
    check-cast v1, Ljava/util/Map$Entry;

    goto/32 :goto_1

    nop

    :goto_23
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_32

    nop

    :goto_24
    iget-object v1, p0, Lgc/p$e;->c:Lgc/f;

    goto/32 :goto_2e

    nop

    :goto_25
    const-string v1, "\' converted to null by "

    goto/32 :goto_2a

    nop

    :goto_26
    invoke-interface {v4, v1}, Lgc/f;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    goto/32 :goto_10

    nop

    :goto_27
    iget-object p1, p0, Lgc/p$e;->a:Ljava/lang/reflect/Method;

    goto/32 :goto_3b

    nop

    :goto_28
    iget-object p1, p0, Lgc/p$e;->a:Ljava/lang/reflect/Method;

    goto/32 :goto_16

    nop

    :goto_29
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_3f

    nop

    :goto_2a
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_24

    nop

    :goto_2b
    check-cast v2, Ljava/lang/String;

    goto/32 :goto_1b

    nop

    :goto_2c
    throw p1

    :goto_2d
    goto/32 :goto_28

    nop

    :goto_2e
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    goto/32 :goto_42

    nop

    :goto_2f
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p2

    goto/32 :goto_1f

    nop

    :goto_30
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    goto/32 :goto_39

    nop

    :goto_31
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_36

    nop

    :goto_32
    const-string v1, " for key \'"

    goto/32 :goto_31

    nop

    :goto_33
    iget-boolean v1, p0, Lgc/p$e;->d:Z

    goto/32 :goto_6

    nop

    :goto_34
    iget p2, p0, Lgc/p$e;->b:I

    goto/32 :goto_41

    nop

    :goto_35
    iget-object v4, p0, Lgc/p$e;->c:Lgc/f;

    goto/32 :goto_26

    nop

    :goto_36
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_3d

    nop

    :goto_37
    if-nez v4, :cond_3

    goto/32 :goto_45

    :cond_3
    goto/32 :goto_33

    nop

    :goto_38
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_a

    nop

    :goto_39
    if-nez v1, :cond_4

    goto/32 :goto_12

    :cond_4
    goto/32 :goto_19

    nop

    :goto_3a
    const-string v3, "\'."

    goto/32 :goto_2

    nop

    :goto_3b
    iget p2, p0, Lgc/p$e;->b:I

    goto/32 :goto_21

    nop

    :goto_3c
    const-string v4, "Field map contained null value for key \'"

    goto/32 :goto_1a

    nop

    :goto_3d
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_3

    nop

    :goto_3e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_18

    nop

    :goto_3f
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto/32 :goto_25

    nop

    :goto_40
    new-array v0, v0, [Ljava/lang/Object;

    goto/32 :goto_b

    nop

    :goto_41
    new-instance v4, Ljava/lang/StringBuilder;

    goto/32 :goto_17

    nop

    :goto_42
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_23

    nop

    :goto_43
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    goto/32 :goto_3a

    nop

    :goto_44
    goto/16 :goto_20

    :goto_45
    goto/32 :goto_46

    nop

    :goto_46
    iget-object p1, p0, Lgc/p$e;->a:Ljava/lang/reflect/Method;

    goto/32 :goto_34

    nop

    :goto_47
    new-array v0, v0, [Ljava/lang/Object;

    goto/32 :goto_0

    nop

    :goto_48
    invoke-static {p1, p2, v1, v0}, Lgc/y;->o(Ljava/lang/reflect/Method;ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object p1

    goto/32 :goto_11

    nop
.end method
