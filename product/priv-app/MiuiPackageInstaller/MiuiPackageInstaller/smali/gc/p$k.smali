.class final Lgc/p$k;
.super Lgc/p;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgc/p;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "k"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lgc/p<",
        "TT;>;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/reflect/Method;

.field private final b:I

.field private final c:Ljava/lang/String;

.field private final d:Lgc/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lgc/f<",
            "TT;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Z


# direct methods
.method constructor <init>(Ljava/lang/reflect/Method;ILjava/lang/String;Lgc/f;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Method;",
            "I",
            "Ljava/lang/String;",
            "Lgc/f<",
            "TT;",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    invoke-direct {p0}, Lgc/p;-><init>()V

    iput-object p1, p0, Lgc/p$k;->a:Ljava/lang/reflect/Method;

    iput p2, p0, Lgc/p$k;->b:I

    const-string p1, "name == null"

    invoke-static {p3, p1}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    iput-object p3, p0, Lgc/p$k;->c:Ljava/lang/String;

    iput-object p4, p0, Lgc/p$k;->d:Lgc/f;

    iput-boolean p5, p0, Lgc/p$k;->e:Z

    return-void
.end method


# virtual methods
.method a(Lgc/r;Ljava/lang/Object;)V
    .locals 2
    .param p2    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lgc/r;",
            "TT;)V"
        }
    .end annotation

    goto/32 :goto_f

    nop

    :goto_0
    invoke-interface {v1, p2}, Lgc/f;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    goto/32 :goto_5

    nop

    :goto_1
    iget-object v1, p0, Lgc/p$k;->d:Lgc/f;

    goto/32 :goto_0

    nop

    :goto_2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_9

    nop

    :goto_3
    invoke-virtual {p1, v0, p2, v1}, Lgc/r;->f(Ljava/lang/String;Ljava/lang/String;Z)V

    goto/32 :goto_c

    nop

    :goto_4
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_15

    nop

    :goto_5
    check-cast p2, Ljava/lang/String;

    goto/32 :goto_e

    nop

    :goto_6
    invoke-static {p1, p2, v0, v1}, Lgc/y;->o(Ljava/lang/reflect/Method;ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object p1

    goto/32 :goto_8

    nop

    :goto_7
    iget p2, p0, Lgc/p$k;->b:I

    goto/32 :goto_4

    nop

    :goto_8
    throw p1

    :goto_9
    const-string v1, "\" value must not be null."

    goto/32 :goto_10

    nop

    :goto_a
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_13

    nop

    :goto_b
    iget-object v0, p0, Lgc/p$k;->c:Ljava/lang/String;

    goto/32 :goto_1

    nop

    :goto_c
    return-void

    :goto_d
    goto/32 :goto_16

    nop

    :goto_e
    iget-boolean v1, p0, Lgc/p$k;->e:Z

    goto/32 :goto_3

    nop

    :goto_f
    if-nez p2, :cond_0

    goto/32 :goto_d

    :cond_0
    goto/32 :goto_b

    nop

    :goto_10
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_a

    nop

    :goto_11
    new-array v1, v1, [Ljava/lang/Object;

    goto/32 :goto_6

    nop

    :goto_12
    iget-object v1, p0, Lgc/p$k;->c:Ljava/lang/String;

    goto/32 :goto_2

    nop

    :goto_13
    const/4 v1, 0x0

    goto/32 :goto_11

    nop

    :goto_14
    const-string v1, "Path parameter \""

    goto/32 :goto_17

    nop

    :goto_15
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_14

    nop

    :goto_16
    iget-object p1, p0, Lgc/p$k;->a:Ljava/lang/reflect/Method;

    goto/32 :goto_7

    nop

    :goto_17
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_12

    nop
.end method
