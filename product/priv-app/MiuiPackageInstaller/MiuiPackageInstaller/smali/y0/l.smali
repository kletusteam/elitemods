.class public Ly0/l;
.super Ly0/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ly0/a<",
        "Lc1/l;",
        "Landroid/graphics/Path;",
        ">;"
    }
.end annotation


# instance fields
.field private final i:Lc1/l;

.field private final j:Landroid/graphics/Path;


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Li1/a<",
            "Lc1/l;",
            ">;>;)V"
        }
    .end annotation

    invoke-direct {p0, p1}, Ly0/a;-><init>(Ljava/util/List;)V

    new-instance p1, Lc1/l;

    invoke-direct {p1}, Lc1/l;-><init>()V

    iput-object p1, p0, Ly0/l;->i:Lc1/l;

    new-instance p1, Landroid/graphics/Path;

    invoke-direct {p1}, Landroid/graphics/Path;-><init>()V

    iput-object p1, p0, Ly0/l;->j:Landroid/graphics/Path;

    return-void
.end method


# virtual methods
.method public bridge synthetic i(Li1/a;F)Ljava/lang/Object;
    .locals 0

    invoke-virtual {p0, p1, p2}, Ly0/l;->o(Li1/a;F)Landroid/graphics/Path;

    move-result-object p1

    return-object p1
.end method

.method public o(Li1/a;F)Landroid/graphics/Path;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Li1/a<",
            "Lc1/l;",
            ">;F)",
            "Landroid/graphics/Path;"
        }
    .end annotation

    iget-object v0, p1, Li1/a;->b:Ljava/lang/Object;

    check-cast v0, Lc1/l;

    iget-object p1, p1, Li1/a;->c:Ljava/lang/Object;

    check-cast p1, Lc1/l;

    iget-object v1, p0, Ly0/l;->i:Lc1/l;

    invoke-virtual {v1, v0, p1, p2}, Lc1/l;->c(Lc1/l;Lc1/l;F)V

    iget-object p1, p0, Ly0/l;->i:Lc1/l;

    iget-object p2, p0, Ly0/l;->j:Landroid/graphics/Path;

    invoke-static {p1, p2}, Lh1/g;->h(Lc1/l;Landroid/graphics/Path;)V

    iget-object p1, p0, Ly0/l;->j:Landroid/graphics/Path;

    return-object p1
.end method
