.class public Ly0/p;
.super Ly0/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "A:",
        "Ljava/lang/Object;",
        ">",
        "Ly0/a<",
        "TK;TA;>;"
    }
.end annotation


# instance fields
.field private final i:Li1/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Li1/b<",
            "TA;>;"
        }
    .end annotation
.end field

.field private final j:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TA;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Li1/c;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Li1/c<",
            "TA;>;)V"
        }
    .end annotation

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Ly0/p;-><init>(Li1/c;Ljava/lang/Object;)V

    return-void
.end method

.method public constructor <init>(Li1/c;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Li1/c<",
            "TA;>;TA;)V"
        }
    .end annotation

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Ly0/a;-><init>(Ljava/util/List;)V

    new-instance v0, Li1/b;

    invoke-direct {v0}, Li1/b;-><init>()V

    iput-object v0, p0, Ly0/p;->i:Li1/b;

    invoke-virtual {p0, p1}, Ly0/a;->m(Li1/c;)V

    iput-object p2, p0, Ly0/p;->j:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method c()F
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    const/high16 v0, 0x3f800000    # 1.0f

    goto/32 :goto_1

    nop

    :goto_1
    return v0
.end method

.method public h()Ljava/lang/Object;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TA;"
        }
    .end annotation

    iget-object v0, p0, Ly0/a;->e:Li1/c;

    iget-object v4, p0, Ly0/p;->j:Ljava/lang/Object;

    invoke-virtual {p0}, Ly0/a;->f()F

    move-result v5

    invoke-virtual {p0}, Ly0/a;->f()F

    move-result v6

    invoke-virtual {p0}, Ly0/a;->f()F

    move-result v7

    const/4 v1, 0x0

    const/4 v2, 0x0

    move-object v3, v4

    invoke-virtual/range {v0 .. v7}, Li1/c;->b(FFLjava/lang/Object;Ljava/lang/Object;FFF)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method i(Li1/a;F)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Li1/a<",
            "TK;>;F)TA;"
        }
    .end annotation

    goto/32 :goto_0

    nop

    :goto_0
    invoke-virtual {p0}, Ly0/p;->h()Ljava/lang/Object;

    move-result-object p1

    goto/32 :goto_1

    nop

    :goto_1
    return-object p1
.end method

.method public j()V
    .locals 1

    iget-object v0, p0, Ly0/a;->e:Li1/c;

    if-eqz v0, :cond_0

    invoke-super {p0}, Ly0/a;->j()V

    :cond_0
    return-void
.end method

.method public l(F)V
    .locals 0

    iput p1, p0, Ly0/a;->d:F

    return-void
.end method
