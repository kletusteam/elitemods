.class public Ly0/b;
.super Ly0/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ly0/f<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Li1/a<",
            "Ljava/lang/Integer;",
            ">;>;)V"
        }
    .end annotation

    invoke-direct {p0, p1}, Ly0/f;-><init>(Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method bridge synthetic i(Li1/a;F)Ljava/lang/Object;
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-object p1

    :goto_1
    invoke-virtual {p0, p1, p2}, Ly0/b;->q(Li1/a;F)Ljava/lang/Integer;

    move-result-object p1

    goto/32 :goto_0

    nop
.end method

.method public o()I
    .locals 2

    invoke-virtual {p0}, Ly0/a;->b()Li1/a;

    move-result-object v0

    invoke-virtual {p0}, Ly0/a;->d()F

    move-result v1

    invoke-virtual {p0, v0, v1}, Ly0/b;->p(Li1/a;F)I

    move-result v0

    return v0
.end method

.method public p(Li1/a;F)I
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Li1/a<",
            "Ljava/lang/Integer;",
            ">;F)I"
        }
    .end annotation

    iget-object v0, p1, Li1/a;->b:Ljava/lang/Object;

    if-eqz v0, :cond_1

    iget-object v1, p1, Li1/a;->c:Ljava/lang/Object;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v1, p1, Li1/a;->c:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v2, p0, Ly0/a;->e:Li1/c;

    if-eqz v2, :cond_0

    iget v3, p1, Li1/a;->e:F

    iget-object p1, p1, Li1/a;->f:Ljava/lang/Float;

    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result v4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {p0}, Ly0/a;->e()F

    move-result v8

    invoke-virtual {p0}, Ly0/a;->f()F

    move-result v9

    move v7, p2

    invoke-virtual/range {v2 .. v9}, Li1/c;->b(FFLjava/lang/Object;Ljava/lang/Object;FFF)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    return p1

    :cond_0
    const/4 p1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-static {p2, p1, v2}, Lh1/g;->b(FFF)F

    move-result p1

    invoke-static {p1, v0, v1}, Lh1/b;->c(FII)I

    move-result p1

    return p1

    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Missing values for keyframe."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method q(Li1/a;F)Ljava/lang/Integer;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Li1/a<",
            "Ljava/lang/Integer;",
            ">;F)",
            "Ljava/lang/Integer;"
        }
    .end annotation

    goto/32 :goto_1

    nop

    :goto_0
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    goto/32 :goto_2

    nop

    :goto_1
    invoke-virtual {p0, p1, p2}, Ly0/b;->p(Li1/a;F)I

    move-result p1

    goto/32 :goto_0

    nop

    :goto_2
    return-object p1
.end method
