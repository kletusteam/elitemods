.class public abstract Ly0/a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ly0/a$e;,
        Ly0/a$f;,
        Ly0/a$c;,
        Ly0/a$d;,
        Ly0/a$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "A:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ly0/a$b;",
            ">;"
        }
    .end annotation
.end field

.field private b:Z

.field private final c:Ly0/a$d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ly0/a$d<",
            "TK;>;"
        }
    .end annotation
.end field

.field protected d:F

.field protected e:Li1/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Li1/c<",
            "TA;>;"
        }
    .end annotation
.end field

.field private f:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TA;"
        }
    .end annotation
.end field

.field private g:F

.field private h:F


# direct methods
.method constructor <init>(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Li1/a<",
            "TK;>;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Ly0/a;->a:Ljava/util/List;

    const/4 v0, 0x0

    iput-boolean v0, p0, Ly0/a;->b:Z

    const/4 v0, 0x0

    iput v0, p0, Ly0/a;->d:F

    const/4 v0, 0x0

    iput-object v0, p0, Ly0/a;->f:Ljava/lang/Object;

    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Ly0/a;->g:F

    iput v0, p0, Ly0/a;->h:F

    invoke-static {p1}, Ly0/a;->n(Ljava/util/List;)Ly0/a$d;

    move-result-object p1

    iput-object p1, p0, Ly0/a;->c:Ly0/a$d;

    return-void
.end method

.method private g()F
    .locals 2

    iget v0, p0, Ly0/a;->g:F

    const/high16 v1, -0x40800000    # -1.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    iget-object v0, p0, Ly0/a;->c:Ly0/a$d;

    invoke-interface {v0}, Ly0/a$d;->c()F

    move-result v0

    iput v0, p0, Ly0/a;->g:F

    :cond_0
    iget v0, p0, Ly0/a;->g:F

    return v0
.end method

.method private static n(Ljava/util/List;)Ly0/a$d;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List<",
            "+",
            "Li1/a<",
            "TT;>;>;)",
            "Ly0/a$d<",
            "TT;>;"
        }
    .end annotation

    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance p0, Ly0/a$c;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Ly0/a$c;-><init>(Ly0/a$a;)V

    return-object p0

    :cond_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    new-instance v0, Ly0/a$f;

    invoke-direct {v0, p0}, Ly0/a$f;-><init>(Ljava/util/List;)V

    return-object v0

    :cond_1
    new-instance v0, Ly0/a$e;

    invoke-direct {v0, p0}, Ly0/a$e;-><init>(Ljava/util/List;)V

    return-object v0
.end method


# virtual methods
.method public a(Ly0/a$b;)V
    .locals 1

    iget-object v0, p0, Ly0/a;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method protected b()Li1/a;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Li1/a<",
            "TK;>;"
        }
    .end annotation

    const-string v0, "BaseKeyframeAnimation#getCurrentKeyframe"

    invoke-static {v0}, Lv0/c;->a(Ljava/lang/String;)V

    iget-object v1, p0, Ly0/a;->c:Ly0/a$d;

    invoke-interface {v1}, Ly0/a$d;->d()Li1/a;

    move-result-object v1

    invoke-static {v0}, Lv0/c;->b(Ljava/lang/String;)F

    return-object v1
.end method

.method c()F
    .locals 2

    goto/32 :goto_3

    nop

    :goto_0
    cmpl-float v0, v0, v1

    goto/32 :goto_6

    nop

    :goto_1
    iput v0, p0, Ly0/a;->h:F

    :goto_2
    goto/32 :goto_7

    nop

    :goto_3
    iget v0, p0, Ly0/a;->h:F

    goto/32 :goto_5

    nop

    :goto_4
    return v0

    :goto_5
    const/high16 v1, -0x40800000    # -1.0f

    goto/32 :goto_0

    nop

    :goto_6
    if-eqz v0, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_8

    nop

    :goto_7
    iget v0, p0, Ly0/a;->h:F

    goto/32 :goto_4

    nop

    :goto_8
    iget-object v0, p0, Ly0/a;->c:Ly0/a$d;

    goto/32 :goto_9

    nop

    :goto_9
    invoke-interface {v0}, Ly0/a$d;->a()F

    move-result v0

    goto/32 :goto_1

    nop
.end method

.method protected d()F
    .locals 2

    invoke-virtual {p0}, Ly0/a;->b()Li1/a;

    move-result-object v0

    invoke-virtual {v0}, Li1/a;->h()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    iget-object v0, v0, Li1/a;->d:Landroid/view/animation/Interpolator;

    invoke-virtual {p0}, Ly0/a;->e()F

    move-result v1

    invoke-interface {v0, v1}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v0

    return v0
.end method

.method e()F
    .locals 3

    goto/32 :goto_d

    nop

    :goto_0
    return v1

    :goto_1
    goto/32 :goto_6

    nop

    :goto_2
    div-float/2addr v1, v2

    goto/32 :goto_e

    nop

    :goto_3
    const/4 v1, 0x0

    goto/32 :goto_9

    nop

    :goto_4
    if-nez v2, :cond_0

    goto/32 :goto_b

    :cond_0
    goto/32 :goto_a

    nop

    :goto_5
    invoke-virtual {v0}, Li1/a;->h()Z

    move-result v2

    goto/32 :goto_4

    nop

    :goto_6
    invoke-virtual {p0}, Ly0/a;->b()Li1/a;

    move-result-object v0

    goto/32 :goto_5

    nop

    :goto_7
    invoke-virtual {v0}, Li1/a;->e()F

    move-result v2

    goto/32 :goto_8

    nop

    :goto_8
    sub-float/2addr v1, v2

    goto/32 :goto_f

    nop

    :goto_9
    if-nez v0, :cond_1

    goto/32 :goto_1

    :cond_1
    goto/32 :goto_0

    nop

    :goto_a
    return v1

    :goto_b
    goto/32 :goto_11

    nop

    :goto_c
    invoke-virtual {v0}, Li1/a;->e()F

    move-result v0

    goto/32 :goto_10

    nop

    :goto_d
    iget-boolean v0, p0, Ly0/a;->b:Z

    goto/32 :goto_3

    nop

    :goto_e
    return v1

    :goto_f
    invoke-virtual {v0}, Li1/a;->b()F

    move-result v2

    goto/32 :goto_c

    nop

    :goto_10
    sub-float/2addr v2, v0

    goto/32 :goto_2

    nop

    :goto_11
    iget v1, p0, Ly0/a;->d:F

    goto/32 :goto_7

    nop
.end method

.method public f()F
    .locals 1

    iget v0, p0, Ly0/a;->d:F

    return v0
.end method

.method public h()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TA;"
        }
    .end annotation

    invoke-virtual {p0}, Ly0/a;->d()F

    move-result v0

    iget-object v1, p0, Ly0/a;->e:Li1/c;

    if-nez v1, :cond_0

    iget-object v1, p0, Ly0/a;->c:Ly0/a$d;

    invoke-interface {v1, v0}, Ly0/a$d;->b(F)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Ly0/a;->f:Ljava/lang/Object;

    return-object v0

    :cond_0
    invoke-virtual {p0}, Ly0/a;->b()Li1/a;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Ly0/a;->i(Li1/a;F)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Ly0/a;->f:Ljava/lang/Object;

    return-object v0
.end method

.method abstract i(Li1/a;F)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Li1/a<",
            "TK;>;F)TA;"
        }
    .end annotation
.end method

.method public j()V
    .locals 2

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ly0/a;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Ly0/a;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ly0/a$b;

    invoke-interface {v1}, Ly0/a$b;->b()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public k()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Ly0/a;->b:Z

    return-void
.end method

.method public l(F)V
    .locals 1

    iget-object v0, p0, Ly0/a;->c:Ly0/a$d;

    invoke-interface {v0}, Ly0/a$d;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    invoke-direct {p0}, Ly0/a;->g()F

    move-result v0

    cmpg-float v0, p1, v0

    if-gez v0, :cond_1

    invoke-direct {p0}, Ly0/a;->g()F

    move-result p1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Ly0/a;->c()F

    move-result v0

    cmpl-float v0, p1, v0

    if-lez v0, :cond_2

    invoke-virtual {p0}, Ly0/a;->c()F

    move-result p1

    :cond_2
    :goto_0
    iget v0, p0, Ly0/a;->d:F

    cmpl-float v0, p1, v0

    if-nez v0, :cond_3

    return-void

    :cond_3
    iput p1, p0, Ly0/a;->d:F

    iget-object v0, p0, Ly0/a;->c:Ly0/a$d;

    invoke-interface {v0, p1}, Ly0/a$d;->e(F)Z

    move-result p1

    if-eqz p1, :cond_4

    invoke-virtual {p0}, Ly0/a;->j()V

    :cond_4
    return-void
.end method

.method public m(Li1/c;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Li1/c<",
            "TA;>;)V"
        }
    .end annotation

    iget-object v0, p0, Ly0/a;->e:Li1/c;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Li1/c;->c(Ly0/a;)V

    :cond_0
    iput-object p1, p0, Ly0/a;->e:Li1/c;

    if-eqz p1, :cond_1

    invoke-virtual {p1, p0}, Li1/c;->c(Ly0/a;)V

    :cond_1
    return-void
.end method
