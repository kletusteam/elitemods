.class public Ly0/c;
.super Ly0/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ly0/f<",
        "Ljava/lang/Float;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Li1/a<",
            "Ljava/lang/Float;",
            ">;>;)V"
        }
    .end annotation

    invoke-direct {p0, p1}, Ly0/f;-><init>(Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method bridge synthetic i(Li1/a;F)Ljava/lang/Object;
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-object p1

    :goto_1
    invoke-virtual {p0, p1, p2}, Ly0/c;->q(Li1/a;F)Ljava/lang/Float;

    move-result-object p1

    goto/32 :goto_0

    nop
.end method

.method public o()F
    .locals 2

    invoke-virtual {p0}, Ly0/a;->b()Li1/a;

    move-result-object v0

    invoke-virtual {p0}, Ly0/a;->d()F

    move-result v1

    invoke-virtual {p0, v0, v1}, Ly0/c;->p(Li1/a;F)F

    move-result v0

    return v0
.end method

.method p(Li1/a;F)F
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Li1/a<",
            "Ljava/lang/Float;",
            ">;F)F"
        }
    .end annotation

    goto/32 :goto_13

    nop

    :goto_0
    iget v2, p1, Li1/a;->e:F

    goto/32 :goto_17

    nop

    :goto_1
    iget-object v5, p1, Li1/a;->c:Ljava/lang/Object;

    goto/32 :goto_a

    nop

    :goto_2
    throw p1

    :goto_3
    return p1

    :goto_4
    goto/32 :goto_1b

    nop

    :goto_5
    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_2

    nop

    :goto_6
    if-nez v0, :cond_0

    goto/32 :goto_19

    :cond_0
    goto/32 :goto_1c

    nop

    :goto_7
    iget-object v4, p1, Li1/a;->b:Ljava/lang/Object;

    goto/32 :goto_1

    nop

    :goto_8
    invoke-static {v0, p1, p2}, Lh1/g;->j(FFF)F

    move-result p1

    goto/32 :goto_18

    nop

    :goto_9
    invoke-virtual {p1}, Li1/a;->c()F

    move-result p1

    goto/32 :goto_8

    nop

    :goto_a
    invoke-virtual {p0}, Ly0/a;->e()F

    move-result v7

    goto/32 :goto_1a

    nop

    :goto_b
    if-nez v1, :cond_1

    goto/32 :goto_4

    :cond_1
    goto/32 :goto_0

    nop

    :goto_c
    if-nez v0, :cond_2

    goto/32 :goto_4

    :cond_2
    goto/32 :goto_15

    nop

    :goto_d
    iget-object v1, p0, Ly0/a;->e:Li1/c;

    goto/32 :goto_b

    nop

    :goto_e
    move v6, p2

    goto/32 :goto_f

    nop

    :goto_f
    invoke-virtual/range {v1 .. v8}, Li1/c;->b(FFLjava/lang/Object;Ljava/lang/Object;FFF)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_14

    nop

    :goto_10
    const-string p2, "Missing values for keyframe."

    goto/32 :goto_5

    nop

    :goto_11
    if-nez v0, :cond_3

    goto/32 :goto_19

    :cond_3
    goto/32 :goto_d

    nop

    :goto_12
    new-instance p1, Ljava/lang/IllegalStateException;

    goto/32 :goto_10

    nop

    :goto_13
    iget-object v0, p1, Li1/a;->b:Ljava/lang/Object;

    goto/32 :goto_6

    nop

    :goto_14
    check-cast v0, Ljava/lang/Float;

    goto/32 :goto_c

    nop

    :goto_15
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result p1

    goto/32 :goto_3

    nop

    :goto_16
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v3

    goto/32 :goto_7

    nop

    :goto_17
    iget-object v0, p1, Li1/a;->f:Ljava/lang/Float;

    goto/32 :goto_16

    nop

    :goto_18
    return p1

    :goto_19
    goto/32 :goto_12

    nop

    :goto_1a
    invoke-virtual {p0}, Ly0/a;->f()F

    move-result v8

    goto/32 :goto_e

    nop

    :goto_1b
    invoke-virtual {p1}, Li1/a;->f()F

    move-result v0

    goto/32 :goto_9

    nop

    :goto_1c
    iget-object v0, p1, Li1/a;->c:Ljava/lang/Object;

    goto/32 :goto_11

    nop
.end method

.method q(Li1/a;F)Ljava/lang/Float;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Li1/a<",
            "Ljava/lang/Float;",
            ">;F)",
            "Ljava/lang/Float;"
        }
    .end annotation

    goto/32 :goto_2

    nop

    :goto_0
    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p1

    goto/32 :goto_1

    nop

    :goto_1
    return-object p1

    :goto_2
    invoke-virtual {p0, p1, p2}, Ly0/c;->p(Li1/a;F)F

    move-result p1

    goto/32 :goto_0

    nop
.end method
