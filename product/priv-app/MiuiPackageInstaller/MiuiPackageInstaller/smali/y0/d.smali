.class public Ly0/d;
.super Ly0/f;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ly0/f<",
        "Lc1/c;",
        ">;"
    }
.end annotation


# instance fields
.field private final i:Lc1/c;


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Li1/a<",
            "Lc1/c;",
            ">;>;)V"
        }
    .end annotation

    invoke-direct {p0, p1}, Ly0/f;-><init>(Ljava/util/List;)V

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Li1/a;

    iget-object p1, p1, Li1/a;->b:Ljava/lang/Object;

    check-cast p1, Lc1/c;

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lc1/c;->c()I

    move-result v0

    :goto_0
    new-instance p1, Lc1/c;

    new-array v1, v0, [F

    new-array v0, v0, [I

    invoke-direct {p1, v1, v0}, Lc1/c;-><init>([F[I)V

    iput-object p1, p0, Ly0/d;->i:Lc1/c;

    return-void
.end method


# virtual methods
.method bridge synthetic i(Li1/a;F)Ljava/lang/Object;
    .locals 0

    goto/32 :goto_1

    nop

    :goto_0
    return-object p1

    :goto_1
    invoke-virtual {p0, p1, p2}, Ly0/d;->o(Li1/a;F)Lc1/c;

    move-result-object p1

    goto/32 :goto_0

    nop
.end method

.method o(Li1/a;F)Lc1/c;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Li1/a<",
            "Lc1/c;",
            ">;F)",
            "Lc1/c;"
        }
    .end annotation

    goto/32 :goto_6

    nop

    :goto_0
    iget-object p1, p1, Li1/a;->c:Ljava/lang/Object;

    goto/32 :goto_3

    nop

    :goto_1
    return-object p1

    :goto_2
    iget-object v1, p1, Li1/a;->b:Ljava/lang/Object;

    goto/32 :goto_7

    nop

    :goto_3
    check-cast p1, Lc1/c;

    goto/32 :goto_5

    nop

    :goto_4
    iget-object p1, p0, Ly0/d;->i:Lc1/c;

    goto/32 :goto_1

    nop

    :goto_5
    invoke-virtual {v0, v1, p1, p2}, Lc1/c;->d(Lc1/c;Lc1/c;F)V

    goto/32 :goto_4

    nop

    :goto_6
    iget-object v0, p0, Ly0/d;->i:Lc1/c;

    goto/32 :goto_2

    nop

    :goto_7
    check-cast v1, Lc1/c;

    goto/32 :goto_0

    nop
.end method
