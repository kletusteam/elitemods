.class public final Ls5/n;
.super Ls5/l;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ls5/n$b;,
        Ls5/n$c;,
        Ls5/n$a;
    }
.end annotation


# static fields
.field public static final p:Ls5/n$b;


# instance fields
.field private h:Landroid/os/Handler;

.field private i:Ljava/lang/Runnable;

.field private final j:Ljava/lang/String;

.field private final k:Ls5/n$c;

.field public l:Ljava/lang/String;

.field public m:Ljava/lang/String;

.field public n:Ljava/lang/String;

.field private o:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ls5/n$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ls5/n$b;-><init>(Lm8/g;)V

    sput-object v0, Ls5/n;->p:Ls5/n$b;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const-string v0, "context"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Ls5/l;-><init>(Landroid/content/Context;)V

    new-instance p1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p1, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object p1, p0, Ls5/n;->h:Landroid/os/Handler;

    new-instance p1, Ls5/m;

    invoke-direct {p1, p0}, Ls5/m;-><init>(Ls5/n;)V

    iput-object p1, p0, Ls5/n;->i:Ljava/lang/Runnable;

    const-string p1, "MarketInstaller"

    iput-object p1, p0, Ls5/n;->j:Ljava/lang/String;

    new-instance v0, Ls5/n$c;

    invoke-direct {v0, p0}, Ls5/n$c;-><init>(Ls5/n;)V

    iput-object v0, p0, Ls5/n;->k:Ls5/n$c;

    const-string v1, "MarketInstaller is init"

    invoke-static {p1, v1}, Lf6/o;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    new-instance p1, Landroid/content/IntentFilter;

    invoke-direct {p1}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "com.xiaomi.market.DOWNLOAD_INSTALL_RESULT"

    invoke-virtual {p1, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    invoke-virtual {p0}, Ls5/l;->e()Landroid/content/Context;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1, v0, p1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :cond_0
    return-void
.end method

.method private static final F(Ls5/n;)V
    .locals 2

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    const v1, -0xea60

    invoke-virtual {p0, v0, v1}, Ls5/l;->f(II)V

    invoke-virtual {p0}, Ls5/n;->l()V

    return-void
.end method

.method public static synthetic o(Ls5/n;)V
    .locals 0

    invoke-static {p0}, Ls5/n;->F(Ls5/n;)V

    return-void
.end method

.method public static final synthetic p(Ls5/n;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ls5/n;->y(II)V

    return-void
.end method

.method private final y(II)V
    .locals 0

    invoke-virtual {p0, p1, p2}, Ls5/l;->f(II)V

    invoke-virtual {p0}, Ls5/n;->l()V

    return-void
.end method


# virtual methods
.method public A()V
    .locals 2

    sget-object v0, Lcom/android/packageinstaller/InstallerApplication;->c:Lcom/android/packageinstaller/InstallerApplication;

    invoke-static {v0}, Li5/a;->c(Landroid/app/Application;)Li5/a;

    move-result-object v0

    invoke-virtual {p0}, Ls5/n;->s()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Li5/a;->f(Ljava/lang/String;)Z

    return-void
.end method

.method public final B(Ljava/lang/String;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Ls5/n;->l:Ljava/lang/String;

    return-void
.end method

.method public final D(Ljava/lang/String;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Ls5/n;->m:Ljava/lang/String;

    return-void
.end method

.method public final E(Ljava/lang/String;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Ls5/n;->n:Ljava/lang/String;

    return-void
.end method

.method public l()V
    .locals 2

    invoke-super {p0}, Ls5/l;->l()V

    iget-object v0, p0, Ls5/n;->k:Ls5/n$c;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Ls5/n;->o:Z

    if-nez v0, :cond_1

    invoke-virtual {p0}, Ls5/l;->e()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Ls5/n;->k:Ls5/n$c;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Ls5/n;->o:Z

    :cond_1
    return-void
.end method

.method public n()V
    .locals 6

    invoke-virtual {p0}, Ls5/n;->s()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    invoke-virtual {p0}, Ls5/l;->e()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_0

    goto/16 :goto_1

    :cond_0
    iget-object v0, p0, Ls5/n;->j:Ljava/lang/String;

    const-string v1, "start market install"

    invoke-static {v0, v1}, Lf6/o;->e(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    iget-object v0, p0, Ls5/n;->j:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "marketDeeplink = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ls5/n;->s()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lf6/o;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    const v0, -0xea63

    const/4 v1, 0x1

    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ls5/n;->s()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "&sourcePackageChain="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ls5/n;->v()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Ls5/n;->B(Ljava/lang/String;)V

    sget-object v2, Lcom/android/packageinstaller/InstallerApplication;->c:Lcom/android/packageinstaller/InstallerApplication;

    invoke-static {v2}, Li5/a;->c(Landroid/app/Application;)Li5/a;

    move-result-object v2

    invoke-virtual {p0}, Ls5/n;->s()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Li5/a;->b(Ljava/lang/String;)Z

    sget-object v2, Lcom/android/packageinstaller/InstallerApplication;->c:Lcom/android/packageinstaller/InstallerApplication;

    invoke-static {v2}, Li5/a;->c(Landroid/app/Application;)Li5/a;

    move-result-object v2

    invoke-virtual {p0}, Ls5/n;->s()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Li5/a;->f(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Ls5/l;->b:Ls5/l$c;

    if-eqz v2, :cond_1

    invoke-interface {v2, p0}, Ls5/l$c;->u(Ls5/l;)V

    :cond_1
    iget-object v2, p0, Ls5/n;->h:Landroid/os/Handler;

    iget-object v3, p0, Ls5/n;->i:Ljava/lang/Runnable;

    const-wide/16 v4, 0x2710

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    :cond_2
    iget-object v2, p0, Ls5/l;->b:Ls5/l$c;

    if-eqz v2, :cond_4

    invoke-interface {v2, p0, v1, v0}, Ls5/l$c;->C(Ls5/l;II)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    iget-object v3, p0, Ls5/l;->b:Ls5/l$c;

    if-eqz v3, :cond_3

    invoke-interface {v3, p0, v1, v0}, Ls5/l$c;->C(Ls5/l;II)V

    :cond_3
    iget-object v0, p0, Ls5/n;->j:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception catched : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v2}, Lf6/o;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_4
    :goto_0
    return-void

    :cond_5
    :goto_1
    iget-object v0, p0, Ls5/n;->j:Ljava/lang/String;

    const-string v1, "marketDeeplink is empty or mContext == null"

    invoke-static {v0, v1}, Lf6/o;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    return-void
.end method

.method public final r()Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Ls5/n;->h:Landroid/os/Handler;

    return-object v0
.end method

.method public final s()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ls5/n;->l:Ljava/lang/String;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    const-string v0, "marketDeeplink"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v0, 0x0

    return-object v0
.end method

.method public final t()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ls5/n;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    const-string v0, "packageName"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v0, 0x0

    return-object v0
.end method

.method public final v()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ls5/n;->n:Ljava/lang/String;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    const-string v0, "sourcePackageName"

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v0, 0x0

    return-object v0
.end method

.method public final w()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ls5/n;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final x()Ljava/lang/Runnable;
    .locals 1

    iget-object v0, p0, Ls5/n;->i:Ljava/lang/Runnable;

    return-object v0
.end method

.method public z()V
    .locals 2

    sget-object v0, Lcom/android/packageinstaller/InstallerApplication;->c:Lcom/android/packageinstaller/InstallerApplication;

    invoke-static {v0}, Li5/a;->c(Landroid/app/Application;)Li5/a;

    move-result-object v0

    invoke-virtual {p0}, Ls5/n;->s()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Li5/a;->e(Ljava/lang/String;)Z

    return-void
.end method
