.class public abstract Ls5/l;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ls5/l$c;,
        Ls5/l$a;,
        Ls5/l$b;
    }
.end annotation


# static fields
.field public static final f:Ls5/l$b;

.field private static final g:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field private a:Landroid/content/Context;

.field protected b:Ls5/l$c;

.field protected c:I

.field private d:I

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ls5/l$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ls5/l$b;-><init>(Lm8/g;)V

    sput-object v0, Ls5/l;->f:Ls5/l$b;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    sput-object v0, Ls5/l;->g:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const-string v0, "mContext"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Ls5/l;->a:Landroid/content/Context;

    sget-object p1, Ls5/l;->g:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result p1

    iput p1, p0, Ls5/l;->c:I

    const/4 p1, -0x1

    iput p1, p0, Ls5/l;->d:I

    return-void
.end method

.method public static synthetic a(Ls5/l;II)V
    .locals 0

    invoke-static {p0, p1, p2}, Ls5/l;->k(Ls5/l;II)V

    return-void
.end method

.method public static synthetic b(Ls5/l;I)V
    .locals 0

    invoke-static {p0, p1}, Ls5/l;->j(Ls5/l;I)V

    return-void
.end method

.method public static synthetic c(Ls5/l;II)V
    .locals 0

    invoke-static {p0, p1, p2}, Ls5/l;->g(Ls5/l;II)V

    return-void
.end method

.method private static final g(Ls5/l;II)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Ls5/l;->b:Ls5/l$c;

    if-eqz v0, :cond_0

    invoke-static {v0}, Lm8/i;->c(Ljava/lang/Object;)V

    invoke-interface {v0, p0, p1, p2}, Ls5/l$c;->C(Ls5/l;II)V

    :cond_0
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "onInstallFinished success :"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " code : "

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string p1, "Installer"

    invoke-static {p1, p0}, Lf6/o;->e(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    return-void
.end method

.method private static final j(Ls5/l;I)V
    .locals 2

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget v0, p0, Ls5/l;->d:I

    if-ne v0, p1, :cond_0

    return-void

    :cond_0
    iput p1, p0, Ls5/l;->d:I

    const/4 v0, 0x0

    iput v0, p0, Ls5/l;->e:I

    iget-object v1, p0, Ls5/l;->b:Ls5/l$c;

    if-eqz v1, :cond_1

    invoke-static {v1}, Lm8/i;->c(Ljava/lang/Object;)V

    invoke-interface {v1, p0, p1, v0}, Ls5/l$c;->q(Ls5/l;II)V

    :cond_1
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "onInstallStateChanged state :"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string p1, "Installer"

    invoke-static {p1, p0}, Lf6/o;->e(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    return-void
.end method

.method private static final k(Ls5/l;II)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget v0, p0, Ls5/l;->d:I

    if-ne v0, p1, :cond_0

    iget v0, p0, Ls5/l;->e:I

    if-ne v0, p2, :cond_0

    return-void

    :cond_0
    iput p1, p0, Ls5/l;->d:I

    iput p2, p0, Ls5/l;->e:I

    iget-object v0, p0, Ls5/l;->b:Ls5/l$c;

    if-eqz v0, :cond_1

    invoke-static {v0}, Lm8/i;->c(Ljava/lang/Object;)V

    invoke-interface {v0, p0, p1, p2}, Ls5/l$c;->q(Ls5/l;II)V

    :cond_1
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "onInstallStateChanged state :"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " progress : "

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string p1, "Installer"

    invoke-static {p1, p0}, Lf6/o;->e(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    return-void
.end method


# virtual methods
.method public d()V
    .locals 0

    return-void
.end method

.method protected final e()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Ls5/l;->a:Landroid/content/Context;

    return-object v0
.end method

.method protected final f(II)V
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ls5/l;->h(I)V

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object v0

    new-instance v1, Ls5/k;

    invoke-direct {v1, p0, p1, p2}, Ls5/k;-><init>(Ls5/l;II)V

    invoke-virtual {v0, v1}, Lf6/z;->e(Ljava/lang/Runnable;)V

    return-void
.end method

.method protected final h(I)V
    .locals 2

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object v0

    new-instance v1, Ls5/i;

    invoke-direct {v1, p0, p1}, Ls5/i;-><init>(Ls5/l;I)V

    invoke-virtual {v0, v1}, Lf6/z;->e(Ljava/lang/Runnable;)V

    return-void
.end method

.method protected final i(II)V
    .locals 2

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object v0

    new-instance v1, Ls5/j;

    invoke-direct {v1, p0, p1, p2}, Ls5/j;-><init>(Ls5/l;II)V

    invoke-virtual {v0, v1}, Lf6/z;->e(Ljava/lang/Runnable;)V

    return-void
.end method

.method public l()V
    .locals 0

    return-void
.end method

.method public final m(Ls5/l$c;)V
    .locals 0

    iput-object p1, p0, Ls5/l;->b:Ls5/l$c;

    return-void
.end method

.method public abstract n()V
.end method
