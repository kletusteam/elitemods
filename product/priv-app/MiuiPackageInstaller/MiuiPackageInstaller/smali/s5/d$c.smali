.class final Ls5/d$c;
.super Landroid/content/BroadcastReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ls5/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "c"
.end annotation


# instance fields
.field final synthetic a:Ls5/d;


# direct methods
.method public constructor <init>(Ls5/d;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    iput-object p1, p0, Ls5/d$c;->a:Ls5/d;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method public static synthetic a(Landroid/content/Intent;Landroid/content/Context;Ls5/d;)V
    .locals 0

    invoke-static {p0, p1, p2}, Ls5/d$c;->b(Landroid/content/Intent;Landroid/content/Context;Ls5/d;)V

    return-void
.end method

.method private static final b(Landroid/content/Intent;Landroid/content/Context;Ls5/d;)V
    .locals 2

    const-string v0, "$intent"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$context"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "this$0"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "android.content.pm.extra.STATUS"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const-string p2, "android.intent.extra.INTENT"

    invoke-virtual {p0, p2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Landroid/content/Intent;

    invoke-virtual {p1, p0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_0
    const/16 p1, -0x3e8

    const-string v1, "android.content.pm.extra.LEGACY_STATUS"

    invoke-virtual {p0, v1, p1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result p0

    invoke-virtual {p2, v0, p0}, Ls5/l;->f(II)V

    :goto_0
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    const-string v0, "context"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "intent"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object v0

    iget-object v1, p0, Ls5/d$c;->a:Ls5/d;

    new-instance v2, Ls5/e;

    invoke-direct {v2, p2, p1, v1}, Ls5/e;-><init>(Landroid/content/Intent;Landroid/content/Context;Ls5/d;)V

    invoke-virtual {v0, v2}, Lf6/z;->g(Ljava/lang/Runnable;)V

    return-void
.end method
