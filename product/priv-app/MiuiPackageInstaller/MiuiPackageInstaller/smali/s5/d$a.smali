.class public Ls5/d$a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ls5/d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private b:Landroid/net/Uri;

.field private c:Landroid/net/Uri;

.field private d:Landroid/net/Uri;

.field private e:I

.field private f:Z

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/Integer;

.field private j:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const-string v0, "mContext"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Ls5/d$a;->a:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public a()Ls5/l;
    .locals 2

    new-instance v0, Ls5/d;

    iget-object v1, p0, Ls5/d$a;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Ls5/d;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Ls5/d$a;->b:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ls5/d;->H(Landroid/net/Uri;)V

    iget-object v1, p0, Ls5/d$a;->c:Landroid/net/Uri;

    invoke-static {v0, v1}, Ls5/d;->y(Ls5/d;Landroid/net/Uri;)V

    iget-object v1, p0, Ls5/d$a;->d:Landroid/net/Uri;

    invoke-static {v0, v1}, Ls5/d;->x(Ls5/d;Landroid/net/Uri;)V

    iget v1, p0, Ls5/d$a;->e:I

    invoke-static {v0, v1}, Ls5/d;->w(Ls5/d;I)V

    iget-boolean v1, p0, Ls5/d$a;->f:Z

    invoke-virtual {v0, v1}, Ls5/d;->G(Z)V

    iget-object v1, p0, Ls5/d$a;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ls5/d;->F(Ljava/lang/String;)V

    iget-object v1, p0, Ls5/d$a;->h:Ljava/lang/String;

    invoke-static {v0, v1}, Ls5/d;->s(Ls5/d;Ljava/lang/String;)V

    iget-object v1, p0, Ls5/d$a;->i:Ljava/lang/Integer;

    invoke-static {v0, v1}, Ls5/d;->t(Ls5/d;Ljava/lang/Integer;)V

    iget-object v1, p0, Ls5/d$a;->j:Ljava/lang/String;

    invoke-static {v0, v1}, Ls5/d;->v(Ls5/d;Ljava/lang/String;)V

    return-object v0
.end method

.method public final b(Ljava/lang/String;)Ls5/d$a;
    .locals 0

    iput-object p1, p0, Ls5/d$a;->h:Ljava/lang/String;

    return-object p0
.end method

.method public final c(I)Ls5/d$a;
    .locals 0

    iput p1, p0, Ls5/d$a;->e:I

    return-object p0
.end method

.method public final d(Landroid/net/Uri;)Ls5/d$a;
    .locals 0

    iput-object p1, p0, Ls5/d$a;->d:Landroid/net/Uri;

    return-object p0
.end method

.method public final e(Ljava/lang/String;)Ls5/d$a;
    .locals 0

    iput-object p1, p0, Ls5/d$a;->g:Ljava/lang/String;

    return-object p0
.end method

.method public final f(Landroid/net/Uri;)Ls5/d$a;
    .locals 0

    iput-object p1, p0, Ls5/d$a;->c:Landroid/net/Uri;

    return-object p0
.end method

.method public final g(Z)Ls5/d$a;
    .locals 0

    iput-boolean p1, p0, Ls5/d$a;->f:Z

    return-object p0
.end method

.method public final h(Landroid/net/Uri;)Ls5/d$a;
    .locals 0

    iput-object p1, p0, Ls5/d$a;->b:Landroid/net/Uri;

    return-object p0
.end method

.method public final i(Ljava/lang/String;)Ls5/d$a;
    .locals 0

    iput-object p1, p0, Ls5/d$a;->j:Ljava/lang/String;

    return-object p0
.end method
