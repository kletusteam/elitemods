.class public final Ls5/g;
.super Ls5/l;

# interfaces
.implements Ls5/l$c;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ls5/g$a;
    }
.end annotation


# instance fields
.field private h:Ls5/d;

.field private i:Lcom/miui/packageInstaller/model/IncrementPackageInfo;

.field private j:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ls5/d;)V
    .locals 1

    const-string v0, "mContext"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mApkInstaller"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Ls5/l;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Ls5/g;->h:Ls5/d;

    invoke-virtual {p2, p0}, Ls5/l;->m(Ls5/l$c;)V

    return-void
.end method

.method public static synthetic o(Ls5/g;)V
    .locals 0

    invoke-static {p0}, Ls5/g;->w(Ls5/g;)V

    return-void
.end method

.method public static final synthetic p(Ls5/g;)Ls5/d;
    .locals 0

    iget-object p0, p0, Ls5/g;->h:Ls5/d;

    return-object p0
.end method

.method public static final synthetic r(Ls5/g;I)I
    .locals 0

    invoke-direct {p0, p1}, Ls5/g;->x(I)I

    move-result p0

    return p0
.end method

.method private final v(Lcom/miui/packageInstaller/model/IncrementPackageInfo;)V
    .locals 4

    new-instance v0, Ln7/b;

    invoke-virtual {p0}, Ls5/l;->e()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Ls5/g;->j:Landroid/net/Uri;

    invoke-static {v2}, Lm8/i;->c(Ljava/lang/Object;)V

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ls5/g$b;

    invoke-direct {v3, p0}, Ls5/g$b;-><init>(Ls5/g;)V

    invoke-direct {v0, v1, v2, p1, v3}, Ln7/b;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/miui/packageInstaller/model/IncrementPackageInfo;Ln7/a;)V

    invoke-virtual {v0}, Ln7/b;->d()V

    return-void
.end method

.method private static final w(Ls5/g;)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Ls5/l;->b:Ls5/l$c;

    if-eqz v0, :cond_0

    invoke-static {v0}, Lm8/i;->c(Ljava/lang/Object;)V

    invoke-interface {v0, p0}, Ls5/l$c;->u(Ls5/l;)V

    :cond_0
    return-void
.end method

.method private final x(I)I
    .locals 1

    const v0, 0x13881

    add-int/2addr p1, v0

    return p1
.end method


# virtual methods
.method public C(Ls5/l;II)V
    .locals 0

    invoke-virtual {p0, p2, p3}, Ls5/l;->f(II)V

    return-void
.end method

.method public n()V
    .locals 2

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object v0

    new-instance v1, Ls5/f;

    invoke-direct {v1, p0}, Ls5/f;-><init>(Ls5/g;)V

    invoke-virtual {v0, v1}, Lf6/z;->e(Ljava/lang/Runnable;)V

    iget-object v0, p0, Ls5/g;->i:Lcom/miui/packageInstaller/model/IncrementPackageInfo;

    invoke-direct {p0, v0}, Ls5/g;->v(Lcom/miui/packageInstaller/model/IncrementPackageInfo;)V

    return-void
.end method

.method public q(Ls5/l;II)V
    .locals 0

    invoke-virtual {p0, p2, p3}, Ls5/l;->i(II)V

    return-void
.end method

.method public final s(Landroid/net/Uri;)V
    .locals 0

    iput-object p1, p0, Ls5/g;->j:Landroid/net/Uri;

    return-void
.end method

.method public final t(Lcom/miui/packageInstaller/model/IncrementPackageInfo;)V
    .locals 0

    iput-object p1, p0, Ls5/g;->i:Lcom/miui/packageInstaller/model/IncrementPackageInfo;

    return-void
.end method

.method public u(Ls5/l;)V
    .locals 0

    return-void
.end method
