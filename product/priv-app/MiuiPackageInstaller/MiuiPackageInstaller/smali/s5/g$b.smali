.class public final Ls5/g$b;
.super Ljava/lang/Object;

# interfaces
.implements Ln7/a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Ls5/g;->v(Lcom/miui/packageInstaller/model/IncrementPackageInfo;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field final synthetic a:Ls5/g;


# direct methods
.method constructor <init>(Ls5/g;)V
    .locals 0

    iput-object p1, p0, Ls5/g$b;->a:Ls5/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 1

    iget-object p1, p0, Ls5/g$b;->a:Ls5/g;

    const/16 v0, 0x11

    invoke-virtual {p1, v0}, Ls5/l;->h(I)V

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 2

    const-string v0, "targetPath"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Ls5/g$b;->a:Ls5/g;

    invoke-static {v0}, Ls5/g;->p(Ls5/g;)Ls5/d;

    move-result-object v0

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object p1

    invoke-virtual {v0, p1}, Ls5/d;->H(Landroid/net/Uri;)V

    iget-object p1, p0, Ls5/g$b;->a:Ls5/g;

    invoke-static {p1}, Ls5/g;->p(Ls5/g;)Ls5/d;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ls5/d;->G(Z)V

    iget-object p1, p0, Ls5/g$b;->a:Ls5/g;

    invoke-static {p1}, Ls5/g;->p(Ls5/g;)Ls5/d;

    move-result-object p1

    invoke-virtual {p1}, Ls5/d;->n()V

    return-void
.end method

.method public c()V
    .locals 3

    iget-object v0, p0, Ls5/g$b;->a:Ls5/g;

    const/4 v1, 0x1

    const v2, 0x13880

    invoke-virtual {v0, v1, v2}, Ls5/l;->f(II)V

    return-void
.end method

.method public d(Ljava/lang/String;)V
    .locals 1

    const-string v0, "filePath"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p0, Ls5/g$b;->a:Ls5/g;

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Ls5/l;->h(I)V

    return-void
.end method

.method public e()V
    .locals 2

    iget-object v0, p0, Ls5/g$b;->a:Ls5/g;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ls5/l;->h(I)V

    return-void
.end method

.method public f(JJ)V
    .locals 1

    iget-object v0, p0, Ls5/g$b;->a:Ls5/g;

    long-to-float p1, p1

    long-to-float p2, p3

    div-float/2addr p1, p2

    float-to-int p1, p1

    mul-int/lit8 p1, p1, 0x64

    const/4 p2, 0x2

    invoke-virtual {v0, p2, p1}, Ls5/l;->i(II)V

    return-void
.end method

.method public g(I)V
    .locals 2

    iget-object v0, p0, Ls5/g$b;->a:Ls5/g;

    invoke-static {v0, p1}, Ls5/g;->r(Ls5/g;I)I

    move-result p1

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Ls5/l;->f(II)V

    return-void
.end method

.method public h(I)V
    .locals 2

    iget-object p1, p0, Ls5/g$b;->a:Ls5/g;

    const/4 v0, 0x1

    const v1, 0x138e4

    invoke-virtual {p1, v0, v1}, Ls5/l;->f(II)V

    return-void
.end method
