.class public final Ls5/n$a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ls5/n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const-string v0, "mContext"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Ls5/n$a;->a:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public final a()Ls5/l;
    .locals 2

    new-instance v0, Ls5/n;

    iget-object v1, p0, Ls5/n$a;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Ls5/n;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Ls5/n$a;->b:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ls5/n;->B(Ljava/lang/String;)V

    iget-object v1, p0, Ls5/n$a;->c:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ls5/n;->D(Ljava/lang/String;)V

    iget-object v1, p0, Ls5/n$a;->d:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ls5/n;->E(Ljava/lang/String;)V

    return-object v0
.end method

.method public final b(Ljava/lang/String;)Ls5/n$a;
    .locals 1

    const-string v0, "marketDeeplink"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Ls5/n$a;->b:Ljava/lang/String;

    return-object p0
.end method

.method public final c(Ljava/lang/String;)Ls5/n$a;
    .locals 1

    const-string v0, "packageName"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Ls5/n$a;->c:Ljava/lang/String;

    return-object p0
.end method

.method public final d(Ljava/lang/String;)Ls5/n$a;
    .locals 1

    const-string v0, "sourcePackage"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Ls5/n$a;->d:Ljava/lang/String;

    return-object p0
.end method
