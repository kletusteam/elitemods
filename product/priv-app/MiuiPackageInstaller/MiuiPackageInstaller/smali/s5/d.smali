.class public Ls5/d;
.super Ls5/l;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ls5/d$c;,
        Ls5/d$b;,
        Ls5/d$a;
    }
.end annotation


# static fields
.field public static final r:Ls5/d$b;


# instance fields
.field private h:Landroid/net/Uri;

.field private i:Landroid/net/Uri;

.field private j:Landroid/net/Uri;

.field private k:I

.field private l:Z

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private final o:Ls5/d$c;

.field private p:Ljava/lang/Integer;

.field private q:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ls5/d$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ls5/d$b;-><init>(Lm8/g;)V

    sput-object v0, Ls5/d;->r:Ls5/d$b;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    const-string v0, "mContext"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Ls5/l;-><init>(Landroid/content/Context;)V

    new-instance v0, Ls5/d$c;

    invoke-direct {v0, p0}, Ls5/d$c;-><init>(Ls5/d;)V

    iput-object v0, p0, Ls5/d;->o:Ls5/d$c;

    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "com.android.packageinstaller.ACTION_INSTALL_COMMIT"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p0, Ls5/l;->c:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "android.permission.INSTALL_PACKAGES"

    const/4 v3, 0x0

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    return-void
.end method

.method private final A(Landroid/content/pm/PackageManager;Landroid/content/pm/PackageInstaller$SessionParams;Ljava/io/File;)V
    .locals 9

    invoke-virtual {p1}, Landroid/content/pm/PackageManager;->getPackageInstaller()Landroid/content/pm/PackageInstaller;

    move-result-object p1

    const-string v0, "pm.packageInstaller"

    invoke-static {p1, v0}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p1, p2}, Landroid/content/pm/PackageInstaller;->createSession(Landroid/content/pm/PackageInstaller$SessionParams;)I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/content/pm/PackageInstaller;->openSession(I)Landroid/content/pm/PackageInstaller$Session;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_4

    :try_start_1
    iget-boolean v2, p0, Ls5/d;->l:Z

    if-eqz v2, :cond_0

    invoke-virtual {p3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v2}, Lcom/android/packageinstaller/utils/t;->d(Landroid/content/pm/PackageInstaller$Session;Ljava/lang/String;)Z

    move-result v2

    xor-int/2addr v2, v0

    goto :goto_0

    :cond_0
    move v2, v0

    :goto_0
    if-eqz v2, :cond_1

    invoke-virtual {p3}, Ljava/io/File;->length()J

    move-result-wide v6

    new-instance v8, Ljava/io/FileInputStream;

    invoke-direct {v8, p3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_5

    :try_start_2
    const-string v3, "PackageInstaller"

    const-wide/16 v4, 0x0

    move-object v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/content/pm/PackageInstaller$Session;->openWrite(Ljava/lang/String;JJ)Ljava/io/OutputStream;

    move-result-object p3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    :try_start_3
    new-instance v2, Lcom/android/packageinstaller/utils/o;

    invoke-direct {v2, v8, p3}, Lcom/android/packageinstaller/utils/o;-><init>(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    invoke-virtual {v2}, Lcom/android/packageinstaller/utils/o;->a()V

    invoke-virtual {p1, p3}, Landroid/content/pm/PackageInstaller$Session;->fsync(Ljava/io/OutputStream;)V

    sget-object v2, La8/v;->a:La8/v;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    invoke-static {p3, v1}, Lj8/a;->a(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    :try_start_5
    invoke-static {v8, v1}, Lj8/a;->a(Ljava/io/Closeable;Ljava/lang/Throwable;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_5

    goto :goto_1

    :catchall_0
    move-exception p2

    :try_start_6
    throw p2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :catchall_1
    move-exception v2

    :try_start_7
    invoke-static {p3, p2}, Lj8/a;->a(Ljava/io/Closeable;Ljava/lang/Throwable;)V

    throw v2
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    :catchall_2
    move-exception p2

    :try_start_8
    throw p2
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    :catchall_3
    move-exception p3

    :try_start_9
    invoke-static {v8, p2}, Lj8/a;->a(Ljava/io/Closeable;Ljava/lang/Throwable;)V

    throw p3

    :cond_1
    :goto_1
    new-instance p3, Landroid/content/Intent;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "com.android.packageinstaller.ACTION_INSTALL_COMMIT"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p0, Ls5/l;->c:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p3, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x1f

    if-lt v2, v3, :cond_2

    const/high16 v2, 0xa000000

    goto :goto_2

    :cond_2
    const/high16 v2, 0x8000000

    :goto_2
    invoke-virtual {p0}, Ls5/l;->e()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, p2, p3, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object p2

    invoke-virtual {p2}, Landroid/app/PendingIntent;->getIntentSender()Landroid/content/IntentSender;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/content/pm/PackageInstaller$Session;->commit(Landroid/content/IntentSender;)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_5

    goto :goto_4

    :catch_0
    move-exception p2

    goto :goto_3

    :catchall_4
    move-exception p2

    goto :goto_5

    :catch_1
    move-exception p2

    move-object p1, v1

    :goto_3
    :try_start_a
    const-string p3, "Installer"

    const-string v2, "doPackageStage"

    invoke-static {p3, v2, p2}, Lf6/o;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-virtual {p2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p2

    const/16 p3, -0x3e7

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    invoke-static {p2}, Lm8/i;->c(Ljava/lang/Object;)V

    const-string v2, "Failed to free"

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-static {p2, v2, v4, v3, v1}, Lu8/g;->v(Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "Failed to allocate"

    invoke-static {p2, v2, v4, v3, v1}, Lu8/g;->v(Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_4

    :cond_3
    const/4 p3, -0x4

    :cond_4
    if-eqz p1, :cond_5

    invoke-virtual {p1}, Landroid/content/pm/PackageInstaller$Session;->abandon()V

    :cond_5
    invoke-virtual {p0, v0, p3}, Ls5/l;->f(II)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_5

    :goto_4
    invoke-static {p1}, Lcom/android/packageinstaller/utils/k;->a(Ljava/lang/AutoCloseable;)V

    return-void

    :catchall_5
    move-exception p2

    move-object v1, p1

    :goto_5
    invoke-static {v1}, Lcom/android/packageinstaller/utils/k;->a(Ljava/lang/AutoCloseable;)V

    throw p2
.end method

.method private final B(Landroid/content/pm/PackageManager;)V
    .locals 4

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Ls5/l;->h(I)V

    new-instance v1, Landroid/content/pm/PackageInstaller$SessionParams;

    invoke-direct {v1, v0}, Landroid/content/pm/PackageInstaller$SessionParams;-><init>(I)V

    iget-object v0, p0, Ls5/d;->i:Landroid/net/Uri;

    invoke-virtual {v1, v0}, Landroid/content/pm/PackageInstaller$SessionParams;->setReferrerUri(Landroid/net/Uri;)V

    iget-object v0, p0, Ls5/d;->j:Landroid/net/Uri;

    invoke-virtual {v1, v0}, Landroid/content/pm/PackageInstaller$SessionParams;->setOriginatingUri(Landroid/net/Uri;)V

    iget v0, p0, Ls5/d;->k:I

    invoke-virtual {v1, v0}, Landroid/content/pm/PackageInstaller$SessionParams;->setOriginatingUid(I)V

    new-instance v0, Ljava/io/File;

    iget-object v2, p0, Ls5/d;->h:Landroid/net/Uri;

    invoke-static {v2}, Lm8/i;->c(Ljava/lang/Object;)V

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/4 v2, 0x0

    :try_start_0
    invoke-static {v0, v2}, Lcom/android/packageinstaller/compat/PackageParserCompat;->parsePackageLite(Ljava/io/File;I)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Lcom/android/packageinstaller/compat/PackageParserCompat;->installLocation(Ljava/lang/Object;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageInstaller$SessionParams;->setInstallLocation(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cannot parse package "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v3, ". Assuming defaults."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Installer"

    invoke-static {v3, v2}, Lf6/o;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    :goto_0
    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object v2

    new-instance v3, Ls5/c;

    invoke-direct {v3, p0, p1, v1, v0}, Ls5/c;-><init>(Ls5/d;Landroid/content/pm/PackageManager;Landroid/content/pm/PackageInstaller$SessionParams;Ljava/io/File;)V

    invoke-virtual {v2, v3}, Lf6/z;->g(Ljava/lang/Runnable;)V

    return-void
.end method

.method private static final D(Ls5/d;Landroid/content/pm/PackageManager;Landroid/content/pm/PackageInstaller$SessionParams;Ljava/io/File;)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$pm"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$params"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$file"

    invoke-static {p3, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2, p3}, Ls5/d;->A(Landroid/content/pm/PackageManager;Landroid/content/pm/PackageInstaller$SessionParams;Ljava/io/File;)V

    return-void
.end method

.method private final E(Landroid/content/pm/PackageManager;)V
    .locals 2

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Ls5/d;->m:Ljava/lang/String;

    invoke-static {p1, v1}, Lcom/android/packageinstaller/compat/PackageManagerCompat;->installExistingPackage(Landroid/content/pm/PackageManager;Ljava/lang/String;)V

    invoke-virtual {p0, v0, v0}, Ls5/l;->f(II)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const/4 p1, 0x1

    invoke-virtual {p0, p1, v0}, Ls5/l;->f(II)V

    :goto_0
    return-void
.end method

.method private static final I(Ls5/d;)V
    .locals 1

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Ls5/l;->b:Ls5/l$c;

    if-eqz v0, :cond_0

    invoke-static {v0}, Lm8/i;->c(Ljava/lang/Object;)V

    invoke-interface {v0, p0}, Ls5/l$c;->u(Ls5/l;)V

    :cond_0
    return-void
.end method

.method private static final J(Ls5/d;)V
    .locals 3

    const-string v0, "this$0"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Ls5/l;->b:Ls5/l$c;

    if-eqz v0, :cond_0

    invoke-static {v0}, Lm8/i;->c(Ljava/lang/Object;)V

    const/4 v1, 0x1

    const/16 v2, -0x2711

    invoke-interface {v0, p0, v1, v2}, Ls5/l$c;->C(Ls5/l;II)V

    :cond_0
    return-void
.end method

.method public static synthetic o(Ls5/d;)V
    .locals 0

    invoke-static {p0}, Ls5/d;->I(Ls5/d;)V

    return-void
.end method

.method public static synthetic p(Ls5/d;Landroid/content/pm/PackageManager;Landroid/content/pm/PackageInstaller$SessionParams;Ljava/io/File;)V
    .locals 0

    invoke-static {p0, p1, p2, p3}, Ls5/d;->D(Ls5/d;Landroid/content/pm/PackageManager;Landroid/content/pm/PackageInstaller$SessionParams;Ljava/io/File;)V

    return-void
.end method

.method public static synthetic r(Ls5/d;)V
    .locals 0

    invoke-static {p0}, Ls5/d;->J(Ls5/d;)V

    return-void
.end method

.method public static final synthetic s(Ls5/d;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Ls5/d;->n:Ljava/lang/String;

    return-void
.end method

.method public static final synthetic t(Ls5/d;Ljava/lang/Integer;)V
    .locals 0

    iput-object p1, p0, Ls5/d;->p:Ljava/lang/Integer;

    return-void
.end method

.method public static final synthetic v(Ls5/d;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Ls5/d;->q:Ljava/lang/String;

    return-void
.end method

.method public static final synthetic w(Ls5/d;I)V
    .locals 0

    iput p1, p0, Ls5/d;->k:I

    return-void
.end method

.method public static final synthetic x(Ls5/d;Landroid/net/Uri;)V
    .locals 0

    iput-object p1, p0, Ls5/d;->j:Landroid/net/Uri;

    return-void
.end method

.method public static final synthetic y(Ls5/d;Landroid/net/Uri;)V
    .locals 0

    iput-object p1, p0, Ls5/d;->i:Landroid/net/Uri;

    return-void
.end method


# virtual methods
.method public final F(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Ls5/d;->m:Ljava/lang/String;

    return-void
.end method

.method public final G(Z)V
    .locals 0

    iput-boolean p1, p0, Ls5/d;->l:Z

    return-void
.end method

.method public final H(Landroid/net/Uri;)V
    .locals 0

    iput-object p1, p0, Ls5/d;->h:Landroid/net/Uri;

    return-void
.end method

.method public d()V
    .locals 2

    const/4 v0, 0x1

    const/16 v1, -0x309

    invoke-virtual {p0, v0, v1}, Ls5/l;->f(II)V

    return-void
.end method

.method public l()V
    .locals 2

    invoke-super {p0}, Ls5/l;->l()V

    invoke-virtual {p0}, Ls5/l;->e()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Ls5/d;->o:Ls5/d$c;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method public n()V
    .locals 2

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object v0

    new-instance v1, Ls5/a;

    invoke-direct {v1, p0}, Ls5/a;-><init>(Ls5/d;)V

    invoke-virtual {v0, v1}, Lf6/z;->e(Ljava/lang/Runnable;)V

    iget-object v0, p0, Ls5/d;->m:Ljava/lang/String;

    const-string v1, "com.android.camera"

    invoke-static {v1, v0}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Ls5/d;->q:Ljava/lang/String;

    const-string v1, "4.3.004660.0"

    invoke-static {v1, v0}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Ls5/d;->q:Ljava/lang/String;

    const-string v1, "4.3.004700.1"

    invoke-static {v1, v0}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object v0

    new-instance v1, Ls5/b;

    invoke-direct {v1, p0}, Ls5/b;-><init>(Ls5/d;)V

    invoke-virtual {v0, v1}, Lf6/z;->e(Ljava/lang/Runnable;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Ls5/d;->z()V

    :goto_0
    return-void
.end method

.method protected final z()V
    .locals 4

    invoke-virtual {p0}, Ls5/l;->e()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v1, p0, Ls5/d;->j:Landroid/net/Uri;

    invoke-static {v1}, Lm8/i;->c(Ljava/lang/Object;)V

    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    const-string v2, "package"

    invoke-static {v2, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    const-string v2, "packageManager"

    if-eqz v1, :cond_0

    invoke-static {v0, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Ls5/d;->E(Landroid/content/pm/PackageManager;)V

    goto :goto_0

    :cond_0
    const-string v1, "Installer"

    const-string v3, "start normal install"

    invoke-static {v1, v3}, Lf6/o;->e(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    invoke-static {v0, v2}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Ls5/d;->B(Landroid/content/pm/PackageManager;)V

    :goto_0
    return-void
.end method
