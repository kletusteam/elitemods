.class final Ls5/n$c;
.super Landroid/content/BroadcastReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ls5/n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "c"
.end annotation


# instance fields
.field final synthetic a:Ls5/n;


# direct methods
.method public constructor <init>(Ls5/n;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    iput-object p1, p0, Ls5/n$c;->a:Ls5/n;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7

    const-string v0, "context"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "intent"

    invoke-static {p2, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "errorCode"

    const/4 v0, 0x0

    invoke-virtual {p2, p1, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result p1

    const-string v1, "packageName"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Ls5/n$c;->a:Ls5/n;

    invoke-virtual {v2}, Ls5/n;->w()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "rPackageName = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "   packageName = "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, p0, Ls5/n$c;->a:Ls5/n;

    invoke-virtual {v6}, Ls5/n;->t()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "  statusCode = "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lf6/o;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    iget-object v2, p0, Ls5/n$c;->a:Ls5/n;

    invoke-virtual {v2}, Ls5/n;->t()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    const/4 v6, 0x0

    invoke-static {v1, v2, v0, v3, v6}, Lu8/g;->k(Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    return-void

    :cond_0
    iget-object v2, p0, Ls5/n$c;->a:Ls5/n;

    invoke-virtual {v2}, Ls5/n;->r()Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Ls5/n$c;->a:Ls5/n;

    invoke-virtual {v3}, Ls5/n;->x()Ljava/lang/Runnable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    const v2, -0xea63

    const/4 v3, 0x1

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    goto/16 :goto_2

    :pswitch_1
    const-string p1, "progress"

    invoke-virtual {p2, p1, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result p1

    const-string v2, "status"

    invoke-virtual {p2, v2, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result p2

    iget-object v0, p0, Ls5/n$c;->a:Ls5/n;

    invoke-virtual {v0}, Ls5/n;->w()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Ls5/n$c;->a:Ls5/n;

    invoke-virtual {v1}, Ls5/n;->t()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "  progressStatus = "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lf6/o;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    const/4 v0, -0x5

    if-eq p2, v0, :cond_3

    const/4 v0, -0x4

    if-eq p2, v0, :cond_2

    const/4 v0, -0x3

    if-eq p2, v0, :cond_1

    const/4 v0, -0x2

    if-eq p2, v0, :cond_3

    const/4 v0, -0x1

    if-eq p2, v0, :cond_3

    goto :goto_2

    :cond_1
    iget-object p1, p0, Ls5/n$c;->a:Ls5/n;

    const/16 p2, 0x9

    invoke-virtual {p1, p2}, Ls5/l;->h(I)V

    goto :goto_2

    :cond_2
    iget-object p2, p0, Ls5/n$c;->a:Ls5/n;

    invoke-virtual {p2, v3, p1}, Ls5/l;->i(II)V

    goto :goto_2

    :cond_3
    iget-object p2, p0, Ls5/n$c;->a:Ls5/n;

    const/4 v0, 0x5

    invoke-virtual {p2, v0, p1}, Ls5/l;->i(II)V

    goto :goto_2

    :pswitch_2
    iget-object p1, p0, Ls5/n$c;->a:Ls5/n;

    invoke-static {p1, v0, v0}, Ls5/n;->p(Ls5/n;II)V

    goto :goto_2

    :pswitch_3
    iget-object p1, p0, Ls5/n$c;->a:Ls5/n;

    const/4 p2, 0x6

    goto :goto_0

    :pswitch_4
    iget-object p1, p0, Ls5/n$c;->a:Ls5/n;

    const/16 p2, 0xc

    goto :goto_0

    :pswitch_5
    iget-object p1, p0, Ls5/n$c;->a:Ls5/n;

    const/4 p2, 0x4

    :goto_0
    invoke-virtual {p1, p2, v0}, Ls5/l;->i(II)V

    goto :goto_2

    :pswitch_6
    iget-object p1, p0, Ls5/n$c;->a:Ls5/n;

    const p2, -0xea61

    goto :goto_1

    :pswitch_7
    iget-object p1, p0, Ls5/n$c;->a:Ls5/n;

    const p2, -0xea62

    goto :goto_1

    :pswitch_8
    iget-object p1, p0, Ls5/n$c;->a:Ls5/n;

    invoke-static {p1, v3, v2}, Ls5/n;->p(Ls5/n;II)V

    goto :goto_2

    :pswitch_9
    iget-object p1, p0, Ls5/n$c;->a:Ls5/n;

    const p2, -0xea66

    invoke-static {p1, v0, p2}, Ls5/n;->p(Ls5/n;II)V

    goto :goto_2

    :pswitch_a
    iget-object p1, p0, Ls5/n$c;->a:Ls5/n;

    const p2, -0xea64

    goto :goto_1

    :pswitch_b
    iget-object p1, p0, Ls5/n$c;->a:Ls5/n;

    const p2, -0xea65

    :goto_1
    invoke-static {p1, v3, p2}, Ls5/n;->p(Ls5/n;II)V

    :goto_2
    return-void

    nop

    :pswitch_data_0
    .packed-switch -0x8
        :pswitch_b
        :pswitch_0
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_8
        :pswitch_0
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
