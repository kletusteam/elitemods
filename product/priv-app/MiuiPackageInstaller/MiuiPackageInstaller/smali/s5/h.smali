.class public Ls5/h;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ls5/h$a;
    }
.end annotation


# instance fields
.field private a:Landroid/content/Context;

.field private b:Z

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;ZLjava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Ls5/h;->a:Landroid/content/Context;

    iput-boolean p2, p0, Ls5/h;->b:Z

    iput-object p3, p0, Ls5/h;->c:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public a(ILjava/lang/String;Z)Ls5/h$a;
    .locals 8

    const/16 v0, -0x2711

    if-eq p1, v0, :cond_12

    const/16 v0, -0x309

    if-eq p1, v0, :cond_11

    const/16 v0, -0x7c

    const v1, 0x7f110174

    const v2, 0x7f110173

    if-eq p1, v0, :cond_10

    const/16 v0, -0x33

    const v3, 0x7f1101cd

    if-eq p1, v0, :cond_f

    const/16 v0, -0x16

    if-eq p1, v0, :cond_e

    const/16 v0, -0x10

    if-eq p1, v0, :cond_d

    const/4 v0, -0x7

    const v4, 0x7f1101c9

    if-eq p1, v0, :cond_c

    const/16 v0, -0x71

    if-eq p1, v0, :cond_a

    const/16 p2, -0x70

    if-eq p1, p2, :cond_10

    const/16 p2, -0x1d

    if-eq p1, p2, :cond_f

    const/16 p2, -0x1c

    if-eq p1, p2, :cond_f

    const/16 p2, -0x1a

    const p3, 0x7f1101ce

    const/high16 v0, 0x800000

    const v1, 0x7f1101d6

    const v2, 0x402000

    const-string v5, "getPackageInfo"

    const-string v6, "FailHandler"

    if-eq p1, p2, :cond_7

    const/16 p2, -0x19

    if-eq p1, p2, :cond_4

    const/4 p2, -0x4

    if-eq p1, p2, :cond_3

    const/4 p2, -0x3

    if-eq p1, p2, :cond_2

    const/4 p2, -0x2

    const p3, 0x7f1101ca

    if-eq p1, p2, :cond_1

    const/4 p2, -0x1

    if-eq p1, p2, :cond_0

    const p2, 0x7f1101bf

    const v0, 0x7f1101d2

    packed-switch p1, :pswitch_data_0

    iget-object p2, p0, Ls5/h;->a:Landroid/content/Context;

    packed-switch p1, :pswitch_data_1

    packed-switch p1, :pswitch_data_2

    const p3, 0x7f1101d8

    invoke-virtual {p2, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p2

    const-string p3, ""

    goto/16 :goto_7

    :pswitch_0
    const p3, 0x7f1101c5

    goto/16 :goto_6

    :pswitch_1
    const p3, 0x7f1101bb

    :goto_0
    invoke-virtual {p2, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p2

    iget-object p3, p0, Ls5/h;->a:Landroid/content/Context;

    invoke-virtual {p3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p3

    goto/16 :goto_7

    :pswitch_2
    const p3, 0x7f1101c4

    goto/16 :goto_6

    :pswitch_3
    const v0, 0x7f1101c7

    :goto_1
    invoke-virtual {p2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p2

    iget-object v0, p0, Ls5/h;->a:Landroid/content/Context;

    invoke-virtual {v0, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p3

    goto/16 :goto_7

    :pswitch_4
    iget-object p2, p0, Ls5/h;->a:Landroid/content/Context;

    const p3, 0x7f1101c1

    goto :goto_2

    :pswitch_5
    iget-object p2, p0, Ls5/h;->a:Landroid/content/Context;

    const p3, 0x7f1101c0

    goto :goto_2

    :pswitch_6
    iget-object p2, p0, Ls5/h;->a:Landroid/content/Context;

    const p3, 0x7f1101c6

    goto :goto_2

    :pswitch_7
    iget-object p3, p0, Ls5/h;->a:Landroid/content/Context;

    invoke-virtual {p3, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto :goto_3

    :pswitch_8
    iget-object p3, p0, Ls5/h;->a:Landroid/content/Context;

    invoke-virtual {p3, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p2

    iget-object p3, p0, Ls5/h;->a:Landroid/content/Context;

    const v0, 0x7f1101d1

    goto :goto_4

    :pswitch_9
    iget-object p2, p0, Ls5/h;->a:Landroid/content/Context;

    const p3, 0x7f1101c2

    :goto_2
    invoke-virtual {p2, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p2

    :goto_3
    iget-object p3, p0, Ls5/h;->a:Landroid/content/Context;

    :goto_4
    invoke-virtual {p3, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p3

    goto/16 :goto_7

    :cond_0
    iget-object p2, p0, Ls5/h;->a:Landroid/content/Context;

    const p3, 0x7f1101ba

    goto :goto_0

    :cond_1
    iget-object p2, p0, Ls5/h;->a:Landroid/content/Context;

    const v0, 0x7f1101bd

    goto :goto_1

    :cond_2
    iget-object p2, p0, Ls5/h;->a:Landroid/content/Context;

    const p3, 0x7f1101be

    invoke-virtual {p2, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p2

    iget-object p3, p0, Ls5/h;->a:Landroid/content/Context;

    const v0, 0x7f1101cb

    goto :goto_4

    :cond_3
    iget-object p2, p0, Ls5/h;->a:Landroid/content/Context;

    const p3, 0x7f1101bc

    invoke-virtual {p2, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p2

    iget-object p3, p0, Ls5/h;->a:Landroid/content/Context;

    const v0, 0x7f1101cc

    goto :goto_4

    :cond_4
    iget-object p2, p0, Ls5/h;->a:Landroid/content/Context;

    const v3, 0x7f1101d5

    invoke-virtual {p2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p2

    iget-object v3, p0, Ls5/h;->a:Landroid/content/Context;

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/android/packageinstaller/compat/UserHandleCompat;->myUserId()I

    move-result v4

    if-nez v4, :cond_6

    :try_start_0
    iget-object v4, p0, Ls5/h;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    iget-object v7, p0, Ls5/h;->c:Ljava/lang/String;

    invoke-virtual {v4, v7, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    if-eqz v2, :cond_5

    iget-object v2, v2, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v2, v2, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/2addr v0, v2

    if-nez v0, :cond_6

    :cond_5
    iget-object v0, p0, Ls5/h;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p2

    iget-object v0, p0, Ls5/h;->a:Landroid/content/Context;

    invoke-virtual {v0, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p3
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_7

    :catch_0
    move-exception p3

    invoke-static {v6, v5, p3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_6
    move-object p3, v3

    goto/16 :goto_7

    :cond_7
    iget-object p2, p0, Ls5/h;->a:Landroid/content/Context;

    const v3, 0x7f1101c8

    invoke-virtual {p2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p2

    iget-boolean v3, p0, Ls5/h;->b:Z

    if-eqz v3, :cond_8

    iget-object v3, p0, Ls5/h;->a:Landroid/content/Context;

    const v4, 0x7f1101cf

    goto :goto_5

    :cond_8
    iget-object v3, p0, Ls5/h;->a:Landroid/content/Context;

    const v4, 0x7f1101d0

    :goto_5
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/android/packageinstaller/compat/UserHandleCompat;->myUserId()I

    move-result v4

    if-nez v4, :cond_6

    :try_start_1
    iget-object v4, p0, Ls5/h;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    iget-object v7, p0, Ls5/h;->c:Ljava/lang/String;

    invoke-virtual {v4, v7, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    if-eqz v2, :cond_9

    iget-object v2, v2, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v2, v2, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/2addr v0, v2

    if-nez v0, :cond_6

    :cond_9
    iget-object v0, p0, Ls5/h;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p2

    iget-object v0, p0, Ls5/h;->a:Landroid/content/Context;

    invoke-virtual {v0, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p3
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_7

    :cond_a
    sget-boolean v0, Lcom/android/packageinstaller/utils/g;->g:Z

    if-nez v0, :cond_10

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_10

    const-string v0, "armeabi-v7a"

    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_10

    const-string v0, "armeabi"

    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p2

    if-nez p2, :cond_10

    iget-object p2, p0, Ls5/h;->a:Landroid/content/Context;

    const v0, 0x7f11016d

    invoke-virtual {p2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p2

    if-eqz p3, :cond_b

    iget-object p3, p0, Ls5/h;->a:Landroid/content/Context;

    const v0, 0x7f11016f

    goto/16 :goto_4

    :cond_b
    iget-object p3, p0, Ls5/h;->a:Landroid/content/Context;

    const v0, 0x7f11016e

    goto/16 :goto_4

    :cond_c
    iget-object p2, p0, Ls5/h;->a:Landroid/content/Context;

    const p3, 0x7f1101d4

    goto/16 :goto_0

    :cond_d
    iget-object p2, p0, Ls5/h;->a:Landroid/content/Context;

    const p3, 0x7f1101d3

    goto :goto_6

    :cond_e
    iget-object p2, p0, Ls5/h;->a:Landroid/content/Context;

    const p3, 0x7f11017f

    invoke-virtual {p2, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p2

    iget-object p3, p0, Ls5/h;->a:Landroid/content/Context;

    const v0, 0x7f110180

    goto/16 :goto_4

    :cond_f
    iget-object p2, p0, Ls5/h;->a:Landroid/content/Context;

    const p3, 0x7f1101c3

    :goto_6
    invoke-virtual {p2, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p2

    iget-object p3, p0, Ls5/h;->a:Landroid/content/Context;

    invoke-virtual {p3, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p3

    goto :goto_7

    :cond_10
    iget-object p2, p0, Ls5/h;->a:Landroid/content/Context;

    invoke-virtual {p2, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p2

    iget-object p3, p0, Ls5/h;->a:Landroid/content/Context;

    invoke-virtual {p3, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p3

    goto :goto_7

    :cond_11
    iget-object p2, p0, Ls5/h;->a:Landroid/content/Context;

    const p3, 0x7f110175

    invoke-virtual {p2, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p2

    iget-object p3, p0, Ls5/h;->a:Landroid/content/Context;

    const v0, 0x7f110176

    goto/16 :goto_4

    :cond_12
    iget-object p2, p0, Ls5/h;->a:Landroid/content/Context;

    const p3, 0x7f1101da

    invoke-virtual {p2, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p2

    iget-object p3, p0, Ls5/h;->a:Landroid/content/Context;

    const v0, 0x7f1101db

    goto/16 :goto_4

    :goto_7
    new-instance v0, Ls5/h$a;

    invoke-direct {v0, p2, p3, p1}, Ls5/h$a;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    return-object v0

    :pswitch_data_0
    .packed-switch -0xea65
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
    .end packed-switch

    :pswitch_data_1
    .packed-switch -0x6d
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch

    :pswitch_data_2
    .packed-switch -0xe
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
