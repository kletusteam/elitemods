.class public final Ls5/g$a;
.super Ls5/d$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ls5/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field private final k:Landroid/content/Context;

.field private l:Landroid/net/Uri;

.field private m:Lcom/miui/packageInstaller/model/IncrementPackageInfo;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const-string v0, "mContext"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Ls5/d$a;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Ls5/g$a;->k:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public a()Ls5/l;
    .locals 4

    invoke-super {p0}, Ls5/d$a;->a()Ls5/l;

    move-result-object v0

    new-instance v1, Ls5/g;

    iget-object v2, p0, Ls5/g$a;->k:Landroid/content/Context;

    const-string v3, "null cannot be cast to non-null type com.miui.packageInstaller.installer.BaseInstaller"

    invoke-static {v0, v3}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ls5/d;

    invoke-direct {v1, v2, v0}, Ls5/g;-><init>(Landroid/content/Context;Ls5/d;)V

    iget-object v0, p0, Ls5/g$a;->l:Landroid/net/Uri;

    invoke-virtual {v1, v0}, Ls5/g;->s(Landroid/net/Uri;)V

    iget-object v0, p0, Ls5/g$a;->m:Lcom/miui/packageInstaller/model/IncrementPackageInfo;

    invoke-virtual {v1, v0}, Ls5/g;->t(Lcom/miui/packageInstaller/model/IncrementPackageInfo;)V

    return-object v1
.end method

.method public final j(Landroid/net/Uri;)Ls5/g$a;
    .locals 0

    iput-object p1, p0, Ls5/g$a;->l:Landroid/net/Uri;

    return-object p0
.end method

.method public final k(Lcom/miui/packageInstaller/model/IncrementPackageInfo;)Ls5/g$a;
    .locals 0

    iput-object p1, p0, Ls5/g$a;->m:Lcom/miui/packageInstaller/model/IncrementPackageInfo;

    return-object p0
.end method
