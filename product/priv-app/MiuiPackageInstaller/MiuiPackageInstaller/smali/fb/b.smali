.class public Lfb/b;
.super Ljava/lang/Object;


# static fields
.field private static final j0:[I


# instance fields
.field private A:Lg9/g;

.field private B:Lg9/g;

.field private C:Lg9/g;

.field private D:Lg9/g;

.field private E:Lg9/g;

.field private F:Lg9/g;

.field private G:Lg9/g;

.field private H:Lg9/g;

.field private I:Lg9/g;

.field private J:Lg9/g;

.field private K:Lg9/g;

.field private L:F

.field private M:F

.field private N:F

.field private O:F

.field private P:F

.field private Q:Landroid/graphics/drawable/Drawable;

.field private R:Landroid/graphics/drawable/Drawable;

.field private S:Z

.field private T:I

.field private U:I

.field private V:Z

.field private W:F

.field private X:Lh9/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lh9/b<",
            "Landroid/widget/CompoundButton;",
            ">;"
        }
    .end annotation
.end field

.field private Y:Lg9/c$c;

.field private Z:Lh9/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lh9/b<",
            "Landroid/widget/CompoundButton;",
            ">;"
        }
    .end annotation
.end field

.field private a:Landroid/graphics/drawable/Drawable;

.field private a0:Lh9/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lh9/b<",
            "Landroid/widget/CompoundButton;",
            ">;"
        }
    .end annotation
.end field

.field private b:I

.field private b0:Lh9/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lh9/b<",
            "Landroid/widget/CompoundButton;",
            ">;"
        }
    .end annotation
.end field

.field private c:Landroid/graphics/drawable/Drawable;

.field private c0:Lh9/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lh9/b<",
            "Landroid/widget/CompoundButton;",
            ">;"
        }
    .end annotation
.end field

.field private d:I

.field private d0:I

.field private e:I

.field private e0:I

.field private f:I

.field private f0:I

.field private g:I

.field private g0:F

.field private h:I

.field private h0:[F

.field private i:I

.field private i0:I

.field private j:I

.field private k:I

.field private l:I

.field private m:I

.field private n:Z

.field private o:Z

.field private p:I

.field private q:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field private r:Landroid/graphics/Rect;

.field private s:Landroid/graphics/drawable/StateListDrawable;

.field private t:Z

.field private u:Lh9/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lh9/b<",
            "Landroid/widget/CompoundButton;",
            ">;"
        }
    .end annotation
.end field

.field private v:Landroid/graphics/drawable/Drawable;

.field private w:Landroid/graphics/drawable/Drawable;

.field private x:Landroid/graphics/drawable/Drawable;

.field private y:Landroid/widget/CompoundButton;

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x10100a0

    aput v2, v0, v1

    sput-object v0, Lfb/b;->j0:[I

    return-void
.end method

.method public constructor <init>(Landroid/widget/CompoundButton;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lfb/b;->r:Landroid/graphics/Rect;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lfb/b;->t:Z

    new-instance v1, Lfb/b$a;

    const-string v2, "SliderOffset"

    invoke-direct {v1, p0, v2}, Lfb/b$a;-><init>(Lfb/b;Ljava/lang/String;)V

    iput-object v1, p0, Lfb/b;->u:Lh9/b;

    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, p0, Lfb/b;->L:F

    const/4 v2, 0x0

    iput v2, p0, Lfb/b;->M:F

    const v3, 0x3dcccccd    # 0.1f

    iput v3, p0, Lfb/b;->N:F

    iput v1, p0, Lfb/b;->O:F

    iput v2, p0, Lfb/b;->P:F

    iput-boolean v0, p0, Lfb/b;->S:Z

    const/4 v3, -0x1

    iput v3, p0, Lfb/b;->T:I

    iput v3, p0, Lfb/b;->U:I

    iput-boolean v0, p0, Lfb/b;->V:Z

    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lfb/b;->W:F

    new-instance v0, Lfb/b$b;

    const-string v3, "SliderScale"

    invoke-direct {v0, p0, v3}, Lfb/b$b;-><init>(Lfb/b;Ljava/lang/String;)V

    iput-object v0, p0, Lfb/b;->X:Lh9/b;

    new-instance v0, Lfb/a;

    invoke-direct {v0, p0}, Lfb/a;-><init>(Lfb/b;)V

    iput-object v0, p0, Lfb/b;->Y:Lg9/c$c;

    new-instance v0, Lfb/b$c;

    const-string v3, "SliderShadowAlpha"

    invoke-direct {v0, p0, v3}, Lfb/b$c;-><init>(Lfb/b;Ljava/lang/String;)V

    iput-object v0, p0, Lfb/b;->Z:Lh9/b;

    new-instance v0, Lfb/b$d;

    const-string v3, "StrokeAlpha"

    invoke-direct {v0, p0, v3}, Lfb/b$d;-><init>(Lfb/b;Ljava/lang/String;)V

    iput-object v0, p0, Lfb/b;->a0:Lh9/b;

    new-instance v0, Lfb/b$e;

    const-string v3, "MaskCheckedSlideBarAlpha"

    invoke-direct {v0, p0, v3}, Lfb/b$e;-><init>(Lfb/b;Ljava/lang/String;)V

    iput-object v0, p0, Lfb/b;->b0:Lh9/b;

    new-instance v0, Lfb/b$f;

    const-string v3, "MaskUnCheckedSlideBarAlpha"

    invoke-direct {v0, p0, v3}, Lfb/b$f;-><init>(Lfb/b;Ljava/lang/String;)V

    iput-object v0, p0, Lfb/b;->c0:Lh9/b;

    iput v1, p0, Lfb/b;->g0:F

    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    iput-object v0, p0, Lfb/b;->h0:[F

    iput-object p1, p0, Lfb/b;->y:Landroid/widget/CompoundButton;

    invoke-virtual {p1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result p1

    iput-boolean p1, p0, Lfb/b;->z:Z

    iget-object p1, p0, Lfb/b;->y:Landroid/widget/CompoundButton;

    invoke-virtual {p1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result p1

    if-nez p1, :cond_0

    iput v2, p0, Lfb/b;->O:F

    :cond_0
    return-void

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method private C(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V
    .locals 0

    iput-object p1, p0, Lfb/b;->v:Landroid/graphics/drawable/Drawable;

    iput-object p2, p0, Lfb/b;->w:Landroid/graphics/drawable/Drawable;

    iput-object p3, p0, Lfb/b;->x:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method private synthetic F(Lg9/c;FF)V
    .locals 0

    iget-object p1, p0, Lfb/b;->y:Landroid/widget/CompoundButton;

    invoke-virtual {p1}, Landroid/widget/CompoundButton;->invalidate()V

    return-void
.end method

.method private G(I)V
    .locals 3

    iget-object v0, p0, Lfb/b;->y:Landroid/widget/CompoundButton;

    invoke-static {v0}, Landroidx/appcompat/widget/s0;->b(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    neg-int p1, p1

    :cond_0
    iget v0, p0, Lfb/b;->k:I

    add-int/2addr v0, p1

    iput v0, p0, Lfb/b;->k:I

    iget p1, p0, Lfb/b;->i:I

    if-ge v0, p1, :cond_1

    iput p1, p0, Lfb/b;->k:I

    goto :goto_0

    :cond_1
    iget v1, p0, Lfb/b;->j:I

    if-le v0, v1, :cond_2

    iput v1, p0, Lfb/b;->k:I

    :cond_2
    :goto_0
    iget v0, p0, Lfb/b;->k:I

    if-eq v0, p1, :cond_4

    iget p1, p0, Lfb/b;->j:I

    if-ne v0, p1, :cond_3

    goto :goto_1

    :cond_3
    const/4 p1, 0x0

    goto :goto_2

    :cond_4
    :goto_1
    const/4 p1, 0x1

    :goto_2
    if-eqz p1, :cond_5

    iget-boolean v0, p0, Lfb/b;->t:Z

    if-nez v0, :cond_5

    iget-object v0, p0, Lfb/b;->y:Landroid/widget/CompoundButton;

    sget v1, Lmiuix/view/c;->F:I

    sget v2, Lmiuix/view/c;->i:I

    invoke-static {v0, v1, v2}, Lmiuix/view/HapticCompat;->e(Landroid/view/View;II)Z

    :cond_5
    iput-boolean p1, p0, Lfb/b;->t:Z

    iget p1, p0, Lfb/b;->k:I

    invoke-virtual {p0, p1}, Lfb/b;->b0(I)V

    return-void
.end method

.method private J(Landroid/graphics/Canvas;F)V
    .locals 3

    iget v0, p0, Lfb/b;->O:F

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float/2addr v1, v0

    const/high16 v0, 0x437f0000    # 255.0f

    mul-float/2addr v1, v0

    mul-float/2addr v1, p2

    float-to-int v1, v1

    if-lez v1, :cond_0

    iget-object v2, p0, Lfb/b;->w:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    iget-object v1, p0, Lfb/b;->w:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_0
    iget v1, p0, Lfb/b;->P:F

    mul-float/2addr v1, v0

    mul-float/2addr v1, p2

    float-to-int v1, v1

    if-lez v1, :cond_1

    iget-object v2, p0, Lfb/b;->x:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    iget-object v1, p0, Lfb/b;->x:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_1
    iget v1, p0, Lfb/b;->O:F

    mul-float/2addr v1, v0

    mul-float/2addr v1, p2

    float-to-int p2, v1

    if-lez p2, :cond_2

    iget-object v0, p0, Lfb/b;->v:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p2}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    iget-object p2, p0, Lfb/b;->v:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_2
    return-void
.end method

.method private K(Landroid/graphics/Canvas;II)V
    .locals 6

    iget v0, p0, Lfb/b;->M:F

    const/high16 v1, 0x437f0000    # 255.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Lfb/b;->Q:Landroid/graphics/drawable/Drawable;

    instance-of v2, v1, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v2, :cond_1

    check-cast v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    iget-object v2, p0, Lfb/b;->Q:Landroid/graphics/drawable/Drawable;

    check-cast v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    iget-object v2, p0, Lfb/b;->Q:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    :goto_0
    iget-object v3, p0, Lfb/b;->Q:Landroid/graphics/drawable/Drawable;

    div-int/lit8 v1, v1, 0x2

    sub-int v4, p2, v1

    div-int/lit8 v2, v2, 0x2

    sub-int v5, p3, v2

    add-int/2addr p2, v1

    add-int/2addr p3, v2

    invoke-virtual {v3, v4, v5, p2, p3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object p2, p0, Lfb/b;->Q:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p2, v0}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    iget-object p2, p0, Lfb/b;->Q:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method private L(Landroid/graphics/Canvas;IIIIF)V
    .locals 3

    iget-object v0, p0, Lfb/b;->R:Landroid/graphics/drawable/Drawable;

    iget v1, p0, Lfb/b;->N:F

    const/high16 v2, 0x437f0000    # 255.0f

    mul-float/2addr v1, v2

    mul-float/2addr v1, p6

    float-to-int p6, v1

    invoke-virtual {v0, p6}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    iget-object p6, p0, Lfb/b;->R:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p6, p2, p3, p4, p5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object p2, p0, Lfb/b;->R:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method private N()V
    .locals 1

    iget-object v0, p0, Lfb/b;->B:Lg9/g;

    invoke-virtual {v0}, Lg9/c;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfb/b;->B:Lg9/g;

    invoke-virtual {v0}, Lg9/c;->d()V

    :cond_0
    iget-object v0, p0, Lfb/b;->A:Lg9/g;

    invoke-virtual {v0}, Lg9/c;->i()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lfb/b;->A:Lg9/g;

    invoke-virtual {v0}, Lg9/g;->q()V

    :cond_1
    iget-object v0, p0, Lfb/b;->C:Lg9/g;

    invoke-virtual {v0}, Lg9/c;->i()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lfb/b;->C:Lg9/g;

    invoke-virtual {v0}, Lg9/g;->q()V

    :cond_2
    iget-object v0, p0, Lfb/b;->y:Landroid/widget/CompoundButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lfb/b;->J:Lg9/g;

    invoke-virtual {v0}, Lg9/c;->i()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lfb/b;->J:Lg9/g;

    invoke-virtual {v0}, Lg9/c;->d()V

    :cond_3
    iget-object v0, p0, Lfb/b;->I:Lg9/g;

    invoke-virtual {v0}, Lg9/c;->i()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lfb/b;->I:Lg9/g;

    invoke-virtual {v0}, Lg9/g;->q()V

    :cond_4
    iget-object v0, p0, Lfb/b;->E:Lg9/g;

    invoke-virtual {v0}, Lg9/c;->i()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lfb/b;->E:Lg9/g;

    invoke-virtual {v0}, Lg9/g;->q()V

    :cond_5
    return-void
.end method

.method private P()V
    .locals 1

    iget-object v0, p0, Lfb/b;->A:Lg9/g;

    invoke-virtual {v0}, Lg9/c;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfb/b;->A:Lg9/g;

    invoke-virtual {v0}, Lg9/c;->d()V

    :cond_0
    iget-object v0, p0, Lfb/b;->B:Lg9/g;

    invoke-virtual {v0}, Lg9/c;->i()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lfb/b;->B:Lg9/g;

    invoke-virtual {v0}, Lg9/g;->q()V

    :cond_1
    iget-object v0, p0, Lfb/b;->C:Lg9/g;

    invoke-virtual {v0}, Lg9/c;->i()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lfb/b;->C:Lg9/g;

    invoke-virtual {v0}, Lg9/c;->d()V

    :cond_2
    iget-object v0, p0, Lfb/b;->D:Lg9/g;

    invoke-virtual {v0}, Lg9/c;->i()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lfb/b;->D:Lg9/g;

    invoke-virtual {v0}, Lg9/g;->q()V

    :cond_3
    iget-object v0, p0, Lfb/b;->E:Lg9/g;

    invoke-virtual {v0}, Lg9/c;->i()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lfb/b;->E:Lg9/g;

    invoke-virtual {v0}, Lg9/c;->d()V

    :cond_4
    iget-object v0, p0, Lfb/b;->y:Landroid/widget/CompoundButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lfb/b;->I:Lg9/g;

    invoke-virtual {v0}, Lg9/c;->i()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lfb/b;->I:Lg9/g;

    invoke-virtual {v0}, Lg9/c;->d()V

    :cond_5
    iget-object v0, p0, Lfb/b;->J:Lg9/g;

    invoke-virtual {v0}, Lg9/c;->i()Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lfb/b;->J:Lg9/g;

    invoke-virtual {v0}, Lg9/g;->q()V

    :cond_6
    iget-object v0, p0, Lfb/b;->F:Lg9/g;

    invoke-virtual {v0}, Lg9/c;->i()Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lfb/b;->F:Lg9/g;

    invoke-virtual {v0}, Lg9/g;->q()V

    :cond_7
    return-void
.end method

.method private Q()V
    .locals 1

    iget-boolean v0, p0, Lfb/b;->S:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lfb/b;->T:I

    iput v0, p0, Lfb/b;->k:I

    iget v0, p0, Lfb/b;->U:I

    iput v0, p0, Lfb/b;->b:I

    iget v0, p0, Lfb/b;->W:F

    iput v0, p0, Lfb/b;->O:F

    iget-boolean v0, p0, Lfb/b;->V:Z

    iput-boolean v0, p0, Lfb/b;->z:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lfb/b;->S:Z

    const/4 v0, -0x1

    iput v0, p0, Lfb/b;->T:I

    iput v0, p0, Lfb/b;->U:I

    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lfb/b;->W:F

    :cond_0
    return-void
.end method

.method private R()V
    .locals 1

    iget v0, p0, Lfb/b;->k:I

    iput v0, p0, Lfb/b;->T:I

    iget v0, p0, Lfb/b;->b:I

    iput v0, p0, Lfb/b;->U:I

    iget v0, p0, Lfb/b;->O:F

    iput v0, p0, Lfb/b;->W:F

    iget-boolean v0, p0, Lfb/b;->z:Z

    iput-boolean v0, p0, Lfb/b;->V:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lfb/b;->S:Z

    return-void
.end method

.method private S(Landroid/graphics/Canvas;)V
    .locals 0

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    return-void
.end method

.method private T(Landroid/graphics/Canvas;II)V
    .locals 1

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    iget v0, p0, Lfb/b;->L:F

    int-to-float p2, p2

    int-to-float p3, p3

    invoke-virtual {p1, v0, v0, p2, p3}, Landroid/graphics/Canvas;->scale(FFFF)V

    return-void
.end method

.method private W(Z)V
    .locals 1

    iget-boolean v0, p0, Lfb/b;->z:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfb/b;->H:Lg9/g;

    invoke-virtual {v0}, Lg9/c;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfb/b;->H:Lg9/g;

    invoke-virtual {v0}, Lg9/c;->d()V

    :cond_0
    iget-object v0, p0, Lfb/b;->G:Lg9/g;

    invoke-virtual {v0}, Lg9/c;->i()Z

    move-result v0

    if-nez v0, :cond_1

    if-nez p1, :cond_1

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lfb/b;->O:F

    :cond_1
    iget-boolean v0, p0, Lfb/b;->z:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lfb/b;->G:Lg9/g;

    invoke-virtual {v0}, Lg9/c;->i()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lfb/b;->G:Lg9/g;

    invoke-virtual {v0}, Lg9/c;->d()V

    :cond_2
    iget-object v0, p0, Lfb/b;->H:Lg9/g;

    invoke-virtual {v0}, Lg9/c;->i()Z

    move-result v0

    if-nez v0, :cond_3

    if-eqz p1, :cond_3

    const/4 p1, 0x0

    iput p1, p0, Lfb/b;->O:F

    :cond_3
    return-void
.end method

.method public static synthetic a(Lfb/b;Lg9/c;FF)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lfb/b;->F(Lg9/c;FF)V

    return-void
.end method

.method static synthetic b(Lfb/b;)F
    .locals 0

    iget p0, p0, Lfb/b;->L:F

    return p0
.end method

.method static synthetic c(Lfb/b;F)F
    .locals 0

    iput p1, p0, Lfb/b;->L:F

    return p1
.end method

.method private c0(Z)V
    .locals 2

    iget-object v0, p0, Lfb/b;->K:Lg9/g;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lg9/c;->i()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_0
    iget-boolean v0, p0, Lfb/b;->z:Z

    if-eqz v0, :cond_1

    iget v1, p0, Lfb/b;->j:I

    goto :goto_0

    :cond_1
    iget v1, p0, Lfb/b;->i:I

    :goto_0
    iput v1, p0, Lfb/b;->k:I

    if-eqz v0, :cond_2

    const/16 v0, 0xff

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    :goto_1
    iput v0, p0, Lfb/b;->b:I

    :cond_3
    invoke-direct {p0}, Lfb/b;->Q()V

    invoke-direct {p0, p1}, Lfb/b;->W(Z)V

    return-void
.end method

.method static synthetic d(Lfb/b;)F
    .locals 0

    iget p0, p0, Lfb/b;->M:F

    return p0
.end method

.method static synthetic e(Lfb/b;F)F
    .locals 0

    iput p1, p0, Lfb/b;->M:F

    return p1
.end method

.method static synthetic f(Lfb/b;)F
    .locals 0

    iget p0, p0, Lfb/b;->N:F

    return p0
.end method

.method static synthetic g(Lfb/b;F)F
    .locals 0

    iput p1, p0, Lfb/b;->N:F

    return p1
.end method

.method static synthetic h(Lfb/b;)F
    .locals 0

    iget p0, p0, Lfb/b;->O:F

    return p0
.end method

.method static synthetic i(Lfb/b;F)F
    .locals 0

    iput p1, p0, Lfb/b;->O:F

    return p1
.end method

.method static synthetic j(Lfb/b;)F
    .locals 0

    iget p0, p0, Lfb/b;->P:F

    return p0
.end method

.method static synthetic k(Lfb/b;F)F
    .locals 0

    iput p1, p0, Lfb/b;->P:F

    return p1
.end method

.method static synthetic l(Lfb/b;Z)Z
    .locals 0

    iput-boolean p1, p0, Lfb/b;->z:Z

    return p1
.end method

.method static synthetic m(Lfb/b;)I
    .locals 0

    iget p0, p0, Lfb/b;->k:I

    return p0
.end method

.method static synthetic n(Lfb/b;)I
    .locals 0

    iget p0, p0, Lfb/b;->j:I

    return p0
.end method

.method private o(Landroid/view/View;Landroid/view/MotionEvent;)[F
    .locals 8

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result p2

    const/4 v1, 0x2

    new-array v2, v1, [I

    invoke-virtual {p1, v2}, Landroid/view/View;->getLocationOnScreen([I)V

    const/4 v3, 0x0

    aget v4, v2, v3

    int-to-float v4, v4

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v5

    int-to-float v5, v5

    const/high16 v6, 0x3f000000    # 0.5f

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    const/4 v5, 0x1

    aget v2, v2, v5

    int-to-float v2, v2

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v7

    int-to-float v7, v7

    mul-float/2addr v7, v6

    add-float/2addr v2, v7

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v6

    const/4 v7, 0x0

    if-nez v6, :cond_0

    move v0, v7

    goto :goto_0

    :cond_0
    sub-float/2addr v0, v4

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v0, v4

    :goto_0
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v4

    if-nez v4, :cond_1

    goto :goto_1

    :cond_1
    sub-float/2addr p2, v2

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result p1

    int-to-float p1, p1

    div-float v7, p2, p1

    :goto_1
    const/high16 p1, 0x3f800000    # 1.0f

    invoke-static {p1, v0}, Ljava/lang/Math;->min(FF)F

    move-result p2

    const/high16 v0, -0x40800000    # -1.0f

    invoke-static {v0, p2}, Ljava/lang/Math;->max(FF)F

    move-result p2

    invoke-static {p1, v7}, Ljava/lang/Math;->min(FF)F

    move-result p1

    invoke-static {v0, p1}, Ljava/lang/Math;->max(FF)F

    move-result p1

    iget v0, p0, Lfb/b;->i0:I

    int-to-float v2, v0

    mul-float/2addr p2, v2

    int-to-float v0, v0

    mul-float/2addr p1, v0

    new-array v0, v1, [F

    aput p2, v0, v3

    aput p1, v0, v5

    return-object v0
.end method

.method private p(Z)V
    .locals 2

    iget-object v0, p0, Lfb/b;->y:Landroid/widget/CompoundButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v0

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lfb/b;->y:Landroid/widget/CompoundButton;

    invoke-virtual {v0, p1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    invoke-direct {p0, p1}, Lfb/b;->c0(Z)V

    invoke-virtual {p0}, Lfb/b;->H()V

    :cond_0
    if-eqz p1, :cond_1

    iget v0, p0, Lfb/b;->j:I

    goto :goto_0

    :cond_1
    iget v0, p0, Lfb/b;->i:I

    :goto_0
    new-instance v1, Lfb/b$h;

    invoke-direct {v1, p0}, Lfb/b$h;-><init>(Lfb/b;)V

    invoke-direct {p0, p1, v0, v1}, Lfb/b;->q(ZILjava/lang/Runnable;)V

    return-void
.end method

.method private q(ZILjava/lang/Runnable;)V
    .locals 3

    iget-object v0, p0, Lfb/b;->K:Lg9/g;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lg9/c;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfb/b;->K:Lg9/g;

    invoke-virtual {v0}, Lg9/c;->d()V

    :cond_0
    iget-object v0, p0, Lfb/b;->y:Landroid/widget/CompoundButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v0

    if-eq p1, v0, :cond_1

    return-void

    :cond_1
    new-instance v0, Lg9/g;

    iget-object v1, p0, Lfb/b;->y:Landroid/widget/CompoundButton;

    iget-object v2, p0, Lfb/b;->u:Lh9/b;

    int-to-float p2, p2

    invoke-direct {v0, v1, v2, p2}, Lg9/g;-><init>(Ljava/lang/Object;Lh9/b;F)V

    iput-object v0, p0, Lfb/b;->K:Lg9/g;

    invoke-virtual {v0}, Lg9/g;->u()Lg9/i;

    move-result-object p2

    const v0, 0x4476bd71

    invoke-virtual {p2, v0}, Lg9/i;->f(F)Lg9/i;

    iget-object p2, p0, Lfb/b;->K:Lg9/g;

    invoke-virtual {p2}, Lg9/g;->u()Lg9/i;

    move-result-object p2

    const v0, 0x3f333333    # 0.7f

    invoke-virtual {p2, v0}, Lg9/i;->d(F)Lg9/i;

    iget-object p2, p0, Lfb/b;->K:Lg9/g;

    iget-object v0, p0, Lfb/b;->Y:Lg9/c$c;

    invoke-virtual {p2, v0}, Lg9/c;->c(Lg9/c$c;)Lg9/c;

    iget-object p2, p0, Lfb/b;->K:Lg9/g;

    new-instance v0, Lfb/b$g;

    invoke-direct {v0, p0, p3}, Lfb/b$g;-><init>(Lfb/b;Ljava/lang/Runnable;)V

    invoke-virtual {p2, v0}, Lg9/c;->b(Lg9/c$b;)Lg9/c;

    iget-object p2, p0, Lfb/b;->K:Lg9/g;

    invoke-virtual {p2}, Lg9/g;->q()V

    if-eqz p1, :cond_3

    iget-object p1, p0, Lfb/b;->G:Lg9/g;

    invoke-virtual {p1}, Lg9/c;->i()Z

    move-result p1

    if-nez p1, :cond_2

    iget-object p1, p0, Lfb/b;->G:Lg9/g;

    invoke-virtual {p1}, Lg9/g;->q()V

    :cond_2
    iget-object p1, p0, Lfb/b;->H:Lg9/g;

    invoke-virtual {p1}, Lg9/c;->i()Z

    move-result p1

    if-eqz p1, :cond_5

    iget-object p1, p0, Lfb/b;->H:Lg9/g;

    goto :goto_0

    :cond_3
    iget-object p1, p0, Lfb/b;->H:Lg9/g;

    invoke-virtual {p1}, Lg9/c;->i()Z

    move-result p1

    if-nez p1, :cond_4

    iget-object p1, p0, Lfb/b;->H:Lg9/g;

    invoke-virtual {p1}, Lg9/g;->q()V

    :cond_4
    iget-object p1, p0, Lfb/b;->G:Lg9/g;

    invoke-virtual {p1}, Lg9/c;->i()Z

    move-result p1

    if-eqz p1, :cond_5

    iget-object p1, p0, Lfb/b;->G:Lg9/g;

    :goto_0
    invoke-virtual {p1}, Lg9/c;->d()V

    :cond_5
    return-void
.end method

.method private r()V
    .locals 3

    iget-object v0, p0, Lfb/b;->y:Landroid/widget/CompoundButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, Lfb/b;->p(Z)V

    iget-object v0, p0, Lfb/b;->y:Landroid/widget/CompoundButton;

    sget v1, Lmiuix/view/c;->F:I

    sget v2, Lmiuix/view/c;->i:I

    invoke-static {v0, v1, v2}, Lmiuix/view/HapticCompat;->e(Landroid/view/View;II)Z

    return-void
.end method

.method private s(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
    .locals 5

    new-instance v0, Lmiuix/smooth/SmoothContainerDrawable;

    invoke-direct {v0}, Lmiuix/smooth/SmoothContainerDrawable;-><init>()V

    iget-object v1, p0, Lfb/b;->y:Landroid/widget/CompoundButton;

    invoke-virtual {v1}, Landroid/widget/CompoundButton;->getLayerType()I

    move-result v1

    invoke-virtual {v0, v1}, Lmiuix/smooth/SmoothContainerDrawable;->i(I)V

    iget v1, p0, Lfb/b;->d0:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lmiuix/smooth/SmoothContainerDrawable;->h(F)V

    invoke-virtual {v0, p1}, Lmiuix/smooth/SmoothContainerDrawable;->f(Landroid/graphics/drawable/Drawable;)V

    new-instance p1, Landroid/graphics/Rect;

    iget v1, p0, Lfb/b;->f0:I

    iget v2, p0, Lfb/b;->e0:I

    iget v3, p0, Lfb/b;->e:I

    sub-int/2addr v3, v1

    iget v4, p0, Lfb/b;->f:I

    sub-int/2addr v4, v2

    invoke-direct {p1, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    return-object v0
.end method

.method private t()Landroid/graphics/drawable/StateListDrawable;
    .locals 4

    new-instance v0, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    iget v1, p0, Lfb/b;->e:I

    iget v2, p0, Lfb/b;->f:I

    const/4 v3, 0x0

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->setBounds(IIII)V

    iget-object v1, p0, Lfb/b;->y:Landroid/widget/CompoundButton;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/StateListDrawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    return-object v0
.end method


# virtual methods
.method public A()V
    .locals 9

    new-instance v0, Lg9/g;

    iget-object v1, p0, Lfb/b;->y:Landroid/widget/CompoundButton;

    iget-object v2, p0, Lfb/b;->X:Lh9/b;

    const v3, 0x3fce147b    # 1.61f

    invoke-direct {v0, v1, v2, v3}, Lg9/g;-><init>(Ljava/lang/Object;Lh9/b;F)V

    iput-object v0, p0, Lfb/b;->A:Lg9/g;

    invoke-virtual {v0}, Lg9/g;->u()Lg9/i;

    move-result-object v0

    const v1, 0x4476bd71

    invoke-virtual {v0, v1}, Lg9/i;->f(F)Lg9/i;

    iget-object v0, p0, Lfb/b;->A:Lg9/g;

    invoke-virtual {v0}, Lg9/g;->u()Lg9/i;

    move-result-object v0

    const v2, 0x3f19999a    # 0.6f

    invoke-virtual {v0, v2}, Lg9/i;->d(F)Lg9/i;

    iget-object v0, p0, Lfb/b;->A:Lg9/g;

    const v3, 0x3b03126f    # 0.002f

    invoke-virtual {v0, v3}, Lg9/c;->k(F)Lg9/c;

    iget-object v0, p0, Lfb/b;->A:Lg9/g;

    iget-object v4, p0, Lfb/b;->Y:Lg9/c$c;

    invoke-virtual {v0, v4}, Lg9/c;->c(Lg9/c$c;)Lg9/c;

    new-instance v0, Lg9/g;

    iget-object v4, p0, Lfb/b;->y:Landroid/widget/CompoundButton;

    iget-object v5, p0, Lfb/b;->X:Lh9/b;

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-direct {v0, v4, v5, v6}, Lg9/g;-><init>(Ljava/lang/Object;Lh9/b;F)V

    iput-object v0, p0, Lfb/b;->B:Lg9/g;

    invoke-virtual {v0}, Lg9/g;->u()Lg9/i;

    move-result-object v0

    invoke-virtual {v0, v1}, Lg9/i;->f(F)Lg9/i;

    iget-object v0, p0, Lfb/b;->B:Lg9/g;

    invoke-virtual {v0}, Lg9/g;->u()Lg9/i;

    move-result-object v0

    invoke-virtual {v0, v2}, Lg9/i;->d(F)Lg9/i;

    iget-object v0, p0, Lfb/b;->B:Lg9/g;

    invoke-virtual {v0, v3}, Lg9/c;->k(F)Lg9/c;

    iget-object v0, p0, Lfb/b;->B:Lg9/g;

    iget-object v2, p0, Lfb/b;->Y:Lg9/c$c;

    invoke-virtual {v0, v2}, Lg9/c;->c(Lg9/c$c;)Lg9/c;

    new-instance v0, Lg9/g;

    iget-object v2, p0, Lfb/b;->y:Landroid/widget/CompoundButton;

    iget-object v3, p0, Lfb/b;->Z:Lh9/b;

    invoke-direct {v0, v2, v3, v6}, Lg9/g;-><init>(Ljava/lang/Object;Lh9/b;F)V

    iput-object v0, p0, Lfb/b;->C:Lg9/g;

    invoke-virtual {v0}, Lg9/g;->u()Lg9/i;

    move-result-object v0

    invoke-virtual {v0, v1}, Lg9/i;->f(F)Lg9/i;

    iget-object v0, p0, Lfb/b;->C:Lg9/g;

    invoke-virtual {v0}, Lg9/g;->u()Lg9/i;

    move-result-object v0

    const v2, 0x3f7d70a4    # 0.99f

    invoke-virtual {v0, v2}, Lg9/i;->d(F)Lg9/i;

    iget-object v0, p0, Lfb/b;->C:Lg9/g;

    const/high16 v3, 0x3b800000    # 0.00390625f

    invoke-virtual {v0, v3}, Lg9/c;->k(F)Lg9/c;

    iget-object v0, p0, Lfb/b;->C:Lg9/g;

    iget-object v4, p0, Lfb/b;->Y:Lg9/c$c;

    invoke-virtual {v0, v4}, Lg9/c;->c(Lg9/c$c;)Lg9/c;

    new-instance v0, Lg9/g;

    iget-object v4, p0, Lfb/b;->y:Landroid/widget/CompoundButton;

    iget-object v5, p0, Lfb/b;->Z:Lh9/b;

    const/4 v7, 0x0

    invoke-direct {v0, v4, v5, v7}, Lg9/g;-><init>(Ljava/lang/Object;Lh9/b;F)V

    iput-object v0, p0, Lfb/b;->D:Lg9/g;

    invoke-virtual {v0}, Lg9/g;->u()Lg9/i;

    move-result-object v0

    invoke-virtual {v0, v1}, Lg9/i;->f(F)Lg9/i;

    iget-object v0, p0, Lfb/b;->D:Lg9/g;

    invoke-virtual {v0}, Lg9/g;->u()Lg9/i;

    move-result-object v0

    invoke-virtual {v0, v2}, Lg9/i;->d(F)Lg9/i;

    iget-object v0, p0, Lfb/b;->D:Lg9/g;

    invoke-virtual {v0, v3}, Lg9/c;->k(F)Lg9/c;

    iget-object v0, p0, Lfb/b;->D:Lg9/g;

    iget-object v4, p0, Lfb/b;->Y:Lg9/c$c;

    invoke-virtual {v0, v4}, Lg9/c;->c(Lg9/c$c;)Lg9/c;

    new-instance v0, Lg9/g;

    iget-object v4, p0, Lfb/b;->y:Landroid/widget/CompoundButton;

    iget-object v5, p0, Lfb/b;->a0:Lh9/b;

    const v8, 0x3e19999a    # 0.15f

    invoke-direct {v0, v4, v5, v8}, Lg9/g;-><init>(Ljava/lang/Object;Lh9/b;F)V

    iput-object v0, p0, Lfb/b;->E:Lg9/g;

    invoke-virtual {v0}, Lg9/g;->u()Lg9/i;

    move-result-object v0

    invoke-virtual {v0, v1}, Lg9/i;->f(F)Lg9/i;

    iget-object v0, p0, Lfb/b;->E:Lg9/g;

    invoke-virtual {v0}, Lg9/g;->u()Lg9/i;

    move-result-object v0

    invoke-virtual {v0, v2}, Lg9/i;->d(F)Lg9/i;

    iget-object v0, p0, Lfb/b;->E:Lg9/g;

    invoke-virtual {v0, v3}, Lg9/c;->k(F)Lg9/c;

    iget-object v0, p0, Lfb/b;->E:Lg9/g;

    iget-object v4, p0, Lfb/b;->Y:Lg9/c$c;

    invoke-virtual {v0, v4}, Lg9/c;->c(Lg9/c$c;)Lg9/c;

    new-instance v0, Lg9/g;

    iget-object v4, p0, Lfb/b;->y:Landroid/widget/CompoundButton;

    iget-object v5, p0, Lfb/b;->a0:Lh9/b;

    const v8, 0x3dcccccd    # 0.1f

    invoke-direct {v0, v4, v5, v8}, Lg9/g;-><init>(Ljava/lang/Object;Lh9/b;F)V

    iput-object v0, p0, Lfb/b;->F:Lg9/g;

    invoke-virtual {v0}, Lg9/g;->u()Lg9/i;

    move-result-object v0

    invoke-virtual {v0, v1}, Lg9/i;->f(F)Lg9/i;

    iget-object v0, p0, Lfb/b;->F:Lg9/g;

    invoke-virtual {v0}, Lg9/g;->u()Lg9/i;

    move-result-object v0

    invoke-virtual {v0, v2}, Lg9/i;->d(F)Lg9/i;

    iget-object v0, p0, Lfb/b;->F:Lg9/g;

    invoke-virtual {v0, v3}, Lg9/c;->k(F)Lg9/c;

    iget-object v0, p0, Lfb/b;->F:Lg9/g;

    iget-object v4, p0, Lfb/b;->Y:Lg9/c$c;

    invoke-virtual {v0, v4}, Lg9/c;->c(Lg9/c$c;)Lg9/c;

    new-instance v0, Lg9/g;

    iget-object v4, p0, Lfb/b;->y:Landroid/widget/CompoundButton;

    iget-object v5, p0, Lfb/b;->b0:Lh9/b;

    invoke-direct {v0, v4, v5, v6}, Lg9/g;-><init>(Ljava/lang/Object;Lh9/b;F)V

    iput-object v0, p0, Lfb/b;->G:Lg9/g;

    invoke-virtual {v0}, Lg9/g;->u()Lg9/i;

    move-result-object v0

    const v4, 0x43db51ec

    invoke-virtual {v0, v4}, Lg9/i;->f(F)Lg9/i;

    iget-object v0, p0, Lfb/b;->G:Lg9/g;

    invoke-virtual {v0}, Lg9/g;->u()Lg9/i;

    move-result-object v0

    invoke-virtual {v0, v2}, Lg9/i;->d(F)Lg9/i;

    iget-object v0, p0, Lfb/b;->G:Lg9/g;

    invoke-virtual {v0, v3}, Lg9/c;->k(F)Lg9/c;

    iget-object v0, p0, Lfb/b;->G:Lg9/g;

    iget-object v4, p0, Lfb/b;->Y:Lg9/c$c;

    invoke-virtual {v0, v4}, Lg9/c;->c(Lg9/c$c;)Lg9/c;

    new-instance v0, Lg9/g;

    iget-object v4, p0, Lfb/b;->y:Landroid/widget/CompoundButton;

    iget-object v5, p0, Lfb/b;->b0:Lh9/b;

    invoke-direct {v0, v4, v5, v7}, Lg9/g;-><init>(Ljava/lang/Object;Lh9/b;F)V

    iput-object v0, p0, Lfb/b;->H:Lg9/g;

    invoke-virtual {v0}, Lg9/g;->u()Lg9/i;

    move-result-object v0

    invoke-virtual {v0, v1}, Lg9/i;->f(F)Lg9/i;

    iget-object v0, p0, Lfb/b;->H:Lg9/g;

    invoke-virtual {v0}, Lg9/g;->u()Lg9/i;

    move-result-object v0

    invoke-virtual {v0, v2}, Lg9/i;->d(F)Lg9/i;

    iget-object v0, p0, Lfb/b;->H:Lg9/g;

    invoke-virtual {v0, v3}, Lg9/c;->k(F)Lg9/c;

    iget-object v0, p0, Lfb/b;->H:Lg9/g;

    iget-object v4, p0, Lfb/b;->Y:Lg9/c$c;

    invoke-virtual {v0, v4}, Lg9/c;->c(Lg9/c$c;)Lg9/c;

    new-instance v0, Lg9/g;

    iget-object v4, p0, Lfb/b;->y:Landroid/widget/CompoundButton;

    iget-object v5, p0, Lfb/b;->c0:Lh9/b;

    const v6, 0x3d4ccccd    # 0.05f

    invoke-direct {v0, v4, v5, v6}, Lg9/g;-><init>(Ljava/lang/Object;Lh9/b;F)V

    iput-object v0, p0, Lfb/b;->I:Lg9/g;

    invoke-virtual {v0}, Lg9/g;->u()Lg9/i;

    move-result-object v0

    invoke-virtual {v0, v1}, Lg9/i;->f(F)Lg9/i;

    iget-object v0, p0, Lfb/b;->I:Lg9/g;

    invoke-virtual {v0}, Lg9/g;->u()Lg9/i;

    move-result-object v0

    invoke-virtual {v0, v2}, Lg9/i;->d(F)Lg9/i;

    iget-object v0, p0, Lfb/b;->I:Lg9/g;

    invoke-virtual {v0, v3}, Lg9/c;->k(F)Lg9/c;

    iget-object v0, p0, Lfb/b;->I:Lg9/g;

    iget-object v4, p0, Lfb/b;->Y:Lg9/c$c;

    invoke-virtual {v0, v4}, Lg9/c;->c(Lg9/c$c;)Lg9/c;

    new-instance v0, Lg9/g;

    iget-object v4, p0, Lfb/b;->y:Landroid/widget/CompoundButton;

    iget-object v5, p0, Lfb/b;->c0:Lh9/b;

    invoke-direct {v0, v4, v5, v7}, Lg9/g;-><init>(Ljava/lang/Object;Lh9/b;F)V

    iput-object v0, p0, Lfb/b;->J:Lg9/g;

    invoke-virtual {v0}, Lg9/g;->u()Lg9/i;

    move-result-object v0

    invoke-virtual {v0, v1}, Lg9/i;->f(F)Lg9/i;

    iget-object v0, p0, Lfb/b;->J:Lg9/g;

    invoke-virtual {v0}, Lg9/g;->u()Lg9/i;

    move-result-object v0

    invoke-virtual {v0, v2}, Lg9/i;->d(F)Lg9/i;

    iget-object v0, p0, Lfb/b;->J:Lg9/g;

    invoke-virtual {v0, v3}, Lg9/c;->k(F)Lg9/c;

    iget-object v0, p0, Lfb/b;->J:Lg9/g;

    iget-object v1, p0, Lfb/b;->Y:Lg9/c$c;

    invoke-virtual {v0, v1}, Lg9/c;->c(Lg9/c$c;)Lg9/c;

    return-void
.end method

.method public B()V
    .locals 2

    iget-object v0, p0, Lfb/b;->y:Landroid/widget/CompoundButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Leb/d;->a:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lfb/b;->Q:Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lfb/b;->y:Landroid/widget/CompoundButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Leb/d;->b:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lfb/b;->R:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method public D(Landroid/content/Context;Landroid/content/res/TypedArray;)V
    .locals 5

    iget-object v0, p0, Lfb/b;->y:Landroid/widget/CompoundButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Leb/c;->a:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lfb/b;->d0:I

    iget-object v0, p0, Lfb/b;->y:Landroid/widget/CompoundButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Leb/c;->f:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lfb/b;->e0:I

    iget-object v0, p0, Lfb/b;->y:Landroid/widget/CompoundButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Leb/c;->e:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lfb/b;->f0:I

    iget-object v0, p0, Lfb/b;->y:Landroid/widget/CompoundButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setDrawingCacheEnabled(Z)V

    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lfb/b;->p:I

    sget v0, Leb/f;->T:I

    invoke-virtual {p2, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lfb/b;->a:Landroid/graphics/drawable/Drawable;

    sget v0, Leb/f;->S:I

    invoke-virtual {p2, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lfb/b;->c:Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lfb/b;->y:Landroid/widget/CompoundButton;

    sget v2, Leb/f;->P:I

    invoke-virtual {p2, v2}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/CompoundButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    const-string v0, "#FF0D84FF"

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    sget v0, Leb/b;->a:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getColor(I)I

    move-result p1

    sget v0, Leb/f;->U:I

    invoke-virtual {p2, v0, p1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result p1

    iput p1, p0, Lfb/b;->d:I

    iget-object p1, p0, Lfb/b;->y:Landroid/widget/CompoundButton;

    invoke-virtual {p1}, Landroid/widget/CompoundButton;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v0, Leb/c;->c:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iget-object v0, p0, Lfb/b;->y:Landroid/widget/CompoundButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Leb/c;->b:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iget-object v2, p0, Lfb/b;->y:Landroid/widget/CompoundButton;

    invoke-virtual {v2}, Landroid/widget/CompoundButton;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Leb/c;->d:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iget-object v3, p0, Lfb/b;->y:Landroid/widget/CompoundButton;

    invoke-virtual {v3}, Landroid/widget/CompoundButton;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Leb/c;->h:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    mul-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v3

    iput v0, p0, Lfb/b;->e:I

    mul-int/lit8 p1, p1, 0x2

    add-int/2addr p1, v2

    iput p1, p0, Lfb/b;->f:I

    iget-object p1, p0, Lfb/b;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result p1

    invoke-static {v0, p1}, Ljava/lang/Math;->min(II)I

    move-result p1

    iput p1, p0, Lfb/b;->g:I

    iget p1, p0, Lfb/b;->f:I

    iget-object v0, p0, Lfb/b;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result p1

    iput p1, p0, Lfb/b;->h:I

    iput v1, p0, Lfb/b;->i:I

    iget p1, p0, Lfb/b;->e:I

    iget v0, p0, Lfb/b;->g:I

    sub-int/2addr p1, v0

    iput p1, p0, Lfb/b;->j:I

    iput v1, p0, Lfb/b;->k:I

    new-instance p1, Landroid/util/TypedValue;

    invoke-direct {p1}, Landroid/util/TypedValue;-><init>()V

    sget v0, Leb/f;->Q:I

    invoke-virtual {p2, v0, p1}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    sget v2, Leb/f;->R:I

    invoke-virtual {p2, v2, v1}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    invoke-virtual {p2, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p2, v2}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p2

    iget v2, p1, Landroid/util/TypedValue;->type:I

    iget v3, v1, Landroid/util/TypedValue;->type:I

    if-ne v2, v3, :cond_0

    iget v2, p1, Landroid/util/TypedValue;->data:I

    iget v3, v1, Landroid/util/TypedValue;->data:I

    if-ne v2, v3, :cond_0

    iget p1, p1, Landroid/util/TypedValue;->resourceId:I

    iget v1, v1, Landroid/util/TypedValue;->resourceId:I

    if-ne p1, v1, :cond_0

    move-object p2, v0

    :cond_0
    if-eqz p2, :cond_1

    if-eqz v0, :cond_1

    iget p1, p0, Lfb/b;->d:I

    invoke-virtual {p2, p1}, Landroid/graphics/drawable/Drawable;->setTint(I)V

    invoke-direct {p0, p2}, Lfb/b;->s(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    invoke-direct {p0, v0}, Lfb/b;->s(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, p2}, Lfb/b;->s(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object p2

    invoke-direct {p0, p1, v0, p2}, Lfb/b;->C(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    invoke-direct {p0}, Lfb/b;->t()Landroid/graphics/drawable/StateListDrawable;

    move-result-object p1

    iput-object p1, p0, Lfb/b;->s:Landroid/graphics/drawable/StateListDrawable;

    :cond_1
    invoke-virtual {p0}, Lfb/b;->a0()V

    iget-object p1, p0, Lfb/b;->y:Landroid/widget/CompoundButton;

    invoke-virtual {p1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result p1

    if-eqz p1, :cond_2

    iget p1, p0, Lfb/b;->j:I

    invoke-virtual {p0, p1}, Lfb/b;->b0(I)V

    :cond_2
    iget-object p1, p0, Lfb/b;->y:Landroid/widget/CompoundButton;

    invoke-virtual {p1}, Landroid/widget/CompoundButton;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget p2, Leb/c;->g:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result p1

    iput p1, p0, Lfb/b;->i0:I

    return-void
.end method

.method public E()V
    .locals 1

    iget-object v0, p0, Lfb/b;->s:Landroid/graphics/drawable/StateListDrawable;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/graphics/drawable/StateListDrawable;->jumpToCurrentState()V

    :cond_0
    return-void
.end method

.method public H()V
    .locals 3

    iget-object v0, p0, Lfb/b;->q:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfb/b;->y:Landroid/widget/CompoundButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v0

    iget-object v1, p0, Lfb/b;->q:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    iget-object v2, p0, Lfb/b;->y:Landroid/widget/CompoundButton;

    invoke-interface {v1, v2, v0}, Landroid/widget/CompoundButton$OnCheckedChangeListener;->onCheckedChanged(Landroid/widget/CompoundButton;Z)V

    :cond_0
    return-void
.end method

.method public I(Landroid/graphics/Canvas;)V
    .locals 10

    iget-object v0, p0, Lfb/b;->y:Landroid/widget/CompoundButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xff

    goto :goto_0

    :cond_0
    const/16 v0, 0x7f

    :goto_0
    int-to-float v0, v0

    iget v1, p0, Lfb/b;->g0:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    int-to-float v1, v0

    const/high16 v2, 0x437f0000    # 255.0f

    div-float v9, v1, v2

    invoke-direct {p0, p1, v9}, Lfb/b;->J(Landroid/graphics/Canvas;F)V

    iget-object v1, p0, Lfb/b;->y:Landroid/widget/CompoundButton;

    invoke-static {v1}, Landroidx/appcompat/widget/s0;->b(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget v2, p0, Lfb/b;->e:I

    iget v3, p0, Lfb/b;->k:I

    sub-int/2addr v2, v3

    iget v3, p0, Lfb/b;->g:I

    sub-int/2addr v2, v3

    goto :goto_1

    :cond_1
    iget v2, p0, Lfb/b;->k:I

    :goto_1
    iget-object v3, p0, Lfb/b;->h0:[F

    const/4 v4, 0x0

    aget v5, v3, v4

    float-to-int v5, v5

    add-int/2addr v5, v2

    if-eqz v1, :cond_2

    iget v1, p0, Lfb/b;->e:I

    iget v2, p0, Lfb/b;->k:I

    sub-int/2addr v1, v2

    goto :goto_2

    :cond_2
    iget v1, p0, Lfb/b;->g:I

    iget v2, p0, Lfb/b;->k:I

    add-int/2addr v1, v2

    :goto_2
    aget v2, v3, v4

    float-to-int v2, v2

    add-int v7, v1, v2

    iget v1, p0, Lfb/b;->f:I

    iget v2, p0, Lfb/b;->h:I

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    const/4 v4, 0x1

    aget v3, v3, v4

    float-to-int v3, v3

    add-int v6, v1, v3

    add-int v8, v6, v2

    add-int v1, v7, v5

    div-int/lit8 v1, v1, 0x2

    add-int v2, v8, v6

    div-int/lit8 v2, v2, 0x2

    invoke-direct {p0, p1, v1, v2}, Lfb/b;->K(Landroid/graphics/Canvas;II)V

    invoke-direct {p0, p1, v1, v2}, Lfb/b;->T(Landroid/graphics/Canvas;II)V

    iget-boolean v1, p0, Lfb/b;->z:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lfb/b;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    iget-object v0, p0, Lfb/b;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v5, v6, v7, v8}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object v0, p0, Lfb/b;->a:Landroid/graphics/drawable/Drawable;

    goto :goto_3

    :cond_3
    iget-object v1, p0, Lfb/b;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    iget-object v0, p0, Lfb/b;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v5, v6, v7, v8}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object v0, p0, Lfb/b;->c:Landroid/graphics/drawable/Drawable;

    :goto_3
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    move-object v3, p0

    move-object v4, p1

    invoke-direct/range {v3 .. v9}, Lfb/b;->L(Landroid/graphics/Canvas;IIIIF)V

    invoke-direct {p0, p1}, Lfb/b;->S(Landroid/graphics/Canvas;)V

    return-void
.end method

.method public M(Landroid/view/MotionEvent;)V
    .locals 2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    const/4 v1, 0x7

    if-eq v0, v1, :cond_4

    const/16 p1, 0x9

    if-eq v0, p1, :cond_2

    const/16 p1, 0xa

    if-eq v0, p1, :cond_0

    goto :goto_1

    :cond_0
    iget-object p1, p0, Lfb/b;->h0:[F

    const/4 v0, 0x0

    const/4 v1, 0x0

    aput v1, p1, v0

    const/4 v0, 0x1

    aput v1, p1, v0

    iget-object p1, p0, Lfb/b;->A:Lg9/g;

    invoke-virtual {p1}, Lg9/c;->i()Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lfb/b;->A:Lg9/g;

    invoke-virtual {p1}, Lg9/c;->d()V

    :cond_1
    iget-object p1, p0, Lfb/b;->B:Lg9/g;

    goto :goto_0

    :cond_2
    iget-object p1, p0, Lfb/b;->B:Lg9/g;

    invoke-virtual {p1}, Lg9/c;->i()Z

    move-result p1

    if-eqz p1, :cond_3

    iget-object p1, p0, Lfb/b;->B:Lg9/g;

    invoke-virtual {p1}, Lg9/c;->d()V

    :cond_3
    iget-object p1, p0, Lfb/b;->A:Lg9/g;

    :goto_0
    invoke-virtual {p1}, Lg9/g;->q()V

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lfb/b;->y:Landroid/widget/CompoundButton;

    invoke-direct {p0, v0, p1}, Lfb/b;->o(Landroid/view/View;Landroid/view/MotionEvent;)[F

    move-result-object p1

    iput-object p1, p0, Lfb/b;->h0:[F

    iget-object p1, p0, Lfb/b;->y:Landroid/widget/CompoundButton;

    invoke-virtual {p1}, Landroid/widget/CompoundButton;->invalidate()V

    :goto_1
    return-void
.end method

.method public O(Landroid/view/MotionEvent;)V
    .locals 7

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result p1

    float-to-int p1, p1

    iget-object v2, p0, Lfb/b;->r:Landroid/graphics/Rect;

    iget-object v3, p0, Lfb/b;->y:Landroid/widget/CompoundButton;

    invoke-static {v3}, Landroidx/appcompat/widget/s0;->b(Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget v4, p0, Lfb/b;->e:I

    iget v5, p0, Lfb/b;->k:I

    sub-int/2addr v4, v5

    iget v5, p0, Lfb/b;->g:I

    sub-int/2addr v4, v5

    goto :goto_0

    :cond_0
    iget v4, p0, Lfb/b;->k:I

    :goto_0
    if-eqz v3, :cond_1

    iget v3, p0, Lfb/b;->e:I

    iget v5, p0, Lfb/b;->k:I

    sub-int/2addr v3, v5

    goto :goto_1

    :cond_1
    iget v3, p0, Lfb/b;->k:I

    iget v5, p0, Lfb/b;->g:I

    add-int/2addr v3, v5

    :goto_1
    iget v5, p0, Lfb/b;->f:I

    const/4 v6, 0x0

    invoke-virtual {v2, v4, v6, v3, v5}, Landroid/graphics/Rect;->set(IIII)V

    const/4 v3, 0x1

    if-eqz v0, :cond_a

    const/4 v4, 0x2

    if-eq v0, v3, :cond_6

    if-eq v0, v4, :cond_5

    const/4 p1, 0x3

    if-eq v0, p1, :cond_2

    goto/16 :goto_8

    :cond_2
    invoke-direct {p0}, Lfb/b;->P()V

    iget-boolean p1, p0, Lfb/b;->n:Z

    if-eqz p1, :cond_4

    iget p1, p0, Lfb/b;->k:I

    iget v0, p0, Lfb/b;->j:I

    div-int/2addr v0, v4

    if-lt p1, v0, :cond_3

    goto :goto_2

    :cond_3
    move v3, v6

    :goto_2
    iput-boolean v3, p0, Lfb/b;->z:Z

    invoke-direct {p0, v3}, Lfb/b;->p(Z)V

    :cond_4
    :goto_3
    iput-boolean v6, p0, Lfb/b;->n:Z

    iput-boolean v6, p0, Lfb/b;->o:Z

    iget-object p1, p0, Lfb/b;->y:Landroid/widget/CompoundButton;

    invoke-virtual {p1, v6}, Landroid/widget/CompoundButton;->setPressed(Z)V

    goto/16 :goto_8

    :cond_5
    iget-boolean p1, p0, Lfb/b;->n:Z

    if-eqz p1, :cond_e

    iget p1, p0, Lfb/b;->l:I

    sub-int p1, v1, p1

    invoke-direct {p0, p1}, Lfb/b;->G(I)V

    iput v1, p0, Lfb/b;->l:I

    iget p1, p0, Lfb/b;->m:I

    sub-int/2addr v1, p1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result p1

    iget v0, p0, Lfb/b;->p:I

    if-lt p1, v0, :cond_e

    iput-boolean v3, p0, Lfb/b;->o:Z

    iget-object p1, p0, Lfb/b;->y:Landroid/widget/CompoundButton;

    invoke-virtual {p1}, Landroid/widget/CompoundButton;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    invoke-interface {p1, v3}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    goto :goto_8

    :cond_6
    iget-object v0, p0, Lfb/b;->y:Landroid/widget/CompoundButton;

    invoke-virtual {v0, v6}, Landroid/widget/CompoundButton;->playSoundEffect(I)V

    invoke-direct {p0}, Lfb/b;->P()V

    iget-boolean v0, p0, Lfb/b;->n:Z

    if-eqz v0, :cond_9

    iget-boolean v0, p0, Lfb/b;->o:Z

    if-nez v0, :cond_7

    goto :goto_5

    :cond_7
    iget v0, p0, Lfb/b;->k:I

    iget v5, p0, Lfb/b;->j:I

    div-int/2addr v5, v4

    if-lt v0, v5, :cond_8

    goto :goto_4

    :cond_8
    move v3, v6

    :goto_4
    iput-boolean v3, p0, Lfb/b;->z:Z

    invoke-direct {p0, v3}, Lfb/b;->p(Z)V

    invoke-virtual {v2, v1, p1}, Landroid/graphics/Rect;->contains(II)Z

    move-result p1

    if-eqz p1, :cond_4

    iget-object p1, p0, Lfb/b;->y:Landroid/widget/CompoundButton;

    sget v0, Lmiuix/view/c;->F:I

    sget v1, Lmiuix/view/c;->i:I

    invoke-static {p1, v0, v1}, Lmiuix/view/HapticCompat;->e(Landroid/view/View;II)Z

    goto :goto_3

    :cond_9
    :goto_5
    invoke-direct {p0}, Lfb/b;->r()V

    goto :goto_3

    :cond_a
    invoke-virtual {v2, v1, p1}, Landroid/graphics/Rect;->contains(II)Z

    move-result p1

    if-eqz p1, :cond_d

    iput-boolean v3, p0, Lfb/b;->n:Z

    iget-object p1, p0, Lfb/b;->y:Landroid/widget/CompoundButton;

    invoke-virtual {p1, v3}, Landroid/widget/CompoundButton;->setPressed(Z)V

    invoke-direct {p0}, Lfb/b;->N()V

    iget p1, p0, Lfb/b;->k:I

    iget v0, p0, Lfb/b;->i:I

    if-le p1, v0, :cond_c

    iget v0, p0, Lfb/b;->j:I

    if-lt p1, v0, :cond_b

    goto :goto_6

    :cond_b
    move v3, v6

    :cond_c
    :goto_6
    iput-boolean v3, p0, Lfb/b;->t:Z

    goto :goto_7

    :cond_d
    iput-boolean v6, p0, Lfb/b;->n:Z

    :goto_7
    iput v1, p0, Lfb/b;->l:I

    iput v1, p0, Lfb/b;->m:I

    iput-boolean v6, p0, Lfb/b;->o:Z

    :cond_e
    :goto_8
    return-void
.end method

.method public U(F)V
    .locals 0

    iput p1, p0, Lfb/b;->g0:F

    return-void
.end method

.method public V(Z)V
    .locals 1

    invoke-direct {p0}, Lfb/b;->R()V

    iput-boolean p1, p0, Lfb/b;->z:Z

    if-eqz p1, :cond_0

    iget v0, p0, Lfb/b;->j:I

    goto :goto_0

    :cond_0
    iget v0, p0, Lfb/b;->i:I

    :goto_0
    iput v0, p0, Lfb/b;->k:I

    if-eqz p1, :cond_1

    const/16 v0, 0xff

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    iput v0, p0, Lfb/b;->b:I

    if-eqz p1, :cond_2

    const/high16 p1, 0x3f800000    # 1.0f

    goto :goto_2

    :cond_2
    const/4 p1, 0x0

    :goto_2
    iput p1, p0, Lfb/b;->O:F

    iget-object p1, p0, Lfb/b;->K:Lg9/g;

    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lg9/c;->i()Z

    move-result p1

    if-eqz p1, :cond_3

    iget-object p1, p0, Lfb/b;->K:Lg9/g;

    invoke-virtual {p1}, Lg9/c;->d()V

    :cond_3
    iget-object p1, p0, Lfb/b;->H:Lg9/g;

    invoke-virtual {p1}, Lg9/c;->i()Z

    move-result p1

    if-eqz p1, :cond_4

    iget-object p1, p0, Lfb/b;->H:Lg9/g;

    invoke-virtual {p1}, Lg9/c;->d()V

    :cond_4
    iget-object p1, p0, Lfb/b;->G:Lg9/g;

    invoke-virtual {p1}, Lg9/c;->i()Z

    move-result p1

    if-eqz p1, :cond_5

    iget-object p1, p0, Lfb/b;->G:Lg9/g;

    invoke-virtual {p1}, Lg9/c;->d()V

    :cond_5
    iget-object p1, p0, Lfb/b;->y:Landroid/widget/CompoundButton;

    invoke-virtual {p1}, Landroid/widget/CompoundButton;->invalidate()V

    return-void
.end method

.method public X(I)V
    .locals 2

    iget-object v0, p0, Lfb/b;->v:Landroid/graphics/drawable/Drawable;

    instance-of v1, v0, Lmiuix/smooth/SmoothContainerDrawable;

    if-eqz v1, :cond_0

    check-cast v0, Lmiuix/smooth/SmoothContainerDrawable;

    invoke-virtual {v0, p1}, Lmiuix/smooth/SmoothContainerDrawable;->i(I)V

    :cond_0
    iget-object v0, p0, Lfb/b;->w:Landroid/graphics/drawable/Drawable;

    instance-of v1, v0, Lmiuix/smooth/SmoothContainerDrawable;

    if-eqz v1, :cond_1

    check-cast v0, Lmiuix/smooth/SmoothContainerDrawable;

    invoke-virtual {v0, p1}, Lmiuix/smooth/SmoothContainerDrawable;->i(I)V

    :cond_1
    iget-object v0, p0, Lfb/b;->x:Landroid/graphics/drawable/Drawable;

    instance-of v1, v0, Lmiuix/smooth/SmoothContainerDrawable;

    if-eqz v1, :cond_2

    check-cast v0, Lmiuix/smooth/SmoothContainerDrawable;

    invoke-virtual {v0, p1}, Lmiuix/smooth/SmoothContainerDrawable;->i(I)V

    :cond_2
    return-void
.end method

.method public Y(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V
    .locals 0

    iput-object p1, p0, Lfb/b;->q:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    return-void
.end method

.method public Z()V
    .locals 2

    iget-object v0, p0, Lfb/b;->y:Landroid/widget/CompoundButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v1, v0, Landroid/view/ViewGroup;

    if-eqz v1, :cond_0

    check-cast v0, Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setClipChildren(Z)V

    :cond_0
    return-void
.end method

.method public a0()V
    .locals 2

    invoke-virtual {p0}, Lfb/b;->z()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lfb/b;->z()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget-object v1, p0, Lfb/b;->y:Landroid/widget/CompoundButton;

    invoke-virtual {v1}, Landroid/widget/CompoundButton;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    invoke-virtual {p0}, Lfb/b;->x()Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    iget-object v1, p0, Lfb/b;->y:Landroid/widget/CompoundButton;

    invoke-virtual {v1}, Landroid/widget/CompoundButton;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/StateListDrawable;->setState([I)Z

    :cond_0
    return-void
.end method

.method public b0(I)V
    .locals 0

    iput p1, p0, Lfb/b;->k:I

    iget-object p1, p0, Lfb/b;->y:Landroid/widget/CompoundButton;

    invoke-virtual {p1}, Landroid/widget/CompoundButton;->invalidate()V

    return-void
.end method

.method public d0(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    iget-object v0, p0, Lfb/b;->s:Landroid/graphics/drawable/StateListDrawable;

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public u()F
    .locals 1

    iget v0, p0, Lfb/b;->g0:F

    return v0
.end method

.method public v()I
    .locals 1

    iget v0, p0, Lfb/b;->f:I

    return v0
.end method

.method public w()I
    .locals 1

    iget v0, p0, Lfb/b;->e:I

    return v0
.end method

.method public x()Landroid/graphics/drawable/StateListDrawable;
    .locals 1

    iget-object v0, p0, Lfb/b;->s:Landroid/graphics/drawable/StateListDrawable;

    return-object v0
.end method

.method public y()I
    .locals 1

    iget v0, p0, Lfb/b;->k:I

    return v0
.end method

.method public z()Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Lfb/b;->a:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method
