.class public abstract Lna/b;
.super Ljava/lang/Object;

# interfaces
.implements Lna/a$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lna/b$r;,
        Lna/b$q;,
        Lna/b$p;,
        Lna/b$s;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lna/b<",
        "TT;>;>",
        "Ljava/lang/Object;",
        "Lna/a$b;"
    }
.end annotation


# static fields
.field public static final A:Lna/b$s;

.field public static final n:Lna/b$s;

.field public static final o:Lna/b$s;

.field public static final p:Lna/b$s;

.field public static final q:Lna/b$s;

.field public static final r:Lna/b$s;

.field public static final s:Lna/b$s;

.field public static final t:Lna/b$s;

.field public static final u:Lna/b$s;

.field public static final v:Lna/b$s;

.field public static final w:Lna/b$s;

.field public static final x:Lna/b$s;

.field public static final y:Lna/b$s;

.field public static final z:Lna/b$s;


# instance fields
.field a:F

.field b:F

.field c:Z

.field final d:Ljava/lang/Object;

.field final e:Lna/d;

.field f:Z

.field g:F

.field h:F

.field private i:J

.field private j:F

.field private final k:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lna/b$q;",
            ">;"
        }
    .end annotation
.end field

.field private final l:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lna/b$r;",
            ">;"
        }
    .end annotation
.end field

.field private m:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lna/b$g;

    const-string v1, "translationX"

    invoke-direct {v0, v1}, Lna/b$g;-><init>(Ljava/lang/String;)V

    sput-object v0, Lna/b;->n:Lna/b$s;

    new-instance v0, Lna/b$h;

    const-string v1, "translationY"

    invoke-direct {v0, v1}, Lna/b$h;-><init>(Ljava/lang/String;)V

    sput-object v0, Lna/b;->o:Lna/b$s;

    new-instance v0, Lna/b$i;

    const-string v1, "translationZ"

    invoke-direct {v0, v1}, Lna/b$i;-><init>(Ljava/lang/String;)V

    sput-object v0, Lna/b;->p:Lna/b$s;

    new-instance v0, Lna/b$j;

    const-string v1, "scaleX"

    invoke-direct {v0, v1}, Lna/b$j;-><init>(Ljava/lang/String;)V

    sput-object v0, Lna/b;->q:Lna/b$s;

    new-instance v0, Lna/b$k;

    const-string v1, "scaleY"

    invoke-direct {v0, v1}, Lna/b$k;-><init>(Ljava/lang/String;)V

    sput-object v0, Lna/b;->r:Lna/b$s;

    new-instance v0, Lna/b$l;

    const-string v1, "rotation"

    invoke-direct {v0, v1}, Lna/b$l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lna/b;->s:Lna/b$s;

    new-instance v0, Lna/b$m;

    const-string v1, "rotationX"

    invoke-direct {v0, v1}, Lna/b$m;-><init>(Ljava/lang/String;)V

    sput-object v0, Lna/b;->t:Lna/b$s;

    new-instance v0, Lna/b$n;

    const-string v1, "rotationY"

    invoke-direct {v0, v1}, Lna/b$n;-><init>(Ljava/lang/String;)V

    sput-object v0, Lna/b;->u:Lna/b$s;

    new-instance v0, Lna/b$o;

    const-string v1, "x"

    invoke-direct {v0, v1}, Lna/b$o;-><init>(Ljava/lang/String;)V

    sput-object v0, Lna/b;->v:Lna/b$s;

    new-instance v0, Lna/b$a;

    const-string v1, "y"

    invoke-direct {v0, v1}, Lna/b$a;-><init>(Ljava/lang/String;)V

    sput-object v0, Lna/b;->w:Lna/b$s;

    new-instance v0, Lna/b$b;

    const-string v1, "z"

    invoke-direct {v0, v1}, Lna/b$b;-><init>(Ljava/lang/String;)V

    sput-object v0, Lna/b;->x:Lna/b$s;

    new-instance v0, Lna/b$c;

    const-string v1, "alpha"

    invoke-direct {v0, v1}, Lna/b$c;-><init>(Ljava/lang/String;)V

    sput-object v0, Lna/b;->y:Lna/b$s;

    new-instance v0, Lna/b$d;

    const-string v1, "scrollX"

    invoke-direct {v0, v1}, Lna/b$d;-><init>(Ljava/lang/String;)V

    sput-object v0, Lna/b;->z:Lna/b$s;

    new-instance v0, Lna/b$e;

    const-string v1, "scrollY"

    invoke-direct {v0, v1}, Lna/b$e;-><init>(Ljava/lang/String;)V

    sput-object v0, Lna/b;->A:Lna/b$s;

    return-void
.end method

.method constructor <init>(Lna/e;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lna/b;->a:F

    const v0, 0x7f7fffff    # Float.MAX_VALUE

    iput v0, p0, Lna/b;->b:F

    const/4 v1, 0x0

    iput-boolean v1, p0, Lna/b;->c:Z

    iput-boolean v1, p0, Lna/b;->f:Z

    iput v0, p0, Lna/b;->g:F

    neg-float v0, v0

    iput v0, p0, Lna/b;->h:F

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lna/b;->i:J

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lna/b;->k:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lna/b;->l:Ljava/util/ArrayList;

    const/4 v0, 0x0

    iput-object v0, p0, Lna/b;->d:Ljava/lang/Object;

    new-instance v0, Lna/b$f;

    const-string v1, "FloatValueHolder"

    invoke-direct {v0, p0, v1, p1}, Lna/b$f;-><init>(Lna/b;Ljava/lang/String;Lna/e;)V

    iput-object v0, p0, Lna/b;->e:Lna/d;

    const/high16 p1, 0x3f800000    # 1.0f

    iput p1, p0, Lna/b;->j:F

    return-void
.end method

.method private d(Z)V
    .locals 4

    const/4 v0, 0x0

    iput-boolean v0, p0, Lna/b;->f:Z

    iget-boolean v1, p0, Lna/b;->m:Z

    if-nez v1, :cond_0

    invoke-static {}, Lna/a;->d()Lna/a;

    move-result-object v1

    invoke-virtual {v1, p0}, Lna/a;->g(Lna/a$b;)V

    :cond_0
    iput-boolean v0, p0, Lna/b;->m:Z

    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lna/b;->i:J

    iput-boolean v0, p0, Lna/b;->c:Z

    :goto_0
    iget-object v1, p0, Lna/b;->k:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lna/b;->k:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lna/b;->k:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lna/b$q;

    iget v2, p0, Lna/b;->b:F

    iget v3, p0, Lna/b;->a:F

    invoke-interface {v1, p0, p1, v2, v3}, Lna/b$q;->a(Lna/b;ZFF)V

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    iget-object p1, p0, Lna/b;->k:Ljava/util/ArrayList;

    invoke-static {p1}, Lna/b;->i(Ljava/util/ArrayList;)V

    return-void
.end method

.method private e()F
    .locals 2

    iget-object v0, p0, Lna/b;->e:Lna/d;

    iget-object v1, p0, Lna/b;->d:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lna/d;->a(Ljava/lang/Object;)F

    move-result v0

    return v0
.end method

.method private static h(Ljava/util/ArrayList;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/ArrayList<",
            "TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p0, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result p1

    if-ltz p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method private static i(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/ArrayList<",
            "TT;>;)V"
        }
    .end annotation

    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private s(Z)V
    .locals 2

    iget-boolean v0, p0, Lna/b;->f:Z

    if-nez v0, :cond_2

    iput-boolean p1, p0, Lna/b;->m:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lna/b;->f:Z

    iget-boolean v0, p0, Lna/b;->c:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lna/b;->e()F

    move-result v0

    iput v0, p0, Lna/b;->b:F

    :cond_0
    iget v0, p0, Lna/b;->b:F

    iget v1, p0, Lna/b;->g:F

    cmpl-float v1, v0, v1

    if-gtz v1, :cond_1

    iget v1, p0, Lna/b;->h:F

    cmpg-float v0, v0, v1

    if-ltz v0, :cond_1

    if-nez p1, :cond_2

    invoke-static {}, Lna/a;->d()Lna/a;

    move-result-object p1

    const-wide/16 v0, 0x0

    invoke-virtual {p1, p0, v0, v1}, Lna/a;->a(Lna/a$b;J)V

    goto :goto_0

    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Starting value("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lna/b;->b:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ") need to be in between min value("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lna/b;->h:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ") and max value("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lna/b;->g:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    :goto_0
    return-void
.end method


# virtual methods
.method public a(J)Z
    .locals 4

    iget-wide v0, p0, Lna/b;->i:J

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    const/4 v3, 0x0

    if-nez v2, :cond_0

    iput-wide p1, p0, Lna/b;->i:J

    iget p1, p0, Lna/b;->b:F

    invoke-virtual {p0, p1}, Lna/b;->n(F)V

    return v3

    :cond_0
    sub-long v0, p1, v0

    iput-wide p1, p0, Lna/b;->i:J

    invoke-virtual {p0, v0, v1}, Lna/b;->t(J)Z

    move-result p1

    iget p2, p0, Lna/b;->b:F

    iget v0, p0, Lna/b;->g:F

    invoke-static {p2, v0}, Ljava/lang/Math;->min(FF)F

    move-result p2

    iput p2, p0, Lna/b;->b:F

    iget v0, p0, Lna/b;->h:F

    invoke-static {p2, v0}, Ljava/lang/Math;->max(FF)F

    move-result p2

    iput p2, p0, Lna/b;->b:F

    invoke-virtual {p0, p2}, Lna/b;->n(F)V

    if-eqz p1, :cond_1

    invoke-direct {p0, v3}, Lna/b;->d(Z)V

    :cond_1
    return p1
.end method

.method public b(Lna/b$r;)Lna/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lna/b$r;",
            ")TT;"
        }
    .end annotation

    invoke-virtual {p0}, Lna/b;->g()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lna/b;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lna/b;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-object p0

    :cond_1
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "Error: Update listeners must be added beforethe animation."

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public c()V
    .locals 2

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_1

    iget-boolean v0, p0, Lna/b;->f:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lna/b;->d(Z)V

    :cond_0
    return-void

    :cond_1
    new-instance v0, Landroid/util/AndroidRuntimeException;

    const-string v1, "Animations may only be canceled on the main thread"

    invoke-direct {v0, v1}, Landroid/util/AndroidRuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method f()F
    .locals 2

    goto/32 :goto_2

    nop

    :goto_0
    mul-float/2addr v0, v1

    goto/32 :goto_1

    nop

    :goto_1
    return v0

    :goto_2
    iget v0, p0, Lna/b;->j:F

    goto/32 :goto_3

    nop

    :goto_3
    const/high16 v1, 0x3f400000    # 0.75f

    goto/32 :goto_0

    nop
.end method

.method public g()Z
    .locals 1

    iget-boolean v0, p0, Lna/b;->f:Z

    return v0
.end method

.method public j(Lna/b$r;)V
    .locals 1

    iget-object v0, p0, Lna/b;->l:Ljava/util/ArrayList;

    invoke-static {v0, p1}, Lna/b;->h(Ljava/util/ArrayList;Ljava/lang/Object;)V

    return-void
.end method

.method public k(F)Lna/b;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(F)TT;"
        }
    .end annotation

    iput p1, p0, Lna/b;->g:F

    return-object p0
.end method

.method public l(F)Lna/b;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(F)TT;"
        }
    .end annotation

    iput p1, p0, Lna/b;->h:F

    return-object p0
.end method

.method public m(F)Lna/b;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(F)TT;"
        }
    .end annotation

    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-lez v0, :cond_0

    iput p1, p0, Lna/b;->j:F

    const/high16 v0, 0x3f400000    # 0.75f

    mul-float/2addr p1, v0

    invoke-virtual {p0, p1}, Lna/b;->q(F)V

    return-object p0

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Minimum visible change must be positive."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method n(F)V
    .locals 3

    goto/32 :goto_2

    nop

    :goto_0
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_4

    nop

    :goto_1
    return-void

    :goto_2
    iget-object v0, p0, Lna/b;->e:Lna/d;

    goto/32 :goto_6

    nop

    :goto_3
    iget-object v0, p0, Lna/b;->l:Ljava/util/ArrayList;

    goto/32 :goto_a

    nop

    :goto_4
    if-nez v0, :cond_0

    goto/32 :goto_16

    :cond_0
    goto/32 :goto_3

    nop

    :goto_5
    iget-object v0, p0, Lna/b;->l:Ljava/util/ArrayList;

    goto/32 :goto_0

    nop

    :goto_6
    iget-object v1, p0, Lna/b;->d:Ljava/lang/Object;

    goto/32 :goto_9

    nop

    :goto_7
    iget-object p1, p0, Lna/b;->l:Ljava/util/ArrayList;

    goto/32 :goto_8

    nop

    :goto_8
    invoke-static {p1}, Lna/b;->i(Ljava/util/ArrayList;)V

    goto/32 :goto_1

    nop

    :goto_9
    invoke-virtual {v0, v1, p1}, Lna/d;->b(Ljava/lang/Object;F)V

    goto/32 :goto_10

    nop

    :goto_a
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_f

    nop

    :goto_b
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto/32 :goto_c

    nop

    :goto_c
    if-lt p1, v0, :cond_1

    goto/32 :goto_14

    :cond_1
    goto/32 :goto_5

    nop

    :goto_d
    iget v2, p0, Lna/b;->a:F

    goto/32 :goto_15

    nop

    :goto_e
    add-int/lit8 p1, p1, 0x1

    goto/32 :goto_13

    nop

    :goto_f
    check-cast v0, Lna/b$r;

    goto/32 :goto_12

    nop

    :goto_10
    const/4 p1, 0x0

    :goto_11
    goto/32 :goto_17

    nop

    :goto_12
    iget v1, p0, Lna/b;->b:F

    goto/32 :goto_d

    nop

    :goto_13
    goto :goto_11

    :goto_14
    goto/32 :goto_7

    nop

    :goto_15
    invoke-interface {v0, p0, v1, v2}, Lna/b$r;->a(Lna/b;FF)V

    :goto_16
    goto/32 :goto_e

    nop

    :goto_17
    iget-object v0, p0, Lna/b;->l:Ljava/util/ArrayList;

    goto/32 :goto_b

    nop
.end method

.method public o(F)Lna/b;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(F)TT;"
        }
    .end annotation

    iput p1, p0, Lna/b;->b:F

    const/4 p1, 0x1

    iput-boolean p1, p0, Lna/b;->c:Z

    return-object p0
.end method

.method public p(F)Lna/b;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(F)TT;"
        }
    .end annotation

    iput p1, p0, Lna/b;->a:F

    return-object p0
.end method

.method abstract q(F)V
.end method

.method public r(Z)V
    .locals 2

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_1

    iget-boolean v0, p0, Lna/b;->f:Z

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lna/b;->s(Z)V

    :cond_0
    return-void

    :cond_1
    new-instance p1, Landroid/util/AndroidRuntimeException;

    const-string v0, "Animations may only be started on the main thread"

    invoke-direct {p1, v0}, Landroid/util/AndroidRuntimeException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method abstract t(J)Z
.end method
