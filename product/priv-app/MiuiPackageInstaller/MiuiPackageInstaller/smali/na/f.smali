.class public final Lna/f;
.super Lna/b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lna/b<",
        "Lna/f;",
        ">;"
    }
.end annotation


# instance fields
.field private B:Lna/g;

.field private C:F

.field private D:Z


# direct methods
.method public constructor <init>(Lna/e;)V
    .locals 0

    invoke-direct {p0, p1}, Lna/b;-><init>(Lna/e;)V

    const/4 p1, 0x0

    iput-object p1, p0, Lna/f;->B:Lna/g;

    const p1, 0x7f7fffff    # Float.MAX_VALUE

    iput p1, p0, Lna/f;->C:F

    const/4 p1, 0x0

    iput-boolean p1, p0, Lna/f;->D:Z

    return-void
.end method

.method private w()V
    .locals 4

    iget-object v0, p0, Lna/f;->B:Lna/g;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lna/g;->a()F

    move-result v0

    float-to-double v0, v0

    iget v2, p0, Lna/b;->g:F

    float-to-double v2, v2

    cmpl-double v2, v0, v2

    if-gtz v2, :cond_1

    iget v2, p0, Lna/b;->h:F

    float-to-double v2, v2

    cmpg-double v0, v0, v2

    if-ltz v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Final position of the spring cannot be less than the min value."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Final position of the spring cannot be greater than the max value."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Incomplete SpringAnimation: Either final position or a spring force needs to be set."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method q(F)V
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    return-void
.end method

.method public r(Z)V
    .locals 3

    invoke-direct {p0}, Lna/f;->w()V

    iget-object v0, p0, Lna/f;->B:Lna/g;

    invoke-virtual {p0}, Lna/b;->f()F

    move-result v1

    float-to-double v1, v1

    invoke-virtual {v0, v1, v2}, Lna/g;->h(D)V

    invoke-super {p0, p1}, Lna/b;->r(Z)V

    return-void
.end method

.method t(J)Z
    .locals 20

    goto/32 :goto_c

    nop

    :goto_0
    float-to-double v5, v1

    goto/32 :goto_46

    nop

    :goto_1
    if-nez v1, :cond_0

    goto/32 :goto_48

    :cond_0
    goto/32 :goto_30

    nop

    :goto_2
    iget v1, v0, Lna/b;->b:F

    goto/32 :goto_26

    nop

    :goto_3
    iput v5, v0, Lna/f;->C:F

    goto/32 :goto_3d

    nop

    :goto_4
    invoke-virtual {v1}, Lna/g;->a()F

    move-result v1

    goto/32 :goto_7

    nop

    :goto_5
    invoke-virtual {v6, v7}, Lna/g;->e(F)Lna/g;

    goto/32 :goto_3

    nop

    :goto_6
    const/4 v3, 0x0

    goto/32 :goto_2d

    nop

    :goto_7
    iput v1, v0, Lna/b;->b:F

    goto/32 :goto_3e

    nop

    :goto_8
    invoke-virtual {v1}, Lna/g;->a()F

    move-result v1

    goto/32 :goto_4a

    nop

    :goto_9
    iget v1, v0, Lna/b;->b:F

    goto/32 :goto_49

    nop

    :goto_a
    iget v1, v0, Lna/b;->b:F

    goto/32 :goto_23

    nop

    :goto_b
    invoke-virtual/range {v6 .. v12}, Lna/g;->i(DDJ)Lna/b$p;

    move-result-object v1

    goto/32 :goto_24

    nop

    :goto_c
    move-object/from16 v0, p0

    goto/32 :goto_1f

    nop

    :goto_d
    iget v5, v1, Lna/b$p;->a:F

    goto/32 :goto_32

    nop

    :goto_e
    return v2

    :goto_f
    goto/32 :goto_3b

    nop

    :goto_10
    cmpl-float v1, v1, v5

    goto/32 :goto_1

    nop

    :goto_11
    iget-object v1, v0, Lna/f;->B:Lna/g;

    goto/32 :goto_8

    nop

    :goto_12
    iget-object v13, v0, Lna/f;->B:Lna/g;

    goto/32 :goto_a

    nop

    :goto_13
    invoke-virtual {v6, v1}, Lna/g;->e(F)Lna/g;

    goto/32 :goto_17

    nop

    :goto_14
    iget v1, v0, Lna/b;->a:F

    goto/32 :goto_0

    nop

    :goto_15
    if-nez v6, :cond_1

    goto/32 :goto_18

    :cond_1
    goto/32 :goto_36

    nop

    :goto_16
    iput v5, v0, Lna/b;->b:F

    goto/32 :goto_39

    nop

    :goto_17
    iput v5, v0, Lna/f;->C:F

    :goto_18
    goto/32 :goto_11

    nop

    :goto_19
    iget v1, v0, Lna/f;->C:F

    goto/32 :goto_10

    nop

    :goto_1a
    div-long v18, p1, v11

    goto/32 :goto_37

    nop

    :goto_1b
    iget-object v1, v0, Lna/f;->B:Lna/g;

    goto/32 :goto_4

    nop

    :goto_1c
    iget v5, v1, Lna/b$p;->a:F

    goto/32 :goto_16

    nop

    :goto_1d
    invoke-virtual {v1}, Lna/g;->a()F

    goto/32 :goto_45

    nop

    :goto_1e
    const-wide/16 v11, 0x2

    goto/32 :goto_1a

    nop

    :goto_1f
    iget-boolean v1, v0, Lna/f;->D:Z

    goto/32 :goto_44

    nop

    :goto_20
    invoke-static {v1, v5}, Ljava/lang/Math;->max(FF)F

    move-result v1

    goto/32 :goto_27

    nop

    :goto_21
    if-nez v1, :cond_2

    goto/32 :goto_f

    :cond_2
    goto/32 :goto_1b

    nop

    :goto_22
    iget v1, v0, Lna/f;->C:F

    goto/32 :goto_41

    nop

    :goto_23
    float-to-double v14, v1

    goto/32 :goto_14

    nop

    :goto_24
    iget-object v6, v0, Lna/f;->B:Lna/g;

    goto/32 :goto_35

    nop

    :goto_25
    invoke-virtual/range {v13 .. v19}, Lna/g;->i(DDJ)Lna/b$p;

    move-result-object v1

    goto/32 :goto_1c

    nop

    :goto_26
    float-to-double v7, v1

    goto/32 :goto_34

    nop

    :goto_27
    iput v1, v0, Lna/b;->b:F

    goto/32 :goto_2e

    nop

    :goto_28
    move-wide/from16 v18, p1

    :goto_29
    goto/32 :goto_25

    nop

    :goto_2a
    invoke-virtual {v0, v1, v5}, Lna/f;->v(FF)Z

    move-result v1

    goto/32 :goto_21

    nop

    :goto_2b
    iput v1, v0, Lna/b;->a:F

    goto/32 :goto_9

    nop

    :goto_2c
    iput-boolean v3, v0, Lna/f;->D:Z

    goto/32 :goto_42

    nop

    :goto_2d
    const/4 v4, 0x0

    goto/32 :goto_3a

    nop

    :goto_2e
    iget v5, v0, Lna/b;->g:F

    goto/32 :goto_33

    nop

    :goto_2f
    if-nez v1, :cond_3

    goto/32 :goto_43

    :cond_3
    goto/32 :goto_22

    nop

    :goto_30
    iget-object v1, v0, Lna/f;->B:Lna/g;

    goto/32 :goto_1d

    nop

    :goto_31
    iget v1, v1, Lna/b$p;->b:F

    goto/32 :goto_38

    nop

    :goto_32
    float-to-double v14, v5

    goto/32 :goto_31

    nop

    :goto_33
    invoke-static {v1, v5}, Ljava/lang/Math;->min(FF)F

    move-result v1

    goto/32 :goto_40

    nop

    :goto_34
    iget v1, v0, Lna/b;->a:F

    goto/32 :goto_4c

    nop

    :goto_35
    iget v7, v0, Lna/f;->C:F

    goto/32 :goto_5

    nop

    :goto_36
    iget-object v6, v0, Lna/f;->B:Lna/g;

    goto/32 :goto_13

    nop

    :goto_37
    move-wide/from16 v11, v18

    goto/32 :goto_b

    nop

    :goto_38
    float-to-double v5, v1

    goto/32 :goto_4b

    nop

    :goto_39
    iget v1, v1, Lna/b$p;->b:F

    goto/32 :goto_2b

    nop

    :goto_3a
    const v5, 0x7f7fffff    # Float.MAX_VALUE

    goto/32 :goto_2f

    nop

    :goto_3b
    return v3

    :goto_3c
    iput v4, v0, Lna/b;->a:F

    goto/32 :goto_2c

    nop

    :goto_3d
    iget-object v13, v0, Lna/f;->B:Lna/g;

    goto/32 :goto_d

    nop

    :goto_3e
    iput v4, v0, Lna/b;->a:F

    goto/32 :goto_e

    nop

    :goto_3f
    iget v5, v0, Lna/b;->a:F

    goto/32 :goto_2a

    nop

    :goto_40
    iput v1, v0, Lna/b;->b:F

    goto/32 :goto_3f

    nop

    :goto_41
    cmpl-float v6, v1, v5

    goto/32 :goto_15

    nop

    :goto_42
    return v2

    :goto_43
    goto/32 :goto_19

    nop

    :goto_44
    const/4 v2, 0x1

    goto/32 :goto_6

    nop

    :goto_45
    iget-object v6, v0, Lna/f;->B:Lna/g;

    goto/32 :goto_2

    nop

    :goto_46
    move-wide/from16 v16, v5

    goto/32 :goto_28

    nop

    :goto_47
    goto/16 :goto_29

    :goto_48
    goto/32 :goto_12

    nop

    :goto_49
    iget v5, v0, Lna/b;->h:F

    goto/32 :goto_20

    nop

    :goto_4a
    iput v1, v0, Lna/b;->b:F

    goto/32 :goto_3c

    nop

    :goto_4b
    move-wide/from16 v16, v5

    goto/32 :goto_47

    nop

    :goto_4c
    float-to-double v9, v1

    goto/32 :goto_1e

    nop
.end method

.method public u()Lna/g;
    .locals 1

    iget-object v0, p0, Lna/f;->B:Lna/g;

    return-object v0
.end method

.method v(FF)Z
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return p1

    :goto_1
    iget-object v0, p0, Lna/f;->B:Lna/g;

    goto/32 :goto_2

    nop

    :goto_2
    invoke-virtual {v0, p1, p2}, Lna/g;->c(FF)Z

    move-result p1

    goto/32 :goto_0

    nop
.end method

.method public x(Lna/g;)Lna/f;
    .locals 0

    iput-object p1, p0, Lna/f;->B:Lna/g;

    return-object p0
.end method
