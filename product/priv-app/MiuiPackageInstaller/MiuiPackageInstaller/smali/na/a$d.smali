.class Lna/a$d;
.super Lna/a$c;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lna/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "d"
.end annotation


# instance fields
.field private final b:Landroid/view/Choreographer;

.field private final c:Landroid/view/Choreographer$FrameCallback;


# direct methods
.method constructor <init>(Lna/a$a;)V
    .locals 0

    invoke-direct {p0, p1}, Lna/a$c;-><init>(Lna/a$a;)V

    invoke-static {}, Landroid/view/Choreographer;->getInstance()Landroid/view/Choreographer;

    move-result-object p1

    iput-object p1, p0, Lna/a$d;->b:Landroid/view/Choreographer;

    new-instance p1, Lna/a$d$a;

    invoke-direct {p1, p0}, Lna/a$d$a;-><init>(Lna/a$d;)V

    iput-object p1, p0, Lna/a$d;->c:Landroid/view/Choreographer$FrameCallback;

    return-void
.end method


# virtual methods
.method a()V
    .locals 2

    goto/32 :goto_2

    nop

    :goto_0
    iget-object v1, p0, Lna/a$d;->c:Landroid/view/Choreographer$FrameCallback;

    goto/32 :goto_1

    nop

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/Choreographer;->postFrameCallback(Landroid/view/Choreographer$FrameCallback;)V

    goto/32 :goto_3

    nop

    :goto_2
    iget-object v0, p0, Lna/a$d;->b:Landroid/view/Choreographer;

    goto/32 :goto_0

    nop

    :goto_3
    return-void
.end method
