.class final Lna/c$a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lna/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "a"
.end annotation


# instance fields
.field private a:F

.field private b:F

.field private final c:Lna/b$p;

.field private d:D

.field private final e:F


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const v0, -0x3f79999a    # -4.2f

    iput v0, p0, Lna/c$a;->a:F

    new-instance v0, Lna/b$p;

    invoke-direct {v0}, Lna/b$p;-><init>()V

    iput-object v0, p0, Lna/c$a;->c:Lna/b$p;

    const/high16 v0, 0x447a0000    # 1000.0f

    iput v0, p0, Lna/c$a;->e:F

    return-void
.end method

.method static synthetic a(Lna/c$a;)F
    .locals 0

    iget p0, p0, Lna/c$a;->a:F

    return p0
.end method

.method static synthetic b(Lna/c$a;)F
    .locals 0

    iget p0, p0, Lna/c$a;->b:F

    return p0
.end method


# virtual methods
.method public c(FF)Z
    .locals 0

    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result p1

    iget p2, p0, Lna/c$a;->b:F

    cmpg-float p1, p1, p2

    if-gez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method d(F)V
    .locals 4

    goto/32 :goto_1

    nop

    :goto_0
    const-wide v2, 0x4005bf0a8b145769L    # Math.E

    goto/32 :goto_7

    nop

    :goto_1
    const v0, -0x3f79999a    # -4.2f

    goto/32 :goto_9

    nop

    :goto_2
    iput p1, p0, Lna/c$a;->a:F

    goto/32 :goto_6

    nop

    :goto_3
    iput-wide v2, p0, Lna/c$a;->d:D

    goto/32 :goto_4

    nop

    :goto_4
    return-void

    :goto_5
    sub-double/2addr v2, v0

    goto/32 :goto_3

    nop

    :goto_6
    float-to-double v0, p1

    goto/32 :goto_0

    nop

    :goto_7
    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    goto/32 :goto_8

    nop

    :goto_8
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    goto/32 :goto_5

    nop

    :goto_9
    mul-float/2addr p1, v0

    goto/32 :goto_2

    nop
.end method

.method e(F)V
    .locals 1

    goto/32 :goto_3

    nop

    :goto_0
    iput p1, p0, Lna/c$a;->b:F

    goto/32 :goto_2

    nop

    :goto_1
    mul-float/2addr p1, v0

    goto/32 :goto_0

    nop

    :goto_2
    return-void

    :goto_3
    const/high16 v0, 0x427a0000    # 62.5f

    goto/32 :goto_1

    nop
.end method

.method f(FFJ)Lna/b$p;
    .locals 4

    goto/32 :goto_5

    nop

    :goto_0
    iput p1, p4, Lna/b$p;->a:F

    goto/32 :goto_19

    nop

    :goto_1
    iget-object p1, p0, Lna/c$a;->c:Lna/b$p;

    goto/32 :goto_3

    nop

    :goto_2
    mul-float/2addr p3, p2

    goto/32 :goto_15

    nop

    :goto_3
    const/4 p2, 0x0

    goto/32 :goto_10

    nop

    :goto_4
    iget-object p1, p0, Lna/c$a;->c:Lna/b$p;

    goto/32 :goto_f

    nop

    :goto_5
    const-wide/16 v0, 0x10

    goto/32 :goto_9

    nop

    :goto_6
    sub-double/2addr v2, v0

    goto/32 :goto_b

    nop

    :goto_7
    mul-double/2addr v2, v0

    goto/32 :goto_17

    nop

    :goto_8
    iget-wide v0, p0, Lna/c$a;->d:D

    goto/32 :goto_e

    nop

    :goto_9
    invoke-static {p3, p4, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide p3

    goto/32 :goto_8

    nop

    :goto_a
    const/high16 p4, 0x447a0000    # 1000.0f

    goto/32 :goto_18

    nop

    :goto_b
    long-to-float p3, p3

    goto/32 :goto_a

    nop

    :goto_c
    float-to-double v2, p2

    goto/32 :goto_7

    nop

    :goto_d
    iget-object p4, p0, Lna/c$a;->c:Lna/b$p;

    goto/32 :goto_c

    nop

    :goto_e
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    goto/32 :goto_6

    nop

    :goto_f
    return-object p1

    :goto_10
    iput p2, p1, Lna/b$p;->b:F

    :goto_11
    goto/32 :goto_4

    nop

    :goto_12
    float-to-double v0, p3

    goto/32 :goto_16

    nop

    :goto_13
    if-nez p1, :cond_0

    goto/32 :goto_11

    :cond_0
    goto/32 :goto_1

    nop

    :goto_14
    iput p2, p4, Lna/b$p;->b:F

    goto/32 :goto_2

    nop

    :goto_15
    add-float/2addr p1, p3

    goto/32 :goto_0

    nop

    :goto_16
    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    goto/32 :goto_d

    nop

    :goto_17
    double-to-float p2, v2

    goto/32 :goto_14

    nop

    :goto_18
    div-float/2addr p3, p4

    goto/32 :goto_12

    nop

    :goto_19
    invoke-virtual {p0, p1, p2}, Lna/c$a;->c(FF)Z

    move-result p1

    goto/32 :goto_13

    nop
.end method
