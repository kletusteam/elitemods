.class public final Lna/c;
.super Lna/b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lna/c$a;,
        Lna/c$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lna/b<",
        "Lna/c;",
        ">;"
    }
.end annotation


# instance fields
.field private final B:Lna/c$a;

.field private C:Lna/c$b;


# direct methods
.method public constructor <init>(Lna/e;Lna/c$b;)V
    .locals 1

    invoke-direct {p0, p1}, Lna/b;-><init>(Lna/e;)V

    new-instance p1, Lna/c$a;

    invoke-direct {p1}, Lna/c$a;-><init>()V

    iput-object p1, p0, Lna/c;->B:Lna/c$a;

    invoke-virtual {p0}, Lna/b;->f()F

    move-result v0

    invoke-virtual {p1, v0}, Lna/c$a;->e(F)V

    iput-object p2, p0, Lna/c;->C:Lna/c$b;

    return-void
.end method

.method private y(F)F
    .locals 4

    iget v0, p0, Lna/b;->a:F

    div-float/2addr p1, v0

    float-to-double v0, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    const-wide v2, 0x408f400000000000L    # 1000.0

    mul-double/2addr v0, v2

    iget-object p1, p0, Lna/c;->B:Lna/c$a;

    invoke-static {p1}, Lna/c$a;->a(Lna/c$a;)F

    move-result p1

    float-to-double v2, p1

    div-double/2addr v0, v2

    double-to-float p1, v0

    return p1
.end method


# virtual methods
.method public A(F)Lna/c;
    .locals 0

    invoke-super {p0, p1}, Lna/b;->k(F)Lna/b;

    return-object p0
.end method

.method public B(F)Lna/c;
    .locals 0

    invoke-super {p0, p1}, Lna/b;->l(F)Lna/b;

    return-object p0
.end method

.method public C(F)Lna/c;
    .locals 0

    invoke-super {p0, p1}, Lna/b;->p(F)Lna/b;

    return-object p0
.end method

.method public bridge synthetic k(F)Lna/b;
    .locals 0

    invoke-virtual {p0, p1}, Lna/c;->A(F)Lna/c;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic l(F)Lna/b;
    .locals 0

    invoke-virtual {p0, p1}, Lna/c;->B(F)Lna/c;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic p(F)Lna/b;
    .locals 0

    invoke-virtual {p0, p1}, Lna/c;->C(F)Lna/c;

    move-result-object p1

    return-object p1
.end method

.method q(F)V
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    invoke-virtual {v0, p1}, Lna/c$a;->e(F)V

    goto/32 :goto_2

    nop

    :goto_1
    iget-object v0, p0, Lna/c;->B:Lna/c$a;

    goto/32 :goto_0

    nop

    :goto_2
    return-void
.end method

.method t(J)Z
    .locals 3

    goto/32 :goto_a

    nop

    :goto_0
    iget v0, p0, Lna/b;->h:F

    goto/32 :goto_2

    nop

    :goto_1
    float-to-int p2, p2

    goto/32 :goto_f

    nop

    :goto_2
    cmpg-float v1, p2, v0

    goto/32 :goto_6

    nop

    :goto_3
    if-nez p1, :cond_0

    goto/32 :goto_1c

    :cond_0
    goto/32 :goto_b

    nop

    :goto_4
    return v2

    :goto_5
    goto/32 :goto_12

    nop

    :goto_6
    const/4 v2, 0x1

    goto/32 :goto_e

    nop

    :goto_7
    iget p2, p0, Lna/b;->b:F

    goto/32 :goto_1

    nop

    :goto_8
    iput v0, p0, Lna/b;->b:F

    goto/32 :goto_17

    nop

    :goto_9
    iget v1, p0, Lna/b;->b:F

    goto/32 :goto_19

    nop

    :goto_a
    iget-object v0, p0, Lna/c;->B:Lna/c$a;

    goto/32 :goto_9

    nop

    :goto_b
    iget-object p1, p0, Lna/c;->C:Lna/c$b;

    goto/32 :goto_7

    nop

    :goto_c
    iput p1, p0, Lna/b;->a:F

    goto/32 :goto_0

    nop

    :goto_d
    iget v0, p0, Lna/b;->g:F

    goto/32 :goto_1e

    nop

    :goto_e
    if-ltz v1, :cond_1

    goto/32 :goto_18

    :cond_1
    goto/32 :goto_8

    nop

    :goto_f
    invoke-interface {p1, p2}, Lna/c$b;->a(I)V

    goto/32 :goto_1b

    nop

    :goto_10
    iget p1, p1, Lna/b$p;->b:F

    goto/32 :goto_c

    nop

    :goto_11
    if-gtz v1, :cond_2

    goto/32 :goto_5

    :cond_2
    goto/32 :goto_14

    nop

    :goto_12
    invoke-virtual {p0, p2, p1}, Lna/c;->u(FF)Z

    move-result p1

    goto/32 :goto_3

    nop

    :goto_13
    return p1

    :goto_14
    iput v0, p0, Lna/b;->b:F

    goto/32 :goto_4

    nop

    :goto_15
    iput p2, p0, Lna/b;->b:F

    goto/32 :goto_10

    nop

    :goto_16
    const/4 p1, 0x0

    goto/32 :goto_13

    nop

    :goto_17
    return v2

    :goto_18
    goto/32 :goto_d

    nop

    :goto_19
    iget v2, p0, Lna/b;->a:F

    goto/32 :goto_1a

    nop

    :goto_1a
    invoke-virtual {v0, v1, v2, p1, p2}, Lna/c$a;->f(FFJ)Lna/b$p;

    move-result-object p1

    goto/32 :goto_1d

    nop

    :goto_1b
    return v2

    :goto_1c
    goto/32 :goto_16

    nop

    :goto_1d
    iget p2, p1, Lna/b$p;->a:F

    goto/32 :goto_15

    nop

    :goto_1e
    cmpl-float v1, p2, v0

    goto/32 :goto_11

    nop
.end method

.method u(FF)Z
    .locals 1

    goto/32 :goto_8

    nop

    :goto_0
    cmpg-float v0, p1, v0

    goto/32 :goto_c

    nop

    :goto_1
    goto :goto_5

    :goto_2
    goto/32 :goto_f

    nop

    :goto_3
    if-nez p1, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_1

    nop

    :goto_4
    goto :goto_b

    :goto_5
    goto/32 :goto_a

    nop

    :goto_6
    iget v0, p0, Lna/b;->h:F

    goto/32 :goto_0

    nop

    :goto_7
    invoke-virtual {v0, p1, p2}, Lna/c$a;->c(FF)Z

    move-result p1

    goto/32 :goto_3

    nop

    :goto_8
    iget v0, p0, Lna/b;->g:F

    goto/32 :goto_9

    nop

    :goto_9
    cmpl-float v0, p1, v0

    goto/32 :goto_10

    nop

    :goto_a
    const/4 p1, 0x1

    :goto_b
    goto/32 :goto_e

    nop

    :goto_c
    if-gtz v0, :cond_1

    goto/32 :goto_5

    :cond_1
    goto/32 :goto_d

    nop

    :goto_d
    iget-object v0, p0, Lna/c;->B:Lna/c$a;

    goto/32 :goto_7

    nop

    :goto_e
    return p1

    :goto_f
    const/4 p1, 0x0

    goto/32 :goto_4

    nop

    :goto_10
    if-ltz v0, :cond_2

    goto/32 :goto_5

    :cond_2
    goto/32 :goto_6

    nop
.end method

.method public v()F
    .locals 2

    iget v0, p0, Lna/b;->a:F

    invoke-static {v0}, Ljava/lang/Math;->signum(F)F

    move-result v0

    iget-object v1, p0, Lna/c;->B:Lna/c$a;

    invoke-static {v1}, Lna/c$a;->b(Lna/c$a;)F

    move-result v1

    mul-float/2addr v0, v1

    invoke-direct {p0, v0}, Lna/c;->y(F)F

    move-result v0

    return v0
.end method

.method public w()F
    .locals 4

    iget v0, p0, Lna/b;->a:F

    invoke-static {v0}, Ljava/lang/Math;->signum(F)F

    move-result v0

    iget v1, p0, Lna/b;->b:F

    iget v2, p0, Lna/b;->a:F

    iget-object v3, p0, Lna/c;->B:Lna/c$a;

    invoke-static {v3}, Lna/c$a;->a(Lna/c$a;)F

    move-result v3

    div-float/2addr v2, v3

    sub-float/2addr v1, v2

    iget-object v2, p0, Lna/c;->B:Lna/c$a;

    invoke-static {v2}, Lna/c$a;->b(Lna/c$a;)F

    move-result v2

    mul-float/2addr v0, v2

    iget-object v2, p0, Lna/c;->B:Lna/c$a;

    invoke-static {v2}, Lna/c$a;->a(Lna/c$a;)F

    move-result v2

    div-float/2addr v0, v2

    add-float/2addr v1, v0

    return v1
.end method

.method public x(F)F
    .locals 2

    iget v0, p0, Lna/b;->b:F

    sub-float/2addr p1, v0

    iget v0, p0, Lna/b;->a:F

    iget-object v1, p0, Lna/c;->B:Lna/c$a;

    invoke-static {v1}, Lna/c$a;->a(Lna/c$a;)F

    move-result v1

    div-float/2addr v0, v1

    add-float/2addr p1, v0

    iget-object v0, p0, Lna/c;->B:Lna/c$a;

    invoke-static {v0}, Lna/c$a;->a(Lna/c$a;)F

    move-result v0

    mul-float/2addr p1, v0

    invoke-direct {p0, p1}, Lna/c;->y(F)F

    move-result p1

    return p1
.end method

.method public z(F)Lna/c;
    .locals 1

    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lna/c;->B:Lna/c$a;

    invoke-virtual {v0, p1}, Lna/c$a;->d(F)V

    return-object p0

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Friction must be positive"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
