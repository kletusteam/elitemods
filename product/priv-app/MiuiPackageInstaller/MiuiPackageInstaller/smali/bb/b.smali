.class public Lbb/b;
.super Lbb/a;


# static fields
.field public static v:Landroid/view/View$OnAttachStateChangeListener;

.field public static w:Lc9/a;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lbb/b$a;

    invoke-direct {v0}, Lbb/b$a;-><init>()V

    sput-object v0, Lbb/b;->v:Landroid/view/View$OnAttachStateChangeListener;

    new-instance v0, Lc9/a;

    invoke-direct {v0}, Lc9/a;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lc9/a;->m(F)Lc9/a;

    move-result-object v0

    sput-object v0, Lbb/b;->w:Lc9/a;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lbb/a;-><init>()V

    return-void
.end method


# virtual methods
.method R(Landroidx/recyclerview/widget/RecyclerView$d0;)V
    .locals 8

    goto/32 :goto_12

    nop

    :goto_0
    const/4 v3, 0x0

    goto/32 :goto_10

    nop

    :goto_1
    new-array v2, v7, [Ljava/lang/Object;

    goto/32 :goto_b

    nop

    :goto_2
    const/high16 v5, 0x3f800000    # 1.0f

    goto/32 :goto_1d

    nop

    :goto_3
    new-array v2, v2, [Ljava/lang/Object;

    goto/32 :goto_8

    nop

    :goto_4
    return-void

    :goto_5
    invoke-static {v1}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object v1

    goto/32 :goto_c

    nop

    :goto_6
    aput-object v6, v2, v7

    goto/32 :goto_1e

    nop

    :goto_7
    new-array v1, v0, [Landroid/view/View;

    goto/32 :goto_1a

    nop

    :goto_8
    sget-object v4, Lh9/h;->o:Lh9/h;

    goto/32 :goto_15

    nop

    :goto_9
    new-instance v3, Lbb/b$d;

    goto/32 :goto_1f

    nop

    :goto_a
    aput-object v5, v2, v0

    goto/32 :goto_18

    nop

    :goto_b
    aput-object v4, v2, v3

    goto/32 :goto_d

    nop

    :goto_c
    invoke-interface {v1}, Lmiuix/animation/e;->b()Lmiuix/animation/h;

    move-result-object v1

    goto/32 :goto_1b

    nop

    :goto_d
    aput-object v5, v2, v0

    goto/32 :goto_1c

    nop

    :goto_e
    new-array v1, v0, [Landroid/view/View;

    goto/32 :goto_16

    nop

    :goto_f
    const/4 v0, 0x1

    goto/32 :goto_e

    nop

    :goto_10
    aput-object v2, v1, v3

    goto/32 :goto_5

    nop

    :goto_11
    iget-object v2, p1, Landroidx/recyclerview/widget/RecyclerView$d0;->itemView:Landroid/view/View;

    goto/32 :goto_9

    nop

    :goto_12
    invoke-virtual {p0, p1}, Lbb/a;->d0(Landroidx/recyclerview/widget/RecyclerView$d0;)V

    goto/32 :goto_f

    nop

    :goto_13
    invoke-virtual {v2, v3, v0, v1}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/32 :goto_4

    nop

    :goto_14
    const/4 v7, 0x2

    goto/32 :goto_6

    nop

    :goto_15
    aput-object v4, v2, v3

    goto/32 :goto_2

    nop

    :goto_16
    iget-object v2, p1, Landroidx/recyclerview/widget/RecyclerView$d0;->itemView:Landroid/view/View;

    goto/32 :goto_0

    nop

    :goto_17
    invoke-static {v1}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object v1

    goto/32 :goto_20

    nop

    :goto_18
    sget-object v6, Lbb/b;->w:Lc9/a;

    goto/32 :goto_14

    nop

    :goto_19
    aput-object v2, v1, v3

    goto/32 :goto_17

    nop

    :goto_1a
    iget-object v2, p1, Landroidx/recyclerview/widget/RecyclerView$d0;->itemView:Landroid/view/View;

    goto/32 :goto_19

    nop

    :goto_1b
    const/4 v2, 0x3

    goto/32 :goto_3

    nop

    :goto_1c
    invoke-interface {v1, v2}, Lmiuix/animation/h;->p([Ljava/lang/Object;)J

    move-result-wide v0

    goto/32 :goto_11

    nop

    :goto_1d
    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    goto/32 :goto_a

    nop

    :goto_1e
    invoke-interface {v1, v2}, Lmiuix/animation/h;->v([Ljava/lang/Object;)Lmiuix/animation/h;

    goto/32 :goto_7

    nop

    :goto_1f
    invoke-direct {v3, p0, p1}, Lbb/b$d;-><init>(Lbb/b;Landroidx/recyclerview/widget/RecyclerView$d0;)V

    goto/32 :goto_13

    nop

    :goto_20
    invoke-interface {v1}, Lmiuix/animation/e;->b()Lmiuix/animation/h;

    move-result-object v1

    goto/32 :goto_1

    nop
.end method

.method S(Lbb/a$c;)V
    .locals 16

    goto/32 :goto_1b

    nop

    :goto_0
    sget-object v6, Lh9/h;->b:Lh9/h;

    goto/32 :goto_56

    nop

    :goto_1
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    goto/32 :goto_61

    nop

    :goto_2
    invoke-direct {v6, v0, v4, v5}, Lbb/b$f;-><init>(Lbb/b;Landroid/view/View;Landroidx/recyclerview/widget/RecyclerView$d0;)V

    goto/32 :goto_36

    nop

    :goto_3
    aput-object v6, v13, v10

    goto/32 :goto_30

    nop

    :goto_4
    invoke-interface {v1}, Lmiuix/animation/e;->b()Lmiuix/animation/h;

    move-result-object v1

    goto/32 :goto_26

    nop

    :goto_5
    new-array v2, v7, [Ljava/lang/Object;

    goto/32 :goto_24

    nop

    :goto_6
    iget-object v3, v5, Landroidx/recyclerview/widget/RecyclerView$d0;->itemView:Landroid/view/View;

    :goto_7
    goto/32 :goto_2e

    nop

    :goto_8
    invoke-interface {v1, v2}, Lmiuix/animation/h;->v([Ljava/lang/Object;)Lmiuix/animation/h;

    goto/32 :goto_18

    nop

    :goto_9
    sub-int/2addr v13, v14

    goto/32 :goto_4d

    nop

    :goto_a
    aput-object v12, v2, v7

    goto/32 :goto_8

    nop

    :goto_b
    const/4 v13, 0x2

    goto/32 :goto_2c

    nop

    :goto_c
    return-void

    :goto_d
    aput-object v14, v12, v11

    goto/32 :goto_29

    nop

    :goto_e
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto/32 :goto_2b

    nop

    :goto_f
    sget-object v9, Lh9/h;->c:Lh9/h;

    goto/32 :goto_3a

    nop

    :goto_10
    iget v14, v1, Lbb/a$c;->c:I

    goto/32 :goto_9

    nop

    :goto_11
    aput-object v9, v2, v12

    goto/32 :goto_64

    nop

    :goto_12
    const/4 v11, 0x0

    goto/32 :goto_52

    nop

    :goto_13
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    goto/32 :goto_44

    nop

    :goto_14
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    goto/32 :goto_3

    nop

    :goto_15
    goto/16 :goto_3e

    :goto_16
    goto/32 :goto_3d

    nop

    :goto_17
    invoke-interface {v12, v13}, Lmiuix/animation/h;->v([Ljava/lang/Object;)Lmiuix/animation/h;

    goto/32 :goto_4c

    nop

    :goto_18
    new-array v1, v10, [Landroid/view/View;

    goto/32 :goto_31

    nop

    :goto_19
    const/4 v10, 0x1

    goto/32 :goto_12

    nop

    :goto_1a
    invoke-interface {v9, v12}, Lmiuix/animation/h;->p([Ljava/lang/Object;)J

    move-result-wide v12

    goto/32 :goto_4e

    nop

    :goto_1b
    move-object/from16 v0, p0

    goto/32 :goto_53

    nop

    :goto_1c
    iget-object v2, v1, Lbb/a$c;->a:Landroidx/recyclerview/widget/RecyclerView$d0;

    goto/32 :goto_1d

    nop

    :goto_1d
    const/4 v3, 0x0

    goto/32 :goto_3f

    nop

    :goto_1e
    new-array v13, v6, [Ljava/lang/Object;

    goto/32 :goto_42

    nop

    :goto_1f
    aput-object v6, v2, v10

    goto/32 :goto_58

    nop

    :goto_20
    aput-object v13, v12, v10

    goto/32 :goto_b

    nop

    :goto_21
    sget-object v12, Lbb/b;->w:Lc9/a;

    goto/32 :goto_a

    nop

    :goto_22
    move-object v4, v3

    goto/32 :goto_15

    nop

    :goto_23
    iget v15, v1, Lbb/a$c;->f:I

    goto/32 :goto_50

    nop

    :goto_24
    aput-object v6, v2, v11

    goto/32 :goto_2f

    nop

    :goto_25
    aput-object v9, v13, v8

    goto/32 :goto_5d

    nop

    :goto_26
    const/4 v2, 0x5

    goto/32 :goto_3c

    nop

    :goto_27
    invoke-interface {v1}, Lmiuix/animation/e;->b()Lmiuix/animation/h;

    move-result-object v1

    goto/32 :goto_5

    nop

    :goto_28
    new-instance v6, Lbb/b$f;

    goto/32 :goto_2

    nop

    :goto_29
    iget v13, v1, Lbb/a$c;->e:I

    goto/32 :goto_10

    nop

    :goto_2a
    invoke-direct {v1, v0, v4, v2}, Lbb/b$e;-><init>(Lbb/b;Landroid/view/View;Landroidx/recyclerview/widget/RecyclerView$d0;)V

    goto/32 :goto_46

    nop

    :goto_2b
    aput-object v1, v12, v8

    goto/32 :goto_1a

    nop

    :goto_2c
    aput-object v6, v12, v13

    goto/32 :goto_33

    nop

    :goto_2d
    iget v1, v1, Lbb/a$c;->d:I

    goto/32 :goto_40

    nop

    :goto_2e
    const/4 v6, 0x5

    goto/32 :goto_62

    nop

    :goto_2f
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    goto/32 :goto_1f

    nop

    :goto_30
    sget-object v6, Lh9/h;->c:Lh9/h;

    goto/32 :goto_5a

    nop

    :goto_31
    aput-object v3, v1, v11

    goto/32 :goto_5c

    nop

    :goto_32
    invoke-interface {v1, v2}, Lmiuix/animation/h;->p([Ljava/lang/Object;)J

    move-result-wide v1

    goto/32 :goto_28

    nop

    :goto_33
    iget v6, v1, Lbb/a$c;->f:I

    goto/32 :goto_2d

    nop

    :goto_34
    aput-object v9, v2, v6

    goto/32 :goto_13

    nop

    :goto_35
    aput-object v4, v9, v11

    goto/32 :goto_55

    nop

    :goto_36
    invoke-virtual {v3, v6, v1, v2}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    :goto_37
    goto/32 :goto_c

    nop

    :goto_38
    aput-object v4, v12, v11

    goto/32 :goto_63

    nop

    :goto_39
    sub-int/2addr v15, v6

    goto/32 :goto_14

    nop

    :goto_3a
    const/4 v12, 0x2

    goto/32 :goto_11

    nop

    :goto_3b
    iget-object v5, v1, Lbb/a$c;->b:Landroidx/recyclerview/widget/RecyclerView$d0;

    goto/32 :goto_67

    nop

    :goto_3c
    new-array v2, v2, [Ljava/lang/Object;

    goto/32 :goto_0

    nop

    :goto_3d
    iget-object v4, v2, Landroidx/recyclerview/widget/RecyclerView$d0;->itemView:Landroid/view/View;

    :goto_3e
    goto/32 :goto_3b

    nop

    :goto_3f
    if-eqz v2, :cond_0

    goto/32 :goto_16

    :cond_0
    goto/32 :goto_22

    nop

    :goto_40
    sub-int/2addr v6, v1

    goto/32 :goto_e

    nop

    :goto_41
    iget v6, v1, Lbb/a$c;->c:I

    goto/32 :goto_39

    nop

    :goto_42
    sget-object v14, Lh9/h;->b:Lh9/h;

    goto/32 :goto_49

    nop

    :goto_43
    const/4 v9, 0x2

    goto/32 :goto_19

    nop

    :goto_44
    aput-object v6, v2, v8

    goto/32 :goto_32

    nop

    :goto_45
    iget v15, v1, Lbb/a$c;->e:I

    goto/32 :goto_41

    nop

    :goto_46
    invoke-virtual {v4, v1, v12, v13}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    :goto_47
    goto/32 :goto_66

    nop

    :goto_48
    sget-object v12, Lbb/b;->v:Landroid/view/View$OnAttachStateChangeListener;

    goto/32 :goto_4b

    nop

    :goto_49
    aput-object v14, v13, v11

    goto/32 :goto_45

    nop

    :goto_4a
    const/4 v8, 0x3

    goto/32 :goto_43

    nop

    :goto_4b
    invoke-virtual {v4, v12}, Landroid/view/View;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    goto/32 :goto_60

    nop

    :goto_4c
    new-array v9, v10, [Landroid/view/View;

    goto/32 :goto_35

    nop

    :goto_4d
    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    goto/32 :goto_20

    nop

    :goto_4e
    new-instance v1, Lbb/b$e;

    goto/32 :goto_2a

    nop

    :goto_4f
    new-array v12, v7, [Ljava/lang/Object;

    goto/32 :goto_d

    nop

    :goto_50
    iget v9, v1, Lbb/a$c;->d:I

    goto/32 :goto_6a

    nop

    :goto_51
    invoke-virtual {v0, v5, v11}, Lbb/a;->f0(Landroidx/recyclerview/widget/RecyclerView$d0;Z)V

    goto/32 :goto_69

    nop

    :goto_52
    if-nez v4, :cond_1

    goto/32 :goto_47

    :cond_1
    goto/32 :goto_54

    nop

    :goto_53
    move-object/from16 v1, p1

    goto/32 :goto_1c

    nop

    :goto_54
    invoke-virtual {v0, v2, v10}, Lbb/a;->f0(Landroidx/recyclerview/widget/RecyclerView$d0;Z)V

    goto/32 :goto_48

    nop

    :goto_55
    invoke-static {v9}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object v9

    goto/32 :goto_59

    nop

    :goto_56
    aput-object v6, v2, v11

    goto/32 :goto_1

    nop

    :goto_57
    aput-object v3, v1, v11

    goto/32 :goto_5f

    nop

    :goto_58
    const/4 v6, 0x2

    goto/32 :goto_34

    nop

    :goto_59
    invoke-interface {v9}, Lmiuix/animation/e;->b()Lmiuix/animation/h;

    move-result-object v9

    goto/32 :goto_4f

    nop

    :goto_5a
    aput-object v6, v13, v9

    goto/32 :goto_23

    nop

    :goto_5b
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    goto/32 :goto_25

    nop

    :goto_5c
    invoke-static {v1}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object v1

    goto/32 :goto_27

    nop

    :goto_5d
    sget-object v9, Lbb/b;->w:Lc9/a;

    goto/32 :goto_5e

    nop

    :goto_5e
    aput-object v9, v13, v7

    goto/32 :goto_17

    nop

    :goto_5f
    invoke-static {v1}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object v1

    goto/32 :goto_4

    nop

    :goto_60
    new-array v12, v10, [Landroid/view/View;

    goto/32 :goto_38

    nop

    :goto_61
    aput-object v9, v2, v10

    goto/32 :goto_f

    nop

    :goto_62
    const/4 v7, 0x4

    goto/32 :goto_4a

    nop

    :goto_63
    invoke-static {v12}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object v12

    goto/32 :goto_65

    nop

    :goto_64
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    goto/32 :goto_68

    nop

    :goto_65
    invoke-interface {v12}, Lmiuix/animation/e;->b()Lmiuix/animation/h;

    move-result-object v12

    goto/32 :goto_1e

    nop

    :goto_66
    if-nez v3, :cond_2

    goto/32 :goto_37

    :cond_2
    goto/32 :goto_51

    nop

    :goto_67
    if-nez v5, :cond_3

    goto/32 :goto_7

    :cond_3
    goto/32 :goto_6

    nop

    :goto_68
    aput-object v12, v2, v8

    goto/32 :goto_21

    nop

    :goto_69
    new-array v1, v10, [Landroid/view/View;

    goto/32 :goto_57

    nop

    :goto_6a
    sub-int/2addr v15, v9

    goto/32 :goto_5b

    nop
.end method

.method T(Lbb/a$d;)V
    .locals 12

    goto/32 :goto_1a

    nop

    :goto_0
    aput-object v3, v2, v4

    goto/32 :goto_28

    nop

    :goto_1
    aput-object v3, v2, v4

    goto/32 :goto_12

    nop

    :goto_2
    aput-object v5, v3, v9

    goto/32 :goto_17

    nop

    :goto_3
    new-array v3, v11, [Ljava/lang/Object;

    goto/32 :goto_22

    nop

    :goto_4
    new-array v3, v3, [Ljava/lang/Object;

    goto/32 :goto_8

    nop

    :goto_5
    new-array v2, v1, [Landroid/view/View;

    goto/32 :goto_1d

    nop

    :goto_6
    const/4 v9, 0x3

    goto/32 :goto_e

    nop

    :goto_7
    aput-object v6, v3, v4

    goto/32 :goto_21

    nop

    :goto_8
    sget-object v6, Lh9/h;->b:Lh9/h;

    goto/32 :goto_7

    nop

    :goto_9
    return-void

    :goto_a
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    goto/32 :goto_0

    nop

    :goto_b
    iget-object v0, p1, Lbb/a$d;->a:Landroidx/recyclerview/widget/RecyclerView$d0;

    goto/32 :goto_1e

    nop

    :goto_c
    sget-object v10, Lbb/b;->w:Lc9/a;

    goto/32 :goto_25

    nop

    :goto_d
    const/4 v3, 0x5

    goto/32 :goto_4

    nop

    :goto_e
    aput-object v5, v3, v9

    goto/32 :goto_c

    nop

    :goto_f
    aput-object v5, v3, v1

    goto/32 :goto_20

    nop

    :goto_10
    aput-object v10, v3, v11

    goto/32 :goto_1b

    nop

    :goto_11
    invoke-interface {v2}, Lmiuix/animation/e;->b()Lmiuix/animation/h;

    move-result-object v2

    goto/32 :goto_3

    nop

    :goto_12
    invoke-static {v2}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object v2

    goto/32 :goto_11

    nop

    :goto_13
    new-instance v3, Lbb/b$c;

    goto/32 :goto_15

    nop

    :goto_14
    invoke-virtual {p1, v3, v1, v2}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/32 :goto_9

    nop

    :goto_15
    invoke-direct {v3, p0, v0}, Lbb/b$c;-><init>(Lbb/b;Landroidx/recyclerview/widget/RecyclerView$d0;)V

    goto/32 :goto_14

    nop

    :goto_16
    iget-object v3, v3, Landroidx/recyclerview/widget/RecyclerView$d0;->itemView:Landroid/view/View;

    goto/32 :goto_1

    nop

    :goto_17
    invoke-interface {v2, v3}, Lmiuix/animation/h;->p([Ljava/lang/Object;)J

    move-result-wide v1

    goto/32 :goto_26

    nop

    :goto_18
    const/4 v4, 0x0

    goto/32 :goto_a

    nop

    :goto_19
    sget-object v7, Lh9/h;->c:Lh9/h;

    goto/32 :goto_27

    nop

    :goto_1a
    iget-object v0, p1, Lbb/a$d;->a:Landroidx/recyclerview/widget/RecyclerView$d0;

    goto/32 :goto_2a

    nop

    :goto_1b
    invoke-interface {v2, v3}, Lmiuix/animation/h;->v([Ljava/lang/Object;)Lmiuix/animation/h;

    goto/32 :goto_5

    nop

    :goto_1c
    aput-object v7, v3, v8

    goto/32 :goto_6

    nop

    :goto_1d
    iget-object v3, p1, Lbb/a$d;->a:Landroidx/recyclerview/widget/RecyclerView$d0;

    goto/32 :goto_16

    nop

    :goto_1e
    const/4 v1, 0x1

    goto/32 :goto_1f

    nop

    :goto_1f
    new-array v2, v1, [Landroid/view/View;

    goto/32 :goto_29

    nop

    :goto_20
    aput-object v7, v3, v8

    goto/32 :goto_2

    nop

    :goto_21
    aput-object v5, v3, v1

    goto/32 :goto_19

    nop

    :goto_22
    aput-object v6, v3, v4

    goto/32 :goto_f

    nop

    :goto_23
    invoke-interface {v2}, Lmiuix/animation/e;->b()Lmiuix/animation/h;

    move-result-object v2

    goto/32 :goto_d

    nop

    :goto_24
    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView$d0;->itemView:Landroid/view/View;

    goto/32 :goto_13

    nop

    :goto_25
    const/4 v11, 0x4

    goto/32 :goto_10

    nop

    :goto_26
    iget-object p1, p1, Lbb/a$d;->a:Landroidx/recyclerview/widget/RecyclerView$d0;

    goto/32 :goto_24

    nop

    :goto_27
    const/4 v8, 0x2

    goto/32 :goto_1c

    nop

    :goto_28
    invoke-static {v2}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object v2

    goto/32 :goto_23

    nop

    :goto_29
    iget-object v3, v0, Landroidx/recyclerview/widget/RecyclerView$d0;->itemView:Landroid/view/View;

    goto/32 :goto_18

    nop

    :goto_2a
    invoke-virtual {p0, v0}, Lbb/a;->h0(Landroidx/recyclerview/widget/RecyclerView$d0;)V

    goto/32 :goto_b

    nop
.end method

.method U(Landroidx/recyclerview/widget/RecyclerView$d0;)V
    .locals 8

    goto/32 :goto_5

    nop

    :goto_0
    sget-object v6, Lbb/b;->w:Lc9/a;

    goto/32 :goto_b

    nop

    :goto_1
    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    goto/32 :goto_19

    nop

    :goto_2
    new-array v1, v0, [Landroid/view/View;

    goto/32 :goto_9

    nop

    :goto_3
    new-array v2, v7, [Ljava/lang/Object;

    goto/32 :goto_4

    nop

    :goto_4
    aput-object v4, v2, v3

    goto/32 :goto_21

    nop

    :goto_5
    invoke-virtual {p0, p1}, Lbb/a;->j0(Landroidx/recyclerview/widget/RecyclerView$d0;)V

    goto/32 :goto_17

    nop

    :goto_6
    invoke-direct {v3, p0, p1}, Lbb/b$b;-><init>(Lbb/b;Landroidx/recyclerview/widget/RecyclerView$d0;)V

    goto/32 :goto_20

    nop

    :goto_7
    return-void

    :goto_8
    const/4 v0, 0x1

    goto/32 :goto_2

    nop

    :goto_9
    iget-object v2, p1, Landroidx/recyclerview/widget/RecyclerView$d0;->itemView:Landroid/view/View;

    goto/32 :goto_22

    nop

    :goto_a
    iget-object v2, p1, Landroidx/recyclerview/widget/RecyclerView$d0;->itemView:Landroid/view/View;

    goto/32 :goto_c

    nop

    :goto_b
    const/4 v7, 0x2

    goto/32 :goto_23

    nop

    :goto_c
    new-instance v3, Lbb/b$b;

    goto/32 :goto_6

    nop

    :goto_d
    sget-object v4, Lh9/h;->o:Lh9/h;

    goto/32 :goto_10

    nop

    :goto_e
    invoke-interface {v1, v2}, Lmiuix/animation/h;->v([Ljava/lang/Object;)Lmiuix/animation/h;

    goto/32 :goto_1b

    nop

    :goto_f
    invoke-static {v1}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object v1

    goto/32 :goto_11

    nop

    :goto_10
    aput-object v4, v2, v3

    goto/32 :goto_1e

    nop

    :goto_11
    invoke-interface {v1}, Lmiuix/animation/e;->b()Lmiuix/animation/h;

    move-result-object v1

    goto/32 :goto_3

    nop

    :goto_12
    new-array v2, v2, [Ljava/lang/Object;

    goto/32 :goto_d

    nop

    :goto_13
    invoke-virtual {v0, v1}, Landroid/view/View;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    goto/32 :goto_8

    nop

    :goto_14
    invoke-interface {v1, v2}, Lmiuix/animation/h;->p([Ljava/lang/Object;)J

    move-result-wide v0

    goto/32 :goto_a

    nop

    :goto_15
    invoke-interface {v1}, Lmiuix/animation/e;->b()Lmiuix/animation/h;

    move-result-object v1

    goto/32 :goto_1c

    nop

    :goto_16
    sget-object v1, Lbb/b;->v:Landroid/view/View$OnAttachStateChangeListener;

    goto/32 :goto_13

    nop

    :goto_17
    iget-object v0, p1, Landroidx/recyclerview/widget/RecyclerView$d0;->itemView:Landroid/view/View;

    goto/32 :goto_16

    nop

    :goto_18
    iget-object v2, p1, Landroidx/recyclerview/widget/RecyclerView$d0;->itemView:Landroid/view/View;

    goto/32 :goto_1d

    nop

    :goto_19
    aput-object v5, v2, v0

    goto/32 :goto_0

    nop

    :goto_1a
    invoke-static {v1}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object v1

    goto/32 :goto_15

    nop

    :goto_1b
    new-array v1, v0, [Landroid/view/View;

    goto/32 :goto_18

    nop

    :goto_1c
    const/4 v2, 0x3

    goto/32 :goto_12

    nop

    :goto_1d
    aput-object v2, v1, v3

    goto/32 :goto_f

    nop

    :goto_1e
    const/4 v5, 0x0

    goto/32 :goto_1

    nop

    :goto_1f
    aput-object v2, v1, v3

    goto/32 :goto_1a

    nop

    :goto_20
    invoke-virtual {v2, v3, v0, v1}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/32 :goto_7

    nop

    :goto_21
    aput-object v5, v2, v0

    goto/32 :goto_14

    nop

    :goto_22
    const/4 v3, 0x0

    goto/32 :goto_1f

    nop

    :goto_23
    aput-object v6, v2, v7

    goto/32 :goto_e

    nop
.end method

.method k0(Landroidx/recyclerview/widget/RecyclerView$d0;)V
    .locals 1

    goto/32 :goto_3

    nop

    :goto_0
    return-void

    :goto_1
    const/4 v0, 0x0

    goto/32 :goto_4

    nop

    :goto_2
    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView$d0;->itemView:Landroid/view/View;

    goto/32 :goto_1

    nop

    :goto_3
    invoke-virtual {p0, p1}, Lbb/b;->o0(Landroidx/recyclerview/widget/RecyclerView$d0;)V

    goto/32 :goto_2

    nop

    :goto_4
    invoke-virtual {p1, v0}, Landroid/view/View;->setAlpha(F)V

    goto/32 :goto_0

    nop
.end method

.method l0(Lbb/a$c;)V
    .locals 5

    goto/32 :goto_5

    nop

    :goto_0
    invoke-virtual {p1, v0}, Landroid/view/View;->setTranslationY(F)V

    :goto_1
    goto/32 :goto_1e

    nop

    :goto_2
    invoke-virtual {v4, v0}, Landroid/view/View;->setTranslationX(F)V

    goto/32 :goto_f

    nop

    :goto_3
    iget-object v0, v0, Landroidx/recyclerview/widget/RecyclerView$d0;->itemView:Landroid/view/View;

    goto/32 :goto_8

    nop

    :goto_4
    sub-int/2addr v3, v4

    goto/32 :goto_a

    nop

    :goto_5
    iget-object v0, p1, Lbb/a$c;->a:Landroidx/recyclerview/widget/RecyclerView$d0;

    goto/32 :goto_3

    nop

    :goto_6
    iget-object v0, v0, Landroidx/recyclerview/widget/RecyclerView$d0;->itemView:Landroid/view/View;

    goto/32 :goto_24

    nop

    :goto_7
    iget-object v4, v4, Landroidx/recyclerview/widget/RecyclerView$d0;->itemView:Landroid/view/View;

    goto/32 :goto_2

    nop

    :goto_8
    invoke-virtual {v0}, Landroid/view/View;->getTranslationX()F

    move-result v0

    goto/32 :goto_19

    nop

    :goto_9
    neg-int v1, v2

    goto/32 :goto_1d

    nop

    :goto_a
    int-to-float v3, v3

    goto/32 :goto_14

    nop

    :goto_b
    iget v3, p1, Lbb/a$c;->f:I

    goto/32 :goto_16

    nop

    :goto_c
    if-nez v0, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_e

    nop

    :goto_d
    neg-int v0, v3

    goto/32 :goto_18

    nop

    :goto_e
    invoke-virtual {p0, v0}, Lbb/b;->o0(Landroidx/recyclerview/widget/RecyclerView$d0;)V

    goto/32 :goto_1c

    nop

    :goto_f
    iget-object v0, p1, Lbb/a$c;->a:Landroidx/recyclerview/widget/RecyclerView$d0;

    goto/32 :goto_6

    nop

    :goto_10
    iget-object v0, v0, Landroidx/recyclerview/widget/RecyclerView$d0;->itemView:Landroid/view/View;

    goto/32 :goto_9

    nop

    :goto_11
    iget-object p1, p1, Lbb/a$c;->b:Landroidx/recyclerview/widget/RecyclerView$d0;

    goto/32 :goto_23

    nop

    :goto_12
    iget-object v4, p1, Lbb/a$c;->a:Landroidx/recyclerview/widget/RecyclerView$d0;

    goto/32 :goto_7

    nop

    :goto_13
    iget-object v0, p1, Lbb/a$c;->b:Landroidx/recyclerview/widget/RecyclerView$d0;

    goto/32 :goto_c

    nop

    :goto_14
    sub-float/2addr v3, v1

    goto/32 :goto_1b

    nop

    :goto_15
    iget v2, p1, Lbb/a$c;->e:I

    goto/32 :goto_25

    nop

    :goto_16
    iget v4, p1, Lbb/a$c;->d:I

    goto/32 :goto_4

    nop

    :goto_17
    float-to-int v2, v2

    goto/32 :goto_b

    nop

    :goto_18
    int-to-float v0, v0

    goto/32 :goto_0

    nop

    :goto_19
    iget-object v1, p1, Lbb/a$c;->a:Landroidx/recyclerview/widget/RecyclerView$d0;

    goto/32 :goto_20

    nop

    :goto_1a
    sub-float/2addr v2, v0

    goto/32 :goto_17

    nop

    :goto_1b
    float-to-int v3, v3

    goto/32 :goto_12

    nop

    :goto_1c
    iget-object v0, p1, Lbb/a$c;->b:Landroidx/recyclerview/widget/RecyclerView$d0;

    goto/32 :goto_10

    nop

    :goto_1d
    int-to-float v1, v1

    goto/32 :goto_28

    nop

    :goto_1e
    return-void

    :goto_1f
    int-to-float v2, v2

    goto/32 :goto_1a

    nop

    :goto_20
    iget-object v1, v1, Landroidx/recyclerview/widget/RecyclerView$d0;->itemView:Landroid/view/View;

    goto/32 :goto_27

    nop

    :goto_21
    sub-int/2addr v2, v3

    goto/32 :goto_1f

    nop

    :goto_22
    iget-object v2, p1, Lbb/a$c;->a:Landroidx/recyclerview/widget/RecyclerView$d0;

    goto/32 :goto_26

    nop

    :goto_23
    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView$d0;->itemView:Landroid/view/View;

    goto/32 :goto_d

    nop

    :goto_24
    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    goto/32 :goto_13

    nop

    :goto_25
    iget v3, p1, Lbb/a$c;->c:I

    goto/32 :goto_21

    nop

    :goto_26
    invoke-virtual {p0, v2}, Lbb/b;->o0(Landroidx/recyclerview/widget/RecyclerView$d0;)V

    goto/32 :goto_15

    nop

    :goto_27
    invoke-virtual {v1}, Landroid/view/View;->getTranslationY()F

    move-result v1

    goto/32 :goto_22

    nop

    :goto_28
    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationX(F)V

    goto/32 :goto_11

    nop
.end method

.method m0(Lbb/a$d;)V
    .locals 3

    goto/32 :goto_a

    nop

    :goto_0
    sub-int/2addr v1, v2

    goto/32 :goto_5

    nop

    :goto_1
    sub-int/2addr v1, p1

    goto/32 :goto_8

    nop

    :goto_2
    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationX(F)V

    goto/32 :goto_e

    nop

    :goto_3
    return-void

    :goto_4
    iget-object v0, v0, Landroidx/recyclerview/widget/RecyclerView$d0;->itemView:Landroid/view/View;

    goto/32 :goto_7

    nop

    :goto_5
    int-to-float v1, v1

    goto/32 :goto_2

    nop

    :goto_6
    invoke-virtual {v0, p1}, Landroid/view/View;->setTranslationY(F)V

    goto/32 :goto_3

    nop

    :goto_7
    iget v1, p1, Lbb/a$d;->c:I

    goto/32 :goto_c

    nop

    :goto_8
    int-to-float p1, v1

    goto/32 :goto_6

    nop

    :goto_9
    iget-object v0, v0, Landroidx/recyclerview/widget/RecyclerView$d0;->itemView:Landroid/view/View;

    goto/32 :goto_b

    nop

    :goto_a
    iget-object v0, p1, Lbb/a$d;->a:Landroidx/recyclerview/widget/RecyclerView$d0;

    goto/32 :goto_9

    nop

    :goto_b
    iget v1, p1, Lbb/a$d;->b:I

    goto/32 :goto_d

    nop

    :goto_c
    iget p1, p1, Lbb/a$d;->e:I

    goto/32 :goto_1

    nop

    :goto_d
    iget v2, p1, Lbb/a$d;->d:I

    goto/32 :goto_0

    nop

    :goto_e
    iget-object v0, p1, Lbb/a$d;->a:Landroidx/recyclerview/widget/RecyclerView$d0;

    goto/32 :goto_4

    nop
.end method

.method o0(Landroidx/recyclerview/widget/RecyclerView$d0;)V
    .locals 5

    goto/32 :goto_3

    nop

    :goto_0
    aput-object v2, v1, v3

    goto/32 :goto_4

    nop

    :goto_1
    const/4 v0, 0x1

    goto/32 :goto_12

    nop

    :goto_2
    const/4 v0, 0x2

    goto/32 :goto_10

    nop

    :goto_3
    if-nez p1, :cond_0

    goto/32 :goto_9

    :cond_0
    goto/32 :goto_1

    nop

    :goto_4
    invoke-static {v1}, Lmiuix/animation/a;->x([Landroid/view/View;)Lmiuix/animation/e;

    move-result-object v1

    goto/32 :goto_e

    nop

    :goto_5
    invoke-interface {v1, v2}, Lmiuix/animation/d;->k([Ljava/lang/Object;)V

    goto/32 :goto_d

    nop

    :goto_6
    const/4 v2, 0x3

    goto/32 :goto_14

    nop

    :goto_7
    aput-object v4, v2, v3

    goto/32 :goto_f

    nop

    :goto_8
    invoke-static {p1}, Lbb/a;->n0(Landroid/view/View;)V

    :goto_9
    goto/32 :goto_c

    nop

    :goto_a
    aput-object v3, v2, v0

    goto/32 :goto_2

    nop

    :goto_b
    sget-object v4, Lh9/h;->b:Lh9/h;

    goto/32 :goto_7

    nop

    :goto_c
    return-void

    :goto_d
    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView$d0;->itemView:Landroid/view/View;

    goto/32 :goto_8

    nop

    :goto_e
    invoke-interface {v1}, Lmiuix/animation/e;->b()Lmiuix/animation/h;

    move-result-object v1

    goto/32 :goto_6

    nop

    :goto_f
    sget-object v3, Lh9/h;->c:Lh9/h;

    goto/32 :goto_a

    nop

    :goto_10
    sget-object v3, Lh9/h;->o:Lh9/h;

    goto/32 :goto_11

    nop

    :goto_11
    aput-object v3, v2, v0

    goto/32 :goto_5

    nop

    :goto_12
    new-array v1, v0, [Landroid/view/View;

    goto/32 :goto_13

    nop

    :goto_13
    iget-object v2, p1, Landroidx/recyclerview/widget/RecyclerView$d0;->itemView:Landroid/view/View;

    goto/32 :goto_15

    nop

    :goto_14
    new-array v2, v2, [Ljava/lang/Object;

    goto/32 :goto_b

    nop

    :goto_15
    const/4 v3, 0x0

    goto/32 :goto_0

    nop
.end method
