.class public Lq2/c;
.super Ljava/lang/Object;


# static fields
.field public static a:Z

.field public static b:Z

.field public static c:Z

.field public static d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const-string v0, "persist.security.sec_debug"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/packageinstaller/compat/SystemPropertiesCompat;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lq2/c;->b:Z

    sput-boolean v1, Lq2/c;->c:Z

    const-string v0, "product"

    sput-object v0, Lq2/c;->d:Ljava/lang/String;

    new-instance v0, Ljava/io/File;

    const-string v1, "/data/system/"

    const-string v2, "package_installer_log_on"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    sput-boolean v2, Lq2/c;->c:Z

    sput-boolean v2, Lq2/c;->a:Z

    :cond_0
    new-instance v0, Ljava/io/File;

    const-string v3, "package_installer_staging"

    invoke-direct {v0, v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "staging"

    sput-object v0, Lq2/c;->d:Ljava/lang/String;

    sput-boolean v2, Lq2/c;->c:Z

    sput-boolean v2, Lq2/c;->a:Z

    invoke-static {v2}, Landroid/webkit/WebView;->setWebContentsDebuggingEnabled(Z)V

    goto :goto_1

    :cond_1
    new-instance v0, Ljava/io/File;

    const-string v3, "package_installer_test"

    invoke-direct {v0, v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "test"

    :goto_0
    sput-object v0, Lq2/c;->d:Ljava/lang/String;

    sput-boolean v2, Lq2/c;->c:Z

    sput-boolean v2, Lq2/c;->a:Z

    goto :goto_1

    :cond_2
    new-instance v0, Ljava/io/File;

    const-string v3, "package_installer_preview"

    invoke-direct {v0, v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "preview"

    goto :goto_0

    :cond_3
    :goto_1
    return-void
.end method
