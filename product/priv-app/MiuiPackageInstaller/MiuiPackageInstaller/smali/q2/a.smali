.class public Lq2/a;
.super Ljava/lang/Object;


# static fields
.field public static final a:Landroid/util/ArrayMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/ArrayMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Ljava/lang/String;

.field public static c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    new-instance v0, Landroid/util/ArrayMap;

    invoke-direct {v0}, Landroid/util/ArrayMap;-><init>()V

    sput-object v0, Lq2/a;->a:Landroid/util/ArrayMap;

    invoke-static {}, Lcom/android/packageinstaller/InstallerApplication;->g()Lcom/android/packageinstaller/InstallerApplication;

    move-result-object v1

    invoke-static {v1}, Lcom/android/packageinstaller/utils/g;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lq2/a;->b:Ljava/lang/String;

    invoke-static {}, Lq2/a;->d()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lq2/a;->c:Ljava/lang/String;

    sget-object v1, Lq2/c;->d:Ljava/lang/String;

    const-string v2, "environment"

    invoke-virtual {v0, v2, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lq2/a;->c:Ljava/lang/String;

    const-string v2, "client_session_id"

    invoke-virtual {v0, v2, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "web_version"

    const-string v2, "null"

    invoke-virtual {v0, v1, v2}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Lf6/d;->e()Ljava/lang/String;

    move-result-object v1

    const-string v3, "screen_size"

    invoke-virtual {v0, v3, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Lf6/d;->c()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    const-string v3, "resolution_ratio"

    invoke-virtual {v0, v3, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Lcom/android/packageinstaller/InstallerApplication;->g()Lcom/android/packageinstaller/InstallerApplication;

    move-result-object v1

    invoke-static {}, Lcom/android/packageinstaller/InstallerApplication;->g()Lcom/android/packageinstaller/InstallerApplication;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Application;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "MD5"

    invoke-static {v1, v3, v4}, Lj2/f;->k(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "os_sign"

    invoke-virtual {v0, v3, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget v3, Lcom/android/packageinstaller/utils/g;->b:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, "GB"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v4, "phone_ram"

    invoke-virtual {v0, v4, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget v4, Lcom/android/packageinstaller/utils/g;->c:I

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v4, "phone_rom"

    invoke-virtual {v0, v4, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget v4, Lcom/android/packageinstaller/utils/g;->d:I

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "phone_rom_used"

    invoke-virtual {v0, v3, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "crowd_id"

    invoke-virtual {v0, v1, v2}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "tzsdk_sign"

    invoke-virtual {v0, v1, v2}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "tz_process_id"

    invoke-virtual {v0, v1, v2}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "tzsdk_params"

    invoke-virtual {v0, v1, v2}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "tzsdk_time"

    invoke-virtual {v0, v1, v2}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-wide v3, Lm5/u1;->a:J

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    const-string v3, "cur_ver_code"

    invoke-virtual {v0, v3, v1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "launch_type"

    invoke-virtual {v0, v1, v2}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    invoke-static {}, Lq2/a;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    sget-object v0, Lq2/a;->a:Landroid/util/ArrayMap;

    invoke-virtual {v0, p0, p1}, Landroid/util/ArrayMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public static c(Lo5/a;Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lo5/a;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    sget-object v0, Lq2/a;->a:Landroid/util/ArrayMap;

    invoke-interface {p1, v0}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    sget-object v0, Lcom/android/packageinstaller/InstallerApplication;->c:Lcom/android/packageinstaller/InstallerApplication;

    invoke-static {v0}, Lcom/android/packageinstaller/compat/MiuiSettingsCompat;->isSafeModelEnable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "scene_mode"

    const-string v1, "safe_model"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Ld6/l0;->b()Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v1, Lcom/android/packageinstaller/InstallerApplication;->e:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;->getPackageMd5()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "ext"

    const-string v1, "single_authorize"

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v1, v1}, Lf6/o;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    :cond_0
    sget-object v0, Lq2/a;->c:Ljava/lang/String;

    const-string v1, "client_session_id"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p0}, Lo5/a;->getRef()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ref"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p0}, Lo5/a;->n()Ljava/lang/String;

    move-result-object v0

    const-string v1, "refs"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p0}, Lo5/a;->v()Ljava/lang/String;

    move-result-object v0

    const-string v1, "install_process"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p0}, Lo5/a;->k()Ljava/lang/String;

    move-result-object v0

    const-string v1, "from_ref"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p0}, Lo5/a;->z()Ljava/lang/String;

    move-result-object v0

    const-string v1, "launch_ref"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p0}, Lo5/a;->H()Ljava/lang/String;

    move-result-object v0

    const-string v1, "process_type"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p0}, Lo5/a;->z()Ljava/lang/String;

    move-result-object v0

    const-string v1, "launch_source_package"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p0}, Lo5/a;->I()Ljava/lang/String;

    move-result-object v0

    const-string v1, "launch_source_package_name"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p0}, Lo5/a;->x()Ljava/lang/String;

    move-result-object v0

    const-string v1, "package_message"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p0}, Lo5/a;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "app_message"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p0}, Lo5/a;->l()Ljava/lang/String;

    move-result-object p0

    const-string v0, "mi_package_name"

    invoke-interface {p1, v0, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Lt5/n;->e()Lt5/n;

    move-result-object p0

    invoke-virtual {p0}, Lt5/n;->c()Ljava/lang/String;

    move-result-object p0

    const-string v0, "exp_id"

    invoke-interface {p1, v0, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p0

    const-string v0, "visit_time"

    invoke-interface {p1, v0, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object p0, Lcom/android/packageinstaller/InstallerApplication;->c:Lcom/android/packageinstaller/InstallerApplication;

    invoke-static {p0}, Lm2/b;->g(Landroid/content/Context;)Lm2/b;

    move-result-object p0

    invoke-virtual {p0}, Lm2/b;->h()Z

    move-result p0

    const-string v0, "riskapp_authorize_status"

    if-eqz p0, :cond_3

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_4

    sget-object p0, Lcom/android/packageinstaller/InstallerApplication;->c:Lcom/android/packageinstaller/InstallerApplication;

    invoke-static {p0}, Lm2/b;->g(Landroid/content/Context;)Lm2/b;

    move-result-object p0

    invoke-virtual {p0}, Lm2/b;->i()Lx5/a;

    move-result-object p0

    sget-object v1, Lx5/a;->f:Lx5/a;

    if-ne p0, v1, :cond_1

    const-string p0, "miid_open"

    goto :goto_0

    :cond_1
    sget-object p0, Lcom/android/packageinstaller/InstallerApplication;->c:Lcom/android/packageinstaller/InstallerApplication;

    invoke-static {p0}, Lm2/b;->g(Landroid/content/Context;)Lm2/b;

    move-result-object p0

    invoke-virtual {p0}, Lm2/b;->i()Lx5/a;

    move-result-object p0

    sget-object v1, Lx5/a;->c:Lx5/a;

    if-ne p0, v1, :cond_2

    const-string p0, "lockscreen_open"

    goto :goto_0

    :cond_2
    sget-object p0, Lcom/android/packageinstaller/InstallerApplication;->c:Lcom/android/packageinstaller/InstallerApplication;

    invoke-static {p0}, Lm2/b;->g(Landroid/content/Context;)Lm2/b;

    move-result-object p0

    invoke-virtual {p0}, Lm2/b;->i()Lx5/a;

    move-result-object p0

    sget-object v1, Lx5/a;->d:Lx5/a;

    if-ne p0, v1, :cond_4

    const-string p0, "touchid_open"

    goto :goto_0

    :cond_3
    const-string p0, "close"

    :goto_0
    invoke-interface {p1, v0, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_4
    return-void
.end method

.method private static d()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lq2/a;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static e(Landroid/content/Context;)V
    .locals 1

    invoke-static {}, Ll2/b;->a()Ll2/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Ll2/b;->d(Landroid/content/Context;)V

    new-instance p0, Lq2/a$a;

    invoke-direct {p0}, Lq2/a$a;-><init>()V

    invoke-static {p0}, Lm5/b;->r(Lm5/b$b;)V

    return-void
.end method

.method public static f(Lcom/miui/packageInstaller/model/ServiceQuality;)V
    .locals 3

    new-instance v0, Lcom/xiaomi/onetrack/ServiceQualityEvent$Builder;

    invoke-direct {v0}, Lcom/xiaomi/onetrack/ServiceQualityEvent$Builder;-><init>()V

    invoke-virtual {p0}, Lcom/miui/packageInstaller/model/ServiceQuality;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/xiaomi/onetrack/ServiceQualityEvent$Builder;->setScheme(Ljava/lang/String;)Lcom/xiaomi/onetrack/ServiceQualityEvent$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/miui/packageInstaller/model/ServiceQuality;->getHost()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/xiaomi/onetrack/ServiceQualityEvent$Builder;->setHost(Ljava/lang/String;)Lcom/xiaomi/onetrack/ServiceQualityEvent$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/miui/packageInstaller/model/ServiceQuality;->getPort()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/xiaomi/onetrack/ServiceQualityEvent$Builder;->setPort(Ljava/lang/Integer;)Lcom/xiaomi/onetrack/ServiceQualityEvent$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/miui/packageInstaller/model/ServiceQuality;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/xiaomi/onetrack/ServiceQualityEvent$Builder;->setPath(Ljava/lang/String;)Lcom/xiaomi/onetrack/ServiceQualityEvent$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/miui/packageInstaller/model/ServiceQuality;->getResponseCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/xiaomi/onetrack/ServiceQualityEvent$Builder;->setResponseCode(Ljava/lang/Integer;)Lcom/xiaomi/onetrack/ServiceQualityEvent$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/miui/packageInstaller/model/ServiceQuality;->getResultType()Lcom/xiaomi/onetrack/ServiceQualityEvent$ResultType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/xiaomi/onetrack/ServiceQualityEvent$Builder;->setResultType(Lcom/xiaomi/onetrack/ServiceQualityEvent$ResultType;)Lcom/xiaomi/onetrack/ServiceQualityEvent$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/miui/packageInstaller/model/ServiceQuality;->getExceptionTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/xiaomi/onetrack/ServiceQualityEvent$Builder;->setExceptionTag(Ljava/lang/String;)Lcom/xiaomi/onetrack/ServiceQualityEvent$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/miui/packageInstaller/model/ServiceQuality;->getDuration()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/xiaomi/onetrack/ServiceQualityEvent$Builder;->setDuration(Ljava/lang/Long;)Lcom/xiaomi/onetrack/ServiceQualityEvent$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/miui/packageInstaller/model/ServiceQuality;->getNetType()Lcom/xiaomi/onetrack/OneTrack$NetType;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/xiaomi/onetrack/ServiceQualityEvent$Builder;->setRequestNetType(Lcom/xiaomi/onetrack/OneTrack$NetType;)Lcom/xiaomi/onetrack/ServiceQualityEvent$Builder;

    move-result-object p0

    invoke-virtual {p0}, Lcom/xiaomi/onetrack/ServiceQualityEvent$Builder;->build()Lcom/xiaomi/onetrack/ServiceQualityEvent;

    move-result-object p0

    invoke-static {}, Ll2/b;->a()Ll2/b;

    move-result-object v0

    invoke-virtual {v0}, Ll2/b;->b()Lcom/xiaomi/onetrack/OneTrack;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/xiaomi/onetrack/OneTrack;->trackServiceQualityEvent(Lcom/xiaomi/onetrack/ServiceQualityEvent;)V

    return-void
.end method
