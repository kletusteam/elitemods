.class public Lq2/b;
.super Lmiuix/appcompat/app/j;

# interfaces
.implements Lo5/a;
.implements Lm5/s;


# instance fields
.field protected q:Lo5/b;

.field private r:Z

.field protected s:Z

.field t:J


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lmiuix/appcompat/app/j;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lq2/b;->s:Z

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lq2/b;->t:J

    return-void
.end method


# virtual methods
.method public A()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lq2/b;->q:Lo5/b;

    invoke-virtual {v0}, Lo5/b;->A()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public D0()Ljava/lang/String;
    .locals 1

    const-string v0, ""

    return-object v0
.end method

.method public E()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lq2/b;->q:Lo5/b;

    invoke-virtual {v0}, Lo5/b;->E()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public E0()Ljava/lang/String;
    .locals 1

    const-string v0, ""

    return-object v0
.end method

.method public F()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lq2/b;->q:Lo5/b;

    invoke-virtual {v0}, Lo5/b;->F()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public F0()Lo5/b;
    .locals 1

    iget-object v0, p0, Lq2/b;->q:Lo5/b;

    return-object v0
.end method

.method public G()Lcom/miui/packageInstaller/model/ApkInfo;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public G0()Ljava/util/Map;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public H()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lq2/b;->q:Lo5/b;

    invoke-virtual {v0}, Lo5/b;->H()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public H0(I)V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "android.intent.extra.INSTALL_RESULT"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    const/4 v1, -0x1

    :cond_0
    invoke-virtual {p0, v1, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    return-void
.end method

.method public I()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lq2/b;->q:Lo5/b;

    invoke-virtual {v0}, Lo5/b;->I()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public I0(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lq2/b;->q:Lo5/b;

    invoke-virtual {v0, p1}, Lo5/b;->y(Ljava/lang/String;)V

    return-void
.end method

.method public N(Ljava/lang/String;)Lo5/c;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public Q()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lq2/b;->q:Lo5/b;

    invoke-virtual {v0}, Lo5/b;->Q()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lq2/b;->q:Lo5/b;

    invoke-virtual {v0}, Lo5/b;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRef()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lq2/b;->q:Lo5/b;

    invoke-virtual {v0}, Lo5/b;->getRef()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getVersionName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lq2/b;->q:Lo5/b;

    invoke-virtual {v0}, Lo5/b;->getVersionName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public i()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lq2/b;->q:Lo5/b;

    invoke-virtual {v0}, Lo5/b;->i()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public k()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lq2/b;->q:Lo5/b;

    invoke-virtual {v0}, Lo5/b;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lq2/b;->q:Lo5/b;

    invoke-virtual {v0}, Lo5/b;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public n()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lq2/b;->q:Lo5/b;

    invoke-virtual {v0}, Lo5/b;->n()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4

    invoke-super {p0, p1}, Lmiuix/appcompat/app/j;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    iget-boolean v0, p0, Lq2/b;->r:Z

    iget p1, p1, Landroid/content/res/Configuration;->uiMode:I

    and-int/lit8 p1, p1, 0x30

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/16 v3, 0x20

    if-ne p1, v3, :cond_0

    move p1, v1

    goto :goto_0

    :cond_0
    move p1, v2

    :goto_0
    if-eq v0, p1, :cond_1

    iput-boolean v2, p0, Lq2/b;->s:Z

    invoke-virtual {p0}, Landroid/app/Activity;->recreate()V

    :cond_1
    sget-boolean p1, Lcom/android/packageinstaller/utils/g;->f:Z

    if-eqz p1, :cond_4

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object p1

    iget p1, p1, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 p1, p1, 0xf

    const/4 v0, 0x3

    if-ne p1, v0, :cond_2

    move v2, v1

    :cond_2
    if-eqz v2, :cond_3

    const/4 p1, 0x2

    invoke-virtual {p0, p1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    goto :goto_1

    :cond_3
    invoke-virtual {p0, v1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    :cond_4
    :goto_1
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "fromPage"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lo5/b;

    if-nez v0, :cond_0

    new-instance v0, Lo5/b;

    invoke-virtual {p0}, Lq2/b;->E0()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lq2/b;->D0()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lo5/b;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    new-instance v1, Lo5/b;

    invoke-virtual {p0}, Lq2/b;->E0()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lq2/b;->D0()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v0, v3}, Lo5/b;-><init>(Ljava/lang/String;Lo5/a;Ljava/lang/String;)V

    move-object v0, v1

    :goto_0
    iput-object v0, p0, Lq2/b;->q:Lo5/b;

    invoke-super {p0, p1}, Lmiuix/appcompat/app/j;->onCreate(Landroid/os/Bundle;)V

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result p1

    const/4 v0, 0x0

    const/4 v1, 0x2

    const/4 v2, 0x1

    if-eqz p1, :cond_1

    :goto_1
    invoke-virtual {p0, v1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    goto :goto_3

    :cond_1
    sget-boolean p1, Lcom/android/packageinstaller/utils/g;->f:Z

    if-eqz p1, :cond_3

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object p1

    iget p1, p1, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 p1, p1, 0xf

    const/4 v3, 0x3

    if-ne p1, v3, :cond_2

    move p1, v2

    goto :goto_2

    :cond_2
    move p1, v0

    :goto_2
    if-eqz p1, :cond_3

    goto :goto_1

    :cond_3
    invoke-virtual {p0, v2}, Landroid/app/Activity;->setRequestedOrientation(I)V

    :goto_3
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object p1

    iget p1, p1, Landroid/content/res/Configuration;->uiMode:I

    and-int/lit8 p1, p1, 0x30

    const/16 v1, 0x20

    if-ne p1, v1, :cond_4

    move v0, v2

    :cond_4
    iput-boolean v0, p0, Lq2/b;->r:Z

    iput-boolean v2, p0, Lq2/b;->s:Z

    return-void
.end method

.method protected onStop()V
    .locals 4

    invoke-super {p0}, Lmiuix/appcompat/app/j;->onStop()V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lq2/b;->t:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    invoke-virtual {p0}, Lq2/b;->E0()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v2, Lp5/l;

    const-string v3, ""

    invoke-direct {v2, v3, v3, p0}, Lp5/l;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    const-string v1, "duration"

    invoke-virtual {v2, v1, v0}, Lp5/f;->f(Ljava/lang/String;Ljava/lang/String;)Lp5/f;

    move-result-object v0

    invoke-virtual {p0}, Lq2/b;->G0()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Lp5/f;->g(Ljava/util/Map;)Lp5/f;

    move-result-object v0

    invoke-virtual {v0}, Lp5/f;->c()Z

    :cond_0
    return-void
.end method

.method public p()Lm5/e;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public startActivityForResult(Landroid/content/Intent;ILandroid/os/Bundle;)V
    .locals 2

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0}, Lj2/f;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lq2/b;->q:Lo5/b;

    const-string v1, "fromPage"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroidx/activity/ComponentActivity;->startActivityForResult(Landroid/content/Intent;ILandroid/os/Bundle;)V

    return-void
.end method

.method public t()Ljava/lang/Boolean;
    .locals 1

    iget-object v0, p0, Lq2/b;->q:Lo5/b;

    invoke-virtual {v0}, Lo5/b;->t()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public v()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lq2/b;->q:Lo5/b;

    invoke-virtual {v0}, Lo5/b;->v()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public x()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lq2/b;->q:Lo5/b;

    invoke-virtual {v0}, Lo5/b;->x()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public z()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lq2/b;->q:Lo5/b;

    invoke-virtual {v0}, Lo5/b;->z()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
