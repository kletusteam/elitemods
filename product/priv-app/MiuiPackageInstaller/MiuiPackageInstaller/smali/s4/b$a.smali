.class final Ls4/b$a;
.super Lp4/v;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ls4/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Lp4/v<",
        "Ljava/util/Collection<",
        "TE;>;>;"
    }
.end annotation


# instance fields
.field private final a:Lp4/v;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lp4/v<",
            "TE;>;"
        }
    .end annotation
.end field

.field private final b:Lr4/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lr4/i<",
            "+",
            "Ljava/util/Collection<",
            "TE;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lp4/e;Ljava/lang/reflect/Type;Lp4/v;Lr4/i;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lp4/e;",
            "Ljava/lang/reflect/Type;",
            "Lp4/v<",
            "TE;>;",
            "Lr4/i<",
            "+",
            "Ljava/util/Collection<",
            "TE;>;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Lp4/v;-><init>()V

    new-instance v0, Ls4/m;

    invoke-direct {v0, p1, p3, p2}, Ls4/m;-><init>(Lp4/e;Lp4/v;Ljava/lang/reflect/Type;)V

    iput-object v0, p0, Ls4/b$a;->a:Lp4/v;

    iput-object p4, p0, Ls4/b$a;->b:Lr4/i;

    return-void
.end method


# virtual methods
.method public bridge synthetic b(Lx4/a;)Ljava/lang/Object;
    .locals 0

    invoke-virtual {p0, p1}, Ls4/b$a;->e(Lx4/a;)Ljava/util/Collection;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic d(Lx4/c;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Ljava/util/Collection;

    invoke-virtual {p0, p1, p2}, Ls4/b$a;->f(Lx4/c;Ljava/util/Collection;)V

    return-void
.end method

.method public e(Lx4/a;)Ljava/util/Collection;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lx4/a;",
            ")",
            "Ljava/util/Collection<",
            "TE;>;"
        }
    .end annotation

    invoke-virtual {p1}, Lx4/a;->a0()Lx4/b;

    move-result-object v0

    sget-object v1, Lx4/b;->i:Lx4/b;

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lx4/a;->W()V

    const/4 p1, 0x0

    return-object p1

    :cond_0
    iget-object v0, p0, Ls4/b$a;->b:Lr4/i;

    invoke-interface {v0}, Lr4/i;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-virtual {p1}, Lx4/a;->b()V

    :goto_0
    invoke-virtual {p1}, Lx4/a;->B()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Ls4/b$a;->a:Lp4/v;

    invoke-virtual {v1, p1}, Lp4/v;->b(Lx4/a;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lx4/a;->r()V

    return-object v0
.end method

.method public f(Lx4/c;Ljava/util/Collection;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lx4/c;",
            "Ljava/util/Collection<",
            "TE;>;)V"
        }
    .end annotation

    if-nez p2, :cond_0

    invoke-virtual {p1}, Lx4/c;->F()Lx4/c;

    return-void

    :cond_0
    invoke-virtual {p1}, Lx4/c;->m()Lx4/c;

    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Ls4/b$a;->a:Lp4/v;

    invoke-virtual {v1, p1, v0}, Lp4/v;->d(Lx4/c;Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lx4/c;->r()Lx4/c;

    return-void
.end method
