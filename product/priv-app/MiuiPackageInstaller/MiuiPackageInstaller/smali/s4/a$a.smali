.class Ls4/a$a;
.super Ljava/lang/Object;

# interfaces
.implements Lp4/w;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ls4/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lp4/e;Lw4/a;)Lp4/v;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lp4/e;",
            "Lw4/a<",
            "TT;>;)",
            "Lp4/v<",
            "TT;>;"
        }
    .end annotation

    invoke-virtual {p2}, Lw4/a;->e()Ljava/lang/reflect/Type;

    move-result-object p2

    instance-of v0, p2, Ljava/lang/reflect/GenericArrayType;

    if-nez v0, :cond_1

    instance-of v0, p2, Ljava/lang/Class;

    if-eqz v0, :cond_0

    move-object v0, p2

    check-cast v0, Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->isArray()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 p1, 0x0

    return-object p1

    :cond_1
    invoke-static {p2}, Lr4/b;->g(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object p2

    invoke-static {p2}, Lw4/a;->b(Ljava/lang/reflect/Type;)Lw4/a;

    move-result-object v0

    invoke-virtual {p1, v0}, Lp4/e;->l(Lw4/a;)Lp4/v;

    move-result-object v0

    new-instance v1, Ls4/a;

    invoke-static {p2}, Lr4/b;->k(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object p2

    invoke-direct {v1, p1, v0, p2}, Ls4/a;-><init>(Lp4/e;Lp4/v;Ljava/lang/Class;)V

    return-object v1
.end method
