.class public final Ls4/e;
.super Ljava/lang/Object;

# interfaces
.implements Lp4/w;


# instance fields
.field private final a:Lr4/c;


# direct methods
.method public constructor <init>(Lr4/c;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Ls4/e;->a:Lr4/c;

    return-void
.end method


# virtual methods
.method public a(Lp4/e;Lw4/a;)Lp4/v;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lp4/e;",
            "Lw4/a<",
            "TT;>;)",
            "Lp4/v<",
            "TT;>;"
        }
    .end annotation

    invoke-virtual {p2}, Lw4/a;->c()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lq4/b;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lq4/b;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    iget-object v1, p0, Ls4/e;->a:Lr4/c;

    invoke-virtual {p0, v1, p1, p2, v0}, Ls4/e;->b(Lr4/c;Lp4/e;Lw4/a;Lq4/b;)Lp4/v;

    move-result-object p1

    return-object p1
.end method

.method b(Lr4/c;Lp4/e;Lw4/a;Lq4/b;)Lp4/v;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lr4/c;",
            "Lp4/e;",
            "Lw4/a<",
            "*>;",
            "Lq4/b;",
            ")",
            "Lp4/v<",
            "*>;"
        }
    .end annotation

    goto/32 :goto_1b

    nop

    :goto_0
    check-cast v1, Lp4/i;

    :goto_1
    goto/32 :goto_22

    nop

    :goto_2
    invoke-interface {p1}, Lr4/i;->a()Ljava/lang/Object;

    move-result-object p1

    goto/32 :goto_15

    nop

    :goto_3
    move-object v3, v1

    :goto_4
    goto/32 :goto_1f

    nop

    :goto_5
    invoke-virtual {p1}, Lp4/v;->a()Lp4/v;

    move-result-object p1

    :goto_6
    goto/32 :goto_b

    nop

    :goto_7
    if-nez v1, :cond_0

    goto/32 :goto_39

    :cond_0
    goto/32 :goto_38

    nop

    :goto_8
    goto :goto_4

    :goto_9
    goto/32 :goto_3

    nop

    :goto_a
    invoke-virtual {p4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_2d

    nop

    :goto_b
    return-object p1

    :goto_c
    if-nez v0, :cond_1

    goto/32 :goto_9

    :cond_1
    goto/32 :goto_29

    nop

    :goto_d
    const-string p1, " as a @JsonAdapter for "

    goto/32 :goto_13

    nop

    :goto_e
    move-object v2, p1

    goto/32 :goto_27

    nop

    :goto_f
    invoke-static {v0}, Lw4/a;->a(Ljava/lang/Class;)Lw4/a;

    move-result-object v0

    goto/32 :goto_1c

    nop

    :goto_10
    invoke-virtual {p4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_a

    nop

    :goto_11
    check-cast p1, Lp4/w;

    goto/32 :goto_17

    nop

    :goto_12
    invoke-virtual {p4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_1a

    nop

    :goto_13
    invoke-virtual {p4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_16

    nop

    :goto_14
    if-nez p2, :cond_2

    goto/32 :goto_6

    :cond_2
    goto/32 :goto_5

    nop

    :goto_15
    instance-of v0, p1, Lp4/v;

    goto/32 :goto_2c

    nop

    :goto_16
    invoke-virtual {p3}, Lw4/a;->toString()Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_1d

    nop

    :goto_17
    invoke-interface {p1, p2, p3}, Lp4/w;->a(Lp4/e;Lw4/a;)Lp4/v;

    move-result-object p1

    goto/32 :goto_3a

    nop

    :goto_18
    if-eqz v0, :cond_3

    goto/32 :goto_42

    :cond_3
    goto/32 :goto_3d

    nop

    :goto_19
    const-string v0, "Invalid attempt to bind an instance of "

    goto/32 :goto_12

    nop

    :goto_1a
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    goto/32 :goto_32

    nop

    :goto_1b
    invoke-interface {p4}, Lq4/b;->value()Ljava/lang/Class;

    move-result-object v0

    goto/32 :goto_f

    nop

    :goto_1c
    invoke-virtual {p1, v0}, Lr4/c;->a(Lw4/a;)Lr4/i;

    move-result-object p1

    goto/32 :goto_2

    nop

    :goto_1d
    invoke-virtual {p4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_23

    nop

    :goto_1e
    move-object v6, p3

    goto/32 :goto_3f

    nop

    :goto_1f
    instance-of v0, p1, Lp4/i;

    goto/32 :goto_26

    nop

    :goto_20
    check-cast p1, Lp4/v;

    goto/32 :goto_30

    nop

    :goto_21
    instance-of v0, p1, Lp4/q;

    goto/32 :goto_18

    nop

    :goto_22
    move-object v4, v1

    goto/32 :goto_35

    nop

    :goto_23
    const-string p1, ". @JsonAdapter value must be a TypeAdapter, TypeAdapterFactory, JsonSerializer or JsonDeserializer."

    goto/32 :goto_10

    nop

    :goto_24
    new-instance p2, Ljava/lang/IllegalArgumentException;

    goto/32 :goto_2a

    nop

    :goto_25
    const/4 v7, 0x0

    goto/32 :goto_e

    nop

    :goto_26
    if-nez v0, :cond_4

    goto/32 :goto_1

    :cond_4
    goto/32 :goto_2f

    nop

    :goto_27
    move-object v5, p2

    goto/32 :goto_1e

    nop

    :goto_28
    const/4 v1, 0x0

    goto/32 :goto_c

    nop

    :goto_29
    move-object v0, p1

    goto/32 :goto_3e

    nop

    :goto_2a
    new-instance p4, Ljava/lang/StringBuilder;

    goto/32 :goto_3c

    nop

    :goto_2b
    invoke-virtual {p4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_d

    nop

    :goto_2c
    if-nez v0, :cond_5

    goto/32 :goto_31

    :cond_5
    goto/32 :goto_20

    nop

    :goto_2d
    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_41

    nop

    :goto_2e
    if-nez p1, :cond_6

    goto/32 :goto_6

    :cond_6
    goto/32 :goto_33

    nop

    :goto_2f
    move-object v1, p1

    goto/32 :goto_0

    nop

    :goto_30
    goto :goto_40

    :goto_31
    goto/32 :goto_34

    nop

    :goto_32
    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_2b

    nop

    :goto_33
    invoke-interface {p4}, Lq4/b;->nullSafe()Z

    move-result p2

    goto/32 :goto_14

    nop

    :goto_34
    instance-of v0, p1, Lp4/w;

    goto/32 :goto_36

    nop

    :goto_35
    new-instance p1, Ls4/l;

    goto/32 :goto_25

    nop

    :goto_36
    if-nez v0, :cond_7

    goto/32 :goto_3b

    :cond_7
    goto/32 :goto_11

    nop

    :goto_37
    move-object v3, v0

    goto/32 :goto_8

    nop

    :goto_38
    goto :goto_42

    :goto_39
    goto/32 :goto_24

    nop

    :goto_3a
    goto :goto_40

    :goto_3b
    goto/32 :goto_21

    nop

    :goto_3c
    invoke-direct {p4}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_19

    nop

    :goto_3d
    instance-of v1, p1, Lp4/i;

    goto/32 :goto_7

    nop

    :goto_3e
    check-cast v0, Lp4/q;

    goto/32 :goto_37

    nop

    :goto_3f
    invoke-direct/range {v2 .. v7}, Ls4/l;-><init>(Lp4/q;Lp4/i;Lp4/e;Lw4/a;Lp4/w;)V

    :goto_40
    goto/32 :goto_2e

    nop

    :goto_41
    throw p2

    :goto_42
    goto/32 :goto_28

    nop
.end method
