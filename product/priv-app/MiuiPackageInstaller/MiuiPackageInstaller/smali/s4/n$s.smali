.class Ls4/n$s;
.super Lp4/v;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ls4/n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lp4/v<",
        "Lp4/j;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lp4/v;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic b(Lx4/a;)Ljava/lang/Object;
    .locals 0

    invoke-virtual {p0, p1}, Ls4/n$s;->e(Lx4/a;)Lp4/j;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic d(Lx4/c;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Lp4/j;

    invoke-virtual {p0, p1, p2}, Ls4/n$s;->f(Lx4/c;Lp4/j;)V

    return-void
.end method

.method public e(Lx4/a;)Lp4/j;
    .locals 3

    instance-of v0, p1, Ls4/f;

    if-eqz v0, :cond_0

    check-cast p1, Ls4/f;

    invoke-virtual {p1}, Ls4/f;->n0()Lp4/j;

    move-result-object p1

    return-object p1

    :cond_0
    sget-object v0, Ls4/n$z;->a:[I

    invoke-virtual {p1}, Lx4/a;->a0()Lx4/b;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    new-instance p1, Ljava/lang/IllegalArgumentException;

    invoke-direct {p1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw p1

    :pswitch_0
    new-instance v0, Lp4/m;

    invoke-direct {v0}, Lp4/m;-><init>()V

    invoke-virtual {p1}, Lx4/a;->g()V

    :goto_0
    invoke-virtual {p1}, Lx4/a;->B()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lx4/a;->U()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p1}, Ls4/n$s;->e(Lx4/a;)Lp4/j;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lp4/m;->h(Ljava/lang/String;Lp4/j;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lx4/a;->s()V

    return-object v0

    :pswitch_1
    new-instance v0, Lp4/g;

    invoke-direct {v0}, Lp4/g;-><init>()V

    invoke-virtual {p1}, Lx4/a;->b()V

    :goto_1
    invoke-virtual {p1}, Lx4/a;->B()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0, p1}, Ls4/n$s;->e(Lx4/a;)Lp4/j;

    move-result-object v1

    invoke-virtual {v0, v1}, Lp4/g;->h(Lp4/j;)V

    goto :goto_1

    :cond_2
    invoke-virtual {p1}, Lx4/a;->r()V

    return-object v0

    :pswitch_2
    invoke-virtual {p1}, Lx4/a;->W()V

    sget-object p1, Lp4/l;->a:Lp4/l;

    return-object p1

    :pswitch_3
    new-instance v0, Lp4/o;

    invoke-virtual {p1}, Lx4/a;->Y()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lp4/o;-><init>(Ljava/lang/String;)V

    return-object v0

    :pswitch_4
    new-instance v0, Lp4/o;

    invoke-virtual {p1}, Lx4/a;->J()Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-direct {v0, p1}, Lp4/o;-><init>(Ljava/lang/Boolean;)V

    return-object v0

    :pswitch_5
    invoke-virtual {p1}, Lx4/a;->Y()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Lp4/o;

    new-instance v1, Lr4/g;

    invoke-direct {v1, p1}, Lr4/g;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lp4/o;-><init>(Ljava/lang/Number;)V

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public f(Lx4/c;Lp4/j;)V
    .locals 2

    if-eqz p2, :cond_8

    invoke-virtual {p2}, Lp4/j;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_2

    :cond_0
    invoke-virtual {p2}, Lp4/j;->g()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p2}, Lp4/j;->c()Lp4/o;

    move-result-object p2

    invoke-virtual {p2}, Lp4/o;->p()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p2}, Lp4/o;->l()Ljava/lang/Number;

    move-result-object p2

    invoke-virtual {p1, p2}, Lx4/c;->b0(Ljava/lang/Number;)Lx4/c;

    goto/16 :goto_3

    :cond_1
    invoke-virtual {p2}, Lp4/o;->n()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p2}, Lp4/o;->h()Z

    move-result p2

    invoke-virtual {p1, p2}, Lx4/c;->d0(Z)Lx4/c;

    goto/16 :goto_3

    :cond_2
    invoke-virtual {p2}, Lp4/o;->m()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lx4/c;->c0(Ljava/lang/String;)Lx4/c;

    goto/16 :goto_3

    :cond_3
    invoke-virtual {p2}, Lp4/j;->d()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lx4/c;->m()Lx4/c;

    invoke-virtual {p2}, Lp4/j;->a()Lp4/g;

    move-result-object p2

    invoke-virtual {p2}, Lp4/g;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lp4/j;

    invoke-virtual {p0, p1, v0}, Ls4/n$s;->f(Lx4/c;Lp4/j;)V

    goto :goto_0

    :cond_4
    invoke-virtual {p1}, Lx4/c;->r()Lx4/c;

    goto :goto_3

    :cond_5
    invoke-virtual {p2}, Lp4/j;->f()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p1}, Lx4/c;->n()Lx4/c;

    invoke-virtual {p2}, Lp4/j;->b()Lp4/m;

    move-result-object p2

    invoke-virtual {p2}, Lp4/m;->i()Ljava/util/Set;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v1}, Lx4/c;->C(Ljava/lang/String;)Lx4/c;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lp4/j;

    invoke-virtual {p0, p1, v0}, Ls4/n$s;->f(Lx4/c;Lp4/j;)V

    goto :goto_1

    :cond_6
    invoke-virtual {p1}, Lx4/c;->s()Lx4/c;

    goto :goto_3

    :cond_7
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Couldn\'t write "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p2

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_8
    :goto_2
    invoke-virtual {p1}, Lx4/c;->F()Lx4/c;

    :goto_3
    return-void
.end method
