.class public final Ls4/n;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ls4/n$h0;
    }
.end annotation


# static fields
.field public static final A:Lp4/v;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lp4/v<",
            "Ljava/math/BigInteger;",
            ">;"
        }
    .end annotation
.end field

.field public static final B:Lp4/w;

.field public static final C:Lp4/v;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lp4/v<",
            "Ljava/lang/StringBuilder;",
            ">;"
        }
    .end annotation
.end field

.field public static final D:Lp4/w;

.field public static final E:Lp4/v;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lp4/v<",
            "Ljava/lang/StringBuffer;",
            ">;"
        }
    .end annotation
.end field

.field public static final F:Lp4/w;

.field public static final G:Lp4/v;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lp4/v<",
            "Ljava/net/URL;",
            ">;"
        }
    .end annotation
.end field

.field public static final H:Lp4/w;

.field public static final I:Lp4/v;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lp4/v<",
            "Ljava/net/URI;",
            ">;"
        }
    .end annotation
.end field

.field public static final J:Lp4/w;

.field public static final K:Lp4/v;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lp4/v<",
            "Ljava/net/InetAddress;",
            ">;"
        }
    .end annotation
.end field

.field public static final L:Lp4/w;

.field public static final M:Lp4/v;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lp4/v<",
            "Ljava/util/UUID;",
            ">;"
        }
    .end annotation
.end field

.field public static final N:Lp4/w;

.field public static final O:Lp4/v;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lp4/v<",
            "Ljava/util/Currency;",
            ">;"
        }
    .end annotation
.end field

.field public static final P:Lp4/w;

.field public static final Q:Lp4/v;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lp4/v<",
            "Ljava/util/Calendar;",
            ">;"
        }
    .end annotation
.end field

.field public static final R:Lp4/w;

.field public static final S:Lp4/v;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lp4/v<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field public static final T:Lp4/w;

.field public static final U:Lp4/v;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lp4/v<",
            "Lp4/j;",
            ">;"
        }
    .end annotation
.end field

.field public static final V:Lp4/w;

.field public static final W:Lp4/w;

.field public static final a:Lp4/v;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lp4/v<",
            "Ljava/lang/Class;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Lp4/w;

.field public static final c:Lp4/v;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lp4/v<",
            "Ljava/util/BitSet;",
            ">;"
        }
    .end annotation
.end field

.field public static final d:Lp4/w;

.field public static final e:Lp4/v;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lp4/v<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final f:Lp4/v;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lp4/v<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final g:Lp4/w;

.field public static final h:Lp4/v;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lp4/v<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field public static final i:Lp4/w;

.field public static final j:Lp4/v;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lp4/v<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field public static final k:Lp4/w;

.field public static final l:Lp4/v;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lp4/v<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field public static final m:Lp4/w;

.field public static final n:Lp4/v;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lp4/v<",
            "Ljava/util/concurrent/atomic/AtomicInteger;",
            ">;"
        }
    .end annotation
.end field

.field public static final o:Lp4/w;

.field public static final p:Lp4/v;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lp4/v<",
            "Ljava/util/concurrent/atomic/AtomicBoolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final q:Lp4/w;

.field public static final r:Lp4/v;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lp4/v<",
            "Ljava/util/concurrent/atomic/AtomicIntegerArray;",
            ">;"
        }
    .end annotation
.end field

.field public static final s:Lp4/w;

.field public static final t:Lp4/v;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lp4/v<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field public static final u:Lp4/v;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lp4/v<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field public static final v:Lp4/v;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lp4/v<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field public static final w:Lp4/v;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lp4/v<",
            "Ljava/lang/Character;",
            ">;"
        }
    .end annotation
.end field

.field public static final x:Lp4/w;

.field public static final y:Lp4/v;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lp4/v<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final z:Lp4/v;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lp4/v<",
            "Ljava/math/BigDecimal;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Ls4/n$k;

    invoke-direct {v0}, Ls4/n$k;-><init>()V

    invoke-virtual {v0}, Lp4/v;->a()Lp4/v;

    move-result-object v0

    sput-object v0, Ls4/n;->a:Lp4/v;

    const-class v1, Ljava/lang/Class;

    invoke-static {v1, v0}, Ls4/n;->b(Ljava/lang/Class;Lp4/v;)Lp4/w;

    move-result-object v0

    sput-object v0, Ls4/n;->b:Lp4/w;

    new-instance v0, Ls4/n$u;

    invoke-direct {v0}, Ls4/n$u;-><init>()V

    invoke-virtual {v0}, Lp4/v;->a()Lp4/v;

    move-result-object v0

    sput-object v0, Ls4/n;->c:Lp4/v;

    const-class v1, Ljava/util/BitSet;

    invoke-static {v1, v0}, Ls4/n;->b(Ljava/lang/Class;Lp4/v;)Lp4/w;

    move-result-object v0

    sput-object v0, Ls4/n;->d:Lp4/w;

    new-instance v0, Ls4/n$a0;

    invoke-direct {v0}, Ls4/n$a0;-><init>()V

    sput-object v0, Ls4/n;->e:Lp4/v;

    new-instance v1, Ls4/n$b0;

    invoke-direct {v1}, Ls4/n$b0;-><init>()V

    sput-object v1, Ls4/n;->f:Lp4/v;

    sget-object v1, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const-class v2, Ljava/lang/Boolean;

    invoke-static {v1, v2, v0}, Ls4/n;->a(Ljava/lang/Class;Ljava/lang/Class;Lp4/v;)Lp4/w;

    move-result-object v0

    sput-object v0, Ls4/n;->g:Lp4/w;

    new-instance v0, Ls4/n$c0;

    invoke-direct {v0}, Ls4/n$c0;-><init>()V

    sput-object v0, Ls4/n;->h:Lp4/v;

    sget-object v1, Ljava/lang/Byte;->TYPE:Ljava/lang/Class;

    const-class v2, Ljava/lang/Byte;

    invoke-static {v1, v2, v0}, Ls4/n;->a(Ljava/lang/Class;Ljava/lang/Class;Lp4/v;)Lp4/w;

    move-result-object v0

    sput-object v0, Ls4/n;->i:Lp4/w;

    new-instance v0, Ls4/n$d0;

    invoke-direct {v0}, Ls4/n$d0;-><init>()V

    sput-object v0, Ls4/n;->j:Lp4/v;

    sget-object v1, Ljava/lang/Short;->TYPE:Ljava/lang/Class;

    const-class v2, Ljava/lang/Short;

    invoke-static {v1, v2, v0}, Ls4/n;->a(Ljava/lang/Class;Ljava/lang/Class;Lp4/v;)Lp4/w;

    move-result-object v0

    sput-object v0, Ls4/n;->k:Lp4/w;

    new-instance v0, Ls4/n$e0;

    invoke-direct {v0}, Ls4/n$e0;-><init>()V

    sput-object v0, Ls4/n;->l:Lp4/v;

    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const-class v2, Ljava/lang/Integer;

    invoke-static {v1, v2, v0}, Ls4/n;->a(Ljava/lang/Class;Ljava/lang/Class;Lp4/v;)Lp4/w;

    move-result-object v0

    sput-object v0, Ls4/n;->m:Lp4/w;

    new-instance v0, Ls4/n$f0;

    invoke-direct {v0}, Ls4/n$f0;-><init>()V

    invoke-virtual {v0}, Lp4/v;->a()Lp4/v;

    move-result-object v0

    sput-object v0, Ls4/n;->n:Lp4/v;

    const-class v1, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-static {v1, v0}, Ls4/n;->b(Ljava/lang/Class;Lp4/v;)Lp4/w;

    move-result-object v0

    sput-object v0, Ls4/n;->o:Lp4/w;

    new-instance v0, Ls4/n$g0;

    invoke-direct {v0}, Ls4/n$g0;-><init>()V

    invoke-virtual {v0}, Lp4/v;->a()Lp4/v;

    move-result-object v0

    sput-object v0, Ls4/n;->p:Lp4/v;

    const-class v1, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-static {v1, v0}, Ls4/n;->b(Ljava/lang/Class;Lp4/v;)Lp4/w;

    move-result-object v0

    sput-object v0, Ls4/n;->q:Lp4/w;

    new-instance v0, Ls4/n$a;

    invoke-direct {v0}, Ls4/n$a;-><init>()V

    invoke-virtual {v0}, Lp4/v;->a()Lp4/v;

    move-result-object v0

    sput-object v0, Ls4/n;->r:Lp4/v;

    const-class v1, Ljava/util/concurrent/atomic/AtomicIntegerArray;

    invoke-static {v1, v0}, Ls4/n;->b(Ljava/lang/Class;Lp4/v;)Lp4/w;

    move-result-object v0

    sput-object v0, Ls4/n;->s:Lp4/w;

    new-instance v0, Ls4/n$b;

    invoke-direct {v0}, Ls4/n$b;-><init>()V

    sput-object v0, Ls4/n;->t:Lp4/v;

    new-instance v0, Ls4/n$c;

    invoke-direct {v0}, Ls4/n$c;-><init>()V

    sput-object v0, Ls4/n;->u:Lp4/v;

    new-instance v0, Ls4/n$d;

    invoke-direct {v0}, Ls4/n$d;-><init>()V

    sput-object v0, Ls4/n;->v:Lp4/v;

    new-instance v0, Ls4/n$e;

    invoke-direct {v0}, Ls4/n$e;-><init>()V

    sput-object v0, Ls4/n;->w:Lp4/v;

    sget-object v1, Ljava/lang/Character;->TYPE:Ljava/lang/Class;

    const-class v2, Ljava/lang/Character;

    invoke-static {v1, v2, v0}, Ls4/n;->a(Ljava/lang/Class;Ljava/lang/Class;Lp4/v;)Lp4/w;

    move-result-object v0

    sput-object v0, Ls4/n;->x:Lp4/w;

    new-instance v0, Ls4/n$f;

    invoke-direct {v0}, Ls4/n$f;-><init>()V

    sput-object v0, Ls4/n;->y:Lp4/v;

    new-instance v1, Ls4/n$g;

    invoke-direct {v1}, Ls4/n$g;-><init>()V

    sput-object v1, Ls4/n;->z:Lp4/v;

    new-instance v1, Ls4/n$h;

    invoke-direct {v1}, Ls4/n$h;-><init>()V

    sput-object v1, Ls4/n;->A:Lp4/v;

    const-class v1, Ljava/lang/String;

    invoke-static {v1, v0}, Ls4/n;->b(Ljava/lang/Class;Lp4/v;)Lp4/w;

    move-result-object v0

    sput-object v0, Ls4/n;->B:Lp4/w;

    new-instance v0, Ls4/n$i;

    invoke-direct {v0}, Ls4/n$i;-><init>()V

    sput-object v0, Ls4/n;->C:Lp4/v;

    const-class v1, Ljava/lang/StringBuilder;

    invoke-static {v1, v0}, Ls4/n;->b(Ljava/lang/Class;Lp4/v;)Lp4/w;

    move-result-object v0

    sput-object v0, Ls4/n;->D:Lp4/w;

    new-instance v0, Ls4/n$j;

    invoke-direct {v0}, Ls4/n$j;-><init>()V

    sput-object v0, Ls4/n;->E:Lp4/v;

    const-class v1, Ljava/lang/StringBuffer;

    invoke-static {v1, v0}, Ls4/n;->b(Ljava/lang/Class;Lp4/v;)Lp4/w;

    move-result-object v0

    sput-object v0, Ls4/n;->F:Lp4/w;

    new-instance v0, Ls4/n$l;

    invoke-direct {v0}, Ls4/n$l;-><init>()V

    sput-object v0, Ls4/n;->G:Lp4/v;

    const-class v1, Ljava/net/URL;

    invoke-static {v1, v0}, Ls4/n;->b(Ljava/lang/Class;Lp4/v;)Lp4/w;

    move-result-object v0

    sput-object v0, Ls4/n;->H:Lp4/w;

    new-instance v0, Ls4/n$m;

    invoke-direct {v0}, Ls4/n$m;-><init>()V

    sput-object v0, Ls4/n;->I:Lp4/v;

    const-class v1, Ljava/net/URI;

    invoke-static {v1, v0}, Ls4/n;->b(Ljava/lang/Class;Lp4/v;)Lp4/w;

    move-result-object v0

    sput-object v0, Ls4/n;->J:Lp4/w;

    new-instance v0, Ls4/n$n;

    invoke-direct {v0}, Ls4/n$n;-><init>()V

    sput-object v0, Ls4/n;->K:Lp4/v;

    const-class v1, Ljava/net/InetAddress;

    invoke-static {v1, v0}, Ls4/n;->d(Ljava/lang/Class;Lp4/v;)Lp4/w;

    move-result-object v0

    sput-object v0, Ls4/n;->L:Lp4/w;

    new-instance v0, Ls4/n$o;

    invoke-direct {v0}, Ls4/n$o;-><init>()V

    sput-object v0, Ls4/n;->M:Lp4/v;

    const-class v1, Ljava/util/UUID;

    invoke-static {v1, v0}, Ls4/n;->b(Ljava/lang/Class;Lp4/v;)Lp4/w;

    move-result-object v0

    sput-object v0, Ls4/n;->N:Lp4/w;

    new-instance v0, Ls4/n$p;

    invoke-direct {v0}, Ls4/n$p;-><init>()V

    invoke-virtual {v0}, Lp4/v;->a()Lp4/v;

    move-result-object v0

    sput-object v0, Ls4/n;->O:Lp4/v;

    const-class v1, Ljava/util/Currency;

    invoke-static {v1, v0}, Ls4/n;->b(Ljava/lang/Class;Lp4/v;)Lp4/w;

    move-result-object v0

    sput-object v0, Ls4/n;->P:Lp4/w;

    new-instance v0, Ls4/n$q;

    invoke-direct {v0}, Ls4/n$q;-><init>()V

    sput-object v0, Ls4/n;->Q:Lp4/v;

    const-class v1, Ljava/util/Calendar;

    const-class v2, Ljava/util/GregorianCalendar;

    invoke-static {v1, v2, v0}, Ls4/n;->c(Ljava/lang/Class;Ljava/lang/Class;Lp4/v;)Lp4/w;

    move-result-object v0

    sput-object v0, Ls4/n;->R:Lp4/w;

    new-instance v0, Ls4/n$r;

    invoke-direct {v0}, Ls4/n$r;-><init>()V

    sput-object v0, Ls4/n;->S:Lp4/v;

    const-class v1, Ljava/util/Locale;

    invoke-static {v1, v0}, Ls4/n;->b(Ljava/lang/Class;Lp4/v;)Lp4/w;

    move-result-object v0

    sput-object v0, Ls4/n;->T:Lp4/w;

    new-instance v0, Ls4/n$s;

    invoke-direct {v0}, Ls4/n$s;-><init>()V

    sput-object v0, Ls4/n;->U:Lp4/v;

    const-class v1, Lp4/j;

    invoke-static {v1, v0}, Ls4/n;->d(Ljava/lang/Class;Lp4/v;)Lp4/w;

    move-result-object v0

    sput-object v0, Ls4/n;->V:Lp4/w;

    new-instance v0, Ls4/n$t;

    invoke-direct {v0}, Ls4/n$t;-><init>()V

    sput-object v0, Ls4/n;->W:Lp4/w;

    return-void
.end method

.method public static a(Ljava/lang/Class;Ljava/lang/Class;Lp4/v;)Lp4/w;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TT:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class<",
            "TTT;>;",
            "Ljava/lang/Class<",
            "TTT;>;",
            "Lp4/v<",
            "-TTT;>;)",
            "Lp4/w;"
        }
    .end annotation

    new-instance v0, Ls4/n$w;

    invoke-direct {v0, p0, p1, p2}, Ls4/n$w;-><init>(Ljava/lang/Class;Ljava/lang/Class;Lp4/v;)V

    return-object v0
.end method

.method public static b(Ljava/lang/Class;Lp4/v;)Lp4/w;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TT:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class<",
            "TTT;>;",
            "Lp4/v<",
            "TTT;>;)",
            "Lp4/w;"
        }
    .end annotation

    new-instance v0, Ls4/n$v;

    invoke-direct {v0, p0, p1}, Ls4/n$v;-><init>(Ljava/lang/Class;Lp4/v;)V

    return-object v0
.end method

.method public static c(Ljava/lang/Class;Ljava/lang/Class;Lp4/v;)Lp4/w;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TT:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class<",
            "TTT;>;",
            "Ljava/lang/Class<",
            "+TTT;>;",
            "Lp4/v<",
            "-TTT;>;)",
            "Lp4/w;"
        }
    .end annotation

    new-instance v0, Ls4/n$x;

    invoke-direct {v0, p0, p1, p2}, Ls4/n$x;-><init>(Ljava/lang/Class;Ljava/lang/Class;Lp4/v;)V

    return-object v0
.end method

.method public static d(Ljava/lang/Class;Lp4/v;)Lp4/w;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T1:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class<",
            "TT1;>;",
            "Lp4/v<",
            "TT1;>;)",
            "Lp4/w;"
        }
    .end annotation

    new-instance v0, Ls4/n$y;

    invoke-direct {v0, p0, p1}, Ls4/n$y;-><init>(Ljava/lang/Class;Lp4/v;)V

    return-object v0
.end method
