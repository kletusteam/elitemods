.class public final Ls4/i;
.super Lp4/v;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lp4/v<",
        "Ljava/lang/Number;",
        ">;"
    }
.end annotation


# static fields
.field private static final b:Lp4/w;


# instance fields
.field private final a:Lp4/u;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lp4/t;->b:Lp4/t;

    invoke-static {v0}, Ls4/i;->f(Lp4/u;)Lp4/w;

    move-result-object v0

    sput-object v0, Ls4/i;->b:Lp4/w;

    return-void
.end method

.method private constructor <init>(Lp4/u;)V
    .locals 0

    invoke-direct {p0}, Lp4/v;-><init>()V

    iput-object p1, p0, Ls4/i;->a:Lp4/u;

    return-void
.end method

.method public static e(Lp4/u;)Lp4/w;
    .locals 1

    sget-object v0, Lp4/t;->b:Lp4/t;

    if-ne p0, v0, :cond_0

    sget-object p0, Ls4/i;->b:Lp4/w;

    return-object p0

    :cond_0
    invoke-static {p0}, Ls4/i;->f(Lp4/u;)Lp4/w;

    move-result-object p0

    return-object p0
.end method

.method private static f(Lp4/u;)Lp4/w;
    .locals 1

    new-instance v0, Ls4/i;

    invoke-direct {v0, p0}, Ls4/i;-><init>(Lp4/u;)V

    new-instance p0, Ls4/i$a;

    invoke-direct {p0, v0}, Ls4/i$a;-><init>(Ls4/i;)V

    return-object p0
.end method


# virtual methods
.method public bridge synthetic b(Lx4/a;)Ljava/lang/Object;
    .locals 0

    invoke-virtual {p0, p1}, Ls4/i;->g(Lx4/a;)Ljava/lang/Number;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic d(Lx4/c;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Ljava/lang/Number;

    invoke-virtual {p0, p1, p2}, Ls4/i;->h(Lx4/c;Ljava/lang/Number;)V

    return-void
.end method

.method public g(Lx4/a;)Ljava/lang/Number;
    .locals 3

    invoke-virtual {p1}, Lx4/a;->a0()Lx4/b;

    move-result-object v0

    sget-object v1, Ls4/i$b;->a:[I

    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v2

    aget v1, v1, v2

    const/4 v2, 0x1

    if-eq v1, v2, :cond_2

    const/4 v2, 0x2

    if-eq v1, v2, :cond_1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Lp4/r;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expecting number, got: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Lp4/r;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    :goto_0
    iget-object v0, p0, Ls4/i;->a:Lp4/u;

    invoke-interface {v0, p1}, Lp4/u;->a(Lx4/a;)Ljava/lang/Number;

    move-result-object p1

    return-object p1

    :cond_2
    invoke-virtual {p1}, Lx4/a;->W()V

    const/4 p1, 0x0

    return-object p1
.end method

.method public h(Lx4/c;Ljava/lang/Number;)V
    .locals 0

    invoke-virtual {p1, p2}, Lx4/c;->b0(Ljava/lang/Number;)Lx4/c;

    return-void
.end method
