.class Ls4/n$b;
.super Lp4/v;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ls4/n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lp4/v<",
        "Ljava/lang/Number;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lp4/v;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic b(Lx4/a;)Ljava/lang/Object;
    .locals 0

    invoke-virtual {p0, p1}, Ls4/n$b;->e(Lx4/a;)Ljava/lang/Number;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic d(Lx4/c;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Ljava/lang/Number;

    invoke-virtual {p0, p1, p2}, Ls4/n$b;->f(Lx4/c;Ljava/lang/Number;)V

    return-void
.end method

.method public e(Lx4/a;)Ljava/lang/Number;
    .locals 2

    invoke-virtual {p1}, Lx4/a;->a0()Lx4/b;

    move-result-object v0

    sget-object v1, Lx4/b;->i:Lx4/b;

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lx4/a;->W()V

    const/4 p1, 0x0

    return-object p1

    :cond_0
    :try_start_0
    invoke-virtual {p1}, Lx4/a;->M()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    new-instance v0, Lp4/r;

    invoke-direct {v0, p1}, Lp4/r;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method public f(Lx4/c;Ljava/lang/Number;)V
    .locals 0

    invoke-virtual {p1, p2}, Lx4/c;->b0(Ljava/lang/Number;)Lx4/c;

    return-void
.end method
