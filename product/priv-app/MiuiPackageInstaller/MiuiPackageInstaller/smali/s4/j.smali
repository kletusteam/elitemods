.class public final Ls4/j;
.super Lp4/v;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lp4/v<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# static fields
.field private static final c:Lp4/w;


# instance fields
.field private final a:Lp4/e;

.field private final b:Lp4/u;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lp4/t;->a:Lp4/t;

    invoke-static {v0}, Ls4/j;->f(Lp4/u;)Lp4/w;

    move-result-object v0

    sput-object v0, Ls4/j;->c:Lp4/w;

    return-void
.end method

.method private constructor <init>(Lp4/e;Lp4/u;)V
    .locals 0

    invoke-direct {p0}, Lp4/v;-><init>()V

    iput-object p1, p0, Ls4/j;->a:Lp4/e;

    iput-object p2, p0, Ls4/j;->b:Lp4/u;

    return-void
.end method

.method synthetic constructor <init>(Lp4/e;Lp4/u;Ls4/j$a;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ls4/j;-><init>(Lp4/e;Lp4/u;)V

    return-void
.end method

.method public static e(Lp4/u;)Lp4/w;
    .locals 1

    sget-object v0, Lp4/t;->a:Lp4/t;

    if-ne p0, v0, :cond_0

    sget-object p0, Ls4/j;->c:Lp4/w;

    return-object p0

    :cond_0
    invoke-static {p0}, Ls4/j;->f(Lp4/u;)Lp4/w;

    move-result-object p0

    return-object p0
.end method

.method private static f(Lp4/u;)Lp4/w;
    .locals 1

    new-instance v0, Ls4/j$a;

    invoke-direct {v0, p0}, Ls4/j$a;-><init>(Lp4/u;)V

    return-object v0
.end method


# virtual methods
.method public b(Lx4/a;)Ljava/lang/Object;
    .locals 3

    invoke-virtual {p1}, Lx4/a;->a0()Lx4/b;

    move-result-object v0

    sget-object v1, Ls4/j$b;->a:[I

    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1}, Ljava/lang/IllegalStateException;-><init>()V

    throw p1

    :pswitch_0
    invoke-virtual {p1}, Lx4/a;->W()V

    const/4 p1, 0x0

    return-object p1

    :pswitch_1
    invoke-virtual {p1}, Lx4/a;->J()Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1

    :pswitch_2
    iget-object v0, p0, Ls4/j;->b:Lp4/u;

    invoke-interface {v0, p1}, Lp4/u;->a(Lx4/a;)Ljava/lang/Number;

    move-result-object p1

    return-object p1

    :pswitch_3
    invoke-virtual {p1}, Lx4/a;->Y()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :pswitch_4
    new-instance v0, Lr4/h;

    invoke-direct {v0}, Lr4/h;-><init>()V

    invoke-virtual {p1}, Lx4/a;->g()V

    :goto_0
    invoke-virtual {p1}, Lx4/a;->B()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lx4/a;->U()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p1}, Ls4/j;->b(Lx4/a;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lx4/a;->s()V

    return-object v0

    :pswitch_5
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p1}, Lx4/a;->b()V

    :goto_1
    invoke-virtual {p1}, Lx4/a;->B()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0, p1}, Ls4/j;->b(Lx4/a;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    invoke-virtual {p1}, Lx4/a;->r()V

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public d(Lx4/c;Ljava/lang/Object;)V
    .locals 2

    if-nez p2, :cond_0

    invoke-virtual {p1}, Lx4/c;->F()Lx4/c;

    return-void

    :cond_0
    iget-object v0, p0, Ls4/j;->a:Lp4/e;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Lp4/e;->k(Ljava/lang/Class;)Lp4/v;

    move-result-object v0

    instance-of v1, v0, Ls4/j;

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lx4/c;->n()Lx4/c;

    invoke-virtual {p1}, Lx4/c;->s()Lx4/c;

    return-void

    :cond_1
    invoke-virtual {v0, p1, p2}, Lp4/v;->d(Lx4/c;Ljava/lang/Object;)V

    return-void
.end method
