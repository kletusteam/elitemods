.class Ls4/k$a;
.super Ls4/k$c;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Ls4/k;->b(Lp4/e;Ljava/lang/reflect/Field;Ljava/lang/String;Lw4/a;ZZ)Ls4/k$c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic d:Ljava/lang/reflect/Field;

.field final synthetic e:Z

.field final synthetic f:Lp4/v;

.field final synthetic g:Lp4/e;

.field final synthetic h:Lw4/a;

.field final synthetic i:Z

.field final synthetic j:Ls4/k;


# direct methods
.method constructor <init>(Ls4/k;Ljava/lang/String;ZZLjava/lang/reflect/Field;ZLp4/v;Lp4/e;Lw4/a;Z)V
    .locals 0

    iput-object p1, p0, Ls4/k$a;->j:Ls4/k;

    iput-object p5, p0, Ls4/k$a;->d:Ljava/lang/reflect/Field;

    iput-boolean p6, p0, Ls4/k$a;->e:Z

    iput-object p7, p0, Ls4/k$a;->f:Lp4/v;

    iput-object p8, p0, Ls4/k$a;->g:Lp4/e;

    iput-object p9, p0, Ls4/k$a;->h:Lw4/a;

    iput-boolean p10, p0, Ls4/k$a;->i:Z

    invoke-direct {p0, p2, p3, p4}, Ls4/k$c;-><init>(Ljava/lang/String;ZZ)V

    return-void
.end method


# virtual methods
.method a(Lx4/a;Ljava/lang/Object;)V
    .locals 1

    goto/32 :goto_1

    nop

    :goto_0
    return-void

    :goto_1
    iget-object v0, p0, Ls4/k$a;->f:Lp4/v;

    goto/32 :goto_2

    nop

    :goto_2
    invoke-virtual {v0, p1}, Lp4/v;->b(Lx4/a;)Ljava/lang/Object;

    move-result-object p1

    goto/32 :goto_4

    nop

    :goto_3
    iget-boolean v0, p0, Ls4/k$a;->i:Z

    goto/32 :goto_7

    nop

    :goto_4
    if-eqz p1, :cond_0

    goto/32 :goto_8

    :cond_0
    goto/32 :goto_3

    nop

    :goto_5
    invoke-virtual {v0, p2, p1}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_6
    goto/32 :goto_0

    nop

    :goto_7
    if-eqz v0, :cond_1

    goto/32 :goto_6

    :cond_1
    :goto_8
    goto/32 :goto_9

    nop

    :goto_9
    iget-object v0, p0, Ls4/k$a;->d:Ljava/lang/reflect/Field;

    goto/32 :goto_5

    nop
.end method

.method b(Lx4/c;Ljava/lang/Object;)V
    .locals 4

    goto/32 :goto_f

    nop

    :goto_0
    invoke-direct {v0, v1, v2, v3}, Ls4/m;-><init>(Lp4/e;Lp4/v;Ljava/lang/reflect/Type;)V

    :goto_1
    goto/32 :goto_a

    nop

    :goto_2
    invoke-virtual {v3}, Lw4/a;->e()Ljava/lang/reflect/Type;

    move-result-object v3

    goto/32 :goto_0

    nop

    :goto_3
    if-nez v0, :cond_0

    goto/32 :goto_5

    :cond_0
    goto/32 :goto_d

    nop

    :goto_4
    goto :goto_1

    :goto_5
    goto/32 :goto_9

    nop

    :goto_6
    iget-object v1, p0, Ls4/k$a;->g:Lp4/e;

    goto/32 :goto_b

    nop

    :goto_7
    invoke-virtual {v0, p2}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    goto/32 :goto_8

    nop

    :goto_8
    iget-boolean v0, p0, Ls4/k$a;->e:Z

    goto/32 :goto_3

    nop

    :goto_9
    new-instance v0, Ls4/m;

    goto/32 :goto_6

    nop

    :goto_a
    invoke-virtual {v0, p1, p2}, Lp4/v;->d(Lx4/c;Ljava/lang/Object;)V

    goto/32 :goto_e

    nop

    :goto_b
    iget-object v2, p0, Ls4/k$a;->f:Lp4/v;

    goto/32 :goto_c

    nop

    :goto_c
    iget-object v3, p0, Ls4/k$a;->h:Lw4/a;

    goto/32 :goto_2

    nop

    :goto_d
    iget-object v0, p0, Ls4/k$a;->f:Lp4/v;

    goto/32 :goto_4

    nop

    :goto_e
    return-void

    :goto_f
    iget-object v0, p0, Ls4/k$a;->d:Ljava/lang/reflect/Field;

    goto/32 :goto_7

    nop
.end method

.method public c(Ljava/lang/Object;)Z
    .locals 2

    iget-boolean v0, p0, Ls4/k$c;->b:Z

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    iget-object v0, p0, Ls4/k$a;->d:Ljava/lang/reflect/Field;

    invoke-virtual {v0, p1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eq v0, p1, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method
