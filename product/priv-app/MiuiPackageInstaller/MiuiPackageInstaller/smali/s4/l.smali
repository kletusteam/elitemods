.class public final Ls4/l;
.super Lp4/v;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ls4/l$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lp4/v<",
        "TT;>;"
    }
.end annotation


# instance fields
.field private final a:Lp4/q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lp4/q<",
            "TT;>;"
        }
    .end annotation
.end field

.field private final b:Lp4/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lp4/i<",
            "TT;>;"
        }
    .end annotation
.end field

.field final c:Lp4/e;

.field private final d:Lw4/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lw4/a<",
            "TT;>;"
        }
    .end annotation
.end field

.field private final e:Lp4/w;

.field private final f:Ls4/l$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ls4/l<",
            "TT;>.b;"
        }
    .end annotation
.end field

.field private g:Lp4/v;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lp4/v<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lp4/q;Lp4/i;Lp4/e;Lw4/a;Lp4/w;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lp4/q<",
            "TT;>;",
            "Lp4/i<",
            "TT;>;",
            "Lp4/e;",
            "Lw4/a<",
            "TT;>;",
            "Lp4/w;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Lp4/v;-><init>()V

    new-instance v0, Ls4/l$b;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Ls4/l$b;-><init>(Ls4/l;Ls4/l$a;)V

    iput-object v0, p0, Ls4/l;->f:Ls4/l$b;

    iput-object p1, p0, Ls4/l;->a:Lp4/q;

    iput-object p2, p0, Ls4/l;->b:Lp4/i;

    iput-object p3, p0, Ls4/l;->c:Lp4/e;

    iput-object p4, p0, Ls4/l;->d:Lw4/a;

    iput-object p5, p0, Ls4/l;->e:Lp4/w;

    return-void
.end method

.method private e()Lp4/v;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lp4/v<",
            "TT;>;"
        }
    .end annotation

    iget-object v0, p0, Ls4/l;->g:Lp4/v;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Ls4/l;->c:Lp4/e;

    iget-object v1, p0, Ls4/l;->e:Lp4/w;

    iget-object v2, p0, Ls4/l;->d:Lw4/a;

    invoke-virtual {v0, v1, v2}, Lp4/e;->m(Lp4/w;Lw4/a;)Lp4/v;

    move-result-object v0

    iput-object v0, p0, Ls4/l;->g:Lp4/v;

    :goto_0
    return-object v0
.end method


# virtual methods
.method public b(Lx4/a;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lx4/a;",
            ")TT;"
        }
    .end annotation

    iget-object v0, p0, Ls4/l;->b:Lp4/i;

    if-nez v0, :cond_0

    invoke-direct {p0}, Ls4/l;->e()Lp4/v;

    move-result-object v0

    invoke-virtual {v0, p1}, Lp4/v;->b(Lx4/a;)Ljava/lang/Object;

    move-result-object p1

    return-object p1

    :cond_0
    invoke-static {p1}, Lr4/l;->a(Lx4/a;)Lp4/j;

    move-result-object p1

    invoke-virtual {p1}, Lp4/j;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 p1, 0x0

    return-object p1

    :cond_1
    iget-object v0, p0, Ls4/l;->b:Lp4/i;

    iget-object v1, p0, Ls4/l;->d:Lw4/a;

    invoke-virtual {v1}, Lw4/a;->e()Ljava/lang/reflect/Type;

    move-result-object v1

    iget-object v2, p0, Ls4/l;->f:Ls4/l$b;

    invoke-interface {v0, p1, v1, v2}, Lp4/i;->a(Lp4/j;Ljava/lang/reflect/Type;Lp4/h;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public d(Lx4/c;Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lx4/c;",
            "TT;)V"
        }
    .end annotation

    iget-object v0, p0, Ls4/l;->a:Lp4/q;

    if-nez v0, :cond_0

    invoke-direct {p0}, Ls4/l;->e()Lp4/v;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lp4/v;->d(Lx4/c;Ljava/lang/Object;)V

    return-void

    :cond_0
    if-nez p2, :cond_1

    invoke-virtual {p1}, Lx4/c;->F()Lx4/c;

    return-void

    :cond_1
    iget-object v1, p0, Ls4/l;->d:Lw4/a;

    invoke-virtual {v1}, Lw4/a;->e()Ljava/lang/reflect/Type;

    move-result-object v1

    iget-object v2, p0, Ls4/l;->f:Ls4/l$b;

    invoke-interface {v0, p2, v1, v2}, Lp4/q;->a(Ljava/lang/Object;Ljava/lang/reflect/Type;Lp4/p;)Lp4/j;

    move-result-object p2

    invoke-static {p2, p1}, Lr4/l;->b(Lp4/j;Lx4/c;)V

    return-void
.end method
