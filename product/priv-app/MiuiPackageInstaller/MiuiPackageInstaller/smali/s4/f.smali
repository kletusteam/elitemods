.class public final Ls4/f;
.super Lx4/a;


# static fields
.field private static final t:Ljava/io/Reader;

.field private static final u:Ljava/lang/Object;


# instance fields
.field private p:[Ljava/lang/Object;

.field private q:I

.field private r:[Ljava/lang/String;

.field private s:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ls4/f$a;

    invoke-direct {v0}, Ls4/f$a;-><init>()V

    sput-object v0, Ls4/f;->t:Ljava/io/Reader;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Ls4/f;->u:Ljava/lang/Object;

    return-void
.end method

.method private F()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " at path "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ls4/f;->z()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private m0(Lx4/b;)V
    .locals 3

    invoke-virtual {p0}, Ls4/f;->a0()Lx4/b;

    move-result-object v0

    if-ne v0, p1, :cond_0

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, " but was "

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ls4/f;->a0()Lx4/b;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ls4/f;->F()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private o0()Ljava/lang/Object;
    .locals 2

    iget-object v0, p0, Ls4/f;->p:[Ljava/lang/Object;

    iget v1, p0, Ls4/f;->q:I

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    return-object v0
.end method

.method private p0()Ljava/lang/Object;
    .locals 4

    iget-object v0, p0, Ls4/f;->p:[Ljava/lang/Object;

    iget v1, p0, Ls4/f;->q:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Ls4/f;->q:I

    aget-object v2, v0, v1

    const/4 v3, 0x0

    aput-object v3, v0, v1

    return-object v2
.end method

.method private r0(Ljava/lang/Object;)V
    .locals 3

    iget v0, p0, Ls4/f;->q:I

    iget-object v1, p0, Ls4/f;->p:[Ljava/lang/Object;

    array-length v2, v1

    if-ne v0, v2, :cond_0

    mul-int/lit8 v0, v0, 0x2

    invoke-static {v1, v0}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v1

    iput-object v1, p0, Ls4/f;->p:[Ljava/lang/Object;

    iget-object v1, p0, Ls4/f;->s:[I

    invoke-static {v1, v0}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v1

    iput-object v1, p0, Ls4/f;->s:[I

    iget-object v1, p0, Ls4/f;->r:[Ljava/lang/String;

    invoke-static {v1, v0}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, p0, Ls4/f;->r:[Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Ls4/f;->p:[Ljava/lang/Object;

    iget v1, p0, Ls4/f;->q:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Ls4/f;->q:I

    aput-object p1, v0, v1

    return-void
.end method


# virtual methods
.method public B()Z
    .locals 2

    invoke-virtual {p0}, Ls4/f;->a0()Lx4/b;

    move-result-object v0

    sget-object v1, Lx4/b;->d:Lx4/b;

    if-eq v0, v1, :cond_0

    sget-object v1, Lx4/b;->b:Lx4/b;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public J()Z
    .locals 4

    sget-object v0, Lx4/b;->h:Lx4/b;

    invoke-direct {p0, v0}, Ls4/f;->m0(Lx4/b;)V

    invoke-direct {p0}, Ls4/f;->p0()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lp4/o;

    invoke-virtual {v0}, Lp4/o;->h()Z

    move-result v0

    iget v1, p0, Ls4/f;->q:I

    if-lez v1, :cond_0

    iget-object v2, p0, Ls4/f;->s:[I

    add-int/lit8 v1, v1, -0x1

    aget v3, v2, v1

    add-int/lit8 v3, v3, 0x1

    aput v3, v2, v1

    :cond_0
    return v0
.end method

.method public K()D
    .locals 5

    invoke-virtual {p0}, Ls4/f;->a0()Lx4/b;

    move-result-object v0

    sget-object v1, Lx4/b;->g:Lx4/b;

    if-eq v0, v1, :cond_1

    sget-object v2, Lx4/b;->f:Lx4/b;

    if-ne v0, v2, :cond_0

    goto :goto_0

    :cond_0
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Expected "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, " but was "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ls4/f;->F()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1
    :goto_0
    invoke-direct {p0}, Ls4/f;->o0()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lp4/o;

    invoke-virtual {v0}, Lp4/o;->i()D

    move-result-wide v0

    invoke-virtual {p0}, Lx4/a;->C()Z

    move-result v2

    if-nez v2, :cond_3

    invoke-static {v0, v1}, Ljava/lang/Double;->isNaN(D)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-static {v0, v1}, Ljava/lang/Double;->isInfinite(D)Z

    move-result v2

    if-nez v2, :cond_2

    goto :goto_1

    :cond_2
    new-instance v2, Ljava/lang/NumberFormatException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "JSON forbids NaN and infinities: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_3
    :goto_1
    invoke-direct {p0}, Ls4/f;->p0()Ljava/lang/Object;

    iget v2, p0, Ls4/f;->q:I

    if-lez v2, :cond_4

    iget-object v3, p0, Ls4/f;->s:[I

    add-int/lit8 v2, v2, -0x1

    aget v4, v3, v2

    add-int/lit8 v4, v4, 0x1

    aput v4, v3, v2

    :cond_4
    return-wide v0
.end method

.method public L()I
    .locals 5

    invoke-virtual {p0}, Ls4/f;->a0()Lx4/b;

    move-result-object v0

    sget-object v1, Lx4/b;->g:Lx4/b;

    if-eq v0, v1, :cond_1

    sget-object v2, Lx4/b;->f:Lx4/b;

    if-ne v0, v2, :cond_0

    goto :goto_0

    :cond_0
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Expected "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, " but was "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ls4/f;->F()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1
    :goto_0
    invoke-direct {p0}, Ls4/f;->o0()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lp4/o;

    invoke-virtual {v0}, Lp4/o;->j()I

    move-result v0

    invoke-direct {p0}, Ls4/f;->p0()Ljava/lang/Object;

    iget v1, p0, Ls4/f;->q:I

    if-lez v1, :cond_2

    iget-object v2, p0, Ls4/f;->s:[I

    add-int/lit8 v1, v1, -0x1

    aget v3, v2, v1

    add-int/lit8 v3, v3, 0x1

    aput v3, v2, v1

    :cond_2
    return v0
.end method

.method public M()J
    .locals 5

    invoke-virtual {p0}, Ls4/f;->a0()Lx4/b;

    move-result-object v0

    sget-object v1, Lx4/b;->g:Lx4/b;

    if-eq v0, v1, :cond_1

    sget-object v2, Lx4/b;->f:Lx4/b;

    if-ne v0, v2, :cond_0

    goto :goto_0

    :cond_0
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Expected "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, " but was "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ls4/f;->F()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1
    :goto_0
    invoke-direct {p0}, Ls4/f;->o0()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lp4/o;

    invoke-virtual {v0}, Lp4/o;->k()J

    move-result-wide v0

    invoke-direct {p0}, Ls4/f;->p0()Ljava/lang/Object;

    iget v2, p0, Ls4/f;->q:I

    if-lez v2, :cond_2

    iget-object v3, p0, Ls4/f;->s:[I

    add-int/lit8 v2, v2, -0x1

    aget v4, v3, v2

    add-int/lit8 v4, v4, 0x1

    aput v4, v3, v2

    :cond_2
    return-wide v0
.end method

.method public U()Ljava/lang/String;
    .locals 4

    sget-object v0, Lx4/b;->e:Lx4/b;

    invoke-direct {p0, v0}, Ls4/f;->m0(Lx4/b;)V

    invoke-direct {p0}, Ls4/f;->o0()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v2, p0, Ls4/f;->r:[Ljava/lang/String;

    iget v3, p0, Ls4/f;->q:I

    add-int/lit8 v3, v3, -0x1

    aput-object v1, v2, v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-direct {p0, v0}, Ls4/f;->r0(Ljava/lang/Object;)V

    return-object v1
.end method

.method public W()V
    .locals 3

    sget-object v0, Lx4/b;->i:Lx4/b;

    invoke-direct {p0, v0}, Ls4/f;->m0(Lx4/b;)V

    invoke-direct {p0}, Ls4/f;->p0()Ljava/lang/Object;

    iget v0, p0, Ls4/f;->q:I

    if-lez v0, :cond_0

    iget-object v1, p0, Ls4/f;->s:[I

    add-int/lit8 v0, v0, -0x1

    aget v2, v1, v0

    add-int/lit8 v2, v2, 0x1

    aput v2, v1, v0

    :cond_0
    return-void
.end method

.method public Y()Ljava/lang/String;
    .locals 5

    invoke-virtual {p0}, Ls4/f;->a0()Lx4/b;

    move-result-object v0

    sget-object v1, Lx4/b;->f:Lx4/b;

    if-eq v0, v1, :cond_1

    sget-object v2, Lx4/b;->g:Lx4/b;

    if-ne v0, v2, :cond_0

    goto :goto_0

    :cond_0
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Expected "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, " but was "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ls4/f;->F()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1
    :goto_0
    invoke-direct {p0}, Ls4/f;->p0()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lp4/o;

    invoke-virtual {v0}, Lp4/o;->m()Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Ls4/f;->q:I

    if-lez v1, :cond_2

    iget-object v2, p0, Ls4/f;->s:[I

    add-int/lit8 v1, v1, -0x1

    aget v3, v2, v1

    add-int/lit8 v3, v3, 0x1

    aput v3, v2, v1

    :cond_2
    return-object v0
.end method

.method public a0()Lx4/b;
    .locals 3

    iget v0, p0, Ls4/f;->q:I

    if-nez v0, :cond_0

    sget-object v0, Lx4/b;->j:Lx4/b;

    return-object v0

    :cond_0
    invoke-direct {p0}, Ls4/f;->o0()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Ljava/util/Iterator;

    if-eqz v1, :cond_4

    iget-object v1, p0, Ls4/f;->p:[Ljava/lang/Object;

    iget v2, p0, Ls4/f;->q:I

    add-int/lit8 v2, v2, -0x2

    aget-object v1, v1, v2

    instance-of v1, v1, Lp4/m;

    check-cast v0, Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    if-eqz v1, :cond_1

    sget-object v0, Lx4/b;->e:Lx4/b;

    return-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    invoke-direct {p0, v0}, Ls4/f;->r0(Ljava/lang/Object;)V

    invoke-virtual {p0}, Ls4/f;->a0()Lx4/b;

    move-result-object v0

    return-object v0

    :cond_2
    if-eqz v1, :cond_3

    sget-object v0, Lx4/b;->d:Lx4/b;

    goto :goto_0

    :cond_3
    sget-object v0, Lx4/b;->b:Lx4/b;

    :goto_0
    return-object v0

    :cond_4
    instance-of v1, v0, Lp4/m;

    if-eqz v1, :cond_5

    sget-object v0, Lx4/b;->c:Lx4/b;

    return-object v0

    :cond_5
    instance-of v1, v0, Lp4/g;

    if-eqz v1, :cond_6

    sget-object v0, Lx4/b;->a:Lx4/b;

    return-object v0

    :cond_6
    instance-of v1, v0, Lp4/o;

    if-eqz v1, :cond_a

    check-cast v0, Lp4/o;

    invoke-virtual {v0}, Lp4/o;->q()Z

    move-result v1

    if-eqz v1, :cond_7

    sget-object v0, Lx4/b;->f:Lx4/b;

    return-object v0

    :cond_7
    invoke-virtual {v0}, Lp4/o;->n()Z

    move-result v1

    if-eqz v1, :cond_8

    sget-object v0, Lx4/b;->h:Lx4/b;

    return-object v0

    :cond_8
    invoke-virtual {v0}, Lp4/o;->p()Z

    move-result v0

    if-eqz v0, :cond_9

    sget-object v0, Lx4/b;->g:Lx4/b;

    return-object v0

    :cond_9
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_a
    instance-of v1, v0, Lp4/l;

    if-eqz v1, :cond_b

    sget-object v0, Lx4/b;->i:Lx4/b;

    return-object v0

    :cond_b
    sget-object v1, Ls4/f;->u:Ljava/lang/Object;

    if-ne v0, v1, :cond_c

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "JsonReader is closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_c
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method public b()V
    .locals 3

    sget-object v0, Lx4/b;->a:Lx4/b;

    invoke-direct {p0, v0}, Ls4/f;->m0(Lx4/b;)V

    invoke-direct {p0}, Ls4/f;->o0()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lp4/g;

    invoke-virtual {v0}, Lp4/g;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-direct {p0, v0}, Ls4/f;->r0(Ljava/lang/Object;)V

    iget-object v0, p0, Ls4/f;->s:[I

    iget v1, p0, Ls4/f;->q:I

    add-int/lit8 v1, v1, -0x1

    const/4 v2, 0x0

    aput v2, v0, v1

    return-void
.end method

.method public close()V
    .locals 4

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    sget-object v2, Ls4/f;->u:Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    iput-object v1, p0, Ls4/f;->p:[Ljava/lang/Object;

    iput v0, p0, Ls4/f;->q:I

    return-void
.end method

.method public g()V
    .locals 1

    sget-object v0, Lx4/b;->c:Lx4/b;

    invoke-direct {p0, v0}, Ls4/f;->m0(Lx4/b;)V

    invoke-direct {p0}, Ls4/f;->o0()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lp4/m;

    invoke-virtual {v0}, Lp4/m;->i()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-direct {p0, v0}, Ls4/f;->r0(Ljava/lang/Object;)V

    return-void
.end method

.method public k0()V
    .locals 3

    invoke-virtual {p0}, Ls4/f;->a0()Lx4/b;

    move-result-object v0

    sget-object v1, Lx4/b;->e:Lx4/b;

    const-string v2, "null"

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Ls4/f;->U()Ljava/lang/String;

    iget-object v0, p0, Ls4/f;->r:[Ljava/lang/String;

    iget v1, p0, Ls4/f;->q:I

    add-int/lit8 v1, v1, -0x2

    aput-object v2, v0, v1

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Ls4/f;->p0()Ljava/lang/Object;

    iget v0, p0, Ls4/f;->q:I

    if-lez v0, :cond_1

    iget-object v1, p0, Ls4/f;->r:[Ljava/lang/String;

    add-int/lit8 v0, v0, -0x1

    aput-object v2, v1, v0

    :cond_1
    :goto_0
    iget v0, p0, Ls4/f;->q:I

    if-lez v0, :cond_2

    iget-object v1, p0, Ls4/f;->s:[I

    add-int/lit8 v0, v0, -0x1

    aget v2, v1, v0

    add-int/lit8 v2, v2, 0x1

    aput v2, v1, v0

    :cond_2
    return-void
.end method

.method n0()Lp4/j;
    .locals 4

    goto/32 :goto_e

    nop

    :goto_0
    if-ne v0, v1, :cond_0

    goto/32 :goto_10

    :cond_0
    goto/32 :goto_16

    nop

    :goto_1
    if-ne v0, v1, :cond_1

    goto/32 :goto_10

    :cond_1
    goto/32 :goto_18

    nop

    :goto_2
    const-string v0, " when reading a JsonElement."

    goto/32 :goto_5

    nop

    :goto_3
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_c

    nop

    :goto_4
    if-ne v0, v1, :cond_2

    goto/32 :goto_10

    :cond_2
    goto/32 :goto_9

    nop

    :goto_5
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_3

    nop

    :goto_6
    invoke-direct {p0}, Ls4/f;->o0()Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_a

    nop

    :goto_7
    invoke-virtual {p0}, Ls4/f;->k0()V

    goto/32 :goto_f

    nop

    :goto_8
    const-string v3, "Unexpected "

    goto/32 :goto_b

    nop

    :goto_9
    sget-object v1, Lx4/b;->b:Lx4/b;

    goto/32 :goto_1

    nop

    :goto_a
    check-cast v0, Lp4/j;

    goto/32 :goto_7

    nop

    :goto_b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_13

    nop

    :goto_c
    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_15

    nop

    :goto_d
    new-instance v1, Ljava/lang/IllegalStateException;

    goto/32 :goto_11

    nop

    :goto_e
    invoke-virtual {p0}, Ls4/f;->a0()Lx4/b;

    move-result-object v0

    goto/32 :goto_14

    nop

    :goto_f
    return-object v0

    :goto_10
    goto/32 :goto_d

    nop

    :goto_11
    new-instance v2, Ljava/lang/StringBuilder;

    goto/32 :goto_17

    nop

    :goto_12
    if-ne v0, v1, :cond_3

    goto/32 :goto_10

    :cond_3
    goto/32 :goto_6

    nop

    :goto_13
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto/32 :goto_2

    nop

    :goto_14
    sget-object v1, Lx4/b;->e:Lx4/b;

    goto/32 :goto_4

    nop

    :goto_15
    throw v1

    :goto_16
    sget-object v1, Lx4/b;->j:Lx4/b;

    goto/32 :goto_12

    nop

    :goto_17
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_8

    nop

    :goto_18
    sget-object v1, Lx4/b;->d:Lx4/b;

    goto/32 :goto_0

    nop
.end method

.method public q0()V
    .locals 2

    sget-object v0, Lx4/b;->e:Lx4/b;

    invoke-direct {p0, v0}, Ls4/f;->m0(Lx4/b;)V

    invoke-direct {p0}, Ls4/f;->o0()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-direct {p0, v1}, Ls4/f;->r0(Ljava/lang/Object;)V

    new-instance v1, Lp4/o;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v1, v0}, Lp4/o;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Ls4/f;->r0(Ljava/lang/Object;)V

    return-void
.end method

.method public r()V
    .locals 3

    sget-object v0, Lx4/b;->b:Lx4/b;

    invoke-direct {p0, v0}, Ls4/f;->m0(Lx4/b;)V

    invoke-direct {p0}, Ls4/f;->p0()Ljava/lang/Object;

    invoke-direct {p0}, Ls4/f;->p0()Ljava/lang/Object;

    iget v0, p0, Ls4/f;->q:I

    if-lez v0, :cond_0

    iget-object v1, p0, Ls4/f;->s:[I

    add-int/lit8 v0, v0, -0x1

    aget v2, v1, v0

    add-int/lit8 v2, v2, 0x1

    aput v2, v1, v0

    :cond_0
    return-void
.end method

.method public s()V
    .locals 3

    sget-object v0, Lx4/b;->d:Lx4/b;

    invoke-direct {p0, v0}, Ls4/f;->m0(Lx4/b;)V

    invoke-direct {p0}, Ls4/f;->p0()Ljava/lang/Object;

    invoke-direct {p0}, Ls4/f;->p0()Ljava/lang/Object;

    iget v0, p0, Ls4/f;->q:I

    if-lez v0, :cond_0

    iget-object v1, p0, Ls4/f;->s:[I

    add-int/lit8 v0, v0, -0x1

    aget v2, v1, v0

    add-int/lit8 v2, v2, 0x1

    aput v2, v1, v0

    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Ls4/f;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ls4/f;->F()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public z()Ljava/lang/String;
    .locals 5

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    :goto_0
    iget v2, p0, Ls4/f;->q:I

    if-ge v1, v2, :cond_2

    iget-object v3, p0, Ls4/f;->p:[Ljava/lang/Object;

    aget-object v4, v3, v1

    instance-of v4, v4, Lp4/g;

    if-eqz v4, :cond_0

    add-int/lit8 v1, v1, 0x1

    if-ge v1, v2, :cond_1

    aget-object v2, v3, v1

    instance-of v2, v2, Ljava/util/Iterator;

    if-eqz v2, :cond_1

    const/16 v2, 0x5b

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v2, p0, Ls4/f;->s:[I

    aget v2, v2, v1

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v2, 0x5d

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_0
    aget-object v4, v3, v1

    instance-of v4, v4, Lp4/m;

    if-eqz v4, :cond_1

    add-int/lit8 v1, v1, 0x1

    if-ge v1, v2, :cond_1

    aget-object v2, v3, v1

    instance-of v2, v2, Ljava/util/Iterator;

    if-eqz v2, :cond_1

    const/16 v2, 0x2e

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v2, p0, Ls4/f;->r:[Ljava/lang/String;

    aget-object v3, v2, v1

    if-eqz v3, :cond_1

    aget-object v2, v2, v1

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
