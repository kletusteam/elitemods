.class Ls4/n$y;
.super Ljava/lang/Object;

# interfaces
.implements Lp4/w;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Ls4/n;->d(Ljava/lang/Class;Lp4/v;)Lp4/w;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/Class;

.field final synthetic b:Lp4/v;


# direct methods
.method constructor <init>(Ljava/lang/Class;Lp4/v;)V
    .locals 0

    iput-object p1, p0, Ls4/n$y;->a:Ljava/lang/Class;

    iput-object p2, p0, Ls4/n$y;->b:Lp4/v;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lp4/e;Lw4/a;)Lp4/v;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T2:",
            "Ljava/lang/Object;",
            ">(",
            "Lp4/e;",
            "Lw4/a<",
            "TT2;>;)",
            "Lp4/v<",
            "TT2;>;"
        }
    .end annotation

    invoke-virtual {p2}, Lw4/a;->c()Ljava/lang/Class;

    move-result-object p1

    iget-object p2, p0, Ls4/n$y;->a:Ljava/lang/Class;

    invoke-virtual {p2, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result p2

    if-nez p2, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    new-instance p2, Ls4/n$y$a;

    invoke-direct {p2, p0, p1}, Ls4/n$y$a;-><init>(Ls4/n$y;Ljava/lang/Class;)V

    return-object p2
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Factory[typeHierarchy="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Ls4/n$y;->a:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ",adapter="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Ls4/n$y;->b:Lp4/v;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
