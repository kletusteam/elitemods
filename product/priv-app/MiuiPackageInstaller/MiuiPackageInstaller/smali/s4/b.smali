.class public final Ls4/b;
.super Ljava/lang/Object;

# interfaces
.implements Lp4/w;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ls4/b$a;
    }
.end annotation


# instance fields
.field private final a:Lr4/c;


# direct methods
.method public constructor <init>(Lr4/c;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Ls4/b;->a:Lr4/c;

    return-void
.end method


# virtual methods
.method public a(Lp4/e;Lw4/a;)Lp4/v;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lp4/e;",
            "Lw4/a<",
            "TT;>;)",
            "Lp4/v<",
            "TT;>;"
        }
    .end annotation

    invoke-virtual {p2}, Lw4/a;->e()Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-virtual {p2}, Lw4/a;->c()Ljava/lang/Class;

    move-result-object v1

    const-class v2, Ljava/util/Collection;

    invoke-virtual {v2, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    invoke-static {v0, v1}, Lr4/b;->h(Ljava/lang/reflect/Type;Ljava/lang/Class;)Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-static {v0}, Lw4/a;->b(Ljava/lang/reflect/Type;)Lw4/a;

    move-result-object v1

    invoke-virtual {p1, v1}, Lp4/e;->l(Lw4/a;)Lp4/v;

    move-result-object v1

    iget-object v2, p0, Ls4/b;->a:Lr4/c;

    invoke-virtual {v2, p2}, Lr4/c;->a(Lw4/a;)Lr4/i;

    move-result-object p2

    new-instance v2, Ls4/b$a;

    invoke-direct {v2, p1, v0, v1, p2}, Ls4/b$a;-><init>(Lp4/e;Ljava/lang/reflect/Type;Lp4/v;Lr4/i;)V

    return-object v2
.end method
