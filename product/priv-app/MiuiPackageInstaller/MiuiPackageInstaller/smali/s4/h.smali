.class public final Ls4/h;
.super Ljava/lang/Object;

# interfaces
.implements Lp4/w;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ls4/h$a;
    }
.end annotation


# instance fields
.field private final a:Lr4/c;

.field final b:Z


# direct methods
.method public constructor <init>(Lr4/c;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Ls4/h;->a:Lr4/c;

    iput-boolean p2, p0, Ls4/h;->b:Z

    return-void
.end method

.method private b(Lp4/e;Ljava/lang/reflect/Type;)Lp4/v;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lp4/e;",
            "Ljava/lang/reflect/Type;",
            ")",
            "Lp4/v<",
            "*>;"
        }
    .end annotation

    sget-object v0, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    if-eq p2, v0, :cond_1

    const-class v0, Ljava/lang/Boolean;

    if-ne p2, v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {p2}, Lw4/a;->b(Ljava/lang/reflect/Type;)Lw4/a;

    move-result-object p2

    invoke-virtual {p1, p2}, Lp4/e;->l(Lw4/a;)Lp4/v;

    move-result-object p1

    goto :goto_1

    :cond_1
    :goto_0
    sget-object p1, Ls4/n;->f:Lp4/v;

    :goto_1
    return-object p1
.end method


# virtual methods
.method public a(Lp4/e;Lw4/a;)Lp4/v;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lp4/e;",
            "Lw4/a<",
            "TT;>;)",
            "Lp4/v<",
            "TT;>;"
        }
    .end annotation

    invoke-virtual {p2}, Lw4/a;->e()Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-virtual {p2}, Lw4/a;->c()Ljava/lang/Class;

    move-result-object v1

    const-class v2, Ljava/util/Map;

    invoke-virtual {v2, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    invoke-static {v0}, Lr4/b;->k(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v1

    invoke-static {v0, v1}, Lr4/b;->j(Ljava/lang/reflect/Type;Ljava/lang/Class;)[Ljava/lang/reflect/Type;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v2, v0, v1

    invoke-direct {p0, p1, v2}, Ls4/h;->b(Lp4/e;Ljava/lang/reflect/Type;)Lp4/v;

    move-result-object v7

    const/4 v2, 0x1

    aget-object v3, v0, v2

    invoke-static {v3}, Lw4/a;->b(Ljava/lang/reflect/Type;)Lw4/a;

    move-result-object v3

    invoke-virtual {p1, v3}, Lp4/e;->l(Lw4/a;)Lp4/v;

    move-result-object v9

    iget-object v3, p0, Ls4/h;->a:Lr4/c;

    invoke-virtual {v3, p2}, Lr4/c;->a(Lw4/a;)Lr4/i;

    move-result-object v10

    new-instance p2, Ls4/h$a;

    aget-object v6, v0, v1

    aget-object v8, v0, v2

    move-object v3, p2

    move-object v4, p0

    move-object v5, p1

    invoke-direct/range {v3 .. v10}, Ls4/h$a;-><init>(Ls4/h;Lp4/e;Ljava/lang/reflect/Type;Lp4/v;Ljava/lang/reflect/Type;Lp4/v;Lr4/i;)V

    return-object p2
.end method
