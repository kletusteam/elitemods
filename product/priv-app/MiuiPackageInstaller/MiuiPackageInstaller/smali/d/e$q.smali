.class public final Ld/e$q;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ld/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1c
    name = "q"
.end annotation


# instance fields
.field a:I

.field b:I

.field c:I

.field d:I

.field e:I

.field f:I

.field g:Landroid/view/ViewGroup;

.field h:Landroid/view/View;

.field i:Landroid/view/View;

.field j:Landroidx/appcompat/view/menu/e;

.field k:Landroidx/appcompat/view/menu/c;

.field l:Landroid/content/Context;

.field m:Z

.field n:Z

.field o:Z

.field public p:Z

.field q:Z

.field r:Z

.field s:Landroid/os/Bundle;


# direct methods
.method constructor <init>(I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Ld/e$q;->a:I

    const/4 p1, 0x0

    iput-boolean p1, p0, Ld/e$q;->q:Z

    return-void
.end method


# virtual methods
.method a(Landroidx/appcompat/view/menu/j$a;)Landroidx/appcompat/view/menu/k;
    .locals 3

    goto/32 :goto_c

    nop

    :goto_0
    iget-object p1, p0, Ld/e$q;->j:Landroidx/appcompat/view/menu/e;

    goto/32 :goto_e

    nop

    :goto_1
    iput-object v0, p0, Ld/e$q;->k:Landroidx/appcompat/view/menu/c;

    goto/32 :goto_7

    nop

    :goto_2
    return-object p1

    :goto_3
    goto/32 :goto_f

    nop

    :goto_4
    iget-object v1, p0, Ld/e$q;->l:Landroid/content/Context;

    goto/32 :goto_6

    nop

    :goto_5
    if-eqz v0, :cond_0

    goto/32 :goto_3

    :cond_0
    goto/32 :goto_d

    nop

    :goto_6
    sget v2, Lc/g;->j:I

    goto/32 :goto_a

    nop

    :goto_7
    invoke-virtual {v0, p1}, Landroidx/appcompat/view/menu/c;->i(Landroidx/appcompat/view/menu/j$a;)V

    goto/32 :goto_0

    nop

    :goto_8
    new-instance v0, Landroidx/appcompat/view/menu/c;

    goto/32 :goto_4

    nop

    :goto_9
    return-object p1

    :goto_a
    invoke-direct {v0, v1, v2}, Landroidx/appcompat/view/menu/c;-><init>(Landroid/content/Context;I)V

    goto/32 :goto_1

    nop

    :goto_b
    invoke-virtual {p1, v0}, Landroidx/appcompat/view/menu/c;->j(Landroid/view/ViewGroup;)Landroidx/appcompat/view/menu/k;

    move-result-object p1

    goto/32 :goto_9

    nop

    :goto_c
    iget-object v0, p0, Ld/e$q;->j:Landroidx/appcompat/view/menu/e;

    goto/32 :goto_5

    nop

    :goto_d
    const/4 p1, 0x0

    goto/32 :goto_2

    nop

    :goto_e
    iget-object v0, p0, Ld/e$q;->k:Landroidx/appcompat/view/menu/c;

    goto/32 :goto_10

    nop

    :goto_f
    iget-object v0, p0, Ld/e$q;->k:Landroidx/appcompat/view/menu/c;

    goto/32 :goto_12

    nop

    :goto_10
    invoke-virtual {p1, v0}, Landroidx/appcompat/view/menu/e;->b(Landroidx/appcompat/view/menu/j;)V

    :goto_11
    goto/32 :goto_13

    nop

    :goto_12
    if-eqz v0, :cond_1

    goto/32 :goto_11

    :cond_1
    goto/32 :goto_8

    nop

    :goto_13
    iget-object p1, p0, Ld/e$q;->k:Landroidx/appcompat/view/menu/c;

    goto/32 :goto_14

    nop

    :goto_14
    iget-object v0, p0, Ld/e$q;->g:Landroid/view/ViewGroup;

    goto/32 :goto_b

    nop
.end method

.method public b()Z
    .locals 3

    iget-object v0, p0, Ld/e$q;->h:Landroid/view/View;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    iget-object v0, p0, Ld/e$q;->i:Landroid/view/View;

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    return v2

    :cond_1
    iget-object v0, p0, Ld/e$q;->k:Landroidx/appcompat/view/menu/c;

    invoke-virtual {v0}, Landroidx/appcompat/view/menu/c;->a()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    move v1, v2

    :cond_2
    return v1
.end method

.method c(Landroidx/appcompat/view/menu/e;)V
    .locals 2

    goto/32 :goto_c

    nop

    :goto_0
    if-eq p1, v0, :cond_0

    goto/32 :goto_a

    :cond_0
    goto/32 :goto_9

    nop

    :goto_1
    invoke-virtual {v0, v1}, Landroidx/appcompat/view/menu/e;->O(Landroidx/appcompat/view/menu/j;)V

    :goto_2
    goto/32 :goto_b

    nop

    :goto_3
    iget-object v1, p0, Ld/e$q;->k:Landroidx/appcompat/view/menu/c;

    goto/32 :goto_1

    nop

    :goto_4
    return-void

    :goto_5
    if-nez v0, :cond_1

    goto/32 :goto_7

    :cond_1
    goto/32 :goto_6

    nop

    :goto_6
    invoke-virtual {p1, v0}, Landroidx/appcompat/view/menu/e;->b(Landroidx/appcompat/view/menu/j;)V

    :goto_7
    goto/32 :goto_4

    nop

    :goto_8
    iget-object v0, p0, Ld/e$q;->k:Landroidx/appcompat/view/menu/c;

    goto/32 :goto_5

    nop

    :goto_9
    return-void

    :goto_a
    goto/32 :goto_d

    nop

    :goto_b
    iput-object p1, p0, Ld/e$q;->j:Landroidx/appcompat/view/menu/e;

    goto/32 :goto_e

    nop

    :goto_c
    iget-object v0, p0, Ld/e$q;->j:Landroidx/appcompat/view/menu/e;

    goto/32 :goto_0

    nop

    :goto_d
    if-nez v0, :cond_2

    goto/32 :goto_2

    :cond_2
    goto/32 :goto_3

    nop

    :goto_e
    if-nez p1, :cond_3

    goto/32 :goto_7

    :cond_3
    goto/32 :goto_8

    nop
.end method

.method d(Landroid/content/Context;)V
    .locals 4

    goto/32 :goto_17

    nop

    :goto_0
    sget v2, Lc/a;->E:I

    goto/32 :goto_12

    nop

    :goto_1
    iget v2, v0, Landroid/util/TypedValue;->resourceId:I

    goto/32 :goto_b

    nop

    :goto_2
    iput-object v0, p0, Ld/e$q;->l:Landroid/content/Context;

    goto/32 :goto_21

    nop

    :goto_3
    const/4 v3, 0x1

    goto/32 :goto_e

    nop

    :goto_4
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    goto/32 :goto_1f

    nop

    :goto_5
    return-void

    :goto_6
    sget v0, Lc/j;->C0:I

    goto/32 :goto_22

    nop

    :goto_7
    sget v2, Lc/a;->a:I

    goto/32 :goto_3

    nop

    :goto_8
    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    goto/32 :goto_4

    nop

    :goto_9
    goto :goto_10

    :goto_a
    goto/32 :goto_f

    nop

    :goto_b
    if-nez v2, :cond_0

    goto/32 :goto_14

    :cond_0
    goto/32 :goto_13

    nop

    :goto_c
    invoke-virtual {v0, p1}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object p1

    goto/32 :goto_6

    nop

    :goto_d
    iput v0, p0, Ld/e$q;->b:I

    goto/32 :goto_1b

    nop

    :goto_e
    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    goto/32 :goto_1

    nop

    :goto_f
    sget v0, Lc/i;->a:I

    :goto_10
    goto/32 :goto_18

    nop

    :goto_11
    const/4 v2, 0x0

    goto/32 :goto_25

    nop

    :goto_12
    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    goto/32 :goto_23

    nop

    :goto_13
    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    :goto_14
    goto/32 :goto_0

    nop

    :goto_15
    if-nez v0, :cond_1

    goto/32 :goto_a

    :cond_1
    goto/32 :goto_9

    nop

    :goto_16
    iput v0, p0, Ld/e$q;->f:I

    goto/32 :goto_1e

    nop

    :goto_17
    new-instance v0, Landroid/util/TypedValue;

    goto/32 :goto_8

    nop

    :goto_18
    invoke-virtual {v1, v0, v3}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    goto/32 :goto_19

    nop

    :goto_19
    new-instance v0, Li/d;

    goto/32 :goto_11

    nop

    :goto_1a
    invoke-virtual {p1, v1}, Landroid/content/res/Resources$Theme;->setTo(Landroid/content/res/Resources$Theme;)V

    goto/32 :goto_2

    nop

    :goto_1b
    sget v0, Lc/j;->B0:I

    goto/32 :goto_24

    nop

    :goto_1c
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    goto/32 :goto_1d

    nop

    :goto_1d
    invoke-virtual {v1, v2}, Landroid/content/res/Resources$Theme;->setTo(Landroid/content/res/Resources$Theme;)V

    goto/32 :goto_7

    nop

    :goto_1e
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    goto/32 :goto_5

    nop

    :goto_1f
    invoke-virtual {v1}, Landroid/content/res/Resources;->newTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    goto/32 :goto_1c

    nop

    :goto_20
    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object p1

    goto/32 :goto_1a

    nop

    :goto_21
    sget-object p1, Lc/j;->z0:[I

    goto/32 :goto_c

    nop

    :goto_22
    invoke-virtual {p1, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    goto/32 :goto_d

    nop

    :goto_23
    iget v0, v0, Landroid/util/TypedValue;->resourceId:I

    goto/32 :goto_15

    nop

    :goto_24
    invoke-virtual {p1, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    goto/32 :goto_16

    nop

    :goto_25
    invoke-direct {v0, p1, v2}, Li/d;-><init>(Landroid/content/Context;I)V

    goto/32 :goto_20

    nop
.end method
