.class public Ld/g;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ld/g$a;
    }
.end annotation


# static fields
.field private static final b:[Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation
.end field

.field private static final c:[I

.field private static final d:[Ljava/lang/String;

.field private static final e:Ln/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ln/g<",
            "Ljava/lang/String;",
            "Ljava/lang/reflect/Constructor<",
            "+",
            "Landroid/view/View;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field private final a:[Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Class;

    const-class v1, Landroid/content/Context;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-class v1, Landroid/util/AttributeSet;

    const/4 v3, 0x1

    aput-object v1, v0, v3

    sput-object v0, Ld/g;->b:[Ljava/lang/Class;

    new-array v0, v3, [I

    const v1, 0x101026f

    aput v1, v0, v2

    sput-object v0, Ld/g;->c:[I

    const-string v0, "android.widget."

    const-string v1, "android.view."

    const-string v2, "android.webkit."

    filled-new-array {v0, v1, v2}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ld/g;->d:[Ljava/lang/String;

    new-instance v0, Ln/g;

    invoke-direct {v0}, Ln/g;-><init>()V

    sput-object v0, Ld/g;->e:Ln/g;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    iput-object v0, p0, Ld/g;->a:[Ljava/lang/Object;

    return-void
.end method

.method private a(Landroid/view/View;Landroid/util/AttributeSet;)V
    .locals 2

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    instance-of v1, v0, Landroid/content/ContextWrapper;

    if-eqz v1, :cond_2

    invoke-static {p1}, Landroidx/core/view/t;->J(Landroid/view/View;)Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0

    :cond_0
    sget-object v1, Ld/g;->c:[I

    invoke-virtual {v0, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p2

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    new-instance v1, Ld/g$a;

    invoke-direct {v1, p1, v0}, Ld/g$a;-><init>(Landroid/view/View;Ljava/lang/String;)V

    invoke-virtual {p1, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    :cond_2
    :goto_0
    return-void
.end method

.method private r(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/view/View;
    .locals 2

    sget-object v0, Ld/g;->e:Ln/g;

    invoke-virtual {v0, p2}, Ln/g;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/reflect/Constructor;

    if-nez v1, :cond_1

    if-eqz p3, :cond_0

    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    goto :goto_0

    :cond_0
    move-object p3, p2

    :goto_0
    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p1

    invoke-static {p3, v1, p1}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object p1

    const-class p3, Landroid/view/View;

    invoke-virtual {p1, p3}, Ljava/lang/Class;->asSubclass(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object p1

    sget-object p3, Ld/g;->b:[Ljava/lang/Class;

    invoke-virtual {p1, p3}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, Ln/g;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    const/4 p1, 0x1

    invoke-virtual {v1, p1}, Ljava/lang/reflect/Constructor;->setAccessible(Z)V

    iget-object p1, p0, Ld/g;->a:[Ljava/lang/Object;

    invoke-virtual {v1, p1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/View;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    const/4 p1, 0x0

    return-object p1
.end method

.method private s(Landroid/content/Context;Ljava/lang/String;Landroid/util/AttributeSet;)Landroid/view/View;
    .locals 5

    const-string v0, "view"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    const-string p2, "class"

    invoke-interface {p3, v1, p2}, Landroid/util/AttributeSet;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    :cond_0
    const/4 v0, 0x1

    const/4 v2, 0x0

    :try_start_0
    iget-object v3, p0, Ld/g;->a:[Ljava/lang/Object;

    aput-object p1, v3, v2

    aput-object p3, v3, v0

    const/4 p3, -0x1

    const/16 v3, 0x2e

    invoke-virtual {p2, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    if-ne p3, v3, :cond_3

    move p3, v2

    :goto_0
    sget-object v3, Ld/g;->d:[Ljava/lang/String;

    array-length v4, v3

    if-ge p3, v4, :cond_2

    aget-object v3, v3, p3

    invoke-direct {p0, p1, p2, v3}, Ld/g;->r(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/view/View;

    move-result-object v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v3, :cond_1

    iget-object p1, p0, Ld/g;->a:[Ljava/lang/Object;

    aput-object v1, p1, v2

    aput-object v1, p1, v0

    return-object v3

    :cond_1
    add-int/lit8 p3, p3, 0x1

    goto :goto_0

    :cond_2
    iget-object p1, p0, Ld/g;->a:[Ljava/lang/Object;

    aput-object v1, p1, v2

    aput-object v1, p1, v0

    return-object v1

    :cond_3
    :try_start_1
    invoke-direct {p0, p1, p2, v1}, Ld/g;->r(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/view/View;

    move-result-object p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object p2, p0, Ld/g;->a:[Ljava/lang/Object;

    aput-object v1, p2, v2

    aput-object v1, p2, v0

    return-object p1

    :catchall_0
    move-exception p1

    iget-object p2, p0, Ld/g;->a:[Ljava/lang/Object;

    aput-object v1, p2, v2

    aput-object v1, p2, v0

    throw p1

    :catch_0
    iget-object p1, p0, Ld/g;->a:[Ljava/lang/Object;

    aput-object v1, p1, v2

    aput-object v1, p1, v0

    return-object v1
.end method

.method private static t(Landroid/content/Context;Landroid/util/AttributeSet;ZZ)Landroid/content/Context;
    .locals 2

    sget-object v0, Lc/j;->M3:[I

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p1

    if-eqz p2, :cond_0

    sget p2, Lc/j;->N3:I

    invoke-virtual {p1, p2, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result p2

    goto :goto_0

    :cond_0
    move p2, v1

    :goto_0
    if-eqz p3, :cond_1

    if-nez p2, :cond_1

    sget p2, Lc/j;->O3:I

    invoke-virtual {p1, p2, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result p2

    if-eqz p2, :cond_1

    const-string p3, "AppCompatViewInflater"

    const-string v0, "app:theme is now deprecated. Please move to using android:theme instead."

    invoke-static {p3, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    if-eqz p2, :cond_3

    instance-of p1, p0, Li/d;

    if-eqz p1, :cond_2

    move-object p1, p0

    check-cast p1, Li/d;

    invoke-virtual {p1}, Li/d;->c()I

    move-result p1

    if-eq p1, p2, :cond_3

    :cond_2
    new-instance p1, Li/d;

    invoke-direct {p1, p0, p2}, Li/d;-><init>(Landroid/content/Context;I)V

    move-object p0, p1

    :cond_3
    return-object p0
.end method

.method private u(Landroid/view/View;Ljava/lang/String;)V
    .locals 2

    if-eqz p1, :cond_0

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " asked to inflate view for <"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, ">, but returned null"

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method protected b(Landroid/content/Context;Landroid/util/AttributeSet;)Landroidx/appcompat/widget/d;
    .locals 1

    new-instance v0, Landroidx/appcompat/widget/d;

    invoke-direct {v0, p1, p2}, Landroidx/appcompat/widget/d;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected c(Landroid/content/Context;Landroid/util/AttributeSet;)Landroidx/appcompat/widget/AppCompatButton;
    .locals 1

    new-instance v0, Landroidx/appcompat/widget/AppCompatButton;

    invoke-direct {v0, p1, p2}, Landroidx/appcompat/widget/AppCompatButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected d(Landroid/content/Context;Landroid/util/AttributeSet;)Landroidx/appcompat/widget/AppCompatCheckBox;
    .locals 1

    new-instance v0, Landroidx/appcompat/widget/AppCompatCheckBox;

    invoke-direct {v0, p1, p2}, Landroidx/appcompat/widget/AppCompatCheckBox;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected e(Landroid/content/Context;Landroid/util/AttributeSet;)Landroidx/appcompat/widget/AppCompatCheckedTextView;
    .locals 1

    new-instance v0, Landroidx/appcompat/widget/AppCompatCheckedTextView;

    invoke-direct {v0, p1, p2}, Landroidx/appcompat/widget/AppCompatCheckedTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected f(Landroid/content/Context;Landroid/util/AttributeSet;)Landroidx/appcompat/widget/AppCompatEditText;
    .locals 1

    new-instance v0, Landroidx/appcompat/widget/AppCompatEditText;

    invoke-direct {v0, p1, p2}, Landroidx/appcompat/widget/AppCompatEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected g(Landroid/content/Context;Landroid/util/AttributeSet;)Landroidx/appcompat/widget/i;
    .locals 1

    new-instance v0, Landroidx/appcompat/widget/i;

    invoke-direct {v0, p1, p2}, Landroidx/appcompat/widget/i;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected h(Landroid/content/Context;Landroid/util/AttributeSet;)Landroidx/appcompat/widget/AppCompatImageView;
    .locals 1

    new-instance v0, Landroidx/appcompat/widget/AppCompatImageView;

    invoke-direct {v0, p1, p2}, Landroidx/appcompat/widget/AppCompatImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected i(Landroid/content/Context;Landroid/util/AttributeSet;)Landroidx/appcompat/widget/k;
    .locals 1

    new-instance v0, Landroidx/appcompat/widget/k;

    invoke-direct {v0, p1, p2}, Landroidx/appcompat/widget/k;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected j(Landroid/content/Context;Landroid/util/AttributeSet;)Landroidx/appcompat/widget/AppCompatRadioButton;
    .locals 1

    new-instance v0, Landroidx/appcompat/widget/AppCompatRadioButton;

    invoke-direct {v0, p1, p2}, Landroidx/appcompat/widget/AppCompatRadioButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected k(Landroid/content/Context;Landroid/util/AttributeSet;)Landroidx/appcompat/widget/n;
    .locals 1

    new-instance v0, Landroidx/appcompat/widget/n;

    invoke-direct {v0, p1, p2}, Landroidx/appcompat/widget/n;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected l(Landroid/content/Context;Landroid/util/AttributeSet;)Landroidx/appcompat/widget/o;
    .locals 1

    new-instance v0, Landroidx/appcompat/widget/o;

    invoke-direct {v0, p1, p2}, Landroidx/appcompat/widget/o;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected m(Landroid/content/Context;Landroid/util/AttributeSet;)Landroidx/appcompat/widget/q;
    .locals 1

    new-instance v0, Landroidx/appcompat/widget/q;

    invoke-direct {v0, p1, p2}, Landroidx/appcompat/widget/q;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected n(Landroid/content/Context;Landroid/util/AttributeSet;)Landroidx/appcompat/widget/AppCompatTextView;
    .locals 1

    new-instance v0, Landroidx/appcompat/widget/AppCompatTextView;

    invoke-direct {v0, p1, p2}, Landroidx/appcompat/widget/AppCompatTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected o(Landroid/content/Context;Landroid/util/AttributeSet;)Landroidx/appcompat/widget/u;
    .locals 1

    new-instance v0, Landroidx/appcompat/widget/u;

    invoke-direct {v0, p1, p2}, Landroidx/appcompat/widget/u;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected p(Landroid/content/Context;Ljava/lang/String;Landroid/util/AttributeSet;)Landroid/view/View;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method final q(Landroid/view/View;Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;ZZZZ)Landroid/view/View;
    .locals 0

    goto/32 :goto_44

    nop

    :goto_0
    invoke-virtual {p0, p1, p4}, Ld/g;->f(Landroid/content/Context;Landroid/util/AttributeSet;)Landroidx/appcompat/widget/AppCompatEditText;

    move-result-object p5

    goto/32 :goto_4

    nop

    :goto_1
    goto/16 :goto_4a

    :goto_2
    goto/32 :goto_1f

    nop

    :goto_3
    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result p6

    sparse-switch p6, :sswitch_data_0

    goto/32 :goto_50

    nop

    :goto_4
    goto :goto_19

    :pswitch_0
    goto/32 :goto_40

    nop

    :goto_5
    goto/16 :goto_4a

    :goto_6
    goto/32 :goto_4c

    nop

    :goto_7
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    goto/32 :goto_2c

    nop

    :goto_8
    invoke-virtual {p0, p1, p4}, Ld/g;->l(Landroid/content/Context;Landroid/util/AttributeSet;)Landroidx/appcompat/widget/o;

    move-result-object p5

    goto/32 :goto_77

    nop

    :goto_9
    goto/16 :goto_4a

    :goto_a
    goto/32 :goto_8a

    nop

    :goto_b
    invoke-direct {p0, p1, p2, p4}, Ld/g;->s(Landroid/content/Context;Ljava/lang/String;Landroid/util/AttributeSet;)Landroid/view/View;

    move-result-object p5

    :goto_c
    goto/32 :goto_3f

    nop

    :goto_d
    invoke-static {p1, p4, p6, p7}, Ld/g;->t(Landroid/content/Context;Landroid/util/AttributeSet;ZZ)Landroid/content/Context;

    move-result-object p1

    :goto_e
    goto/32 :goto_4e

    nop

    :goto_f
    invoke-direct {p0, p5, p2}, Ld/g;->u(Landroid/view/View;Ljava/lang/String;)V

    goto/32 :goto_32

    nop

    :goto_10
    goto/16 :goto_4a

    :sswitch_0
    goto/32 :goto_92

    nop

    :goto_11
    invoke-virtual {p2, p6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p6

    goto/32 :goto_84

    nop

    :goto_12
    const-string p6, "Spinner"

    goto/32 :goto_37

    nop

    :goto_13
    move-object p1, p3

    :goto_14
    goto/32 :goto_67

    nop

    :goto_15
    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    goto/32 :goto_42

    nop

    :goto_16
    goto/16 :goto_4a

    :sswitch_1
    goto/32 :goto_12

    nop

    :goto_17
    const-string p6, "ImageView"

    goto/32 :goto_25

    nop

    :goto_18
    invoke-virtual {p0, p1, p4}, Ld/g;->n(Landroid/content/Context;Landroid/util/AttributeSet;)Landroidx/appcompat/widget/AppCompatTextView;

    move-result-object p5

    :goto_19
    goto/32 :goto_f

    nop

    :goto_1a
    const-string p6, "RadioButton"

    goto/32 :goto_31

    nop

    :goto_1b
    invoke-virtual {p0, p1, p2, p4}, Ld/g;->p(Landroid/content/Context;Ljava/lang/String;Landroid/util/AttributeSet;)Landroid/view/View;

    move-result-object p5

    goto/32 :goto_63

    nop

    :goto_1c
    goto/16 :goto_4a

    :goto_1d
    goto/32 :goto_27

    nop

    :goto_1e
    const-string p6, "SeekBar"

    goto/32 :goto_5c

    nop

    :goto_1f
    const/4 p5, 0x4

    goto/32 :goto_7b

    nop

    :goto_20
    if-eqz p6, :cond_0

    goto/32 :goto_96

    :cond_0
    goto/32 :goto_95

    nop

    :goto_21
    invoke-virtual {p2, p6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p6

    goto/32 :goto_47

    nop

    :goto_22
    invoke-virtual {p0, p1, p4}, Ld/g;->g(Landroid/content/Context;Landroid/util/AttributeSet;)Landroidx/appcompat/widget/i;

    move-result-object p5

    goto/32 :goto_5d

    nop

    :goto_23
    goto :goto_19

    :pswitch_1
    goto/32 :goto_33

    nop

    :goto_24
    goto :goto_19

    :pswitch_2
    goto/32 :goto_0

    nop

    :goto_25
    invoke-virtual {p2, p6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p6

    goto/32 :goto_28

    nop

    :goto_26
    if-ne p3, p1, :cond_1

    goto/32 :goto_c

    :cond_1
    goto/32 :goto_b

    nop

    :goto_27
    const/16 p5, 0x8

    goto/32 :goto_35

    nop

    :goto_28
    if-eqz p6, :cond_2

    goto/32 :goto_86

    :cond_2
    goto/32 :goto_85

    nop

    :goto_29
    invoke-virtual {p0, p1, p4}, Ld/g;->o(Landroid/content/Context;Landroid/util/AttributeSet;)Landroidx/appcompat/widget/u;

    move-result-object p5

    goto/32 :goto_2f

    nop

    :goto_2a
    if-eqz p6, :cond_3

    goto/32 :goto_99

    :cond_3
    goto/32 :goto_98

    nop

    :goto_2b
    const-string p6, "MultiAutoCompleteTextView"

    goto/32 :goto_21

    nop

    :goto_2c
    goto/16 :goto_14

    :goto_2d
    goto/32 :goto_13

    nop

    :goto_2e
    if-eqz p6, :cond_4

    goto/32 :goto_6c

    :cond_4
    goto/32 :goto_6b

    nop

    :goto_2f
    goto/16 :goto_19

    :pswitch_3
    goto/32 :goto_38

    nop

    :goto_30
    goto/16 :goto_19

    :pswitch_4
    goto/32 :goto_29

    nop

    :goto_31
    invoke-virtual {p2, p6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p6

    goto/32 :goto_90

    nop

    :goto_32
    goto/16 :goto_65

    :pswitch_5
    goto/32 :goto_4f

    nop

    :goto_33
    invoke-virtual {p0, p1, p4}, Ld/g;->h(Landroid/content/Context;Landroid/util/AttributeSet;)Landroidx/appcompat/widget/AppCompatImageView;

    move-result-object p5

    goto/32 :goto_30

    nop

    :goto_34
    return-object p5

    :sswitch_data_0
    .sparse-switch
        -0x7404ceea -> :sswitch_3
        -0x56c015e7 -> :sswitch_d
        -0x503aa7ad -> :sswitch_5
        -0x37f7066e -> :sswitch_b
        -0x37e04bb3 -> :sswitch_0
        -0x274065a5 -> :sswitch_7
        -0x1440b607 -> :sswitch_1
        0x2e46a6ed -> :sswitch_2
        0x2fa453c6 -> :sswitch_4
        0x431b5280 -> :sswitch_8
        0x5445f9ba -> :sswitch_c
        0x5f7507c3 -> :sswitch_a
        0x63577677 -> :sswitch_9
        0x77471352 -> :sswitch_6
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_9
        :pswitch_d
        :pswitch_5
        :pswitch_7
        :pswitch_a
        :pswitch_c
        :pswitch_6
        :pswitch_3
        :pswitch_4
        :pswitch_1
        :pswitch_b
        :pswitch_0
        :pswitch_2
        :pswitch_8
    .end packed-switch

    :goto_35
    goto/16 :goto_4a

    :sswitch_2
    goto/32 :goto_1a

    nop

    :goto_36
    goto/16 :goto_4a

    :sswitch_3
    goto/32 :goto_71

    nop

    :goto_37
    invoke-virtual {p2, p6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p6

    goto/32 :goto_72

    nop

    :goto_38
    invoke-virtual {p0, p1, p4}, Ld/g;->j(Landroid/content/Context;Landroid/util/AttributeSet;)Landroidx/appcompat/widget/AppCompatRadioButton;

    move-result-object p5

    goto/32 :goto_48

    nop

    :goto_39
    goto/16 :goto_4a

    :sswitch_4
    goto/32 :goto_5e

    nop

    :goto_3a
    if-eqz p6, :cond_5

    goto/32 :goto_5b

    :cond_5
    goto/32 :goto_5a

    nop

    :goto_3b
    goto :goto_4a

    :sswitch_5
    goto/32 :goto_2b

    nop

    :goto_3c
    if-eqz p6, :cond_6

    goto/32 :goto_1d

    :cond_6
    goto/32 :goto_1c

    nop

    :goto_3d
    const/16 p5, 0xb

    goto/32 :goto_7e

    nop

    :goto_3e
    invoke-virtual {p0, p1, p4}, Ld/g;->c(Landroid/content/Context;Landroid/util/AttributeSet;)Landroidx/appcompat/widget/AppCompatButton;

    move-result-object p5

    goto/32 :goto_24

    nop

    :goto_3f
    if-nez p5, :cond_7

    goto/32 :goto_46

    :cond_7
    goto/32 :goto_45

    nop

    :goto_40
    invoke-virtual {p0, p1, p4}, Ld/g;->d(Landroid/content/Context;Landroid/util/AttributeSet;)Landroidx/appcompat/widget/AppCompatCheckBox;

    move-result-object p5

    goto/32 :goto_82

    nop

    :goto_41
    const-string p6, "TextView"

    goto/32 :goto_11

    nop

    :goto_42
    const/4 p5, -0x1

    goto/32 :goto_3

    nop

    :goto_43
    const/4 p5, 0x1

    goto/32 :goto_36

    nop

    :goto_44
    if-nez p5, :cond_8

    goto/32 :goto_2d

    :cond_8
    goto/32 :goto_68

    nop

    :goto_45
    invoke-direct {p0, p5, p4}, Ld/g;->a(Landroid/view/View;Landroid/util/AttributeSet;)V

    :goto_46
    goto/32 :goto_34

    nop

    :goto_47
    if-eqz p6, :cond_9

    goto/32 :goto_6e

    :cond_9
    goto/32 :goto_6d

    nop

    :goto_48
    goto/16 :goto_19

    :pswitch_6
    goto/32 :goto_56

    nop

    :goto_49
    const/4 p5, 0x0

    :goto_4a
    packed-switch p5, :pswitch_data_0

    goto/32 :goto_1b

    nop

    :goto_4b
    invoke-virtual {p0, p1, p4}, Ld/g;->e(Landroid/content/Context;Landroid/util/AttributeSet;)Landroidx/appcompat/widget/AppCompatCheckedTextView;

    move-result-object p5

    goto/32 :goto_69

    nop

    :goto_4c
    const/4 p5, 0x6

    goto/32 :goto_51

    nop

    :goto_4d
    const/16 p5, 0xa

    goto/32 :goto_55

    nop

    :goto_4e
    if-nez p8, :cond_a

    goto/32 :goto_80

    :cond_a
    goto/32 :goto_7f

    nop

    :goto_4f
    invoke-virtual {p0, p1, p4}, Ld/g;->i(Landroid/content/Context;Landroid/util/AttributeSet;)Landroidx/appcompat/widget/k;

    move-result-object p5

    goto/32 :goto_93

    nop

    :goto_50
    goto/16 :goto_4a

    :sswitch_6
    goto/32 :goto_97

    nop

    :goto_51
    goto :goto_4a

    :sswitch_7
    goto/32 :goto_1e

    nop

    :goto_52
    const/4 p5, 0x5

    goto/32 :goto_10

    nop

    :goto_53
    const-string p6, "EditText"

    goto/32 :goto_81

    nop

    :goto_54
    invoke-virtual {p2, p6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p6

    goto/32 :goto_89

    nop

    :goto_55
    goto/16 :goto_4a

    :sswitch_8
    goto/32 :goto_17

    nop

    :goto_56
    invoke-virtual {p0, p1, p4}, Ld/g;->m(Landroid/content/Context;Landroid/util/AttributeSet;)Landroidx/appcompat/widget/q;

    move-result-object p5

    goto/32 :goto_8d

    nop

    :goto_57
    const/16 p5, 0x9

    goto/32 :goto_39

    nop

    :goto_58
    goto :goto_4a

    :goto_59
    goto/32 :goto_43

    nop

    :goto_5a
    goto/16 :goto_4a

    :goto_5b
    goto/32 :goto_94

    nop

    :goto_5c
    invoke-virtual {p2, p6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p6

    goto/32 :goto_78

    nop

    :goto_5d
    goto/16 :goto_19

    :pswitch_7
    goto/32 :goto_18

    nop

    :goto_5e
    const-string p6, "ToggleButton"

    goto/32 :goto_7d

    nop

    :goto_5f
    invoke-virtual {p2, p6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p6

    goto/32 :goto_70

    nop

    :goto_60
    invoke-virtual {p2, p6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p6

    goto/32 :goto_2e

    nop

    :goto_61
    goto/16 :goto_4a

    :goto_62
    goto/32 :goto_8e

    nop

    :goto_63
    goto :goto_65

    :pswitch_8
    goto/32 :goto_3e

    nop

    :goto_64
    goto/16 :goto_19

    :goto_65
    goto/32 :goto_6a

    nop

    :goto_66
    goto/16 :goto_4a

    :sswitch_9
    goto/32 :goto_53

    nop

    :goto_67
    if-eqz p6, :cond_b

    goto/32 :goto_75

    :cond_b
    goto/32 :goto_74

    nop

    :goto_68
    if-nez p1, :cond_c

    goto/32 :goto_2d

    :cond_c
    goto/32 :goto_7

    nop

    :goto_69
    goto/16 :goto_19

    :pswitch_9
    goto/32 :goto_7a

    nop

    :goto_6a
    if-eqz p5, :cond_d

    goto/32 :goto_c

    :cond_d
    goto/32 :goto_26

    nop

    :goto_6b
    goto/16 :goto_4a

    :goto_6c
    goto/32 :goto_4d

    nop

    :goto_6d
    goto/16 :goto_4a

    :goto_6e
    goto/32 :goto_73

    nop

    :goto_6f
    goto/16 :goto_4a

    :sswitch_a
    goto/32 :goto_76

    nop

    :goto_70
    if-eqz p6, :cond_e

    goto/32 :goto_59

    :cond_e
    goto/32 :goto_58

    nop

    :goto_71
    const-string p6, "RatingBar"

    goto/32 :goto_87

    nop

    :goto_72
    if-eqz p6, :cond_f

    goto/32 :goto_6

    :cond_f
    goto/32 :goto_5

    nop

    :goto_73
    const/4 p5, 0x2

    goto/32 :goto_88

    nop

    :goto_74
    if-nez p7, :cond_10

    goto/32 :goto_e

    :cond_10
    :goto_75
    goto/32 :goto_d

    nop

    :goto_76
    const-string p6, "CheckBox"

    goto/32 :goto_9d

    nop

    :goto_77
    goto/16 :goto_19

    :pswitch_a
    goto/32 :goto_22

    nop

    :goto_78
    if-eqz p6, :cond_11

    goto/32 :goto_8c

    :cond_11
    goto/32 :goto_8b

    nop

    :goto_79
    invoke-virtual {p2, p6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p6

    goto/32 :goto_3a

    nop

    :goto_7a
    invoke-virtual {p0, p1, p4}, Ld/g;->k(Landroid/content/Context;Landroid/util/AttributeSet;)Landroidx/appcompat/widget/n;

    move-result-object p5

    goto/32 :goto_64

    nop

    :goto_7b
    goto/16 :goto_4a

    :sswitch_b
    goto/32 :goto_41

    nop

    :goto_7c
    invoke-virtual {p0, p1, p4}, Ld/g;->b(Landroid/content/Context;Landroid/util/AttributeSet;)Landroidx/appcompat/widget/d;

    move-result-object p5

    goto/32 :goto_23

    nop

    :goto_7d
    invoke-virtual {p2, p6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p6

    goto/32 :goto_3c

    nop

    :goto_7e
    goto/16 :goto_4a

    :sswitch_c
    goto/32 :goto_91

    nop

    :goto_7f
    invoke-static {p1}, Landroidx/appcompat/widget/l0;->b(Landroid/content/Context;)Landroid/content/Context;

    move-result-object p1

    :goto_80
    goto/32 :goto_15

    nop

    :goto_81
    invoke-virtual {p2, p6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p6

    goto/32 :goto_2a

    nop

    :goto_82
    goto/16 :goto_19

    :pswitch_b
    goto/32 :goto_7c

    nop

    :goto_83
    if-eqz p6, :cond_12

    goto/32 :goto_9b

    :cond_12
    goto/32 :goto_9a

    nop

    :goto_84
    if-eqz p6, :cond_13

    goto/32 :goto_a

    :cond_13
    goto/32 :goto_9

    nop

    :goto_85
    goto/16 :goto_4a

    :goto_86
    goto/32 :goto_57

    nop

    :goto_87
    invoke-virtual {p2, p6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p6

    goto/32 :goto_20

    nop

    :goto_88
    goto/16 :goto_4a

    :sswitch_d
    goto/32 :goto_8f

    nop

    :goto_89
    if-eqz p6, :cond_14

    goto/32 :goto_2

    :cond_14
    goto/32 :goto_1

    nop

    :goto_8a
    const/4 p5, 0x3

    goto/32 :goto_3b

    nop

    :goto_8b
    goto/16 :goto_4a

    :goto_8c
    goto/32 :goto_52

    nop

    :goto_8d
    goto/16 :goto_19

    :pswitch_c
    goto/32 :goto_8

    nop

    :goto_8e
    const/4 p5, 0x7

    goto/32 :goto_16

    nop

    :goto_8f
    const-string p6, "CheckedTextView"

    goto/32 :goto_5f

    nop

    :goto_90
    if-eqz p6, :cond_15

    goto/32 :goto_62

    :cond_15
    goto/32 :goto_61

    nop

    :goto_91
    const-string p6, "AutoCompleteTextView"

    goto/32 :goto_60

    nop

    :goto_92
    const-string p6, "ImageButton"

    goto/32 :goto_54

    nop

    :goto_93
    goto/16 :goto_19

    :pswitch_d
    goto/32 :goto_4b

    nop

    :goto_94
    const/16 p5, 0xd

    goto/32 :goto_66

    nop

    :goto_95
    goto/16 :goto_4a

    :goto_96
    goto/32 :goto_49

    nop

    :goto_97
    const-string p6, "Button"

    goto/32 :goto_79

    nop

    :goto_98
    goto/16 :goto_4a

    :goto_99
    goto/32 :goto_9c

    nop

    :goto_9a
    goto/16 :goto_4a

    :goto_9b
    goto/32 :goto_3d

    nop

    :goto_9c
    const/16 p5, 0xc

    goto/32 :goto_6f

    nop

    :goto_9d
    invoke-virtual {p2, p6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p6

    goto/32 :goto_83

    nop
.end method
