.class Ld/e;
.super Ld/d;

# interfaces
.implements Landroidx/appcompat/view/menu/e$a;
.implements Landroid/view/LayoutInflater$Factory2;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ld/e$n;,
        Ld/e$m;,
        Ld/e$l;,
        Ld/e$i;,
        Ld/e$k;,
        Ld/e$j;,
        Ld/e$h;,
        Ld/e$p;,
        Ld/e$q;,
        Ld/e$f;,
        Ld/e$r;,
        Ld/e$g;,
        Ld/e$o;
    }
.end annotation


# static fields
.field private static final e0:Ln/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ln/g<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final f0:Z

.field private static final g0:[I

.field private static final h0:Z

.field private static final i0:Z


# instance fields
.field A:Z

.field B:Z

.field C:Z

.field D:Z

.field E:Z

.field private F:Z

.field private G:[Ld/e$q;

.field private H:Ld/e$q;

.field private I:Z

.field private J:Z

.field private K:Z

.field private L:Z

.field M:Z

.field private N:I

.field private O:I

.field private P:Z

.field private Q:Z

.field private R:Ld/e$j;

.field private S:Ld/e$j;

.field T:Z

.field U:I

.field private final V:Ljava/lang/Runnable;

.field private W:Z

.field private X:Landroid/graphics/Rect;

.field private Y:Landroid/graphics/Rect;

.field private Z:Ld/g;

.field final d:Ljava/lang/Object;

.field final e:Landroid/content/Context;

.field f:Landroid/view/Window;

.field private g:Ld/e$h;

.field final h:Ld/c;

.field i:Ld/a;

.field j:Landroid/view/MenuInflater;

.field private k:Ljava/lang/CharSequence;

.field private l:Landroidx/appcompat/widget/v;

.field private m:Ld/e$f;

.field private n:Ld/e$r;

.field o:Li/b;

.field p:Landroidx/appcompat/widget/ActionBarContextView;

.field q:Landroid/widget/PopupWindow;

.field r:Ljava/lang/Runnable;

.field s:Landroidx/core/view/y;

.field private t:Z

.field private u:Z

.field v:Landroid/view/ViewGroup;

.field private w:Landroid/widget/TextView;

.field private x:Landroid/view/View;

.field private y:Z

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Ln/g;

    invoke-direct {v0}, Ln/g;-><init>()V

    sput-object v0, Ld/e;->e0:Ln/g;

    const/4 v0, 0x0

    sput-boolean v0, Ld/e;->f0:Z

    const/4 v1, 0x1

    new-array v2, v1, [I

    const v3, 0x1010054

    aput v3, v2, v0

    sput-object v2, Ld/e;->g0:[I

    sget-object v0, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    const-string v2, "robolectric"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    xor-int/2addr v0, v1

    sput-boolean v0, Ld/e;->h0:Z

    sput-boolean v1, Ld/e;->i0:Z

    return-void
.end method

.method constructor <init>(Landroid/app/Activity;Ld/c;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2, p1}, Ld/e;-><init>(Landroid/content/Context;Landroid/view/Window;Ld/c;Ljava/lang/Object;)V

    return-void
.end method

.method constructor <init>(Landroid/app/Dialog;Ld/c;)V
    .locals 2

    invoke-virtual {p1}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-direct {p0, v0, v1, p2, p1}, Ld/e;-><init>(Landroid/content/Context;Landroid/view/Window;Ld/c;Ljava/lang/Object;)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/view/Window;Ld/c;Ljava/lang/Object;)V
    .locals 2

    invoke-direct {p0}, Ld/d;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Ld/e;->s:Landroidx/core/view/y;

    const/4 v0, 0x1

    iput-boolean v0, p0, Ld/e;->t:Z

    const/16 v0, -0x64

    iput v0, p0, Ld/e;->N:I

    new-instance v1, Ld/e$a;

    invoke-direct {v1, p0}, Ld/e$a;-><init>(Ld/e;)V

    iput-object v1, p0, Ld/e;->V:Ljava/lang/Runnable;

    iput-object p1, p0, Ld/e;->e:Landroid/content/Context;

    iput-object p3, p0, Ld/e;->h:Ld/c;

    iput-object p4, p0, Ld/e;->d:Ljava/lang/Object;

    iget p1, p0, Ld/e;->N:I

    if-ne p1, v0, :cond_0

    instance-of p1, p4, Landroid/app/Dialog;

    if-eqz p1, :cond_0

    invoke-direct {p0}, Ld/e;->I0()Ld/b;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ld/b;->f0()Ld/d;

    move-result-object p1

    invoke-virtual {p1}, Ld/d;->k()I

    move-result p1

    iput p1, p0, Ld/e;->N:I

    :cond_0
    iget p1, p0, Ld/e;->N:I

    if-ne p1, v0, :cond_1

    sget-object p1, Ld/e;->e0:Ln/g;

    invoke-virtual {p4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p3

    invoke-virtual {p3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p1, p3}, Ln/g;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/lang/Integer;

    if-eqz p3, :cond_1

    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result p3

    iput p3, p0, Ld/e;->N:I

    invoke-virtual {p4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p3

    invoke-virtual {p3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p1, p3}, Ln/g;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    if-eqz p2, :cond_2

    invoke-direct {p0, p2}, Ld/e;->I(Landroid/view/Window;)V

    :cond_2
    invoke-static {}, Landroidx/appcompat/widget/g;->h()V

    return-void
.end method

.method private A0(Ld/e$q;Landroid/view/KeyEvent;)Z
    .locals 8

    iget-boolean v0, p0, Ld/e;->M:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    :cond_0
    iget-boolean v0, p1, Ld/e$q;->m:Z

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    return v2

    :cond_1
    iget-object v0, p0, Ld/e;->H:Ld/e$q;

    if-eqz v0, :cond_2

    if-eq v0, p1, :cond_2

    invoke-virtual {p0, v0, v1}, Ld/e;->O(Ld/e$q;Z)V

    :cond_2
    invoke-virtual {p0}, Ld/e;->f0()Landroid/view/Window$Callback;

    move-result-object v0

    if-eqz v0, :cond_3

    iget v3, p1, Ld/e$q;->a:I

    invoke-interface {v0, v3}, Landroid/view/Window$Callback;->onCreatePanelView(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p1, Ld/e$q;->i:Landroid/view/View;

    :cond_3
    iget v3, p1, Ld/e$q;->a:I

    if-eqz v3, :cond_5

    const/16 v4, 0x6c

    if-ne v3, v4, :cond_4

    goto :goto_0

    :cond_4
    move v3, v1

    goto :goto_1

    :cond_5
    :goto_0
    move v3, v2

    :goto_1
    if-eqz v3, :cond_6

    iget-object v4, p0, Ld/e;->l:Landroidx/appcompat/widget/v;

    if-eqz v4, :cond_6

    invoke-interface {v4}, Landroidx/appcompat/widget/v;->d()V

    :cond_6
    iget-object v4, p1, Ld/e$q;->i:Landroid/view/View;

    if-nez v4, :cond_15

    if-eqz v3, :cond_7

    invoke-virtual {p0}, Ld/e;->y0()Ld/a;

    :cond_7
    iget-object v4, p1, Ld/e$q;->j:Landroidx/appcompat/view/menu/e;

    const/4 v5, 0x0

    if-eqz v4, :cond_8

    iget-boolean v6, p1, Ld/e$q;->r:Z

    if-eqz v6, :cond_f

    :cond_8
    if-nez v4, :cond_a

    invoke-direct {p0, p1}, Ld/e;->j0(Ld/e$q;)Z

    move-result v4

    if-eqz v4, :cond_9

    iget-object v4, p1, Ld/e$q;->j:Landroidx/appcompat/view/menu/e;

    if-nez v4, :cond_a

    :cond_9
    return v1

    :cond_a
    if-eqz v3, :cond_c

    iget-object v4, p0, Ld/e;->l:Landroidx/appcompat/widget/v;

    if-eqz v4, :cond_c

    iget-object v4, p0, Ld/e;->m:Ld/e$f;

    if-nez v4, :cond_b

    new-instance v4, Ld/e$f;

    invoke-direct {v4, p0}, Ld/e$f;-><init>(Ld/e;)V

    iput-object v4, p0, Ld/e;->m:Ld/e$f;

    :cond_b
    iget-object v4, p0, Ld/e;->l:Landroidx/appcompat/widget/v;

    iget-object v6, p1, Ld/e$q;->j:Landroidx/appcompat/view/menu/e;

    iget-object v7, p0, Ld/e;->m:Ld/e$f;

    invoke-interface {v4, v6, v7}, Landroidx/appcompat/widget/v;->a(Landroid/view/Menu;Landroidx/appcompat/view/menu/j$a;)V

    :cond_c
    iget-object v4, p1, Ld/e$q;->j:Landroidx/appcompat/view/menu/e;

    invoke-virtual {v4}, Landroidx/appcompat/view/menu/e;->d0()V

    iget v4, p1, Ld/e$q;->a:I

    iget-object v6, p1, Ld/e$q;->j:Landroidx/appcompat/view/menu/e;

    invoke-interface {v0, v4, v6}, Landroid/view/Window$Callback;->onCreatePanelMenu(ILandroid/view/Menu;)Z

    move-result v4

    if-nez v4, :cond_e

    invoke-virtual {p1, v5}, Ld/e$q;->c(Landroidx/appcompat/view/menu/e;)V

    if-eqz v3, :cond_d

    iget-object p1, p0, Ld/e;->l:Landroidx/appcompat/widget/v;

    if-eqz p1, :cond_d

    iget-object p2, p0, Ld/e;->m:Ld/e$f;

    invoke-interface {p1, v5, p2}, Landroidx/appcompat/widget/v;->a(Landroid/view/Menu;Landroidx/appcompat/view/menu/j$a;)V

    :cond_d
    return v1

    :cond_e
    iput-boolean v1, p1, Ld/e$q;->r:Z

    :cond_f
    iget-object v4, p1, Ld/e$q;->j:Landroidx/appcompat/view/menu/e;

    invoke-virtual {v4}, Landroidx/appcompat/view/menu/e;->d0()V

    iget-object v4, p1, Ld/e$q;->s:Landroid/os/Bundle;

    if-eqz v4, :cond_10

    iget-object v6, p1, Ld/e$q;->j:Landroidx/appcompat/view/menu/e;

    invoke-virtual {v6, v4}, Landroidx/appcompat/view/menu/e;->P(Landroid/os/Bundle;)V

    iput-object v5, p1, Ld/e$q;->s:Landroid/os/Bundle;

    :cond_10
    iget-object v4, p1, Ld/e$q;->i:Landroid/view/View;

    iget-object v6, p1, Ld/e$q;->j:Landroidx/appcompat/view/menu/e;

    invoke-interface {v0, v1, v4, v6}, Landroid/view/Window$Callback;->onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z

    move-result v0

    if-nez v0, :cond_12

    if-eqz v3, :cond_11

    iget-object p2, p0, Ld/e;->l:Landroidx/appcompat/widget/v;

    if-eqz p2, :cond_11

    iget-object v0, p0, Ld/e;->m:Ld/e$f;

    invoke-interface {p2, v5, v0}, Landroidx/appcompat/widget/v;->a(Landroid/view/Menu;Landroidx/appcompat/view/menu/j$a;)V

    :cond_11
    iget-object p1, p1, Ld/e$q;->j:Landroidx/appcompat/view/menu/e;

    invoke-virtual {p1}, Landroidx/appcompat/view/menu/e;->c0()V

    return v1

    :cond_12
    if-eqz p2, :cond_13

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getDeviceId()I

    move-result p2

    goto :goto_2

    :cond_13
    const/4 p2, -0x1

    :goto_2
    invoke-static {p2}, Landroid/view/KeyCharacterMap;->load(I)Landroid/view/KeyCharacterMap;

    move-result-object p2

    invoke-virtual {p2}, Landroid/view/KeyCharacterMap;->getKeyboardType()I

    move-result p2

    if-eq p2, v2, :cond_14

    move p2, v2

    goto :goto_3

    :cond_14
    move p2, v1

    :goto_3
    iput-boolean p2, p1, Ld/e$q;->p:Z

    iget-object v0, p1, Ld/e$q;->j:Landroidx/appcompat/view/menu/e;

    invoke-virtual {v0, p2}, Landroidx/appcompat/view/menu/e;->setQwertyMode(Z)V

    iget-object p2, p1, Ld/e$q;->j:Landroidx/appcompat/view/menu/e;

    invoke-virtual {p2}, Landroidx/appcompat/view/menu/e;->c0()V

    :cond_15
    iput-boolean v2, p1, Ld/e$q;->m:Z

    iput-boolean v1, p1, Ld/e$q;->n:Z

    iput-object p1, p0, Ld/e;->H:Ld/e$q;

    return v2
.end method

.method private B0(Z)V
    .locals 5

    iget-object v0, p0, Ld/e;->l:Landroidx/appcompat/widget/v;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_5

    invoke-interface {v0}, Landroidx/appcompat/widget/v;->h()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Ld/e;->e:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->hasPermanentMenuKey()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ld/e;->l:Landroidx/appcompat/widget/v;

    invoke-interface {v0}, Landroidx/appcompat/widget/v;->e()Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_0
    invoke-virtual {p0}, Ld/e;->f0()Landroid/view/Window$Callback;

    move-result-object v0

    iget-object v3, p0, Ld/e;->l:Landroidx/appcompat/widget/v;

    invoke-interface {v3}, Landroidx/appcompat/widget/v;->b()Z

    move-result v3

    const/16 v4, 0x6c

    if-eqz v3, :cond_2

    if-nez p1, :cond_1

    goto :goto_0

    :cond_1
    iget-object p1, p0, Ld/e;->l:Landroidx/appcompat/widget/v;

    invoke-interface {p1}, Landroidx/appcompat/widget/v;->f()Z

    iget-boolean p1, p0, Ld/e;->M:Z

    if-nez p1, :cond_4

    invoke-virtual {p0, v2, v1}, Ld/e;->d0(IZ)Ld/e$q;

    move-result-object p1

    iget-object p1, p1, Ld/e$q;->j:Landroidx/appcompat/view/menu/e;

    invoke-interface {v0, v4, p1}, Landroid/view/Window$Callback;->onPanelClosed(ILandroid/view/Menu;)V

    goto :goto_1

    :cond_2
    :goto_0
    if-eqz v0, :cond_4

    iget-boolean p1, p0, Ld/e;->M:Z

    if-nez p1, :cond_4

    iget-boolean p1, p0, Ld/e;->T:Z

    if-eqz p1, :cond_3

    iget p1, p0, Ld/e;->U:I

    and-int/2addr p1, v1

    if-eqz p1, :cond_3

    iget-object p1, p0, Ld/e;->f:Landroid/view/Window;

    invoke-virtual {p1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object p1

    iget-object v3, p0, Ld/e;->V:Ljava/lang/Runnable;

    invoke-virtual {p1, v3}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-object p1, p0, Ld/e;->V:Ljava/lang/Runnable;

    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    :cond_3
    invoke-virtual {p0, v2, v1}, Ld/e;->d0(IZ)Ld/e$q;

    move-result-object p1

    iget-object v1, p1, Ld/e$q;->j:Landroidx/appcompat/view/menu/e;

    if-eqz v1, :cond_4

    iget-boolean v3, p1, Ld/e$q;->r:Z

    if-nez v3, :cond_4

    iget-object v3, p1, Ld/e$q;->i:Landroid/view/View;

    invoke-interface {v0, v2, v3, v1}, Landroid/view/Window$Callback;->onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object p1, p1, Ld/e$q;->j:Landroidx/appcompat/view/menu/e;

    invoke-interface {v0, v4, p1}, Landroid/view/Window$Callback;->onMenuOpened(ILandroid/view/Menu;)Z

    iget-object p1, p0, Ld/e;->l:Landroidx/appcompat/widget/v;

    invoke-interface {p1}, Landroidx/appcompat/widget/v;->g()Z

    :cond_4
    :goto_1
    return-void

    :cond_5
    invoke-virtual {p0, v2, v1}, Ld/e;->d0(IZ)Ld/e$q;

    move-result-object p1

    iput-boolean v1, p1, Ld/e$q;->q:Z

    invoke-virtual {p0, p1, v2}, Ld/e;->O(Ld/e$q;Z)V

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Ld/e;->x0(Ld/e$q;Landroid/view/KeyEvent;)V

    return-void
.end method

.method private C0(I)I
    .locals 2

    const-string v0, "AppCompatDelegate"

    const/16 v1, 0x8

    if-ne p1, v1, :cond_0

    const-string p1, "You should now use the AppCompatDelegate.FEATURE_SUPPORT_ACTION_BAR id when requesting this feature."

    invoke-static {v0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/16 p1, 0x6c

    return p1

    :cond_0
    const/16 v1, 0x9

    if-ne p1, v1, :cond_1

    const-string p1, "You should now use the AppCompatDelegate.FEATURE_SUPPORT_ACTION_BAR_OVERLAY id when requesting this feature."

    invoke-static {v0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/16 p1, 0x6d

    :cond_1
    return p1
.end method

.method private E0(Landroid/view/ViewParent;)Z
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    :cond_0
    iget-object v1, p0, Ld/e;->f:Landroid/view/Window;

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    :goto_0
    if-nez p1, :cond_1

    const/4 p1, 0x1

    return p1

    :cond_1
    if-eq p1, v1, :cond_3

    instance-of v2, p1, Landroid/view/View;

    if-eqz v2, :cond_3

    move-object v2, p1

    check-cast v2, Landroid/view/View;

    invoke-static {v2}, Landroidx/core/view/t;->N(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_1

    :cond_2
    invoke-interface {p1}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    goto :goto_0

    :cond_3
    :goto_1
    return v0
.end method

.method private G(Z)Z
    .locals 2

    iget-boolean v0, p0, Ld/e;->M:Z

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    invoke-direct {p0}, Ld/e;->J()I

    move-result v0

    iget-object v1, p0, Ld/e;->e:Landroid/content/Context;

    invoke-virtual {p0, v1, v0}, Ld/e;->n0(Landroid/content/Context;I)I

    move-result v1

    invoke-direct {p0, v1, p1}, Ld/e;->J0(IZ)Z

    move-result p1

    if-nez v0, :cond_1

    iget-object v1, p0, Ld/e;->e:Landroid/content/Context;

    invoke-direct {p0, v1}, Ld/e;->c0(Landroid/content/Context;)Ld/e$j;

    move-result-object v1

    invoke-virtual {v1}, Ld/e$j;->e()V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Ld/e;->R:Ld/e$j;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ld/e$j;->a()V

    :cond_2
    :goto_0
    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Ld/e;->e:Landroid/content/Context;

    invoke-direct {p0, v0}, Ld/e;->b0(Landroid/content/Context;)Ld/e$j;

    move-result-object v0

    invoke-virtual {v0}, Ld/e$j;->e()V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Ld/e;->S:Ld/e$j;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ld/e$j;->a()V

    :cond_4
    :goto_1
    return p1
.end method

.method private H()V
    .locals 5

    iget-object v0, p0, Ld/e;->v:Landroid/view/ViewGroup;

    const v1, 0x1020002

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/ContentFrameLayout;

    iget-object v1, p0, Ld/e;->f:Landroid/view/Window;

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getPaddingLeft()I

    move-result v2

    invoke-virtual {v1}, Landroid/view/View;->getPaddingTop()I

    move-result v3

    invoke-virtual {v1}, Landroid/view/View;->getPaddingRight()I

    move-result v4

    invoke-virtual {v1}, Landroid/view/View;->getPaddingBottom()I

    move-result v1

    invoke-virtual {v0, v2, v3, v4, v1}, Landroidx/appcompat/widget/ContentFrameLayout;->a(IIII)V

    iget-object v1, p0, Ld/e;->e:Landroid/content/Context;

    sget-object v2, Lc/j;->z0:[I

    invoke-virtual {v1, v2}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v1

    sget v2, Lc/j;->L0:I

    invoke-virtual {v0}, Landroidx/appcompat/widget/ContentFrameLayout;->getMinWidthMajor()Landroid/util/TypedValue;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    sget v2, Lc/j;->M0:I

    invoke-virtual {v0}, Landroidx/appcompat/widget/ContentFrameLayout;->getMinWidthMinor()Landroid/util/TypedValue;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    sget v2, Lc/j;->J0:I

    invoke-virtual {v1, v2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Landroidx/appcompat/widget/ContentFrameLayout;->getFixedWidthMajor()Landroid/util/TypedValue;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    :cond_0
    sget v2, Lc/j;->K0:I

    invoke-virtual {v1, v2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v0}, Landroidx/appcompat/widget/ContentFrameLayout;->getFixedWidthMinor()Landroid/util/TypedValue;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    :cond_1
    sget v2, Lc/j;->H0:I

    invoke-virtual {v1, v2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v0}, Landroidx/appcompat/widget/ContentFrameLayout;->getFixedHeightMajor()Landroid/util/TypedValue;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    :cond_2
    sget v2, Lc/j;->I0:I

    invoke-virtual {v1, v2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v0}, Landroidx/appcompat/widget/ContentFrameLayout;->getFixedHeightMinor()Landroid/util/TypedValue;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    :cond_3
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->requestLayout()V

    return-void
.end method

.method private H0()V
    .locals 2

    iget-boolean v0, p0, Ld/e;->u:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Landroid/util/AndroidRuntimeException;

    const-string v1, "Window feature must be requested before adding content"

    invoke-direct {v0, v1}, Landroid/util/AndroidRuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private I(Landroid/view/Window;)V
    .locals 3

    iget-object v0, p0, Ld/e;->f:Landroid/view/Window;

    const-string v1, "AppCompat has already installed itself into the Window"

    if-nez v0, :cond_2

    invoke-virtual {p1}, Landroid/view/Window;->getCallback()Landroid/view/Window$Callback;

    move-result-object v0

    instance-of v2, v0, Ld/e$h;

    if-nez v2, :cond_1

    new-instance v1, Ld/e$h;

    invoke-direct {v1, p0, v0}, Ld/e$h;-><init>(Ld/e;Landroid/view/Window$Callback;)V

    iput-object v1, p0, Ld/e;->g:Ld/e$h;

    invoke-virtual {p1, v1}, Landroid/view/Window;->setCallback(Landroid/view/Window$Callback;)V

    iget-object v0, p0, Ld/e;->e:Landroid/content/Context;

    const/4 v1, 0x0

    sget-object v2, Ld/e;->g0:[I

    invoke-static {v0, v1, v2}, Landroidx/appcompat/widget/o0;->u(Landroid/content/Context;Landroid/util/AttributeSet;[I)Landroidx/appcompat/widget/o0;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/o0;->h(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1, v1}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    invoke-virtual {v0}, Landroidx/appcompat/widget/o0;->w()V

    iput-object p1, p0, Ld/e;->f:Landroid/view/Window;

    return-void

    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private I0()Ld/b;
    .locals 3

    iget-object v0, p0, Ld/e;->e:Landroid/content/Context;

    :goto_0
    const/4 v1, 0x0

    if-eqz v0, :cond_1

    instance-of v2, v0, Ld/b;

    if-eqz v2, :cond_0

    check-cast v0, Ld/b;

    return-object v0

    :cond_0
    instance-of v2, v0, Landroid/content/ContextWrapper;

    if-eqz v2, :cond_1

    check-cast v0, Landroid/content/ContextWrapper;

    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method private J()I
    .locals 2

    iget v0, p0, Ld/e;->N:I

    const/16 v1, -0x64

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {}, Ld/d;->j()I

    move-result v0

    :goto_0
    return v0
.end method

.method private J0(IZ)Z
    .locals 6

    iget-object v0, p0, Ld/e;->e:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, v1}, Ld/e;->P(Landroid/content/Context;ILandroid/content/res/Configuration;)Landroid/content/res/Configuration;

    move-result-object v0

    invoke-direct {p0}, Ld/e;->l0()Z

    move-result v2

    iget-object v3, p0, Ld/e;->e:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->uiMode:I

    and-int/lit8 v3, v3, 0x30

    iget v0, v0, Landroid/content/res/Configuration;->uiMode:I

    and-int/lit8 v0, v0, 0x30

    const/4 v4, 0x1

    if-eq v3, v0, :cond_1

    if-eqz p2, :cond_1

    if-nez v2, :cond_1

    iget-boolean p2, p0, Ld/e;->J:Z

    if-eqz p2, :cond_1

    sget-boolean p2, Ld/e;->h0:Z

    if-nez p2, :cond_0

    iget-boolean p2, p0, Ld/e;->K:Z

    if-eqz p2, :cond_1

    :cond_0
    iget-object p2, p0, Ld/e;->d:Ljava/lang/Object;

    instance-of v5, p2, Landroid/app/Activity;

    if-eqz v5, :cond_1

    check-cast p2, Landroid/app/Activity;

    invoke-virtual {p2}, Landroid/app/Activity;->isChild()Z

    move-result p2

    if-nez p2, :cond_1

    iget-object p2, p0, Ld/e;->d:Ljava/lang/Object;

    check-cast p2, Landroid/app/Activity;

    invoke-static {p2}, Landroidx/core/app/a;->j(Landroid/app/Activity;)V

    move p2, v4

    goto :goto_0

    :cond_1
    const/4 p2, 0x0

    :goto_0
    if-nez p2, :cond_2

    if-eq v3, v0, :cond_2

    invoke-direct {p0, v0, v2, v1}, Ld/e;->K0(IZLandroid/content/res/Configuration;)V

    goto :goto_1

    :cond_2
    move v4, p2

    :goto_1
    if-eqz v4, :cond_3

    iget-object p2, p0, Ld/e;->d:Ljava/lang/Object;

    instance-of v0, p2, Ld/b;

    if-eqz v0, :cond_3

    check-cast p2, Ld/b;

    invoke-virtual {p2, p1}, Ld/b;->i0(I)V

    :cond_3
    return v4
.end method

.method private K0(IZLandroid/content/res/Configuration;)V
    .locals 3

    iget-object v0, p0, Ld/e;->e:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    new-instance v1, Landroid/content/res/Configuration;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/content/res/Configuration;-><init>(Landroid/content/res/Configuration;)V

    if-eqz p3, :cond_0

    invoke-virtual {v1, p3}, Landroid/content/res/Configuration;->updateFrom(Landroid/content/res/Configuration;)I

    :cond_0
    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object p3

    iget p3, p3, Landroid/content/res/Configuration;->uiMode:I

    and-int/lit8 p3, p3, -0x31

    or-int/2addr p1, p3

    iput p1, v1, Landroid/content/res/Configuration;->uiMode:I

    const/4 p1, 0x0

    invoke-virtual {v0, v1, p1}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    iget p1, p0, Ld/e;->O:I

    if-eqz p1, :cond_1

    iget-object p3, p0, Ld/e;->e:Landroid/content/Context;

    invoke-virtual {p3, p1}, Landroid/content/Context;->setTheme(I)V

    iget-object p1, p0, Ld/e;->e:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object p1

    iget p3, p0, Ld/e;->O:I

    const/4 v0, 0x1

    invoke-virtual {p1, p3, v0}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    :cond_1
    if-eqz p2, :cond_3

    iget-object p1, p0, Ld/e;->d:Ljava/lang/Object;

    instance-of p2, p1, Landroid/app/Activity;

    if-eqz p2, :cond_3

    check-cast p1, Landroid/app/Activity;

    instance-of p2, p1, Landroidx/lifecycle/j;

    if-eqz p2, :cond_2

    move-object p2, p1

    check-cast p2, Landroidx/lifecycle/j;

    invoke-interface {p2}, Landroidx/lifecycle/j;->a()Landroidx/lifecycle/d;

    move-result-object p2

    invoke-virtual {p2}, Landroidx/lifecycle/d;->b()Landroidx/lifecycle/d$c;

    move-result-object p2

    sget-object p3, Landroidx/lifecycle/d$c;->d:Landroidx/lifecycle/d$c;

    invoke-virtual {p2, p3}, Landroidx/lifecycle/d$c;->a(Landroidx/lifecycle/d$c;)Z

    move-result p2

    if-eqz p2, :cond_3

    goto :goto_0

    :cond_2
    iget-boolean p2, p0, Ld/e;->L:Z

    if-eqz p2, :cond_3

    :goto_0
    invoke-virtual {p1, v1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    :cond_3
    return-void
.end method

.method private M()V
    .locals 1

    iget-object v0, p0, Ld/e;->R:Ld/e$j;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ld/e$j;->a()V

    :cond_0
    iget-object v0, p0, Ld/e;->S:Ld/e$j;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ld/e$j;->a()V

    :cond_1
    return-void
.end method

.method private M0(Landroid/view/View;)V
    .locals 2

    invoke-static {p1}, Landroidx/core/view/t;->G(Landroid/view/View;)I

    move-result v0

    and-int/lit16 v0, v0, 0x2000

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    iget-object v0, p0, Ld/e;->e:Landroid/content/Context;

    sget v1, Lc/c;->b:I

    goto :goto_1

    :cond_1
    iget-object v0, p0, Ld/e;->e:Landroid/content/Context;

    sget v1, Lc/c;->a:I

    :goto_1
    invoke-static {v0, v1}, Landroidx/core/content/a;->c(Landroid/content/Context;I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundColor(I)V

    return-void
.end method

.method private P(Landroid/content/Context;ILandroid/content/res/Configuration;)Landroid/content/res/Configuration;
    .locals 1

    const/4 v0, 0x1

    if-eq p2, v0, :cond_1

    const/4 v0, 0x2

    if-eq p2, v0, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object p1

    iget p1, p1, Landroid/content/res/Configuration;->uiMode:I

    and-int/lit8 p1, p1, 0x30

    goto :goto_0

    :cond_0
    const/16 p1, 0x20

    goto :goto_0

    :cond_1
    const/16 p1, 0x10

    :goto_0
    new-instance p2, Landroid/content/res/Configuration;

    invoke-direct {p2}, Landroid/content/res/Configuration;-><init>()V

    const/4 v0, 0x0

    iput v0, p2, Landroid/content/res/Configuration;->fontScale:F

    if-eqz p3, :cond_2

    invoke-virtual {p2, p3}, Landroid/content/res/Configuration;->setTo(Landroid/content/res/Configuration;)V

    :cond_2
    iget p3, p2, Landroid/content/res/Configuration;->uiMode:I

    and-int/lit8 p3, p3, -0x31

    or-int/2addr p1, p3

    iput p1, p2, Landroid/content/res/Configuration;->uiMode:I

    return-object p2
.end method

.method private Q()Landroid/view/ViewGroup;
    .locals 7

    iget-object v0, p0, Ld/e;->e:Landroid/content/Context;

    sget-object v1, Lc/j;->z0:[I

    invoke-virtual {v0, v1}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    sget v1, Lc/j;->E0:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v2

    if-eqz v2, :cond_10

    sget v2, Lc/j;->N0:I

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    const/4 v4, 0x1

    if-eqz v2, :cond_0

    invoke-virtual {p0, v4}, Ld/e;->z(I)Z

    goto :goto_0

    :cond_0
    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v1, 0x6c

    invoke-virtual {p0, v1}, Ld/e;->z(I)Z

    :cond_1
    :goto_0
    sget v1, Lc/j;->F0:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    const/16 v2, 0x6d

    if-eqz v1, :cond_2

    invoke-virtual {p0, v2}, Ld/e;->z(I)Z

    :cond_2
    sget v1, Lc/j;->G0:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    if-eqz v1, :cond_3

    const/16 v1, 0xa

    invoke-virtual {p0, v1}, Ld/e;->z(I)Z

    :cond_3
    sget v1, Lc/j;->A0:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Ld/e;->D:Z

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    invoke-direct {p0}, Ld/e;->X()V

    iget-object v0, p0, Ld/e;->f:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    iget-object v0, p0, Ld/e;->e:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget-boolean v1, p0, Ld/e;->E:Z

    const/4 v5, 0x0

    if-nez v1, :cond_9

    iget-boolean v1, p0, Ld/e;->D:Z

    if-eqz v1, :cond_4

    sget v1, Lc/g;->f:I

    invoke-virtual {v0, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-boolean v3, p0, Ld/e;->B:Z

    iput-boolean v3, p0, Ld/e;->A:Z

    goto/16 :goto_3

    :cond_4
    iget-boolean v0, p0, Ld/e;->A:Z

    if-eqz v0, :cond_8

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    iget-object v1, p0, Ld/e;->e:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    sget v6, Lc/a;->f:I

    invoke-virtual {v1, v6, v0, v4}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    iget v1, v0, Landroid/util/TypedValue;->resourceId:I

    if-eqz v1, :cond_5

    new-instance v1, Li/d;

    iget-object v4, p0, Ld/e;->e:Landroid/content/Context;

    iget v0, v0, Landroid/util/TypedValue;->resourceId:I

    invoke-direct {v1, v4, v0}, Li/d;-><init>(Landroid/content/Context;I)V

    goto :goto_1

    :cond_5
    iget-object v1, p0, Ld/e;->e:Landroid/content/Context;

    :goto_1
    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lc/g;->p:I

    invoke-virtual {v0, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    sget v1, Lc/f;->p:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroidx/appcompat/widget/v;

    iput-object v1, p0, Ld/e;->l:Landroidx/appcompat/widget/v;

    invoke-virtual {p0}, Ld/e;->f0()Landroid/view/Window$Callback;

    move-result-object v4

    invoke-interface {v1, v4}, Landroidx/appcompat/widget/v;->setWindowCallback(Landroid/view/Window$Callback;)V

    iget-boolean v1, p0, Ld/e;->B:Z

    if-eqz v1, :cond_6

    iget-object v1, p0, Ld/e;->l:Landroidx/appcompat/widget/v;

    invoke-interface {v1, v2}, Landroidx/appcompat/widget/v;->k(I)V

    :cond_6
    iget-boolean v1, p0, Ld/e;->y:Z

    if-eqz v1, :cond_7

    iget-object v1, p0, Ld/e;->l:Landroidx/appcompat/widget/v;

    const/4 v2, 0x2

    invoke-interface {v1, v2}, Landroidx/appcompat/widget/v;->k(I)V

    :cond_7
    iget-boolean v1, p0, Ld/e;->z:Z

    if-eqz v1, :cond_b

    iget-object v1, p0, Ld/e;->l:Landroidx/appcompat/widget/v;

    const/4 v2, 0x5

    invoke-interface {v1, v2}, Landroidx/appcompat/widget/v;->k(I)V

    goto :goto_3

    :cond_8
    move-object v0, v5

    goto :goto_3

    :cond_9
    iget-boolean v1, p0, Ld/e;->C:Z

    if-eqz v1, :cond_a

    sget v1, Lc/g;->o:I

    goto :goto_2

    :cond_a
    sget v1, Lc/g;->n:I

    :goto_2
    invoke-virtual {v0, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    :cond_b
    :goto_3
    if-eqz v0, :cond_f

    new-instance v1, Ld/e$b;

    invoke-direct {v1, p0}, Ld/e$b;-><init>(Ld/e;)V

    invoke-static {v0, v1}, Landroidx/core/view/t;->u0(Landroid/view/View;Landroidx/core/view/p;)V

    iget-object v1, p0, Ld/e;->l:Landroidx/appcompat/widget/v;

    if-nez v1, :cond_c

    sget v1, Lc/f;->L:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Ld/e;->w:Landroid/widget/TextView;

    :cond_c
    invoke-static {v0}, Landroidx/appcompat/widget/s0;->c(Landroid/view/View;)V

    sget v1, Lc/f;->b:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroidx/appcompat/widget/ContentFrameLayout;

    iget-object v2, p0, Ld/e;->f:Landroid/view/Window;

    const v4, 0x1020002

    invoke-virtual {v2, v4}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    if-eqz v2, :cond_e

    :goto_4
    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v6

    if-lez v6, :cond_d

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->removeViewAt(I)V

    invoke-virtual {v1, v6}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    goto :goto_4

    :cond_d
    const/4 v3, -0x1

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->setId(I)V

    invoke-virtual {v1, v4}, Landroid/widget/FrameLayout;->setId(I)V

    instance-of v3, v2, Landroid/widget/FrameLayout;

    if-eqz v3, :cond_e

    check-cast v2, Landroid/widget/FrameLayout;

    invoke-virtual {v2, v5}, Landroid/widget/FrameLayout;->setForeground(Landroid/graphics/drawable/Drawable;)V

    :cond_e
    iget-object v2, p0, Ld/e;->f:Landroid/view/Window;

    invoke-virtual {v2, v0}, Landroid/view/Window;->setContentView(Landroid/view/View;)V

    new-instance v2, Ld/e$c;

    invoke-direct {v2, p0}, Ld/e$c;-><init>(Ld/e;)V

    invoke-virtual {v1, v2}, Landroidx/appcompat/widget/ContentFrameLayout;->setAttachListener(Landroidx/appcompat/widget/ContentFrameLayout$a;)V

    return-object v0

    :cond_f
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "AppCompat does not support the current theme features: { windowActionBar: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Ld/e;->A:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v2, ", windowActionBarOverlay: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Ld/e;->B:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v2, ", android:windowIsFloating: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Ld/e;->D:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v2, ", windowActionModeOverlay: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Ld/e;->C:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v2, ", windowNoTitle: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Ld/e;->E:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v2, " }"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_10
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "You need to use a Theme.AppCompat theme (or descendant) with this activity."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private W()V
    .locals 2

    iget-boolean v0, p0, Ld/e;->u:Z

    if-nez v0, :cond_4

    invoke-direct {p0}, Ld/e;->Q()Landroid/view/ViewGroup;

    move-result-object v0

    iput-object v0, p0, Ld/e;->v:Landroid/view/ViewGroup;

    invoke-virtual {p0}, Ld/e;->e0()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Ld/e;->l:Landroidx/appcompat/widget/v;

    if-eqz v1, :cond_0

    invoke-interface {v1, v0}, Landroidx/appcompat/widget/v;->setWindowTitle(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Ld/e;->y0()Ld/a;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Ld/e;->y0()Ld/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Ld/a;->w(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Ld/e;->w:Landroid/widget/TextView;

    if-eqz v1, :cond_2

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    :goto_0
    invoke-direct {p0}, Ld/e;->H()V

    iget-object v0, p0, Ld/e;->v:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0}, Ld/e;->w0(Landroid/view/ViewGroup;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Ld/e;->u:Z

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v0}, Ld/e;->d0(IZ)Ld/e$q;

    move-result-object v0

    iget-boolean v1, p0, Ld/e;->M:Z

    if-nez v1, :cond_4

    if-eqz v0, :cond_3

    iget-object v0, v0, Ld/e$q;->j:Landroidx/appcompat/view/menu/e;

    if-nez v0, :cond_4

    :cond_3
    const/16 v0, 0x6c

    invoke-direct {p0, v0}, Ld/e;->k0(I)V

    :cond_4
    return-void
.end method

.method private X()V
    .locals 2

    iget-object v0, p0, Ld/e;->f:Landroid/view/Window;

    if-nez v0, :cond_0

    iget-object v0, p0, Ld/e;->d:Ljava/lang/Object;

    instance-of v1, v0, Landroid/app/Activity;

    if-eqz v1, :cond_0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-direct {p0, v0}, Ld/e;->I(Landroid/view/Window;)V

    :cond_0
    iget-object v0, p0, Ld/e;->f:Landroid/view/Window;

    if-eqz v0, :cond_1

    return-void

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "We have not been given a Window"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static Z(Landroid/content/res/Configuration;Landroid/content/res/Configuration;)Landroid/content/res/Configuration;
    .locals 4

    new-instance v0, Landroid/content/res/Configuration;

    invoke-direct {v0}, Landroid/content/res/Configuration;-><init>()V

    const/4 v1, 0x0

    iput v1, v0, Landroid/content/res/Configuration;->fontScale:F

    if-eqz p1, :cond_13

    invoke-virtual {p0, p1}, Landroid/content/res/Configuration;->diff(Landroid/content/res/Configuration;)I

    move-result v1

    if-nez v1, :cond_0

    goto/16 :goto_0

    :cond_0
    iget v1, p0, Landroid/content/res/Configuration;->fontScale:F

    iget v2, p1, Landroid/content/res/Configuration;->fontScale:F

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_1

    iput v2, v0, Landroid/content/res/Configuration;->fontScale:F

    :cond_1
    iget v1, p0, Landroid/content/res/Configuration;->mcc:I

    iget v2, p1, Landroid/content/res/Configuration;->mcc:I

    if-eq v1, v2, :cond_2

    iput v2, v0, Landroid/content/res/Configuration;->mcc:I

    :cond_2
    iget v1, p0, Landroid/content/res/Configuration;->mnc:I

    iget v2, p1, Landroid/content/res/Configuration;->mnc:I

    if-eq v1, v2, :cond_3

    iput v2, v0, Landroid/content/res/Configuration;->mnc:I

    :cond_3
    invoke-static {p0, p1, v0}, Ld/e$m;->a(Landroid/content/res/Configuration;Landroid/content/res/Configuration;Landroid/content/res/Configuration;)V

    iget v1, p0, Landroid/content/res/Configuration;->touchscreen:I

    iget v2, p1, Landroid/content/res/Configuration;->touchscreen:I

    if-eq v1, v2, :cond_4

    iput v2, v0, Landroid/content/res/Configuration;->touchscreen:I

    :cond_4
    iget v1, p0, Landroid/content/res/Configuration;->keyboard:I

    iget v2, p1, Landroid/content/res/Configuration;->keyboard:I

    if-eq v1, v2, :cond_5

    iput v2, v0, Landroid/content/res/Configuration;->keyboard:I

    :cond_5
    iget v1, p0, Landroid/content/res/Configuration;->keyboardHidden:I

    iget v2, p1, Landroid/content/res/Configuration;->keyboardHidden:I

    if-eq v1, v2, :cond_6

    iput v2, v0, Landroid/content/res/Configuration;->keyboardHidden:I

    :cond_6
    iget v1, p0, Landroid/content/res/Configuration;->navigation:I

    iget v2, p1, Landroid/content/res/Configuration;->navigation:I

    if-eq v1, v2, :cond_7

    iput v2, v0, Landroid/content/res/Configuration;->navigation:I

    :cond_7
    iget v1, p0, Landroid/content/res/Configuration;->navigationHidden:I

    iget v2, p1, Landroid/content/res/Configuration;->navigationHidden:I

    if-eq v1, v2, :cond_8

    iput v2, v0, Landroid/content/res/Configuration;->navigationHidden:I

    :cond_8
    iget v1, p0, Landroid/content/res/Configuration;->orientation:I

    iget v2, p1, Landroid/content/res/Configuration;->orientation:I

    if-eq v1, v2, :cond_9

    iput v2, v0, Landroid/content/res/Configuration;->orientation:I

    :cond_9
    iget v1, p0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v1, v1, 0xf

    iget v2, p1, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v3, v2, 0xf

    if-eq v1, v3, :cond_a

    iget v1, v0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v2, v2, 0xf

    or-int/2addr v1, v2

    iput v1, v0, Landroid/content/res/Configuration;->screenLayout:I

    :cond_a
    iget v1, p0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit16 v1, v1, 0xc0

    iget v2, p1, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit16 v3, v2, 0xc0

    if-eq v1, v3, :cond_b

    iget v1, v0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit16 v2, v2, 0xc0

    or-int/2addr v1, v2

    iput v1, v0, Landroid/content/res/Configuration;->screenLayout:I

    :cond_b
    iget v1, p0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v1, v1, 0x30

    iget v2, p1, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v3, v2, 0x30

    if-eq v1, v3, :cond_c

    iget v1, v0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v2, v2, 0x30

    or-int/2addr v1, v2

    iput v1, v0, Landroid/content/res/Configuration;->screenLayout:I

    :cond_c
    iget v1, p0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit16 v1, v1, 0x300

    iget v2, p1, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit16 v3, v2, 0x300

    if-eq v1, v3, :cond_d

    iget v1, v0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit16 v2, v2, 0x300

    or-int/2addr v1, v2

    iput v1, v0, Landroid/content/res/Configuration;->screenLayout:I

    :cond_d
    invoke-static {p0, p1, v0}, Ld/e$n;->a(Landroid/content/res/Configuration;Landroid/content/res/Configuration;Landroid/content/res/Configuration;)V

    iget v1, p0, Landroid/content/res/Configuration;->uiMode:I

    and-int/lit8 v1, v1, 0xf

    iget v2, p1, Landroid/content/res/Configuration;->uiMode:I

    and-int/lit8 v3, v2, 0xf

    if-eq v1, v3, :cond_e

    iget v1, v0, Landroid/content/res/Configuration;->uiMode:I

    and-int/lit8 v2, v2, 0xf

    or-int/2addr v1, v2

    iput v1, v0, Landroid/content/res/Configuration;->uiMode:I

    :cond_e
    iget v1, p0, Landroid/content/res/Configuration;->uiMode:I

    and-int/lit8 v1, v1, 0x30

    iget v2, p1, Landroid/content/res/Configuration;->uiMode:I

    and-int/lit8 v3, v2, 0x30

    if-eq v1, v3, :cond_f

    iget v1, v0, Landroid/content/res/Configuration;->uiMode:I

    and-int/lit8 v2, v2, 0x30

    or-int/2addr v1, v2

    iput v1, v0, Landroid/content/res/Configuration;->uiMode:I

    :cond_f
    iget v1, p0, Landroid/content/res/Configuration;->screenWidthDp:I

    iget v2, p1, Landroid/content/res/Configuration;->screenWidthDp:I

    if-eq v1, v2, :cond_10

    iput v2, v0, Landroid/content/res/Configuration;->screenWidthDp:I

    :cond_10
    iget v1, p0, Landroid/content/res/Configuration;->screenHeightDp:I

    iget v2, p1, Landroid/content/res/Configuration;->screenHeightDp:I

    if-eq v1, v2, :cond_11

    iput v2, v0, Landroid/content/res/Configuration;->screenHeightDp:I

    :cond_11
    iget v1, p0, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    iget v2, p1, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    if-eq v1, v2, :cond_12

    iput v2, v0, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    :cond_12
    invoke-static {p0, p1, v0}, Ld/e$l;->a(Landroid/content/res/Configuration;Landroid/content/res/Configuration;Landroid/content/res/Configuration;)V

    :cond_13
    :goto_0
    return-object v0
.end method

.method private b0(Landroid/content/Context;)Ld/e$j;
    .locals 1

    iget-object v0, p0, Ld/e;->S:Ld/e$j;

    if-nez v0, :cond_0

    new-instance v0, Ld/e$i;

    invoke-direct {v0, p0, p1}, Ld/e$i;-><init>(Ld/e;Landroid/content/Context;)V

    iput-object v0, p0, Ld/e;->S:Ld/e$j;

    :cond_0
    iget-object p1, p0, Ld/e;->S:Ld/e$j;

    return-object p1
.end method

.method private c0(Landroid/content/Context;)Ld/e$j;
    .locals 1

    iget-object v0, p0, Ld/e;->R:Ld/e$j;

    if-nez v0, :cond_0

    new-instance v0, Ld/e$k;

    invoke-static {p1}, Ld/i;->a(Landroid/content/Context;)Ld/i;

    move-result-object p1

    invoke-direct {v0, p0, p1}, Ld/e$k;-><init>(Ld/e;Ld/i;)V

    iput-object v0, p0, Ld/e;->R:Ld/e$j;

    :cond_0
    iget-object p1, p0, Ld/e;->R:Ld/e$j;

    return-object p1
.end method

.method private g0()V
    .locals 3

    invoke-direct {p0}, Ld/e;->W()V

    iget-boolean v0, p0, Ld/e;->A:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Ld/e;->i:Ld/a;

    if-eqz v0, :cond_0

    goto :goto_2

    :cond_0
    iget-object v0, p0, Ld/e;->d:Ljava/lang/Object;

    instance-of v1, v0, Landroid/app/Activity;

    if-eqz v1, :cond_1

    new-instance v0, Ld/j;

    iget-object v1, p0, Ld/e;->d:Ljava/lang/Object;

    check-cast v1, Landroid/app/Activity;

    iget-boolean v2, p0, Ld/e;->B:Z

    invoke-direct {v0, v1, v2}, Ld/j;-><init>(Landroid/app/Activity;Z)V

    :goto_0
    iput-object v0, p0, Ld/e;->i:Ld/a;

    goto :goto_1

    :cond_1
    instance-of v0, v0, Landroid/app/Dialog;

    if-eqz v0, :cond_2

    new-instance v0, Ld/j;

    iget-object v1, p0, Ld/e;->d:Ljava/lang/Object;

    check-cast v1, Landroid/app/Dialog;

    invoke-direct {v0, v1}, Ld/j;-><init>(Landroid/app/Dialog;)V

    goto :goto_0

    :cond_2
    :goto_1
    iget-object v0, p0, Ld/e;->i:Ld/a;

    if-eqz v0, :cond_3

    iget-boolean v1, p0, Ld/e;->W:Z

    invoke-virtual {v0, v1}, Ld/a;->t(Z)V

    :cond_3
    :goto_2
    return-void
.end method

.method private h0(Ld/e$q;)Z
    .locals 3

    iget-object v0, p1, Ld/e$q;->i:Landroid/view/View;

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    iput-object v0, p1, Ld/e$q;->h:Landroid/view/View;

    return v1

    :cond_0
    iget-object v0, p1, Ld/e$q;->j:Landroidx/appcompat/view/menu/e;

    const/4 v2, 0x0

    if-nez v0, :cond_1

    return v2

    :cond_1
    iget-object v0, p0, Ld/e;->n:Ld/e$r;

    if-nez v0, :cond_2

    new-instance v0, Ld/e$r;

    invoke-direct {v0, p0}, Ld/e$r;-><init>(Ld/e;)V

    iput-object v0, p0, Ld/e;->n:Ld/e$r;

    :cond_2
    iget-object v0, p0, Ld/e;->n:Ld/e$r;

    invoke-virtual {p1, v0}, Ld/e$q;->a(Landroidx/appcompat/view/menu/j$a;)Landroidx/appcompat/view/menu/k;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p1, Ld/e$q;->h:Landroid/view/View;

    if-eqz v0, :cond_3

    goto :goto_0

    :cond_3
    move v1, v2

    :goto_0
    return v1
.end method

.method private i0(Ld/e$q;)Z
    .locals 2

    invoke-virtual {p0}, Ld/e;->a0()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1, v0}, Ld/e$q;->d(Landroid/content/Context;)V

    new-instance v0, Ld/e$p;

    iget-object v1, p1, Ld/e$q;->l:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Ld/e$p;-><init>(Ld/e;Landroid/content/Context;)V

    iput-object v0, p1, Ld/e$q;->g:Landroid/view/ViewGroup;

    const/16 v0, 0x51

    iput v0, p1, Ld/e$q;->c:I

    const/4 p1, 0x1

    return p1
.end method

.method private j0(Ld/e$q;)Z
    .locals 6

    iget-object v0, p0, Ld/e;->e:Landroid/content/Context;

    iget v1, p1, Ld/e$q;->a:I

    const/4 v2, 0x1

    if-eqz v1, :cond_0

    const/16 v3, 0x6c

    if-ne v1, v3, :cond_4

    :cond_0
    iget-object v1, p0, Ld/e;->l:Landroidx/appcompat/widget/v;

    if-eqz v1, :cond_4

    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v3

    sget v4, Lc/a;->f:I

    invoke-virtual {v3, v4, v1, v2}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    const/4 v4, 0x0

    iget v5, v1, Landroid/util/TypedValue;->resourceId:I

    if-eqz v5, :cond_1

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->newTheme()Landroid/content/res/Resources$Theme;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/content/res/Resources$Theme;->setTo(Landroid/content/res/Resources$Theme;)V

    iget v5, v1, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v4, v5, v2}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    sget v5, Lc/a;->g:I

    invoke-virtual {v4, v5, v1, v2}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    goto :goto_0

    :cond_1
    sget v5, Lc/a;->g:I

    invoke-virtual {v3, v5, v1, v2}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    :goto_0
    iget v5, v1, Landroid/util/TypedValue;->resourceId:I

    if-eqz v5, :cond_3

    if-nez v4, :cond_2

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->newTheme()Landroid/content/res/Resources$Theme;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/content/res/Resources$Theme;->setTo(Landroid/content/res/Resources$Theme;)V

    :cond_2
    iget v1, v1, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v4, v1, v2}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    :cond_3
    if-eqz v4, :cond_4

    new-instance v1, Li/d;

    const/4 v3, 0x0

    invoke-direct {v1, v0, v3}, Li/d;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/res/Resources$Theme;->setTo(Landroid/content/res/Resources$Theme;)V

    move-object v0, v1

    :cond_4
    new-instance v1, Landroidx/appcompat/view/menu/e;

    invoke-direct {v1, v0}, Landroidx/appcompat/view/menu/e;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, p0}, Landroidx/appcompat/view/menu/e;->R(Landroidx/appcompat/view/menu/e$a;)V

    invoke-virtual {p1, v1}, Ld/e$q;->c(Landroidx/appcompat/view/menu/e;)V

    return v2
.end method

.method private k0(I)V
    .locals 2

    iget v0, p0, Ld/e;->U:I

    const/4 v1, 0x1

    shl-int p1, v1, p1

    or-int/2addr p1, v0

    iput p1, p0, Ld/e;->U:I

    iget-boolean p1, p0, Ld/e;->T:Z

    if-nez p1, :cond_0

    iget-object p1, p0, Ld/e;->f:Landroid/view/Window;

    invoke-virtual {p1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object p1

    iget-object v0, p0, Ld/e;->V:Ljava/lang/Runnable;

    invoke-static {p1, v0}, Landroidx/core/view/t;->a0(Landroid/view/View;Ljava/lang/Runnable;)V

    iput-boolean v1, p0, Ld/e;->T:Z

    :cond_0
    return-void
.end method

.method private l0()Z
    .locals 7

    iget-boolean v0, p0, Ld/e;->Q:Z

    const/4 v1, 0x1

    if-nez v0, :cond_3

    iget-object v0, p0, Ld/e;->d:Ljava/lang/Object;

    instance-of v0, v0, Landroid/app/Activity;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ld/e;->e:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/4 v2, 0x0

    if-nez v0, :cond_0

    return v2

    :cond_0
    :try_start_0
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x1d

    if-lt v3, v4, :cond_1

    const/high16 v3, 0x100c0000

    goto :goto_0

    :cond_1
    const/high16 v3, 0xc0000

    :goto_0
    new-instance v4, Landroid/content/ComponentName;

    iget-object v5, p0, Ld/e;->e:Landroid/content/Context;

    iget-object v6, p0, Ld/e;->d:Ljava/lang/Object;

    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v4, v3}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    move-result-object v0

    if-eqz v0, :cond_2

    iget v0, v0, Landroid/content/pm/ActivityInfo;->configChanges:I

    and-int/lit16 v0, v0, 0x200

    if-eqz v0, :cond_2

    move v0, v1

    goto :goto_1

    :cond_2
    move v0, v2

    :goto_1
    iput-boolean v0, p0, Ld/e;->P:Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v0

    const-string v3, "AppCompatDelegate"

    const-string v4, "Exception while getting ActivityInfo"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    iput-boolean v2, p0, Ld/e;->P:Z

    :cond_3
    :goto_2
    iput-boolean v1, p0, Ld/e;->Q:Z

    iget-boolean v0, p0, Ld/e;->P:Z

    return v0
.end method

.method private q0(ILandroid/view/KeyEvent;)Z
    .locals 1

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Ld/e;->d0(IZ)Ld/e$q;

    move-result-object p1

    iget-boolean v0, p1, Ld/e$q;->o:Z

    if-nez v0, :cond_0

    invoke-direct {p0, p1, p2}, Ld/e;->A0(Ld/e$q;Landroid/view/KeyEvent;)Z

    move-result p1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method private t0(ILandroid/view/KeyEvent;)Z
    .locals 4

    iget-object v0, p0, Ld/e;->o:Li/b;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Ld/e;->d0(IZ)Ld/e$q;

    move-result-object v2

    if-nez p1, :cond_2

    iget-object p1, p0, Ld/e;->l:Landroidx/appcompat/widget/v;

    if-eqz p1, :cond_2

    invoke-interface {p1}, Landroidx/appcompat/widget/v;->h()Z

    move-result p1

    if-eqz p1, :cond_2

    iget-object p1, p0, Ld/e;->e:Landroid/content/Context;

    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/ViewConfiguration;->hasPermanentMenuKey()Z

    move-result p1

    if-nez p1, :cond_2

    iget-object p1, p0, Ld/e;->l:Landroidx/appcompat/widget/v;

    invoke-interface {p1}, Landroidx/appcompat/widget/v;->b()Z

    move-result p1

    if-nez p1, :cond_1

    iget-boolean p1, p0, Ld/e;->M:Z

    if-nez p1, :cond_5

    invoke-direct {p0, v2, p2}, Ld/e;->A0(Ld/e$q;Landroid/view/KeyEvent;)Z

    move-result p1

    if-eqz p1, :cond_5

    iget-object p1, p0, Ld/e;->l:Landroidx/appcompat/widget/v;

    invoke-interface {p1}, Landroidx/appcompat/widget/v;->g()Z

    move-result v0

    goto :goto_2

    :cond_1
    iget-object p1, p0, Ld/e;->l:Landroidx/appcompat/widget/v;

    invoke-interface {p1}, Landroidx/appcompat/widget/v;->f()Z

    move-result v0

    goto :goto_2

    :cond_2
    iget-boolean p1, v2, Ld/e$q;->o:Z

    if-nez p1, :cond_6

    iget-boolean v3, v2, Ld/e$q;->n:Z

    if-eqz v3, :cond_3

    goto :goto_1

    :cond_3
    iget-boolean p1, v2, Ld/e$q;->m:Z

    if-eqz p1, :cond_5

    iget-boolean p1, v2, Ld/e$q;->r:Z

    if-eqz p1, :cond_4

    iput-boolean v1, v2, Ld/e$q;->m:Z

    invoke-direct {p0, v2, p2}, Ld/e;->A0(Ld/e$q;Landroid/view/KeyEvent;)Z

    move-result p1

    goto :goto_0

    :cond_4
    move p1, v0

    :goto_0
    if-eqz p1, :cond_5

    invoke-direct {p0, v2, p2}, Ld/e;->x0(Ld/e$q;Landroid/view/KeyEvent;)V

    goto :goto_2

    :cond_5
    move v0, v1

    goto :goto_2

    :cond_6
    :goto_1
    invoke-virtual {p0, v2, v0}, Ld/e;->O(Ld/e$q;Z)V

    move v0, p1

    :goto_2
    if-eqz v0, :cond_8

    iget-object p1, p0, Ld/e;->e:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const-string p2, "audio"

    invoke-virtual {p1, p2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/media/AudioManager;

    if-eqz p1, :cond_7

    invoke-virtual {p1, v1}, Landroid/media/AudioManager;->playSoundEffect(I)V

    goto :goto_3

    :cond_7
    const-string p1, "AppCompatDelegate"

    const-string p2, "Couldn\'t get audio manager"

    invoke-static {p1, p2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    :goto_3
    return v0
.end method

.method private x0(Ld/e$q;Landroid/view/KeyEvent;)V
    .locals 13

    iget-boolean v0, p1, Ld/e$q;->o:Z

    if-nez v0, :cond_10

    iget-boolean v0, p0, Ld/e;->M:Z

    if-eqz v0, :cond_0

    goto/16 :goto_4

    :cond_0
    iget v0, p1, Ld/e$q;->a:I

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-nez v0, :cond_2

    iget-object v0, p0, Ld/e;->e:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v0, v0, 0xf

    const/4 v3, 0x4

    if-ne v0, v3, :cond_1

    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v1

    :goto_0
    if-eqz v0, :cond_2

    return-void

    :cond_2
    invoke-virtual {p0}, Ld/e;->f0()Landroid/view/Window$Callback;

    move-result-object v0

    if-eqz v0, :cond_3

    iget v3, p1, Ld/e$q;->a:I

    iget-object v4, p1, Ld/e$q;->j:Landroidx/appcompat/view/menu/e;

    invoke-interface {v0, v3, v4}, Landroid/view/Window$Callback;->onMenuOpened(ILandroid/view/Menu;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p0, p1, v2}, Ld/e;->O(Ld/e$q;Z)V

    return-void

    :cond_3
    iget-object v0, p0, Ld/e;->e:Landroid/content/Context;

    const-string v3, "window"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    if-nez v0, :cond_4

    return-void

    :cond_4
    invoke-direct {p0, p1, p2}, Ld/e;->A0(Ld/e$q;Landroid/view/KeyEvent;)Z

    move-result p2

    if-nez p2, :cond_5

    return-void

    :cond_5
    iget-object p2, p1, Ld/e$q;->g:Landroid/view/ViewGroup;

    const/4 v3, -0x1

    const/4 v4, -0x2

    if-eqz p2, :cond_7

    iget-boolean v5, p1, Ld/e$q;->q:Z

    if-eqz v5, :cond_6

    goto :goto_1

    :cond_6
    iget-object p2, p1, Ld/e$q;->i:Landroid/view/View;

    if-eqz p2, :cond_e

    invoke-virtual {p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p2

    if-eqz p2, :cond_e

    iget p2, p2, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-ne p2, v3, :cond_e

    move v6, v3

    goto :goto_2

    :cond_7
    :goto_1
    if-nez p2, :cond_9

    invoke-direct {p0, p1}, Ld/e;->i0(Ld/e$q;)Z

    move-result p2

    if-eqz p2, :cond_8

    iget-object p2, p1, Ld/e$q;->g:Landroid/view/ViewGroup;

    if-nez p2, :cond_a

    :cond_8
    return-void

    :cond_9
    iget-boolean v3, p1, Ld/e$q;->q:Z

    if-eqz v3, :cond_a

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result p2

    if-lez p2, :cond_a

    iget-object p2, p1, Ld/e$q;->g:Landroid/view/ViewGroup;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->removeAllViews()V

    :cond_a
    invoke-direct {p0, p1}, Ld/e;->h0(Ld/e$q;)Z

    move-result p2

    if-eqz p2, :cond_f

    invoke-virtual {p1}, Ld/e$q;->b()Z

    move-result p2

    if-nez p2, :cond_b

    goto :goto_3

    :cond_b
    iget-object p2, p1, Ld/e$q;->h:Landroid/view/View;

    invoke-virtual {p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p2

    if-nez p2, :cond_c

    new-instance p2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {p2, v4, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    :cond_c
    iget v3, p1, Ld/e$q;->b:I

    iget-object v5, p1, Ld/e$q;->g:Landroid/view/ViewGroup;

    invoke-virtual {v5, v3}, Landroid/view/ViewGroup;->setBackgroundResource(I)V

    iget-object v3, p1, Ld/e$q;->h:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    instance-of v5, v3, Landroid/view/ViewGroup;

    if-eqz v5, :cond_d

    check-cast v3, Landroid/view/ViewGroup;

    iget-object v5, p1, Ld/e$q;->h:Landroid/view/View;

    invoke-virtual {v3, v5}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_d
    iget-object v3, p1, Ld/e$q;->g:Landroid/view/ViewGroup;

    iget-object v5, p1, Ld/e$q;->h:Landroid/view/View;

    invoke-virtual {v3, v5, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object p2, p1, Ld/e$q;->h:Landroid/view/View;

    invoke-virtual {p2}, Landroid/view/View;->hasFocus()Z

    move-result p2

    if-nez p2, :cond_e

    iget-object p2, p1, Ld/e$q;->h:Landroid/view/View;

    invoke-virtual {p2}, Landroid/view/View;->requestFocus()Z

    :cond_e
    move v6, v4

    :goto_2
    iput-boolean v1, p1, Ld/e$q;->n:Z

    new-instance p2, Landroid/view/WindowManager$LayoutParams;

    const/4 v7, -0x2

    iget v8, p1, Ld/e$q;->d:I

    iget v9, p1, Ld/e$q;->e:I

    const/16 v10, 0x3ea

    const/high16 v11, 0x820000

    const/4 v12, -0x3

    move-object v5, p2

    invoke-direct/range {v5 .. v12}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIIIII)V

    iget v1, p1, Ld/e$q;->c:I

    iput v1, p2, Landroid/view/WindowManager$LayoutParams;->gravity:I

    iget v1, p1, Ld/e$q;->f:I

    iput v1, p2, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    iget-object v1, p1, Ld/e$q;->g:Landroid/view/ViewGroup;

    invoke-interface {v0, v1, p2}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iput-boolean v2, p1, Ld/e$q;->o:Z

    return-void

    :cond_f
    :goto_3
    iput-boolean v2, p1, Ld/e$q;->q:Z

    :cond_10
    :goto_4
    return-void
.end method

.method private z0(Ld/e$q;ILandroid/view/KeyEvent;I)Z
    .locals 2

    invoke-virtual {p3}, Landroid/view/KeyEvent;->isSystem()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    :cond_0
    iget-boolean v0, p1, Ld/e$q;->m:Z

    if-nez v0, :cond_1

    invoke-direct {p0, p1, p3}, Ld/e;->A0(Ld/e$q;Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    iget-object v0, p1, Ld/e$q;->j:Landroidx/appcompat/view/menu/e;

    if-eqz v0, :cond_2

    invoke-virtual {v0, p2, p3, p4}, Landroidx/appcompat/view/menu/e;->performShortcut(ILandroid/view/KeyEvent;I)Z

    move-result v1

    :cond_2
    if-eqz v1, :cond_3

    const/4 p2, 0x1

    and-int/lit8 p3, p4, 0x1

    if-nez p3, :cond_3

    iget-object p3, p0, Ld/e;->l:Landroidx/appcompat/widget/v;

    if-nez p3, :cond_3

    invoke-virtual {p0, p1, p2}, Ld/e;->O(Ld/e$q;Z)V

    :cond_3
    return v1
.end method


# virtual methods
.method public A(I)V
    .locals 2

    invoke-direct {p0}, Ld/e;->W()V

    iget-object v0, p0, Ld/e;->v:Landroid/view/ViewGroup;

    const v1, 0x1020002

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    iget-object v1, p0, Ld/e;->e:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    iget-object p1, p0, Ld/e;->g:Ld/e$h;

    invoke-virtual {p1}, Li/i;->a()Landroid/view/Window$Callback;

    move-result-object p1

    invoke-interface {p1}, Landroid/view/Window$Callback;->onContentChanged()V

    return-void
.end method

.method public B(Landroid/view/View;)V
    .locals 2

    invoke-direct {p0}, Ld/e;->W()V

    iget-object v0, p0, Ld/e;->v:Landroid/view/ViewGroup;

    const v1, 0x1020002

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object p1, p0, Ld/e;->g:Ld/e$h;

    invoke-virtual {p1}, Li/i;->a()Landroid/view/Window$Callback;

    move-result-object p1

    invoke-interface {p1}, Landroid/view/Window$Callback;->onContentChanged()V

    return-void
.end method

.method public C(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 2

    invoke-direct {p0}, Ld/e;->W()V

    iget-object v0, p0, Ld/e;->v:Landroid/view/ViewGroup;

    const v1, 0x1020002

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    invoke-virtual {v0, p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object p1, p0, Ld/e;->g:Ld/e$h;

    invoke-virtual {p1}, Li/i;->a()Landroid/view/Window$Callback;

    move-result-object p1

    invoke-interface {p1}, Landroid/view/Window$Callback;->onContentChanged()V

    return-void
.end method

.method public D(I)V
    .locals 0

    iput p1, p0, Ld/e;->O:I

    return-void
.end method

.method final D0()Z
    .locals 1

    goto/32 :goto_9

    nop

    :goto_0
    if-nez v0, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_a

    nop

    :goto_1
    if-nez v0, :cond_1

    goto/32 :goto_4

    :cond_1
    goto/32 :goto_5

    nop

    :goto_2
    const/4 v0, 0x1

    goto/32 :goto_3

    nop

    :goto_3
    goto :goto_8

    :goto_4
    goto/32 :goto_7

    nop

    :goto_5
    invoke-static {v0}, Landroidx/core/view/t;->O(Landroid/view/View;)Z

    move-result v0

    goto/32 :goto_b

    nop

    :goto_6
    return v0

    :goto_7
    const/4 v0, 0x0

    :goto_8
    goto/32 :goto_6

    nop

    :goto_9
    iget-boolean v0, p0, Ld/e;->u:Z

    goto/32 :goto_0

    nop

    :goto_a
    iget-object v0, p0, Ld/e;->v:Landroid/view/ViewGroup;

    goto/32 :goto_1

    nop

    :goto_b
    if-nez v0, :cond_2

    goto/32 :goto_4

    :cond_2
    goto/32 :goto_2

    nop
.end method

.method public final E(Ljava/lang/CharSequence;)V
    .locals 1

    iput-object p1, p0, Ld/e;->k:Ljava/lang/CharSequence;

    iget-object v0, p0, Ld/e;->l:Landroidx/appcompat/widget/v;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Landroidx/appcompat/widget/v;->setWindowTitle(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Ld/e;->y0()Ld/a;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Ld/e;->y0()Ld/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Ld/a;->w(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Ld/e;->w:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    :goto_0
    return-void
.end method

.method public F()Z
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Ld/e;->G(Z)Z

    move-result v0

    return v0
.end method

.method public F0(Li/b$a;)Li/b;
    .locals 2

    if-eqz p1, :cond_3

    iget-object v0, p0, Ld/e;->o:Li/b;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Li/b;->c()V

    :cond_0
    new-instance v0, Ld/e$g;

    invoke-direct {v0, p0, p1}, Ld/e$g;-><init>(Ld/e;Li/b$a;)V

    invoke-virtual {p0}, Ld/e;->m()Ld/a;

    move-result-object p1

    if-eqz p1, :cond_1

    invoke-virtual {p1, v0}, Ld/a;->x(Li/b$a;)Li/b;

    move-result-object p1

    iput-object p1, p0, Ld/e;->o:Li/b;

    if-eqz p1, :cond_1

    iget-object v1, p0, Ld/e;->h:Ld/c;

    if-eqz v1, :cond_1

    invoke-interface {v1, p1}, Ld/c;->j(Li/b;)V

    :cond_1
    iget-object p1, p0, Ld/e;->o:Li/b;

    if-nez p1, :cond_2

    invoke-virtual {p0, v0}, Ld/e;->G0(Li/b$a;)Li/b;

    move-result-object p1

    iput-object p1, p0, Ld/e;->o:Li/b;

    :cond_2
    iget-object p1, p0, Ld/e;->o:Li/b;

    return-object p1

    :cond_3
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "ActionMode callback can not be null."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method G0(Li/b$a;)Li/b;
    .locals 7

    goto/32 :goto_15

    nop

    :goto_0
    iget v5, v0, Landroid/util/TypedValue;->resourceId:I

    goto/32 :goto_46

    nop

    :goto_1
    iput-object p1, p0, Ld/e;->s:Landroidx/core/view/y;

    goto/32 :goto_59

    nop

    :goto_2
    const/4 v2, 0x0

    goto/32 :goto_8c

    nop

    :goto_3
    invoke-direct {v0, v4, v5, p1, v3}, Li/e;-><init>(Landroid/content/Context;Landroidx/appcompat/widget/ActionBarContextView;Li/b$a;Z)V

    goto/32 :goto_a7

    nop

    :goto_4
    invoke-static {p1}, Landroidx/core/view/t;->f0(Landroid/view/View;)V

    :goto_5
    goto/32 :goto_51

    nop

    :goto_6
    invoke-virtual {v4}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v4

    goto/32 :goto_38

    nop

    :goto_7
    new-instance v0, Landroid/util/TypedValue;

    goto/32 :goto_84

    nop

    :goto_8
    iget-object v4, p0, Ld/e;->p:Landroidx/appcompat/widget/ActionBarContextView;

    goto/32 :goto_43

    nop

    :goto_9
    invoke-virtual {v6, v5}, Landroid/content/res/Resources$Theme;->setTo(Landroid/content/res/Resources$Theme;)V

    goto/32 :goto_9f

    nop

    :goto_a
    new-instance v4, Li/d;

    goto/32 :goto_13

    nop

    :goto_b
    if-nez v0, :cond_0

    goto/32 :goto_83

    :cond_0
    goto/32 :goto_82

    nop

    :goto_c
    if-nez p1, :cond_1

    goto/32 :goto_5

    :cond_1
    goto/32 :goto_1f

    nop

    :goto_d
    invoke-direct {v5, v4, v1, v6}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    goto/32 :goto_21

    nop

    :goto_e
    invoke-virtual {v5, v6, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    goto/32 :goto_f

    nop

    :goto_f
    iget v0, v0, Landroid/util/TypedValue;->data:I

    goto/32 :goto_8d

    nop

    :goto_10
    invoke-direct {v5, v4}, Landroidx/appcompat/widget/ActionBarContextView;-><init>(Landroid/content/Context;)V

    goto/32 :goto_6d

    nop

    :goto_11
    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    goto/32 :goto_9d

    nop

    :goto_12
    if-nez p1, :cond_2

    goto/32 :goto_37

    :cond_2
    goto/32 :goto_7c

    nop

    :goto_13
    iget-object v6, p0, Ld/e;->e:Landroid/content/Context;

    goto/32 :goto_45

    nop

    :goto_14
    invoke-virtual {v0}, Landroidx/appcompat/widget/ViewStubCompat;->a()Landroid/view/View;

    move-result-object v0

    goto/32 :goto_35

    nop

    :goto_15
    invoke-virtual {p0}, Ld/e;->V()V

    goto/32 :goto_54

    nop

    :goto_16
    invoke-virtual {v5, v4, v3}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    goto/32 :goto_a

    nop

    :goto_17
    if-nez v0, :cond_3

    goto/32 :goto_91

    :cond_3
    goto/32 :goto_5f

    nop

    :goto_18
    iget-object v0, p0, Ld/e;->q:Landroid/widget/PopupWindow;

    goto/32 :goto_74

    nop

    :goto_19
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->sendAccessibilityEvent(I)V

    goto/32 :goto_75

    nop

    :goto_1a
    if-eqz v6, :cond_4

    goto/32 :goto_1d

    :cond_4
    goto/32 :goto_1c

    nop

    :goto_1b
    iget-object p1, p0, Ld/e;->p:Landroidx/appcompat/widget/ActionBarContextView;

    goto/32 :goto_79

    nop

    :goto_1c
    goto/16 :goto_56

    :goto_1d
    goto/32 :goto_55

    nop

    :goto_1e
    const/4 v6, -0x1

    goto/32 :goto_29

    nop

    :goto_1f
    iget-object p1, p0, Ld/e;->p:Landroidx/appcompat/widget/ActionBarContextView;

    goto/32 :goto_80

    nop

    :goto_20
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    goto/32 :goto_53

    nop

    :goto_21
    iput-object v5, p0, Ld/e;->q:Landroid/widget/PopupWindow;

    goto/32 :goto_60

    nop

    :goto_22
    if-eqz v2, :cond_5

    goto/32 :goto_2f

    :cond_5
    :try_start_0
    invoke-interface {v0, p1}, Ld/c;->s(Li/b$a;)Li/b;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/AbstractMethodError; {:try_start_0 .. :try_end_0} :catch_0

    goto/32 :goto_2e

    nop

    :goto_23
    iget-object v4, p0, Ld/e;->p:Landroidx/appcompat/widget/ActionBarContextView;

    goto/32 :goto_6

    nop

    :goto_24
    invoke-virtual {v0}, Li/b;->k()V

    goto/32 :goto_32

    nop

    :goto_25
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    goto/32 :goto_a8

    nop

    :goto_26
    if-eqz v0, :cond_6

    goto/32 :goto_91

    :cond_6
    goto/32 :goto_30

    nop

    :goto_27
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->setAlpha(F)V

    goto/32 :goto_1b

    nop

    :goto_28
    invoke-virtual {p0}, Ld/e;->V()V

    goto/32 :goto_99

    nop

    :goto_29
    invoke-virtual {v5, v6}, Landroid/widget/PopupWindow;->setWidth(I)V

    goto/32 :goto_52

    nop

    :goto_2a
    move-object p1, v0

    :goto_2b
    goto/32 :goto_65

    nop

    :goto_2c
    invoke-virtual {p0}, Ld/e;->D0()Z

    move-result p1

    goto/32 :goto_5d

    nop

    :goto_2d
    iget-object p1, p0, Ld/e;->p:Landroidx/appcompat/widget/ActionBarContextView;

    goto/32 :goto_40

    nop

    :goto_2e
    goto/16 :goto_48

    :catch_0
    :goto_2f
    goto/32 :goto_47

    nop

    :goto_30
    iget-boolean v0, p0, Ld/e;->D:Z

    goto/32 :goto_58

    nop

    :goto_31
    iget-object p1, p0, Ld/e;->o:Li/b;

    goto/32 :goto_33

    nop

    :goto_32
    iget-object p1, p0, Ld/e;->p:Landroidx/appcompat/widget/ActionBarContextView;

    goto/32 :goto_9a

    nop

    :goto_33
    if-nez p1, :cond_7

    goto/32 :goto_83

    :cond_7
    goto/32 :goto_5a

    nop

    :goto_34
    sget v5, Lc/a;->f:I

    goto/32 :goto_66

    nop

    :goto_35
    check-cast v0, Landroidx/appcompat/widget/ActionBarContextView;

    goto/32 :goto_90

    nop

    :goto_36
    iput-object v1, p0, Ld/e;->o:Li/b;

    :goto_37
    goto/32 :goto_31

    nop

    :goto_38
    iget-object v5, p0, Ld/e;->p:Landroidx/appcompat/widget/ActionBarContextView;

    goto/32 :goto_a5

    nop

    :goto_39
    iget-object v5, p0, Ld/e;->q:Landroid/widget/PopupWindow;

    goto/32 :goto_1e

    nop

    :goto_3a
    iget v4, v0, Landroid/util/TypedValue;->resourceId:I

    goto/32 :goto_16

    nop

    :goto_3b
    invoke-direct {v0, p0}, Ld/e$e;-><init>(Ld/e;)V

    goto/32 :goto_64

    nop

    :goto_3c
    iget-object v0, p0, Ld/e;->p:Landroidx/appcompat/widget/ActionBarContextView;

    goto/32 :goto_2

    nop

    :goto_3d
    goto/16 :goto_37

    :goto_3e
    goto/32 :goto_3c

    nop

    :goto_3f
    invoke-virtual {v5, v4}, Landroid/content/res/Resources$Theme;->setTo(Landroid/content/res/Resources$Theme;)V

    goto/32 :goto_3a

    nop

    :goto_40
    const/4 v1, 0x0

    goto/32 :goto_57

    nop

    :goto_41
    invoke-virtual {v0, v4}, Landroid/widget/PopupWindow;->setHeight(I)V

    goto/32 :goto_a3

    nop

    :goto_42
    invoke-direct {v0, p0}, Ld/e$d;-><init>(Ld/e;)V

    goto/32 :goto_5c

    nop

    :goto_43
    invoke-virtual {v4, v0}, Landroidx/appcompat/widget/ActionBarContextView;->setContentHeight(I)V

    goto/32 :goto_18

    nop

    :goto_44
    invoke-virtual {p1, v0}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    goto/32 :goto_6b

    nop

    :goto_45
    invoke-direct {v4, v6, v2}, Li/d;-><init>(Landroid/content/Context;I)V

    goto/32 :goto_7e

    nop

    :goto_46
    if-nez v5, :cond_8

    goto/32 :goto_a0

    :cond_8
    goto/32 :goto_85

    nop

    :goto_47
    move-object v0, v1

    :goto_48
    goto/32 :goto_5e

    nop

    :goto_49
    invoke-static {p1}, Landroidx/core/view/t;->d(Landroid/view/View;)Landroidx/core/view/y;

    move-result-object p1

    goto/32 :goto_63

    nop

    :goto_4a
    invoke-virtual {v0}, Landroidx/appcompat/widget/ActionBarContextView;->k()V

    goto/32 :goto_6e

    nop

    :goto_4b
    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    goto/32 :goto_8a

    nop

    :goto_4c
    if-nez p1, :cond_9

    goto/32 :goto_96

    :cond_9
    goto/32 :goto_2d

    nop

    :goto_4d
    iget-object v4, p0, Ld/e;->e:Landroid/content/Context;

    goto/32 :goto_76

    nop

    :goto_4e
    iget-object v0, p0, Ld/e;->v:Landroid/view/ViewGroup;

    goto/32 :goto_68

    nop

    :goto_4f
    return-object p1

    :goto_50
    invoke-virtual {v5, v6}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    goto/32 :goto_39

    nop

    :goto_51
    iget-object p1, p0, Ld/e;->q:Landroid/widget/PopupWindow;

    goto/32 :goto_12

    nop

    :goto_52
    invoke-virtual {v4}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v5

    goto/32 :goto_73

    nop

    :goto_53
    invoke-virtual {v5}, Landroid/content/res/Resources;->newTheme()Landroid/content/res/Resources$Theme;

    move-result-object v5

    goto/32 :goto_3f

    nop

    :goto_54
    iget-object v0, p0, Ld/e;->o:Li/b;

    goto/32 :goto_5b

    nop

    :goto_55
    move v3, v2

    :goto_56
    goto/32 :goto_3

    nop

    :goto_57
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->setAlpha(F)V

    goto/32 :goto_93

    nop

    :goto_58
    if-nez v0, :cond_a

    goto/32 :goto_98

    :cond_a
    goto/32 :goto_7

    nop

    :goto_59
    new-instance v0, Ld/e$e;

    goto/32 :goto_3b

    nop

    :goto_5a
    iget-object v0, p0, Ld/e;->h:Ld/c;

    goto/32 :goto_b

    nop

    :goto_5b
    if-nez v0, :cond_b

    goto/32 :goto_9c

    :cond_b
    goto/32 :goto_9b

    nop

    :goto_5c
    iput-object v0, p0, Ld/e;->r:Ljava/lang/Runnable;

    goto/32 :goto_97

    nop

    :goto_5d
    const/high16 v0, 0x3f800000    # 1.0f

    goto/32 :goto_4c

    nop

    :goto_5e
    if-nez v0, :cond_c

    goto/32 :goto_3e

    :cond_c
    goto/32 :goto_77

    nop

    :goto_5f
    invoke-virtual {p0}, Ld/e;->a0()Landroid/content/Context;

    move-result-object v4

    goto/32 :goto_4b

    nop

    :goto_60
    const/4 v6, 0x2

    goto/32 :goto_6a

    nop

    :goto_61
    new-instance v0, Ld/e$g;

    goto/32 :goto_a1

    nop

    :goto_62
    iget-object p1, p0, Ld/e;->p:Landroidx/appcompat/widget/ActionBarContextView;

    goto/32 :goto_8b

    nop

    :goto_63
    invoke-virtual {p1, v0}, Landroidx/core/view/y;->a(F)Landroidx/core/view/y;

    move-result-object p1

    goto/32 :goto_1

    nop

    :goto_64
    invoke-virtual {p1, v0}, Landroidx/core/view/y;->f(Landroidx/core/view/z;)Landroidx/core/view/y;

    goto/32 :goto_95

    nop

    :goto_65
    iget-object v0, p0, Ld/e;->h:Ld/c;

    goto/32 :goto_8f

    nop

    :goto_66
    invoke-virtual {v4, v5, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    goto/32 :goto_0

    nop

    :goto_67
    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    goto/32 :goto_7d

    nop

    :goto_68
    sget v4, Lc/f;->h:I

    goto/32 :goto_67

    nop

    :goto_69
    if-nez v0, :cond_d

    goto/32 :goto_37

    :cond_d
    goto/32 :goto_28

    nop

    :goto_6a
    invoke-static {v5, v6}, Landroidx/core/widget/h;->b(Landroid/widget/PopupWindow;I)V

    goto/32 :goto_89

    nop

    :goto_6b
    goto/16 :goto_37

    :goto_6c
    goto/32 :goto_36

    nop

    :goto_6d
    iput-object v5, p0, Ld/e;->p:Landroidx/appcompat/widget/ActionBarContextView;

    goto/32 :goto_7f

    nop

    :goto_6e
    new-instance v0, Li/e;

    goto/32 :goto_23

    nop

    :goto_6f
    iget-object v6, p0, Ld/e;->p:Landroidx/appcompat/widget/ActionBarContextView;

    goto/32 :goto_50

    nop

    :goto_70
    if-nez p1, :cond_e

    goto/32 :goto_6c

    :cond_e
    goto/32 :goto_24

    nop

    :goto_71
    iput-object v0, p0, Ld/e;->o:Li/b;

    goto/32 :goto_2c

    nop

    :goto_72
    check-cast p1, Landroid/view/View;

    goto/32 :goto_4

    nop

    :goto_73
    sget v6, Lc/a;->b:I

    goto/32 :goto_e

    nop

    :goto_74
    const/4 v4, -0x2

    goto/32 :goto_41

    nop

    :goto_75
    iget-object p1, p0, Ld/e;->p:Landroidx/appcompat/widget/ActionBarContextView;

    goto/32 :goto_25

    nop

    :goto_76
    invoke-virtual {v4}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v4

    goto/32 :goto_34

    nop

    :goto_77
    iput-object v0, p0, Ld/e;->o:Li/b;

    goto/32 :goto_3d

    nop

    :goto_78
    invoke-interface {p1, v0, v3}, Li/b$a;->d(Li/b;Landroid/view/Menu;)Z

    move-result p1

    goto/32 :goto_70

    nop

    :goto_79
    invoke-virtual {p1, v2}, Landroidx/appcompat/widget/ActionBarContextView;->setVisibility(I)V

    goto/32 :goto_62

    nop

    :goto_7a
    iget-object v4, p0, Ld/e;->e:Landroid/content/Context;

    :goto_7b
    goto/32 :goto_9e

    nop

    :goto_7c
    iget-object p1, p0, Ld/e;->f:Landroid/view/Window;

    goto/32 :goto_a2

    nop

    :goto_7d
    check-cast v0, Landroidx/appcompat/widget/ViewStubCompat;

    goto/32 :goto_17

    nop

    :goto_7e
    invoke-virtual {v4}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v6

    goto/32 :goto_9

    nop

    :goto_7f
    new-instance v5, Landroid/widget/PopupWindow;

    goto/32 :goto_88

    nop

    :goto_80
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    goto/32 :goto_72

    nop

    :goto_81
    iget-boolean v2, p0, Ld/e;->M:Z

    goto/32 :goto_22

    nop

    :goto_82
    invoke-interface {v0, p1}, Ld/c;->j(Li/b;)V

    :goto_83
    goto/32 :goto_92

    nop

    :goto_84
    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    goto/32 :goto_4d

    nop

    :goto_85
    iget-object v5, p0, Ld/e;->e:Landroid/content/Context;

    goto/32 :goto_20

    nop

    :goto_86
    iget-object v0, p0, Ld/e;->p:Landroidx/appcompat/widget/ActionBarContextView;

    goto/32 :goto_69

    nop

    :goto_87
    iget-object p1, p0, Ld/e;->p:Landroidx/appcompat/widget/ActionBarContextView;

    goto/32 :goto_27

    nop

    :goto_88
    sget v6, Lc/a;->i:I

    goto/32 :goto_d

    nop

    :goto_89
    iget-object v5, p0, Ld/e;->q:Landroid/widget/PopupWindow;

    goto/32 :goto_6f

    nop

    :goto_8a
    invoke-virtual {v0, v4}, Landroidx/appcompat/widget/ViewStubCompat;->setLayoutInflater(Landroid/view/LayoutInflater;)V

    goto/32 :goto_14

    nop

    :goto_8b
    const/16 v0, 0x20

    goto/32 :goto_19

    nop

    :goto_8c
    const/4 v3, 0x1

    goto/32 :goto_26

    nop

    :goto_8d
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    goto/32 :goto_11

    nop

    :goto_8e
    if-eqz v0, :cond_f

    goto/32 :goto_2b

    :cond_f
    goto/32 :goto_61

    nop

    :goto_8f
    const/4 v1, 0x0

    goto/32 :goto_a4

    nop

    :goto_90
    iput-object v0, p0, Ld/e;->p:Landroidx/appcompat/widget/ActionBarContextView;

    :goto_91
    goto/32 :goto_86

    nop

    :goto_92
    iget-object p1, p0, Ld/e;->o:Li/b;

    goto/32 :goto_4f

    nop

    :goto_93
    iget-object p1, p0, Ld/e;->p:Landroidx/appcompat/widget/ActionBarContextView;

    goto/32 :goto_49

    nop

    :goto_94
    iget-object v0, p0, Ld/e;->r:Ljava/lang/Runnable;

    goto/32 :goto_44

    nop

    :goto_95
    goto/16 :goto_5

    :goto_96
    goto/32 :goto_87

    nop

    :goto_97
    goto :goto_91

    :goto_98
    goto/32 :goto_4e

    nop

    :goto_99
    iget-object v0, p0, Ld/e;->p:Landroidx/appcompat/widget/ActionBarContextView;

    goto/32 :goto_4a

    nop

    :goto_9a
    invoke-virtual {p1, v0}, Landroidx/appcompat/widget/ActionBarContextView;->h(Li/b;)V

    goto/32 :goto_71

    nop

    :goto_9b
    invoke-virtual {v0}, Li/b;->c()V

    :goto_9c
    goto/32 :goto_a6

    nop

    :goto_9d
    invoke-static {v0, v4}, Landroid/util/TypedValue;->complexToDimensionPixelSize(ILandroid/util/DisplayMetrics;)I

    move-result v0

    goto/32 :goto_8

    nop

    :goto_9e
    new-instance v5, Landroidx/appcompat/widget/ActionBarContextView;

    goto/32 :goto_10

    nop

    :goto_9f
    goto/16 :goto_7b

    :goto_a0
    goto/32 :goto_7a

    nop

    :goto_a1
    invoke-direct {v0, p0, p1}, Ld/e$g;-><init>(Ld/e;Li/b$a;)V

    goto/32 :goto_2a

    nop

    :goto_a2
    invoke-virtual {p1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object p1

    goto/32 :goto_94

    nop

    :goto_a3
    new-instance v0, Ld/e$d;

    goto/32 :goto_42

    nop

    :goto_a4
    if-nez v0, :cond_10

    goto/32 :goto_2f

    :cond_10
    goto/32 :goto_81

    nop

    :goto_a5
    iget-object v6, p0, Ld/e;->q:Landroid/widget/PopupWindow;

    goto/32 :goto_1a

    nop

    :goto_a6
    instance-of v0, p1, Ld/e$g;

    goto/32 :goto_8e

    nop

    :goto_a7
    invoke-virtual {v0}, Li/b;->e()Landroid/view/Menu;

    move-result-object v3

    goto/32 :goto_78

    nop

    :goto_a8
    instance-of p1, p1, Landroid/view/View;

    goto/32 :goto_c

    nop
.end method

.method K(ILd/e$q;Landroid/view/Menu;)V
    .locals 2

    goto/32 :goto_14

    nop

    :goto_0
    iget-boolean p2, p2, Ld/e$q;->o:Z

    goto/32 :goto_6

    nop

    :goto_1
    if-nez p2, :cond_0

    goto/32 :goto_a

    :cond_0
    goto/32 :goto_9

    nop

    :goto_2
    if-eqz p2, :cond_1

    goto/32 :goto_12

    :cond_1
    goto/32 :goto_10

    nop

    :goto_3
    if-nez p2, :cond_2

    goto/32 :goto_8

    :cond_2
    goto/32 :goto_0

    nop

    :goto_4
    if-eqz p2, :cond_3

    goto/32 :goto_16

    :cond_3
    goto/32 :goto_b

    nop

    :goto_5
    array-length v1, v0

    goto/32 :goto_e

    nop

    :goto_6
    if-eqz p2, :cond_4

    goto/32 :goto_8

    :cond_4
    goto/32 :goto_7

    nop

    :goto_7
    return-void

    :goto_8
    goto/32 :goto_d

    nop

    :goto_9
    iget-object p3, p2, Ld/e$q;->j:Landroidx/appcompat/view/menu/e;

    :goto_a
    goto/32 :goto_3

    nop

    :goto_b
    iget-object p2, p0, Ld/e;->g:Ld/e$h;

    goto/32 :goto_f

    nop

    :goto_c
    return-void

    :goto_d
    iget-boolean p2, p0, Ld/e;->M:Z

    goto/32 :goto_4

    nop

    :goto_e
    if-lt p1, v1, :cond_5

    goto/32 :goto_12

    :cond_5
    goto/32 :goto_11

    nop

    :goto_f
    invoke-virtual {p2}, Li/i;->a()Landroid/view/Window$Callback;

    move-result-object p2

    goto/32 :goto_15

    nop

    :goto_10
    if-gez p1, :cond_6

    goto/32 :goto_12

    :cond_6
    goto/32 :goto_13

    nop

    :goto_11
    aget-object p2, v0, p1

    :goto_12
    goto/32 :goto_1

    nop

    :goto_13
    iget-object v0, p0, Ld/e;->G:[Ld/e$q;

    goto/32 :goto_5

    nop

    :goto_14
    if-eqz p3, :cond_7

    goto/32 :goto_a

    :cond_7
    goto/32 :goto_2

    nop

    :goto_15
    invoke-interface {p2, p1, p3}, Landroid/view/Window$Callback;->onPanelClosed(ILandroid/view/Menu;)V

    :goto_16
    goto/32 :goto_c

    nop
.end method

.method L(Landroidx/appcompat/view/menu/e;)V
    .locals 2

    goto/32 :goto_f

    nop

    :goto_0
    const/4 p1, 0x0

    goto/32 :goto_a

    nop

    :goto_1
    if-nez v0, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_6

    nop

    :goto_2
    const/16 v1, 0x6c

    goto/32 :goto_4

    nop

    :goto_3
    if-eqz v1, :cond_1

    goto/32 :goto_5

    :cond_1
    goto/32 :goto_2

    nop

    :goto_4
    invoke-interface {v0, v1, p1}, Landroid/view/Window$Callback;->onPanelClosed(ILandroid/view/Menu;)V

    :goto_5
    goto/32 :goto_0

    nop

    :goto_6
    return-void

    :goto_7
    goto/32 :goto_9

    nop

    :goto_8
    invoke-virtual {p0}, Ld/e;->f0()Landroid/view/Window$Callback;

    move-result-object v0

    goto/32 :goto_10

    nop

    :goto_9
    const/4 v0, 0x1

    goto/32 :goto_c

    nop

    :goto_a
    iput-boolean p1, p0, Ld/e;->F:Z

    goto/32 :goto_11

    nop

    :goto_b
    iget-object v0, p0, Ld/e;->l:Landroidx/appcompat/widget/v;

    goto/32 :goto_d

    nop

    :goto_c
    iput-boolean v0, p0, Ld/e;->F:Z

    goto/32 :goto_b

    nop

    :goto_d
    invoke-interface {v0}, Landroidx/appcompat/widget/v;->l()V

    goto/32 :goto_8

    nop

    :goto_e
    iget-boolean v1, p0, Ld/e;->M:Z

    goto/32 :goto_3

    nop

    :goto_f
    iget-boolean v0, p0, Ld/e;->F:Z

    goto/32 :goto_1

    nop

    :goto_10
    if-nez v0, :cond_2

    goto/32 :goto_5

    :cond_2
    goto/32 :goto_e

    nop

    :goto_11
    return-void
.end method

.method final L0(Landroidx/core/view/d0;Landroid/graphics/Rect;)I
    .locals 10

    goto/32 :goto_1a

    nop

    :goto_0
    if-nez p2, :cond_0

    goto/32 :goto_54

    :cond_0
    goto/32 :goto_44

    nop

    :goto_1
    iget v4, p1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    goto/32 :goto_2c

    nop

    :goto_2
    iput v4, v2, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    goto/32 :goto_c

    nop

    :goto_3
    goto/16 :goto_9d

    :goto_4
    goto/32 :goto_9c

    nop

    :goto_5
    const/16 v8, 0x33

    goto/32 :goto_6f

    nop

    :goto_6
    iget-object v4, p0, Ld/e;->X:Landroid/graphics/Rect;

    goto/32 :goto_56

    nop

    :goto_7
    invoke-virtual {v4}, Landroid/view/ViewGroup;->isShown()Z

    move-result v4

    goto/32 :goto_30

    nop

    :goto_8
    move p2, v0

    goto/32 :goto_8b

    nop

    :goto_9
    goto/16 :goto_8c

    :goto_a
    goto/32 :goto_8

    nop

    :goto_b
    invoke-virtual {p1, v3}, Landroid/view/View;->setVisibility(I)V

    goto/32 :goto_12

    nop

    :goto_c
    move p2, v5

    :goto_d
    goto/32 :goto_7c

    nop

    :goto_e
    invoke-virtual {v4, p1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :goto_f
    goto/32 :goto_37

    nop

    :goto_10
    if-eq v8, p2, :cond_1

    goto/32 :goto_8c

    :cond_1
    goto/32 :goto_86

    nop

    :goto_11
    if-nez v5, :cond_2

    goto/32 :goto_18

    :cond_2
    goto/32 :goto_36

    nop

    :goto_12
    new-instance p1, Landroid/widget/FrameLayout$LayoutParams;

    goto/32 :goto_93

    nop

    :goto_13
    if-eqz p1, :cond_3

    goto/32 :goto_35

    :cond_3
    goto/32 :goto_6b

    nop

    :goto_14
    iget-object v4, p0, Ld/e;->X:Landroid/graphics/Rect;

    goto/32 :goto_23

    nop

    :goto_15
    iget v4, v4, Landroid/graphics/Rect;->right:I

    goto/32 :goto_51

    nop

    :goto_16
    if-eqz v6, :cond_4

    goto/32 :goto_43

    :cond_4
    goto/32 :goto_41

    nop

    :goto_17
    invoke-direct {p0, p1}, Ld/e;->M0(Landroid/view/View;)V

    :goto_18
    goto/32 :goto_21

    nop

    :goto_19
    if-eqz p1, :cond_5

    goto/32 :goto_a4

    :cond_5
    goto/32 :goto_22

    nop

    :goto_1a
    const/4 v0, 0x0

    goto/32 :goto_55

    nop

    :goto_1b
    if-nez p1, :cond_6

    goto/32 :goto_4

    :cond_6
    goto/32 :goto_3

    nop

    :goto_1c
    iget-object p1, p0, Ld/e;->x:Landroid/view/View;

    goto/32 :goto_97

    nop

    :goto_1d
    invoke-virtual {p2, v2}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/32 :goto_5f

    nop

    :goto_1e
    goto/16 :goto_3c

    :goto_1f
    goto/32 :goto_3b

    nop

    :goto_20
    move p1, v5

    goto/32 :goto_6e

    nop

    :goto_21
    iget-boolean p1, p0, Ld/e;->C:Z

    goto/32 :goto_80

    nop

    :goto_22
    new-instance p1, Landroid/view/View;

    goto/32 :goto_3e

    nop

    :goto_23
    if-eqz v4, :cond_7

    goto/32 :goto_69

    :cond_7
    goto/32 :goto_4f

    nop

    :goto_24
    invoke-direct {p1, v4}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    goto/32 :goto_27

    nop

    :goto_25
    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    goto/32 :goto_68

    nop

    :goto_26
    check-cast v2, Landroid/view/ViewGroup$MarginLayoutParams;

    goto/32 :goto_38

    nop

    :goto_27
    iput-object p1, p0, Ld/e;->x:Landroid/view/View;

    goto/32 :goto_b

    nop

    :goto_28
    goto/16 :goto_4e

    :goto_29
    goto/32 :goto_0

    nop

    :goto_2a
    if-nez v5, :cond_8

    goto/32 :goto_85

    :cond_8
    goto/32 :goto_84

    nop

    :goto_2b
    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    goto/32 :goto_4a

    nop

    :goto_2c
    if-eq v4, v7, :cond_9

    goto/32 :goto_71

    :cond_9
    goto/32 :goto_73

    nop

    :goto_2d
    if-nez v2, :cond_a

    goto/32 :goto_60

    :cond_a
    goto/32 :goto_83

    nop

    :goto_2e
    invoke-virtual {v4, p2, v7, v8, p1}, Landroid/graphics/Rect;->set(IIII)V

    :goto_2f
    goto/32 :goto_5d

    nop

    :goto_30
    const/4 v5, 0x1

    goto/32 :goto_a1

    nop

    :goto_31
    if-nez v2, :cond_b

    goto/32 :goto_60

    :cond_b
    goto/32 :goto_5c

    nop

    :goto_32
    return v1

    :goto_33
    invoke-virtual {v4, v6, v9, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    goto/32 :goto_a3

    nop

    :goto_34
    goto :goto_2f

    :goto_35
    goto/32 :goto_67

    nop

    :goto_36
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result p1

    goto/32 :goto_79

    nop

    :goto_37
    iget-object p1, p0, Ld/e;->x:Landroid/view/View;

    goto/32 :goto_59

    nop

    :goto_38
    iget-object v4, p0, Ld/e;->p:Landroidx/appcompat/widget/ActionBarContextView;

    goto/32 :goto_7

    nop

    :goto_39
    iget-object p1, p0, Ld/e;->x:Landroid/view/View;

    goto/32 :goto_17

    nop

    :goto_3a
    move p1, v0

    goto/32 :goto_9a

    nop

    :goto_3b
    move v5, v0

    :goto_3c
    goto/32 :goto_11

    nop

    :goto_3d
    iput v8, p1, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    goto/32 :goto_5a

    nop

    :goto_3e
    iget-object v4, p0, Ld/e;->e:Landroid/content/Context;

    goto/32 :goto_24

    nop

    :goto_3f
    invoke-virtual {p1}, Landroidx/core/view/d0;->j()I

    move-result v8

    goto/32 :goto_a2

    nop

    :goto_40
    if-nez v5, :cond_c

    goto/32 :goto_4c

    :cond_c
    goto/32 :goto_82

    nop

    :goto_41
    move v7, v0

    goto/32 :goto_42

    nop

    :goto_42
    goto/16 :goto_7b

    :goto_43
    goto/32 :goto_7a

    nop

    :goto_44
    iget v1, p2, Landroid/graphics/Rect;->top:I

    goto/32 :goto_53

    nop

    :goto_45
    invoke-virtual {p1}, Landroidx/core/view/d0;->k()I

    move-result v1

    goto/32 :goto_28

    nop

    :goto_46
    invoke-virtual {v6}, Landroidx/core/view/d0;->j()I

    move-result v6

    :goto_47
    goto/32 :goto_77

    nop

    :goto_48
    instance-of v2, v2, Landroid/view/ViewGroup$MarginLayoutParams;

    goto/32 :goto_31

    nop

    :goto_49
    if-eqz v6, :cond_d

    goto/32 :goto_65

    :cond_d
    goto/32 :goto_9e

    nop

    :goto_4a
    iput-object v4, p0, Ld/e;->X:Landroid/graphics/Rect;

    goto/32 :goto_52

    nop

    :goto_4b
    move p1, v0

    :goto_4c
    goto/32 :goto_91

    nop

    :goto_4d
    move v1, v0

    :goto_4e
    goto/32 :goto_81

    nop

    :goto_4f
    new-instance v4, Landroid/graphics/Rect;

    goto/32 :goto_2b

    nop

    :goto_50
    if-nez p1, :cond_e

    goto/32 :goto_9b

    :cond_e
    goto/32 :goto_78

    nop

    :goto_51
    iget-object v6, p0, Ld/e;->v:Landroid/view/ViewGroup;

    goto/32 :goto_63

    nop

    :goto_52
    new-instance v4, Landroid/graphics/Rect;

    goto/32 :goto_25

    nop

    :goto_53
    goto :goto_4e

    :goto_54
    goto/32 :goto_4d

    nop

    :goto_55
    if-nez p1, :cond_f

    goto/32 :goto_29

    :cond_f
    goto/32 :goto_45

    nop

    :goto_56
    iget-object v6, p0, Ld/e;->Y:Landroid/graphics/Rect;

    goto/32 :goto_13

    nop

    :goto_57
    goto/16 :goto_88

    :goto_58
    goto/32 :goto_90

    nop

    :goto_59
    if-nez p1, :cond_10

    goto/32 :goto_1f

    :cond_10
    goto/32 :goto_1e

    nop

    :goto_5a
    iput v7, p1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    goto/32 :goto_6c

    nop

    :goto_5b
    if-nez p2, :cond_11

    goto/32 :goto_7e

    :cond_11
    goto/32 :goto_1b

    nop

    :goto_5c
    iget-object v2, p0, Ld/e;->p:Landroidx/appcompat/widget/ActionBarContextView;

    goto/32 :goto_74

    nop

    :goto_5d
    iget-object p1, p0, Ld/e;->v:Landroid/view/ViewGroup;

    goto/32 :goto_62

    nop

    :goto_5e
    if-eq v8, p1, :cond_12

    goto/32 :goto_8c

    :cond_12
    goto/32 :goto_76

    nop

    :goto_5f
    goto :goto_4c

    :goto_60
    goto/32 :goto_4b

    nop

    :goto_61
    const/16 v3, 0x8

    goto/32 :goto_2d

    nop

    :goto_62
    invoke-static {p1, v4, v6}, Landroidx/appcompat/widget/s0;->a(Landroid/view/View;Landroid/graphics/Rect;Landroid/graphics/Rect;)V

    goto/32 :goto_75

    nop

    :goto_63
    invoke-static {v6}, Landroidx/core/view/t;->D(Landroid/view/View;)Landroidx/core/view/d0;

    move-result-object v6

    goto/32 :goto_16

    nop

    :goto_64
    goto/16 :goto_47

    :goto_65
    goto/32 :goto_46

    nop

    :goto_66
    if-eq v4, v8, :cond_13

    goto/32 :goto_71

    :cond_13
    goto/32 :goto_1

    nop

    :goto_67
    invoke-virtual {p1}, Landroidx/core/view/d0;->i()I

    move-result p2

    goto/32 :goto_8a

    nop

    :goto_68
    iput-object v4, p0, Ld/e;->Y:Landroid/graphics/Rect;

    :goto_69
    goto/32 :goto_6

    nop

    :goto_6a
    move p1, v0

    goto/32 :goto_87

    nop

    :goto_6b
    invoke-virtual {v4, p2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    goto/32 :goto_34

    nop

    :goto_6c
    iput v6, p1, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    goto/32 :goto_96

    nop

    :goto_6d
    iput p1, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    goto/32 :goto_8f

    nop

    :goto_6e
    move v5, p2

    goto/32 :goto_57

    nop

    :goto_6f
    const/4 v9, -0x1

    goto/32 :goto_8d

    nop

    :goto_70
    if-ne v4, v6, :cond_14

    goto/32 :goto_f

    :cond_14
    :goto_71
    goto/32 :goto_3d

    nop

    :goto_72
    iget-object v6, p0, Ld/e;->x:Landroid/view/View;

    goto/32 :goto_33

    nop

    :goto_73
    iget v4, p1, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    goto/32 :goto_70

    nop

    :goto_74
    invoke-virtual {v2}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    goto/32 :goto_26

    nop

    :goto_75
    iget p1, v4, Landroid/graphics/Rect;->top:I

    goto/32 :goto_94

    nop

    :goto_76
    iget v8, v2, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    goto/32 :goto_10

    nop

    :goto_77
    iget v8, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    goto/32 :goto_5e

    nop

    :goto_78
    iput v0, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    goto/32 :goto_3a

    nop

    :goto_79
    if-nez p1, :cond_15

    goto/32 :goto_18

    :cond_15
    goto/32 :goto_39

    nop

    :goto_7a
    invoke-virtual {v6}, Landroidx/core/view/d0;->i()I

    move-result v7

    :goto_7b
    goto/32 :goto_49

    nop

    :goto_7c
    if-gtz p1, :cond_16

    goto/32 :goto_a4

    :cond_16
    goto/32 :goto_89

    nop

    :goto_7d
    invoke-virtual {p2, v0}, Landroid/view/View;->setVisibility(I)V

    :goto_7e
    goto/32 :goto_32

    nop

    :goto_7f
    iput v6, p1, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    goto/32 :goto_99

    nop

    :goto_80
    if-eqz p1, :cond_17

    goto/32 :goto_85

    :cond_17
    goto/32 :goto_2a

    nop

    :goto_81
    iget-object v2, p0, Ld/e;->p:Landroidx/appcompat/widget/ActionBarContextView;

    goto/32 :goto_61

    nop

    :goto_82
    iget-object p2, p0, Ld/e;->p:Landroidx/appcompat/widget/ActionBarContextView;

    goto/32 :goto_1d

    nop

    :goto_83
    invoke-virtual {v2}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    goto/32 :goto_48

    nop

    :goto_84
    move v1, v0

    :goto_85
    goto/32 :goto_20

    nop

    :goto_86
    iget v8, v2, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    goto/32 :goto_a0

    nop

    :goto_87
    move v5, p1

    :goto_88
    goto/32 :goto_40

    nop

    :goto_89
    iget-object p1, p0, Ld/e;->x:Landroid/view/View;

    goto/32 :goto_19

    nop

    :goto_8a
    invoke-virtual {p1}, Landroidx/core/view/d0;->k()I

    move-result v7

    goto/32 :goto_3f

    nop

    :goto_8b
    goto/16 :goto_d

    :goto_8c
    goto/32 :goto_6d

    nop

    :goto_8d
    invoke-direct {p1, v9, v4, v8}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    goto/32 :goto_92

    nop

    :goto_8e
    check-cast p1, Landroid/view/ViewGroup$MarginLayoutParams;

    goto/32 :goto_95

    nop

    :goto_8f
    iput p2, v2, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    goto/32 :goto_2

    nop

    :goto_90
    iget p1, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    goto/32 :goto_50

    nop

    :goto_91
    iget-object p2, p0, Ld/e;->x:Landroid/view/View;

    goto/32 :goto_5b

    nop

    :goto_92
    iput v7, p1, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    goto/32 :goto_7f

    nop

    :goto_93
    iget v4, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    goto/32 :goto_5

    nop

    :goto_94
    iget p2, v4, Landroid/graphics/Rect;->left:I

    goto/32 :goto_15

    nop

    :goto_95
    iget v4, p1, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    goto/32 :goto_9f

    nop

    :goto_96
    iget-object v4, p0, Ld/e;->x:Landroid/view/View;

    goto/32 :goto_e

    nop

    :goto_97
    if-nez p1, :cond_18

    goto/32 :goto_f

    :cond_18
    goto/32 :goto_98

    nop

    :goto_98
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    goto/32 :goto_8e

    nop

    :goto_99
    iget-object v4, p0, Ld/e;->v:Landroid/view/ViewGroup;

    goto/32 :goto_72

    nop

    :goto_9a
    goto :goto_88

    :goto_9b
    goto/32 :goto_6a

    nop

    :goto_9c
    move v0, v3

    :goto_9d
    goto/32 :goto_7d

    nop

    :goto_9e
    move v6, v0

    goto/32 :goto_64

    nop

    :goto_9f
    iget v8, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    goto/32 :goto_66

    nop

    :goto_a0
    if-ne v8, v4, :cond_19

    goto/32 :goto_a

    :cond_19
    goto/32 :goto_9

    nop

    :goto_a1
    if-nez v4, :cond_1a

    goto/32 :goto_58

    :cond_1a
    goto/32 :goto_14

    nop

    :goto_a2
    invoke-virtual {p1}, Landroidx/core/view/d0;->h()I

    move-result p1

    goto/32 :goto_2e

    nop

    :goto_a3
    goto/16 :goto_f

    :goto_a4
    goto/32 :goto_1c

    nop
.end method

.method N(I)V
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    const/4 v0, 0x1

    goto/32 :goto_2

    nop

    :goto_1
    return-void

    :goto_2
    invoke-virtual {p0, p1, v0}, Ld/e;->d0(IZ)Ld/e$q;

    move-result-object p1

    goto/32 :goto_3

    nop

    :goto_3
    invoke-virtual {p0, p1, v0}, Ld/e;->O(Ld/e$q;Z)V

    goto/32 :goto_1

    nop
.end method

.method O(Ld/e$q;Z)V
    .locals 3

    goto/32 :goto_0

    nop

    :goto_0
    if-nez p2, :cond_0

    goto/32 :goto_22

    :cond_0
    goto/32 :goto_25

    nop

    :goto_1
    iget-object v0, p0, Ld/e;->l:Landroidx/appcompat/widget/v;

    goto/32 :goto_15

    nop

    :goto_2
    const/4 v1, 0x0

    goto/32 :goto_23

    nop

    :goto_3
    if-eqz v0, :cond_1

    goto/32 :goto_22

    :cond_1
    goto/32 :goto_1

    nop

    :goto_4
    iget-object p1, p1, Ld/e$q;->j:Landroidx/appcompat/view/menu/e;

    goto/32 :goto_1a

    nop

    :goto_5
    invoke-interface {v0}, Landroidx/appcompat/widget/v;->b()Z

    move-result v0

    goto/32 :goto_10

    nop

    :goto_6
    iput-boolean p2, p1, Ld/e$q;->n:Z

    goto/32 :goto_f

    nop

    :goto_7
    const/4 p2, 0x0

    goto/32 :goto_14

    nop

    :goto_8
    if-nez v2, :cond_2

    goto/32 :goto_19

    :cond_2
    goto/32 :goto_b

    nop

    :goto_9
    iget-boolean v2, p1, Ld/e$q;->o:Z

    goto/32 :goto_8

    nop

    :goto_a
    iget-object p2, p0, Ld/e;->H:Ld/e$q;

    goto/32 :goto_1b

    nop

    :goto_b
    iget-object v2, p1, Ld/e$q;->g:Landroid/view/ViewGroup;

    goto/32 :goto_1d

    nop

    :goto_c
    iput-object v1, p0, Ld/e;->H:Ld/e$q;

    :goto_d
    goto/32 :goto_1e

    nop

    :goto_e
    const-string v1, "window"

    goto/32 :goto_1f

    nop

    :goto_f
    iput-boolean p2, p1, Ld/e$q;->o:Z

    goto/32 :goto_20

    nop

    :goto_10
    if-nez v0, :cond_3

    goto/32 :goto_22

    :cond_3
    goto/32 :goto_4

    nop

    :goto_11
    iget-object v0, p0, Ld/e;->e:Landroid/content/Context;

    goto/32 :goto_e

    nop

    :goto_12
    iput-boolean p2, p1, Ld/e$q;->q:Z

    goto/32 :goto_a

    nop

    :goto_13
    if-nez p2, :cond_4

    goto/32 :goto_19

    :cond_4
    goto/32 :goto_24

    nop

    :goto_14
    iput-boolean p2, p1, Ld/e$q;->m:Z

    goto/32 :goto_6

    nop

    :goto_15
    if-nez v0, :cond_5

    goto/32 :goto_22

    :cond_5
    goto/32 :goto_5

    nop

    :goto_16
    invoke-interface {v0, v2}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    goto/32 :goto_13

    nop

    :goto_17
    const/4 p2, 0x1

    goto/32 :goto_12

    nop

    :goto_18
    invoke-virtual {p0, p2, p1, v1}, Ld/e;->K(ILd/e$q;Landroid/view/Menu;)V

    :goto_19
    goto/32 :goto_7

    nop

    :goto_1a
    invoke-virtual {p0, p1}, Ld/e;->L(Landroidx/appcompat/view/menu/e;)V

    goto/32 :goto_21

    nop

    :goto_1b
    if-eq p2, p1, :cond_6

    goto/32 :goto_d

    :cond_6
    goto/32 :goto_c

    nop

    :goto_1c
    check-cast v0, Landroid/view/WindowManager;

    goto/32 :goto_2

    nop

    :goto_1d
    if-nez v2, :cond_7

    goto/32 :goto_19

    :cond_7
    goto/32 :goto_16

    nop

    :goto_1e
    return-void

    :goto_1f
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_1c

    nop

    :goto_20
    iput-object v1, p1, Ld/e$q;->h:Landroid/view/View;

    goto/32 :goto_17

    nop

    :goto_21
    return-void

    :goto_22
    goto/32 :goto_11

    nop

    :goto_23
    if-nez v0, :cond_8

    goto/32 :goto_19

    :cond_8
    goto/32 :goto_9

    nop

    :goto_24
    iget p2, p1, Ld/e$q;->a:I

    goto/32 :goto_18

    nop

    :goto_25
    iget v0, p1, Ld/e$q;->a:I

    goto/32 :goto_3

    nop
.end method

.method public R(Landroid/view/View;Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;
    .locals 11

    iget-object v0, p0, Ld/e;->Z:Ld/g;

    const/4 v1, 0x0

    if-nez v0, :cond_1

    iget-object v0, p0, Ld/e;->e:Landroid/content/Context;

    sget-object v2, Lc/j;->z0:[I

    invoke-virtual {v0, v2}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    sget v2, Lc/j;->D0:I

    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ld/g;

    invoke-direct {v0}, Ld/g;-><init>()V

    :goto_0
    iput-object v0, p0, Ld/e;->Z:Ld/g;

    goto :goto_1

    :cond_0
    :try_start_0
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    new-array v3, v1, [Ljava/lang/Class;

    invoke-virtual {v2, v3}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v2

    new-array v3, v1, [Ljava/lang/Object;

    invoke-virtual {v2, v3}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ld/g;

    iput-object v2, p0, Ld/e;->Z:Ld/g;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to instantiate custom view inflater "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ". Falling back to default."

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v3, "AppCompatDelegate"

    invoke-static {v3, v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    new-instance v0, Ld/g;

    invoke-direct {v0}, Ld/g;-><init>()V

    goto :goto_0

    :cond_1
    :goto_1
    sget-boolean v8, Ld/e;->f0:Z

    if-eqz v8, :cond_3

    instance-of v0, p4, Lorg/xmlpull/v1/XmlPullParser;

    const/4 v2, 0x1

    if-eqz v0, :cond_2

    move-object v0, p4

    check-cast v0, Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v0

    if-le v0, v2, :cond_3

    move v1, v2

    goto :goto_2

    :cond_2
    move-object v0, p1

    check-cast v0, Landroid/view/ViewParent;

    invoke-direct {p0, v0}, Ld/e;->E0(Landroid/view/ViewParent;)Z

    move-result v0

    move v1, v0

    :cond_3
    :goto_2
    move v7, v1

    iget-object v2, p0, Ld/e;->Z:Ld/g;

    const/4 v9, 0x1

    invoke-static {}, Landroidx/appcompat/widget/r0;->b()Z

    move-result v10

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-virtual/range {v2 .. v10}, Ld/g;->q(Landroid/view/View;Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;ZZZZ)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method S()V
    .locals 2

    goto/32 :goto_5

    nop

    :goto_0
    const/4 v0, 0x0

    goto/32 :goto_c

    nop

    :goto_1
    const/4 v0, 0x0

    goto/32 :goto_16

    nop

    :goto_2
    if-nez v0, :cond_0

    goto/32 :goto_19

    :cond_0
    goto/32 :goto_b

    nop

    :goto_3
    return-void

    :goto_4
    iget-object v1, p0, Ld/e;->r:Ljava/lang/Runnable;

    goto/32 :goto_8

    nop

    :goto_5
    iget-object v0, p0, Ld/e;->l:Landroidx/appcompat/widget/v;

    goto/32 :goto_13

    nop

    :goto_6
    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    goto/32 :goto_4

    nop

    :goto_7
    iget-object v0, p0, Ld/e;->q:Landroid/widget/PopupWindow;

    goto/32 :goto_a

    nop

    :goto_8
    invoke-virtual {v0, v1}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    goto/32 :goto_7

    nop

    :goto_9
    iget-object v0, p0, Ld/e;->q:Landroid/widget/PopupWindow;

    goto/32 :goto_17

    nop

    :goto_a
    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    goto/32 :goto_14

    nop

    :goto_b
    iget-object v0, v0, Ld/e$q;->j:Landroidx/appcompat/view/menu/e;

    goto/32 :goto_e

    nop

    :goto_c
    iput-object v0, p0, Ld/e;->q:Landroid/widget/PopupWindow;

    :goto_d
    goto/32 :goto_f

    nop

    :goto_e
    if-nez v0, :cond_1

    goto/32 :goto_19

    :cond_1
    goto/32 :goto_18

    nop

    :goto_f
    invoke-virtual {p0}, Ld/e;->V()V

    goto/32 :goto_1

    nop

    :goto_10
    invoke-interface {v0}, Landroidx/appcompat/widget/v;->l()V

    :goto_11
    goto/32 :goto_9

    nop

    :goto_12
    iget-object v0, p0, Ld/e;->f:Landroid/view/Window;

    goto/32 :goto_6

    nop

    :goto_13
    if-nez v0, :cond_2

    goto/32 :goto_11

    :cond_2
    goto/32 :goto_10

    nop

    :goto_14
    if-nez v0, :cond_3

    goto/32 :goto_15

    :cond_3
    :try_start_0
    iget-object v0, p0, Ld/e;->q:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :goto_15
    goto/32 :goto_0

    nop

    :goto_16
    invoke-virtual {p0, v0, v0}, Ld/e;->d0(IZ)Ld/e$q;

    move-result-object v0

    goto/32 :goto_2

    nop

    :goto_17
    if-nez v0, :cond_4

    goto/32 :goto_d

    :cond_4
    goto/32 :goto_12

    nop

    :goto_18
    invoke-virtual {v0}, Landroidx/appcompat/view/menu/e;->close()V

    :goto_19
    goto/32 :goto_3

    nop
.end method

.method T(Landroid/view/KeyEvent;)Z
    .locals 3

    goto/32 :goto_e

    nop

    :goto_0
    instance-of v1, v0, Landroidx/core/view/e$a;

    goto/32 :goto_22

    nop

    :goto_1
    if-eqz v1, :cond_0

    goto/32 :goto_21

    :cond_0
    goto/32 :goto_23

    nop

    :goto_2
    const/4 v2, 0x0

    :goto_3
    goto/32 :goto_16

    nop

    :goto_4
    if-eq v0, v1, :cond_1

    goto/32 :goto_1a

    :cond_1
    goto/32 :goto_15

    nop

    :goto_5
    invoke-static {v0, p1}, Landroidx/core/view/e;->a(Landroid/view/View;Landroid/view/KeyEvent;)Z

    move-result v0

    goto/32 :goto_9

    nop

    :goto_6
    goto :goto_12

    :goto_7
    goto/32 :goto_11

    nop

    :goto_8
    if-nez v0, :cond_2

    goto/32 :goto_1a

    :cond_2
    goto/32 :goto_19

    nop

    :goto_9
    if-nez v0, :cond_3

    goto/32 :goto_18

    :cond_3
    goto/32 :goto_17

    nop

    :goto_a
    goto :goto_3

    :goto_b
    goto/32 :goto_2

    nop

    :goto_c
    return p1

    :goto_d
    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    goto/32 :goto_1c

    nop

    :goto_e
    iget-object v0, p0, Ld/e;->d:Ljava/lang/Object;

    goto/32 :goto_0

    nop

    :goto_f
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    goto/32 :goto_13

    nop

    :goto_10
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    goto/32 :goto_f

    nop

    :goto_11
    invoke-virtual {p0, v0, p1}, Ld/e;->s0(ILandroid/view/KeyEvent;)Z

    move-result p1

    :goto_12
    goto/32 :goto_c

    nop

    :goto_13
    if-eqz v1, :cond_4

    goto/32 :goto_b

    :cond_4
    goto/32 :goto_a

    nop

    :goto_14
    invoke-virtual {v0}, Li/i;->a()Landroid/view/Window$Callback;

    move-result-object v0

    goto/32 :goto_24

    nop

    :goto_15
    iget-object v0, p0, Ld/e;->g:Ld/e$h;

    goto/32 :goto_14

    nop

    :goto_16
    if-nez v2, :cond_5

    goto/32 :goto_7

    :cond_5
    goto/32 :goto_1e

    nop

    :goto_17
    return v2

    :goto_18
    goto/32 :goto_1f

    nop

    :goto_19
    return v2

    :goto_1a
    goto/32 :goto_10

    nop

    :goto_1b
    iget-object v0, p0, Ld/e;->f:Landroid/view/Window;

    goto/32 :goto_d

    nop

    :goto_1c
    if-nez v0, :cond_6

    goto/32 :goto_18

    :cond_6
    goto/32 :goto_5

    nop

    :goto_1d
    const/16 v1, 0x52

    goto/32 :goto_4

    nop

    :goto_1e
    invoke-virtual {p0, v0, p1}, Ld/e;->p0(ILandroid/view/KeyEvent;)Z

    move-result p1

    goto/32 :goto_6

    nop

    :goto_1f
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    goto/32 :goto_1d

    nop

    :goto_20
    if-nez v0, :cond_7

    goto/32 :goto_18

    :cond_7
    :goto_21
    goto/32 :goto_1b

    nop

    :goto_22
    const/4 v2, 0x1

    goto/32 :goto_1

    nop

    :goto_23
    instance-of v0, v0, Ld/f;

    goto/32 :goto_20

    nop

    :goto_24
    invoke-interface {v0, p1}, Landroid/view/Window$Callback;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    goto/32 :goto_8

    nop
.end method

.method U(I)V
    .locals 4

    goto/32 :goto_0

    nop

    :goto_0
    const/4 v0, 0x1

    goto/32 :goto_c

    nop

    :goto_1
    iput-boolean p1, v0, Ld/e$q;->m:Z

    goto/32 :goto_1c

    nop

    :goto_2
    return-void

    :goto_3
    invoke-virtual {v3, v2}, Landroidx/appcompat/view/menu/e;->Q(Landroid/os/Bundle;)V

    goto/32 :goto_17

    nop

    :goto_4
    invoke-virtual {v2}, Landroidx/appcompat/view/menu/e;->d0()V

    goto/32 :goto_6

    nop

    :goto_5
    iput-boolean v0, v1, Ld/e$q;->q:Z

    goto/32 :goto_9

    nop

    :goto_6
    iget-object v2, v1, Ld/e$q;->j:Landroidx/appcompat/view/menu/e;

    goto/32 :goto_1f

    nop

    :goto_7
    if-nez v0, :cond_0

    goto/32 :goto_e

    :cond_0
    goto/32 :goto_1

    nop

    :goto_8
    iget-object p1, p0, Ld/e;->l:Landroidx/appcompat/widget/v;

    goto/32 :goto_18

    nop

    :goto_9
    const/16 v0, 0x6c

    goto/32 :goto_19

    nop

    :goto_a
    invoke-virtual {p0, p1, p1}, Ld/e;->d0(IZ)Ld/e$q;

    move-result-object v0

    goto/32 :goto_7

    nop

    :goto_b
    iget-object v3, v1, Ld/e$q;->j:Landroidx/appcompat/view/menu/e;

    goto/32 :goto_3

    nop

    :goto_c
    invoke-virtual {p0, p1, v0}, Ld/e;->d0(IZ)Ld/e$q;

    move-result-object v1

    goto/32 :goto_1b

    nop

    :goto_d
    invoke-direct {p0, v0, p1}, Ld/e;->A0(Ld/e$q;Landroid/view/KeyEvent;)Z

    :goto_e
    goto/32 :goto_2

    nop

    :goto_f
    const/4 p1, 0x0

    goto/32 :goto_a

    nop

    :goto_10
    iput-object v2, v1, Ld/e$q;->s:Landroid/os/Bundle;

    :goto_11
    goto/32 :goto_1d

    nop

    :goto_12
    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    goto/32 :goto_b

    nop

    :goto_13
    if-eqz p1, :cond_1

    goto/32 :goto_e

    :cond_1
    :goto_14
    goto/32 :goto_8

    nop

    :goto_15
    if-nez v2, :cond_2

    goto/32 :goto_20

    :cond_2
    goto/32 :goto_1a

    nop

    :goto_16
    if-gtz v3, :cond_3

    goto/32 :goto_11

    :cond_3
    goto/32 :goto_10

    nop

    :goto_17
    invoke-virtual {v2}, Landroid/os/Bundle;->size()I

    move-result v3

    goto/32 :goto_16

    nop

    :goto_18
    if-nez p1, :cond_4

    goto/32 :goto_e

    :cond_4
    goto/32 :goto_f

    nop

    :goto_19
    if-ne p1, v0, :cond_5

    goto/32 :goto_14

    :cond_5
    goto/32 :goto_13

    nop

    :goto_1a
    new-instance v2, Landroid/os/Bundle;

    goto/32 :goto_12

    nop

    :goto_1b
    iget-object v2, v1, Ld/e$q;->j:Landroidx/appcompat/view/menu/e;

    goto/32 :goto_15

    nop

    :goto_1c
    const/4 p1, 0x0

    goto/32 :goto_d

    nop

    :goto_1d
    iget-object v2, v1, Ld/e$q;->j:Landroidx/appcompat/view/menu/e;

    goto/32 :goto_4

    nop

    :goto_1e
    iput-boolean v0, v1, Ld/e$q;->r:Z

    goto/32 :goto_5

    nop

    :goto_1f
    invoke-virtual {v2}, Landroidx/appcompat/view/menu/e;->clear()V

    :goto_20
    goto/32 :goto_1e

    nop
.end method

.method V()V
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Ld/e;->s:Landroidx/core/view/y;

    goto/32 :goto_2

    nop

    :goto_1
    return-void

    :goto_2
    if-nez v0, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_3

    nop

    :goto_3
    invoke-virtual {v0}, Landroidx/core/view/y;->b()V

    :goto_4
    goto/32 :goto_1

    nop
.end method

.method Y(Landroid/view/Menu;)Ld/e$q;
    .locals 5

    goto/32 :goto_b

    nop

    :goto_0
    const/4 v1, 0x0

    goto/32 :goto_8

    nop

    :goto_1
    if-nez v3, :cond_0

    goto/32 :goto_6

    :cond_0
    goto/32 :goto_c

    nop

    :goto_2
    array-length v2, v0

    goto/32 :goto_3

    nop

    :goto_3
    goto :goto_11

    :goto_4
    goto/32 :goto_10

    nop

    :goto_5
    return-object v3

    :goto_6
    goto/32 :goto_e

    nop

    :goto_7
    return-object p1

    :goto_8
    if-nez v0, :cond_1

    goto/32 :goto_4

    :cond_1
    goto/32 :goto_2

    nop

    :goto_9
    if-eq v4, p1, :cond_2

    goto/32 :goto_6

    :cond_2
    goto/32 :goto_5

    nop

    :goto_a
    aget-object v3, v0, v1

    goto/32 :goto_1

    nop

    :goto_b
    iget-object v0, p0, Ld/e;->G:[Ld/e$q;

    goto/32 :goto_0

    nop

    :goto_c
    iget-object v4, v3, Ld/e$q;->j:Landroidx/appcompat/view/menu/e;

    goto/32 :goto_9

    nop

    :goto_d
    const/4 p1, 0x0

    goto/32 :goto_7

    nop

    :goto_e
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_12

    nop

    :goto_f
    if-lt v1, v2, :cond_3

    goto/32 :goto_13

    :cond_3
    goto/32 :goto_a

    nop

    :goto_10
    move v2, v1

    :goto_11
    goto/32 :goto_f

    nop

    :goto_12
    goto :goto_11

    :goto_13
    goto/32 :goto_d

    nop
.end method

.method public a(Landroidx/appcompat/view/menu/e;Landroid/view/MenuItem;)Z
    .locals 2

    invoke-virtual {p0}, Ld/e;->f0()Landroid/view/Window$Callback;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-boolean v1, p0, Ld/e;->M:Z

    if-nez v1, :cond_0

    invoke-virtual {p1}, Landroidx/appcompat/view/menu/e;->D()Landroidx/appcompat/view/menu/e;

    move-result-object p1

    invoke-virtual {p0, p1}, Ld/e;->Y(Landroid/view/Menu;)Ld/e$q;

    move-result-object p1

    if-eqz p1, :cond_0

    iget p1, p1, Ld/e$q;->a:I

    invoke-interface {v0, p1, p2}, Landroid/view/Window$Callback;->onMenuItemSelected(ILandroid/view/MenuItem;)Z

    move-result p1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method final a0()Landroid/content/Context;
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    invoke-virtual {p0}, Ld/e;->m()Ld/a;

    move-result-object v0

    goto/32 :goto_8

    nop

    :goto_1
    iget-object v0, p0, Ld/e;->e:Landroid/content/Context;

    :goto_2
    goto/32 :goto_5

    nop

    :goto_3
    invoke-virtual {v0}, Ld/a;->k()Landroid/content/Context;

    move-result-object v0

    goto/32 :goto_6

    nop

    :goto_4
    if-eqz v0, :cond_0

    goto/32 :goto_2

    :cond_0
    goto/32 :goto_1

    nop

    :goto_5
    return-object v0

    :goto_6
    goto :goto_a

    :goto_7
    goto/32 :goto_9

    nop

    :goto_8
    if-nez v0, :cond_1

    goto/32 :goto_7

    :cond_1
    goto/32 :goto_3

    nop

    :goto_9
    const/4 v0, 0x0

    :goto_a
    goto/32 :goto_4

    nop
.end method

.method public b(Landroidx/appcompat/view/menu/e;)V
    .locals 0

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Ld/e;->B0(Z)V

    return-void
.end method

.method public d(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 2

    invoke-direct {p0}, Ld/e;->W()V

    iget-object v0, p0, Ld/e;->v:Landroid/view/ViewGroup;

    const v1, 0x1020002

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object p1, p0, Ld/e;->g:Ld/e$h;

    invoke-virtual {p1}, Li/i;->a()Landroid/view/Window$Callback;

    move-result-object p1

    invoke-interface {p1}, Landroid/view/Window$Callback;->onContentChanged()V

    return-void
.end method

.method protected d0(IZ)Ld/e$q;
    .locals 3

    iget-object p2, p0, Ld/e;->G:[Ld/e$q;

    if-eqz p2, :cond_0

    array-length v0, p2

    if-gt v0, p1, :cond_2

    :cond_0
    add-int/lit8 v0, p1, 0x1

    new-array v0, v0, [Ld/e$q;

    if-eqz p2, :cond_1

    array-length v1, p2

    const/4 v2, 0x0

    invoke-static {p2, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    iput-object v0, p0, Ld/e;->G:[Ld/e$q;

    move-object p2, v0

    :cond_2
    aget-object v0, p2, p1

    if-nez v0, :cond_3

    new-instance v0, Ld/e$q;

    invoke-direct {v0, p1}, Ld/e$q;-><init>(I)V

    aput-object v0, p2, p1

    :cond_3
    return-object v0
.end method

.method final e0()Ljava/lang/CharSequence;
    .locals 2

    goto/32 :goto_3

    nop

    :goto_0
    return-object v0

    :goto_1
    goto/32 :goto_2

    nop

    :goto_2
    iget-object v0, p0, Ld/e;->k:Ljava/lang/CharSequence;

    goto/32 :goto_4

    nop

    :goto_3
    iget-object v0, p0, Ld/e;->d:Ljava/lang/Object;

    goto/32 :goto_8

    nop

    :goto_4
    return-object v0

    :goto_5
    invoke-virtual {v0}, Landroid/app/Activity;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    goto/32 :goto_0

    nop

    :goto_6
    check-cast v0, Landroid/app/Activity;

    goto/32 :goto_5

    nop

    :goto_7
    if-nez v1, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_6

    nop

    :goto_8
    instance-of v1, v0, Landroid/app/Activity;

    goto/32 :goto_7

    nop
.end method

.method public f(Landroid/content/Context;)Landroid/content/Context;
    .locals 6

    const/4 v0, 0x1

    iput-boolean v0, p0, Ld/e;->J:Z

    invoke-direct {p0}, Ld/e;->J()I

    move-result v1

    invoke-virtual {p0, p1, v1}, Ld/e;->n0(Landroid/content/Context;I)I

    move-result v1

    sget-boolean v2, Ld/e;->i0:Z

    const/4 v3, 0x0

    if-eqz v2, :cond_0

    instance-of v2, p1, Landroid/view/ContextThemeWrapper;

    if-eqz v2, :cond_0

    invoke-direct {p0, p1, v1, v3}, Ld/e;->P(Landroid/content/Context;ILandroid/content/res/Configuration;)Landroid/content/res/Configuration;

    move-result-object v2

    :try_start_0
    move-object v4, p1

    check-cast v4, Landroid/view/ContextThemeWrapper;

    invoke-static {v4, v2}, Ld/e$o;->a(Landroid/view/ContextThemeWrapper;Landroid/content/res/Configuration;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    :cond_0
    instance-of v2, p1, Li/d;

    if-eqz v2, :cond_1

    invoke-direct {p0, p1, v1, v3}, Ld/e;->P(Landroid/content/Context;ILandroid/content/res/Configuration;)Landroid/content/res/Configuration;

    move-result-object v2

    :try_start_1
    move-object v4, p1

    check-cast v4, Li/d;

    invoke-virtual {v4, v2}, Li/d;->a(Landroid/content/res/Configuration;)V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1

    return-object p1

    :catch_1
    :cond_1
    sget-boolean v2, Ld/e;->h0:Z

    if-nez v2, :cond_2

    invoke-super {p0, p1}, Ld/d;->f(Landroid/content/Context;)Landroid/content/Context;

    move-result-object p1

    return-object p1

    :cond_2
    :try_start_2
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Landroid/content/pm/ApplicationInfo;)Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_3

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/content/res/Configuration;->equals(Landroid/content/res/Configuration;)Z

    move-result v5

    if-nez v5, :cond_3

    invoke-static {v2, v4}, Ld/e;->Z(Landroid/content/res/Configuration;Landroid/content/res/Configuration;)Landroid/content/res/Configuration;

    move-result-object v3

    :cond_3
    invoke-direct {p0, p1, v1, v3}, Ld/e;->P(Landroid/content/Context;ILandroid/content/res/Configuration;)Landroid/content/res/Configuration;

    move-result-object v1

    new-instance v2, Li/d;

    sget v3, Lc/i;->b:I

    invoke-direct {v2, p1, v3}, Li/d;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v2, v1}, Li/d;->a(Landroid/content/res/Configuration;)V

    const/4 v1, 0x0

    :try_start_3
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object p1
    :try_end_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_2

    if-eqz p1, :cond_4

    goto :goto_0

    :cond_4
    move v0, v1

    :goto_0
    move v1, v0

    :catch_2
    if-eqz v1, :cond_5

    invoke-virtual {v2}, Li/d;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object p1

    invoke-static {p1}, Lw/b$b;->a(Landroid/content/res/Resources$Theme;)V

    :cond_5
    invoke-super {p0, v2}, Ld/d;->f(Landroid/content/Context;)Landroid/content/Context;

    move-result-object p1

    return-object p1

    :catch_3
    move-exception p1

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Application failed to obtain resources from itself"

    invoke-direct {v0, v1, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method final f0()Landroid/view/Window$Callback;
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Ld/e;->f:Landroid/view/Window;

    goto/32 :goto_1

    nop

    :goto_1
    invoke-virtual {v0}, Landroid/view/Window;->getCallback()Landroid/view/Window$Callback;

    move-result-object v0

    goto/32 :goto_2

    nop

    :goto_2
    return-object v0
.end method

.method public i(I)Landroid/view/View;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(I)TT;"
        }
    .end annotation

    invoke-direct {p0}, Ld/e;->W()V

    iget-object v0, p0, Ld/e;->f:Landroid/view/Window;

    invoke-virtual {v0, p1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public k()I
    .locals 1

    iget v0, p0, Ld/e;->N:I

    return v0
.end method

.method public l()Landroid/view/MenuInflater;
    .locals 2

    iget-object v0, p0, Ld/e;->j:Landroid/view/MenuInflater;

    if-nez v0, :cond_1

    invoke-direct {p0}, Ld/e;->g0()V

    new-instance v0, Li/g;

    iget-object v1, p0, Ld/e;->i:Ld/a;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ld/a;->k()Landroid/content/Context;

    move-result-object v1

    goto :goto_0

    :cond_0
    iget-object v1, p0, Ld/e;->e:Landroid/content/Context;

    :goto_0
    invoke-direct {v0, v1}, Li/g;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Ld/e;->j:Landroid/view/MenuInflater;

    :cond_1
    iget-object v0, p0, Ld/e;->j:Landroid/view/MenuInflater;

    return-object v0
.end method

.method public m()Ld/a;
    .locals 1

    invoke-direct {p0}, Ld/e;->g0()V

    iget-object v0, p0, Ld/e;->i:Ld/a;

    return-object v0
.end method

.method public m0()Z
    .locals 1

    iget-boolean v0, p0, Ld/e;->t:Z

    return v0
.end method

.method public n()V
    .locals 2

    iget-object v0, p0, Ld/e;->e:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/LayoutInflater;->getFactory()Landroid/view/LayoutInflater$Factory;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-static {v0, p0}, Landroidx/core/view/f;->a(Landroid/view/LayoutInflater;Landroid/view/LayoutInflater$Factory2;)V

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Landroid/view/LayoutInflater;->getFactory2()Landroid/view/LayoutInflater$Factory2;

    move-result-object v0

    instance-of v0, v0, Ld/e;

    if-nez v0, :cond_1

    const-string v0, "AppCompatDelegate"

    const-string v1, "The Activity\'s LayoutInflater already has a Factory installed so we can not install AppCompat\'s"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    return-void
.end method

.method n0(Landroid/content/Context;I)I
    .locals 2

    goto/32 :goto_f

    nop

    :goto_0
    throw p1

    :goto_1
    goto/32 :goto_6

    nop

    :goto_2
    if-eqz p2, :cond_0

    goto/32 :goto_22

    :cond_0
    goto/32 :goto_21

    nop

    :goto_3
    if-ne p2, v0, :cond_1

    goto/32 :goto_11

    :cond_1
    goto/32 :goto_1a

    nop

    :goto_4
    if-ne p2, v0, :cond_2

    goto/32 :goto_15

    :cond_2
    goto/32 :goto_19

    nop

    :goto_5
    check-cast p2, Landroid/app/UiModeManager;

    goto/32 :goto_1c

    nop

    :goto_6
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p2

    goto/32 :goto_20

    nop

    :goto_7
    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p2

    goto/32 :goto_5

    nop

    :goto_8
    if-nez p2, :cond_3

    goto/32 :goto_1

    :cond_3
    goto/32 :goto_9

    nop

    :goto_9
    const/4 v0, 0x1

    goto/32 :goto_16

    nop

    :goto_a
    return v1

    :goto_b
    invoke-direct {p0, p1}, Ld/e;->c0(Landroid/content/Context;)Ld/e$j;

    move-result-object p1

    goto/32 :goto_10

    nop

    :goto_c
    return p1

    :goto_d
    goto/32 :goto_13

    nop

    :goto_e
    if-eq p2, v0, :cond_4

    goto/32 :goto_d

    :cond_4
    goto/32 :goto_17

    nop

    :goto_f
    const/16 v0, -0x64

    goto/32 :goto_1f

    nop

    :goto_10
    goto :goto_18

    :goto_11
    goto/32 :goto_14

    nop

    :goto_12
    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    goto/32 :goto_0

    nop

    :goto_13
    new-instance p1, Ljava/lang/IllegalStateException;

    goto/32 :goto_1e

    nop

    :goto_14
    return p2

    :goto_15
    goto/32 :goto_a

    nop

    :goto_16
    if-ne p2, v0, :cond_5

    goto/32 :goto_11

    :cond_5
    goto/32 :goto_1b

    nop

    :goto_17
    invoke-direct {p0, p1}, Ld/e;->b0(Landroid/content/Context;)Ld/e$j;

    move-result-object p1

    :goto_18
    goto/32 :goto_1d

    nop

    :goto_19
    if-ne p2, v1, :cond_6

    goto/32 :goto_11

    :cond_6
    goto/32 :goto_8

    nop

    :goto_1a
    const/4 v0, 0x3

    goto/32 :goto_e

    nop

    :goto_1b
    const/4 v0, 0x2

    goto/32 :goto_3

    nop

    :goto_1c
    invoke-virtual {p2}, Landroid/app/UiModeManager;->getNightMode()I

    move-result p2

    goto/32 :goto_2

    nop

    :goto_1d
    invoke-virtual {p1}, Ld/e$j;->c()I

    move-result p1

    goto/32 :goto_c

    nop

    :goto_1e
    const-string p2, "Unknown value set for night mode. Please use one of the MODE_NIGHT values from AppCompatDelegate."

    goto/32 :goto_12

    nop

    :goto_1f
    const/4 v1, -0x1

    goto/32 :goto_4

    nop

    :goto_20
    const-class v0, Landroid/app/UiModeManager;

    goto/32 :goto_7

    nop

    :goto_21
    return v1

    :goto_22
    goto/32 :goto_b

    nop
.end method

.method public o()V
    .locals 1

    invoke-virtual {p0}, Ld/e;->m()Ld/a;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ld/a;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Ld/e;->k0(I)V

    return-void
.end method

.method o0()Z
    .locals 2

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Ld/e;->o:Li/b;

    goto/32 :goto_b

    nop

    :goto_1
    return v0

    :goto_2
    invoke-virtual {p0}, Ld/e;->m()Ld/a;

    move-result-object v0

    goto/32 :goto_5

    nop

    :goto_3
    if-nez v0, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_6

    nop

    :goto_4
    if-nez v0, :cond_1

    goto/32 :goto_a

    :cond_1
    goto/32 :goto_d

    nop

    :goto_5
    if-nez v0, :cond_2

    goto/32 :goto_7

    :cond_2
    goto/32 :goto_8

    nop

    :goto_6
    return v1

    :goto_7
    goto/32 :goto_c

    nop

    :goto_8
    invoke-virtual {v0}, Ld/a;->h()Z

    move-result v0

    goto/32 :goto_3

    nop

    :goto_9
    return v1

    :goto_a
    goto/32 :goto_2

    nop

    :goto_b
    const/4 v1, 0x1

    goto/32 :goto_4

    nop

    :goto_c
    const/4 v0, 0x0

    goto/32 :goto_1

    nop

    :goto_d
    invoke-virtual {v0}, Li/b;->c()V

    goto/32 :goto_9

    nop
.end method

.method public final onCreateView(Landroid/view/View;Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;
    .locals 0

    invoke-virtual {p0, p1, p2, p3, p4}, Ld/e;->R(Landroid/view/View;Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public onCreateView(Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1, p2, p3}, Ld/e;->onCreateView(Landroid/view/View;Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public p(Landroid/content/res/Configuration;)V
    .locals 1

    iget-boolean v0, p0, Ld/e;->A:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Ld/e;->u:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ld/e;->m()Ld/a;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Ld/a;->n(Landroid/content/res/Configuration;)V

    :cond_0
    invoke-static {}, Landroidx/appcompat/widget/g;->b()Landroidx/appcompat/widget/g;

    move-result-object p1

    iget-object v0, p0, Ld/e;->e:Landroid/content/Context;

    invoke-virtual {p1, v0}, Landroidx/appcompat/widget/g;->g(Landroid/content/Context;)V

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Ld/e;->G(Z)Z

    return-void
.end method

.method p0(ILandroid/view/KeyEvent;)Z
    .locals 3

    goto/32 :goto_2

    nop

    :goto_0
    goto :goto_e

    :goto_1
    goto/32 :goto_c

    nop

    :goto_2
    const/4 v0, 0x4

    goto/32 :goto_11

    nop

    :goto_3
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getFlags()I

    move-result p1

    goto/32 :goto_b

    nop

    :goto_4
    if-nez p1, :cond_0

    goto/32 :goto_14

    :cond_0
    goto/32 :goto_13

    nop

    :goto_5
    const/4 v2, 0x0

    goto/32 :goto_10

    nop

    :goto_6
    return v1

    :goto_7
    goto/32 :goto_3

    nop

    :goto_8
    return v2

    :goto_9
    move v1, v2

    :goto_a
    goto/32 :goto_d

    nop

    :goto_b
    and-int/lit16 p1, p1, 0x80

    goto/32 :goto_4

    nop

    :goto_c
    invoke-direct {p0, v2, p2}, Ld/e;->q0(ILandroid/view/KeyEvent;)Z

    goto/32 :goto_6

    nop

    :goto_d
    iput-boolean v1, p0, Ld/e;->I:Z

    :goto_e
    goto/32 :goto_8

    nop

    :goto_f
    if-ne p1, v0, :cond_1

    goto/32 :goto_1

    :cond_1
    goto/32 :goto_0

    nop

    :goto_10
    if-ne p1, v0, :cond_2

    goto/32 :goto_7

    :cond_2
    goto/32 :goto_12

    nop

    :goto_11
    const/4 v1, 0x1

    goto/32 :goto_5

    nop

    :goto_12
    const/16 v0, 0x52

    goto/32 :goto_f

    nop

    :goto_13
    goto :goto_a

    :goto_14
    goto/32 :goto_9

    nop
.end method

.method public q(Landroid/os/Bundle;)V
    .locals 2

    const/4 p1, 0x1

    iput-boolean p1, p0, Ld/e;->J:Z

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Ld/e;->G(Z)Z

    invoke-direct {p0}, Ld/e;->X()V

    iget-object v0, p0, Ld/e;->d:Ljava/lang/Object;

    instance-of v1, v0, Landroid/app/Activity;

    if-eqz v1, :cond_2

    const/4 v1, 0x0

    :try_start_0
    check-cast v0, Landroid/app/Activity;

    invoke-static {v0}, Landroidx/core/app/d;->c(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    if-eqz v1, :cond_1

    invoke-virtual {p0}, Ld/e;->y0()Ld/a;

    move-result-object v0

    if-nez v0, :cond_0

    iput-boolean p1, p0, Ld/e;->W:Z

    goto :goto_0

    :cond_0
    invoke-virtual {v0, p1}, Ld/a;->t(Z)V

    :cond_1
    :goto_0
    invoke-static {p0}, Ld/d;->c(Ld/d;)V

    :cond_2
    iput-boolean p1, p0, Ld/e;->K:Z

    return-void
.end method

.method public r()V
    .locals 3

    iget-object v0, p0, Ld/e;->d:Ljava/lang/Object;

    instance-of v0, v0, Landroid/app/Activity;

    if-eqz v0, :cond_0

    invoke-static {p0}, Ld/d;->x(Ld/d;)V

    :cond_0
    iget-boolean v0, p0, Ld/e;->T:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Ld/e;->f:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Ld/e;->V:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Ld/e;->L:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Ld/e;->M:Z

    iget v0, p0, Ld/e;->N:I

    const/16 v1, -0x64

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Ld/e;->d:Ljava/lang/Object;

    instance-of v1, v0, Landroid/app/Activity;

    if-eqz v1, :cond_2

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isChangingConfigurations()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Ld/e;->e0:Ln/g;

    iget-object v1, p0, Ld/e;->d:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Ld/e;->N:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ln/g;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    sget-object v0, Ld/e;->e0:Ln/g;

    iget-object v1, p0, Ld/e;->d:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ln/g;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    iget-object v0, p0, Ld/e;->i:Ld/a;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ld/a;->o()V

    :cond_3
    invoke-direct {p0}, Ld/e;->M()V

    return-void
.end method

.method r0(ILandroid/view/KeyEvent;)Z
    .locals 3

    goto/32 :goto_13

    nop

    :goto_0
    return v1

    :goto_1
    goto/32 :goto_4

    nop

    :goto_2
    const/4 v1, 0x1

    goto/32 :goto_5

    nop

    :goto_3
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    goto/32 :goto_d

    nop

    :goto_4
    iget-object p1, p0, Ld/e;->H:Ld/e$q;

    goto/32 :goto_e

    nop

    :goto_5
    if-nez v0, :cond_0

    goto/32 :goto_c

    :cond_0
    goto/32 :goto_6

    nop

    :goto_6
    invoke-virtual {v0, p1, p2}, Ld/a;->p(ILandroid/view/KeyEvent;)Z

    move-result p1

    goto/32 :goto_1a

    nop

    :goto_7
    iget-object p1, p0, Ld/e;->H:Ld/e$q;

    goto/32 :goto_19

    nop

    :goto_8
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    goto/32 :goto_1c

    nop

    :goto_9
    return v1

    :goto_a
    goto/32 :goto_14

    nop

    :goto_b
    return v1

    :goto_c
    goto/32 :goto_7

    nop

    :goto_d
    invoke-direct {p0, p1, v0, p2, v1}, Ld/e;->z0(Ld/e$q;ILandroid/view/KeyEvent;I)Z

    move-result p1

    goto/32 :goto_1b

    nop

    :goto_e
    const/4 v0, 0x0

    goto/32 :goto_17

    nop

    :goto_f
    if-nez p2, :cond_1

    goto/32 :goto_a

    :cond_1
    goto/32 :goto_9

    nop

    :goto_10
    if-nez p1, :cond_2

    goto/32 :goto_12

    :cond_2
    goto/32 :goto_11

    nop

    :goto_11
    iput-boolean v1, p1, Ld/e$q;->n:Z

    :goto_12
    goto/32 :goto_0

    nop

    :goto_13
    invoke-virtual {p0}, Ld/e;->m()Ld/a;

    move-result-object v0

    goto/32 :goto_2

    nop

    :goto_14
    return v0

    :goto_15
    invoke-virtual {p0, v0, v1}, Ld/e;->d0(IZ)Ld/e$q;

    move-result-object p1

    goto/32 :goto_1d

    nop

    :goto_16
    iget-object p1, p0, Ld/e;->H:Ld/e$q;

    goto/32 :goto_10

    nop

    :goto_17
    if-eqz p1, :cond_3

    goto/32 :goto_a

    :cond_3
    goto/32 :goto_15

    nop

    :goto_18
    iput-boolean v0, p1, Ld/e$q;->m:Z

    goto/32 :goto_f

    nop

    :goto_19
    if-nez p1, :cond_4

    goto/32 :goto_1

    :cond_4
    goto/32 :goto_3

    nop

    :goto_1a
    if-nez p1, :cond_5

    goto/32 :goto_c

    :cond_5
    goto/32 :goto_b

    nop

    :goto_1b
    if-nez p1, :cond_6

    goto/32 :goto_1

    :cond_6
    goto/32 :goto_16

    nop

    :goto_1c
    invoke-direct {p0, p1, v2, p2, v1}, Ld/e;->z0(Ld/e$q;ILandroid/view/KeyEvent;I)Z

    move-result p2

    goto/32 :goto_18

    nop

    :goto_1d
    invoke-direct {p0, p1, p2}, Ld/e;->A0(Ld/e$q;Landroid/view/KeyEvent;)Z

    goto/32 :goto_8

    nop
.end method

.method public s(Landroid/os/Bundle;)V
    .locals 0

    invoke-direct {p0}, Ld/e;->W()V

    return-void
.end method

.method s0(ILandroid/view/KeyEvent;)Z
    .locals 3

    goto/32 :goto_9

    nop

    :goto_0
    iget-boolean p1, p0, Ld/e;->I:Z

    goto/32 :goto_c

    nop

    :goto_1
    if-nez v0, :cond_0

    goto/32 :goto_7

    :cond_0
    goto/32 :goto_10

    nop

    :goto_2
    return v1

    :goto_3
    goto/32 :goto_d

    nop

    :goto_4
    if-ne p1, v0, :cond_1

    goto/32 :goto_12

    :cond_1
    goto/32 :goto_11

    nop

    :goto_5
    invoke-virtual {p0, v2, v2}, Ld/e;->d0(IZ)Ld/e$q;

    move-result-object p2

    goto/32 :goto_1a

    nop

    :goto_6
    return v1

    :goto_7
    goto/32 :goto_e

    nop

    :goto_8
    iget-boolean v0, p2, Ld/e$q;->o:Z

    goto/32 :goto_1

    nop

    :goto_9
    const/4 v0, 0x4

    goto/32 :goto_19

    nop

    :goto_a
    invoke-direct {p0, v2, p2}, Ld/e;->t0(ILandroid/view/KeyEvent;)Z

    goto/32 :goto_14

    nop

    :goto_b
    if-nez p1, :cond_2

    goto/32 :goto_3

    :cond_2
    goto/32 :goto_2

    nop

    :goto_c
    iput-boolean v2, p0, Ld/e;->I:Z

    goto/32 :goto_5

    nop

    :goto_d
    return v2

    :goto_e
    invoke-virtual {p0}, Ld/e;->o0()Z

    move-result p1

    goto/32 :goto_b

    nop

    :goto_f
    const/4 v2, 0x0

    goto/32 :goto_13

    nop

    :goto_10
    if-eqz p1, :cond_3

    goto/32 :goto_17

    :cond_3
    goto/32 :goto_16

    nop

    :goto_11
    goto :goto_3

    :goto_12
    goto/32 :goto_a

    nop

    :goto_13
    if-ne p1, v0, :cond_4

    goto/32 :goto_15

    :cond_4
    goto/32 :goto_18

    nop

    :goto_14
    return v1

    :goto_15
    goto/32 :goto_0

    nop

    :goto_16
    invoke-virtual {p0, p2, v1}, Ld/e;->O(Ld/e$q;Z)V

    :goto_17
    goto/32 :goto_6

    nop

    :goto_18
    const/16 v0, 0x52

    goto/32 :goto_4

    nop

    :goto_19
    const/4 v1, 0x1

    goto/32 :goto_f

    nop

    :goto_1a
    if-nez p2, :cond_5

    goto/32 :goto_7

    :cond_5
    goto/32 :goto_8

    nop
.end method

.method public t()V
    .locals 2

    invoke-virtual {p0}, Ld/e;->m()Ld/a;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ld/a;->u(Z)V

    :cond_0
    return-void
.end method

.method public u(Landroid/os/Bundle;)V
    .locals 0

    return-void
.end method

.method u0(I)V
    .locals 1

    goto/32 :goto_4

    nop

    :goto_0
    invoke-virtual {p1, v0}, Ld/a;->i(Z)V

    :goto_1
    goto/32 :goto_7

    nop

    :goto_2
    invoke-virtual {p0}, Ld/e;->m()Ld/a;

    move-result-object p1

    goto/32 :goto_5

    nop

    :goto_3
    if-eq p1, v0, :cond_0

    goto/32 :goto_1

    :cond_0
    goto/32 :goto_2

    nop

    :goto_4
    const/16 v0, 0x6c

    goto/32 :goto_3

    nop

    :goto_5
    if-nez p1, :cond_1

    goto/32 :goto_1

    :cond_1
    goto/32 :goto_6

    nop

    :goto_6
    const/4 v0, 0x1

    goto/32 :goto_0

    nop

    :goto_7
    return-void
.end method

.method public v()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Ld/e;->L:Z

    invoke-virtual {p0}, Ld/e;->F()Z

    return-void
.end method

.method v0(I)V
    .locals 2

    goto/32 :goto_2

    nop

    :goto_0
    const/4 v1, 0x1

    goto/32 :goto_e

    nop

    :goto_1
    return-void

    :goto_2
    const/4 v0, 0x0

    goto/32 :goto_a

    nop

    :goto_3
    goto :goto_6

    :goto_4
    goto/32 :goto_d

    nop

    :goto_5
    invoke-virtual {p0, p1, v0}, Ld/e;->O(Ld/e$q;Z)V

    :goto_6
    goto/32 :goto_1

    nop

    :goto_7
    if-eq p1, v1, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_c

    nop

    :goto_8
    if-nez v1, :cond_1

    goto/32 :goto_6

    :cond_1
    goto/32 :goto_5

    nop

    :goto_9
    invoke-virtual {p1, v0}, Ld/a;->i(Z)V

    goto/32 :goto_3

    nop

    :goto_a
    const/16 v1, 0x6c

    goto/32 :goto_7

    nop

    :goto_b
    if-nez p1, :cond_2

    goto/32 :goto_6

    :cond_2
    goto/32 :goto_9

    nop

    :goto_c
    invoke-virtual {p0}, Ld/e;->m()Ld/a;

    move-result-object p1

    goto/32 :goto_b

    nop

    :goto_d
    if-eqz p1, :cond_3

    goto/32 :goto_6

    :cond_3
    goto/32 :goto_0

    nop

    :goto_e
    invoke-virtual {p0, p1, v1}, Ld/e;->d0(IZ)Ld/e$q;

    move-result-object p1

    goto/32 :goto_f

    nop

    :goto_f
    iget-boolean v1, p1, Ld/e$q;->o:Z

    goto/32 :goto_8

    nop
.end method

.method public w()V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Ld/e;->L:Z

    invoke-virtual {p0}, Ld/e;->m()Ld/a;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1, v0}, Ld/a;->u(Z)V

    :cond_0
    return-void
.end method

.method w0(Landroid/view/ViewGroup;)V
    .locals 0

    goto/32 :goto_0

    nop

    :goto_0
    return-void
.end method

.method final y0()Ld/a;
    .locals 1

    goto/32 :goto_0

    nop

    :goto_0
    iget-object v0, p0, Ld/e;->i:Ld/a;

    goto/32 :goto_1

    nop

    :goto_1
    return-object v0
.end method

.method public z(I)Z
    .locals 4

    invoke-direct {p0, p1}, Ld/e;->C0(I)I

    move-result p1

    iget-boolean v0, p0, Ld/e;->E:Z

    const/4 v1, 0x0

    const/16 v2, 0x6c

    if-eqz v0, :cond_0

    if-ne p1, v2, :cond_0

    return v1

    :cond_0
    iget-boolean v0, p0, Ld/e;->A:Z

    const/4 v3, 0x1

    if-eqz v0, :cond_1

    if-ne p1, v3, :cond_1

    iput-boolean v1, p0, Ld/e;->A:Z

    :cond_1
    if-eq p1, v3, :cond_7

    const/4 v0, 0x2

    if-eq p1, v0, :cond_6

    const/4 v0, 0x5

    if-eq p1, v0, :cond_5

    const/16 v0, 0xa

    if-eq p1, v0, :cond_4

    if-eq p1, v2, :cond_3

    const/16 v0, 0x6d

    if-eq p1, v0, :cond_2

    iget-object v0, p0, Ld/e;->f:Landroid/view/Window;

    invoke-virtual {v0, p1}, Landroid/view/Window;->requestFeature(I)Z

    move-result p1

    return p1

    :cond_2
    invoke-direct {p0}, Ld/e;->H0()V

    iput-boolean v3, p0, Ld/e;->B:Z

    return v3

    :cond_3
    invoke-direct {p0}, Ld/e;->H0()V

    iput-boolean v3, p0, Ld/e;->A:Z

    return v3

    :cond_4
    invoke-direct {p0}, Ld/e;->H0()V

    iput-boolean v3, p0, Ld/e;->C:Z

    return v3

    :cond_5
    invoke-direct {p0}, Ld/e;->H0()V

    iput-boolean v3, p0, Ld/e;->z:Z

    return v3

    :cond_6
    invoke-direct {p0}, Ld/e;->H0()V

    iput-boolean v3, p0, Ld/e;->y:Z

    return v3

    :cond_7
    invoke-direct {p0}, Ld/e;->H0()V

    iput-boolean v3, p0, Ld/e;->E:Z

    return v3
.end method
