.class Ld/e$g;
.super Ljava/lang/Object;

# interfaces
.implements Li/b$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ld/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "g"
.end annotation


# instance fields
.field private a:Li/b$a;

.field final synthetic b:Ld/e;


# direct methods
.method public constructor <init>(Ld/e;Li/b$a;)V
    .locals 0

    iput-object p1, p0, Ld/e$g;->b:Ld/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Ld/e$g;->a:Li/b$a;

    return-void
.end method


# virtual methods
.method public a(Li/b;Landroid/view/MenuItem;)Z
    .locals 1

    iget-object v0, p0, Ld/e$g;->a:Li/b$a;

    invoke-interface {v0, p1, p2}, Li/b$a;->a(Li/b;Landroid/view/MenuItem;)Z

    move-result p1

    return p1
.end method

.method public b(Li/b;)V
    .locals 2

    iget-object v0, p0, Ld/e$g;->a:Li/b$a;

    invoke-interface {v0, p1}, Li/b$a;->b(Li/b;)V

    iget-object p1, p0, Ld/e$g;->b:Ld/e;

    iget-object v0, p1, Ld/e;->q:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    iget-object p1, p1, Ld/e;->f:Landroid/view/Window;

    invoke-virtual {p1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object p1

    iget-object v0, p0, Ld/e$g;->b:Ld/e;

    iget-object v0, v0, Ld/e;->r:Ljava/lang/Runnable;

    invoke-virtual {p1, v0}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    :cond_0
    iget-object p1, p0, Ld/e$g;->b:Ld/e;

    iget-object v0, p1, Ld/e;->p:Landroidx/appcompat/widget/ActionBarContextView;

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Ld/e;->V()V

    iget-object p1, p0, Ld/e$g;->b:Ld/e;

    iget-object v0, p1, Ld/e;->p:Landroidx/appcompat/widget/ActionBarContextView;

    invoke-static {v0}, Landroidx/core/view/t;->d(Landroid/view/View;)Landroidx/core/view/y;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroidx/core/view/y;->a(F)Landroidx/core/view/y;

    move-result-object v0

    iput-object v0, p1, Ld/e;->s:Landroidx/core/view/y;

    iget-object p1, p0, Ld/e$g;->b:Ld/e;

    iget-object p1, p1, Ld/e;->s:Landroidx/core/view/y;

    new-instance v0, Ld/e$g$a;

    invoke-direct {v0, p0}, Ld/e$g$a;-><init>(Ld/e$g;)V

    invoke-virtual {p1, v0}, Landroidx/core/view/y;->f(Landroidx/core/view/z;)Landroidx/core/view/y;

    :cond_1
    iget-object p1, p0, Ld/e$g;->b:Ld/e;

    iget-object v0, p1, Ld/e;->h:Ld/c;

    if-eqz v0, :cond_2

    iget-object p1, p1, Ld/e;->o:Li/b;

    invoke-interface {v0, p1}, Ld/c;->f(Li/b;)V

    :cond_2
    iget-object p1, p0, Ld/e$g;->b:Ld/e;

    const/4 v0, 0x0

    iput-object v0, p1, Ld/e;->o:Li/b;

    iget-object p1, p1, Ld/e;->v:Landroid/view/ViewGroup;

    invoke-static {p1}, Landroidx/core/view/t;->f0(Landroid/view/View;)V

    return-void
.end method

.method public c(Li/b;Landroid/view/Menu;)Z
    .locals 1

    iget-object v0, p0, Ld/e$g;->b:Ld/e;

    iget-object v0, v0, Ld/e;->v:Landroid/view/ViewGroup;

    invoke-static {v0}, Landroidx/core/view/t;->f0(Landroid/view/View;)V

    iget-object v0, p0, Ld/e$g;->a:Li/b$a;

    invoke-interface {v0, p1, p2}, Li/b$a;->c(Li/b;Landroid/view/Menu;)Z

    move-result p1

    return p1
.end method

.method public d(Li/b;Landroid/view/Menu;)Z
    .locals 1

    iget-object v0, p0, Ld/e$g;->a:Li/b$a;

    invoke-interface {v0, p1, p2}, Li/b$a;->d(Li/b;Landroid/view/Menu;)Z

    move-result p1

    return p1
.end method
