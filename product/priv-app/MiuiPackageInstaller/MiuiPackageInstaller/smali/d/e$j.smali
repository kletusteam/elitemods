.class abstract Ld/e$j;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ld/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x400
    name = "j"
.end annotation


# instance fields
.field private a:Landroid/content/BroadcastReceiver;

.field final synthetic b:Ld/e;


# direct methods
.method constructor <init>(Ld/e;)V
    .locals 0

    iput-object p1, p0, Ld/e$j;->b:Ld/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method a()V
    .locals 2

    goto/32 :goto_3

    nop

    :goto_0
    iput-object v0, p0, Ld/e$j;->a:Landroid/content/BroadcastReceiver;

    :goto_1
    goto/32 :goto_4

    nop

    :goto_2
    if-nez v0, :cond_0

    goto/32 :goto_1

    :cond_0
    :try_start_0
    iget-object v1, p0, Ld/e$j;->b:Ld/e;

    iget-object v1, v1, Ld/e;->e:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    goto/32 :goto_5

    nop

    :goto_3
    iget-object v0, p0, Ld/e$j;->a:Landroid/content/BroadcastReceiver;

    goto/32 :goto_2

    nop

    :goto_4
    return-void

    :goto_5
    const/4 v0, 0x0

    goto/32 :goto_0

    nop
.end method

.method abstract b()Landroid/content/IntentFilter;
.end method

.method abstract c()I
.end method

.method abstract d()V
.end method

.method e()V
    .locals 3

    goto/32 :goto_5

    nop

    :goto_0
    invoke-virtual {v0}, Landroid/content/IntentFilter;->countActions()I

    move-result v1

    goto/32 :goto_9

    nop

    :goto_1
    iget-object v1, p0, Ld/e$j;->b:Ld/e;

    goto/32 :goto_a

    nop

    :goto_2
    iget-object v2, p0, Ld/e$j;->a:Landroid/content/BroadcastReceiver;

    goto/32 :goto_7

    nop

    :goto_3
    goto :goto_8

    :goto_4
    goto/32 :goto_d

    nop

    :goto_5
    invoke-virtual {p0}, Ld/e$j;->a()V

    goto/32 :goto_11

    nop

    :goto_6
    return-void

    :goto_7
    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :goto_8
    goto/32 :goto_6

    nop

    :goto_9
    if-eqz v1, :cond_0

    goto/32 :goto_4

    :cond_0
    goto/32 :goto_3

    nop

    :goto_a
    iget-object v1, v1, Ld/e;->e:Landroid/content/Context;

    goto/32 :goto_2

    nop

    :goto_b
    if-nez v0, :cond_1

    goto/32 :goto_8

    :cond_1
    goto/32 :goto_0

    nop

    :goto_c
    invoke-direct {v1, p0}, Ld/e$j$a;-><init>(Ld/e$j;)V

    goto/32 :goto_f

    nop

    :goto_d
    iget-object v1, p0, Ld/e$j;->a:Landroid/content/BroadcastReceiver;

    goto/32 :goto_e

    nop

    :goto_e
    if-eqz v1, :cond_2

    goto/32 :goto_10

    :cond_2
    goto/32 :goto_12

    nop

    :goto_f
    iput-object v1, p0, Ld/e$j;->a:Landroid/content/BroadcastReceiver;

    :goto_10
    goto/32 :goto_1

    nop

    :goto_11
    invoke-virtual {p0}, Ld/e$j;->b()Landroid/content/IntentFilter;

    move-result-object v0

    goto/32 :goto_b

    nop

    :goto_12
    new-instance v1, Ld/e$j$a;

    goto/32 :goto_c

    nop
.end method
