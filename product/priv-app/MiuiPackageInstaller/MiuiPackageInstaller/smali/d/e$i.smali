.class Ld/e$i;
.super Ld/e$j;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ld/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "i"
.end annotation


# instance fields
.field private final c:Landroid/os/PowerManager;

.field final synthetic d:Ld/e;


# direct methods
.method constructor <init>(Ld/e;Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Ld/e$i;->d:Ld/e;

    invoke-direct {p0, p1}, Ld/e$j;-><init>(Ld/e;)V

    invoke-virtual {p2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const-string p2, "power"

    invoke-virtual {p1, p2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/os/PowerManager;

    iput-object p1, p0, Ld/e$i;->c:Landroid/os/PowerManager;

    return-void
.end method


# virtual methods
.method b()Landroid/content/IntentFilter;
    .locals 2

    goto/32 :goto_4

    nop

    :goto_0
    return-object v0

    :goto_1
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    goto/32 :goto_2

    nop

    :goto_2
    const-string v1, "android.os.action.POWER_SAVE_MODE_CHANGED"

    goto/32 :goto_3

    nop

    :goto_3
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    goto/32 :goto_0

    nop

    :goto_4
    new-instance v0, Landroid/content/IntentFilter;

    goto/32 :goto_1

    nop
.end method

.method public c()I
    .locals 1

    iget-object v0, p0, Ld/e$i;->c:Landroid/os/PowerManager;

    invoke-virtual {v0}, Landroid/os/PowerManager;->isPowerSaveMode()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0
.end method

.method public d()V
    .locals 1

    iget-object v0, p0, Ld/e$i;->d:Ld/e;

    invoke-virtual {v0}, Ld/e;->F()Z

    return-void
.end method
