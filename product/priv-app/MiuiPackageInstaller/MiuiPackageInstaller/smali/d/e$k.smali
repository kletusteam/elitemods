.class Ld/e$k;
.super Ld/e$j;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ld/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "k"
.end annotation


# instance fields
.field private final c:Ld/i;

.field final synthetic d:Ld/e;


# direct methods
.method constructor <init>(Ld/e;Ld/i;)V
    .locals 0

    iput-object p1, p0, Ld/e$k;->d:Ld/e;

    invoke-direct {p0, p1}, Ld/e$j;-><init>(Ld/e;)V

    iput-object p2, p0, Ld/e$k;->c:Ld/i;

    return-void
.end method


# virtual methods
.method b()Landroid/content/IntentFilter;
    .locals 2

    goto/32 :goto_6

    nop

    :goto_0
    const-string v1, "android.intent.action.TIME_SET"

    goto/32 :goto_7

    nop

    :goto_1
    const-string v1, "android.intent.action.TIME_TICK"

    goto/32 :goto_4

    nop

    :goto_2
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    goto/32 :goto_0

    nop

    :goto_3
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    goto/32 :goto_1

    nop

    :goto_4
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    goto/32 :goto_5

    nop

    :goto_5
    return-object v0

    :goto_6
    new-instance v0, Landroid/content/IntentFilter;

    goto/32 :goto_2

    nop

    :goto_7
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    goto/32 :goto_8

    nop

    :goto_8
    const-string v1, "android.intent.action.TIMEZONE_CHANGED"

    goto/32 :goto_3

    nop
.end method

.method public c()I
    .locals 1

    iget-object v0, p0, Ld/e$k;->c:Ld/i;

    invoke-virtual {v0}, Ld/i;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0
.end method

.method public d()V
    .locals 1

    iget-object v0, p0, Ld/e$k;->d:Ld/e;

    invoke-virtual {v0}, Ld/e;->F()Z

    return-void
.end method
