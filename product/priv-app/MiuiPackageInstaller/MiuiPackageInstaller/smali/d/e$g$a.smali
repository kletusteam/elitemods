.class Ld/e$g$a;
.super Landroidx/core/view/a0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Ld/e$g;->b(Li/b;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Ld/e$g;


# direct methods
.method constructor <init>(Ld/e$g;)V
    .locals 0

    iput-object p1, p0, Ld/e$g$a;->a:Ld/e$g;

    invoke-direct {p0}, Landroidx/core/view/a0;-><init>()V

    return-void
.end method


# virtual methods
.method public b(Landroid/view/View;)V
    .locals 1

    iget-object p1, p0, Ld/e$g$a;->a:Ld/e$g;

    iget-object p1, p1, Ld/e$g;->b:Ld/e;

    iget-object p1, p1, Ld/e;->p:Landroidx/appcompat/widget/ActionBarContextView;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroidx/appcompat/widget/ActionBarContextView;->setVisibility(I)V

    iget-object p1, p0, Ld/e$g$a;->a:Ld/e$g;

    iget-object p1, p1, Ld/e$g;->b:Ld/e;

    iget-object v0, p1, Ld/e;->q:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    goto :goto_0

    :cond_0
    iget-object p1, p1, Ld/e;->p:Landroidx/appcompat/widget/ActionBarContextView;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    instance-of p1, p1, Landroid/view/View;

    if-eqz p1, :cond_1

    iget-object p1, p0, Ld/e$g$a;->a:Ld/e$g;

    iget-object p1, p1, Ld/e$g;->b:Ld/e;

    iget-object p1, p1, Ld/e;->p:Landroidx/appcompat/widget/ActionBarContextView;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Landroidx/core/view/t;->f0(Landroid/view/View;)V

    :cond_1
    :goto_0
    iget-object p1, p0, Ld/e$g$a;->a:Ld/e$g;

    iget-object p1, p1, Ld/e$g;->b:Ld/e;

    iget-object p1, p1, Ld/e;->p:Landroidx/appcompat/widget/ActionBarContextView;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->removeAllViews()V

    iget-object p1, p0, Ld/e$g$a;->a:Ld/e$g;

    iget-object p1, p1, Ld/e$g;->b:Ld/e;

    iget-object p1, p1, Ld/e;->s:Landroidx/core/view/y;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroidx/core/view/y;->f(Landroidx/core/view/z;)Landroidx/core/view/y;

    iget-object p1, p0, Ld/e$g$a;->a:Ld/e$g;

    iget-object p1, p1, Ld/e$g;->b:Ld/e;

    iput-object v0, p1, Ld/e;->s:Landroidx/core/view/y;

    iget-object p1, p1, Ld/e;->v:Landroid/view/ViewGroup;

    invoke-static {p1}, Landroidx/core/view/t;->f0(Landroid/view/View;)V

    return-void
.end method
