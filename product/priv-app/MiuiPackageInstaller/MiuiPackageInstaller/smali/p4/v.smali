.class public abstract Lp4/v;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lp4/v;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lp4/v<",
            "TT;>;"
        }
    .end annotation

    new-instance v0, Lp4/v$a;

    invoke-direct {v0, p0}, Lp4/v$a;-><init>(Lp4/v;)V

    return-object v0
.end method

.method public abstract b(Lx4/a;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lx4/a;",
            ")TT;"
        }
    .end annotation
.end method

.method public final c(Ljava/lang/Object;)Lp4/j;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Lp4/j;"
        }
    .end annotation

    :try_start_0
    new-instance v0, Ls4/g;

    invoke-direct {v0}, Ls4/g;-><init>()V

    invoke-virtual {p0, v0, p1}, Lp4/v;->d(Lx4/c;Ljava/lang/Object;)V

    invoke-virtual {v0}, Ls4/g;->f0()Lp4/j;

    move-result-object p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    new-instance v0, Lp4/k;

    invoke-direct {v0, p1}, Lp4/k;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method public abstract d(Lx4/c;Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lx4/c;",
            "TT;)V"
        }
    .end annotation
.end method
