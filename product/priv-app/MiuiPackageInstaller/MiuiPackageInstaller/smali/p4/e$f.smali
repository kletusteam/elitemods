.class Lp4/e$f;
.super Lp4/v;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lp4/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "f"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lp4/v<",
        "TT;>;"
    }
.end annotation


# instance fields
.field private a:Lp4/v;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lp4/v<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lp4/v;-><init>()V

    return-void
.end method


# virtual methods
.method public b(Lx4/a;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lx4/a;",
            ")TT;"
        }
    .end annotation

    iget-object v0, p0, Lp4/e$f;->a:Lp4/v;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lp4/v;->b(Lx4/a;)Ljava/lang/Object;

    move-result-object p1

    return-object p1

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1}, Ljava/lang/IllegalStateException;-><init>()V

    throw p1
.end method

.method public d(Lx4/c;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lx4/c;",
            "TT;)V"
        }
    .end annotation

    iget-object v0, p0, Lp4/e$f;->a:Lp4/v;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1, p2}, Lp4/v;->d(Lx4/c;Ljava/lang/Object;)V

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1}, Ljava/lang/IllegalStateException;-><init>()V

    throw p1
.end method

.method public e(Lp4/v;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lp4/v<",
            "TT;>;)V"
        }
    .end annotation

    iget-object v0, p0, Lp4/e$f;->a:Lp4/v;

    if-nez v0, :cond_0

    iput-object p1, p0, Lp4/e$f;->a:Lp4/v;

    return-void

    :cond_0
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1
.end method
