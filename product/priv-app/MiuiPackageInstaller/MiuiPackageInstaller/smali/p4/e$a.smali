.class Lp4/e$a;
.super Lp4/v;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lp4/e;->e(Z)Lp4/v;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lp4/v<",
        "Ljava/lang/Number;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lp4/e;


# direct methods
.method constructor <init>(Lp4/e;)V
    .locals 0

    iput-object p1, p0, Lp4/e$a;->a:Lp4/e;

    invoke-direct {p0}, Lp4/v;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic b(Lx4/a;)Ljava/lang/Object;
    .locals 0

    invoke-virtual {p0, p1}, Lp4/e$a;->e(Lx4/a;)Ljava/lang/Double;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic d(Lx4/c;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Ljava/lang/Number;

    invoke-virtual {p0, p1, p2}, Lp4/e$a;->f(Lx4/c;Ljava/lang/Number;)V

    return-void
.end method

.method public e(Lx4/a;)Ljava/lang/Double;
    .locals 2

    invoke-virtual {p1}, Lx4/a;->a0()Lx4/b;

    move-result-object v0

    sget-object v1, Lx4/b;->i:Lx4/b;

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lx4/a;->W()V

    const/4 p1, 0x0

    return-object p1

    :cond_0
    invoke-virtual {p1}, Lx4/a;->K()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p1

    return-object p1
.end method

.method public f(Lx4/c;Ljava/lang/Number;)V
    .locals 2

    if-nez p2, :cond_0

    invoke-virtual {p1}, Lx4/c;->F()Lx4/c;

    return-void

    :cond_0
    invoke-virtual {p2}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v0

    invoke-static {v0, v1}, Lp4/e;->d(D)V

    invoke-virtual {p1, p2}, Lx4/c;->b0(Ljava/lang/Number;)Lx4/c;

    return-void
.end method
