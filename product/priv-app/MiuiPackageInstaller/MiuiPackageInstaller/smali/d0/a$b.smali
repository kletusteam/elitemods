.class Ld0/a$b;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ld0/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation


# static fields
.field private static final f:[B


# instance fields
.field private final a:Ljava/lang/CharSequence;

.field private final b:Z

.field private final c:I

.field private d:I

.field private e:C


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/16 v0, 0x700

    new-array v1, v0, [B

    sput-object v1, Ld0/a$b;->f:[B

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    sget-object v2, Ld0/a$b;->f:[B

    invoke-static {v1}, Ljava/lang/Character;->getDirectionality(I)B

    move-result v3

    aput-byte v3, v2, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method constructor <init>(Ljava/lang/CharSequence;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Ld0/a$b;->a:Ljava/lang/CharSequence;

    iput-boolean p2, p0, Ld0/a$b;->b:Z

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result p1

    iput p1, p0, Ld0/a$b;->c:I

    return-void
.end method

.method private static c(C)B
    .locals 1

    const/16 v0, 0x700

    if-ge p0, v0, :cond_0

    sget-object v0, Ld0/a$b;->f:[B

    aget-byte p0, v0, p0

    goto :goto_0

    :cond_0
    invoke-static {p0}, Ljava/lang/Character;->getDirectionality(C)B

    move-result p0

    :goto_0
    return p0
.end method

.method private f()B
    .locals 4

    iget v0, p0, Ld0/a$b;->d:I

    :cond_0
    iget v1, p0, Ld0/a$b;->d:I

    const/16 v2, 0x3b

    if-lez v1, :cond_2

    iget-object v3, p0, Ld0/a$b;->a:Ljava/lang/CharSequence;

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Ld0/a$b;->d:I

    invoke-interface {v3, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    iput-char v1, p0, Ld0/a$b;->e:C

    const/16 v3, 0x26

    if-ne v1, v3, :cond_1

    const/16 v0, 0xc

    return v0

    :cond_1
    if-ne v1, v2, :cond_0

    :cond_2
    iput v0, p0, Ld0/a$b;->d:I

    iput-char v2, p0, Ld0/a$b;->e:C

    const/16 v0, 0xd

    return v0
.end method

.method private g()B
    .locals 3

    :goto_0
    iget v0, p0, Ld0/a$b;->d:I

    iget v1, p0, Ld0/a$b;->c:I

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Ld0/a$b;->a:Ljava/lang/CharSequence;

    add-int/lit8 v2, v0, 0x1

    iput v2, p0, Ld0/a$b;->d:I

    invoke-interface {v1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    iput-char v0, p0, Ld0/a$b;->e:C

    const/16 v1, 0x3b

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/16 v0, 0xc

    return v0
.end method

.method private h()B
    .locals 4

    iget v0, p0, Ld0/a$b;->d:I

    :cond_0
    iget v1, p0, Ld0/a$b;->d:I

    const/16 v2, 0x3e

    if-lez v1, :cond_4

    iget-object v3, p0, Ld0/a$b;->a:Ljava/lang/CharSequence;

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Ld0/a$b;->d:I

    invoke-interface {v3, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    iput-char v1, p0, Ld0/a$b;->e:C

    const/16 v3, 0x3c

    if-ne v1, v3, :cond_1

    const/16 v0, 0xc

    return v0

    :cond_1
    if-ne v1, v2, :cond_2

    goto :goto_1

    :cond_2
    const/16 v2, 0x22

    if-eq v1, v2, :cond_3

    const/16 v2, 0x27

    if-ne v1, v2, :cond_0

    :cond_3
    :goto_0
    iget v2, p0, Ld0/a$b;->d:I

    if-lez v2, :cond_0

    iget-object v3, p0, Ld0/a$b;->a:Ljava/lang/CharSequence;

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Ld0/a$b;->d:I

    invoke-interface {v3, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    iput-char v2, p0, Ld0/a$b;->e:C

    if-eq v2, v1, :cond_0

    goto :goto_0

    :cond_4
    :goto_1
    iput v0, p0, Ld0/a$b;->d:I

    iput-char v2, p0, Ld0/a$b;->e:C

    const/16 v0, 0xd

    return v0
.end method

.method private i()B
    .locals 5

    iget v0, p0, Ld0/a$b;->d:I

    :cond_0
    iget v1, p0, Ld0/a$b;->d:I

    iget v2, p0, Ld0/a$b;->c:I

    if-ge v1, v2, :cond_3

    iget-object v2, p0, Ld0/a$b;->a:Ljava/lang/CharSequence;

    add-int/lit8 v3, v1, 0x1

    iput v3, p0, Ld0/a$b;->d:I

    invoke-interface {v2, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    iput-char v1, p0, Ld0/a$b;->e:C

    const/16 v2, 0x3e

    if-ne v1, v2, :cond_1

    const/16 v0, 0xc

    return v0

    :cond_1
    const/16 v2, 0x22

    if-eq v1, v2, :cond_2

    const/16 v2, 0x27

    if-ne v1, v2, :cond_0

    :cond_2
    :goto_0
    iget v2, p0, Ld0/a$b;->d:I

    iget v3, p0, Ld0/a$b;->c:I

    if-ge v2, v3, :cond_0

    iget-object v3, p0, Ld0/a$b;->a:Ljava/lang/CharSequence;

    add-int/lit8 v4, v2, 0x1

    iput v4, p0, Ld0/a$b;->d:I

    invoke-interface {v3, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    iput-char v2, p0, Ld0/a$b;->e:C

    if-eq v2, v1, :cond_0

    goto :goto_0

    :cond_3
    iput v0, p0, Ld0/a$b;->d:I

    const/16 v0, 0x3c

    iput-char v0, p0, Ld0/a$b;->e:C

    const/16 v0, 0xd

    return v0
.end method


# virtual methods
.method a()B
    .locals 3

    goto/32 :goto_b

    nop

    :goto_0
    invoke-static {v0}, Ljava/lang/Character;->charCount(I)I

    move-result v2

    goto/32 :goto_1c

    nop

    :goto_1
    iget v0, p0, Ld0/a$b;->d:I

    goto/32 :goto_17

    nop

    :goto_2
    iget-object v0, p0, Ld0/a$b;->a:Ljava/lang/CharSequence;

    goto/32 :goto_8

    nop

    :goto_3
    if-nez v0, :cond_0

    goto/32 :goto_1b

    :cond_0
    goto/32 :goto_2

    nop

    :goto_4
    if-eq v1, v2, :cond_1

    goto/32 :goto_7

    :cond_1
    goto/32 :goto_11

    nop

    :goto_5
    iget-char v1, p0, Ld0/a$b;->e:C

    goto/32 :goto_16

    nop

    :goto_6
    goto :goto_15

    :goto_7
    goto/32 :goto_1f

    nop

    :goto_8
    iget v1, p0, Ld0/a$b;->d:I

    goto/32 :goto_19

    nop

    :goto_9
    invoke-interface {v0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    goto/32 :goto_13

    nop

    :goto_a
    iget-char v0, p0, Ld0/a$b;->e:C

    goto/32 :goto_21

    nop

    :goto_b
    iget-object v0, p0, Ld0/a$b;->a:Ljava/lang/CharSequence;

    goto/32 :goto_18

    nop

    :goto_c
    iput v0, p0, Ld0/a$b;->d:I

    goto/32 :goto_a

    nop

    :goto_d
    iget-boolean v1, p0, Ld0/a$b;->b:Z

    goto/32 :goto_20

    nop

    :goto_e
    invoke-static {v0}, Ljava/lang/Character;->isLowSurrogate(C)Z

    move-result v0

    goto/32 :goto_3

    nop

    :goto_f
    iget v1, p0, Ld0/a$b;->d:I

    goto/32 :goto_0

    nop

    :goto_10
    invoke-static {v0}, Ljava/lang/Character;->getDirectionality(I)B

    move-result v0

    goto/32 :goto_1a

    nop

    :goto_11
    invoke-direct {p0}, Ld0/a$b;->h()B

    move-result v0

    goto/32 :goto_6

    nop

    :goto_12
    add-int/lit8 v1, v1, -0x1

    goto/32 :goto_9

    nop

    :goto_13
    iput-char v0, p0, Ld0/a$b;->e:C

    goto/32 :goto_e

    nop

    :goto_14
    invoke-direct {p0}, Ld0/a$b;->f()B

    move-result v0

    :goto_15
    goto/32 :goto_22

    nop

    :goto_16
    const/16 v2, 0x3e

    goto/32 :goto_4

    nop

    :goto_17
    add-int/lit8 v0, v0, -0x1

    goto/32 :goto_c

    nop

    :goto_18
    iget v1, p0, Ld0/a$b;->d:I

    goto/32 :goto_12

    nop

    :goto_19
    invoke-static {v0, v1}, Ljava/lang/Character;->codePointBefore(Ljava/lang/CharSequence;I)I

    move-result v0

    goto/32 :goto_f

    nop

    :goto_1a
    return v0

    :goto_1b
    goto/32 :goto_1

    nop

    :goto_1c
    sub-int/2addr v1, v2

    goto/32 :goto_1e

    nop

    :goto_1d
    if-eq v1, v2, :cond_2

    goto/32 :goto_15

    :cond_2
    goto/32 :goto_14

    nop

    :goto_1e
    iput v1, p0, Ld0/a$b;->d:I

    goto/32 :goto_10

    nop

    :goto_1f
    const/16 v2, 0x3b

    goto/32 :goto_1d

    nop

    :goto_20
    if-nez v1, :cond_3

    goto/32 :goto_15

    :cond_3
    goto/32 :goto_5

    nop

    :goto_21
    invoke-static {v0}, Ld0/a$b;->c(C)B

    move-result v0

    goto/32 :goto_d

    nop

    :goto_22
    return v0
.end method

.method b()B
    .locals 3

    goto/32 :goto_3

    nop

    :goto_0
    if-eq v1, v2, :cond_0

    goto/32 :goto_a

    :cond_0
    goto/32 :goto_1c

    nop

    :goto_1
    invoke-static {v0}, Ljava/lang/Character;->charCount(I)I

    move-result v2

    goto/32 :goto_18

    nop

    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto/32 :goto_e

    nop

    :goto_3
    iget-object v0, p0, Ld0/a$b;->a:Ljava/lang/CharSequence;

    goto/32 :goto_5

    nop

    :goto_4
    invoke-static {v0}, Ld0/a$b;->c(C)B

    move-result v0

    goto/32 :goto_11

    nop

    :goto_5
    iget v1, p0, Ld0/a$b;->d:I

    goto/32 :goto_12

    nop

    :goto_6
    iput v1, p0, Ld0/a$b;->d:I

    goto/32 :goto_15

    nop

    :goto_7
    if-nez v1, :cond_1

    goto/32 :goto_20

    :cond_1
    goto/32 :goto_8

    nop

    :goto_8
    iget-char v1, p0, Ld0/a$b;->e:C

    goto/32 :goto_14

    nop

    :goto_9
    goto/16 :goto_20

    :goto_a
    goto/32 :goto_13

    nop

    :goto_b
    iput-char v0, p0, Ld0/a$b;->e:C

    goto/32 :goto_1e

    nop

    :goto_c
    iget v0, p0, Ld0/a$b;->d:I

    goto/32 :goto_2

    nop

    :goto_d
    iget v1, p0, Ld0/a$b;->d:I

    goto/32 :goto_17

    nop

    :goto_e
    iput v0, p0, Ld0/a$b;->d:I

    goto/32 :goto_f

    nop

    :goto_f
    iget-char v0, p0, Ld0/a$b;->e:C

    goto/32 :goto_4

    nop

    :goto_10
    iget-object v0, p0, Ld0/a$b;->a:Ljava/lang/CharSequence;

    goto/32 :goto_d

    nop

    :goto_11
    iget-boolean v1, p0, Ld0/a$b;->b:Z

    goto/32 :goto_7

    nop

    :goto_12
    invoke-interface {v0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    goto/32 :goto_b

    nop

    :goto_13
    const/16 v2, 0x26

    goto/32 :goto_19

    nop

    :goto_14
    const/16 v2, 0x3c

    goto/32 :goto_0

    nop

    :goto_15
    invoke-static {v0}, Ljava/lang/Character;->getDirectionality(I)B

    move-result v0

    goto/32 :goto_1a

    nop

    :goto_16
    if-nez v0, :cond_2

    goto/32 :goto_1b

    :cond_2
    goto/32 :goto_10

    nop

    :goto_17
    invoke-static {v0, v1}, Ljava/lang/Character;->codePointAt(Ljava/lang/CharSequence;I)I

    move-result v0

    goto/32 :goto_1d

    nop

    :goto_18
    add-int/2addr v1, v2

    goto/32 :goto_6

    nop

    :goto_19
    if-eq v1, v2, :cond_3

    goto/32 :goto_20

    :cond_3
    goto/32 :goto_1f

    nop

    :goto_1a
    return v0

    :goto_1b
    goto/32 :goto_c

    nop

    :goto_1c
    invoke-direct {p0}, Ld0/a$b;->i()B

    move-result v0

    goto/32 :goto_9

    nop

    :goto_1d
    iget v1, p0, Ld0/a$b;->d:I

    goto/32 :goto_1

    nop

    :goto_1e
    invoke-static {v0}, Ljava/lang/Character;->isHighSurrogate(C)Z

    move-result v0

    goto/32 :goto_16

    nop

    :goto_1f
    invoke-direct {p0}, Ld0/a$b;->g()B

    move-result v0

    :goto_20
    goto/32 :goto_21

    nop

    :goto_21
    return v0
.end method

.method d()I
    .locals 8

    goto/32 :goto_4

    nop

    :goto_0
    goto :goto_3

    :pswitch_0
    goto/32 :goto_17

    nop

    :goto_1
    move v3, v0

    goto/32 :goto_8

    nop

    :goto_2
    move v5, v4

    :goto_3
    goto/32 :goto_1b

    nop

    :goto_4
    const/4 v0, 0x0

    goto/32 :goto_28

    nop

    :goto_5
    iget v7, p0, Ld0/a$b;->c:I

    goto/32 :goto_e

    nop

    :goto_6
    move v4, v1

    goto/32 :goto_2b

    nop

    :goto_7
    if-eq v3, v5, :cond_0

    goto/32 :goto_35

    :cond_0
    goto/32 :goto_f

    nop

    :goto_8
    move v4, v3

    goto/32 :goto_2

    nop

    :goto_9
    const/4 v2, 0x1

    goto/32 :goto_1

    nop

    :goto_a
    add-int/lit8 v5, v5, -0x1

    goto/32 :goto_21

    nop

    :goto_b
    goto :goto_12

    :pswitch_1
    goto/32 :goto_7

    nop

    :goto_c
    move v4, v2

    goto/32 :goto_37

    nop

    :goto_d
    if-ne v6, v7, :cond_1

    goto/32 :goto_2c

    :cond_1
    goto/32 :goto_19

    nop

    :goto_e
    if-lt v6, v7, :cond_2

    goto/32 :goto_2a

    :cond_2
    goto/32 :goto_31

    nop

    :goto_f
    return v2

    :pswitch_2
    goto/32 :goto_1d

    nop

    :goto_10
    if-ne v6, v2, :cond_3

    goto/32 :goto_2c

    :cond_3
    goto/32 :goto_23

    nop

    :goto_11
    return v4

    :goto_12
    goto/32 :goto_13

    nop

    :goto_13
    iget v4, p0, Ld0/a$b;->d:I

    goto/32 :goto_16

    nop

    :goto_14
    add-int/lit8 v5, v5, 0x1

    goto/32 :goto_6

    nop

    :goto_15
    goto :goto_1f

    :pswitch_3
    goto/32 :goto_18

    nop

    :goto_16
    if-gtz v4, :cond_4

    goto/32 :goto_22

    :cond_4
    goto/32 :goto_32

    nop

    :goto_17
    add-int/lit8 v5, v5, 0x1

    goto/32 :goto_c

    nop

    :goto_18
    add-int/lit8 v5, v5, -0x1

    goto/32 :goto_1c

    nop

    :goto_19
    const/16 v7, 0x9

    goto/32 :goto_25

    nop

    :goto_1a
    if-eqz v3, :cond_5

    goto/32 :goto_2e

    :cond_5
    goto/32 :goto_2d

    nop

    :goto_1b
    iget v6, p0, Ld0/a$b;->d:I

    goto/32 :goto_5

    nop

    :goto_1c
    move v4, v0

    goto/32 :goto_0

    nop

    :goto_1d
    if-eq v3, v5, :cond_6

    goto/32 :goto_35

    :cond_6
    goto/32 :goto_34

    nop

    :goto_1e
    return v1

    :goto_1f
    goto/32 :goto_3a

    nop

    :goto_20
    if-eqz v5, :cond_7

    goto/32 :goto_1f

    :cond_7
    goto/32 :goto_2f

    nop

    :goto_21
    goto :goto_12

    :goto_22
    goto/32 :goto_3b

    nop

    :goto_23
    const/4 v7, 0x2

    goto/32 :goto_d

    nop

    :goto_24
    invoke-virtual {p0}, Ld0/a$b;->b()B

    move-result v6

    goto/32 :goto_38

    nop

    :goto_25
    if-ne v6, v7, :cond_8

    goto/32 :goto_3

    :cond_8
    packed-switch v6, :pswitch_data_0

    goto/32 :goto_15

    nop

    :goto_26
    const/4 v1, -0x1

    goto/32 :goto_9

    nop

    :goto_27
    if-eqz v5, :cond_9

    goto/32 :goto_1f

    :cond_9
    goto/32 :goto_1e

    nop

    :goto_28
    iput v0, p0, Ld0/a$b;->d:I

    goto/32 :goto_26

    nop

    :goto_29
    goto/16 :goto_3

    :goto_2a
    goto/32 :goto_1a

    nop

    :goto_2b
    goto/16 :goto_3

    :goto_2c
    goto/32 :goto_20

    nop

    :goto_2d
    return v0

    :goto_2e
    goto/32 :goto_39

    nop

    :goto_2f
    return v2

    :goto_30
    goto/32 :goto_27

    nop

    :goto_31
    if-eqz v3, :cond_a

    goto/32 :goto_2a

    :cond_a
    goto/32 :goto_24

    nop

    :goto_32
    invoke-virtual {p0}, Ld0/a$b;->a()B

    move-result v4

    packed-switch v4, :pswitch_data_1

    goto/32 :goto_36

    nop

    :goto_33
    add-int/lit8 v5, v5, 0x1

    goto/32 :goto_b

    nop

    :goto_34
    return v1

    :goto_35
    goto/32 :goto_a

    nop

    :goto_36
    goto/16 :goto_12

    :pswitch_4
    goto/32 :goto_33

    nop

    :goto_37
    goto/16 :goto_3

    :pswitch_5
    goto/32 :goto_14

    nop

    :goto_38
    if-nez v6, :cond_b

    goto/32 :goto_30

    :cond_b
    goto/32 :goto_10

    nop

    :goto_39
    if-nez v4, :cond_c

    goto/32 :goto_12

    :cond_c
    goto/32 :goto_11

    nop

    :goto_3a
    move v3, v5

    goto/32 :goto_29

    nop

    :goto_3b
    return v0

    nop

    :pswitch_data_0
    .packed-switch 0xe
        :pswitch_5
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0xe
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_4
    .end packed-switch
.end method

.method e()I
    .locals 7

    goto/32 :goto_2a

    nop

    :goto_0
    invoke-virtual {p0}, Ld0/a$b;->a()B

    move-result v3

    goto/32 :goto_10

    nop

    :goto_1
    if-ne v3, v6, :cond_0

    goto/32 :goto_1b

    :cond_0
    goto/32 :goto_12

    nop

    :goto_2
    iget v3, p0, Ld0/a$b;->d:I

    goto/32 :goto_15

    nop

    :goto_3
    const/4 v5, 0x1

    goto/32 :goto_26

    nop

    :goto_4
    if-eq v2, v1, :cond_1

    goto/32 :goto_23

    :cond_1
    goto/32 :goto_22

    nop

    :goto_5
    goto :goto_14

    :goto_6
    goto/32 :goto_2b

    nop

    :goto_7
    move v2, v1

    :goto_8
    goto/32 :goto_2

    nop

    :goto_9
    move v1, v0

    :goto_a
    goto/32 :goto_7

    nop

    :goto_b
    iput v0, p0, Ld0/a$b;->d:I

    goto/32 :goto_c

    nop

    :goto_c
    const/4 v0, 0x0

    goto/32 :goto_9

    nop

    :goto_d
    if-eq v2, v1, :cond_2

    goto/32 :goto_23

    :cond_2
    goto/32 :goto_19

    nop

    :goto_e
    if-eqz v2, :cond_3

    goto/32 :goto_8

    :cond_3
    goto/32 :goto_21

    nop

    :goto_f
    if-ne v3, v6, :cond_4

    goto/32 :goto_8

    :cond_4
    packed-switch v3, :pswitch_data_0

    goto/32 :goto_e

    nop

    :goto_10
    const/4 v4, -0x1

    goto/32 :goto_1d

    nop

    :goto_11
    if-eqz v1, :cond_5

    goto/32 :goto_20

    :cond_5
    goto/32 :goto_1f

    nop

    :goto_12
    const/16 v6, 0x9

    goto/32 :goto_f

    nop

    :goto_13
    if-eqz v2, :cond_6

    goto/32 :goto_8

    :cond_6
    :goto_14
    goto/32 :goto_24

    nop

    :goto_15
    if-gtz v3, :cond_7

    goto/32 :goto_25

    :cond_7
    goto/32 :goto_0

    nop

    :goto_16
    const/4 v6, 0x2

    goto/32 :goto_1

    nop

    :goto_17
    goto :goto_8

    :pswitch_0
    goto/32 :goto_d

    nop

    :goto_18
    if-eqz v2, :cond_8

    goto/32 :goto_8

    :cond_8
    goto/32 :goto_5

    nop

    :goto_19
    return v5

    :pswitch_1
    goto/32 :goto_4

    nop

    :goto_1a
    goto :goto_8

    :goto_1b
    goto/32 :goto_11

    nop

    :goto_1c
    add-int/lit8 v1, v1, 0x1

    goto/32 :goto_17

    nop

    :goto_1d
    if-nez v3, :cond_9

    goto/32 :goto_6

    :cond_9
    goto/32 :goto_3

    nop

    :goto_1e
    add-int/lit8 v1, v1, -0x1

    goto/32 :goto_1a

    nop

    :goto_1f
    return v5

    :goto_20
    goto/32 :goto_18

    nop

    :goto_21
    goto :goto_14

    :pswitch_2
    goto/32 :goto_1c

    nop

    :goto_22
    return v4

    :goto_23
    goto/32 :goto_1e

    nop

    :goto_24
    goto/16 :goto_a

    :goto_25
    goto/32 :goto_27

    nop

    :goto_26
    if-ne v3, v5, :cond_a

    goto/32 :goto_1b

    :cond_a
    goto/32 :goto_16

    nop

    :goto_27
    return v0

    :pswitch_data_0
    .packed-switch 0xe
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch

    :goto_28
    return v4

    :goto_29
    goto/32 :goto_13

    nop

    :goto_2a
    iget v0, p0, Ld0/a$b;->c:I

    goto/32 :goto_b

    nop

    :goto_2b
    if-eqz v1, :cond_b

    goto/32 :goto_29

    :cond_b
    goto/32 :goto_28

    nop
.end method
