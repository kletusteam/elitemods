.class Le8/c;
.super Ljava/lang/Object;


# direct methods
.method public static a(Ll8/p;Ljava/lang/Object;Ld8/d;)Ld8/d;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            "T:",
            "Ljava/lang/Object;",
            ">(",
            "Ll8/p<",
            "-TR;-",
            "Ld8/d<",
            "-TT;>;+",
            "Ljava/lang/Object;",
            ">;TR;",
            "Ld8/d<",
            "-TT;>;)",
            "Ld8/d<",
            "La8/v;",
            ">;"
        }
    .end annotation

    const-string v0, "<this>"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "completion"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p2}, Lf8/h;->a(Ld8/d;)Ld8/d;

    move-result-object p2

    instance-of v0, p0, Lf8/a;

    if-eqz v0, :cond_0

    check-cast p0, Lf8/a;

    invoke-virtual {p0, p1, p2}, Lf8/a;->b(Ljava/lang/Object;Ld8/d;)Ld8/d;

    move-result-object p0

    goto :goto_0

    :cond_0
    invoke-interface {p2}, Ld8/d;->c()Ld8/g;

    move-result-object v0

    sget-object v1, Ld8/h;->a:Ld8/h;

    if-ne v0, v1, :cond_1

    new-instance v0, Le8/c$a;

    invoke-direct {v0, p2, p0, p1}, Le8/c$a;-><init>(Ld8/d;Ll8/p;Ljava/lang/Object;)V

    move-object p0, v0

    goto :goto_0

    :cond_1
    new-instance v1, Le8/c$b;

    invoke-direct {v1, p2, v0, p0, p1}, Le8/c$b;-><init>(Ld8/d;Ld8/g;Ll8/p;Ljava/lang/Object;)V

    move-object p0, v1

    :goto_0
    return-object p0
.end method

.method public static b(Ld8/d;)Ld8/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ld8/d<",
            "-TT;>;)",
            "Ld8/d<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "<this>"

    invoke-static {p0, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    instance-of v0, p0, Lf8/d;

    if-eqz v0, :cond_0

    move-object v0, p0

    check-cast v0, Lf8/d;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lf8/d;->p()Ld8/d;

    move-result-object v0

    if-nez v0, :cond_1

    goto :goto_1

    :cond_1
    move-object p0, v0

    :cond_2
    :goto_1
    return-object p0
.end method
