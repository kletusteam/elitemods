.class public abstract Lg1/c;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lg1/c$b;,
        Lg1/c$a;
    }
.end annotation


# static fields
.field private static final g:[Ljava/lang/String;


# instance fields
.field a:I

.field b:[I

.field c:[Ljava/lang/String;

.field d:[I

.field e:Z

.field f:Z


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/16 v0, 0x80

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Lg1/c;->g:[Ljava/lang/String;

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    const/16 v2, 0x1f

    if-gt v1, v2, :cond_0

    sget-object v2, Lg1/c;->g:[Ljava/lang/String;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    const-string v4, "\\u%04x"

    invoke-static {v4, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    sget-object v0, Lg1/c;->g:[Ljava/lang/String;

    const/16 v1, 0x22

    const-string v2, "\\\""

    aput-object v2, v0, v1

    const/16 v1, 0x5c

    const-string v2, "\\\\"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "\\t"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "\\b"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "\\n"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "\\r"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "\\f"

    aput-object v2, v0, v1

    return-void
.end method

.method constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x20

    new-array v1, v0, [I

    iput-object v1, p0, Lg1/c;->b:[I

    new-array v1, v0, [Ljava/lang/String;

    iput-object v1, p0, Lg1/c;->c:[Ljava/lang/String;

    new-array v0, v0, [I

    iput-object v0, p0, Lg1/c;->d:[I

    return-void
.end method

.method public static F(Ldc/g;)Lg1/c;
    .locals 1

    new-instance v0, Lg1/e;

    invoke-direct {v0, p0}, Lg1/e;-><init>(Ldc/g;)V

    return-object v0
.end method

.method private static V(Ldc/f;Ljava/lang/String;)V
    .locals 7

    sget-object v0, Lg1/c;->g:[Ljava/lang/String;

    const/16 v1, 0x22

    invoke-interface {p0, v1}, Ldc/f;->v(I)Ldc/f;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x0

    move v4, v3

    :goto_0
    if-ge v3, v2, :cond_5

    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x80

    if-ge v5, v6, :cond_0

    aget-object v5, v0, v5

    if-nez v5, :cond_2

    goto :goto_2

    :cond_0
    const/16 v6, 0x2028

    if-ne v5, v6, :cond_1

    const-string v5, "\\u2028"

    goto :goto_1

    :cond_1
    const/16 v6, 0x2029

    if-ne v5, v6, :cond_4

    const-string v5, "\\u2029"

    :cond_2
    :goto_1
    if-ge v4, v3, :cond_3

    invoke-interface {p0, p1, v4, v3}, Ldc/f;->f(Ljava/lang/String;II)Ldc/f;

    :cond_3
    invoke-interface {p0, v5}, Ldc/f;->O(Ljava/lang/String;)Ldc/f;

    add-int/lit8 v4, v3, 0x1

    :cond_4
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_5
    if-ge v4, v2, :cond_6

    invoke-interface {p0, p1, v4, v2}, Ldc/f;->f(Ljava/lang/String;II)Ldc/f;

    :cond_6
    invoke-interface {p0, v1}, Ldc/f;->v(I)Ldc/f;

    return-void
.end method

.method static synthetic b(Ldc/f;Ljava/lang/String;)V
    .locals 0

    invoke-static {p0, p1}, Lg1/c;->V(Ldc/f;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public abstract B()I
.end method

.method public abstract C()Ljava/lang/String;
.end method

.method public abstract D()Ljava/lang/String;
.end method

.method public abstract J()Lg1/c$b;
.end method

.method final K(I)V
    .locals 3

    goto/32 :goto_10

    nop

    :goto_0
    array-length v2, v1

    goto/32 :goto_18

    nop

    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    goto/32 :goto_2

    nop

    :goto_2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_15

    nop

    :goto_3
    return-void

    :goto_4
    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    goto/32 :goto_b

    nop

    :goto_5
    iput-object v0, p0, Lg1/c;->d:[I

    goto/32 :goto_25

    nop

    :goto_6
    invoke-direct {p1, v0}, Lg1/a;-><init>(Ljava/lang/String;)V

    goto/32 :goto_13

    nop

    :goto_7
    array-length v1, v0

    goto/32 :goto_c

    nop

    :goto_8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_d

    nop

    :goto_9
    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v0

    goto/32 :goto_5

    nop

    :goto_a
    array-length v1, v0

    goto/32 :goto_1e

    nop

    :goto_b
    check-cast v0, [Ljava/lang/String;

    goto/32 :goto_11

    nop

    :goto_c
    mul-int/lit8 v1, v1, 0x2

    goto/32 :goto_9

    nop

    :goto_d
    invoke-virtual {p0}, Lg1/c;->r()Ljava/lang/String;

    move-result-object v1

    goto/32 :goto_f

    nop

    :goto_e
    aput p1, v0, v1

    goto/32 :goto_3

    nop

    :goto_f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_16

    nop

    :goto_10
    iget v0, p0, Lg1/c;->a:I

    goto/32 :goto_1b

    nop

    :goto_11
    iput-object v0, p0, Lg1/c;->c:[Ljava/lang/String;

    goto/32 :goto_1d

    nop

    :goto_12
    mul-int/lit8 v0, v0, 0x2

    goto/32 :goto_23

    nop

    :goto_13
    throw p1

    :goto_14
    goto/32 :goto_22

    nop

    :goto_15
    const-string v1, "Nesting too deep at "

    goto/32 :goto_8

    nop

    :goto_16
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/32 :goto_6

    nop

    :goto_17
    iget v1, p0, Lg1/c;->a:I

    goto/32 :goto_1c

    nop

    :goto_18
    if-eq v0, v2, :cond_0

    goto/32 :goto_14

    :cond_0
    goto/32 :goto_27

    nop

    :goto_19
    iput-object v0, p0, Lg1/c;->b:[I

    goto/32 :goto_1a

    nop

    :goto_1a
    iget-object v0, p0, Lg1/c;->c:[Ljava/lang/String;

    goto/32 :goto_a

    nop

    :goto_1b
    iget-object v1, p0, Lg1/c;->b:[I

    goto/32 :goto_0

    nop

    :goto_1c
    add-int/lit8 v2, v1, 0x1

    goto/32 :goto_1f

    nop

    :goto_1d
    iget-object v0, p0, Lg1/c;->d:[I

    goto/32 :goto_7

    nop

    :goto_1e
    mul-int/lit8 v1, v1, 0x2

    goto/32 :goto_4

    nop

    :goto_1f
    iput v2, p0, Lg1/c;->a:I

    goto/32 :goto_e

    nop

    :goto_20
    if-ne v0, v2, :cond_1

    goto/32 :goto_26

    :cond_1
    goto/32 :goto_24

    nop

    :goto_21
    new-instance p1, Lg1/a;

    goto/32 :goto_1

    nop

    :goto_22
    iget-object v0, p0, Lg1/c;->b:[I

    goto/32 :goto_17

    nop

    :goto_23
    invoke-static {v1, v0}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v0

    goto/32 :goto_19

    nop

    :goto_24
    array-length v0, v1

    goto/32 :goto_12

    nop

    :goto_25
    goto :goto_14

    :goto_26
    goto/32 :goto_21

    nop

    :goto_27
    const/16 v2, 0x100

    goto/32 :goto_20

    nop
.end method

.method public abstract L(Lg1/c$a;)I
.end method

.method public abstract M()V
.end method

.method public abstract U()V
.end method

.method final W(Ljava/lang/String;)Lg1/b;
    .locals 2

    goto/32 :goto_a

    nop

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    goto/32 :goto_3

    nop

    :goto_1
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_6

    nop

    :goto_2
    throw v0

    :goto_3
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    goto/32 :goto_8

    nop

    :goto_4
    invoke-direct {v0, p1}, Lg1/b;-><init>(Ljava/lang/String;)V

    goto/32 :goto_2

    nop

    :goto_5
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_4

    nop

    :goto_6
    invoke-virtual {p0}, Lg1/c;->r()Ljava/lang/String;

    move-result-object p1

    goto/32 :goto_9

    nop

    :goto_7
    const-string p1, " at path "

    goto/32 :goto_1

    nop

    :goto_8
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_7

    nop

    :goto_9
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/32 :goto_5

    nop

    :goto_a
    new-instance v0, Lg1/b;

    goto/32 :goto_0

    nop
.end method

.method public abstract g()V
.end method

.method public abstract m()V
.end method

.method public abstract n()V
.end method

.method public abstract p()V
.end method

.method public final r()Ljava/lang/String;
    .locals 4

    iget v0, p0, Lg1/c;->a:I

    iget-object v1, p0, Lg1/c;->b:[I

    iget-object v2, p0, Lg1/c;->c:[Ljava/lang/String;

    iget-object v3, p0, Lg1/c;->d:[I

    invoke-static {v0, v1, v2, v3}, Lg1/d;->a(I[I[Ljava/lang/String;[I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public abstract s()Z
.end method

.method public abstract y()Z
.end method

.method public abstract z()D
.end method
