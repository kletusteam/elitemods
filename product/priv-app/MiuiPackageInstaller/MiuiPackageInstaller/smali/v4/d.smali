.class public final Lv4/d;
.super Ljava/lang/Object;


# static fields
.field public static final a:Z

.field public static final b:Ls4/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ls4/d<",
            "+",
            "Ljava/util/Date;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:Ls4/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ls4/d<",
            "+",
            "Ljava/util/Date;",
            ">;"
        }
    .end annotation
.end field

.field public static final d:Lp4/w;

.field public static final e:Lp4/w;

.field public static final f:Lp4/w;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    :try_start_0
    const-string v0, "java.sql.Date"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    goto :goto_0

    :catch_0
    const/4 v0, 0x0

    :goto_0
    sput-boolean v0, Lv4/d;->a:Z

    if-eqz v0, :cond_0

    new-instance v0, Lv4/d$a;

    const-class v1, Ljava/sql/Date;

    invoke-direct {v0, v1}, Lv4/d$a;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lv4/d;->b:Ls4/d;

    new-instance v0, Lv4/d$b;

    const-class v1, Ljava/sql/Timestamp;

    invoke-direct {v0, v1}, Lv4/d$b;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lv4/d;->c:Ls4/d;

    sget-object v0, Lv4/a;->b:Lp4/w;

    sput-object v0, Lv4/d;->d:Lp4/w;

    sget-object v0, Lv4/b;->b:Lp4/w;

    sput-object v0, Lv4/d;->e:Lp4/w;

    sget-object v0, Lv4/c;->b:Lp4/w;

    goto :goto_1

    :cond_0
    const/4 v0, 0x0

    sput-object v0, Lv4/d;->b:Ls4/d;

    sput-object v0, Lv4/d;->c:Ls4/d;

    sput-object v0, Lv4/d;->d:Lp4/w;

    sput-object v0, Lv4/d;->e:Lp4/w;

    :goto_1
    sput-object v0, Lv4/d;->f:Lp4/w;

    return-void
.end method
