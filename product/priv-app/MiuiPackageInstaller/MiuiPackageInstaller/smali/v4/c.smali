.class Lv4/c;
.super Lp4/v;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lp4/v<",
        "Ljava/sql/Timestamp;",
        ">;"
    }
.end annotation


# static fields
.field static final b:Lp4/w;


# instance fields
.field private final a:Lp4/v;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lp4/v<",
            "Ljava/util/Date;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lv4/c$a;

    invoke-direct {v0}, Lv4/c$a;-><init>()V

    sput-object v0, Lv4/c;->b:Lp4/w;

    return-void
.end method

.method private constructor <init>(Lp4/v;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lp4/v<",
            "Ljava/util/Date;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Lp4/v;-><init>()V

    iput-object p1, p0, Lv4/c;->a:Lp4/v;

    return-void
.end method

.method synthetic constructor <init>(Lp4/v;Lv4/c$a;)V
    .locals 0

    invoke-direct {p0, p1}, Lv4/c;-><init>(Lp4/v;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic b(Lx4/a;)Ljava/lang/Object;
    .locals 0

    invoke-virtual {p0, p1}, Lv4/c;->e(Lx4/a;)Ljava/sql/Timestamp;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic d(Lx4/c;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Ljava/sql/Timestamp;

    invoke-virtual {p0, p1, p2}, Lv4/c;->f(Lx4/c;Ljava/sql/Timestamp;)V

    return-void
.end method

.method public e(Lx4/a;)Ljava/sql/Timestamp;
    .locals 3

    iget-object v0, p0, Lv4/c;->a:Lp4/v;

    invoke-virtual {v0, p1}, Lp4/v;->b(Lx4/a;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Date;

    if-eqz p1, :cond_0

    new-instance v0, Ljava/sql/Timestamp;

    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Ljava/sql/Timestamp;-><init>(J)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public f(Lx4/c;Ljava/sql/Timestamp;)V
    .locals 1

    iget-object v0, p0, Lv4/c;->a:Lp4/v;

    invoke-virtual {v0, p1, p2}, Lp4/v;->d(Lx4/c;Ljava/lang/Object;)V

    return-void
.end method
