.class public abstract Lv8/a0;
.super Ld8/a;

# interfaces
.implements Ld8/e;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lv8/a0$a;
    }
.end annotation


# static fields
.field public static final a:Lv8/a0$a;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lv8/a0$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lv8/a0$a;-><init>(Lm8/g;)V

    sput-object v0, Lv8/a0;->a:Lv8/a0$a;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    sget-object v0, Ld8/e;->b0:Ld8/e$b;

    invoke-direct {p0, v0}, Ld8/a;-><init>(Ld8/g$c;)V

    return-void
.end method


# virtual methods
.method public final K(Ld8/d;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ld8/d<",
            "*>;)V"
        }
    .end annotation

    check-cast p1, Lkotlinx/coroutines/internal/e;

    invoke-virtual {p1}, Lkotlinx/coroutines/internal/e;->p()V

    return-void
.end method

.method public abstract U(Ld8/g;Ljava/lang/Runnable;)V
.end method

.method public V(Ld8/g;)Z
    .locals 0

    const/4 p1, 0x1

    return p1
.end method

.method public W(I)Lv8/a0;
    .locals 1

    invoke-static {p1}, Lkotlinx/coroutines/internal/j;->a(I)V

    new-instance v0, Lkotlinx/coroutines/internal/i;

    invoke-direct {v0, p0, p1}, Lkotlinx/coroutines/internal/i;-><init>(Lv8/a0;I)V

    return-object v0
.end method

.method public get(Ld8/g$c;)Ld8/g$b;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Ld8/g$b;",
            ">(",
            "Ld8/g$c<",
            "TE;>;)TE;"
        }
    .end annotation

    invoke-static {p0, p1}, Ld8/e$a;->a(Ld8/e;Ld8/g$c;)Ld8/g$b;

    move-result-object p1

    return-object p1
.end method

.method public minusKey(Ld8/g$c;)Ld8/g;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ld8/g$c<",
            "*>;)",
            "Ld8/g;"
        }
    .end annotation

    invoke-static {p0, p1}, Ld8/e$a;->b(Ld8/e;Ld8/g$c;)Ld8/g;

    move-result-object p1

    return-object p1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p0}, Lv8/i0;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-static {p0}, Lv8/i0;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final y(Ld8/d;)Ld8/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ld8/d<",
            "-TT;>;)",
            "Ld8/d<",
            "TT;>;"
        }
    .end annotation

    new-instance v0, Lkotlinx/coroutines/internal/e;

    invoke-direct {v0, p0, p1}, Lkotlinx/coroutines/internal/e;-><init>(Lv8/a0;Ld8/d;)V

    return-object v0
.end method
