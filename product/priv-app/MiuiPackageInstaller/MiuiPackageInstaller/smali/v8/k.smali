.class public Lv8/k;
.super Lv8/r0;

# interfaces
.implements Lv8/j;
.implements Lf8/e;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lv8/r0<",
        "TT;>;",
        "Lv8/j<",
        "TT;>;",
        "Lf8/e;"
    }
.end annotation


# static fields
.field private static final synthetic g:Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

.field private static final synthetic h:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;


# instance fields
.field private volatile synthetic _decision:I

.field private volatile synthetic _state:Ljava/lang/Object;

.field private final d:Ld8/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ld8/d<",
            "TT;>;"
        }
    .end annotation
.end field

.field private final e:Ld8/g;

.field private f:Lv8/u0;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const-class v0, Lv8/k;

    const-string v1, "_decision"

    invoke-static {v0, v1}, Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;->newUpdater(Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

    move-result-object v0

    sput-object v0, Lv8/k;->g:Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

    const-class v0, Lv8/k;

    const-class v1, Ljava/lang/Object;

    const-string v2, "_state"

    invoke-static {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->newUpdater(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    move-result-object v0

    sput-object v0, Lv8/k;->h:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    return-void
.end method

.method public constructor <init>(Ld8/d;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ld8/d<",
            "-TT;>;I)V"
        }
    .end annotation

    invoke-direct {p0, p2}, Lv8/r0;-><init>(I)V

    iput-object p1, p0, Lv8/k;->d:Ld8/d;

    invoke-interface {p1}, Ld8/d;->c()Ld8/g;

    move-result-object p1

    iput-object p1, p0, Lv8/k;->e:Ld8/g;

    const/4 p1, 0x0

    iput p1, p0, Lv8/k;->_decision:I

    sget-object p1, Lv8/d;->a:Lv8/d;

    iput-object p1, p0, Lv8/k;->_state:Ljava/lang/Object;

    return-void
.end method

.method private final B()Z
    .locals 1

    iget v0, p0, Lv8/r0;->c:I

    invoke-static {v0}, Lv8/s0;->c(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lv8/k;->d:Ld8/d;

    check-cast v0, Lkotlinx/coroutines/internal/e;

    invoke-virtual {v0}, Lkotlinx/coroutines/internal/e;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private final C(Ll8/l;)Lv8/h;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ll8/l<",
            "-",
            "Ljava/lang/Throwable;",
            "La8/v;",
            ">;)",
            "Lv8/h;"
        }
    .end annotation

    instance-of v0, p1, Lv8/h;

    if-eqz v0, :cond_0

    check-cast p1, Lv8/h;

    goto :goto_0

    :cond_0
    new-instance v0, Lv8/h1;

    invoke-direct {v0, p1}, Lv8/h1;-><init>(Ll8/l;)V

    move-object p1, v0

    :goto_0
    return-object p1
.end method

.method private final D(Ll8/l;Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ll8/l<",
            "-",
            "Ljava/lang/Throwable;",
            "La8/v;",
            ">;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "It\'s prohibited to register multiple handlers, tried to register "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, ", already has "

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private final G()V
    .locals 3

    iget-object v0, p0, Lv8/k;->d:Ld8/d;

    instance-of v1, v0, Lkotlinx/coroutines/internal/e;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    check-cast v0, Lkotlinx/coroutines/internal/e;

    goto :goto_0

    :cond_0
    move-object v0, v2

    :goto_0
    if-nez v0, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {v0, p0}, Lkotlinx/coroutines/internal/e;->q(Lv8/j;)Ljava/lang/Throwable;

    move-result-object v2

    :goto_1
    if-nez v2, :cond_2

    return-void

    :cond_2
    invoke-virtual {p0}, Lv8/k;->r()V

    invoke-virtual {p0, v2}, Lv8/k;->p(Ljava/lang/Throwable;)Z

    return-void
.end method

.method private final H(Ljava/lang/Object;ILl8/l;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "I",
            "Ll8/l<",
            "-",
            "Ljava/lang/Throwable;",
            "La8/v;",
            ">;)V"
        }
    .end annotation

    :goto_0
    iget-object v0, p0, Lv8/k;->_state:Ljava/lang/Object;

    instance-of v1, v0, Lv8/y1;

    if-eqz v1, :cond_1

    move-object v3, v0

    check-cast v3, Lv8/y1;

    const/4 v7, 0x0

    move-object v2, p0

    move-object v4, p1

    move v5, p2

    move-object v6, p3

    invoke-direct/range {v2 .. v7}, Lv8/k;->J(Lv8/y1;Ljava/lang/Object;ILl8/l;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    sget-object v2, Lv8/k;->h:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    invoke-static {v2, p0, v0, v1}, La8/o;->a(Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lv8/k;->s()V

    invoke-direct {p0, p2}, Lv8/k;->t(I)V

    return-void

    :cond_1
    instance-of p2, v0, Lv8/m;

    if-eqz p2, :cond_3

    check-cast v0, Lv8/m;

    invoke-virtual {v0}, Lv8/m;->c()Z

    move-result p2

    if-eqz p2, :cond_3

    if-nez p3, :cond_2

    goto :goto_1

    :cond_2
    iget-object p1, v0, Lv8/t;->a:Ljava/lang/Throwable;

    invoke-virtual {p0, p3, p1}, Lv8/k;->o(Ll8/l;Ljava/lang/Throwable;)V

    :goto_1
    return-void

    :cond_3
    invoke-direct {p0, p1}, Lv8/k;->l(Ljava/lang/Object;)Ljava/lang/Void;

    new-instance p1, La8/d;

    invoke-direct {p1}, La8/d;-><init>()V

    throw p1
.end method

.method static synthetic I(Lv8/k;Ljava/lang/Object;ILl8/l;ILjava/lang/Object;)V
    .locals 0

    if-nez p5, :cond_1

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    const/4 p3, 0x0

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lv8/k;->H(Ljava/lang/Object;ILl8/l;)V

    return-void

    :cond_1
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    const-string p1, "Super calls with default arguments not supported in this target, function: resumeImpl"

    invoke-direct {p0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method private final J(Lv8/y1;Ljava/lang/Object;ILl8/l;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lv8/y1;",
            "Ljava/lang/Object;",
            "I",
            "Ll8/l<",
            "-",
            "Ljava/lang/Throwable;",
            "La8/v;",
            ">;",
            "Ljava/lang/Object;",
            ")",
            "Ljava/lang/Object;"
        }
    .end annotation

    instance-of v0, p2, Lv8/t;

    if-eqz v0, :cond_0

    goto :goto_2

    :cond_0
    invoke-static {p3}, Lv8/s0;->b(I)Z

    move-result p3

    if-nez p3, :cond_1

    if-nez p5, :cond_1

    goto :goto_2

    :cond_1
    if-nez p4, :cond_3

    instance-of p3, p1, Lv8/h;

    if-eqz p3, :cond_2

    goto :goto_0

    :cond_2
    if-eqz p5, :cond_5

    :cond_3
    :goto_0
    new-instance p3, Lv8/s;

    instance-of v0, p1, Lv8/h;

    if-eqz v0, :cond_4

    check-cast p1, Lv8/h;

    goto :goto_1

    :cond_4
    const/4 p1, 0x0

    :goto_1
    move-object v2, p1

    const/4 v5, 0x0

    const/16 v6, 0x10

    const/4 v7, 0x0

    move-object v0, p3

    move-object v1, p2

    move-object v3, p4

    move-object v4, p5

    invoke-direct/range {v0 .. v7}, Lv8/s;-><init>(Ljava/lang/Object;Lv8/h;Ll8/l;Ljava/lang/Object;Ljava/lang/Throwable;ILm8/g;)V

    move-object p2, p3

    :cond_5
    :goto_2
    return-object p2
.end method

.method private final K()Z
    .locals 4

    :cond_0
    iget v0, p0, Lv8/k;->_decision:I

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_2

    if-ne v0, v2, :cond_1

    return v1

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Already resumed"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    sget-object v0, Lv8/k;->g:Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

    const/4 v3, 0x2

    invoke-virtual {v0, p0, v1, v3}, Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;->compareAndSet(Ljava/lang/Object;II)Z

    move-result v0

    if-eqz v0, :cond_0

    return v2
.end method

.method private final L()Z
    .locals 3

    :cond_0
    iget v0, p0, Lv8/k;->_decision:I

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    const/4 v2, 0x2

    if-ne v0, v2, :cond_1

    return v1

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Already suspended"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    sget-object v0, Lv8/k;->g:Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

    const/4 v2, 0x1

    invoke-virtual {v0, p0, v1, v2}, Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;->compareAndSet(Ljava/lang/Object;II)Z

    move-result v0

    if-eqz v0, :cond_0

    return v2
.end method

.method private final l(Ljava/lang/Object;)Ljava/lang/Void;
    .locals 2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Already resumed, but proposed with update "

    invoke-static {v1, p1}, Lm8/i;->l(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private final m(Ll8/l;Ljava/lang/Throwable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ll8/l<",
            "-",
            "Ljava/lang/Throwable;",
            "La8/v;",
            ">;",
            "Ljava/lang/Throwable;",
            ")V"
        }
    .end annotation

    :try_start_0
    invoke-interface {p1, p2}, Ll8/l;->j(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    invoke-virtual {p0}, Lv8/k;->c()Ld8/g;

    move-result-object p2

    new-instance v0, Lv8/w;

    const-string v1, "Exception in invokeOnCancellation handler for "

    invoke-static {v1, p0}, Lm8/i;->l(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lv8/w;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-static {p2, v0}, Lv8/d0;->a(Ld8/g;Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method private final q(Ljava/lang/Throwable;)Z
    .locals 1

    invoke-direct {p0}, Lv8/k;->B()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    iget-object v0, p0, Lv8/k;->d:Ld8/d;

    check-cast v0, Lkotlinx/coroutines/internal/e;

    invoke-virtual {v0, p1}, Lkotlinx/coroutines/internal/e;->o(Ljava/lang/Throwable;)Z

    move-result p1

    return p1
.end method

.method private final s()V
    .locals 1

    invoke-direct {p0}, Lv8/k;->B()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lv8/k;->r()V

    :cond_0
    return-void
.end method

.method private final t(I)V
    .locals 1

    invoke-direct {p0}, Lv8/k;->K()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    invoke-static {p0, p1}, Lv8/s0;->a(Lv8/r0;I)V

    return-void
.end method

.method private final x()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lv8/k;->w()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lv8/y1;

    if-eqz v1, :cond_0

    const-string v0, "Active"

    goto :goto_0

    :cond_0
    instance-of v0, v0, Lv8/m;

    if-eqz v0, :cond_1

    const-string v0, "Cancelled"

    goto :goto_0

    :cond_1
    const-string v0, "Completed"

    :goto_0
    return-object v0
.end method

.method private final z()Lv8/u0;
    .locals 7

    invoke-virtual {p0}, Lv8/k;->c()Ld8/g;

    move-result-object v0

    sget-object v1, Lv8/k1;->d0:Lv8/k1$b;

    invoke-interface {v0, v1}, Ld8/g;->get(Ld8/g$c;)Ld8/g$b;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lv8/k1;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    const/4 v2, 0x1

    const/4 v3, 0x0

    new-instance v4, Lv8/n;

    invoke-direct {v4, p0}, Lv8/n;-><init>(Lv8/k;)V

    const/4 v5, 0x2

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lv8/k1$a;->d(Lv8/k1;ZZLl8/l;ILjava/lang/Object;)Lv8/u0;

    move-result-object v0

    iput-object v0, p0, Lv8/k;->f:Lv8/u0;

    return-object v0
.end method


# virtual methods
.method public A()Z
    .locals 1

    invoke-virtual {p0}, Lv8/k;->w()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lv8/y1;

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method protected E()Ljava/lang/String;
    .locals 1

    const-string v0, "CancellableContinuation"

    return-object v0
.end method

.method public final F(Ljava/lang/Throwable;)V
    .locals 1

    invoke-direct {p0, p1}, Lv8/k;->q(Ljava/lang/Throwable;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0, p1}, Lv8/k;->p(Ljava/lang/Throwable;)Z

    invoke-direct {p0}, Lv8/k;->s()V

    return-void
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Throwable;)V
    .locals 10

    :cond_0
    iget-object p1, p0, Lv8/k;->_state:Ljava/lang/Object;

    instance-of v0, p1, Lv8/y1;

    if-nez v0, :cond_4

    instance-of v0, p1, Lv8/t;

    if-eqz v0, :cond_1

    return-void

    :cond_1
    instance-of v0, p1, Lv8/s;

    if-eqz v0, :cond_3

    move-object v0, p1

    check-cast v0, Lv8/s;

    invoke-virtual {v0}, Lv8/s;->c()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_2

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v7, 0xf

    const/4 v8, 0x0

    move-object v1, v0

    move-object v6, p2

    invoke-static/range {v1 .. v8}, Lv8/s;->b(Lv8/s;Ljava/lang/Object;Lv8/h;Ll8/l;Ljava/lang/Object;Ljava/lang/Throwable;ILjava/lang/Object;)Lv8/s;

    move-result-object v1

    sget-object v2, Lv8/k;->h:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    invoke-static {v2, p0, p1, v1}, La8/o;->a(Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-virtual {v0, p0, p2}, Lv8/s;->d(Lv8/k;Ljava/lang/Throwable;)V

    return-void

    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Must be called at most once"

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_3
    sget-object v8, Lv8/k;->h:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    new-instance v9, Lv8/s;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v6, 0xe

    const/4 v7, 0x0

    move-object v0, v9

    move-object v1, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v7}, Lv8/s;-><init>(Ljava/lang/Object;Lv8/h;Ll8/l;Ljava/lang/Object;Ljava/lang/Throwable;ILm8/g;)V

    invoke-static {v8, p0, p1, v9}, La8/o;->a(Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    :cond_4
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Not completed"

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final b()Ld8/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ld8/d<",
            "TT;>;"
        }
    .end annotation

    iget-object v0, p0, Lv8/k;->d:Ld8/d;

    return-object v0
.end method

.method public c()Ld8/g;
    .locals 1

    iget-object v0, p0, Lv8/k;->e:Ld8/g;

    return-object v0
.end method

.method public d(Ljava/lang/Object;)Ljava/lang/Throwable;
    .locals 0

    invoke-super {p0, p1}, Lv8/r0;->d(Ljava/lang/Object;)Ljava/lang/Throwable;

    move-result-object p1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lv8/k;->b()Ld8/d;

    :goto_0
    return-object p1
.end method

.method public e()Lf8/e;
    .locals 2

    iget-object v0, p0, Lv8/k;->d:Ld8/d;

    instance-of v1, v0, Lf8/e;

    if-eqz v1, :cond_0

    check-cast v0, Lf8/e;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public f(Lv8/a0;Ljava/lang/Object;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lv8/a0;",
            "TT;)V"
        }
    .end annotation

    iget-object v0, p0, Lv8/k;->d:Ld8/d;

    instance-of v1, v0, Lkotlinx/coroutines/internal/e;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    check-cast v0, Lkotlinx/coroutines/internal/e;

    goto :goto_0

    :cond_0
    move-object v0, v2

    :goto_0
    if-nez v0, :cond_1

    goto :goto_1

    :cond_1
    iget-object v2, v0, Lkotlinx/coroutines/internal/e;->d:Lv8/a0;

    :goto_1
    if-ne v2, p1, :cond_2

    const/4 p1, 0x4

    goto :goto_2

    :cond_2
    iget p1, p0, Lv8/r0;->c:I

    :goto_2
    move v2, p1

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p2

    invoke-static/range {v0 .. v5}, Lv8/k;->I(Lv8/k;Ljava/lang/Object;ILl8/l;ILjava/lang/Object;)V

    return-void
.end method

.method public g(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Object;",
            ")TT;"
        }
    .end annotation

    instance-of v0, p1, Lv8/s;

    if-eqz v0, :cond_0

    check-cast p1, Lv8/s;

    iget-object p1, p1, Lv8/s;->a:Ljava/lang/Object;

    :cond_0
    return-object p1
.end method

.method public i(Ljava/lang/Object;)V
    .locals 6

    invoke-static {p1, p0}, Lv8/x;->c(Ljava/lang/Object;Lv8/j;)Ljava/lang/Object;

    move-result-object v1

    iget v2, p0, Lv8/r0;->c:I

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lv8/k;->I(Lv8/k;Ljava/lang/Object;ILl8/l;ILjava/lang/Object;)V

    return-void
.end method

.method public j()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lv8/k;->w()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public k(Ll8/l;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ll8/l<",
            "-",
            "Ljava/lang/Throwable;",
            "La8/v;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0, p1}, Lv8/k;->C(Ll8/l;)Lv8/h;

    move-result-object v8

    :cond_0
    :goto_0
    iget-object v9, p0, Lv8/k;->_state:Ljava/lang/Object;

    instance-of v0, v9, Lv8/d;

    if-eqz v0, :cond_1

    sget-object v0, Lv8/k;->h:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    invoke-static {v0, p0, v9, v8}, La8/o;->a(Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_1
    instance-of v0, v9, Lv8/h;

    if-eqz v0, :cond_2

    invoke-direct {p0, p1, v9}, Lv8/k;->D(Ll8/l;Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    instance-of v0, v9, Lv8/t;

    if-eqz v0, :cond_7

    move-object v1, v9

    check-cast v1, Lv8/t;

    invoke-virtual {v1}, Lv8/t;->b()Z

    move-result v2

    if-nez v2, :cond_3

    invoke-direct {p0, p1, v9}, Lv8/k;->D(Ll8/l;Ljava/lang/Object;)V

    :cond_3
    instance-of v2, v9, Lv8/m;

    if-eqz v2, :cond_6

    const/4 v2, 0x0

    if-eqz v0, :cond_4

    goto :goto_1

    :cond_4
    move-object v1, v2

    :goto_1
    if-nez v1, :cond_5

    goto :goto_2

    :cond_5
    iget-object v2, v1, Lv8/t;->a:Ljava/lang/Throwable;

    :goto_2
    invoke-direct {p0, p1, v2}, Lv8/k;->m(Ll8/l;Ljava/lang/Throwable;)V

    :cond_6
    return-void

    :cond_7
    instance-of v0, v9, Lv8/s;

    if-eqz v0, :cond_a

    move-object v0, v9

    check-cast v0, Lv8/s;

    iget-object v1, v0, Lv8/s;->b:Lv8/h;

    if-eqz v1, :cond_8

    invoke-direct {p0, p1, v9}, Lv8/k;->D(Ll8/l;Ljava/lang/Object;)V

    :cond_8
    invoke-virtual {v0}, Lv8/s;->c()Z

    move-result v1

    if-eqz v1, :cond_9

    iget-object v0, v0, Lv8/s;->e:Ljava/lang/Throwable;

    invoke-direct {p0, p1, v0}, Lv8/k;->m(Ll8/l;Ljava/lang/Throwable;)V

    return-void

    :cond_9
    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x1d

    const/4 v7, 0x0

    move-object v2, v8

    invoke-static/range {v0 .. v7}, Lv8/s;->b(Lv8/s;Ljava/lang/Object;Lv8/h;Ll8/l;Ljava/lang/Object;Ljava/lang/Throwable;ILjava/lang/Object;)Lv8/s;

    move-result-object v0

    sget-object v1, Lv8/k;->h:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    invoke-static {v1, p0, v9, v0}, La8/o;->a(Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_a
    new-instance v10, Lv8/s;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x1c

    const/4 v7, 0x0

    move-object v0, v10

    move-object v1, v9

    move-object v2, v8

    invoke-direct/range {v0 .. v7}, Lv8/s;-><init>(Ljava/lang/Object;Lv8/h;Ll8/l;Ljava/lang/Object;Ljava/lang/Throwable;ILm8/g;)V

    sget-object v0, Lv8/k;->h:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    invoke-static {v0, p0, v9, v10}, La8/o;->a(Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void
.end method

.method public final n(Lv8/h;Ljava/lang/Throwable;)V
    .locals 2

    :try_start_0
    invoke-virtual {p1, p2}, Lv8/i;->b(Ljava/lang/Throwable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    invoke-virtual {p0}, Lv8/k;->c()Ld8/g;

    move-result-object p2

    new-instance v0, Lv8/w;

    const-string v1, "Exception in invokeOnCancellation handler for "

    invoke-static {v1, p0}, Lm8/i;->l(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lv8/w;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-static {p2, v0}, Lv8/d0;->a(Ld8/g;Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method public final o(Ll8/l;Ljava/lang/Throwable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ll8/l<",
            "-",
            "Ljava/lang/Throwable;",
            "La8/v;",
            ">;",
            "Ljava/lang/Throwable;",
            ")V"
        }
    .end annotation

    :try_start_0
    invoke-interface {p1, p2}, Ll8/l;->j(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    invoke-virtual {p0}, Lv8/k;->c()Ld8/g;

    move-result-object p2

    new-instance v0, Lv8/w;

    const-string v1, "Exception in resume onCancellation handler for "

    invoke-static {v1, p0}, Lm8/i;->l(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lv8/w;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-static {p2, v0}, Lv8/d0;->a(Ld8/g;Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method public p(Ljava/lang/Throwable;)Z
    .locals 4

    :goto_0
    iget-object v0, p0, Lv8/k;->_state:Ljava/lang/Object;

    instance-of v1, v0, Lv8/y1;

    if-nez v1, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    new-instance v1, Lv8/m;

    instance-of v2, v0, Lv8/h;

    invoke-direct {v1, p0, p1, v2}, Lv8/m;-><init>(Ld8/d;Ljava/lang/Throwable;Z)V

    sget-object v3, Lv8/k;->h:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    invoke-static {v3, p0, v0, v1}, La8/o;->a(Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    goto :goto_0

    :cond_1
    if-eqz v2, :cond_2

    check-cast v0, Lv8/h;

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    :goto_1
    if-nez v0, :cond_3

    goto :goto_2

    :cond_3
    invoke-virtual {p0, v0, p1}, Lv8/k;->n(Lv8/h;Ljava/lang/Throwable;)V

    :goto_2
    invoke-direct {p0}, Lv8/k;->s()V

    iget p1, p0, Lv8/r0;->c:I

    invoke-direct {p0, p1}, Lv8/k;->t(I)V

    const/4 p1, 0x1

    return p1
.end method

.method public final r()V
    .locals 1

    iget-object v0, p0, Lv8/k;->f:Lv8/u0;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-interface {v0}, Lv8/u0;->c()V

    sget-object v0, Lv8/x1;->a:Lv8/x1;

    iput-object v0, p0, Lv8/k;->f:Lv8/u0;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lv8/k;->E()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lv8/k;->d:Ld8/d;

    invoke-static {v1}, Lv8/i0;->c(Ld8/d;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "){"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-direct {p0}, Lv8/k;->x()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "}@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p0}, Lv8/i0;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public u(Lv8/k1;)Ljava/lang/Throwable;
    .locals 0

    invoke-interface {p1}, Lv8/k1;->C()Ljava/util/concurrent/CancellationException;

    move-result-object p1

    return-object p1
.end method

.method public final v()Ljava/lang/Object;
    .locals 3

    invoke-direct {p0}, Lv8/k;->B()Z

    move-result v0

    invoke-direct {p0}, Lv8/k;->L()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lv8/k;->f:Lv8/u0;

    if-nez v1, :cond_0

    invoke-direct {p0}, Lv8/k;->z()Lv8/u0;

    :cond_0
    if-eqz v0, :cond_1

    invoke-direct {p0}, Lv8/k;->G()V

    :cond_1
    invoke-static {}, Le8/b;->c()Ljava/lang/Object;

    move-result-object v0

    return-object v0

    :cond_2
    if-eqz v0, :cond_3

    invoke-direct {p0}, Lv8/k;->G()V

    :cond_3
    invoke-virtual {p0}, Lv8/k;->w()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lv8/t;

    if-nez v1, :cond_6

    iget v1, p0, Lv8/r0;->c:I

    invoke-static {v1}, Lv8/s0;->b(I)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-virtual {p0}, Lv8/k;->c()Ld8/g;

    move-result-object v1

    sget-object v2, Lv8/k1;->d0:Lv8/k1$b;

    invoke-interface {v1, v2}, Ld8/g;->get(Ld8/g$c;)Ld8/g$b;

    move-result-object v1

    check-cast v1, Lv8/k1;

    if-eqz v1, :cond_5

    invoke-interface {v1}, Lv8/k1;->b()Z

    move-result v2

    if-eqz v2, :cond_4

    goto :goto_0

    :cond_4
    invoke-interface {v1}, Lv8/k1;->C()Ljava/util/concurrent/CancellationException;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lv8/k;->a(Ljava/lang/Object;Ljava/lang/Throwable;)V

    throw v1

    :cond_5
    :goto_0
    invoke-virtual {p0, v0}, Lv8/k;->g(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0

    :cond_6
    check-cast v0, Lv8/t;

    iget-object v0, v0, Lv8/t;->a:Ljava/lang/Throwable;

    throw v0
.end method

.method public final w()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lv8/k;->_state:Ljava/lang/Object;

    return-object v0
.end method

.method public y()V
    .locals 2

    invoke-direct {p0}, Lv8/k;->z()Lv8/u0;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lv8/k;->A()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Lv8/u0;->c()V

    sget-object v0, Lv8/x1;->a:Lv8/x1;

    iput-object v0, p0, Lv8/k;->f:Lv8/u0;

    :cond_1
    return-void
.end method
