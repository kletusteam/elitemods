.class final synthetic Lv8/g;
.super Ljava/lang/Object;


# direct methods
.method public static final a(Lv8/e0;Ld8/g;Lv8/g0;Ll8/p;)Lv8/l0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lv8/e0;",
            "Ld8/g;",
            "Lv8/g0;",
            "Ll8/p<",
            "-",
            "Lv8/e0;",
            "-",
            "Ld8/d<",
            "-TT;>;+",
            "Ljava/lang/Object;",
            ">;)",
            "Lv8/l0<",
            "TT;>;"
        }
    .end annotation

    invoke-static {p0, p1}, Lv8/z;->e(Lv8/e0;Ld8/g;)Ld8/g;

    move-result-object p0

    invoke-virtual {p2}, Lv8/g0;->c()Z

    move-result p1

    if-eqz p1, :cond_0

    new-instance p1, Lv8/t1;

    invoke-direct {p1, p0, p3}, Lv8/t1;-><init>(Ld8/g;Ll8/p;)V

    goto :goto_0

    :cond_0
    new-instance p1, Lv8/m0;

    const/4 v0, 0x1

    invoke-direct {p1, p0, v0}, Lv8/m0;-><init>(Ld8/g;Z)V

    :goto_0
    invoke-virtual {p1, p2, p1, p3}, Lv8/a;->B0(Lv8/g0;Ljava/lang/Object;Ll8/p;)V

    return-object p1
.end method

.method public static synthetic b(Lv8/e0;Ld8/g;Lv8/g0;Ll8/p;ILjava/lang/Object;)Lv8/l0;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    sget-object p1, Ld8/h;->a:Ld8/h;

    :cond_0
    and-int/lit8 p4, p4, 0x2

    if-eqz p4, :cond_1

    sget-object p2, Lv8/g0;->a:Lv8/g0;

    :cond_1
    invoke-static {p0, p1, p2, p3}, Lv8/f;->a(Lv8/e0;Ld8/g;Lv8/g0;Ll8/p;)Lv8/l0;

    move-result-object p0

    return-object p0
.end method

.method public static final c(Lv8/e0;Ld8/g;Lv8/g0;Ll8/p;)Lv8/k1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lv8/e0;",
            "Ld8/g;",
            "Lv8/g0;",
            "Ll8/p<",
            "-",
            "Lv8/e0;",
            "-",
            "Ld8/d<",
            "-",
            "La8/v;",
            ">;+",
            "Ljava/lang/Object;",
            ">;)",
            "Lv8/k1;"
        }
    .end annotation

    invoke-static {p0, p1}, Lv8/z;->e(Lv8/e0;Ld8/g;)Ld8/g;

    move-result-object p0

    invoke-virtual {p2}, Lv8/g0;->c()Z

    move-result p1

    if-eqz p1, :cond_0

    new-instance p1, Lv8/u1;

    invoke-direct {p1, p0, p3}, Lv8/u1;-><init>(Ld8/g;Ll8/p;)V

    goto :goto_0

    :cond_0
    new-instance p1, Lv8/b2;

    const/4 v0, 0x1

    invoke-direct {p1, p0, v0}, Lv8/b2;-><init>(Ld8/g;Z)V

    :goto_0
    invoke-virtual {p1, p2, p1, p3}, Lv8/a;->B0(Lv8/g0;Ljava/lang/Object;Ll8/p;)V

    return-object p1
.end method

.method public static synthetic d(Lv8/e0;Ld8/g;Lv8/g0;Ll8/p;ILjava/lang/Object;)Lv8/k1;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    sget-object p1, Ld8/h;->a:Ld8/h;

    :cond_0
    and-int/lit8 p4, p4, 0x2

    if-eqz p4, :cond_1

    sget-object p2, Lv8/g0;->a:Lv8/g0;

    :cond_1
    invoke-static {p0, p1, p2, p3}, Lv8/f;->c(Lv8/e0;Ld8/g;Lv8/g0;Ll8/p;)Lv8/k1;

    move-result-object p0

    return-object p0
.end method

.method public static final e(Ld8/g;Ll8/p;Ld8/d;)Ljava/lang/Object;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ld8/g;",
            "Ll8/p<",
            "-",
            "Lv8/e0;",
            "-",
            "Ld8/d<",
            "-TT;>;+",
            "Ljava/lang/Object;",
            ">;",
            "Ld8/d<",
            "-TT;>;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    invoke-interface {p2}, Ld8/d;->c()Ld8/g;

    move-result-object v0

    invoke-static {v0, p0}, Lv8/z;->d(Ld8/g;Ld8/g;)Ld8/g;

    move-result-object p0

    invoke-static {p0}, Lv8/o1;->f(Ld8/g;)V

    if-ne p0, v0, :cond_0

    new-instance v0, Lkotlinx/coroutines/internal/w;

    invoke-direct {v0, p0, p2}, Lkotlinx/coroutines/internal/w;-><init>(Ld8/g;Ld8/d;)V

    invoke-static {v0, v0, p1}, Lx8/b;->b(Lkotlinx/coroutines/internal/w;Ljava/lang/Object;Ll8/p;)Ljava/lang/Object;

    move-result-object p0

    goto :goto_0

    :cond_0
    sget-object v1, Ld8/e;->b0:Ld8/e$b;

    invoke-interface {p0, v1}, Ld8/g;->get(Ld8/g$c;)Ld8/g$b;

    move-result-object v2

    invoke-interface {v0, v1}, Ld8/g;->get(Ld8/g$c;)Ld8/g$b;

    move-result-object v0

    invoke-static {v2, v0}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lv8/k2;

    invoke-direct {v0, p0, p2}, Lv8/k2;-><init>(Ld8/g;Ld8/d;)V

    const/4 v1, 0x0

    invoke-static {p0, v1}, Lkotlinx/coroutines/internal/b0;->c(Ld8/g;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    :try_start_0
    invoke-static {v0, v0, p1}, Lx8/b;->b(Lkotlinx/coroutines/internal/w;Ljava/lang/Object;Ll8/p;)Ljava/lang/Object;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {p0, v1}, Lkotlinx/coroutines/internal/b0;->a(Ld8/g;Ljava/lang/Object;)V

    move-object p0, p1

    goto :goto_0

    :catchall_0
    move-exception p1

    invoke-static {p0, v1}, Lkotlinx/coroutines/internal/b0;->a(Ld8/g;Ljava/lang/Object;)V

    throw p1

    :cond_1
    new-instance v0, Lv8/q0;

    invoke-direct {v0, p0, p2}, Lv8/q0;-><init>(Ld8/g;Ld8/d;)V

    const/4 v5, 0x0

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v2, p1

    move-object v3, v0

    move-object v4, v0

    invoke-static/range {v2 .. v7}, Lx8/a;->d(Ll8/p;Ljava/lang/Object;Ld8/d;Ll8/l;ILjava/lang/Object;)V

    invoke-virtual {v0}, Lv8/q0;->C0()Ljava/lang/Object;

    move-result-object p0

    :goto_0
    invoke-static {}, Le8/b;->c()Ljava/lang/Object;

    move-result-object p1

    if-ne p0, p1, :cond_2

    invoke-static {p2}, Lf8/h;->c(Ld8/d;)V

    :cond_2
    return-object p0
.end method
