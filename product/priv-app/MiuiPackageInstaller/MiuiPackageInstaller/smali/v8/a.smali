.class public abstract Lv8/a;
.super Lv8/r1;

# interfaces
.implements Ld8/d;
.implements Lv8/e0;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lv8/r1;",
        "Ld8/d<",
        "TT;>;",
        "Lv8/e0;"
    }
.end annotation


# instance fields
.field private final b:Ld8/g;


# direct methods
.method public constructor <init>(Ld8/g;ZZ)V
    .locals 0

    invoke-direct {p0, p3}, Lv8/r1;-><init>(Z)V

    if-eqz p2, :cond_0

    sget-object p2, Lv8/k1;->d0:Lv8/k1$b;

    invoke-interface {p1, p2}, Ld8/g;->get(Ld8/g$c;)Ld8/g$b;

    move-result-object p2

    check-cast p2, Lv8/k1;

    invoke-virtual {p0, p2}, Lv8/r1;->X(Lv8/k1;)V

    :cond_0
    invoke-interface {p1, p0}, Ld8/g;->plus(Ld8/g;)Ld8/g;

    move-result-object p1

    iput-object p1, p0, Lv8/a;->b:Ld8/g;

    return-void
.end method


# virtual methods
.method protected A()Ljava/lang/String;
    .locals 2

    invoke-static {p0}, Lv8/i0;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, " was cancelled"

    invoke-static {v0, v1}, Lm8/i;->l(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected A0(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    return-void
.end method

.method public final B0(Lv8/g0;Ljava/lang/Object;Ll8/p;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Lv8/g0;",
            "TR;",
            "Ll8/p<",
            "-TR;-",
            "Ld8/d<",
            "-TT;>;+",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p1, p3, p2, p0}, Lv8/g0;->b(Ll8/p;Ljava/lang/Object;Ld8/d;)V

    return-void
.end method

.method public final W(Ljava/lang/Throwable;)V
    .locals 1

    iget-object v0, p0, Lv8/a;->b:Ld8/g;

    invoke-static {v0, p1}, Lv8/d0;->a(Ld8/g;Ljava/lang/Throwable;)V

    return-void
.end method

.method public b()Z
    .locals 1

    invoke-super {p0}, Lv8/r1;->b()Z

    move-result v0

    return v0
.end method

.method public final c()Ld8/g;
    .locals 1

    iget-object v0, p0, Lv8/a;->b:Ld8/g;

    return-object v0
.end method

.method public d0()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lv8/a;->b:Ld8/g;

    invoke-static {v0}, Lv8/z;->b(Ld8/g;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-super {p0}, Lv8/r1;->d0()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v2, 0x22

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\":"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-super {p0}, Lv8/r1;->d0()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public h()Ld8/g;
    .locals 1

    iget-object v0, p0, Lv8/a;->b:Ld8/g;

    return-object v0
.end method

.method public final i(Ljava/lang/Object;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {p1, v0, v1, v0}, Lv8/x;->d(Ljava/lang/Object;Ll8/l;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {p0, p1}, Lv8/r1;->b0(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    sget-object v0, Lv8/s1;->b:Lkotlinx/coroutines/internal/x;

    if-ne p1, v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0, p1}, Lv8/a;->y0(Ljava/lang/Object;)V

    return-void
.end method

.method protected final i0(Ljava/lang/Object;)V
    .locals 1

    instance-of v0, p1, Lv8/t;

    if-eqz v0, :cond_0

    check-cast p1, Lv8/t;

    iget-object v0, p1, Lv8/t;->a:Ljava/lang/Throwable;

    invoke-virtual {p1}, Lv8/t;->a()Z

    move-result p1

    invoke-virtual {p0, v0, p1}, Lv8/a;->z0(Ljava/lang/Throwable;Z)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p1}, Lv8/a;->A0(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method protected y0(Ljava/lang/Object;)V
    .locals 0

    invoke-virtual {p0, p1}, Lv8/r1;->o(Ljava/lang/Object;)V

    return-void
.end method

.method protected z0(Ljava/lang/Throwable;Z)V
    .locals 0

    return-void
.end method
