.class public final Lv8/f0;
.super Ljava/lang/Object;


# direct methods
.method public static final a(Ld8/g;)Lv8/e0;
    .locals 3

    new-instance v0, Lkotlinx/coroutines/internal/d;

    sget-object v1, Lv8/k1;->d0:Lv8/k1$b;

    invoke-interface {p0, v1}, Ld8/g;->get(Ld8/g$c;)Ld8/g$b;

    move-result-object v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {v2, v1, v2}, Lv8/o1;->b(Lv8/k1;ILjava/lang/Object;)Lv8/r;

    move-result-object v1

    invoke-interface {p0, v1}, Ld8/g;->plus(Ld8/g;)Ld8/g;

    move-result-object p0

    :goto_0
    invoke-direct {v0, p0}, Lkotlinx/coroutines/internal/d;-><init>(Ld8/g;)V

    return-object v0
.end method

.method public static final b(Lv8/e0;Ljava/util/concurrent/CancellationException;)V
    .locals 2

    invoke-interface {p0}, Lv8/e0;->h()Ld8/g;

    move-result-object v0

    sget-object v1, Lv8/k1;->d0:Lv8/k1$b;

    invoke-interface {v0, v1}, Ld8/g;->get(Ld8/g$c;)Ld8/g$b;

    move-result-object v0

    check-cast v0, Lv8/k1;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lv8/k1;->J(Ljava/util/concurrent/CancellationException;)V

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Scope cannot be cancelled because it does not have a job: "

    invoke-static {v0, p0}, Lm8/i;->l(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public static synthetic c(Lv8/e0;Ljava/util/concurrent/CancellationException;ILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    const/4 p1, 0x0

    :cond_0
    invoke-static {p0, p1}, Lv8/f0;->b(Lv8/e0;Ljava/util/concurrent/CancellationException;)V

    return-void
.end method

.method public static final d(Ll8/p;Ld8/d;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Ll8/p<",
            "-",
            "Lv8/e0;",
            "-",
            "Ld8/d<",
            "-TR;>;+",
            "Ljava/lang/Object;",
            ">;",
            "Ld8/d<",
            "-TR;>;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    new-instance v0, Lkotlinx/coroutines/internal/w;

    invoke-interface {p1}, Ld8/d;->c()Ld8/g;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lkotlinx/coroutines/internal/w;-><init>(Ld8/g;Ld8/d;)V

    invoke-static {v0, v0, p0}, Lx8/b;->b(Lkotlinx/coroutines/internal/w;Ljava/lang/Object;Ll8/p;)Ljava/lang/Object;

    move-result-object p0

    invoke-static {}, Le8/b;->c()Ljava/lang/Object;

    move-result-object v0

    if-ne p0, v0, :cond_0

    invoke-static {p1}, Lf8/h;->c(Ld8/d;)V

    :cond_0
    return-object p0
.end method
