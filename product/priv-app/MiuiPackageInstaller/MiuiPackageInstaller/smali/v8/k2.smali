.class public final Lv8/k2;
.super Lkotlinx/coroutines/internal/w;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lkotlinx/coroutines/internal/w<",
        "TT;>;"
    }
.end annotation


# instance fields
.field private d:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal<",
            "La8/l<",
            "Ld8/g;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ld8/g;Ld8/d;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ld8/g;",
            "Ld8/d<",
            "-TT;>;)V"
        }
    .end annotation

    sget-object v0, Lv8/l2;->a:Lv8/l2;

    invoke-interface {p1, v0}, Ld8/g;->get(Ld8/g$c;)Ld8/g$b;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-interface {p1, v0}, Ld8/g;->plus(Ld8/g;)Ld8/g;

    move-result-object p1

    :cond_0
    invoke-direct {p0, p1, p2}, Lkotlinx/coroutines/internal/w;-><init>(Ld8/g;Ld8/d;)V

    new-instance p1, Ljava/lang/ThreadLocal;

    invoke-direct {p1}, Ljava/lang/ThreadLocal;-><init>()V

    iput-object p1, p0, Lv8/k2;->d:Ljava/lang/ThreadLocal;

    return-void
.end method


# virtual methods
.method public final C0()Z
    .locals 2

    iget-object v0, p0, Lv8/k2;->d:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    iget-object v0, p0, Lv8/k2;->d:Ljava/lang/ThreadLocal;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    const/4 v0, 0x1

    return v0
.end method

.method public final D0(Ld8/g;Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lv8/k2;->d:Ljava/lang/ThreadLocal;

    invoke-static {p1, p2}, La8/r;->a(Ljava/lang/Object;Ljava/lang/Object;)La8/l;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    return-void
.end method

.method protected y0(Ljava/lang/Object;)V
    .locals 5

    iget-object v0, p0, Lv8/k2;->d:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, La8/l;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, La8/l;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ld8/g;

    invoke-virtual {v0}, La8/l;->b()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v2, v0}, Lkotlinx/coroutines/internal/b0;->a(Ld8/g;Ljava/lang/Object;)V

    iget-object v0, p0, Lv8/k2;->d:Ljava/lang/ThreadLocal;

    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    :goto_0
    iget-object v0, p0, Lkotlinx/coroutines/internal/w;->c:Ld8/d;

    invoke-static {p1, v0}, Lv8/x;->a(Ljava/lang/Object;Ld8/d;)Ljava/lang/Object;

    move-result-object p1

    iget-object v0, p0, Lkotlinx/coroutines/internal/w;->c:Ld8/d;

    invoke-interface {v0}, Ld8/d;->c()Ld8/g;

    move-result-object v2

    invoke-static {v2, v1}, Lkotlinx/coroutines/internal/b0;->c(Ld8/g;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    sget-object v4, Lkotlinx/coroutines/internal/b0;->a:Lkotlinx/coroutines/internal/x;

    if-eq v3, v4, :cond_1

    invoke-static {v0, v2, v3}, Lv8/z;->g(Ld8/d;Ld8/g;Ljava/lang/Object;)Lv8/k2;

    move-result-object v1

    :cond_1
    :try_start_0
    iget-object v0, p0, Lkotlinx/coroutines/internal/w;->c:Ld8/d;

    invoke-interface {v0, p1}, Ld8/d;->i(Ljava/lang/Object;)V

    sget-object p1, La8/v;->a:La8/v;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lv8/k2;->C0()Z

    move-result p1

    if-eqz p1, :cond_3

    :cond_2
    invoke-static {v2, v3}, Lkotlinx/coroutines/internal/b0;->a(Ld8/g;Ljava/lang/Object;)V

    :cond_3
    return-void

    :catchall_0
    move-exception p1

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lv8/k2;->C0()Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_4
    invoke-static {v2, v3}, Lkotlinx/coroutines/internal/b0;->a(Ld8/g;Ljava/lang/Object;)V

    :cond_5
    throw p1
.end method
