.class public abstract Lv8/a1;
.super Lv8/y0;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lv8/y0;-><init>()V

    return-void
.end method


# virtual methods
.method protected abstract g0()Ljava/lang/Thread;
.end method

.method protected h0(JLv8/z0$c;)V
    .locals 1

    sget-object v0, Lv8/j0;->g:Lv8/j0;

    invoke-virtual {v0, p1, p2, p3}, Lv8/z0;->t0(JLv8/z0$c;)V

    return-void
.end method

.method protected final i0()V
    .locals 2

    invoke-virtual {p0}, Lv8/a1;->g0()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    if-eq v1, v0, :cond_0

    invoke-static {}, Lv8/c;->a()Lv8/b;

    invoke-static {v0}, Ljava/util/concurrent/locks/LockSupport;->unpark(Ljava/lang/Thread;)V

    :cond_0
    return-void
.end method
