.class public final Lv8/z;
.super Ljava/lang/Object;


# direct methods
.method private static final a(Ld8/g;Ld8/g;Z)Ld8/g;
    .locals 3

    invoke-static {p0}, Lv8/z;->c(Ld8/g;)Z

    move-result v0

    invoke-static {p1}, Lv8/z;->c(Ld8/g;)Z

    move-result v1

    if-nez v0, :cond_0

    if-nez v1, :cond_0

    invoke-interface {p0, p1}, Ld8/g;->plus(Ld8/g;)Ld8/g;

    move-result-object p0

    return-object p0

    :cond_0
    new-instance v0, Lm8/t;

    invoke-direct {v0}, Lm8/t;-><init>()V

    iput-object p1, v0, Lm8/t;->a:Ljava/lang/Object;

    sget-object p1, Ld8/h;->a:Ld8/h;

    new-instance v2, Lv8/z$b;

    invoke-direct {v2, v0, p2}, Lv8/z$b;-><init>(Lm8/t;Z)V

    invoke-interface {p0, p1, v2}, Ld8/g;->fold(Ljava/lang/Object;Ll8/p;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ld8/g;

    if-eqz v1, :cond_1

    iget-object p2, v0, Lm8/t;->a:Ljava/lang/Object;

    check-cast p2, Ld8/g;

    sget-object v1, Lv8/z$a;->b:Lv8/z$a;

    invoke-interface {p2, p1, v1}, Ld8/g;->fold(Ljava/lang/Object;Ll8/p;)Ljava/lang/Object;

    move-result-object p1

    iput-object p1, v0, Lm8/t;->a:Ljava/lang/Object;

    :cond_1
    iget-object p1, v0, Lm8/t;->a:Ljava/lang/Object;

    check-cast p1, Ld8/g;

    invoke-interface {p0, p1}, Ld8/g;->plus(Ld8/g;)Ld8/g;

    move-result-object p0

    return-object p0
.end method

.method public static final b(Ld8/g;)Ljava/lang/String;
    .locals 0

    const/4 p0, 0x0

    return-object p0
.end method

.method private static final c(Ld8/g;)Z
    .locals 2

    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    sget-object v1, Lv8/z$c;->b:Lv8/z$c;

    invoke-interface {p0, v0, v1}, Ld8/g;->fold(Ljava/lang/Object;Ll8/p;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method public static final d(Ld8/g;Ld8/g;)Ld8/g;
    .locals 1

    invoke-static {p1}, Lv8/z;->c(Ld8/g;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p0, p1}, Ld8/g;->plus(Ld8/g;)Ld8/g;

    move-result-object p0

    return-object p0

    :cond_0
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lv8/z;->a(Ld8/g;Ld8/g;Z)Ld8/g;

    move-result-object p0

    return-object p0
.end method

.method public static final e(Lv8/e0;Ld8/g;)Ld8/g;
    .locals 1

    invoke-interface {p0}, Lv8/e0;->h()Ld8/g;

    move-result-object p0

    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Lv8/z;->a(Ld8/g;Ld8/g;Z)Ld8/g;

    move-result-object p0

    invoke-static {}, Lv8/t0;->a()Lv8/a0;

    move-result-object p1

    if-eq p0, p1, :cond_0

    sget-object p1, Ld8/e;->b0:Ld8/e$b;

    invoke-interface {p0, p1}, Ld8/g;->get(Ld8/g$c;)Ld8/g$b;

    move-result-object p1

    if-nez p1, :cond_0

    invoke-static {}, Lv8/t0;->a()Lv8/a0;

    move-result-object p1

    invoke-interface {p0, p1}, Ld8/g;->plus(Ld8/g;)Ld8/g;

    move-result-object p0

    :cond_0
    return-object p0
.end method

.method public static final f(Lf8/e;)Lv8/k2;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lf8/e;",
            ")",
            "Lv8/k2<",
            "*>;"
        }
    .end annotation

    :cond_0
    instance-of v0, p0, Lv8/q0;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    return-object v1

    :cond_1
    invoke-interface {p0}, Lf8/e;->e()Lf8/e;

    move-result-object p0

    if-nez p0, :cond_2

    return-object v1

    :cond_2
    instance-of v0, p0, Lv8/k2;

    if-eqz v0, :cond_0

    check-cast p0, Lv8/k2;

    return-object p0
.end method

.method public static final g(Ld8/d;Ld8/g;Ljava/lang/Object;)Lv8/k2;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ld8/d<",
            "*>;",
            "Ld8/g;",
            "Ljava/lang/Object;",
            ")",
            "Lv8/k2<",
            "*>;"
        }
    .end annotation

    instance-of v0, p0, Lf8/e;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    sget-object v0, Lv8/l2;->a:Lv8/l2;

    invoke-interface {p1, v0}, Ld8/g;->get(Ld8/g$c;)Ld8/g$b;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_2

    return-object v1

    :cond_2
    check-cast p0, Lf8/e;

    invoke-static {p0}, Lv8/z;->f(Lf8/e;)Lv8/k2;

    move-result-object p0

    if-nez p0, :cond_3

    goto :goto_1

    :cond_3
    invoke-virtual {p0, p1, p2}, Lv8/k2;->D0(Ld8/g;Ljava/lang/Object;)V

    :goto_1
    return-object p0
.end method
