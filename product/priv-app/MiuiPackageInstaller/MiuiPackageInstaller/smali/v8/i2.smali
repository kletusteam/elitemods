.class public final Lv8/i2;
.super Ljava/lang/Object;


# direct methods
.method public static final a(JLv8/k1;)Lv8/g2;
    .locals 3

    new-instance v0, Lv8/g2;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Timed out waiting for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string p0, " ms"

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0, p2}, Lv8/g2;-><init>(Ljava/lang/String;Lv8/k1;)V

    return-object v0
.end method

.method private static final b(Lv8/h2;Ll8/p;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<U:",
            "Ljava/lang/Object;",
            "T::TU;>(",
            "Lv8/h2<",
            "TU;-TT;>;",
            "Ll8/p<",
            "-",
            "Lv8/e0;",
            "-",
            "Ld8/d<",
            "-TT;>;+",
            "Ljava/lang/Object;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    iget-object v0, p0, Lkotlinx/coroutines/internal/w;->c:Ld8/d;

    invoke-interface {v0}, Ld8/d;->c()Ld8/g;

    move-result-object v0

    invoke-static {v0}, Lv8/o0;->b(Ld8/g;)Lv8/n0;

    move-result-object v0

    iget-wide v1, p0, Lv8/h2;->d:J

    invoke-virtual {p0}, Lv8/a;->c()Ld8/g;

    move-result-object v3

    invoke-interface {v0, v1, v2, p0, v3}, Lv8/n0;->s(JLjava/lang/Runnable;Ld8/g;)Lv8/u0;

    move-result-object v0

    invoke-static {p0, v0}, Lv8/o1;->e(Lv8/k1;Lv8/u0;)Lv8/u0;

    invoke-static {p0, p0, p1}, Lx8/b;->c(Lkotlinx/coroutines/internal/w;Ljava/lang/Object;Ll8/p;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method public static final c(JLl8/p;Ld8/d;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(J",
            "Ll8/p<",
            "-",
            "Lv8/e0;",
            "-",
            "Ld8/d<",
            "-TT;>;+",
            "Ljava/lang/Object;",
            ">;",
            "Ld8/d<",
            "-TT;>;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    const-wide/16 v0, 0x0

    cmp-long v0, p0, v0

    if-lez v0, :cond_1

    new-instance v0, Lv8/h2;

    invoke-direct {v0, p0, p1, p3}, Lv8/h2;-><init>(JLd8/d;)V

    invoke-static {v0, p2}, Lv8/i2;->b(Lv8/h2;Ll8/p;)Ljava/lang/Object;

    move-result-object p0

    invoke-static {}, Le8/b;->c()Ljava/lang/Object;

    move-result-object p1

    if-ne p0, p1, :cond_0

    invoke-static {p3}, Lf8/h;->c(Ld8/d;)V

    :cond_0
    return-object p0

    :cond_1
    new-instance p0, Lv8/g2;

    const-string p1, "Timed out immediately"

    invoke-direct {p0, p1}, Lv8/g2;-><init>(Ljava/lang/String;)V

    throw p0
.end method
