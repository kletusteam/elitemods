.class final Lv8/r1$a;
.super Lv8/k;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lv8/r1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lv8/k<",
        "TT;>;"
    }
.end annotation


# instance fields
.field private final i:Lv8/r1;


# direct methods
.method public constructor <init>(Ld8/d;Lv8/r1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ld8/d<",
            "-TT;>;",
            "Lv8/r1;",
            ")V"
        }
    .end annotation

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lv8/k;-><init>(Ld8/d;I)V

    iput-object p2, p0, Lv8/r1$a;->i:Lv8/r1;

    return-void
.end method


# virtual methods
.method protected E()Ljava/lang/String;
    .locals 1

    const-string v0, "AwaitContinuation"

    return-object v0
.end method

.method public u(Lv8/k1;)Ljava/lang/Throwable;
    .locals 2

    iget-object v0, p0, Lv8/r1$a;->i:Lv8/r1;

    invoke-virtual {v0}, Lv8/r1;->U()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lv8/r1$c;

    if-eqz v1, :cond_1

    move-object v1, v0

    check-cast v1, Lv8/r1$c;

    invoke-virtual {v1}, Lv8/r1$c;->e()Ljava/lang/Throwable;

    move-result-object v1

    if-nez v1, :cond_0

    goto :goto_0

    :cond_0
    return-object v1

    :cond_1
    :goto_0
    instance-of v1, v0, Lv8/t;

    if-eqz v1, :cond_2

    check-cast v0, Lv8/t;

    iget-object p1, v0, Lv8/t;->a:Ljava/lang/Throwable;

    return-object p1

    :cond_2
    invoke-interface {p1}, Lv8/k1;->C()Ljava/util/concurrent/CancellationException;

    move-result-object p1

    return-object p1
.end method
