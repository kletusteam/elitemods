.class final Lv8/z$b;
.super Lm8/j;

# interfaces
.implements Ll8/p;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lv8/z;->a(Ld8/g;Ld8/g;Z)Ld8/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lm8/j;",
        "Ll8/p<",
        "Ld8/g;",
        "Ld8/g$b;",
        "Ld8/g;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic b:Lm8/t;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lm8/t<",
            "Ld8/g;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic c:Z


# direct methods
.method constructor <init>(Lm8/t;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lm8/t<",
            "Ld8/g;",
            ">;Z)V"
        }
    .end annotation

    iput-object p1, p0, Lv8/z$b;->b:Lm8/t;

    iput-boolean p2, p0, Lv8/z$b;->c:Z

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lm8/j;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final b(Ld8/g;Ld8/g$b;)Ld8/g;
    .locals 4

    instance-of v0, p2, Lv8/y;

    if-nez v0, :cond_0

    invoke-interface {p1, p2}, Ld8/g;->plus(Ld8/g;)Ld8/g;

    move-result-object p1

    return-object p1

    :cond_0
    iget-object v0, p0, Lv8/z$b;->b:Lm8/t;

    iget-object v0, v0, Lm8/t;->a:Ljava/lang/Object;

    check-cast v0, Ld8/g;

    invoke-interface {p2}, Ld8/g$b;->getKey()Ld8/g$c;

    move-result-object v1

    invoke-interface {v0, v1}, Ld8/g;->get(Ld8/g$c;)Ld8/g$b;

    move-result-object v0

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lv8/z$b;->c:Z

    check-cast p2, Lv8/y;

    if-eqz v0, :cond_1

    invoke-interface {p2}, Lv8/y;->p()Lv8/y;

    move-result-object p2

    :cond_1
    invoke-interface {p1, p2}, Ld8/g;->plus(Ld8/g;)Ld8/g;

    move-result-object p1

    return-object p1

    :cond_2
    iget-object v1, p0, Lv8/z$b;->b:Lm8/t;

    iget-object v2, v1, Lm8/t;->a:Ljava/lang/Object;

    check-cast v2, Ld8/g;

    invoke-interface {p2}, Ld8/g$b;->getKey()Ld8/g$c;

    move-result-object v3

    invoke-interface {v2, v3}, Ld8/g;->minusKey(Ld8/g$c;)Ld8/g;

    move-result-object v2

    iput-object v2, v1, Lm8/t;->a:Ljava/lang/Object;

    check-cast p2, Lv8/y;

    invoke-interface {p2, v0}, Lv8/y;->n(Ld8/g$b;)Ld8/g;

    move-result-object p2

    invoke-interface {p1, p2}, Ld8/g;->plus(Ld8/g;)Ld8/g;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic g(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ld8/g;

    check-cast p2, Ld8/g$b;

    invoke-virtual {p0, p1, p2}, Lv8/z$b;->b(Ld8/g;Ld8/g$b;)Ld8/g;

    move-result-object p1

    return-object p1
.end method
