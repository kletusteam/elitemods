.class final Lv8/l2;
.super Ljava/lang/Object;

# interfaces
.implements Ld8/g$b;
.implements Ld8/g$c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ld8/g$b;",
        "Ld8/g$c<",
        "Lv8/l2;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lv8/l2;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lv8/l2;

    invoke-direct {v0}, Lv8/l2;-><init>()V

    sput-object v0, Lv8/l2;->a:Lv8/l2;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public fold(Ljava/lang/Object;Ll8/p;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(TR;",
            "Ll8/p<",
            "-TR;-",
            "Ld8/g$b;",
            "+TR;>;)TR;"
        }
    .end annotation

    invoke-static {p0, p1, p2}, Ld8/g$b$a;->a(Ld8/g$b;Ljava/lang/Object;Ll8/p;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public get(Ld8/g$c;)Ld8/g$b;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Ld8/g$b;",
            ">(",
            "Ld8/g$c<",
            "TE;>;)TE;"
        }
    .end annotation

    invoke-static {p0, p1}, Ld8/g$b$a;->b(Ld8/g$b;Ld8/g$c;)Ld8/g$b;

    move-result-object p1

    return-object p1
.end method

.method public getKey()Ld8/g$c;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ld8/g$c<",
            "*>;"
        }
    .end annotation

    return-object p0
.end method

.method public minusKey(Ld8/g$c;)Ld8/g;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ld8/g$c<",
            "*>;)",
            "Ld8/g;"
        }
    .end annotation

    invoke-static {p0, p1}, Ld8/g$b$a;->c(Ld8/g$b;Ld8/g$c;)Ld8/g;

    move-result-object p1

    return-object p1
.end method

.method public plus(Ld8/g;)Ld8/g;
    .locals 0

    invoke-static {p0, p1}, Ld8/g$b$a;->d(Ld8/g$b;Ld8/g;)Ld8/g;

    move-result-object p1

    return-object p1
.end method
