.class public final Lv8/k1$a;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lv8/k1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# direct methods
.method public static synthetic a(Lv8/k1;Ljava/util/concurrent/CancellationException;ILjava/lang/Object;)V
    .locals 0

    if-nez p3, :cond_1

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    const/4 p1, 0x0

    :cond_0
    invoke-interface {p0, p1}, Lv8/k1;->J(Ljava/util/concurrent/CancellationException;)V

    return-void

    :cond_1
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    const-string p1, "Super calls with default arguments not supported in this target, function: cancel"

    invoke-direct {p0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static b(Lv8/k1;Ljava/lang/Object;Ll8/p;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Lv8/k1;",
            "TR;",
            "Ll8/p<",
            "-TR;-",
            "Ld8/g$b;",
            "+TR;>;)TR;"
        }
    .end annotation

    invoke-static {p0, p1, p2}, Ld8/g$b$a;->a(Ld8/g$b;Ljava/lang/Object;Ll8/p;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method public static c(Lv8/k1;Ld8/g$c;)Ld8/g$b;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Ld8/g$b;",
            ">(",
            "Lv8/k1;",
            "Ld8/g$c<",
            "TE;>;)TE;"
        }
    .end annotation

    invoke-static {p0, p1}, Ld8/g$b$a;->b(Ld8/g$b;Ld8/g$c;)Ld8/g$b;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic d(Lv8/k1;ZZLl8/l;ILjava/lang/Object;)Lv8/u0;
    .locals 0

    if-nez p5, :cond_2

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    const/4 p1, 0x0

    :cond_0
    and-int/lit8 p4, p4, 0x2

    if-eqz p4, :cond_1

    const/4 p2, 0x1

    :cond_1
    invoke-interface {p0, p1, p2, p3}, Lv8/k1;->F(ZZLl8/l;)Lv8/u0;

    move-result-object p0

    return-object p0

    :cond_2
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    const-string p1, "Super calls with default arguments not supported in this target, function: invokeOnCompletion"

    invoke-direct {p0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static e(Lv8/k1;Ld8/g$c;)Ld8/g;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lv8/k1;",
            "Ld8/g$c<",
            "*>;)",
            "Ld8/g;"
        }
    .end annotation

    invoke-static {p0, p1}, Ld8/g$b$a;->c(Ld8/g$b;Ld8/g$c;)Ld8/g;

    move-result-object p0

    return-object p0
.end method

.method public static f(Lv8/k1;Ld8/g;)Ld8/g;
    .locals 0

    invoke-static {p0, p1}, Ld8/g$b$a;->d(Ld8/g$b;Ld8/g;)Ld8/g;

    move-result-object p0

    return-object p0
.end method
