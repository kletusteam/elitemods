.class final synthetic Lv8/p1;
.super Ljava/lang/Object;


# direct methods
.method public static final a(Lv8/k1;)Lv8/r;
    .locals 1

    new-instance v0, Lv8/n1;

    invoke-direct {v0, p0}, Lv8/n1;-><init>(Lv8/k1;)V

    return-object v0
.end method

.method public static synthetic b(Lv8/k1;ILjava/lang/Object;)Lv8/r;
    .locals 0

    and-int/lit8 p1, p1, 0x1

    if-eqz p1, :cond_0

    const/4 p0, 0x0

    :cond_0
    invoke-static {p0}, Lv8/o1;->a(Lv8/k1;)Lv8/r;

    move-result-object p0

    return-object p0
.end method

.method public static final c(Ld8/g;Ljava/util/concurrent/CancellationException;)V
    .locals 1

    sget-object v0, Lv8/k1;->d0:Lv8/k1$b;

    invoke-interface {p0, v0}, Ld8/g;->get(Ld8/g$c;)Ld8/g$b;

    move-result-object p0

    check-cast p0, Lv8/k1;

    if-nez p0, :cond_0

    goto :goto_0

    :cond_0
    invoke-interface {p0, p1}, Lv8/k1;->J(Ljava/util/concurrent/CancellationException;)V

    :goto_0
    return-void
.end method

.method public static synthetic d(Ld8/g;Ljava/util/concurrent/CancellationException;ILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    const/4 p1, 0x0

    :cond_0
    invoke-static {p0, p1}, Lv8/o1;->c(Ld8/g;Ljava/util/concurrent/CancellationException;)V

    return-void
.end method

.method public static final e(Lv8/k1;Lv8/u0;)Lv8/u0;
    .locals 1

    new-instance v0, Lv8/w0;

    invoke-direct {v0, p1}, Lv8/w0;-><init>(Lv8/u0;)V

    invoke-interface {p0, v0}, Lv8/k1;->L(Ll8/l;)Lv8/u0;

    move-result-object p0

    return-object p0
.end method

.method public static final f(Ld8/g;)V
    .locals 1

    sget-object v0, Lv8/k1;->d0:Lv8/k1$b;

    invoke-interface {p0, v0}, Ld8/g;->get(Ld8/g$c;)Ld8/g$b;

    move-result-object p0

    check-cast p0, Lv8/k1;

    if-nez p0, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {p0}, Lv8/o1;->g(Lv8/k1;)V

    :goto_0
    return-void
.end method

.method public static final g(Lv8/k1;)V
    .locals 1

    invoke-interface {p0}, Lv8/k1;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    invoke-interface {p0}, Lv8/k1;->C()Ljava/util/concurrent/CancellationException;

    move-result-object p0

    throw p0
.end method
