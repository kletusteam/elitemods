.class public Lv8/n1;
.super Lv8/r1;

# interfaces
.implements Lv8/r;


# instance fields
.field private final b:Z


# direct methods
.method public constructor <init>(Lv8/k1;)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lv8/r1;-><init>(Z)V

    invoke-virtual {p0, p1}, Lv8/r1;->X(Lv8/k1;)V

    invoke-direct {p0}, Lv8/n1;->y0()Z

    move-result p1

    iput-boolean p1, p0, Lv8/n1;->b:Z

    return-void
.end method

.method private final y0()Z
    .locals 4

    invoke-virtual {p0}, Lv8/r1;->T()Lv8/o;

    move-result-object v0

    instance-of v1, v0, Lv8/p;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    check-cast v0, Lv8/p;

    goto :goto_0

    :cond_0
    move-object v0, v2

    :goto_0
    if-nez v0, :cond_1

    move-object v0, v2

    goto :goto_1

    :cond_1
    invoke-virtual {v0}, Lv8/q1;->x()Lv8/r1;

    move-result-object v0

    :goto_1
    const/4 v1, 0x0

    if-nez v0, :cond_2

    return v1

    :cond_2
    invoke-virtual {v0}, Lv8/r1;->Q()Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v0, 0x1

    return v0

    :cond_3
    invoke-virtual {v0}, Lv8/r1;->T()Lv8/o;

    move-result-object v0

    instance-of v3, v0, Lv8/p;

    if-eqz v3, :cond_4

    check-cast v0, Lv8/p;

    goto :goto_2

    :cond_4
    move-object v0, v2

    :goto_2
    if-nez v0, :cond_5

    move-object v0, v2

    goto :goto_3

    :cond_5
    invoke-virtual {v0}, Lv8/q1;->x()Lv8/r1;

    move-result-object v0

    :goto_3
    if-nez v0, :cond_2

    return v1
.end method


# virtual methods
.method public Q()Z
    .locals 1

    iget-boolean v0, p0, Lv8/n1;->b:Z

    return v0
.end method

.method public R()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
