.class public final Lv8/s0;
.super Ljava/lang/Object;


# direct methods
.method public static final a(Lv8/r0;I)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lv8/r0<",
            "-TT;>;I)V"
        }
    .end annotation

    invoke-virtual {p0}, Lv8/r0;->b()Ld8/d;

    move-result-object v0

    const/4 v1, 0x4

    if-ne p1, v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-nez v1, :cond_2

    instance-of v2, v0, Lkotlinx/coroutines/internal/e;

    if-eqz v2, :cond_2

    invoke-static {p1}, Lv8/s0;->b(I)Z

    move-result p1

    iget v2, p0, Lv8/r0;->c:I

    invoke-static {v2}, Lv8/s0;->b(I)Z

    move-result v2

    if-ne p1, v2, :cond_2

    move-object p1, v0

    check-cast p1, Lkotlinx/coroutines/internal/e;

    iget-object p1, p1, Lkotlinx/coroutines/internal/e;->d:Lv8/a0;

    invoke-interface {v0}, Ld8/d;->c()Ld8/g;

    move-result-object v0

    invoke-virtual {p1, v0}, Lv8/a0;->V(Ld8/g;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p1, v0, p0}, Lv8/a0;->U(Ld8/g;Ljava/lang/Runnable;)V

    goto :goto_1

    :cond_1
    invoke-static {p0}, Lv8/s0;->e(Lv8/r0;)V

    goto :goto_1

    :cond_2
    invoke-static {p0, v0, v1}, Lv8/s0;->d(Lv8/r0;Ld8/d;Z)V

    :goto_1
    return-void
.end method

.method public static final b(I)Z
    .locals 2

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v1, 0x2

    if-ne p0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :cond_1
    :goto_0
    return v0
.end method

.method public static final c(I)Z
    .locals 1

    const/4 v0, 0x2

    if-ne p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static final d(Lv8/r0;Ld8/d;Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lv8/r0<",
            "-TT;>;",
            "Ld8/d<",
            "-TT;>;Z)V"
        }
    .end annotation

    invoke-virtual {p0}, Lv8/r0;->j()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Lv8/r0;->d(Ljava/lang/Object;)Ljava/lang/Throwable;

    move-result-object v1

    if-eqz v1, :cond_0

    sget-object p0, La8/m;->a:La8/m$a;

    invoke-static {v1}, La8/n;->a(Ljava/lang/Throwable;)Ljava/lang/Object;

    move-result-object p0

    goto :goto_0

    :cond_0
    sget-object v1, La8/m;->a:La8/m$a;

    invoke-virtual {p0, v0}, Lv8/r0;->g(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    :goto_0
    invoke-static {p0}, La8/m;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    if-eqz p2, :cond_5

    check-cast p1, Lkotlinx/coroutines/internal/e;

    iget-object p2, p1, Lkotlinx/coroutines/internal/e;->e:Ld8/d;

    iget-object v0, p1, Lkotlinx/coroutines/internal/e;->g:Ljava/lang/Object;

    invoke-interface {p2}, Ld8/d;->c()Ld8/g;

    move-result-object v1

    invoke-static {v1, v0}, Lkotlinx/coroutines/internal/b0;->c(Ld8/g;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    sget-object v2, Lkotlinx/coroutines/internal/b0;->a:Lkotlinx/coroutines/internal/x;

    if-eq v0, v2, :cond_1

    invoke-static {p2, v1, v0}, Lv8/z;->g(Ld8/d;Ld8/g;Ljava/lang/Object;)Lv8/k2;

    move-result-object p2

    goto :goto_1

    :cond_1
    const/4 p2, 0x0

    :goto_1
    :try_start_0
    iget-object p1, p1, Lkotlinx/coroutines/internal/e;->e:Ld8/d;

    invoke-interface {p1, p0}, Ld8/d;->i(Ljava/lang/Object;)V

    sget-object p0, La8/v;->a:La8/v;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lv8/k2;->C0()Z

    move-result p0

    if-eqz p0, :cond_6

    :cond_2
    invoke-static {v1, v0}, Lkotlinx/coroutines/internal/b0;->a(Ld8/g;Ljava/lang/Object;)V

    goto :goto_2

    :catchall_0
    move-exception p0

    if-eqz p2, :cond_3

    invoke-virtual {p2}, Lv8/k2;->C0()Z

    move-result p1

    if-eqz p1, :cond_4

    :cond_3
    invoke-static {v1, v0}, Lkotlinx/coroutines/internal/b0;->a(Ld8/g;Ljava/lang/Object;)V

    :cond_4
    throw p0

    :cond_5
    invoke-interface {p1, p0}, Ld8/d;->i(Ljava/lang/Object;)V

    :cond_6
    :goto_2
    return-void
.end method

.method private static final e(Lv8/r0;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lv8/r0<",
            "*>;)V"
        }
    .end annotation

    sget-object v0, Lv8/f2;->a:Lv8/f2;

    invoke-virtual {v0}, Lv8/f2;->a()Lv8/y0;

    move-result-object v0

    invoke-virtual {v0}, Lv8/y0;->d0()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, p0}, Lv8/y0;->Z(Lv8/r0;)V

    goto :goto_1

    :cond_0
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lv8/y0;->b0(Z)V

    :try_start_0
    invoke-virtual {p0}, Lv8/r0;->b()Ld8/d;

    move-result-object v2

    invoke-static {p0, v2, v1}, Lv8/s0;->d(Lv8/r0;Ld8/d;Z)V

    :cond_1
    invoke-virtual {v0}, Lv8/y0;->f0()Z

    move-result v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_1

    goto :goto_0

    :catchall_0
    move-exception v2

    const/4 v3, 0x0

    :try_start_1
    invoke-virtual {p0, v2, v3}, Lv8/r0;->h(Ljava/lang/Throwable;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :goto_0
    invoke-virtual {v0, v1}, Lv8/y0;->X(Z)V

    :goto_1
    return-void

    :catchall_1
    move-exception p0

    invoke-virtual {v0, v1}, Lv8/y0;->X(Z)V

    throw p0
.end method
