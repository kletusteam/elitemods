.class public Lv8/r1;
.super Ljava/lang/Object;

# interfaces
.implements Lv8/k1;
.implements Lv8/q;
.implements Lv8/z1;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lv8/r1$c;,
        Lv8/r1$b;,
        Lv8/r1$a;
    }
.end annotation


# static fields
.field private static final synthetic a:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;


# instance fields
.field private volatile synthetic _parentHandle:Ljava/lang/Object;

.field private volatile synthetic _state:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const-class v0, Lv8/r1;

    const-class v1, Ljava/lang/Object;

    const-string v2, "_state"

    invoke-static {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->newUpdater(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    move-result-object v0

    sput-object v0, Lv8/r1;->a:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    return-void
.end method

.method public constructor <init>(Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p1, :cond_0

    invoke-static {}, Lv8/s1;->c()Lv8/x0;

    move-result-object p1

    goto :goto_0

    :cond_0
    invoke-static {}, Lv8/s1;->d()Lv8/x0;

    move-result-object p1

    :goto_0
    iput-object p1, p0, Lv8/r1;->_state:Ljava/lang/Object;

    const/4 p1, 0x0

    iput-object p1, p0, Lv8/r1;->_parentHandle:Ljava/lang/Object;

    return-void
.end method

.method private final E(Lv8/f1;Ljava/lang/Object;)V
    .locals 3

    invoke-virtual {p0}, Lv8/r1;->T()Lv8/o;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Lv8/u0;->c()V

    sget-object v0, Lv8/x1;->a:Lv8/x1;

    invoke-virtual {p0, v0}, Lv8/r1;->n0(Lv8/o;)V

    :goto_0
    instance-of v0, p2, Lv8/t;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    check-cast p2, Lv8/t;

    goto :goto_1

    :cond_1
    move-object p2, v1

    :goto_1
    if-nez p2, :cond_2

    goto :goto_2

    :cond_2
    iget-object v1, p2, Lv8/t;->a:Ljava/lang/Throwable;

    :goto_2
    instance-of p2, p1, Lv8/q1;

    if-eqz p2, :cond_3

    :try_start_0
    move-object p2, p1

    check-cast p2, Lv8/q1;

    invoke-virtual {p2, v1}, Lv8/v;->w(Ljava/lang/Throwable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_3

    :catchall_0
    move-exception p2

    new-instance v0, Lv8/w;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Exception in completion handler "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, " for "

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1, p2}, Lv8/w;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {p0, v0}, Lv8/r1;->W(Ljava/lang/Throwable;)V

    goto :goto_3

    :cond_3
    invoke-interface {p1}, Lv8/f1;->f()Lv8/w1;

    move-result-object p1

    if-nez p1, :cond_4

    goto :goto_3

    :cond_4
    invoke-direct {p0, p1, v1}, Lv8/r1;->g0(Lv8/w1;Ljava/lang/Throwable;)V

    :goto_3
    return-void
.end method

.method private final G(Lv8/r1$c;Lv8/p;Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0, p2}, Lv8/r1;->e0(Lkotlinx/coroutines/internal/m;)Lv8/p;

    move-result-object p2

    if-eqz p2, :cond_0

    invoke-direct {p0, p1, p2, p3}, Lv8/r1;->x0(Lv8/r1$c;Lv8/p;Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_0

    return-void

    :cond_0
    invoke-direct {p0, p1, p3}, Lv8/r1;->I(Lv8/r1$c;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {p0, p1}, Lv8/r1;->o(Ljava/lang/Object;)V

    return-void
.end method

.method private final H(Ljava/lang/Object;)Ljava/lang/Throwable;
    .locals 2

    if-nez p1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    instance-of v0, p1, Ljava/lang/Throwable;

    :goto_0
    if-eqz v0, :cond_1

    check-cast p1, Ljava/lang/Throwable;

    if-nez p1, :cond_2

    const/4 p1, 0x0

    new-instance v0, Lv8/l1;

    invoke-static {p0}, Lv8/r1;->a(Lv8/r1;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1, p0}, Lv8/l1;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lv8/k1;)V

    move-object p1, v0

    goto :goto_1

    :cond_1
    const-string v0, "null cannot be cast to non-null type kotlinx.coroutines.ParentJob"

    invoke-static {p1, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    check-cast p1, Lv8/z1;

    invoke-interface {p1}, Lv8/z1;->B()Ljava/util/concurrent/CancellationException;

    move-result-object p1

    :cond_2
    :goto_1
    return-object p1
.end method

.method private final I(Lv8/r1$c;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    instance-of v0, p2, Lv8/t;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    move-object v0, p2

    check-cast v0, Lv8/t;

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    if-nez v0, :cond_1

    move-object v0, v1

    goto :goto_1

    :cond_1
    iget-object v0, v0, Lv8/t;->a:Ljava/lang/Throwable;

    :goto_1
    monitor-enter p1

    :try_start_0
    invoke-virtual {p1}, Lv8/r1$c;->g()Z

    move-result v2

    invoke-virtual {p1, v0}, Lv8/r1$c;->j(Ljava/lang/Throwable;)Ljava/util/List;

    move-result-object v3

    invoke-direct {p0, p1, v3}, Lv8/r1;->P(Lv8/r1$c;Ljava/util/List;)Ljava/lang/Throwable;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-direct {p0, v4, v3}, Lv8/r1;->l(Ljava/lang/Throwable;Ljava/util/List;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    monitor-exit p1

    const/4 v3, 0x0

    if-nez v4, :cond_3

    goto :goto_2

    :cond_3
    if-ne v4, v0, :cond_4

    goto :goto_2

    :cond_4
    new-instance p2, Lv8/t;

    const/4 v0, 0x2

    invoke-direct {p2, v4, v3, v0, v1}, Lv8/t;-><init>(Ljava/lang/Throwable;ZILm8/g;)V

    :goto_2
    if-eqz v4, :cond_7

    invoke-direct {p0, v4}, Lv8/r1;->x(Ljava/lang/Throwable;)Z

    move-result v0

    if-nez v0, :cond_5

    invoke-virtual {p0, v4}, Lv8/r1;->V(Ljava/lang/Throwable;)Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_5
    const/4 v3, 0x1

    :cond_6
    if-eqz v3, :cond_7

    const-string v0, "null cannot be cast to non-null type kotlinx.coroutines.CompletedExceptionally"

    invoke-static {p2, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-object v0, p2

    check-cast v0, Lv8/t;

    invoke-virtual {v0}, Lv8/t;->b()Z

    :cond_7
    if-nez v2, :cond_8

    invoke-virtual {p0, v4}, Lv8/r1;->h0(Ljava/lang/Throwable;)V

    :cond_8
    invoke-virtual {p0, p2}, Lv8/r1;->i0(Ljava/lang/Object;)V

    sget-object v0, Lv8/r1;->a:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    invoke-static {p2}, Lv8/s1;->g(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, p0, p1, v1}, La8/o;->a(Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    invoke-direct {p0, p1, p2}, Lv8/r1;->E(Lv8/f1;Ljava/lang/Object;)V

    return-object p2

    :catchall_0
    move-exception p2

    monitor-exit p1

    throw p2
.end method

.method private final N(Lv8/f1;)Lv8/p;
    .locals 2

    instance-of v0, p1, Lv8/p;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lv8/p;

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    if-nez v0, :cond_2

    invoke-interface {p1}, Lv8/f1;->f()Lv8/w1;

    move-result-object p1

    if-nez p1, :cond_1

    goto :goto_1

    :cond_1
    invoke-direct {p0, p1}, Lv8/r1;->e0(Lkotlinx/coroutines/internal/m;)Lv8/p;

    move-result-object v1

    goto :goto_1

    :cond_2
    move-object v1, v0

    :goto_1
    return-object v1
.end method

.method private final O(Ljava/lang/Object;)Ljava/lang/Throwable;
    .locals 2

    instance-of v0, p1, Lv8/t;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    check-cast p1, Lv8/t;

    goto :goto_0

    :cond_0
    move-object p1, v1

    :goto_0
    if-nez p1, :cond_1

    goto :goto_1

    :cond_1
    iget-object v1, p1, Lv8/t;->a:Ljava/lang/Throwable;

    :goto_1
    return-object v1
.end method

.method private final P(Lv8/r1$c;Ljava/util/List;)Ljava/lang/Throwable;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lv8/r1$c;",
            "Ljava/util/List<",
            "+",
            "Ljava/lang/Throwable;",
            ">;)",
            "Ljava/lang/Throwable;"
        }
    .end annotation

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lv8/r1$c;->g()Z

    move-result p1

    if-eqz p1, :cond_0

    new-instance p1, Lv8/l1;

    invoke-static {p0}, Lv8/r1;->a(Lv8/r1;)Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2, v1, p0}, Lv8/l1;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lv8/k1;)V

    return-object p1

    :cond_0
    return-object v1

    :cond_1
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    const/4 v2, 0x1

    if-eqz v0, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Ljava/lang/Throwable;

    instance-of v3, v3, Ljava/util/concurrent/CancellationException;

    xor-int/2addr v3, v2

    if-eqz v3, :cond_2

    goto :goto_0

    :cond_3
    move-object v0, v1

    :goto_0
    check-cast v0, Ljava/lang/Throwable;

    if-eqz v0, :cond_4

    return-object v0

    :cond_4
    const/4 p1, 0x0

    invoke-interface {p2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    instance-of v3, v0, Lv8/g2;

    if-eqz v3, :cond_8

    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_5
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v4, v3

    check-cast v4, Ljava/lang/Throwable;

    if-eq v4, v0, :cond_6

    instance-of v4, v4, Lv8/g2;

    if-eqz v4, :cond_6

    move v4, v2

    goto :goto_1

    :cond_6
    move v4, p1

    :goto_1
    if-eqz v4, :cond_5

    move-object v1, v3

    :cond_7
    check-cast v1, Ljava/lang/Throwable;

    if-eqz v1, :cond_8

    return-object v1

    :cond_8
    return-object v0
.end method

.method private final S(Lv8/f1;)Lv8/w1;
    .locals 2

    invoke-interface {p1}, Lv8/f1;->f()Lv8/w1;

    move-result-object v0

    if-nez v0, :cond_2

    instance-of v0, p1, Lv8/x0;

    if-eqz v0, :cond_0

    new-instance v0, Lv8/w1;

    invoke-direct {v0}, Lv8/w1;-><init>()V

    goto :goto_0

    :cond_0
    instance-of v0, p1, Lv8/q1;

    if-eqz v0, :cond_1

    check-cast p1, Lv8/q1;

    invoke-direct {p0, p1}, Lv8/r1;->l0(Lv8/q1;)V

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "State should have list: "

    invoke-static {v1, p1}, Lm8/i;->l(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    :goto_0
    return-object v0
.end method

.method public static final synthetic a(Lv8/r1;)Ljava/lang/String;
    .locals 0

    invoke-virtual {p0}, Lv8/r1;->A()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private final a0(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    const/4 v0, 0x0

    move-object v1, v0

    :cond_0
    :goto_0
    invoke-virtual {p0}, Lv8/r1;->U()Ljava/lang/Object;

    move-result-object v2

    instance-of v3, v2, Lv8/r1$c;

    if-eqz v3, :cond_7

    monitor-enter v2

    :try_start_0
    move-object v3, v2

    check-cast v3, Lv8/r1$c;

    invoke-virtual {v3}, Lv8/r1$c;->i()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-static {}, Lv8/s1;->f()Lkotlinx/coroutines/internal/x;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v2

    return-object p1

    :cond_1
    :try_start_1
    move-object v3, v2

    check-cast v3, Lv8/r1$c;

    invoke-virtual {v3}, Lv8/r1$c;->g()Z

    move-result v3

    if-nez p1, :cond_2

    if-nez v3, :cond_4

    :cond_2
    if-nez v1, :cond_3

    invoke-direct {p0, p1}, Lv8/r1;->H(Ljava/lang/Object;)Ljava/lang/Throwable;

    move-result-object v1

    :cond_3
    move-object p1, v2

    check-cast p1, Lv8/r1$c;

    invoke-virtual {p1, v1}, Lv8/r1$c;->a(Ljava/lang/Throwable;)V

    :cond_4
    move-object p1, v2

    check-cast p1, Lv8/r1$c;

    invoke-virtual {p1}, Lv8/r1$c;->e()Ljava/lang/Throwable;

    move-result-object p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    xor-int/lit8 v1, v3, 0x1

    if-eqz v1, :cond_5

    move-object v0, p1

    :cond_5
    monitor-exit v2

    if-nez v0, :cond_6

    goto :goto_1

    :cond_6
    check-cast v2, Lv8/r1$c;

    invoke-virtual {v2}, Lv8/r1$c;->f()Lv8/w1;

    move-result-object p1

    invoke-direct {p0, p1, v0}, Lv8/r1;->f0(Lv8/w1;Ljava/lang/Throwable;)V

    :goto_1
    invoke-static {}, Lv8/s1;->a()Lkotlinx/coroutines/internal/x;

    move-result-object p1

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit v2

    throw p1

    :cond_7
    instance-of v3, v2, Lv8/f1;

    if-eqz v3, :cond_c

    if-nez v1, :cond_8

    invoke-direct {p0, p1}, Lv8/r1;->H(Ljava/lang/Object;)Ljava/lang/Throwable;

    move-result-object v1

    :cond_8
    move-object v3, v2

    check-cast v3, Lv8/f1;

    invoke-interface {v3}, Lv8/f1;->b()Z

    move-result v4

    if-eqz v4, :cond_9

    invoke-direct {p0, v3, v1}, Lv8/r1;->u0(Lv8/f1;Ljava/lang/Throwable;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {}, Lv8/s1;->a()Lkotlinx/coroutines/internal/x;

    move-result-object p1

    return-object p1

    :cond_9
    new-instance v3, Lv8/t;

    const/4 v4, 0x0

    const/4 v5, 0x2

    invoke-direct {v3, v1, v4, v5, v0}, Lv8/t;-><init>(Ljava/lang/Throwable;ZILm8/g;)V

    invoke-direct {p0, v2, v3}, Lv8/r1;->v0(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-static {}, Lv8/s1;->a()Lkotlinx/coroutines/internal/x;

    move-result-object v4

    if-eq v3, v4, :cond_b

    invoke-static {}, Lv8/s1;->b()Lkotlinx/coroutines/internal/x;

    move-result-object v2

    if-ne v3, v2, :cond_a

    goto/16 :goto_0

    :cond_a
    return-object v3

    :cond_b
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Cannot happen in "

    invoke-static {v0, v2}, Lm8/i;->l(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_c
    invoke-static {}, Lv8/s1;->f()Lkotlinx/coroutines/internal/x;

    move-result-object p1

    return-object p1
.end method

.method private final c0(Ll8/l;Z)Lv8/q1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ll8/l<",
            "-",
            "Ljava/lang/Throwable;",
            "La8/v;",
            ">;Z)",
            "Lv8/q1;"
        }
    .end annotation

    const/4 v0, 0x0

    if-eqz p2, :cond_1

    instance-of p2, p1, Lv8/m1;

    if-eqz p2, :cond_0

    move-object v0, p1

    check-cast v0, Lv8/m1;

    :cond_0
    if-nez v0, :cond_4

    new-instance v0, Lv8/i1;

    invoke-direct {v0, p1}, Lv8/i1;-><init>(Ll8/l;)V

    goto :goto_2

    :cond_1
    instance-of p2, p1, Lv8/q1;

    if-eqz p2, :cond_2

    move-object p2, p1

    check-cast p2, Lv8/q1;

    goto :goto_0

    :cond_2
    move-object p2, v0

    :goto_0
    if-nez p2, :cond_3

    goto :goto_1

    :cond_3
    move-object v0, p2

    :goto_1
    if-nez v0, :cond_4

    new-instance v0, Lv8/j1;

    invoke-direct {v0, p1}, Lv8/j1;-><init>(Ll8/l;)V

    :cond_4
    :goto_2
    invoke-virtual {v0, p0}, Lv8/q1;->y(Lv8/r1;)V

    return-object v0
.end method

.method public static final synthetic d(Lv8/r1;Lv8/r1$c;Lv8/p;Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lv8/r1;->G(Lv8/r1$c;Lv8/p;Ljava/lang/Object;)V

    return-void
.end method

.method private final e0(Lkotlinx/coroutines/internal/m;)Lv8/p;
    .locals 1

    :goto_0
    invoke-virtual {p1}, Lkotlinx/coroutines/internal/m;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lkotlinx/coroutines/internal/m;->q()Lkotlinx/coroutines/internal/m;

    move-result-object p1

    goto :goto_0

    :cond_0
    :goto_1
    invoke-virtual {p1}, Lkotlinx/coroutines/internal/m;->p()Lkotlinx/coroutines/internal/m;

    move-result-object p1

    invoke-virtual {p1}, Lkotlinx/coroutines/internal/m;->r()Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    instance-of v0, p1, Lv8/p;

    if-eqz v0, :cond_2

    check-cast p1, Lv8/p;

    return-object p1

    :cond_2
    instance-of v0, p1, Lv8/w1;

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    return-object p1
.end method

.method private final f0(Lv8/w1;Ljava/lang/Throwable;)V
    .locals 7

    invoke-virtual {p0, p2}, Lv8/r1;->h0(Ljava/lang/Throwable;)V

    invoke-virtual {p1}, Lkotlinx/coroutines/internal/m;->o()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlinx/coroutines/internal/m;

    const/4 v1, 0x0

    move-object v2, v1

    :goto_0
    invoke-static {v0, p1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    instance-of v3, v0, Lv8/m1;

    if-eqz v3, :cond_1

    move-object v3, v0

    check-cast v3, Lv8/q1;

    :try_start_0
    invoke-virtual {v3, p2}, Lv8/v;->w(Ljava/lang/Throwable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v4

    if-nez v2, :cond_0

    move-object v5, v1

    goto :goto_1

    :cond_0
    invoke-static {v2, v4}, La8/a;->a(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    move-object v5, v2

    :goto_1
    if-nez v5, :cond_1

    new-instance v2, Lv8/w;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Exception in completion handler "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v3, " for "

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v4}, Lv8/w;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_1
    :goto_2
    invoke-virtual {v0}, Lkotlinx/coroutines/internal/m;->p()Lkotlinx/coroutines/internal/m;

    move-result-object v0

    goto :goto_0

    :cond_2
    if-nez v2, :cond_3

    goto :goto_3

    :cond_3
    invoke-virtual {p0, v2}, Lv8/r1;->W(Ljava/lang/Throwable;)V

    :goto_3
    invoke-direct {p0, p2}, Lv8/r1;->x(Ljava/lang/Throwable;)Z

    return-void
.end method

.method private final g0(Lv8/w1;Ljava/lang/Throwable;)V
    .locals 7

    invoke-virtual {p1}, Lkotlinx/coroutines/internal/m;->o()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlinx/coroutines/internal/m;

    const/4 v1, 0x0

    move-object v2, v1

    :goto_0
    invoke-static {v0, p1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    instance-of v3, v0, Lv8/q1;

    if-eqz v3, :cond_1

    move-object v3, v0

    check-cast v3, Lv8/q1;

    :try_start_0
    invoke-virtual {v3, p2}, Lv8/v;->w(Ljava/lang/Throwable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v4

    if-nez v2, :cond_0

    move-object v5, v1

    goto :goto_1

    :cond_0
    invoke-static {v2, v4}, La8/a;->a(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    move-object v5, v2

    :goto_1
    if-nez v5, :cond_1

    new-instance v2, Lv8/w;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Exception in completion handler "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v3, " for "

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v4}, Lv8/w;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_1
    :goto_2
    invoke-virtual {v0}, Lkotlinx/coroutines/internal/m;->p()Lkotlinx/coroutines/internal/m;

    move-result-object v0

    goto :goto_0

    :cond_2
    if-nez v2, :cond_3

    goto :goto_3

    :cond_3
    invoke-virtual {p0, v2}, Lv8/r1;->W(Ljava/lang/Throwable;)V

    :goto_3
    return-void
.end method

.method private final j(Ljava/lang/Object;Lv8/w1;Lv8/q1;)Z
    .locals 2

    new-instance v0, Lv8/r1$d;

    invoke-direct {v0, p3, p0, p1}, Lv8/r1$d;-><init>(Lkotlinx/coroutines/internal/m;Lv8/r1;Ljava/lang/Object;)V

    :goto_0
    invoke-virtual {p2}, Lkotlinx/coroutines/internal/m;->q()Lkotlinx/coroutines/internal/m;

    move-result-object p1

    invoke-virtual {p1, p3, p2, v0}, Lkotlinx/coroutines/internal/m;->v(Lkotlinx/coroutines/internal/m;Lkotlinx/coroutines/internal/m;Lkotlinx/coroutines/internal/m$a;)I

    move-result p1

    const/4 v1, 0x1

    if-eq p1, v1, :cond_1

    const/4 v1, 0x2

    if-eq p1, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :cond_1
    return v1
.end method

.method private final k0(Lv8/x0;)V
    .locals 2

    new-instance v0, Lv8/w1;

    invoke-direct {v0}, Lv8/w1;-><init>()V

    invoke-virtual {p1}, Lv8/x0;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    new-instance v1, Lv8/e1;

    invoke-direct {v1, v0}, Lv8/e1;-><init>(Lv8/w1;)V

    move-object v0, v1

    :goto_0
    sget-object v1, Lv8/r1;->a:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    invoke-static {v1, p0, p1, v0}, La8/o;->a(Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    return-void
.end method

.method private final l(Ljava/lang/Throwable;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Throwable;",
            "Ljava/util/List<",
            "+",
            "Ljava/lang/Throwable;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    return-void

    :cond_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    new-instance v1, Ljava/util/IdentityHashMap;

    invoke-direct {v1, v0}, Ljava/util/IdentityHashMap;-><init>(I)V

    invoke-static {v1}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_1
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Throwable;

    if-eq v1, p1, :cond_1

    if-eq v1, p1, :cond_1

    instance-of v2, v1, Ljava/util/concurrent/CancellationException;

    if-nez v2, :cond_1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {p1, v1}, La8/a;->a(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_2
    return-void
.end method

.method private final l0(Lv8/q1;)V
    .locals 2

    new-instance v0, Lv8/w1;

    invoke-direct {v0}, Lv8/w1;-><init>()V

    invoke-virtual {p1, v0}, Lkotlinx/coroutines/internal/m;->k(Lkotlinx/coroutines/internal/m;)Z

    invoke-virtual {p1}, Lkotlinx/coroutines/internal/m;->p()Lkotlinx/coroutines/internal/m;

    move-result-object v0

    sget-object v1, Lv8/r1;->a:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    invoke-static {v1, p0, p1, v0}, La8/o;->a(Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    return-void
.end method

.method private final o0(Ljava/lang/Object;)I
    .locals 4

    instance-of v0, p1, Lv8/x0;

    const/4 v1, -0x1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v0, :cond_2

    move-object v0, p1

    check-cast v0, Lv8/x0;

    invoke-virtual {v0}, Lv8/x0;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    return v3

    :cond_0
    sget-object v0, Lv8/r1;->a:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    invoke-static {}, Lv8/s1;->c()Lv8/x0;

    move-result-object v3

    invoke-static {v0, p0, p1, v3}, La8/o;->a(Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_1

    return v1

    :cond_1
    invoke-virtual {p0}, Lv8/r1;->j0()V

    return v2

    :cond_2
    instance-of v0, p1, Lv8/e1;

    if-eqz v0, :cond_4

    sget-object v0, Lv8/r1;->a:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    move-object v3, p1

    check-cast v3, Lv8/e1;

    invoke-virtual {v3}, Lv8/e1;->f()Lv8/w1;

    move-result-object v3

    invoke-static {v0, p0, p1, v3}, La8/o;->a(Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_3

    return v1

    :cond_3
    invoke-virtual {p0}, Lv8/r1;->j0()V

    return v2

    :cond_4
    return v3
.end method

.method private final p0(Ljava/lang/Object;)Ljava/lang/String;
    .locals 2

    instance-of v0, p1, Lv8/r1$c;

    const-string v1, "Active"

    if-eqz v0, :cond_1

    check-cast p1, Lv8/r1$c;

    invoke-virtual {p1}, Lv8/r1$c;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v1, "Cancelling"

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lv8/r1$c;->h()Z

    move-result p1

    if-eqz p1, :cond_5

    const-string v1, "Completing"

    goto :goto_0

    :cond_1
    instance-of v0, p1, Lv8/f1;

    if-eqz v0, :cond_3

    check-cast p1, Lv8/f1;

    invoke-interface {p1}, Lv8/f1;->b()Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const-string v1, "New"

    goto :goto_0

    :cond_3
    instance-of p1, p1, Lv8/t;

    if-eqz p1, :cond_4

    const-string v1, "Cancelled"

    goto :goto_0

    :cond_4
    const-string v1, "Completed"

    :cond_5
    :goto_0
    return-object v1
.end method

.method public static synthetic r0(Lv8/r1;Ljava/lang/Throwable;Ljava/lang/String;ILjava/lang/Object;)Ljava/util/concurrent/CancellationException;
    .locals 0

    if-nez p4, :cond_1

    and-int/lit8 p3, p3, 0x1

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    :cond_0
    invoke-virtual {p0, p1, p2}, Lv8/r1;->q0(Ljava/lang/Throwable;Ljava/lang/String;)Ljava/util/concurrent/CancellationException;

    move-result-object p0

    return-object p0

    :cond_1
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    const-string p1, "Super calls with default arguments not supported in this target, function: toCancellationException"

    invoke-direct {p0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method private final s(Ld8/d;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ld8/d<",
            "Ljava/lang/Object;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    new-instance v0, Lv8/r1$a;

    invoke-static {p1}, Le8/b;->b(Ld8/d;)Ld8/d;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lv8/r1$a;-><init>(Ld8/d;Lv8/r1;)V

    invoke-virtual {v0}, Lv8/k;->y()V

    new-instance v1, Lv8/a2;

    invoke-direct {v1, v0}, Lv8/a2;-><init>(Lv8/k;)V

    invoke-virtual {p0, v1}, Lv8/r1;->L(Ll8/l;)Lv8/u0;

    move-result-object v1

    invoke-static {v0, v1}, Lv8/l;->a(Lv8/j;Lv8/u0;)V

    invoke-virtual {v0}, Lv8/k;->v()Ljava/lang/Object;

    move-result-object v0

    invoke-static {}, Le8/b;->c()Ljava/lang/Object;

    move-result-object v1

    if-ne v0, v1, :cond_0

    invoke-static {p1}, Lf8/h;->c(Ld8/d;)V

    :cond_0
    return-object v0
.end method

.method private final t0(Lv8/f1;Ljava/lang/Object;)Z
    .locals 2

    sget-object v0, Lv8/r1;->a:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    invoke-static {p2}, Lv8/s1;->g(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, p0, p1, v1}, La8/o;->a(Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lv8/r1;->h0(Ljava/lang/Throwable;)V

    invoke-virtual {p0, p2}, Lv8/r1;->i0(Ljava/lang/Object;)V

    invoke-direct {p0, p1, p2}, Lv8/r1;->E(Lv8/f1;Ljava/lang/Object;)V

    const/4 p1, 0x1

    return p1
.end method

.method private final u0(Lv8/f1;Ljava/lang/Throwable;)Z
    .locals 4

    invoke-direct {p0, p1}, Lv8/r1;->S(Lv8/f1;)Lv8/w1;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    new-instance v2, Lv8/r1$c;

    invoke-direct {v2, v0, v1, p2}, Lv8/r1$c;-><init>(Lv8/w1;ZLjava/lang/Throwable;)V

    sget-object v3, Lv8/r1;->a:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    invoke-static {v3, p0, p1, v2}, La8/o;->a(Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_1

    return v1

    :cond_1
    invoke-direct {p0, v0, p2}, Lv8/r1;->f0(Lv8/w1;Ljava/lang/Throwable;)V

    const/4 p1, 0x1

    return p1
.end method

.method private final v0(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    instance-of v0, p1, Lv8/f1;

    if-nez v0, :cond_0

    invoke-static {}, Lv8/s1;->a()Lkotlinx/coroutines/internal/x;

    move-result-object p1

    return-object p1

    :cond_0
    instance-of v0, p1, Lv8/x0;

    if-nez v0, :cond_1

    instance-of v0, p1, Lv8/q1;

    if-eqz v0, :cond_3

    :cond_1
    instance-of v0, p1, Lv8/p;

    if-nez v0, :cond_3

    instance-of v0, p2, Lv8/t;

    if-nez v0, :cond_3

    check-cast p1, Lv8/f1;

    invoke-direct {p0, p1, p2}, Lv8/r1;->t0(Lv8/f1;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    return-object p2

    :cond_2
    invoke-static {}, Lv8/s1;->b()Lkotlinx/coroutines/internal/x;

    move-result-object p1

    return-object p1

    :cond_3
    check-cast p1, Lv8/f1;

    invoke-direct {p0, p1, p2}, Lv8/r1;->w0(Lv8/f1;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method private final w(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    :cond_0
    invoke-virtual {p0}, Lv8/r1;->U()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lv8/f1;

    if-eqz v1, :cond_2

    instance-of v1, v0, Lv8/r1$c;

    if-eqz v1, :cond_1

    move-object v1, v0

    check-cast v1, Lv8/r1$c;

    invoke-virtual {v1}, Lv8/r1$c;->h()Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_0

    :cond_1
    new-instance v1, Lv8/t;

    invoke-direct {p0, p1}, Lv8/r1;->H(Ljava/lang/Object;)Ljava/lang/Throwable;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lv8/t;-><init>(Ljava/lang/Throwable;ZILm8/g;)V

    invoke-direct {p0, v0, v1}, Lv8/r1;->v0(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {}, Lv8/s1;->b()Lkotlinx/coroutines/internal/x;

    move-result-object v1

    if-eq v0, v1, :cond_0

    return-object v0

    :cond_2
    :goto_0
    invoke-static {}, Lv8/s1;->a()Lkotlinx/coroutines/internal/x;

    move-result-object p1

    return-object p1
.end method

.method private final w0(Lv8/f1;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    invoke-direct {p0, p1}, Lv8/r1;->S(Lv8/f1;)Lv8/w1;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lv8/s1;->b()Lkotlinx/coroutines/internal/x;

    move-result-object p1

    return-object p1

    :cond_0
    instance-of v1, p1, Lv8/r1$c;

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    move-object v1, p1

    check-cast v1, Lv8/r1$c;

    goto :goto_0

    :cond_1
    move-object v1, v2

    :goto_0
    if-nez v1, :cond_2

    new-instance v1, Lv8/r1$c;

    const/4 v3, 0x0

    invoke-direct {v1, v0, v3, v2}, Lv8/r1$c;-><init>(Lv8/w1;ZLjava/lang/Throwable;)V

    :cond_2
    monitor-enter v1

    :try_start_0
    invoke-virtual {v1}, Lv8/r1$c;->h()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-static {}, Lv8/s1;->a()Lkotlinx/coroutines/internal/x;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object p1

    :cond_3
    const/4 v3, 0x1

    :try_start_1
    invoke-virtual {v1, v3}, Lv8/r1$c;->k(Z)V

    if-eq v1, p1, :cond_4

    sget-object v4, Lv8/r1;->a:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    invoke-static {v4, p0, p1, v1}, La8/o;->a(Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    invoke-static {}, Lv8/s1;->b()Lkotlinx/coroutines/internal/x;

    move-result-object p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v1

    return-object p1

    :cond_4
    :try_start_2
    invoke-virtual {v1}, Lv8/r1$c;->g()Z

    move-result v4

    instance-of v5, p2, Lv8/t;

    if-eqz v5, :cond_5

    move-object v5, p2

    check-cast v5, Lv8/t;

    goto :goto_1

    :cond_5
    move-object v5, v2

    :goto_1
    if-nez v5, :cond_6

    goto :goto_2

    :cond_6
    iget-object v5, v5, Lv8/t;->a:Ljava/lang/Throwable;

    invoke-virtual {v1, v5}, Lv8/r1$c;->a(Ljava/lang/Throwable;)V

    :goto_2
    invoke-virtual {v1}, Lv8/r1$c;->e()Ljava/lang/Throwable;

    move-result-object v5

    xor-int/2addr v3, v4

    if-eqz v3, :cond_7

    move-object v2, v5

    :cond_7
    sget-object v3, La8/v;->a:La8/v;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit v1

    if-nez v2, :cond_8

    goto :goto_3

    :cond_8
    invoke-direct {p0, v0, v2}, Lv8/r1;->f0(Lv8/w1;Ljava/lang/Throwable;)V

    :goto_3
    invoke-direct {p0, p1}, Lv8/r1;->N(Lv8/f1;)Lv8/p;

    move-result-object p1

    if-eqz p1, :cond_9

    invoke-direct {p0, v1, p1, p2}, Lv8/r1;->x0(Lv8/r1$c;Lv8/p;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_9

    sget-object p1, Lv8/s1;->b:Lkotlinx/coroutines/internal/x;

    return-object p1

    :cond_9
    invoke-direct {p0, v1, p2}, Lv8/r1;->I(Lv8/r1$c;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit v1

    throw p1
.end method

.method private final x(Ljava/lang/Throwable;)Z
    .locals 4

    invoke-virtual {p0}, Lv8/r1;->Z()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    :cond_0
    instance-of v0, p1, Ljava/util/concurrent/CancellationException;

    invoke-virtual {p0}, Lv8/r1;->T()Lv8/o;

    move-result-object v2

    if-eqz v2, :cond_4

    sget-object v3, Lv8/x1;->a:Lv8/x1;

    if-ne v2, v3, :cond_1

    goto :goto_1

    :cond_1
    invoke-interface {v2, p1}, Lv8/o;->e(Ljava/lang/Throwable;)Z

    move-result p1

    if-nez p1, :cond_3

    if-eqz v0, :cond_2

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :cond_3
    :goto_0
    return v1

    :cond_4
    :goto_1
    return v0
.end method

.method private final x0(Lv8/r1$c;Lv8/p;Ljava/lang/Object;)Z
    .locals 6

    :cond_0
    iget-object v0, p2, Lv8/p;->e:Lv8/q;

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-instance v3, Lv8/r1$b;

    invoke-direct {v3, p0, p1, p2, p3}, Lv8/r1$b;-><init>(Lv8/r1;Lv8/r1$c;Lv8/p;Ljava/lang/Object;)V

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lv8/k1$a;->d(Lv8/k1;ZZLl8/l;ILjava/lang/Object;)Lv8/u0;

    move-result-object v0

    sget-object v1, Lv8/x1;->a:Lv8/x1;

    if-eq v0, v1, :cond_1

    const/4 p1, 0x1

    return p1

    :cond_1
    invoke-direct {p0, p2}, Lv8/r1;->e0(Lkotlinx/coroutines/internal/m;)Lv8/p;

    move-result-object p2

    if-nez p2, :cond_0

    const/4 p1, 0x0

    return p1
.end method


# virtual methods
.method protected A()Ljava/lang/String;
    .locals 1

    const-string v0, "Job was cancelled"

    return-object v0
.end method

.method public B()Ljava/util/concurrent/CancellationException;
    .locals 4

    invoke-virtual {p0}, Lv8/r1;->U()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lv8/r1$c;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    move-object v1, v0

    check-cast v1, Lv8/r1$c;

    invoke-virtual {v1}, Lv8/r1$c;->e()Ljava/lang/Throwable;

    move-result-object v1

    goto :goto_0

    :cond_0
    instance-of v1, v0, Lv8/t;

    if-eqz v1, :cond_1

    move-object v1, v0

    check-cast v1, Lv8/t;

    iget-object v1, v1, Lv8/t;->a:Ljava/lang/Throwable;

    goto :goto_0

    :cond_1
    instance-of v1, v0, Lv8/f1;

    if-nez v1, :cond_4

    move-object v1, v2

    :goto_0
    instance-of v3, v1, Ljava/util/concurrent/CancellationException;

    if-eqz v3, :cond_2

    move-object v2, v1

    check-cast v2, Ljava/util/concurrent/CancellationException;

    :cond_2
    if-nez v2, :cond_3

    new-instance v2, Lv8/l1;

    invoke-direct {p0, v0}, Lv8/r1;->p0(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "Parent job is "

    invoke-static {v3, v0}, Lm8/i;->l(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0, v1, p0}, Lv8/l1;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lv8/k1;)V

    :cond_3
    return-object v2

    :cond_4
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Cannot be cancelling child in this state: "

    invoke-static {v2, v0}, Lm8/i;->l(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public final C()Ljava/util/concurrent/CancellationException;
    .locals 4

    invoke-virtual {p0}, Lv8/r1;->U()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lv8/r1$c;

    const-string v2, "Job is still new or active: "

    const/4 v3, 0x0

    if-eqz v1, :cond_2

    check-cast v0, Lv8/r1$c;

    invoke-virtual {v0}, Lv8/r1$c;->e()Ljava/lang/Throwable;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {p0}, Lv8/i0;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v3, " is cancelling"

    invoke-static {v1, v3}, Lm8/i;->l(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lv8/r1;->q0(Ljava/lang/Throwable;Ljava/lang/String;)Ljava/util/concurrent/CancellationException;

    move-result-object v3

    :goto_0
    if-eqz v3, :cond_1

    goto :goto_1

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v2, p0}, Lm8/i;->l(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    instance-of v1, v0, Lv8/f1;

    if-nez v1, :cond_4

    instance-of v1, v0, Lv8/t;

    if-eqz v1, :cond_3

    check-cast v0, Lv8/t;

    iget-object v0, v0, Lv8/t;->a:Ljava/lang/Throwable;

    const/4 v1, 0x1

    invoke-static {p0, v0, v3, v1, v3}, Lv8/r1;->r0(Lv8/r1;Ljava/lang/Throwable;Ljava/lang/String;ILjava/lang/Object;)Ljava/util/concurrent/CancellationException;

    move-result-object v3

    goto :goto_1

    :cond_3
    new-instance v0, Lv8/l1;

    invoke-static {p0}, Lv8/i0;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, " has completed normally"

    invoke-static {v1, v2}, Lm8/i;->l(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v3, p0}, Lv8/l1;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lv8/k1;)V

    move-object v3, v0

    :goto_1
    return-object v3

    :cond_4
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v2, p0}, Lm8/i;->l(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public D(Ljava/lang/Throwable;)Z
    .locals 2

    instance-of v0, p1, Ljava/util/concurrent/CancellationException;

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    :cond_0
    invoke-virtual {p0, p1}, Lv8/r1;->u(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lv8/r1;->Q()Z

    move-result p1

    if-eqz p1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public final F(ZZLl8/l;)Lv8/u0;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ",
            "Ll8/l<",
            "-",
            "Ljava/lang/Throwable;",
            "La8/v;",
            ">;)",
            "Lv8/u0;"
        }
    .end annotation

    invoke-direct {p0, p3, p1}, Lv8/r1;->c0(Ll8/l;Z)Lv8/q1;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-virtual {p0}, Lv8/r1;->U()Ljava/lang/Object;

    move-result-object v1

    instance-of v2, v1, Lv8/x0;

    if-eqz v2, :cond_2

    move-object v2, v1

    check-cast v2, Lv8/x0;

    invoke-virtual {v2}, Lv8/x0;->b()Z

    move-result v3

    if-eqz v3, :cond_1

    sget-object v2, Lv8/r1;->a:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    invoke-static {v2, p0, v1, v0}, La8/o;->a(Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    return-object v0

    :cond_1
    invoke-direct {p0, v2}, Lv8/r1;->k0(Lv8/x0;)V

    goto :goto_0

    :cond_2
    instance-of v2, v1, Lv8/f1;

    const/4 v3, 0x0

    if-eqz v2, :cond_b

    move-object v2, v1

    check-cast v2, Lv8/f1;

    invoke-interface {v2}, Lv8/f1;->f()Lv8/w1;

    move-result-object v2

    if-nez v2, :cond_3

    const-string v2, "null cannot be cast to non-null type kotlinx.coroutines.JobNode"

    invoke-static {v1, v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    check-cast v1, Lv8/q1;

    invoke-direct {p0, v1}, Lv8/r1;->l0(Lv8/q1;)V

    goto :goto_0

    :cond_3
    sget-object v4, Lv8/x1;->a:Lv8/x1;

    if-eqz p1, :cond_8

    instance-of v5, v1, Lv8/r1$c;

    if-eqz v5, :cond_8

    monitor-enter v1

    :try_start_0
    move-object v3, v1

    check-cast v3, Lv8/r1$c;

    invoke-virtual {v3}, Lv8/r1$c;->e()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_4

    instance-of v5, p3, Lv8/p;

    if-eqz v5, :cond_7

    move-object v5, v1

    check-cast v5, Lv8/r1$c;

    invoke-virtual {v5}, Lv8/r1$c;->h()Z

    move-result v5

    if-nez v5, :cond_7

    :cond_4
    invoke-direct {p0, v1, v2, v0}, Lv8/r1;->j(Ljava/lang/Object;Lv8/w1;Lv8/q1;)Z

    move-result v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v4, :cond_5

    monitor-exit v1

    goto :goto_0

    :cond_5
    if-nez v3, :cond_6

    monitor-exit v1

    return-object v0

    :cond_6
    move-object v4, v0

    :cond_7
    :try_start_1
    sget-object v5, La8/v;->a:La8/v;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v1

    goto :goto_1

    :catchall_0
    move-exception p1

    monitor-exit v1

    throw p1

    :cond_8
    :goto_1
    if-eqz v3, :cond_a

    if-eqz p2, :cond_9

    invoke-interface {p3, v3}, Ll8/l;->j(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_9
    return-object v4

    :cond_a
    invoke-direct {p0, v1, v2, v0}, Lv8/r1;->j(Ljava/lang/Object;Lv8/w1;Lv8/q1;)Z

    move-result v1

    if-eqz v1, :cond_0

    return-object v0

    :cond_b
    if-eqz p2, :cond_e

    instance-of p1, v1, Lv8/t;

    if-eqz p1, :cond_c

    check-cast v1, Lv8/t;

    goto :goto_2

    :cond_c
    move-object v1, v3

    :goto_2
    if-nez v1, :cond_d

    goto :goto_3

    :cond_d
    iget-object v3, v1, Lv8/t;->a:Ljava/lang/Throwable;

    :goto_3
    invoke-interface {p3, v3}, Ll8/l;->j(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_e
    sget-object p1, Lv8/x1;->a:Lv8/x1;

    return-object p1
.end method

.method public J(Ljava/util/concurrent/CancellationException;)V
    .locals 2

    if-nez p1, :cond_0

    const/4 p1, 0x0

    new-instance v0, Lv8/l1;

    invoke-static {p0}, Lv8/r1;->a(Lv8/r1;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1, p0}, Lv8/l1;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lv8/k1;)V

    move-object p1, v0

    :cond_0
    invoke-virtual {p0, p1}, Lv8/r1;->v(Ljava/lang/Throwable;)V

    return-void
.end method

.method public final L(Ll8/l;)Lv8/u0;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ll8/l<",
            "-",
            "Ljava/lang/Throwable;",
            "La8/v;",
            ">;)",
            "Lv8/u0;"
        }
    .end annotation

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1, p1}, Lv8/r1;->F(ZZLl8/l;)Lv8/u0;

    move-result-object p1

    return-object p1
.end method

.method public final M(Lv8/z1;)V
    .locals 0

    invoke-virtual {p0, p1}, Lv8/r1;->u(Ljava/lang/Object;)Z

    return-void
.end method

.method public Q()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public R()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final T()Lv8/o;
    .locals 1

    iget-object v0, p0, Lv8/r1;->_parentHandle:Ljava/lang/Object;

    check-cast v0, Lv8/o;

    return-object v0
.end method

.method public final U()Ljava/lang/Object;
    .locals 2

    :goto_0
    iget-object v0, p0, Lv8/r1;->_state:Ljava/lang/Object;

    instance-of v1, v0, Lkotlinx/coroutines/internal/t;

    if-nez v1, :cond_0

    return-object v0

    :cond_0
    check-cast v0, Lkotlinx/coroutines/internal/t;

    invoke-virtual {v0, p0}, Lkotlinx/coroutines/internal/t;->c(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method protected V(Ljava/lang/Throwable;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public W(Ljava/lang/Throwable;)V
    .locals 0

    throw p1
.end method

.method protected final X(Lv8/k1;)V
    .locals 1

    if-nez p1, :cond_0

    sget-object p1, Lv8/x1;->a:Lv8/x1;

    invoke-virtual {p0, p1}, Lv8/r1;->n0(Lv8/o;)V

    return-void

    :cond_0
    invoke-interface {p1}, Lv8/k1;->start()Z

    invoke-interface {p1, p0}, Lv8/k1;->r(Lv8/q;)Lv8/o;

    move-result-object p1

    invoke-virtual {p0, p1}, Lv8/r1;->n0(Lv8/o;)V

    invoke-virtual {p0}, Lv8/r1;->Y()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Lv8/u0;->c()V

    sget-object p1, Lv8/x1;->a:Lv8/x1;

    invoke-virtual {p0, p1}, Lv8/r1;->n0(Lv8/o;)V

    :cond_1
    return-void
.end method

.method public final Y()Z
    .locals 1

    invoke-virtual {p0}, Lv8/r1;->U()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lv8/f1;

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method protected Z()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public b()Z
    .locals 2

    invoke-virtual {p0}, Lv8/r1;->U()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lv8/f1;

    if-eqz v1, :cond_0

    check-cast v0, Lv8/f1;

    invoke-interface {v0}, Lv8/f1;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final b0(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    :goto_0
    invoke-virtual {p0}, Lv8/r1;->U()Ljava/lang/Object;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lv8/r1;->v0(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {}, Lv8/s1;->a()Lkotlinx/coroutines/internal/x;

    move-result-object v1

    if-eq v0, v1, :cond_1

    invoke-static {}, Lv8/s1;->b()Lkotlinx/coroutines/internal/x;

    move-result-object v1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    return-object v0

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Job "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, " is already complete or completing, but is being completed with "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1}, Lv8/r1;->O(Ljava/lang/Object;)Ljava/lang/Throwable;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method public d0()Ljava/lang/String;
    .locals 1

    invoke-static {p0}, Lv8/i0;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public fold(Ljava/lang/Object;Ll8/p;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(TR;",
            "Ll8/p<",
            "-TR;-",
            "Ld8/g$b;",
            "+TR;>;)TR;"
        }
    .end annotation

    invoke-static {p0, p1, p2}, Lv8/k1$a;->b(Lv8/k1;Ljava/lang/Object;Ll8/p;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public get(Ld8/g$c;)Ld8/g$b;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Ld8/g$b;",
            ">(",
            "Ld8/g$c<",
            "TE;>;)TE;"
        }
    .end annotation

    invoke-static {p0, p1}, Lv8/k1$a;->c(Lv8/k1;Ld8/g$c;)Ld8/g$b;

    move-result-object p1

    return-object p1
.end method

.method public final getKey()Ld8/g$c;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ld8/g$c<",
            "*>;"
        }
    .end annotation

    sget-object v0, Lv8/k1;->d0:Lv8/k1$b;

    return-object v0
.end method

.method protected h0(Ljava/lang/Throwable;)V
    .locals 0

    return-void
.end method

.method protected i0(Ljava/lang/Object;)V
    .locals 0

    return-void
.end method

.method protected j0()V
    .locals 0

    return-void
.end method

.method public final m0(Lv8/q1;)V
    .locals 3

    :cond_0
    invoke-virtual {p0}, Lv8/r1;->U()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lv8/q1;

    if-eqz v1, :cond_2

    if-eq v0, p1, :cond_1

    return-void

    :cond_1
    sget-object v1, Lv8/r1;->a:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    invoke-static {}, Lv8/s1;->c()Lv8/x0;

    move-result-object v2

    invoke-static {v1, p0, v0, v2}, La8/o;->a(Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_2
    instance-of v1, v0, Lv8/f1;

    if-eqz v1, :cond_3

    check-cast v0, Lv8/f1;

    invoke-interface {v0}, Lv8/f1;->f()Lv8/w1;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lkotlinx/coroutines/internal/m;->s()Z

    :cond_3
    return-void
.end method

.method public minusKey(Ld8/g$c;)Ld8/g;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ld8/g$c<",
            "*>;)",
            "Ld8/g;"
        }
    .end annotation

    invoke-static {p0, p1}, Lv8/k1$a;->e(Lv8/k1;Ld8/g$c;)Ld8/g;

    move-result-object p1

    return-object p1
.end method

.method public final n0(Lv8/o;)V
    .locals 0

    iput-object p1, p0, Lv8/r1;->_parentHandle:Ljava/lang/Object;

    return-void
.end method

.method protected o(Ljava/lang/Object;)V
    .locals 0

    return-void
.end method

.method public plus(Ld8/g;)Ld8/g;
    .locals 0

    invoke-static {p0, p1}, Lv8/k1$a;->f(Lv8/k1;Ld8/g;)Ld8/g;

    move-result-object p1

    return-object p1
.end method

.method public final q(Ld8/d;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ld8/d<",
            "Ljava/lang/Object;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    :cond_0
    invoke-virtual {p0}, Lv8/r1;->U()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lv8/f1;

    if-nez v1, :cond_2

    instance-of p1, v0, Lv8/t;

    if-nez p1, :cond_1

    invoke-static {v0}, Lv8/s1;->h(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1

    :cond_1
    check-cast v0, Lv8/t;

    iget-object p1, v0, Lv8/t;->a:Ljava/lang/Throwable;

    throw p1

    :cond_2
    invoke-direct {p0, v0}, Lv8/r1;->o0(Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_0

    invoke-direct {p0, p1}, Lv8/r1;->s(Ld8/d;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method protected final q0(Ljava/lang/Throwable;Ljava/lang/String;)Ljava/util/concurrent/CancellationException;
    .locals 1

    instance-of v0, p1, Ljava/util/concurrent/CancellationException;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Ljava/util/concurrent/CancellationException;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_2

    new-instance v0, Lv8/l1;

    if-nez p2, :cond_1

    invoke-static {p0}, Lv8/r1;->a(Lv8/r1;)Ljava/lang/String;

    move-result-object p2

    :cond_1
    invoke-direct {v0, p2, p1, p0}, Lv8/l1;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lv8/k1;)V

    :cond_2
    return-object v0
.end method

.method public final r(Lv8/q;)Lv8/o;
    .locals 6

    new-instance v3, Lv8/p;

    invoke-direct {v3, p1}, Lv8/p;-><init>(Lv8/q;)V

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x2

    const/4 v5, 0x0

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lv8/k1$a;->d(Lv8/k1;ZZLl8/l;ILjava/lang/Object;)Lv8/u0;

    move-result-object p1

    check-cast p1, Lv8/o;

    return-object p1
.end method

.method public final s0()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lv8/r1;->d0()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7b

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lv8/r1;->U()Ljava/lang/Object;

    move-result-object v1

    invoke-direct {p0, v1}, Lv8/r1;->p0(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final start()Z
    .locals 2

    :goto_0
    invoke-virtual {p0}, Lv8/r1;->U()Ljava/lang/Object;

    move-result-object v0

    invoke-direct {p0, v0}, Lv8/r1;->o0(Ljava/lang/Object;)I

    move-result v0

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    return v1

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public final t(Ljava/lang/Throwable;)Z
    .locals 0

    invoke-virtual {p0, p1}, Lv8/r1;->u(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lv8/r1;->s0()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-static {p0}, Lv8/i0;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final u(Ljava/lang/Object;)Z
    .locals 3

    invoke-static {}, Lv8/s1;->a()Lkotlinx/coroutines/internal/x;

    move-result-object v0

    invoke-virtual {p0}, Lv8/r1;->R()Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_0

    invoke-direct {p0, p1}, Lv8/r1;->w(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lv8/s1;->b:Lkotlinx/coroutines/internal/x;

    if-ne v0, v1, :cond_0

    return v2

    :cond_0
    invoke-static {}, Lv8/s1;->a()Lkotlinx/coroutines/internal/x;

    move-result-object v1

    if-ne v0, v1, :cond_1

    invoke-direct {p0, p1}, Lv8/r1;->a0(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    :cond_1
    invoke-static {}, Lv8/s1;->a()Lkotlinx/coroutines/internal/x;

    move-result-object p1

    if-ne v0, p1, :cond_2

    goto :goto_0

    :cond_2
    sget-object p1, Lv8/s1;->b:Lkotlinx/coroutines/internal/x;

    if-ne v0, p1, :cond_3

    goto :goto_0

    :cond_3
    invoke-static {}, Lv8/s1;->f()Lkotlinx/coroutines/internal/x;

    move-result-object p1

    if-ne v0, p1, :cond_4

    const/4 v2, 0x0

    goto :goto_0

    :cond_4
    invoke-virtual {p0, v0}, Lv8/r1;->o(Ljava/lang/Object;)V

    :goto_0
    return v2
.end method

.method public v(Ljava/lang/Throwable;)V
    .locals 0

    invoke-virtual {p0, p1}, Lv8/r1;->u(Ljava/lang/Object;)Z

    return-void
.end method
