.class public interface abstract Lv8/k1;
.super Ljava/lang/Object;

# interfaces
.implements Ld8/g$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lv8/k1$b;,
        Lv8/k1$a;
    }
.end annotation


# static fields
.field public static final d0:Lv8/k1$b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lv8/k1$b;->a:Lv8/k1$b;

    sput-object v0, Lv8/k1;->d0:Lv8/k1$b;

    return-void
.end method


# virtual methods
.method public abstract C()Ljava/util/concurrent/CancellationException;
.end method

.method public abstract F(ZZLl8/l;)Lv8/u0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ",
            "Ll8/l<",
            "-",
            "Ljava/lang/Throwable;",
            "La8/v;",
            ">;)",
            "Lv8/u0;"
        }
    .end annotation
.end method

.method public abstract J(Ljava/util/concurrent/CancellationException;)V
.end method

.method public abstract L(Ll8/l;)Lv8/u0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ll8/l<",
            "-",
            "Ljava/lang/Throwable;",
            "La8/v;",
            ">;)",
            "Lv8/u0;"
        }
    .end annotation
.end method

.method public abstract b()Z
.end method

.method public abstract r(Lv8/q;)Lv8/o;
.end method

.method public abstract start()Z
.end method
