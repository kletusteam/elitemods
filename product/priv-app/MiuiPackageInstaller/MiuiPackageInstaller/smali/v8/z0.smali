.class public abstract Lv8/z0;
.super Lv8/a1;

# interfaces
.implements Lv8/n0;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lv8/z0$c;,
        Lv8/z0$a;,
        Lv8/z0$b;,
        Lv8/z0$d;
    }
.end annotation


# static fields
.field private static final synthetic e:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

.field private static final synthetic f:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;


# instance fields
.field private volatile synthetic _delayed:Ljava/lang/Object;

.field private volatile synthetic _isCompleted:I

.field private volatile synthetic _queue:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const-class v0, Ljava/lang/Object;

    const-class v1, Lv8/z0;

    const-string v2, "_queue"

    invoke-static {v1, v0, v2}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->newUpdater(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    move-result-object v2

    sput-object v2, Lv8/z0;->e:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    const-string v2, "_delayed"

    invoke-static {v1, v0, v2}, Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;->newUpdater(Ljava/lang/Class;Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    move-result-object v0

    sput-object v0, Lv8/z0;->f:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lv8/a1;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lv8/z0;->_queue:Ljava/lang/Object;

    iput-object v0, p0, Lv8/z0;->_delayed:Ljava/lang/Object;

    const/4 v0, 0x0

    iput v0, p0, Lv8/z0;->_isCompleted:I

    return-void
.end method

.method public static final synthetic j0(Lv8/z0;)Z
    .locals 0

    invoke-direct {p0}, Lv8/z0;->o0()Z

    move-result p0

    return p0
.end method

.method private final k0()V
    .locals 4

    :cond_0
    iget-object v0, p0, Lv8/z0;->_queue:Ljava/lang/Object;

    if-nez v0, :cond_1

    sget-object v0, Lv8/z0;->e:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    const/4 v1, 0x0

    invoke-static {}, Lv8/c1;->a()Lkotlinx/coroutines/internal/x;

    move-result-object v2

    invoke-static {v0, p0, v1, v2}, La8/o;->a(Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_1
    instance-of v1, v0, Lkotlinx/coroutines/internal/o;

    if-eqz v1, :cond_2

    check-cast v0, Lkotlinx/coroutines/internal/o;

    invoke-virtual {v0}, Lkotlinx/coroutines/internal/o;->d()Z

    return-void

    :cond_2
    invoke-static {}, Lv8/c1;->a()Lkotlinx/coroutines/internal/x;

    move-result-object v1

    if-ne v0, v1, :cond_3

    return-void

    :cond_3
    new-instance v1, Lkotlinx/coroutines/internal/o;

    const/16 v2, 0x8

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lkotlinx/coroutines/internal/o;-><init>(IZ)V

    move-object v2, v0

    check-cast v2, Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Lkotlinx/coroutines/internal/o;->a(Ljava/lang/Object;)I

    sget-object v2, Lv8/z0;->e:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    invoke-static {v2, p0, v0, v1}, La8/o;->a(Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void
.end method

.method private final l0()Ljava/lang/Runnable;
    .locals 4

    :cond_0
    :goto_0
    iget-object v0, p0, Lv8/z0;->_queue:Ljava/lang/Object;

    const/4 v1, 0x0

    if-nez v0, :cond_1

    return-object v1

    :cond_1
    instance-of v2, v0, Lkotlinx/coroutines/internal/o;

    if-eqz v2, :cond_3

    move-object v1, v0

    check-cast v1, Lkotlinx/coroutines/internal/o;

    invoke-virtual {v1}, Lkotlinx/coroutines/internal/o;->j()Ljava/lang/Object;

    move-result-object v2

    sget-object v3, Lkotlinx/coroutines/internal/o;->h:Lkotlinx/coroutines/internal/x;

    if-eq v2, v3, :cond_2

    check-cast v2, Ljava/lang/Runnable;

    return-object v2

    :cond_2
    sget-object v2, Lv8/z0;->e:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    invoke-virtual {v1}, Lkotlinx/coroutines/internal/o;->i()Lkotlinx/coroutines/internal/o;

    move-result-object v1

    invoke-static {v2, p0, v0, v1}, La8/o;->a(Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    invoke-static {}, Lv8/c1;->a()Lkotlinx/coroutines/internal/x;

    move-result-object v2

    if-ne v0, v2, :cond_4

    return-object v1

    :cond_4
    sget-object v2, Lv8/z0;->e:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    invoke-static {v2, p0, v0, v1}, La8/o;->a(Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/Runnable;

    return-object v0
.end method

.method private final n0(Ljava/lang/Runnable;)Z
    .locals 5

    :cond_0
    :goto_0
    iget-object v0, p0, Lv8/z0;->_queue:Ljava/lang/Object;

    invoke-direct {p0}, Lv8/z0;->o0()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    return v2

    :cond_1
    const/4 v1, 0x1

    if-nez v0, :cond_2

    sget-object v0, Lv8/z0;->e:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    const/4 v2, 0x0

    invoke-static {v0, p0, v2, p1}, La8/o;->a(Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return v1

    :cond_2
    instance-of v3, v0, Lkotlinx/coroutines/internal/o;

    if-eqz v3, :cond_6

    move-object v3, v0

    check-cast v3, Lkotlinx/coroutines/internal/o;

    invoke-virtual {v3, p1}, Lkotlinx/coroutines/internal/o;->a(Ljava/lang/Object;)I

    move-result v4

    if-eqz v4, :cond_5

    if-eq v4, v1, :cond_4

    const/4 v0, 0x2

    if-eq v4, v0, :cond_3

    goto :goto_0

    :cond_3
    return v2

    :cond_4
    sget-object v1, Lv8/z0;->e:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    invoke-virtual {v3}, Lkotlinx/coroutines/internal/o;->i()Lkotlinx/coroutines/internal/o;

    move-result-object v2

    invoke-static {v1, p0, v0, v2}, La8/o;->a(Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    goto :goto_0

    :cond_5
    return v1

    :cond_6
    invoke-static {}, Lv8/c1;->a()Lkotlinx/coroutines/internal/x;

    move-result-object v3

    if-ne v0, v3, :cond_7

    return v2

    :cond_7
    new-instance v2, Lkotlinx/coroutines/internal/o;

    const/16 v3, 0x8

    invoke-direct {v2, v3, v1}, Lkotlinx/coroutines/internal/o;-><init>(IZ)V

    move-object v3, v0

    check-cast v3, Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Lkotlinx/coroutines/internal/o;->a(Ljava/lang/Object;)I

    invoke-virtual {v2, p1}, Lkotlinx/coroutines/internal/o;->a(Ljava/lang/Object;)I

    sget-object v3, Lv8/z0;->e:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    invoke-static {v3, p0, v0, v2}, La8/o;->a(Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return v1
.end method

.method private final o0()Z
    .locals 1

    iget v0, p0, Lv8/z0;->_isCompleted:I

    return v0
.end method

.method private final r0()V
    .locals 3

    invoke-static {}, Lv8/c;->a()Lv8/b;

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    :goto_0
    iget-object v2, p0, Lv8/z0;->_delayed:Ljava/lang/Object;

    check-cast v2, Lv8/z0$d;

    if-nez v2, :cond_0

    const/4 v2, 0x0

    goto :goto_1

    :cond_0
    invoke-virtual {v2}, Lkotlinx/coroutines/internal/c0;->i()Lkotlinx/coroutines/internal/d0;

    move-result-object v2

    check-cast v2, Lv8/z0$c;

    :goto_1
    if-nez v2, :cond_1

    return-void

    :cond_1
    invoke-virtual {p0, v0, v1, v2}, Lv8/a1;->h0(JLv8/z0$c;)V

    goto :goto_0
.end method

.method private final u0(JLv8/z0$c;)I
    .locals 3

    invoke-direct {p0}, Lv8/z0;->o0()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    iget-object v0, p0, Lv8/z0;->_delayed:Ljava/lang/Object;

    check-cast v0, Lv8/z0$d;

    if-nez v0, :cond_1

    sget-object v0, Lv8/z0;->f:Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;

    const/4 v1, 0x0

    new-instance v2, Lv8/z0$d;

    invoke-direct {v2, p1, p2}, Lv8/z0$d;-><init>(J)V

    invoke-static {v0, p0, v1, v2}, La8/o;->a(Ljava/util/concurrent/atomic/AtomicReferenceFieldUpdater;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Z

    iget-object v0, p0, Lv8/z0;->_delayed:Ljava/lang/Object;

    invoke-static {v0}, Lm8/i;->c(Ljava/lang/Object;)V

    check-cast v0, Lv8/z0$d;

    :cond_1
    invoke-virtual {p3, p1, p2, v0, p0}, Lv8/z0$c;->h(JLv8/z0$d;Lv8/z0;)I

    move-result p1

    return p1
.end method

.method private final w0(Z)V
    .locals 0

    iput p1, p0, Lv8/z0;->_isCompleted:I

    return-void
.end method

.method private final x0(Lv8/z0$c;)Z
    .locals 1

    iget-object v0, p0, Lv8/z0;->_delayed:Ljava/lang/Object;

    check-cast v0, Lv8/z0$d;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Lkotlinx/coroutines/internal/c0;->e()Lkotlinx/coroutines/internal/d0;

    move-result-object v0

    check-cast v0, Lv8/z0$c;

    :goto_0
    if-ne v0, p1, :cond_1

    const/4 p1, 0x1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    :goto_1
    return p1
.end method


# virtual methods
.method public D(JLv8/j;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lv8/j<",
            "-",
            "La8/v;",
            ">;)V"
        }
    .end annotation

    invoke-static {p1, p2}, Lv8/c1;->c(J)J

    move-result-wide p1

    const-wide v0, 0x3fffffffffffffffL    # 1.9999999999999998

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    invoke-static {}, Lv8/c;->a()Lv8/b;

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    new-instance v2, Lv8/z0$a;

    add-long/2addr p1, v0

    invoke-direct {v2, p0, p1, p2, p3}, Lv8/z0$a;-><init>(Lv8/z0;JLv8/j;)V

    invoke-static {p3, v2}, Lv8/l;->a(Lv8/j;Lv8/u0;)V

    invoke-virtual {p0, v0, v1, v2}, Lv8/z0;->t0(JLv8/z0$c;)V

    :cond_0
    return-void
.end method

.method public final U(Ld8/g;Ljava/lang/Runnable;)V
    .locals 0

    invoke-virtual {p0, p2}, Lv8/z0;->m0(Ljava/lang/Runnable;)V

    return-void
.end method

.method protected a0()J
    .locals 6

    invoke-super {p0}, Lv8/y0;->a0()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    return-wide v2

    :cond_0
    iget-object v0, p0, Lv8/z0;->_queue:Ljava/lang/Object;

    const-wide v4, 0x7fffffffffffffffL

    if-nez v0, :cond_1

    goto :goto_0

    :cond_1
    instance-of v1, v0, Lkotlinx/coroutines/internal/o;

    if-eqz v1, :cond_5

    check-cast v0, Lkotlinx/coroutines/internal/o;

    invoke-virtual {v0}, Lkotlinx/coroutines/internal/o;->g()Z

    move-result v0

    if-nez v0, :cond_2

    return-wide v2

    :cond_2
    :goto_0
    iget-object v0, p0, Lv8/z0;->_delayed:Ljava/lang/Object;

    check-cast v0, Lv8/z0$d;

    if-nez v0, :cond_3

    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    invoke-virtual {v0}, Lkotlinx/coroutines/internal/c0;->e()Lkotlinx/coroutines/internal/d0;

    move-result-object v0

    check-cast v0, Lv8/z0$c;

    :goto_1
    if-nez v0, :cond_4

    return-wide v4

    :cond_4
    iget-wide v0, v0, Lv8/z0$c;->a:J

    invoke-static {}, Lv8/c;->a()Lv8/b;

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v4

    sub-long/2addr v0, v4

    invoke-static {v0, v1, v2, v3}, Lr8/d;->c(JJ)J

    move-result-wide v0

    return-wide v0

    :cond_5
    invoke-static {}, Lv8/c1;->a()Lkotlinx/coroutines/internal/x;

    move-result-object v1

    if-ne v0, v1, :cond_6

    return-wide v4

    :cond_6
    return-wide v2
.end method

.method public m0(Ljava/lang/Runnable;)V
    .locals 1

    invoke-direct {p0, p1}, Lv8/z0;->n0(Ljava/lang/Runnable;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lv8/a1;->i0()V

    goto :goto_0

    :cond_0
    sget-object v0, Lv8/j0;->g:Lv8/j0;

    invoke-virtual {v0, p1}, Lv8/j0;->m0(Ljava/lang/Runnable;)V

    :goto_0
    return-void
.end method

.method protected p0()Z
    .locals 4

    invoke-virtual {p0}, Lv8/y0;->e0()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    iget-object v0, p0, Lv8/z0;->_delayed:Ljava/lang/Object;

    check-cast v0, Lv8/z0$d;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lkotlinx/coroutines/internal/c0;->d()Z

    move-result v0

    if-nez v0, :cond_1

    return v1

    :cond_1
    iget-object v0, p0, Lv8/z0;->_queue:Ljava/lang/Object;

    const/4 v2, 0x1

    if-nez v0, :cond_2

    :goto_0
    move v1, v2

    goto :goto_1

    :cond_2
    instance-of v3, v0, Lkotlinx/coroutines/internal/o;

    if-eqz v3, :cond_3

    check-cast v0, Lkotlinx/coroutines/internal/o;

    invoke-virtual {v0}, Lkotlinx/coroutines/internal/o;->g()Z

    move-result v1

    goto :goto_1

    :cond_3
    invoke-static {}, Lv8/c1;->a()Lkotlinx/coroutines/internal/x;

    move-result-object v3

    if-ne v0, v3, :cond_4

    goto :goto_0

    :cond_4
    :goto_1
    return v1
.end method

.method public q0()J
    .locals 9

    invoke-virtual {p0}, Lv8/y0;->f0()Z

    move-result v0

    const-wide/16 v1, 0x0

    if-eqz v0, :cond_0

    return-wide v1

    :cond_0
    iget-object v0, p0, Lv8/z0;->_delayed:Ljava/lang/Object;

    check-cast v0, Lv8/z0$d;

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lkotlinx/coroutines/internal/c0;->d()Z

    move-result v3

    if-nez v3, :cond_5

    invoke-static {}, Lv8/c;->a()Lv8/b;

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v3

    :cond_1
    monitor-enter v0

    :try_start_0
    invoke-virtual {v0}, Lkotlinx/coroutines/internal/c0;->b()Lkotlinx/coroutines/internal/d0;

    move-result-object v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v6, 0x0

    if-nez v5, :cond_3

    :cond_2
    :goto_0
    monitor-exit v0

    goto :goto_2

    :cond_3
    :try_start_1
    check-cast v5, Lv8/z0$c;

    invoke-virtual {v5, v3, v4}, Lv8/z0$c;->i(J)Z

    move-result v7

    const/4 v8, 0x0

    if-eqz v7, :cond_4

    invoke-direct {p0, v5}, Lv8/z0;->n0(Ljava/lang/Runnable;)Z

    move-result v5

    goto :goto_1

    :cond_4
    move v5, v8

    :goto_1
    if-eqz v5, :cond_2

    invoke-virtual {v0, v8}, Lkotlinx/coroutines/internal/c0;->h(I)Lkotlinx/coroutines/internal/d0;

    move-result-object v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v6, v5

    goto :goto_0

    :goto_2
    check-cast v6, Lv8/z0$c;

    if-nez v6, :cond_1

    goto :goto_3

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1

    :cond_5
    :goto_3
    invoke-direct {p0}, Lv8/z0;->l0()Ljava/lang/Runnable;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    return-wide v1

    :cond_6
    invoke-virtual {p0}, Lv8/z0;->a0()J

    move-result-wide v0

    return-wide v0
.end method

.method public s(JLjava/lang/Runnable;Ld8/g;)Lv8/u0;
    .locals 0

    invoke-static {p0, p1, p2, p3, p4}, Lv8/n0$a;->a(Lv8/n0;JLjava/lang/Runnable;Ld8/g;)Lv8/u0;

    move-result-object p1

    return-object p1
.end method

.method protected final s0()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lv8/z0;->_queue:Ljava/lang/Object;

    iput-object v0, p0, Lv8/z0;->_delayed:Ljava/lang/Object;

    return-void
.end method

.method public shutdown()V
    .locals 4

    sget-object v0, Lv8/f2;->a:Lv8/f2;

    invoke-virtual {v0}, Lv8/f2;->b()V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lv8/z0;->w0(Z)V

    invoke-direct {p0}, Lv8/z0;->k0()V

    :goto_0
    invoke-virtual {p0}, Lv8/z0;->q0()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lv8/z0;->r0()V

    return-void
.end method

.method public final t0(JLv8/z0$c;)V
    .locals 2

    invoke-direct {p0, p1, p2, p3}, Lv8/z0;->u0(JLv8/z0$c;)I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 p1, 0x2

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "unexpected result"

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    invoke-virtual {p0, p1, p2, p3}, Lv8/a1;->h0(JLv8/z0$c;)V

    goto :goto_0

    :cond_2
    invoke-direct {p0, p3}, Lv8/z0;->x0(Lv8/z0$c;)Z

    move-result p1

    if-eqz p1, :cond_3

    invoke-virtual {p0}, Lv8/a1;->i0()V

    :cond_3
    :goto_0
    return-void
.end method

.method protected final v0(JLjava/lang/Runnable;)Lv8/u0;
    .locals 3

    invoke-static {p1, p2}, Lv8/c1;->c(J)J

    move-result-wide p1

    const-wide v0, 0x3fffffffffffffffL    # 1.9999999999999998

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    invoke-static {}, Lv8/c;->a()Lv8/b;

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    new-instance v2, Lv8/z0$b;

    add-long/2addr p1, v0

    invoke-direct {v2, p1, p2, p3}, Lv8/z0$b;-><init>(JLjava/lang/Runnable;)V

    invoke-virtual {p0, v0, v1, v2}, Lv8/z0;->t0(JLv8/z0$c;)V

    goto :goto_0

    :cond_0
    sget-object v2, Lv8/x1;->a:Lv8/x1;

    :goto_0
    return-object v2
.end method
