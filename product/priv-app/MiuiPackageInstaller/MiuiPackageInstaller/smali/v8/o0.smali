.class public final Lv8/o0;
.super Ljava/lang/Object;


# direct methods
.method public static final a(JLd8/d;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ld8/d<",
            "-",
            "La8/v;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    const-wide/16 v0, 0x0

    cmp-long v0, p0, v0

    if-gtz v0, :cond_0

    sget-object p0, La8/v;->a:La8/v;

    return-object p0

    :cond_0
    new-instance v0, Lv8/k;

    invoke-static {p2}, Le8/b;->b(Ld8/d;)Ld8/d;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lv8/k;-><init>(Ld8/d;I)V

    invoke-virtual {v0}, Lv8/k;->y()V

    const-wide v1, 0x7fffffffffffffffL

    cmp-long v1, p0, v1

    if-gez v1, :cond_1

    invoke-interface {v0}, Ld8/d;->c()Ld8/g;

    move-result-object v1

    invoke-static {v1}, Lv8/o0;->b(Ld8/g;)Lv8/n0;

    move-result-object v1

    invoke-interface {v1, p0, p1, v0}, Lv8/n0;->D(JLv8/j;)V

    :cond_1
    invoke-virtual {v0}, Lv8/k;->v()Ljava/lang/Object;

    move-result-object p0

    invoke-static {}, Le8/b;->c()Ljava/lang/Object;

    move-result-object p1

    if-ne p0, p1, :cond_2

    invoke-static {p2}, Lf8/h;->c(Ld8/d;)V

    :cond_2
    invoke-static {}, Le8/b;->c()Ljava/lang/Object;

    move-result-object p1

    if-ne p0, p1, :cond_3

    return-object p0

    :cond_3
    sget-object p0, La8/v;->a:La8/v;

    return-object p0
.end method

.method public static final b(Ld8/g;)Lv8/n0;
    .locals 1

    sget-object v0, Ld8/e;->b0:Ld8/e$b;

    invoke-interface {p0, v0}, Ld8/g;->get(Ld8/g$c;)Ld8/g$b;

    move-result-object p0

    instance-of v0, p0, Lv8/n0;

    if-eqz v0, :cond_0

    check-cast p0, Lv8/n0;

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    if-nez p0, :cond_1

    invoke-static {}, Lv8/k0;->a()Lv8/n0;

    move-result-object p0

    :cond_1
    return-object p0
.end method
