.class public Lb5/g;
.super Landroid/database/sqlite/SQLiteOpenHelper;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lb5/g$a;,
        Lb5/g$b;
    }
.end annotation


# instance fields
.field private a:Lb5/g$b;

.field private b:Lb5/g$a;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;ILb5/g$b;Lb5/g$a;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    iput-object p5, p0, Lb5/g;->a:Lb5/g$b;

    iput-object p6, p0, Lb5/g;->b:Lb5/g$a;

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    iget-object v0, p0, Lb5/g;->b:Lb5/g$a;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lb5/g$a;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    :cond_0
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 1

    iget-object v0, p0, Lb5/g;->a:Lb5/g$b;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1, p2, p3}, Lb5/g$b;->a(Landroid/database/sqlite/SQLiteDatabase;II)V

    :cond_0
    return-void
.end method
