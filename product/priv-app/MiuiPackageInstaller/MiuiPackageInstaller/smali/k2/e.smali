.class public Lk2/e;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lk2/e$c;,
        Lk2/e$b;
    }
.end annotation


# static fields
.field private static c:Lk2/e;


# instance fields
.field private a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lk2/g;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lk2/h;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lk2/e;->a:Ljava/util/Map;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lk2/e;->b:Ljava/util/List;

    invoke-static {}, Lcom/android/packageinstaller/InstallerApplication;->g()Lcom/android/packageinstaller/InstallerApplication;

    move-result-object v0

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "com.xiaomi.market.DOWNLOAD_INSTALL_RESULT"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const-string v2, "com.miui.newhome.SYNC_DOWNLOAD_STATUS"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "com.miui.newhome.UPDATE_DOWN_LOAD_TASK"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    new-instance v2, Lk2/e$b;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lk2/e$b;-><init>(Lk2/e;Lk2/e$a;)V

    invoke-virtual {v0, v2, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    const-string v2, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "package"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    new-instance v2, Lk2/e$c;

    invoke-direct {v2, p0, v3}, Lk2/e$c;-><init>(Lk2/e;Lk2/e$a;)V

    invoke-virtual {v0, v2, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method public static synthetic a(Lk2/e;Ljava/lang/String;Lk2/g;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lk2/e;->i(Ljava/lang/String;Lk2/g;)V

    return-void
.end method

.method public static synthetic b(Lk2/e;Lk2/h;)V
    .locals 0

    invoke-direct {p0, p1}, Lk2/e;->k(Lk2/h;)V

    return-void
.end method

.method public static synthetic c(Lk2/e;Lk2/h;)V
    .locals 0

    invoke-direct {p0, p1}, Lk2/e;->j(Lk2/h;)V

    return-void
.end method

.method static synthetic d(Lk2/e;)Ljava/util/Map;
    .locals 0

    iget-object p0, p0, Lk2/e;->a:Ljava/util/Map;

    return-object p0
.end method

.method static synthetic e(Lk2/e;Ljava/lang/String;Lk2/g;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lk2/e;->l(Ljava/lang/String;Lk2/g;)V

    return-void
.end method

.method private f(Ljava/lang/String;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "floatCardData : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AdDownLoadManager"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Li5/d;->b()Li5/d;

    move-result-object v0

    invoke-virtual {v0}, Li5/d;->a()Li5/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Li5/a;->b(Ljava/lang/String;)Z

    invoke-virtual {v0, p1}, Li5/a;->f(Ljava/lang/String;)Z

    return-void
.end method

.method public static declared-synchronized h()Lk2/e;
    .locals 2

    const-class v0, Lk2/e;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lk2/e;->c:Lk2/e;

    if-nez v1, :cond_0

    new-instance v1, Lk2/e;

    invoke-direct {v1}, Lk2/e;-><init>()V

    sput-object v1, Lk2/e;->c:Lk2/e;

    :cond_0
    sget-object v1, Lk2/e;->c:Lk2/e;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method private synthetic i(Ljava/lang/String;Lk2/g;)V
    .locals 2

    iget-object v0, p0, Lk2/e;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lk2/h;

    invoke-interface {v1, p1, p2}, Lk2/h;->b(Ljava/lang/String;Lk2/g;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private synthetic j(Lk2/h;)V
    .locals 2

    iget-object v0, p0, Lk2/e;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lk2/e;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-object p1, p0, Lk2/e;->b:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lk2/h;

    instance-of v1, v0, Lk2/j;

    if-eqz v1, :cond_1

    check-cast v0, Lk2/j;

    invoke-virtual {v0}, Lk2/j;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_2
    return-void
.end method

.method private synthetic k(Lk2/h;)V
    .locals 1

    iget-object v0, p0, Lk2/e;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method private l(Ljava/lang/String;Lk2/g;)V
    .locals 2

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object v0

    new-instance v1, Lk2/b;

    invoke-direct {v1, p0, p1, p2}, Lk2/b;-><init>(Lk2/e;Ljava/lang/String;Lk2/g;)V

    invoke-virtual {v0, v1}, Lf6/z;->e(Ljava/lang/Runnable;)V

    return-void
.end method


# virtual methods
.method public g(Ljava/lang/String;)Lk2/g;
    .locals 1

    iget-object v0, p0, Lk2/e;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lk2/g;

    return-object p1
.end method

.method public m(Lk2/h;)V
    .locals 2

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object v0

    new-instance v1, Lk2/d;

    invoke-direct {v1, p0, p1}, Lk2/d;-><init>(Lk2/e;Lk2/h;)V

    invoke-virtual {v0, v1}, Lf6/z;->e(Ljava/lang/Runnable;)V

    return-void
.end method

.method public n(Landroid/content/Context;Lcom/miui/packageInstaller/model/AdInterface;)V
    .locals 3

    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.xiaomi.market.service.AppDownloadInstallService"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.xiaomi.market"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    invoke-interface {p2}, Lcom/miui/packageInstaller/model/AdInterface;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "packageName"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-interface {p2}, Lcom/miui/packageInstaller/model/AdInterface;->getRef()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ref"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "show_cta"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "senderPackageName"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-interface {p2}, Lcom/miui/packageInstaller/model/AdInterface;->getAppChannel()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {p2}, Lcom/miui/packageInstaller/model/AdInterface;->getAppChannel()Ljava/lang/String;

    move-result-object p2

    const-string v1, "ext_apkChannel"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    :cond_1
    return-void
.end method

.method public o(Landroid/content/Context;Lcom/miui/packageInstaller/model/AdInterface;)V
    .locals 2

    if-eqz p1, :cond_1

    if-nez p2, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.xiaomi.market.service.AppDownloadService"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.xiaomi.market"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    new-instance v1, Lk2/e$a;

    invoke-direct {v1, p0, p2, p1, v0}, Lk2/e$a;-><init>(Lk2/e;Lcom/miui/packageInstaller/model/AdInterface;Landroid/content/Context;Landroid/content/Intent;)V

    const/4 p2, 0x1

    invoke-virtual {p1, v0, v1, p2}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    :cond_1
    :goto_0
    return-void
.end method

.method public p(Landroid/content/Context;Lcom/miui/packageInstaller/model/AdInterface;)V
    .locals 4

    const-string v0, "com.xiaomi.market"

    invoke-static {p1, v0}, Lj2/f;->g(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "marketVersionCode :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "AdDownLoadManager"

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p2}, Lcom/miui/packageInstaller/model/AdInterface;->getFloatCardData()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "&sourcePackageChain="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p2}, Lcom/miui/packageInstaller/model/AdInterface;->getSourcePackage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2}, Lcom/miui/packageInstaller/model/AdInterface;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lk2/e;->g(Ljava/lang/String;)Lk2/g;

    move-result-object v2

    const v3, 0x1d371b

    if-le v0, v3, :cond_0

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-direct {p0, v1}, Lk2/e;->f(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    const v1, 0x1d34ff

    if-lt v0, v1, :cond_1

    invoke-virtual {p0, p1, p2}, Lk2/e;->o(Landroid/content/Context;Lcom/miui/packageInstaller/model/AdInterface;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0, p1, p2}, Lk2/e;->n(Landroid/content/Context;Lcom/miui/packageInstaller/model/AdInterface;)V

    :goto_0
    if-nez v2, :cond_2

    new-instance p1, Lk2/g;

    invoke-direct {p1}, Lk2/g;-><init>()V

    invoke-interface {p2}, Lcom/miui/packageInstaller/model/AdInterface;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lk2/g;->b:Ljava/lang/String;

    const/4 v0, 0x2

    iput v0, p1, Lk2/g;->d:I

    iget-object v0, p0, Lk2/e;->a:Ljava/util/Map;

    invoke-interface {p2}, Lcom/miui/packageInstaller/model/AdInterface;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p2}, Lcom/miui/packageInstaller/model/AdInterface;->getPackageName()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p0, p2, p1}, Lk2/e;->l(Ljava/lang/String;Lk2/g;)V

    :cond_2
    return-void
.end method

.method public q(Lk2/h;)V
    .locals 2

    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object v0

    new-instance v1, Lk2/c;

    invoke-direct {v1, p0, p1}, Lk2/c;-><init>(Lk2/e;Lk2/h;)V

    invoke-virtual {v0, v1}, Lf6/z;->e(Ljava/lang/Runnable;)V

    return-void
.end method
