.class Lk2/e$b;
.super Landroid/content/BroadcastReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lk2/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lk2/e;


# direct methods
.method private constructor <init>(Lk2/e;)V
    .locals 0

    iput-object p1, p0, Lk2/e$b;->a:Lk2/e;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lk2/e;Lk2/e$a;)V
    .locals 0

    invoke-direct {p0, p1}, Lk2/e$b;-><init>(Lk2/e;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 12

    const-string p1, "packageName"

    invoke-virtual {p2, p1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    new-instance v0, Lk2/g;

    invoke-direct {v0}, Lk2/g;-><init>()V

    iget-object v1, p0, Lk2/e$b;->a:Lk2/e;

    invoke-static {v1}, Lk2/e;->d(Lk2/e;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lk2/g;

    if-eqz v1, :cond_0

    iget v1, v1, Lk2/g;->e:I

    iput v1, v0, Lk2/g;->e:I

    :cond_0
    const-string v1, "errorCode"

    const/4 v2, 0x0

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, v0, Lk2/g;->a:I

    iput-object p1, v0, Lk2/g;->b:Ljava/lang/String;

    const/4 v3, -0x8

    const/4 v4, 0x7

    const/4 v5, 0x1

    if-eq v1, v3, :cond_9

    const/4 v3, -0x6

    if-eq v1, v3, :cond_9

    const/4 v3, -0x5

    if-eq v1, v3, :cond_9

    const/4 v6, -0x4

    if-eq v1, v6, :cond_9

    const/4 v7, -0x3

    if-eq v1, v7, :cond_9

    const/4 v8, -0x2

    if-eq v1, v8, :cond_9

    if-eq v1, v5, :cond_8

    const/4 v9, 0x2

    if-eq v1, v9, :cond_7

    const/4 v10, 0x4

    if-eq v1, v10, :cond_6

    const/4 v11, 0x5

    if-eq v1, v11, :cond_1

    iget p2, v0, Lk2/g;->d:I

    if-nez p2, :cond_a

    goto :goto_1

    :cond_1
    const-string v1, "progress"

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    const-string v4, "status"

    invoke-virtual {p2, v4, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result p2

    iput p2, v0, Lk2/g;->c:I

    if-eq p2, v3, :cond_5

    if-eq p2, v6, :cond_4

    if-eq p2, v7, :cond_3

    if-eq p2, v8, :cond_2

    const/4 v3, -0x1

    if-eq p2, v3, :cond_5

    goto :goto_0

    :cond_2
    const/4 p2, 0x3

    iput p2, v0, Lk2/g;->d:I

    goto :goto_0

    :cond_3
    iput v10, v0, Lk2/g;->d:I

    goto :goto_0

    :cond_4
    iput v11, v0, Lk2/g;->d:I

    goto :goto_0

    :cond_5
    iput v9, v0, Lk2/g;->d:I

    :goto_0
    iput v1, v0, Lk2/g;->e:I

    goto :goto_3

    :cond_6
    const/4 p2, 0x6

    iput p2, v0, Lk2/g;->d:I

    goto :goto_2

    :cond_7
    const/16 p2, 0x8

    iput p2, v0, Lk2/g;->d:I

    const/16 p2, 0x64

    iput p2, v0, Lk2/g;->e:I

    goto :goto_3

    :cond_8
    iput v5, v0, Lk2/g;->d:I

    iput v2, v0, Lk2/g;->e:I

    goto :goto_3

    :cond_9
    :goto_1
    iput v4, v0, Lk2/g;->d:I

    :goto_2
    move v2, v5

    :cond_a
    :goto_3
    iget-object p2, p0, Lk2/e$b;->a:Lk2/e;

    invoke-static {p2}, Lk2/e;->d(Lk2/e;)Ljava/util/Map;

    move-result-object p2

    invoke-interface {p2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p2, p0, Lk2/e$b;->a:Lk2/e;

    invoke-static {p2, p1, v0}, Lk2/e;->e(Lk2/e;Ljava/lang/String;Lk2/g;)V

    if-eqz v2, :cond_b

    iget-object p2, p0, Lk2/e$b;->a:Lk2/e;

    invoke-static {p2}, Lk2/e;->d(Lk2/e;)Ljava/util/Map;

    move-result-object p2

    invoke-interface {p2, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_b
    return-void
.end method
