.class public Lk2/a$b;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lk2/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Lcom/miui/packageInstaller/model/AdInterface;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/miui/packageInstaller/model/AdInterface;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lk2/a$b;->a:Ljava/lang/String;

    iput-object p2, p0, Lk2/a$b;->b:Lcom/miui/packageInstaller/model/AdInterface;

    iput-object p3, p0, Lk2/a$b;->c:Ljava/lang/String;

    iput-object p4, p0, Lk2/a$b;->d:Ljava/lang/String;

    iput-object p5, p0, Lk2/a$b;->e:Ljava/lang/String;

    return-void
.end method

.method static synthetic a(Lk2/a$b;)Lcom/miui/packageInstaller/model/AdInterface;
    .locals 0

    iget-object p0, p0, Lk2/a$b;->b:Lcom/miui/packageInstaller/model/AdInterface;

    return-object p0
.end method

.method static synthetic b(Lk2/a$b;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lk2/a$b;->a:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic c(Lk2/a$b;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lk2/a$b;->d:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic d(Lk2/a$b;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lk2/a$b;->c:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic e(Lk2/a$b;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lk2/a$b;->e:Ljava/lang/String;

    return-object p0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "btn: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lk2/a$b;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", displayType: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lk2/a$b;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", event: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lk2/a$b;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
