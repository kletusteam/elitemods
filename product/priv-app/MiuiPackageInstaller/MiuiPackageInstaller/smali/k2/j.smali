.class public Lk2/j;
.super Ljava/lang/Object;

# interfaces
.implements Lk2/h;


# instance fields
.field public a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lk2/h;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lk2/h;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lk2/j;->a:Ljava/lang/ref/WeakReference;

    iput-object p1, p0, Lk2/j;->b:Ljava/lang/String;

    return-void
.end method

.method public static synthetic a(Lk2/j;)V
    .locals 0

    invoke-direct {p0}, Lk2/j;->d()V

    return-void
.end method

.method private synthetic d()V
    .locals 1

    invoke-static {}, Lk2/e;->h()Lk2/e;

    move-result-object v0

    invoke-virtual {v0, p0}, Lk2/e;->q(Lk2/h;)V

    return-void
.end method


# virtual methods
.method public b(Ljava/lang/String;Lk2/g;)V
    .locals 2

    iget-object v0, p0, Lk2/j;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lk2/h;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lk2/j;->b:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lk2/j;->b:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    invoke-interface {v0, p1, p2}, Lk2/h;->b(Ljava/lang/String;Lk2/g;)V

    goto :goto_0

    :cond_1
    invoke-static {}, Lf6/z;->b()Lf6/z;

    move-result-object p1

    new-instance p2, Lk2/i;

    invoke-direct {p2, p0}, Lk2/i;-><init>(Lk2/j;)V

    invoke-virtual {p1, p2}, Lf6/z;->e(Ljava/lang/Runnable;)V

    :cond_2
    :goto_0
    return-void
.end method

.method public c()Z
    .locals 1

    iget-object v0, p0, Lk2/j;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
