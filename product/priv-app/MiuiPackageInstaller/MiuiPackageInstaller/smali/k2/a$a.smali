.class Lk2/a$a;
.super Landroid/os/AsyncTask;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lk2/a;->e(Landroid/content/Context;Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Landroid/content/Context;

.field final synthetic b:Ljava/util/ArrayList;


# direct methods
.method constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 0

    iput-object p1, p0, Lk2/a$a;->a:Landroid/content/Context;

    iput-object p2, p0, Lk2/a$a;->b:Ljava/util/ArrayList;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 6

    iget-object p1, p0, Lk2/a$a;->a:Landroid/content/Context;

    invoke-static {p1}, Lcom/xiaomi/analytics/Analytics;->getInstance(Landroid/content/Context;)Lcom/xiaomi/analytics/Analytics;

    move-result-object p1

    sget-boolean v0, Lq2/c;->a:Z

    invoke-virtual {p1, v0}, Lcom/xiaomi/analytics/Analytics;->setDebugOn(Z)V

    sget-boolean v0, Lq2/c;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "packageinstaller_ads0"

    goto :goto_0

    :cond_0
    const-string v0, "packageinstaller_ads"

    :goto_0
    invoke-virtual {p1, v0}, Lcom/xiaomi/analytics/Analytics;->getTracker(Ljava/lang/String;)Lcom/xiaomi/analytics/Tracker;

    move-result-object p1

    iget-object v0, p0, Lk2/a$a;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lk2/a$b;

    invoke-static {v1}, Lk2/a$b;->a(Lk2/a$b;)Lcom/miui/packageInstaller/model/AdInterface;

    move-result-object v2

    invoke-static {v1}, Lk2/a$b;->b(Lk2/a$b;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/xiaomi/analytics/Actions;->newAdAction(Ljava/lang/String;)Lcom/xiaomi/analytics/AdAction;

    move-result-object v3

    invoke-static {v1}, Lk2/a$b;->b(Lk2/a$b;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "e"

    invoke-virtual {v3, v5, v4}, Lcom/xiaomi/analytics/Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/xiaomi/analytics/Action;

    invoke-static {v1}, Lk2/a$b;->c(Lk2/a$b;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "displayType"

    invoke-virtual {v3, v5, v4}, Lcom/xiaomi/analytics/Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/xiaomi/analytics/Action;

    invoke-static {v1}, Lk2/a$b;->d(Lk2/a$b;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "btn"

    invoke-virtual {v3, v5, v4}, Lcom/xiaomi/analytics/Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/xiaomi/analytics/Action;

    invoke-interface {v2}, Lcom/miui/packageInstaller/model/AdInterface;->getEx()Ljava/lang/String;

    move-result-object v2

    const-string v4, "ex"

    invoke-virtual {v3, v4, v2}, Lcom/xiaomi/analytics/Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/xiaomi/analytics/Action;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    const-string v4, "t"

    invoke-virtual {v3, v4, v2}, Lcom/xiaomi/analytics/Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/xiaomi/analytics/Action;

    invoke-static {v1}, Lk2/a$b;->e(Lk2/a$b;)Ljava/lang/String;

    move-result-object v2

    const-string v4, "oneTrackParams"

    invoke-virtual {v3, v4, v2}, Lcom/xiaomi/analytics/Action;->addParam(Ljava/lang/String;Ljava/lang/String;)Lcom/xiaomi/analytics/Action;

    invoke-static {v1}, Lk2/a$b;->b(Lk2/a$b;)Ljava/lang/String;

    move-result-object v2

    const-string v4, "VIEW"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {v1}, Lk2/a$b;->a(Lk2/a$b;)Lcom/miui/packageInstaller/model/AdInterface;

    move-result-object v1

    invoke-interface {v1}, Lcom/miui/packageInstaller/model/AdInterface;->getViewMonitorUrls()[Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lk2/a;->a(Lcom/xiaomi/analytics/AdAction;[Ljava/lang/String;)V

    goto :goto_2

    :cond_1
    invoke-static {v1}, Lk2/a$b;->b(Lk2/a$b;)Ljava/lang/String;

    move-result-object v2

    const-string v4, "CLICK"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static {v1}, Lk2/a$b;->a(Lk2/a$b;)Lcom/miui/packageInstaller/model/AdInterface;

    move-result-object v1

    invoke-interface {v1}, Lcom/miui/packageInstaller/model/AdInterface;->getClickMonitorUrls()[Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lk2/a;->a(Lcom/xiaomi/analytics/AdAction;[Ljava/lang/String;)V

    :cond_2
    :goto_2
    invoke-virtual {p1, v3}, Lcom/xiaomi/analytics/Tracker;->track(Lcom/xiaomi/analytics/Action;)V

    goto/16 :goto_1

    :cond_3
    const/4 p1, 0x0

    return-object p1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lk2/a$a;->a([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method
