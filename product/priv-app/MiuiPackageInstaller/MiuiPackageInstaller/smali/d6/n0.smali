.class public final Ld6/n0;
.super Ljava/lang/Object;


# instance fields
.field private final a:Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;

.field private final b:Landroid/widget/TextView;

.field private c:Landroid/view/ViewGroup;

.field private d:Landroidx/appcompat/widget/LinearLayoutCompat;

.field private e:Landroid/widget/ImageView;

.field private f:Landroid/view/ViewGroup;

.field private g:Landroidx/recyclerview/widget/RecyclerView;

.field private h:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>(Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;)V
    .locals 2

    const-string v0, "securityActivity"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Ld6/n0;->a:Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;

    const v0, 0x7f0a036f

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "securityActivity.findVie\u2026d(R.id.text_above_button)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ld6/n0;->b:Landroid/widget/TextView;

    const v0, 0x7f0a01ff

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "securityActivity.findVie\u2026Id(R.id.ll_status_layout)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Ld6/n0;->c:Landroid/view/ViewGroup;

    const v0, 0x7f0a0095

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "securityActivity.findViewById(R.id.bottom_layout)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroidx/appcompat/widget/LinearLayoutCompat;

    iput-object v0, p0, Ld6/n0;->d:Landroidx/appcompat/widget/LinearLayoutCompat;

    const v0, 0x7f0a020b

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "securityActivity.findViewById(R.id.logo)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Ld6/n0;->e:Landroid/widget/ImageView;

    const v0, 0x7f0a005f

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "securityActivity.findViewById(R.id.anim_content)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Ld6/n0;->f:Landroid/view/ViewGroup;

    const v0, 0x7f0a02f8

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "securityActivity.findVie\u2026curity_mode_service_list)"

    invoke-static {v0, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    iput-object v0, p0, Ld6/n0;->g:Landroidx/recyclerview/widget/RecyclerView;

    const v0, 0x7f0a0060

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v0, "securityActivity.findVie\u2026d.anim_group_root_layout)"

    invoke-static {p1, v0}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/view/ViewGroup;

    iput-object p1, p0, Ld6/n0;->h:Landroid/view/ViewGroup;

    return-void
.end method

.method public static synthetic a(Landroid/content/res/Resources;Ld6/n0;Landroid/animation/ValueAnimator;)V
    .locals 0

    invoke-static {p0, p1, p2}, Ld6/n0;->e(Landroid/content/res/Resources;Ld6/n0;Landroid/animation/ValueAnimator;)V

    return-void
.end method

.method private final b()F
    .locals 2

    iget-object v0, p0, Ld6/n0;->a:Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;

    const v1, 0x7f0a020c

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Ld6/n0;->e:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getWidth()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    return v0
.end method

.method private final c()[F
    .locals 9

    const/4 v0, 0x2

    new-array v1, v0, [F

    new-array v2, v0, [I

    iget-object v3, p0, Ld6/n0;->e:Landroid/widget/ImageView;

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->getLocationInWindow([I)V

    const/4 v3, 0x0

    aget v4, v2, v3

    iget-object v5, p0, Ld6/n0;->e:Landroid/widget/ImageView;

    invoke-virtual {v5}, Landroid/widget/ImageView;->getWidth()I

    move-result v5

    div-int/2addr v5, v0

    add-int/2addr v4, v5

    aput v4, v2, v3

    const/4 v4, 0x1

    aget v5, v2, v4

    iget-object v6, p0, Ld6/n0;->e:Landroid/widget/ImageView;

    invoke-virtual {v6}, Landroid/widget/ImageView;->getHeight()I

    move-result v6

    div-int/2addr v6, v0

    add-int/2addr v5, v6

    aput v5, v2, v4

    new-array v5, v0, [I

    iget-object v6, p0, Ld6/n0;->a:Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;

    const v7, 0x7f0a020c

    invoke-virtual {v6, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v5}, Landroid/view/View;->getLocationInWindow([I)V

    aget v7, v5, v3

    invoke-virtual {v6}, Landroid/view/View;->getWidth()I

    move-result v8

    div-int/2addr v8, v0

    add-int/2addr v7, v8

    aput v7, v5, v3

    aget v7, v5, v4

    invoke-virtual {v6}, Landroid/view/View;->getHeight()I

    move-result v6

    div-int/2addr v6, v0

    add-int/2addr v7, v6

    aput v7, v5, v4

    aget v0, v5, v3

    aget v6, v2, v3

    sub-int/2addr v0, v6

    int-to-float v0, v0

    aput v0, v1, v3

    aget v0, v5, v4

    aget v2, v2, v4

    sub-int/2addr v0, v2

    int-to-float v0, v0

    aput v0, v1, v4

    return-object v1
.end method

.method private static final e(Landroid/content/res/Resources;Ld6/n0;Landroid/animation/ValueAnimator;)V
    .locals 4

    const-string v0, "this$0"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "animation"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object p2

    const-string v0, "null cannot be cast to non-null type kotlin.Float"

    invoke-static {p2, v0}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Float;

    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result p2

    float-to-int v0, p2

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    float-to-long v2, p2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    const/4 v2, 0x0

    aput-object p2, v1, v2

    const p2, 0x7f0f0018

    invoke-virtual {p0, p2, v0, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0, v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;I)Landroid/text/Spanned;

    move-result-object p0

    iget-object p1, p1, Ld6/n0;->b:Landroid/widget/TextView;

    invoke-virtual {p1, p0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public final d(JJLd8/d;)Ljava/lang/Object;
    .locals 24
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Ld8/d<",
            "-",
            "La8/v;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p5

    instance-of v2, v1, Ld6/n0$a;

    if-eqz v2, :cond_0

    move-object v2, v1

    check-cast v2, Ld6/n0$a;

    iget v3, v2, Ld6/n0$a;->m:I

    const/high16 v4, -0x80000000

    and-int v5, v3, v4

    if-eqz v5, :cond_0

    sub-int/2addr v3, v4

    iput v3, v2, Ld6/n0$a;->m:I

    goto :goto_0

    :cond_0
    new-instance v2, Ld6/n0$a;

    invoke-direct {v2, v0, v1}, Ld6/n0$a;-><init>(Ld6/n0;Ld8/d;)V

    :goto_0
    iget-object v1, v2, Ld6/n0$a;->k:Ljava/lang/Object;

    invoke-static {}, Le8/b;->c()Ljava/lang/Object;

    move-result-object v3

    iget v4, v2, Ld6/n0$a;->m:I

    const-string v5, "translationX"

    const-string v6, "scaleY"

    const-string v7, "scaleX"

    const/4 v8, 0x5

    const/4 v9, 0x4

    const/4 v11, 0x3

    const-string v13, "translationY"

    const-string v14, "alpha"

    move-object/from16 v16, v13

    const/16 v18, 0x0

    const/4 v12, 0x1

    const/4 v13, 0x2

    if-eqz v4, :cond_6

    if-eq v4, v12, :cond_5

    if-eq v4, v13, :cond_4

    if-eq v4, v11, :cond_3

    if-eq v4, v9, :cond_2

    if-ne v4, v8, :cond_1

    iget-object v2, v2, Ld6/n0$a;->d:Ljava/lang/Object;

    check-cast v2, Ld6/n0;

    invoke-static {v1}, La8/n;->b(Ljava/lang/Object;)V

    goto/16 :goto_6

    :cond_1
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    iget v4, v2, Ld6/n0$a;->j:F

    iget v9, v2, Ld6/n0$a;->i:F

    iget-object v11, v2, Ld6/n0$a;->f:Ljava/lang/Object;

    check-cast v11, Landroid/animation/ObjectAnimator;

    iget-object v8, v2, Ld6/n0$a;->e:Ljava/lang/Object;

    check-cast v8, Landroid/content/res/Resources;

    iget-object v10, v2, Ld6/n0$a;->d:Ljava/lang/Object;

    check-cast v10, Ld6/n0;

    invoke-static {v1}, La8/n;->b(Ljava/lang/Object;)V

    move-object v0, v2

    move-object v1, v5

    move-object/from16 p1, v6

    move-object/from16 v22, v7

    move-object v2, v10

    move-object v7, v11

    move-object v11, v14

    move-object/from16 v12, v16

    const-wide/16 v5, 0x3e8

    goto/16 :goto_5

    :cond_3
    iget v4, v2, Ld6/n0$a;->j:F

    iget v8, v2, Ld6/n0$a;->i:F

    iget-object v10, v2, Ld6/n0$a;->g:Ljava/lang/Object;

    check-cast v10, Ljava/util/Iterator;

    iget-object v9, v2, Ld6/n0$a;->f:Ljava/lang/Object;

    check-cast v9, Landroid/animation/ObjectAnimator;

    iget-object v11, v2, Ld6/n0$a;->e:Ljava/lang/Object;

    check-cast v11, Landroid/content/res/Resources;

    iget-object v15, v2, Ld6/n0$a;->d:Ljava/lang/Object;

    check-cast v15, Ld6/n0;

    invoke-static {v1}, La8/n;->b(Ljava/lang/Object;)V

    move-object v1, v5

    move-object/from16 p1, v6

    move-object/from16 v22, v7

    move-object v0, v9

    move-object/from16 v12, v16

    const/4 v7, 0x3

    move v9, v8

    move-object v8, v11

    move-object v11, v14

    goto/16 :goto_4

    :cond_4
    iget v4, v2, Ld6/n0$a;->j:F

    iget v8, v2, Ld6/n0$a;->i:F

    iget-object v9, v2, Ld6/n0$a;->g:Ljava/lang/Object;

    check-cast v9, Landroid/animation/ObjectAnimator;

    iget-object v10, v2, Ld6/n0$a;->f:Ljava/lang/Object;

    check-cast v10, Landroid/content/res/Resources;

    iget-object v11, v2, Ld6/n0$a;->e:Ljava/lang/Object;

    check-cast v11, Ljava/util/List;

    iget-object v15, v2, Ld6/n0$a;->d:Ljava/lang/Object;

    check-cast v15, Ld6/n0;

    invoke-static {v1}, La8/n;->b(Ljava/lang/Object;)V

    move-object v1, v5

    move-object/from16 p1, v6

    move-object/from16 v22, v7

    move-object v0, v11

    move-object v11, v14

    move-object/from16 v12, v16

    goto/16 :goto_3

    :cond_5
    iget-object v4, v2, Ld6/n0$a;->h:Ljava/lang/Object;

    check-cast v4, Landroid/content/res/Resources;

    iget-object v8, v2, Ld6/n0$a;->g:Ljava/lang/Object;

    check-cast v8, Ljava/util/List;

    iget-object v9, v2, Ld6/n0$a;->f:Ljava/lang/Object;

    check-cast v9, Landroidx/recyclerview/widget/RecyclerView;

    iget-object v10, v2, Ld6/n0$a;->e:Ljava/lang/Object;

    check-cast v10, Landroid/view/View;

    iget-object v11, v2, Ld6/n0$a;->d:Ljava/lang/Object;

    check-cast v11, Ld6/n0;

    invoke-static {v1}, La8/n;->b(Ljava/lang/Object;)V

    move-object v1, v10

    move-object v15, v11

    move-object/from16 v20, v14

    move-object v10, v4

    move-object v11, v6

    move-object v4, v8

    move-object v8, v5

    goto/16 :goto_2

    :cond_6
    invoke-static {v1}, La8/n;->b(Ljava/lang/Object;)V

    iget-object v1, v0, Ld6/n0;->a:Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;

    const v4, 0x7f0a0062

    invoke-virtual {v1, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v10

    const-string v1, "securityActivity.findVie\u2026.anim_view_group_welcome)"

    invoke-static {v10, v1}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, v0, Ld6/n0;->a:Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;

    const v4, 0x7f0a0307

    invoke-virtual {v1, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const-string v4, "securityActivity.findViewById(R.id.service_list)"

    invoke-static {v1, v4}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v9, v1

    check-cast v9, Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v9}, Landroidx/recyclerview/widget/RecyclerView;->getAdapter()Landroidx/recyclerview/widget/RecyclerView$g;

    move-result-object v1

    const-string v4, "null cannot be cast to non-null type com.miui.packageInstaller.view.recyclerview.CommonRecyclerViewAdapter"

    invoke-static {v1, v4}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lj6/b;

    invoke-virtual {v1}, Lj6/b;->R()Ljava/util/List;

    move-result-object v1

    iget-object v4, v0, Ld6/n0;->a:Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;

    invoke-virtual {v4}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const-wide/16 v20, 0x0

    cmp-long v8, p3, v20

    if-eqz v8, :cond_7

    move-object v8, v5

    move-object v11, v6

    move-wide/from16 v5, p3

    goto :goto_1

    :cond_7
    move-object v8, v5

    move-object v11, v6

    move-wide/from16 v5, p1

    :goto_1
    new-array v15, v13, [F

    move-object/from16 v20, v14

    move-wide/from16 v13, p1

    long-to-float v13, v13

    aput v13, v15, v18

    long-to-float v5, v5

    aput v5, v15, v12

    invoke-static {v15}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v5

    const-wide/16 v13, 0xc8

    invoke-virtual {v5, v13, v14}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    const-wide/16 v13, 0x32

    invoke-virtual {v5, v13, v14}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    new-instance v6, Ld6/m0;

    invoke-direct {v6, v4, v0}, Ld6/m0;-><init>(Landroid/content/res/Resources;Ld6/n0;)V

    invoke-virtual {v5, v6}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    new-instance v6, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v6}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v5, v6}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    invoke-virtual {v5}, Landroid/animation/ValueAnimator;->start()V

    iput-object v0, v2, Ld6/n0$a;->d:Ljava/lang/Object;

    iput-object v10, v2, Ld6/n0$a;->e:Ljava/lang/Object;

    iput-object v9, v2, Ld6/n0$a;->f:Ljava/lang/Object;

    iput-object v1, v2, Ld6/n0$a;->g:Ljava/lang/Object;

    iput-object v4, v2, Ld6/n0$a;->h:Ljava/lang/Object;

    iput v12, v2, Ld6/n0$a;->m:I

    const-wide/16 v5, 0xc8

    invoke-static {v5, v6, v2}, Lv8/o0;->a(JLd8/d;)Ljava/lang/Object;

    move-result-object v5

    if-ne v5, v3, :cond_8

    return-object v3

    :cond_8
    move-object v15, v0

    move-object/from16 v23, v4

    move-object v4, v1

    move-object v1, v10

    move-object/from16 v10, v23

    :goto_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    iget-object v5, v15, Ld6/n0;->c:Landroid/view/ViewGroup;

    const/4 v6, 0x2

    new-array v13, v6, [F

    fill-array-data v13, :array_0

    move-object/from16 v14, v20

    invoke-static {v5, v14, v13}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v5

    const/4 v6, 0x0

    const/high16 v12, 0x3f800000    # 1.0f

    const/high16 v13, 0x3f000000    # 0.5f

    invoke-static {v6, v13, v13, v12}, Lg0/a;->a(FFFF)Landroid/view/animation/Interpolator;

    move-result-object v0

    const-wide/16 v12, 0x14a

    invoke-virtual {v5, v12, v13}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    invoke-virtual {v5}, Landroid/animation/ObjectAnimator;->start()V

    iget-object v5, v15, Ld6/n0;->d:Landroidx/appcompat/widget/LinearLayoutCompat;

    const/4 v6, 0x2

    new-array v12, v6, [F

    fill-array-data v12, :array_1

    invoke-static {v5, v14, v12}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v5

    const-wide/16 v12, 0x14a

    invoke-virtual {v5, v12, v13}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    invoke-virtual {v5}, Landroid/animation/ObjectAnimator;->start()V

    iget-object v5, v15, Ld6/n0;->e:Landroid/widget/ImageView;

    const/4 v6, 0x3

    new-array v12, v6, [F

    fill-array-data v12, :array_2

    invoke-static {v5, v14, v12}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v5

    const-wide/16 v12, 0x3e8

    invoke-virtual {v5, v12, v13}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    invoke-virtual {v5}, Landroid/animation/ObjectAnimator;->start()V

    iget-object v6, v15, Ld6/n0;->e:Landroid/widget/ImageView;

    const/4 v12, 0x2

    new-array v13, v12, [F

    const/high16 v17, 0x3f800000    # 1.0f

    aput v17, v13, v18

    invoke-direct {v15}, Ld6/n0;->b()F

    move-result v21

    const/16 v20, 0x1

    aput v21, v13, v20

    invoke-static {v6, v7, v13}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v6

    const-wide/16 v12, 0x3e8

    invoke-virtual {v6, v12, v13}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    invoke-virtual {v6}, Landroid/animation/ObjectAnimator;->start()V

    iget-object v6, v15, Ld6/n0;->e:Landroid/widget/ImageView;

    const/4 v12, 0x2

    new-array v13, v12, [F

    aput v17, v13, v18

    invoke-direct {v15}, Ld6/n0;->b()F

    move-result v21

    aput v21, v13, v20

    invoke-static {v6, v11, v13}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v6

    const-wide/16 v12, 0x3e8

    invoke-virtual {v6, v12, v13}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    invoke-virtual {v6}, Landroid/animation/ObjectAnimator;->start()V

    invoke-direct {v15}, Ld6/n0;->c()[F

    move-result-object v6

    aget v12, v6, v18

    aget v6, v6, v20

    iget-object v13, v15, Ld6/n0;->e:Landroid/widget/ImageView;

    move-object/from16 v22, v7

    move-object/from16 p1, v11

    const/4 v11, 0x2

    new-array v7, v11, [F

    const/16 v19, 0x0

    aput v19, v7, v18

    aput v12, v7, v20

    invoke-static {v13, v8, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v7

    move/from16 p2, v12

    const-wide/16 v11, 0x3e8

    invoke-virtual {v7, v11, v12}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    invoke-virtual {v7}, Landroid/animation/ObjectAnimator;->start()V

    iget-object v7, v15, Ld6/n0;->e:Landroid/widget/ImageView;

    const/4 v13, 0x2

    new-array v11, v13, [F

    aput v19, v11, v18

    aput v6, v11, v20

    move-object/from16 v12, v16

    invoke-static {v7, v12, v11}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v7

    move-object v11, v14

    const-wide/16 v13, 0x3e8

    invoke-virtual {v7, v13, v14}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    invoke-virtual {v7, v0}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    invoke-virtual {v7}, Landroid/animation/ObjectAnimator;->start()V

    const v0, 0x7f0700ff

    invoke-virtual {v10, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setTranslationY(F)V

    const/4 v7, 0x2

    new-array v13, v7, [F

    aput v0, v13, v18

    aput v19, v13, v20

    invoke-static {v1, v12, v13}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    const-wide/16 v13, 0x14d

    invoke-virtual {v0, v13, v14}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    const-wide/16 v13, 0x29b

    invoke-virtual {v0, v13, v14}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    new-array v0, v7, [F

    fill-array-data v0, :array_3

    invoke-static {v1, v11, v0}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    move-object v1, v8

    const-wide/16 v7, 0x14d

    invoke-virtual {v0, v7, v8}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, v13, v14}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    const v0, 0x7f0700dd

    invoke-virtual {v10, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    invoke-virtual {v9, v0}, Landroid/view/ViewGroup;->setTranslationY(F)V

    const/4 v13, 0x2

    new-array v14, v13, [F

    aput v0, v14, v18

    const/4 v0, 0x0

    const/16 v16, 0x1

    aput v0, v14, v16

    invoke-static {v9, v12, v14}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0, v7, v8}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    const-wide/16 v7, 0x29b

    invoke-virtual {v0, v7, v8}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    new-array v0, v13, [F

    fill-array-data v0, :array_4

    invoke-static {v9, v11, v0}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    const-wide/16 v13, 0x14d

    invoke-virtual {v0, v13, v14}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, v7, v8}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    iput-object v15, v2, Ld6/n0$a;->d:Ljava/lang/Object;

    iput-object v4, v2, Ld6/n0$a;->e:Ljava/lang/Object;

    iput-object v10, v2, Ld6/n0$a;->f:Ljava/lang/Object;

    iput-object v5, v2, Ld6/n0$a;->g:Ljava/lang/Object;

    const/4 v0, 0x0

    iput-object v0, v2, Ld6/n0$a;->h:Ljava/lang/Object;

    move/from16 v0, p2

    iput v0, v2, Ld6/n0$a;->i:F

    iput v6, v2, Ld6/n0$a;->j:F

    const/4 v7, 0x2

    iput v7, v2, Ld6/n0$a;->m:I

    const-wide/16 v7, 0x3e8

    invoke-static {v7, v8, v2}, Lv8/o0;->a(JLd8/d;)Ljava/lang/Object;

    move-result-object v9

    if-ne v9, v3, :cond_9

    return-object v3

    :cond_9
    move v8, v0

    move-object v0, v4

    move-object v9, v5

    move v4, v6

    :goto_3
    const-string v5, "serviceItems"

    invoke-static {v0, v5}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lb8/j;->t(Ljava/lang/Iterable;)Lt8/f;

    move-result-object v0

    sget-object v5, Ld6/n0$b;->b:Ld6/n0$b;

    invoke-static {v0, v5}, Lt8/g;->j(Lt8/f;Ll8/l;)Lt8/f;

    move-result-object v0

    invoke-interface {v0}, Lt8/f;->iterator()Ljava/util/Iterator;

    move-result-object v0

    move-object/from16 v23, v10

    move-object v10, v0

    move-object v0, v9

    move v9, v8

    move-object/from16 v8, v23

    :cond_a
    :goto_4
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_b

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceAnimViewObject;

    invoke-virtual {v5}, Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceAnimViewObject;->z()V

    const-wide/16 v5, 0x104

    iput-object v15, v2, Ld6/n0$a;->d:Ljava/lang/Object;

    iput-object v8, v2, Ld6/n0$a;->e:Ljava/lang/Object;

    iput-object v0, v2, Ld6/n0$a;->f:Ljava/lang/Object;

    iput-object v10, v2, Ld6/n0$a;->g:Ljava/lang/Object;

    iput v9, v2, Ld6/n0$a;->i:F

    iput v4, v2, Ld6/n0$a;->j:F

    const/4 v7, 0x3

    iput v7, v2, Ld6/n0$a;->m:I

    invoke-static {v5, v6, v2}, Lv8/o0;->a(JLd8/d;)Ljava/lang/Object;

    move-result-object v5

    if-ne v5, v3, :cond_a

    return-object v3

    :cond_b
    iput-object v15, v2, Ld6/n0$a;->d:Ljava/lang/Object;

    iput-object v8, v2, Ld6/n0$a;->e:Ljava/lang/Object;

    iput-object v0, v2, Ld6/n0$a;->f:Ljava/lang/Object;

    const/4 v5, 0x0

    iput-object v5, v2, Ld6/n0$a;->g:Ljava/lang/Object;

    iput v9, v2, Ld6/n0$a;->i:F

    iput v4, v2, Ld6/n0$a;->j:F

    const/4 v5, 0x4

    iput v5, v2, Ld6/n0$a;->m:I

    const-wide/16 v5, 0x3e8

    invoke-static {v5, v6, v2}, Lv8/o0;->a(JLd8/d;)Ljava/lang/Object;

    move-result-object v7

    if-ne v7, v3, :cond_c

    return-object v3

    :cond_c
    move-object v7, v0

    move-object v0, v2

    move-object v2, v15

    :goto_5
    iget-object v10, v2, Ld6/n0;->a:Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;

    invoke-virtual {v10}, Lcom/miui/packageInstaller/ui/secure/SecureModeActivity;->m1()V

    iget-object v10, v2, Ld6/n0;->d:Landroidx/appcompat/widget/LinearLayoutCompat;

    const/16 v13, 0x8

    invoke-virtual {v10, v13}, Landroid/view/ViewGroup;->setVisibility(I)V

    const v10, 0x3f333333    # 0.7f

    const/high16 v13, 0x3f000000    # 0.5f

    const/high16 v14, 0x3f800000    # 1.0f

    const/4 v15, 0x0

    invoke-static {v13, v15, v14, v10}, Lg0/a;->a(FFFF)Landroid/view/animation/Interpolator;

    move-result-object v10

    invoke-virtual {v7, v5, v6}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    invoke-virtual {v7}, Landroid/animation/ObjectAnimator;->start()V

    iget-object v7, v2, Ld6/n0;->e:Landroid/widget/ImageView;

    const/4 v13, 0x2

    new-array v15, v13, [F

    invoke-direct {v2}, Ld6/n0;->b()F

    move-result v16

    aput v16, v15, v18

    const/16 v16, 0x1

    aput v14, v15, v16

    move-object/from16 v14, v22

    invoke-static {v7, v14, v15}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v7

    invoke-virtual {v7, v5, v6}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    invoke-virtual {v7}, Landroid/animation/ObjectAnimator;->start()V

    iget-object v7, v2, Ld6/n0;->e:Landroid/widget/ImageView;

    new-array v14, v13, [F

    invoke-direct {v2}, Ld6/n0;->b()F

    move-result v15

    aput v15, v14, v18

    const/high16 v15, 0x3f800000    # 1.0f

    aput v15, v14, v16

    move-object/from16 v15, p1

    invoke-static {v7, v15, v14}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v7

    invoke-virtual {v7, v5, v6}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    invoke-virtual {v7}, Landroid/animation/ObjectAnimator;->start()V

    iget-object v7, v2, Ld6/n0;->e:Landroid/widget/ImageView;

    new-array v14, v13, [F

    aput v9, v14, v18

    const/4 v9, 0x0

    aput v9, v14, v16

    invoke-static {v7, v1, v14}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    invoke-virtual {v1, v5, v6}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->start()V

    iget-object v1, v2, Ld6/n0;->e:Landroid/widget/ImageView;

    new-array v7, v13, [F

    aput v4, v7, v18

    aput v9, v7, v16

    invoke-static {v1, v12, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    invoke-virtual {v1, v5, v6}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    invoke-virtual {v1, v10}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->start()V

    iget-object v1, v2, Ld6/n0;->h:Landroid/view/ViewGroup;

    new-array v4, v13, [F

    fill-array-data v4, :array_5

    invoke-static {v1, v11, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    const-wide/16 v4, 0x120

    invoke-virtual {v1, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->start()V

    iget-object v1, v2, Ld6/n0;->c:Landroid/view/ViewGroup;

    new-array v4, v13, [F

    fill-array-data v4, :array_6

    invoke-static {v1, v11, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    const-wide/16 v4, 0x14a

    invoke-virtual {v1, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    const-wide/16 v6, 0x244

    invoke-virtual {v1, v6, v7}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->start()V

    iget-object v1, v2, Ld6/n0;->d:Landroidx/appcompat/widget/LinearLayoutCompat;

    new-array v9, v13, [F

    fill-array-data v9, :array_7

    invoke-static {v1, v11, v9}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    invoke-virtual {v1, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    invoke-virtual {v1, v6, v7}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->start()V

    const v1, 0x7f0700ab

    invoke-virtual {v8, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    iget-object v4, v2, Ld6/n0;->g:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v4, v1}, Landroid/view/ViewGroup;->setTranslationY(F)V

    iget-object v4, v2, Ld6/n0;->g:Landroidx/recyclerview/widget/RecyclerView;

    new-array v5, v13, [F

    aput v1, v5, v18

    const/4 v1, 0x0

    const/4 v8, 0x1

    aput v1, v5, v8

    invoke-static {v4, v12, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    const-wide/16 v4, 0x14a

    invoke-virtual {v1, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    invoke-virtual {v1, v6, v7}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->start()V

    const-wide/16 v4, 0x1f4

    iput-object v2, v0, Ld6/n0$a;->d:Ljava/lang/Object;

    const/4 v1, 0x0

    iput-object v1, v0, Ld6/n0$a;->e:Ljava/lang/Object;

    iput-object v1, v0, Ld6/n0$a;->f:Ljava/lang/Object;

    const/4 v1, 0x5

    iput v1, v0, Ld6/n0$a;->m:I

    invoke-static {v4, v5, v0}, Lv8/o0;->a(JLd8/d;)Ljava/lang/Object;

    move-result-object v0

    if-ne v0, v3, :cond_d

    return-object v3

    :cond_d
    :goto_6
    iget-object v0, v2, Ld6/n0;->f:Landroid/view/ViewGroup;

    iget-object v1, v2, Ld6/n0;->h:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    sget-object v0, La8/v;->a:La8/v;

    return-object v0

    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data

    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data

    :array_2
    .array-data 4
        0x3f800000    # 1.0f
        0x3f000000    # 0.5f
        0x3f800000    # 1.0f
    .end array-data

    :array_3
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    :array_4
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    :array_5
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data

    :array_6
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    :array_7
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method
