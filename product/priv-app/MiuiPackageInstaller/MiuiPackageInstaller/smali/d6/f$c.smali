.class final Ld6/f$c;
.super Lf8/k;

# interfaces
.implements Ll8/p;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Ld6/f;->N0(Landroid/view/View;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf8/k;",
        "Ll8/p<",
        "Lv8/e0;",
        "Ld8/d<",
        "-",
        "La8/v;",
        ">;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation runtime Lf8/f;
    c = "com.miui.packageInstaller.ui.secure.OpenSecurityModeFragment$onViewCreated$3"
    f = "OpenSecurityModeFragment.kt"
    l = {
        0x9b
    }
    m = "invokeSuspend"
.end annotation


# instance fields
.field e:I

.field final synthetic f:Ld6/f;

.field final synthetic g:Landroid/view/View;


# direct methods
.method constructor <init>(Ld6/f;Landroid/view/View;Ld8/d;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ld6/f;",
            "Landroid/view/View;",
            "Ld8/d<",
            "-",
            "Ld6/f$c;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Ld6/f$c;->f:Ld6/f;

    iput-object p2, p0, Ld6/f$c;->g:Landroid/view/View;

    const/4 p1, 0x2

    invoke-direct {p0, p1, p3}, Lf8/k;-><init>(ILd8/d;)V

    return-void
.end method


# virtual methods
.method public final b(Ljava/lang/Object;Ld8/d;)Ld8/d;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ld8/d<",
            "*>;)",
            "Ld8/d<",
            "La8/v;",
            ">;"
        }
    .end annotation

    new-instance p1, Ld6/f$c;

    iget-object v0, p0, Ld6/f$c;->f:Ld6/f;

    iget-object v1, p0, Ld6/f$c;->g:Landroid/view/View;

    invoke-direct {p1, v0, v1, p2}, Ld6/f$c;-><init>(Ld6/f;Landroid/view/View;Ld8/d;)V

    return-object p1
.end method

.method public bridge synthetic g(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lv8/e0;

    check-cast p2, Ld8/d;

    invoke-virtual {p0, p1, p2}, Ld6/f$c;->q(Lv8/e0;Ld8/d;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final n(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 14

    invoke-static {}, Le8/b;->c()Ljava/lang/Object;

    move-result-object v0

    iget v1, p0, Ld6/f$c;->e:I

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    if-ne v1, v2, :cond_0

    invoke-static {p1}, La8/n;->b(Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    invoke-static {p1}, La8/n;->b(Ljava/lang/Object;)V

    invoke-static {}, Lcom/android/packageinstaller/InstallerApplication;->g()Lcom/android/packageinstaller/InstallerApplication;

    move-result-object p1

    const-string v1, "progress_done_anim.json"

    invoke-static {p1, v1}, Lv0/e;->f(Landroid/content/Context;Ljava/lang/String;)Lv0/k;

    move-result-object p1

    invoke-virtual {p1}, Lv0/k;->b()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lv0/d;

    iget-object v1, p0, Ld6/f$c;->f:Ld6/f;

    invoke-static {v1}, Ld6/f;->M1(Ld6/f;)Ljava/util/ArrayList;

    move-result-object v1

    new-instance v12, Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceAnimView2Object;

    iget-object v3, p0, Ld6/f$c;->g:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string v13, "view.context"

    invoke-static {v4, v13}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const v6, 0x7f08018b

    const v7, 0x7f110356

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x30

    const/4 v11, 0x0

    move-object v3, v12

    move-object v5, p1

    invoke-direct/range {v3 .. v11}, Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceAnimView2Object;-><init>(Landroid/content/Context;Lv0/d;IILl6/c;Lm6/b;ILm8/g;)V

    invoke-virtual {v1, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Ld6/f$c;->f:Ld6/f;

    invoke-static {v1}, Ld6/f;->M1(Ld6/f;)Ljava/util/ArrayList;

    move-result-object v1

    new-instance v12, Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceAnimView2Object;

    iget-object v3, p0, Ld6/f$c;->g:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v13}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const v6, 0x7f08018c

    const v7, 0x7f110357

    move-object v3, v12

    invoke-direct/range {v3 .. v11}, Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceAnimView2Object;-><init>(Landroid/content/Context;Lv0/d;IILl6/c;Lm6/b;ILm8/g;)V

    invoke-virtual {v1, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Ld6/f$c;->f:Ld6/f;

    invoke-static {v1}, Ld6/f;->M1(Ld6/f;)Ljava/util/ArrayList;

    move-result-object v1

    new-instance v12, Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceAnimView2Object;

    iget-object v3, p0, Ld6/f$c;->g:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v13}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const v6, 0x7f08018d

    const v7, 0x7f110358

    move-object v3, v12

    invoke-direct/range {v3 .. v11}, Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceAnimView2Object;-><init>(Landroid/content/Context;Lv0/d;IILl6/c;Lm6/b;ILm8/g;)V

    invoke-virtual {v1, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Ld6/f$c;->f:Ld6/f;

    invoke-static {v1}, Ld6/f;->M1(Ld6/f;)Ljava/util/ArrayList;

    move-result-object v1

    new-instance v12, Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceAnimView2Object;

    iget-object v3, p0, Ld6/f$c;->g:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v13}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const v6, 0x7f08018e

    const v7, 0x7f110359

    move-object v3, v12

    invoke-direct/range {v3 .. v11}, Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceAnimView2Object;-><init>(Landroid/content/Context;Lv0/d;IILl6/c;Lm6/b;ILm8/g;)V

    invoke-virtual {v1, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Ld6/f$c;->f:Ld6/f;

    invoke-static {v1}, Ld6/f;->M1(Ld6/f;)Ljava/util/ArrayList;

    move-result-object v1

    new-instance v12, Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceAnimView2Object;

    iget-object v3, p0, Ld6/f$c;->g:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v13}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const v6, 0x7f08018f

    const v7, 0x7f11035a

    move-object v3, v12

    invoke-direct/range {v3 .. v11}, Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceAnimView2Object;-><init>(Landroid/content/Context;Lv0/d;IILl6/c;Lm6/b;ILm8/g;)V

    invoke-virtual {v1, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {}, Lv8/t0;->c()Lv8/v1;

    move-result-object p1

    new-instance v1, Ld6/f$c$a;

    iget-object v3, p0, Ld6/f$c;->f:Ld6/f;

    const/4 v4, 0x0

    invoke-direct {v1, v3, v4}, Ld6/f$c$a;-><init>(Ld6/f;Ld8/d;)V

    iput v2, p0, Ld6/f$c;->e:I

    invoke-static {p1, v1, p0}, Lv8/f;->e(Ld8/g;Ll8/p;Ld8/d;)Ljava/lang/Object;

    move-result-object p1

    if-ne p1, v0, :cond_2

    return-object v0

    :cond_2
    :goto_0
    sget-object p1, La8/v;->a:La8/v;

    return-object p1
.end method

.method public final q(Lv8/e0;Ld8/d;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lv8/e0;",
            "Ld8/d<",
            "-",
            "La8/v;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Ld6/f$c;->b(Ljava/lang/Object;Ld8/d;)Ld8/d;

    move-result-object p1

    check-cast p1, Ld6/f$c;

    sget-object p2, La8/v;->a:La8/v;

    invoke-virtual {p1, p2}, Ld6/f$c;->n(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method
