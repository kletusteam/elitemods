.class public final Ld6/l0;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ld6/l0$a;
    }
.end annotation


# static fields
.field public static final c:Ld6/l0$a;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ld6/l0$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ld6/l0$a;-><init>(Lm8/g;)V

    sput-object v0, Ld6/l0;->c:Ld6/l0$a;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Ld6/l0;->a:Landroid/content/Context;

    iput-object p2, p0, Ld6/l0;->b:Ljava/lang/String;

    return-void
.end method

.method public static final synthetic a(Ld6/l0;)V
    .locals 0

    invoke-direct {p0}, Ld6/l0;->c()V

    return-void
.end method

.method public static final b()Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;
    .locals 1

    sget-object v0, Ld6/l0;->c:Ld6/l0$a;

    invoke-virtual {v0}, Ld6/l0$a;->c()Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;

    move-result-object v0

    return-object v0
.end method

.method private final c()V
    .locals 3

    iget-object v0, p0, Ld6/l0;->b:Ljava/lang/String;

    const-string v1, "packageinstaller"

    invoke-static {v0, v1}, Lm8/i;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ld6/l0;->a:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.action.once_Authorize"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public final d(Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;)V
    .locals 1

    const-string v0, "authorizeInfo"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;->setAuthorized(Z)V

    sget-object v0, Ld6/l0;->c:Ld6/l0$a;

    invoke-virtual {v0, p1}, Ld6/l0$a;->f(Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;)V

    return-void
.end method

.method public final e(Lcom/miui/packageInstaller/model/ApkInfo;Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;Ljava/lang/Runnable;)V
    .locals 3

    const-string v0, "authorizeInfo"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "callback"

    invoke-static {p3, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lx5/d;

    iget-object v1, p0, Ld6/l0;->a:Landroid/content/Context;

    const-string v2, "null cannot be cast to non-null type android.app.Activity"

    invoke-static {v1, v2}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Landroid/app/Activity;

    invoke-direct {v0, v1}, Lx5/d;-><init>(Landroid/app/Activity;)V

    new-instance v1, Ld6/l0$b;

    invoke-direct {v1, p0, p2, p3}, Ld6/l0$b;-><init>(Ld6/l0;Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;Ljava/lang/Runnable;)V

    invoke-virtual {v0, p1, p2, v1}, Lx5/d;->f(Lcom/miui/packageInstaller/model/ApkInfo;Lcom/miui/packageInstaller/model/InstallAuthorizeInfo;Ll8/p;)V

    return-void
.end method
