.class final Ld6/f$a;
.super Lf8/k;

# interfaces
.implements Ll8/p;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Ld6/f;->P1()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf8/k;",
        "Ll8/p<",
        "Lv8/e0;",
        "Ld8/d<",
        "-",
        "La8/v;",
        ">;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation runtime Lf8/f;
    c = "com.miui.packageInstaller.ui.secure.OpenSecurityModeFragment$exit$1"
    f = "OpenSecurityModeFragment.kt"
    l = {
        0xab
    }
    m = "invokeSuspend"
.end annotation


# instance fields
.field e:I

.field final synthetic f:Ld6/f;


# direct methods
.method constructor <init>(Ld6/f;Ld8/d;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ld6/f;",
            "Ld8/d<",
            "-",
            "Ld6/f$a;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Ld6/f$a;->f:Ld6/f;

    const/4 p1, 0x2

    invoke-direct {p0, p1, p2}, Lf8/k;-><init>(ILd8/d;)V

    return-void
.end method


# virtual methods
.method public final b(Ljava/lang/Object;Ld8/d;)Ld8/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ld8/d<",
            "*>;)",
            "Ld8/d<",
            "La8/v;",
            ">;"
        }
    .end annotation

    new-instance p1, Ld6/f$a;

    iget-object v0, p0, Ld6/f$a;->f:Ld6/f;

    invoke-direct {p1, v0, p2}, Ld6/f$a;-><init>(Ld6/f;Ld8/d;)V

    return-object p1
.end method

.method public bridge synthetic g(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lv8/e0;

    check-cast p2, Ld8/d;

    invoke-virtual {p0, p1, p2}, Ld6/f$a;->q(Lv8/e0;Ld8/d;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final n(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    invoke-static {}, Le8/b;->c()Ljava/lang/Object;

    move-result-object v0

    iget v1, p0, Ld6/f$a;->e:I

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    if-ne v1, v2, :cond_0

    invoke-static {p1}, La8/n;->b(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    invoke-static {p1}, La8/n;->b(Ljava/lang/Object;)V

    iget-object p1, p0, Ld6/f$a;->f:Ld6/f;

    invoke-static {p1}, Ld6/f;->O1(Ld6/f;)V

    const-wide/16 v3, 0x12c

    iput v2, p0, Ld6/f$a;->e:I

    invoke-static {v3, v4, p0}, Lv8/o0;->a(JLd8/d;)Ljava/lang/Object;

    move-result-object p1

    if-ne p1, v0, :cond_2

    return-object v0

    :cond_2
    :goto_0
    iget-object p1, p0, Ld6/f$a;->f:Ld6/f;

    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->p()Landroidx/fragment/app/e;

    move-result-object p1

    if-eqz p1, :cond_3

    invoke-virtual {p1}, Landroidx/fragment/app/e;->X()Landroidx/fragment/app/m;

    move-result-object p1

    if-eqz p1, :cond_3

    invoke-virtual {p1}, Landroidx/fragment/app/m;->l()Landroidx/fragment/app/v;

    move-result-object p1

    if-eqz p1, :cond_3

    iget-object v0, p0, Ld6/f$a;->f:Ld6/f;

    invoke-virtual {p1, v0}, Landroidx/fragment/app/v;->m(Landroidx/fragment/app/Fragment;)Landroidx/fragment/app/v;

    move-result-object p1

    if-eqz p1, :cond_3

    invoke-virtual {p1}, Landroidx/fragment/app/v;->h()I

    move-result p1

    invoke-static {p1}, Lf8/b;->b(I)Ljava/lang/Integer;

    :cond_3
    sget-object p1, La8/v;->a:La8/v;

    return-object p1
.end method

.method public final q(Lv8/e0;Ld8/d;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lv8/e0;",
            "Ld8/d<",
            "-",
            "La8/v;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Ld6/f$a;->b(Ljava/lang/Object;Ld8/d;)Ld8/d;

    move-result-object p1

    check-cast p1, Ld6/f$a;

    sget-object p2, La8/v;->a:La8/v;

    invoke-virtual {p1, p2}, Ld6/f$a;->n(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method
