.class final Ld6/f$c$a;
.super Lf8/k;

# interfaces
.implements Ll8/p;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Ld6/f$c;->n(Ljava/lang/Object;)Ljava/lang/Object;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lf8/k;",
        "Ll8/p<",
        "Lv8/e0;",
        "Ld8/d<",
        "-",
        "La8/v;",
        ">;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation runtime Lf8/f;
    c = "com.miui.packageInstaller.ui.secure.OpenSecurityModeFragment$onViewCreated$3$1"
    f = "OpenSecurityModeFragment.kt"
    l = {
        0x9f
    }
    m = "invokeSuspend"
.end annotation


# instance fields
.field e:I

.field final synthetic f:Ld6/f;


# direct methods
.method constructor <init>(Ld6/f;Ld8/d;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ld6/f;",
            "Ld8/d<",
            "-",
            "Ld6/f$c$a;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Ld6/f$c$a;->f:Ld6/f;

    const/4 p1, 0x2

    invoke-direct {p0, p1, p2}, Lf8/k;-><init>(ILd8/d;)V

    return-void
.end method


# virtual methods
.method public final b(Ljava/lang/Object;Ld8/d;)Ld8/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ld8/d<",
            "*>;)",
            "Ld8/d<",
            "La8/v;",
            ">;"
        }
    .end annotation

    new-instance p1, Ld6/f$c$a;

    iget-object v0, p0, Ld6/f$c$a;->f:Ld6/f;

    invoke-direct {p1, v0, p2}, Ld6/f$c$a;-><init>(Ld6/f;Ld8/d;)V

    return-object p1
.end method

.method public bridge synthetic g(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lv8/e0;

    check-cast p2, Ld8/d;

    invoke-virtual {p0, p1, p2}, Ld6/f$c$a;->q(Lv8/e0;Ld8/d;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final n(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    invoke-static {}, Le8/b;->c()Ljava/lang/Object;

    move-result-object v0

    iget v1, p0, Ld6/f$c$a;->e:I

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    if-ne v1, v2, :cond_0

    invoke-static {p1}, La8/n;->b(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    invoke-static {p1}, La8/n;->b(Ljava/lang/Object;)V

    new-instance p1, Lj6/b;

    iget-object v1, p0, Ld6/f$c$a;->f:Ld6/f;

    invoke-static {v1}, Ld6/f;->N1(Ld6/f;)Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v1

    if-nez v1, :cond_2

    const-string v1, "serviceListRecyclerView"

    invoke-static {v1}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v1, 0x0

    :cond_2
    invoke-direct {p1, v1}, Lj6/b;-><init>(Landroidx/recyclerview/widget/RecyclerView;)V

    iget-object v1, p0, Ld6/f$c$a;->f:Ld6/f;

    invoke-static {v1}, Ld6/f;->M1(Ld6/f;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {p1, v1}, Lj6/b;->j0(Ljava/util/List;)V

    iget-object p1, p0, Ld6/f$c$a;->f:Ld6/f;

    iput v2, p0, Ld6/f$c$a;->e:I

    invoke-virtual {p1, p0}, Ld6/f;->S1(Ld8/d;)Ljava/lang/Object;

    move-result-object p1

    if-ne p1, v0, :cond_3

    return-object v0

    :cond_3
    :goto_0
    sget-object p1, La8/v;->a:La8/v;

    return-object p1
.end method

.method public final q(Lv8/e0;Ld8/d;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lv8/e0;",
            "Ld8/d<",
            "-",
            "La8/v;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Ld6/f$c$a;->b(Ljava/lang/Object;Ld8/d;)Ld8/d;

    move-result-object p1

    check-cast p1, Ld6/f$c$a;

    sget-object p2, La8/v;->a:La8/v;

    invoke-virtual {p1, p2}, Ld6/f$c$a;->n(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method
