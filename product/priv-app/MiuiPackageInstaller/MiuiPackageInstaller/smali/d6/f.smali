.class public final Ld6/f;
.super Landroidx/fragment/app/Fragment;

# interfaces
.implements Lz5/a;


# instance fields
.field private e0:Landroid/view/ViewGroup;

.field private f0:Landroid/view/ViewGroup;

.field private g0:Landroidx/appcompat/widget/AppCompatImageView;

.field private final h0:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lm6/a<",
            "*>;>;"
        }
    .end annotation
.end field

.field private i0:Landroid/content/Context;

.field private j0:Landroidx/recyclerview/widget/RecyclerView;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Landroidx/fragment/app/Fragment;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Ld6/f;->h0:Ljava/util/ArrayList;

    return-void
.end method

.method public static synthetic H1(Landroid/graphics/drawable/Drawable;Ld6/f;Landroid/animation/ValueAnimator;)V
    .locals 0

    invoke-static {p0, p1, p2}, Ld6/f;->V1(Landroid/graphics/drawable/Drawable;Ld6/f;Landroid/animation/ValueAnimator;)V

    return-void
.end method

.method public static synthetic I1(Ld6/f;Landroid/view/View;)V
    .locals 0

    invoke-static {p0, p1}, Ld6/f;->R1(Ld6/f;Landroid/view/View;)V

    return-void
.end method

.method public static synthetic J1(Landroid/view/View;)V
    .locals 0

    invoke-static {p0}, Ld6/f;->Q1(Landroid/view/View;)V

    return-void
.end method

.method public static synthetic K1(Landroid/graphics/drawable/Drawable;Ld6/f;Landroid/animation/ValueAnimator;)V
    .locals 0

    invoke-static {p0, p1, p2}, Ld6/f;->T1(Landroid/graphics/drawable/Drawable;Ld6/f;Landroid/animation/ValueAnimator;)V

    return-void
.end method

.method public static final synthetic L1(Ld6/f;)Landroid/view/ViewGroup;
    .locals 0

    iget-object p0, p0, Ld6/f;->f0:Landroid/view/ViewGroup;

    return-object p0
.end method

.method public static final synthetic M1(Ld6/f;)Ljava/util/ArrayList;
    .locals 0

    iget-object p0, p0, Ld6/f;->h0:Ljava/util/ArrayList;

    return-object p0
.end method

.method public static final synthetic N1(Ld6/f;)Landroidx/recyclerview/widget/RecyclerView;
    .locals 0

    iget-object p0, p0, Ld6/f;->j0:Landroidx/recyclerview/widget/RecyclerView;

    return-object p0
.end method

.method public static final synthetic O1(Ld6/f;)V
    .locals 0

    invoke-direct {p0}, Ld6/f;->U1()V

    return-void
.end method

.method private static final Q1(Landroid/view/View;)V
    .locals 0

    return-void
.end method

.method private static final R1(Ld6/f;Landroid/view/View;)V
    .locals 3

    const-string p1, "this$0"

    invoke-static {p0, p1}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Lp5/b;

    iget-object v0, p0, Ld6/f;->i0:Landroid/content/Context;

    const-string v1, "null cannot be cast to non-null type com.miui.packageInstaller.analytics.IPage"

    invoke-static {v0, v1}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lo5/a;

    const-string v1, "safe_mode_guidance_popup_know_btn"

    const-string v2, "button"

    invoke-direct {p1, v1, v2, v0}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p1}, Lp5/f;->c()Z

    invoke-virtual {p0}, Ld6/f;->P1()V

    return-void
.end method

.method private static final T1(Landroid/graphics/drawable/Drawable;Ld6/f;Landroid/animation/ValueAnimator;)V
    .locals 2

    const-string v0, "this$0"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "animation"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/animation/ValueAnimator;->getAnimatedFraction()F

    move-result p2

    const/16 v0, 0xff

    int-to-float v0, v0

    mul-float/2addr v0, p2

    float-to-int v0, v0

    invoke-virtual {p0, v0}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    iget-object p0, p1, Ld6/f;->f0:Landroid/view/ViewGroup;

    const/4 v0, 0x0

    const-string v1, "mContentLayout"

    if-nez p0, :cond_0

    invoke-static {v1}, Lm8/i;->s(Ljava/lang/String;)V

    move-object p0, v0

    :cond_0
    iget-object p1, p1, Ld6/f;->f0:Landroid/view/ViewGroup;

    if-nez p1, :cond_1

    invoke-static {v1}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move-object v0, p1

    :goto_0
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getHeight()I

    move-result p1

    int-to-float p1, p1

    const/4 v0, 0x1

    int-to-float v0, v0

    sub-float/2addr v0, p2

    mul-float/2addr p1, v0

    invoke-virtual {p0, p1}, Landroid/view/ViewGroup;->setTranslationY(F)V

    return-void
.end method

.method private final U1()V
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    const-wide/16 v1, 0xfa

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    const-wide/16 v1, 0x32

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    iget-object v1, p0, Ld6/f;->e0:Landroid/view/ViewGroup;

    if-nez v1, :cond_0

    const-string v1, "mRootLayout"

    invoke-static {v1}, Lm8/i;->s(Ljava/lang/String;)V

    const/4 v1, 0x0

    :cond_0
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    new-instance v2, Ld6/b;

    invoke-direct {v2, v1, p0}, Ld6/b;-><init>(Landroid/graphics/drawable/Drawable;Ld6/f;)V

    invoke-virtual {v0, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    new-instance v1, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    return-void

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private static final V1(Landroid/graphics/drawable/Drawable;Ld6/f;Landroid/animation/ValueAnimator;)V
    .locals 2

    const-string v0, "this$0"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "animation"

    invoke-static {p2, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/animation/ValueAnimator;->getAnimatedFraction()F

    move-result p2

    const/16 v0, 0xff

    int-to-float v0, v0

    const/4 v1, 0x1

    int-to-float v1, v1

    sub-float/2addr v1, p2

    mul-float/2addr v0, v1

    float-to-int v0, v0

    invoke-virtual {p0, v0}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    iget-object p0, p1, Ld6/f;->f0:Landroid/view/ViewGroup;

    const/4 v0, 0x0

    const-string v1, "mContentLayout"

    if-nez p0, :cond_0

    invoke-static {v1}, Lm8/i;->s(Ljava/lang/String;)V

    move-object p0, v0

    :cond_0
    iget-object p1, p1, Ld6/f;->f0:Landroid/view/ViewGroup;

    if-nez p1, :cond_1

    invoke-static {v1}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move-object v0, p1

    :goto_0
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getHeight()I

    move-result p1

    int-to-float p1, p1

    mul-float/2addr p1, p2

    invoke-virtual {p0, p1}, Landroid/view/ViewGroup;->setTranslationY(F)V

    return-void
.end method


# virtual methods
.method public N0(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 9

    const-string v0, "view"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1, p2}, Landroidx/fragment/app/Fragment;->N0(Landroid/view/View;Landroid/os/Bundle;)V

    sget-object p2, Ld6/e;->a:Ld6/e;

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p2, 0x7f0a02d2

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string v0, "view.findViewById(R.id.root_layout)"

    invoke-static {p2, v0}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Landroid/view/ViewGroup;

    iput-object p2, p0, Ld6/f;->e0:Landroid/view/ViewGroup;

    const p2, 0x7f0a00ea

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string v0, "view.findViewById<ViewGroup>(R.id.content_layout)"

    invoke-static {p2, v0}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Landroid/view/ViewGroup;

    iput-object p2, p0, Ld6/f;->f0:Landroid/view/ViewGroup;

    const p2, 0x7f0a020b

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string v0, "view.findViewById(R.id.logo)"

    invoke-static {p2, v0}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Landroidx/appcompat/widget/AppCompatImageView;

    iput-object p2, p0, Ld6/f;->g0:Landroidx/appcompat/widget/AppCompatImageView;

    invoke-static {}, Lcom/android/packageinstaller/utils/g;->z()Z

    move-result p2

    const-string v0, "mIvLogo"

    const/4 v1, 0x0

    const/4 v2, 0x0

    if-eqz p2, :cond_1

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->N()Landroid/content/res/Resources;

    move-result-object p2

    invoke-virtual {p2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object p2

    iget p2, p2, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x2

    if-ne p2, v3, :cond_1

    iget-object p2, p0, Ld6/f;->g0:Landroidx/appcompat/widget/AppCompatImageView;

    if-nez p2, :cond_0

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object p2, v2

    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    :cond_1
    iget-object p2, p0, Ld6/f;->g0:Landroidx/appcompat/widget/AppCompatImageView;

    if-nez p2, :cond_2

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object p2, v2

    :cond_2
    invoke-virtual {p2, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_0
    iget-object p2, p0, Ld6/f;->f0:Landroid/view/ViewGroup;

    const-string v0, "mContentLayout"

    if-nez p2, :cond_3

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object p2, v2

    :cond_3
    const/4 v3, 0x1

    invoke-virtual {p2, v3}, Landroid/view/ViewGroup;->setClipToOutline(Z)V

    iget-object p2, p0, Ld6/f;->f0:Landroid/view/ViewGroup;

    if-nez p2, :cond_4

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object p2, v2

    :cond_4
    new-instance v3, Lcom/miui/packageInstaller/view/i;

    const v4, 0x7f0801aa

    invoke-direct {v3, v4}, Lcom/miui/packageInstaller/view/i;-><init>(I)V

    invoke-virtual {p2, v3}, Landroid/view/ViewGroup;->setBackground(Landroid/graphics/drawable/Drawable;)V

    iget-object p2, p0, Ld6/f;->f0:Landroid/view/ViewGroup;

    if-nez p2, :cond_5

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object p2, v2

    :cond_5
    new-instance v3, Ld6/f$b;

    invoke-direct {v3, p0}, Ld6/f$b;-><init>(Ld6/f;)V

    invoke-virtual {p2, v3}, Landroid/view/ViewGroup;->setOutlineProvider(Landroid/view/ViewOutlineProvider;)V

    iget-object p2, p0, Ld6/f;->f0:Landroid/view/ViewGroup;

    if-nez p2, :cond_6

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object p2, v2

    :cond_6
    const v3, 0x7f0a0307

    invoke-virtual {p2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string v3, "mContentLayout.findViewById(R.id.service_list)"

    invoke-static {p2, v3}, Lm8/i;->e(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Landroidx/recyclerview/widget/RecyclerView;

    iput-object p2, p0, Ld6/f;->j0:Landroidx/recyclerview/widget/RecyclerView;

    if-nez p2, :cond_7

    const-string p2, "serviceListRecyclerView"

    invoke-static {p2}, Lm8/i;->s(Ljava/lang/String;)V

    move-object p2, v2

    :cond_7
    new-instance v3, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    invoke-virtual {p2, v3}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$o;)V

    iget-object p2, p0, Ld6/f;->e0:Landroid/view/ViewGroup;

    if-nez p2, :cond_8

    const-string p2, "mRootLayout"

    invoke-static {p2}, Lm8/i;->s(Ljava/lang/String;)V

    move-object p2, v2

    :cond_8
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object p2

    invoke-virtual {p2, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    iget-object p2, p0, Ld6/f;->f0:Landroid/view/ViewGroup;

    if-nez p2, :cond_9

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    move-object p2, v2

    :cond_9
    const v1, 0x47c35000    # 100000.0f

    invoke-virtual {p2, v1}, Landroid/view/ViewGroup;->setTranslationY(F)V

    invoke-static {p0}, Landroidx/lifecycle/k;->a(Landroidx/lifecycle/j;)Landroidx/lifecycle/e;

    move-result-object v3

    invoke-static {}, Lv8/t0;->b()Lv8/a0;

    move-result-object v4

    const/4 v5, 0x0

    new-instance v6, Ld6/f$c;

    invoke-direct {v6, p0, p1, v2}, Ld6/f$c;-><init>(Ld6/f;Landroid/view/View;Ld8/d;)V

    const/4 v7, 0x2

    const/4 v8, 0x0

    invoke-static/range {v3 .. v8}, Lv8/f;->d(Lv8/e0;Ld8/g;Lv8/g0;Ll8/p;ILjava/lang/Object;)Lv8/k1;

    iget-object p1, p0, Ld6/f;->f0:Landroid/view/ViewGroup;

    if-nez p1, :cond_a

    invoke-static {v0}, Lm8/i;->s(Ljava/lang/String;)V

    goto :goto_1

    :cond_a
    move-object v2, p1

    :goto_1
    const p1, 0x7f0a0285

    invoke-virtual {v2, p1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p1

    new-instance p2, Ld6/d;

    invoke-direct {p2, p0}, Ld6/d;-><init>(Ld6/f;)V

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final P1()V
    .locals 6

    invoke-static {p0}, Landroidx/lifecycle/k;->a(Landroidx/lifecycle/j;)Landroidx/lifecycle/e;

    move-result-object v0

    invoke-static {}, Lv8/t0;->c()Lv8/v1;

    move-result-object v1

    new-instance v3, Ld6/f$a;

    const/4 v2, 0x0

    invoke-direct {v3, p0, v2}, Ld6/f$a;-><init>(Ld6/f;Ld8/d;)V

    const/4 v4, 0x2

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lv8/f;->d(Lv8/e0;Ld8/g;Lv8/g0;Ll8/p;ILjava/lang/Object;)Lv8/k1;

    return-void
.end method

.method public final S1(Ld8/d;)Ljava/lang/Object;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ld8/d<",
            "-",
            "La8/v;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    instance-of v0, p1, Ld6/f$d;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Ld6/f$d;

    iget v1, v0, Ld6/f$d;->i:I

    const/high16 v2, -0x80000000

    and-int v3, v1, v2

    if-eqz v3, :cond_0

    sub-int/2addr v1, v2

    iput v1, v0, Ld6/f$d;->i:I

    goto :goto_0

    :cond_0
    new-instance v0, Ld6/f$d;

    invoke-direct {v0, p0, p1}, Ld6/f$d;-><init>(Ld6/f;Ld8/d;)V

    :goto_0
    iget-object p1, v0, Ld6/f$d;->g:Ljava/lang/Object;

    invoke-static {}, Le8/b;->c()Ljava/lang/Object;

    move-result-object v1

    iget v2, v0, Ld6/f$d;->i:I

    const/4 v3, 0x0

    const-wide/16 v4, 0xfa

    const/4 v6, 0x2

    const/4 v7, 0x1

    if-eqz v2, :cond_3

    if-eq v2, v7, :cond_2

    if-ne v2, v6, :cond_1

    iget v2, v0, Ld6/f$d;->f:I

    iget-object v7, v0, Ld6/f$d;->e:Ljava/lang/Object;

    check-cast v7, Ljava/util/Iterator;

    iget-object v8, v0, Ld6/f$d;->d:Ljava/lang/Object;

    check-cast v8, Ld6/f;

    invoke-static {p1}, La8/n;->b(Ljava/lang/Object;)V

    move p1, v2

    goto/16 :goto_2

    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    iget-object v2, v0, Ld6/f$d;->d:Ljava/lang/Object;

    check-cast v2, Ld6/f;

    invoke-static {p1}, La8/n;->b(Ljava/lang/Object;)V

    goto :goto_1

    :cond_3
    invoke-static {p1}, La8/n;->b(Ljava/lang/Object;)V

    new-array p1, v6, [F

    fill-array-data p1, :array_0

    invoke-static {p1}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object p1

    invoke-virtual {p1, v4, v5}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    const-wide/16 v8, 0x32

    invoke-virtual {p1, v8, v9}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    iget-object v2, p0, Ld6/f;->e0:Landroid/view/ViewGroup;

    if-nez v2, :cond_4

    const-string v2, "mRootLayout"

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v2, v3

    :cond_4
    invoke-virtual {v2}, Landroid/view/ViewGroup;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    new-instance v8, Ld6/c;

    invoke-direct {v8, v2, p0}, Ld6/c;-><init>(Landroid/graphics/drawable/Drawable;Ld6/f;)V

    invoke-virtual {p1, v8}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    new-instance v2, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v2}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {p1, v2}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->start()V

    const-wide/16 v8, 0x12c

    iput-object p0, v0, Ld6/f$d;->d:Ljava/lang/Object;

    iput v7, v0, Ld6/f$d;->i:I

    invoke-static {v8, v9, v0}, Lv8/o0;->a(JLd8/d;)Ljava/lang/Object;

    move-result-object p1

    if-ne p1, v1, :cond_5

    return-object v1

    :cond_5
    move-object v2, p0

    :goto_1
    new-instance p1, Lp5/g;

    iget-object v7, v2, Ld6/f;->i0:Landroid/content/Context;

    const-string v8, "null cannot be cast to non-null type com.miui.packageInstaller.analytics.IPage"

    invoke-static {v7, v8}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v7, Lo5/a;

    const-string v9, "safe_mode_guidance_popup"

    const-string v10, "popup"

    invoke-direct {p1, v9, v10, v7}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p1}, Lp5/f;->c()Z

    new-instance p1, Lp5/g;

    iget-object v7, v2, Ld6/f;->i0:Landroid/content/Context;

    invoke-static {v7, v8}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v7, Lo5/a;

    const-string v8, "safe_mode_guidance_popup_know_btn"

    const-string v9, "button"

    invoke-direct {p1, v8, v9, v7}, Lp5/g;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {p1}, Lp5/f;->c()Z

    iget-object p1, v2, Ld6/f;->h0:Ljava/util/ArrayList;

    invoke-static {p1}, Lb8/j;->t(Ljava/lang/Iterable;)Lt8/f;

    move-result-object p1

    sget-object v7, Ld6/f$e;->b:Ld6/f$e;

    invoke-static {p1, v7}, Lt8/g;->j(Lt8/f;Ll8/l;)Lt8/f;

    move-result-object p1

    const/4 v7, 0x0

    invoke-interface {p1}, Lt8/f;->iterator()Ljava/util/Iterator;

    move-result-object p1

    move-object v8, v2

    move v11, v7

    move-object v7, p1

    move p1, v11

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    add-int/lit8 v9, p1, 0x1

    if-gez p1, :cond_6

    invoke-static {}, Lb8/j;->m()V

    :cond_6
    check-cast v2, Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceAnimView2Object;

    invoke-virtual {v2}, Lcom/miui/packageInstaller/ui/listcomponets/SecurityModeServiceAnimView2Object;->z()V

    iget-object v2, v8, Ld6/f;->j0:Landroidx/recyclerview/widget/RecyclerView;

    if-nez v2, :cond_7

    const-string v2, "serviceListRecyclerView"

    invoke-static {v2}, Lm8/i;->s(Ljava/lang/String;)V

    move-object v2, v3

    :cond_7
    invoke-virtual {v2, p1}, Landroidx/recyclerview/widget/RecyclerView;->q1(I)V

    iput-object v8, v0, Ld6/f$d;->d:Ljava/lang/Object;

    iput-object v7, v0, Ld6/f$d;->e:Ljava/lang/Object;

    iput v9, v0, Ld6/f$d;->f:I

    iput v6, v0, Ld6/f$d;->i:I

    invoke-static {v4, v5, v0}, Lv8/o0;->a(JLd8/d;)Ljava/lang/Object;

    move-result-object p1

    if-ne p1, v1, :cond_8

    return-object v1

    :cond_8
    move p1, v9

    goto :goto_2

    :cond_9
    sget-object p1, La8/v;->a:La8/v;

    return-object p1

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public b()Z
    .locals 4

    invoke-virtual {p0}, Ld6/f;->P1()V

    new-instance v0, Lp5/b;

    iget-object v1, p0, Ld6/f;->i0:Landroid/content/Context;

    const-string v2, "null cannot be cast to non-null type com.miui.packageInstaller.analytics.IPage"

    invoke-static {v1, v2}, Lm8/i;->d(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lo5/a;

    const-string v2, "safe_mode_guidance_popup_back_btn"

    const-string v3, "button"

    invoke-direct {v0, v2, v3, v1}, Lp5/b;-><init>(Ljava/lang/String;Ljava/lang/String;Lo5/a;)V

    invoke-virtual {v0}, Lp5/f;->c()Z

    const/4 v0, 0x1

    return v0
.end method

.method public l0(Landroid/content/Context;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->l0(Landroid/content/Context;)V

    iput-object p1, p0, Ld6/f;->i0:Landroid/content/Context;

    instance-of v0, p1, Le6/a;

    if-eqz v0, :cond_0

    check-cast p1, Le6/a;

    invoke-interface {p1, p0}, Le6/a;->h(Lz5/a;)V

    :cond_0
    return-void
.end method

.method public o0(Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->o0(Landroid/os/Bundle;)V

    return-void
.end method

.method public s0(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    const-string p2, "inflater"

    invoke-static {p1, p2}, Lm8/i;->f(Ljava/lang/Object;Ljava/lang/String;)V

    const p2, 0x7f0d0057

    const/4 p3, 0x0

    const/4 v0, 0x0

    invoke-virtual {p1, p2, p3, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public w0()V
    .locals 2

    invoke-super {p0}, Landroidx/fragment/app/Fragment;->w0()V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->p()Landroidx/fragment/app/e;

    move-result-object v0

    if-eqz v0, :cond_0

    instance-of v1, v0, Le6/a;

    if-eqz v1, :cond_0

    check-cast v0, Le6/a;

    invoke-interface {v0, p0}, Le6/a;->M(Lz5/a;)V

    :cond_0
    return-void
.end method
