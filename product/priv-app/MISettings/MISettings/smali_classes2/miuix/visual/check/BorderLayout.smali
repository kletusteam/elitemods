.class public Lmiuix/visual/check/BorderLayout;
.super Landroid/widget/LinearLayout;

# interfaces
.implements Lmiuix/visual/check/c;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiuix/visual/check/BorderLayout$DrawableTarget;
    }
.end annotation


# instance fields
.field private a:Lmiuix/animation/h;

.field private b:Landroid/graphics/drawable/Drawable;

.field private mDrawableTarget:Lmiuix/visual/check/BorderLayout$DrawableTarget;
    .annotation build Landroidx/annotation/Keep;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x1

    new-array v0, v0, [Landroid/view/View;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    invoke-static {v0}, Lmiuix/animation/d;->a([Landroid/view/View;)Lmiuix/animation/h;

    move-result-object v0

    iput-object v0, p0, Lmiuix/visual/check/BorderLayout;->a:Lmiuix/animation/h;

    sget-object v0, Ld/w/c;->BorderLayout:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p1

    sget p2, Ld/w/c;->BorderLayout_checkedBackGround:I

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p2

    iput-object p2, p0, Lmiuix/visual/check/BorderLayout;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    iget-object p1, p0, Lmiuix/visual/check/BorderLayout;->b:Landroid/graphics/drawable/Drawable;

    if-nez p1, :cond_0

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget p2, Ld/w/b;->borderlayout_bg:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    iput-object p1, p0, Lmiuix/visual/check/BorderLayout;->b:Landroid/graphics/drawable/Drawable;

    :cond_0
    new-instance p1, Lmiuix/visual/check/BorderLayout$DrawableTarget;

    iget-object p2, p0, Lmiuix/visual/check/BorderLayout;->b:Landroid/graphics/drawable/Drawable;

    invoke-direct {p1, p2}, Lmiuix/visual/check/BorderLayout$DrawableTarget;-><init>(Landroid/graphics/drawable/Drawable;)V

    iput-object p1, p0, Lmiuix/visual/check/BorderLayout;->mDrawableTarget:Lmiuix/visual/check/BorderLayout$DrawableTarget;

    iget-object p1, p0, Lmiuix/visual/check/BorderLayout;->a:Lmiuix/animation/h;

    invoke-interface {p1}, Lmiuix/animation/h;->c()Lmiuix/animation/i;

    move-result-object p1

    sget-object p2, Lmiuix/animation/i$a;->b:Lmiuix/animation/i$a;

    invoke-interface {p1, p2}, Lmiuix/animation/i;->a(Lmiuix/animation/i$a;)Lmiuix/animation/i;

    new-array p2, v1, [Lmiuix/animation/a/a;

    invoke-interface {p1, p0, p2}, Lmiuix/animation/i;->b(Landroid/view/View;[Lmiuix/animation/a/a;)V

    return-void
.end method


# virtual methods
.method public a(Lmiuix/visual/check/VisualCheckBox;Landroid/view/MotionEvent;)V
    .locals 4

    iget-object p1, p0, Lmiuix/visual/check/BorderLayout;->a:Lmiuix/animation/h;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Lmiuix/animation/h;->d()Lmiuix/animation/m;

    move-result-object p1

    invoke-interface {p1, p2}, Lmiuix/animation/m;->a(Landroid/view/MotionEvent;)V

    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result p1

    const/4 p2, 0x1

    if-ne p1, p2, :cond_1

    iget-object p1, p0, Lmiuix/visual/check/BorderLayout;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getAlpha()I

    move-result p1

    const/16 v0, 0xff

    if-eq p1, v0, :cond_1

    iget-object p1, p0, Lmiuix/visual/check/BorderLayout;->mDrawableTarget:Lmiuix/visual/check/BorderLayout$DrawableTarget;

    if-eqz p1, :cond_1

    new-array v0, p2, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-static {v0}, Lmiuix/animation/d;->c([Ljava/lang/Object;)Lmiuix/animation/k;

    move-result-object p1

    const-wide/16 v2, 0x1

    invoke-interface {p1, v2, v3}, Lmiuix/animation/k;->a(J)Lmiuix/animation/k;

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const-string v2, "alpha"

    aput-object v2, v0, v1

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    aput-object v1, v0, p2

    const/4 p2, -0x2

    const/4 v1, 0x2

    new-array v2, v1, [F

    fill-array-data v2, :array_0

    invoke-static {p2, v2}, Lmiuix/animation/h/c;->c(I[F)Lmiuix/animation/h/c$a;

    move-result-object p2

    aput-object p2, v0, v1

    invoke-interface {p1, v0}, Lmiuix/animation/k;->c([Ljava/lang/Object;)Lmiuix/animation/k;

    :cond_1
    return-void

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x3e800000    # 0.25f
    .end array-data
.end method

.method public a(Z)V
    .locals 9

    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_1

    iget-object v0, p0, Lmiuix/visual/check/BorderLayout;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lmiuix/visual/check/BorderLayout;->b:Landroid/graphics/drawable/Drawable;

    if-eqz p1, :cond_0

    const/16 v1, 0xff

    :cond_0
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    goto :goto_0

    :cond_1
    const/4 v0, -0x2

    const-string v2, "alpha"

    const/4 v3, 0x3

    const-wide/16 v4, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x1

    if-nez p1, :cond_2

    new-array p1, v7, [Ljava/lang/Object;

    iget-object v8, p0, Lmiuix/visual/check/BorderLayout;->mDrawableTarget:Lmiuix/visual/check/BorderLayout$DrawableTarget;

    aput-object v8, p1, v1

    invoke-static {p1}, Lmiuix/animation/d;->c([Ljava/lang/Object;)Lmiuix/animation/k;

    move-result-object p1

    invoke-interface {p1, v4, v5}, Lmiuix/animation/k;->a(J)Lmiuix/animation/k;

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v2, v3, v1

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    aput-object v1, v3, v7

    new-array v1, v6, [F

    fill-array-data v1, :array_0

    invoke-static {v0, v1}, Lmiuix/animation/h/c;->c(I[F)Lmiuix/animation/h/c$a;

    move-result-object v0

    aput-object v0, v3, v6

    invoke-interface {p1, v3}, Lmiuix/animation/k;->c([Ljava/lang/Object;)Lmiuix/animation/k;

    goto :goto_0

    :cond_2
    new-array p1, v7, [Ljava/lang/Object;

    iget-object v8, p0, Lmiuix/visual/check/BorderLayout;->mDrawableTarget:Lmiuix/visual/check/BorderLayout$DrawableTarget;

    aput-object v8, p1, v1

    invoke-static {p1}, Lmiuix/animation/d;->c([Ljava/lang/Object;)Lmiuix/animation/k;

    move-result-object p1

    invoke-interface {p1, v4, v5}, Lmiuix/animation/k;->a(J)Lmiuix/animation/k;

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v2, v3, v1

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    aput-object v1, v3, v7

    new-array v1, v6, [F

    fill-array-data v1, :array_1

    invoke-static {v0, v1}, Lmiuix/animation/h/c;->c(I[F)Lmiuix/animation/h/c$a;

    move-result-object v0

    aput-object v0, v3, v6

    invoke-interface {p1, v3}, Lmiuix/animation/k;->c([Ljava/lang/Object;)Lmiuix/animation/k;

    :goto_0
    return-void

    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x3e800000    # 0.25f
    .end array-data

    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x3e800000    # 0.25f
    .end array-data
.end method
