.class public Lmiuix/stretchablewidget/StretchableDatePicker;
.super Lmiuix/stretchablewidget/StretchableWidget;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiuix/stretchablewidget/StretchableDatePicker$a;
    }
.end annotation


# instance fields
.field private A:Z

.field private B:Z

.field private C:I

.field private D:J

.field private E:Lmiuix/stretchablewidget/StretchableDatePicker$a;

.field private r:Lmiuix/pickerwidget/widget/DateTimePicker;

.field private s:Lmiuix/slidingwidget/widget/SlidingButton;

.field private t:Landroid/widget/LinearLayout;

.field private u:Landroid/widget/RelativeLayout;

.field private v:Ld/l/a/a;

.field private w:Lmiuix/pickerwidget/widget/DateTimePicker$b;

.field private x:Landroid/widget/TextView;

.field private y:Ljava/lang/String;

.field private z:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lmiuix/stretchablewidget/StretchableDatePicker;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lmiuix/stretchablewidget/StretchableDatePicker;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0, p1, p2, p3}, Lmiuix/stretchablewidget/StretchableWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 p1, 0x1

    iput p1, p0, Lmiuix/stretchablewidget/StretchableDatePicker;->z:I

    return-void
.end method

.method static synthetic a(Lmiuix/stretchablewidget/StretchableDatePicker;J)J
    .locals 0

    iput-wide p1, p0, Lmiuix/stretchablewidget/StretchableDatePicker;->D:J

    return-wide p1
.end method

.method private a(JLandroid/content/Context;)Ljava/lang/String;
    .locals 5

    iget-object v0, p0, Lmiuix/stretchablewidget/StretchableDatePicker;->w:Lmiuix/pickerwidget/widget/DateTimePicker$b;

    iget-object v1, p0, Lmiuix/stretchablewidget/StretchableDatePicker;->v:Ld/l/a/a;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ld/l/a/a;->b(I)I

    move-result v1

    iget-object v2, p0, Lmiuix/stretchablewidget/StretchableDatePicker;->v:Ld/l/a/a;

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Ld/l/a/a;->b(I)I

    move-result v2

    iget-object v3, p0, Lmiuix/stretchablewidget/StretchableDatePicker;->v:Ld/l/a/a;

    const/16 v4, 0x9

    invoke-virtual {v3, v4}, Ld/l/a/a;->b(I)I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lmiuix/pickerwidget/widget/DateTimePicker$b;->a(III)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0xc

    invoke-static {p3, p1, p2, v1}, Ld/l/a/e;->a(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object p1

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p3, " "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method static synthetic a(Lmiuix/stretchablewidget/StretchableDatePicker;)Lmiuix/pickerwidget/widget/DateTimePicker;
    .locals 0

    iget-object p0, p0, Lmiuix/stretchablewidget/StretchableDatePicker;->r:Lmiuix/pickerwidget/widget/DateTimePicker;

    return-object p0
.end method

.method static synthetic a(Lmiuix/stretchablewidget/StretchableDatePicker;ZLandroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lmiuix/stretchablewidget/StretchableDatePicker;->a(ZLandroid/content/Context;)V

    return-void
.end method

.method private a(ZLandroid/content/Context;)V
    .locals 0

    if-eqz p1, :cond_0

    invoke-virtual {p0, p2}, Lmiuix/stretchablewidget/StretchableDatePicker;->a(Landroid/content/Context;)V

    goto :goto_0

    :cond_0
    invoke-direct {p0, p2}, Lmiuix/stretchablewidget/StretchableDatePicker;->b(Landroid/content/Context;)V

    :goto_0
    return-void
.end method

.method static synthetic a(Lmiuix/stretchablewidget/StretchableDatePicker;Z)Z
    .locals 0

    iput-boolean p1, p0, Lmiuix/stretchablewidget/StretchableDatePicker;->B:Z

    return p1
.end method

.method private b(JLandroid/content/Context;)Ljava/lang/String;
    .locals 1

    const/16 v0, 0x38c

    invoke-static {p3, p1, p2, v0}, Ld/l/a/e;->a(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private b(Landroid/content/Context;)V
    .locals 2

    iget-object v0, p0, Lmiuix/stretchablewidget/StretchableDatePicker;->v:Ld/l/a/a;

    invoke-virtual {v0}, Ld/l/a/a;->b()J

    move-result-wide v0

    invoke-direct {p0, v0, v1, p1}, Lmiuix/stretchablewidget/StretchableDatePicker;->b(JLandroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lmiuix/stretchablewidget/StretchableWidget;->setDetailMessage(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private b(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    sget-object v0, Lmiuix/stretchablewidget/e;->StretchableDatePicker:[I

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p2

    sget p3, Lmiuix/stretchablewidget/e;->StretchableDatePicker_show_lunar:I

    invoke-virtual {p2, p3, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p3

    iput-boolean p3, p0, Lmiuix/stretchablewidget/StretchableDatePicker;->A:Z

    sget p3, Lmiuix/stretchablewidget/e;->StretchableDatePicker_lunar_text:I

    invoke-virtual {p2, p3}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object p3

    iput-object p3, p0, Lmiuix/stretchablewidget/StretchableDatePicker;->y:Ljava/lang/String;

    sget p3, Lmiuix/stretchablewidget/e;->StretchableDatePicker_minuteInterval:I

    const/4 v0, 0x1

    invoke-virtual {p2, p3, v0}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result p3

    iput p3, p0, Lmiuix/stretchablewidget/StretchableDatePicker;->z:I

    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    const-string p2, "layout_inflater"

    invoke-virtual {p1, p2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/view/LayoutInflater;

    sget p3, Lmiuix/stretchablewidget/d;->miuix_stretchable_widget_picker_part:I

    const/4 v0, 0x0

    invoke-virtual {p2, p3, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/LinearLayout;

    iput-object p2, p0, Lmiuix/stretchablewidget/StretchableDatePicker;->t:Landroid/widget/LinearLayout;

    iget-object p2, p0, Lmiuix/stretchablewidget/StretchableDatePicker;->t:Landroid/widget/LinearLayout;

    sget p3, Lmiuix/stretchablewidget/c;->datetime_picker:I

    invoke-virtual {p2, p3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lmiuix/pickerwidget/widget/DateTimePicker;

    iput-object p2, p0, Lmiuix/stretchablewidget/StretchableDatePicker;->r:Lmiuix/pickerwidget/widget/DateTimePicker;

    iget-object p2, p0, Lmiuix/stretchablewidget/StretchableDatePicker;->t:Landroid/widget/LinearLayout;

    sget p3, Lmiuix/stretchablewidget/c;->lunar_layout:I

    invoke-virtual {p2, p3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/RelativeLayout;

    iput-object p2, p0, Lmiuix/stretchablewidget/StretchableDatePicker;->u:Landroid/widget/RelativeLayout;

    iget-object p2, p0, Lmiuix/stretchablewidget/StretchableDatePicker;->t:Landroid/widget/LinearLayout;

    sget p3, Lmiuix/stretchablewidget/c;->lunar_text:I

    invoke-virtual {p2, p3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    iput-object p2, p0, Lmiuix/stretchablewidget/StretchableDatePicker;->x:Landroid/widget/TextView;

    iget-object p2, p0, Lmiuix/stretchablewidget/StretchableDatePicker;->t:Landroid/widget/LinearLayout;

    sget p3, Lmiuix/stretchablewidget/c;->lunar_button:I

    invoke-virtual {p2, p3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lmiuix/slidingwidget/widget/SlidingButton;

    iput-object p2, p0, Lmiuix/stretchablewidget/StretchableDatePicker;->s:Lmiuix/slidingwidget/widget/SlidingButton;

    iget-boolean p2, p0, Lmiuix/stretchablewidget/StretchableDatePicker;->A:Z

    if-nez p2, :cond_0

    iget-object p2, p0, Lmiuix/stretchablewidget/StretchableDatePicker;->u:Landroid/widget/RelativeLayout;

    const/16 p3, 0x8

    invoke-virtual {p2, p3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    :cond_0
    iget-object p2, p0, Lmiuix/stretchablewidget/StretchableDatePicker;->s:Lmiuix/slidingwidget/widget/SlidingButton;

    new-instance p3, Lmiuix/stretchablewidget/f;

    invoke-direct {p3, p0, p1}, Lmiuix/stretchablewidget/f;-><init>(Lmiuix/stretchablewidget/StretchableDatePicker;Landroid/content/Context;)V

    invoke-virtual {p2, p3}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object p2, p0, Lmiuix/stretchablewidget/StretchableDatePicker;->t:Landroid/widget/LinearLayout;

    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p3

    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {p2, p3, v0}, Landroid/widget/LinearLayout;->measure(II)V

    iget-object p2, p0, Lmiuix/stretchablewidget/StretchableDatePicker;->t:Landroid/widget/LinearLayout;

    invoke-virtual {p2}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result p2

    iput p2, p0, Lmiuix/stretchablewidget/StretchableDatePicker;->C:I

    iget-object p2, p0, Lmiuix/stretchablewidget/StretchableDatePicker;->t:Landroid/widget/LinearLayout;

    invoke-virtual {p0, p2}, Lmiuix/stretchablewidget/StretchableWidget;->setLayout(Landroid/view/View;)V

    new-instance p2, Ld/l/a/a;

    invoke-direct {p2}, Ld/l/a/a;-><init>()V

    iput-object p2, p0, Lmiuix/stretchablewidget/StretchableDatePicker;->v:Ld/l/a/a;

    iget-object p2, p0, Lmiuix/stretchablewidget/StretchableDatePicker;->y:Ljava/lang/String;

    invoke-virtual {p0, p2}, Lmiuix/stretchablewidget/StretchableDatePicker;->setLunarText(Ljava/lang/String;)V

    new-instance p2, Lmiuix/pickerwidget/widget/DateTimePicker$b;

    invoke-direct {p2, p1}, Lmiuix/pickerwidget/widget/DateTimePicker$b;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lmiuix/stretchablewidget/StretchableDatePicker;->w:Lmiuix/pickerwidget/widget/DateTimePicker$b;

    iget p2, p0, Lmiuix/stretchablewidget/StretchableDatePicker;->z:I

    invoke-virtual {p0, p2}, Lmiuix/stretchablewidget/StretchableDatePicker;->setMinuteInterval(I)V

    invoke-direct {p0, p1}, Lmiuix/stretchablewidget/StretchableDatePicker;->b(Landroid/content/Context;)V

    iget-object p2, p0, Lmiuix/stretchablewidget/StretchableDatePicker;->v:Ld/l/a/a;

    invoke-virtual {p2}, Ld/l/a/a;->b()J

    move-result-wide p2

    iput-wide p2, p0, Lmiuix/stretchablewidget/StretchableDatePicker;->D:J

    iget-object p2, p0, Lmiuix/stretchablewidget/StretchableDatePicker;->r:Lmiuix/pickerwidget/widget/DateTimePicker;

    new-instance p3, Lmiuix/stretchablewidget/g;

    invoke-direct {p3, p0, p1}, Lmiuix/stretchablewidget/g;-><init>(Lmiuix/stretchablewidget/StretchableDatePicker;Landroid/content/Context;)V

    invoke-virtual {p2, p3}, Lmiuix/pickerwidget/widget/DateTimePicker;->setOnTimeChangedListener(Lmiuix/pickerwidget/widget/DateTimePicker$c;)V

    return-void
.end method

.method static synthetic b(Lmiuix/stretchablewidget/StretchableDatePicker;)Z
    .locals 0

    iget-boolean p0, p0, Lmiuix/stretchablewidget/StretchableDatePicker;->B:Z

    return p0
.end method

.method static synthetic c(Lmiuix/stretchablewidget/StretchableDatePicker;)Ld/l/a/a;
    .locals 0

    iget-object p0, p0, Lmiuix/stretchablewidget/StretchableDatePicker;->v:Ld/l/a/a;

    return-object p0
.end method

.method static synthetic d(Lmiuix/stretchablewidget/StretchableDatePicker;)Lmiuix/stretchablewidget/StretchableDatePicker$a;
    .locals 0

    iget-object p0, p0, Lmiuix/stretchablewidget/StretchableDatePicker;->E:Lmiuix/stretchablewidget/StretchableDatePicker$a;

    return-object p0
.end method


# virtual methods
.method protected a()V
    .locals 1

    iget v0, p0, Lmiuix/stretchablewidget/StretchableDatePicker;->C:I

    iput v0, p0, Lmiuix/stretchablewidget/StretchableWidget;->q:I

    return-void
.end method

.method public a(Landroid/content/Context;)V
    .locals 2

    iget-object v0, p0, Lmiuix/stretchablewidget/StretchableDatePicker;->v:Ld/l/a/a;

    invoke-virtual {v0}, Ld/l/a/a;->b()J

    move-result-wide v0

    invoke-direct {p0, v0, v1, p1}, Lmiuix/stretchablewidget/StretchableDatePicker;->a(JLandroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lmiuix/stretchablewidget/StretchableWidget;->setDetailMessage(Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected a(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lmiuix/stretchablewidget/StretchableDatePicker;->b(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public getTime()J
    .locals 2

    iget-wide v0, p0, Lmiuix/stretchablewidget/StretchableDatePicker;->D:J

    return-wide v0
.end method

.method public setLunarModeOn(Z)V
    .locals 1

    iget-object v0, p0, Lmiuix/stretchablewidget/StretchableDatePicker;->s:Lmiuix/slidingwidget/widget/SlidingButton;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lmiuix/slidingwidget/widget/SlidingButton;->setChecked(Z)V

    :cond_0
    return-void
.end method

.method public setLunarText(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lmiuix/stretchablewidget/StretchableDatePicker;->x:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setMinuteInterval(I)V
    .locals 1

    iget-object v0, p0, Lmiuix/stretchablewidget/StretchableDatePicker;->r:Lmiuix/pickerwidget/widget/DateTimePicker;

    invoke-virtual {v0, p1}, Lmiuix/pickerwidget/widget/DateTimePicker;->setMinuteInterval(I)V

    return-void
.end method

.method public setOnTimeChangeListener(Lmiuix/stretchablewidget/StretchableDatePicker$a;)V
    .locals 0

    iput-object p1, p0, Lmiuix/stretchablewidget/StretchableDatePicker;->E:Lmiuix/stretchablewidget/StretchableDatePicker$a;

    return-void
.end method
