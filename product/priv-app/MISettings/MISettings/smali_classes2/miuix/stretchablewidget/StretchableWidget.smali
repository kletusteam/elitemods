.class public Lmiuix/stretchablewidget/StretchableWidget;
.super Landroid/widget/LinearLayout;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmiuix/stretchablewidget/StretchableWidget$a;
    }
.end annotation


# instance fields
.field private a:Landroid/widget/RelativeLayout;

.field private b:Landroid/widget/TextView;

.field protected c:Landroid/widget/TextView;

.field private d:Landroid/widget/ImageView;

.field private e:Landroid/widget/ImageView;

.field private f:Lmiuix/stretchablewidget/WidgetContainer;

.field private g:Landroid/view/View;

.field private h:Landroid/view/View;

.field private i:Z

.field private j:Landroid/content/Context;

.field private k:Ljava/lang/String;

.field private l:I

.field private m:I

.field private n:Landroid/view/View;

.field private o:Ljava/lang/String;

.field private p:Lmiuix/stretchablewidget/StretchableWidget$a;

.field protected q:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lmiuix/stretchablewidget/StretchableWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    sget v0, Lmiuix/stretchablewidget/a;->stretchableWidgetStyle:I

    invoke-direct {p0, p1, p2, v0}, Lmiuix/stretchablewidget/StretchableWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, 0x0

    iput v0, p0, Lmiuix/stretchablewidget/StretchableWidget;->q:I

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    iput-object p1, p0, Lmiuix/stretchablewidget/StretchableWidget;->j:Landroid/content/Context;

    sget-object v1, Lmiuix/stretchablewidget/e;->StretchableWidget:[I

    invoke-virtual {p1, p2, v1, p3, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    sget v2, Lmiuix/stretchablewidget/e;->StretchableWidget_title:I

    invoke-virtual {v1, v2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lmiuix/stretchablewidget/StretchableWidget;->k:Ljava/lang/String;

    sget v2, Lmiuix/stretchablewidget/e;->StretchableWidget_icon:I

    invoke-virtual {v1, v2, v0}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    iput v2, p0, Lmiuix/stretchablewidget/StretchableWidget;->l:I

    sget v2, Lmiuix/stretchablewidget/e;->StretchableWidget_layout:I

    invoke-virtual {v1, v2, v0}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    iput v2, p0, Lmiuix/stretchablewidget/StretchableWidget;->m:I

    sget v2, Lmiuix/stretchablewidget/e;->StretchableWidget_detail_message:I

    invoke-virtual {v1, v2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lmiuix/stretchablewidget/StretchableWidget;->o:Ljava/lang/String;

    sget v2, Lmiuix/stretchablewidget/e;->StretchableWidget_expand_state:I

    invoke-virtual {v1, v2, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lmiuix/stretchablewidget/StretchableWidget;->i:Z

    invoke-direct {p0, p1, p2, p3}, Lmiuix/stretchablewidget/StretchableWidget;->b(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method static synthetic a(Lmiuix/stretchablewidget/StretchableWidget;)V
    .locals 0

    invoke-direct {p0}, Lmiuix/stretchablewidget/StretchableWidget;->b()V

    return-void
.end method

.method private b(I)Landroid/view/View;
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return-object v0

    :cond_0
    iget-object v1, p0, Lmiuix/stretchablewidget/StretchableWidget;->j:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    invoke-virtual {v1, p1, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method private b()V
    .locals 6

    iget-boolean v0, p0, Lmiuix/stretchablewidget/StretchableWidget;->i:Z

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    iput-boolean v0, p0, Lmiuix/stretchablewidget/StretchableWidget;->i:Z

    new-instance v0, Lmiuix/animation/a/c;

    invoke-direct {v0}, Lmiuix/animation/a/c;-><init>()V

    const/4 v2, -0x2

    const/4 v3, 0x2

    new-array v3, v3, [F

    fill-array-data v3, :array_0

    invoke-virtual {v0, v2, v3}, Lmiuix/animation/a/a;->a(I[F)Lmiuix/animation/a/a;

    check-cast v0, Lmiuix/animation/a/c;

    iget-boolean v2, p0, Lmiuix/stretchablewidget/StretchableWidget;->i:Z

    const/4 v3, 0x0

    const/4 v4, 0x0

    if-eqz v2, :cond_0

    new-array v2, v1, [Ljava/lang/Object;

    iget-object v5, p0, Lmiuix/stretchablewidget/StretchableWidget;->f:Lmiuix/stretchablewidget/WidgetContainer;

    aput-object v5, v2, v4

    invoke-static {v2}, Lmiuix/animation/d;->c([Ljava/lang/Object;)Lmiuix/animation/k;

    move-result-object v2

    new-array v1, v1, [Lmiuix/animation/a/a;

    new-instance v5, Lmiuix/animation/a/a;

    invoke-direct {v5}, Lmiuix/animation/a/a;-><init>()V

    invoke-virtual {v5, v3}, Lmiuix/animation/a/a;->a(F)Lmiuix/animation/a/a;

    sget-object v3, Lmiuix/animation/g/A;->o:Lmiuix/animation/g/A;

    invoke-virtual {v5, v3, v0}, Lmiuix/animation/a/a;->a(Lmiuix/animation/g/b;Lmiuix/animation/a/c;)Lmiuix/animation/a/a;

    move-result-object v0

    aput-object v0, v1, v4

    const-string v0, "start"

    invoke-interface {v2, v0, v1}, Lmiuix/animation/k;->a(Ljava/lang/Object;[Lmiuix/animation/a/a;)Lmiuix/animation/k;

    iget-object v0, p0, Lmiuix/stretchablewidget/StretchableWidget;->e:Landroid/widget/ImageView;

    sget v1, Lmiuix/stretchablewidget/b;->miuix_stretchable_widget_state_expand:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v0, p0, Lmiuix/stretchablewidget/StretchableWidget;->h:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lmiuix/stretchablewidget/StretchableWidget;->g:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_0
    new-array v2, v1, [Ljava/lang/Object;

    iget-object v5, p0, Lmiuix/stretchablewidget/StretchableWidget;->f:Lmiuix/stretchablewidget/WidgetContainer;

    aput-object v5, v2, v4

    invoke-static {v2}, Lmiuix/animation/d;->c([Ljava/lang/Object;)Lmiuix/animation/k;

    move-result-object v2

    new-array v1, v1, [Lmiuix/animation/a/a;

    new-instance v5, Lmiuix/animation/a/a;

    invoke-direct {v5}, Lmiuix/animation/a/a;-><init>()V

    invoke-virtual {v5, v3}, Lmiuix/animation/a/a;->a(F)Lmiuix/animation/a/a;

    sget-object v3, Lmiuix/animation/g/A;->o:Lmiuix/animation/g/A;

    invoke-virtual {v5, v3, v0}, Lmiuix/animation/a/a;->a(Lmiuix/animation/g/b;Lmiuix/animation/a/c;)Lmiuix/animation/a/a;

    move-result-object v0

    aput-object v0, v1, v4

    const-string v0, "end"

    invoke-interface {v2, v0, v1}, Lmiuix/animation/k;->a(Ljava/lang/Object;[Lmiuix/animation/a/a;)Lmiuix/animation/k;

    iget-object v0, p0, Lmiuix/stretchablewidget/StretchableWidget;->e:Landroid/widget/ImageView;

    sget v1, Lmiuix/stretchablewidget/b;->miuix_stretchable_widget_state_collapse:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v0, p0, Lmiuix/stretchablewidget/StretchableWidget;->h:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lmiuix/stretchablewidget/StretchableWidget;->g:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    iget-object v0, p0, Lmiuix/stretchablewidget/StretchableWidget;->p:Lmiuix/stretchablewidget/StretchableWidget$a;

    if-eqz v0, :cond_1

    iget-boolean v1, p0, Lmiuix/stretchablewidget/StretchableWidget;->i:Z

    invoke-interface {v0, v1}, Lmiuix/stretchablewidget/StretchableWidget$a;->a(Z)V

    :cond_1
    return-void

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x3e4ccccd    # 0.2f
    .end array-data
.end method

.method private b(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/LayoutInflater;

    sget v0, Lmiuix/stretchablewidget/d;->miuix_stretchable_widget_layout:I

    const/4 v1, 0x1

    invoke-virtual {p1, v0, p0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    sget v0, Lmiuix/stretchablewidget/c;->top_view:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lmiuix/stretchablewidget/StretchableWidget;->a:Landroid/widget/RelativeLayout;

    sget v0, Lmiuix/stretchablewidget/c;->icon:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lmiuix/stretchablewidget/StretchableWidget;->d:Landroid/widget/ImageView;

    sget v0, Lmiuix/stretchablewidget/c;->start_text:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lmiuix/stretchablewidget/StretchableWidget;->b:Landroid/widget/TextView;

    sget v0, Lmiuix/stretchablewidget/c;->state_image:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lmiuix/stretchablewidget/StretchableWidget;->e:Landroid/widget/ImageView;

    sget v0, Lmiuix/stretchablewidget/c;->detail_msg_text:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lmiuix/stretchablewidget/StretchableWidget;->c:Landroid/widget/TextView;

    sget v0, Lmiuix/stretchablewidget/c;->customize_container:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lmiuix/stretchablewidget/WidgetContainer;

    iput-object v0, p0, Lmiuix/stretchablewidget/StretchableWidget;->f:Lmiuix/stretchablewidget/WidgetContainer;

    sget v0, Lmiuix/stretchablewidget/c;->button_line:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lmiuix/stretchablewidget/StretchableWidget;->g:Landroid/view/View;

    sget v0, Lmiuix/stretchablewidget/c;->top_line:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lmiuix/stretchablewidget/StretchableWidget;->h:Landroid/view/View;

    iget-object p1, p0, Lmiuix/stretchablewidget/StretchableWidget;->k:Ljava/lang/String;

    invoke-virtual {p0, p1}, Lmiuix/stretchablewidget/StretchableWidget;->setTitle(Ljava/lang/CharSequence;)V

    iget-object p1, p0, Lmiuix/stretchablewidget/StretchableWidget;->j:Landroid/content/Context;

    invoke-virtual {p0, p1, p2, p3}, Lmiuix/stretchablewidget/StretchableWidget;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iget p1, p0, Lmiuix/stretchablewidget/StretchableWidget;->m:I

    invoke-virtual {p0, p1}, Lmiuix/stretchablewidget/StretchableWidget;->a(I)Landroid/view/View;

    iget p1, p0, Lmiuix/stretchablewidget/StretchableWidget;->l:I

    invoke-virtual {p0, p1}, Lmiuix/stretchablewidget/StretchableWidget;->setIcon(I)V

    iget-object p1, p0, Lmiuix/stretchablewidget/StretchableWidget;->o:Ljava/lang/String;

    invoke-virtual {p0, p1}, Lmiuix/stretchablewidget/StretchableWidget;->setDetailMessage(Ljava/lang/CharSequence;)V

    iget-boolean p1, p0, Lmiuix/stretchablewidget/StretchableWidget;->i:Z

    invoke-virtual {p0, p1}, Lmiuix/stretchablewidget/StretchableWidget;->setState(Z)V

    iget-object p1, p0, Lmiuix/stretchablewidget/StretchableWidget;->a:Landroid/widget/RelativeLayout;

    new-instance p2, Lmiuix/stretchablewidget/h;

    invoke-direct {p2, p0}, Lmiuix/stretchablewidget/h;-><init>(Lmiuix/stretchablewidget/StretchableWidget;)V

    invoke-virtual {p1, p2}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private setContainerAmin(Z)V
    .locals 7

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    iget-object v2, p0, Lmiuix/stretchablewidget/StretchableWidget;->f:Lmiuix/stretchablewidget/WidgetContainer;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v1}, Lmiuix/animation/d;->c([Ljava/lang/Object;)Lmiuix/animation/k;

    move-result-object v1

    const-string v2, "start"

    invoke-interface {v1, v2}, Lmiuix/animation/k;->a(Ljava/lang/Object;)Lmiuix/animation/k;

    iget v4, p0, Lmiuix/stretchablewidget/StretchableWidget;->q:I

    const-string v5, "widgetHeight"

    invoke-interface {v1, v5, v4}, Lmiuix/animation/k;->a(Ljava/lang/String;I)Lmiuix/animation/k;

    sget-object v4, Lmiuix/animation/g/A;->o:Lmiuix/animation/g/A;

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-interface {v1, v4, v6}, Lmiuix/animation/k;->a(Lmiuix/animation/g/b;F)Lmiuix/animation/k;

    const-string v4, "end"

    invoke-interface {v1, v4}, Lmiuix/animation/k;->a(Ljava/lang/Object;)Lmiuix/animation/k;

    invoke-interface {v1, v5, v3}, Lmiuix/animation/k;->a(Ljava/lang/String;I)Lmiuix/animation/k;

    sget-object v5, Lmiuix/animation/g/A;->o:Lmiuix/animation/g/A;

    const/4 v6, 0x0

    invoke-interface {v1, v5, v6}, Lmiuix/animation/k;->a(Lmiuix/animation/g/b;F)Lmiuix/animation/k;

    new-array v0, v0, [Ljava/lang/Object;

    iget-object v1, p0, Lmiuix/stretchablewidget/StretchableWidget;->f:Lmiuix/stretchablewidget/WidgetContainer;

    aput-object v1, v0, v3

    invoke-static {v0}, Lmiuix/animation/d;->c([Ljava/lang/Object;)Lmiuix/animation/k;

    move-result-object v0

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    move-object v2, v4

    :goto_0
    invoke-interface {v0, v2}, Lmiuix/animation/k;->b(Ljava/lang/Object;)Lmiuix/animation/k;

    return-void
.end method


# virtual methods
.method public a(I)Landroid/view/View;
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    invoke-direct {p0, p1}, Lmiuix/stretchablewidget/StretchableWidget;->b(I)Landroid/view/View;

    move-result-object p1

    invoke-virtual {p0, p1}, Lmiuix/stretchablewidget/StretchableWidget;->setView(Landroid/view/View;)V

    return-object p1
.end method

.method protected a()V
    .locals 0

    return-void
.end method

.method protected a(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    return-void
.end method

.method public getLayout()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lmiuix/stretchablewidget/StretchableWidget;->n:Landroid/view/View;

    return-object v0
.end method

.method public setDetailMessage(Ljava/lang/CharSequence;)V
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lmiuix/stretchablewidget/StretchableWidget;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public setIcon(I)V
    .locals 1

    if-nez p1, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lmiuix/stretchablewidget/StretchableWidget;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    return-void
.end method

.method public setLayout(Landroid/view/View;)V
    .locals 0

    invoke-virtual {p0, p1}, Lmiuix/stretchablewidget/StretchableWidget;->setView(Landroid/view/View;)V

    return-void
.end method

.method public setState(Z)V
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p0, Lmiuix/stretchablewidget/StretchableWidget;->e:Landroid/widget/ImageView;

    sget v1, Lmiuix/stretchablewidget/b;->miuix_stretchable_widget_state_expand:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v0, p0, Lmiuix/stretchablewidget/StretchableWidget;->h:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lmiuix/stretchablewidget/StretchableWidget;->g:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lmiuix/stretchablewidget/StretchableWidget;->e:Landroid/widget/ImageView;

    sget v1, Lmiuix/stretchablewidget/b;->miuix_stretchable_widget_state_collapse:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v0, p0, Lmiuix/stretchablewidget/StretchableWidget;->h:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lmiuix/stretchablewidget/StretchableWidget;->g:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    invoke-direct {p0, p1}, Lmiuix/stretchablewidget/StretchableWidget;->setContainerAmin(Z)V

    return-void
.end method

.method public setStateChangedListener(Lmiuix/stretchablewidget/StretchableWidget$a;)V
    .locals 0

    iput-object p1, p0, Lmiuix/stretchablewidget/StretchableWidget;->p:Lmiuix/stretchablewidget/StretchableWidget$a;

    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lmiuix/stretchablewidget/StretchableWidget;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public setView(Landroid/view/View;)V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "WrongConstant"
        }
    .end annotation

    if-nez p1, :cond_0

    return-void

    :cond_0
    iput-object p1, p0, Lmiuix/stretchablewidget/StretchableWidget;->n:Landroid/view/View;

    instance-of v0, p1, Lmiuix/stretchablewidget/k;

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, Lmiuix/stretchablewidget/k;

    new-instance v1, Lmiuix/stretchablewidget/i;

    invoke-direct {v1, p0}, Lmiuix/stretchablewidget/i;-><init>(Lmiuix/stretchablewidget/StretchableWidget;)V

    invoke-interface {v0, v1}, Lmiuix/stretchablewidget/k;->a(Lmiuix/stretchablewidget/j;)V

    :cond_1
    iget-object v0, p0, Lmiuix/stretchablewidget/StretchableWidget;->f:Lmiuix/stretchablewidget/WidgetContainer;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lmiuix/stretchablewidget/StretchableWidget;->f:Lmiuix/stretchablewidget/WidgetContainer;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lmiuix/stretchablewidget/StretchableWidget;->f:Lmiuix/stretchablewidget/WidgetContainer;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    iget-object v0, p0, Lmiuix/stretchablewidget/StretchableWidget;->f:Lmiuix/stretchablewidget/WidgetContainer;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    :goto_0
    const/4 v0, 0x0

    invoke-static {v0, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-static {v0, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {p1, v1, v0}, Landroid/view/View;->measure(II)V

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result p1

    iput p1, p0, Lmiuix/stretchablewidget/StretchableWidget;->q:I

    invoke-virtual {p0}, Lmiuix/stretchablewidget/StretchableWidget;->a()V

    iget-boolean p1, p0, Lmiuix/stretchablewidget/StretchableWidget;->i:Z

    invoke-direct {p0, p1}, Lmiuix/stretchablewidget/StretchableWidget;->setContainerAmin(Z)V

    return-void
.end method
