.class public Lorg/hapjs/features/channel/transparentactivity/TransparentActivity;
.super Landroid/app/Activity;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x38

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-static {}, Lorg/hapjs/features/channel/transparentactivity/b;->a()Lorg/hapjs/features/channel/transparentactivity/b;

    move-result-object p1

    invoke-virtual {p1, p0}, Lorg/hapjs/features/channel/transparentactivity/b;->a(Lorg/hapjs/features/channel/transparentactivity/TransparentActivity;)V

    return-void
.end method

.method public onDestroy()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    invoke-static {}, Lorg/hapjs/features/channel/transparentactivity/b;->a()Lorg/hapjs/features/channel/transparentactivity/b;

    move-result-object v0

    invoke-virtual {v0}, Lorg/hapjs/features/channel/transparentactivity/b;->b()V

    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    invoke-static {}, Lorg/hapjs/features/channel/transparentactivity/b;->a()Lorg/hapjs/features/channel/transparentactivity/b;

    move-result-object p1

    invoke-virtual {p1, p0}, Lorg/hapjs/features/channel/transparentactivity/b;->a(Lorg/hapjs/features/channel/transparentactivity/TransparentActivity;)V

    return-void
.end method
