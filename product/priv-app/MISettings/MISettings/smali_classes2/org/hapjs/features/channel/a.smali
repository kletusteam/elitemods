.class Lorg/hapjs/features/channel/a;
.super Landroid/os/Handler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/hapjs/features/channel/b;-><init>(Lorg/hapjs/features/channel/a/a;Lorg/hapjs/features/channel/a/b;Landroid/os/HandlerThread;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lorg/hapjs/features/channel/b;


# direct methods
.method constructor <init>(Lorg/hapjs/features/channel/b;Landroid/os/Looper;)V
    .locals 0

    iput-object p1, p0, Lorg/hapjs/features/channel/a;->a:Lorg/hapjs/features/channel/b;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4

    iget v0, p1, Landroid/os/Message;->what:I

    if-eqz v0, :cond_7

    const/4 v1, 0x1

    if-eq v0, v1, :cond_5

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Lorg/hapjs/features/channel/b$b;

    iget-object v0, p0, Lorg/hapjs/features/channel/a;->a:Lorg/hapjs/features/channel/b;

    iget v1, p1, Lorg/hapjs/features/channel/b$b;->a:I

    iget-object p1, p1, Lorg/hapjs/features/channel/b$b;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lorg/hapjs/features/channel/b;->a(ILjava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lorg/hapjs/features/channel/a;->a:Lorg/hapjs/features/channel/b;

    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Ljava/lang/String;

    invoke-static {v0, p1}, Lorg/hapjs/features/channel/b;->a(Lorg/hapjs/features/channel/b;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lorg/hapjs/features/channel/a;->a:Lorg/hapjs/features/channel/b;

    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Landroid/os/Bundle;

    invoke-static {v0, p1}, Lorg/hapjs/features/channel/b;->a(Lorg/hapjs/features/channel/b;Landroid/os/Bundle;)V

    goto :goto_0

    :cond_3
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Lorg/hapjs/features/channel/b$a;

    iget-object v0, p1, Lorg/hapjs/features/channel/b$a;->d:Lorg/hapjs/features/channel/b/b;

    iget-object v1, p0, Lorg/hapjs/features/channel/a;->a:Lorg/hapjs/features/channel/b;

    iget v2, p1, Lorg/hapjs/features/channel/b$a;->a:I

    iget-object v3, p1, Lorg/hapjs/features/channel/b$a;->c:Ljava/lang/String;

    iget-boolean p1, p1, Lorg/hapjs/features/channel/b$a;->b:Z

    invoke-virtual {v1, v2, v3, p1}, Lorg/hapjs/features/channel/b;->b(ILjava/lang/String;Z)Z

    move-result p1

    if-eqz p1, :cond_4

    if-eqz v0, :cond_8

    invoke-interface {v0}, Lorg/hapjs/features/channel/b/b;->onSuccess()V

    goto :goto_0

    :cond_4
    if-eqz v0, :cond_8

    invoke-interface {v0}, Lorg/hapjs/features/channel/b/b;->a()V

    goto :goto_0

    :cond_5
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Lorg/hapjs/features/channel/b$d;

    iget-object v0, p1, Lorg/hapjs/features/channel/b$d;->b:Lorg/hapjs/features/channel/b/b;

    iget-object v1, p0, Lorg/hapjs/features/channel/a;->a:Lorg/hapjs/features/channel/b;

    iget-object p1, p1, Lorg/hapjs/features/channel/b$d;->a:Lorg/hapjs/features/channel/d;

    invoke-static {v1, p1}, Lorg/hapjs/features/channel/b;->a(Lorg/hapjs/features/channel/b;Lorg/hapjs/features/channel/d;)Z

    move-result p1

    if-eqz p1, :cond_6

    if-eqz v0, :cond_8

    invoke-interface {v0}, Lorg/hapjs/features/channel/b/b;->onSuccess()V

    goto :goto_0

    :cond_6
    if-eqz v0, :cond_8

    invoke-interface {v0}, Lorg/hapjs/features/channel/b/b;->a()V

    goto :goto_0

    :cond_7
    iget-object v0, p0, Lorg/hapjs/features/channel/a;->a:Lorg/hapjs/features/channel/b;

    invoke-virtual {v0, p1}, Lorg/hapjs/features/channel/b;->a(Landroid/os/Message;)V

    :cond_8
    :goto_0
    return-void
.end method
