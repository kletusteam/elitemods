.class public Lorg/hapjs/features/channel/transparentactivity/b;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/hapjs/features/channel/transparentactivity/b$a;
    }
.end annotation


# instance fields
.field private a:Z

.field private b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lorg/hapjs/features/channel/transparentactivity/TransparentActivity;",
            ">;"
        }
    .end annotation
.end field

.field private c:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lorg/hapjs/features/channel/transparentactivity/a;

    invoke-direct {v0, p0}, Lorg/hapjs/features/channel/transparentactivity/a;-><init>(Lorg/hapjs/features/channel/transparentactivity/b;)V

    iput-object v0, p0, Lorg/hapjs/features/channel/transparentactivity/b;->c:Landroid/os/Handler;

    return-void
.end method

.method public static a()Lorg/hapjs/features/channel/transparentactivity/b;
    .locals 1

    sget-object v0, Lorg/hapjs/features/channel/transparentactivity/b$a;->a:Lorg/hapjs/features/channel/transparentactivity/b;

    return-object v0
.end method

.method static synthetic a(Lorg/hapjs/features/channel/transparentactivity/b;)V
    .locals 0

    invoke-direct {p0}, Lorg/hapjs/features/channel/transparentactivity/b;->e()V

    return-void
.end method

.method private e()V
    .locals 2

    iget-object v0, p0, Lorg/hapjs/features/channel/transparentactivity/b;->b:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/hapjs/features/channel/transparentactivity/TransparentActivity;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/hapjs/features/channel/transparentactivity/b;->b:Ljava/lang/ref/WeakReference;

    :cond_1
    iget-object v0, p0, Lorg/hapjs/features/channel/transparentactivity/b;->c:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    return-void
.end method


# virtual methods
.method public a(Lorg/hapjs/features/channel/transparentactivity/TransparentActivity;)V
    .locals 4

    iget-boolean v0, p0, Lorg/hapjs/features/channel/transparentactivity/b;->a:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    goto :goto_1

    :cond_0
    iget-object v0, p0, Lorg/hapjs/features/channel/transparentactivity/b;->b:Ljava/lang/ref/WeakReference;

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/hapjs/features/channel/transparentactivity/TransparentActivity;

    if-eqz v0, :cond_2

    if-ne v0, p1, :cond_1

    iget-object v0, p0, Lorg/hapjs/features/channel/transparentactivity/b;->c:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lorg/hapjs/features/channel/transparentactivity/b;->e()V

    :cond_2
    :goto_0
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lorg/hapjs/features/channel/transparentactivity/b;->b:Ljava/lang/ref/WeakReference;

    iget-object p1, p0, Lorg/hapjs/features/channel/transparentactivity/b;->c:Landroid/os/Handler;

    const-wide/16 v2, 0x2710

    invoke-virtual {p1, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :goto_1
    return-void
.end method

.method public b()V
    .locals 0

    invoke-direct {p0}, Lorg/hapjs/features/channel/transparentactivity/b;->e()V

    return-void
.end method

.method public c()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/hapjs/features/channel/transparentactivity/b;->a:Z

    invoke-direct {p0}, Lorg/hapjs/features/channel/transparentactivity/b;->e()V

    return-void
.end method

.method public d()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/hapjs/features/channel/transparentactivity/b;->a:Z

    return-void
.end method
